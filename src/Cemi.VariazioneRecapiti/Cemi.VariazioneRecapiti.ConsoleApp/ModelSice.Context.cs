//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Data.Objects;
using System.Data.EntityClient;

namespace Cemi.VariazioneRecapiti.ConsoleApp
{
    public partial class SICEEntities : ObjectContext
    {
        public const string ConnectionString = "name=SICEEntities";
        public const string ContainerName = "SICEEntities";
    
        #region Constructors
    
        public SICEEntities()
            : base(ConnectionString, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        public SICEEntities(string connectionString)
            : base(connectionString, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        public SICEEntities(EntityConnection connection)
            : base(connection, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        #endregion
    
        #region ObjectSet Properties
    
        public ObjectSet<Impresa> Imprese
        {
            get { return _imprese  ?? (_imprese = CreateObjectSet<Impresa>("Imprese")); }
        }
        private ObjectSet<Impresa> _imprese;
    
        public ObjectSet<ImpresaRecapito> ImpreseRecapiti
        {
            get { return _impreseRecapiti  ?? (_impreseRecapiti = CreateObjectSet<ImpresaRecapito>("ImpreseRecapiti")); }
        }
        private ObjectSet<ImpresaRecapito> _impreseRecapiti;
    
        public ObjectSet<ImpresaRichiestaVariazioneRecapito> ImpreseRichiesteVariazioneRecapiti
        {
            get { return _impreseRichiesteVariazioneRecapiti  ?? (_impreseRichiesteVariazioneRecapiti = CreateObjectSet<ImpresaRichiestaVariazioneRecapito>("ImpreseRichiesteVariazioneRecapiti")); }
        }
        private ObjectSet<ImpresaRichiestaVariazioneRecapito> _impreseRichiesteVariazioneRecapiti;
    
        public ObjectSet<TipoRecapito> TipiRecapito
        {
            get { return _tipiRecapito  ?? (_tipiRecapito = CreateObjectSet<TipoRecapito>("TipiRecapito")); }
        }
        private ObjectSet<TipoRecapito> _tipiRecapito;
    
        public ObjectSet<TipoVia> TipiVia
        {
            get { return _tipiVia  ?? (_tipiVia = CreateObjectSet<TipoVia>("TipiVia")); }
        }
        private ObjectSet<TipoVia> _tipiVia;

        #endregion
    }
}
