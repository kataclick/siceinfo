//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.VariazioneRecapiti.ConsoleApp
{
    public partial class ImpresaRecapito
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual int IdTipoRecapito
        {
            get { return _idTipoRecapito; }
            set
            {
                if (_idTipoRecapito != value)
                {
                    if (TipiRecapito != null && TipiRecapito.Id != value)
                    {
                        TipiRecapito = null;
                    }
                    _idTipoRecapito = value;
                }
            }
        }
        private int _idTipoRecapito;
    
        public virtual string Via
        {
            get;
            set;
        }
    
        public virtual string Civico
        {
            get;
            set;
        }
    
        public virtual string Comune
        {
            get;
            set;
        }
    
        public virtual string ComuneCodiceCatastale
        {
            get;
            set;
        }
    
        public virtual string Cap
        {
            get;
            set;
        }
    
        public virtual string Provincia
        {
            get;
            set;
        }
    
        public virtual string Stato
        {
            get;
            set;
        }
    
        public virtual string InformazioniAggiuntive
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> Latitudine
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> Longitudine
        {
            get;
            set;
        }
    
        public virtual string Telefono
        {
            get;
            set;
        }
    
        public virtual string Fax
        {
            get;
            set;
        }
    
        public virtual string Email
        {
            get;
            set;
        }
    
        public virtual string Presso
        {
            get;
            set;
        }
    
        public virtual string Pec
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<ImpresaRichiestaVariazioneRecapito> ImpreseRichiesteVariazioneRecapiti
        {
            get
            {
                if (_impreseRichiesteVariazioneRecapiti == null)
                {
                    var newCollection = new FixupCollection<ImpresaRichiestaVariazioneRecapito>();
                    newCollection.CollectionChanged += FixupImpreseRichiesteVariazioneRecapiti;
                    _impreseRichiesteVariazioneRecapiti = newCollection;
                }
                return _impreseRichiesteVariazioneRecapiti;
            }
            set
            {
                if (!ReferenceEquals(_impreseRichiesteVariazioneRecapiti, value))
                {
                    var previousValue = _impreseRichiesteVariazioneRecapiti as FixupCollection<ImpresaRichiestaVariazioneRecapito>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupImpreseRichiesteVariazioneRecapiti;
                    }
                    _impreseRichiesteVariazioneRecapiti = value;
                    var newValue = value as FixupCollection<ImpresaRichiestaVariazioneRecapito>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupImpreseRichiesteVariazioneRecapiti;
                    }
                }
            }
        }
        private ICollection<ImpresaRichiestaVariazioneRecapito> _impreseRichiesteVariazioneRecapiti;
    
        public virtual TipoRecapito TipiRecapito
        {
            get { return _tipiRecapito; }
            set
            {
                if (!ReferenceEquals(_tipiRecapito, value))
                {
                    var previousValue = _tipiRecapito;
                    _tipiRecapito = value;
                    FixupTipiRecapito(previousValue);
                }
            }
        }
        private TipoRecapito _tipiRecapito;

        #endregion
        #region Association Fixup
    
        private void FixupTipiRecapito(TipoRecapito previousValue)
        {
            if (previousValue != null && previousValue.ImpreseRecapiti.Contains(this))
            {
                previousValue.ImpreseRecapiti.Remove(this);
            }
    
            if (TipiRecapito != null)
            {
                if (!TipiRecapito.ImpreseRecapiti.Contains(this))
                {
                    TipiRecapito.ImpreseRecapiti.Add(this);
                }
                if (IdTipoRecapito != TipiRecapito.Id)
                {
                    IdTipoRecapito = TipiRecapito.Id;
                }
            }
        }
    
        private void FixupImpreseRichiesteVariazioneRecapiti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ImpresaRichiestaVariazioneRecapito item in e.NewItems)
                {
                    item.ImpreseRecapiti = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ImpresaRichiestaVariazioneRecapito item in e.OldItems)
                {
                    if (ReferenceEquals(item.ImpreseRecapiti, this))
                    {
                        item.ImpreseRecapiti = null;
                    }
                }
            }
        }

        #endregion
    }
}
