﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using Cemi.VariazioneRecapiti.ConsoleApp.EmailService;

namespace Cemi.VariazioneRecapiti.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["CEMI"].ToString();
                DateTime dataConfronto = DateTime.Today.AddDays(-1);
                List<RichiestaGestita> richieste = new List<RichiestaGestita>();
                const string query =
                    "SELECT DISTINCT idImpresa, dbo.UF_DateOnly(dataRichiesta) AS dataRichiesta, idTipoRecapito FROM dbo.ImpreseRichiesteVariazioneRecapiti v INNER JOIN dbo.ImpreseRecapiti r ON v.idRecapito = r.idRecapito WHERE dataGestione = @dataGestione";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@dataGestione", dataConfronto);
                        connection.Open();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                RichiestaGestita richiesta = new RichiestaGestita
                                                                 {
                                                                     DataRichiesta = (DateTime) reader["dataRichiesta"],
                                                                     IdImpresa = (int) reader["idImpresa"],
                                                                     IdTipoRecapito = (int) reader["idTipoRecapito"]
                                                                 };
                                richieste.Add(richiesta);
                            }
                        }
                    }
                }

                foreach (RichiestaGestita richiestaGestita in richieste)
                {
                    InvioEMail(richiestaGestita.DataRichiesta, richiestaGestita.IdTipoRecapito,
                               richiestaGestita.IdImpresa);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        private static void InvioEMail(DateTime dataRichiesta, int idTipoRecapito, Int32 idImpresa)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            bool usaMailTest = true;
            string usaMailTestString = ConfigurationManager.AppSettings["UsaMailTest"];
            if (usaMailTestString == "false")
                usaMailTest = false;

            ImpresaDatiRecapiti recapito;

            using (SICEEntities context = new SICEEntities())
            {
                var queryImpreseRecapiti = from impresa in context.Imprese
                                           where impresa.Id == idImpresa
                                           select
                                               new ImpresaDatiRecapiti
                                                   {
                                                       IdImpresa = impresa.Id,
                                                       CodiceFiscale = impresa.CodiceFiscale,
                                                       RagioneSociale = impresa.RagioneSociale,
                                                       IndirizzoSedeLegale =
                                                           new Indirizzo
                                                               {
                                                                   Qualificatore = impresa.TipiViaLegale.descrizione,
                                                                   NomeVia = impresa.denominazioneIndirizzoSedeLegale,
                                                                   Civico = impresa.civicoSedeLegale,
                                                                   Cap = impresa.capSedeLegale,
                                                                   Comune = impresa.localitaSedeLegale,
                                                                   Provincia = impresa.provinciaSedeLegale
                                                               },
                                                       PressoSedeLegale = impresa.pressoSedeLegale,
                                                       TelefonoSedeLegale = impresa.telefonoSedeLegale,
                                                       FaxSedeLegale = impresa.faxSedeLegale,
                                                       EmailSedeLegale = impresa.eMailSedeLegale,
                                                       IndirizzoSedeAmministrativa =
                                                           new Indirizzo
                                                               {
                                                                   Qualificatore =
                                                                       impresa.TipiViaAmm.descrizione,
                                                                   NomeVia =
                                                                       impresa.denominazioneIndirizzoSedeAmministrativa,
                                                                   Civico = impresa.civicoSedeAmministrativa,
                                                                   Cap = impresa.capSedeAmministrazione,
                                                                   Comune = impresa.localitaSedeAmministrazione,
                                                                   Provincia = impresa.provinciaSedeAmministrazione
                                                               },
                                                       PressoSedeAmministrativa = impresa.pressoSedeAmministrazione,
                                                       TelefonoSedeAmministrativa = impresa.telefonosedeAmministrazione,
                                                       FaxSedeAmministrativa = impresa.faxSedeAmministrazione,
                                                       EmailSedeAmministrativa = impresa.eMailSedeAmministrazione,
                                                       IndirizzoCorrispondenza =
                                                           new Indirizzo
                                                               {
                                                                   Qualificatore =
                                                                       impresa.TipiViaCorrisp.descrizione,
                                                                   NomeVia =
                                                                       impresa.denominazioneIndirizzoCorrispondenza,
                                                                   Civico = impresa.civicoCorrispondenza,
                                                                   Cap = impresa.capCorrispondenza,
                                                                   Comune = impresa.localitaCorrispondenza,
                                                                   Provincia = impresa.provinciaCorrispondenza
                                                               },
                                                       PressoCorrispondenza = impresa.pressoCorrispondenza,
                                                       TelefonoCorrispondenza = impresa.telefonoCorrispondenza,
                                                       FaxCorrispondenza = impresa.faxCorrispondenza,
                                                       EmailCorrispondenza = impresa.eMailCorrispondenza,
                                                       Pec = impresa.pecSedeLegaleSiceNew
                                                   };

                recapito = queryImpreseRecapiti.SingleOrDefault();
            }


            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            email.Mittente = new EmailAddress
                                 {
                                     Nome = "Cassa Edile di Milano - Ufficio Servizi alle Imprese",
                                     Indirizzo = "noreply-datorilavoro@cassaedilemilano.it"
                                 };

            email.Oggetto = String.Format("Variazione recapiti impresa {0} - {1} eseguita", recapito.RagioneSociale, recapito.IdImpresa);

            // HTML
            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("<font face=\"Arial\" size=2>");
            sbTestoHtml.Append("Buongiorno,");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.AppendFormat("si informa che la variazione anagrafica relativa all'impresa {0} (codice iscrizione n. {1}), effettuata in data <b>", recapito.RagioneSociale, recapito.IdImpresa);
            sbTestoHtml.Append(dataRichiesta.ToShortDateString());
            sbTestoHtml.Append(
                "</b> tramite la funzione ad accesso privato \"<b>Variazione recapiti impresa</b>\" dell’area \"<b>Servizi on-line</b>\" del sito internet <a href='http://www.cassaedilemilano.it'>www.cassaedilemilano.it</a>, è stata registrata sui nostri sistemi correttamente.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            switch (idTipoRecapito)
            {
                case 1:
                    sbTestoHtml.Append("I nuovi recapiti per la sede legale sono:<br/>");
                    sbTestoHtml.Append("Indirizzo: " + recapito.IndirizzoSedeLegale.IndirizzoBase + "<br/>");
                    sbTestoHtml.Append("CAP, città, provincia: " + recapito.IndirizzoSedeLegale.Localita + "<br/>");
                    sbTestoHtml.Append("Presso: " + recapito.PressoSedeLegale + "<br/>");
                    sbTestoHtml.Append("Telefono: " + recapito.TelefonoSedeLegale + "<br/>");
                    sbTestoHtml.Append("Fax: " + recapito.FaxSedeLegale + "<br/>");
                    sbTestoHtml.Append("E-mail: " + recapito.EmailSedeLegale + "<br/>");
                    sbTestoHtml.Append("PEC: " + recapito.Pec);
                    break;
                case 2:
                    sbTestoHtml.Append("I nuovi recapiti per la sede amministrativa sono:<br/>");
                    sbTestoHtml.Append("Indirizzo: " + recapito.IndirizzoSedeAmministrativa.IndirizzoBase + "<br/>");
                    sbTestoHtml.Append("CAP, città, provincia: " + recapito.IndirizzoSedeAmministrativa.Localita + "<br/>");
                    sbTestoHtml.Append("Presso: " + recapito.PressoSedeAmministrativa + "<br/>");
                    sbTestoHtml.Append("Telefono: " + recapito.TelefonoSedeAmministrativa + "<br/>");
                    sbTestoHtml.Append("Fax: " + recapito.FaxSedeAmministrativa + "<br/>");
                    sbTestoHtml.Append("E-mail: " + recapito.EmailSedeAmministrativa + "<br/>");
                    sbTestoHtml.Append("PEC: " + recapito.Pec);
                    break;
                case 3:
                    sbTestoHtml.Append("I nuovi recapiti per la corrispondenza sono:<br/>");
                    sbTestoHtml.Append("Indirizzo: " + recapito.IndirizzoCorrispondenza.IndirizzoBase + "<br/>");
                    sbTestoHtml.Append("CAP, città, provincia: " + recapito.IndirizzoCorrispondenza.Localita + "<br/>");
                    sbTestoHtml.Append("Presso: " + recapito.PressoCorrispondenza + "<br/>");
                    sbTestoHtml.Append("Telefono: " + recapito.TelefonoCorrispondenza + "<br/>");
                    sbTestoHtml.Append("Fax: " + recapito.FaxCorrispondenza + "<br/>");
                    sbTestoHtml.Append("E-mail: " + recapito.EmailCorrispondenza + "<br/>");
                    sbTestoHtml.Append("PEC: " + recapito.Pec);
                    break;
            }
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Si ringrazia per la cortese collaborazione.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cordiali saluti.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Ufficio Servizi alle Imprese");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            sbTestoHtml.Append("</font>");


            // PLAIN
            StringBuilder sbTestoPlain = new StringBuilder();
            sbTestoPlain.Append("Buongiorno,");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.AppendFormat("si informa che la variazione anagrafica relativa all'impresa {0} (codice iscrizione n. {1}), effettuata in data ", recapito.RagioneSociale, recapito.IdImpresa);
            sbTestoPlain.Append(dataRichiesta.ToShortDateString());
            sbTestoPlain.Append(
                " tramite la funzione ad accesso privato \"Variazione recapiti impresa\" dell’area \"Servizi on-line\" del sito internet <a href='http://www.cassaedilemilano.it'>www.cassaedilemilano.it</a>, è stata registrata sui nostri sistemi correttamente.");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("<br />");
            switch (idTipoRecapito)
            {
                case 1:
                    sbTestoPlain.Append("I nuovi recapiti per la Sede Legale sono:<br/>");
                    sbTestoPlain.Append("Indirizzo: " + recapito.IndirizzoSedeLegale.IndirizzoBase + "<br/>");
                    sbTestoPlain.Append("CAP, città, provincia: " + recapito.IndirizzoSedeLegale.Localita + "<br/>");
                    sbTestoPlain.Append("Presso: " + recapito.PressoSedeLegale + "<br/>");
                    sbTestoPlain.Append("Telefono: " + recapito.TelefonoSedeLegale + "<br/>");
                    sbTestoPlain.Append("Fax: " + recapito.FaxSedeLegale + "<br/>");
                    sbTestoPlain.Append("E-mail: " + recapito.EmailSedeLegale + "<br/>");
                    sbTestoPlain.Append("PEC: " + recapito.Pec);
                    break;
                case 2:
                    sbTestoPlain.Append("I nuovi recapiti per la Sede Amministrativa sono:<br/>");
                    sbTestoPlain.Append("Indirizzo: " + recapito.IndirizzoSedeAmministrativa.IndirizzoBase + "<br/>");
                    sbTestoPlain.Append("CAP, città, provincia: " + recapito.IndirizzoSedeAmministrativa.Localita + "<br/>");
                    sbTestoPlain.Append("Presso: " + recapito.PressoSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("Telefono: " + recapito.TelefonoSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("Fax: " + recapito.FaxSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("E-mail: " + recapito.EmailSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("PEC: " + recapito.Pec);
                    break;
                case 3:
                    sbTestoPlain.Append("I nuovi recapiti per la Corrispondenza sono:<br/>");
                    sbTestoPlain.Append("Indirizzo: " + recapito.IndirizzoCorrispondenza.IndirizzoBase + "<br/>");
                    sbTestoPlain.Append("CAP, città, provincia: " + recapito.IndirizzoCorrispondenza.Localita + "<br/>");
                    sbTestoPlain.Append("Presso: " + recapito.PressoCorrispondenza + "<br/>");
                    sbTestoPlain.Append("Telefono: " + recapito.TelefonoSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("Fax: " + recapito.FaxSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("E-mail: " + recapito.EmailSedeAmministrativa + "<br/>");
                    sbTestoPlain.Append("PEC: " + recapito.Pec);
                    break;
            }
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("Si ringrazia per la cortese collaborazione.");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("Cordiali saluti.");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("Ufficio Servizi alle Imprese");
            sbTestoPlain.Append("<br />");
            sbTestoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            if (usaMailTest)
            {
                email.Destinatari = new EmailAddress[1];
                //email.Destinatari[0] = new EmailAddress
                //                           {
                //                               Indirizzo = "alessio.mosto@itsinfinity.com",
                //                               Nome = recapito.RagioneSociale
                //                           };
                email.Destinatari[0] = new EmailAddress
                                           {
                                               Indirizzo = "massimo.sonzini@cassaedilemilano.it",
                                               Nome = recapito.RagioneSociale
                                           };
            }
            else
            {
                email.Destinatari = new EmailAddress[1];
                string indirizzoDestinazione = "marco.catalano@cassaedilemilano.it";
                switch (idTipoRecapito)
                {
                    case 1:
                        indirizzoDestinazione = recapito.EmailSedeLegale;
                        break;
                    case 2:
                        indirizzoDestinazione = recapito.EmailSedeAmministrativa;
                        break;
                    case 3:
                        indirizzoDestinazione = recapito.EmailCorrispondenza;
                        break;
                }
                email.Destinatari[0] = new EmailAddress
                                           {
                                               Indirizzo = indirizzoDestinazione,
                                               Nome = recapito.RagioneSociale
                                           };
            }

            EmailInfoService client = new EmailInfoService();
            NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
            client.Credentials = credentials;
            client.InviaEmail(email);
        }
    }
}