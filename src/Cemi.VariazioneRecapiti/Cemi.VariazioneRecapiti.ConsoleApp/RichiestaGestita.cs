﻿using System;

namespace Cemi.VariazioneRecapiti.ConsoleApp
{
    public class RichiestaGestita
    {
        public Int32 IdImpresa { get; set; }
        private DateTime _dataRichiesta;
        public Impresa ImpresaGestita { get; set; }
        public Int32 IdTipoRecapito { get; set; }

        public DateTime DataRichiesta
        {
            get { return _dataRichiesta; }
            set { _dataRichiesta = new DateTime(value.Year, value.Month, value.Day); }
        }
    }
}