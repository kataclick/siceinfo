﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inserimento_ConfermaInserimentoNotifica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "auto_postback", "window.onload = function() { __doPostBack(\"" + ButtonPDFNascosto.ClientID + "\", \"first\"); }; ", true);

            CaricaAiutoETitolo();

            Int32 idNotifica = (Int32)Context.Items["IdNotifica"];
            ViewState["IdNotifica"] = idNotifica;
            ViewState["Certificato"] = Context.Items["Certificato"];
            ViewState["Password"] = Context.Items["Password"];
            CaricaNotifica(idNotifica);

            ViewState["PrimoCaricamento"] = true;
        }
        else
        {
            // Questa schifezza serve per non inviare più volte la mail
            if (Page.Request.Params[5] != "PDF")
            {
                ViewState["PrimoCaricamento"] = null;
                CaricaPDF(true);
            }
        }
    }

    private void CaricaNotifica(Int32 idNotifica)
    {
        NotificaBreve1.CaricaNotifica(idNotifica);
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "Questa pagina conferma l'avvenuta comunicazione della notifica preliminare e permette la stampa del documento in formato PDF.",
            "Ricevuta della notifica");
    }
    #endregion
    
    protected void ButtonPDF_Click(object sender, EventArgs e)
    {
        CaricaPDF(false);
    }

    private void CaricaPDF(Boolean primoInserimento)
    {
        Context.Items["IdNotifica"] = ViewState["IdNotifica"];
        if (primoInserimento)
        {
            Context.Items["PrimoInserimento"] = true;
            Context.Items["Certificato"] = ViewState["Certificato"];
            Context.Items["Password"] = ViewState["Password"];
        }
        Server.Transfer("~/Ricerca/RicevutaNotifica.aspx");
    }
}