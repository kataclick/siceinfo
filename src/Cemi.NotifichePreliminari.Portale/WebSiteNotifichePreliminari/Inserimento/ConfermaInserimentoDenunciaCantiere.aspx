﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaInserimentoDenunciaCantiere.aspx.cs" Inherits="Inserimento_ConfermaInserimentoDenunciaCantiere" %>
<%@ Register TagPrefix="uc1" TagName="NotificaBreve" Src="~/WebControls/Visualizzazione/NotificaBreve.ascx" %>

<%@ Register src="../WebControls/ConfermaDenunciaCantiere.ascx" tagname="ConfermaDenunciaCantiere" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <p>
        La comunicazione della <b>denuncia</b> è andata a buon fine.
    </p>
    <p>
        Siete pregati di stampare e conservare il documento che vi viene fornito in formato PDF come ricevuta dell'avvenuta denuncia.
        Una copia di tale documento vi sarà recapita tramite l'indirizzo <b>email</b> fornito in fase di registrazione.
    </p>
    <p>
        <asp:Button
            ID="ButtonPDF"
            runat="server"
            Width="150px"
            Text="PDF" onclick="ButtonPDF_Click" />
        <asp:Button
            ID="ButtonPDFNascosto"
            runat="server"
            Width="150px"
            Text="PDF" 
            Visible="false" />
    </p>
    <uc2:ConfermaDenunciaCantiere 
            ID="ConfermaDenunciaCantiere1" runat="server" />
    <br />
    <br />
</asp:Content>


