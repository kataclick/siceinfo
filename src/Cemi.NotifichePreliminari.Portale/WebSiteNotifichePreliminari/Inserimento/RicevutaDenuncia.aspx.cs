using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RicevutaDenuncia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CaricaAiutoETitolo();
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "In questa pagina � possibile stampare la ricevuta della denuncia cliccando sull'icona della stampante posta nella barra sopra alla ricevuta.<br /><br />E' possibile anche esportarla in diversi formati, ad esempio PDF, CSV,...",
            "Ricevuta della denuncia");
    }
    #endregion
}
