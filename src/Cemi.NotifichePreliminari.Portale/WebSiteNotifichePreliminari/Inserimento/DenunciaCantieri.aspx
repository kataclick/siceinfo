﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DenunciaCantieri.aspx.cs" Inherits="Inserimento_DenunciaCantieri" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="../WebControls/Visualizzazione/UltimeDenunce.ascx" tagname="UltimeDenunce" tagprefix="uc1" %>

<%@ Register src="../WebControls/Ricerca/Cantiere.ascx" tagname="Cantiere" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <uc1:UltimeDenunce ID="UltimeDenunce1" runat="server" />
    <br />
    <b>Prima di inserire un nuovo cantiere</b>,  se il cantiere di cui si vuole effettuare la denuncia non compare nell'elenco precedente, <b>effettuare una ricerca</b> per codice cantiere (ad esempio LU/2012/00001), per codice fiscale del committente oppure indicarne l'indirizzo nella casella sottostante e premere il pulsante "Geocodifica".
    <br />
    <br />
    Qualora anche la ricerca per indirizzo dia esito negativo, la procedura proporrà l’indirizzo inserito come indirizzo del nuovo cantiere.
    <br />
    <br />
        <asp:MultiView
            ID="MultiViewCantieri"
            runat="server"
            ActiveViewIndex="0">
            <asp:View
                ID="ViewRicerca"
                runat="server">
                <b>
                    Ricerca
                </b>
                <div class="tabellaConBordo">
                <br />
                <uc2:Cantiere ID="Cantiere1" runat="server" />
                </div>
            </asp:View>
            <asp:View
                ID="ViewCantieri"
                runat="server">
                <b>
                    Ricerca
                </b>
                <table class="tabellaConBordo" width="100%">
                    <tr>
                        <td>
                            <asp:GridView
                                ID="GridViewDenunce"
                                runat="server"
                                AutoGenerateColumns="False"
                                Width="100%" CssClass="tabellaConBordo" DataKeyNames="IdDenunciaCantiere" 
                                onrowcommand="GridViewDenunce_RowCommand" 
                                onrowdatabound="GridViewDenunce_RowDataBound"
                                AllowPaging="true"
                                PageSize="5" onpageindexchanging="GridViewDenunce_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="Data" DataField="Data" 
                                        DataFormatString="{0:dd/MM/yyyy}">
                                    <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Codice" DataField="CodiceCantiere">
                                    <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Ruolo" DataField="Ruolo">
                                    <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Cantiere">
                                        <ItemTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="2">
                                                        <b><asp:Label ID="LabelIndirizzo" runat="server"></asp:Label></b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 120px">
                                                        Data inizio lavori:
                                                    </td>
                                                    <td>
                                                        <b><asp:Label ID="LabelDataInizioLavori" runat="server"></asp:Label></b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 120px">
                                                        Committente:
                                                    </td>
                                                    <td>
                                                        <b><asp:Label ID="LabelCommittente" runat="server"></asp:Label></b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton
                                                ID="ImageButtonDettaglio"
                                                runat="server" 
                                                ImageUrl="~/Images/dettagliNotifica32.png"
                                                CommandName="dettaglio"
                                                CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                ToolTip="Dettaglio" />
                                        </ItemTemplate>
                                        <ItemStyle Width="10px" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <br />
                                    Non è stata trovata una denuncia corrispondente ai criteri di ricerca utilizzati. 
                                    <br />
                                    Se è stata fatta una ricerca per indirizzo e si vuole inserire una nuova denuncia premere il tasto "Nuovo cantiere".
                                    <br />
                                    <br />
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <asp:Button
                                ID="ButtonTornaRicerca"
                                runat="server"
                                Text="Torna alla Ricerca" 
                                onclick="ButtonTornaRicerca_Click"
                                Width="150px" />
                            &nbsp;
                            <asp:Button
                                ID="ButtonNuovo"
                                runat="server"
                                Text="Nuovo cantiere"
                                Width="150px" 
                                onclick="ButtonNuovo_Click"
                                ToolTip="Selezionare se il cantiere che si vuole denunciare non è tra quelli mostrati" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    <br />
</asp:Content>

