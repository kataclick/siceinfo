﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.Geocode.Type;

public partial class Inserimento_InserimentoDenunciaCantiere : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            if (Context.Items["IdCantiere"] == null)
            {
                tabellaDenunciaNonTrovata.Visible = true;
                Indirizzo indirizzo = (Indirizzo) Context.Items["Indirizzo"];
                Denuncia1.CaricaDatiImpresa(indirizzo);
            }
            else
            {
                tabellaDenunciaNonTrovata.Visible = false;
                Int32 idDenuncia = (Int32) Context.Items["IdCantiere"];
                Denuncia1.CaricaDenuncia(idDenuncia);
            }
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage) this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp("In questa pagina è possibile visualizzare la denuncia di un cantiere",
                "Denuncia cantiere");
    }
    #endregion
}