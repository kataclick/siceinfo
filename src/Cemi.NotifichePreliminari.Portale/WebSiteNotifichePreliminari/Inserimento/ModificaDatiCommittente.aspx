﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModificaDatiCommittente.aspx.cs" Inherits="Inserimento_ModificaDatiCommittente" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="../WebControls/Visualizzazione/Committente.ascx" tagname="Committente" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <asp:Panel
        ID="PanelCommittente"
        runat="server"
        Visible="false">
        <uc1:Committente ID="Committente1" runat="server" />
        <br />
        <table>
            <tr>
                <td>
                    <asp:Button
                        ID="ButtonModificaDati"
                        runat="server"
                        Text="Salva"
                        Width="150px" 
                        onclick="ButtonModificaDati_Click" 
                        ValidationGroup="stop" />
                </td>
                <td>
                    <asp:Label ID="LabelMessaggio" runat="server" CssClass="messaggiErrore"></asp:Label>
                    <asp:ValidationSummary
                        ID="ValidationSummaryErrori"
                        runat="server"
                        ValidationGroup="stop"
                        CssClass="messaggiErrore" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

