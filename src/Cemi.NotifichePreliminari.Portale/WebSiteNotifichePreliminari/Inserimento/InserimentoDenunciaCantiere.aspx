﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InserimentoDenunciaCantiere.aspx.cs" Inherits="Inserimento_InserimentoDenunciaCantiere"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/WebControls/Ricerca/Cantiere.ascx" TagName="Cantiere" TagPrefix="uc1" %>
<%@ Register src="../WebControls/Denuncia.ascx" tagname="Denuncia" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro"
    runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" runat="Server">
    <br />
    <table id="tabellaDenunciaNonTrovata" runat="server" class="tabellaConBordo" width="100%">
        <tr>
            <td>
                <b>
                <asp:Label
                    ID="LabelDenunciaNonTrovata"
                    runat="server"
                    Text="Non è stata trovata una denuncia corrispondente all'indirizzo dichiarato. Proseguire con l'inserimento di una nuova denuncia.">
                </asp:Label>
                </b>
            </td>
        </tr>
    </table>
    <br />
    <table class="tabellaConBordo" width="100%">
        <tr>
            <td>
                <b>
                    Attenzione!! La denuncia non verrà salvata fino a quando non si effettuerà la conferma della stessa.
                </b>
            </td>
        </tr>
    </table>
    <br />
    <uc2:Denuncia ID="Denuncia1" runat="server" />
    <br />
    <br />
</asp:Content>
