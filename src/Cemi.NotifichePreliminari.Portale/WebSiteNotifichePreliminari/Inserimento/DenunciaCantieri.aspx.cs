﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.Geocode.Type;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.NotifichePreliminari.Portale.Type.Filters;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class Inserimento_DenunciaCantieri : System.Web.UI.Page
{
    private const Int32 INDICERICERCA = 0;
    private const Int32 INDICERISULTATI = 1;

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        Cantiere1.OnIndirizzoSelected += new IndirizzoSelectedEventHandler(SelezioneIndirizzoLibero1_OnIndirizzoSelected);
        Cantiere1.OnCodiceSelected += new CodiceSelectedEventHandler(Cantiere1_OnCodiceSelected);
        Cantiere1.OnCodiceFiscaleSelected += new CodiceSelectedEventHandler(Cantiere1_OnCodiceFiscaleSelected);

        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            CaricaUltimiCantieri();
        }
    }

    private void CaricaUltimiCantieri()
    {
        // Recupero il codice fiscale dell'azienda/utente
        String codiceFiscale = String.Empty;

        UltimeDenunce1.CaricaCantieri(codiceFiscale);
    }

    void SelezioneIndirizzoLibero1_OnIndirizzoSelected(Indirizzo indirizzo)
    {
        ButtonNuovo.Visible = true;
        ViewState["Indirizzo"] = indirizzo;
        CaricaPerIndirizzo(0);        
    }

    void Cantiere1_OnCodiceSelected(String codice)
    {
        ButtonNuovo.Visible = false;
        ViewState["Codice"] = codice;
        CaricaPerCodice(0);
    }

    void Cantiere1_OnCodiceFiscaleSelected(String codice)
    {
        ButtonNuovo.Visible = false;
        ViewState["CodiceFiscale"] = codice;
        CaricaPerCodiceFiscale(0);
    }

    private void CaricaPerIndirizzo(Int32 pagina)
    {
        ViewState["UltimoCaricamento"] = "INDIRIZZO";
        CantiereFilter filtro = new CantiereFilter()
        {
            Indirizzo = ViewState["Indirizzo"] as Indirizzo,
            CodiceFiscaleImpresa = biz.GetUtenteNotifiche().AziendaCodiceFiscale
        };

        CantiereCollection cantieri = biz.GetCantieri(filtro);
        //if (cantieri.Count == 0)
        //{
        //    Context.Items["Indirizzo"] = ViewState["Indirizzo"] as Indirizzo;
        //    Server.Transfer("~/Inserimento/InserimentoDenunciaCantiere.aspx");
        //}
        //else
        //{
            MultiViewCantieri.ActiveViewIndex = INDICERISULTATI;
            Presenter.CaricaElementiInGridView(
                GridViewDenunce,
                cantieri,
                pagina);
        //}
    }

    private void CaricaPerCodice(Int32 pagina)
    {
        ViewState["UltimoCaricamento"] = "CODICE";
        CantiereFilter filtro = new CantiereFilter()
        {
            Codice = ViewState["Codice"] as String,
            CodiceFiscaleImpresa = biz.GetUtenteNotifiche().AziendaCodiceFiscale
        };

        CantiereCollection cantieri = biz.GetCantieri(filtro);
        MultiViewCantieri.ActiveViewIndex = INDICERISULTATI;
        Presenter.CaricaElementiInGridView(
            GridViewDenunce,
            cantieri,
            pagina);
    }

    private void CaricaPerCodiceFiscale(Int32 pagina)
    {
        ViewState["UltimoCaricamento"] = "CODICEFISCALE";
        CantiereFilter filtro = new CantiereFilter()
        {
            CodiceFiscaleCommittente = ViewState["CodiceFiscale"] as String,
            CodiceFiscaleImpresa = biz.GetUtenteNotifiche().AziendaCodiceFiscale
        };

        CantiereCollection cantieri = biz.GetCantieri(filtro);
        MultiViewCantieri.ActiveViewIndex = INDICERISULTATI;
        Presenter.CaricaElementiInGridView(
            GridViewDenunce,
            cantieri,
            pagina);
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage) this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp("In questa pagina è possibile visionare le ultime denunce inserite, cercarne una inserita nel passato o inserirne una nuova.",
                "Denuncia cantieri");
    }
    #endregion

    protected void ButtonTornaRicerca_Click(object sender, EventArgs e)
    {
        MultiViewCantieri.ActiveViewIndex = INDICERICERCA;
        Cantiere1.Reset();
    }

    protected void GridViewDenunce_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 indice = -1;

        switch (e.CommandName)
        {
            case "dettaglio":
                indice = Int32.Parse(e.CommandArgument.ToString());
                Context.Items["IdCantiere"] = (Int32) GridViewDenunce.DataKeys[indice].Values["IdDenunciaCantiere"];
                Server.Transfer("~/Inserimento/InserimentoDenunciaCantiere.aspx");
                break;
        }
    }

    protected void GridViewDenunce_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Cantiere cantiere = (Cantiere) e.Row.DataItem;

            Label lIndirizzo = (Label) e.Row.FindControl("LabelIndirizzo");
            Label lDataInizioLavori = (Label) e.Row.FindControl("LabelDataInizioLavori");
            Label lCommittente = (Label) e.Row.FindControl("LabelCommittente");

            lIndirizzo.Text = cantiere.IndirizzoCantiere.IndirizzoCompleto;
            if (cantiere.DataInizioLavori.HasValue)
            {
                lDataInizioLavori.Text = cantiere.DataInizioLavori.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                lDataInizioLavori.Text = "--";
            }
            if (cantiere.Committente != null)
            {
                lCommittente.Text = cantiere.Committente.ToString();
            }
            else
            {
                lCommittente.Text = "--";
            }
        }
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        Context.Items["Indirizzo"] = ViewState["Indirizzo"];
        Server.Transfer("~/Inserimento/InserimentoDenunciaCantiere.aspx");
    }

    protected void GridViewDenunce_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        String ultimoCaricamento = ViewState["UltimoCaricamento"] as String;
        switch (ultimoCaricamento)
        {
            case "CODICE":
                CaricaPerCodice(e.NewPageIndex);
                break;
            case "CODICEFISCALE":
                CaricaPerCodiceFiscale(e.NewPageIndex);
                break;
            case "INDIRIZZO":
                CaricaPerIndirizzo(e.NewPageIndex);
                break;
        }
    }
}