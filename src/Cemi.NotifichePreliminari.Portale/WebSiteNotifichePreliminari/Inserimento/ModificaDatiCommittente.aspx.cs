using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.Presenter;

public partial class Inserimento_ModificaDatiCommittente : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                UtenteNotificheTelematiche utente = biz.GetUtenteTelematiche((Guid)user.ProviderUserKey);

                if (utente != null && utente.CommittenteTelematiche != null)
                {
                    CaricaDatiCommittente(utente.CommittenteTelematiche);
                }
            }
        }
    }

    private void CaricaDatiCommittente(CommittenteNotificheTelematiche committente)
    {
        PanelCommittente.Visible = true;
        Committente1.CaricaDati(committente);
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "In questa pagina � possibile modificare alcuni dati dell'utente",
            "Modifica Dati Committente");
    }
    #endregion

    protected void ButtonModificaDati_Click(object sender, EventArgs e)
    {
        Presenter.SvuotaCampo(LabelMessaggio);

        if (Page.IsValid)
        {
            CommittenteNotificheTelematiche committente = Committente1.CreaCommittente();

            if (biz.UpdateCommittenteTelematiche(committente))
            {
                LabelMessaggio.Text = "Dati aggiornati con successo.";
            }
            else
            {
                LabelMessaggio.Text = "Errore durante l'aggiornamento.";
            }
        }
    }
}
