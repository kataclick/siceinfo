<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InserimentoNotifica.aspx.cs" Inherits="InserimentoNotifica" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="~/WebControls/DatiGenerali.ascx" tagname="DatiGenerali" tagprefix="uc1" %>
<%@ Register src="~/WebControls/Ricerca/Committente.ascx" tagname="Committente" tagprefix="uc2" %>
<%@ Register src="~/WebControls/DateNumeri.ascx" tagname="DateNumeri" tagprefix="uc3" %>
<%@ Register src="~/WebControls/Ricerca/Impresa.ascx" tagname="Impresa" tagprefix="uc4" %>
<%@ Register src="~/WebControls/ResponsabileDeiLavori.ascx" tagname="ResponsabileDeiLavori" tagprefix="uc6" %>
<%@ Register src="~/WebControls/CoordinatoreSicurezzaProgettazione.ascx" tagname="CoordinatoreSicurezzaProgettazione" tagprefix="uc7" %>
<%@ Register src="~/WebControls/CoordinatoreSicurezzaEsecuzione.ascx" tagname="CoordinatoreSicurezzaEsecuzione" tagprefix="uc8" %>

<%@ Register src="~/WebControls/ImpreseAffidatarie.ascx" tagname="ImpreseAffidatarie" tagprefix="uc5" %>

<%@ Register src="~/WebControls/ImpreseEsecutrici.ascx" tagname="ImpreseEsecutrici" tagprefix="uc9" %>

<%@ Register src="~/WebControls/Riassunto.ascx" tagname="Riassunto" tagprefix="uc10" %>

<%@ Register src="~/WebControls/Visualizzazione/NotificaInAggiornamento.ascx" tagname="NotificaInAggiornamento" tagprefix="uc11" %>

<%@ Register src="../WebControls/Destinatari.ascx" tagname="Destinatari" tagprefix="uc12" %>

<%@ Register src="../WebControls/FirmaDigitale.ascx" tagname="FirmaDigitale" tagprefix="uc13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <asp:Panel 
        ID="PanelAggiornamento"
        runat="server"
        Visible="false">
        
        <uc11:NotificaInAggiornamento ID="NotificaInAggiornamento1" runat="server" />
        
        <br />
    </asp:Panel>
    <span id="spanObbligatori" runat="server">
        I campi contrassegnati da <b>*</b> sono obbligatori
    </span>
    <br />
    <br />
    <b>
        <asp:Label
            ID="LabelNoInserimento"
            runat="server"
            Text="Non � pi� possibile utilizzare lo sportello per inserire nuove notifiche."
            Visible="false">
        </asp:Label>
    </b>
    <center class="passi">
        <table class="tabellaConBordo" id="tabellaPassi" runat="server">
            <tr>
                <td>
                    Passo 
                    <b>
                        <asp:Label
                            ID="LabelPasso"
                            runat="server">
                        </asp:Label>
                    </b>
                    di
                    <b>
                        <asp:Label
                            ID="LabelTotalePassi"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
        </table>
    </center>
    <br />
    <asp:Wizard ID="WizardInserimentoNotifica" runat="server" Width="100%" 
        ActiveStepIndex="0" CssClass="wizard"
        onactivestepchanged="WizardInserimentoNotifica_ActiveStepChanged" 
        onfinishbuttonclick="WizardInserimentoNotifica_FinishButtonClick" 
        onnextbuttonclick="WizardInserimentoNotifica_NextButtonClick" 
        onsidebarbuttonclick="WizardInserimentoNotifica_SideBarButtonClick" >
        <SideBarTemplate>
            <asp:DataList ID="SideBarList" runat="server">
                <ItemTemplate>
                    <asp:LinkButton ID="SideBarButton" runat="server"></asp:LinkButton>
                </ItemTemplate>
                <SelectedItemStyle Font-Bold="True" />
            </asp:DataList>
        </SideBarTemplate>
        <StepStyle VerticalAlign="Top" />
        <StartNavigationTemplate>
            <asp:Button ID="btnSuccessivoStart" runat="server" Text="Avanti" CommandName="MoveNext"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StepNavigationTemplate>
        <SideBarStyle CssClass="wizardSideBar" />
        <FinishNavigationTemplate>
            <asp:Button ID="btnPrecedenteFinish" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Conferma"
                Width="150px" TabIndex="1" ValidationGroup="stop" />
        </FinishNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep ID="datiGenerali" runat="server" StepType="Start" 
                Title="- Dati Generali">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Dati generali
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:DatiGenerali ID="DatiGenerali1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryDatiGenerali"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="committente" runat="server" StepType="Step" 
                Title="- Committente">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Committente
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <uc2:Committente ID="Committente1" runat="server" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryCommittente"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="responsabileLavori" runat="server" StepType="Step" 
                Title="- Responsabile dei Lavori">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Responsabile dei Lavori
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc6:ResponsabileDeiLavori ID="ResponsabileDeiLavori1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryResponsabileDeiLavori"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep runat="server" 
                Title="- Coord. Sicurezza durante la Progettazione" StepType="Step" 
                ID="coordinatoreSicurezzaProgettazione">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Coordinatore della Sicurezza durante la Progettazione
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc7:CoordinatoreSicurezzaProgettazione ID="CoordinatoreSicurezzaProgettazione1" 
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryCoordinatoreSicurezzaProgettazione"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep runat="server" Title="- Coord. Sicurezza durante l'Esecuzione" 
                StepType="Step" ID="coordinatoreSicurezzaEsecuzione">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Coordinatore della Sicurezza durante l&#39;Esecuzione
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc8:CoordinatoreSicurezzaEsecuzione ID="CoordinatoreSicurezzaEsecuzione1" 
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryCoordinatoreSicurezzaEsecuzione"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="dateNumeri" runat="server" StepType="Step" 
                Title="- Date e Numeri">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Date e numeri
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc3:DateNumeri ID="DateNumeri1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryDateNumeri"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="impreseAffidatarie" runat="server" StepType="Step" 
                Title="- Imprese Affidatarie">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Imprese Affidatarie
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc5:ImpreseAffidatarie ID="ImpreseAffidatarie1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryImpreseAffidatarie"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="impreseEsecutrici" runat="server" 
                Title="- Imprese Esecutrici">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Imprese Esecutrici
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc9:ImpreseEsecutrici ID="ImpreseEsecutrici1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="destinatari" runat="server" 
                Title="- Destinatari">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Destinatari della notifica
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <uc12:Destinatari ID="Destinatari1" runat="server" />
                            <asp:CustomValidator
                                ID="CustomValidatorDestinatari"
                                runat="server"
                                ValidationGroup="stop"
                                ErrorMessage="Deve essere selezionato almeno un destinatario per tipologia: ASL, Cassa Edile, DTL, INPS e INAIL (se presente nella provincia del cantiere)" 
                                onservervalidate="CustomValidatorDestinatari_ServerValidate">
                                &nbsp;
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryDestinatari"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="conferma" runat="server" StepType="Finish" 
                Title="- Conferma">
                <table width="100%">
                    <tr>
                        <td>
                            <b>
                                Conferma
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc10:Riassunto ID="Riassunto1" runat="server" />
                            <br />
                            <br />
                            <uc13:FirmaDigitale ID="FirmaDigitale1" runat="server" />
                            <asp:CustomValidator
                                ID="CustomValidatorFirmaDigitale"
                                runat="server"
                                ValidationGroup="stop"
                                ErrorMessage="Non � stato fornito un certificato digitale" 
                                OnServerValidate="CustomValidatorFirmaDigitale_ServerValidate">
                                *
                            </asp:CustomValidator>
                            <asp:CustomValidator
                                ID="CustomValidatorFirmaDigitaleValidita"
                                runat="server"
                                ValidationGroup="stop"
                                ErrorMessage="Il certificato digitale fornito non � valido o la password � errata"
                                OnServerValidate="CustomValidatorFirmaDigitaleValidita_ServerValidate">
                                *
                            </asp:CustomValidator>
                            <br />
                            <br />
                            <asp:CheckBox ID="CheckBoxPianoSicurezza" runat="server" Text="Ai sensi dell'art. 82 c.9 della LRT 1/05 con la presente notifica preliminare si attesta che � stato redatto il piano di sicurezza e coordinamento ex art. 100 del D.Lgs. 81/08 e che � rispettata la LRT 64/03 (Norme per la prevenzione delle cadute dall'alto nei cantieri edili)." />
                            <br />
                            <br />
                            <asp:CheckBox ID="CheckBoxDisclaimer" runat="server" />
                            <a ID="LinkButtonDisclaimer" runat="server" href="~/RisorseStatiche/Privacy.pdf">Dichiaro di aver preso visione dell'informativa sulla privacy e presto il consenso per il trattamento dei dati personali nei termini indicati nella stessa.</a>
                            <br />
                            <asp:Label
                                ID="LabelConfermaObbligatoriaDisclaimer"
                                runat="server"
                                Visible="false"
                                Text="Per proseguire � obbligatorio autorizzare il trattamento dei dati e attestare la redazione del piano di sicurezza. Se era stato fornito un certificato per la firma digitale del documento e la relativa password per ragioni di sicurezza devono essere nuovamente forniti."
                                CssClass="messaggiErrore">
                            </asp:Label>
                            <br />
                            <br />
                            <b>ATTENZIONE</b>: IL DOCUMENTO INSERITO NON VALE COME COMUNICAZIONE DI NUOVO LAVORO DA INVIARE ALL�INAIL.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryConferma"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                            <asp:Label
                                ID="LabelMessaggio"
                                runat="server"
                                Text="La notifica � gi� stata comunicata. Per comunicare una nuova notifica scegliere la voce Notifica Preliminare dal menu."
                                Visible="false"
                                CssClass="messaggiErrore">
                            </asp:Label>
                            <asp:Label
                                ID="LabelErrore"
                                runat="server"
                                Text="Si � verificato un errore. Riprovare pi� tardi."
                                Visible="false"
                                CssClass="messaggiErrore">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
        </WizardSteps>
    </asp:Wizard>
    <asp:Button
        ID="ButtonSalvaTemporaneo"
        runat="server"
        Text="Salva temporaneamente" onclick="ButtonSalvaTemporaneo_Click" 
    Visible="False" />
</asp:Content>

