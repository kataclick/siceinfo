﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AggiornamentoNotifica.aspx.cs" Inherits="AggiornamentoNotifica" %>

<%@ Register src="~/WebControls/Ricerca/Notifica.ascx" tagname="Notifica" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <br />
    <br />
    <b>
        <asp:Label
            ID="LabelNoAggiornamento"
            runat="server"
            Text="Non è più possibile utilizzare lo sportello per aggiornare le notifiche."
            Visible="false">
        </asp:Label>
    </b>
    <uc1:Notifica ID="Notifica1" runat="server" />
</asp:Content>

