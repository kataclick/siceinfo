﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaInserimentoNotifica.aspx.cs" Inherits="Inserimento_ConfermaInserimentoNotifica" %>

<%@ Register src="../WebControls/Visualizzazione/NotificaBreve.ascx" tagname="NotificaBreve" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <p>
        La comunicazione della <b>notifica</b> preliminare è andata a buon fine.
    </p>
    <p>
        Siete pregati di stampare e conservare il documento che vi viene fornito in formato PDF come ricevuta dell'avvenuta notifica.
        Una copia di tale documento vi sarà recapita tramite l'indirizzo <b>email</b> fornito in fase di registrazione.
    </p>
    <p>
        <asp:Button
            ID="ButtonPDF"
            runat="server"
            Width="150px"
            Text="PDF" onclick="ButtonPDF_Click" />
        <asp:Button
            ID="ButtonPDFNascosto"
            runat="server"
            Width="150px"
            Text="PDF" 
            Visible="false" />
    </p>
    <uc1:NotificaBreve ID="NotificaBreve1" runat="server" />
    <br />
    <br />
</asp:Content>

