﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Business;

public partial class Inserimento_ConfermaInserimentoDenunciaCantiere : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "auto_postback", "window.onload = function() { __doPostBack(\"" + ButtonPDFNascosto.ClientID + "\", \"first\"); }; ", true);

            CaricaAiutoETitolo();

            //Int32 idDenuncia = (Int32) Context.Items["IdDenuncia"];
            Int32 idDenuncia = Int32.Parse(Request.QueryString["IdDenuncia"]);
            ViewState["IdDenuncia"] = idDenuncia;
            CaricaDenuncia(idDenuncia);

            ViewState["PrimoCaricamento"] = true;
        }
        else
        {
            // Questa schifezza serve per non inviare più volte la mail
            if (Page.Request.Params[6] != "PDF")
            {
                ViewState["PrimoCaricamento"] = null;
                CaricaPDF(true);
            }
        }
    }

    private void CaricaDenuncia(Int32 idDenuncia)
    {
        Cantiere denuncia = biz.GetCantiere(idDenuncia);

        ConfermaDenunciaCantiere1.CaricaRiepilogoDenuncia(denuncia);
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage) this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp("Questa pagina conferma l'avvenuta comunicazione della denuncia e permette la stampa del documento in formato PDF.",
                "Ricevuta della denuncia");
    }
    #endregion

    private void CaricaPDF(Boolean primoInserimento)
    {
        Context.Items["IdDenuncia"] = ViewState["IdDenuncia"];
        if (primoInserimento)
        {
            Context.Items["PrimoInserimento"] = true;
        }
        Server.Transfer("~/Inserimento/RicevutaDenuncia.aspx");
    }

    protected void ButtonPDF_Click(object sender, EventArgs e)
    {
        CaricaPDF(false);
    }
}