﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CambiaPassword.aspx.cs" Inherits="Comuni_CambiaPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <asp:ChangePassword ID="ChangePassword2" runat="server" 
    ChangePasswordButtonText="Cambia Password" 
    ChangePasswordFailureText="Password sbagliata o nuova Password non valida. La lunghezza minima della nuova password deve essere di {0} caratteri. Devono essere utilizzati almeno {1} caratteri non alfanumerici." 
    ChangePasswordTitleText="Cambia Password" 
    ConfirmNewPasswordLabelText="Ridigita la nuova Password:" 
    ConfirmPasswordCompareErrorMessage="Le due Password digitate devono essere identiche." 
    ConfirmPasswordRequiredErrorMessage="Ridigitare la Password." 
    NewPasswordLabelText="Nuova Password:" 
    NewPasswordRegularExpressionErrorMessage="Immettere una Password diversa." 
    NewPasswordRequiredErrorMessage="Digitare una nuova Password." 
    PasswordRequiredErrorMessage="Password obbligatoria." 
    SuccessText="La Password è stata modificata!" 
    SuccessTitleText="Cambio Password completato" UserNameLabelText="Nome Utente:" 
    UserNameRequiredErrorMessage="Digitare il Nome Utente." 
    CancelButtonText="Annulla" CancelDestinationPageUrl="~/Default.aspx" 
    Width="100%" ContinueButtonText="Torna alla Home Page" 
    ContinueDestinationPageUrl="~/Default.aspx" CssClass="wizard">
    <TitleTextStyle CssClass="wizardNoLink" />
</asp:ChangePassword>
</asp:Content>

