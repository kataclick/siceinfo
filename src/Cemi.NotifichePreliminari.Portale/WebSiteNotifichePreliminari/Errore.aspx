﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Errore.aspx.cs" Inherits="Errore" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <br />
    <center>
        Spiacenti, si è verificato un <b>errore</b>.
        <br />
        <br />
        Se l'operazione che si stava effettuando era la compilazione di una notifica i dati potrebbero essere stati persi. Verificare nell'area apposita se la notifica è presente a sistema, altrimenti reinserirla.
    </center>
</asp:Content>

