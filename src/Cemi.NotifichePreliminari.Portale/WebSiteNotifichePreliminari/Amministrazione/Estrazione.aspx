﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Estrazione.aspx.cs" Inherits="Amministrazione_Estrazione" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    Cliccando sul pulsante è possibile estrarre in formato <b>ZIP</b> tutte le notifiche presenti nell'anagrafica.
    <br />
    <br />
    <asp:Button ID="ButtonEstrai" runat="server" Text="Estrazione notifiche" Width="150px" 
        onclick="ButtonEstrai_Click" />
    <br />
    <br />
    Cliccando sul pulsante è possibile estrarre in formato <b>ZIP</b> tutte le denunce presenti nell'anagrafica.
    <br />
    <br />
    <asp:Button ID="ButtonEstraiDenunce" runat="server" Text="Estrazione denunce" 
        Width="150px" onclick="ButtonEstraiDenunce_Click" />
</asp:Content>

