﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

public partial class Amministrazione_Estrazione : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "In questa pagina è possibile effettuare un'estrazione delle notifiche preliminari presenti nella base dati.",
            "Estrazione");
    }
    #endregion

    protected void ButtonEstrai_Click(object sender, EventArgs e)
    {
        String notifiche;
        String indirizzi;
        String affidatarie;
        String esecutrici;

        Estrazione.EstraiTutto(out notifiche, out indirizzi, out affidatarie, out esecutrici);
        
        using (MemoryStream outputFile = new MemoryStream())
        {
            byte[] binaryNotifiche;
            binaryNotifiche = System.Text.Encoding.Unicode.GetBytes(notifiche);
            //binaryNotifiche = System.Text.Encoding.ASCII.GetBytes(notifiche);
            byte[] binaryIndirizzi;
            binaryIndirizzi = System.Text.Encoding.Unicode.GetBytes(indirizzi);
            //binaryIndirizzi = System.Text.Encoding.ASCII.GetBytes(indirizzi);
            byte[] binaryAffidatarie;
            binaryAffidatarie = System.Text.Encoding.Unicode.GetBytes(affidatarie);
            //binaryAffidatarie = System.Text.Encoding.ASCII.GetBytes(affidatarie);
            byte[] binaryEsecutrici;
            binaryEsecutrici = System.Text.Encoding.Unicode.GetBytes(esecutrici);
            //binaryEsecutrici = System.Text.Encoding.ASCII.GetBytes(esecutrici);

            using (ZipOutputStream zipStream = new ZipOutputStream(outputFile))
            {
                // Scrittura file delle notifiche
                ZipEntry notificheFile = new ZipEntry("NOTIFICHE.CSV");
                zipStream.PutNextEntry(notificheFile);
                zipStream.Write(binaryNotifiche, 0, binaryNotifiche.Length);

                // Scrittura file degli indirizzi
                ZipEntry indirizziFile = new ZipEntry("INDIRIZZI.CSV");
                zipStream.PutNextEntry(indirizziFile);
                zipStream.Write(binaryIndirizzi, 0, binaryIndirizzi.Length);

                // Scrittura imprese affidatarie
                ZipEntry affidatarieFile = new ZipEntry("IMPRESEAFFIDATARIE.CSV");
                zipStream.PutNextEntry(affidatarieFile);
                zipStream.Write(binaryAffidatarie, 0, binaryAffidatarie.Length);

                // Scrittura imprese esecutrici
                ZipEntry esecutriciFile = new ZipEntry("IMPRESEESECUTRICI.CSV");
                zipStream.PutNextEntry(esecutriciFile);
                zipStream.Write(binaryEsecutrici, 0, binaryEsecutrici.Length);

                zipStream.Finish();

                byte[] zipFile = outputFile.ToArray();
                
                zipStream.Close();

                Response.AddHeader("content-disposition",
                               String.Format("attachment; filename=EstrazioneNotifiche.zip"));
                Response.ContentType = "application/zip";
                Response.BinaryWrite(zipFile);
                Response.Flush();
                Response.End();
            }
        }
    }

    protected void ButtonEstraiDenunce_Click(object sender, EventArgs e)
    {
        String denunce;
        String subappalti;

        Estrazione.EstraiTuttoDenunce(out denunce, out subappalti);

        using (MemoryStream outputFile = new MemoryStream())
        {
            byte[] binaryDenunce;
            binaryDenunce = System.Text.Encoding.Unicode.GetBytes(denunce);
            //binaryDenunce = System.Text.Encoding.ASCII.GetBytes(denunce);
            byte[] binarySubappalti;
            binarySubappalti = System.Text.Encoding.Unicode.GetBytes(subappalti);
            //binarySubappalti = System.Text.Encoding.ASCII.GetBytes(subappalti);

            using (ZipOutputStream zipStream = new ZipOutputStream(outputFile))
            {
                // Scrittura file delle denunce
                ZipEntry denunceFile = new ZipEntry("DENUNCE.CSV");
                zipStream.PutNextEntry(denunceFile);
                zipStream.Write(binaryDenunce, 0, binaryDenunce.Length);

                // Scrittura file dei subappalti
                ZipEntry subappaltiFile = new ZipEntry("SUBAPPALTI.CSV");
                zipStream.PutNextEntry(subappaltiFile);
                zipStream.Write(binarySubappalti, 0, binarySubappalti.Length);

                zipStream.Finish();

                byte[] zipFile = outputFile.ToArray();

                zipStream.Close();

                Response.AddHeader("content-disposition",
                               String.Format("attachment; filename=EstrazioneDenunce.zip"));
                Response.ContentType = "application/zip";
                Response.BinaryWrite(zipFile);
                Response.Flush();
                Response.End();
            }
        }
    }
}