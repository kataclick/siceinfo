﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Delegates;

public partial class Amministrazione_AggiornamentoNotificaCartacea : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Notifica1.ModalitaAggiornamento();
        Notifica1.OnNotificaSelected += new NotificaSelectedEventHandler(Notifica1_OnNotificaSelected);

        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
            Notifica1.ModalitaCartacea();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "In questa pagina è possibile inserire l'aggiornamento di una notifica preliminare ricevuto in formato cartaceo.",
            "Aggiornamento notifica cartacea");
    }
    #endregion

    void Notifica1_OnNotificaSelected(Int32 idNotifica, Int32 idNotificaRiferimento)
    {
        Context.Items["IdNotifica"] = idNotificaRiferimento;
        Server.Transfer("~/Amministrazione/InserimentoNotificaCartacea.aspx");
    }
}