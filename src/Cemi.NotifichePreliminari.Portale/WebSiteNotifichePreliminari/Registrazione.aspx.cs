using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class Registrazione : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        LinkButtonDisclaimer.Attributes.Add("onclick", String.Format("window.open('{0}'); return false;", ResolveUrl("~/RisorseStatiche/Privacy.pdf")));

        if (!Page.IsPostBack)
        {
            CaricaTitoloEAiuto();
            LabelTotalePassi.Text = CreateUserWizard1.WizardSteps.Count.ToString();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaTitoloEAiuto(String titolo, String aiuto)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaTitolo(titolo);
        master.CaricaStringaHelp(aiuto);
    }

    private void CaricaTitoloEAiuto()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaTitoloEAiuto(
            "Registrazione", "Compilare i campi contrassegnati da <b>*</b> e premere il pulsante <b>Avanti</b> per proseguire con la registrazione di un nuovo utente.");
    }
    #endregion

    //protected void CheckBoxUtenteCommittente_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (CheckBoxUtenteCommittente.Checked)
    //    {
    //        PanelCommittente.Enabled = true;
    //        Committente1.ImpostaValidationGroup(String.Empty);
    //    }
    //    else
    //    {
    //        PanelCommittente.Enabled = false;
    //        Committente1.ImpostaValidationGroup("stop");
    //    }
    //}

    protected void CreateUserWizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        LabelCodiceFiscale.Visible = false;

        if (!CheckBoxDisclaimer.Checked)
        {
            LabelConfermaObbligatoriaDisclaimer.Visible = true;
            if (CreateUserWizard1.ActiveStepIndex > 0)
            {
                CreateUserWizard1.ActiveStepIndex--;
            }
        }
        else
        {
            LabelConfermaObbligatoriaDisclaimer.Visible = false;
            LabelPasso.Text = (CreateUserWizard1.ActiveStepIndex + 1).ToString();

            // Se viene selezionata la qualifica "Stazione Appaltante" permetto di 
            // indicare un committente
            //if (Utente1.Qualifica == "3")
            //{
            //    CheckBoxUtenteCommittente.Enabled = true;
            //}
            //else
            //{
            //    CheckBoxUtenteCommittente.Enabled = false;
            //    Committente1.ResetCampi();
            //}
        }
    }

    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        LabelCodiceFiscale.Visible = false;
        String userName = ((CreateUserWizard)sender).UserName;
        MembershipUser nuovoUtente = Membership.GetUser(userName);

        UtenteNotificheTelematiche utente = Utente1.GetUtente();
        
        //if (CheckBoxUtenteCommittente.Checked)
        //{
        //    utente.CommittenteTelematiche = Committente1.CreaCommittente();
        //}

        utente.IdUtente = (Guid)nuovoUtente.ProviderUserKey;
        try
        {
            if (!biz.InsertUtenteTelematiche(utente))
            {
                Membership.DeleteUser(userName);
            }
            else
            {
                Label lLogin = (Label)stepComplete.ContentTemplateContainer.FindControl("LabelLogin");
                Label lPassword = (Label)stepComplete.ContentTemplateContainer.FindControl("LabelPassword");

                lLogin.Text = nuovoUtente.UserName;
                lPassword.Text = nuovoUtente.GetPassword();

                try
                {
                    MailManager mailManager = new MailManager();
                    mailManager.InviaMailRegistrazione(nuovoUtente.UserName, nuovoUtente.GetPassword(), nuovoUtente.Email, utente);
                }
                catch { }
            }
        }
        catch 
        {
            if (Membership.GetUser(userName) != null)
            {
                Membership.DeleteUser(userName);
            }
        }
    }

    protected void CreateUserWizard1_CreateUserError(object sender, CreateUserErrorEventArgs e)
    {
        Int32 i = 0;
    }

    protected void CreateUserWizard1_CreatingUser(object sender, LoginCancelEventArgs e)
    {
        //UtenteNotificheTelematiche utente = Utente1.GetUtente();

        //if (biz.VerificaUtenteGiaPresente(utente.CodiceFiscale))
        //{
        //    e.Cancel = true;
        //    LabelCodiceFiscale.Visible = true;
        //}
    }
}
