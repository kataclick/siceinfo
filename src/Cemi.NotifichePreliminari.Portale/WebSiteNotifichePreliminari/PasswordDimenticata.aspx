﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PasswordDimenticata.aspx.cs" Inherits="PasswordDimenticata" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro"
    runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" runat="Server">
    <asp:Panel ID="PanelPasswordDimenticata" runat="server" DefaultButton="ButtonInviaMail">
        <p>
            Nel caso in cui venga dimenticata la password di accesso al portale è possibile recuperarla attraverso questa pagina
            indicando il proprio nome utente nella casella di testo e premendo il pulsante "Recupera Password".
            Una email verrà inviata all'indirizzo di posta comunicato contenente l'accoppiata nome utente / password.
        </p>
        <table width="100%">
            <colgroup width="150px">
            </colgroup>
            <colgroup>
            </colgroup>
            <colgroup>
            </colgroup>
            <tr>
                <td>
                    Nome utente:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNomeUtente" runat="server" Width="150px" MaxLength="256">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNomeUtente" runat="server"
                        ErrorMessage="Indicare il proprio nome utente" ValidationGroup="passwordDimenticata"
                        ControlToValidate="TextBoxNomeUtente" CssClass="messaggiErrore">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonInviaMail" runat="server" Text="Recupera passsword" Width="150px"
                        ValidationGroup="passwordDimenticata" OnClick="ButtonInviaMail_Click" />
                </td>
                <td>
                    <br />
                    <asp:ValidationSummary ID="ValidationSummaryInviaMail" runat="server" ValidationGroup="passwordDimenticata" />
                    <asp:Label ID="LabelMessaggio" runat="server" CssClass="messaggiErrore">
                    </asp:Label>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
