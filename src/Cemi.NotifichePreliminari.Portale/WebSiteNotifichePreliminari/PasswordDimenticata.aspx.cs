﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.Presenter;
using System.Web.Security;

public partial class PasswordDimenticata : System.Web.UI.Page
{
    MailManager mailBiz = new MailManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "Indicando il proprio nome utente verra inviata un messaggio di <b>posta elettronica</b> con la propria password.",
            "Password dimenticata");
    }
    #endregion

    protected void ButtonInviaMail_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String userName = Presenter.NormalizzaCampoTesto(TextBoxNomeUtente.Text);
            
            MembershipUser user = null;
            try
            {
                user = Membership.GetUser(userName);
            }
            catch (ArgumentException)
            {
                LabelMessaggio.Text = "Nome utente non valido";
                return;
            }

            if (user != null)
            {
                try
                {
                    mailBiz.InviaMailPasswordDimenticata(user);
                    LabelMessaggio.Text = "Un messaggio di posta elettronica contenente la sua password è stato inviato al suo indirizzo.";
                }
                catch (Exception)
                {
                    LabelMessaggio.Text = "Si è verificato un errore durante l'invio della mail, riprovare più tardi.";
                }
            }
            else
            {
                LabelMessaggio.Text = "Utente non trovato.";
            }
        }
    }
}