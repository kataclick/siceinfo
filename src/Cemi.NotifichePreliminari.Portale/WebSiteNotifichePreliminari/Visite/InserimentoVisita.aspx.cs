﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Business;

public partial class Visite_InserimentoVisita : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            int idNotifica = Int32.Parse(Request.QueryString["idNotifica"]);
            DatiVisita1.ImpostaIdNotifica(idNotifica);
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp("In questa pagina è possibile inserire un visita effettuata presso il cantiere denunciato dalla notifica preliminare.",
            "Inserimento Visita");
    }
    #endregion

    protected void ButtonInserisci_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Visita visita = CreaVisita();

            if (biz.InsertVisita(visita))
            {
                DatiVisita1.BloccaCampi();
                ButtonInserisci.Enabled = false;
                PanelAllegati.Enabled = true;
                ViewState["IdVisita"] = visita.IdVisita.Value;
                AllegatiVisite1.ImpostaIdVisita(visita.IdVisita.Value);

                LabelMessaggio.Visible = true;
            }
            else
            {
                LabelMessaggio.Visible = false;
            }
        }
    }

    private Visita CreaVisita()
    {
        return DatiVisita1.CreaVisita();
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Ricerca/RicercaNotifica.aspx?back=true");
    }
}