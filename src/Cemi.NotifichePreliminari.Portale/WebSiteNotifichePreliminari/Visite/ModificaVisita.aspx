﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModificaVisita.aspx.cs" Inherits="Visite_ModificaVisita" %>

<%@ Register src="WebControls/AllegatiVisite.ascx" tagname="AllegatiVisite" tagprefix="uc1" %>

<%@ Register src="WebControls/DatiVisita.ascx" tagname="DatiVisita" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc2:DatiVisita id="CptDatiAttivita1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonModifica" runat="server" OnClick="ButtonModifica_Click" Text="Modifica" ValidationGroup="visita" Width="150px" />&nbsp;
                <asp:Button ID="ButtonAnnulla" runat="server" Text="Torna all'elenco" 
                    Width="150px" CausesValidation="false" onclick="ButtonAnnulla_Click" />&nbsp;
                <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelAllegati" runat="server" Visible="false">
                    <uc1:AllegatiVisite ID="CptAllegatiAttivita1" runat="server" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>

