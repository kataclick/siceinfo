﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Enums;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Entities;
using System.Web.Security;

public partial class Visite_WebControls_DatiVisita : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieVisita();
            CaricaEsitiVisita();
            CaricaEnti();
            CaricaGradiIrregolarita();
        }
    }

    private void CaricaEnti()
    {
        DropDownListEnte.Items.Clear();
        DropDownListEnte.Items.Add(new ListItem(string.Empty, string.Empty));

        if (Roles.IsUserInRole("ContributoVisiteASL"))
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.ASL.ToString(), EnteVisita.ASL.ToString()));
        }
        if (Roles.IsUserInRole("ContributoVisiteCPT"))
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.CPT.ToString(), EnteVisita.CPT.ToString()));
        }
        if (Roles.IsUserInRole("ContributoVisiteDTL"))
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.DTL.ToString(), EnteVisita.DTL.ToString()));
        }
        if (Roles.IsUserInRole("ContributoVisiteINAIL"))
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.INAIL.ToString(), EnteVisita.INAIL.ToString()));
        }
        if (Roles.IsUserInRole("ContributoVisiteINPS"))
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.INPS.ToString(), EnteVisita.INPS.ToString()));
        }
        if (Roles.IsUserInRole("ContributoVisiteCassaEdile"))
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.CassaEdile.ToString(), EnteVisita.CassaEdile.ToString()));
        }

        if (DropDownListEnte.Items.Count == 2)
        {
            DropDownListEnte.SelectedIndex = 1;
            DropDownListEnte.Enabled = false;
        }

        DropDownListEnte.DataBind();
    }

    private void CaricaEsitiVisita()
    {
        DropDownListEsito.Items.Clear();
        DropDownListEsito.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListEsito.DataSource = biz.GetEsitiVisita();
        DropDownListEsito.DataTextField = "Descrizione";
        DropDownListEsito.DataValueField = "IdEsitoVisita";
        DropDownListEsito.DataBind();
    }

    private void CaricaTipologieVisita()
    {
        DropDownListTipologia.Items.Clear();
        DropDownListTipologia.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListTipologia.DataSource = biz.GetTipologieVisita();
        DropDownListTipologia.DataTextField = "Descrizione";
        DropDownListTipologia.DataValueField = "IdTipologiaVisita";
        DropDownListTipologia.DataBind();
    }

    private void CaricaGradiIrregolarita()
    {
        GradoIrregolaritaCollection gradi = biz.GetGradiIrregolarita();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListGradoIrregolarita,
            gradi,
            "Descrizione",
            "IdGradoIrregolarita");
    }

    public void BloccaCampi()
    {
        TextBoxData.Enabled = false;
        DropDownListTipologia.Enabled = false;
        DropDownListEsito.Enabled = false;
        DropDownListEnte.Enabled = false;
        DropDownListGradoIrregolarita.Enabled = false;
    }

    public Visita CreaVisita()
    {
        //int idUtente = ((TBridge.Cemi.GestioneUtenti.Business.Identities.UtenteAbilitato)
        //    HttpContext.Current.User.Identity).IdUtente;
        //int idUtente = GestioneUtentiBiz.GetIdUtente();

        Visita visita = new Visita();

        visita.Ente = (EnteVisita)Enum.Parse(typeof(EnteVisita), DropDownListEnte.SelectedValue);
        visita.Data = DateTime.Parse(TextBoxData.Text.Replace('.', '/'));
        if (!String.IsNullOrEmpty(DropDownListTipologia.SelectedItem.Value))
        {
            visita.Tipologia = new TipologiaVisita();
            visita.Tipologia.IdTipologiaVisita = Int32.Parse(DropDownListTipologia.SelectedItem.Value);
        }
        if (!String.IsNullOrEmpty(DropDownListEsito.SelectedItem.Value))
        {
            visita.Esito = new EsitoVisita();
            visita.Esito.IdEsitoVisita = Int32.Parse(DropDownListEsito.SelectedItem.Value);
        }
        //visita.IdUtente = idUtente;
        visita.IdNotifica = (int)ViewState["IdNotifica"];

        if (!string.IsNullOrEmpty(DropDownListGradoIrregolarita.SelectedItem.Value))
        {
            visita.GradoIrregolarita = new GradoIrregolarita();
            visita.GradoIrregolarita.IdGradoIrregolarita = Int16.Parse(DropDownListGradoIrregolarita.SelectedItem.Value);
        }

        return visita;
    }

    public void ImpostaIdNotifica(int idNotifica)
    {
        ViewState["IdNotifica"] = idNotifica;
    }

    public void CaricaVisita(Visita visita)
    {
        DropDownListEnte.SelectedValue = visita.Ente.ToString();
        TextBoxData.Text = visita.Data.ToShortDateString();
        if (visita.Tipologia != null)
        {
            DropDownListTipologia.SelectedValue = visita.Tipologia.IdTipologiaVisita.ToString();
        }
        if (visita.Esito != null)
        {
            DropDownListEsito.SelectedValue = visita.Esito.IdEsitoVisita.ToString();
        }

        if (visita.Esito != null && visita.Esito.Descrizione == "Irregolare")
        {
            DropDownListGradoIrregolarita.Enabled = true;
        }
        else
        {
            DropDownListGradoIrregolarita.Enabled = false;
        }

        if (visita.GradoIrregolarita != null)
        {
            DropDownListGradoIrregolarita.SelectedValue = visita.GradoIrregolarita.IdGradoIrregolarita.ToString();
        }

        ImpostaIdNotifica(visita.IdNotifica);
    }

    protected void DropDownListEsito_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownListGradoIrregolarita.SelectedIndex = 0;

        if (DropDownListEsito.SelectedItem != null)
        {
            if (DropDownListEsito.SelectedItem.Text == "Irregolare")
            {
                DropDownListGradoIrregolarita.Enabled = true;
            }
            else
            {
                DropDownListGradoIrregolarita.Enabled = false;
            }
        }
    }
}