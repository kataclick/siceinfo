﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AllegatiVisite.ascx.cs" Inherits="Visite_WebControls_AllegatiVisite" %>
<table class="standardTable">
    <tr id="trInserimentoAllegati" runat="server">
        <td>
            Allegati:
        </td>
        <td>
            <asp:FileUpload ID="FileUploadAllegato" runat="server" Width="300px" />
            <asp:Button ID="ButtonUpload" runat="server" Width="100px" Text="Allega" OnClick="ButtonUpload_Click" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <table class="standardTable">
                <asp:Repeater ID="RepeaterAllegati" runat="server" OnItemDataBound="RepeaterAllegati_OnItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="LabelIdAllegato" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td>
                                <asp:Image ID="ImageIconaFile" runat="server" />
                            </td>
                            <td>
                                <asp:LinkButton ID="LinkButtonAllegato" runat="server" OnClick="LinkButtonAllegato_OnClick"></asp:LinkButton>
                            </td>
                            <td>
                                <asp:Button ID="ButtonEliminaAllegato" runat="server" Text="Elimina" OnClick="ButtonEliminaAllegato_OnClick" Visible="false" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </td>
    </tr>
</table>