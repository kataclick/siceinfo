﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.NotifichePreliminari.Type.Entities;
using System.IO;

public partial class Visite_WebControls_AllegatiVisite : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ImpostaIdVisita(int idVisita)
    {
        ViewState["IdVisita"] = idVisita;
        trInserimentoAllegati.Visible = true;
    }

    public void ImpostaAllegati(AllegatoCollection allegati)
    {
        trInserimentoAllegati.Visible = false;
        CaricaAllegati(allegati);
    }

    public void ImpostaAllegati(AllegatoCollection allegati, int idVisita)
    {
        ImpostaIdVisita(idVisita);
        CaricaAllegati(allegati);
    }

    private void CaricaAllegati(AllegatoCollection allegati)
    {
        RepeaterAllegati.DataSource = allegati;
        RepeaterAllegati.DataBind();
    }

    private void CaricaAllegati()
    {
        if (ViewState["IdVisita"] != null)
        {
            int idVisita = (int)ViewState["IdVisita"];

            AllegatoCollection allegati = biz.GetAllegati(idVisita);
            CaricaAllegati(allegati);
        }
    }

    protected void ButtonUpload_Click(object sender, EventArgs e)
    {
        if (ViewState["IdVisita"] != null && FileUploadAllegato.HasFile)
        {
            string fileName = Path.GetFileName(FileUploadAllegato.PostedFile.FileName);
            Stream fileStream = FileUploadAllegato.PostedFile.InputStream;

            byte[] fileAllegato = new byte[fileStream.Length];
            fileStream.Read(fileAllegato, 0, (int)fileStream.Length);

            Allegato allegato = new Allegato();
            allegato.NomeFile = fileName;
            allegato.File = fileAllegato;
            allegato.IdVisita = (int)ViewState["IdVisita"];

            if (biz.InsertAllegato(allegato))
            {
                CaricaAllegati();
            }
        }
    }

    protected void RepeaterAllegati_OnItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Allegato allegato = (Allegato)e.Item.DataItem;

            Label lIdAllegato = (Label)e.Item.FindControl("LabelIdAllegato");
            Image iIconaFile = (Image)e.Item.FindControl("ImageIconaFile");
            LinkButton lbApri = (LinkButton)e.Item.FindControl("LinkButtonAllegato");
            Button bElimina = (Button)e.Item.FindControl("ButtonEliminaAllegato");

            lIdAllegato.Text = allegato.IdAllegato.ToString();
            lbApri.Text = allegato.NomeFile;
            iIconaFile.ImageUrl = biz.GetAllegatoImageUrl(allegato.NomeFile);

            if (ViewState["IdVisita"] != null)
                bElimina.Visible = true;
        }
    }

    protected void LinkButtonAllegato_OnClick(Object Sender, EventArgs e)
    {
        LinkButton lbFile = (LinkButton)Sender;
        Label lIdAllegato = (Label)lbFile.Parent.FindControl("LabelIdAllegato");

        int idAllegato = Int32.Parse(lIdAllegato.Text);
        Allegato allegato = biz.GetAllegato(idAllegato);

        RestituisciFile(allegato);
    }

    protected void ButtonEliminaAllegato_OnClick(Object Sender, EventArgs e)
    {
        Button bElimina = (Button)Sender;
        Label lIdAllegato = (Label)bElimina.Parent.FindControl("LabelIdAllegato");
        int idAllegato = Int32.Parse(lIdAllegato.Text);
        int idVisita = (int)ViewState["IdVisita"];

        if (biz.DeleteVisitaAllegato(idVisita, idAllegato))
        {
            CaricaAllegati();
        }
    }

    private void RestituisciFile(Allegato allegato)
    {
        //Set the appropriate ContentType.
        Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", allegato.NomeFile));
        Response.ContentType = "application/download";
        //Write the file directly to the HTTP content output stream.
        Response.BinaryWrite(allegato.File);
        Response.Flush();
        Response.End();
    }
}