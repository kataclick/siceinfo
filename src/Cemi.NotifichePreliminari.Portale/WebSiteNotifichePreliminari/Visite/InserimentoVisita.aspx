﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InserimentoVisita.aspx.cs" Inherits="Visite_InserimentoVisita" %>

<%@ Register src="WebControls/AllegatiVisite.ascx" tagname="AllegatiVisite" tagprefix="uc1" %>

<%@ Register src="WebControls/DatiVisita.ascx" tagname="DatiVisita" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <table class="standardTable">
        <tr>
            <td>
                
                <uc2:DatiVisita ID="DatiVisita1" runat="server" />
                
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonInserisci" runat="server" OnClick="ButtonInserisci_Click" Text="Inserisci" ValidationGroup="visita" Width="150px" />&nbsp;
                <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Torna alla ricerca" Width="150px" />&nbsp;<asp:Label 
                    ID="LabelMessaggio" runat="server" ForeColor="Red" 
                    Text="Inserimento effettuato" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelAllegati" runat="server" Enabled="false" Visible="false">
                    <uc1:AllegatiVisite ID="AllegatiVisite1" runat="server" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>

