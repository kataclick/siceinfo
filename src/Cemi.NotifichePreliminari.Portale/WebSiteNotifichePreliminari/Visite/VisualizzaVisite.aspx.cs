﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using Cemi.NotifichePreliminari.Type.Enums;

public partial class Visite_VisualizzaVisite : System.Web.UI.Page
{
    private const int INDICEELIMINA = 7;
    private const int INDICEMODIFICA = 6;
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            int idNotificaRiferimento = Int32.Parse(Request.QueryString["idNotifica"]);
            ViewState["IdNotifica"] = idNotificaRiferimento;

            CaricaVisite(idNotificaRiferimento);
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp("In questa pagina è possibile visualizzare quali visite sono state effettuate nel cantiere denunciato dalla notifica preliminare.",
            "Visualizza Visita");
    }
    #endregion

    private void CaricaVisite(int idNotificaRiferimento)
    {
        VisitaCollection visite = biz.GetVisite(idNotificaRiferimento);

        visite = FiltraVisite(visite);

        GridViewVisite.DataSource = visite;
        GridViewVisite.DataBind();
    }

    protected void GridViewVisite_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Visita visita = (Visita)e.Row.DataItem;
            //Visite_WebControls_AllegatiVisite allAtt =
            //    (Visite_WebControls_AllegatiVisite)e.Row.FindControl("CptAllegatiAttivita1");

            Button bConfermaEliminazioneSi = (Button)e.Row.FindControl("ButtonConfermaEliminazioneSi");
            bConfermaEliminazioneSi.CommandArgument = e.Row.RowIndex.ToString();
            Button bConfermaEliminazioneNo = (Button)e.Row.FindControl("ButtonConfermaEliminazioneNo");
            bConfermaEliminazioneNo.CommandArgument = e.Row.RowIndex.ToString();

            //allAtt.ImpostaAllegati(visita.Allegati);

            e.Row.Cells[INDICEMODIFICA].Enabled = false;
            e.Row.Cells[INDICEELIMINA].Enabled = false;

            switch (visita.Ente)
            {
                case EnteVisita.ASL:
                    if (Roles.IsUserInRole("ContributoVisiteASL"))
                    {
                        e.Row.Cells[INDICEMODIFICA].Enabled = true;
                        e.Row.Cells[INDICEELIMINA].Enabled = true;
                    }
                    break;
                case EnteVisita.CPT:
                    if (Roles.IsUserInRole("ContributoVisiteCPT"))
                    {
                        e.Row.Cells[INDICEMODIFICA].Enabled = true;
                        e.Row.Cells[INDICEELIMINA].Enabled = true;
                    }
                    break;
                case EnteVisita.DTL:
                    if (Roles.IsUserInRole("ContributoVisiteDTL"))
                    {
                        e.Row.Cells[INDICEMODIFICA].Enabled = true;
                        e.Row.Cells[INDICEELIMINA].Enabled = true;
                    }
                    break;
                case EnteVisita.INAIL:
                    if (Roles.IsUserInRole("ContributoVisiteINAIL"))
                    {
                        e.Row.Cells[INDICEMODIFICA].Enabled = true;
                        e.Row.Cells[INDICEELIMINA].Enabled = true;
                    }
                    break;
                case EnteVisita.INPS:
                    if (Roles.IsUserInRole("ContributoVisiteINPS"))
                    {
                        e.Row.Cells[INDICEMODIFICA].Enabled = true;
                        e.Row.Cells[INDICEELIMINA].Enabled = true;
                    }
                    break;
                case EnteVisita.CassaEdile:
                    if (Roles.IsUserInRole("ContributoVisiteCassaEdile"))
                    {
                        e.Row.Cells[INDICEMODIFICA].Enabled = true;
                        e.Row.Cells[INDICEELIMINA].Enabled = true;
                    }
                    break;
            }
            
        }
    }

    private VisitaCollection FiltraVisite(VisitaCollection visite)
    {
        VisitaCollection visiteFiltrate = new VisitaCollection();

        foreach (Visita visita in visite)
        {
            switch (visita.Ente)
            {
                case EnteVisita.ASL:
                    if (Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteASL") || Roles.IsUserInRole("ContributoVisiteASL"))
                    {
                        visiteFiltrate.Add(visita);
                    }
                    break;
                case EnteVisita.CPT:
                    if (Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteCPT") || Roles.IsUserInRole("ContributoVisiteCPT"))
                    {
                        visiteFiltrate.Add(visita);
                    }
                    break;
                case EnteVisita.DTL:
                    if (Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteDTL") || Roles.IsUserInRole("ContributoVisiteDTL"))
                    {
                        visiteFiltrate.Add(visita);
                    }
                    break;
                case EnteVisita.INAIL:
                    if (Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINAIL") || Roles.IsUserInRole("ContributoVisiteINAIL"))
                    {
                        visiteFiltrate.Add(visita);
                    }
                    break;
                case EnteVisita.INPS:
                    if (Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINPS") || Roles.IsUserInRole("ContributoVisiteINPS"))
                    {
                        visiteFiltrate.Add(visita);
                    }
                    break;
                case EnteVisita.CassaEdile:
                    if (Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteCassaEdile") || Roles.IsUserInRole("ContributoVisiteCassaEdile"))
                    {
                        visiteFiltrate.Add(visita);
                    }
                    break;
            }
        }

        return visiteFiltrate;
    }

    protected void GridViewVisite_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idVisita = (int)GridViewVisite.DataKeys[e.NewSelectedIndex].Values["IdVisita"];

        Context.Items["IdVisita"] = idVisita;
        Server.Transfer("~/Visite/ModificaVisita.aspx");
    }

    protected void GridViewVisite_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int indice;

        switch (e.CommandName)
        {
            case "confermaDeleteSi":
                indice = Int32.Parse(e.CommandArgument.ToString());
                int idVisita = (int)GridViewVisite.DataKeys[indice].Values["IdVisita"];
                if (biz.DeleteVisita(idVisita))
                {
                    int idNotifica = (int)ViewState["IdNotifica"];
                    CaricaVisite(idNotifica);
                }
                break;
            case "confermaDeleteNo":
                indice = Int32.Parse(e.CommandArgument.ToString());
                HtmlTableRow rigaConferma =
                    (HtmlTableRow)GridViewVisite.Rows[indice].FindControl("trConfermaEliminazione");
                rigaConferma.Visible = false;
                break;
        }
    }

    protected void GridViewVisite_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HtmlTableRow rigaConferma = (HtmlTableRow)GridViewVisite.Rows[e.RowIndex].FindControl("trConfermaEliminazione");
        rigaConferma.Visible = true;
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Ricerca/RicercaNotifica.aspx?back=true");
    }
}