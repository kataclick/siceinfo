﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class Visite_ModificaVisita : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            if (Context.Items["IdVisita"] != null)
            {
                int idVisita = (int)Context.Items["IdVisita"];
                ViewState["IdVisita"] = idVisita;

                CaricaVisita(idVisita);
            }
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp("In questa pagina è possibile modificare un visita effettuata presso il cantiere denunciato dalla notifica preliminare.",
            "Modifica Visita");
    }
    #endregion

    private void CaricaVisita(int idVisita)
    {
        Visita visita = biz.GetVisita(idVisita);
        ViewState["IdNotifica"] = visita.IdNotifica.ToString();
        CptDatiAttivita1.CaricaVisita(visita);
        CptAllegatiAttivita1.ImpostaAllegati(visita.Allegati, visita.IdVisita.Value);
    }

    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        Visita visita = CptDatiAttivita1.CreaVisita();
        visita.IdVisita = (int)ViewState["IdVisita"];

        if (biz.UpdateVisita(visita))
        {
            LabelMessaggio.Text = "Aggiornamento effettuato";

            int idVisita = (int)ViewState["IdVisita"];
            CaricaVisita(idVisita);

            Response.Redirect(String.Format("~/Visite/VisualizzaVisite.aspx?idNotifica={0}", ViewState["IdNotifica"]));
        }
        else
        {
            LabelMessaggio.Text = "Errore durante l'aggiornamento";
        }
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/Visite/VisualizzaVisite.aspx?idNotifica={0}", ViewState["IdNotifica"]));
    }
}