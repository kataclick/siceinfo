﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <p>
        Benvenuti sul sito per la comunicazione delle Notifiche preliminari.
    </p>
    <p>
        Da queste pagine si possono comunicare in via telematica le notifiche preliminari e i relativi aggiornamenti. 
        L'accesso è consentito agli utenti autorizzati.
        Per ottenere l'accesso al portale occorre seguire la procedura di <a href="Registrazione.aspx">Registrazione</a> cliccando sull'apposita voce nel menu in alto.
    </p>
    <p>
        <asp:Label
            ID="LabelDatiPersona"
            runat="server"
            Font-Bold="true"
            Text="I dati della persona che chiede la registrazione ( Utente )  sono esclusivamente quelli relativi al Committente od al Responsabile dei lavori anche se il materiale inserimento è svolto da altri per incarico dei medesimi.">
        </asp:Label>
    </p>
    <p>
        La <a href="Registrazione.aspx">Registrazione</a> non garantisce l'accesso diretto al portale Web che deve essere autorizzato dall'Amministratore del sistema.
    </p>
    <br />
    <table class="tabellaConBordo">
        <tr>
            <td>
                <asp:Label 
                    ID="LabelNota1"
                    runat="server"
                    Font-Bold="true"
                    Text="Nota sulle notifiche preliminari">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    Le notifiche preliminari sono lo strumento che la normativa individua per la comunicazione, agli Organi di Vigilanza ed agli altri Enti, della prossima apertura di un cantiere dove si effettuano lavori edili o di ingegneria civile ( art 99 D.Lgs. 9 aprile 2008 n. 81).
                </p>
                <p>
                    Attualmente i committenti, pubblici e privati, devono effettuare la comunicazione della notifica preliminare,  a mezzo di raccomandata a/r, alla AUSL, alla Direzione Provinciale del Lavoro e, dove previsto da specifica norma, alla Cassa Edile, all’INAIL e all’INPS.
                </p>
                <p>
                    La notifica preliminare può anche essere inviata per Posta Elettronica Certificata che, con l’apposizione della firma digitale, ha il valore di una raccomandata a/r.
                </p>
                <p>
                    <asp:Label
                        ID="LabelMancataPresentazione"
                        runat="server"
                        Font-Bold="true"
                        Text="La mancata presentazione della notifica preliminare comporta la sospensione d’efficacia del titolo abilitativo ed è obbligo degli Organi di vigilanza comunicare l’inadempienza all’Amministrazione concedente (art.90 D.Leg.tivo 2008/81, c.10/11).">
                    </asp:Label>
                </p>
                <p>
                    Utilizzando lo Sportello Unico Notifiche Preliminari i Committenti o i Responsabili dei lavori possono adempiere agevolmente agli obblighi di legge indicati; compilando il format predisposto saranno certi di avere comunicato tutte le informazioni ed i dati contenuti nell’allegato XII e la procedura invia automaticamente una PEC a: DTL, AUSL, INPS, INAIL, CASSA EDILE competenti per territorio.
                </p>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <small>
                    <asp:Label 
                        ID="LabelNota2"
                        runat="server"
                        Font-Bold="true"
                        Text="Norme di riferimento relative alle notifiche preliminari">
                    </asp:Label>
                </small>
            </td>
        </tr>
        <tr>
            <td>
                <small>
                    AUSL Dlgs 81/2008 art.99 comma 1
                </small>
            </td>
        </tr>
        <tr>
            <td>
                <small>
                    DPL Dlgs 81/2008 art.99 comma 1
                </small>
            </td>
        </tr>
        <tr>
            <td>
                <small>
                    INPS-INAIL-CASSA EDILE L.R. 8/2000 art. 2 comma 2
                </small>
            </td>
        </tr>
    </table>
</asp:Content>

