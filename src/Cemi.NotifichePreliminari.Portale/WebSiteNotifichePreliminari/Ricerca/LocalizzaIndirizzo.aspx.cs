using System;
using System.Text;
using System.Web.UI;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Filters;
using Cemi.NotifichePreliminari.Type.Collections;

public partial class LocalizzaIndirizzo : Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonIndietro.Attributes.Add("onclick", "javascript:history.back(); return false;");

        if (!Page.IsPostBack)
        {
            Int32? idIndirizzo = null;

            if (Request.QueryString["idIndirizzo"] != null)
            {
                idIndirizzo = Int32.Parse(Request.QueryString["idIndirizzo"].ToString());
            }

            if (!idIndirizzo.HasValue)
            {
                idIndirizzo = Context.Items["IdIndirizzo"] as Int32?;
            }

            if (idIndirizzo.HasValue)
            {
                CaricaIndirizzo(idIndirizzo.Value);
            }
        }
    }

    private void CaricaIndirizzo(Int32 idIndirizzo)
    {
        NotificaFilter filtro = new NotificaFilter();
        filtro.IdIndirizzo = idIndirizzo;
        filtro.IdArea = 1;

        CantiereNotificaCollection cantieri = biz.RicercaCantieriPerCommittente(filtro);
        if (cantieri.Count == 1)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("LoadMap();");

            stringBuilder.Append(Localizzazione1.CaricaCantiere(cantieri[0], true));

            RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
        }
    }
}