﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="LocalizzaIndirizzo.aspx.cs"
    Inherits="LocalizzaIndirizzo" %>

<%@ Register Src="~/WebControls/Visualizzazione/Localizzazione.ascx" TagName="Localizzazione"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Indirizzo</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
            <asp:Button ID="ButtonIndietro" runat="server" Text="Torna indietro" Width="150px" Visible="false" />
            <uc1:Localizzazione ID="Localizzazione1" runat="server" />
        </telerik:RadAjaxPanel>
    </div>
    </form>
</body>
</html>
