using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RicercaNotifica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            String.Format("In questa pagina � possibile cercare tra le notifiche preliminari precedentemente inserite e aggiornamenti.<br /><br />Impostando i filtri di ricerca e premendo il taso \"<b>Ricerca</b>\" vengono mostrate tutte le notifiche (o aggiornamenti) trovati.<br /><br />La sezione \"<b>Storia</b>\" visualizza la notifica preliminare e i successivi aggiornamenti in ordine cronologico<br /><br /><hr /><b>Legenda</b><br /><br /><img src=\"{0}\" /> Visualizza la mappa<br /><img src=\"{1}\" /> Dettagli della notifica<br /><img src=\"{2}\" /> Ricevuta",
            ResolveUrl("~/Images/localizzazione24.png"),
            ResolveUrl("~/Images/dettagliNotifica24.png"),
            ResolveUrl("~/Images/report24.png")),
            "Ricerca notifica");
    }
    #endregion
}
