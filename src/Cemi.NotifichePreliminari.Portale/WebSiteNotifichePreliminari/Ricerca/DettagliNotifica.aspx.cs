using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class DettagliNotifica : System.Web.UI.Page
{
    private NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        //ButtonIndietro.Attributes.Add("onclick", "javascript:history.back(); return false;");

        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();

            if (Context.Items["IdNotifica"] != null)
            {
                Int32 idNotifica = (Int32)Context.Items["IdNotifica"];

                NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica);
                Riassunto1.CaricaNotifica(notifica);
            }
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            String.Format("In questa pagina viene visualizzata la scheda completa della notifica preliminare o dell'aggiornamento selezionato<br /><br /><hr /><b>Legenda</b><br /><br /><img src=\"{0}\" /> Espande la sezione<br /><img src=\"{1}\" /> Chiude la sezione",
            ResolveUrl("~/Images/frecciaGiu.png"),
            ResolveUrl("~/Images/frecciaSu.png")),
            "Dettagli della notifica");
    }
    #endregion
    
    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Ricerca/RicercaNotifica.aspx?back=true");
    }
}
