﻿using System;
using System.Text;
using Cemi.Geocode.Type;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Telerik.Web.UI;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Enums;
using Indirizzo = Cemi.Geocode.Type.Indirizzo;
using System.Web.Security;

public partial class WebControls_Denuncia : System.Web.UI.UserControl
{
    private const Int32 INDICESTEPDATIGENERALI = 0;
    private const Int32 INDICESTEPCANTIERE = 1;
    private const Int32 INDICESTEPSUBAPPALTI = 2;
    private const Int32 INDICESTEPCONFERMA = 3;

    private const String VALIDATIONGROUPDATIGENERALI = "datiGenerali";
    private const String VALIDATIONGROUPCANTIERE = "datiCantiere";
    private const String VALIDATIONGROUPSUBAPPALTI = "DettaglioSubappaltoValidation";
    private const String VALIDATIONSINGOLOSUBAPPALTO = "singoloSubappalto";
    private const String VALIDATIONGROUPCONFERMA = "conferma";

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    private Boolean[] TABATTIVI
    {
        get { return (Boolean[])ViewState["TABATTIVI"]; }
        set { ViewState["TABATTIVI"] = value; }
    }

    private Int32 IndiceCorrenteTab
    {
        get { return (Int32)ViewState["IndiceCorrenteTab"]; }
        set { ViewState["IndiceCorrenteTab"] = value; }
    }

    //public Cantiere Cantiere
    //{
    //    set { ViewState["CantiereSelezionato"] = value; }
    //    get
    //    {
    //        if (ViewState["CantiereSelezionato"] == null)
    //            ViewState["CantiereSelezionato"] = new Cantiere();
    //        return (Cantiere) ViewState["CantiereSelezionato"];
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            //ButtonAvanti.Enabled = false;
            #region Inizializzazione TABATTIVI

            Boolean[] nuoviTabAttivi = new Boolean[RadTabStripCantiere.Tabs.Count];
            for (Int32 i = 0; i < RadTabStripCantiere.Tabs.Count; i++)
            {
                nuoviTabAttivi[i] = true;
            }
            TABATTIVI = nuoviTabAttivi;

            IndiceCorrenteTab = 0;

            #endregion

            #region Click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('");
            sb.Append(VALIDATIONGROUPCONFERMA);
            sb.Append("') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonAvanti, null) + ";");
            sb.Append("return true;");

            ButtonAvanti.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAvanti);
            #endregion

            //CaricaRuoliImpresa();

            //InizializzaDenuncia();
            AggiornaLabelPasso();
        }
        //DatiSubappaltoDenuciaCantiere1.CaricaCantiere(Cantiere);
        
        GestisciAbilitazionePassi();
    }

    //private void CaricaRuoliImpresa()
    //{
    //    Presenter.CaricaElementiInDropDown(
    //        RadComboBoxRuolo,
    //        Enum.GetNames(typeof(RuoloImpresa)),
    //        String.Empty,
    //        String.Empty);
    //}

    public void CaricaDatiImpresa(Indirizzo indirizzo)
    {
        DatiSubappaltoDenuciaCantiere1.CaricaDatiImpresa();

        trCodice.Visible = false;

        LabelIndirizzo1.Text = String.IsNullOrEmpty(indirizzo.InformazioniAggiuntive) ? indirizzo.IndirizzoBase : String.Format("{0} ({1})", indirizzo.IndirizzoBase, indirizzo.InformazioniAggiuntive);
        LabelIndirizzo2.Text = String.Format("{0} {1} ({2})", indirizzo.Cap, indirizzo.Comune, indirizzo.Provincia);
        ViewState["Indirizzo"] = indirizzo;
    }

    public void CaricaDenuncia(Int32 idDenuncia)
    {
        ViewState["IdDenuncia"] = idDenuncia;
        
        //DatiDenunciaCantiere1.CaricaDatiCantiere(Cantiere);
        //DatiSubappaltoDenuciaCantiere1.CaricaCantiere(Cantiere);

        Cantiere cantiere = biz.GetCantiere(idDenuncia);
        LabelCodice.Text = cantiere.CodiceCantiere;
        LabelIndirizzo1.Text = String.IsNullOrEmpty(cantiere.IndirizzoCantiere.InformazioniAggiuntive) ? cantiere.IndirizzoCantiere.IndirizzoBase : String.Format("{0} ({1})", cantiere.IndirizzoCantiere.IndirizzoBase, cantiere.IndirizzoCantiere.InformazioniAggiuntive);
        LabelIndirizzo2.Text = String.Format("{0} {1} ({2})", cantiere.IndirizzoCantiere.Cap, cantiere.IndirizzoCantiere.Comune, cantiere.IndirizzoCantiere.Provincia);
        ViewState["Indirizzo"] = cantiere.IndirizzoCantiere;
        switch (cantiere.Ruolo)
        {
            case RuoloImpresa.Capofila:
            case RuoloImpresa.Subappalto:
                RadComboBoxRuolo.SelectedValue = cantiere.Ruolo.ToString();
                RadComboBoxRuolo.Enabled = false;
                break;
            case RuoloImpresa.NonPresente:
                if (cantiere.Capofila != null)
                {
                    RadComboBoxRuolo.SelectedValue = RuoloImpresa.Subappalto.ToString();
                    RadComboBoxRuolo.Enabled = false;
                    DatiSubappaltoDenuciaCantiere1.CaricaDatiImpresa();

                    LabelMessaggioRuolo.Text = "Il cantiere presenta già un'impresa capofila, è possibile inserire la propria solamente in subappalto.";
                }
                else
                {
                    RadComboBoxRuolo.Enabled = true;
                }
                break;
        }
        

        DatiDenunciaCantiere1.CaricaDatiCantiere(cantiere);
        DatiDenunciaCantiere1.AbilitaModifica(cantiere.Ruolo == RuoloImpresa.Capofila);

        DatiSubappaltoDenuciaCantiere1.CaricaCantiere(cantiere);
    }

    //private void InizializzaDenuncia()
    //{
    //    //DatiDenunciaCantiere1.CaricaDatiCantiere(Cantiere);
    //    //DatiSubappaltoDenuciaCantiere1.CaricaCantiere(Cantiere);

    //    if (Context.Items["IdCantiere"] != null)
    //    {
    //        Int32 idCantiere = (Int32)Context.Items["IdCantiere"];
    //        Cantiere cantiere = biz.GetCantiere(idCantiere);
    //        LabelIndirizzo1.Text = cantiere.IndirizzoCantiere.IndirizzoBase;
    //        LabelIndirizzo2.Text = String.Format("{0} {1} ({2})", cantiere.IndirizzoCantiere.Cap, cantiere.IndirizzoCantiere.Comune, cantiere.IndirizzoCantiere.Provincia);
    //        RadComboBoxRuolo.SelectedValue = cantiere.Ruolo.ToString();
    //        RadComboBoxRuolo.Enabled = false;

    //        DatiDenunciaCantiere1.CaricaDatiCantiere(cantiere);
    //        DatiDenunciaCantiere1.AbilitaModifica(cantiere.Ruolo == RuoloImpresa.Capofila);

    //        DatiSubappaltoDenuciaCantiere1.CaricaCantiere(cantiere);
    //    }
    //    else
    //    {
    //        Indirizzo indirizzo = (Indirizzo) Context.Items["Indirizzo"];
    //        LabelIndirizzo1.Text = indirizzo.IndirizzoBase;
    //        LabelIndirizzo2.Text = String.Format("{0} {1} ({2})", indirizzo.Cap, indirizzo.Comune, indirizzo.Provincia);
    //    }
    //}

    protected void RadTabStripCantiereTabClick(object sender, RadTabStripEventArgs e)
    {
        RadMultiPageSceltaCantiere.SelectedIndex = RadTabStripCantiere.SelectedIndex;

        IndiceCorrenteTab = RadTabStripCantiere.SelectedIndex;
        VerificaVisualizzazioneTastoIndietro();
        VerificaVisualizzazioneTastoAvanti();
        GestisciAbilitazionePassi();
        AggiornaLabelPasso();
    }

    private void GestisciAbilitazionePassi()
    {
        foreach (RadTab tab in RadTabStripCantiere.Tabs)
        {
            if (tab.Index > RadTabStripCantiere.SelectedIndex)
            {
                tab.Enabled = false;
            }
            else
            {
                tab.Enabled = TABATTIVI[tab.Index];
            }
        }
    }

    protected void ButtonAvanti_Click(object sender, EventArgs e)
    {
        switch (RadTabStripCantiere.SelectedIndex)
        {
            case INDICESTEPDATIGENERALI:
                ValidationSummary.ValidationGroup = VALIDATIONGROUPDATIGENERALI;
                Page.Validate(VALIDATIONGROUPDATIGENERALI);
                break;
            case INDICESTEPCANTIERE:
                if (RadComboBoxRuolo.SelectedValue == RuoloImpresa.Capofila.ToString())
                {
                    ValidationSummary.ValidationGroup = VALIDATIONGROUPCANTIERE;
                    Page.Validate(VALIDATIONGROUPCANTIERE);
                }
                break;
            case INDICESTEPSUBAPPALTI:
                if (RadComboBoxRuolo.SelectedValue == RuoloImpresa.Capofila.ToString())
                {
                    ValidationSummary.ValidationGroup = VALIDATIONGROUPSUBAPPALTI;
                    Page.Validate(VALIDATIONGROUPSUBAPPALTI);
                }
                else
                {
                    ValidationSummary.ValidationGroup = VALIDATIONSINGOLOSUBAPPALTO;
                    Page.Validate(VALIDATIONSINGOLOSUBAPPALTO);
                }
                if (Page.IsValid)
                {
                    CreaRiassunto();
                }
                break;
            case INDICESTEPCONFERMA:
                //Page.Validate(VALIDATIONGROUPCONFERMA);
                if (Page.IsValid)
                {
                    Cantiere cantiere = CreaCantiere();
                    if (RadComboBoxRuolo.SelectedValue == RuoloImpresa.Capofila.ToString()
                        || (RadComboBoxRuolo.SelectedValue == RuoloImpresa.Subappalto.ToString() && !cantiere.IdDenunciaCantiere.HasValue))
                    {
                        if (!cantiere.IdDenunciaCantiere.HasValue)
                        {
                            biz.InsertDenunciaCantiere(cantiere);
                        }
                        else
                        {
                            biz.UpdateDenunciaCantiere(cantiere);
                        }
                    }
                    else
                    {
                        if (cantiere.Subappalti[0].IdSubappalto.HasValue)
                        {
                            biz.UpdateDenunciaSubappalto(cantiere.Subappalti[0]);
                        }
                        else
                        {
                            biz.InsertDenunciaSubappalto(cantiere.Subappalti[0], cantiere.IdDenunciaCantiere.Value);
                        }
                    }
                    //Context.Items["IdDenuncia"] = cantiere.IdDenunciaCantiere.Value;
                    RadAjaxPanel1.Redirect(String.Format("~/Inserimento/ConfermaInserimentoDenunciaCantiere.aspx?idDenuncia={0}", cantiere.IdDenunciaCantiere));
                }

                break;
        }
        if (RadTabStripCantiere.SelectedIndex < 3)
        {
            if (Page.IsValid)
            {
                do
                {
                    RadTabStripCantiere.SelectedIndex++;
                    RadMultiPageSceltaCantiere.SelectedIndex++;
                } while (!TABATTIVI[RadTabStripCantiere.SelectedIndex]);

                VerificaVisualizzazioneTastoIndietro();
                VerificaVisualizzazioneTastoAvanti();
            }

            IndiceCorrenteTab = RadTabStripCantiere.SelectedIndex;
            GestisciAbilitazionePassi();
            AggiornaLabelPasso();
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        do
        {
            RadTabStripCantiere.SelectedIndex--;
            RadMultiPageSceltaCantiere.SelectedIndex--;
        } while (!TABATTIVI[RadTabStripCantiere.SelectedIndex]);

        VerificaVisualizzazioneTastoIndietro();
        VerificaVisualizzazioneTastoAvanti();

        IndiceCorrenteTab = RadTabStripCantiere.SelectedIndex;
        GestisciAbilitazionePassi();
        AggiornaLabelPasso();
    }

    private void VerificaVisualizzazioneTastoIndietro()
    {
        if (RadTabStripCantiere.SelectedIndex == 0 && RadMultiPageSceltaCantiere.SelectedIndex == 0)
        {
            ButtonIndietro.Visible = false;
        }
        else
        {
            ButtonIndietro.Visible = true;
        }
    }

    private void VerificaVisualizzazioneTastoAvanti()
    {
        Int32 indiceUltimoPasso = RadTabStripCantiere.Tabs.Count - 1;

        if (RadTabStripCantiere.SelectedIndex == indiceUltimoPasso
            && RadMultiPageSceltaCantiere.SelectedIndex == indiceUltimoPasso)
        {
            ButtonAvanti.Text = "Conferma";
        }
        else
        {
            ButtonAvanti.Text = "Avanti";
        }
    }

    private void AggiornaLabelPasso()
    {
        LabelTotalePassi.Text = RadTabStripCantiere.Tabs.Count.ToString();
        LabelPassoAttuale.Text = (RadTabStripCantiere.SelectedIndex + 1).ToString();

    }

    private void CreaRiassunto()
    {
        Cantiere cantiere = CreaCantiere();
        ConfermaDenunciaCantiere1.CaricaRiepilogoDenuncia(cantiere);
    }

    private Cantiere CreaCantiere()
    {
        Cantiere cantiere = new Cantiere();

        cantiere.Data = DateTime.Now;
        cantiere.GuidUtenteTelematiche = (Guid) Membership.GetUser().ProviderUserKey;
        if (ViewState["IdDenuncia"] != null)
        {
            cantiere.IdDenunciaCantiere = (Int32) ViewState["IdDenuncia"];
        }

        cantiere.IndirizzoCantiere = (Indirizzo) ViewState["Indirizzo"];
        cantiere.Ruolo = (RuoloImpresa) Enum.Parse(typeof(RuoloImpresa), RadComboBoxRuolo.SelectedValue);
        if (RadComboBoxRuolo.SelectedValue == RuoloImpresa.Capofila.ToString())
        {
            DatiDenunciaCantiere1.GetDatiCantiere(cantiere);
        }
        DatiSubappaltoDenuciaCantiere1.GetDatiSubappalti(cantiere);

        return cantiere;
    }

    protected void RadComboBoxRuolo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (RadComboBoxRuolo.SelectedValue == RuoloImpresa.Capofila.ToString())
        {
            DatiDenunciaCantiere1.AbilitaModifica(true);
            DatiSubappaltoDenuciaCantiere1.InizializzaDatiCapofila();
        }
        else
        {
            DatiDenunciaCantiere1.Reset();
            DatiDenunciaCantiere1.AbilitaModifica(false);
            DatiSubappaltoDenuciaCantiere1.ResetDatiCapofila();
            DatiSubappaltoDenuciaCantiere1.CaricaDatiImpresa();
        }
    }
}