﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiGenerali.ascx.cs" Inherits="WebControls_DatiGenerali" %>
<%@ Register src="ListaIndirizzi.ascx" tagname="ListaIndirizzi" tagprefix="uc1" %>
<table>
    <tr>
        <td>
            Natura dell'opera (descrizione dettagliata delle attività)<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNaturaOpera" 
                runat="server" 
                Width="400px" 
                Height="70px" 
                TextMode="MultiLine" 
                MaxLength="500">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorTextBoxNaturaOpera"
                runat="server" 
                ControlToValidate="TextBoxNaturaOpera"
                ErrorMessage="Natura dell'opera mancante" 
                ValidationGroup="stop">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Tipologia di Appalto<b>*</b>:
        </td>
        <td>
            <asp:RadioButton
                ID="RadioButtonPrivato"
                runat="server"
                GroupName="tipologiaAppalto"
                Text="Privato"
                Checked="true" />
            <asp:RadioButton
                ID="RadioButtonPubblico"
                runat="server"
                GroupName="tipologiaAppalto"
                Text="Pubblico" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Tipologia di Lavoro<b><asp:Label ID="LabelTipologiaLavoro" runat="server" Text="*"></asp:Label></b>:
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListTipologiaLavoro" 
                runat="server" 
                Width="400px"
                AppendDataBoundItems="true">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorDropDownListTipologiaLavoro"
                runat="server" 
                ControlToValidate="DropDownListTipologiaLavoro"
                ErrorMessage="Selezionare la tipologia del lavoro" 
                ValidationGroup="stop">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            CIG/SCIA/Permesso costr.:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroAppalto" 
                runat="server" 
                Width="400px" 
                MaxLength="50">
            </asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo del cantiere<b>*</b>:
        </td>
        <td>
            <uc1:ListaIndirizzi 
                ID="ListaIndirizzi1" 
                runat="server" />
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorListaIndirizzi"
                runat="server"
                ErrorMessage="Non è stato inserito nessun indirizzo"
                ValidationGroup="stop" 
                onservervalidate="CustomValidatorListaIndirizzi_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>