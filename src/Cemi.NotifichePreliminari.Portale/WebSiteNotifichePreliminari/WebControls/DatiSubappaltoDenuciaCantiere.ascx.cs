﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Type.Enums;
using Telerik.Web.UI;
using Cemi.Presenter;
using System.Collections.Generic;
using System.Reflection;

public partial class WebControls_DatiSubappaltoDenuciaCantiere : System.Web.UI.UserControl
{
    private NotifichePreliminariManager _biz = new NotifichePreliminariManager();
    private const String validationSingoloSubappalto = "singoloSubappalto";

    private SubappaltoCollection subappalti
    {
        get
        {
            if (ViewState["Subappalti"] == null)
            {
                return new SubappaltoCollection();
            }

            return (SubappaltoCollection) ViewState["Subappalti"];
        }

        set
        {
            ViewState["Subappalti"] = value;
        }
    }

    protected void RadGrid1_PreRender(object sender, System.EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.RadGridSubappalti.MasterTableView.Items[1].Edit = true;
            this.RadGridSubappalti.MasterTableView.Rebind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //InizializzazioneDettaglioSubappalti();
        DettaglioSubappalto1.SetValidation(validationSingoloSubappalto);
    }

    public void CaricaCantiere(Cantiere cantiere)
    {
        ImpostaVisualizzazione(cantiere.Ruolo);

        if (cantiere.Capofila != null)
        {
            ViewState["IdImpresa"] = cantiere.Capofila.IdImpresa.Value;
            TextBoxRagioneSociale.Text = cantiere.Capofila.RagioneSociale;
            TextBoxCodiceFiscale.Text = cantiere.Capofila.CodiceFiscale;
            TextBoxPartitaIva.Text = cantiere.Capofila.PartitaIva;
        }

        switch(cantiere.Ruolo)
        {
            case RuoloImpresa.Capofila:
                this.subappalti = cantiere.Subappalti;
                CaricaSubappalti();
            break;
            case RuoloImpresa.Subappalto:
                DettaglioSubappalto1.CaricaSubappalto(cantiere.Subappalti.GetSubappaltoPerCodiceFiscale(_biz.GetUtenteNotifiche().AziendaCodiceFiscale));
            break;
        }


        //InizializzazioneDettaglioSubappalti();
    }

    private void CaricaSubappalti()
    {
        Presenter.CaricaElementiInGridView(
            RadGridSubappalti,
            this.subappalti);
    }

    private void ImpostaVisualizzazione(RuoloImpresa ruolo)
    {
        ViewState["Ruolo"] = ruolo;
        RadMultiPageSceltaCantiere.SelectedIndex = ruolo == RuoloImpresa.Capofila ? RadPageViewCapofila.Index : RadPageViewImpresaSubappalto.Index;

        DettaglioSubappalto1.SingoloSubappalto(ruolo == RuoloImpresa.Subappalto || ruolo == RuoloImpresa.NonPresente);
    }

    //private void InizializzazioneDettaglioSubappalti()
    //{
    //    if (Cantiere != null)
    //    {
    //        if (Cantiere.Ruolo == RuoloImpresa.Subappalto)
    //        {
    //            DettaglioSubappalto1.IsImpresaCapofila = false;
    //            if (Cantiere.Subappalti.Count == 1)
    //            {
    //                DettaglioSubappalto1.CaricaSubappalto(Subappalti[0]);
    //            }
    //            else
    //            {
    //                DettaglioSubappalto1.CaricaDatiImpresa();
    //            }
    //            //else
    //            //{
    //            //    DettaglioSubappalto1.CaricaSubappalto(new Subappalto { Ammontare = 120000 });
    //            //}
    //        }
    //    }
    //}

    public void CaricaDatiImpresa()
    {
        DettaglioSubappalto1.CaricaDatiImpresa();
    }

    public void InizializzaDatiCapofila()
    {
        UtenteNotificheTelematiche utente = _biz.GetUtenteNotifiche();

        if (utente != null)
        {
            TextBoxCodiceFiscale.Text = utente.AziendaCodiceFiscale;
            TextBoxPartitaIva.Text = utente.AziendaPartitaIVA;
            TextBoxRagioneSociale.Text = utente.AziendaRagioneSociale;
        }

        CaricaSubappalti();

        ImpostaVisualizzazione(RuoloImpresa.Capofila);
    }

    public void ResetDatiCapofila()
    {
        TextBoxCodiceFiscale.Text = String.Empty;
        TextBoxPartitaIva.Text = String.Empty;
        TextBoxRagioneSociale.Text = String.Empty;

        ImpostaVisualizzazione(RuoloImpresa.Subappalto);
    }

    public void GetDatiSubappalti(Cantiere cantiere)
    {
        RuoloImpresa ruolo = (RuoloImpresa) ViewState["Ruolo"];

        if (!String.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            cantiere.Capofila = new ImpresaNotificheTelematiche();
            if (ViewState["IdImpresa"] != null)
            {
                cantiere.Capofila.IdImpresa = (Int32) ViewState["IdImpresa"];
            }
            cantiere.Capofila.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
            cantiere.Capofila.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
            cantiere.Capofila.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxPartitaIva.Text);
        }

        if (ruolo == RuoloImpresa.Capofila)
        {
            cantiere.Subappalti = this.subappalti;
        }
        else
        {
            cantiere.Subappalti.Add(DettaglioSubappalto1.GetSubappalto());
        }
    }

    protected void RadGrid1_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        this.RadGridSubappalti.DataSource = this.subappalti;
    }

    protected void RadGrid1_UpdateCommand(object source, GridCommandEventArgs e)
    {
        Page.Validate("DettaglioSubappaltoValidation");

        if (Page.IsValid)
        {
            GridEditableItem editedItem = e.Item as GridEditableItem;
            UserControl userControl = (UserControl) e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
            Subappalto subappalto = (Subappalto) InvokeMethod(userControl, "GetSubappalto");
            String codiceFiscaleCapofila = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

            LabelMessaggioSubappalti.Text = subappalti.UpdateWithCheck(subappalto, editedItem.DataSetIndex, codiceFiscaleCapofila);
            subappalti = subappalti;

            RadGridSubappalti.Rebind();
        }
        else
        {
            e.Canceled = true;
        }
    }

    private object InvokeMethod(object instance, string methodName)
    {
        //Getting the method information using the method info class
        MethodInfo mi = instance.GetType().GetMethod(methodName);

        //invoing the method
        //null- no parameter for the function [or] we can pass the array of parameters
        return mi.Invoke(instance, null);
    }

    private object InvokeMethod(object instance, string methodName, object[] parameters)
    {
        //Getting the method information using the method info class
        MethodInfo mi = instance.GetType().GetMethod(methodName);

        //invoing the method
        //null- no parameter for the function [or] we can pass the array of parameters
        return mi.Invoke(instance, parameters);
    }

    protected void RadGrid1_InsertCommand(object source, GridCommandEventArgs e)
    {
        Page.Validate("DettaglioSubappaltoValidation");

        if (Page.IsValid)
        {
            UserControl userControl = (UserControl) e.Item.FindControl(GridEditFormItem.EditFormUserControlID);
            Subappalto subappalto = (Subappalto) InvokeMethod(userControl, "GetSubappalto");
            String codiceFiscaleCapofila = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

            SubappaltoCollection subTemp = this.subappalti;
            LabelMessaggioSubappalti.Text = subTemp.AddWithCheck(subappalto, codiceFiscaleCapofila);
            subappalti = subTemp;

            RadGridSubappalti.Rebind();
        }
        else
        {
            e.Canceled = true;
        }
    }
    
    protected void RadGrid1_DeleteCommand(object source, GridCommandEventArgs e)
    {
        LabelMessaggioSubappalti.Text = null;
        GridEditableItem editedItem = e.Item as GridEditableItem;

        //int id = (int) editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["IdSubappalto"];
        Subappalto subappalto = subappalti[editedItem.DataSetIndex];

        if (!subappalti.Exists(s => s.Appaltatrice != null && s.Appaltatrice.CodiceFiscale == subappalto.ImpresaInSubappalto.CodiceFiscale))
        {
            subappalti.RemoveAt(editedItem.DataSetIndex);
            subappalti = subappalti;

            RadGridSubappalti.Rebind();
        }
        else
        {
            LabelMessaggioSubappalti.Text = "Il subappalto non può essere eliminato perchè l'impresa è indicata come affidataria in un altro subappalto.";
        }
    }

    protected void RadGrid1_CancelCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            RadGridSubappalti.Rebind();
        }
        catch
        {
        }
    }

    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Subappalto sub = (Subappalto) e.Item.DataItem;

            if (sub.ImpresaInSubappalto != null)
            {
                Label lRagioneSociale = (Label) e.Item.FindControl("LabelRagioneSocialeSub");
                Label lCodiceFiscale = (Label) e.Item.FindControl("LabelCodiceFiscaleSub");
                Label lPartitaIVA = (Label) e.Item.FindControl("LabelPartitaIVASub");
                Label lInizio = (Label) e.Item.FindControl("LabelInizio");
                Label lFine = (Label) e.Item.FindControl("LabelFine");

                lRagioneSociale.Text = sub.ImpresaInSubappalto.RagioneSociale;
                lCodiceFiscale.Text = sub.ImpresaInSubappalto.CodiceFiscale;
                lPartitaIVA.Text = sub.ImpresaInSubappalto.PartitaIva;

                if (sub.DataInizioLavori.HasValue)
                {
                    lInizio.Text = sub.DataInizioLavori.Value.ToString("dd/MM/yyyy");
                }
                if (sub.DataFineLavori.HasValue)
                {
                    lFine.Text = sub.DataFineLavori.Value.ToString("dd/MM/yyyy");
                }
            }
        }
    }

    protected void RadGridSubappalti_EditCommand(object sender, GridCommandEventArgs e)
    {
        //UserControl userControl = (UserControl) e.Item.FindControl(GridEditFormItem.EditFormUserControlID);

        //object[] parameters = new object[3];
        //parameters[1] = subappalti;
        //parameters[2] = false;
        //InvokeMethod(userControl, "CaricaImpreseAffidatarie", parameters);
        PreCaricaListaImprese();
    }

    protected void RadGridSubappalti_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "InitInsert")
        {
            //UserControl userControl = (UserControl) e.Item.FindControl(GridEditFormItem.EditFormUserControlID);

            //object[] parameters = new object[3];
            //parameters[1] = subappalti;
            //parameters[2] = false;
            //InvokeMethod(userControl, "CaricaImpreseAffidatarie", parameters);
            PreCaricaListaImprese();
        }
    }

    private void PreCaricaListaImprese()
    {
        Context.Items["Subappalti"] = subappalti;

        if (!String.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            ImpresaNotificheTelematiche impresaCapofila = new ImpresaNotificheTelematiche();
            impresaCapofila.RagioneSociale = TextBoxRagioneSociale.Text;
            impresaCapofila.CodiceFiscale = TextBoxCodiceFiscale.Text;

            Context.Items["Capofila"] = impresaCapofila;
        }
    }
}