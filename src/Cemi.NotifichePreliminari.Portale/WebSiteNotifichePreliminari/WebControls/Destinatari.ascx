﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Destinatari.ascx.cs" Inherits="WebControls_Destinatari" %>

<asp:UpdatePanel
ID="UpdatePanelEnti"
runat="server">
<ContentTemplate>
    <asp:CheckBox
        ID="CheckBoxSoloMittente"
        runat="server"
        Text="Selezionare se la notifica deve essere solamente inviata al compilatore"
        AutoPostBack="true" 
        oncheckedchanged="CheckBoxSoloMittente_CheckedChanged" />
    <br />
    <br />
    <asp:Panel ID="PanelDestinatari" runat="server">
        <table width="100%" style="height:300px;">
            <tr>
                <td style="width:48%;">
                    <b>
                        Destinatari possibili
                    </b>
                </td>
                <td style="width:4%;">
                </td>
                <td style="width:48%;">
                    <b>
                        Destinatari selezionati
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td valign="top">
                    Tipologia:
                    <asp:DropDownList
                        ID="DropDownListTipologia"
                        runat="server"
                        Width="200px"
                        AppendDataBoundItems="true" AutoPostBack="True" 
                        onselectedindexchanged="DropDownListTipologia_SelectedIndexChanged">
                    </asp:DropDownList>
                    <br />
                    Provincia:
                    <asp:DropDownList
                        ID="DropDownListProvincia"
                        runat="server"
                        Width="200px"
                        AppendDataBoundItems="true" AutoPostBack="True" 
                        onselectedindexchanged="DropDownListProvincia_SelectedIndexChanged">
                    </asp:DropDownList>
                    <br />
            
                    <asp:GridView
                        ID="GridViewEnti"
                        runat="server"
                        Width="100%"
                        AutoGenerateColumns="False"
                        ShowHeader="False"
                        AllowPaging="true"
                        PageSize="5" DataKeyNames="Id" 
                        onpageindexchanging="GridViewEnti_PageIndexChanging" 
                        onrowdatabound="GridViewEnti_RowDataBound" 
                        onselectedindexchanging="GridViewEnti_SelectedIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label
                                        ID="LabelDescrizione"
                                        runat="server">
                                    </asp:Label>
                                    <br />
                                    <small>
                                    <asp:Label
                                        ID="LabelIndirizzo"
                                        runat="server">
                                    </asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField 
                                ButtonType="Image" 
                                SelectText="" 
                                ShowSelectButton="True" 
                                SelectImageUrl="~/Images/frecciaDestra16.png">
                            <ItemStyle Width="10px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun ente disponibile
                        </EmptyDataTemplate>
                    </asp:GridView>
                    
                </td>
                <td valign="top">
                </td>
                <td valign="top">
                    <asp:GridView
                        ID="GridViewEntiSelezionati"
                        runat="server"
                        Width="100%"
                        AutoGenerateColumns="False"
                        ShowHeader="False" onrowdatabound="GridViewEntiSelezionati_RowDataBound" 
                        onselectedindexchanging="GridViewEntiSelezionati_SelectedIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <b>
                                        <asp:Label
                                            ID="LabelDescrizione"
                                            runat="server">
                                        </asp:Label>
                                    </b>
                                    <br />
                                    <small>
                                    <asp:Label
                                        ID="LabelIndirizzo"
                                        runat="server">
                                    </asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField 
                                ButtonType="Image" 
                                SelectText="" 
                                ShowSelectButton="True" 
                                SelectImageUrl="~/Images/pallinoElimina.png">
                            <ItemStyle Width="10px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun ente selezionato
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <a href="../RisorseStatiche/ASLProvinciaFirenze.pdf">Suddivisione territoriale ASL della provincia di Firenze</a>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    </ContentTemplate>
</asp:UpdatePanel>
