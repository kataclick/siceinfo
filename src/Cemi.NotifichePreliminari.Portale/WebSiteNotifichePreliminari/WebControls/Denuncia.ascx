﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Denuncia.ascx.cs" Inherits="WebControls_Denuncia" %>
<%@ Register Src="SelezioneIndirizzoLibero.ascx" TagName="SelezioneIndirizzoLibero"
    TagPrefix="uc1" %><%@ Register Src="SelezioneCantiere.ascx" TagName="SelezioneCantiere" TagPrefix="uc1" %><%@ Register Src="DatiSubappaltoDenuciaCantiere.ascx" TagName="DatiSubappaltoDenuciaCantiere"
    TagPrefix="uc1" %><%@ Register Src="DatiDenunciaCantiere.ascx" TagName="DatiDenunciaCantiere" TagPrefix="uc1" %><%@ Register Src="ConfermaDenunciaCantiere.ascx" TagName="ConfermaDenunciaCantiere"
    TagPrefix="uc1" %>
<style type="text/css">
    .style2
    {
        width: 160px;
    }
</style>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1"
    Width="100%">
    <br />
    <center class="passi">
        <table class="tabellaConBordo">
            <tr>
                <td>
                    Passo <b>
                        <asp:Label ID="LabelPassoAttuale" runat="server"></asp:Label>
                    </b>di <b>
                        <asp:Label ID="LabelTotalePassi" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
        </table>
    </center>
    <br />
    <br />
    <table class="tabellaConBordo" width="100%">
        <tr>
            <td width="100px" valign="top">
                <telerik:RadTabStrip ID="RadTabStripCantiere" runat="server" MultiPageID="RadMultiPageSceltaCantiere"
                    EnableEmbeddedSkins="False" SelectedIndex="0" Width="100%" OnTabClick="RadTabStripCantiereTabClick"
                    Orientation="VerticalLeft" Font-Size="Smaller" Height="650px">
                    <Tabs>
                        <telerik:RadTab Text="- Dati generali" CssClass="tab" SelectedCssClass="tabSelected"
                            HoveredCssClass="tabHovered">
                        </telerik:RadTab>
                        <telerik:RadTab Text="- Dati Cantiere" CssClass="tab" SelectedCssClass="tabSelected"
                            HoveredCssClass="tabHovered" Selected="True" Enabled="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="- Subappalti" CssClass="tab" SelectedCssClass="tabSelected" HoveredCssClass="tabHovered">
                        </telerik:RadTab>
                        <telerik:RadTab Text="- Conferma" CssClass="tab" SelectedCssClass="tabSelected" 
                            HoveredCssClass="tabHovered" Selected="True">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
            <td valign="top">
                <telerik:RadMultiPage ID="RadMultiPageSceltaCantiere" runat="server" Width="100%"
                    SelectedIndex="0" RenderSelectedPageOnly="true" Height="650px">
                    <telerik:RadPageView ID="RadPageViewDatiIndirizzo" runat="server">
                        <div class="standardDiv">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <b>
                                            Dati Generali
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trCodice" runat="server">
                                    <td class="style2">
                                        Codice:
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label
                                                ID="LabelCodice"
                                                runat="server">
                                            </asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        Indirizzo:
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label
                                                ID="LabelIndirizzo1"
                                                runat="server">
                                            </asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label
                                                ID="LabelIndirizzo2"
                                                runat="server">
                                            </asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        Ruolo dell'impresa:
                                    </td>
                                    <td>
                                        <telerik:RadComboBox
                                            ID="RadComboBoxRuolo"
                                            runat="server"
                                            Width="200px" 
                                            AutoPostBack="True" 
                                            EmptyMessage="Selezionare il ruolo"
                                            onselectedindexchanged="RadComboBoxRuolo_SelectedIndexChanged">
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Text="Capofila" Value="Capofila" />
                                                <telerik:RadComboBoxItem runat="server" Text="Subappalto" Value="Subappalto" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorRuolo"
                                            runat="server"
                                            ControlToValidate="RadComboBoxRuolo"
                                            ValidationGroup="datiGenerali"
                                            ErrorMessage="Selezionare il ruolo della propria impresa">
                                            *
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelMessaggioRuolo"
                                                runat="server"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewDatiCantiere" runat="server">
                        <b>
                            Dati Cantiere
                        </b>
                        <br />
                        <br />
                        <div>
                            <small>
                                Questi dati possono essere inseriti/modificati solamente dall'impresa capofila.
                            </small>
                            <br />
                            <uc1:DatiDenunciaCantiere runat="server" ID="DatiDenunciaCantiere1" />
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewSubappalto" runat="server">
                        <b>
                            Subappalti
                        </b>
                        <br />
                        <br />
                        <uc1:DatiSubappaltoDenuciaCantiere runat="server" ID="DatiSubappaltoDenuciaCantiere1" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewConferma" runat="server">
                        Premendo "Conferma" la denuncia verrà salvata nell'anagrafica.
                        <br />
                        <br />
                        <uc1:ConfermaDenunciaCantiere runat="server" ID="ConfermaDenunciaCantiere1" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
    <div style="width:300px; float:left;">
        <asp:ValidationSummary
            ID="ValidationSummary"
            runat="server"
            CssClass="messaggiErrore" />
    </div>
    <div style="text-align:right; width:350px; float:right;">
        <br />
        <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="150px" OnClick="ButtonIndietro_Click"
            Visible="False" />
        &nbsp;
        <asp:Button ID="ButtonAvanti" runat="server" Text="Avanti" Width="150px" OnClick="ButtonAvanti_Click"
            ValidationGroup="stop" />
    </div>
</telerik:RadAjaxPanel>
