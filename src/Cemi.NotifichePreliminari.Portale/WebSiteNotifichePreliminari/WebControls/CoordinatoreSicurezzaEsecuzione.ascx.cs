using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_CoordinatoreSicurezzaEsecuzione : System.Web.UI.UserControl
{
    public event CoordinatoreSicurezzaEsecuzioneEventHandler OnCoordinatoreSicurezzaEsecuzioneNonNominato;
    public event CoordinatoreSicurezzaEsecuzioneComeResponsabileLavoriEventHandler OnStessiDatiResponsabileLavori;
    public event CoordinatoreSicurezzaEsecuzioneComeCoordinatoreSicurezzaProgettazioneEventHandler OnStessiDatiCoordinatoreProgettazione;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void NotificaCartacea()
    {
        PersonaCoordinatoreSicurezzaEsecuzione.NotificaCartacea();
    }

    public Boolean StessiDatiResponsabileLavori
    {
        get { return this.CheckBoxRealizzazioneStessiResponsabile.Checked; }
    }

    public Boolean StessiDatiCoordinatoreProgettazione
    {
        get { return this.CheckBoxRealizzazioneStessiProgettazione.Checked; }
    }

    public void ResetCampi()
    {
        PersonaCoordinatoreSicurezzaEsecuzione.ResetCampi();
    }

    public void ResetTuttiCampi()
    {
        CheckBoxRealizzazioneStessiResponsabile.Checked = false;
        CheckBoxRealizzazioneStessiProgettazione.Checked = false;

        ResetCampi();
    }

    public void CaricaPersona(PersonaNotificheTelematiche persona)
    {
        PersonaCoordinatoreSicurezzaEsecuzione.CaricaPersona(persona);
    }

    private void AbilitazionePannelloComune()
    {
        ResetCampi();

        if (CheckBoxRealizzazioneStessiProgettazione.Checked || CheckBoxRealizzazioneStessiResponsabile.Checked)
        {
            CheckBoxNonNominato.Checked = false;
            PanelCoordinatoreSicurezzaEsecuzione.Enabled = false;
        }
        else
        {
            PanelCoordinatoreSicurezzaEsecuzione.Enabled = true;
        }
    }

    protected void CheckBoxRealizzazioneStessiResponsabile_CheckedChanged(object sender, EventArgs e)
    {
        AbilitazionePannelloComune();

        if (CheckBoxRealizzazioneStessiResponsabile.Checked)
        {
            CheckBoxNonNominato.Checked = false;
            CheckBoxRealizzazioneStessiProgettazione.Checked = false;

            if (OnCoordinatoreSicurezzaEsecuzioneNonNominato != null)
            {
                OnCoordinatoreSicurezzaEsecuzioneNonNominato(CheckBoxNonNominato.Checked);
            }
        }

        if (OnStessiDatiResponsabileLavori != null)
        {
            OnStessiDatiResponsabileLavori(CheckBoxRealizzazioneStessiResponsabile.Checked);
        }
    }

    protected void CheckBoxRealizzazioneStessiProgettazione_CheckedChanged(object sender, EventArgs e)
    {
        AbilitazionePannelloComune();

        if (CheckBoxRealizzazioneStessiProgettazione.Checked)
        {
            CheckBoxNonNominato.Checked = false;
            CheckBoxRealizzazioneStessiResponsabile.Checked = false;

            if (OnCoordinatoreSicurezzaEsecuzioneNonNominato != null)
            {
                OnCoordinatoreSicurezzaEsecuzioneNonNominato(CheckBoxNonNominato.Checked);
            }
        }

        if (OnStessiDatiCoordinatoreProgettazione != null)
        {
            OnStessiDatiCoordinatoreProgettazione(CheckBoxRealizzazioneStessiProgettazione.Checked);
        }
    }

    public void IntegraNotificaConCoordinatoreEsecuzione(NotificaTelematica notifica)
    {
        if (CheckBoxNonNominato.Checked)
        {
            notifica.CoordinatoreEsecuzioneNonNominato = true;
        }
        else
        {
            notifica.CoordinatoreEsecuzioneNonNominato = false;


            if (CheckBoxRealizzazioneStessiResponsabile.Checked)
            {
                notifica.CoordinatoreSicurezzaRealizzazione = notifica.DirettoreLavori;
            }
            else
                if (CheckBoxRealizzazioneStessiProgettazione.Checked)
                {
                    notifica.CoordinatoreSicurezzaRealizzazione = notifica.CoordinatoreSicurezzaProgettazione;
                }
                else
                {
                    notifica.CoordinatoreSicurezzaRealizzazione = PersonaCoordinatoreSicurezzaEsecuzione.CreaPersona();
                }
        }
    }

    public void GestisciStessiDatiResponsabileLavori(Boolean nonNominato)
    {
        CheckBoxRealizzazioneStessiResponsabile.Enabled = !nonNominato;

        if (nonNominato
            && CheckBoxRealizzazioneStessiResponsabile.Checked)
        {
            CheckBoxRealizzazioneStessiResponsabile.Checked = false;
            PanelCoordinatoreSicurezzaEsecuzione.Enabled = true;
        }
    }

    public void GestisciStessiDatiCoordinatoreSicurezzaProgettazione(Boolean nonNominato)
    {
        CheckBoxRealizzazioneStessiProgettazione.Enabled = !nonNominato;

        if (nonNominato
            && CheckBoxRealizzazioneStessiProgettazione.Checked)
        {
            CheckBoxRealizzazioneStessiProgettazione.Checked = false;
            PanelCoordinatoreSicurezzaEsecuzione.Enabled = true;
        }
    }

    public void CaricaCoordinatoreEsecuzione(NotificaTelematica notifica)
    {
        if (!notifica.CoordinatoreEsecuzioneNonNominato)
        {
            PersonaCoordinatoreSicurezzaEsecuzione.CaricaPersona(notifica.CoordinatoreSicurezzaRealizzazione);

            if (notifica.CoordinatoreSicurezzaRealizzazione != null
                && notifica.DirettoreLavori != null
                && notifica.CoordinatoreSicurezzaRealizzazione.IdPersona == notifica.DirettoreLavori.IdPersona)
            {
                CheckBoxRealizzazioneStessiResponsabile.Checked = true;
                PanelCoordinatoreSicurezzaEsecuzione.Enabled = false;
            }
            else
            {
                if (notifica.CoordinatoreSicurezzaRealizzazione != null
                && notifica.CoordinatoreSicurezzaProgettazione != null
                && notifica.CoordinatoreSicurezzaRealizzazione.IdPersona == notifica.CoordinatoreSicurezzaProgettazione.IdPersona)
                {
                    CheckBoxRealizzazioneStessiProgettazione.Checked = true;
                    PanelCoordinatoreSicurezzaEsecuzione.Enabled = false;
                }
            }
        }
        else
        {
            CheckBoxNonNominato.Checked = true;
            PanelCoordinatoreSicurezzaEsecuzione.Enabled = false;

            PersonaCoordinatoreSicurezzaEsecuzione.GestisciValidator(!CheckBoxNonNominato.Checked);
        }

        if (OnStessiDatiResponsabileLavori != null)
        {
            OnStessiDatiResponsabileLavori(CheckBoxRealizzazioneStessiResponsabile.Checked);
        }
    }

    protected void CheckBoxNonNominato_CheckedChanged(object sender, EventArgs e)
    {
        ResetTuttiCampi();

        // Se non � nominato devo diabilitare il controllo e i validator
        PanelCoordinatoreSicurezzaEsecuzione.Enabled = !CheckBoxNonNominato.Checked;
        PersonaCoordinatoreSicurezzaEsecuzione.GestisciValidator(!CheckBoxNonNominato.Checked);

        if (CheckBoxNonNominato.Checked)
        {
            CheckBoxRealizzazioneStessiResponsabile.Checked = false;
            CheckBoxRealizzazioneStessiProgettazione.Checked = false;
        }

        if (OnCoordinatoreSicurezzaEsecuzioneNonNominato != null)
        {
            OnCoordinatoreSicurezzaEsecuzioneNonNominato(CheckBoxNonNominato.Checked);
        }
    }
}
