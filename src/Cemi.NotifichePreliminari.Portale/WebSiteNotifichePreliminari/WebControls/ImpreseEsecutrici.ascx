﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImpreseEsecutrici.ascx.cs" Inherits="WebControls_ImpreseEsecutrici" %>
<%@ Register src="Ricerca/Impresa.ascx" tagname="Impresa" tagprefix="uc1" %>
<%@ Register src="Visualizzazione/Impresa.ascx" tagname="Impresa" tagprefix="uc2" %>
<div>
    <asp:Label
        ID="LabelAvvertimento"
        runat="server"
        Font-Bold="true"
        Text="Inserire le imprese solo se diverse dalle imprese affidatarie">
    </asp:Label>
    <br />
    <br />
    <asp:GridView ID="GridViewImpreseEsecutrici" runat="server" Width="100%" 
        AutoGenerateColumns="False" 
        onrowdatabound="GridViewImpreseEsecutrici_RowDataBound" 
        onrowdeleting="GridViewImpreseEsecutrici_RowDeleting" 
        onrowediting="GridViewImpreseAffidatarie_RowEditing" 
        onrowupdating="GridViewImpreseEsecutrici_RowUpdating" 
        onselectedindexchanging="GridViewImpreseAffidatarie_SelectedIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="Impresa">
                <ItemTemplate>
                    <asp:MultiView
                        ID="MultiViewModifica"
                        runat="server"
                        ActiveViewIndex="0">
                        <asp:View
                            ID="ViewDati"
                            runat="server">
                            <table>
                                <tr>
                                    <td style="width:7%;">
                                        <asp:ImageButton 
                                            ID="ButtonModificaDati" 
                                            runat="server" 
                                            Text="Modifica i dati" 
                                            Width="24px"
                                            CommandName="Select"
                                            ImageUrl="~/Images/modifica.png" />
                                    </td>
                                    <td style="width:93%;">
                                        <b>
                                            <asp:Label ID="LabelImpresaRagioneSociale" runat="server"></asp:Label>
                                        </b>
                                        <br />
                                        <asp:Label ID="LabelImpresaPartitaIva" runat="server"></asp:Label>
                                        <br />
                                        <asp:Label ID="LabelImpresaCodiceFiscale" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b><asp:Label ID="LabelSubappaltoDi" runat="server" Text="Subappalto di:" Visible="false"></asp:Label></b>
                                        <asp:Label ID="LabelSubappaltoDiRagioneSociale" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View
                            ID="ViewModifica"
                            runat="server">
                            <uc2:Impresa ID="Impresa2" runat="server" />
                            <br />
                            <asp:Button
                                ID="ButtonSalvaDati"
                                runat="server"
                                Text="Salva"
                                CommandName="Edit"
                                ValidationGroup="stop"
                                Width="150px" />
                            <asp:Button
                                ID="ButtonAnnullaModifica"
                                runat="server"
                                Text="Annulla"
                                CommandName="Update"
                                Width="150px" />
                        </asp:View>
                    </asp:MultiView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButtonElimina" runat="server" CommandName="Delete" 
                        ImageUrl="~/Images/pallinoElimina.png" />
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna impresa nella lista.
        </EmptyDataTemplate>
    </asp:GridView>
</div>
<div>
    <asp:Button ID="ButtonAltraImpresa" runat="server" Text="Aggiungi un impresa" 
        onclick="ButtonAltraImpresa_Click" />
</div>
<asp:Panel ID="PanelAltraImpresa" runat="server" Visible="false">
    <br />
    <hr />
    <div>
        <table>
            <tr>
                <td>
                    Subappalto di:
                </td>
                <td>
                    <asp:DropDownList 
                        ID="DropDownListSubappalto" 
                        runat="server" 
                        Width="300px" 
                        AppendDataBoundItems="True"></asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <uc1:Impresa 
            ID="Impresa1" 
            runat="server" />
    </div>
    <div>
        <asp:ValidationSummary
            ID="ValidationSummaryNuovaImpresa"
            runat="server"
            ValidationGroup="inserisciImpresa"
            CssClass="messaggiErrore" />
    </div>
    <div>
        <asp:Button ID="ButtonAggiungi" runat="server" Text="Aggiungi alla lista" 
            onclick="ButtonAggiungi_Click" Enabled="False" 
            ValidationGroup="inserisciImpresa" />
        <asp:Label ID="LabelErrore" runat="server" CssClass="messaggiErrore"></asp:Label>
    </div>
</asp:Panel>