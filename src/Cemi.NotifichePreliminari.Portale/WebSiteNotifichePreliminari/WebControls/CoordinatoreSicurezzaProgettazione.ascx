﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CoordinatoreSicurezzaProgettazione.ascx.cs" Inherits="WebControls_CoordinatoreSicurezzaProgettazione" %>
<%@ Register src="Visualizzazione/Persona.ascx" tagname="Persona" tagprefix="uc1" %>
<div>
    <asp:CheckBox 
        ID="CheckBoxNonNominato" 
        Checked="false" 
        runat="server" 
        Text="Non nominato"
        AutoPostBack="True" 
        oncheckedchanged="CheckBoxNonNominato_CheckedChanged" />
</div>
<div>
    <asp:CheckBox ID="CheckBoxProgettazioneStessiResponsabile" Checked="false" 
        runat="server" Text="Stessi dati del Responsabile dei Lavori" 
        AutoPostBack="True" 
        oncheckedchanged="CheckBoxProgettazioneStessiResponsabile_CheckedChanged" />
</div>
<div>
    <asp:Panel ID="PanelCoordinatoreSicurezzaProgettazione" Visible="true" runat="server">
        <uc1:Persona ID="PersonaCoordinatoreSicurezzaProgettazione" runat="server" />
    </asp:Panel>
</div>