using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class WebControls_Riassunto : System.Web.UI.UserControl
{
    private const string TESTOMOSTRA = "Mostra";
    private const string TESTONASCONDI = "Nascondi";
    private const String IMAGEMOSTRA = "~/Images/frecciaGiu.png";
    private const String IMAGENASCONDI = "~/Images/frecciaSu.png";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            #region Inizializzazione visibilitÓ pannelli
            ViewState["MostraDatiGenerali"] = true;
            ViewState["MostraCommittente"] = true;
            ViewState["MostraResponsabileLavori"] = true;
            ViewState["MostraCoordinatoreSicurezzaProgettazione"] = true;
            ViewState["MostraCoordinatoreSicurezzaEsecuzione"] = true;
            ViewState["MostraDateNumeri"] = true;
            ViewState["MostraImpreseAffidatarie"] = true;
            ViewState["MostraImpreseEsecutrici"] = true;
            ViewState["MostraDestinatari"] = true;
            #endregion

            GestisciBottoni();
        }
    }

    #region Gestione dei bottoni Mostra/Nascondi
    private void GestisciBottoni()
    {
        bool mostraDatiGenerali = (bool)ViewState["MostraDatiGenerali"];
        bool mostraCommittente = (bool)ViewState["MostraCommittente"];
        bool mostraResponsabileLavori = (bool)ViewState["MostraResponsabileLavori"];
        bool mostraCoordinatoreSicurezzaProgettazione = (bool)ViewState["MostraCoordinatoreSicurezzaProgettazione"];
        bool mostraCoordinatoreSicurezzaEsecuzione = (bool)ViewState["MostraCoordinatoreSicurezzaEsecuzione"];
        bool mostraDateNumeri = (bool)ViewState["MostraDateNumeri"];
        bool mostraImpreseAffidatarie = (bool)ViewState["MostraImpreseAffidatarie"];
        bool mostraImpreseEsecutrici = (bool)ViewState["MostraImpreseEsecutrici"];
        bool mostraDestinatari = (bool)ViewState["MostraDestinatari"];
        
        if (mostraDatiGenerali)
        {
            ImageButtonNascondiDatiGenerali.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiDatiGenerali.ToolTip = "Nascondi";
            divDatiGenerali.Visible = true;
        }
        else
        {
            ImageButtonNascondiDatiGenerali.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiDatiGenerali.ToolTip = "Mostra";
            divDatiGenerali.Visible = false;
        }

        if (mostraCommittente)
        {
            ImageButtonNascondiCommittente.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiCommittente.ToolTip = "Nascondi";
            divCommittente.Visible = true;
        }
        else
        {
            ImageButtonNascondiCommittente.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiCommittente.ToolTip = "Mostra";
            divCommittente.Visible = false;
        }

        if (mostraResponsabileLavori)
        {
            ImageButtonNascondiResponsabileLavori.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiResponsabileLavori.ToolTip = "Nascondi";
            divResponsabileLavori.Visible = true;
        }
        else
        {
            ImageButtonNascondiResponsabileLavori.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiResponsabileLavori.ToolTip = "Mostra";
            divResponsabileLavori.Visible = false;
        }

        if (mostraCoordinatoreSicurezzaProgettazione)
        {
            ImageButtonNascondiCoordinatoreProgettazione.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiCoordinatoreProgettazione.ToolTip = "Nascondi";
            divCoordinatoreSicurezzaProgettazione.Visible = true;
        }
        else
        {
            ImageButtonNascondiCoordinatoreProgettazione.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiCoordinatoreProgettazione.ToolTip = "Mostra";
            divCoordinatoreSicurezzaProgettazione.Visible = false;
        }

        if (mostraCoordinatoreSicurezzaEsecuzione)
        {
            ImageButtonNascondiCoordinatoreRealizzazione.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiCoordinatoreRealizzazione.ToolTip = "Nascondi";
            divCoordinatoreSicurezzaEsecuzione.Visible = true;
        }
        else
        {
            ImageButtonNascondiCoordinatoreRealizzazione.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiCoordinatoreRealizzazione.ToolTip = "Mostra";
            divCoordinatoreSicurezzaEsecuzione.Visible = false;
        }

        if (mostraDateNumeri)
        {
            ImageButtonNascondiDateNumeri.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiDateNumeri.ToolTip = "Nascondi";
            divDateNumeri.Visible = true;
        }
        else
        {
            ImageButtonNascondiDateNumeri.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiDateNumeri.ToolTip = "Mostra";
            divDateNumeri.Visible = false;
        }

        if (mostraImpreseAffidatarie)
        {
            ImageButtonNascondiImpreseAffidatarie.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiImpreseAffidatarie.ToolTip = "Nascondi";
            divImpreseAffidatarie.Visible = true;
        }
        else
        {
            ImageButtonNascondiImpreseAffidatarie.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiImpreseAffidatarie.ToolTip = "Mostra";
            divImpreseAffidatarie.Visible = false;
        }

        if (mostraImpreseEsecutrici)
        {
            ImageButtonNascondiImpreseEsecutrici.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiImpreseEsecutrici.ToolTip = "Nascondi";
            divImpreseEsecutrici.Visible = true;
        }
        else
        {
            ImageButtonNascondiImpreseEsecutrici.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiImpreseEsecutrici.ToolTip = "Mostra";
            divImpreseEsecutrici.Visible = false;
        }

        if (mostraDestinatari)
        {
            ImageButtonNascondiDestinatari.ImageUrl = IMAGENASCONDI;
            ImageButtonNascondiDestinatari.ToolTip = "Nascondi";
            divDestinatari.Visible = true;
        }
        else
        {
            ImageButtonNascondiDestinatari.ImageUrl = IMAGEMOSTRA;
            ImageButtonNascondiDestinatari.ToolTip = "Mostra";
            divDestinatari.Visible = false;
        }
    }
    #endregion

    public void CaricaNotifica(NotificaTelematica notifica)
    {
        if (notifica.Utente == "Cartacea")
        {
            tableNotificaCartacea.Visible = true;
        }

        // Dati generali
        LabelDatiGeneraliData.Text = notifica.Data.ToShortDateString();
        LabelDatiGeneraliNaturaOpera.Text = notifica.NaturaOpera;
        LabelDatiGeneraliTipologiaAppalto.Text = notifica.AppaltoPrivato ? "Privato" : "Pubblico";
        if (notifica.TipologiaLavoro != null)
        {
            LabelDatiGeneraliTipologiaLavoro.Text = notifica.TipologiaLavoro.ToString();
        }
        LabelDatiGeneraliNumeroAppalto.Text = notifica.NumeroAppalto;
        Presenter.CaricaElementiInListView(ListViewDatiGeneraliIndirizzi, notifica.Indirizzi);

        // Committente/i
        if (notifica.Committente != null)
        {
            //LabelCommittenteTipologia.Text = notifica.Committente.TipologiaCommittente.ToString();

            // Persona
            LabelCommittentePersonaCognome.Text = notifica.Committente.PersonaCognome;
            LabelCommittentePersonaNome.Text = notifica.Committente.PersonaNome;
            LabelCommittentePersonaCodiceFiscale.Text = notifica.Committente.PersonaCodiceFiscale;
            LabelCommittentePersonaIndirizzo.Text = notifica.Committente.PersonaIndirizzo;
            LabelCommittentePersonaCitta.Text = notifica.Committente.PersonaComune;
            LabelCommittentePersonaProvincia.Text = notifica.Committente.PersonaProvincia;
            LabelCommittentePersonaCap.Text = notifica.Committente.PersonaCap;
            LabelCommittentePersonaTelefono.Text = notifica.Committente.PersonaTelefono;
            LabelCommittentePersonaFax.Text = notifica.Committente.PersonaFax;
            LabelCommittentePersonaCellulare.Text = notifica.Committente.PersonaCellulare;
            LabelCommittentePersonaEmail.Text = notifica.Committente.PersonaEmail;
            
            // Ente
            LabelCommittenteEnteRagioneSociale.Text = notifica.Committente.RagioneSociale;
            LabelCommittenteEntePartitaIVA.Text = notifica.Committente.PartitaIva;
            LabelCommittenteEnteCodiceFiscale.Text = notifica.Committente.CodiceFiscale;
            LabelCommittenteEnteIndirizzo.Text = notifica.Committente.Indirizzo;
            LabelCommittenteEnteCitta.Text = notifica.Committente.Comune;
            LabelCommittenteEnteProvincia.Text = notifica.Committente.Provincia;
            LabelCommittenteEnteCap.Text = notifica.Committente.Cap;
            LabelCommittenteEnteTelefono.Text = notifica.Committente.Telefono;
            LabelCommittenteEnteFax.Text = notifica.Committente.Fax;
        }

        // Responsabile dei lavori
        if (!notifica.ResponsabileCommittente && notifica.DirettoreLavori != null)
        {
            // Persona
            LabelResponsabileLavoriPersonaCognome.Text = notifica.DirettoreLavori.PersonaCognome;
            LabelResponsabileLavoriPersonaNome.Text = notifica.DirettoreLavori.PersonaNome;
            LabelResponsabileLavoriPersonaCodiceFiscale.Text = notifica.DirettoreLavori.PersonaCodiceFiscale;
            LabelResponsabileLavoriPersonaIndirizzo.Text = notifica.DirettoreLavori.Indirizzo;
            LabelResponsabileLavoriPersonaCitta.Text = notifica.DirettoreLavori.PersonaComune;
            LabelResponsabileLavoriPersonaProvincia.Text = notifica.DirettoreLavori.PersonaProvincia;
            LabelResponsabileLavoriPersonaCap.Text = notifica.DirettoreLavori.PersonaCap;
            LabelResponsabileLavoriPersonaTelefono.Text = notifica.DirettoreLavori.Telefono;
            LabelResponsabileLavoriPersonaFax.Text = notifica.DirettoreLavori.Fax;
            LabelResponsabileLavoriPersonaCellulare.Text = notifica.DirettoreLavori.PersonaCellulare;
            LabelResponsabileLavoriPersonaEmail.Text = notifica.DirettoreLavori.PersonaEmail;

            // Ente
            //LabelResponsabileLavoriEnteRagioneSociale.Text = notifica.DirettoreLavori.RagioneSociale;
            //LabelResponsabileLavoriEntePartitaIva.Text = notifica.DirettoreLavori.EntePartitaIva;
            //LabelResponsabileLavoriEnteCodiceFiscale.Text = notifica.DirettoreLavori.EnteCodiceFiscale;
            //LabelResponsabileLavoriEnteIndirizzo.Text = notifica.DirettoreLavori.EnteIndirizzo;
            //LabelResponsabileLavoriEnteCitta.Text = notifica.DirettoreLavori.EnteComune;
            //LabelResponsabileLavoriEnteProvincia.Text = notifica.DirettoreLavori.EnteProvincia;
            //LabelResponsabileLavoriEnteCap.Text = notifica.DirettoreLavori.EnteCap;
            //LabelResponsabileLavoriEnteTelefono.Text = notifica.DirettoreLavori.EnteTelefono;
            //LabelResponsabileLavoriEnteFax.Text = notifica.DirettoreLavori.EnteFax;
        }
        else
        {
            if (notifica.ResponsabileCommittente)
            {
                CheckBoxResponsabileLavoriNonNominato.Checked = true;
            }
        }
        
        // Coordinatore sicurezza progettazione
        if (notifica.CoordinatoreSicurezzaProgettazione != null)
        {
            // Persona
            LabelCoordinatoreProgettazionePersonaCognome.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaCognome;
            LabelCoordinatoreProgettazionePersonaNome.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaNome;
            LabelCoordinatoreProgettazionePersonaCodiceFiscale.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaCodiceFiscale;
            LabelCoordinatoreProgettazionePersonaIndirizzo.Text = notifica.CoordinatoreSicurezzaProgettazione.Indirizzo;
            LabelCoordinatoreProgettazionePersonaCitta.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaComune;
            LabelCoordinatoreProgettazionePersonaProvincia.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaProvincia;
            LabelCoordinatoreProgettazionePersonaCap.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaCap;
            LabelCoordinatoreProgettazionePersonaTelefono.Text = notifica.CoordinatoreSicurezzaProgettazione.Telefono;
            LabelCoordinatoreProgettazionePersonaFax.Text = notifica.CoordinatoreSicurezzaProgettazione.Fax;
            LabelCoordinatoreProgettazionePersonaCellulare.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaCellulare;
            LabelCoordinatoreProgettazionePersonaEmail.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaEmail;

            // Ente
            //LabelCoordinatoreProgettazioneEnteRagioneSociale.Text = notifica.CoordinatoreSicurezzaProgettazione.RagioneSociale;
            //LabelCoordinatoreProgettazioneEntePartitaIva.Text = notifica.CoordinatoreSicurezzaProgettazione.EntePartitaIva;
            //LabelCoordinatoreProgettazioneEnteCodiceFiscale.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteCodiceFiscale;
            //LabelCoordinatoreProgettazioneEnteIndirizzo.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteIndirizzo;
            //LabelCoordinatoreProgettazioneEnteCitta.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteComune;
            //LabelCoordinatoreProgettazioneEnteProvincia.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteProvincia;
            //LabelCoordinatoreProgettazioneEnteCap.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteCap;
            //LabelCoordinatoreProgettazioneEnteTelefono.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteTelefono;
            //LabelCoordinatoreProgettazioneEnteFax.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteFax;
        }
        else
        {
            if (notifica.CoordinatoreProgettazioneNonNominato)
            {
                CheckBoxCoordinatoreProgettazioneNonNominato.Checked = true;
            }
        }

        // Coordinatore sicurezza esecuzione
        if (notifica.CoordinatoreSicurezzaRealizzazione != null)
        {
            // Persona
            LabelCoordinatoreEsecuzionePersonaCognome.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaCognome;
            LabelCoordinatoreEsecuzionePersonaNome.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaNome;
            LabelCoordinatoreEsecuzionePersonaCodiceFiscale.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaCodiceFiscale;
            LabelCoordinatoreEsecuzionePersonaIndirizzo.Text = notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo;
            LabelCoordinatoreEsecuzionePersonaCitta.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaComune;
            LabelCoordinatoreEsecuzionePersonaProvincia.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaProvincia;
            LabelCoordinatoreEsecuzionePersonaCap.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaCap;
            LabelCoordinatoreEsecuzionePersonaTelefono.Text = notifica.CoordinatoreSicurezzaRealizzazione.Telefono;
            LabelCoordinatoreEsecuzionePersonaFax.Text = notifica.CoordinatoreSicurezzaRealizzazione.Fax;
            LabelCoordinatoreEsecuzionePersonaCellulare.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaCellulare;
            LabelCoordinatoreEsecuzionePersonaEmail.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaEmail;

            // Ente
            //LabelCoordinatoreEsecuzioneEnteRagioneSociale.Text = notifica.CoordinatoreSicurezzaRealizzazione.RagioneSociale;
            //LabelCoordinatoreEsecuzioneEntePartitaIva.Text = notifica.CoordinatoreSicurezzaRealizzazione.EntePartitaIva;
            //LabelCoordinatoreEsecuzioneEnteCodiceFiscale.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteCodiceFiscale;
            //LabelCoordinatoreEsecuzioneEnteIndirizzo.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteIndirizzo;
            //LabelCoordinatoreEsecuzioneEnteCitta.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteComune;
            //LabelCoordinatoreEsecuzioneEnteProvincia.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteProvincia;
            //LabelCoordinatoreEsecuzioneEnteCap.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteCap;
            //LabelCoordinatoreEsecuzioneEnteTelefono.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteTelefono;
            //LabelCoordinatoreEsecuzioneEnteFax.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteFax;
        }
        else
        {
            if (notifica.CoordinatoreEsecuzioneNonNominato)
            {
                CheckBoxCoordinatoreEsecuzioneNonNominato.Checked = true;
            }
        }

        // Date e numeri
        LabelDateNumeriAmmontareComplessivo.Text = Presenter.StampaValoreMonetario(notifica.AmmontareComplessivo);
        LabelDateNumeriDataInizioLavori.Text = Presenter.StampaDataFormattataClassica(notifica.DataInizioLavori);
        LabelDateNumeriDurataLavori.Text = notifica.Durata.ToString();
        LabelDateNumeriDataFineLavori.Text = Presenter.StampaDataFormattataClassica(notifica.DataFineLavori);
        LabelDateNumeriNumeroLavoratori.Text = notifica.NumeroMassimoLavoratori.ToString();
        LabelDateNumeriNumeroImprese.Text = notifica.NumeroImprese.ToString();
        LabelDateNumeriNumeroAutonomi.Text = notifica.NumeroLavoratoriAutonomi.ToString();
        LabelDateNumeriNumeroUominiGiorno.Text = notifica.NumeroGiorniUomo.ToString();

        // Imprese affidatarie
        Presenter.CaricaElementiInGridView(
            GridViewImpreseAffidatarie,
            notifica.ImpreseAffidatarie,
            0);
        
        // Imprese esecutrici
        Presenter.CaricaElementiInGridView(
            GridViewImpreseEsecutrici,
            notifica.ImpreseEsecutrici,
            0);

        // Destinatari
        Presenter.CaricaElementiInGridView(
            GridViewDestinatari,
            notifica.EntiDestinatari,
            0);
    }

    protected void GridViewImpreseEsecutrici_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoNotificheTelematiche subappalto = (SubappaltoNotificheTelematiche)e.Row.DataItem;

            Label lImpresaRagioneSociale = (Label)e.Row.FindControl("LabelImpresaRagioneSociale");
            Label lImpresaPartitaIva = (Label)e.Row.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label)e.Row.FindControl("LabelImpresaCodiceFiscale");
            Label lSubappaltoDiRagioneSociale = (Label)e.Row.FindControl("LabelSubappaltoDiRagioneSociale");
            Label lSubappaltoDi = (Label)e.Row.FindControl("LabelSubappaltoDi");

            if (subappalto.ImpresaSelezionata != null)
            {
                lImpresaRagioneSociale.Text = subappalto.ImpresaSelezionata.RagioneSociale;
                lImpresaPartitaIva.Text = String.Format("P.Iva: {0}", subappalto.ImpresaSelezionata.PartitaIva);
                lImpresaCodiceFiscale.Text = String.Format("Cod.Fisc.: {0}", subappalto.ImpresaSelezionata.CodiceFiscale);
            }

            if (subappalto.AppaltataDa != null)
            {
                lSubappaltoDi.Visible = true;
                lSubappaltoDiRagioneSociale.Text = subappalto.AppaltataDa.RagioneSociale;
            }
        }
    }

    #region Eventi bottoni per mostrare o nascondere sezioni

    protected void ImageButtonNascondiDatiGenerali_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraDatiGenerali = (bool)ViewState["MostraDatiGenerali"];
        ViewState["MostraDatiGenerali"] = !mostraDatiGenerali;

        GestisciBottoni();
    }

    protected void ImageButtonNascondiCommittente_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraCommittente = (bool)ViewState["MostraCommittente"];
        ViewState["MostraCommittente"] = !mostraCommittente;

        GestisciBottoni();
    }

    protected void ImageButtonNascondiResponsabileLavori_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraResponsabileLavori = (bool)ViewState["MostraResponsabileLavori"];
        ViewState["MostraResponsabileLavori"] = !mostraResponsabileLavori;

        GestisciBottoni();
    }
    protected void ImageButtonNascondiCoordinatoreProgettazione_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraCoordinatoreProgettazione = (bool)ViewState["MostraCoordinatoreSicurezzaProgettazione"];
        ViewState["MostraCoordinatoreSicurezzaProgettazione"] = !mostraCoordinatoreProgettazione;

        GestisciBottoni();
    }
    protected void ImageButtonNascondiCoordinatoreRealizzazione_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraCoordinatoreEsecuzione = (bool)ViewState["MostraCoordinatoreSicurezzaEsecuzione"];
        ViewState["MostraCoordinatoreSicurezzaEsecuzione"] = !mostraCoordinatoreEsecuzione;

        GestisciBottoni();
    }
    protected void ImageButtonNascondiDateNumeri_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraDateNumeri = (bool)ViewState["MostraDateNumeri"];
        ViewState["MostraDateNumeri"] = !mostraDateNumeri;

        GestisciBottoni();
    }
    protected void ImageButtonNascondiImpreseAffidatarie_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraImpreseAffidatarie = (bool)ViewState["MostraImpreseAffidatarie"];
        ViewState["MostraImpreseAffidatarie"] = !mostraImpreseAffidatarie;

        GestisciBottoni();
    }
    protected void ImageButtonNascondiImpreseEsecutrici_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraImpreseEsecutrici = (bool)ViewState["MostraImpreseEsecutrici"];
        ViewState["MostraImpreseEsecutrici"] = !mostraImpreseEsecutrici;

        GestisciBottoni();
    }
    protected void ImageButtonNascondiDestinatari_Click(object sender, ImageClickEventArgs e)
    {
        bool mostraDestinatari = (bool)ViewState["MostraDestinatari"];
        ViewState["MostraDestinatari"] = !mostraDestinatari;

        GestisciBottoni();
    }
    #endregion

    protected void GridViewImpreseAffidatarie_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoNotificheTelematiche subappalto = (SubappaltoNotificheTelematiche)e.Row.DataItem;
            Label lRagioneSociale = (Label)e.Row.FindControl("LabelRagioneSociale");
            Label lPartitaIva = (Label)e.Row.FindControl("LabelPartitaIva");
            Label lCodiceFiscale = (Label)e.Row.FindControl("LabelCodiceFiscale");

            lRagioneSociale.Text = subappalto.ImpresaSelezionataRagioneSociale;
            lPartitaIva.Text = String.Format("P.Iva: {0}", subappalto.ImpresaSelezionataPartitaIva);
            lCodiceFiscale.Text = String.Format("Cod.Fisc.: {0}", subappalto.ImpresaSelezionataCodiceFiscale);
        }
    }

    protected void GridViewDestinatari_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            EnteDestinatario ente = (EnteDestinatario)e.Row.DataItem;

            Label lDescrizione = (Label)e.Row.FindControl("LabelDescrizione");
            Label lIndirizzo = (Label)e.Row.FindControl("LabelIndirizzo");
            Label lEmail = (Label)e.Row.FindControl("LabelEmail");

            lDescrizione.Text = ente.Descrizione;
            lIndirizzo.Text = ente.IndirizzoCompleto;
            lEmail.Text = ente.EmailTo;
        }
    }
}
