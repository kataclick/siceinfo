﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class WebControls_Destinatari : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();
    public EnteDestinatarioCollection EntiSelezionati
    {
        get
        {
            if (ViewState["EntiSelezionati"] != null)
            {
                return (EnteDestinatarioCollection)ViewState["EntiSelezionati"];
            }
            else
            {
                return new EnteDestinatarioCollection();
            }
        }
        set
        {
            ViewState["EntiSelezionati"] = value;
        }
    }

    public Int32 DestinatariSelezionati
    {
        get
        {
            return this.EntiSelezionati.Count;
        }
    }

    public Boolean SoloMittente
    {
        get
        {
            return this.CheckBoxSoloMittente.Checked;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaCombo();
            CaricaEnti(0);
            CaricaEntiSelezionati();
        }
    }

    public void PreImpostaFiltroProvincia(String provincia)
    {
        if (DropDownListProvincia.Items.FindByValue(provincia) != null)
        {
            DropDownListProvincia.SelectedValue = provincia;
            CaricaEnti(0);
        }
    }

    public void CaricaDestinatari(NotificaTelematica notifica)
    {
        this.EntiSelezionati = notifica.EntiDestinatari;

        if (this.EntiSelezionati.Count == 0)
        {
            CheckBoxSoloMittente.Checked = true;
            AbilitaDisabilitaDestinatari(false);
        }
        else
        {
            Indirizzo indirizzo = notifica.Indirizzi[0];
            PreImpostaFiltroProvincia(indirizzo.Provincia);
        }
    }

    private void CaricaCombo()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListProvincia,
            biz.GetEntiDestinatariProvince(),
            String.Empty,
            String.Empty);

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipologia,
            biz.GetEntiDestinatariTipologie(null),
            "Descrizione",
            "Id");
    }

    private void CaricaEnti(Int32 pagina)
    {
        String provincia = DropDownListProvincia.SelectedValue;
        Int32? idTipologia = !String.IsNullOrEmpty(DropDownListTipologia.SelectedValue) ? (Int32?)Int32.Parse(DropDownListTipologia.SelectedValue) : null;

        Presenter.CaricaElementiInGridView(
            GridViewEnti,
            biz.GetEntiDestinatari(idTipologia, provincia),
            pagina);
    }

    private void CaricaEntiSelezionati()
    {
        Presenter.CaricaElementiInGridView(
            GridViewEntiSelezionati,
            this.EntiSelezionati,
            0);
    }

    protected void GridViewEnti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idEnte = (Int32)GridViewEnti.DataKeys[e.NewSelectedIndex].Values["Id"];
        EnteDestinatario ente = biz.GetEnteDestinatario(idEnte);

        AggiungiEnte(ente);
    }

    private void AggiungiEnte(EnteDestinatario ente)
    {
        EnteDestinatarioCollection enti = this.EntiSelezionati;

        Boolean presente = false;
        foreach (EnteDestinatario enteTemp in enti)
        {
            if (ente.Id == enteTemp.Id)
            {
                presente = true;
            }
        }

        if (!presente)
        {
            enti.Add(ente);
            this.EntiSelezionati = enti;
            CaricaEntiSelezionati();
        }
    }

    public void IntegraNotificaConDestinatari(NotificaTelematica notifica)
    {
        notifica.EntiDestinatari = this.EntiSelezionati;
    }

    protected void GridViewEnti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaEnti(e.NewPageIndex);
    }

    protected void GridViewEnti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            EnteDestinatario ente = (EnteDestinatario)e.Row.DataItem;

            Label lDescrizione = (Label)e.Row.FindControl("LabelDescrizione");
            Label lIndirizzo = (Label)e.Row.FindControl("LabelIndirizzo");

            lDescrizione.Text = ente.Descrizione;
            lIndirizzo.Text = ente.IndirizzoCompleto;
        }
    }

    protected void GridViewEntiSelezionati_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            EnteDestinatario ente = (EnteDestinatario)e.Row.DataItem;

            Label lDescrizione = (Label)e.Row.FindControl("LabelDescrizione");
            Label lIndirizzo = (Label)e.Row.FindControl("LabelIndirizzo");

            lDescrizione.Text = ente.Descrizione;
            lIndirizzo.Text = ente.IndirizzoCompleto;
        }
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaEnti(0);
    }

    protected void DropDownListTipologia_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaEnti(0);
    }

    protected void GridViewEntiSelezionati_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        EnteDestinatarioCollection enti = this.EntiSelezionati;
        enti.RemoveAt(e.NewSelectedIndex);
        this.EntiSelezionati = enti;

        CaricaEntiSelezionati();
    }

    protected void CheckBoxSoloMittente_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaDisabilitaDestinatari(!CheckBoxSoloMittente.Checked);
    }

    protected void AbilitaDisabilitaDestinatari(Boolean abilita)
    {
        if (!abilita)
        {
            this.EntiSelezionati = null;
            CaricaEntiSelezionati();

            PanelDestinatari.Enabled = false;
        }
        else
        {
            PanelDestinatari.Enabled = true;
        }
    }
}