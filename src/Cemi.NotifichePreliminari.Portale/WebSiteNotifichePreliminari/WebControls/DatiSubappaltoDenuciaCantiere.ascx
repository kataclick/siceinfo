﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiSubappaltoDenuciaCantiere.ascx.cs"
    Inherits="WebControls_DatiSubappaltoDenuciaCantiere" %>
<!-- content start -->
<style type="text/css">
    .style1
    {
        width: 200px;
    }
    .style2
    {
        width: 80px;
    }
</style>
<%--        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

            <script type="text/javascript">
                function RowDblClick(sender, eventArgs) {
                    sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                }
            </script>

        </telerik:RadCodeBlock>--%>
<%@ Register Src="DettaglioSubappalto.ascx" TagName="DettaglioSubappalto"
    TagPrefix="uc1" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGrid1">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<div style="width:100%">
    <b>Impresa capofila</b>
    <asp:Panel
        ID="PanelCapofila"
        runat="server"
        Enabled="false">
        <table width="100%">
            <tr>
                <td class="style1">
                    Ragione sociale
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Partita IVA
                </td>
                <td>
                    <asp:TextBox ID="TextBoxPartitaIva" runat="server" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Codice Fiscale
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="250px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<br />
<telerik:RadMultiPage ID="RadMultiPageSceltaCantiere" runat="server" Width="100%"
    SelectedIndex="0" RenderSelectedPageOnly="False">
    <telerik:RadPageView ID="RadPageViewCapofila" runat="server">
        <asp:Label
            ID="LabelMessaggioSubappalti"
            runat="server"
            ForeColor="Red">
        </asp:Label>
        <telerik:RadGrid ID="RadGridSubappalti" runat="server" GridLines="None" AllowPaging="True"
            CssClass="RadGrid" AllowSorting="True" AutoGenerateColumns="False" ShowStatusBar="True"
            OnPreRender="RadGrid1_PreRender" 
            OnNeedDataSource="RadGrid1_NeedDataSource" OnUpdateCommand="RadGrid1_UpdateCommand"
            OnInsertCommand="RadGrid1_InsertCommand" OnDeleteCommand="RadGrid1_DeleteCommand"
            OnCancelCommand="RadGrid1_CancelCommand" CellSpacing="0" 
            onitemdatabound="RadGrid1_ItemDataBound" Width="100%" Height="380px" 
            PageSize="3" oneditcommand="RadGridSubappalti_EditCommand" 
            onitemcommand="RadGridSubappalti_ItemCommand" >
            <ClientSettings>
                <Selecting CellSelectionMode="None" />
                <Scrolling AllowScroll="True"></Scrolling>
            </ClientSettings>
            <MasterTableView CommandItemDisplay="Top" DataKeyNames="IdSubappalto" 
                Width="100%">
                <CommandItemTemplate>
                  <asp:Button ID="ButtonNuovoSubappalto" Text="Aggiungi subappalto" 
                   Runat="server" Font-Size="X-Small" CommandName="InitInsert" Width="150px" Height="20px"></asp:Button>
                </CommandItemTemplate>
                <NoRecordsTemplate>
                    Nessun subappalto presente.
                </NoRecordsTemplate>
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="edit" 
                        FilterControlAltText="Filter edit column" ImageUrl="~/images/modifica.png" 
                        UniqueName="edit">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" 
                        HeaderText="Impresa" UniqueName="impresa">
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <b>
                                            <asp:Label
                                                ID="LabelRagioneSocialeSub"
                                                runat="server">
                                            </asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        <small>
                                            Codice fiscale:
                                        </small>
                                    </td>
                                    <td>
                                        <small>
                                            <asp:Label
                                                ID="LabelCodiceFiscaleSub"
                                                runat="server">
                                            </asp:Label>
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        <small>
                                            Partita IVA:
                                        </small>
                                    </td>
                                    <td>
                                        <small>
                                            <asp:Label
                                                ID="LabelPartitaIVASub"
                                                runat="server">
                                            </asp:Label>
                                        </small>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="180px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="TipologiaAttività" 
                        FilterControlAltText="Filter Tipologia column" HeaderText="Opere" 
                        UniqueName="Tipologia">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ammontare" 
                        FilterControlAltText="Filter Ammontare column" HeaderText="Ammontare" 
                        UniqueName="Ammontare" DataFormatString="{0:C}">
                        <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" 
                        HeaderText="Periodo" UniqueName="periodo">
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                        Inizio: 
                                    </td>
                                    <td>
                                        <asp:Label
                                            ID="LabelInizio"
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Fine: 
                                    </td>
                                    <td>
                                        <asp:Label
                                            ID="LabelFine"
                                            runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="delete" 
                        FilterControlAltText="Filter delete column" ImageUrl="~/images/pallinoElimina.png" 
                        UniqueName="delete">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings EditFormType="WebUserControl" 
                    UserControlName="../WebControls/DettaglioSubappalto.ascx">
                    <EditColumn UniqueName="EditCommandColumn1">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid></telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewImpresaSubappalto" runat="server">
        <uc1:DettaglioSubappalto ID="DettaglioSubappalto1" runat="server" />
    </telerik:RadPageView>
</telerik:RadMultiPage>
