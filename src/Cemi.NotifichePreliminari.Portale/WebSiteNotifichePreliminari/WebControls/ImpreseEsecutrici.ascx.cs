using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_ImpreseEsecutrici : System.Web.UI.UserControl
{
    private const String URLELIMINA = "~/Images/pallinoElimina.png";
    private const String URLELIMINADISABILITATO = "~/Images/pallinoEliminaDisabilitato.png";

    public event SubappaltoCreatedOrDeletedEventHandler OnSubappaltoCreatoOCancellato;

    private const Int32 INDICEVISTARAGIONESOCIALE = 0;
    private const Int32 INDICEVISTAMODIFICA = 1;

    public SubappaltoNotificheTelematicheCollection ListaSubappaltoDi
    {
        get
        {
            SubappaltoNotificheTelematicheCollection impreseAffidatarie = ViewState["ImpreseAffidatarie"] as SubappaltoNotificheTelematicheCollection;
            SubappaltoNotificheTelematicheCollection impreseEsecutrici = ViewState["ImpreseEsecutrici"] as SubappaltoNotificheTelematicheCollection;

            SubappaltoNotificheTelematicheCollection listaSubappaltoDi = new SubappaltoNotificheTelematicheCollection();
            if (impreseAffidatarie != null)
            {
                listaSubappaltoDi.AddRange(impreseAffidatarie);
            }
            if (impreseEsecutrici != null)
            {
                listaSubappaltoDi.AddRange(impreseEsecutrici);
            }

            return listaSubappaltoDi;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Registrazione eventi
        Impresa1.OnImpresaSelected += new ImpresaSelectedEventHandler(Impresa1_OnImpresaSelected);
        Impresa1.OnImpresaNuova += new ImpresaNuovaEventHandler(Impresa1_OnImpresaNuova);
        #endregion

        if (!Page.IsPostBack)
        {
            if (ViewState["ImpreseEsecutrici"] == null)
            {
                ViewState["ImpreseEsecutrici"] = new SubappaltoNotificheTelematicheCollection();
            }

            CaricaImpreseEsecutriciSelezionate();
        }
    }

    public void NotificaCartacea()
    {
        Impresa1.NotificaCartacea();
    }

    void Impresa1_OnImpresaNuova()
    {
        ButtonAggiungi.Enabled = true;
    }

    void Impresa1_OnImpresaSelected()
    {
        ButtonAggiungi.Enabled = true;
    }

    private void CaricaImpreseEsecutriciSelezionate()
    {
        SubappaltoNotificheTelematicheCollection impreseEsecutrici = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"];
        Presenter.CaricaElementiInGridView(GridViewImpreseEsecutrici, impreseEsecutrici, 0);
    }

    public void CaricaImpreseAffidatarie(SubappaltoNotificheTelematicheCollection impreseAffidatarie)
    {
        ViewState["ImpreseAffidatarie"] = impreseAffidatarie;
        CaricaListaSubappaltoDi();
    }

    private void CaricaListaSubappaltoDi()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListSubappalto,
            this.ListaSubappaltoDi,
            "ImpresaSelezionataRagioneSociale",
            "");
    }

    protected void ButtonAggiungi_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SubappaltoNotificheTelematiche subappalto = CreaAppalto();
            if (AggiungiImpresaALista(subappalto))
            {
                Impresa1.ResetCampi();
                CaricaImpreseEsecutriciSelezionate();
                PanelAltraImpresa.Visible = false;
                ButtonAltraImpresa.Enabled = true;
                CaricaListaSubappaltoDi();

                if (OnSubappaltoCreatoOCancellato != null)
                {
                    OnSubappaltoCreatoOCancellato((SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"]);
                }
            }
        }
    }

    private Boolean AggiungiImpresaALista(SubappaltoNotificheTelematiche subappalto)
    {
        Boolean impresaDaInserire = true;

        if (
            // E' stato dichiarato un appalto con la stessa impresa nella selezionata e nell'appaltata da
             subappalto.ImpresaSelezionata != null && subappalto.AppaltataDa != null
             &&
            (
            (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.PartitaIva) && subappalto.ImpresaSelezionata.PartitaIva == subappalto.AppaltataDa.PartitaIva)
            ||
            (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.CodiceFiscale) && subappalto.ImpresaSelezionata.CodiceFiscale == subappalto.AppaltataDa.CodiceFiscale)
            )
            )
        {
            LabelErrore.Text = "Non pu� essere inserito un appalto mettendo la stessa impresa come Selezionata e come Subappalto di.";
            impresaDaInserire = false;
        }

        SubappaltoNotificheTelematicheCollection impreseEsecutrici = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"];
        if (impresaDaInserire)
        {
            foreach (SubappaltoNotificheTelematiche sub in impreseEsecutrici)
            {
                if (
                    // Non � stata dichiarata l'impresa che ha dato l'appalto e si inserisce la stessa impresa selezionata
                     subappalto.AppaltataDa == null && sub.AppaltataDa == null
                     &&
                    (
                    (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.PartitaIva) && sub.ImpresaSelezionata.PartitaIva == subappalto.ImpresaSelezionata.PartitaIva)
                    ||
                    (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.CodiceFiscale) && sub.ImpresaSelezionata.CodiceFiscale == subappalto.ImpresaSelezionata.CodiceFiscale)
                    )
                    )
                {
                    LabelErrore.Text = "Partita Iva o Codice fiscale gi� presente nella lista delle Imprese Esecutrici";
                    impresaDaInserire = false;
                    break;
                }
                else
                {
                    if (
                        // E' stata dichiarata l'impresa che ha dato l'appalto e si inserisce una coppia gi� presente
                     subappalto.AppaltataDa != null && sub.AppaltataDa != null
                     &&
                    (
                    (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.PartitaIva) && sub.ImpresaSelezionata.PartitaIva == subappalto.ImpresaSelezionata.PartitaIva)
                    ||
                    (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.CodiceFiscale) && sub.ImpresaSelezionata.CodiceFiscale == subappalto.ImpresaSelezionata.CodiceFiscale)
                    )
                    &&
                    (
                    (!String.IsNullOrEmpty(subappalto.AppaltataDa.PartitaIva) && sub.AppaltataDa.PartitaIva == subappalto.AppaltataDa.PartitaIva)
                    ||
                    (!String.IsNullOrEmpty(subappalto.AppaltataDa.CodiceFiscale) && sub.AppaltataDa.CodiceFiscale == subappalto.AppaltataDa.CodiceFiscale)
                    )
                    )
                    {
                        LabelErrore.Text = "Appalto gi� presente nella lista delle Imprese Esecutrici";
                        impresaDaInserire = false;
                        break;
                    }
                }
            }
        }

        SubappaltoNotificheTelematicheCollection impreseAffidatarie = ViewState["ImpreseAffidatarie"] as SubappaltoNotificheTelematicheCollection;

        if (impresaDaInserire && impreseAffidatarie != null)
        {
            foreach (SubappaltoNotificheTelematiche sub in impreseAffidatarie)
            {
                if (
                    // E' stato dichiarato un appalto con impresa selezionata un impresa che era tra le affidatarie
                 sub.ImpresaSelezionata != null
                 &&
                (
                (!String.IsNullOrEmpty(sub.ImpresaSelezionata.PartitaIva) && sub.ImpresaSelezionata.PartitaIva == subappalto.ImpresaSelezionata.PartitaIva)
                ||
                (!String.IsNullOrEmpty(sub.ImpresaSelezionata.CodiceFiscale) && sub.ImpresaSelezionata.CodiceFiscale == subappalto.ImpresaSelezionata.CodiceFiscale)
                )
                )
                {
                    LabelErrore.Text = "Non pu� essere inserita un impresa gi� presente tra le imprese affidatarie";
                    impresaDaInserire = false;
                    break;
                }
            }
        }

        if (impresaDaInserire)
        {
            Presenter.SvuotaCampo(LabelErrore);
            impreseEsecutrici.Add(subappalto);
            ViewState["ImpreseEsecutrici"] = impreseEsecutrici;
        }

        return impresaDaInserire;
    }

    private SubappaltoNotificheTelematiche CreaAppalto()
    {
        SubappaltoNotificheTelematiche subappalto = new SubappaltoNotificheTelematiche();
        subappalto.ImpresaSelezionata = Impresa1.CreaImpresa();

        if (this.ListaSubappaltoDi != null && DropDownListSubappalto.SelectedIndex > 0)
        {
            subappalto.AppaltataDa = this.ListaSubappaltoDi[DropDownListSubappalto.SelectedIndex - 1].ImpresaSelezionata;
        }

        return subappalto;
    }

    protected void ButtonAltraImpresa_Click(object sender, EventArgs e)
    {
        PanelAltraImpresa.Visible = true;
        ButtonAltraImpresa.Enabled = false;
        DropDownListSubappalto.SelectedIndex = 0;
        Impresa1.DisabilitaNuovo();
    }

    public SubappaltoNotificheTelematicheCollection GetImpreseEsecutrici()
    {
        SubappaltoNotificheTelematicheCollection impreseEsecutrici = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"];
        return impreseEsecutrici;
    }

    protected void GridViewImpreseEsecutrici_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoNotificheTelematiche subappalto = (SubappaltoNotificheTelematiche)e.Row.DataItem;

            Label lImpresaRagioneSociale = (Label)e.Row.FindControl("LabelImpresaRagioneSociale");
            Label lImpresaPartitaIva = (Label)e.Row.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label)e.Row.FindControl("LabelImpresaCodiceFiscale");
            Label lSubappaltoDiRagioneSociale = (Label)e.Row.FindControl("LabelSubappaltoDiRagioneSociale");
            Label lSubappaltoDi = (Label)e.Row.FindControl("LabelSubappaltoDi");
            ImageButton ibElimina = (ImageButton)e.Row.FindControl("ImageButtonElimina");
            
            if (subappalto.ImpresaSelezionata != null)
            {
                lImpresaRagioneSociale.Text = subappalto.ImpresaSelezionata.RagioneSociale;
                lImpresaPartitaIva.Text = String.Format("P.Iva: {0}", subappalto.ImpresaSelezionata.PartitaIva);
                lImpresaCodiceFiscale.Text = String.Format("Cod.Fisc.: {0}", subappalto.ImpresaSelezionata.CodiceFiscale);

                SubappaltoNotificheTelematicheCollection impreseEsecutrici = ViewState["ImpreseEsecutrici"] as SubappaltoNotificheTelematicheCollection;
                if (impreseEsecutrici != null 
                    &&
                    impreseEsecutrici.ImpresaPresente(
                    subappalto.ImpresaSelezionata.IdImpresaTelematica,
                    subappalto.ImpresaSelezionata.IdTemporaneo))
                {
                    ibElimina.ImageUrl = URLELIMINADISABILITATO;
                    ibElimina.Enabled = false;
                }
            }

            if (subappalto.AppaltataDa != null)
            {
                lSubappaltoDi.Visible = true;
                lSubappaltoDiRagioneSociale.Text = subappalto.AppaltataDa.RagioneSociale;
            }
        }
    }

    public void CaricaImpreseEsecutrici(NotificaTelematica notifica)
    {
        CaricaImpreseAffidatarie(notifica.ImpreseAffidatarie);

        ViewState["ImpreseEsecutrici"] = notifica.ImpreseEsecutrici;
        CaricaImpreseEsecutriciSelezionate();
    }

    protected void GridViewImpreseEsecutrici_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Int32 indiceSubappalto = e.RowIndex;
        SubappaltoNotificheTelematicheCollection subappalti = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"];

        subappalti.RemoveAt(indiceSubappalto);
        CaricaImpreseEsecutriciSelezionate();

        if (OnSubappaltoCreatoOCancellato != null)
        {
            OnSubappaltoCreatoOCancellato((SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"]);
        }
    }

    protected void GridViewImpreseAffidatarie_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        MultiView mvModifica = (MultiView)GridViewImpreseEsecutrici.Rows[e.NewSelectedIndex].FindControl("MultiViewModifica");
        mvModifica.ActiveViewIndex = INDICEVISTAMODIFICA;

        WebControls_Visualizzazione_Impresa visImp = (WebControls_Visualizzazione_Impresa)GridViewImpreseEsecutrici.Rows[e.NewSelectedIndex].FindControl("Impresa2");
        SubappaltoNotificheTelematicheCollection subappalti = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"];
        visImp.CaricaImpresa(subappalti[e.NewSelectedIndex].ImpresaSelezionata);
    }

    protected void GridViewImpreseAffidatarie_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Page.Validate("stop");

        if (Page.IsValid)
        {
            WebControls_Visualizzazione_Impresa visImp = (WebControls_Visualizzazione_Impresa)GridViewImpreseEsecutrici.Rows[e.NewEditIndex].FindControl("Impresa2");
            SubappaltoNotificheTelematicheCollection subappalti = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseEsecutrici"];
            subappalti[e.NewEditIndex].ImpresaSelezionata = visImp.CreaImpresa();
            ViewState["ImpreseEsecutrici"] = subappalti;

            MultiView mvModifica = (MultiView)GridViewImpreseEsecutrici.Rows[e.NewEditIndex].FindControl("MultiViewModifica");
            mvModifica.ActiveViewIndex = INDICEVISTARAGIONESOCIALE;
            CaricaImpreseEsecutriciSelezionate();
        }
    }

    protected void GridViewImpreseEsecutrici_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        MultiView mvModifica = (MultiView)GridViewImpreseEsecutrici.Rows[e.RowIndex].FindControl("MultiViewModifica");
        mvModifica.ActiveViewIndex = INDICEVISTARAGIONESOCIALE;
    }
}
