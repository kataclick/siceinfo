using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class WebControls_DatiGenerali : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieLavoro();
        }
    }

    public void NotificaCartacea()
    {
        RequiredFieldValidatorDropDownListTipologiaLavoro.Enabled = false;
        LabelTipologiaLavoro.Visible = false;
    }

    private void CaricaTipologieLavoro()
    {
        if (DropDownListTipologiaLavoro.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListTipologiaLavoro,
                biz.GetTipologieLavoro(),
                "Descrizione",
                "Id");
        }
    }

    public void IntegraNotificaConDatiGenerali(Notifica notifica)
    {
        notifica.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
        notifica.NumeroAppalto = Presenter.NormalizzaCampoTesto(TextBoxNumeroAppalto.Text);
        notifica.AppaltoPrivato = RadioButtonPrivato.Checked;
        if (!String.IsNullOrEmpty(DropDownListTipologiaLavoro.SelectedValue))
        {
            notifica.TipologiaLavoro = new TipologiaLavoro()
                {
                    Id = Int32.Parse(DropDownListTipologiaLavoro.SelectedValue),
                    Descrizione = DropDownListTipologiaLavoro.Text
                };
        }

        notifica.Indirizzi = ListaIndirizzi1.GetIndirizzi();
    }

    public IndirizzoCollection GetIndirizzi()
    {
        return ListaIndirizzi1.GetIndirizzi();
    }

    public void CaricaNotifica(Notifica notifica)
    {
        CaricaTipologieLavoro();

        TextBoxNaturaOpera.Text = notifica.NaturaOpera;
        TextBoxNumeroAppalto.Text = notifica.NumeroAppalto;
        RadioButtonPrivato.Checked = notifica.AppaltoPrivato;
        RadioButtonPubblico.Checked = !notifica.AppaltoPrivato;
        if (notifica.TipologiaLavoro != null)
        {
            DropDownListTipologiaLavoro.SelectedValue = notifica.TipologiaLavoro.Id.ToString();
        }

        ListaIndirizzi1.CaricaIndirizzi(notifica.Indirizzi);
    }

    #region Metodi per i Validator
    protected void CustomValidatorListaIndirizzi_ServerValidate(object source, ServerValidateEventArgs args)
    {
        IndirizzoCollection indirizzi = ListaIndirizzi1.GetIndirizzi();

        if (indirizzi.Count > 0)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
    #endregion
}
