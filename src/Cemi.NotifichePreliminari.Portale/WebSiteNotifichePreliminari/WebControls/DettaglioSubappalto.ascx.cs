﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using System.ComponentModel;
using System.Web.Security;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Telerik.Web.UI;

public partial class WebControls_DettaglioSubappalto : System.Web.UI.UserControl
{
    private object _dataItem;

    private NotifichePreliminariManager _biz = new NotifichePreliminariManager();

    public void SingoloSubappalto(Boolean singoloSubappalto)
    {
        RadComboBoxCodiceFiscaleAppaltatrice.AllowCustomText = singoloSubappalto;

        if (singoloSubappalto)
        {
            LabelAffidatoDa.Text = "Codice fiscale";
        }
        else
        {
            LabelAffidatoDa.Text = "Ragione sociale";
        }

        btnUpdate.Visible = !singoloSubappalto;
        btnInsert.Visible = !singoloSubappalto;
        btnCancel.Visible = !singoloSubappalto;

        LabelRagioneSociale.Enabled = !singoloSubappalto;
        TextBoxRagioneSociale.Enabled = !singoloSubappalto;
        LabelCodiceFiscale.Enabled = !singoloSubappalto;
        TextBoxCodiceFiscale.Enabled = !singoloSubappalto;
        LabelPartitaIva.Enabled = !singoloSubappalto;
        TextBoxPartitaIva.Enabled = !singoloSubappalto;

        if (singoloSubappalto)
        {
            ImageButtonCercaImpresa.ImageUrl = "~/images/dettagliNotificaGrigia16.png";
            ImageButtonCercaImpresa.Enabled = false;
        }
        else
        {
            ImageButtonCercaImpresa.ImageUrl = "~/images/dettagliNotifica16.png";
            ImageButtonCercaImpresa.Enabled = true;
        }

        ViewState["SingoloSubappalto"] = singoloSubappalto;
    }

    //public void CaricaImpreseAffidatarie(Impresa capofila, SubappaltoCollection subappalti, Boolean libero)
    //{
    //    RadComboBoxCodiceFiscaleAppaltatrice.AllowCustomText = libero;

    //    RadComboBoxCodiceFiscaleAppaltatrice.Items.Clear();
    //    RadComboBoxCodiceFiscaleAppaltatrice.Text = String.Empty;

    //    if (capofila != null)
    //    {
    //        RadComboBoxCodiceFiscaleAppaltatrice.Items.Add(new Telerik.Web.UI.RadComboBoxItem(capofila.CodiceFiscale, capofila.CodiceFiscale));
    //    }

    //    if (subappalti != null)
    //    {
    //        RadComboBoxCodiceFiscaleAppaltatrice.DataSource = subappalti;
    //        RadComboBoxCodiceFiscaleAppaltatrice.DataTextField = "ImpresaInSubappalto.CodiceFiscale";
    //        RadComboBoxCodiceFiscaleAppaltatrice.DataValueField = "ImpresaInSubappalto.CodiceFiscale";
    //    }
    //}

    public void SetValidation(String validationGroup)
    {
        RequiredFieldValidatorAmmontare.ValidationGroup = validationGroup;
        RequiredFieldValidatorCodiceFiscale.ValidationGroup = validationGroup;
        RequiredFieldValidatorCodiceFiscaleAppaltatrice.ValidationGroup = validationGroup;
        RequiredFieldValidatorDataFine.ValidationGroup = validationGroup;
        RequiredFieldValidatorDataInizio.ValidationGroup = validationGroup;
        RequiredFieldValidatorPartitaIva.ValidationGroup = validationGroup;
        RequiredFieldValidatorRagioneSociale.ValidationGroup = validationGroup;
        RequiredFieldValidatorTipoOpera.ValidationGroup = validationGroup;
    }

    public WebControls_DettaglioSubappalto()
    {
        IsImpresaCapofila = true;
    }

    public void CaricaSubappalto(Subappalto subappalto)
    {
        if (subappalto != null)
        {
            DataItem = subappalto;
            CaricaDatiSubapalto();
        }
    }

    public void CaricaDatiImpresa()
    {
        MembershipUser user = Membership.GetUser();
        UtenteNotificheTelematiche utente = _biz.GetUtenteTelematiche((Guid)user.ProviderUserKey);

        TextBoxRagioneSociale.Text = utente.AziendaRagioneSociale;
        TextBoxRagioneSociale.Enabled = false;
        LabelRagioneSociale.Enabled = false;
        TextBoxCodiceFiscale.Text = utente.AziendaCodiceFiscale;
        TextBoxCodiceFiscale.Enabled = false;
        LabelCodiceFiscale.Enabled = false;
        TextBoxPartitaIva.Text = utente.AziendaPartitaIVA;
        TextBoxPartitaIva.Enabled = false;
        LabelPartitaIva.Enabled = false;
    }

    private void CaricaDatiSubapalto()
    {
        Boolean? singoloSubappalto = null;

        if (ViewState["SingoloSubappalto"] != null)
        {
            singoloSubappalto = (Boolean) ViewState["SingoloSubappalto"];
        }

        Subappalto subappalto = DataItem as Subappalto;
        if (subappalto!= null)
        {
            if (subappalto.IdSubappalto.HasValue)
            {
                ViewState["IdSubappalto"] = subappalto.IdSubappalto.Value;
            }
            TextBoxAmmontare.Text = subappalto.Ammontare.ToString();
            if (subappalto.ImpresaInSubappalto != null)
            {
                TextBoxCodiceFiscale.Text = subappalto.ImpresaInSubappalto.CodiceFiscale;
                TextBoxPartitaIva.Text = subappalto.ImpresaInSubappalto.PartitaIva;
                TextBoxRagioneSociale.Text = subappalto.ImpresaInSubappalto.RagioneSociale;
                if (!IsImpresaCapofila)
                {
                    TextBoxCodiceFiscale.Enabled = false;
                    TextBoxPartitaIva.Enabled = false;
                    TextBoxRagioneSociale.Enabled = false;
                }
            }
            ComboTipoOpera.SelectedValue = subappalto.TipologiaAttività;

            RadDatePickerDataInizio.SelectedDate = subappalto.DataInizioLavori;

            RadDatePickerDataFine.SelectedDate = subappalto.DataFineLavori;

            TextBoxNote.Text = subappalto.Note;

            if (subappalto.Appaltatrice != null)
            {
                //TextBoxCodiceFiscaleAppaltatrice.Text = subappalto.Appaltatrice.CodiceFiscale;

                if (ViewState["SingoloSubappalto"] != null)
                {
                    RadComboBoxCodiceFiscaleAppaltatrice.Text = subappalto.Appaltatrice.CodiceFiscale;
                    CaricaInfoAppaltatrice();
                }
                else
                {
                    CaricaElencoImprese(subappalto.ImpresaInSubappalto.CodiceFiscale);
                    RadComboBoxCodiceFiscaleAppaltatrice.SelectedValue = subappalto.Appaltatrice.CodiceFiscale;
                }
            }

            btnInsert.Visible = false;
            if (!singoloSubappalto.HasValue || !singoloSubappalto.Value)
            {
                btnUpdate.Visible = true;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //SetVisibilitaBottoni();

        if (!Page.IsPostBack)
        {
            CaricaTipologieOpera();
        }

        CaricaElencoImprese(null);
    }

    private void CaricaElencoImprese(String codiceFiscaleImpresa)
    {
        List<ImpresaNotificheTelematiche> subappalti = null;

        if (Context.Items["Subappalti"] != null)
        {
            subappalti = new List<ImpresaNotificheTelematiche>();

            foreach (Subappalto sub in (SubappaltoCollection) Context.Items["Subappalti"])
            {
                if (String.IsNullOrEmpty(codiceFiscaleImpresa) || codiceFiscaleImpresa != sub.ImpresaInSubappalto.CodiceFiscale)
                {
                    subappalti.Add(sub.ImpresaInSubappalto);
                }
            }
            //RadComboBoxCodiceFiscaleAppaltatrice.AllowCustomText = libero;

            //if (capofila != null)
            //{
            //    RadComboBoxCodiceFiscaleAppaltatrice.Items.Add(new Telerik.Web.UI.RadComboBoxItem(capofila.CodiceFiscale, capofila.CodiceFiscale));
            //}
        }

        if (Context.Items["Capofila"] != null)
        {
            ImpresaNotificheTelematiche imp = (ImpresaNotificheTelematiche)Context.Items["Capofila"];

            if (String.IsNullOrEmpty(codiceFiscaleImpresa) || codiceFiscaleImpresa != imp.CodiceFiscale)
            {
                if (subappalti != null)
                {
                    subappalti.Insert(0, new ImpresaNotificheTelematiche()
                        {
                            RagioneSociale = imp.RagioneSociale,
                            CodiceFiscale = imp.CodiceFiscale
                        });
                }
                else
                {
                    subappalti = new List<ImpresaNotificheTelematiche>();

                    subappalti.Add(new ImpresaNotificheTelematiche()
                        {
                            RagioneSociale = imp.RagioneSociale,
                            CodiceFiscale = imp.CodiceFiscale
                        });
                }
            }
        }

        if (subappalti != null)
        {
            RadComboBoxCodiceFiscaleAppaltatrice.Items.Clear();
            RadComboBoxCodiceFiscaleAppaltatrice.Text = String.Empty;

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                RadComboBoxCodiceFiscaleAppaltatrice,
                subappalti,
                "RagioneSociale",
                "CodiceFiscale");

        }
    }

    //private void SetVisibilitaBottoni()
    //{

    //    if (!IsImpresaCapofila)
    //    {
    //        btnCancel.Visible = false;
    //        btnUpdate.Visible = false;
    //        btnInsert.Visible = false;
    //    }
    //    else
    //    {
    //        if (DataItem != null)
    //        {
    //            btnUpdate.Visible = !(DataItem is Telerik.Web.UI.GridInsertionObject);
    //            btnInsert.Visible = (DataItem is Telerik.Web.UI.GridInsertionObject);
    //            btnCancel.Visible = true;
    //        }
    //    }
    //}

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///        Required method for Designer support - do not modify
    ///        the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.DataBinding += new System.EventHandler(this.DettagliSubappalto_DataBinding);


    }
    #endregion

    public object DataItem
    {
        get
        {
            return this._dataItem;
        }
        set
        {
            this._dataItem = value;
        }
    }

    public bool IsImpresaCapofila { set; get; }

    protected void DettagliSubappalto_DataBinding(object sender, System.EventArgs e)
    {
        CaricaDatiSubapalto();
    }

    protected void ComboTipoOpera_Load(object sender, EventArgs e)
    {
        CaricaTipologieOpera();
    }

    private void CaricaTipologieOpera()
    {
        if (ComboTipoOpera.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                ComboTipoOpera,
                _biz.GetTipologieAttivita(),
                String.Empty,
                String.Empty);
        }
    }

    public Subappalto GetSubappalto()
    {
        Subappalto subappalto = new Subappalto();

        if (ViewState["IdSubappalto"] != null)
        {
            subappalto.IdSubappalto = (Int32) ViewState["IdSubappalto"];
        }

        //if (!String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscaleAppaltatrice.Text)))
        //{
        //    subappalto.Appaltatrice = new ImpresaNotificheTelematiche();
        //    subappalto.Appaltatrice.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscaleAppaltatrice.Text);
        //}
        subappalto.Appaltatrice = new ImpresaNotificheTelematiche();
        if (!String.IsNullOrEmpty(RadComboBoxCodiceFiscaleAppaltatrice.SelectedValue))
        {
            subappalto.Appaltatrice.CodiceFiscale = RadComboBoxCodiceFiscaleAppaltatrice.SelectedValue;
        }
        else
        {
            subappalto.Appaltatrice.CodiceFiscale = RadComboBoxCodiceFiscaleAppaltatrice.Text;
        }
        
        Decimal ammontare = 0;
        if (Decimal.TryParse(TextBoxAmmontare.Text, out ammontare))
        {
            subappalto.Ammontare = ammontare;
        }
        subappalto.ImpresaInSubappalto = new ImpresaNotificheTelematiche();
        subappalto.ImpresaInSubappalto.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        subappalto.ImpresaInSubappalto.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
        subappalto.ImpresaInSubappalto.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxPartitaIva.Text);
        subappalto.TipologiaAttività = ComboTipoOpera.SelectedValue;
        subappalto.DataInizioLavori = RadDatePickerDataInizio.SelectedDate;
        subappalto.DataFineLavori = RadDatePickerDataFine.SelectedDate;
        subappalto.Note = Presenter.NormalizzaCampoTesto(TextBoxNote.Text);

        return subappalto;
    }

    protected void ImageButtonCercaImpresa_Click(object sender, ImageClickEventArgs e)
    {
        String codiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

        ImpresaNotificheTelematiche impresa = _biz.GetImpreseTelematicheDaSiceNewEAnagrafica(codiceFiscale);
        if (impresa != null)
        {
            TextBoxCodiceFiscale.Text = Presenter.NormalizzaCampoTesto(impresa.CodiceFiscale);
            TextBoxPartitaIva.Text = impresa.PartitaIva;
            TextBoxRagioneSociale.Text = Presenter.NormalizzaCampoTesto(impresa.RagioneSociale);
        }
    }

    #region Custom Validators
    protected void RequiredFieldValidatorCodiceFiscaleAppaltatrice_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["SingoloSubappalto"] != null)
        {
            if (String.IsNullOrWhiteSpace(RadComboBoxCodiceFiscaleAppaltatrice.Text))
            {
                args.IsValid = false;
            }
        }
        else
        {
            if (RadComboBoxCodiceFiscaleAppaltatrice.SelectedItem == null)
            {
                args.IsValid = false;
            }
        }
    }
    #endregion

    protected void RadComboBoxCodiceFiscaleAppaltatrice_TextChanged(object sender, EventArgs e)
    {
        CaricaInfoAppaltatrice();
    }

    private void CaricaInfoAppaltatrice()
    {
        String codiceFiscale = Presenter.NormalizzaCampoTesto(RadComboBoxCodiceFiscaleAppaltatrice.Text);

        ImpresaNotificheTelematiche impresa = _biz.GetImpreseTelematicheDaSiceNewEAnagrafica(codiceFiscale);
        LabelAppaltatrice.Text = null;
        if (impresa != null)
        {
            LabelAppaltatrice.Text = impresa.RagioneSociale;
        }
    }
}