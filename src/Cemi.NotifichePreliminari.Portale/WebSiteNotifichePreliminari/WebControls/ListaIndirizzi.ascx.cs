using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.Geocode.Business;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_ListaIndirizzi : System.Web.UI.UserControl
{
    private const Int16 INDICEINDIRIZZOINSERITO = 0;
    private const Int16 INDICEINDIRIZZOPROPOSTO = 1;

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (ViewState["Indirizzi"] == null)
            {
                ViewState["Indirizzi"] = new IndirizzoCollection();
            }
        }
    }

    protected void ButtonGeocodifica_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Indirizzo indirizzo = CreaIndirizzo();

            LabelIndirizzoFornito.Text = indirizzo.IndirizzoCompleto;
            IndirizzoCollection indirizzi = biz.GeoCodeGoogleMultiplo(indirizzo.IndirizzoPerGeocoder);

            ViewState["IndirizziGeocodificati"] = indirizzi;
            Presenter.CaricaElementiInGridView(GridViewIndirizzi, indirizzi, 0);

            LabelSceltaIndirizzo.Visible = false;
            if (indirizzi != null)
            {
                if (indirizzi.Count > 0)
                {
                    LabelSceltaIndirizzo.Visible = true;
                }
            }
            

            MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOPROPOSTO;
        }
    }

    private Indirizzo CreaIndirizzo()
    {
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.Indirizzo1 = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        indirizzo.Civico = Presenter.NormalizzaCampoTesto(TextBoxCivico.Text);
        indirizzo.InfoAggiuntiva = Presenter.NormalizzaCampoTesto(TextBoxInfoAggiuntiva.Text);
        indirizzo.Cap = Presenter.NormalizzaCampoTesto(TextBoxCap.Text);
        indirizzo.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        indirizzo.Provincia = Presenter.NormalizzaCampoTesto(TextBoxProvincia.Text);

        return indirizzo;
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (ViewState["IndirizziGeocodificati"] != null)
        {
            IndirizzoCollection indirizziGeo = (IndirizzoCollection)ViewState["IndirizziGeocodificati"];
            indirizziGeo[e.NewSelectedIndex].Civico = Presenter.NormalizzaCampoTesto(TextBoxCivico.Text);
            indirizziGeo[e.NewSelectedIndex].InfoAggiuntiva = Presenter.NormalizzaCampoTesto(TextBoxInfoAggiuntiva.Text);

            AggiungiIndirizzoAllaLista(indirizziGeo[e.NewSelectedIndex]);
            CaricaIndirizzi();

            SvuotaCampi();
            MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOINSERITO;
            PanelNuovoIndirizzo.Visible = false;
            ButtonNuovoIndirizzo.Visible = true;
        }
    }

    private void CaricaIndirizzi()
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection)ViewState["Indirizzi"];
        Presenter.CaricaElementiInCheckBoxList(CheckBoxListIndirizzi, indirizzi, "IndirizzoCompleto", "IndirizzoCompleto");

        if (indirizzi.Count > 0)
        {
            ButtonEliminaSelezione.Enabled = true;
            LabelNessunIndirizzo.Visible = false;
        }
        else
        {
            ButtonEliminaSelezione.Enabled = false;
            LabelNessunIndirizzo.Visible = true;
        }
    }
    
    private void SvuotaCampi()
    {
        Presenter.SvuotaCampo(TextBoxIndirizzo);
        Presenter.SvuotaCampo(TextBoxCivico);
        Presenter.SvuotaCampo(TextBoxInfoAggiuntiva);
        Presenter.SvuotaCampo(TextBoxCap);
        Presenter.SvuotaCampo(TextBoxComune);
        Presenter.SvuotaCampo(TextBoxProvincia);
        
        Presenter.SvuotaCampo(LabelIndirizzoFornito);
    }
    
    protected void ButtonAggiungiIndirizzo_Click(object sender, EventArgs e)
    {
        Indirizzo indirizzo = CreaIndirizzo();
        AggiungiIndirizzoAllaLista(indirizzo);
        CaricaIndirizzi();

        SvuotaCampi();
        MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOINSERITO;
        PanelNuovoIndirizzo.Visible = false;
        ButtonNuovoIndirizzo.Visible = true;
    }

    private void AggiungiIndirizzoAllaLista(Indirizzo indirizzo)
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection)ViewState["Indirizzi"];
        indirizzi.Add(indirizzo);
        ViewState["Indirizzi"] = indirizzi;
    }

    protected void ButtonEliminaSelezione_Click(object sender, EventArgs e)
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection)ViewState["Indirizzi"];

        while (CheckBoxListIndirizzi.SelectedIndex != -1)
        {
            int indice = CheckBoxListIndirizzi.SelectedIndex;

            CheckBoxListIndirizzi.Items.RemoveAt(indice);
            indirizzi.RemoveAt(indice);
        }

        CaricaIndirizzi();
    }

    protected void ButtonNuovoIndirizzo_Click(object sender, EventArgs e)
    {
        PanelNuovoIndirizzo.Visible = true;
        ButtonNuovoIndirizzo.Visible = false;
    }

    public IndirizzoCollection GetIndirizzi()
    {
        IndirizzoCollection indirizzi = null;

        Page.Validate("ListaIndirizzi");

        //if (Page.IsValid)
        //{
            if (ViewState["Indirizzi"] != null)
            {
                indirizzi = (IndirizzoCollection)ViewState["Indirizzi"];
            }
        //}

        return indirizzi;
    }

    public void CaricaIndirizzi(IndirizzoCollection indirizzi)
    {
        ViewState["Indirizzi"] = indirizzi;
        CaricaIndirizzi();
    }
}
