using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_ResponsabileDeiLavori : System.Web.UI.UserControl
{
    public event ResponsabileDeiLavoriEventHandler OnResponsabileDeiLavori;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void NotificaCartacea()
    {
        PersonaResponsabileDeiLavori.NotificaCartacea();
    }

    protected void CheckBoxNonNominato_CheckedChanged(object sender, EventArgs e)
    {
        ResetCampi();

        // Se non � nominato devo diabilitare il controllo e i validator
        PanelResponsabileLavori.Enabled = !CheckBoxNonNominato.Checked;
        PersonaResponsabileDeiLavori.GestisciValidator(!CheckBoxNonNominato.Checked);

        if (OnResponsabileDeiLavori != null)
        {
            OnResponsabileDeiLavori(CheckBoxNonNominato.Checked);
        }
    }

    public void ResetCampi()
    {
        PersonaResponsabileDeiLavori.ResetCampi();
    }

    public PersonaNotificheTelematiche GetResponsabileLavori()
    {
        return PersonaResponsabileDeiLavori.CreaPersona();
    }

    public void IntegraNotificaConResponsabileLavori(NotificaTelematica notifica)
    {
        if (CheckBoxNonNominato.Checked)
        {
            notifica.ResponsabileNonNominato = true;
            notifica.ResponsabileCommittente = true;
        }
        else
        {
            notifica.ResponsabileNonNominato = false;
            notifica.DirettoreLavori = PersonaResponsabileDeiLavori.CreaPersona();
        }
    }

    public void CaricaResponsabileDeiLavori(NotificaTelematica notifica)
    {
        if (!notifica.ResponsabileCommittente)
        {
            PersonaResponsabileDeiLavori.CaricaPersona(notifica.DirettoreLavori);
        }
        else
        {
            CheckBoxNonNominato.Checked = true;
            PanelResponsabileLavori.Enabled = false;

            PersonaResponsabileDeiLavori.GestisciValidator(!CheckBoxNonNominato.Checked);
        }

        if (OnResponsabileDeiLavori != null)
        {
            OnResponsabileDeiLavori(CheckBoxNonNominato.Checked);
        }
    }
}
