﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebControls_FirmaDigitale : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public byte[] Certificato
    {
        get
        {
            if (FileUploadCertificato.HasFile)
            {
                return FileUploadCertificato.FileBytes;
            }

            return null;
        }
    }

    public String Password
    {
        get
        {
            return TextBoxPassword.Text;
        }
    }
}