﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.Geocode.Type;
using Cemi.Presenter;

public partial class WebControls_Ricerca_Cantiere : System.Web.UI.UserControl
{
    public event IndirizzoSelectedEventHandler OnIndirizzoSelected;
    public event CodiceSelectedEventHandler OnCodiceSelected;
    public event CodiceSelectedEventHandler OnCodiceFiscaleSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        SelezioneIndirizzoLibero1.OnIndirizzoSelected += new IndirizzoSelectedEventHandler(SelezioneIndirizzoLibero1_OnIndirizzoSelected);
    }

    void SelezioneIndirizzoLibero1_OnIndirizzoSelected(Indirizzo indirizzo)
    {
        if (OnIndirizzoSelected != null)
        {
            OnIndirizzoSelected(indirizzo);
        }
    }

    public void Reset()
    {
        SelezioneIndirizzoLibero1.Reset();
    }

    protected void ButtonRicercaCodice_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (OnCodiceSelected != null)
            {
                OnCodiceSelected(Presenter.NormalizzaCampoTesto(TextBoxCodice.Text));
            }
        }
    }

    protected void ButtonRicercaCodiceFiscale_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (OnCodiceFiscaleSelected != null)
            {
                OnCodiceFiscaleSelected(Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text));
            }
        }
    }
}