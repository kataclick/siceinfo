﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificaMappa.ascx.cs" Inherits="WebControls_Ricerca_NotificaMappa" %>


<%@ Register src="../Visualizzazione/Localizzazione.ascx" tagname="Localizzazione" tagprefix="uc1" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<%@ Register src="WebControls/NotificheFilter.ascx" tagname="NotificheFilter" tagprefix="uc2" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1" Width="100%">
<div>
    <table width="100%">
        <tr>
            <td colspan="2">
                <uc2:NotificheFilter ID="NotificheFilter1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricerca"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top" style="text-align:right;">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="150px" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricercaPerAggiornamento" />
            </td>
        </tr>
    </table>
    <asp:Label 
        ID="LabelErrore" 
        runat="server" 
        Text="I cantieri da visualizzare sono troppi, filtrare maggiormente"
        CssClass="messaggiErrore"
        Visible="False">
    </asp:Label>
    <br />
    <asp:Label 
        ID="LabelCantieriTrovati" 
        runat="server" 
        Text="Notifiche trovate" 
        Visible="False">
    </asp:Label>
</div>
<br />
<div>
    <uc1:Localizzazione ID="Localizzazione1" runat="server" />
</div>
</telerik:RadAjaxPanel>