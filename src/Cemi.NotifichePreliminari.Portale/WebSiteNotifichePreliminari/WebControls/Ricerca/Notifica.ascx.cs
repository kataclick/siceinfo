using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Security;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Filters;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_Ricerca_Notifica : System.Web.UI.UserControl
{
    public event NotificaSelectedEventHandler OnNotificaSelected;

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    private const Int32 INDICEBOTTONEMAPPA = 0;
    private const Int32 INDICEBOTTONESELEZIONA = 6;
    private const String IMMAGINEAGGIORNAMENTO = "~/Images/aggiornaNotifica32.png";
    private const String IMMAGINELOCALIZZAZIONE = "~/Images/localizzazioneGrigia24.png";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ModalitaAggiornamento()
    {
        ViewState["Aggiornamento"] = true;
        //CommandField bSeleziona = (CommandField)GridViewNotifiche.Columns[INDICEBOTTONESELEZIONA];
        //bSeleziona.SelectImageUrl = IMMAGINEAGGIORNAMENTO;
    }

    public void ModalitaCartacea()
    {
        ViewState["Cartacea"] = true;
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche(0);
        }
    }

    private void CaricaNotifiche(int numeroPagina)
    {
        NotificaFilter filtro = NotificheFilter1.GetFiltro(true);
        Boolean cartacea = ViewState["Cartacea"] != null;

        NotificaCollection notifiche = biz.RicercaNotificheTelematichePerAggiornamento(filtro, cartacea);
        
        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Notifiche trovate: {0}", notifiche.Count);

        Presenter.CaricaElementiInGridView(
            GridViewNotifiche,
            notifiche,
            numeroPagina);
    }

    protected void GridViewNotifiche_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notifica = (Notifica)e.Row.DataItem;
            GridView gvIndirizzi = (GridView)e.Row.FindControl("GridViewIndirizzi");
            Label lTipoNotifica = (Label)e.Row.FindControl("LabelTipoNotifica");
            Label lNaturaOpera = (Label)e.Row.FindControl("LabelNaturaOpera");
            Label lTipoAppalto = (Label)e.Row.FindControl("LabelTipoAppalto");
            ImageButton ibSeleziona = (ImageButton)e.Row.FindControl("ImageButtonDettagli");

            if (ViewState["Aggiornamento"] != null)
            {
                ibSeleziona.ImageUrl = IMMAGINEAGGIORNAMENTO;
                ibSeleziona.ToolTip = "Aggiorna";
            }

            lNaturaOpera.Text = notifica.NaturaOpera;
            lTipoAppalto.Text = notifica.AppaltoPrivato ? "Privato" : "Pubblico";

            if (notifica.IdNotificaPadre.HasValue)
            {
                lTipoNotifica.Text = "Aggiornamento";
                e.Row.ForeColor = Color.Gray;
            }
            else
            {
                lTipoNotifica.Text = "Notifica preliminare";
            }

            Presenter.CaricaElementiInGridView(
                gvIndirizzi,
                notifica.Indirizzi,
                0);
        }
    }

    protected void GridViewNotifiche_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaNotifiche(e.NewPageIndex);
    }

    protected void GridViewNotifiche_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idNotifica = (Int32)GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotifica"];
        Int32 idNotificaRiferimento = (Int32)GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotificaRiferimento"];

        if (OnNotificaSelected != null)
        {
            OnNotificaSelected(idNotifica, idNotificaRiferimento);
        }
    }

    protected void GridViewNotifiche_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Int32 idNotifica = (Int32)GridViewNotifiche.DataKeys[e.RowIndex].Values["IdNotifica"];

        Context.Items["IdNotifica"] = idNotifica;
        Server.Transfer("~/Ricerca/RicevutaNotifica.aspx");
    }

    protected void GridViewNotifiche_RowEditing(object sender, GridViewEditEventArgs e)
    {
        
    }

    protected void GridViewIndirizzi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Indirizzo indirizzo = (Indirizzo)e.Row.DataItem;
            ImageButton ibMappa = (ImageButton)e.Row.FindControl("ImageButtonMappa");

            if (!indirizzo.HaCoordinate())
            {
                ibMappa.ImageUrl = IMMAGINELOCALIZZAZIONE;
                e.Row.Cells[INDICEBOTTONEMAPPA].Enabled = false;
            }
            else
            {
                ibMappa.Attributes.Add("onclick", String.Format("openRadWindow({0}); return false;", indirizzo.IdIndirizzo));
            }
        }
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvIndirizzi = (GridView)sender;
        Int32 idIndirizzo = (Int32)gvIndirizzi.DataKeys[e.NewSelectedIndex].Values["IdIndirizzo"];

        Context.Items["IdIndirizzo"] = idIndirizzo;
        Server.Transfer("~/Ricerca/LocalizzaIndirizzo.aspx");
    }
}
