﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificheFilter.ascx.cs" Inherits="WebControls_Ricerca_WebControls_NotificheFilter" %>

<table width="100%">
    <tr>
        <td style="width:25%">
            Dal (gg/mm/aaaa)
            <asp:CompareValidator
                ID="CompareValidatorDataInserimentoDal"
                runat="server"
                ControlToValidate="TextBoxDataInserimentoDal"
                Type="Date"
                Operator="DataTypeCheck"
                ErrorMessage="Formato data Dal errato"
                ValidationGroup="ricerca">
                *
            </asp:CompareValidator>
            <asp:RangeValidator
                ID="RangeValidatorDataInserimentoDal"
                runat="server"
                ControlToValidate="TextBoxDataInserimentoDal"
                Type="Date"
                ErrorMessage="La data Dal deve essere compresa tra l'1/1/2000 e il 6/6/2079"
                ValidationGroup="ricerca" MaximumValue="6/6/2079" MinimumValue="1/1/2000">
                *
            </asp:RangeValidator>
        </td>
        <td style="width:25%">
            Al (gg/mm/aaaa)
            <asp:CompareValidator
                ID="CompareValidatorDataInserimentoAl"
                runat="server"
                ControlToValidate="TextBoxDataInserimentoAl"
                Type="Date"
                Operator="DataTypeCheck"
                ErrorMessage="Formato data Al errato"
                ValidationGroup="ricerca">
                *
            </asp:CompareValidator>
            <asp:RangeValidator
                ID="RangeValidatorDataInserimentoAl"
                runat="server"
                ControlToValidate="TextBoxDataInserimentoAl"
                Type="Date"
                ErrorMessage="La data Al deve essere compresa tra l'1/1/2000 e il 6/6/2079"
                ValidationGroup="ricerca" MaximumValue="6/6/2079" MinimumValue="1/1/2000">
                *
            </asp:RangeValidator>
            <asp:CompareValidator
                ID="CompareValidatorDataInserimentoDalAl"
                runat="server"
                ControlToValidate="TextBoxDataInserimentoDal"
                Type="Date"
                Operator="LessThanEqual"
                ControlToCompare="TextBoxDataInserimentoAl"
                ErrorMessage="Periodo di ricerca non valido"
                ValidationGroup="ricerca">
                *
            </asp:CompareValidator>
        </td>
        <td style="width:25%">
            Tipo Appalto
        </td>
        <td style="width:25%">
            Tipologia Lavoro
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox 
                ID="TextBoxDataInserimentoDal" 
                runat="server"
                MaxLength="10"
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxDataInserimentoAl" 
                runat="server"
                MaxLength="10"
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListTipoAppalto"
                runat="server"
                Width="100%">
                <asp:ListItem Selected="True" Value="TUTTI">Tutti</asp:ListItem>
                <asp:ListItem Value="PRIVATI">Privati</asp:ListItem>
                <asp:ListItem Value="PUBBLICI">Pubblici</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListTipologiaLavoro"
                AppendDataBoundItems="true"
                runat="server"
                Width="100%">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Protocollo
            <asp:RangeValidator 
                ID="RangeValidatorIdNotifica"
                runat="server"
                ControlToValidate="TextBoxProtocollo"
                Type="Integer"
                MinimumValue="1"
                MaximumValue="2000000000"
                ErrorMessage="Formato n° protocollo errato"
                ValidationGroup="ricerca">
                *
            </asp:RangeValidator>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidatorCodice"
                runat="server"
                ControlToValidate="TextBoxProtocollo"
                ErrorMessage="Protocollo non valido"
                ValidationGroup="ricerca"
                ValidationExpression="^[a-z]|[A-Z]{2}/\d{4}/\d{5}$"
                Enabled="false">
                *
            </asp:RegularExpressionValidator>
        </td>
        <td>
            CIG/DIA/Permesso costr.
        </td>
        <td colspan="2">
            Natura opera
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox 
                ID="TextBoxProtocollo" 
                runat="server" 
                MaxLength="10"
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroAppalto" 
                runat="server" 
                MaxLength="50"
                Width="100%">
            </asp:TextBox>
        </td>
        <td colspan="2">
            <asp:TextBox 
                ID="TextBoxNaturaOpera" 
                runat="server"
                MaxLength="500"
                Width="100%">
            </asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo
        </td>
        <td>
            Comune
        </td>
        <td>
            Provincia
        </td>
        <td>
             CAP
            <asp:RegularExpressionValidator 
                ID="RegularExpressionValidatorCap"
                runat="server"
                ControlToValidate="TextBoxCap"
                ErrorMessage="Formato del CAP errato"
                ValidationGroup="ricerca"
                ValidationExpression="^\d{5}$">
                *
            </asp:RegularExpressionValidator>   
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox 
                ID="TextBoxIndirizzo" 
                runat="server"
                MaxLength="255" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxComune" 
                runat="server" 
                MaxLength="50"
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListProvincia"
                runat="server"
                AppendDataBoundItems="true"
                Width="100%">
            </asp:DropDownList>
        </td>
        <td>
            <asp:TextBox
                ID="TextBoxCap"
                runat="server"
                MaxLength="5"
                Width="100%">
            </asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Rag.Soc.Impresa
        </td>
        <td>
            P.IVA/Cod.Fisc.Impresa
            <asp:RegularExpressionValidator 
                ID="RegularExpressionCF" 
                runat="server" 
                ControlToValidate="TextBoxIvaFiscImpresa"
                ErrorMessage="P.IVA/Cod.Fisc. non valido" 
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                ValidationGroup="ricerca">
                *
            </asp:RegularExpressionValidator>
        </td>
        <td>
            Rag.Soc.Committente
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox 
                ID="TextBoxRagSocImpresa" 
                runat="server"
                MaxLength="255" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxIvaFiscImpresa" 
                runat="server"
                MaxLength="16" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxRagSocCommittente" 
                runat="server"
                MaxLength="255" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Ammontare (>)
            <asp:RegularExpressionValidator 
                ID="RegularExpressionValidatorAmmontare"
                runat="server"
                ControlToValidate="TextBoxAmmontare"
                ErrorMessage="Formato dell'ammontare errato"
                ValidationGroup="ricerca"
                ValidationExpression="^[.][0-9]+$|[0-9]*[,]*[0-9]{0,2}$">
                *
            </asp:RegularExpressionValidator>
        </td>
        <td>
            Inizio Lavori (>) (gg/mm/aaaa)
            <asp:CompareValidator
                ID="CompareValidatorInizioLavori"
                runat="server"
                ControlToValidate="TextBoxInizioLavori"
                Type="Date"
                Operator="DataTypeCheck"
                ErrorMessage="Formato data Inizio Lavori errato"
                ValidationGroup="ricerca">
                *
            </asp:CompareValidator>
        </td>
        <td>
            Fine lavori (<) (gg/mm/aaaa)
            <asp:CompareValidator
                ID="CompareValidatorFineLavori"
                runat="server"
                ControlToValidate="TextBoxFineLavori"
                Type="Date"
                Operator="DataTypeCheck"
                ErrorMessage="Formato data Fine Lavori errato"
                ValidationGroup="ricerca">
                *
            </asp:CompareValidator>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox 
                ID="TextBoxAmmontare" 
                runat="server"
                MaxLength="10" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxInizioLavori" 
                runat="server"
                MaxLength="10" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxFineLavori" 
                runat="server"
                MaxLength="10" 
                Width="100%">
            </asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
</table>