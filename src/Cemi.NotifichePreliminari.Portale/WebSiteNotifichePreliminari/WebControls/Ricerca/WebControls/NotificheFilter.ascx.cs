﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Filters;
using System.Web.Security;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_Ricerca_WebControls_NotificheFilter : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaProvince();
            CaricaTipologieLavoro();
        }
    }

    public void ModalitaDenuncia()
    {
        RangeValidatorIdNotifica.Enabled = false;
        RegularExpressionValidatorCodice.Enabled = true;
        TextBoxProtocollo.MaxLength = 13;
        ViewState["DENUNCIA"] = true;
    }

    public void CaricaFiltroRicerca(NotificaFilter filtro)
    {
        if (filtro != null)
        {
            CaricaProvince();

            if (filtro.DataDal.HasValue)
            {
                TextBoxDataInserimentoDal.Text = filtro.DataDal.Value.ToString("dd/MM/yyyy");
            }
            if (filtro.DataAl.HasValue)
            {
                TextBoxDataInserimentoAl.Text = filtro.DataAl.Value.ToString("dd/MM/yyyy");
            }
            if (filtro.AppaltoPrivato.HasValue)
            {
                if (filtro.AppaltoPrivato.Value)
                {
                    DropDownListTipoAppalto.SelectedValue = "PRIVATI";
                }
                else
                {
                    DropDownListTipoAppalto.SelectedValue = "PUBBLICI";
                }
            }
            else
            {
                DropDownListTipoAppalto.SelectedValue = "TUTTI";
            }
            if (filtro.IdTipologiaLavoro.HasValue)
            {
                DropDownListTipologiaLavoro.SelectedValue = filtro.IdTipologiaLavoro.ToString();
            }
            if (ViewState["DENUNCIA"] == null)
            {
                TextBoxProtocollo.Text = filtro.IdNotifica.ToString();
            }
            else
            {
                TextBoxProtocollo.Text = filtro.ProtocolloDenuncia;
            }
            TextBoxNumeroAppalto.Text = filtro.NumeroAppalto;
            TextBoxNaturaOpera.Text = filtro.NaturaOpera;
            TextBoxIndirizzo.Text = filtro.Indirizzo;
            TextBoxComune.Text = filtro.IndirizzoComune;
            //if (!String.IsNullOrEmpty(filtro.IndirizzoProvincia))
            //{
                DropDownListProvincia.SelectedValue = filtro.IndirizzoProvincia;
            //}
            TextBoxCap.Text = filtro.Cap;
            TextBoxRagSocImpresa.Text = filtro.Impresa;
            TextBoxIvaFiscImpresa.Text = filtro.FiscIva;
            TextBoxRagSocCommittente.Text = filtro.Committente;
            if (filtro.Ammontare.HasValue)
            {
                TextBoxAmmontare.Text = filtro.Ammontare.Value.ToString("G");
            }
            if (filtro.DataInizio.HasValue)
            {
                TextBoxInizioLavori.Text = filtro.DataInizio.Value.ToString("dd/MM/yyyy");
            }
            if (filtro.DataFine.HasValue)
            {
                TextBoxFineLavori.Text = filtro.DataFine.Value.ToString("dd/MM/yyyy");
            }

            ViewState["FiltroPrecaricato"] = true;
        }
    }

    private void CaricaTipologieLavoro()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipologiaLavoro,
            biz.GetTipologieLavoro(),
            "Descrizione",
            "Id");
    }

    private void CaricaProvince()
    {
        if (DropDownListProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(
                DropDownListProvincia);

            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                Guid guidUtente = (Guid)user.ProviderUserKey;

                // Filtro su utente
                if (Roles.IsUserInRole("Ricerca") || Roles.IsUserInRole("Amministratore"))
                {
                    UtenteNotificheTelematiche utente = biz.GetUtenteTelematiche(guidUtente);
                    if (!String.IsNullOrEmpty(utente.FiltroProvincia))
                    {
                        DropDownListProvincia.SelectedValue = utente.FiltroProvincia;
                        DropDownListProvincia.Enabled = false;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(utente.ProvinciaSuggerita)
                            && String.IsNullOrEmpty(DropDownListProvincia.SelectedValue)
                            && ViewState["FiltroPrecaricato"] == null)
                        {
                            DropDownListProvincia.SelectedValue = utente.ProvinciaSuggerita;
                        }
                    }
                }
            }
        }
    }

    public NotificaFilter GetFiltro(Boolean forzaUtente)
    {
        NotificaFilter filtro = new NotificaFilter();

        if (!String.IsNullOrEmpty(TextBoxDataInserimentoDal.Text))
        {
            filtro.DataDal = DateTime.Parse(TextBoxDataInserimentoDal.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxDataInserimentoAl.Text))
        {
            filtro.DataAl = DateTime.Parse(TextBoxDataInserimentoAl.Text);
        }

        if (DropDownListTipoAppalto.SelectedValue != "TUTTI")
        {
            filtro.AppaltoPrivato = DropDownListTipoAppalto.SelectedValue == "PRIVATI";
        }

        if (!String.IsNullOrEmpty(DropDownListTipologiaLavoro.SelectedValue))
        {
            filtro.IdTipologiaLavoro = Int32.Parse(DropDownListTipologiaLavoro.SelectedValue);
        }

        if (!String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxProtocollo.Text)))
        {
            if (ViewState["DENUNCIA"] == null)
            {
                filtro.IdNotifica = Int32.Parse(TextBoxProtocollo.Text);
            }
            else
            {
                filtro.ProtocolloDenuncia = Presenter.NormalizzaCampoTesto(TextBoxProtocollo.Text);
            }
        }

        if (!String.IsNullOrEmpty(TextBoxNumeroAppalto.Text))
        {
            filtro.NumeroAppalto = Presenter.NormalizzaCampoTesto(TextBoxNumeroAppalto.Text.Trim());
        }

        if (!String.IsNullOrEmpty(TextBoxNaturaOpera.Text))
        {
            filtro.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxIndirizzo.Text))
        {
            filtro.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxComune.Text))
        {
            filtro.IndirizzoComune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        }

        if (!String.IsNullOrEmpty(DropDownListProvincia.SelectedValue))
        {
            filtro.IndirizzoProvincia = DropDownListProvincia.SelectedValue;
        }

        if (!String.IsNullOrEmpty(TextBoxCap.Text))
        {
            filtro.Cap = Presenter.NormalizzaCampoTesto(TextBoxCap.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxRagSocImpresa.Text))
        {
            filtro.Impresa = Presenter.NormalizzaCampoTesto(TextBoxRagSocImpresa.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxIvaFiscImpresa.Text))
        {
            filtro.FiscIva = Presenter.NormalizzaCampoTesto(TextBoxIvaFiscImpresa.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxRagSocCommittente.Text))
        {
            filtro.Committente = Presenter.NormalizzaCampoTesto(TextBoxRagSocCommittente.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxAmmontare.Text))
        {
            filtro.Ammontare = Decimal.Parse(TextBoxAmmontare.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxInizioLavori.Text))
        {
            filtro.DataInizio = DateTime.Parse(TextBoxInizioLavori.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxFineLavori.Text))
        {
            filtro.DataFine = DateTime.Parse(TextBoxFineLavori.Text);
        }

        // Filtro su area Milano
        filtro.IdArea = 1;

        Guid guidUtente = (Guid)Membership.GetUser().ProviderUserKey;

        // Filtro su utente
        if (forzaUtente || (!Roles.IsUserInRole("Ricerca") && !Roles.IsUserInRole("Amministratore")))
        {
            filtro.IdUtenteTelematiche = guidUtente;
        }

        return filtro;
    }
}