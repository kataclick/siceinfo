using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Cemi.NotifichePreliminari.Type.Filters;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;
using System.Collections.Specialized;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Type.Collections;

public partial class WebControls_Ricerca_Utente : System.Web.UI.UserControl
{
    private String[] ruoli = null;
    private StringCollection province = null;
    private UtenteNotificheTelematiche userTemp = null;
    private readonly Common commonBiz = new Common();
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaCasseEdili();
        }
    }

    private void CaricaCasseEdili()
    {
        if (DropDownListCassaEdile.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCassaEdile,
                biz.GetCasseEdili(true),
                "Descrizione",
                "IdCassaEdile");
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        CaricaUtenti(0);
    }

    private void CaricaUtenti(Int32 pagina)
    {
        UtenteNotificheTelematicheFilter filtro = CreaFiltro();
        UtenteNotificheTelematicheCollection utenti = biz.GetUtentiTelematiche(filtro);
        
        Presenter.CaricaElementiInGridView(
            GridViewUtenti,
            utenti,
            pagina);
    }

    private UtenteNotificheTelematicheFilter CreaFiltro()
    {
        UtenteNotificheTelematicheFilter filtro = new UtenteNotificheTelematicheFilter();

        filtro.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        filtro.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        filtro.UserName = Presenter.NormalizzaCampoTesto(TextBoxNomeUtente.Text);
        if (!String.IsNullOrEmpty(DropDownListPosizione.SelectedValue))
        {
            switch(DropDownListPosizione.SelectedValue)
            {
                case "ABILITATI":
                    filtro.Abilitati = true;
                    break;
                case "NONABILITATI":
                    filtro.Abilitati = false;
                    break;
            }
        }
        filtro.IdCassaEdile = DropDownListCassaEdile.SelectedValue;

        return filtro;
    }

    protected void GridViewUtenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        String userName = GridViewUtenti.DataKeys[e.NewSelectedIndex].Values["UserName"] as String;

        if (!String.IsNullOrEmpty(userName))
        {
            MembershipUser user = Membership.GetUser(userName);
            user.IsApproved = !user.IsApproved;
            
            Membership.UpdateUser(user);

            try
            {
                MailManager mailManager = new MailManager();
                mailManager.InviaMailAttivazioneDisattivazione(user.UserName, user.Email, user.IsApproved);
            }
            catch { }

            CaricaUtenti(GridViewUtenti.PageIndex);
        }
    }

    protected void GridViewUtenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            UtenteNotificheTelematiche user = (UtenteNotificheTelematiche)e.Row.DataItem;
            GridView gvRuoli = (GridView)e.Row.FindControl("GridViewRuoli");
            CheckBox cbAbilitato = (CheckBox)e.Row.FindControl("CheckBoxAbilitato");
            DropDownList ddlProvinciaSuggerita = (DropDownList)e.Row.FindControl("DropDownListFiltroProvinciaSuggerita");
            DropDownList ddlProvinciaObbligata = (DropDownList)e.Row.FindControl("DropDownListFiltroProvinciaObbligata");
            Button bSalvaFiltri = (Button)e.Row.FindControl("ButtonSalvaFiltri");
            Button bAggiornaRuoli = (Button)e.Row.FindControl("ButtonAggiornaRuoli");
            CustomValidator cvRuoli = (CustomValidator)e.Row.FindControl("CustomValidatorAggiornaRuoli");

            bAggiornaRuoli.ValidationGroup = e.Row.RowIndex.ToString();
            cvRuoli.ValidationGroup = e.Row.RowIndex.ToString();

            // Ad ogni utente faccio una chiamata al DB. E' una bruttura ma per
            // sistemarlo bisogna implementare il provider Membership e gestire
            // diversamente gli utenti

            //UtenteNotificheTelematiche utente = biz.GetUtenteTelematiche((Guid)user.ProviderUserKey);

            bSalvaFiltri.CommandName = "SalvaFiltri";
            bSalvaFiltri.CommandArgument = e.Row.RowIndex.ToString();

            cbAbilitato.Checked = user.IsApproved;
            if (!user.IsApproved)
            {
                e.Row.ForeColor = Color.Gray;
            }

            if (Roles.IsUserInRole(user.UserName, "Ricerca") || Roles.IsUserInRole(user.UserName, "Amministratore"))
            {
                ddlProvinciaSuggerita.Enabled = true;
                ddlProvinciaObbligata.Enabled = true;
            }
            else
            {
                ddlProvinciaSuggerita.Enabled = false;
                ddlProvinciaObbligata.Enabled = false;
            }

            userTemp = user;

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                ddlProvinciaSuggerita,
                province,
                String.Empty,
                String.Empty);

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                ddlProvinciaObbligata,
                province,
                String.Empty,
                String.Empty);

            if (user != null)
            {
                ddlProvinciaObbligata.SelectedValue = user.FiltroProvincia;
                ddlProvinciaSuggerita.SelectedValue = user.ProvinciaSuggerita;
            }

            Presenter.CaricaElementiInGridView(
                gvRuoli,
                ruoli,
                0);
        }
    }

    protected void GridViewUtenti_DataBinding(object sender, EventArgs e)
    {
        ruoli = Roles.GetAllRoles();
        province = commonBiz.GetProvinceSiceNew();
    }

    protected void GridViewRuoli_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbRuolo = (CheckBox)e.Row.FindControl("CheckBoxRuolo");
            
            String ruolo = (String)e.Row.DataItem;
            cbRuolo.Text = ruolo;

            if (Roles.IsUserInRole(userTemp.UserName, ruolo))
            {
                cbRuolo.Checked = true;
            }
        }
    }

    protected void GridViewUtenti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        String userName = GridViewUtenti.DataKeys[e.RowIndex].Values["UserName"] as String;
        Guid userID = (Guid)GridViewUtenti.DataKeys[e.RowIndex].Values["IdUtente"];

        Page.Validate(e.RowIndex.ToString());

        if (Page.IsValid)
        {
            if (!String.IsNullOrEmpty(userName))
            {
                GridViewRow row = GridViewUtenti.Rows[e.RowIndex];
                GridView gvRuoli = (GridView)row.FindControl("GridViewRuoli");

                foreach (GridViewRow rowRuolo in gvRuoli.Rows)
                {
                    CheckBox cbRuolo = (CheckBox)rowRuolo.FindControl("CheckBoxRuolo");
                    if (cbRuolo.Checked)
                    {
                        if (!Roles.IsUserInRole(userName, cbRuolo.Text))
                        {
                            Roles.AddUserToRole(userName, cbRuolo.Text);
                        }
                    }
                    else
                    {
                        if (Roles.IsUserInRole(userName, cbRuolo.Text))
                        {
                            Roles.RemoveUserFromRole(userName, cbRuolo.Text);
                        }
                    }
                }

                if (!Roles.IsUserInRole(userName, "Ricerca") && !Roles.IsUserInRole(userName, "Amministratore"))
                {
                    SalvaFiltri(userID, String.Empty, String.Empty);
                }

                CaricaUtenti(GridViewUtenti.PageIndex);
            }
        }
    }

    protected void GridViewUtenti_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Guid userID = (Guid)GridViewUtenti.DataKeys[e.NewEditIndex].Values["IdUtente"];
        Context.Items["userID"] = userID;

        Server.Transfer("~/Amministrazione/DettagliUtente.aspx");
    }

    protected void GridViewUtenti_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "SalvaFiltri":
                Int32 indice = Int32.Parse(e.CommandArgument.ToString());
                Guid userID = (Guid)GridViewUtenti.DataKeys[indice].Values["IdUtente"];
                DropDownList ddlProvinciaObbligata = (DropDownList)GridViewUtenti.Rows[indice].FindControl("DropDownListFiltroProvinciaObbligata");
                DropDownList ddlProvinciaSuggerita = (DropDownList)GridViewUtenti.Rows[indice].FindControl("DropDownListFiltroProvinciaSuggerita");

                SalvaFiltri(userID, ddlProvinciaObbligata.SelectedValue, ddlProvinciaSuggerita.SelectedValue);
                CaricaUtenti(GridViewUtenti.PageIndex);
                break;
        }
    }

    private void SalvaFiltri(Guid guid, String provincia, String provinciaSuggerita)
    {
        biz.UpdateFiltri(guid, provincia, provinciaSuggerita);
    }

    protected void GridViewUtenti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaUtenti(e.NewPageIndex);
    }

    protected void CustomValidatorAggiornaRuoli_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // Recupero il GridView dei ruoli da controllare
        GridView gvRuoli = (GridView)((CustomValidator)source).NamingContainer.FindControl("GridViewRuoli");
        Boolean ricerca = false;
        Boolean inserimento = false;

        foreach (GridViewRow rowRuolo in gvRuoli.Rows)
        {
            CheckBox cbRuolo = (CheckBox)rowRuolo.FindControl("CheckBoxRuolo");
            if (cbRuolo.Text == "Inserimento" && cbRuolo.Checked)
            {
                inserimento = true;
            }
            if (cbRuolo.Text == "Ricerca" && cbRuolo.Checked)
            {
                ricerca = true;
            }
        }

        if (inserimento && ricerca)
        {
            args.IsValid = false;
        }
    }
}
