using System;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Filters;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.Presenter;

public partial class WebControls_Ricerca_NotificaMappa : UserControl
{
    private const Int32 MAXPUSHPIN = 300;

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MembershipUser utente = Membership.GetUser();
            NotificaFilter filtro = biz.GetFiltroRicercaMappa((Guid)utente.ProviderUserKey);

            if (filtro != null && !filtro.Vuoto())
            {
                NotificheFilter1.CaricaFiltroRicerca(filtro);
                CaricaCantieri();
            }
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaCantieri();
        }
    }

    private void CaricaCantieri()
    {
        NotificaFilter filtro = NotificheFilter1.GetFiltro(false);
        
        MembershipUser user = Membership.GetUser();
        biz.InsertFiltroRicercaMappa((Guid)user.ProviderUserKey, filtro);

        CantiereNotificaCollection cantieri = null;
        try
        {
            cantieri = biz.RicercaCantieriPerCommittente(filtro);
        }
        catch
        {
            biz.DeleteFiltroRicerca((Guid)user.ProviderUserKey);
            throw;
        }

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", cantieri.Count);

        if (cantieri.Count <= MAXPUSHPIN)
        {
            CaricaMappa(cantieri);
            LabelErrore.Visible = false;
        }
        else
        {
            LabelErrore.Visible = true;
        }
    }

    private void CaricaMappa(CantiereNotificaCollection cantieri)
    {
        StringBuilder stringBuilder;
        stringBuilder = Localizzazione1.CaricaCantieri(cantieri);
        RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
    }
}