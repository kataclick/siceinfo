﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Cantiere.ascx.cs" Inherits="WebControls_Ricerca_Cantiere" %>
<%@ Register src="../SelezioneIndirizzoLibero.ascx" tagname="SelezioneIndirizzoLibero" tagprefix="uc1" %>

<table width="100%">
    <tr>
        <td>
            <b>
                Per Codice
            </b>
            (es. LU/2012/00001)
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel
                ID="PanelRicercaCodice"
                runat="server"
                DefaultButton="ButtonRicercaCodice">
            <asp:TextBox
                ID="TextBoxCodice"
                runat="server"
                MaxLength="13">
            </asp:TextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorCodice"
                runat="server"
                ControlToValidate="TextBoxCodice"
                ErrorMessage="Inserire il codice"
                ValidationGroup="ricercaCodice">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidatorCodice"
                runat="server"
                ControlToValidate="TextBoxCodice"
                ErrorMessage="Codice non valido"
                ValidationGroup="ricercaCodice"
                ValidationExpression="^[a-z]|[A-Z]{2}/\d{4}/\d{5}$">
                *
            </asp:RegularExpressionValidator>
            <br />
            <asp:Button
                ID="ButtonRicercaCodice"
                runat="server"
                Text="Cerca"
                Width="160px"
                ValidationGroup="ricercaCodice" onclick="ButtonRicercaCodice_Click" />
            <asp:ValidationSummary
                ID="ValidationSummaryCodice"
                runat="server"
                ValidationGroup="ricercaCodice" />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            <b>
                Per Codice Fiscale del Committente
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel
                ID="PanelRicercaCodiceFiscale"
                runat="server"
                DefaultButton="ButtonRicercaCodiceFiscale">
            <asp:TextBox
                ID="TextBoxCodiceFiscale"
                runat="server"
                MaxLength="16">
            </asp:TextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorCodiceFiscale"
                runat="server"
                ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Inserire il codice fiscale"
                ValidationGroup="ricercaCodiceFiscale">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidatorCodiceFiscale"
                runat="server"
                ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non valido"
                ValidationGroup="ricercaCodiceFiscale"
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$">
                *
            </asp:RegularExpressionValidator>
            <br />
            <asp:Button
                ID="ButtonRicercaCodiceFiscale"
                runat="server"
                Text="Cerca"
                Width="160px"
                ValidationGroup="ricercaCodiceFiscale" onclick="ButtonRicercaCodiceFiscale_Click" />
            <asp:ValidationSummary
                ID="ValidationSummaryCodiceFiscale"
                runat="server"
                ValidationGroup="ricercaCodiceFiscale" />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            <b>
                Per Indirizzo
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <uc1:SelezioneIndirizzoLibero ID="SelezioneIndirizzoLibero1" runat="server" />
        </td>
    </tr>
</table>