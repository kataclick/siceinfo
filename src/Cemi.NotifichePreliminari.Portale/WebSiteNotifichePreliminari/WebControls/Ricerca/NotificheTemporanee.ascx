﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificheTemporanee.ascx.cs" Inherits="WebControls_Ricerca_NotificheTemporanee" %>
<div>
    <asp:GridView
        ID="GridViewNotificheTemporanee"
        runat="server"
        Width="100%" AutoGenerateColumns="False" 
        onrowdatabound="GridViewNotificheTemporanee_RowDataBound" 
        AllowPaging="True" DataKeyNames="IdNotificaTemporanea" 
        onpageindexchanging="GridViewNotificheTemporanee_PageIndexChanging" 
        onrowdeleting="GridViewNotificheTemporanee_RowDeleting" 
        onselectedindexchanging="GridViewNotificheTemporanee_SelectedIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="Natura opera">
                <ItemTemplate>
                    <b>
                        <asp:Label ID="LabelNaturaOpera" runat="server"></asp:Label>
                    </b>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Committente" HeaderText="Committente">
            <ItemStyle Width="250px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Indirizzi">
                <ItemTemplate>
                    <asp:BulletedList ID="BulletedListIndirizzi" runat="server">
                    </asp:BulletedList>
                </ItemTemplate>
                <ItemStyle Width="250px" />
            </asp:TemplateField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectImageUrl="~/Images/modifica.png" 
                ShowSelectButton="True">
            <ItemStyle Width="10px" />
            </asp:CommandField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" 
                DeleteImageUrl="~/Images/pallinoElimina.png" ShowDeleteButton="True">
            <ItemStyle Width="10px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna notifica non confermata presente.
        </EmptyDataTemplate>
    </asp:GridView>
</div>