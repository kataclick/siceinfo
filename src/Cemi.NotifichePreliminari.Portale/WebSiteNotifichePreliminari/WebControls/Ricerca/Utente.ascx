﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Utente.ascx.cs" Inherits="WebControls_Ricerca_Utente" %>

<style type="text/css">
    .style1
    {
        width: 110px;
    }
</style>

<div>
    <table width="100%">
        <tr>
            <td style="width:25%">
                Cognome
            </td>
            <td style="width:25%">
                Nome
            </td>
            <td style="width:25%">
                Nome Utente
            </td>
            <td style="width:25%">
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox 
                    ID="TextBoxCognome" 
                    runat="server" 
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxNome" 
                    runat="server"
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxNomeUtente" 
                    runat="server"
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Posizione
            </td>
            <td>
                Richiesta alla
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DropDownList
                    ID="DropDownListPosizione"
                    runat="server"
                    Width="100%">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value="ABILITATI">Abilitati all&#39;accesso</asp:ListItem>
                    <asp:ListItem Value="NONABILITATI">Non abilitati all&#39;accesso</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList
                    ID="DropDownListCassaEdile"
                    AppendDataBoundItems="true"
                    runat="server"
                    Width="300px">
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricercaPerAggiornamento"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="100%" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricercaPerAggiornamento" />
            </td>
        </tr>
    </table>
</div>
<br />
<div>
    <asp:GridView 
        ID="GridViewUtenti" 
        runat="server" 
        Width="100%" AutoGenerateColumns="False" DataKeyNames="UserName,IdUtente" 
        onselectedindexchanging="GridViewUtenti_SelectedIndexChanging" 
        ondatabinding="GridViewUtenti_DataBinding" 
        onrowdatabound="GridViewUtenti_RowDataBound" 
        onrowdeleting="GridViewUtenti_RowDeleting" 
        onrowediting="GridViewUtenti_RowEditing" 
        onrowcommand="GridViewUtenti_RowCommand" AllowPaging="True" 
        onpageindexchanging="GridViewUtenti_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="Utente">
                <ItemTemplate>
                    <table>
                        <tr>
                            <td class="style1">
                                Nome Utente:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Cognome:
                            </td>
                            <td>
                                <asp:Label ID="LabelCognome" runat="server" Text='<%# Bind("Cognome") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Nome:
                            </td>
                            <td>
                                <asp:Label ID="LabelNome" runat="server" Text='<%# Bind("Nome") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Email:
                            </td>
                            <td>
                                <asp:Label ID="LabelEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Richiesta alla:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelCassaEdile" runat="server" Text='<%# Bind("DescrizioneCassaEdile") %>'></asp:Label>
                                </b>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="90px" />
            </asp:TemplateField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Dettagli" ShowEditButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

            <ItemStyle Width="10px" />
            </asp:CommandField>
            <asp:TemplateField HeaderText="Ruoli">
                <ItemTemplate>
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <asp:GridView ID="GridViewRuoli" runat="server" AutoGenerateColumns="False" 
                                    onrowdatabound="GridViewRuoli_RowDataBound" ShowHeader="False" Width="100%" GridLines="None" BorderStyle="None">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxRuolo" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button
                                    ID="ButtonAggiornaRuoli"
                                    runat="server"
                                    Text="Agg. ruoli"
                                    CommandName="Delete"
                                    Width="150px" />
                                <br />
                                <asp:CustomValidator
                                    ID="CustomValidatorAggiornaRuoli"
                                    runat="server"
                                    CssClass="messaggiErrore"
                                    
                                    ErrorMessage="Non è possibile selezionare contemporaneamente i ruoli di Inserimento e Ricerca" 
                                    onservervalidate="CustomValidatorAggiornaRuoli_ServerValidate">
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <b>Filtri ricerca</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provincia obbligata:
                                        </td>
                                        <td>
                                            <asp:DropDownList
                                                ID="DropDownListFiltroProvinciaObbligata"
                                                runat="server"
                                                Width="100px" 
                                                AppendDataBoundItems="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provincia suggerita:
                                        </td>
                                        <td>
                                            <asp:DropDownList
                                                ID="DropDownListFiltroProvinciaSuggerita"
                                                runat="server"
                                                Width="100px" 
                                                AppendDataBoundItems="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:Button
                                                ID="ButtonSalvaFiltri"
                                                runat="server"
                                                Width="80px"
                                                Text="Salva Filtri" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="230px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Abilitazione">
                <ItemTemplate>
                    <asp:CheckBox
                        ID="CheckBoxAbilitato"
                        runat="server"
                        Enabled="false" />
                    <asp:Button
                        ID="ButtonAbilitaDisabilita"
                        runat="server"
                        CommandName="Select"
                        Text="Abil./Disabil."
                        Width="80px" />
                </ItemTemplate>
                <ItemStyle Width="120px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessun utente corrisponde ai parametri di ricerca.
        </EmptyDataTemplate>
    </asp:GridView>
</div>