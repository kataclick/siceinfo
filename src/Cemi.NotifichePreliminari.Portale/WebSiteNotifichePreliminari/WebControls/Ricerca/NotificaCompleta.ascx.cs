using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.Security;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Filters;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_Ricerca_NotificaCompleta : System.Web.UI.UserControl
{
    private const String IMMAGINEAGGIORNAMENTO = "~/Images/aggiornaNotifica32.png";
    private const String IMMAGINELOCALIZZAZIONE = "~/Images/localizzazioneGrigia24.png";
    private const Int32 INDICEBOTTONEMAPPA = 0;

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Int32 pagina;
            MembershipUser utente = Membership.GetUser();
            NotificaFilter filtro = biz.GetFiltroRicercaNotifiche((Guid)utente.ProviderUserKey, out pagina);
            if (filtro != null && (!filtro.Vuoto() || Request.QueryString["back"] != null))
            {
                NotificheFilter1.CaricaFiltroRicerca(filtro);
                CaricaNotifiche(pagina);
            }
        }
    }

    public void CaricaNotifiche(Int32 pagina)
    {
        NotificaFilter filtro = NotificheFilter1.GetFiltro(false);

        MembershipUser user = Membership.GetUser();
        biz.InsertFiltroRicercaNotifiche((Guid)user.ProviderUserKey, filtro, pagina);

        NotificaCollection notifiche = null;
        try
        {
            notifiche = biz.RicercaNotifiche(filtro);
        }
        catch
        {
            biz.DeleteFiltroRicerca((Guid) user.ProviderUserKey);
            throw;
        }

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Notifiche trovate: {0}", notifiche.Count.ToString());

        Presenter.CaricaElementiInGridView(
            GridViewNotifiche,
            notifiche,
            pagina);
    }

    protected void GridViewNotifiche_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notificaRif = (Notifica)e.Row.DataItem;

            Label lNotificaPrel = (Label)e.Row.FindControl("LabelProtocolloPreliminare");
            Label lNotificaUlti = (Label)e.Row.FindControl("LabelProtocolloUltimo");
            Label lData = (Label)e.Row.FindControl("LabelData");
            Label lNaturaOpera = (Label)e.Row.FindControl("LabelNaturaOpera");
            Label lTipologiaAppalto = (Label)e.Row.FindControl("LabelTipologiaAppalto");
            Label lTipologiaLavoro = (Label)e.Row.FindControl("LabelTipologiaLavoro");
            Label lNumeroAppalto = (Label)e.Row.FindControl("LabelNumeroAppalto");
            Label lCommittente = (Label)e.Row.FindControl("LabelCommittente");
            Label lDataUltimoAgg = (Label)e.Row.FindControl("LabelDataUltimoAggiornamento");
            GridView gvStoria = (GridView)e.Row.FindControl("GridViewAggiornamenti");
            GridView gvIndirizzi = (GridView)e.Row.FindControl("GridViewIndirizzi");
            HtmlTable tableCartacea = (HtmlTable)e.Row.FindControl("tableNotificaCartacea");

            HtmlTableRow trVisiteTitolo = (HtmlTableRow)e.Row.FindControl("trVisiteTitolo");
            HtmlTableRow trVisiteContenuto = (HtmlTableRow)e.Row.FindControl("trVisiteContenuto");
            
            Label lVisiteASLTitolo = (Label)e.Row.FindControl("LabelVisiteASLTitolo");
            Label lVisiteASL = (Label)e.Row.FindControl("LabelVisiteASL");
            Label lVisiteCPTTitolo = (Label)e.Row.FindControl("LabelVisiteCPTTitolo");
            Label lVisiteCPT = (Label)e.Row.FindControl("LabelVisiteCPT");
            Label lVisiteDTLTitolo = (Label)e.Row.FindControl("LabelVisiteDTLTitolo");
            Label lVisiteDTL = (Label)e.Row.FindControl("LabelVisiteDTL");
            Label lVisiteINAILTitolo = (Label)e.Row.FindControl("LabelVisiteINAILTitolo");
            Label lVisiteINAIL = (Label)e.Row.FindControl("LabelVisiteINAIL");
            Label lVisiteINPSTitolo = (Label)e.Row.FindControl("LabelVisiteINPSTitolo");
            Label lVisiteINPS = (Label)e.Row.FindControl("LabelVisiteINPS");
            Label lVisiteCassaEdileTitolo = (Label)e.Row.FindControl("LabelVisiteCassaEdileTitolo");
            Label lVisiteCassaEdile = (Label)e.Row.FindControl("LabelVisiteCassaEdile");

            Button bVisualizzaVisite = (Button)e.Row.FindControl("ButtonVisualizzaVisite");
            Button bInserisciVisita = (Button)e.Row.FindControl("ButtonInserisciVisita");

            if (notificaRif.Utente == "Cartacea")
            {
                tableCartacea.Visible = true;
            }

            lNotificaPrel.Text = notificaRif.IdNotificaPadre.ToString();
            if (notificaRif.IdNotifica != notificaRif.IdNotificaPadre)
            {
                lNotificaUlti.Text = notificaRif.IdNotifica.ToString();
                lData.Text = notificaRif.DataNotificaPadre.Value.ToShortDateString();
                lDataUltimoAgg.Text = notificaRif.Data.ToShortDateString();
            }
            else
            {
                lData.Text = notificaRif.Data.ToShortDateString();
            }
            lNaturaOpera.Text = notificaRif.NaturaOpera;
            lTipologiaAppalto.Text = notificaRif.AppaltoPrivato ? "Privato" : "Pubblico";
            if (notificaRif.TipologiaLavoro != null)
            {
                lTipologiaLavoro.Text = notificaRif.TipologiaLavoro.ToString();
            }
            lNumeroAppalto.Text = notificaRif.NumeroAppalto;
            lCommittente.Text =
                String.Format("{0}<BR/>{1} - {2} {3}", notificaRif.CommittenteRagioneSociale,
                              notificaRif.Committente.Indirizzo, notificaRif.Committente.Comune,
                              notificaRif.Committente.Provincia);

            Presenter.CaricaElementiInGridView(
                gvIndirizzi,
                notificaRif.Indirizzi,
                0);

            Presenter.CaricaElementiInGridView(
                gvStoria,
                notificaRif.Storia,
                0);

            #region Visite
            lVisiteASL.Text = notificaRif.NumeroVisiteASL.ToString();
            lVisiteCPT.Text = notificaRif.NumeroVisiteCPT.ToString();
            lVisiteDTL.Text = notificaRif.NumeroVisiteDPL.ToString();
            lVisiteINAIL.Text = notificaRif.NumeroVisiteINAIL.ToString();
            lVisiteINPS.Text = notificaRif.NumeroVisiteINPS.ToString();
            lVisiteCassaEdile.Text = notificaRif.NumeroVisiteCassaEdile.ToString();

            // Visualizzazione numero visite
            lVisiteASLTitolo.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteASL") || Roles.IsUserInRole("ContributoVisiteASL");
            lVisiteASL.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteASL") || Roles.IsUserInRole("ContributoVisiteASL");
            lVisiteCPTTitolo.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteCPT") || Roles.IsUserInRole("ContributoVisiteCPT");
            lVisiteCPT.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteCPT") || Roles.IsUserInRole("ContributoVisiteCPT");
            lVisiteDTLTitolo.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteDTL") || Roles.IsUserInRole("ContributoVisiteDTL");
            lVisiteDTL.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteDTL") || Roles.IsUserInRole("ContributoVisiteDTL");
            lVisiteINAILTitolo.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINAIL") || Roles.IsUserInRole("ContributoVisiteINAIL");
            lVisiteINAIL.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINAIL") || Roles.IsUserInRole("ContributoVisiteINAIL");
            lVisiteINPSTitolo.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINPS") || Roles.IsUserInRole("ContributoVisiteINPS");
            lVisiteINPS.Visible = Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINPS") || Roles.IsUserInRole("ContributoVisiteINPS");
            lVisiteCassaEdileTitolo.Visible = Roles.IsUserInRole("VisualizzazioneVisiteCassaEdile") || Roles.IsUserInRole("ContributoVisiteCassaEdile");
            lVisiteCassaEdile.Visible = Roles.IsUserInRole("VisualizzazioneVisiteCassaEdile") || Roles.IsUserInRole("ContributoVisiteCassaEdile");

            // Visualizzazione bottoni
            if (biz.UtentePuoVisualizzareVisite())
            {
                trVisiteTitolo.Visible = true;
                trVisiteContenuto.Visible = true;
                
                bVisualizzaVisite.Visible = true;

                if (biz.UtentePuoInserireVisita())
                {
                    bInserisciVisita.Visible = true;
                }
                else
                {
                    bInserisciVisita.Visible = false;
                }
            }
            else
            {
                trVisiteTitolo.Visible = false;
                trVisiteContenuto.Visible = false;
            }
            #endregion
        }
    }

    protected void GridViewNotifiche_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName != "Page")
        {
            Int32 indice = Int32.Parse(e.CommandArgument.ToString());
            Int32 idNotificaRiferimento = -1;

            switch (e.CommandName)
            {
                case "visualizzaVisite":
                    idNotificaRiferimento =
                        (Int32)
                        GridViewNotifiche.DataKeys[indice % GridViewNotifiche.PageSize].Values["IdNotificaRiferimento"];
                    Response.Redirect(String.Format("~/Visite/VisualizzaVisite.aspx?idNotifica={0}",
                                                    idNotificaRiferimento));
                    break;
                case "inserisciVisita":
                    idNotificaRiferimento =
                        (Int32)
                        GridViewNotifiche.DataKeys[indice % GridViewNotifiche.PageSize].Values["IdNotificaRiferimento"];
                    Response.Redirect(String.Format("~/Visite/InserimentoVisita.aspx?idNotifica={0}", idNotificaRiferimento));
                    break;
            }
        }
    }

    protected void GridViewAggiornamenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvStoria = (GridView)sender;
        Int32 idNotifica = (int)gvStoria.DataKeys[e.NewSelectedIndex].Value;

        Context.Items["IdNotifica"] = idNotifica;
        Server.Transfer("~/Ricerca/DettagliNotifica.aspx");
    }

    protected void GridViewAggiornamenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notifica = (Notifica)e.Row.DataItem;

            if (notifica.IdNotificaPadre.HasValue)
                e.Row.ForeColor = Color.Gray;
        }
    }

    protected void GridViewNotifiche_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaNotifiche(e.NewPageIndex);
    }

    protected void GridViewAggiornamenti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gvStoria = (GridView)sender;
        Int32 idNotifica = (int)gvStoria.DataKeys[e.RowIndex].Value;

        Context.Items["IdNotifica"] = idNotifica;
        Server.Transfer("~/Ricerca/RicevutaNotifica.aspx");
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche(0);
        }
    }

    protected void GridViewIndirizzi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Indirizzo indirizzo = (Indirizzo)e.Row.DataItem;
            ImageButton ibMappa = (ImageButton)e.Row.FindControl("ImageButtonMappa");

            if (!indirizzo.HaCoordinate())
            {
                ibMappa.ImageUrl = IMMAGINELOCALIZZAZIONE;
                e.Row.Cells[INDICEBOTTONEMAPPA].Enabled = false;
            }
            else
            {
                ibMappa.Attributes.Add("onclick", String.Format("openRadWindow({0}); return false;", indirizzo.IdIndirizzo));
            }
        }
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvIndirizzi = (GridView)sender;
        Int32 idIndirizzo = (Int32)gvIndirizzi.DataKeys[e.NewSelectedIndex].Values["IdIndirizzo"];

        Context.Items["IdIndirizzo"] = idIndirizzo;
        Server.Transfer("~/Ricerca/LocalizzaIndirizzo.aspx");
    }
}
