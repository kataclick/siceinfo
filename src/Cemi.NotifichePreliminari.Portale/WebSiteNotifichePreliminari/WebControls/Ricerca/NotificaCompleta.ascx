﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificaCompleta.ascx.cs" Inherits="WebControls_Ricerca_NotificaCompleta" %>
<%@ Register src="WebControls/NotificheFilter.ascx" tagname="NotificheFilter" tagprefix="uc1" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        function openRadWindow(idIndirizzo) {
            var oWindow = radopen("../Ricerca/LocalizzaIndirizzo.aspx?idIndirizzo=" + idIndirizzo, null);
            oWindow.set_title("Indirizzo");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 560);
            oWindow.center();
        }
    </script>

</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" />

<style type="text/css">
    .style1
    {
        width: 140px;
    }
</style>
<div>
    <table width="100%">
        <tr>
            <td colspan="2">
                <uc1:NotificheFilter ID="NotificheFilter1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricerca"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top" style="text-align:right;">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="150px" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricerca" />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Notifiche trovate" Visible="False"></asp:Label>
</div>
<br />
<div>
    <asp:GridView 
        ID="GridViewNotifiche"
        runat="server"  
        AutoGenerateColumns="False" 
        ShowHeader="False" 
        Width="100%" 
        OnRowDataBound="GridViewNotifiche_RowDataBound" 
        DataKeyNames="IdNotifica,IdNotificaRiferimento" 
        AllowPaging="True" 
        OnPageIndexChanging="GridViewNotifiche_PageIndexChanging" 
        PageSize="5" BorderColor="#0D859C" BorderStyle="Solid" BorderWidth="1.5pt" 
        GridLines="Horizontal" onrowcommand="GridViewNotifiche_RowCommand">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <table id="tableNotificaCartacea" class="tabellaConBordo" runat="server" visible="false" width="100%">
                                            <tr>
                                                <td style="text-align:center;">
                                                    NOTIFICA CARTACEA
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Notifica prelim.
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelProtocolloPreliminare" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Data:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelData" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Ultimo agg.
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelProtocolloUltimo" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Data ultimo agg.
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelDataUltimoAggiornamento" runat="server" Font-Bold="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Natura opera:
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelNaturaOpera" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Tipologia Appalto:
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelTipologiaAppalto" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Tipologia Lavoro:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelTipologiaLavoro" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        CIG/DIA/Permesso costr.:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelNumeroAppalto" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Committente:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Indirizzi:
                                    </td>
                                    <td>
                                        <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" 
                                                ShowHeader="False" Width="100%" DataKeyNames="IdIndirizzo" 
                                                onrowdatabound="GridViewIndirizzi_RowDataBound" 
                                                onselectedindexchanging="GridViewIndirizzi_SelectedIndexChanging" 
                                                BorderStyle="None" BorderWidth="0px" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButtonMappa" runat="server" CommandName="Select" 
                                                            ImageUrl="~/Images/localizzazione24.png" ToolTip="Visualizza la Mappa" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="24px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
                                            </Columns>
                                            <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="60%" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelAgg" runat="server" Font-Bold="True">Storia</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridViewAggiornamenti" runat="server" 
                                            AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdNotifica" 
                                            OnSelectedIndexChanging="GridViewAggiornamenti_SelectedIndexChanging" 
                                            Width="100%" OnRowDataBound="GridViewAggiornamenti_RowDataBound" 
                                            BorderColor="#0D859C" BorderStyle="Solid" 
                                            OnRowDeleting="GridViewAggiornamenti_RowDeleting" BorderWidth="1.5pt" 
                                            GridLines="Horizontal">
                                            <Columns>
                                                <asp:BoundField HeaderText="Protocollo" DataField="IdNotifica" >
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Data" DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" >
                                                </asp:BoundField>
                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectText="Dettagli" 
                                                    ShowSelectButton="True" SelectImageUrl="~/Images/dettagliNotifica32.png" >
                                                    <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Documento" 
                                                    ShowDeleteButton="True" DeleteImageUrl="~/Images/report32.png" >
                                                    <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trVisiteTitolo" runat="server">
                                    <td>
                                        <asp:Label ID="LabelVisite" runat="server" Font-Bold="True">Visite</asp:Label>
                                    </td>
                                </tr>
                                <tr id="trVisiteContenuto" runat="server">
                                    <td>
                                        <table class="tabellaConBordo" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelVisiteASLTitolo" runat="server" Text="Visite ASL:"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteASL" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteDTLTitolo" runat="server" Text="Visite DTL:"></asp:Label> 
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteDTL" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelVisiteINPSTitolo" runat="server" Text="Visite INPS:"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteINPS" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteINAILTitolo" runat="server" Text="Visite INAIL:"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteINAIL" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelVisiteCPTTitolo" runat="server" Text="Visite CPT:"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteCPT" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteCassaEdileTitolo" runat="server" Text="Visite Cassa Edile:"></asp:Label>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="LabelVisiteCassaEdile" runat="server"></asp:Label>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Button ID="ButtonVisualizzaVisite" runat="server" Width="120px" Text="Dettaglio visite"
                                                        CommandName="visualizzaVisite" CommandArgument="<%# Container.DataItemIndex %>" />
                                                </td>
                                                <td colspan="2">
                                                    <asp:Button ID="ButtonInserisciVisita" runat="server" Width="120px" Text="Inserisci visita"
                                                        CommandName="inserisciVisita" CommandArgument="<%# Container.DataItemIndex %>" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="200px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna notifica trovata
                </EmptyDataTemplate>
            </asp:GridView>
</div>