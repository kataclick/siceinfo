﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Filters;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Business;
using System.Web.Security;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class WebControls_Ricerca_Denuncia : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        NotificheFilter1.ModalitaDenuncia();

        if (!Page.IsPostBack)
        {
            Int32 pagina;
            MembershipUser utente = Membership.GetUser();
            NotificaFilter filtro = biz.GetFiltroRicercaDenunce((Guid) utente.ProviderUserKey, out pagina);
            if (filtro != null && !filtro.Vuoto())
            {
                NotificheFilter1.CaricaFiltroRicerca(filtro);
                CaricaDenunce(pagina);
            }
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaDenunce(0);
        }
    }

    public void CaricaDenunce(Int32 pagina)
    {
        NotificaFilter filtro = NotificheFilter1.GetFiltro(false);

        MembershipUser user = Membership.GetUser();
        biz.InsertFiltroRicercaDenunce((Guid) user.ProviderUserKey, filtro, pagina);

        CantiereCollection denunce = null;
        try
        {
            denunce = biz.GetCantieri(filtro);
        }
        catch
        {
            biz.DeleteFiltroRicerca((Guid) user.ProviderUserKey);
            throw;
        }

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Denunce trovate: {0}", denunce.Count.ToString());

        Presenter.CaricaElementiInGridView(
            GridViewDenunce,
            denunce,
            pagina);
    }

    protected void GridViewDenunce_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Cantiere cantiere = (Cantiere) e.Row.DataItem;

            Label lIndirizzo = (Label) e.Row.FindControl("LabelIndirizzo");
            Label lNaturaOpera = (Label) e.Row.FindControl("LabelNaturaOpera");
            Label lDataInizioLavori = (Label) e.Row.FindControl("LabelDataInizioLavori");
            Label lCommittente = (Label) e.Row.FindControl("LabelCommittente");
            Label lCapofila = (Label) e.Row.FindControl("LabelCapofila");
            Label lNessunaImpresa = (Label) e.Row.FindControl("LabelNessunaImpresa");
            BulletedList blImprese = (BulletedList) e.Row.FindControl("BulletedListImprese");
            
            lIndirizzo.Text = cantiere.IndirizzoCantiere.IndirizzoCompleto;
            if (!String.IsNullOrEmpty(cantiere.NaturaOpera))
            {
                lNaturaOpera.Text = cantiere.NaturaOpera;
            }
            else
            {
                lNaturaOpera.Text = "--";
            }
            if (cantiere.DataInizioLavori.HasValue)
            {
                lDataInizioLavori.Text = cantiere.DataInizioLavori.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                lDataInizioLavori.Text = "--";
            }
            if (cantiere.Committente != null)
            {
                lCommittente.Text = cantiere.Committente.ToString();
            }
            else
            {
                lCommittente.Text = "--";
            }
            if (cantiere.Capofila != null)
            {
                lCapofila.Text = cantiere.Capofila.RagioneSociale;
            }
            else
            {
                lCapofila.Text = "--";
            }

            if (cantiere.Subappalti != null && cantiere.Subappalti.Count > 0)
            {
                // Metto solo i primi 2

                blImprese.Visible = true;
                lNessunaImpresa.Visible = false;

                if (cantiere.Subappalti.Count < 3)
                {
                    Presenter.CaricaElementiInBulletedList(
                        blImprese,
                        cantiere.Subappalti,
                        "ImpresaInSubappaltoRagioneSociale",
                        "IdSubappalto");
                }
                else
                {
                    for (Int32 i = 0; i < 2; i++)
                    {
                        blImprese.Items.Add(cantiere.Subappalti[i].ImpresaInSubappaltoRagioneSociale);
                    }
                    blImprese.Items.Add("...");
                }
            }
            else
            {
                blImprese.Visible = false;
                lNessunaImpresa.Visible = true;
            }
        }
    }

    protected void GridViewDenunce_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaDenunce(e.NewPageIndex);
    }

    protected void GridViewDenunce_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 indice = -1;

        switch (e.CommandName)
        {
            case "dettaglio":
                indice = Int32.Parse(e.CommandArgument.ToString());
                Context.Items["IdDenuncia"] = (Int32) GridViewDenunce.DataKeys[indice].Values["IdDenunciaCantiere"];
                Server.Transfer("~/Inserimento/RicevutaDenuncia.aspx");
                break;
        }
    }
}