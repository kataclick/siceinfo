﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Denuncia.ascx.cs" Inherits="WebControls_Ricerca_Denuncia" %>
<%@ Register src="WebControls/NotificheFilter.ascx" tagname="NotificheFilter" tagprefix="uc1" %>

<div>
    <table width="100%">
        <tr>
            <td colspan="2">
                <uc1:NotificheFilter ID="NotificheFilter1" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricerca"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top" style="text-align:right;">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="150px" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricerca" />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Denunce trovate" Visible="False"></asp:Label>
</div>
<div>
    <asp:GridView
        ID="GridViewDenunce"
        runat="server"
        AutoGenerateColumns="False"
        Width="100%" CssClass="tabellaConBordo" DataKeyNames="IdDenunciaCantiere" 
        onrowcommand="GridViewDenunce_RowCommand" 
        onrowdatabound="GridViewDenunce_RowDataBound"
        AllowPaging="true"
        PageSize="5" onpageindexchanging="GridViewDenunce_PageIndexChanging">
        <Columns>
            <asp:BoundField HeaderText="Data" DataField="Data" 
                DataFormatString="{0:dd/MM/yyyy}">
            <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Codice" DataField="CodiceCantiere">
            <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Cantiere">
                <ItemTemplate>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <b><asp:Label ID="LabelIndirizzo" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px">
                                Natura opera:
                            </td>
                            <td>
                                <b><asp:Label ID="LabelNaturaOpera" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px">
                                Data inizio lavori:
                            </td>
                            <td>
                                <b><asp:Label ID="LabelDataInizioLavori" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px">
                                Committente:
                            </td>
                            <td>
                                <b><asp:Label ID="LabelCommittente" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px">
                                Capofila:
                            </td>
                            <td>
                                <b><asp:Label ID="LabelCapofila" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    Imprese
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label
                                    ID="LabelNessunaImpresa"
                                    runat="server"
                                    Visible="false"
                                    Text="Nessun'altra impresa presente">
                                </asp:Label>
                                <asp:BulletedList
                                    ID="BulletedListImprese"
                                    runat="server">
                                </asp:BulletedList>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton
                        ID="ImageButtonDettaglio"
                        runat="server" 
                        ImageUrl="~/Images/report32.png"
                        CommandName="dettaglio"
                        CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                        ToolTip="Dettaglio" />
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna denuncia presente.
        </EmptyDataTemplate>
    </asp:GridView>
</div>
