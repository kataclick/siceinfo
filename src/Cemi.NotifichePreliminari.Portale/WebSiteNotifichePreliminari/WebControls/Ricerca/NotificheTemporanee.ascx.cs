using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_Ricerca_NotificheTemporanee : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaNotificheTemporanee(0);
        }
    }

    private void CaricaNotificheTemporanee(Int32 pagina)
    {
        MembershipUser user = Membership.GetUser();

        Presenter.CaricaElementiInGridView(
            GridViewNotificheTemporanee,
            biz.GetNotificheTemporanee((Guid)user.ProviderUserKey),
            pagina);
    }

    protected void GridViewNotificheTemporanee_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            NotificaTelematica notifica = (NotificaTelematica)e.Row.DataItem;
            Label lRagioneSociale = (Label)e.Row.FindControl("LabelNaturaOpera");
            BulletedList blIndirizzi = (BulletedList)e.Row.FindControl("BulletedListIndirizzi");

            lRagioneSociale.Text = notifica.NaturaOpera;

            Presenter.CaricaElementiInBulletedList(
                blIndirizzi,
                notifica.Indirizzi,
                "IndirizzoCompleto",
                "");
        }
    }

    protected void GridViewNotificheTemporanee_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idNotificaTemporanea = (Int32)GridViewNotificheTemporanee.DataKeys[e.NewSelectedIndex].Values["IdNotificaTemporanea"];

        Context.Items["IdNotificaTemporanea"] = idNotificaTemporanea;
        Server.Transfer("~/Inserimento/InserimentoNotifica.aspx");
    }

    protected void GridViewNotificheTemporanee_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Int32 idNotificaTemporanea = (Int32)GridViewNotificheTemporanee.DataKeys[e.RowIndex].Values["IdNotificaTemporanea"];

        if (biz.DeleteNotificaTemporanea(idNotificaTemporanea))
        {
            CaricaNotificheTemporanee(0);
        }
    }

    protected void GridViewNotificheTemporanee_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaNotificheTemporanee(e.NewPageIndex);
    }
}
