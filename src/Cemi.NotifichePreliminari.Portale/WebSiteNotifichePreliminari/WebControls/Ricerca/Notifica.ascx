﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Notifica.ascx.cs" Inherits="WebControls_Ricerca_Notifica" %>
<%@ Register src="WebControls/NotificheFilter.ascx" tagname="NotificheFilter" tagprefix="uc1" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        function openRadWindow(idIndirizzo) {
            var oWindow = radopen("../Ricerca/LocalizzaIndirizzo.aspx?idIndirizzo=" + idIndirizzo, null);
            oWindow.set_title("Indirizzo");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 560);
            oWindow.center();
        }
    </script>

</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" />

<div>
    <table width="100%">
        <tr>
            <td colspan="2">
                
                <uc1:NotificheFilter ID="NotificheFilter1" runat="server" />
                
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricerca"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top" style="text-align:right;">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="150px" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricerca" />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Notifiche trovate" Visible="False"></asp:Label>
</div>
<br />
<div>
    <asp:GridView ID="GridViewNotifiche" runat="server" Width="100%" 
        AutoGenerateColumns="False" 
        onrowdatabound="GridViewNotifiche_RowDataBound" AllowPaging="True" 
        DataKeyNames="IdNotifica,IdNotificaRiferimento" 
        onpageindexchanging="GridViewNotifiche_PageIndexChanging" 
        onselectedindexchanging="GridViewNotifiche_SelectedIndexChanging" 
        PageSize="5" onrowdeleting="GridViewNotifiche_RowDeleting" 
        onrowediting="GridViewNotifiche_RowEditing">
        <Columns>
            <asp:TemplateField HeaderText="Tipo">
                <ItemTemplate>
                    <asp:Label 
                        ID="LabelTipoNotifica" 
                        runat="server">
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="80px" />
            </asp:TemplateField>
            <asp:BoundField HeaderText="Protocollo" DataField="IdNotifica" >
            <ItemStyle Width="60px" />
            </asp:BoundField>
            <asp:BoundField DataField="IdNotificaPadre" HeaderText="Prot. rif.">
            <ItemStyle Width="60px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Data ins." DataField="DataInserimento" 
                DataFormatString="{0:dd/MM/yyyy}">
            <ItemStyle Width="70px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Tipo Appalto">
                <ItemTemplate>
                    <asp:Label 
                        ID="LabelTipoAppalto"
                        runat="server">
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dati">
                <ItemTemplate>
                    <b>
                        <asp:Label
                            ID="LabelNaturaOpera"
                            runat="server">
                        </asp:Label>
                    </b>
                    <br />
                    <br />
                    <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" 
                            ShowHeader="False" Width="100%" DataKeyNames="IdIndirizzo" 
                            onrowdatabound="GridViewIndirizzi_RowDataBound" 
                            onselectedindexchanging="GridViewIndirizzi_SelectedIndexChanging" 
                        BorderStyle="None" BorderWidth="0px" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButtonMappa" runat="server" CommandName="Select" 
                                            ImageUrl="~/Images/localizzazione24.png" ToolTip="Visualizza su Mappa" />
                                    </ItemTemplate>
                                    <ItemStyle Width="24px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
                            </Columns>
                            <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButtonDettagli" runat="server" CausesValidation="False" 
                        CommandName="Select" ImageUrl="~/Images/dettagliNotifica32.png" ToolTip="Dettagli" />
                </ItemTemplate>
                <ControlStyle CssClass="bottoneGriglia" />
                <ItemStyle Width="10px" />
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:ImageButton ID="ImageButtonRicevuta" runat="server" CausesValidation="False" 
                        CommandName="Delete" ImageUrl="~/Images/report32.png" ToolTip="Ricevuta" />
                </ItemTemplate>
                <ControlStyle CssClass="bottoneGriglia" />
                <ItemStyle Width="10px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna notifica trovata
        </EmptyDataTemplate>
    </asp:GridView>
</div>