using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Delegates;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.Presenter;

public partial class WebControls_Ricerca_Impresa : System.Web.UI.UserControl
{
    public event ImpresaSelectedEventHandler OnImpresaSelected;
    public event ImpresaNuovaEventHandler OnImpresaNuova;

    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void NotificaCartacea()
    {
        Impresa1.NotificaCartacea();
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ImpresaNotificheTelematiche impresa = biz.GetImpreseTelematicheDaSiceNewEAnagrafica(
                Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text));
            
            CaricaImprese(impresa);

            ButtonNuovo.Enabled = true;
        }
    }

    private void CaricaImprese(ImpresaNotificheTelematiche impresa)
    {
        if (impresa != null)
        {
            // Carico i dati dell'impresa
            PanelDatiImprese.Enabled = true;
            Impresa1.CaricaImpresa(impresa);
            LabelImpresaNonTrovata.Visible = false;

            if (OnImpresaSelected != null)
            {
                OnImpresaSelected();
            }
        }
        else
        {
            // L'impresa va inserita come nuova
            PanelDatiImprese.Enabled = true;
            Impresa1.Nuovo();
            LabelImpresaNonTrovata.Visible = true;

            if (OnImpresaNuova != null)
            {
                OnImpresaNuova();
            }
        }
    }

    public ImpresaNotificheTelematiche CreaImpresa()
    {
        return Impresa1.CreaImpresa();
    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        LabelImpresaNonTrovata.Visible = false;
        Impresa1.ResetCampi();
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        Impresa1.Nuovo();

        if (OnImpresaNuova != null)
        {
            OnImpresaNuova();
        }
    }

    public void DisabilitaNuovo()
    {
        ButtonNuovo.Enabled = false;
    }
}
