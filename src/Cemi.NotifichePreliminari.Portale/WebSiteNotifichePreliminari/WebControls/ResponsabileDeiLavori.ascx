﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ResponsabileDeiLavori.ascx.cs" Inherits="WebControls_ResponsabileDeiLavori" %>
<%@ Register src="Visualizzazione/Persona.ascx" tagname="Persona" tagprefix="uc1" %>
<div>
    <asp:CheckBox 
        ID="CheckBoxNonNominato" 
        Checked="false" 
        runat="server" 
        Text="Non nominato (responsabilità del committente)"
        AutoPostBack="True" 
        oncheckedchanged="CheckBoxNonNominato_CheckedChanged" />
</div>
<div>
    <asp:Panel ID="PanelResponsabileLavori" Visible="true" runat="server">
        <uc1:Persona ID="PersonaResponsabileDeiLavori" runat="server" />
    </asp:Panel>
</div>