using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Type.Delegates;

public partial class WebControls_ImpreseAffidatarie : System.Web.UI.UserControl
{
    private const String URLELIMINA = "~/Images/pallinoElimina.png";
    private const String URLELIMINADISABILITATO = "~/Images/pallinoEliminaDisabilitato.png";
    private const Int32 INDICEVISTARAGIONESOCIALE = 0;
    private const Int32 INDICEVISTAMODIFICA = 1;

    private SubappaltoNotificheTelematicheCollection impreseEsecutrici = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Registrazione eventi
        Impresa1.OnImpresaSelected += new ImpresaSelectedEventHandler(Impresa1_OnImpresaSelected);
        Impresa1.OnImpresaNuova += new ImpresaNuovaEventHandler(Impresa1_OnImpresaNuova);
        #endregion

        if (!Page.IsPostBack)
        {
            if (ViewState["ImpreseAffidatarie"] == null)
            {
                ViewState["ImpreseAffidatarie"] = new SubappaltoNotificheTelematicheCollection();
            }

            CaricaImpreseAffidatarieSelezionate();
        }
    }

    public void NotificaCartacea()
    {
        Impresa1.NotificaCartacea();
    }

    void Impresa1_OnImpresaNuova()
    {
        ButtonAggiungi.Enabled = true;
    }

    void Impresa1_OnImpresaSelected()
    {
        ButtonAggiungi.Enabled = true;
    }

    private void CaricaImpreseAffidatarieSelezionate()
    {
        SubappaltoNotificheTelematicheCollection impreseAffidatarie = ViewState["ImpreseAffidatarie"] as SubappaltoNotificheTelematicheCollection;
        Presenter.CaricaElementiInGridView(
            GridViewImpreseAffidatarie, 
            impreseAffidatarie, 
            0);
    }

    public void ForzaCaricamento(SubappaltoNotificheTelematicheCollection impreseEsecutrici)
    {
        ViewState["ImpreseEsecutrici"] = impreseEsecutrici;
        CaricaImpreseAffidatarieSelezionate();
    }

    protected void ButtonAggiungi_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SubappaltoNotificheTelematiche subappalto = CreaAppalto();
            if (AggiungiImpresaALista(subappalto))
            {
                Impresa1.ResetCampi();
                CaricaImpreseAffidatarieSelezionate();
                PanelAltraImpresa.Visible = false;
                ButtonAltraImpresa.Enabled = true;
            }
        }
    }

    private Boolean AggiungiImpresaALista(SubappaltoNotificheTelematiche subappalto)
    {
        Boolean impresaPresente = false;
        SubappaltoNotificheTelematicheCollection impreseAffidatarie = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseAffidatarie"];
        

        foreach (SubappaltoNotificheTelematiche sub in impreseAffidatarie)
        {
            if ((!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.PartitaIva) && sub.ImpresaSelezionata.PartitaIva == subappalto.ImpresaSelezionata.PartitaIva)
                ||
                (!String.IsNullOrEmpty(subappalto.ImpresaSelezionata.CodiceFiscale) && sub.ImpresaSelezionata.CodiceFiscale == subappalto.ImpresaSelezionata.CodiceFiscale))
            {
                impresaPresente = true;
                break;
            }
        }

        if (!impresaPresente)
        {
            Presenter.SvuotaCampo(LabelErrore);
            impreseAffidatarie.Add(subappalto);
            ViewState["ImpreseAffidatarie"] = impreseAffidatarie;
        }
        else
        {
            LabelErrore.Text = "Partita Iva o Codice fiscale gi� presente nella lista delle Imprese Affidatarie";
        }

        return !impresaPresente;
    }

    private SubappaltoNotificheTelematiche CreaAppalto()
    {
        SubappaltoNotificheTelematiche subappalto = new SubappaltoNotificheTelematiche();
        subappalto.ImpresaSelezionata = Impresa1.CreaImpresa();
        subappalto.Affidatarie = true;
        
        return subappalto;
    }

    protected void ButtonAltraImpresa_Click(object sender, EventArgs e)
    {
        PanelAltraImpresa.Visible = true;
        ButtonAltraImpresa.Enabled = false;
        Impresa1.DisabilitaNuovo();
    }

    public SubappaltoNotificheTelematicheCollection GetImpreseAffidatarie()
    {
        SubappaltoNotificheTelematicheCollection impreseAffidatarie = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseAffidatarie"];
        return impreseAffidatarie;
    }

    public void CaricaImpreseAffidatarie(NotificaTelematica notifica)
    {
        ViewState["ImpreseAffidatarie"] = notifica.ImpreseAffidatarie;
        ViewState["ImpreseEsecutrici"] = notifica.ImpreseEsecutrici;
        CaricaImpreseAffidatarieSelezionate();
    }

    protected void GridViewImpreseAffidatarie_DataBinding(object sender, EventArgs e)
    {
        impreseEsecutrici = ViewState["ImpreseEsecutrici"] as SubappaltoNotificheTelematicheCollection;
    }

    protected void GridViewImpreseAffidatarie_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoNotificheTelematiche subappalto = (SubappaltoNotificheTelematiche)e.Row.DataItem;
            ImageButton ibElimina = (ImageButton)e.Row.FindControl("ImageButtonElimina");
            Label lRagioneSociale = (Label)e.Row.FindControl("LabelRagioneSociale");
            Label lPartitaIva = (Label)e.Row.FindControl("LabelPartitaIva");
            Label lCodiceFiscale = (Label)e.Row.FindControl("LabelCodiceFiscale");

            lRagioneSociale.Text = subappalto.ImpresaSelezionataRagioneSociale;
            lPartitaIva.Text = String.Format("P.Iva: {0}", subappalto.ImpresaSelezionataPartitaIva);
            lCodiceFiscale.Text = String.Format("Cod.Fisc.: {0}", subappalto.ImpresaSelezionataCodiceFiscale);

            if (impreseEsecutrici != null)
            {
                if (impreseEsecutrici.ImpresaPresente(
                    subappalto.ImpresaSelezionata.IdImpresaTelematica,
                    subappalto.ImpresaSelezionata.IdTemporaneo))
                {
                    ibElimina.ImageUrl = URLELIMINADISABILITATO;
                    ibElimina.Enabled = false;
                }
            }
        }
    }

    protected void GridViewImpreseAffidatarie_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        MultiView mvModifica = (MultiView)GridViewImpreseAffidatarie.Rows[e.NewSelectedIndex].FindControl("MultiViewModifica");
        mvModifica.ActiveViewIndex = INDICEVISTAMODIFICA;
        
        WebControls_Visualizzazione_Impresa visImp = (WebControls_Visualizzazione_Impresa)GridViewImpreseAffidatarie.Rows[e.NewSelectedIndex].FindControl("Impresa2");
        SubappaltoNotificheTelematicheCollection subappalti = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseAffidatarie"];
        visImp.CaricaImpresa(subappalti[e.NewSelectedIndex].ImpresaSelezionata);
    }

    protected void GridViewImpreseAffidatarie_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Int32 indiceSubappalto = e.RowIndex;
        SubappaltoNotificheTelematicheCollection subappalti = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseAffidatarie"];

        subappalti.RemoveAt(indiceSubappalto);
        CaricaImpreseAffidatarieSelezionate();
    }

    protected void GridViewImpreseAffidatarie_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Page.Validate("stop");

        if (Page.IsValid)
        {
            WebControls_Visualizzazione_Impresa visImp = (WebControls_Visualizzazione_Impresa)GridViewImpreseAffidatarie.Rows[e.NewEditIndex].FindControl("Impresa2");
            SubappaltoNotificheTelematicheCollection subappalti = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseAffidatarie"];
            subappalti[e.NewEditIndex].ImpresaSelezionata = visImp.CreaImpresa();
            ViewState["ImpreseAffidatarie"] = subappalti;

            MultiView mvModifica = (MultiView)GridViewImpreseAffidatarie.Rows[e.NewEditIndex].FindControl("MultiViewModifica");
            mvModifica.ActiveViewIndex = INDICEVISTARAGIONESOCIALE;
            CaricaImpreseAffidatarieSelezionate();
        }
    }

    protected void GridViewImpreseAffidatarie_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        MultiView mvModifica = (MultiView)GridViewImpreseAffidatarie.Rows[e.RowIndex].FindControl("MultiViewModifica");
        mvModifica.ActiveViewIndex = INDICEVISTARAGIONESOCIALE;
    }

    protected void CustomValidatorImpreseAffidatarie_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (ViewState["ImpreseAffidatarie"] != null)
        {
            SubappaltoNotificheTelematicheCollection impAff = (SubappaltoNotificheTelematicheCollection)ViewState["ImpreseAffidatarie"];
            if (impAff.Count > 0)
            {
                args.IsValid = true;
            }
        }
    }
}
