﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Localizzazione.ascx.cs" Inherits="WebControls_Visualizzazione_Localizzazione" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" %>

            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 43.437493;
                var longitudine = 11.051392;
                var zoom = 8;

                function LoadMap() {
                    var latitudine = 43.437493;
                    var longitudine = 11.051392;
                    var zoom = 8;
                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }

                function SetCenter(lat, lon, zoomLevel) {
                    var position = new VELatLong(lat, lon);
                    map.SetCenterAndZoom(position, zoomLevel);
                }
                
            </script>
        <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
        </div>
            <script type="text/javascript">
                LoadMap();
            </script>

