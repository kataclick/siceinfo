using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_Visualizzazione_Persona : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (DropDownListPersonaProvincia.Items.Count == 0)
            {
                Presenter.CaricaProvince(DropDownListPersonaProvincia);
            }
            //if (DropDownListEnteProvincia.Items.Count == 0)
            //{
            //    Presenter.CaricaProvince(DropDownListEnteProvincia);
            //}
        }
    }

    protected void DropDownListPersonaProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        Presenter.CaricaComuni(
            DropDownListPersonaComune,
            DropDownListPersonaProvincia.SelectedItem.Text);
    }

    //protected void DropDownListEnteProvincia_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Presenter.CaricaComuni(
    //        DropDownListEnteComune,
    //        DropDownListEnteProvincia.SelectedItem.Text);
    //}

    public void NotificaCartacea()
    {
        RequiredFieldValidatorTextBoxPersonaCodiceFiscale.Enabled = false;
        RequiredFieldValidatorTextBoxPersonaIndirizzo.Enabled = false;
        RequiredFieldValidatorTextBoxPersonaProvincia.Enabled = false;
        RequiredFieldValidatorTextBoxPersonaCitta.Enabled = false;
        RequiredFieldValidatorTelefono.Enabled = false;

        LabelCodiceFiscale.Visible = false;
        LabelIndirizzo.Visible = false;
        LabelProvincia.Visible = false;
        LabelCitta.Visible = false;
        LabelTelefono.Visible = false;

        ViewState["Cartacea"] = true;
    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxPersonaCognome);
        Presenter.SvuotaCampo(TextBoxPersonaNome);
        Presenter.SvuotaCampo(TextBoxPersonaIndirizzo);

        DropDownListPersonaComune.Items.Clear();
        DropDownListPersonaProvincia.SelectedValue = String.Empty;
        
        Presenter.SvuotaCampo(TextBoxPersonaCap);
        Presenter.SvuotaCampo(TextBoxPersonaTelefono);
        Presenter.SvuotaCampo(TextBoxPersonaFax);
        Presenter.SvuotaCampo(TextBoxPersonaCellulare);
        Presenter.SvuotaCampo(TextBoxPersonaEmail);
        Presenter.SvuotaCampo(TextBoxPersonaCodiceFiscale);
        //Presenter.SvuotaCampo(TextBoxEnteRagioneSociale);
        //Presenter.SvuotaCampo(TextBoxEntePartitaIva);
        //Presenter.SvuotaCampo(TextBoxEnteCodiceFiscale);
        //Presenter.SvuotaCampo(TextBoxEnteIndirizzo);

        //DropDownListEnteComune.Items.Clear();
        //DropDownListEnteProvincia.SelectedIndex = 0;
        
        //Presenter.SvuotaCampo(TextBoxEnteCap);
        //Presenter.SvuotaCampo(TextBoxEnteTelefono);
        //Presenter.SvuotaCampo(TextBoxEnteFax);
    }

    public void CaricaPersona(PersonaNotificheTelematiche persona)
    {
        ResetCampi();

        if (persona != null)
        {
            TextBoxPersonaCognome.Text = persona.PersonaCognome;
            TextBoxPersonaNome.Text = persona.PersonaNome;
            TextBoxPersonaIndirizzo.Text = persona.Indirizzo;

            Presenter.CaricaProvince(DropDownListPersonaProvincia);
            DropDownListPersonaProvincia.SelectedValue = persona.PersonaProvincia;
            DropDownListPersonaProvincia_SelectedIndexChanged(this, new EventArgs());

            if (DropDownListPersonaComune.Items.Count > 0)
            {
                DropDownListPersonaComune.SelectedValue = persona.PersonaComune;
            }
            
            TextBoxPersonaCap.Text = persona.PersonaCap;
            TextBoxPersonaTelefono.Text = persona.Telefono;
            TextBoxPersonaFax.Text = persona.Fax;
            TextBoxPersonaCellulare.Text = persona.PersonaCellulare;
            TextBoxPersonaEmail.Text = persona.PersonaEmail;
            TextBoxPersonaCodiceFiscale.Text = persona.PersonaCodiceFiscale;

            //TextBoxEnteRagioneSociale.Text = persona.RagioneSociale;
            //TextBoxEntePartitaIva.Text = persona.EntePartitaIva;
            //TextBoxEnteCodiceFiscale.Text = persona.EnteCodiceFiscale;
            //TextBoxEnteIndirizzo.Text = persona.EnteIndirizzo;

            //Presenter.CaricaProvince(DropDownListEnteProvincia);
            //DropDownListEnteProvincia.SelectedValue = persona.EnteProvincia;
            //DropDownListEnteProvincia_SelectedIndexChanged(this, new EventArgs());

            //if (DropDownListEnteComune.Items.Count > 0)
            //{
            //    DropDownListEnteComune.SelectedValue = persona.EnteComune;
            //}

            //TextBoxEnteCap.Text = persona.EnteCap;
            //TextBoxEnteTelefono.Text = persona.EnteTelefono;
            //TextBoxEnteFax.Text = persona.EnteFax;
        }
    }

    public PersonaNotificheTelematiche CreaPersona()
    {
        PersonaNotificheTelematiche persona = new PersonaNotificheTelematiche();

        // Creo la persona
        persona.PersonaCognome = Presenter.NormalizzaCampoTesto(TextBoxPersonaCognome.Text);
        persona.PersonaNome = Presenter.NormalizzaCampoTesto(TextBoxPersonaNome.Text);
        persona.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxPersonaIndirizzo.Text);
        persona.PersonaProvincia = DropDownListPersonaProvincia.SelectedItem.Text;
        if (DropDownListPersonaComune.SelectedItem != null)
        {
            persona.PersonaComune = DropDownListPersonaComune.SelectedItem.Text;
        }
        persona.PersonaCap = Presenter.NormalizzaCampoTesto(TextBoxPersonaCap.Text);
        persona.Telefono = Presenter.NormalizzaCampoTesto(TextBoxPersonaTelefono.Text);
        persona.Fax = Presenter.NormalizzaCampoTesto(TextBoxPersonaFax.Text);
        persona.PersonaCellulare = Presenter.NormalizzaCampoTesto(TextBoxPersonaCellulare.Text);
        persona.PersonaEmail = TextBoxPersonaEmail.Text;
        persona.PersonaCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxPersonaCodiceFiscale.Text);

        //persona.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxEnteRagioneSociale.Text);
        //persona.EntePartitaIva = Presenter.NormalizzaCampoTesto(TextBoxEntePartitaIva.Text);
        //persona.EnteCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxEnteCodiceFiscale.Text);
        //persona.EnteIndirizzo = Presenter.NormalizzaCampoTesto(TextBoxEnteIndirizzo.Text);
        //persona.EnteProvincia = DropDownListEnteProvincia.SelectedItem.Text;
        //if (DropDownListEnteComune.SelectedItem != null)
        //{
        //    persona.EnteComune = DropDownListEnteComune.SelectedItem.Text;
        //}
        //persona.EnteCap = Presenter.NormalizzaCampoTesto(TextBoxEnteCap.Text);
        //persona.EnteTelefono = Presenter.NormalizzaCampoTesto(TextBoxEnteTelefono.Text);
        //persona.EnteFax = Presenter.NormalizzaCampoTesto(TextBoxEnteFax.Text);

        return persona;
    }

    public void GestisciValidator(Boolean abilita)
    {
        //RequiredFieldValidatorTextBoxPersonaCitta.Enabled = abilita;
        //RequiredFieldValidatorTextBoxPersonaCodiceFiscale.Enabled = abilita;
        //RequiredFieldValidatorTextBoxPersonaCognome.Enabled = abilita;
        //RequiredFieldValidatorTextBoxPersonaIndirizzo.Enabled = abilita;
        //RequiredFieldValidatorTextBoxPersonaNome.Enabled = abilita;
        //RequiredFieldValidatorTextBoxPersonaProvincia.Enabled = abilita;
        foreach (Control control in this.Controls)
        {
            if (control is BaseValidator)
            {
                BaseValidator val = control as BaseValidator;
                val.Enabled = abilita;
            }
        }

        if (ViewState["Cartacea"] != null)
        {
            NotificaCartacea();
        }
    }

    //protected void CustomValidatorEntePartitaIvaFormato_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    if (!Presenter.VerificaPartitaIVA(TextBoxEntePartitaIva.Text))
    //    {
    //        args.IsValid = false;
    //    }
    //}
}
