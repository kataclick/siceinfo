﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Utente.ascx.cs" Inherits="WebControls_Visualizzazione_Utente" %>
<style type="text/css">
    .primaColonna
    {
        width: 186px;
    }
    .secondaColonna
    {
        width: 320px;
    }
</style>
<asp:Panel
    ID="PanelUtente"
    runat="server"
    Width="100%">
    <table width="100%">
        <tr>
            <td colspan="3">
                <b>
                    Dati Utente
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Tipo di soggetto<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:DropDownList
                    ID="DropDownListQualifica"
                    AppendDataBoundItems="true"
                    runat="server"
                    Width="300px" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListQualifica_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorQualifica"
                    runat="server"
                    ControlToValidate="DropDownListQualifica"
                    ErrorMessage="Selezionare la Qualifica">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <%--<asp:Panel
                    ID="PanelAzienda"
                    runat="server"
                    Width="100%">--%>
                    <%--<table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="3">--%>
                                &nbsp;&nbsp;<small>Se si opera per conto di un'azienda devono essere indicati anche la ragione sociale, la partita iva e il codice fiscale</small>
                            </td>
                        </tr>
                        <tr>
                            <td class="primaColonna">
                                &nbsp;&nbsp;<small>Ragione Sociale</small>
                            </td>
                            <td class="secondaColonna">
                                <asp:TextBox
                                    ID="TextBoxAziendaRagioneSociale"
                                    runat="server"
                                    MaxLength="500"
                                    Width="300px">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:CustomValidator
                                    ID="CustomValidatorAziendaRagioneSociale"
                                    runat="server"
                                    ErrorMessage="Ragione sociale dell'azienda mancante" 
                                    onservervalidate="CustomValidatorAziendaRagioneSociale_ServerValidate">
                                    *
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="primaColonna">
                                &nbsp;&nbsp;<small>Partita IVA</small>
                            </td>
                            <td class="secondaColonna">
                                <asp:TextBox
                                    ID="TextBoxAziendaPartitaIVA"
                                    runat="server"
                                    MaxLength="11"
                                    Width="300px">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:CustomValidator
                                    ID="CustomValidatorTextBoxAziendaPartitaIVA"
                                    runat="server"
                                    ErrorMessage="Partita IVA dell'azienda mancante" 
                                    onservervalidate="CustomValidatorTextBoxAziendaPartitaIVA_ServerValidate">
                                    *
                                </asp:CustomValidator>
                                <asp:CustomValidator
                                    ID="CustomValidatorTextBoxAziendaPartitaIVAFormato"
                                    runat="server"
                                    ErrorMessage="Partita Iva dell'azienda non valida" 
                                    onservervalidate="CustomValidatorTextBoxAziendaPartitaIVAFormato_ServerValidate">
                                    *
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="primaColonna">
                                &nbsp;&nbsp;<small>Codice Fiscale</small>
                            </td>
                            <td class="secondaColonna">
                                <asp:TextBox
                                    ID="TextBoxAziendaCodiceFiscale"
                                    runat="server"
                                    MaxLength="16"
                                    Width="300px">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:CustomValidator
                                    ID="CustomValidatorTextBoxAziendaCodiceFiscale"
                                    runat="server"
                                    ErrorMessage="Codice fiscale dell'azienda mancante" 
                                    onservervalidate="CustomValidatorTextBoxAziendaCodiceFiscale_ServerValidate">
                                    *
                                </asp:CustomValidator>
                                <asp:RegularExpressionValidator 
                                    ID="RegularExpressionValidatorTextBoxAziendaCodiceFiscale" 
                                    runat="server" 
                                    ControlToValidate="TextBoxAziendaCodiceFiscale"
                                    ErrorMessage="Codice fiscale dell'azienda non corretto" 
                                    ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$">
                                    *
                                </asp:RegularExpressionValidator>
                            <%--</td>
                        </tr>--%>
                    <%--</table>--%>
                <%--</asp:Panel>--%>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Richiesta aut. alla:<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:DropDownList
                    ID="DropDownListCassaEdile"
                    AppendDataBoundItems="true"
                    runat="server"
                    Width="300px">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCassaEdile"
                    runat="server"
                    ControlToValidate="DropDownListCassaEdile"
                    ErrorMessage="Selezionare la Cassa Edile a cui viene richiesta l'autorizzazione">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Cognome<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxCognome"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCognome"
                    runat="server"
                    ControlToValidate="TextBoxCognome"
                    ErrorMessage="Cognome mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Nome<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxNome"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorNome"
                    runat="server"
                    ControlToValidate="TextBoxNome"
                    ErrorMessage="Nome mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Sesso<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:RadioButtonList
                    ID="RadioButtonListSesso"
                    runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True">M</asp:ListItem>
                    <asp:ListItem>F</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Data di Nascita<b>*</b>:
            </td>
            <td class="secondaColonna">
                <telerik:RadDatePicker
                    ID="RadDatePickerDataNascita"
                    runat="server"
                    Width="300px" MinDate="1920-01-01">
                </telerik:RadDatePicker>
            </td>
            <td>
                <asp:CustomValidator
                    ID="CustomValidatorDataNascita"
                    runat="server"
                    ControlToValidate="RadDatePickerDataNascita"
                    ErrorMessage="Data di Nascita mancante" 
                    onservervalidate="CustomValidatorDataNascita_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Codice Fiscale<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxCodiceFiscale"
                    runat="server"
                    MaxLength="16"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCodiceFiscale"
                    runat="server"
                    ControlToValidate="TextBoxCodiceFiscale"
                    ErrorMessage="Codice Fiscale mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:CustomValidator
                    ID="CustomValidatorCodiceFiscale"
                    runat="server"
                    ControlToValidate="TextBoxCodiceFiscale"
                    ErrorMessage="Codice fiscale non valido in base ai dati forniti" 
                    onservervalidate="CustomValidatorCodiceFiscale_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Posta elettronica certificata:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxPEC"
                    runat="server"
                    MaxLength="256"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator 
                    ID="RegularExpressionValidatorPEC" 
                    runat="server"
                    ControlToValidate="TextBoxPEC"
                    ErrorMessage="Posta Elettronica Certificata non valida" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Indirizzo<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxIndirizzo"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorIndirizzo"
                    runat="server"
                    ControlToValidate="TextBoxIndirizzo"
                    ErrorMessage="Indirizzo mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Provincia<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:DropDownList
                    ID="DropDownListProvincia"
                    AppendDataBoundItems="true"
                    runat="server"
                    Width="300px" AutoPostBack="True" 
                    onselectedindexchanged="DropDownListProvincia_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorProvincia"
                    runat="server"
                    ControlToValidate="DropDownListProvincia"
                    ErrorMessage="Selezionare la Provincia">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Comune<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:DropDownList
                    ID="DropDownListComune"
                    AppendDataBoundItems="true"
                    runat="server"
                    Width="300px">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorComune"
                    runat="server"
                    ControlToValidate="DropDownListComune"
                    ErrorMessage="Selezionare il Comune">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Cap<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxCap"
                    runat="server"
                    MaxLength="5"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCap"
                    runat="server"
                    ControlToValidate="TextBoxCap"
                    ErrorMessage="Cap mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="RegularExpressionValidatorFax"
                    runat="server"
                    ControlToValidate="TextBoxCap"
                    ErrorMessage="Cap errato"
                    ValidationExpression="^\d{5}$">
                *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Telefono<b>*</b>:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxTelefono"
                    runat="server"
                    MaxLength="15"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorTelefono"
                    runat="server"
                    ControlToValidate="TextBoxTelefono"
                    ErrorMessage="Telefono mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="primaColonna">
                Fax:
            </td>
            <td class="secondaColonna">
                <asp:TextBox
                    ID="TextBoxFax"
                    runat="server"
                    MaxLength="15"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:ValidationSummary
                    ID="ValidationSummaryRegistrazione"
                    runat="server"
                    CssClass="messaggiErrore" />
            </td>
        </tr>
    </table>
</asp:Panel>