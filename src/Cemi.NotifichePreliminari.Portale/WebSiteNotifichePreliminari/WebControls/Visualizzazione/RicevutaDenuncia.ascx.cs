using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;
using System.Web.Security;
using Cemi.NotifichePreliminari.PDFSign;
using System.Net;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class WebControls_Visualizzazione_RicevutaDenuncia : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Int32? idDenuncia = Context.Items["IdDenuncia"] as Int32?;
            byte[] bytes = null;
            Cantiere denuncia = null;

            if (idDenuncia.HasValue)
            {
                denuncia = biz.GetCantiere(idDenuncia.Value);

                ReportViewerRicevuta.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewerRicevuta.ServerReport.ReportPath = "/ReportNotifichePreliminari/ReportCompletoDenuncia";
                if (NotificheConfigReportServerCredentials.CredenzialiPresenti)
                {
                    ReportViewerRicevuta.ServerReport.ReportServerCredentials = new NotificheConfigReportServerCredentials();
                }

                ReportParameter[] listaParam = new ReportParameter[2];
                listaParam[0] = new ReportParameter("idDenuncia", idDenuncia.ToString());

                String cfImpresa = biz.GetUtenteNotifiche().AziendaCodiceFiscale;
                if (cfImpresa == null)
                {
                    cfImpresa = "-";
                }

                listaParam[1] = new ReportParameter("cfImpresa", cfImpresa);

                ReportViewerRicevuta.ServerReport.SetParameters(listaParam);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                //PDF
                bytes = ReportViewerRicevuta.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);


                // Invio email
                if (Context.Items["PrimoInserimento"] != null)
                {
                    MembershipUser utente = Membership.GetUser();

                    MailManager mailManager = new MailManager();
                    try
                    {
                        mailManager.InviaMailInserimentoDenuncia(denuncia, utente, bytes);
                    }
                    catch { }
                }
            }

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Denuncia {0}.pdf", denuncia.CodiceCantiere.Replace("/", "-")));
            Response.BinaryWrite(bytes);
                
            Response.Flush();
            Response.End();
        }
    }
}
