using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Entities;

public partial class WebControls_Visualizzazione_NotificaInAggiornamento : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaInformazioniNotifica(Notifica notifica)
    {
        LabelNotificaAggiornamentoProtocollo.Text = notifica.IdNotifica.ToString();
        LabelNotificaAggiornamentoNaturaOpera.Text = notifica.NaturaOpera;
    }
}
