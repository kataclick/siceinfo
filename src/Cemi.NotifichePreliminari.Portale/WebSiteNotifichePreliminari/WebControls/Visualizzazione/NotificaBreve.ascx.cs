﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.Presenter;

public partial class WebControls_Visualizzazione_NotificaBreve : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaNotifica(Int32 idNotifica)
    {
        NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica);

        LabelDatiGeneraliProtocollo.Text = notifica.IdNotifica.ToString();
        LabelDatiGeneraliData.Text = notifica.Data.ToString("dd/MM/yyyy");
        LabelDatiGeneraliNaturaOpera.Text = notifica.NaturaOpera;
        LabelDatiGeneraliNumeroAppalto.Text = notifica.NumeroAppalto;
        LabelDatiGeneraliTipologiaAppalto.Text = notifica.AppaltoPrivato ? "Privato" : "Pubblico";
        if (notifica.TipologiaLavoro != null)
        {
            LabelDatiGeneraliTipologiaLavoro.Text = notifica.TipologiaLavoro.ToString();
        }
        Presenter.CaricaElementiInListView(
            ListViewDatiGeneraliIndirizzi,
            notifica.Indirizzi);
    }
}