using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;

public partial class WebControls_Visualizzazione_Utente : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    public String Qualifica
    {
        get
        {
            return DropDownListQualifica.SelectedValue;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadDatePickerDataNascita.MaxDate = DateTime.Now.AddYears(-15);

        if (!Page.IsPostBack)
        {
            CaricaProvince();
            CaricaQualifiche();
            CaricaCasseEdili();

            AbilitaDatiAzienda(false);
        }
    }

    private void CaricaQualifiche()
    {
        if (DropDownListQualifica.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListQualifica,
                biz.GetQualifiche(),
                "Descrizione",
                "Id");
        }
    }

    private void CaricaCasseEdili()
    {
        if (DropDownListCassaEdile.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCassaEdile,
                biz.GetCasseEdili(true),
                "Descrizione",
                "IdCassaEdile");
        }
    }

    private void CaricaProvince()
    {
        if (DropDownListProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(DropDownListProvincia);
        }
    }

    public UtenteNotificheTelematiche GetUtente()
    {
        UtenteNotificheTelematiche utente = new UtenteNotificheTelematiche();

        utente.IdQualifica = Int32.Parse(DropDownListQualifica.SelectedValue);
        utente.IdCassaEdile = DropDownListCassaEdile.SelectedValue;
        utente.DescrizioneCassaEdile = DropDownListCassaEdile.SelectedItem.Text;
        utente.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        utente.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        utente.Sesso = RadioButtonListSesso.SelectedValue;
        utente.DataNascita = RadDatePickerDataNascita.SelectedDate.Value;
        utente.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        utente.Pec = TextBoxPEC.Text;
        utente.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        utente.Comune = DropDownListComune.SelectedValue;
        utente.Provincia = DropDownListProvincia.SelectedValue;
        utente.Cap = TextBoxCap.Text;
        utente.Telefono = TextBoxTelefono.Text;
        utente.Fax = TextBoxFax.Text;

        // Azienda
        if (utente.IdQualifica == 2)
        {
            utente.AziendaRagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxAziendaRagioneSociale.Text);
            utente.AziendaPartitaIVA = TextBoxAziendaPartitaIVA.Text;
            utente.AziendaCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxAziendaCodiceFiscale.Text);
        }

        return utente;
    }

    public void CaricaUtente(UtenteNotificheTelematiche utente)
    {
        CaricaQualifiche();
        CaricaCasseEdili();
        CaricaProvince();
        DisabilitaTutto();

        DropDownListQualifica.SelectedValue = utente.IdQualifica.ToString();
        DropDownListCassaEdile.SelectedValue = utente.IdCassaEdile;
        TextBoxCognome.Text = utente.Cognome;
        TextBoxNome.Text = utente.Nome;
        RadioButtonListSesso.SelectedValue = utente.Sesso;
        RadDatePickerDataNascita.SelectedDate = utente.DataNascita;
        TextBoxCodiceFiscale.Text = utente.CodiceFiscale;
        TextBoxPEC.Text = utente.Pec;
        TextBoxIndirizzo.Text = utente.Indirizzo;
        DropDownListProvincia.SelectedValue = utente.Provincia;
        Presenter.CaricaComuni(DropDownListComune, DropDownListProvincia.SelectedValue);
        DropDownListComune.SelectedValue = utente.Comune;
        TextBoxCap.Text = utente.Cap;
        TextBoxTelefono.Text = utente.Telefono;
        TextBoxFax.Text = utente.Fax;

        TextBoxAziendaRagioneSociale.Text = utente.AziendaRagioneSociale;
        TextBoxAziendaPartitaIVA.Text = utente.AziendaPartitaIVA;
        TextBoxAziendaCodiceFiscale.Text = utente.AziendaCodiceFiscale;
    }

    private void DisabilitaTutto()
    {
        DropDownListQualifica.Enabled = false;
        DropDownListCassaEdile.Enabled = false;
        TextBoxCognome.Enabled = false;
        TextBoxNome.Enabled = false;
        RadioButtonListSesso.Enabled = false;
        RadDatePickerDataNascita.Enabled = false;
        TextBoxCodiceFiscale.Enabled = false;
        TextBoxPEC.Enabled = false;
        TextBoxIndirizzo.Enabled = false;
        DropDownListProvincia.Enabled = false;
        DropDownListComune.Enabled = false;
        TextBoxCap.Enabled = false;
        TextBoxTelefono.Enabled = false;
        TextBoxFax.Enabled = false;

        //PanelAzienda.Enabled = false;
        AbilitaDatiAzienda(false);
    }

    #region Custom Validators
    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;
        
        String cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        String nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        String sesso = RadioButtonListSesso.SelectedValue;
        String codiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        
        if (RadDatePickerDataNascita.SelectedDate.HasValue)
        {
            DateTime dataNascita = RadDatePickerDataNascita.SelectedDate.Value;

            try
            {
                if (Presenter.VerificaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita, codiceFiscale))
                {
                    args.IsValid = true;
                }
            }
            catch{}
        }
    }

    protected void CustomValidatorDataNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadDatePickerDataNascita.SelectedDate.HasValue)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorAziendaRagioneSociale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListQualifica.SelectedValue == "2"
            && String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxAziendaRagioneSociale.Text)))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorTextBoxAziendaPartitaIVA_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListQualifica.SelectedValue == "2"
            && String.IsNullOrWhiteSpace(Presenter.NormalizzaCampoTesto(TextBoxAziendaPartitaIVA.Text)))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorTextBoxAziendaCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListQualifica.SelectedValue == "2"
            && String.IsNullOrWhiteSpace(Presenter.NormalizzaCampoTesto(TextBoxAziendaCodiceFiscale.Text)))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorTextBoxAziendaPartitaIVAFormato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListQualifica.SelectedValue == "2")
        {
            args.IsValid = false;

            try
            {
                if (Presenter.VerificaPartitaIVA(TextBoxAziendaPartitaIVA.Text))
                {
                    args.IsValid = true;
                }
            }
            catch
            {
            }
        }
    }
    #endregion

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        Presenter.CaricaComuni(DropDownListComune, DropDownListProvincia.SelectedValue);
    }

    protected void DropDownListQualifica_SelectedIndexChanged(object sender, EventArgs e)
    {
        AbilitaDatiAzienda(DropDownListQualifica.SelectedValue == "2");
    }

    private void AbilitaDatiAzienda(Boolean abilita)
    {
        //PanelAzienda.Enabled = abilita;
        TextBoxAziendaRagioneSociale.Enabled = abilita;
        TextBoxAziendaPartitaIVA.Enabled = abilita;
        TextBoxAziendaCodiceFiscale.Enabled = abilita;

        if (!abilita)
        {
            Presenter.SvuotaCampo(TextBoxAziendaRagioneSociale);
            Presenter.SvuotaCampo(TextBoxAziendaPartitaIVA);
            Presenter.SvuotaCampo(TextBoxAziendaCodiceFiscale);

            TextBoxAziendaRagioneSociale.CssClass = "campoDisabilitato";
            TextBoxAziendaPartitaIVA.CssClass = "campoDisabilitato";
            TextBoxAziendaCodiceFiscale.CssClass = "campoDisabilitato";
        }
        else
        {
            TextBoxAziendaRagioneSociale.CssClass = String.Empty;
            TextBoxAziendaPartitaIVA.CssClass = String.Empty;
            TextBoxAziendaCodiceFiscale.CssClass = String.Empty;
        }
    }
}
