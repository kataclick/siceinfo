﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificaBreve.ascx.cs" Inherits="WebControls_Visualizzazione_NotificaBreve" %>

<table width="100%">
    <tr>
        <td colspan="2">
            <b>
                Riepilogo della notifica
            </b>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            Protocollo:
        </td>
        <td>
            <b><asp:Label ID="LabelDatiGeneraliProtocollo" runat="server"></asp:Label></b>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            Data:
        </td>
        <td>
            <b><asp:Label ID="LabelDatiGeneraliData" runat="server"></asp:Label></b>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            Natura dell'opera:
        </td>
        <td>
            <b><asp:Label ID="LabelDatiGeneraliNaturaOpera" runat="server"></asp:Label></b>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            Tipologia Appalto:
        </td>
        <td>
            <b><asp:Label ID="LabelDatiGeneraliTipologiaAppalto" runat="server"></asp:Label></b>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            Tipologia Lavoro:
        </td>
        <td>
            <asp:Label ID="LabelDatiGeneraliTipologiaLavoro" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            CIG/DIA/Permesso costr.:
        </td>
        <td>
            <b><asp:Label ID="LabelDatiGeneraliNumeroAppalto" runat="server"></asp:Label></b>
        </td>
    </tr>
    <tr>
        <td class="styleIntestazione">
            Indirizzi:
        </td>
        <td>
            <b>
            <asp:ListView ID="ListViewDatiGeneraliIndirizzi" runat="server">
                    <LayoutTemplate>
                    <table>
                        <asp:Panel ID="itemPlaceholder" runat="server" />
                    </table>
                </LayoutTemplate> 
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# ((Cemi.NotifichePreliminari.Type.Entities.Indirizzo)Container.DataItem).IndirizzoCompleto %>
                        </td>
                    </tr>
                </ItemTemplate> 
            </asp:ListView>
            </b>
        </td>
    </tr>
</table>