﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicevutaDenuncia.ascx.cs" Inherits="WebControls_Visualizzazione_RicevutaDenuncia" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<rsweb:ReportViewer 
    ID="ReportViewerRicevuta"
    runat="server"
    Width="100%" 
    ShowFindControls="False" 
    ShowRefreshButton="False" 
    ShowZoomControl="False" ShowParameterPrompts="False">
</rsweb:ReportViewer>
