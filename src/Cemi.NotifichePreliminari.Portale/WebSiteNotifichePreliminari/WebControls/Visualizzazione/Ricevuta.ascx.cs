using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;
using System.Web.Security;
using Cemi.NotifichePreliminari.PDFSign;
using System.Net;

public partial class WebControls_Visualizzazione_Ricevuta : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Int32? idNotifica = Context.Items["IdNotifica"] as Int32?;
            byte[] fileFirmato = null;
            byte[] bytes = null;
            
            if (idNotifica.HasValue)
            {
                fileFirmato = biz.GetPdfFirmato(idNotifica.Value);

                if (fileFirmato == null)
                {
                    ReportViewerRicevuta.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                    ReportViewerRicevuta.ServerReport.ReportPath = "/ReportNotifichePreliminari/ReportCompleto";
                    if (NotificheConfigReportServerCredentials.CredenzialiPresenti)
                    {
                        ReportViewerRicevuta.ServerReport.ReportServerCredentials = new NotificheConfigReportServerCredentials();
                    }

                    ReportParameter[] listaParam = new ReportParameter[1];
                    listaParam[0] = new ReportParameter("idNotifica", idNotifica.ToString());

                    ReportViewerRicevuta.ServerReport.SetParameters(listaParam);

                    Warning[] warnings;
                    string[] streamids;
                    string mimeType;
                    string encoding;
                    string extension;

                    //PDF
                    bytes = ReportViewerRicevuta.ServerReport.Render(
                        "PDF", null, out mimeType, out encoding, out extension,
                        out streamids, out warnings);


                    // Invio email
                    if (Context.Items["PrimoInserimento"] != null)
                    {
                        byte[] certificato = null;
                        String password = null;

                        if (Context.Items["Certificato"] != null)
                        {
                            certificato = (byte[])Context.Items["Certificato"];
                        }
                        if (Context.Items["Password"] != null)
                        {
                            password = (String)Context.Items["Password"];
                        }

                        // Se � presente un certificato firmo il PDF
                        if (certificato != null)
                        {
                            fileFirmato = FirmaDocumentoPDF.FirmaDocumento(bytes, certificato, password);
                        }

                        NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica.Value);
                        MembershipUser utente = Membership.GetUser(notifica.IdUtenteTelematiche);

                        MailManager mailManager = new MailManager();
                        if (fileFirmato != null)
                        {
                            biz.UpdateNotificaPdfFirmato(idNotifica.Value, fileFirmato);

                            try
                            {
                                mailManager.InviaMailInserimentoNotifica(notifica.IdNotifica.Value, utente.Email, notifica.EntiDestinatari.IndirizziTo, notifica.EntiDestinatari.IndirizziCcn, fileFirmato);
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                mailManager.InviaMailInserimentoNotifica(notifica.IdNotifica.Value, utente.Email, notifica.EntiDestinatari.IndirizziTo, notifica.EntiDestinatari.IndirizziCcn, bytes);
                            }
                            catch { }
                        }
                    }
                }

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Notifica {0}.pdf", idNotifica));
                if (fileFirmato != null)
                {
                    Response.BinaryWrite(fileFirmato);
                }
                else
                {
                    Response.BinaryWrite(bytes);
                }

                Response.Flush();
                Response.End();
            }
        }
    }
}
