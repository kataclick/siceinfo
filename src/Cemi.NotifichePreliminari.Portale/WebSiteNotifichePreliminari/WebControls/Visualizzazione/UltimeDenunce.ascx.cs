﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

public partial class WebControls_Visualizzazione_UltimeDenunce : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaCantieri(String codiceFiscaleImpresa)
    {
        Presenter.CaricaElementiInGridView(
            GridViewUltimeDenunce,
            biz.GetUltimiCantieri(codiceFiscaleImpresa),
            0);
    }

    protected void GridViewUltimeDenunce_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 indice = -1;

        switch (e.CommandName)
        {
            case "dettaglio":
                indice = Int32.Parse(e.CommandArgument.ToString());
                Context.Items["IdCantiere"] = (Int32)GridViewUltimeDenunce.DataKeys[indice].Values["IdDenunciaCantiere"];
                Server.Transfer("~/Inserimento/InserimentoDenunciaCantiere.aspx");
                break;
        }
    }

    protected void GridViewUltimeDenunce_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Cantiere cantiere = (Cantiere) e.Row.DataItem;

            Label lIndirizzo = (Label) e.Row.FindControl("LabelIndirizzo");
            Label lDataInizioLavori = (Label) e.Row.FindControl("LabelDataInizioLavori");
            Label lCommittente = (Label) e.Row.FindControl("LabelCommittente");

            lIndirizzo.Text = cantiere.IndirizzoCantiere.IndirizzoCompleto;
            if (cantiere.DataInizioLavori.HasValue)
            {
                lDataInizioLavori.Text = cantiere.DataInizioLavori.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                lDataInizioLavori.Text = "--";
            }
            if (cantiere.Committente != null)
            {
                lCommittente.Text = cantiere.Committente.ToString();
            }
            else
            {
                lCommittente.Text = "--";
            }
        }
    }
}