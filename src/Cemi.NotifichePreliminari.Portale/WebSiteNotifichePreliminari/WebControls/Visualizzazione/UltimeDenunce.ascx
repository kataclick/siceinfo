﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UltimeDenunce.ascx.cs" Inherits="WebControls_Visualizzazione_UltimeDenunce" %>

<b>
    Ultime denunce
</b>
<br />
Le denunce mostrate sono le tre cronologicamente più recenti in cui è presente la sua impresa.
<asp:GridView
    ID="GridViewUltimeDenunce"
    runat="server"
    AutoGenerateColumns="False"
    Width="100%" CssClass="tabellaConBordo" DataKeyNames="IdDenunciaCantiere" 
    onrowcommand="GridViewUltimeDenunce_RowCommand" 
    onrowdatabound="GridViewUltimeDenunce_RowDataBound">
    <Columns>
        <asp:BoundField HeaderText="Data" DataField="Data" 
            DataFormatString="{0:dd/MM/yyyy}">
        <ItemStyle Width="80px" />
        </asp:BoundField>
        <asp:BoundField HeaderText="Codice" DataField="CodiceCantiere">
        <ItemStyle Width="100px" />
        </asp:BoundField>
        <asp:BoundField HeaderText="Ruolo" DataField="Ruolo">
        <ItemStyle Width="100px" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="Cantiere">
            <ItemTemplate>
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <b><asp:Label ID="LabelIndirizzo" runat="server"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 120px">
                            Data inizio lavori:
                        </td>
                        <td>
                            <b><asp:Label ID="LabelDataInizioLavori" runat="server"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 120px">
                            Committente:
                        </td>
                        <td>
                            <b><asp:Label ID="LabelCommittente" runat="server"></asp:Label></b>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton
                    ID="ImageButtonDettaglio"
                    runat="server" 
                    ImageUrl="~/Images/dettagliNotifica32.png"
                    CommandName="dettaglio"
                    CommandArgument="<%# Container.DataItemIndex %>"
                    ToolTip="Dettaglio" />
            </ItemTemplate>
            <ItemStyle Width="10px" />
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        <b>Non è presente nessuna denuncia legata alla sua impresa.</b>
    </EmptyDataTemplate>
</asp:GridView>