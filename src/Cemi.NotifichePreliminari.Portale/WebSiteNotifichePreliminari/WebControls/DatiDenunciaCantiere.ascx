﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiDenunciaCantiere.ascx.cs"
    Inherits="WebControls_DatiDenunciaCantiere" %>
<%@ Register TagPrefix="uc1" TagName="ListaIndirizzi" Src="~/WebControls/ListaIndirizzi.ascx" %>
<style type="text/css">
    .primaColonna
    {
        width: 330px;
    }
    .style1
    {
        width: 200px;
    }
</style>
<asp:Panel
    ID="PanelAbilitaModifica"
    runat="server">
<div class="borderedDiv">
    <br />
    <table width="100%">
        <tr>
            <td colspan="2">
                <b>Committente</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadAjaxLoadingPanel
                    ID="RadAjaxLoadingPanelCommittente"
                    runat="server"
                    Width="100%">
                </telerik:RadAjaxLoadingPanel>
                <telerik:RadAjaxPanel
                    ID="RadAjaxPanelCommittente"
                    runat="server"
                    Width="100%"
                    LoadingPanelID="RadAjaxLoadingPanelCommittente">
                    Tipologia:&nbsp; <telerik:RadComboBox
                        ID="RadComboBoxTipoCommittente"
                        runat="server"
                        Width="300px" AutoPostBack="True" 
                        onselectedindexchanged="RadComboBoxTipoCommittente_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Persona Fisica" Value="PRIVATO" />
                            <telerik:RadComboBoxItem runat="server" Text="Ente/Azienda/Stazione Appaltante" 
                                Value="AZIENDA" />
                        </Items>
                    </telerik:RadComboBox>
                    <asp:MultiView
                        ID="MultiViewCommittente"
                        runat="server"
                        ActiveViewIndex="0">
                        <asp:View
                            ID="ViewPrivato"
                            runat="server">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <b><small>Persona Fisica</small></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Cognome<b>*</b>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxCognome" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorCognome"
                                            runat="server"
                                            ControlToValidate="TextBoxCognome"
                                            ValidationGroup="datiCantiere"
                                            ErrorMessage="Indicare il cognome del committente">
                                            *
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Nome<b>*</b>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxNome" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorNome"
                                            runat="server"
                                            ControlToValidate="TextBoxNome"
                                            ValidationGroup="datiCantiere"
                                            ErrorMessage="Indicare il nome del committente">
                                            *
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Codice fiscale<b>*</b>:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxCodiceFiscale" MaxLength="16" runat="server" Width="250px"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorCodiceFiscale"
                                            runat="server"
                                            ControlToValidate="TextBoxCodiceFiscale"
                                            ValidationGroup="datiCantiere"
                                            ErrorMessage="Indicare il codice fiscale del committente">
                                            *
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionCodiceFiscale" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                                                ErrorMessage="Codice fiscale del committente non corretto" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}$"
                                                ValidationGroup="datiCantiere">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View
                            ID="ViewAzienda"
                            runat="server">
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <b><small>Ente/Azienda/Stazione Appaltante</small></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Pubblico/Privato<b>*</b>:
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButtonPrivato" Text="Privato" GroupName="PubblicoPrivato"
                                            runat="server" Checked="true" />
                                        <asp:RadioButton ID="RadioButtonPubblico" Text="Pubblico" GroupName="PubblicoPrivato"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Ragione Sociale:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="250px" MaxLength="255"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorRagioneSociale"
                                            runat="server"
                                            ControlToValidate="TextBoxRagioneSociale"
                                            ValidationGroup="datiCantiere"
                                            ErrorMessage="Indicare la ragione sociale del committente">
                                            *
                                        </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Codice fiscale:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxEnteCodiceFiscale" MaxLength="16" runat="server" Width="250px"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorEnteCodiceFiscale"
                                            runat="server"
                                            ControlToValidate="TextBoxEnteCodiceFiscale"
                                            ValidationGroup="datiCantiere"
                                            ErrorMessage="Indicare il codice fiscale del committente">
                                            *
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionEnteCodiceFiscale" runat="server" ControlToValidate="TextBoxEnteCodiceFiscale"
                                                ErrorMessage="Codice fiscale dell'ente del committente non corretto" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                                                ValidationGroup="datiCantiere">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Partita IVA:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxEntePartitaIVA" MaxLength="11" runat="server" Width="250px"></asp:TextBox>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorEntepartitaIva"
                                            runat="server"
                                            ControlToValidate="TextBoxEntePartitaIVA"
                                            ValidationGroup="datiCantiere"
                                            ErrorMessage="Indicare la partita IVA del committente">
                                            *
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionEntePartitaIva" runat="server" ControlToValidate="TextBoxEntePartitaIVA"
                                                ErrorMessage="Partita IVA dell'ente del committente non corretto" ValidationExpression="^\d{11}$"
                                                ValidationGroup="datiCantiere">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b><small>Indirizzo</small></b>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Via/Piazza<b>*</b>:
            </td>
            <td>
                <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorIndirizzo"
                    runat="server"
                    ControlToValidate="TextBoxIndirizzo"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare la via/piazza del committente">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Provincia<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox
                    ID="RadComboBoxProvincia"
                    runat="server"
                    AppendDataBoundItems="true"
                    Width="250px"
                    AutoPostBack="true" 
                    onselectedindexchanged="RadComboBoxProvincia_SelectedIndexChanged">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorProvincia"
                    runat="server"
                    ControlToValidate="RadComboBoxProvincia"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Selezionare la provincia del committente">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Comune<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox
                    ID="RadComboBoxComune"
                    runat="server"
                    AppendDataBoundItems="true"
                    Width="250px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorComune"
                    runat="server"
                    ControlToValidate="RadComboBoxComune"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Selezionare il comune del committente">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                CAP<b>*</b>:
            </td>
            <td>
                <asp:TextBox ID="TextBoxCap" runat="server" Width="250px" MaxLength="5"></asp:TextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCap"
                    runat="server"
                    ControlToValidate="TextBoxCap"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare il CAP del committente">
                    *
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="RegularExpressionValidatorCap"
                    runat="server"
                    ControlToValidate="TextBoxCap"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="CAP del committente non valido"
                    ValidationExpression="^\d{5}$">
                    *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
    </table>
</div>
<br />
<div class="borderedDiv">
    <table width="100%">
        <tr>
            <td colspan="2">
               <b>Dati Opera</b>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Natura dell'opera
                <br />
                (descr. dettagliata delle attività)<b>*</b>:
            </td>
            <td>
                <asp:TextBox ID="TextBoxNaturaOpera" runat="server" Width="250px" Height="70px" TextMode="MultiLine"
                    MaxLength="500">
                </asp:TextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorNaturaOpera"
                    runat="server"
                    ControlToValidate="TextBoxNaturaOpera"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare la natura dell'opera">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Tipologia del lavoro<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox
                    ID="RadComboBoxTipologiaLavoro"
                    runat="server"
                    EmptyMessage="Selezionare la tipologia del lavoro"
                    Width="250px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorTipologiaLavoro"
                    runat="server"
                    ControlToValidate="RadComboBoxTipologiaLavoro"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Selezionare la tipologia del lavoro">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                CIG/SCIA/Permesso costr.:
            </td>
            <td>
                <asp:TextBox ID="TextBoxPermessoCostruire" runat="server" Width="250px" MaxLength="50">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Ammontare complessivo<b>*</b>:
            </td>
            <td>
                <telerik:RadNumericTextBox ID="TextBoxAmmontareComplessivo" runat="server" Width="250px"></telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorAmmontareComplessivo"
                    runat="server"
                    ControlToValidate="TextBoxAmmontareComplessivo"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare l'ammontare complessivo dei lavori">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Ammontare Lavori Edili<b>*</b>:
            </td>
            <td>
                <telerik:RadNumericTextBox ID="TextBoxAmmontareLavoriEdili" runat="server" Width="250px"></telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorAmmontareLavoriEdili"
                    runat="server"
                    ControlToValidate="TextBoxAmmontareLavoriEdili"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare l'ammontare dei lavori edili">
                    *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Data inizio lavori<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataInizio" runat="server" MinDate="1900-01-01"
                    AutoPostBack="true" Width="250px">
                    <Calendar ID="CalendarDataInizio" RangeMinDate="1900-01-01" runat="server">
                    </Calendar>
                </telerik:RadDatePicker>
                <asp:CustomValidator
                    ID="CustomValidatorDataInizio"
                    runat="server"
                    ControlToValidate="RadDatePickerDataInizio"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare la data di inizio dei lavori" 
                    onservervalidate="CustomValidatorDataInizio_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Data fine lavori (presunta)<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataFine" runat="server" MinDate="1900-01-01"
                    AutoPostBack="true" Width="250px">
                    <Calendar ID="Calendar1" RangeMinDate="1900-01-01" runat="server">
                    </Calendar>
                </telerik:RadDatePicker>
                <asp:CustomValidator
                    ID="CustomValidatorDataFine"
                    runat="server"
                    ControlToValidate="RadDatePickerDataFine"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="Indicare la data presunta di fine lavori" 
                    onservervalidate="CustomValidatorDataFine_ServerValidate">
                    *
                </asp:CustomValidator>
                <asp:CustomValidator
                    ID="CustomValidatorDataInizioFine"
                    runat="server"
                    ValidationGroup="datiCantiere"
                    ErrorMessage="La data presunta di fine lavori deve essere successiva all'inizio">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                Note:
            </td>
            <td>
                <asp:TextBox ID="TextBoxNote" runat="server" Width="250px" Height="50px" 
                    TextMode="MultiLine" MaxLength="200"></asp:TextBox>
            </td>
        </tr>
    </table>
</div>
</asp:Panel>
