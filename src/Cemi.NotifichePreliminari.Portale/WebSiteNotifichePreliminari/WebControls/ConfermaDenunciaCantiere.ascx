﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfermaDenunciaCantiere.ascx.cs" Inherits="WebControls_ConfermaDenunciaCantiere" %>
<style type="text/css">
    .style1
    {
        width: 210px;
    }
</style>
<table width="100%">
        <tr>
            <td colspan="2">
                <b>Cantiere</b>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Indirizzo:
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Natura opera:
            </td>
            <td>
                <asp:Label ID="LabelNaturaOpera" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Committente:
            </td>
            <td>
                <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Capofila:
            </td>
            <td>
                <asp:Label ID="LabelCapofila" runat="server"></asp:Label>
            </td>
        </tr>     
        <tr>
            <td class="style1">
                Ammontare complessivo:
            </td>
            <td>
                <asp:Label ID="LabelAmmontare" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Ruolo</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Imprese</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ListView ID="ListViewImprese" runat="server">
                        <LayoutTemplate>
                        <ul>
                            <asp:Panel ID="itemPlaceholder" runat="server" />
                        </ul>
                    </LayoutTemplate> 
                    <ItemTemplate>
                        <li>
                             <b><%# ((Cemi.NotifichePreliminari.Portale.Type.Entities.Subappalto) Container.DataItem).ImpresaInSubappalto != null ? ((Cemi.NotifichePreliminari.Portale.Type.Entities.Subappalto) Container.DataItem).ImpresaInSubappalto.RagioneSociale : String.Empty %></b>
                             <br />
                             <small><%# ((Cemi.NotifichePreliminari.Portale.Type.Entities.Subappalto) Container.DataItem).ImpresaInSubappalto != null ? ((Cemi.NotifichePreliminari.Portale.Type.Entities.Subappalto) Container.DataItem).ImpresaInSubappalto.CodiceFiscale : String.Empty %></small>
                             <br />
                             <small><%# ((Cemi.NotifichePreliminari.Portale.Type.Entities.Subappalto) Container.DataItem).Ammontare.HasValue ? ((Cemi.NotifichePreliminari.Portale.Type.Entities.Subappalto) Container.DataItem).Ammontare.Value.ToString("C") : String.Empty %></small>
                        </li>
                    </ItemTemplate> 
                </asp:ListView>
            </td>
        </tr>
    </table>
