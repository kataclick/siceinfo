﻿using System;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;

public partial class WebControls_DatiDenunciaCantiere : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieLavoro();
            CaricaProvince();
        }
    }

    private void CaricaProvince()
    {
        if (RadComboBoxProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(RadComboBoxProvincia);
        }
    }

    private void CaricaTipologieLavoro()
    {
        if (RadComboBoxTipologiaLavoro.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipologiaLavoro,
                biz.GetTipologieLavoro(),
                "Descrizione",
                "Id");
        }
    }

    public void CaricaDatiCantiere(Cantiere cantiere)
    {
        if (cantiere != null)
        {
            if (cantiere.Committente != null)
            {
                CaricaProvince();

                //TextBoxNome.Text = cantiere.Committente.Nome;
                //TextBoxCognome.Text = cantiere.Committente.Cognome;
                ViewState["IdCommittente"] = cantiere.Committente.IdCommittente.Value;

                if (!String.IsNullOrEmpty(cantiere.Committente.PersonaCognome))
                {
                    RadComboBoxTipoCommittente.SelectedValue = "PRIVATO";
                    MultiViewCommittente.SetActiveView(ViewPrivato);
                    TextBoxCognome.Text = cantiere.Committente.PersonaCognome;
                    TextBoxNome.Text = cantiere.Committente.PersonaNome;
                    TextBoxCodiceFiscale.Text = cantiere.Committente.PersonaCodiceFiscale;
                }
                else
                {
                    RadComboBoxTipoCommittente.SelectedValue = "AZIENDA";
                    MultiViewCommittente.SetActiveView(ViewAzienda);
                    TextBoxRagioneSociale.Text = cantiere.Committente.EnteRagioneSociale;
                    TextBoxEnteCodiceFiscale.Text = cantiere.Committente.EnteCodiceFiscale;
                    TextBoxEntePartitaIVA.Text = cantiere.Committente.EntePartitaIva;

                    RadioButtonPubblico.Checked = cantiere.Committente.Pubblico;
                    RadioButtonPrivato.Checked = !cantiere.Committente.Pubblico;
                }

                TextBoxIndirizzo.Text = cantiere.Committente.Indirizzo;
                RadComboBoxProvincia.SelectedValue = cantiere.Committente.Provincia;
                CaricaComuni(cantiere.Committente.Provincia);
                RadComboBoxComune.SelectedValue = cantiere.Committente.Comune;
                TextBoxCap.Text = cantiere.Committente.Cap;
            }

            TextBoxNaturaOpera.Text = cantiere.NaturaOpera;
            TextBoxAmmontareComplessivo.Value = (double?) cantiere.AmmontareComplessivo;
            TextBoxAmmontareLavoriEdili.Value = (double?) cantiere.AmmontareEdile;
            RadDatePickerDataFine.SelectedDate = cantiere.DataFineLavori;
            RadDatePickerDataInizio.SelectedDate = cantiere.DataInizioLavori;
            TextBoxNote.Text = cantiere.Note;
            CaricaTipologieLavoro();
            if (cantiere.TipologiaLavoro != null)
            {
                RadComboBoxTipologiaLavoro.SelectedValue = cantiere.TipologiaLavoro.Id.ToString();
            }
            TextBoxPermessoCostruire.Text = cantiere.PermessoCostruire;

            //RadioButtonPubblico.Checked = cantiere.AppaltoPubblico;
            //RadioButtonPrivato.Checked = !cantiere.AppaltoPubblico;
        }
    }

    public void AbilitaModifica(Boolean abilita)
    {
        PanelAbilitaModifica.Enabled = abilita;
    }

    public void Reset()
    {
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxRagioneSociale);
        Presenter.SvuotaCampo(TextBoxEnteCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxEntePartitaIVA);
        Presenter.SvuotaCampo(TextBoxNaturaOpera);

        Presenter.SvuotaCampo(TextBoxIndirizzo);
        Presenter.SvuotaCampo(TextBoxCap);
        RadComboBoxProvincia.ClearSelection();
        RadComboBoxProvincia.Text = String.Empty;
        CaricaComuni(String.Empty);

        TextBoxAmmontareComplessivo.Value = null;
        TextBoxAmmontareLavoriEdili.Value = null;
        RadDatePickerDataFine.SelectedDate = null;
        RadDatePickerDataInizio.SelectedDate = null;
        RadComboBoxTipologiaLavoro.ClearSelection();
        RadComboBoxTipologiaLavoro.Text = String.Empty;
        Presenter.SvuotaCampo(TextBoxPermessoCostruire);
        Presenter.SvuotaCampo(TextBoxNote);

        RadioButtonPubblico.Checked = false;
        RadioButtonPrivato.Checked = true;
    }

    public void GetDatiCantiere(Cantiere cantiere)
    {
        if (cantiere != null)
        {
            if (!String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text)) || !String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCognome.Text)))
            {
                cantiere.Committente = new CommittenteDenuncieCantiere();
                if (ViewState["IdCommittente"] != null)
                {
                    cantiere.Committente.IdCommittente = (Int32) ViewState["IdCommittente"];
                }

                switch (RadComboBoxTipoCommittente.SelectedValue)
                {
                    case "PRIVATO":
                        cantiere.Committente.PersonaCognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
                        cantiere.Committente.PersonaNome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
                        cantiere.Committente.PersonaCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
                        break;
                    case "AZIENDA":
                        cantiere.AppaltoPubblico = RadioButtonPubblico.Checked;
                        cantiere.Committente.Pubblico = RadioButtonPubblico.Checked;
                        cantiere.Committente.EnteRagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
                        cantiere.Committente.EnteCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxEnteCodiceFiscale.Text);
                        cantiere.Committente.EntePartitaIva = Presenter.NormalizzaCampoTesto(TextBoxEntePartitaIVA.Text);
                        break;
                }

                cantiere.Committente.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
                cantiere.Committente.Provincia = RadComboBoxProvincia.SelectedValue;
                cantiere.Committente.Comune = RadComboBoxComune.SelectedValue;
                cantiere.Committente.Cap = TextBoxCap.Text;
            }
            cantiere.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
            cantiere.AmmontareComplessivo = Convert.ToDecimal(TextBoxAmmontareComplessivo.Value.Value);
            cantiere.AmmontareEdile = Convert.ToDecimal(TextBoxAmmontareLavoriEdili.Value.Value);
            cantiere.DataInizioLavori = RadDatePickerDataInizio.SelectedDate;
            cantiere.DataFineLavori = RadDatePickerDataFine.SelectedDate;
            cantiere.Note = Presenter.NormalizzaCampoTesto(TextBoxNote.Text);

            if (!String.IsNullOrEmpty(RadComboBoxTipologiaLavoro.SelectedValue))
            {
                cantiere.TipologiaLavoro = new TipologiaLavoro();
                cantiere.TipologiaLavoro.Id = Int32.Parse(RadComboBoxTipologiaLavoro.SelectedItem.Value);
                cantiere.TipologiaLavoro.Descrizione = RadComboBoxTipologiaLavoro.SelectedItem.Text;
            }
            cantiere.PermessoCostruire = Presenter.NormalizzaCampoTesto(TextBoxPermessoCostruire.Text);
        }
    }

    #region Custom validators
    protected void CustomValidatorDataInizio_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (!RadDatePickerDataInizio.SelectedDate.HasValue)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDataFine_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (!RadDatePickerDataFine.SelectedDate.HasValue)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDataInizioFine_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (RadDatePickerDataInizio.SelectedDate.HasValue
            && RadDatePickerDataFine.SelectedDate.HasValue)
        {
            if (RadDatePickerDataFine.SelectedDate.Value < RadDatePickerDataInizio.SelectedDate.Value)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorEnteCodiceFiscale_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (!String.IsNullOrWhiteSpace(TextBoxRagioneSociale.Text)
            && String.IsNullOrWhiteSpace(TextBoxEnteCodiceFiscale.Text))
        {
            args.IsValid = false;
        }
    }
    protected void RequiredFieldValidatorEnteCodiceFiscale_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        if (!String.IsNullOrWhiteSpace(TextBoxRagioneSociale.Text)
            && String.IsNullOrWhiteSpace(TextBoxEntePartitaIVA.Text))
        {
            args.IsValid = false;
        }
    }
    #endregion

    protected void RadComboBoxTipoCommittente_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        switch (RadComboBoxTipoCommittente.SelectedValue)
        {
            case "PRIVATO":
                ResetAzienda();
                MultiViewCommittente.SetActiveView(ViewPrivato);
                break;
            case "AZIENDA":
                ResetPrivato();
                MultiViewCommittente.SetActiveView(ViewAzienda);
                break;
        }
    }

    private void ResetAzienda()
    {
        Presenter.SvuotaCampo(TextBoxRagioneSociale);
        Presenter.SvuotaCampo(TextBoxEnteCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxEntePartitaIVA);
        RadioButtonPrivato.Checked = true;
        RadioButtonPubblico.Checked = false;
    }

    private void ResetPrivato()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
    }

    protected void RadComboBoxProvincia_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        CaricaComuni(RadComboBoxProvincia.SelectedValue);
    }

    private void CaricaComuni(String provincia)
    {
        Presenter.CaricaComuni(RadComboBoxComune, provincia);
    }
}