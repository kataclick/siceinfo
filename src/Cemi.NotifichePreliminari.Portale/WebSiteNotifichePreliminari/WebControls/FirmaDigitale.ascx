﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FirmaDigitale.ascx.cs" Inherits="WebControls_FirmaDigitale" %>

<table class="tabellaConBordo" width="100%">
    <colgroup>
        <col width="30%" />
        <col width="70%" />
    </colgroup>
    <tr>
        <td colspan="2">
            <b>
                Firma digitale</b></td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Certificato (deve essere disponibile sotto forma di file con estensione PFX):
        </td>
        <td colspan="2">
            <asp:FileUpload 
                ID="FileUploadCertificato" 
                runat="server"
                Width="300px" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <small>
                Se il certificato è installato sul vostro PC dall'archivio dei certificati dovete scegliere l'esportazione (includendo la chiave privata) nel formato Personal Information Exchange (.PFX). Una volta generato il file potete selezionarlo premendo il tasto "Sfoglia...".
            </small>
        </td>
    </tr>
    <tr>
        <td>
            Password (se il certificato è protetto): 
        </td>
        <td>
            <asp:TextBox
                ID="TextBoxPassword"
                runat="server" 
                MaxLength="50" 
                TextMode="Password"
                Width="210px"></asp:TextBox>
        </td>
    </tr>
</table>