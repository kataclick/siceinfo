﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CoordinatoreSicurezzaEsecuzione.ascx.cs" Inherits="WebControls_CoordinatoreSicurezzaEsecuzione" %>
<%@ Register src="Visualizzazione/Persona.ascx" tagname="Persona" tagprefix="uc1" %>
<div>
    <asp:CheckBox 
        ID="CheckBoxNonNominato" 
        Checked="false" 
        runat="server" 
        Text="Non nominato"
        AutoPostBack="True" 
        oncheckedchanged="CheckBoxNonNominato_CheckedChanged" />
</div>
<div>
    <asp:CheckBox ID="CheckBoxRealizzazioneStessiResponsabile" 
        Checked="false" runat="server" 
        Text="Stessi dati del Responsabile dei Lavori" GroupName="stessiDati" 
        AutoPostBack="True" 
        
        oncheckedchanged="CheckBoxRealizzazioneStessiResponsabile_CheckedChanged" />
</div>
<div>
    <asp:CheckBox ID="CheckBoxRealizzazioneStessiProgettazione" 
        Checked="false" runat="server" 
        Text="Stessi dati del Coordinatore per la Progettazione" 
        GroupName="stessiDati" AutoPostBack="True" 
        
        oncheckedchanged="CheckBoxRealizzazioneStessiProgettazione_CheckedChanged" />
</div>
<div>
    <asp:Panel ID="PanelCoordinatoreSicurezzaEsecuzione" Visible="true" runat="server">
        <uc1:Persona ID="PersonaCoordinatoreSicurezzaEsecuzione" runat="server" />
    </asp:Panel>
</div>