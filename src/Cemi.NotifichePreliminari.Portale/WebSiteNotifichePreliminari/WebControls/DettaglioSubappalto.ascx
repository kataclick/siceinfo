﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DettaglioSubappalto.ascx.cs"
    Inherits="WebControls_DettaglioSubappalto" %>
<div>
    <table width="100%">
        <tr>
            <td colspan="2">
                <b>Affidato da</b>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelAffidatoDa" runat="server" Text="Ragione sociale"></asp:Label><b>*</b>:
            </td>
            <td>
                <%--<asp:TextBox ID="TextBoxCodiceFiscaleAppaltatrice" runat="server" MaxLength="16" Width="200px"></asp:TextBox>--%>
                <telerik:RadAjaxPanel
                    ID="RadAjaxPanelAppaltatrice"
                    runat="server"
                    Width="100%">
                    <telerik:RadComboBox
                        ID="RadComboBoxCodiceFiscaleAppaltatrice"
                        runat="server"
                        Width="200px"
                        EmptyMessage="- Appaltatrice" AutoPostBack="True" 
                        ontextchanged="RadComboBoxCodiceFiscaleAppaltatrice_TextChanged">
                    </telerik:RadComboBox>
                    <asp:CustomValidator ID="RequiredFieldValidatorCodiceFiscaleAppaltatrice"
                        runat="server" ControlToValidate="RadComboBoxCodiceFiscaleAppaltatrice" 
                        ErrorMessage="Impresa affidataria mancante o "
                        ValidationGroup="DettaglioSubappaltoValidation" 
                        onservervalidate="RequiredFieldValidatorCodiceFiscaleAppaltatrice_ServerValidate">
                    *
                    </asp:CustomValidator>
                    <br />
                    <small>
                    <asp:Label
                        ID="LabelAppaltatrice"
                        runat="server">
                    </asp:Label>
                    </small>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Subappaltatrice</b>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCodiceFiscale" runat="server" Text="Codice fiscale"></asp:Label><b>*</b>:
            </td>
            <td>
                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                    ErrorMessage="Indicare il codice fiscale dell'impresa in subappalto" ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
                <asp:ImageButton
                    ID="ImageButtonCercaImpresa"
                    runat="server" ImageUrl="~/Images/dettagliNotifica16.png" 
                    onclick="ImageButtonCercaImpresa_Click"
                    CausesValidation="false"
                    ToolTip="Cerca l'impresa nell'anagrafica" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPartitaIva" runat="server" Text="Partita iva"></asp:Label><b>*</b>:
            </td>
            <td>
                <asp:TextBox ID="TextBoxPartitaIva" runat="server" MaxLength="11" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPartitaIva" runat="server" ControlToValidate="TextBoxPartitaIva"
                    ErrorMessage="Indicare la partita iva dell'impresa in subappalto" ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelRagioneSociale" runat="server" Text="Ragione sociale"></asp:Label><b>*</b>:
            </td>
            <td>
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorRagioneSociale" runat="server" ControlToValidate="TextBoxRagioneSociale"
                    ErrorMessage="Indicare la ragione sociale dell'impresa in subappalto" ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Dati opera</b>
            </td>
        </tr>
        <tr>
            <td>
                Tipologia opere<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox Height="120px" Width="200px" runat="server"
                    ID="ComboTipoOpera" EmptyMessage="Selezionare la tipologia delle opere" 
                    onload="ComboTipoOpera_Load">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoOpera" runat="server" ControlToValidate="ComboTipoOpera"
                    ErrorMessage="Indicare la tipologia delle opere eseguite" ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Ammontare opere<b>*</b>:
            </td>
            <td>
                <telerik:RadNumericTextBox ID="TextBoxAmmontare" runat="server" Width="200px">
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAmmontare" runat="server" ControlToValidate="TextBoxAmmontare"
                    ErrorMessage="Indicare l'ammontare delle opere" ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Data inizio lavori<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataInizio" runat="server" MinDate="1900-01-01"
                    AutoPostBack="true" Width="200px">
                    <Calendar ID="CalendarDataInizio" RangeMinDate="1900-01-01" runat="server">
                    </Calendar>
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataInizio" runat="server"
                    ControlToValidate="RadDatePickerDataInizio" ErrorMessage="Indicare la data di inizio lavori"
                    ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Data fine lavori (presunta)<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataFine" runat="server" MinDate="1900-01-01"
                    AutoPostBack="true" Width="200px">
                    <Calendar ID="CalendarDataFine" RangeMinDate="1900-01-01" runat="server">
                    </Calendar>
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataFine" runat="server" ControlToValidate="RadDatePickerDataFine"
                    ErrorMessage="Indicare la data presunta di fine lavori" ValidationGroup="DettaglioSubappaltoValidation">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Note:
            </td>
            <td>
                <asp:TextBox ID="TextBoxNote" runat="server" Width="200px" Height="50px" 
                    TextMode="MultiLine" MaxLength="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ValidationSummary
                    ID="ValidationSummarySubappalto"
                    runat="server"
                    ValidationGroup="DettaglioSubappaltoValidation"
                    CssClass="messaggiErrore" />
            </td>
            <td>
                <div style="text-align: right;">
                <asp:Button ID="btnUpdate" Text="Aggiorna" runat="server" CommandName="Update" Visible="false" ValidationGroup = "DettaglioSubappaltoValidation"></asp:Button>
                <asp:Button ID="btnInsert" Text="Inserisci" runat="server" 
                        CommandName="PerformInsert" >
                </asp:Button>
                <asp:Button ID="btnCancel" Text="Annulla" runat="server" CausesValidation="False"
                    CommandName="Cancel"></asp:Button>
                    </div>
            </td>
        </tr>
    </table>
</div>
