﻿using System;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Type.Enums;
using Cemi.Presenter;
using Cemi.NotifichePreliminari.Portale.Business;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Type.Collections;

public partial class WebControls_ConfermaDenunciaCantiere : System.Web.UI.UserControl
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaRiepilogoDenuncia(Cantiere cantiere)
    {
        if (cantiere != null)
        {
            if (cantiere.IndirizzoCantiere != null)
            {
                LabelIndirizzo.Text = cantiere.IndirizzoCantiere.IndirizzoCompleto;
                
                LabelNaturaOpera.Text = cantiere.NaturaOpera;
                
                if (cantiere.AmmontareComplessivo.HasValue)
                {
                    LabelAmmontare.Text = cantiere.AmmontareComplessivo.Value.ToString("C");
                }
                else
                {
                    LabelAmmontare.Text = String.Empty;
                }

                if (cantiere.Committente != null)
                {
                    LabelCommittente.Text = cantiere.Committente.ToString();
                }

                if (cantiere.Capofila != null)
                {
                    LabelCapofila.Text = cantiere.Capofila.RagioneSociale;
                }

                LabelRuolo.Text = cantiere.Ruolo.ToString();

                switch(cantiere.Ruolo)
                {
                    case RuoloImpresa.Capofila:
                        Presenter.CaricaElementiInListView(
                            ListViewImprese,
                            cantiere.Subappalti);
                        break;
                    case RuoloImpresa.Subappalto:
                        UtenteNotificheTelematiche utente = biz.GetUtenteNotifiche();
                        SubappaltoCollection tempSub = new SubappaltoCollection();
                        Subappalto sub = cantiere.Subappalti.GetSubappaltoPerCodiceFiscale(utente.AziendaCodiceFiscale);
                        if (sub != null)
                        {
                            tempSub.Add(sub);
                        }

                        Presenter.CaricaElementiInListView(
                            ListViewImprese,
                            tempSub);
                        break;
                }
            }
        }
    }
}