﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListaIndirizzi.ascx.cs" Inherits="WebControls_ListaIndirizzi" %>
<div>
    <div>
        <asp:CheckBoxList 
            ID="CheckBoxListIndirizzi" 
            runat="server" 
            Width="410px" 
            CssClass="tabellaConBordo">
        </asp:CheckBoxList>
        <asp:Label 
            ID="LabelNessunIndirizzo" 
            runat="server"
            CssClass="tabellaConBordo">
            Nessun indirizzo inserito
        </asp:Label>
    </div>
    <div>
        <asp:Button ID="ButtonEliminaSelezione" runat="server" 
            Text="Elimina indirizzi selezionati" Height="25px" Enabled="False" 
            Width="200px" onclick="ButtonEliminaSelezione_Click" />
        &nbsp;<asp:Button ID="ButtonNuovoIndirizzo" runat="server"
            Text="Aggiungi indirizzo" Height="25px"
            Width="200px" onclick="ButtonNuovoIndirizzo_Click" />
    </div>
</div>
<div>
    <asp:Panel ID="PanelNuovoIndirizzo" runat="server" Visible="false">
        <asp:MultiView ID="MultiViewIndirizzo" runat="server" ActiveViewIndex="0">
            <asp:View ID="ViewIndirizzoInserito" runat="server">
                <asp:Panel ID="PanelIndirizzoInserito" DefaultButton="ButtonGeocodifica" runat="server">
                    <table width="400px" class="tabellaConBordo">
                        <tr>
                            <td>
                                Indirizzo<b>*</b>
                            </td>
                            <td>
                                <asp:TextBox 
                                    ID="TextBoxIndirizzo" 
                                    runat="server" 
                                    Width="250px" 
                                    Height="50px" 
                                    MaxLength="245" 
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator 
                                    ID="RequiredFieldValidatorIndirizzo" 
                                    runat="server" 
                                    ErrorMessage="Indirizzo mancante" 
                                    ControlToValidate="TextBoxIndirizzo" 
                                    ValidationGroup="inserimentoIndirizzo">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Civico
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="TextBoxCivico" runat="server" Width="50px" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Info agg.
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="TextBoxInfoAggiuntiva" runat="server" Width="250px" 
                                    MaxLength="100" Height="50px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Comune
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="TextBoxComune" runat="server" Width="250px" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Provincia<b>*</b>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxProvincia" runat="server" Width="250px" MaxLength="2"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator 
                                    ID="RequiredFieldValidatorProvincia" 
                                    runat="server" 
                                    ErrorMessage="Provincia mancante" 
                                    ControlToValidate="TextBoxProvincia" 
                                    ValidationGroup="inserimentoIndirizzo">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cap
                            </td>
                            <td>
                                <asp:TextBox 
                                    ID="TextBoxCap" 
                                    runat="server" 
                                    Width="250px" 
                                    MaxLength="5">
                                </asp:TextBox>
                            </td>
                            <td>
                                <asp:RegularExpressionValidator 
                                    ID="RegularExpressionValidatorCap" 
                                    runat="server" 
                                    ErrorMessage="Formato del Cap errato" 
                                    ValidationExpression="^\d{5}$" 
                                    ControlToValidate="TextBoxCap">
                                    *
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:ValidationSummary
                                    ID="ValidationSummeryNuovoIndirizzo"
                                    runat="server"
                                    ValidationGroup="inserimentoIndirizzo"
                                    CssClass="messaggiErrore" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Button 
                                    ID="ButtonGeocodifica" 
                                    runat="server" 
                                    Text="Aggiungi" 
                                    onclick="ButtonGeocodifica_Click" 
                                    ValidationGroup="inserimentoIndirizzo" 
                                    Width="200px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:View>
            <asp:View ID="ViewIndirizzoProposto" runat="server">
                <table width="400px">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="LabelSceltaIndirizzo" runat="server" 
                                Text="Se l'indirizzo inserito è presente nella tabella sottostante cliccare su &quot;Aggiungi&quot;. Se nessuno degli indirizzi proposti corrisponde cliccare su &quot;Aggiungi indirizzo dichiarato&quot;"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Indirizzo fornito:
                        </td>
                        <td>
                            <asp:Label ID="LabelIndirizzoFornito" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" 
                                ShowHeader="False" 
                                OnSelectedIndexChanging="GridViewIndirizzi_SelectedIndexChanging" Width="100%">
                                <Columns>
                                    <asp:BoundField DataField="Provincia">
                                        <ItemStyle Font-Size="XX-Small" Width="20px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Comune">
                                        <ItemStyle Font-Size="XX-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IndirizzoDenominazione">
                                        <ItemStyle Font-Size="XX-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Cap">
                                        <ItemStyle Font-Size="XX-Small" Width="20px" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemStyle Width="20px" />
                                        <ItemTemplate>
                                            <asp:Button ID="ButtonSeleziona" runat="server" CommandName="Select" Font-Size="XX-Small"
                                                Height="18px" Text="Aggiungi" Width="56px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    Non è stato trovato un indirizzo corrispondente nell'anagrafica degli indirizzi. 
                                    Se l'indirizzo digitato è corretto cliccare su "Aggiungi indirizzo dichiarato".
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="ButtonAggiungiIndirizzo" runat="server" 
                                OnClick="ButtonAggiungiIndirizzo_Click" Text="Aggiungi indirizzo dichiarato" 
                                Width="200px" ValidationGroup="inserimentoIndirizzo" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </asp:Panel>
</div>


