﻿using System;
using System.Configuration;

namespace Cemi.NotifichePreliminari.Portale.Business
{
    public class Configurazione
    {
        #region Impostazioni Smtp
        public static String SmtpServer
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpServer"];
            }
        }

        public static Int32 SmtpPort
        {
            get
            {
                return Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]);
            }
        }

        public static String SmtpDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpDomain"];
            }
        }

        public static String SmtpUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpUsername"];
            }
        }

        public static String SmtpPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpPassword"];
            }
        }

        public static String SmtpAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpAddress"];
            }
        }

        public static String SmtpFriendlyName
        {
            get
            {
                return ConfigurationManager.AppSettings["SmtpFriendlyName"];
            }
        }
        #endregion

        #region Impostazioni Pec
        public static String PecServer
        {
            get
            {
                return ConfigurationManager.AppSettings["PecServer"];
            }
        }

        public static Int32 PecPort
        {
            get
            {
                return Int32.Parse(ConfigurationManager.AppSettings["PecPort"]);
            }
        }

        public static String PecDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["PecDomain"];
            }
        }

        public static String PecUsername
        {
            get
            {
                return ConfigurationManager.AppSettings["PecUsername"];
            }
        }

        public static String PecPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["PecPassword"];
            }
        }

        public static String PecAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["PecAddress"];
            }
        }

        public static String PecFriendlyName
        {
            get
            {
                return ConfigurationManager.AppSettings["PecFriendlyName"];
            }
        }

        public static String PercorsoFileTemporanei
        {
            get
            {
                return ConfigurationManager.AppSettings["PercorsoFileTemporanei"];
            }
        }
        #endregion

        #region Impostazioni Estrazione
        public static String EstrazioneCarattereSeparatore
        {
            get
            {
                if (ConfigurationManager.AppSettings["CarattereSeparatore"] != null)
                {
                    return ConfigurationManager.AppSettings["CarattereSeparatore"];
                }

                throw new Exception("CarattereSeparatore non è definito del file di configurazione");
            }
        }
        #endregion
    }
}
