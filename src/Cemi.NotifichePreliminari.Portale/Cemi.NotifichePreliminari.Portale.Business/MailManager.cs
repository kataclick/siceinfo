﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web.Security;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Type.Collections;

namespace Cemi.NotifichePreliminari.Portale.Business
{
    public class MailManager
    {
        #region Parametri per Pec
        private const string SMTP_SERVER = "http://schemas.microsoft.com/cdo/configuration/smtpserver";
        private const string SMTP_SERVER_PORT = "http://schemas.microsoft.com/cdo/configuration/smtpserverport";
        private const string SEND_USING = "http://schemas.microsoft.com/cdo/configuration/sendusing";
        private const string SMTP_USE_SSL = "http://schemas.microsoft.com/cdo/configuration/smtpusessl";
        private const string SMTP_AUTHENTICATE = "http://schemas.microsoft.com/cdo/configuration/smtpauthenticate";
        private const string SEND_USERNAME = "http://schemas.microsoft.com/cdo/configuration/sendusername";
        private const string SEND_PASSWORD = "http://schemas.microsoft.com/cdo/configuration/sendpassword";
        #endregion

        public void InviaMailRegistrazione(String userName, String password, String emailAddress, UtenteNotificheTelematiche utente)
        {
            MailMessage messaggio = new MailMessage();

            messaggio.IsBodyHtml = true;
            messaggio.Subject = "Sportello Unico Notifiche - Conferma Avvenuta Registrazione";
            messaggio.To.Add(emailAddress);
            messaggio.Body = String.Format("La sua richiesta è stata acquisita dal sistema. L'<b>attivazione</b> della sua utenza verrà comunicata tramite <b>posta elettronica</b>.<br /><br />I dati da lei scelti sono:<br /><b>Nome utente:</b> {0}<br />Password: {1}<br />", userName, password);

            // Invio messaggio al compilatore
            InviaMailPec(messaggio, null, null);

            messaggio.To.Clear();
            // L'email di conferma registrazione la invio in copia nascosta anche a tutti gli amministratori
            String[] utentiAmministratori = Roles.GetUsersInRole("Amministratore");
            foreach (String usTemp in utentiAmministratori)
            {
                MembershipUser user = Membership.GetUser(usTemp);
                messaggio.To.Add(user.Email);
            }

            messaggio.Subject = String.Format("{0} - Registrazione nuovo utente", utente.DescrizioneCassaEdile);
            messaggio.Body = String.Format("E' stata richiesta l'attivazione di una nuova utenza.<br /><br /><b>Nome utente:</b> {0}<br />Password: {1}", userName, password);

            // Invio messaggio agli amministratori
            InviaMailPec(messaggio, null, null);
        }

        public void InviaMailPasswordDimenticata(MembershipUser user)
        {
            MailMessage messaggio = new MailMessage();

            messaggio.IsBodyHtml = true;
            messaggio.Subject = "Sportello Unico Notifiche - Password dimenticata";
            messaggio.To.Add(user.Email);
            messaggio.Body = String.Format("A seguito della sua richiesta le ritrasmettiamo i dati per il login.<br /><br />I suoi dati sono:<br /><b>Nome utente:</b> {0}<br />Password: {1}<br />", user.UserName, user.GetPassword());

            // Invio messaggio al compilatore
            InviaMailPec(messaggio, null, null);
        }

        public void InviaMailAttivazioneDisattivazione(String userName, String emailAddress, Boolean attivato)
        {
            MailMessage messaggio = new MailMessage();

            messaggio.IsBodyHtml = true;
            messaggio.Subject = "Sportello Unico Notifiche - Modifica Stato Utente";
            messaggio.To.Add(emailAddress);
            messaggio.Body = String.Format("Il suo accesso allo Sportello Unico Notifiche (utente: <b>{0}</b>) è stato <b>{1}</b>.<br /><br />Per chiarimenti la preghiamo di contattare la sua Cassa Edile di riferimento.", userName, attivato ? "attivato" : "disattivato");

            InviaMailPec(messaggio, null, null);
        }

        public void InviaMailInserimentoNotifica(Int32 idNotifica, String emailAddress, List<String> entiTo, List<String> entiCcn, Byte[] notificaPDF)
        {
            //enti.Add(emailAddress);

            using (MemoryStream s = new MemoryStream(notificaPDF))
            {
                String nomeAllegato = String.Format("Notifica{0}.pdf", idNotifica);

                MailMessage messaggio = new MailMessage();

                messaggio.IsBodyHtml = true;
                messaggio.Subject = "Sportello Unico Notifiche - Ricevuta Notifica Preliminare / Aggiornamento";
                foreach (String ind in entiTo)
                {
                    messaggio.To.Add(ind);
                }
                messaggio.CC.Add(emailAddress);
                messaggio.Body = String.Format("Lo Sportello Unico Notifiche ha <b>ricevuto</b> una nuova Notifica Preliminare / Aggiornamento. Il protocollo assegnato è <b>{0}</b>.<br /><br />In <b>allegato</b> trova il <b>documento</b> della notifica.", idNotifica);

                messaggio.Attachments.Add(new Attachment(s, nomeAllegato));
                InviaMailPec(messaggio, notificaPDF, nomeAllegato);

                foreach (String ind in entiCcn)
                {
                    messaggio.CC.Clear();
                    messaggio.To.Clear();
                    messaggio.To.Add(ind);

                    InviaMailPec(messaggio, notificaPDF, nomeAllegato);
                }
            }
        }

        public void InviaMailInserimentoDenuncia(Type.Entities.Cantiere denuncia, MembershipUser utente, byte[] denunciaPDF)
        {
            using (MemoryStream s = new MemoryStream(denunciaPDF))
            {
                String nomeAllegato = String.Format("Denuncia {0}.pdf", denuncia.CodiceCantiere.Replace('/', '-'));

                MailMessage messaggio = new MailMessage();

                messaggio.IsBodyHtml = true;
                messaggio.Subject = "Sportello Unico Notifiche - Ricevuta Denuncia";
                messaggio.To.Add(utente.Email);

                // Mando copia della denuncia alla Cassa Edile territorialmente competente
                String provincia = denuncia.IndirizzoCantiere.Provincia;
                NotifichePreliminariManager biz = new NotifichePreliminariManager();
                EnteDestinatarioCollection enti = biz.GetEntiDestinatari(5, provincia);
                if (enti != null && enti.Count == 1)
                {
                    messaggio.CC.Add(enti[0].EmailTo);
                }

                messaggio.Body = String.Format("Lo Sportello Unico Notifiche ha <b>ricevuto</b> una nuova denuncia di cantiere. Il protocollo assegnato è <b>{0}</b>.<br /><br />In <b>allegato</b> trova il <b>documento</b> della denuncia.", denuncia.CodiceCantiere);

                messaggio.Attachments.Add(new Attachment(s, nomeAllegato));
                InviaMailPec(messaggio, denunciaPDF, nomeAllegato);
            }
        }

        public void InviaMail(MailMessage messaggio)
        {
            messaggio.From = new MailAddress(Configurazione.SmtpAddress, Configurazione.SmtpFriendlyName);

            using (SmtpClient smtpClient = new SmtpClient(Configurazione.SmtpServer, Configurazione.SmtpPort))
            {
                if (!String.IsNullOrEmpty(Configurazione.SmtpUsername))
                {
                    smtpClient.Credentials = new System.Net.NetworkCredential(Configurazione.SmtpUsername, Configurazione.SmtpPassword, Configurazione.SmtpDomain);
                }

                smtpClient.Send(messaggio);
            }
        }

        public void InviaMailPec(MailMessage messaggio, byte[] allegato, String nomeAllegato)
        {
            // Conversione da System.Net.Mail.Message a System.Web.Mail.MailMessage
            System.Web.Mail.MailMessage messaggioConvertito = ConvertiMail(messaggio, allegato, nomeAllegato);
            System.Web.Mail.SmtpMail.SmtpServer = String.Format("{0}{1}", Configurazione.PecServer, Configurazione.PecPort);
            System.Web.Mail.SmtpMail.Send(messaggioConvertito);
        }

        private System.Web.Mail.MailMessage ConvertiMail(MailMessage messaggio, byte[] allegato, String nomeAllegato)
        {
            System.Web.Mail.MailMessage mailPec = new System.Web.Mail.MailMessage();
            mailPec.Subject = messaggio.Subject;
            mailPec.Body = messaggio.Body;
            if (messaggio.IsBodyHtml)
            {
                mailPec.BodyFormat = System.Web.Mail.MailFormat.Html;
            }
            else
            {
                mailPec.BodyFormat = System.Web.Mail.MailFormat.Text;
            }

            // Mittente
            mailPec.From = Configurazione.PecAddress;

            // Destinatari
            StringBuilder to = new StringBuilder();
            foreach (MailAddress address in messaggio.To)
            {
                to.Append(String.Format("{0};", address.Address));
            }
            mailPec.To = to.ToString();

            // Destinatari in copia
            StringBuilder cc = new StringBuilder();
            foreach (MailAddress address in messaggio.CC)
            {
                cc.Append(String.Format("{0};", address.Address));
            }
            mailPec.Cc = cc.ToString();

            // Destinatari in copia nascosta
            StringBuilder bcc = new StringBuilder();
            foreach (MailAddress address in messaggio.Bcc)
            {
                bcc.Append(String.Format("{0};", address.Address));
            }
            mailPec.Bcc = bcc.ToString();

            // Allegati
            //foreach (Attachment attachment in messaggio.Attachments)
            //{
            //    String fileName = Path.Combine(Configurazione.PercorsoFileTemporanei, attachment.Name);

            //    using (FileStream att = File.Create(fileName))
            //    {
            //        byte[] file = new byte[attachment.ContentStream.Length];
            //        attachment.ContentStream.Read(file, 0, (Int32)attachment.ContentStream.Length);

            //        att.Write(file, 0, file.Length);
            //        att.Close();
            //    }

            //    System.Web.Mail.MailAttachment pecAttachment = new System.Web.Mail.MailAttachment(fileName);
            //    mailPec.Attachments.Add(pecAttachment);
            //}
            if (allegato != null)
            {
                String fileName = Path.Combine(Configurazione.PercorsoFileTemporanei, nomeAllegato);

                using (FileStream att = File.Create(fileName))
                {
                    att.Write(allegato, 0, allegato.Length);
                    att.Close();
                }

                System.Web.Mail.MailAttachment pecAttachment = new System.Web.Mail.MailAttachment(fileName);
                mailPec.Attachments.Add(pecAttachment);
            }

            mailPec.Fields[SMTP_SERVER] = Configurazione.PecServer;
            mailPec.Fields[SMTP_SERVER_PORT] = Configurazione.PecPort;
            mailPec.Fields[SEND_USING] = 2;
            mailPec.Fields[SMTP_USE_SSL] = true;
            mailPec.Fields[SMTP_AUTHENTICATE] = 1;
            mailPec.Fields[SEND_USERNAME] = Configurazione.PecUsername;
            mailPec.Fields[SEND_PASSWORD] = Configurazione.PecPassword;

            return mailPec;
        }
    }
}
