﻿using System;
using System.Configuration;
using System.Net;
using System.Security.Principal;
using Microsoft.Reporting.WebForms;

namespace Cemi.NotifichePreliminari.Portale.Business
{
    [Serializable]
    public class NotificheConfigReportServerCredentials : IReportServerCredentials
    {
        public static Boolean CredenzialiPresenti
        {
            get
            {
                return !String.IsNullOrEmpty(ConfigurationManager.AppSettings["ReportingUsername"]);
            }
        }

        public NotificheConfigReportServerCredentials()
        {
        }

        public WindowsIdentity ImpersonationUser
        {
            get { return null; }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                return new NetworkCredential(
                    ConfigurationManager.AppSettings["ReportingUsername"],
                    ConfigurationManager.AppSettings["ReportingPassword"],
                    ConfigurationManager.AppSettings["ReportingDomain"]);
            }
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = null;
            password = null;
            authority = null;

            return false;
        }
    }
}
