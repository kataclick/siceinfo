using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.Security;
using System.Xml;
using System.Xml.Serialization;
using Cemi.Geocode.Business;
using Cemi.NotifichePreliminari.Portale.Data;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Portale.Type.Filters;
using Cemi.NotifichePreliminari.Type.Collections;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Type.Enums;
using Cemi.NotifichePreliminari.Type.Filters;

namespace Cemi.NotifichePreliminari.Portale.Business
{
    public class NotifichePreliminariManager
    {
        private const string URLFILEEXCEL = "~/images/excel.gif";
        private const string URLFILEJPG = "~/images/jpg.gif";
        private const string URLFILEPDF = "~/images/pdf.gif";
        private const string URLFILESCONOSCIUTO = "~/images/sconosciuto.gif";
        private const string URLFILETXT = "~/images/txt.gif";
        private const string URLFILEWORD = "~/images/word.gif";

        private readonly NotifichePreliminariDataProvider dataAccess = new NotifichePreliminariDataProvider();

        public decimal GetLimiteImporto()
        {
            try
            {
                return dataAccess.GetLimiteImporto();
            }
            catch
            {
            }

            return 0;
        }

        public Boolean InserisciNotifica(NotificaTelematica notifica)
        {
            return dataAccess.InserisciNotifica(notifica);
        }

        public NotificaCollection RicercaNotificheTelematichePerAggiornamento(
            NotificaFilter filtro, Boolean cartacea)
        {
            return dataAccess.RicercaNotifichePerAggiornamento(filtro, cartacea);
        }

        public NotificaTelematica GetNotificaTelematica(int idNotifica)
        {
            return dataAccess.GetNotificaTelematica(idNotifica);
        }

        public NotificaTelematica GetNotificaTelematicaUltimaVersione(int idNotificaPadre)
        {
            return dataAccess.GetNotificaTelematicaUltimaVersione(idNotificaPadre);
        }

        public NotificaCollection RicercaNotifiche(NotificaFilter filtro)
        {
            return dataAccess.RicercaNotifiche(filtro);
        }

        /// <summary>
        /// Ricerca i cantieri legati ad una notifica CPT ordinando per committente
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public CantiereNotificaCollection RicercaCantieriPerCommittente(NotificaFilter filtro)
        {
            return dataAccess.RicercaCantieriPerCommittente(filtro);
        }

        /// <summary>
        /// Ricerca i cantieri legati a notifiche CPT ordinando per impresa
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public CantiereNotificaCollection RicercaCantieriPerImpresa(NotificaFilter filtro)
        {
            return dataAccess.RicercaCantieriPerImpresa(filtro);
        }

        public bool AnnullaNotifica(int idNotifica, string utente)
        {
            bool res = false;

            try
            {
                res = dataAccess.AnnullaNotifica(idNotifica, utente);
            }
            catch
            {
            }

            return res;
        }

        public List<int> GetIdNotificheCorrelate(int idNotifica)
        {
            List<int> notificheCorrelate = null;

            try
            {
                notificheCorrelate = dataAccess.GetIdNotificheCorrelate(idNotifica);
            }
            catch
            {
            }

            return notificheCorrelate;
        }

        public List<string> GetTipologieAttivita()
        {
            List<string> tipologieAttivita = null;

            try
            {
                tipologieAttivita = dataAccess.GetTipologieAttivita();
            }
            catch
            {
            }

            return tipologieAttivita;
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIVA, string codiceFiscale)
        {
            bool[] res = null;

            try
            {
                res = dataAccess.EsisteIvaFiscImpresa(partitaIVA, codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

        public bool[] EsisteIvaFiscCommittente(string partitaIVA, string codiceFiscale)
        {
            bool[] res = null;

            try
            {
                res = dataAccess.EsisteIvaFiscCommittente(partitaIVA, codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

        public string GetAllegatoImageUrl(string nomeFile)
        {
            string estensione = Path.GetExtension(nomeFile);
            estensione = estensione.ToUpper();

            switch (estensione)
            {
                case ".DOC":
                case ".DOCX":
                    return URLFILEWORD;
                case ".PDF":
                    return URLFILEPDF;
                case ".JPG":
                    return URLFILEJPG;
                case ".XLS":
                case ".XLSX":
                    return URLFILEEXCEL;
                case ".TXT":
                    return URLFILETXT;
                default:
                    return URLFILESCONOSCIUTO;
            }
        }

        public AreaCollection GetAree()
        {
            return dataAccess.GetAree();
        }

        public Boolean InsertUtenteTelematiche(UtenteNotificheTelematiche utente)
        {
            return dataAccess.InsertUtenteTelematiche(utente);
        }

        public UtenteNotificheTelematiche GetUtenteTelematiche(Guid userID)
        {
            return dataAccess.GetUtenteTelematiche(userID);
        }

        public CommittenteNotificheTelematiche GetCommittentiTelematicheDaAnagrafica(String ivaFisc)
        {
            return dataAccess.GetCommittentiTelematicheDaAnagrafica(ivaFisc);
        }

        public ImpresaNotificheTelematiche GetImpreseTelematicheDaSiceNewEAnagrafica(String ivaFisc)
        {
            return dataAccess.GetImpreseTelematicheDaSiceNewEAnagrafica(ivaFisc);
        }

        public Boolean UpdateCommittenteTelematiche(CommittenteNotificheTelematiche committente)
        {
            return dataAccess.UpdateCommittenteTelematiche(committente);
        }

        public Boolean InsertNotificaTemporanea(Guid userId, NotificaTelematica notifica)
        {
            return dataAccess.InsertNotificaTemporanea(userId, notifica);
        }

        public Boolean UpdateNotificaTemporanea(NotificaTelematica notifica)
        {
            return dataAccess.UpdateNotificaTemporanea(notifica);
        }

        public NotificaTelematicaCollection GetNotificheTemporanee(Guid userId)
        {
            return dataAccess.GetNotificheTemporanee(userId);
        }

        public NotificaTelematica GetNotificaTemporanea(Int32 idNotificaTemporanea)
        {
            return dataAccess.GetNotificaTemporanea(idNotificaTemporanea);
        }

        public Boolean DeleteNotificaTemporanea(Int32 idNotificaTemporanea)
        {
            return dataAccess.DeleteNotificaTemporanea(idNotificaTemporanea, null);
        }

        public Boolean IsNotificaTelematica(Int32 idNotifica)
        {
            return dataAccess.IsNotificaTelematica(idNotifica);
        }

        public TipologiaCommittenteCollection GetTipologieCommittente()
        {
            return dataAccess.GetTipologieCommittente();
        }

        #region Log ricerche

        public string GetPropertiesDelFiltro(object filtro)
        {
            StringBuilder res = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(res);

            XmlSerializer x = new XmlSerializer(filtro.GetType());
            x.Serialize(xmlWriter, filtro);

            return res.ToString();
        }

        public void InsertLogRicerca(LogRicerca log)
        {
            dataAccess.InsertLogRicerca(log);
        }

        #endregion

        public Int32? GetIdImpresaDaCodiceFiscale(String codiceFiscale)
        {
            return dataAccess.GetIdImpresaDaCodiceFiscale(codiceFiscale);
        }

        public Boolean EsisteNotificaRegione(String protocolloRegione)
        {
            return dataAccess.EsisteNotificaRegione(protocolloRegione);
        }

        public Boolean VerificaDataSmallDateTime(DateTime data)
        {
            DateTime limiteInferiore = new DateTime(1900, 1, 1);
            DateTime limiteSuperiore = new DateTime(2079, 6, 6);

            if (limiteInferiore <= data && data <= limiteSuperiore)
            {
                return true;
            }

            return false;
        }

        public void CompletaNotificaTelematicaConRiferimenti(NotificaTelematica notifica)
        {
            dataAccess.CompletaNotificaTelematicaConRiferimenti(notifica);
        }

        public Boolean UpdateNotifica(NotificaTelematica notificaPreliminare)
        {
            CompletaNotificaTelematicaConRiferimenti(notificaPreliminare);

            if (notificaPreliminare.IdNotifica.HasValue)
            {
                return dataAccess.UpdateNotifica(notificaPreliminare);
            }
            else
            {
                throw new Exception("UpdateNotifica: notifica non trovata");
            }
        }

        public Boolean NotificaRegioneDaAggiornare(String protocolloRegione, DateTime data)
        {
            return dataAccess.NotificaRegioneDaAggiornare(protocolloRegione, data);
        }

        public List<String> GetProtocolliRegioneCaricati()
        {
            return dataAccess.GetProtocolliRegioneCaricati();
        }

        public IndirizzoCollection GetCantieriGenerati(Int32 idNotificaRiferimento)
        {
            return dataAccess.GetCantieriGenerati(idNotificaRiferimento);
        }

        public CassaEdileCollection GetCasseEdili(Boolean? partecipanti)
        {
            return dataAccess.GetCasseEdili(partecipanti);
        }

        public IndirizzoCollection GeoCodeGoogleMultiplo(string address)
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            List<Geocode.Type.Indirizzo> indirizziGeocodificati = GeocodeProvider.Geocode(address);

            if (indirizziGeocodificati != null)
            {
                foreach (Geocode.Type.Indirizzo indirizzo in indirizziGeocodificati)
                {
                    indirizzi.Add(new Indirizzo(indirizzo.Via, indirizzo.Civico, indirizzo.Comune,
                                                                  indirizzo.Provincia, indirizzo.Cap,
                                                                  indirizzo.Latitudine, indirizzo.Longitudine));
                }
            }

            return indirizzi;
        }

        public Boolean VerificaUtenteGiaPresente(String codiceFiscale)
        {
            return dataAccess.VerificaUtenteGiaPresente(codiceFiscale);
        }

        public QualificaCollection GetQualifiche()
        {
            return dataAccess.GetQualifiche();
        }

        public void UpdateFiltri(Guid userGuid, String provincia, String provinciaSuggerita)
        {
            dataAccess.UpdateFiltri(userGuid, provincia, provinciaSuggerita);
        }

        public TipologiaLavoroCollection GetTipologieLavoro()
        {
            return dataAccess.GetTipologieLavoro();
        }

        public EnteDestinatarioTipologiaCollection GetEntiDestinatariTipologie(String provincia)
        {
            return dataAccess.GetEntiDestinatariTipologie(provincia);
        }

        public StringCollection GetEntiDestinatariProvince()
        {
            return dataAccess.GetEntiDestinatariProvince();
        }

        public EnteDestinatario GetEnteDestinatario(Int32 id)
        {
            return dataAccess.GetEnteDestinatario(id);
        }

        public EnteDestinatarioCollection GetEntiDestinatari(Int32? idTipologia, String provincia)
        {
            return dataAccess.GetEntiDestinatari(idTipologia, provincia);
        }

        public Boolean UpdateNotificaPdfFirmato(Int32 idNotifica, byte[] fileFirmato)
        {
            return dataAccess.UpdateNotificaPdfFirmato(idNotifica, fileFirmato);
        }

        public byte[] GetPdfFirmato(Int32 idNotifica)
        {
            return dataAccess.GetPdfFirmato(idNotifica);
        }

        public List<Int32> GetIdNotificheValide()
        {
            return dataAccess.GetIdNotificheValide();
        }

        public List<Int32> GetIdDenunceValide()
        {
            return dataAccess.GetIdDenunceValide();
        }

        public AllegatoCollection GetAllegati(int idVisita)
        {
            return dataAccess.GetAllegati(idVisita);
        }

        public bool InsertAllegato(Allegato allegato)
        {
            return dataAccess.InsertAllegato(allegato);
        }

        public Allegato GetAllegato(int idAllegato)
        {
            return dataAccess.GetAllegato(idAllegato);
        }

        public bool DeleteVisitaAllegato(int idVisita, int idAllegato)
        {
            return dataAccess.DeleteVisitaAllegato(idVisita, idAllegato);
        }

        public VisitaCollection GetVisite(int idNotificaRiferimento)
        {
            return dataAccess.GetVisite(idNotificaRiferimento);
        }

        public bool DeleteVisita(int idVisita)
        {
            return dataAccess.DeleteVisita(idVisita);
        }

        public TipologiaVisitaCollection GetTipologieVisita()
        {
            return dataAccess.GetTipologieVisita();
        }

        public EsitoVisitaCollection GetEsitiVisita()
        {
            return dataAccess.GetEsitiVisita();
        }

        public GradoIrregolaritaCollection GetGradiIrregolarita()
        {
            return dataAccess.GetGradiIrregolarita();
        }

        public bool InsertVisita(Visita visita)
        {
            return dataAccess.InsertVisita(visita);
        }

        public Visita GetVisita(int idVisita)
        {
            return dataAccess.GetVisita(idVisita);
        }

        public bool UpdateVisita(Visita visita)
        {
            return dataAccess.UpdateVisita(visita);
        }

        public Boolean UtentePuoVisualizzareVisite()
        {
            return Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteASL") || Roles.IsUserInRole("ContributoVisiteASL")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteASL") || Roles.IsUserInRole("ContributoVisiteASL")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteCPT") || Roles.IsUserInRole("ContributoVisiteCPT")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteCPT") || Roles.IsUserInRole("ContributoVisiteCPT")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteDTL") || Roles.IsUserInRole("ContributoVisiteDTL")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteDTL") || Roles.IsUserInRole("ContributoVisiteDTL")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINAIL") || Roles.IsUserInRole("ContributoVisiteINAIL")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINAIL") || Roles.IsUserInRole("ContributoVisiteINAIL")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINPS") || Roles.IsUserInRole("ContributoVisiteINPS")
                || Roles.IsUserInRole("VisualizzazioneVisiteTutte") || Roles.IsUserInRole("VisualizzazioneVisiteINPS") || Roles.IsUserInRole("ContributoVisiteINPS")
                || Roles.IsUserInRole("VisualizzazioneVisiteCassaEdile") || Roles.IsUserInRole("ContributoVisiteCassaEdile")
                || Roles.IsUserInRole("VisualizzazioneVisiteCassaEdile") || Roles.IsUserInRole("ContributoVisiteCassaEdile");
        }

        public Boolean UtentePuoInserireVisita()
        {
            return Roles.IsUserInRole("ContributoVisiteASL")
                    || Roles.IsUserInRole("ContributoVisiteASL")
                    || Roles.IsUserInRole("ContributoVisiteCPT")
                    || Roles.IsUserInRole("ContributoVisiteCPT")
                    || Roles.IsUserInRole("ContributoVisiteDTL")
                    || Roles.IsUserInRole("ContributoVisiteDTL")
                    || Roles.IsUserInRole("ContributoVisiteINAIL")
                    || Roles.IsUserInRole("ContributoVisiteINAIL")
                    || Roles.IsUserInRole("ContributoVisiteINPS")
                    || Roles.IsUserInRole("ContributoVisiteINPS")
                    || Roles.IsUserInRole("ContributoVisiteCassaEdile")
                    || Roles.IsUserInRole("ContributoVisiteCassaEdile");
        }

        public void InsertFiltroRicercaNotifiche(Guid userID, NotificaFilter filtro, Int32 pagina)
        {
            dataAccess.InsertFiltroRicercaNotifiche(userID, filtro, pagina, null, null, null);
        }

        public void InsertFiltroRicercaMappa(Guid userID, NotificaFilter filtro)
        {
            dataAccess.InsertFiltroRicercaNotifiche(userID, null, null, filtro, null, null);
        }

        public void InsertFiltroRicercaDenunce(Guid userID, NotificaFilter filtro, int pagina)
        {
            dataAccess.InsertFiltroRicercaNotifiche(userID, null, null, null, filtro, pagina);
        }

        public NotificaFilter GetFiltroRicercaNotifiche(Guid userID, out Int32 pagina)
        {
            return dataAccess.GetFiltroRicerca(userID, TipoFiltro.RicercaNotifiche, out pagina);
        }

        public NotificaFilter GetFiltroRicercaMappa(Guid userID)
        {
            Int32 pagina;
            return dataAccess.GetFiltroRicerca(userID, TipoFiltro.RicercaMappa, out pagina);
        }

        public NotificaFilter GetFiltroRicercaDenunce(Guid userID, out Int32 pagina)
        {
            return dataAccess.GetFiltroRicerca(userID, TipoFiltro.RicercaDenunce, out pagina);
        }

        public void DeleteFiltroRicerca(Guid userID)
        {
            dataAccess.DeleteFiltroRicerca(userID);
        }

        public UtenteNotificheTelematicheCollection GetUtentiTelematiche(UtenteNotificheTelematicheFilter filtro)
        {
            return dataAccess.GetUtentiTelematiche(filtro);
        }

        public Boolean VerificaTuttiGliEntiPresenti(String provincia, Int32 numeroTipologieEntiTrovati)
        {
            Boolean res = false;

            EnteDestinatarioTipologiaCollection enti = GetEntiDestinatariTipologie(provincia);
            if (enti.Count == numeroTipologieEntiTrovati)
            {
                res = true;
            }

            return res;
        }

        public ImpresaNotificheTelematiche GetImpresaDaUtente()
        {
            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                UtenteNotificheTelematiche utente = GetUtenteTelematiche((Guid) user.ProviderUserKey);
                int? idImpresa = GetIdImpresaDaCodiceFiscale(utente.CodiceFiscale);
                ImpresaNotificheTelematiche impresa = GetImpresaDenunciaCantiere(idImpresa);
                return impresa;
            }
            return null;
        }

        public UtenteNotificheTelematiche GetUtenteNotifiche()
        {
            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                UtenteNotificheTelematiche utente = GetUtenteTelematiche((Guid) user.ProviderUserKey);
                return utente;
            }

            return null;
        }

        #region Denunce Cantiere
        public CantiereCollection GetCantieri(CantiereFilter filtro)
        {
            return dataAccess.GetCantieri(filtro, null, false);
        }

        public Cantiere GetCantiere(Int32 idCantiere)
        {
            return dataAccess.GetCantiere(idCantiere, GetUtenteNotifiche().AziendaCodiceFiscale);
        }

        public CantiereCollection GetCantieri(NotificaFilter filtro)
        {
            return dataAccess.GetCantieri(null, filtro, false);
        }

        public CantiereCollection GetUltimiCantieri(String codiceFiscale)
        {
            CantiereFilter filtro = new CantiereFilter()
            {
                CodiceFiscaleImpresa = GetUtenteNotifiche().AziendaCodiceFiscale
            };

            return dataAccess.GetCantieri(filtro, null, true);
        }

        public ImpresaNotificheTelematiche GetImpresaDenunciaCantiere(int? idImpresa)
        {
            return dataAccess.GetImpresaCantieriDenunce(idImpresa);
        }

        public int InsertDenunciaCantiere(Cantiere cantiere)
        {
            return dataAccess.InsertDenunciaCantiere(cantiere);
        }

        //public int InsertSubappalto(int idDenunciaCantiere, Subappalto subappalto)
        //{
        //    return dataAccess.InsertSubappalto(idDenunciaCantiere, subappalto);
        //}

        public int InsertIndirizzoDenunciaCantiere(Indirizzo indirizzo)
        {
            return dataAccess.InsertIndirizzoDenunciaCantiere(indirizzo);
        }

        public void InsertDenunciaDaNotifica(NotificaTelematica notifica)
        {
            // Inserisco la notifica solamente se non č un aggiornamento
            if (!notifica.IdNotificaPadre.HasValue)
            {
                if (notifica.ImpreseAffidatarie.Count > 1 && notifica.ImpreseEsecutrici.Count == 0)
                {
                    foreach (Indirizzo indirizzo in notifica.Indirizzi)
                    {
                        Cantiere denuncia = new Cantiere();
                        denuncia.GuidUtenteTelematiche = notifica.IdUtenteTelematiche;
                        denuncia.IdNotifica = notifica.IdNotifica;

                        // Indirizzo
                        denuncia.IndirizzoCantiere = new Geocode.Type.Indirizzo();
                        denuncia.IndirizzoCantiere.NomeVia = indirizzo.NomeVia;
                        denuncia.IndirizzoCantiere.Civico = indirizzo.Civico;
                        denuncia.IndirizzoCantiere.Provincia = indirizzo.Provincia;
                        denuncia.IndirizzoCantiere.Comune = indirizzo.Comune;
                        denuncia.IndirizzoCantiere.Cap = indirizzo.Cap;
                        denuncia.IndirizzoCantiere.InformazioniAggiuntive = indirizzo.InformazioniAggiuntive;
                        denuncia.IndirizzoCantiere.Latitudine = indirizzo.Latitudine;
                        denuncia.IndirizzoCantiere.Longitudine = indirizzo.Longitudine;

                        // Dati
                        denuncia.NaturaOpera = notifica.NaturaOpera;
                        denuncia.AmmontareComplessivo = notifica.AmmontareComplessivo;
                        denuncia.DataInizioLavori = notifica.DataInizioLavori;
                        denuncia.DataFineLavori = notifica.DataFineLavori;
                        denuncia.TipologiaLavoro = notifica.TipologiaLavoro;
                        denuncia.PermessoCostruire = notifica.NumeroAppalto;

                        // Committente
                        if (notifica.Committente != null)
                        {
                            denuncia.Committente = new CommittenteDenuncieCantiere();
                            if (!String.IsNullOrEmpty(notifica.Committente.PersonaCognome))
                            {
                                denuncia.Committente.PersonaCognome = notifica.Committente.PersonaCognome;
                            }
                            if (!String.IsNullOrEmpty(notifica.Committente.PersonaNome))
                            {
                                denuncia.Committente.PersonaNome = notifica.Committente.PersonaNome;
                            }
                            if (!String.IsNullOrEmpty(notifica.Committente.PersonaCodiceFiscale))
                            {
                                denuncia.Committente.PersonaCodiceFiscale = notifica.Committente.PersonaCodiceFiscale;
                            }
                            if (!String.IsNullOrEmpty(notifica.Committente.RagioneSociale))
                            {
                                denuncia.Committente.EnteRagioneSociale = notifica.Committente.RagioneSociale;
                            }
                            if (!String.IsNullOrEmpty(notifica.Committente.CodiceFiscale))
                            {
                                denuncia.Committente.EnteCodiceFiscale = notifica.Committente.CodiceFiscale;
                            }
                            if (!String.IsNullOrEmpty(notifica.Committente.PartitaIva))
                            {
                                denuncia.Committente.EntePartitaIva = notifica.Committente.PartitaIva;
                            }
                            denuncia.Committente.Pubblico = !notifica.AppaltoPrivato;
                        }

                        // Capofila
                        //denuncia.Capofila = new ImpresaNotificheTelematiche();
                        //denuncia.Capofila.RagioneSociale = affidataria.ImpresaSelezionata.RagioneSociale;
                        //denuncia.Capofila.CodiceFiscale = affidataria.ImpresaSelezionata.CodiceFiscale;
                        //denuncia.Capofila.PartitaIva = affidataria.ImpresaSelezionata.PartitaIva;

                        // Appalti
                        foreach (SubappaltoNotificheTelematiche esecutrice in notifica.ImpreseAffidatarie)
                        {
                            Subappalto subDen = new Subappalto();
                            denuncia.Subappalti.Add(subDen);
                            subDen.ImpresaInSubappalto = new ImpresaNotificheTelematiche();

                            subDen.ImpresaInSubappalto.RagioneSociale = esecutrice.ImpresaSelezionata.RagioneSociale;
                            subDen.ImpresaInSubappalto.CodiceFiscale = esecutrice.ImpresaSelezionata.CodiceFiscale;
                            subDen.ImpresaInSubappalto.PartitaIva = esecutrice.ImpresaSelezionata.PartitaIva;
                            subDen.TipologiaAttivitą = esecutrice.ImpresaSelezionata.AttivitaPrevalente;

                            if (esecutrice.AppaltataDa != null)
                            {
                                subDen.Appaltatrice = new ImpresaNotificheTelematiche();
                                subDen.Appaltatrice.CodiceFiscale = esecutrice.AppaltataDa.CodiceFiscale;
                            }
                        }

                        InsertDenunciaCantiere(denuncia);
                    }
                }
                else
                {
                    foreach (SubappaltoNotificheTelematiche affidataria in notifica.ImpreseAffidatarie)
                    {
                        foreach (Indirizzo indirizzo in notifica.Indirizzi)
                        {
                            Cantiere denuncia = new Cantiere();
                            denuncia.GuidUtenteTelematiche = notifica.IdUtenteTelematiche;
                            denuncia.IdNotifica = notifica.IdNotifica;

                            // Indirizzo
                            denuncia.IndirizzoCantiere = new Geocode.Type.Indirizzo();
                            denuncia.IndirizzoCantiere.NomeVia = indirizzo.NomeVia;
                            denuncia.IndirizzoCantiere.Civico = indirizzo.Civico;
                            denuncia.IndirizzoCantiere.Provincia = indirizzo.Provincia;
                            denuncia.IndirizzoCantiere.Comune = indirizzo.Comune;
                            denuncia.IndirizzoCantiere.Cap = indirizzo.Cap;
                            denuncia.IndirizzoCantiere.InformazioniAggiuntive = indirizzo.InformazioniAggiuntive;

                            // Dati
                            denuncia.NaturaOpera = notifica.NaturaOpera;
                            denuncia.AmmontareComplessivo = notifica.AmmontareComplessivo;
                            denuncia.DataInizioLavori = notifica.DataInizioLavori;
                            denuncia.DataFineLavori = notifica.DataFineLavori;
                            denuncia.TipologiaLavoro = notifica.TipologiaLavoro;
                            denuncia.PermessoCostruire = notifica.NumeroAppalto;

                            // Committente
                            if (notifica.Committente != null)
                            {
                                denuncia.Committente = new CommittenteDenuncieCantiere();
                                if (!String.IsNullOrEmpty(notifica.Committente.PersonaCognome))
                                {
                                    denuncia.Committente.PersonaCognome = notifica.Committente.PersonaCognome;
                                }
                                if (!String.IsNullOrEmpty(notifica.Committente.PersonaNome))
                                {
                                    denuncia.Committente.PersonaNome = notifica.Committente.PersonaNome;
                                }
                                if (!String.IsNullOrEmpty(notifica.Committente.PersonaCodiceFiscale))
                                {
                                    denuncia.Committente.PersonaCodiceFiscale = notifica.Committente.PersonaCodiceFiscale;
                                }
                                if (!String.IsNullOrEmpty(notifica.Committente.RagioneSociale))
                                {
                                    denuncia.Committente.EnteRagioneSociale = notifica.Committente.RagioneSociale;
                                }
                                if (!String.IsNullOrEmpty(notifica.Committente.CodiceFiscale))
                                {
                                    denuncia.Committente.EnteCodiceFiscale = notifica.Committente.CodiceFiscale;
                                }
                                if (!String.IsNullOrEmpty(notifica.Committente.PartitaIva))
                                {
                                    denuncia.Committente.EntePartitaIva = notifica.Committente.PartitaIva;
                                }
                                denuncia.Committente.Pubblico = !notifica.AppaltoPrivato;
                            }

                            // Capofila
                            denuncia.Capofila = new ImpresaNotificheTelematiche();
                            denuncia.Capofila.RagioneSociale = affidataria.ImpresaSelezionata.RagioneSociale;
                            denuncia.Capofila.CodiceFiscale = affidataria.ImpresaSelezionata.CodiceFiscale;
                            denuncia.Capofila.PartitaIva = affidataria.ImpresaSelezionata.PartitaIva;

                            // Appalti
                            foreach (SubappaltoNotificheTelematiche esecutrice in notifica.ImpreseEsecutrici)
                            {
                                Subappalto subDen = new Subappalto();
                                denuncia.Subappalti.Add(subDen);
                                subDen.ImpresaInSubappalto = new ImpresaNotificheTelematiche();

                                subDen.ImpresaInSubappalto.RagioneSociale = esecutrice.ImpresaSelezionata.RagioneSociale;
                                subDen.ImpresaInSubappalto.CodiceFiscale = esecutrice.ImpresaSelezionata.CodiceFiscale;
                                subDen.ImpresaInSubappalto.PartitaIva = esecutrice.ImpresaSelezionata.PartitaIva;
                                subDen.TipologiaAttivitą = esecutrice.ImpresaSelezionata.AttivitaPrevalente;

                                if (esecutrice.AppaltataDa != null)
                                {
                                    subDen.Appaltatrice = new ImpresaNotificheTelematiche();
                                    subDen.Appaltatrice.CodiceFiscale = esecutrice.AppaltataDa.CodiceFiscale;
                                }
                            }

                            InsertDenunciaCantiere(denuncia);
                        }
                    }
                }
            }
        }

        //public int InsertImpreseDenunceCantieri(ImpresaNotificheTelematiche impresa)
        //{
        //    return dataAccess.InsertImpreseDenunceCantieri(impresa);
        //}

        public void UpdateDenunciaCantiere(Cantiere cantiere)
        {
            dataAccess.UpdateDenunciaCantiere(cantiere);
        }

        public void InsertDenunciaSubappalto(Subappalto subappalto, Int32 idDenuncia)
        {
            dataAccess.InsertDenunciaSubappalto(idDenuncia, subappalto, null);
        }

        public void UpdateDenunciaSubappalto(Subappalto subappalto)
        {
            dataAccess.UpdateDenunciaSubappalto(subappalto, null);
        }
        #endregion
    }
}