﻿using System;
using System.Collections.Generic;
using System.Text;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Business
{
    public class Estrazione
    {
        public static void EstraiTutto(out String notifiche, out String indirizzi, out String affidatarie, out String esecutrici)
        {
            String carattereSeparatore = Configurazione.EstrazioneCarattereSeparatore;
            notifiche = String.Empty;
            indirizzi = String.Empty;
            affidatarie = String.Empty;
            esecutrici = String.Empty;

            StringBuilder bNotifiche = new StringBuilder();
            StringBuilder bindirizzi = new StringBuilder();
            StringBuilder bAffidatarie = new StringBuilder();
            StringBuilder bEsecutrici = new StringBuilder();

            // Recupero tutte le notifiche presenti
            NotifichePreliminariManager biz = new NotifichePreliminariManager();
            List<Int32> notifichePresenti = biz.GetIdNotificheValide();

            // Per ogni notifica recupero tutti i dati (purtroppo è fatto così 
            // perchè reimplementando con una singola query sarebbe stato troppo lungo)
            foreach (Int32 idNotifica in notifichePresenti)
            {
                NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica);

                bNotifiche.AppendLine(String.Format("{0}{67}{1}{67}{2}{67}{3}{67}{4}{67}{5}{67}{6}{67}{7}{67}{8}{67}{9}{67}{10}{67}{11}{67}{12}{67}{13}{67}{14}{67}{15}{67}{16}{67}{17}{67}{18}{67}{19}{67}{20}{67}{21}{67}{22}{67}{23}{67}{24}{67}{25}{67}{26}{67}{27}{67}{28}{67}{29}{67}{30}{67}{31}{67}{32}{67}{33}{67}{34}{67}{35}{67}{36}{67}{37}{67}{38}{67}{39}{67}{40}{67}{41}{67}{42}{67}{43}{67}{44}{67}{45}{67}{46}{67}{47}{67}{48}{67}{49}{67}{50}{67}{51}{67}{52}{67}{53}{67}{54}{67}{55}{67}{56}{67}{57}{67}{58}{67}{59}{67}{60}{67}{61}{67}{62}{67}{63}{67}{64}{67}{65}{67}{66}{67}",
                    notifica.IdNotifica,
                    notifica.IdNotificaPadre,
                    notifica.Data.ToString("dd/MM/yyyy"),
                    Presenter.Presenter.StampaStringaExcel(notifica.NaturaOpera),
                    notifica.TipologiaLavoro,
                    notifica.AppaltoPrivato ? "PRIVATO" : "PUBBLICO",
                    notifica.NumeroAppalto,
                    notifica.AmmontareComplessivo,
                    notifica.DataInizioLavori.HasValue ? notifica.DataInizioLavori.Value.ToString("dd/MM/yyyy") : String.Empty,
                    notifica.Durata,
                    notifica.NumeroMassimoLavoratori,
                    notifica.NumeroImprese,
                    notifica.NumeroLavoratoriAutonomi,
                    notifica.NumeroGiorniUomo,
                    notifica.Committente.PersonaCognome,
                    notifica.Committente.PersonaNome,
                    notifica.Committente.PersonaCodiceFiscale,
                    notifica.Committente.PersonaIndirizzo,
                    notifica.Committente.PersonaProvincia,
                    notifica.Committente.PersonaComune,
                    notifica.Committente.PersonaCap,
                    notifica.Committente.PersonaTelefono,
                    notifica.Committente.Fax,
                    notifica.Committente.PersonaCellulare,
                    notifica.Committente.PersonaEmail,
                    notifica.Committente.RagioneSociale,
                    notifica.Committente.PartitaIva,
                    notifica.Committente.CodiceFiscale,
                    notifica.Committente.Indirizzo,
                    notifica.Committente.Provincia,
                    notifica.Committente.Comune,
                    notifica.Committente.Cap,
                    notifica.Committente.Telefono,
                    notifica.Committente.Fax,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaCognome : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaNome : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaCodiceFiscale : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.Indirizzo : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaProvincia : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaComune : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaCap : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.Telefono : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.Fax : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaCellulare : String.Empty,
                    notifica.DirettoreLavori != null ? notifica.DirettoreLavori.PersonaEmail : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaCognome : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaNome : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaCodiceFiscale : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.Indirizzo : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaProvincia : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaComune : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaCap : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.Telefono : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.Fax : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaCellulare : String.Empty,
                    notifica.CoordinatoreSicurezzaProgettazione != null ? notifica.CoordinatoreSicurezzaProgettazione.PersonaEmail : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaCognome : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaNome : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaCodiceFiscale : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaProvincia : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaComune : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaCap : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.Telefono : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.Fax : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaCellulare : String.Empty,
                    notifica.CoordinatoreSicurezzaRealizzazione != null ? notifica.CoordinatoreSicurezzaRealizzazione.PersonaEmail : String.Empty,
                    carattereSeparatore));

                foreach (Indirizzo indirizzo in notifica.Indirizzi)
                {
                    bindirizzi.AppendLine(String.Format("{0}{7}{1}{7}{2}{7}{3}{7}{4}{7}{5}{7}{6}{7}",
                        notifica.IdNotifica,
                        Presenter.Presenter.StampaStringaExcel(indirizzo.Indirizzo1),
                        indirizzo.Civico,
                        indirizzo.InfoAggiuntiva,
                        indirizzo.Comune,
                        indirizzo.Provincia,
                        indirizzo.Cap,
                        carattereSeparatore));
                }

                foreach (SubappaltoNotificheTelematiche subappalto in notifica.ImpreseAffidatarie)
                {
                    bAffidatarie.AppendLine(String.Format("{0}{18}{1}{18}{2}{18}{3}{18}{4}{18}{5}{18}{6}{18}{7}{18}{8}{18}{9}{18}{10}{18}{11}{18}{12}{18}{13}{18}{14}{18}{15}{18}{16}{18}{17}{18}",
                        notifica.IdNotifica,
                        subappalto.ImpresaSelezionata.RagioneSociale,
                        subappalto.ImpresaSelezionata.LavoratoreAutonomo ? "1" : "0",
                        subappalto.ImpresaSelezionata.PartitaIva,
                        subappalto.ImpresaSelezionata.CodiceFiscale,
                        subappalto.ImpresaSelezionata.AttivitaPrevalente,
                        subappalto.ImpresaSelezionata.IdCassaEdile,
                        subappalto.ImpresaSelezionata.MatricolaINAIL,
                        subappalto.ImpresaSelezionata.MatricolaINPS,
                        subappalto.ImpresaSelezionata.MatricolaCCIAA,
                        subappalto.ImpresaSelezionata.Email,
                        subappalto.ImpresaSelezionata.Pec,
                        subappalto.ImpresaSelezionata.Indirizzo,
                        subappalto.ImpresaSelezionata.Provincia,
                        subappalto.ImpresaSelezionata.Comune,
                        subappalto.ImpresaSelezionata.Cap,
                        subappalto.ImpresaSelezionata.Telefono,
                        subappalto.ImpresaSelezionata.Fax,
                        carattereSeparatore));
                }

                foreach (SubappaltoNotificheTelematiche subappalto in notifica.ImpreseEsecutrici)
                {
                    bEsecutrici.AppendLine(String.Format("{0}{20}{1}{20}{2}{20}{3}{20}{4}{20}{5}{20}{6}{20}{7}{20}{8}{20}{9}{20}{10}{20}{11}{20}{12}{20}{13}{20}{14}{20}{15}{20}{16}{20}{17}{20}{18}{20}{19}{20}",
                        notifica.IdNotifica,
                        subappalto.ImpresaSelezionata.RagioneSociale,
                        subappalto.ImpresaSelezionata.LavoratoreAutonomo ? "1" : "0",
                        subappalto.ImpresaSelezionata.PartitaIva,
                        subappalto.ImpresaSelezionata.CodiceFiscale,
                        subappalto.ImpresaSelezionata.AttivitaPrevalente,
                        subappalto.ImpresaSelezionata.IdCassaEdile,
                        subappalto.ImpresaSelezionata.MatricolaINAIL,
                        subappalto.ImpresaSelezionata.MatricolaINPS,
                        subappalto.ImpresaSelezionata.MatricolaCCIAA,
                        subappalto.ImpresaSelezionata.Email,
                        subappalto.ImpresaSelezionata.Pec,
                        subappalto.ImpresaSelezionata.Indirizzo,
                        subappalto.ImpresaSelezionata.Provincia,
                        subappalto.ImpresaSelezionata.Comune,
                        subappalto.ImpresaSelezionata.Cap,
                        subappalto.ImpresaSelezionata.Telefono,
                        subappalto.ImpresaSelezionata.Fax,
                        subappalto.ImpresaAppaltataDaRagioneSociale,
                        subappalto.ImpresaAppaltataDaPartitaIva,
                        carattereSeparatore));
                }
            }

            notifiche = bNotifiche.ToString();
            indirizzi = bindirizzi.ToString();
            affidatarie = bAffidatarie.ToString();
            esecutrici = bEsecutrici.ToString();
        }

        public static void EstraiTuttoDenunce(out String denunce, out String subappalti)
        {
            String carattereSeparatore = Configurazione.EstrazioneCarattereSeparatore;
            denunce = String.Empty;
            subappalti = String.Empty;

            StringBuilder bDenunce = new StringBuilder();
            StringBuilder bSubappalti = new StringBuilder();

            // Recupero tutte le notifiche presenti
            NotifichePreliminariManager biz = new NotifichePreliminariManager();
            List<Int32> denuncePresenti = biz.GetIdDenunceValide();

            // Per ogni denuncia recupero tutti i dati (purtroppo è fatto così 
            // perchè reimplementando con una singola query sarebbe stato troppo lungo)
            foreach (Int32 idDenuncia in denuncePresenti)
            {
                Cantiere denuncia = biz.GetCantiere(idDenuncia);

                bDenunce.AppendLine(String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}{0}{14}{0}{15}{0}{16}{0}{17}{0}{18}{0}{19}{0}{20}{0}{21}{0}{22}{0}{23}{0}{24}{0}{25}{0}{26}{0}{27}{0}",
                    carattereSeparatore,
                    denuncia.CodiceCantiere,
                    Presenter.Presenter.StampaStringaExcel(denuncia.IndirizzoCantiere.IndirizzoBase),
                    denuncia.IndirizzoCantiere.Civico,
                    denuncia.IndirizzoCantiere.InformazioniAggiuntive,
                    denuncia.IndirizzoCantiere.Comune,
                    denuncia.IndirizzoCantiere.Provincia,
                    denuncia.IndirizzoCantiere.Cap,
                    denuncia.Committente != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Committente.PersonaCognome) : String.Empty,
                    denuncia.Committente != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Committente.PersonaNome) : String.Empty,
                    denuncia.Committente != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Committente.PersonaCodiceFiscale) : String.Empty,
                    denuncia.Committente != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Committente.EnteRagioneSociale) : String.Empty,
                    denuncia.Committente != null ? denuncia.Committente.EnteCodiceFiscale : String.Empty,
                    denuncia.Committente != null ? denuncia.Committente.EntePartitaIva : String.Empty,
                    denuncia.Committente != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Committente.Indirizzo) : String.Empty,
                    denuncia.Committente != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Committente.Comune) : String.Empty,
                    denuncia.Committente != null ? denuncia.Committente.Provincia : String.Empty,
                    denuncia.Committente != null ? denuncia.Committente.Cap : String.Empty,
                    Presenter.Presenter.StampaStringaExcel(denuncia.NaturaOpera),
                    denuncia.AmmontareComplessivo,
                    denuncia.AmmontareEdile,
                    denuncia.DataInizioLavori.HasValue ? denuncia.DataInizioLavori.Value.ToString("dd/MM/yyyy") : String.Empty,
                    denuncia.DataFineLavori.HasValue ? denuncia.DataFineLavori.Value.ToString("dd/MM/yyyy") : String.Empty,
                    denuncia.PermessoCostruire,
                    denuncia.TipologiaLavoro != null ? denuncia.TipologiaLavoro.Descrizione : String.Empty,
                    denuncia.Capofila != null ? Presenter.Presenter.StampaStringaExcel(denuncia.Capofila.RagioneSociale) : String.Empty,
                    denuncia.Capofila != null ? denuncia.Capofila.CodiceFiscale : String.Empty,
                    denuncia.Capofila != null ? denuncia.Capofila.PartitaIva : String.Empty));

                foreach (Subappalto subappalto in denuncia.Subappalti)
                {
                    bSubappalti.AppendLine(String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}",
                        carattereSeparatore,
                        denuncia.CodiceCantiere,
                        subappalto.Appaltatrice != null ? subappalto.Appaltatrice.CodiceFiscale : String.Empty,
                        subappalto.ImpresaInSubappalto.RagioneSociale,
                        subappalto.ImpresaInSubappalto.CodiceFiscale,
                        subappalto.ImpresaInSubappalto.PartitaIva,
                        subappalto.ImpresaInSubappalto.AttivitaPrevalente,
                        subappalto.Ammontare,
                        subappalto.DataInizioLavori.HasValue ? subappalto.DataInizioLavori.Value.ToString("dd/MM/yyyy") : String.Empty,
                        subappalto.DataFineLavori.HasValue ? subappalto.DataFineLavori.Value.ToString("dd/MM/yyyy") : String.Empty));
                }
            }

            denunce = bDenunce.ToString();
            subappalti = bSubappalti.ToString();
        }
    }
}
