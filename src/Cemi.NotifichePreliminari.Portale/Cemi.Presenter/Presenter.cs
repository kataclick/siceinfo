using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Cemi.Presenter
{
    public class Presenter
    {
        #region Metodi per il DataBind

        public static void CaricaElementiInDropDownConElementoVuoto(
            DropDownList dropDown,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.Items.Add(new ListItem(string.Empty, string.Empty));
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInDropDown(
            DropDownList dropDown,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInDropDown(
            DropDownList dropDown,
            DataTable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInGridView(
            GridView gridView,
            IEnumerable lista,
            Int32 pagina)
        {
            gridView.PageIndex = pagina;
            gridView.DataSource = lista;
            gridView.DataBind();
        }

        public static void CaricaElementiInListView(
            ListView listView,
            IEnumerable lista)
        {
            listView.DataSource = lista;
            listView.DataBind();
        }

        public static void CaricaElementiInCheckBoxList(
            CheckBoxList checkBoxList,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            checkBoxList.DataSource = lista;
            checkBoxList.DataTextField = nomeCampoTesto;
            checkBoxList.DataValueField = nomeCampoValore;
            checkBoxList.DataBind();
        }

        public static void CaricaElementiInBulletedList(
            BulletedList bulletedList,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            bulletedList.DataSource = lista;
            bulletedList.DataTextField = nomeCampoTesto;
            bulletedList.DataValueField = nomeCampoValore;
            bulletedList.DataBind();
        }

        public static void CaricaElementiInRadioButtonList(
            RadioButtonList radioButtonList,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore
            )
        {
            radioButtonList.DataSource = lista;
            radioButtonList.DataTextField = nomeCampoTesto;
            radioButtonList.DataValueField = nomeCampoValore;
            radioButtonList.DataBind();
        }

        #endregion

        #region Metodi per il DataBind dei controlli Telerik

        public static void CaricaElementiInDropDown(
            RadComboBox dropDown,
            IEnumerable lista,
            String nomeCampoTesto,
            String nomeCampoValore)
        {
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInDropDownConElementoVuoto(
            RadComboBox dropDown,
            IEnumerable lista,
            String nomeCampoTesto,
            String nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.Items.Add(new RadComboBoxItem(string.Empty, string.Empty));
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInGridView(
            RadGrid gridView,
            IEnumerable lista)
        {
            gridView.DataSource = lista;
            gridView.DataBind();
        }

        public static void CaricaElementiInGridView(
            RadGrid gridView,
            DataTable tabella)
        {
            gridView.DataSource = tabella;
            gridView.DataBind();
        }

        public static void CaricaElementiInListView(
            RadListView listView,
            IEnumerable lista)
        {
            listView.DataSource = lista;
            listView.DataBind();
        }

        #endregion

        #region Metodi per recuperare i dati dall'interfaccia

        public static string NormalizzaCampoTesto(string campo)
        {
            if (!String.IsNullOrEmpty(campo))
            {
                return campo.Trim().ToUpper();
            }

            return String.Empty;
        }

        public static void SvuotaCampo(TextBox textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(RadTextBox textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(RadNumericTextBox textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(RadDateInput textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(Label label)
        {
            label.Text = null;
        }

        #endregion

        #region Metodi per mandare stringhe in output

        public static String StampaValoreMonetario(Decimal? valore)
        {
            string ret = String.Empty;

            if (valore.HasValue)
            {
                ret = valore.Value.ToString("C");
            }

            return ret;
        }

        public static String StampaDataFormattataClassica(DateTime? data)
        {
            string ret = String.Empty;

            if (data.HasValue)
            {
                ret = data.Value.ToShortDateString();
            }

            return ret;
        }

        public static String StampaStringaExcel(String valore)
        {
            String result = String.Empty;

            if (valore != null)
            {
                result = valore.Replace(';', ' ').Replace('\n', ' ').Replace('\r', ' ');
            }

            return result;
        }

        #endregion

        #region Restituzione immagini

        public static void RestituisciFileArchidoc(string idArchidoc, byte[] file, Page page)
        {
            //Set the appropriate ContentType.
            page.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.pdf", idArchidoc));
            page.Response.ContentType = "application/pdf";
            //Write the file directly to the HTTP content output stream.
            page.Response.BinaryWrite(file);
            page.Response.Flush();
            page.Response.End();
        }

        #endregion

        #region Metodi per il caricamento delle province e comuni nelle DropDown

        public static void CaricaProvince(DropDownList dropDownProvince)
        {
            Common commonBiz = new Common();
            CaricaElementiInDropDownConElementoVuoto(
                dropDownProvince,
                commonBiz.GetProvinceSiceNew(),
                String.Empty,
                String.Empty);
        }

        public static void CaricaProvince(RadComboBox dropDownProvince)
        {
            Common commonBiz = new Common();
            CaricaElementiInDropDownConElementoVuoto(
                dropDownProvince,
                commonBiz.GetProvinceSiceNew(),
                String.Empty,
                String.Empty);
        }

        public static void CaricaComuni(DropDownList dropDownComuni, String provincia)
        {
            if (!String.IsNullOrEmpty(provincia))
            {
                Common commonBiz = new Common();

                CaricaElementiInDropDownConElementoVuoto(
                    dropDownComuni,
                    commonBiz.GetComuniSiceNew(provincia),
                    "",
                    "");
            }
            else
            {
                dropDownComuni.Items.Clear();
            }
        }

        public static void CaricaComuni(RadComboBox dropDownComuni, String provincia)
        {
            if (!String.IsNullOrEmpty(provincia))
            {
                Common commonBiz = new Common();

                CaricaElementiInDropDownConElementoVuoto(
                    dropDownComuni,
                    commonBiz.GetComuniSiceNew(provincia),
                    "",
                    "");
            }
            else
            {
                dropDownComuni.Items.Clear();
                dropDownComuni.Text = String.Empty;
            }
        }

        #endregion

        #region Controlli
        public static Boolean VerificaPartitaIVA(String partitaIva)
        {
            bool res = true;

            int i;
            int s = 0;
            for (i = 0; i <= 9; i += 2)
                s += partitaIva[i] - '0';
            for (i = 1; i <= 9; i += 2)
            {
                int c = 2 * (partitaIva[i] - '0');
                if (c > 9)
                    c = c - 9;
                s += c;
            }
            if ((10 - s % 10) % 10 != partitaIva[10] - '0')
                res = false;

            return res;
        }

        public static bool VerificaPrimi11CaratteriCodiceFiscale(string nome, string cognome, string sesso,
                                                                 DateTime dataNascita, string codiceFiscale)
        {
            return (VerificaCodiceControlloCodiceFiscale(codiceFiscale) &&
                    codiceFiscale.Substring(0, 11) ==
                    CalcolaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita));
        }

        private static Boolean VerificaCodiceControlloCodiceFiscale(String codiceFiscale)
        {
            Boolean res = false;

            const string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int[,] matricecod = GetMatriceControlloCf();

            codiceFiscale = codiceFiscale.ToUpper();
            int codcontrollo = 0;
            // richiede una using System.Text;
            ASCIIEncoding ascii = new ASCIIEncoding();
            for (int i = 0; i <= 14; i++)
            {
                string carattere = codiceFiscale[i].ToString();
                byte[] asciibytes = ascii.GetBytes(carattere);
                int asciicode = Int32.Parse(asciibytes[0].ToString());
                codcontrollo = codcontrollo + matricecod[asciicode, i % 2];
            }

            if (alfabeto[codcontrollo % 26] == codiceFiscale[15])
            {
                res = true;
            }

            return res;
        }

        private static int[,] GetMatriceControlloCf()
        {
            int[,] matricecod = new int[100, 2];
            //matrice per calcolare il carattere di controllo
            //il primo indice � il codice ascii del carattere il secondo indice la posizione pari o dispari
            // "A"
            matricecod[65, 0] = 1;
            matricecod[65, 1] = 0;
            // "B"
            matricecod[66, 0] = 0;
            matricecod[66, 1] = 1;
            // "C"
            matricecod[67, 0] = 5;
            matricecod[67, 1] = 2;
            // "D"
            matricecod[68, 0] = 7;
            matricecod[68, 1] = 3;
            // "E"
            matricecod[69, 0] = 9;
            matricecod[69, 1] = 4;
            // "F"
            matricecod[70, 0] = 13;
            matricecod[70, 1] = 5;
            // "G"
            matricecod[71, 0] = 15;
            matricecod[71, 1] = 6;
            // "H"
            matricecod[72, 0] = 17;
            matricecod[72, 1] = 7;
            // "I"
            matricecod[73, 0] = 19;
            matricecod[73, 1] = 8;
            // "J"
            matricecod[74, 0] = 21;
            matricecod[74, 1] = 9;
            // "K"
            matricecod[75, 0] = 2;
            matricecod[75, 1] = 10;
            // "L"
            matricecod[76, 0] = 4;
            matricecod[76, 1] = 11;
            // "M"
            matricecod[77, 0] = 18;
            matricecod[77, 1] = 12;
            // "N"
            matricecod[78, 0] = 20;
            matricecod[78, 1] = 13;
            // "O"
            matricecod[79, 0] = 11;
            matricecod[79, 1] = 14;
            // "P"
            matricecod[80, 0] = 3;
            matricecod[80, 1] = 15;
            // "Q"
            matricecod[81, 0] = 6;
            matricecod[81, 1] = 16;
            // "R"
            matricecod[82, 0] = 8;
            matricecod[82, 1] = 17;
            // "S"
            matricecod[83, 0] = 12;
            matricecod[83, 1] = 18;
            // "T"
            matricecod[84, 0] = 14;
            matricecod[84, 1] = 19;
            // "U"
            matricecod[85, 0] = 16;
            matricecod[85, 1] = 20;
            // "V"
            matricecod[86, 0] = 10;
            matricecod[86, 1] = 21;
            // "W"
            matricecod[87, 0] = 22;
            matricecod[87, 1] = 22;
            // "X"
            matricecod[88, 0] = 25;
            matricecod[88, 1] = 23;
            // "Y"
            matricecod[89, 0] = 24;
            matricecod[89, 1] = 24;
            // "Z"
            matricecod[90, 0] = 23;
            matricecod[90, 1] = 25;
            // "0"
            matricecod[48, 0] = 1;
            matricecod[48, 1] = 0;
            // "1"
            matricecod[49, 0] = 0;
            matricecod[49, 1] = 1;
            // "2"
            matricecod[50, 0] = 5;
            matricecod[50, 1] = 2;
            // "3"
            matricecod[51, 0] = 7;
            matricecod[51, 1] = 3;
            // "4"
            matricecod[52, 0] = 9;
            matricecod[52, 1] = 4;
            // "5"
            matricecod[53, 0] = 13;
            matricecod[53, 1] = 5;
            // "6"
            matricecod[54, 0] = 15;
            matricecod[54, 1] = 6;
            // "7"
            matricecod[55, 0] = 17;
            matricecod[55, 1] = 7;
            // "8"
            matricecod[56, 0] = 19;
            matricecod[56, 1] = 8;
            // "9"
            matricecod[57, 0] = 21;
            matricecod[57, 1] = 9;

            return matricecod;
        }

        private static string CalcolaPrimi11CaratteriCodiceFiscale(string nome, string cognome, string sesso,
                                                                  DateTime dataNascita)
        {
            const string mesi = "ABCDEHLMPRST";

            string ccognome = cognome.ToUpper();
            string cnome = nome.ToUpper();

            string codfisc = CalcolaLettereCognome(ccognome.Trim());
            codfisc = codfisc + CalcolaLettereNome(cnome.Trim());
            codfisc = codfisc + dataNascita.Year.ToString().Substring(2);
            codfisc = codfisc + mesi[dataNascita.Month - 1];
            if (sesso == "F")
                codfisc = codfisc + (dataNascita.Day + 40);
            else
            {
                string tmpGiorno = dataNascita.Day.ToString();
                if (tmpGiorno.Length == 1)
                    tmpGiorno = "0" + tmpGiorno;
                codfisc = codfisc + tmpGiorno;
            }

            return codfisc;
        }

        private static string CalcolaLettereCognome(string cognome)
        {
            // restituisce le 3 lettere relative al cognome

            string treLettereCognome = String.Empty;

            const string vocali = "AEIOU";
            const string consonanti = "BCDFGHJKLMNPQRSTVWXYZ";

            cognome = StripAccentate(cognome);
            int i = 0;
            while ((treLettereCognome.Length < 3) && (i < cognome.Length))
            {
                for (int j = 0; j < consonanti.Length; j++)
                {
                    if (cognome[i] == consonanti[j])
                    {
                        treLettereCognome = treLettereCognome + cognome[i];
                    }
                }
                i++;
            }

            i = 0;
            //se non ha ancora 3 consonanti sceglie fra le vocali
            while ((treLettereCognome.Length < 3) && (i < cognome.Length))
            {
                for (int j = 0; j < vocali.Length; j++)
                {
                    if (cognome[i] == vocali[j])
                    {
                        treLettereCognome = treLettereCognome + cognome[i];
                    }
                }
                i++;
            }

            //se non ha ancora 3 lettere aggiunge x per arrivare a 3
            if (treLettereCognome.Length < 3)
            {
                for (int k = treLettereCognome.Length; k <= 2; k++)
                    treLettereCognome = treLettereCognome + "X";
            }

            return treLettereCognome;
        }

        private static string CalcolaLettereNome(string nome)
        {
            // restituisce le 3 lettere relative al nome

            const string vocali = "AEIOU";
            const string consonanti = "BCDFGHJKLMNPQRSTVWXYZ";

            nome = StripAccentate(nome);
            int i = 0;
            string cons = "";
            string treLettereNome;
            while ((cons.Length < 4) && (i < nome.Length))
            {
                for (int j = 0; j < consonanti.Length; j++)
                {
                    if (nome[i] == consonanti[j])
                    {
                        cons = cons + nome[i];
                    }
                }
                i++;
            }

            if (cons.Length > 3)
            {
                //se ha 4 consonanti prende la 1a, 3a e 4a
                treLettereNome = cons[0] + cons[2].ToString() + cons[3];
            }
            else
                treLettereNome = cons;

            i = 0;
            //scorre il nome in cerca di vocali finch� $stringa non contiene 3 lettere
            while ((treLettereNome.Length < 3) && (i < nome.Length))
            {
                for (int j = 0; j < vocali.Length; j++)
                {
                    if (nome[i] == vocali[j])
                    {
                        treLettereNome = treLettereNome + nome[i];
                    }
                }
                i++;
            }

            //se non ha ancora 3 lettere aggiunge x per arrivare a 3
            if (treLettereNome.Length < 3)
            {
                for (int k = treLettereNome.Length; k <= 2; k++)
                    treLettereNome = treLettereNome + "X";
            }

            return treLettereNome;
        }

        private static string StripAccentate(string s)
        {
            const string accentate = "������������";
            const string noaccento = "AEEIOUAEEIOU";
            string tmpString = "";

            for (int i = 0; i < s.Length; i++)
            {
                bool accentata = false;
                int tmpIndex = 0;
                for (int j = 0; j < accentate.Length; j++)
                {
                    if (s[i] == accentate[j])
                    {
                        accentata = true;
                        tmpIndex = j;
                    }
                }
                if (accentata)
                    tmpString = tmpString + noaccento[tmpIndex];
                else
                    tmpString = tmpString + s[i];
            }

            return tmpString;
        }
        #endregion
    }
}