﻿using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.Presenter
{
    public class Common
    {
        private Database databaseCemi;

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        public Common()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public StringCollection GetProvinceSiceNew()
        {
            StringCollection province = new StringCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ComuniSiceNewSelectProvince"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        province.Add(reader["provincia"].ToString());
                    }
                }
            }

            return province;
        }

        public StringCollection GetComuniSiceNew(string provincia)
        {
            StringCollection comuni = new StringCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ComuniSiceNewSelect"))
            {
                if (!string.IsNullOrEmpty(provincia))
                    DatabaseCemi.AddInParameter(dbCommand, "@provincia", DbType.String, provincia);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        comuni.Add(reader["comune"].ToString());
                    }
                }
            }

            return comuni;
        }
    }
}
