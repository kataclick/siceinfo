namespace Cemi.NotifichePreliminari.Type.Entities
{
    public class TipologiaVisita
    {
        public int IdTipologiaVisita { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}