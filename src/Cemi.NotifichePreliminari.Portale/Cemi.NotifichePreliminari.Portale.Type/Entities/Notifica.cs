using System;
using System.Xml.Serialization;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Type.Collections;

namespace Cemi.NotifichePreliminari.Type.Entities
{
    [Serializable]
    public class Notifica
    {
        private decimal? ammontareComplessivo;
        private bool annullata;
        private Area area;
        private Persona coordinatoreSicurezzaProgettazione;
        private Persona coordinatoreSicurezzaRealizzazione;
        private DateTime data;
        private DateTime? dataAnnullamento;
        private DateTime? dataFineLavori;
        private DateTime? dataInizioLavori;
        private DateTime dataInserimento;
        private DateTime? dataNotificaPadre;
        private Persona direttoreLavori;
        private int? durata;
        private int? idNotifica;
        private int? idNotificaPadre;
        private Int32 idNotificaRiferimento;
        private bool impresaPresente;
        private IndirizzoCollection indirizzi;
        private string naturaOpera;
        private string numeroAppalto;
        private int? numeroGiorniUomo;
        private int? numeroImprese;
        private int? numeroLavoratoriAutonomi;
        private int? numeroMassimoLavoratori;
        private bool responsabileCommittente;
        private NotificaCollection storia;
        private string utente;
        private string utenteAnnullamento;

        public Notifica()
        {
            indirizzi = new IndirizzoCollection();
            storia = new NotificaCollection();
        }

        public int? IdNotifica
        {
            get { return idNotifica; }
            set { idNotifica = value; }
        }

        public int? IdNotificaPadre
        {
            get { return idNotificaPadre; }
            set { idNotificaPadre = value; }
        }

        public DateTime? DataNotificaPadre
        {
            get { return dataNotificaPadre; }
            set { dataNotificaPadre = value; }
        }

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public IndirizzoCollection Indirizzi
        {
            get { return indirizzi; }
            set { indirizzi = value; }
        }

        [XmlIgnore]
        public Committente Committente { get; set; }

        public string CommittenteRagioneSociale
        {
            get
            {
                if (Committente != null) return Committente.RagioneSociale;
                else return string.Empty;
            }
        }

        public string NaturaOpera
        {
            get { return naturaOpera; }
            set { naturaOpera = value; }
        }

        public string NumeroAppalto
        {
            get { return numeroAppalto; }
            set { numeroAppalto = value; }
        }

        [XmlIgnore]
        public Persona CoordinatoreSicurezzaProgettazione
        {
            get { return coordinatoreSicurezzaProgettazione; }
            set { coordinatoreSicurezzaProgettazione = value; }
        }

        [XmlIgnore]
        public Persona CoordinatoreSicurezzaRealizzazione
        {
            get { return coordinatoreSicurezzaRealizzazione; }
            set { coordinatoreSicurezzaRealizzazione = value; }
        }

        [XmlIgnore]
        public Persona DirettoreLavori
        {
            get { return direttoreLavori; }
            set { direttoreLavori = value; }
        }

        public bool ResponsabileCommittente
        {
            get { return responsabileCommittente; }
            set { responsabileCommittente = value; }
        }

        public DateTime? DataInizioLavori
        {
            get { return dataInizioLavori; }
            set { dataInizioLavori = value; }
        }

        public DateTime? DataFineLavori
        {
            get { return dataFineLavori; }
            set { dataFineLavori = value; }
        }

        public int? Durata
        {
            get { return durata; }
            set { durata = value; }
        }

        public int? NumeroGiorniUomo
        {
            get { return numeroGiorniUomo; }
            set { numeroGiorniUomo = value; }
        }

        public int? NumeroMassimoLavoratori
        {
            get { return numeroMassimoLavoratori; }
            set { numeroMassimoLavoratori = value; }
        }

        public int? NumeroImprese
        {
            get { return numeroImprese; }
            set { numeroImprese = value; }
        }

        public int? NumeroLavoratoriAutonomi
        {
            get { return numeroLavoratoriAutonomi; }
            set { numeroLavoratoriAutonomi = value; }
        }

        public decimal? AmmontareComplessivo
        {
            get { return ammontareComplessivo; }
            set { ammontareComplessivo = value; }
        }

        public string Utente
        {
            get { return utente; }
            set { utente = value; }
        }

        public DateTime DataInserimento
        {
            get { return dataInserimento; }
            set { dataInserimento = value; }
        }

        public bool Annullata
        {
            get { return annullata; }
            set { annullata = value; }
        }

        public DateTime? DataAnnullamento
        {
            get { return dataAnnullamento; }
            set { dataAnnullamento = value; }
        }

        public string UtenteAnnullamento
        {
            get { return utenteAnnullamento; }
            set { utenteAnnullamento = value; }
        }

        public NotificaCollection Storia
        {
            get { return storia; }
            set { storia = value; }
        }

        public bool ImpresaPresente
        {
            get { return impresaPresente; }
            set { impresaPresente = value; }
        }

        public int NumeroVisiteASL { get; set; }

        public int NumeroVisiteCPT { get; set; }

        public int NumeroVisiteDPL { get; set; }

        public int NumeroVisiteINPS { get; set; }

        public int NumeroVisiteINAIL { get; set; }

        public int NumeroVisiteCassaEdile { get; set; }

        public Area Area
        {
            get { return area; }
            set { area = value; }
        }

        public Int32 IdNotificaRiferimento
        {
            get { return idNotificaRiferimento; }
            set { idNotificaRiferimento = value; }
        }

        public String ProtocolloRegione { get; set; }

        public DateTime? DataPrimoInserimento { get; set; }

        public Boolean AppaltoPrivato { get; set; }

        public TipologiaLavoro TipologiaLavoro { get; set; }
    }
}