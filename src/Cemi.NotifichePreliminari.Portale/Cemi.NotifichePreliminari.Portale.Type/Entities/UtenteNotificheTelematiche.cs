using System;

namespace Cemi.NotifichePreliminari.Type.Entities
{
    public class UtenteNotificheTelematiche
    {
        public Int32 IdQualifica
        {
            get;
            set;
        }

        public String Cognome
        {
            get;
            set;
        }

        public String Nome
        {
            get;
            set;
        }

        public String Sesso
        {
            get;
            set;
        }

        public DateTime DataNascita
        {
            get;
            set;
        }

        public String CodiceFiscale
        {
            get;
            set;
        }

        public String Pec
        {
            get;
            set;
        }

        public String Indirizzo
        {
            get;
            set;
        }

        public String Comune
        {
            get;
            set;
        }

        public String Provincia
        {
            get;
            set;
        }

        public String Cap
        {
            get;
            set;
        }

        public String Telefono
        {
            get;
            set;
        }

        public String Fax
        {
            get;
            set;
        }

        public Guid IdUtente
        {
            get;
            set;
        }

        public CommittenteNotificheTelematiche CommittenteTelematiche
        {
            get;
            set;
        }

        public String FiltroProvincia
        {
            get;
            set;
        }

        public String ProvinciaSuggerita
        {
            get;
            set;
        }

        public String IdCassaEdile
        {
            get;
            set;
        }

        public String DescrizioneCassaEdile
        {
            get;
            set;
        }

        public String UserName
        {
            get;
            set;
        }

        public Guid ProviderUserKey
        {
            get;
            set;
        }

        public Boolean IsApproved
        {
            get;
            set;
        }

        public String EMail
        {
            get;
            set;
        }

        public String AziendaRagioneSociale
        {
            get;
            set;
        }

        public String AziendaPartitaIVA
        {
            get;
            set;
        }

        public String AziendaCodiceFiscale
        {
            get;
            set;
        }
    }
}