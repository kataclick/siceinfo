using System;

namespace Cemi.NotifichePreliminari.Type.Entities
{
    [Serializable]
    public class SubappaltoNotificheTelematiche
    {
        public Int32? IdSubappalto { get; set; }

        public ImpresaNotificheTelematiche ImpresaSelezionata { get; set; }

        public ImpresaNotificheTelematiche AppaltataDa { get; set; }

        public Boolean Affidatarie { get; set; }

        public String ImpresaSelezionataRagioneSociale
        {
            get { return ImpresaSelezionata != null ? ImpresaSelezionata.RagioneSociale : String.Empty; }
        }

        public String ImpresaSelezionataPartitaIva
        {
            get { return ImpresaSelezionata != null ? ImpresaSelezionata.PartitaIva : String.Empty; }
        }

        public String ImpresaSelezionataCodiceFiscale
        {
            get { return ImpresaSelezionata != null ? ImpresaSelezionata.CodiceFiscale : String.Empty; }
        }

        public String ImpresaAppaltataDaRagioneSociale
        {
            get { return AppaltataDa != null ? AppaltataDa.RagioneSociale : String.Empty; }
        }

        public String ImpresaAppaltataDaPartitaIva
        {
            get { return AppaltataDa != null ? AppaltataDa.PartitaIva : String.Empty; }
        }

        public Guid IdTemporaneoImpresaSelezionata
        {
            get
            {
                if (ImpresaSelezionata != null) return ImpresaSelezionata.IdTemporaneo;
                else return Guid.Empty;
            }
        }
    }
}