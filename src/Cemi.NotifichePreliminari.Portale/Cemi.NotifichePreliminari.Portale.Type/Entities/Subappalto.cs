﻿using System;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class Subappalto
    {
        public Int32? IdSubappalto
        {
            get;
            set;
        }
        public ImpresaNotificheTelematiche ImpresaInSubappalto
        {
            get;
            set;
        }
        public ImpresaNotificheTelematiche Appaltatrice
        {
            get;
            set;
        }
        public String TipologiaAttività
        {
            get;
            set;
        }
        public Decimal? Ammontare
        {
            get;
            set;
        }
        public DateTime? DataInizioLavori
        {
            get;
            set;
        }
        public DateTime? DataFineLavori
        {
            get;
            set;
        }
        public String ImpresaInSubappaltoCodiceFiscale
        {
            get
            {
                if (this.ImpresaInSubappalto != null)
                {
                    return this.ImpresaInSubappalto.CodiceFiscale;
                }

                return null;
            }
        }
        public String ImpresaInSubappaltoRagioneSociale
        {
            get
            {
                if (this.ImpresaInSubappalto != null)
                {
                    return this.ImpresaInSubappalto.RagioneSociale;
                }

                return null;
            }
        }

        public String Note { get; set; }
    }
}
