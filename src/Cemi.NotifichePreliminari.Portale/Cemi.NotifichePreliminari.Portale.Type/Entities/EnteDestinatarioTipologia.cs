﻿using System;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class EnteDestinatarioTipologia
    {
        public Int32 Id { get; set; }

        public String Descrizione { get; set; }

        public override string ToString()
        {
            return this.Descrizione;
        }
    }
}
