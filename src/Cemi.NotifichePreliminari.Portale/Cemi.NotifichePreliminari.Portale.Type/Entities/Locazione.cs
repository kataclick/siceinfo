﻿using System;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class Locazione
    {
        private String comune;
        private String indirizzo;
        private String provincia;
        public Int32? IdLocazione { get; set; }

        public String Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public String Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public String Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public String Cap { get; set; }

        public Decimal? Latitudine { get; set; }

        public Decimal? Longitudine { get; set; }

        public String Note { get; set; }

        public Boolean EsistePianificazione { get; set; }

        public string IndirizzoPerGeocoder
        {
            get { return String.Format("{0} {1} {2}, Lombardia, Italy", indirizzo, comune, provincia); }
        }

        public string IndirizzoCompleto
        {
            get { return String.Format("{0} {1} {2}", indirizzo, comune, provincia); }
        }
    }
}