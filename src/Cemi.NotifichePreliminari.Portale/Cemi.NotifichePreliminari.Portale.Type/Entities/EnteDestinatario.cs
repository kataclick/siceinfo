﻿using System;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class EnteDestinatario
    {
        public Int32 Id { get; set; }

        public String Provincia { get; set; }

        public String Indirizzo { get; set; }

        public String Comune { get; set; }

        public String Cap { get; set; }

        public EnteDestinatarioTipologia Tipologia { get; set; }

        public String Descrizione { get; set; }

        public String EmailTo { get; set; }

        public String EmailCcn { get; set; }

        public String IndirizzoCompleto
        {
            get
            {
                return String.Format("{0} - {1} {2} ({3})", this.Indirizzo, this.Cap, this.Comune, this.Provincia);
            }
        }

        public override string ToString()
        {
            return this.Descrizione;
        }
    }
}
