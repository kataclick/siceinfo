using System;

namespace Cemi.NotifichePreliminari.Type.Entities
{
    public class TipologiaCommittente
    {
        public Int32 IdTipologiaCommittente { get; set; }

        public String Descrizione { get; set; }

        public override String ToString()
        {
            return Descrizione;
        }
    }
}