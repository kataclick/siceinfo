using System;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

namespace Cemi.NotifichePreliminari.Type.Entities
{
    [Serializable]
    public class CommittenteNotificheTelematiche : Committente
    {
        public Int32? IdCommittenteTelematiche { get; set; }

        public Int32? IdCommittenteAnagrafica { get; set; }

        public TipologiaCommittente TipologiaCommittente { get; set; }

        public String PersonaCognome { get; set; }

        public String PersonaNome { get; set; }

        public String PersonaIndirizzo { get; set; }

        public String PersonaComune { get; set; }

        public String PersonaProvincia { get; set; }

        public String PersonaCap { get; set; }

        public String PersonaTelefono { get; set; }

        public String PersonaFax { get; set; }

        public String PersonaCellulare { get; set; }

        public String PersonaEmail { get; set; }

        public String PersonaCodiceFiscale { get; set; }

        public override String ToString()
        {
            if (!String.IsNullOrEmpty(RagioneSociale))
            {
                return RagioneSociale;
            }
            else
            {
                return String.Format("{0} {1}", PersonaCognome, PersonaNome);
            }
        }
    }
}