﻿using System;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    public class TipologiaLavoro
    {
        public Int32 Id { get; set; }

        public String Descrizione { get; set; }

        public override string ToString()
        {
            return this.Descrizione;
        }
    }
}
