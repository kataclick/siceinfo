namespace Cemi.NotifichePreliminari.Type.Entities
{
    public class Allegato
    {
        public int? IdAllegato { get; set; }

        public string NomeFile { get; set; }

        public byte[] File { get; set; }

        public int IdVisita { get; set; }
    }
}