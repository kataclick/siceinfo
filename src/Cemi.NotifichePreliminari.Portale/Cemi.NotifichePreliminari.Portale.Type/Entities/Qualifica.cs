﻿using System;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    public class Qualifica
    {
        public Int32 Id { get; set; }

        public String Descrizione { get; set; }
    }
}
