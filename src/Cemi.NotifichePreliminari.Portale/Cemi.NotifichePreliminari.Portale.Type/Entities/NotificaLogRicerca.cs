
using Cemi.NotifichePreliminari.Type.Enums;
namespace Cemi.NotifichePreliminari.Type.Entities
{
    public class LogRicerca
    {
        public int IdUtente { get; set; }

        public string XmlFiltro { get; set; }

        public SezioneLogRicerca Sezione { get; set; }
    }
}