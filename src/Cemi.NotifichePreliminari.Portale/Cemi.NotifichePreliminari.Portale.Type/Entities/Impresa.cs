﻿using System;
using Cemi.NotifichePreliminari.Portale.Type.Enums;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class Impresa
    {
        private const string SEPARATORE_CAMPO_COMPOSTO = "|";
        private string ammiCap;
        private string ammiComune;
        private string ammiIndirizzo;
        private string ammiProvincia;
        private string cap;
        private string codiceFiscale;
        private string comune;
        private DateTime? dataFineValiditaSedeAmministrativa;
        private DateTime? dataFineValiditaSedeLegale;
        private DateTime? dataInizioValiditaSedeAmministrativa;
        private DateTime? dataInizioValiditaSedeLegale;
        private string fax;
        private bool fonteNotifica;
        private int? idImpresa;
        private int? idImpresaTemporaneo;
        private string indirizzo;
        private bool lavoratoreAutonomo;
        private bool modificato;
        private string partitaIva;
        private string personaRiferimento;
        private string provincia;
        private string ragioneSociale;
        private string telefono;
        private string tipoAttivita;

        private TipologiaImpresa tipoImpresa;

        public Impresa()
        {
        }

        public Impresa(TipologiaImpresa tipoImpresa, int? idImpresa, string ragioneSociale, string indirizzo,
                       string provincia, string comune, string cap, string partitaIva, string codiceFiscale)
        {
            this.tipoImpresa = tipoImpresa;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;

            lavoratoreAutonomo = false;
            idImpresaTemporaneo = null;
        }

        public Impresa(TipologiaImpresa tipoImpresa, int? idImpresa, string ragioneSociale, string indirizzo,
                       string provincia, string comune, string cap, string partitaIva, string codiceFiscale,
                       string tipoAttivita, string telefono, string fax, string personaRiferimento,
                       bool lavoratoreAutonomo)
        {
            this.tipoImpresa = tipoImpresa;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;

            // Per parte Cpt
            this.tipoAttivita = tipoAttivita;
            this.telefono = telefono;
            this.fax = fax;
            this.personaRiferimento = personaRiferimento;
            this.lavoratoreAutonomo = lavoratoreAutonomo;
            idImpresaTemporaneo = null;
        }

        public TipologiaImpresa TipoImpresa
        {
            get { return tipoImpresa; }
            set { tipoImpresa = value; }
        }

        public int? IdImpresa
        {
            get { return idImpresa; }
            set { idImpresa = value; }
        }

        // Campo per Cpt, gestione subappalti

        public int? IdImpresaTemporaneo
        {
            get { return idImpresaTemporaneo; }
            set { idImpresaTemporaneo = value; }
        }

        public bool LavoratoreAutonomo
        {
            get { return lavoratoreAutonomo; }
            set { lavoratoreAutonomo = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string AmmiIndirizzo
        {
            get { return ammiIndirizzo; }
            set { ammiIndirizzo = value; }
        }

        public string AmmiProvincia
        {
            get { return ammiProvincia; }
            set { ammiProvincia = value; }
        }

        public string AmmiComune
        {
            get { return ammiComune; }
            set { ammiComune = value; }
        }

        public string AmmiCap
        {
            get { return ammiCap; }
            set { ammiCap = value; }
        }

        public string PartitaIva
        {
            get { return partitaIva; }
            set { partitaIva = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        //private LavoratoreCollection lavoratori;
        //public LavoratoreCollection Lavoratori
        //{
        //    get { return lavoratori; }
        //    set { lavoratori = value; }
        //}

        public string TipoAttivita
        {
            get { return tipoAttivita; }
            set { tipoAttivita = value; }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public string PersonaRiferimento
        {
            get { return personaRiferimento; }
            set { personaRiferimento = value; }
        }

        public DateTime? DataInizioValiditaSedeLegale
        {
            get { return dataInizioValiditaSedeLegale; }
            set { dataInizioValiditaSedeLegale = value; }
        }

        public DateTime? DataFineValiditaSedeLegale
        {
            get { return dataFineValiditaSedeLegale; }
            set { dataFineValiditaSedeLegale = value; }
        }

        public DateTime? DataInizioValiditaSedeAmministrativa
        {
            get { return dataInizioValiditaSedeAmministrativa; }
            set { dataInizioValiditaSedeAmministrativa = value; }
        }

        public DateTime? DataFineValiditaSedeAmministrativa
        {
            get { return dataFineValiditaSedeAmministrativa; }
            set { dataFineValiditaSedeAmministrativa = value; }
        }

        public bool FonteNotifica
        {
            get { return fonteNotifica; }
            set { fonteNotifica = value; }
        }

        public string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(provincia))
                    return String.Format("{0} {1} ({2}) {3}", indirizzo, comune, provincia, cap);
                else return String.Format("{0} {1} {2}", indirizzo, comune, cap);
            }
        }

        public string AmmiIndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(ammiProvincia))
                    return String.Format("{0} {1} ({2}) {3}", ammiIndirizzo, ammiComune, ammiProvincia, ammiCap);
                else return String.Format("{0} {1} {2}", ammiIndirizzo, ammiComune, ammiCap);
            }
        }

        /// <summary>
        /// Ritorna TipoImpresa + SEPARATORE + IdImpresa; per conoscere il separatore accedere alla proprietà SeparatoreCampoComposto
        /// </summary>
        public string IdImpresaComposto
        {
            get { return ((int)TipoImpresa) + SEPARATORE_CAMPO_COMPOSTO + IdImpresa; }
        }

        /// <summary>
        /// Ritorna il valore del separatore usato nei campi composti (es IdImpresaComposto)
        /// </summary>
        public static string SeparatoreCampoComposto
        {
            get { return SEPARATORE_CAMPO_COMPOSTO; }
        }

        public string NomeCompleto
        {
            get
            {
                //return
                //    RagioneSociale + Environment.NewLine + Indirizzo + Environment.NewLine + Comune + " - " + Provincia +
                //    " " + Cap;
                return
                    String.Format("{0}\nLeg: {1}\nAmmi:{2}", RagioneSociale, IndirizzoCompleto, AmmiIndirizzoCompleto);
            }
        }

        public bool Modificato
        {
            get { return modificato; }
            set { modificato = value; }
        }

        /// <summary>
        /// Separa l'idcomposto
        /// </summary>
        /// <param name="idImpresaComposto">Id Impresa composto</param>
        /// <param name="idImpresa">valorizza idimpresa</param>
        /// <param name="tipoImpresa">valorizza tipoimpresa</param>
        public static void SplitIdImpresaComposto(string idImpresaComposto, out int idImpresa, out int tipoImpresa)
        {
            string[] impresaIds = idImpresaComposto.Split(SeparatoreCampoComposto.ToCharArray());
            idImpresa = int.Parse(impresaIds[1]);
            tipoImpresa = int.Parse(impresaIds[0]);
        }
    }
}
