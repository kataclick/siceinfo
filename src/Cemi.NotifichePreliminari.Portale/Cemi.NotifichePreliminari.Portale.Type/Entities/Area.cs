using System;

namespace Cemi.NotifichePreliminari.Type.Entities
{
    [Serializable]
    public class Area
    {
        private Int16 idArea;
        public Int16 IdArea
        {
            get { return idArea; }
            set { idArea = value; }
        }

        private String descrizione;
        public String Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }
    }
}
