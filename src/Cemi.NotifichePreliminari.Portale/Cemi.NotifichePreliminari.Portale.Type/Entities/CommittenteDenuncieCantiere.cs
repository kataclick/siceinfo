﻿
using System;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class CommittenteDenuncieCantiere
    {
        public Int32? IdCommittente
        {
            get;
            set;
        }
        public Boolean Pubblico
        {
            get;
            set;
        }
        public String EnteRagioneSociale
        {
            get;
            set;
        }
        public String PersonaNome
        {
            get;
            set;
        }
        public String PersonaCognome
        {
            get;
            set;
        }
        public String PersonaCodiceFiscale
        {
            get;
            set;
        }
        public String EnteCodiceFiscale
        {
            get;
            set;
        }
        public String EntePartitaIva
        {
            get;
            set;
        }
        public String Indirizzo
        {
            get;
            set;
        }
        public String Comune
        {
            get;
            set;
        }
        public String Provincia
        {
            get;
            set;
        }
        public String Cap
        {
            get;
            set;
        }

        public override String ToString()
        {
            if (!String.IsNullOrEmpty(this.EnteRagioneSociale))
            {
                if (!String.IsNullOrEmpty(this.PersonaCognome))
                {
                    return String.Format("{0} ({1} {2})", this.EnteRagioneSociale, this.PersonaCognome, this.PersonaNome).Trim();
                }
                else
                {
                    return this.EnteRagioneSociale;
                }
            }

            return String.Format("{0} {1}", this.PersonaCognome, this.PersonaNome);
        }
    }
}
