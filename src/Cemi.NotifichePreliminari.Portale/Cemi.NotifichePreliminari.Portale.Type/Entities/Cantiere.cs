﻿using System;
using Cemi.NotifichePreliminari.Portale.Type.Collections;
using Cemi.NotifichePreliminari.Portale.Type.Enums;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Type.Enums;
using Indirizzo = Cemi.Geocode.Type.Indirizzo;

namespace Cemi.NotifichePreliminari.Portale.Type.Entities
{
    [Serializable]
    public class Cantiere
    {
        public String Note { get; set; }

        public DateTime Data
        {
            get;
            set;
        }
        public Int32? IdDenunciaCantiere
        {
            get;
            set;
        }
        public String NaturaOpera
        {
            get;
            set;
        }
        public Int32 Progressivo
        {
            get;
            set;
        }
        public Indirizzo IndirizzoCantiere
        {
            get;
            set;
        }
        public Decimal? AmmontareComplessivo
        {
            get;
            set;
        }
        public Decimal? AmmontareEdile
        {
            get;
            set;
        }
        public DateTime? DataInizioLavori
        {
            get;
            set;
        }
        public DateTime? DataFineLavori
        {
            get;
            set;
        }
        public CommittenteDenuncieCantiere Committente
        {
            get;
            set;
        }
        public ImpresaNotificheTelematiche Capofila
        {
            get;
            set;
        }
        public SubappaltoCollection Subappalti
        {
            get;
            set;
        }
        public TipoUtenteInserimentoDenuncia InseritoCome
        {
            get;
            set;
        }

        public String CodiceCantiere
        {
            get
            {
                String codiceProvincia = null;
                String provincia = this.IndirizzoCantiere.Provincia.ToUpper().Trim();
                switch (provincia)
                {
                    case "AREZZO":
                    case "AR":
                        codiceProvincia = "AR";
                        break;
                    case "FIRENZE":
                    case "FI":
                        codiceProvincia = "FI";
                        break;
                    case "GROSSETO":
                    case "GR":
                        codiceProvincia = "GR";
                        break;
                    case "LIVORNO":
                    case "LI":
                        codiceProvincia = "LI";
                        break;
                    case "LUCCA":
                    case "LU":
                        codiceProvincia = "LU";
                        break;
                    case "MASSA CARRARA":
                    case "MASSACARRARA":
                    case "MASSA-CARRARA":
                    case "MASSA":
                    case "CARRARA":
                    case "MS":
                        codiceProvincia = "MS";
                        break;
                    case "PISA":
                    case "PI":
                        codiceProvincia = "PI";
                        break;
                    case "PISTOIA":
                    case "PT":
                        codiceProvincia = "PT";
                        break;
                    case "PRATO":
                    case "PO":
                        codiceProvincia = "PO";
                        break;
                    case "SIENA":
                    case "SI":
                        codiceProvincia = "SI";
                        break;
                    default:
                        if (this.IndirizzoCantiere.Provincia.Length >= 2)
                        {
                            codiceProvincia = this.IndirizzoCantiere.Provincia.Substring(0, 2).ToUpper().Trim();
                        }
                        else
                        {
                            codiceProvincia = this.IndirizzoCantiere.Provincia.ToUpper().Trim();
                        }
                        break;
                }

                return String.Format("{0}/{1}/{2:D5}", codiceProvincia, this.Data.Year, this.Progressivo);
            }
        }

        public RuoloImpresa Ruolo
        {
            get;
            set;
        }

        public Boolean AppaltoPubblico
        {
            get;
            set;
        }

        public Guid GuidUtenteTelematiche
        {
            get;
            set;
        }

        public TipologiaLavoro TipologiaLavoro
        {
            get;
            set;
        }

        public String PermessoCostruire
        {
            get;
            set;
        }

        public Int32? IdNotifica
        {
            get;
            set;
        }

        public Cantiere()
        {
            //Committente = new CommittenteDenuncieCantiere();
            //Capofila = new ImpresaNotificheTelematiche();
            Subappalti = new SubappaltoCollection();
            IndirizzoCantiere = new Indirizzo();
        }
    }
}
