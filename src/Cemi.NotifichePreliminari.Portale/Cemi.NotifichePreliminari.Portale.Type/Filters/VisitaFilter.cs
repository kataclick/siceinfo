﻿using System;
using Cemi.NotifichePreliminari.Type.Entities;
using Cemi.NotifichePreliminari.Type.Enums;

namespace Cemi.NotifichePreliminari.Portale.Type.Filters
{
    public class VisitaFilter
    {
        public Int16 IdArea { get; set; }

        public Int32? IdASL { get; set; }

        public String Comune { get; set; }

        public String Indirizzo { get; set; }

        public DateTime? Data { get; set; }

        public TipologiaVisita Tipologia { get; set; }

        public EnteVisita? Ente { get; set; }

        public EsitoVisita Esito { get; set; }
    }
}
