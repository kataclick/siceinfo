using System;

namespace Cemi.NotifichePreliminari.Type.Filters
{
    [Serializable]
    public class NotificaFilter
    {
        public Int16 IdArea
        {
            get;
            set;
        }

        public DateTime? DataDal
        {
            get;
            set;
        }

        public DateTime? DataAl
        {
            get;
            set;
        }

        public Boolean? AppaltoPrivato
        {
            get;
            set;
        }

        public String Committente
        {
            get;
            set;
        }

        public String Indirizzo
        {
            get;
            set;
        }

        public String Impresa
        {
            get;
            set;
        }

        public String NaturaOpera
        {
            get;
            set;
        }

        public Decimal? Ammontare
        {
            get;
            set;
        }

        public DateTime? DataInizio
        {
            get;
            set;
        }

        public DateTime? DataFine
        {
            get;
            set;
        }

        public String FiscIva
        {
            get;
            set;
        }

        public String IndirizzoComune
        {
            get;
            set;
        }

        public String IndirizzoProvincia
        {
            get;
            set;
        }

        public String NumeroAppalto
        {
            get;
            set;
        }

        public String Cap
        {
            get;
            set;
        }

        public Guid? IdUtenteTelematiche
        {
            get;
            set;
        }

        public Int32? IdNotifica
        {
            get;
            set;
        }

        public Int32? IdIndirizzo
        {
            get;
            set;
        }

        public Int32? IdTipologiaLavoro
        {
            get;
            set;
        }

        public String ProtocolloDenuncia
        {
            get;
            set;
        }

        public Boolean Vuoto()
        {
            if (!this.DataDal.HasValue
                && !this.DataAl.HasValue
                && !this.AppaltoPrivato.HasValue
                && String.IsNullOrEmpty(this.Committente)
                && String.IsNullOrEmpty(this.Indirizzo)
                && String.IsNullOrEmpty(this.Impresa)
                && String.IsNullOrEmpty(this.NaturaOpera)
                && !this.Ammontare.HasValue
                && !this.DataInizio.HasValue
                && !this.DataFine.HasValue
                && String.IsNullOrEmpty(this.FiscIva)
                && String.IsNullOrEmpty(this.IndirizzoComune)
                && String.IsNullOrEmpty(this.IndirizzoProvincia)
                && String.IsNullOrEmpty(this.NumeroAppalto)
                && String.IsNullOrEmpty(this.Cap)
                && !this.IdNotifica.HasValue
                && !this.IdTipologiaLavoro.HasValue
                && String.IsNullOrEmpty(this.ProtocolloDenuncia))
            {
                return true;
            }

            return false;
        }
    }
}