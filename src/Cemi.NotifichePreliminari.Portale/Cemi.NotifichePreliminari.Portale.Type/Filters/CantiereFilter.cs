﻿
using System;
namespace Cemi.NotifichePreliminari.Portale.Type.Filters
{
    public class CantiereFilter
    {
        public Cemi.Geocode.Type.Indirizzo Indirizzo
        {
            get;
            set;
        }
        public String CodiceFiscaleCommittente
        {
            get;
            set;
        }
        public String CodiceFiscaleImpresa
        {
            get;
            set;
        }
        public String CodiceFiscaleImpresaAppaltatrice
        {
            get;
            set;
        }
        public String Codice
        {
            get;
            set;
        }
    }
}
