using System;

namespace Cemi.NotifichePreliminari.Type.Filters
{
    public class UtenteNotificheTelematicheFilter
    {
        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String UserName { get; set; }

        public Boolean? Abilitati { get; set; }

        public String IdCassaEdile { get; set; }
    }
}