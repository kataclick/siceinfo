using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Type.Collections
{
    [Serializable]
    public class NotificaCollection : List<Notifica>
    {
    }
}