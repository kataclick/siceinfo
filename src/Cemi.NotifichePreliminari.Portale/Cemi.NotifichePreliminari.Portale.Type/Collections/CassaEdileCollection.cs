﻿using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Collections
{
    [Serializable]
    public class CassaEdileCollection : List<CassaEdile>
    {
        public bool SonoTutteNelCircuitoCNCE()
        {
            foreach (CassaEdile cassa in this)
            {
                if (!cassa.Cnce)
                    return false;
            }

            return true;
        }

        public Boolean PresenteNellaLista(String idCassaEdile)
        {
            foreach (CassaEdile cassa in this)
            {
                if (cassa.IdCassaEdile == idCassaEdile)
                    return true;
            }

            return false;
        }
    }
}
