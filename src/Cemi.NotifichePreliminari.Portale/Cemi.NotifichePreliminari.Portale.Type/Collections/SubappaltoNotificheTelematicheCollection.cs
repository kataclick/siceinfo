using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Type.Collections
{
    [Serializable]
    public class SubappaltoNotificheTelematicheCollection : List<SubappaltoNotificheTelematiche>
    {
        public Boolean ImpresaPresente(Int32? idImpresaTelematica, Guid? idImpresaTemporaneo)
        {
            foreach (SubappaltoNotificheTelematiche sub in this)
            {
                if (sub.AppaltataDa != null
                    &&
                    (
                        (
                            sub.AppaltataDa.IdImpresaTelematica.HasValue
                            && idImpresaTelematica.HasValue
                            && sub.AppaltataDa.IdImpresaTelematica == idImpresaTelematica
                        )
                    //||
                    //(
                    //    sub.AppaltataDa.IdImpresa.HasValue
                    //    && idImpresaTelematica.HasValue
                    //    && sub.AppaltataDa.IdImpresaTelematica == idImpresaTelematica
                    //)
                        ||
                        (
                            sub.AppaltataDa.IdTemporaneo != Guid.Empty
                            && idImpresaTemporaneo.HasValue
                            && idImpresaTemporaneo.Value != Guid.Empty
                            && sub.AppaltataDa.IdTemporaneo == idImpresaTemporaneo
                        )
                    ))
                {
                    return true;
                }
            }

            return false;
        }

        public ImpresaNotificheTelematiche RecuperaImpresaPerCodiceFiscale(String codiceFiscale)
        {
            ImpresaNotificheTelematiche impresa = null;

            foreach (SubappaltoNotificheTelematiche sub in this)
            {
                if (sub.ImpresaSelezionata != null && sub.ImpresaSelezionata.CodiceFiscale == codiceFiscale)
                {
                    impresa = sub.ImpresaSelezionata;
                    break;
                }
            }

            return impresa;
        }
    }
}
