﻿using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Collections
{
    [Serializable]
    public class CantiereCollection:List<Cantiere>
    {
    }
}
