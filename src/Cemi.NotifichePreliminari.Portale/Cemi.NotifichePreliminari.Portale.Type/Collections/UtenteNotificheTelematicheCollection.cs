﻿using System.Collections.Generic;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Collections
{
    public class UtenteNotificheTelematicheCollection : List<UtenteNotificheTelematiche>
    {
    }
}
