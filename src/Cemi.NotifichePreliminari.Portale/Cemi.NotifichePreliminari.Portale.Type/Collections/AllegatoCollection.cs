using System.Collections.Generic;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Type.Collections
{
    public class AllegatoCollection : List<Allegato>
    {
        public void AddUnico(Allegato allegato)
        {
            foreach (Allegato all in this)
            {
                if (all.IdAllegato == allegato.IdAllegato)
                    return;
            }

            base.Add(allegato);
        }
    }
}