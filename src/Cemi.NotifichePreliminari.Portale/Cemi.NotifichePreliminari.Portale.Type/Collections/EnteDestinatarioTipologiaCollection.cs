﻿using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Collections
{
    public class EnteDestinatarioTipologiaCollection : List<EnteDestinatarioTipologia>
    {
        public Boolean Contains(EnteDestinatarioTipologia ente)
        {
            foreach (EnteDestinatarioTipologia enteTemp in this)
            {
                if (ente.Id == enteTemp.Id)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
