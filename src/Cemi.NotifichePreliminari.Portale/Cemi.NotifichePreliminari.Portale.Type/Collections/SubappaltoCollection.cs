﻿using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Collections
{
    [Serializable]
    public class SubappaltoCollection : List<Subappalto>
    {
        public Subappalto GetSubappaltoPerCodiceFiscale(String codiceFiscale)
        {
            foreach (Subappalto sub in this)
            {
                if (sub.ImpresaInSubappalto.CodiceFiscale.Trim().ToUpper() == codiceFiscale.Trim().ToUpper())
                {
                    return sub;
                }
            }

            return null;
        }

        public String AddWithCheck(Subappalto subappalto, String codiceFiscaleCapofila)
        {
            if (subappalto.ImpresaInSubappalto.CodiceFiscale.ToUpper() == codiceFiscaleCapofila.ToUpper())
            {
                return "L'impresa selezionata è già presente come capofila";
            }

            foreach (Subappalto sub in this)
            {
                if (subappalto.ImpresaInSubappalto.CodiceFiscale == sub.ImpresaInSubappalto.CodiceFiscale)
                {
                    return "L'impresa selezionata è già presente nei subappalti, non è possibile inserirla nuovamente";
                }
            }

            this.Add(subappalto);
            return String.Empty;
        }

        public String UpdateWithCheck(Subappalto subappalto, Int32 index, String codiceFiscaleCapofila)
        {
            if (subappalto.ImpresaInSubappalto.CodiceFiscale.ToUpper() == codiceFiscaleCapofila.ToUpper())
            {
                return "L'impresa selezionata è già presente come capofila";
            }

            for (Int32 i = 0; i < this.Count; i++)
            {
                if (subappalto.ImpresaInSubappalto.CodiceFiscale == this[i].ImpresaInSubappalto.CodiceFiscale
                    && i != index)
                {
                    return "L'impresa selezionata è già presente nei subappalti, non è possibile aggiornare il subappalto con i dati inseriti";
                }
            }

            this[index] = subappalto;
            return String.Empty;
        }
    }
}
