﻿using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Portale.Type.Entities;

namespace Cemi.NotifichePreliminari.Portale.Type.Collections
{
    [Serializable]
    public class EnteDestinatarioCollection : List<EnteDestinatario>
    {
        public List<String> IndirizziTo
        {
            get
            {
                List<String> emails = new List<String>();

                foreach (EnteDestinatario ente in this)
                {
                    if (!String.IsNullOrEmpty(ente.EmailTo))
                    {
                        String[] indSplit = ente.EmailTo.Split(';');

                        foreach (String ind in indSplit)
                        {
                            emails.Add(ind);
                        }
                    }
                }

                return emails;
            }
        }

        public List<String> IndirizziCcn
        {
            get
            {
                List<String> emails = new List<String>();

                foreach (EnteDestinatario ente in this)
                {
                    if (!String.IsNullOrEmpty(ente.EmailCcn))
                    {
                        String[] indSplit = ente.EmailCcn.Split(';');

                        foreach (String ind in indSplit)
                        {
                            emails.Add(ind);
                        }
                    }
                }

                return emails;
            }
        }

        public Int32 TipologieEntiPresenti()
        {
            EnteDestinatarioTipologiaCollection tipologieEnti = new EnteDestinatarioTipologiaCollection();

            foreach (EnteDestinatario ente in this)
            {
                if (!tipologieEnti.Contains(ente.Tipologia))
                {
                    tipologieEnti.Add(ente.Tipologia);
                }
            }

            return tipologieEnti.Count;
        }
    }
}
