using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Type.Entities;

namespace Cemi.NotifichePreliminari.Type.Collections
{
    /// <summary>
    /// Collezione di indirizzi
    /// </summary>
    [Serializable]
    public class IndirizzoCollection : List<Indirizzo>
    {
        public void AddUnico(Indirizzo indirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == indirizzo.IdIndirizzo)
                    return;
            }

            base.Add(indirizzo);
        }

        public Indirizzo GetById(Int32 idIndirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == idIndirizzo)
                    return ind;
            }

            return null;
        }
    }
}