using System;
using Cemi.Geocode.Type;
using Cemi.NotifichePreliminari.Portale.Type.Entities;
using Cemi.NotifichePreliminari.Type.Collections;

namespace Cemi.NotifichePreliminari.Type.Delegates
{
    public delegate void ResponsabileDeiLavoriEventHandler(Boolean nonNominato);

    public delegate void CoordinatoreSicurezzaProgettazioneEventHandler(Boolean nonNominato);

    public delegate void CoordinatoreSicurezzaEsecuzioneEventHandler(Boolean nonNominato);

    public delegate void CoordinatoreSicurezzaProgettazioneComeResponsabileLavoriEventHandler(Boolean stato);

    public delegate void CoordinatoreSicurezzaEsecuzioneComeResponsabileLavoriEventHandler(Boolean stato);

    public delegate void CoordinatoreSicurezzaEsecuzioneComeCoordinatoreSicurezzaProgettazioneEventHandler(Boolean stato);

    public delegate void ImpresaSelectedEventHandler();

    public delegate void ImpresaNuovaEventHandler();

    public delegate void NotificaSelectedEventHandler(Int32 idNotifica, Int32 idNotificaRiferimento);

    public delegate void SubappaltoCreatedOrDeletedEventHandler(SubappaltoNotificheTelematicheCollection subappalti);

    public delegate void IndirizzoSelectedEventHandler(Indirizzo indirizzo);

    public delegate void CodiceSelectedEventHandler(String codice);

    public delegate void CantiereSelectedEventHandler(Cantiere cantiere, bool exists);
}