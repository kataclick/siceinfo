﻿
namespace Cemi.NotifichePreliminari.Portale.Type.Enums
{
    public enum TipologiaImpresa
    {
        SiceNew = 0,
        Cantieri,
        TutteLeFonti
    }
}
