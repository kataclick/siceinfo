namespace Cemi.NotifichePreliminari.Type.Enums
{
    public enum RuoloImpresa
    {
        Capofila = 0,
        Subappalto,
        NonPresente
    }
}