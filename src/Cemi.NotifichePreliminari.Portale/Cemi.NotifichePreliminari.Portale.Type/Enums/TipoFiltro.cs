namespace Cemi.NotifichePreliminari.Type.Enums
{
    public enum TipoFiltro
    {
        RicercaNotifiche = 0,
        RicercaMappa,
        RicercaDenunce
    }
}