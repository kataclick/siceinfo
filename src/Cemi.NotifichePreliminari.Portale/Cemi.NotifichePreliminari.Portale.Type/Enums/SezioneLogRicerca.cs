namespace Cemi.NotifichePreliminari.Type.Enums
{
    public enum SezioneLogRicerca
    {
        RicercaNotifiche = 0,
        RicercaCantieri,
        LocalizzazioneCantieri
    }
}