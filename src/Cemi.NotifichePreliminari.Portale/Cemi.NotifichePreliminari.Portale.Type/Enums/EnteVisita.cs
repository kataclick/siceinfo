namespace Cemi.NotifichePreliminari.Type.Enums
{
    public enum EnteVisita
    {
        ASL = 0,
        CPT,
        DTL,
        INAIL,
        INPS,
        CassaEdile
    }
}