﻿using System;
using Org.Eurekaa.PDF.iSafePDF;

namespace Cemi.NotifichePreliminari.PDFSign
{
    public class FirmaDocumentoPDF
    {
        public static byte[] FirmaDocumento(byte[] sorgente, byte[] certificatoB, String password)
        {
            byte[] fileFirmato = null;

            Cert certificato = new Cert(certificatoB, password, null, null, null);
            PDFSigner pdfs = new PDFSigner(sorgente, fileFirmato, certificato);
            fileFirmato = pdfs.SignBinary(null, false, null);

            return fileFirmato;
        }
    }
}
