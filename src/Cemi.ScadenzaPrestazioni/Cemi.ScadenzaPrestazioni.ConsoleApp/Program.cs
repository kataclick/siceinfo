﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Cemi.ScadenzaPrestazioni.ConsoleApp.SmsServiceReference;

namespace Cemi.ScadenzaPrestazioni.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-- Inizio procedura --");
            Console.WriteLine(DateTime.Now);

            try
            {
                List<SmsDaInviare> listaSmsDaInviare = GetSmsDaInviare();
                if (listaSmsDaInviare.Count > 0)
                {
                    SmsServiceClient client = new SmsServiceClient();
                    client.InviaListaSms(listaSmsDaInviare.ToArray());
                }
            }
            catch (Exception exception)
            {
                TracciaErrore(exception.Message + " - Inner: " + exception.InnerException);
            }

            Console.WriteLine("-- Fine procedura --");
            Console.WriteLine(DateTime.Now);
        }

        private static List<SmsDaInviare> GetSmsDaInviare()
        {
            List<SmsDaInviare> listaSmsDaInviare = new List<SmsDaInviare>();

            string connectionString = ConfigurationManager.ConnectionStrings["SICE"].ConnectionString;
            int giorniAttesa = Convert.ToInt32(ConfigurationManager.AppSettings["GiorniAttesa"]);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.USP_PrestazioniInScadenzaPerSms", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@numeroGiorni", giorniAttesa);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listaSmsDaInviare.Add(new SmsDaInviare
                                                      {
                                                          Data = DateTime.Now.AddDays(1),
                                                          Destinatari = new[] {new SmsDaInviare.Destinatario{Numero = (string) reader["numero"], IdUtenteSiceInfo = reader["idUtente"] as int?}},
                                                          Testo = String.Format("Cassa Edile Milano Le ricorda di inviare al più presto i documenti già richiesti per {0}", reader["descrizione"]),
                                                          Tipologia = TipologiaSms.Prestazioni
                                                      });
                        }
                    }
                }
            }

            return listaSmsDaInviare;
        }

        private static void TracciaErrore(string messaggioErrore)
        {
            string logPrestazioniSms = ConfigurationManager.AppSettings["LogPrestazioniSms"];

            Console.WriteLine(messaggioErrore);

            if (logPrestazioniSms == "true")
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione SMS Prestazioni Scadenza: {0}", messaggioErrore));
                }
            }
        }
    }
}
