using System.Collections.Generic;
using TBridge.Cemi.InfoSindacati.Type.Entities;

namespace TBridge.Cemi.InfoSindacati.Type
{
    public class ImpreseExportParameter
    {
        private List<Impresa> imprese;
        private string fileName;

        public List<Impresa> Imprese
        {
            get { return imprese; }
            set { imprese = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
    }
}