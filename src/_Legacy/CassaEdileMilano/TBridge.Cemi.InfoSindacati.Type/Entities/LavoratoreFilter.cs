using System;
using TBridge.Cemi.InfoSindacati.Type.Enums;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class LavoratoreFilter
    {
        private int? idLavoratore;
        private Operatori idLavoratoreOperatore = Operatori.Uguale;
        private string sindacato = "Tutti";
        private string cognome;
        private string nome;
        private string codiceFiscale;
        private DateTime? dataNascita;
        private string indirizzoDenominazione;
        private string indirizzoCAP;
        private string indirizzoProvincia;
        private string indirizzoComune;
        private int? idImpresa;
        private Operatori idImpresaOperatore = Operatori.Uguale;
        private string ragioneSociale;
        private string tipoFineRapporto = "Tutti";
        private bool attivo;
        private string comprensorioSindacale = "Tutti";

        public int? IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public Operatori IdLavoratoreOperatore
        {
            get { return idLavoratoreOperatore; }
            set { idLavoratoreOperatore = value; }
        }

        public string Sindacato
        {
            get { return sindacato; }
            set { sindacato = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public DateTime? DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }

        public string IndirizzoDenominazione
        {
            get { return indirizzoDenominazione; }
            set { indirizzoDenominazione = value; }
        }

        public string IndirizzoCAP
        {
            get { return indirizzoCAP; }
            set { indirizzoCAP = value; }
        }

        public string IndirizzoProvincia
        {
            get { return indirizzoProvincia; }
            set { indirizzoProvincia = value; }
        }

        public string IndirizzoComune
        {
            get { return indirizzoComune; }
            set { indirizzoComune = value; }
        }

        public int? IdImpresa
        {
            get { return idImpresa; }
            set { idImpresa = value; }
        }

        public Operatori IdImpresaOperatore
        {
            get { return idImpresaOperatore; }
            set { idImpresaOperatore = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string TipoFineRapporto
        {
            get { return tipoFineRapporto; }
            set { tipoFineRapporto = value; }
        }

        public bool Attivo
        {
            get { return attivo; }
            set { attivo = value; }
        }

        public string ComprensorioSindacale
        {
            get { return comprensorioSindacale; }
            set { comprensorioSindacale = value; }
        }
    }
}