using System;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class Impresa
    {
        private int idImpresa;
        private string ragioneSociale;
        private string codiceFiscaleImpresa;
        private string partitaIVA;
        private string indirizzoSedeLegale;
        private string capSedeLegale;
        private string localitaSedeLegale;
        private string provinciaSedeLegale;
        private int numeroDipendenti;
        private DateTime dataDenuncia;

        public int IdImpresa
        {
            get { return idImpresa; }
            set { idImpresa = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string CodiceFiscaleImpresa
        {
            get { return codiceFiscaleImpresa; }
            set { codiceFiscaleImpresa = value; }
        }

        public string PartitaIVA
        {
            get { return partitaIVA; }
            set { partitaIVA = value; }
        }

        public string IndirizzoSedeLegale
        {
            get { return indirizzoSedeLegale; }
            set { indirizzoSedeLegale = value; }
        }

        public string CapSedeLegale
        {
            get { return capSedeLegale; }
            set { capSedeLegale = value; }
        }

        public string LocalitaSedeLegale
        {
            get { return localitaSedeLegale; }
            set { localitaSedeLegale = value; }
        }

        public string ProvinciaSedeLegale
        {
            get { return provinciaSedeLegale; }
            set { provinciaSedeLegale = value; }
        }

        public string IndirizzoCompletoSedeLegale
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoSedeLegale) && !String.IsNullOrEmpty(localitaSedeLegale) &&
                    !String.IsNullOrEmpty(capSedeLegale) && !String.IsNullOrEmpty(provinciaSedeLegale))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoSedeLegale, localitaSedeLegale, capSedeLegale,
                                      provinciaSedeLegale);
                return ret;
            }
        }

        public int NumeroDipendenti
        {
            get { return numeroDipendenti; }
            set { numeroDipendenti = value; }
        }

        public DateTime DataDenuncia
        {
            get { return dataDenuncia; }
            set { dataDenuncia = value; }
        }
    }
}