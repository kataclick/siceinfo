using System.Collections.Generic;
using TBridge.Cemi.InfoSindacati.Type.Entities;

namespace TBridge.Cemi.InfoSindacati.Type
{
    public class LavoratoriExportParameter
    {
        private List<Lavoratore> lavoratori;
        private string fileName;

        public List<Lavoratore> Lavoratori
        {
            get { return lavoratori; }
            set { lavoratori = value; }
        }

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }
    }
}