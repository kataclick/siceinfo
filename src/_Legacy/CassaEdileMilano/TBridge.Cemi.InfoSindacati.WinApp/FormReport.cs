using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using TBridge.Cemi.InfoSindacati.Business;

namespace TBridge.Cemi.InfoSindacati.WinApp
{
    public partial class FormReport : FormBase
    {
        private readonly LavoratoriManager lavoratoriManager;
        private readonly int idImpresa;

        public FormReport(int _idImpresa)
        {
            InitializeComponent();

            idImpresa = _idImpresa;
            lavoratoriManager = new LavoratoriManager();
            DataSet ds = lavoratoriManager.GetLavoratori(idImpresa);
            DataSetLavoratori d = new DataSetLavoratori();

            ReportDataSource dsl = new ReportDataSource("DataSetLavoratori_LavoratoriDeleghe", ds.Tables[0]);

            reportViewerLav.LocalReport.DataSources.Add(dsl);
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            reportViewerLav.RefreshReport();
        }
    }
}