namespace TBridge.Cemi.InfoSindacati.WinApp
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxFiltroLav = new System.Windows.Forms.GroupBox();
            this.comboBoxIdImpOp = new System.Windows.Forms.ComboBox();
            this.comboBoxIdLavOp = new System.Windows.Forms.ComboBox();
            this.checkBoxAttivo = new System.Windows.Forms.CheckBox();
            this.comboBoxComprensori = new System.Windows.Forms.ComboBox();
            this.comboBoxTipoCessazione = new System.Windows.Forms.ComboBox();
            this.comboBoxSindacato = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxCodiceFiscale = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.buttonResetLav = new System.Windows.Forms.Button();
            this.buttonFiltraLav = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxProvincia = new System.Windows.Forms.TextBox();
            this.textBoxComune = new System.Windows.Forms.TextBox();
            this.textBoxIndirizzo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxRagioneSociale = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.maskedTextBoxIdImpresa = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxIdLavoratore = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxCap = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxDataNascita = new System.Windows.Forms.MaskedTextBox();
            this.textBoxCognome = new System.Windows.Forms.TextBox();
            this.groupBoxElencoLav = new System.Windows.Forms.GroupBox();
            this.dataGridViewElencoLav = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caricaDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemReport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorkerElencoLavoratori = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelOperation = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelRow = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxFiltroImp = new System.Windows.Forms.GroupBox();
            this.comboBoxIdImpDittaOp = new System.Windows.Forms.ComboBox();
            this.buttonResetImp = new System.Windows.Forms.Button();
            this.buttonFiltraImp = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxImpRagioneSociale = new System.Windows.Forms.TextBox();
            this.textBoxImpPartitaIva = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxImpCodiceFiscale = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxImpIndirizzo = new System.Windows.Forms.TextBox();
            this.textBoxImpProvincia = new System.Windows.Forms.TextBox();
            this.maskedTextBoxImpCap = new System.Windows.Forms.MaskedTextBox();
            this.textBoxImpComune = new System.Windows.Forms.TextBox();
            this.maskedTextBoxImpIdImpresa = new System.Windows.Forms.MaskedTextBox();
            this.backgroundWorkerElencoImprese = new System.ComponentModel.BackgroundWorker();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageLavoratori = new System.Windows.Forms.TabPage();
            this.tabPageImprese = new System.Windows.Forms.TabPage();
            this.groupBoxElencoImp = new System.Windows.Forms.GroupBox();
            this.dataGridViewElencoImp = new System.Windows.Forms.DataGridView();
            this.tabPageRiepilogo = new System.Windows.Forms.TabPage();
            this.groupBoxDettagli = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxUIL = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBoxCISL = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxCGIL = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxDeleghe = new System.Windows.Forms.TextBox();
            this.textBoxSalario = new System.Windows.Forms.TextBox();
            this.textBoxCartella = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBoxParametri = new System.Windows.Forms.GroupBox();
            this.dateTimePickerDataIscrizioneA = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerDataIscrizioneDa = new System.Windows.Forms.DateTimePicker();
            this.buttonDettagli = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBoxComprensorio = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.backgroundWorkerExport = new System.ComponentModel.BackgroundWorker();
            this.groupBoxFiltroLav.SuspendLayout();
            this.groupBoxElencoLav.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewElencoLav)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBoxFiltroImp.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPageLavoratori.SuspendLayout();
            this.tabPageImprese.SuspendLayout();
            this.groupBoxElencoImp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewElencoImp)).BeginInit();
            this.tabPageRiepilogo.SuspendLayout();
            this.groupBoxDettagli.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxParametri.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxFiltroLav
            // 
            this.groupBoxFiltroLav.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFiltroLav.Controls.Add(this.comboBoxIdImpOp);
            this.groupBoxFiltroLav.Controls.Add(this.comboBoxIdLavOp);
            this.groupBoxFiltroLav.Controls.Add(this.checkBoxAttivo);
            this.groupBoxFiltroLav.Controls.Add(this.comboBoxComprensori);
            this.groupBoxFiltroLav.Controls.Add(this.comboBoxTipoCessazione);
            this.groupBoxFiltroLav.Controls.Add(this.comboBoxSindacato);
            this.groupBoxFiltroLav.Controls.Add(this.label10);
            this.groupBoxFiltroLav.Controls.Add(this.label11);
            this.groupBoxFiltroLav.Controls.Add(this.label9);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxCodiceFiscale);
            this.groupBoxFiltroLav.Controls.Add(this.label32);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxNome);
            this.groupBoxFiltroLav.Controls.Add(this.label30);
            this.groupBoxFiltroLav.Controls.Add(this.buttonResetLav);
            this.groupBoxFiltroLav.Controls.Add(this.buttonFiltraLav);
            this.groupBoxFiltroLav.Controls.Add(this.label31);
            this.groupBoxFiltroLav.Controls.Add(this.label29);
            this.groupBoxFiltroLav.Controls.Add(this.label14);
            this.groupBoxFiltroLav.Controls.Add(this.label13);
            this.groupBoxFiltroLav.Controls.Add(this.label12);
            this.groupBoxFiltroLav.Controls.Add(this.label5);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxProvincia);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxComune);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxIndirizzo);
            this.groupBoxFiltroLav.Controls.Add(this.label4);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxRagioneSociale);
            this.groupBoxFiltroLav.Controls.Add(this.label3);
            this.groupBoxFiltroLav.Controls.Add(this.label2);
            this.groupBoxFiltroLav.Controls.Add(this.label1);
            this.groupBoxFiltroLav.Controls.Add(this.maskedTextBoxIdImpresa);
            this.groupBoxFiltroLav.Controls.Add(this.maskedTextBoxIdLavoratore);
            this.groupBoxFiltroLav.Controls.Add(this.maskedTextBoxCap);
            this.groupBoxFiltroLav.Controls.Add(this.maskedTextBoxDataNascita);
            this.groupBoxFiltroLav.Controls.Add(this.textBoxCognome);
            this.groupBoxFiltroLav.Location = new System.Drawing.Point(6, 6);
            this.groupBoxFiltroLav.Name = "groupBoxFiltroLav";
            this.groupBoxFiltroLav.Size = new System.Drawing.Size(784, 222);
            this.groupBoxFiltroLav.TabIndex = 0;
            this.groupBoxFiltroLav.TabStop = false;
            this.groupBoxFiltroLav.Text = "Filtro Lavoratori";
            this.groupBoxFiltroLav.Enter += new System.EventHandler(this.groupBoxFiltroLav_Enter);
            // 
            // comboBoxIdImpOp
            // 
            this.comboBoxIdImpOp.FormattingEnabled = true;
            this.comboBoxIdImpOp.Items.AddRange(new object[] {
            "=",
            ">",
            "<"});
            this.comboBoxIdImpOp.Location = new System.Drawing.Point(132, 118);
            this.comboBoxIdImpOp.Name = "comboBoxIdImpOp";
            this.comboBoxIdImpOp.Size = new System.Drawing.Size(41, 20);
            this.comboBoxIdImpOp.TabIndex = 17;
            this.comboBoxIdImpOp.Text = "=";
            // 
            // comboBoxIdLavOp
            // 
            this.comboBoxIdLavOp.FormattingEnabled = true;
            this.comboBoxIdLavOp.Items.AddRange(new object[] {
            "=",
            ">",
            "<"});
            this.comboBoxIdLavOp.Location = new System.Drawing.Point(132, 18);
            this.comboBoxIdLavOp.Name = "comboBoxIdLavOp";
            this.comboBoxIdLavOp.Size = new System.Drawing.Size(41, 20);
            this.comboBoxIdLavOp.TabIndex = 17;
            this.comboBoxIdLavOp.Text = "=";
            // 
            // checkBoxAttivo
            // 
            this.checkBoxAttivo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxAttivo.AutoSize = true;
            this.checkBoxAttivo.Location = new System.Drawing.Point(522, 169);
            this.checkBoxAttivo.Name = "checkBoxAttivo";
            this.checkBoxAttivo.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAttivo.TabIndex = 16;
            this.checkBoxAttivo.UseVisualStyleBackColor = true;
            // 
            // comboBoxComprensori
            // 
            this.comboBoxComprensori.FormattingEnabled = true;
            this.comboBoxComprensori.Location = new System.Drawing.Point(132, 193);
            this.comboBoxComprensori.Name = "comboBoxComprensori";
            this.comboBoxComprensori.Size = new System.Drawing.Size(256, 20);
            this.comboBoxComprensori.TabIndex = 15;
            // 
            // comboBoxTipoCessazione
            // 
            this.comboBoxTipoCessazione.FormattingEnabled = true;
            this.comboBoxTipoCessazione.Location = new System.Drawing.Point(132, 168);
            this.comboBoxTipoCessazione.Name = "comboBoxTipoCessazione";
            this.comboBoxTipoCessazione.Size = new System.Drawing.Size(256, 20);
            this.comboBoxTipoCessazione.TabIndex = 15;
            // 
            // comboBoxSindacato
            // 
            this.comboBoxSindacato.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSindacato.FormattingEnabled = true;
            this.comboBoxSindacato.Location = new System.Drawing.Point(522, 143);
            this.comboBoxSindacato.Name = "comboBoxSindacato";
            this.comboBoxSindacato.Size = new System.Drawing.Size(256, 20);
            this.comboBoxSindacato.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "Codice Lavoratore";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 96);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 12);
            this.label11.TabIndex = 14;
            this.label11.Text = "CodiceFiscale";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 12);
            this.label9.TabIndex = 14;
            this.label9.Text = "Nome";
            // 
            // textBoxCodiceFiscale
            // 
            this.textBoxCodiceFiscale.Location = new System.Drawing.Point(132, 93);
            this.textBoxCodiceFiscale.Name = "textBoxCodiceFiscale";
            this.textBoxCodiceFiscale.Size = new System.Drawing.Size(256, 20);
            this.textBoxCodiceFiscale.TabIndex = 4;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 196);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(83, 12);
            this.label32.TabIndex = 11;
            this.label32.Text = "Comprensorio";
            // 
            // textBoxNome
            // 
            this.textBoxNome.Location = new System.Drawing.Point(132, 68);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.Size = new System.Drawing.Size(256, 20);
            this.textBoxNome.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 171);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 12);
            this.label30.TabIndex = 11;
            this.label30.Text = "Tipo cessazione";
            // 
            // buttonResetLav
            // 
            this.buttonResetLav.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonResetLav.Location = new System.Drawing.Point(522, 195);
            this.buttonResetLav.Name = "buttonResetLav";
            this.buttonResetLav.Size = new System.Drawing.Size(123, 21);
            this.buttonResetLav.TabIndex = 12;
            this.buttonResetLav.Text = "Reset";
            this.buttonResetLav.UseVisualStyleBackColor = true;
            this.buttonResetLav.Click += new System.EventHandler(this.buttonResetLav_Click);
            // 
            // buttonFiltraLav
            // 
            this.buttonFiltraLav.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFiltraLav.Location = new System.Drawing.Point(655, 195);
            this.buttonFiltraLav.Name = "buttonFiltraLav";
            this.buttonFiltraLav.Size = new System.Drawing.Size(123, 21);
            this.buttonFiltraLav.TabIndex = 12;
            this.buttonFiltraLav.Text = "Filtra";
            this.buttonFiltraLav.UseVisualStyleBackColor = true;
            this.buttonFiltraLav.Click += new System.EventHandler(this.buttonFiltraLav_Click);
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(424, 171);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(60, 12);
            this.label31.TabIndex = 11;
            this.label31.Text = "Solo Attivi";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(424, 146);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(59, 12);
            this.label29.TabIndex = 11;
            this.label29.Text = "Sindacato";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(424, 121);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 12);
            this.label14.TabIndex = 11;
            this.label14.Text = "Provincia";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(424, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 12);
            this.label13.TabIndex = 11;
            this.label13.Text = "CAP";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(424, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 12);
            this.label12.TabIndex = 11;
            this.label12.Text = "Comune";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(424, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 11;
            this.label5.Text = "Indirizzo";
            // 
            // textBoxProvincia
            // 
            this.textBoxProvincia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxProvincia.Location = new System.Drawing.Point(522, 118);
            this.textBoxProvincia.MaxLength = 2;
            this.textBoxProvincia.Name = "textBoxProvincia";
            this.textBoxProvincia.Size = new System.Drawing.Size(256, 20);
            this.textBoxProvincia.TabIndex = 9;
            // 
            // textBoxComune
            // 
            this.textBoxComune.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxComune.Location = new System.Drawing.Point(522, 68);
            this.textBoxComune.Name = "textBoxComune";
            this.textBoxComune.Size = new System.Drawing.Size(256, 20);
            this.textBoxComune.TabIndex = 7;
            // 
            // textBoxIndirizzo
            // 
            this.textBoxIndirizzo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIndirizzo.Location = new System.Drawing.Point(522, 43);
            this.textBoxIndirizzo.Name = "textBoxIndirizzo";
            this.textBoxIndirizzo.Size = new System.Drawing.Size(256, 20);
            this.textBoxIndirizzo.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "Ragione Sociale";
            // 
            // textBoxRagioneSociale
            // 
            this.textBoxRagioneSociale.Location = new System.Drawing.Point(132, 143);
            this.textBoxRagioneSociale.Name = "textBoxRagioneSociale";
            this.textBoxRagioneSociale.Size = new System.Drawing.Size(256, 20);
            this.textBoxRagioneSociale.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Codice Ditta";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(424, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "Data di nascita";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cognome";
            // 
            // maskedTextBoxIdImpresa
            // 
            this.maskedTextBoxIdImpresa.Location = new System.Drawing.Point(179, 118);
            this.maskedTextBoxIdImpresa.Mask = "099999999";
            this.maskedTextBoxIdImpresa.Name = "maskedTextBoxIdImpresa";
            this.maskedTextBoxIdImpresa.RejectInputOnFirstFailure = true;
            this.maskedTextBoxIdImpresa.Size = new System.Drawing.Size(209, 20);
            this.maskedTextBoxIdImpresa.TabIndex = 10;
            // 
            // maskedTextBoxIdLavoratore
            // 
            this.maskedTextBoxIdLavoratore.Location = new System.Drawing.Point(179, 18);
            this.maskedTextBoxIdLavoratore.Mask = "099999999";
            this.maskedTextBoxIdLavoratore.Name = "maskedTextBoxIdLavoratore";
            this.maskedTextBoxIdLavoratore.Size = new System.Drawing.Size(209, 20);
            this.maskedTextBoxIdLavoratore.TabIndex = 1;
            // 
            // maskedTextBoxCap
            // 
            this.maskedTextBoxCap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedTextBoxCap.Location = new System.Drawing.Point(522, 93);
            this.maskedTextBoxCap.Mask = "00000";
            this.maskedTextBoxCap.Name = "maskedTextBoxCap";
            this.maskedTextBoxCap.Size = new System.Drawing.Size(256, 20);
            this.maskedTextBoxCap.TabIndex = 8;
            // 
            // maskedTextBoxDataNascita
            // 
            this.maskedTextBoxDataNascita.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedTextBoxDataNascita.Location = new System.Drawing.Point(522, 18);
            this.maskedTextBoxDataNascita.Mask = "00/00/0000";
            this.maskedTextBoxDataNascita.Name = "maskedTextBoxDataNascita";
            this.maskedTextBoxDataNascita.Size = new System.Drawing.Size(256, 20);
            this.maskedTextBoxDataNascita.TabIndex = 5;
            this.maskedTextBoxDataNascita.ValidatingType = typeof(System.DateTime);
            // 
            // textBoxCognome
            // 
            this.textBoxCognome.Location = new System.Drawing.Point(132, 43);
            this.textBoxCognome.Name = "textBoxCognome";
            this.textBoxCognome.Size = new System.Drawing.Size(256, 20);
            this.textBoxCognome.TabIndex = 2;
            // 
            // groupBoxElencoLav
            // 
            this.groupBoxElencoLav.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxElencoLav.Controls.Add(this.dataGridViewElencoLav);
            this.groupBoxElencoLav.Location = new System.Drawing.Point(6, 233);
            this.groupBoxElencoLav.Name = "groupBoxElencoLav";
            this.groupBoxElencoLav.Size = new System.Drawing.Size(784, 142);
            this.groupBoxElencoLav.TabIndex = 1;
            this.groupBoxElencoLav.TabStop = false;
            this.groupBoxElencoLav.Text = "Elenco";
            // 
            // dataGridViewElencoLav
            // 
            this.dataGridViewElencoLav.AllowUserToAddRows = false;
            this.dataGridViewElencoLav.AllowUserToDeleteRows = false;
            this.dataGridViewElencoLav.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewElencoLav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewElencoLav.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewElencoLav.MultiSelect = false;
            this.dataGridViewElencoLav.Name = "dataGridViewElencoLav";
            this.dataGridViewElencoLav.ReadOnly = true;
            this.dataGridViewElencoLav.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridViewElencoLav.Size = new System.Drawing.Size(778, 123);
            this.dataGridViewElencoLav.TabIndex = 0;
            this.dataGridViewElencoLav.TabStop = false;
            this.dataGridViewElencoLav.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewElencoLav_CellContentDoubleClick);
            this.dataGridViewElencoLav.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewElencoLav_CellContentClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(804, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caricaDBToolStripMenuItem,
            this.toolStripSeparator1,
            this.exportToolStripMenuItem,
            this.toolStripMenuItemReport,
            this.toolStripSeparator2,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // caricaDBToolStripMenuItem
            // 
            this.caricaDBToolStripMenuItem.Name = "caricaDBToolStripMenuItem";
            this.caricaDBToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.caricaDBToolStripMenuItem.Text = "Carica DB";
            this.caricaDBToolStripMenuItem.Click += new System.EventHandler(this.caricaDBToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.exportToolStripMenuItem.Text = "Esporta Excel";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // toolStripMenuItemReport
            // 
            this.toolStripMenuItemReport.Name = "toolStripMenuItemReport";
            this.toolStripMenuItemReport.Size = new System.Drawing.Size(142, 22);
            this.toolStripMenuItemReport.Text = "Report";
            this.toolStripMenuItemReport.Click += new System.EventHandler(this.toolStripMenuItemReport_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.esciToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // backgroundWorkerElencoLavoratori
            // 
            this.backgroundWorkerElencoLavoratori.WorkerReportsProgress = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelOperation,
            this.toolStripStatusLabelRow});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(804, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelOperation
            // 
            this.toolStripStatusLabelOperation.Name = "toolStripStatusLabelOperation";
            this.toolStripStatusLabelOperation.Size = new System.Drawing.Size(733, 17);
            this.toolStripStatusLabelOperation.Spring = true;
            this.toolStripStatusLabelOperation.Text = "Ready";
            this.toolStripStatusLabelOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabelRow
            // 
            this.toolStripStatusLabelRow.Name = "toolStripStatusLabelRow";
            this.toolStripStatusLabelRow.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabelRow.Text = "Record: 0";
            // 
            // groupBoxFiltroImp
            // 
            this.groupBoxFiltroImp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFiltroImp.Controls.Add(this.comboBoxIdImpDittaOp);
            this.groupBoxFiltroImp.Controls.Add(this.buttonResetImp);
            this.groupBoxFiltroImp.Controls.Add(this.buttonFiltraImp);
            this.groupBoxFiltroImp.Controls.Add(this.label15);
            this.groupBoxFiltroImp.Controls.Add(this.label6);
            this.groupBoxFiltroImp.Controls.Add(this.label7);
            this.groupBoxFiltroImp.Controls.Add(this.textBoxImpRagioneSociale);
            this.groupBoxFiltroImp.Controls.Add(this.textBoxImpPartitaIva);
            this.groupBoxFiltroImp.Controls.Add(this.label19);
            this.groupBoxFiltroImp.Controls.Add(this.textBoxImpCodiceFiscale);
            this.groupBoxFiltroImp.Controls.Add(this.label18);
            this.groupBoxFiltroImp.Controls.Add(this.label8);
            this.groupBoxFiltroImp.Controls.Add(this.label17);
            this.groupBoxFiltroImp.Controls.Add(this.label16);
            this.groupBoxFiltroImp.Controls.Add(this.textBoxImpIndirizzo);
            this.groupBoxFiltroImp.Controls.Add(this.textBoxImpProvincia);
            this.groupBoxFiltroImp.Controls.Add(this.maskedTextBoxImpCap);
            this.groupBoxFiltroImp.Controls.Add(this.textBoxImpComune);
            this.groupBoxFiltroImp.Controls.Add(this.maskedTextBoxImpIdImpresa);
            this.groupBoxFiltroImp.Location = new System.Drawing.Point(6, 6);
            this.groupBoxFiltroImp.Name = "groupBoxFiltroImp";
            this.groupBoxFiltroImp.Size = new System.Drawing.Size(784, 149);
            this.groupBoxFiltroImp.TabIndex = 4;
            this.groupBoxFiltroImp.TabStop = false;
            this.groupBoxFiltroImp.Text = "Filtro Imprese";
            this.groupBoxFiltroImp.Enter += new System.EventHandler(this.groupBoxFiltroImp_Enter);
            // 
            // comboBoxIdImpDittaOp
            // 
            this.comboBoxIdImpDittaOp.FormattingEnabled = true;
            this.comboBoxIdImpDittaOp.Items.AddRange(new object[] {
            "=",
            ">",
            "<"});
            this.comboBoxIdImpDittaOp.Location = new System.Drawing.Point(110, 18);
            this.comboBoxIdImpDittaOp.Name = "comboBoxIdImpDittaOp";
            this.comboBoxIdImpDittaOp.Size = new System.Drawing.Size(41, 20);
            this.comboBoxIdImpDittaOp.TabIndex = 32;
            this.comboBoxIdImpDittaOp.Text = "=";
            // 
            // buttonResetImp
            // 
            this.buttonResetImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonResetImp.Location = new System.Drawing.Point(513, 118);
            this.buttonResetImp.Name = "buttonResetImp";
            this.buttonResetImp.Size = new System.Drawing.Size(123, 21);
            this.buttonResetImp.TabIndex = 31;
            this.buttonResetImp.Text = "Reset";
            this.buttonResetImp.UseVisualStyleBackColor = true;
            this.buttonResetImp.Click += new System.EventHandler(this.buttonResetImp_Click);
            // 
            // buttonFiltraImp
            // 
            this.buttonFiltraImp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFiltraImp.Location = new System.Drawing.Point(655, 118);
            this.buttonFiltraImp.Name = "buttonFiltraImp";
            this.buttonFiltraImp.Size = new System.Drawing.Size(123, 21);
            this.buttonFiltraImp.TabIndex = 30;
            this.buttonFiltraImp.Text = "Filtra";
            this.buttonFiltraImp.UseVisualStyleBackColor = true;
            this.buttonFiltraImp.Click += new System.EventHandler(this.buttonFiltraImp_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 12);
            this.label15.TabIndex = 14;
            this.label15.Text = "Partita IVA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "CodiceFiscale";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ragione Sociale";
            // 
            // textBoxImpRagioneSociale
            // 
            this.textBoxImpRagioneSociale.Location = new System.Drawing.Point(110, 43);
            this.textBoxImpRagioneSociale.Name = "textBoxImpRagioneSociale";
            this.textBoxImpRagioneSociale.Size = new System.Drawing.Size(265, 20);
            this.textBoxImpRagioneSociale.TabIndex = 21;
            // 
            // textBoxImpPartitaIva
            // 
            this.textBoxImpPartitaIva.Location = new System.Drawing.Point(110, 93);
            this.textBoxImpPartitaIva.Name = "textBoxImpPartitaIva";
            this.textBoxImpPartitaIva.Size = new System.Drawing.Size(265, 20);
            this.textBoxImpPartitaIva.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(391, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 12);
            this.label19.TabIndex = 11;
            this.label19.Text = "Provincia S. Legale";
            // 
            // textBoxImpCodiceFiscale
            // 
            this.textBoxImpCodiceFiscale.Location = new System.Drawing.Point(110, 68);
            this.textBoxImpCodiceFiscale.Name = "textBoxImpCodiceFiscale";
            this.textBoxImpCodiceFiscale.Size = new System.Drawing.Size(265, 20);
            this.textBoxImpCodiceFiscale.TabIndex = 22;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(391, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 12);
            this.label18.TabIndex = 11;
            this.label18.Text = "CAP S. Legale";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "Codice Ditta";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(391, 46);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 12);
            this.label17.TabIndex = 11;
            this.label17.Text = "Comune S. Legale";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(391, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(109, 12);
            this.label16.TabIndex = 11;
            this.label16.Text = "Indirizzo S. Legale";
            // 
            // textBoxImpIndirizzo
            // 
            this.textBoxImpIndirizzo.Location = new System.Drawing.Point(513, 18);
            this.textBoxImpIndirizzo.Name = "textBoxImpIndirizzo";
            this.textBoxImpIndirizzo.Size = new System.Drawing.Size(265, 20);
            this.textBoxImpIndirizzo.TabIndex = 24;
            // 
            // textBoxImpProvincia
            // 
            this.textBoxImpProvincia.Location = new System.Drawing.Point(513, 93);
            this.textBoxImpProvincia.MaxLength = 2;
            this.textBoxImpProvincia.Name = "textBoxImpProvincia";
            this.textBoxImpProvincia.Size = new System.Drawing.Size(265, 20);
            this.textBoxImpProvincia.TabIndex = 27;
            // 
            // maskedTextBoxImpCap
            // 
            this.maskedTextBoxImpCap.Location = new System.Drawing.Point(513, 68);
            this.maskedTextBoxImpCap.Mask = "00000";
            this.maskedTextBoxImpCap.Name = "maskedTextBoxImpCap";
            this.maskedTextBoxImpCap.Size = new System.Drawing.Size(265, 20);
            this.maskedTextBoxImpCap.TabIndex = 26;
            // 
            // textBoxImpComune
            // 
            this.textBoxImpComune.Location = new System.Drawing.Point(513, 43);
            this.textBoxImpComune.Name = "textBoxImpComune";
            this.textBoxImpComune.Size = new System.Drawing.Size(265, 20);
            this.textBoxImpComune.TabIndex = 25;
            // 
            // maskedTextBoxImpIdImpresa
            // 
            this.maskedTextBoxImpIdImpresa.Location = new System.Drawing.Point(157, 18);
            this.maskedTextBoxImpIdImpresa.Mask = "099999999";
            this.maskedTextBoxImpIdImpresa.Name = "maskedTextBoxImpIdImpresa";
            this.maskedTextBoxImpIdImpresa.Size = new System.Drawing.Size(218, 20);
            this.maskedTextBoxImpIdImpresa.TabIndex = 20;
            // 
            // backgroundWorkerElencoImprese
            // 
            this.backgroundWorkerElencoImprese.WorkerReportsProgress = true;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageLavoratori);
            this.tabControlMain.Controls.Add(this.tabPageImprese);
            this.tabControlMain.Controls.Add(this.tabPageRiepilogo);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 24);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(804, 404);
            this.tabControlMain.TabIndex = 5;
            // 
            // tabPageLavoratori
            // 
            this.tabPageLavoratori.Controls.Add(this.groupBoxFiltroLav);
            this.tabPageLavoratori.Controls.Add(this.groupBoxElencoLav);
            this.tabPageLavoratori.Location = new System.Drawing.Point(4, 21);
            this.tabPageLavoratori.Name = "tabPageLavoratori";
            this.tabPageLavoratori.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLavoratori.Size = new System.Drawing.Size(796, 379);
            this.tabPageLavoratori.TabIndex = 0;
            this.tabPageLavoratori.Text = "Lavoratori";
            this.tabPageLavoratori.UseVisualStyleBackColor = true;
            // 
            // tabPageImprese
            // 
            this.tabPageImprese.Controls.Add(this.groupBoxElencoImp);
            this.tabPageImprese.Controls.Add(this.groupBoxFiltroImp);
            this.tabPageImprese.Location = new System.Drawing.Point(4, 21);
            this.tabPageImprese.Name = "tabPageImprese";
            this.tabPageImprese.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageImprese.Size = new System.Drawing.Size(796, 379);
            this.tabPageImprese.TabIndex = 1;
            this.tabPageImprese.Text = "Imprese";
            this.tabPageImprese.UseVisualStyleBackColor = true;
            // 
            // groupBoxElencoImp
            // 
            this.groupBoxElencoImp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxElencoImp.Controls.Add(this.dataGridViewElencoImp);
            this.groupBoxElencoImp.Location = new System.Drawing.Point(6, 160);
            this.groupBoxElencoImp.Name = "groupBoxElencoImp";
            this.groupBoxElencoImp.Size = new System.Drawing.Size(784, 219);
            this.groupBoxElencoImp.TabIndex = 5;
            this.groupBoxElencoImp.TabStop = false;
            this.groupBoxElencoImp.Text = "Elenco";
            // 
            // dataGridViewElencoImp
            // 
            this.dataGridViewElencoImp.AllowUserToAddRows = false;
            this.dataGridViewElencoImp.AllowUserToDeleteRows = false;
            this.dataGridViewElencoImp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewElencoImp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewElencoImp.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewElencoImp.MultiSelect = false;
            this.dataGridViewElencoImp.Name = "dataGridViewElencoImp";
            this.dataGridViewElencoImp.ReadOnly = true;
            this.dataGridViewElencoImp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridViewElencoImp.Size = new System.Drawing.Size(778, 200);
            this.dataGridViewElencoImp.TabIndex = 0;
            this.dataGridViewElencoImp.TabStop = false;
            this.dataGridViewElencoImp.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewElencoImp_CellContentDoubleClick);
            this.dataGridViewElencoImp.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewElencoImp_CellContentClick);
            // 
            // tabPageRiepilogo
            // 
            this.tabPageRiepilogo.Controls.Add(this.groupBoxDettagli);
            this.tabPageRiepilogo.Controls.Add(this.groupBoxParametri);
            this.tabPageRiepilogo.Location = new System.Drawing.Point(4, 21);
            this.tabPageRiepilogo.Name = "tabPageRiepilogo";
            this.tabPageRiepilogo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRiepilogo.Size = new System.Drawing.Size(796, 379);
            this.tabPageRiepilogo.TabIndex = 2;
            this.tabPageRiepilogo.Text = "Riepilogo";
            this.tabPageRiepilogo.UseVisualStyleBackColor = true;
            // 
            // groupBoxDettagli
            // 
            this.groupBoxDettagli.Controls.Add(this.groupBox1);
            this.groupBoxDettagli.Controls.Add(this.label22);
            this.groupBoxDettagli.Controls.Add(this.textBoxDeleghe);
            this.groupBoxDettagli.Controls.Add(this.textBoxSalario);
            this.groupBoxDettagli.Controls.Add(this.textBoxCartella);
            this.groupBoxDettagli.Controls.Add(this.label23);
            this.groupBoxDettagli.Controls.Add(this.label24);
            this.groupBoxDettagli.Location = new System.Drawing.Point(389, 6);
            this.groupBoxDettagli.Name = "groupBoxDettagli";
            this.groupBoxDettagli.Size = new System.Drawing.Size(288, 198);
            this.groupBoxDettagli.TabIndex = 1;
            this.groupBoxDettagli.TabStop = false;
            this.groupBoxDettagli.Text = "Dettagli";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxUIL);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.textBoxCISL);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.textBoxCGIL);
            this.groupBox1.Location = new System.Drawing.Point(6, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(273, 98);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Numero iscritti";
            // 
            // textBoxUIL
            // 
            this.textBoxUIL.Location = new System.Drawing.Point(119, 68);
            this.textBoxUIL.Name = "textBoxUIL";
            this.textBoxUIL.ReadOnly = true;
            this.textBoxUIL.Size = new System.Drawing.Size(148, 20);
            this.textBoxUIL.TabIndex = 8;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(40, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(24, 12);
            this.label27.TabIndex = 11;
            this.label27.Text = "UIL";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(40, 21);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 12);
            this.label25.TabIndex = 7;
            this.label25.Text = "CGIL";
            // 
            // textBoxCISL
            // 
            this.textBoxCISL.Location = new System.Drawing.Point(119, 43);
            this.textBoxCISL.Name = "textBoxCISL";
            this.textBoxCISL.ReadOnly = true;
            this.textBoxCISL.Size = new System.Drawing.Size(148, 20);
            this.textBoxCISL.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(40, 46);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(31, 12);
            this.label26.TabIndex = 6;
            this.label26.Text = "CISL";
            // 
            // textBoxCGIL
            // 
            this.textBoxCGIL.Location = new System.Drawing.Point(119, 18);
            this.textBoxCGIL.Name = "textBoxCGIL";
            this.textBoxCGIL.ReadOnly = true;
            this.textBoxCGIL.Size = new System.Drawing.Size(148, 20);
            this.textBoxCGIL.TabIndex = 10;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 12);
            this.label22.TabIndex = 11;
            this.label22.Text = "Importo cartella";
            // 
            // textBoxDeleghe
            // 
            this.textBoxDeleghe.Location = new System.Drawing.Point(131, 43);
            this.textBoxDeleghe.Name = "textBoxDeleghe";
            this.textBoxDeleghe.ReadOnly = true;
            this.textBoxDeleghe.Size = new System.Drawing.Size(148, 20);
            this.textBoxDeleghe.TabIndex = 9;
            // 
            // textBoxSalario
            // 
            this.textBoxSalario.Location = new System.Drawing.Point(131, 18);
            this.textBoxSalario.Name = "textBoxSalario";
            this.textBoxSalario.ReadOnly = true;
            this.textBoxSalario.Size = new System.Drawing.Size(148, 20);
            this.textBoxSalario.TabIndex = 10;
            // 
            // textBoxCartella
            // 
            this.textBoxCartella.Location = new System.Drawing.Point(131, 68);
            this.textBoxCartella.Name = "textBoxCartella";
            this.textBoxCartella.ReadOnly = true;
            this.textBoxCartella.Size = new System.Drawing.Size(148, 20);
            this.textBoxCartella.TabIndex = 8;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(92, 12);
            this.label23.TabIndex = 6;
            this.label23.Text = "Importo delega";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(116, 12);
            this.label24.TabIndex = 7;
            this.label24.Text = "Imponibile salariale";
            // 
            // groupBoxParametri
            // 
            this.groupBoxParametri.Controls.Add(this.dateTimePickerDataIscrizioneA);
            this.groupBoxParametri.Controls.Add(this.dateTimePickerDataIscrizioneDa);
            this.groupBoxParametri.Controls.Add(this.buttonDettagli);
            this.groupBoxParametri.Controls.Add(this.label28);
            this.groupBoxParametri.Controls.Add(this.comboBoxComprensorio);
            this.groupBoxParametri.Controls.Add(this.label21);
            this.groupBoxParametri.Controls.Add(this.label20);
            this.groupBoxParametri.Location = new System.Drawing.Point(6, 6);
            this.groupBoxParametri.Name = "groupBoxParametri";
            this.groupBoxParametri.Size = new System.Drawing.Size(377, 126);
            this.groupBoxParametri.TabIndex = 0;
            this.groupBoxParametri.TabStop = false;
            this.groupBoxParametri.Text = "Parametri";
            // 
            // dateTimePickerDataIscrizioneA
            // 
            this.dateTimePickerDataIscrizioneA.Location = new System.Drawing.Point(136, 67);
            this.dateTimePickerDataIscrizioneA.Name = "dateTimePickerDataIscrizioneA";
            this.dateTimePickerDataIscrizioneA.Size = new System.Drawing.Size(230, 20);
            this.dateTimePickerDataIscrizioneA.TabIndex = 15;
            // 
            // dateTimePickerDataIscrizioneDa
            // 
            this.dateTimePickerDataIscrizioneDa.Location = new System.Drawing.Point(136, 43);
            this.dateTimePickerDataIscrizioneDa.Name = "dateTimePickerDataIscrizioneDa";
            this.dateTimePickerDataIscrizioneDa.Size = new System.Drawing.Size(230, 20);
            this.dateTimePickerDataIscrizioneDa.TabIndex = 14;
            this.dateTimePickerDataIscrizioneDa.Value = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            // 
            // buttonDettagli
            // 
            this.buttonDettagli.Location = new System.Drawing.Point(291, 100);
            this.buttonDettagli.Name = "buttonDettagli";
            this.buttonDettagli.Size = new System.Drawing.Size(75, 21);
            this.buttonDettagli.TabIndex = 13;
            this.buttonDettagli.Text = "Visualizza";
            this.buttonDettagli.UseVisualStyleBackColor = true;
            this.buttonDettagli.Click += new System.EventHandler(this.buttonDettagli_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(16, 72);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(99, 12);
            this.label28.TabIndex = 10;
            this.label28.Text = "Data Iscrizione A";
            // 
            // comboBoxComprensorio
            // 
            this.comboBoxComprensorio.FormattingEnabled = true;
            this.comboBoxComprensorio.Location = new System.Drawing.Point(136, 18);
            this.comboBoxComprensorio.Name = "comboBoxComprensorio";
            this.comboBoxComprensorio.Size = new System.Drawing.Size(230, 20);
            this.comboBoxComprensorio.TabIndex = 11;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 12);
            this.label21.TabIndex = 10;
            this.label21.Text = "Data Iscrizione DA";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(16, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 12);
            this.label20.TabIndex = 10;
            this.label20.Text = "Comprensorio";
            // 
            // backgroundWorkerExport
            // 
            this.backgroundWorkerExport.WorkerReportsProgress = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 450);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(812, 484);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CEMI - Informativa ai Sindacati";
            this.groupBoxFiltroLav.ResumeLayout(false);
            this.groupBoxFiltroLav.PerformLayout();
            this.groupBoxElencoLav.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewElencoLav)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBoxFiltroImp.ResumeLayout(false);
            this.groupBoxFiltroImp.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageLavoratori.ResumeLayout(false);
            this.tabPageImprese.ResumeLayout(false);
            this.groupBoxElencoImp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewElencoImp)).EndInit();
            this.tabPageRiepilogo.ResumeLayout(false);
            this.groupBoxDettagli.ResumeLayout(false);
            this.groupBoxDettagli.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxParametri.ResumeLayout(false);
            this.groupBoxParametri.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFiltroLav;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxDataNascita;
        private System.Windows.Forms.TextBox textBoxCognome;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxIndirizzo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxRagioneSociale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxElencoLav;
        private System.Windows.Forms.DataGridView dataGridViewElencoLav;
        private System.Windows.Forms.Button buttonFiltraLav;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caricaDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorkerElencoLavoratori;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupBoxFiltroImp;
        private System.Windows.Forms.Button buttonFiltraImp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxImpRagioneSociale;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker backgroundWorkerElencoImprese;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRow;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelOperation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxCodiceFiscale;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxProvincia;
        private System.Windows.Forms.TextBox textBoxComune;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxCap;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxIdImpresa;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxIdLavoratore;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxImpPartitaIva;
        private System.Windows.Forms.TextBox textBoxImpCodiceFiscale;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxImpIndirizzo;
        private System.Windows.Forms.TextBox textBoxImpProvincia;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxImpCap;
        private System.Windows.Forms.TextBox textBoxImpComune;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxImpIdImpresa;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageLavoratori;
        private System.Windows.Forms.TabPage tabPageImprese;
        private System.Windows.Forms.GroupBox groupBoxElencoImp;
        private System.Windows.Forms.DataGridView dataGridViewElencoImp;
        private System.Windows.Forms.TabPage tabPageRiepilogo;
        private System.Windows.Forms.GroupBox groupBoxParametri;
        private System.Windows.Forms.Button buttonDettagli;
        private System.Windows.Forms.ComboBox comboBoxComprensorio;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBoxDettagli;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxDeleghe;
        private System.Windows.Forms.TextBox textBoxSalario;
        private System.Windows.Forms.TextBox textBoxCartella;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBoxUIL;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxCISL;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBoxCGIL;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dateTimePickerDataIscrizioneDa;
        private System.Windows.Forms.DateTimePicker dateTimePickerDataIscrizioneA;
        private System.Windows.Forms.ComboBox comboBoxSindacato;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox checkBoxAttivo;
        private System.Windows.Forms.ComboBox comboBoxTipoCessazione;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorkerExport;
        private System.Windows.Forms.Button buttonResetLav;
        private System.Windows.Forms.Button buttonResetImp;
        private System.Windows.Forms.ComboBox comboBoxComprensori;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemReport;
        private System.Windows.Forms.ComboBox comboBoxIdLavOp;
        private System.Windows.Forms.ComboBox comboBoxIdImpOp;
        private System.Windows.Forms.ComboBox comboBoxIdImpDittaOp;
    }
}

