namespace TBridge.Cemi.InfoSindacati.WinAppLite
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            GridViewExtensions.GridFilterFactories.DefaultGridFilterFactory defaultGridFilterFactory1 = new GridViewExtensions.GridFilterFactories.DefaultGridFilterFactory();
            this.dataGridViewLav = new System.Windows.Forms.DataGridView();
            this.dgExtender = new GridViewExtensions.DataGridFilterExtender(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLav)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgExtender)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewLav
            // 
            this.dataGridViewLav.AllowUserToAddRows = false;
            this.dataGridViewLav.AllowUserToDeleteRows = false;
            this.dataGridViewLav.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewLav.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLav.Location = new System.Drawing.Point(12, 28);
            this.dataGridViewLav.Name = "dataGridViewLav";
            this.dataGridViewLav.ReadOnly = true;
            this.dataGridViewLav.Size = new System.Drawing.Size(654, 425);
            this.dataGridViewLav.TabIndex = 0;
            // 
            // dgExtender
            // 
            this.dgExtender.AutoRefreshMode = GridViewExtensions.RefreshMode.OnEnterOrLeave;
            this.dgExtender.DataGridView = this.dataGridViewLav;
            defaultGridFilterFactory1.CreateDistinctGridFilters = false;
            defaultGridFilterFactory1.DefaultGridFilterType = typeof(GridViewExtensions.GridFilters.TextGridFilter);
            defaultGridFilterFactory1.DefaultShowDateInBetweenOperator = false;
            defaultGridFilterFactory1.DefaultShowNumericInBetweenOperator = false;
            defaultGridFilterFactory1.HandleEnumerationTypes = true;
            defaultGridFilterFactory1.MaximumDistinctValues = 20;
            this.dgExtender.FilterFactory = defaultGridFilterFactory1;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 465);
            this.Controls.Add(this.dataGridViewLav);
            this.Name = "FormMain";
            this.Text = "CEMI - Informativa ai Sindacati - Lite";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLav)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgExtender)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewLav;
        private GridViewExtensions.DataGridFilterExtender dgExtender;
    }
}

