using System.Collections.Generic;
using System.Data;
using TBridge.Cemi.InfoSindacati.Data;
using TBridge.Cemi.InfoSindacati.Type.Entities;

namespace TBridge.Cemi.InfoSindacati.Business
{
    public class LavoratoriManager
    {
        private readonly DataProvider dataProvider = new DataProvider();

        public List<Lavoratore> GetLavoratori(LavoratoreFilter filter)
        {
            return dataProvider.GetLavoratori(filter);
        }

        public LavoratoreDettaglio GetLavoratore(int idLavoratore)
        {
            return dataProvider.GetLavoratore(idLavoratore);
        }

        public List<string> GetElencoSindacati()
        {
            List<string> ret = new List<string>();
            ret.Add("Tutti");
            List<string> comp = dataProvider.GetElencoSindacati();
            ret.AddRange(comp);
            ret.Add("Non iscritto");
            return ret;
        }

        public List<string> GetElencoTipoFineRapporto()
        {
            List<string> ret = new List<string>();
            ret.Add("Tutti");
            List<string> comp = dataProvider.GetElencoTipoFineRapporto();
            ret.AddRange(comp);
            return ret;
        }

        public DataSet GetLavoratori()
        {
            return dataProvider.GetLavoratori();
        }

        public DataSet GetLavoratori(int idImpresa)
        {
            return dataProvider.GetLavoratori(idImpresa);
        }
    }
}