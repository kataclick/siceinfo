using System.Collections.Generic;
using TBridge.Cemi.InfoSindacati.Data;
using TBridge.Cemi.InfoSindacati.Type.Entities;

namespace TBridge.Cemi.InfoSindacati.Business
{
    public class ImpreseManager
    {
        private readonly DataProvider dataProvider = new DataProvider();

        public List<Impresa> GetImprese(ImpresaFilter filter)
        {
            List<Impresa> imprese;

            imprese = dataProvider.GetImprese(filter);

            return imprese;
        }

        public ImpresaDettaglio GetImpresa(int idImpresa)
        {
            return dataProvider.GetImpresa(idImpresa);
        }
    }
}