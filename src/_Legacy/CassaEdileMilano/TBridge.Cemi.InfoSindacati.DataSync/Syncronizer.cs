using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;

namespace TBridge.Cemi.InfoSindacati.DataSync
{
    internal static class Syncronizer
    {
        internal static int SyncImprese(string connStringSource, string connStringDest)
        {
            int ret;

            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            ret = CopyData(sqlConn, sqlCeConn, "SELECT * FROM _ImpreseDelegheTemp", "SELECT * FROM ImpreseDeleghe");

            return ret;
        }

        internal static int SyncLavoratori(string connStringSource, string connStringDest)
        {
            int ret;

            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            ret = CopyData(sqlConn, sqlCeConn, "SELECT * FROM _LavoratoriDelegheTemp", "SELECT * FROM LavoratoriDeleghe");

            return ret;
        }

        internal static int SyncSindacati(string connStringSource, string connStringDest)
        {
            int ret;

            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            ret = CopyData(sqlConn, sqlCeConn, "SELECT DISTINCT(sindacato) FROM _LavoratoriDelegheTemp WHERE sindacato IS NOT NULL", "SELECT * FROM Sindacati");

            return ret;
        }

        internal static int SyncTipiCessazione(string connStringSource, string connStringDest)
        {
            int ret;

            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            ret = CopyData(sqlConn, sqlCeConn, "SELECT DISTINCT(tipoFineRapporto) FROM _LavoratoriDelegheTemp WHERE tipoFineRapporto IS NOT NULL", "SELECT * FROM TipiCessazione");

            return ret;
        }

        internal static int SyncComprensori(string connStringSource, string connStringDest)
        {
            int ret;

            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            ret = CopyData(sqlConn, sqlCeConn, "SELECT DISTINCT(comprensorioSindacale) FROM _LavoratoriDelegheTemp WHERE comprensorioSindacale IS NOT NULL", "SELECT * FROM ComprensoriSindacali");

            return ret;
        }

        internal static int CopyData(SqlConnection connectionSource, SqlCeConnection connectionDest, string selectSource,
                                     string selectDestination)
        {
            int ret;

            try
            {
                SqlCommand commandSource = new SqlCommand(selectSource, connectionSource);
                SqlDataAdapter source = new SqlDataAdapter(commandSource);
                DataSet data = new DataSet();
                source.Fill(data);
                for (int index = 0; index < data.Tables[0].Rows.Count; index++)
                    data.Tables[0].Rows[index].SetAdded();
                SqlCeCommand commandDest = new SqlCeCommand(selectDestination, connectionDest);
                SqlCeDataAdapter dest = new SqlCeDataAdapter(commandDest);
                SqlCeCommandBuilder builder = new SqlCeCommandBuilder(dest);
                dest.InsertCommand = builder.GetInsertCommand();
                ret = dest.Update(data);
            }
            catch
            {
                ret = -1;
            }

            return ret;
        }

        internal static void SvuotaTabelle(string connString)
        {
            const string deleteLav = "DELETE FROM LavoratoriDeleghe";
            const string deleteImp = "DELETE FROM ImpreseDeleghe";
            const string deleteSin = "DELETE FROM Sindacati";
            const string deleteCom = "DELETE FROM ComprensoriSindacali";
            const string deleteCes = "DELETE FROM TipiCessazione";

            using (SqlCeConnection sqlCeConn = new SqlCeConnection(connString))
            {
                sqlCeConn.Open();

                using (SqlCeCommand commandLav = new SqlCeCommand(deleteLav, sqlCeConn))
                {
                    commandLav.ExecuteNonQuery();
                }

                using (SqlCeCommand commandImp = new SqlCeCommand(deleteImp, sqlCeConn))
                {
                    commandImp.ExecuteNonQuery();
                }

                using (SqlCeCommand commandSin = new SqlCeCommand(deleteSin, sqlCeConn))
                {
                    commandSin.ExecuteNonQuery();
                }

                using (SqlCeCommand commandCom = new SqlCeCommand(deleteCom, sqlCeConn))
                {
                    commandCom.ExecuteNonQuery();
                }

                using (SqlCeCommand commandCes = new SqlCeCommand(deleteCes, sqlCeConn))
                {
                    commandCes.ExecuteNonQuery();
                }

                sqlCeConn.Close();
            }
        }
    }
}