using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Archidoc.Business.ArchidocWsInfinity;
using TBridge.Cemi.Archidoc.Data;
using TBridge.Cemi.Archidoc.Type.Collection;
using TBridge.Cemi.Archidoc.Type.Collections;
using TBridge.Cemi.Archidoc.Type.Entities;
using TBridge.Cemi.Archidoc.Type.Exceptions;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using WSArchidoc = TBridge.Cemi.Business.Archidoc;


namespace TBridge.Cemi.Archidoc.Business
{
    public class ArchidocBusiness
    {
        private readonly ArchidocDataAccess dataProvider = new ArchidocDataAccess();

        #region Get Collection from SICEINFO

        public CertificatoCollection GetCertificati(DateTime? data)
        {
            return dataProvider.GetCertificati(data);
        }

        public CodiceFiscaleCollection GetCodiceFiscaleCollection(DateTime? data)
        {
            return dataProvider.GetCodiceFiscaleCollection(data);
        }

        public DichiarazioneInterventoCollection GetDichiarazioniInterventi(DateTime? data)
        {
            return dataProvider.GetDichiarazioneInterventoCollection(data);
        }

        public ModuloCollection GetModuloPrestazioneCollection(DateTime? data)
        {
            return dataProvider.GetModuloPrestazioneCollection(data);
        }

        public FatturaCollection GetPrestazioniFattureCollection(DateTime? data)
        {
            return dataProvider.GetPrestazioniFatture(data);
        }

        public StatoFamigliaCollection GetStatoFamigliaCollection(DateTime? data)
        {
            return dataProvider.GetStatoFamigliaCollection(data);
        }

        public MalattiaTelematicaCollection GetMalattiaTelematicaCertificatiMedici(DateTime? dataDa, DateTime? dataA)
        {
            return dataProvider.GetMalattiaTelematicaCertificatiMedici(dataDa, dataA);
        }

        public MalattiaTelematicaCollection GetMalattiaTelematicaAltriDocumenti(DateTime? dataDa, DateTime? dataA)
        {
            return dataProvider.GetMalattiaTelematicaAltriDocumenti(dataDa, dataA);
        }

        public LibroUnicoCollection GetDocumentiSospensioneImpresaLibroUnico()
        {
            return dataProvider.GetDocumentiSospensioneImpresaLibroUnico();
        }

        public DenunciaACECollection GetDocumentiSospensioneImpresaDenunceACE()
        {
            return dataProvider.GetDocumentiSospensioneImpresaDenunceACE();
        }

        public UniemensCollection GetDocumentiSospensioneImpresaUniemens()
        {
            return dataProvider.GetDocumentiSospensioneImpresaUniemens();
        }

        #endregion

        #region Insert-Update SICEINFO (SICE type)
        private void InsertUpdateCertificati(IEnumerable<Certificato> certificati)
        {
            if (certificati != null)
                foreach (Certificato certificato in certificati)
                {
                    dataProvider.InsertUpdateCertificato(certificato);
                }
        }
        private void InsertUpdateCodiciFiscali(IEnumerable<CodiceFiscale> codiciFiscali)
        {
            if (codiciFiscali != null)
                foreach (CodiceFiscale codiceFiscale in codiciFiscali)
                {
                    dataProvider.InsertUpdateCodiceFiscale(codiceFiscale);
                }
        }
        private void InsertUpdateStatiFamiglia(IEnumerable<StatoFamiglia> statiFamiglia)
        {
            if (statiFamiglia != null)
                foreach (StatoFamiglia statofamiglia in statiFamiglia)
                {
                    dataProvider.InsertUpdateStatoFamiglia(statofamiglia);
                }
        }
        private void InsertUpdateModuli(IEnumerable<Modulo> moduli)
        {
            if (moduli != null)
                foreach (Modulo modulo in moduli)
                {
                    dataProvider.InsertUpdateModuloPrestazioni(modulo);
                }
        }
        private void InsertUpdateDichiarazioni(IEnumerable<DichiarazioneIntervento> dichiarazioni)
        {
            if (dichiarazioni != null)
                foreach (DichiarazioneIntervento dichiarazione in dichiarazioni)
                {
                    dataProvider.InsertUpdateDichiarazioneIntervento(dichiarazione);
                }
        }
        private void InsertUpdateFatture(IEnumerable<Fattura> fatture)
        {
            if (fatture != null)
                foreach (Fattura fattura in fatture)
                {
                    dataProvider.InsertUpdateFattura(fattura);
                }
        }
        private void InsertUpdateDeleghe(IEnumerable<Delega> deleghe)
        {
            if (deleghe != null)
                foreach (Delega delega in deleghe)
                {
                    dataProvider.InsertUpdateDeleghe(delega);
                }
        }

        #region Malattia telematica
        public void MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(MalattiaTelematica certificato)
        {
            dataProvider.MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(certificato);
        }

        public void MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(MalattiaTelematica certificato)
        {
            dataProvider.MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(certificato);
        }

        public void MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(MalattiaTelematicaCollection documenti)
        {
            bool documentoNonInserito = false;
            foreach (MalattiaTelematica documento in documenti)
            {
                if (!String.IsNullOrEmpty(documento.IdArchidoc))
                {
                    dataProvider.MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(documento);
                }
                else
                {
                    documentoNonInserito = true;
                }
            }

            if (documentoNonInserito)
            {
                dataProvider.InsertLog("WARNING", "Some Archidoc insertions failed - CertificatiMedici");
            }
        }

        public void LibroUnicoUpdateIdArchidoc(LibroUnicoCollection documentiSospensioni)
        {
            bool documentoNonInserito = false;
            foreach (LibroUnico documento in documentiSospensioni)
            {
                if (!String.IsNullOrEmpty(documento.IdArchidoc))
                {
                    dataProvider.ImpreseRichiesteVariazioniStatoUpdateLibroUnicoIdArchidoc(documento);
                }
                else
                {
                    documentoNonInserito = true;
                }
            }

            if (documentoNonInserito)
            {
                dataProvider.InsertLog("WARNING", "Some Archidoc insertions failed - SospensioneImpresaLibroUnico");
            }
        }

        public void UniemensUpdateIdArchidoc(UniemensCollection documentiSospensioni)
        {
            bool documentoNonInserito = false;
            foreach (Uniemens documento in documentiSospensioni)
            {
                if (!String.IsNullOrEmpty(documento.IdArchidoc))
                {
                    dataProvider.ImpreseRichiesteVariazioniStatoUpdateUniemensIdArchidoc(documento);
                }
                else
                {
                    documentoNonInserito = true;
                }
            }

            if (documentoNonInserito)
            {
                dataProvider.InsertLog("WARNING", "Some Archidoc insertions failed - SospensioneImpresaUniemens");
            }
        }

        public void DenunceACEUpdateIdArchidoc(DenunciaACECollection documentiSospensioni)
        {
            bool documentoNonInserito = false;
            foreach (DenunciaACE documento in documentiSospensioni)
            {
                if (!String.IsNullOrEmpty(documento.IdArchidoc))
                {
                    dataProvider.ImpreseRichiesteVariazioniStatoUpdateDenunceACEIdArchidoc(documento);
                }
                else
                {
                    documentoNonInserito = true;
                }
            }

            if (documentoNonInserito)
            {
                dataProvider.InsertLog("WARNING", "Some Archidoc insertions failed - SospensioneImpresaDenunceACE");
            }
        }

        public void MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(MalattiaTelematicaCollection documenti)
        {
            bool documentoNonInserito = false;
            foreach (MalattiaTelematica documento in documenti)
            {

                if (!String.IsNullOrEmpty(documento.IdArchidoc))
                {
                    dataProvider.MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(documento);
                }
                else
                {
                    documentoNonInserito = true;
                }
            }

            if (documentoNonInserito)
            {
                dataProvider.InsertLog("WARNING", "Some Archidoc insertions failed - AltriDocumenti");
            }
        }

        #endregion

        #endregion

        #region Archidoc

        /// <summary>
        /// Inserisce in Archidoc una nuova scheda e il relativo documento se presente
        /// </summary>
        public bool InsertCardWithDocumentInArchidoc(String nomeTipoDocumento, Documento documento, bool dbLog)
        {
            bool success = true;
            try
            {
                IArchidocService archidocConnector = WSArchidoc.ArchidocConnector.GetIstance();
                archidocConnector.InsertCardWithDocument("Generale", nomeTipoDocumento, documento);
            }
            catch (Exception e)
            {
                success = false;
                if (dbLog)
                {
                    dataProvider.InsertLog("ERROR", String.Format("Archidoc: card insert failed. DocumentType={0} \n{1}", nomeTipoDocumento, e.Message));
                }
            }

            return success;
        }

        /// <summary>
        /// Inserisce in Archidoc una lista di nuova schede e i relativi documento se presente
        /// I documento devono essere tutti dello stesso tipo
        /// </summary>
        public bool InsertCardsWithDocumentsInArchidoc<T>(String nomeTipoDocumento, List<T> documenti, bool dbLog) where T : Documento
        {
            bool success = true;
            try
            {
                IArchidocService archidocConnector = WSArchidoc.ArchidocConnector.GetIstance();
                archidocConnector.InsertCardsWithDocuments<T>("Generale", nomeTipoDocumento, documenti);
            }
            catch (Exception e)
            {
                success = false;
                if (dbLog)
                {
                    dataProvider.InsertLog("ERROR", String.Format("Archidoc: card insert failed. DocumentType={0} \n{1}", nomeTipoDocumento, e.Message));
                }
            }

            return success;
        }

        /// <summary>
        /// Recupera da Archidoc i dati degli ultimi giorni
        /// </summary>
        /// <param name="dataDa"></param>
        public void GetDataFromArchidoc(DateTime dataDa, DateTime dataA)
        {
            try
            {
                CodiceFiscaleCollection codiciFiscali = null;
                StatoFamigliaCollection statiFamiglia = null;
                ModuloCollection moduli = null;
                CertificatoCollection certificati = null;
                FatturaCollection fatture = null;
                DichiarazioneInterventoCollection dichiarazioni = null;

                try
                {
                    //Carichiamo i vari documenti
                    IArchidocService archidocConnector = WSArchidoc.ArchidocConnector.GetIstance();

                    dataProvider.InsertLog("STARTARCHIDOC", string.Empty);

                    codiciFiscali = archidocConnector.GetCodiciFiscali(dataDa, dataA);
                    dataProvider.InsertLog("CodiciFiscali fatto!", string.Empty);
                    statiFamiglia = archidocConnector.GetStatiDiFamiglia(dataDa, dataA);
                    dataProvider.InsertLog("StatiFamiglia fatto!", string.Empty);
                    moduli = archidocConnector.GetModuliPrestazioni(dataDa, dataA);
                    dataProvider.InsertLog("ModuliPrestazioni fatto!", string.Empty);
                    certificati = archidocConnector.GetCertificati(dataDa, dataA);
                    dataProvider.InsertLog("Certificati fatto!", string.Empty);
                    fatture = archidocConnector.GetFatture(dataDa, dataA);
                    dataProvider.InsertLog("Fatture fatto!", string.Empty);
                    dichiarazioni = archidocConnector.GetDichiarazioniIntervento(dataDa, dataA);
                    dataProvider.InsertLog("DichiarazioniIntervento fatto!", string.Empty);

                    dataProvider.InsertLog("STOPARCHIDOC", string.Empty);
                }
                catch (ArchidocWebServiceException ex)
                {
                    dataProvider.InsertLog("ERROR", ex.Message);
                }

                try
                {
                    dataProvider.InsertLog("STARTSICE", string.Empty);

                    InsertUpdateCodiciFiscali(codiciFiscali);
                    InsertUpdateStatiFamiglia(statiFamiglia);
                    InsertUpdateModuli(moduli);
                    InsertUpdateCertificati(certificati);
                    InsertUpdateFatture(fatture);
                    InsertUpdateDichiarazioni(dichiarazioni);

                    dataProvider.InsertLog("STOPSICE", string.Empty);
                }
                catch (ArchidocUpdateSiceInfoException ex)
                {
                    dataProvider.InsertLog("ERROR", ex.Message);
                }
            }
            catch (Exception ex)
            {
                dataProvider.InsertLog("ERROR", ex.Message);
            }
        }

        #endregion

        /// <summary>
        /// Recupera i dati da SICEINFO modificati da una certa data in poi
        /// </summary>
        /// <param name="data"></param>
        public void GetDataFromSiceInfo(DateTime data)
        {
            try
            {
                dataProvider.InsertLog("STARTARCHIDOCUPDATE", string.Empty);

                //Carichiamo i vari documenti
                IArchidocService archidocConnector = WSArchidoc.ArchidocConnector.GetIstance();

                CodiceFiscaleCollection codiciFiscali = dataProvider.GetCodiceFiscaleCollection(data);
                StatoFamigliaCollection statiFamiglia = dataProvider.GetStatoFamigliaCollection(data);
                ModuloCollection moduli = dataProvider.GetModuloPrestazioneCollection(data);
                CertificatoCollection certificati = dataProvider.GetCertificati(data);
                FatturaCollection fatture = dataProvider.GetPrestazioniFatture(data);
                DichiarazioneInterventoCollection dichiarazioni = dataProvider.GetDichiarazioneInterventoCollection(data);


                foreach (CodiceFiscale codiceFiscale in codiciFiscali)
                {
                    archidocConnector.UpdateCard(codiceFiscale, "Generale", "Codice Fiscale");
                }

                foreach (StatoFamiglia statofamiglia in statiFamiglia)
                {
                    archidocConnector.UpdateCard(statofamiglia, "Generale", "Stato di Famiglia");
                }

                foreach (Modulo modulo in moduli)
                {
                    archidocConnector.UpdateCard(modulo, "Generale", "Prestazioni");
                }

                foreach (Certificato certificato in certificati)
                {
                    archidocConnector.UpdateCard(certificato, "Generale", "Certificati");
                }

                foreach (Fattura fattura in fatture)
                {
                    archidocConnector.UpdateCard(fattura, "Generale", "Fatture");
                }

                foreach (DichiarazioneIntervento dichiarazioneIntervento in dichiarazioni)
                {
                    archidocConnector.UpdateCard(dichiarazioneIntervento, "Generale", "Dettaglio Fatture");
                }

                dataProvider.InsertLog("STOPARCHIDOCUPDATE", string.Empty);
            }
            catch (Exception ex)
            {
                dataProvider.InsertLog("ERROR", ex.Message);
            }


        }

        /// <summary>
        /// Prende l'ultima data elaborazione
        /// </summary>
        /// <returns></returns>
        public DateTime? GetDataElaborazione()
        {
            return dataProvider.GetDataElaborazione();
        }

        /// <summary>
        /// Inserisce un record nella tabella ArchidocLog
        /// </summary>
        /// <returns></returns>
        public void InsertLog(String tipoElaborazione, String messaggio)
        {
            dataProvider.InsertLog(tipoElaborazione, messaggio);
        }

        #region Metodi gestione deleghe
        public Int32? GetMinDelega()
        {
            return dataProvider.GetMinDelega();
        }
        public Int32? GetMaxDelega()
        {
            return dataProvider.GetMaxDelega();
        }
        public void GetDelegheFromArchidoc(Int32 delegaDa, Int32 delegaA)
        {
            try
            {

                DelegaCollection deleghe = null;

                try
                {
                    //Carichiamo i vari documenti
                    IArchidocService archidocConnector = WSArchidoc.ArchidocConnector.GetIstance();

                    dataProvider.InsertLog("STARTARCHIDOC_Deleghe", string.Empty);

                    deleghe = archidocConnector.GetDeleghe(delegaDa, delegaA);

                    dataProvider.InsertLog("STOPARCHIDOC_Deleghe", "da: " + delegaDa.ToString() + " a: " + delegaA.ToString() + " Count: " + deleghe.Count.ToString()/* string.Empty*/);
                }
                catch (ArchidocWebServiceException ex)
                {
                    dataProvider.InsertLog("ERROR_Deleghe", ex.Message);
                }

                try
                {
                    dataProvider.InsertLog("STARTSICE_Deleghe", string.Empty);

                    InsertUpdateDeleghe(deleghe);

                    dataProvider.InsertLog("STOPSICE_Deleghe", string.Empty);
                }
                catch (ArchidocUpdateSiceInfoException ex)
                {
                    dataProvider.InsertLog("ERROR_Deleghe", ex.Message);
                }
            }
            catch (Exception ex)
            {
                dataProvider.InsertLog("ERROR_Deleghe", ex.Message);
            }
        }
        #endregion

        #region Gestione allegati deleghe

        public void AllegaBusteDeleghe(DateTime dateFromUtc, DateTime dateToUtc, String notaAllegatoBusta)
        {
            const String NOME_PROCEDURA = "BusteDeleghe";
            const String NOME_TIPO_DOCUMENTO_DELEGHE = "Deleghe";
            int addModified = 0;
            int deleted = 0;
            List<Card> cards = null;

            try
            {

                dataProvider.InsertLog(String.Format("START_{0}", NOME_PROCEDURA), String.Empty);

                cards = GetCardsWithModifiedInsertedAttachment(dateFromUtc, dateToUtc, NOME_TIPO_DOCUMENTO_DELEGHE);

                if (cards != null)
                {
                    Delega delega;
                    foreach (Card item in cards)
                    {
                        delega = null;
                        delega = dataProvider.GetDelegaByProgressivoAnnuo(item.ProgressivoAnnuo);
                        if (delega != null)
                        {
                            IArchidocService archidocConnector = WSArchidoc.ArchidocConnector.GetIstance();
                            AllegatoCollection allegati = archidocConnector.GetAllegati(delega.IdArchidoc, false);
                            Allegato allegatoBusta = allegati.FirstOrDefault(x => x.Note.Equals(notaAllegatoBusta, StringComparison.CurrentCultureIgnoreCase));

                            if (allegatoBusta != null)
                            {
                                dataProvider.UpdateDelegaAllegatoBusta(delega, allegatoBusta.NomeFile);
                                addModified++;
                            }
                            else
                            {
                                //LOG
                                //dataProvider.InsertLog(String.Format("WARNING_{0}", NOME_PROCEDURA),
                                //                        String.Format("Delega {0} : allegato con campo Nota=\"{1}\" non presente in Archidoc", item.ProgressivoAnnuo, notaAllegatoBusta));

                                // Allegato cancellato o insermento allegato lettera
                                dataProvider.UpdateDelegaAllegatoBusta(delega, null);
                                deleted++;
                            }

                        }
                        else
                        {
                            //LOG
                            dataProvider.InsertLog(String.Format("WARNING_{0}", NOME_PROCEDURA),
                                                    String.Format("Delega {0} non presente in SiceInfo", item.ProgressivoAnnuo));
                        }
                    }
                }

                dataProvider.InsertLog(String.Format("END_{0}", NOME_PROCEDURA),
                    cards != null ? String.Format("Processati {0} allegati busta: {1} inseriti/modificati {2} cancellati/mancanti", cards.Count, addModified, deleted) : String.Empty);
            }
            catch (Exception exc)
            {
                dataProvider.InsertLog(String.Format("ERROR_{0}", NOME_PROCEDURA), exc.Message);
            }
        }

        private static List<Card> GetCardsWithModifiedInsertedAttachment(DateTime dateFromUtc, DateTime dateToUtc, String NOME_TIPO_DOCUMENTO_DELEGHE)
        {
            List<Card> cards;

            //Da provare
            //using (ArchidocServiceInfinityClient archidocServiceInfinity = new ArchidocServiceInfinityClient())
            //{
            //    //Chiamo il ws con un intervallo di un giorno alla volta per problemi di timeuot sul tempo di risposta.
            //    TimeSpan giorniTimeSpan = dateToUtc.Date.Subtract(dateFromUtc.Date);
            //    int days = giorniTimeSpan.Days;

            //    if (days == 0)
            //    {
            //        cards = archidocServiceInfinity.GetCardsWithModifiedAttachment(NOME_TIPO_DOCUMENTO_DELEGHE, dateFromUtc, dateToUtc);
            //    }
            //    else
            //    {
            //        cards = new List<Card>();
            //        for (int giornoScansione = 0; giornoScansione <= days; giornoScansione++)
            //        {
            //            DateTime partialFrom;
            //            DateTime partialTo;
            //            if (giornoScansione == 0)
            //            {
            //                partialFrom = dateFromUtc;
            //                partialTo = dateFromUtc.Date.AddDays(giornoScansione + 1);
            //            }
            //            else if (giornoScansione + 1 == days)
            //            {
            //                partialFrom = dateFromUtc.Date.AddDays(giornoScansione);
            //                partialTo = dateToUtc;
            //            }
            //            else
            //            {
            //                partialFrom = dateFromUtc.Date.AddDays(giornoScansione);
            //                partialTo = dateFromUtc.Date.AddDays(giornoScansione + 1);
            //            }

            //            // Restituisce le schede di tipo documentType  per cui � stato inserito o modificato almeno un allegato nel periodo tra dateFromUtc e dateToUtc
            //            List<Card> cardsTemp = archidocServiceInfinity.GetCardsWithModifiedAttachment(NOME_TIPO_DOCUMENTO_DELEGHE, partialFrom, partialTo);
            //            cards.AddRange(cardsTemp);
            //        }
            //    }
            //}

            using (ArchidocServiceInfinityClient archidocServiceInfinity = new ArchidocServiceInfinityClient())
            {
                // Restituisce le schede di tipo documentType  per cui � stato inserito o modificato almeno un allegato nel periodo tra dateFromUtc e dateToUtc
                cards = archidocServiceInfinity.GetCardsWithModifiedAttachment(NOME_TIPO_DOCUMENTO_DELEGHE, dateFromUtc, dateToUtc);
            }
            return cards;
        }

        #endregion
    }
}