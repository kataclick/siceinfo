using TBridge.Cemi.SmsInfo.Business.Carrier.it.mdchannels.mdc71;
using TBridge.Cemi.SmsInfo.Type.Entities;
using TBridge.Cemi.SmsInfo.Type.Collections;

namespace TBridge.Cemi.SmsInfo.Business.Carrier
{
    public class SmsCarrier
    {
        private string Username { get; set; }

        private string Password { get; set; }

        public SmsSender CarrierSmsSender { get; set; }

        //private it.laureloak.services.SMSSender carrierSmsSender;

        //private it.laureloak.services.SMSSender CarrierSmsSender
        //{
        //    get { return carrierSmsSender; }
        //    set { carrierSmsSender = value; }
        //}

        public SmsCarrier(string username, string password)
        {
            Username = username;
            Password = password;

            CarrierSmsSender = new SmsSender();
        }

        public SmsCollection GetListaSmsRicevuti()
        {
            SmsCollection smsRicevuti = null;
            st_MessaggioRicevuto[] smsRicevutiCarrier = CarrierSmsSender.MessaggiRicevuti(Username, Password);
            if (smsRicevutiCarrier.LongLength > 0)
            {
                smsRicevuti = new SmsCollection();
                for (int i = 0; i < smsRicevutiCarrier.Length; i++)
                {
                    ReceivedSms smsRicevuto = new ReceivedSms();
                    smsRicevuto.IdCarrier = smsRicevutiCarrier[i].IDMessaggio;
                    smsRicevuto.Mittente = smsRicevutiCarrier[i].Mittente;
                    if (!smsRicevuto.Mittente.StartsWith("+39"))
                        smsRicevuto.Mittente = "+39" + smsRicevuto.Mittente;
                    smsRicevuto.Data = smsRicevutiCarrier[i].DataRicezione;
                    smsRicevuto.Testo = smsRicevutiCarrier[i].Testo;
                    smsRicevuti.Add(smsRicevuto);
                }
            }

            return smsRicevuti;
        }

        public int RegistraSms(SentSms sms)
        {
            int idCarrier;

            string[] destinatari = new string[sms.Destinatari.Count];
            int i = 0;
            foreach (SmsAddressee destinatario in sms.Destinatari)
            {
                destinatari[i] = destinatario.Numero;
                if (!destinatari[i].StartsWith("+39"))
                    destinatari[i] = "+39" + destinatari[i];
                i++;
            }
            idCarrier = CarrierSmsSender.InserisciMessaggio(Username, Password, 1, "Cassa Edile", sms.Testo, null, null, destinatari, false);

            return idCarrier;
        }

        public int InviaSms(SentSms sms)
        {
            return CarrierSmsSender.InviaMessaggio(Username, Password, sms.IdCarrier);
        }

        public SmsCollection GetListaSmsInviati()
        {
            SmsCollection smsInviati = null;
            st_RapportoInvioMessaggio[] smsInviatiCarrier = CarrierSmsSender.RapportoInviiAziendali(Username, Password);
            if (smsInviatiCarrier.LongLength > 0)
            {
                smsInviati = new SmsCollection();
                for (int i = 0; i < smsInviatiCarrier.Length; i++)
                {
                    SentSms smsInviato = new SentSms();
                    smsInviato.IdCarrier = smsInviatiCarrier[i].IDMessaggio;
                    smsInviato.IdInvioCarrier = smsInviatiCarrier[i].IDMessaggioInvio;
                    smsInviato.Data = smsInviatiCarrier[i].DataInvio;
                    smsInviato.Testo = smsInviatiCarrier[i].Corpo;
                    smsInviati.Add(smsInviato);
                }
            }

            return smsInviati;
        }

        public bool SetAck(int idSmsCarrier)
        {
            bool ret = false;
            
            if (CarrierSmsSender.SetAck(idSmsCarrier) != -1)
                ret = true;
            
            return ret;
        }
    }
}