using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TimbraturaSelesta = TBridge.Cemi.AccessoCantieri.SelestaProvider.Timbratura;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class SelestaConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        public Int32 CaricaTimbrature(TimbraturaSelesta[] timbratureSelesta)
        {
            Int32 ret;

            TimbraturaCollection timbrature = new TimbraturaCollection();

            foreach (TimbraturaSelesta t in timbratureSelesta)
            {
                try
                {
                    Timbratura timbratura = new Timbratura
                                                {
                                                    CodiceFiscale = t.datolettura.ToUpper().Trim(),
                                                    CodiceRilevatore = t.idterminale.Trim(),
                                                    DataOra = DateTime.Parse(t.tempo),
                                                    IngressoUscita = t.inout.ToString().Trim() == "0",
                                                    RagioneSociale = "Click & Find",
                                                    Latitudine = Decimal.Parse(t.latitudine),
                                                    Longitudine = Decimal.Parse(t.longitudine)
                                                };

                    //return - 2;

                    timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.ClickFind, timbratura.CodiceRilevatore, timbratura.DataOra);

                    if (timbratura.IdCantiere != -1)
                        timbratura.PartitaIvaImpresa = _biz.GetImpresaByIdCantiereCodFiscLav(
                            timbratura.IdCantiere.Value, timbratura.CodiceFiscale, timbratura.DataOra);

                    if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.ClickFind) == -1)
                        timbrature.Add(timbratura);
                }
                catch (Exception)
                {
                    continue;
                }
            }

            if (timbrature.Count == 0)
                ret = -1;
            else
                ret = _biz.InsertTimbrature(timbrature, TipologiaFornitore.ClickFind);

            return ret;
        }
    }
}