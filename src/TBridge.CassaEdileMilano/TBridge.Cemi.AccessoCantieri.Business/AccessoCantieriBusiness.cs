﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Data;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Collections.CexChange;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.EmailClient;
using TBridge.Cemi.Business.SmsServiceReference;
using TBridge.Cemi.Type.Collections;
using ImpresaCollection = TBridge.Cemi.AccessoCantieri.Type.Collections.ImpresaCollection;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class AccessoCantieriBusiness
    {
        private readonly Common _commonBiz = new Common();
        private readonly AccessoCantieriData _dataAccess = new AccessoCantieriData();

        public static void CaricaComuni(DropDownList dropDownListComuni, string provincia,
                                        DropDownList dropDownListFrazioni)
        {
            Common bizCommon = new Common();

            //svuotiamo e aggiumgiamo l'item vuoto
            dropDownListComuni.Items.Clear();
            dropDownListComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (dropDownListFrazioni != null)
                dropDownListFrazioni.Items.Clear();

            if (!string.IsNullOrEmpty(provincia))
            {
                ComuneSiceNewCollection comuni = bizCommon.GetComuniSiceNew(provincia);
                dropDownListComuni.DataSource = comuni;
                dropDownListComuni.DataTextField = "Comune";
                dropDownListComuni.DataValueField = "CodicePerCombo";
            }

            dropDownListComuni.DataBind();
        }

        public static string GetCodiceCatastaleDaCombo(string selectedValue)
        {
            return SplittaValoreCombo(selectedValue);
        }

        private static string SplittaValoreCombo(string selectedValue)
        {
            string codice = null;

            string[] splitted = selectedValue.Split('|');
            if (splitted.Length > 1)
                codice = splitted[0];

            return codice;
        }

        public void CaricaProvinceInDropDown(DropDownList dropDownProvince)
        {
            DataTable dtProvince = _commonBiz.GetProvince();

            dropDownProvince.Items.Clear();
            dropDownProvince.Items.Add(new ListItem(string.Empty, null));

            dropDownProvince.DataSource = dtProvince;
            dropDownProvince.DataTextField = "sigla";
            dropDownProvince.DataValueField = "idProvincia";

            dropDownProvince.DataBind();
        }

        public void CaricaComuniInDropDown(DropDownList dropDownComuni, int idProvincia)
        {
            dropDownComuni.Items.Clear();
            dropDownComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idProvincia > 0)
            {
                DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

                dropDownComuni.DataSource = dtComuni;
                dropDownComuni.DataTextField = "denominazione";
                dropDownComuni.DataValueField = "idComune";
            }

            dropDownComuni.DataBind();
        }

        public void CaricaCapInDropDown(DropDownList dropDownCap, Int64 idComune)
        {
            dropDownCap.Items.Clear();
            dropDownCap.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idComune > 0)
            {
                DataTable dtCap = _commonBiz.GetCAPDelComune(idComune);

                dropDownCap.DataSource = dtCap;
                dropDownCap.DataTextField = "cap";
                dropDownCap.DataValueField = "cap";
            }

            dropDownCap.DataBind();
        }

        public void CambioSelezioneDropDownProvincia(DropDownList ddlProvincia, DropDownList ddlComune,
                                                     DropDownList ddlCap)
        {
            int idProvincia = -1;

            if (ddlProvincia.SelectedValue != null)
                Int32.TryParse(ddlProvincia.SelectedValue, out idProvincia);

            CaricaComuniInDropDown(ddlComune, idProvincia);

            CaricaCapInDropDown(ddlCap, -1);
        }

        public void CambioSelezioneDropDownComune(DropDownList ddlComune, DropDownList ddlCap)
        {
            long idComune = -1;

            if (ddlComune.SelectedValue != null)
                Int64.TryParse(ddlComune.SelectedValue, out idComune);

            CaricaCapInDropDown(ddlCap, idComune);
        }

        public string GetImpresaByIdCantiereCodFiscLav(Int32? idCantiere, string codiceFiscale, DateTime dataOra)
        {
            return _dataAccess.GetImpresaByIdCantiereCodFiscLav(idCantiere, codiceFiscale, dataOra);
        }

        public ImpresaCollection GetimpreseOrdinate(int? idImpresaParam,
                                                    string ragioneSocialeParam, string comuneParam,
                                                    string indirizzoParam, string ivaFiscale, string stringExpression,
                                                    string sortDirection)
        {
            ImpresaCollection listaImprese = null;

            try
            {
                listaImprese =
                    _dataAccess.GetImprese(idImpresaParam, ragioneSocialeParam, comuneParam, indirizzoParam, ivaFiscale,
                                           stringExpression, sortDirection);
            }
            catch
            {
                return listaImprese;
            }

            return listaImprese;
        }

        public void LogSelezioneImpresaSubappalto(int idUtente, Subappalto subappalto)
        {
            if ((subappalto.Appaltante != null && subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                ||
                (subappalto.Appaltata != null && subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew))
            {
                _dataAccess.LogSelezioneImpresaSubappalto(idUtente, subappalto);
            }
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIva, string codiceFiscale)
        {
            bool[] res = null;

            try
            {
                res = _dataAccess.EsisteIvaFiscImpresa(partitaIva, codiceFiscale);
            }
            catch
            {
                return res;
            }

            return res;
        }

        public LavoratoreCollection GetLavoratoriOrdinati( /*TipologiaLavoratore tipoLavoratore,*/
            int? idLavoratore, string cognome, string nome, DateTime? dataNascita, string codiceFiscale,
            DateTime? dataAssunzione, DateTime? dataCessazione, string sortExpression,
            string direct, int? idImpresa)
        {
            LavoratoreCollection listaLavoratori = null;

            try
            {
                listaLavoratori =
                    _dataAccess.GetLavoratoriOrdinatiReader( /*tipoLavoratore,*/
                        idLavoratore, cognome, nome, dataNascita, codiceFiscale, dataAssunzione, dataCessazione,
                        sortExpression, direct, idImpresa);
            }
            catch
            {
                //Valla: brutto...
                return listaLavoratori;
            }

            return listaLavoratori;
        }

        public WhiteListImpresa GetLavoratoriInDomandaPerImpresa(Int32 idDomanda, Int32? idImpresa,
                                                                 Int32? idAttestatoImpresa)
        {
            return _dataAccess.GetLavoratoriInDomandaPerImpresa(idDomanda, idImpresa, idAttestatoImpresa);
        }

        public WhiteListImpresaCollection GetLavoratoriInDomanda(Int32 idDomanda)
        {
            return _dataAccess.GetLavoratoriInDomanda(idDomanda);
        }

        public byte[] GetFotoLavoratoreByLavoratoreImpresaCantiere(Int32 idLavoratore, TipologiaLavoratore tipoLavoratore, Int32 idImpresa, TipologiaImpresa tipoImpresa, Int32 idCantiere)
        {
            return _dataAccess.GetFotoLavoratoreByLavoratoreImpresaCantiere(idLavoratore, tipoLavoratore, idImpresa, tipoImpresa, idCantiere);
        }

        public Boolean UpdateFotoLavoratoreByLavoratoreImpresaCantiere(Int32 idLavoratore, TipologiaLavoratore tipoLavoratore, Int32 idImpresa, TipologiaImpresa tipoImpresa, Int32 idCantiere, byte[] foto)
        {
            return _dataAccess.UpdateFotoLavoratoreByLavoratoreImpresaCantiere(idLavoratore, tipoLavoratore, idImpresa, tipoImpresa, idCantiere, foto);
        }

        public WhiteListImpresaCollection GetLavoratoriInDomandaNoFoto(Int32 idDomanda)
        {
            return _dataAccess.GetLavoratoriInDomandaNoFoto(idDomanda);
        }

        public string[] GetLavoratoreByCodFisc(string codiceFiscale)
        {
            return _dataAccess.GetLavoratoreByCodFisc(codiceFiscale);
        }

        //public void ImportaExcelINPS(string path)
        //{
        //    this._dataAccess.ImportaExcelINPS(path);
        //}

        public void ImportaExcelINPS2(string pathScaricati, string pathElaborati)
        {
            this._dataAccess.ImportaExcelINPS2(pathScaricati, pathElaborati);
        }

        public WhiteListCollection GetDomandeByFilter(WhiteListFilter filtro)
        {
            return _dataAccess.GetDomandeByFilter(filtro);
        }

        public WhiteList GetDomandaByKey(int idDomanda)
        {
            return _dataAccess.GetDomandaByKey(idDomanda);
        }

        public ImpresaCollection GetImpreseSelezionateInSubappalto(Int32 idDomanda)
        {
            return _dataAccess.GetImpreseSelezionateInSubappalto(idDomanda);
        }

        public ImpresaCollection GetImpreseAutonomeSelezionateInSubappalto(Int32 idDomanda)
        {
            return _dataAccess.GetImpreseAutonomeSelezionateInSubappalto(idDomanda);
        }

        public bool InsertDomanda(WhiteList domanda)
        {
            return _dataAccess.InsertDomanda(domanda);
        }

        public bool UpdateDomanda(WhiteList domanda)
        {
            Boolean res = _dataAccess.UpdateDomanda(domanda);

            // Se ci sono dei rilevatori terminati di Tymbro
            // cancello la white list
            if (res)
            {
                try
                {
                    TymbroConnector tConn = new TymbroConnector();

                    foreach (RilevatoreCantiere ril in domanda.Rilevatori)
                    {
                        if (ril.RagioneSociale == "Tymbro"
                            && ril.Terminato)
                        {
                            tConn.DeleteWhitelist(ril.CodiceRilevatore);
                        }
                    }
                }
                catch { }
            }

            return res;
        }

        public AltraPersonaCollection GetAltrePersone(Int32 idDomanda)
        {
            return _dataAccess.GetAltrePersone(idDomanda);
        }

        public ReferenteCollection GetReferenti(Int32 idDomanda)
        {
            return _dataAccess.GetReferenti(idDomanda);
        }

        public ControlloIdentitaCollection GetControlloIdentita(ControlloIdentitaFilter filtro)
        {
            return _dataAccess.GetControlloIdentita(filtro);
        }

        public ControlloWhiteListCollection GetControlliWhiteList(ControlloIdentitaFilter filtro)
        {
            return _dataAccess.GetControlliWhiteList(filtro);
        }

        public TimbraturaCollection GetTimbrature2(Int32 idAccessoCantieriWhiteList)
        {
            return _dataAccess.GetTimbrature2(idAccessoCantieriWhiteList);
        }

        public TimbraturaCollection GetTimbratureTrexom()
        {
            return _dataAccess.GetTimbratureTrexom();
        }

        public TimbraturaCollection GetTimbratureByFilter(TimbraturaFilter filtro)
        {
            return _dataAccess.GetTimbratureByFilter(filtro);
        }

        public Int32 GetCountTimbrature(string partitaIva, string codiceFiscale, int idCantiere)
        {
            return _dataAccess.GetCountTimbrature(partitaIva, codiceFiscale, idCantiere);
        }

        private void UpdateTimbratura(int idTimbratura, TipologiaAnomalia tipoAnomalia)
        {
            _dataAccess.UpdateTimbratura(idTimbratura, tipoAnomalia);
        }

        public void UpdateTimbraturaAnomalieDenunce(int idTimbratura, TipologiaAnomaliaDenuncia tipoAnomaliaDenuncia)
        {
            _dataAccess.UpdateTimbraturaAnomalieDenunce(idTimbratura, tipoAnomaliaDenuncia);
        }

        public void UpdateTimbraturaAnomalieDebiti(int idTimbratura, bool debiti)
        {
            _dataAccess.UpdateTimbraturaAnomalieDebiti(idTimbratura, debiti);
        }

        public RilevatoreCantiereCollection GetRilevatoriCantieri(Int32 idCantiere)
        {
            return _dataAccess.GetRilevatoriCantieri(idCantiere);
        }

        public RilevatoreCantiereCollection GetRilevatoriLiberiCantieri()
        {
            return _dataAccess.GetRilevatoriLiberiCantieri();
        }

        public Boolean InsertRilevatore(String codiceRilevatore, Int32 idFornitore, Boolean invioWhiteList)
        {
            return _dataAccess.InsertRilevatore(codiceRilevatore, idFornitore, invioWhiteList);
        }

        public FornitoreCollection GetFornitori()
        {
            return _dataAccess.GetFornitori();
        }

        public Int32 InsertTimbrature(TimbraturaCollection timbrature, TipologiaFornitore fornitore)
        {
            int ret = _dataAccess.InsertTimbrature(timbrature);

            foreach (Timbratura timb in timbrature)
            {
                int idRilevatore = _dataAccess.GetIdRilevatore(timb.CodiceRilevatore, timb.RagioneSociale, timb.DataOra);

                if (idRilevatore != -1)
                    ControllaTimbratura(timb, fornitore);
            }

            return ret;
        }

        public void ControllaTimbratureNonGestite()
        {
            TimbraturaCollection timbrature = GetTimbratureNonGestite();

            foreach (Timbratura timb in timbrature)
            {
                int idRilevatore = _dataAccess.GetIdRilevatore(timb.CodiceRilevatore, timb.RagioneSociale, timb.DataOra);

                if (idRilevatore != -1)
                    ControllaTimbratura(timb, timb.Fornitore);
            }
        }

        //public void ControllaTimbratureNonGestiteNoCom()
        //{
        //    TimbraturaCollection timbrature = GetTimbratureNonGestite();

        //    foreach (Timbratura timb in timbrature)
        //    {
        //        int idRilevatore = _dataAccess.GetIdRilevatore(timb.CodiceRilevatore, timb.RagioneSociale, timb.DataOra);

        //        if (idRilevatore != -1)
        //            ControllaTimbraturaNoCom(timb);
        //    }
        //}

        public List<string> GetCodFiscWhiteListByCodRilevatore(Int32 idRilevatore)
        {
            return _dataAccess.GetCodFiscWhiteListByCodRilevatore(idRilevatore);
        }

        public Dictionary<String, Boolean> GetCodFiscWhiteListCompletaByCodRilevatore(Int32 idRilevatore)
        {
            return _dataAccess.GetCodFiscWhiteListCompletaByCodRilevatore(idRilevatore);
        }

        public RilevatoreCollection GetRilevatori()
        {
            return _dataAccess.GetRilevatori();
        }

        public RilevatoreCollection GetRilevatoriAttiviByIdFornitore(int idFornitore)
        {
            return _dataAccess.GetRilevatoriAttiviByIdFornitore(idFornitore);
        }

        public CantiereCollection GetCantieriByIdImpresa(int idImpresa)
        {
            return _dataAccess.GetCantieriByIdImpresa(idImpresa);
        }

        public ImpresaCollection GetSubappaltateByIdImpresa(int idImpresa, int idCantiere)
        {
            return _dataAccess.GetSubappaltateByIdImpresa(idImpresa, idCantiere);
        }

        private void InsertComunicazione(int? idSms, int? idEmail, int idCantiere, int? idReferente, int? idAltraPersona)
        {
            _dataAccess.InsertComunicazione(idSms, idEmail, idCantiere, idReferente, idAltraPersona);
        }

        public Int32 GetIdTimbratura(Timbratura timbratura, TipologiaFornitore fornitore)
        {
            return _dataAccess.GetIdTimbratura(timbratura, fornitore);
        }

        private TimbraturaCollection GetTimbratureNonGestite()
        {
            return _dataAccess.GetTimbratureNonGestite();
        }

        public List<string> GetTipologieAttivita()
        {
            List<string> tipologieAttivita = null;

            try
            {
                tipologieAttivita = _dataAccess.GetTipologieAttivita();
            }
            catch
            {
                return tipologieAttivita;
            }

            return tipologieAttivita;
        }

        public Impresa GetImpresaControlli(int idCantiere, string codiceFiscale, string partitaIva)
        {
            return _dataAccess.GetImpresaControlli(idCantiere, codiceFiscale, partitaIva);
        }

        public LavoratoreCollection GetLavoratoreControlli(int idCantiere, string codiceFiscale, int? idImpresa)
        {
            return _dataAccess.GetLavoratoreControlli(idCantiere, codiceFiscale, idImpresa);
        }

        public Int32 GetCodiceCe(string codiceFiscale, int? idImpresa, int? idCantiere)
        {
            return _dataAccess.GetCodiceCe(codiceFiscale, idImpresa, idCantiere);
        }

        public Nominativo GetNominativo(int codiceCe)
        {
            return _dataAccess.GetNominativo(codiceCe);
        }

        private WhiteList GetDomandaByRilevatoreData(string codiceRilevatore, DateTime dataOra)
        {
            return _dataAccess.GetDomandaByRilevatoreData(codiceRilevatore, dataOra);
        }

        public Statistica GetStatistica(Int32? idCantiere, Int32? mese, Int32? anno, DateTime? dataDa, DateTime? dataA,
                                        Int32? idFornitore, string codiceRilevatore)
        {
            return _dataAccess.GetStatistica(idCantiere, mese, anno, dataDa, dataA, idFornitore, codiceRilevatore);
        }

        public Denuncia GetDenuncia(int anno, int mese, int idImpresa)
        {
            return _dataAccess.GetDenuncia(anno, mese, idImpresa);
        }

        public Int32 GetIdCantiere(TipologiaFornitore fornitore, String codiceRilevatore, DateTime dataOra)
        {
            return _dataAccess.GetIdCantiere(fornitore, codiceRilevatore, dataOra);
        }

        public RapportoLavoratoreImpresa GetRapportoLavoratoreImpresa(int idImpresa, int idLavoratore,
                                                                      DateTime dataDenuncia)
        {
            return _dataAccess.GetRapportoLavoratoreImpresa(idImpresa, idLavoratore, dataDenuncia);
        }

        private OreDenunciate GetOreDenunciate(int idImpresa, int idLavoratore, DateTime dataDenuncia, string tipoOra)
        {
            return _dataAccess.GetOreDenunciate(idImpresa, idLavoratore, dataDenuncia, tipoOra);
        }

        public TimbraturaCollection GetTimbratureAnomalieDenunce()
        {
            return _dataAccess.GetTimbratureAnomalieDenunce();
        }

        public TimbraturaCollection GetTimbratureAnomalieDebiti()
        {
            return _dataAccess.GetTimbratureAnomalieDebiti();
        }

        private Boolean ImpresaRegolareBni(int idImpresa)
        {
            return _dataAccess.ImpresaRegolareBni(idImpresa);
        }

        private Boolean ImpresaRegolareFreccia(int idImpresa, int anno, int mese, int tolleranza)
        {
            return _dataAccess.ImpresaRegolareFreccia(idImpresa, anno, mese, tolleranza);
        }

        private Boolean ImpresaRegolareSaldoContabile(int idImpresa, int anno, int mese, int tolleranza)
        {
            return _dataAccess.ImpresaRegolareSaldoContabile(idImpresa, anno, mese, tolleranza);
        }

        public int? GetIdLavoratore(string codiceFiscale)
        {
            return _dataAccess.GetIdLavoratore(codiceFiscale);
        }

        public void UpdateDataStampaBadge(int idDomanda, int? idLavoratore, int? idAccessoCantieriLavoratore,
                                          int? idImpresa, int? idAccessoCantieriImpresa)
        {
            _dataAccess.UpdateDataStampaBadge(idDomanda, idLavoratore, idAccessoCantieriLavoratore, idImpresa,
                                              idAccessoCantieriImpresa, DateTime.Now);
        }

        public void UpdateDataStampaBadgeAutonomo(int idAccessoCantieriImpresa)
        {
            _dataAccess.UpdateDataStampaBadgeAutonomo(idAccessoCantieriImpresa);
        }

        //da mettere nel common
        public string GetCodiceCatastale(string nazione)
        {
            return _dataAccess.GetCodiceCatastale(nazione);
        }

        //public void FillINPS()
        //{
        //    _dataAccess.FillINPS();
        //}

        //public void tmpPULISCI()
        //{
        //    _dataAccess.tmpPULISCI();
        //}

        //public void ImportaExcelINPS(string path)
        //{
        //    _dataAccess.ImportaExcelINPS(path);
        //}

        //public void ImportaExcelINPS2(string pathScaricati, string pathElaborati)
        //{
        //    _dataAccess.ImportaExcelINPS2(pathScaricati, pathElaborati);
        //}

        //public void CreaExcelINPS(string path)
        //{
        //    _dataAccess.CreaExcelINPS(path);
        //}

        //public void ImportaExcelINPSTemp(string path)
        //{
        //    _dataAccess.ImportaExcelINPSTemp(path);
        //}

        public TipologiaAnomaliaDenuncia ControlloDenunce(int anno, int mese, int idImpresa, int? idLavoratore)
        {
            Denuncia denuncia = GetDenuncia(anno, mese, idImpresa);

            if (denuncia != null)
            {
                if (idLavoratore.HasValue)
                {
                    RapportoLavoratoreImpresa rapporto = GetRapportoLavoratoreImpresa(idImpresa, idLavoratore.Value,
                                                                                      denuncia.DataDenuncia);
                    if (rapporto != null)
                    {
                        OreDenunciate ore = GetOreDenunciate(idImpresa, idLavoratore.Value, denuncia.DataDenuncia, "001");

                        if (ore != null)
                        {
                            if (ore.OreDichiarate > 0)
                                return TipologiaAnomaliaDenuncia.No;
                            return TipologiaAnomaliaDenuncia.OreNonCoerenti;
                        }
                        return TipologiaAnomaliaDenuncia.OreNonCoerenti;
                    }
                    return TipologiaAnomaliaDenuncia.LavoratoreNonPresente;
                }
                return TipologiaAnomaliaDenuncia.LavoratoreNonPresente;
            }
            return TipologiaAnomaliaDenuncia.NonPresentata;
        }

        private static void InviaEmail(string destinatario, DateTime dataOra, string rilevatore, string codFisc, string nomeLavoratore, string cognomeLavoratore)
        {
            // Per fare delle prove
            //destinatario = "a.mosto@tbridge.it";

            if (!string.IsNullOrEmpty(destinatario))
            {
                string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
                string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

                StringBuilder sbPlain = new StringBuilder();
                StringBuilder sbHtml = new StringBuilder();

                EmailInfoService service = new EmailInfoService();
                EmailMessageSerializzabile email = new EmailMessageSerializzabile();

                sbPlain.Append("Attenzione,");
                sbPlain.Append("\n\n");
                sbPlain.Append(
                    "è stata rilevata un’entrata/uscita in cantiere non autorizzata con le seguenti specifiche:\n");
                sbPlain.Append("\n\n");
                if (nomeLavoratore != string.Empty && cognomeLavoratore != string.Empty)
                {
                    sbPlain.Append(string.Format("Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2} Nome:{3} Cognome:{4}\n",
                                                 dataOra, rilevatore,
                                                 codFisc, nomeLavoratore, cognomeLavoratore));
                }
                else
                {
                    sbPlain.Append(string.Format("Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2}\n", dataOra, rilevatore,
                                            codFisc));
                }
                sbPlain.Append("\n\n");
                sbPlain.Append(
                    "Si consiglia di effettuare le opportune verifiche e in caso di entrata/uscita di personale autorizzato aggiungere il/ i  nominativo/i  nella lista degli accessi consentiti. In caso contrario si ricorda che le anomalie riscontrate dovranno essere regolarizzate entro la fine del mese nel quale sono state evidenziate. La regolarizzazione deve essere segnalata a Cassa Edile. \n");
                sbPlain.Append("\n\n");
                sbPlain.Append("Cordiali saluti.\n");
                sbPlain.Append("\n\n");
                sbPlain.Append("Cassa Edile di Milano\n");
                email.BodyPlain = sbPlain.ToString();

                sbHtml.Append("Attenzione,");
                sbHtml.Append("<br /><br />");
                sbHtml.Append(
                    "è stata rilevata un’entrata/uscita in cantiere non autorizzata con le seguenti specifiche:<br />");
                sbHtml.Append("<br /><br />");
                if (nomeLavoratore != string.Empty && cognomeLavoratore != string.Empty)
                {
                    sbHtml.Append(string.Format(
                        "Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2} Nome:{3} Cognome:{4}<br />", dataOra, rilevatore,
                        codFisc, nomeLavoratore, cognomeLavoratore));
                }
                else
                {
                    sbHtml.Append(string.Format("Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2}<br />", dataOra, rilevatore,
                                           codFisc));
                }
                sbHtml.Append("<br /><br />");
                sbHtml.Append(
                    "Si consiglia di effettuare le opportune verifiche e in caso di entrata/uscita di personale autorizzato aggiungere il/ i  nominativo/i  nella lista degli accessi consentiti. In caso contrario si ricorda che le anomalie riscontrate dovranno essere regolarizzate entro la fine del mese nel quale sono state evidenziate. La regolarizzazione deve essere segnalata a Cassa Edile. <br />");
                sbHtml.Append("<br /><br />");
                sbHtml.Append("Cordiali saluti.<br />");
                sbHtml.Append("<br /><br />");
                sbHtml.Append("Cassa Edile di Milano<br />");
                email.BodyHTML = sbHtml.ToString();

                email.Destinatari = new List<EmailAddress> { new EmailAddress() };
                email.Destinatari[0].Indirizzo = destinatario;
                //email.Destinatari[0].Nome = iscrizione.RagioneSociale;

                email.Mittente = new EmailAddress
                                     {
                                         Indirizzo = "accessocantieri@cassaedilemilano.it",
                                         Nome = "Accesso cantieri Cassa Edile di Milano"
                                     };

                email.Oggetto = "Cassa Edile di Milano - Servizio di monitoraggio degli accessi al cantiere";

                email.DataSchedulata = DateTime.Now;
                email.Priorita = MailPriority.High;

                NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
                service.Credentials = credentials;

                service.InviaEmail(email);
            }
        }

        private void ControllaTimbratura(Timbratura timb, TipologiaFornitore fornitore)
        {
            int idTimbratura = GetIdTimbratura(timb, fornitore);
            timb.IdTimbratura = idTimbratura;

            string[] nominativo = GetLavoratoreByCodFisc(timb.CodiceFiscale);
            string nomeLavoratore = nominativo[0];
            string cognomeLavoratore = nominativo[1];

            WhiteList whiteList = GetDomandaByRilevatoreData(timb.CodiceRilevatore, timb.DataOra);

            if (whiteList.IdWhiteList != null)
            {
                WhiteListImpresaCollection wliColl = GetLavoratoriInDomanda(whiteList.IdWhiteList.Value);

                whiteList.Lavoratori = wliColl;
            }

            if (whiteList.IdWhiteList != null)
            {
                AltraPersonaCollection altrePers = GetAltrePersone(whiteList.IdWhiteList.Value);

                whiteList.ListaAltrePersone = altrePers;
            }

            if (whiteList.IdWhiteList != null)
            {
                ReferenteCollection referenti = GetReferenti(whiteList.IdWhiteList.Value);

                whiteList.ListaReferenti = referenti;
            }

            if (whiteList.Subappalti.ControlloTimbratureImpresa(timb.PartitaIvaImpresa, timb.DataOra) ==
                TipologiaAnomalia.PeriodoNonValido)
            {
                UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.PeriodoNonValido);
                //messaggi in caso di anomalia

                List<string> lista = new List<string>();

                if (fornitore != TipologiaFornitore.WebService && fornitore != TipologiaFornitore.Manuale)
                {
                    foreach (Referente referente in whiteList.ListaReferenti)
                    {
                        if (referente.ModalitaContatto)
                        {
                            string numero = Common.PulisciNumeroCellulare(referente.Telefono);
                            if (Common.NumeroCellulareValido(numero))
                                lista.Add(numero);
                        }
                        else
                        {
                            try
                            {
                                InviaEmail(referente.Email, timb.DataOra, timb.CodiceRilevatore, timb.CodiceFiscale.ToUpper(), nomeLavoratore, cognomeLavoratore);
                            }
                            catch { }
                        }
                    }

                    string testo =
                        string.Format(
                            "Ingresso/uscita in periodo non valido.Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2} {3} {4} Non rispondere al presente messaggio",
                            timb.DataOra, timb.CodiceRilevatore, timb.CodiceFiscale.ToUpper(), nomeLavoratore, cognomeLavoratore);

                    /*
                    List<SmsDaInviare> listaSmsDaInviare = lista.Select(numeroTelefono => new SmsDaInviare
                                                                                              {
                                                                                                  Data = DateTime.Now,
                                                                                                  Tipologia =
                                                                                                      TipologiaSms.Pin,
                                                                                                  Testo = testo,
                                                                                                  IdUtenteSiceInfo = null,
                                                                                                  Numero = numeroTelefono
                                                                                              }).ToList();
                     */

                    SmsDaInviare.Destinatario[] destinatari = (from numero in lista
                                                               select new SmsDaInviare.Destinatario
                                                                          {
                                                                              Numero = numero,
                                                                              IdUtenteSiceInfo = null
                                                                          }).ToArray();

                    SmsDaInviare smsDaInviare = new SmsDaInviare
                                                    {
                                                        Data = DateTime.Now,
                                                        Tipologia = TipologiaSms.AccessoCantieri,
                                                        Testo = testo.Replace("?", "").Replace("\\", "").Replace("_", ""),
                                                        Destinatari = destinatari
                                                    };


                    SmsServiceClient client = new SmsServiceClient();
                    bool invio = false;

                    try
                    {
                        if (destinatari.Length > 0)
                            invio = client.InviaSms(smsDaInviare);
                    }
                    catch { }

                    foreach (Referente referente2 in whiteList.ListaReferenti)
                    {
                        if (referente2.ModalitaContatto)
                        {
                            if (invio)
                                if (whiteList.IdWhiteList != null)
                                    InsertComunicazione(0, null, whiteList.IdWhiteList.Value, referente2.IdReferente, null);
                        }
                        else
                        {
                            if (whiteList.IdWhiteList != null)
                                InsertComunicazione(null, 0, whiteList.IdWhiteList.Value, referente2.IdReferente, null);
                        }
                    }
                }
            }
            else
            {
                if ((whiteList.Lavoratori.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                     TipologiaAnomalia.PeriodoNonValido) ||
                    (whiteList.ListaAltrePersone.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                     TipologiaAnomalia.PeriodoNonValido) ||
                    (whiteList.ListaReferenti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                     TipologiaAnomalia.PeriodoNonValido) ||
                    (whiteList.Subappalti.ControlloTimbratureImpresa(timb.CodiceFiscale, timb.DataOra) ==
                     TipologiaAnomalia.PeriodoNonValido))
                {

                    if ((whiteList.ListaAltrePersone.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                         TipologiaAnomalia.No) ||
                        (whiteList.ListaReferenti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                         TipologiaAnomalia.No))
                    {
                        UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.No);
                    }
                    else
                    {
                        UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.PeriodoNonValido);

                        if (fornitore != TipologiaFornitore.WebService && fornitore != TipologiaFornitore.Manuale)
                        {
                            //messaggi in caso di anomalia
                            List<string> lista = new List<string>();

                            foreach (Referente referente in whiteList.ListaReferenti)
                            {
                                if (referente.ModalitaContatto)
                                {
                                    string numero = Common.PulisciNumeroCellulare(referente.Telefono);
                                    if (Common.NumeroCellulareValido(numero))
                                        lista.Add(numero);
                                }
                                else
                                {
                                    try
                                    {
                                        InviaEmail(referente.Email, timb.DataOra, timb.CodiceRilevatore,
                                                   timb.CodiceFiscale.ToUpper(), nomeLavoratore, cognomeLavoratore);
                                    }
                                    catch { }
                                }
                            }

                            string testo =
                                string.Format(
                                    "Ingresso/uscita in periodo non valido.Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2} {3} {4} Non rispondere al presente messaggio",
                                    timb.DataOra, timb.CodiceRilevatore, timb.CodiceFiscale.ToUpper(), nomeLavoratore, cognomeLavoratore);

                            /*
                        List<SmsDaInviare> listaSmsDaInviare = lista.Select(numeroTelefono => new SmsDaInviare
                                                                                                  {
                                                                                                      Data = DateTime.Now,
                                                                                                      Tipologia =
                                                                                                          TipologiaSms.Pin,
                                                                                                      Testo = testo,
                                                                                                      IdUtenteSiceInfo = null,
                                                                                                      Numero = numeroTelefono
                                                                                                  }).ToList();
                         */

                            SmsDaInviare.Destinatario[] destinatari = (from numero in lista
                                                                       select new SmsDaInviare.Destinatario
                                                                                  {
                                                                                      Numero = numero,
                                                                                      IdUtenteSiceInfo = null
                                                                                  }).ToArray();

                            SmsDaInviare smsDaInviare = new SmsDaInviare
                                                            {
                                                                Data = DateTime.Now,
                                                                Tipologia = TipologiaSms.AccessoCantieri,
                                                                Testo = testo.Replace("?", "").Replace("\\", "").Replace("_", ""),
                                                                Destinatari = destinatari
                                                            };


                            SmsServiceClient client = new SmsServiceClient();
                            bool invio = false;

                            try
                            {
                                if (destinatari.Length > 0)
                                    invio = client.InviaSms(smsDaInviare);
                            }
                            catch { }

                            foreach (Referente referente2 in whiteList.ListaReferenti)
                            {
                                if (referente2.ModalitaContatto)
                                {
                                    if (invio)
                                        if (whiteList.IdWhiteList != null)
                                            InsertComunicazione(0, null, whiteList.IdWhiteList.Value, referente2.IdReferente,
                                                                null);
                                }
                                else
                                {
                                    if (whiteList.IdWhiteList != null)
                                        InsertComunicazione(null, 0, whiteList.IdWhiteList.Value, referente2.IdReferente,
                                                            null);
                                }
                            }
                        }
                    }
                }
                else if ((whiteList.Lavoratori.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                          TipologiaAnomalia.No) ||
                         (whiteList.ListaAltrePersone.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                          TipologiaAnomalia.No) ||
                         (whiteList.ListaReferenti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
                          TipologiaAnomalia.No) ||
                         (whiteList.Subappalti.ControlloTimbratureImpresa(timb.CodiceFiscale, timb.DataOra) ==
                          TipologiaAnomalia.No))
                {
                    UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.No);
                }
                else
                {
                    UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.NonInLista);

                    if (fornitore != TipologiaFornitore.WebService && fornitore != TipologiaFornitore.Manuale)
                    {
                        List<string> lista = new List<string>();

                        foreach (Referente referente in whiteList.ListaReferenti)
                        {
                            if (referente.ModalitaContatto)
                            {
                                string numero = Common.PulisciNumeroCellulare(referente.Telefono);
                                if (Common.NumeroCellulareValido(numero))
                                    lista.Add(numero);
                            }
                            else
                            {
                                try
                                {
                                    InviaEmail(referente.Email, timb.DataOra, timb.CodiceRilevatore,
                                               timb.CodiceFiscale.ToUpper(), nomeLavoratore, cognomeLavoratore);
                                }
                                catch { }
                            }
                        }

                        string testo =
                            string.Format(
                                "Ingresso/uscita non autorizzato.Timbratura:{0} Rilevatore:{1} Cod. Fisc:{2} {3} {4} Non rispondere al presente messaggio",
                                timb.DataOra, timb.CodiceRilevatore, timb.CodiceFiscale.ToUpper(), nomeLavoratore, cognomeLavoratore);

                        /*
                    List<SmsDaInviare> listaSmsDaInviare = lista.Select(numeroTelefono => new SmsDaInviare
                                                                                              {
                                                                                                  Data = DateTime.Now,
                                                                                                  Tipologia =
                                                                                                      TipologiaSms.Pin,
                                                                                                  Testo = testo,
                                                                                                  IdUtenteSiceInfo = null,
                                                                                                  Numero = numeroTelefono
                                                                                              }).ToList();
                     */

                        SmsDaInviare.Destinatario[] destinatari = (from numero in lista
                                                                   select new SmsDaInviare.Destinatario
                                                                              {
                                                                                  Numero = numero,
                                                                                  IdUtenteSiceInfo = null
                                                                              }).ToArray();

                        SmsDaInviare smsDaInviare = new SmsDaInviare
                                                        {
                                                            Data = DateTime.Now,
                                                            Tipologia = TipologiaSms.AccessoCantieri,
                                                            Testo = testo.Replace("?", "").Replace("\\", "").Replace("_", ""),
                                                            Destinatari = destinatari
                                                        };


                        SmsServiceClient client = new SmsServiceClient();
                        bool invio = false;

                        try
                        {
                            if (destinatari.Length > 0)
                                invio = client.InviaSms(smsDaInviare);
                        }
                        catch { }

                        foreach (Referente referente2 in whiteList.ListaReferenti)
                        {
                            if (referente2.ModalitaContatto)
                            {
                                if (invio)
                                    if (whiteList.IdWhiteList != null)
                                        InsertComunicazione(0, null, whiteList.IdWhiteList.Value, referente2.IdReferente,
                                                            null);
                            }
                            else
                            {
                                if (whiteList.IdWhiteList != null)
                                    InsertComunicazione(null, 0, whiteList.IdWhiteList.Value, referente2.IdReferente, null);
                            }
                        }
                    }
                }
            }
        }

        //private void ControllaTimbraturaNoCom(Timbratura timb)
        //{
        //    int idTimbratura = GetIdTimbratura(timb);
        //    timb.IdTimbratura = idTimbratura;

        //    WhiteList whiteList = GetDomandaByRilevatoreData(timb.CodiceRilevatore, timb.DataOra);

        //    if (whiteList.IdWhiteList != null)
        //    {
        //        WhiteListImpresaCollection wliColl = GetLavoratoriInDomanda(whiteList.IdWhiteList.Value);

        //        whiteList.Lavoratori = wliColl;
        //    }

        //    if (whiteList.IdWhiteList != null)
        //    {
        //        AltraPersonaCollection altrePers = GetAltrePersone(whiteList.IdWhiteList.Value);

        //        whiteList.ListaAltrePersone = altrePers;
        //    }

        //    if (whiteList.IdWhiteList != null)
        //    {
        //        ReferenteCollection referenti = GetReferenti(whiteList.IdWhiteList.Value);

        //        whiteList.ListaReferenti = referenti;
        //    }

        //    if (whiteList.Subappalti.ControlloTimbratureImpresa(timb.PartitaIVAImpresa, timb.DataOra) ==
        //        TipologiaAnomalia.PeriodoNonValido)
        //    {
        //        UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.PeriodoNonValido);
        //    }
        //    else
        //    {
        //        if ((whiteList.Lavoratori.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //             TipologiaAnomalia.PeriodoNonValido) ||
        //            (whiteList.ListaAltrePersone.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //             TipologiaAnomalia.PeriodoNonValido) ||
        //            (whiteList.ListaReferenti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //             TipologiaAnomalia.PeriodoNonValido) ||
        //            (whiteList.Subappalti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //             TipologiaAnomalia.PeriodoNonValido))
        //        {
        //            UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.PeriodoNonValido);
        //        }
        //        else if ((whiteList.Lavoratori.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //                  TipologiaAnomalia.NO) ||
        //                 (whiteList.ListaAltrePersone.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //                  TipologiaAnomalia.NO) ||
        //                 (whiteList.ListaReferenti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //            TipologiaAnomalia.NO) ||
        //                 (whiteList.Subappalti.ControlloTimbrature(timb.CodiceFiscale, timb.DataOra) ==
        //                  TipologiaAnomalia.NO))
        //        {
        //            UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.NO);
        //        }
        //        else
        //        {
        //            UpdateTimbratura(timb.IdTimbratura, TipologiaAnomalia.NonInLista);
        //        }
        //    }
        //}

        public static DateTime GetDataDaConsiderarePerControlli(DateTime dataPartenza)
        {
            DateTime dataDaConsiderare;

            if (dataPartenza.Day < 11)
            {
                dataDaConsiderare = dataPartenza.AddMonths(-3);
            }
            else
            {
                dataDaConsiderare = dataPartenza.AddMonths(-2);
            }

            return dataDaConsiderare;
        }

        public Boolean ControlloDebiti(Int32 idImpresa, Int32 anno, Int32 mese)
        {
            Boolean risultatoControllo = false;
            Int32 tolleranza = 0;

            // Lettura tolleranza da file di configurazione
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["TolleranzaControlloDebiti"]))
            {
                tolleranza = Int32.Parse(ConfigurationManager.AppSettings["TolleranzaControlloDebiti"]);
            }

            if (ImpresaRegolareBni(idImpresa))
            {
                if (ImpresaRegolareFreccia(idImpresa, anno, mese, tolleranza))
                {
                    risultatoControllo = true;
                }
            }
            else
            {
                if (ImpresaRegolareSaldoContabile(idImpresa, anno, mese, tolleranza))
                {
                    risultatoControllo = true;
                }
            }

            return risultatoControllo;
        }

        public static Boolean BadgeReady(Lavoratore lavoratore, String autSub)
        {
            if (!string.IsNullOrEmpty(lavoratore.Nome) &&
                !string.IsNullOrEmpty(lavoratore.Cognome) &&
                lavoratore.DataNascita.HasValue &&
                ((!String.IsNullOrEmpty(lavoratore.PaeseNascita) && lavoratore.PaeseNascita != "1") ||
                 !String.IsNullOrEmpty(lavoratore.LuogoNascita)) &&
                lavoratore.DataAssunzione.HasValue &&
                !string.IsNullOrEmpty(autSub) &&
                lavoratore.Foto != null)
            {
                return true;
            }

            return false;
        }

        public Boolean BadgeReadyFoto(Lavoratore lavoratore, String autSub, Impresa impresa, Int32 idCantiere)
        {
            byte[] foto = null;

            if (lavoratore.IdLavoratore.HasValue && impresa.IdImpresa.HasValue)
            {
                foto = GetFotoLavoratoreByLavoratoreImpresaCantiere(lavoratore.IdLavoratore.Value,
                                                                    lavoratore.TipoLavoratore,
                                                                    impresa.IdImpresa.Value, impresa.TipoImpresa,
                                                                    idCantiere);
            }

            if (!string.IsNullOrEmpty(lavoratore.Nome) &&
                !string.IsNullOrEmpty(lavoratore.Cognome) &&
                lavoratore.DataNascita.HasValue &&
                ((!String.IsNullOrEmpty(lavoratore.PaeseNascita) && lavoratore.PaeseNascita != "1") ||
                 !String.IsNullOrEmpty(lavoratore.LuogoNascita)) &&
                lavoratore.DataAssunzione.HasValue &&
                !string.IsNullOrEmpty(autSub) &&
                //lavoratore.Foto != null
                foto != null
                )
            {
                return true;
            }

            return false;
        }

        public void DeleteWhiteList(TipologiaFornitore fornitore, String codiceLettore)
        {
            switch (fornitore)
            {
                case TipologiaFornitore.Tymbro:
                    TymbroConnector tyConn = new TymbroConnector();
                    tyConn.DeleteWhitelist(codiceLettore);
                    break;
                case TipologiaFornitore.Trexom:
                    TrexomConnector trConn = new TrexomConnector();
                    trConn.DeleteWhitelist(codiceLettore);
                    break;
            }
        }

        #region Trexom

        public Boolean? TrexomGetUltimoComandoWhiteList(String codiceRilevatore, String codiceFiscale)
        {
            return _dataAccess.TrexomGetUltimoComandoWhiteList(codiceRilevatore, codiceFiscale);
        }

        public void TrexomInsertComandoWhiteList(String codiceRilevatore, String codiceFiscale, Boolean comando)
        {
            _dataAccess.TrexomInsertComandoWhiteList(codiceRilevatore, codiceFiscale, comando);
        }

        public void TrexomInsertDeleteWhiteList(String codiceRilevatore)
        {
            _dataAccess.TrexomInsertDeleteWhiteList(codiceRilevatore);
        }

        #endregion

        public LavoratoreCollection GetLavoratoriRicerca(LavoratoreFilter filtro)
        {
            return _dataAccess.GetLavoratoriRicerca(filtro);
        }

        public Boolean CheckCodiceControlloPerImportAutomatico(Int32 idCantiere, Guid controlCode)
        {
            return _dataAccess.CheckCodiceControlloPerImportAutomatico(idCantiere, controlCode);
        }

        public Boolean InsertUpdateSubappaltieLavoratori(WhiteList domanda)
        {
            return _dataAccess.InsertUpdateSubappaltieLavoratori(domanda, null);
        }

        public Boolean InsertReferenti(IEnumerable<Referente> referenti, Int32 idDomanda)
        {
            return _dataAccess.InsertReferenti(referenti, idDomanda, null);
        }

        public void DeleteSubappaltiDellaDomanda(Int32 idDomanda)
        {
            _dataAccess.DeleteSubappaltiDellaDomanda(idDomanda, null);
        }

        public void DeleteSubappalto(Int32 idSubappalto)
        {
            _dataAccess.DeleteSubappalto(idSubappalto, null);
        }

        public void DeleteLavoratoriDellaDomanda(Int32 idDomanda)
        {
            _dataAccess.DeleteLavoratoriDellaDomanda(idDomanda, null);
        }

        public void DeleteReferentiDellaDomanda(Int32 idDomanda)
        {
            _dataAccess.DeleteReferentiDellaDomanda(idDomanda, null);
        }

        public void InsertLogWSImprese(String codiceRilevatore, String ragioneSociale, String codiceFiscale, String partitaIva, String errore)
        {
            _dataAccess.InsertLogWSImprese(codiceRilevatore, ragioneSociale, codiceFiscale, partitaIva, errore);
        }

        public void InsertLogWSLavoratore(String codiceRilevatore, String cognome, String nome, String codiceFiscale, String errore)
        {
            _dataAccess.InsertLogWSLavoratore(codiceRilevatore, cognome, nome, codiceFiscale, errore);
        }

        public List<Presenze> GetPresenze(PresenzeFilter filtro)
        {
            return _dataAccess.GetPresenze(filtro);
        }

        public List<Impresa> GetPresenzeImprese(PresenzeFilter filtro)
        {
            return _dataAccess.GetPresenzeImprese(filtro);
        }

        public ControlloIdentitaCexChangeCollection GetControlloIdentitaCexchange(ControlloIdentitaFilter filter)
        {
            return _dataAccess.GetControlloIdentitaCexchange(filter);
        }

        public void UpdateImpresa(Impresa impresa)
        {
            _dataAccess.UpdateImpresa(impresa, null);
        }

        public Boolean InsertSubappalto(Subappalto subappalto, Int32 idDomanda)
        {
            return _dataAccess.InsertSubappalto(subappalto, idDomanda, null);
        }

        public bool InsertLavoratoreImpresa(Lavoratore lavoratore, Int32 idDomanda, Impresa impresa)
        {
            return _dataAccess.InsertLavoratoreImpresa(lavoratore, idDomanda, impresa, null);
        }

        public void DeleteLavoratoreInWhiteList(Int32 idWhiteListLavoratore)
        {
            _dataAccess.DeleteLavoratoreInWhiteList(idWhiteListLavoratore);
        }

        public Boolean UpdateLavoratore(Lavoratore lavoratore)
        {
            return _dataAccess.UpdateLavoratore(lavoratore, null);
        }

        public Boolean UpdateWhiteListLavoratore(Lavoratore lavoratore)
        {
            return _dataAccess.UpdateWhiteListLavoratore(lavoratore, null);
        }

        public void ResetDataStampaBadge(int idDomanda, int? idLavoratore, int? idAccessoCantieriLavoratore,
                                          int? idImpresa, int? idAccessoCantieriImpresa)
        {
            _dataAccess.UpdateDataStampaBadge(idDomanda, idLavoratore, idAccessoCantieriLavoratore, idImpresa,
                                              idAccessoCantieriImpresa, null);
        }

        public LavoratoreCollection GetLavoratoriInForza(Int32 idImpresa)
        {
            return _dataAccess.GetLavoratoriInForza(idImpresa);
        }
    }
}
