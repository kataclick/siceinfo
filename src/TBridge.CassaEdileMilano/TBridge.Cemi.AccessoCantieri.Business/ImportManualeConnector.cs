﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class ImportManualeConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        private static DataTable ImportToMem(string csvFile)
        {
            StreamReader sr = new StreamReader(csvFile);
            string inputLine;

            bool firstString = true;

            using (DataTable dt = new DataTable())
            {
                while ((inputLine = sr.ReadLine()) != null)
                {
                    String[] values = inputLine.Split(';');
                    if (firstString)
                    {
                        foreach (string s in values)
                        {
                            dt.Columns.Add(s);
                            firstString = false;
                        }
                    }
                    //else
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            row[i] = values[i];
                        }
                        dt.Rows.Add(row);
                    }
                }

                sr.Close();
                return dt;
            }
        }

        public Int32 CaricaDati()
        {
            //try
            //{
            string[] fileList = Directory.GetFiles(
                ConfigurationManager.AppSettings["PathImportManuale"], "*.csv");

            Int32 ret = 0;

            foreach (string fileName in fileList)
            {

                DataTable dt = ImportToMem(fileName);

                TimbraturaCollection timbrature = new TimbraturaCollection();

                foreach (DataRow row in dt.Rows)
                {
                    Timbratura timbratura = new Timbratura
                    {
                        CodiceFiscale = row[0].ToString(),
                        CodiceRilevatore = ConfigurationManager.AppSettings["CodiceRilevatore"]
                    };

                    Int32 day = Int32.Parse(string.Format("{0}{1}", row[1].ToString().ElementAt(0), row[1].ToString().ElementAt(1)));
                    Int32 month = Int32.Parse(string.Format("{0}{1}", row[1].ToString().ElementAt(2), row[1].ToString().ElementAt(3)));
                    Int32 year = Int32.Parse(string.Format("{0}{1}{2}{3}", row[1].ToString().ElementAt(4), row[1].ToString().ElementAt(5), row[1].ToString().ElementAt(6), row[1].ToString().ElementAt(7)));
                    Int32 hour = Int32.Parse(string.Format("{0}{1}", row[1].ToString().ElementAt(8), row[1].ToString().ElementAt(9)));
                    Int32 minute = Int32.Parse(string.Format("{0}{1}", row[1].ToString().ElementAt(10), row[1].ToString().ElementAt(11)));

                    DateTime data = new DateTime(year, month, day, hour, minute, 0);

                    timbratura.DataOra = data;

                    timbratura.IngressoUscita = Int32.Parse(row[2].ToString()) == 1;

                    timbratura.RagioneSociale = ConfigurationManager.AppSettings["FornitoreRilevatore"];

                    timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.Manuale, timbratura.CodiceRilevatore, timbratura.DataOra);

                    timbratura.PartitaIvaImpresa = row[3].ToString();
                    timbratura.CodiceFiscaleImpresa = row[3].ToString();

                    //if (timbratura.IdCantiere != -1)
                    //    timbratura.PartitaIvaImpresa =
                    //        _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere.Value,
                    //                                              timbratura.CodiceFiscale, timbratura.DataOra);

                    if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.Manuale) == -1)
                        timbrature.Add(timbratura);
                }

                ret = ret + _biz.InsertTimbrature(timbrature, TipologiaFornitore.Manuale);
            }

            if (fileList.Length == 0)
                ret = -1;

            return ret;
            //}
            //catch
            //{
            //    return -2;
            //}
        }
    }
}