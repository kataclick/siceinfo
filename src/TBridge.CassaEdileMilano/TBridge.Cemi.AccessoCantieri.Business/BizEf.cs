﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.AccessoCantieri.Type.Dto;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using Cantiere = TBridge.Cemi.AccessoCantieri.Type.Dto.Cantiere;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class BizEf
    {
        public static List<Lettore> GetLettori()
        {
            List<Lettore> lettori;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from rilevatore in context.AccessoCantieriRilevatori
                            select new Lettore
                                       {
                                           Id = rilevatore.Id,
                                           Codice = rilevatore.Codice,
                                           Fornitore = rilevatore.AccessoCantieriFornitore.RagioneSociale,
                                           Assegnabile = rilevatore.Assegnabile,
                                           DataInserimento = rilevatore.dataInserimentoRecord,
                                           InvioWhiteList = rilevatore.InvioWhiteList
                                       };

                lettori = query.ToList();
            }

            return lettori;
        }

        public static Lettore GetLettore(int idLettore)
        {
            Lettore lettore;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from rilevatore in context.AccessoCantieriRilevatori
                            where rilevatore.Id == idLettore
                            select new Lettore
                                       {
                                           Id = rilevatore.Id,
                                           Codice = rilevatore.Codice,
                                           IdFornitore = rilevatore.IdFornitore,
                                           Fornitore = rilevatore.AccessoCantieriFornitore.RagioneSociale,
                                           Assegnabile = rilevatore.Assegnabile,
                                           DataInserimento = rilevatore.dataInserimentoRecord,
                                           InvioWhiteList = rilevatore.InvioWhiteList
                                       };

                lettore = query.SingleOrDefault();
            }

            return lettore;
        }

        public static List<AssegnazioneLettore> GetAssegnazioni(int idLettore)
        {
            List<AssegnazioneLettore> assegnazioni;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from rilevatore in context.AccessoCantieriRilevatori
                            join rilevatoriCantiere in context.AccessoCantieriRilevatoriCantieri on rilevatore.Id equals
                                rilevatoriCantiere.IdRilevatore
                            join cantiere in context.AccessoCantieriWhiteListes on rilevatoriCantiere.IdWhiteList equals
                                cantiere.Id
                            join utente in context.Utenti on cantiere.IdUtente equals utente.Id
                            where rilevatore.Id == idLettore
                            select new AssegnazioneLettore
                                       {
                                           IdLettore = rilevatore.Id,
                                           Utente = utente.Username,
                                           //IndirizzoCantiere =
                                           //    cantiere.Indirizzo + " " + cantiere.Cap + " " + cantiere.Comune + " (" +
                                           //    cantiere.Provincia + ")",
                                           Indirizzo = cantiere.Indirizzo,
                                           Cap = cantiere.Cap,
                                           Comune = cantiere.Comune,
                                           Provincia = cantiere.Provincia,
                                           DataInizio = rilevatoriCantiere.DataInizio,
                                           DataFine = rilevatoriCantiere.DataFine
                                       };

                assegnazioni = query.ToList();
            }

            return assegnazioni;
        }

        public static AssegnazioneLettore GetAssegnazioneAttiva(int idLettore)
        {
            AssegnazioneLettore assegnazione;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from rilevatore in context.AccessoCantieriRilevatori
                            join rilevatoriCantiere in context.AccessoCantieriRilevatoriCantieri on rilevatore.Id equals
                                rilevatoriCantiere.IdRilevatore
                            join cantiere in context.AccessoCantieriWhiteListes on rilevatoriCantiere.IdWhiteList equals
                                cantiere.Id
                            join utente in context.Utenti on cantiere.IdUtente equals utente.Id
                            where rilevatore.Id == idLettore && !rilevatoriCantiere.DataFine.HasValue
                            select new AssegnazioneLettore
                                       {
                                           IdLettore = rilevatore.Id,
                                           Utente = utente.Username,
                                           //IndirizzoCantiere =
                                           //    cantiere.Indirizzo + " " + cantiere.Cap + " " + cantiere.Comune + " (" +
                                           //    cantiere.Provincia + ")",
                                           Indirizzo = cantiere.Indirizzo,
                                           Cap = cantiere.Cap,
                                           Comune = cantiere.Comune,
                                           Provincia = cantiere.Provincia,
                                           DataInizio = rilevatoriCantiere.DataInizio,
                                           DataFine = rilevatoriCantiere.DataFine
                                       };

                assegnazione = query.SingleOrDefault();
            }

            return assegnazione;
        }

        public static void DisassociaLettore(int idLettore)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from rilevatoriCantiere in context.AccessoCantieriRilevatoriCantieri
                            where
                                rilevatoriCantiere.IdRilevatore == idLettore && rilevatoriCantiere.DataInizio.HasValue &&
                                !rilevatoriCantiere.DataFine.HasValue
                            select rilevatoriCantiere;

                AccessoCantieriRilevatoriCantiere rilevatore = query.SingleOrDefault();
                rilevatore.DataFine = DateTime.Now;

                if (rilevatore.AccessoCantieriRilevatore.InvioWhiteList)
                {
                    AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
                    biz.DeleteWhiteList((TipologiaFornitore) rilevatore.AccessoCantieriRilevatore.IdFornitore, rilevatore.AccessoCantieriRilevatore.Codice);
                }

                context.SaveChanges();
            }
        }

        public static List<Cantiere> GetCantieriAttivi()
        {
            List<Cantiere> cantieriAttivi;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from cantiere in context.AccessoCantieriWhiteListes
                            join utente in context.Utenti on cantiere.IdUtente equals utente.Id
                            //where !cantiere.DataFine.HasValue || cantiere.DataFine.Value >= DateTime.Today
                            //let IndirizzoCompleto = String.Format("{0} {1} {2} ({3})", cantiere.Indirizzo, cantiere.Cap, cantiere.Comune, cantiere.Provincia)
                            select new Cantiere
                                       {
                                           Id = cantiere.Id,
                                           Utente = utente.Username,
                                           IndirizzoCantiere = String.Concat(String.Concat(String.Concat(String.IsNullOrEmpty(cantiere.Indirizzo) ? "" : cantiere.Indirizzo, String.IsNullOrEmpty(cantiere.Civico) ? "" : String.Concat(" ", cantiere.Civico), " ", String.IsNullOrEmpty(cantiere.Cap) ? "" : cantiere.Cap), " ", cantiere.Comune, " "), "(", cantiere.Provincia, ")"),
                                           DataInizio = cantiere.DataInizio,
                                           DataFine = cantiere.DataFine
                                       };

                cantieriAttivi = query.ToList();
            }

            return cantieriAttivi;
        }

        public static void AssociaLettore(int idLettore, int idCantiere)
        {
            String codiceLettore = null;
            Int32 idFornitore = -1;
            Boolean invioWl = false;

            using (SICEEntities context = new SICEEntities())
            {
                AccessoCantieriRilevatoriCantiere rilevatoriCantiere = new AccessoCantieriRilevatoriCantiere
                                                                           {
                                                                               IdRilevatore = idLettore,
                                                                               IdWhiteList = idCantiere,
                                                                               DataInizio = DateTime.Now
                                                                           };
                context.AccessoCantieriRilevatoriCantieri.AddObject(rilevatoriCantiere);

                var query = from rilevatori in context.AccessoCantieriRilevatori
                            where rilevatori.Id == idLettore
                            select rilevatori;

                AccessoCantieriRilevatore rilevatore = query.SingleOrDefault();
                idLettore = rilevatore.Id;
                codiceLettore = rilevatore.Codice;
                idFornitore = rilevatore.IdFornitore;
                invioWl = rilevatore.InvioWhiteList;

                context.SaveChanges();
            }

            if (!String.IsNullOrEmpty(codiceLettore))
            {
                // Se è un Tymbro con invio della white list la invio
                if (idFornitore == (Int32) TipologiaFornitore.Tymbro
                    && invioWl)
                {
                    TymbroConnector tConn = new TymbroConnector();
                    AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
                    Dictionary<String, Boolean> whiteList = biz.GetCodFiscWhiteListCompletaByCodRilevatore(idLettore);
                    tConn.CaricaWhitelist(whiteList, codiceLettore);
                }

                // Se è un Trexom con invio della white list la invio
                if (idFornitore == (Int32) TipologiaFornitore.Trexom
                    && invioWl)
                {
                    TrexomConnector tConn = new TrexomConnector();
                    AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
                    Dictionary<String, Boolean> whiteList = biz.GetCodFiscWhiteListCompletaByCodRilevatore(idLettore);
                    tConn.CaricaWhitelist(whiteList, codiceLettore);
                }
            }
        }

        public static void AssociaCommittente(Int32 idWhiteList, Int32 idCommittente)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from cantiere in context.AccessoCantieriWhiteListes
                            where cantiere.Id == idWhiteList
                            select cantiere;

                AccessoCantieriWhiteList cantUpd = query.Single();
                cantUpd.IdCommittente = idCommittente;

                context.SaveChanges();
            }
        }
    }
}