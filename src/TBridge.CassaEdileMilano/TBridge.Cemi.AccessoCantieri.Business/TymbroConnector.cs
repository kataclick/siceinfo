﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class TymbroConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        public Int32 CaricaTimbrature()
        {
            Int32 ret;

            string connectionStringTymbro =
                ConfigurationManager.AppSettings["Tymbro"];

            DataSet dsTimbrature = new DataSet();

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionStringTymbro))
                {
                    string query;

                    if (ConfigurationManager.AppSettings["Test"] == "1")
                    {
                        query = "select * from tymbra where term_id = '044' order by Ty_timestamp desc limit 1000";
                    }
                    else
                    {
                        //query = "select * from tymbra order by Ty_timestamp desc limit 1000";
                        query = "select * from tymbra where id_dth >= ADDDATE(NOW(), INTERVAL -2 DAY)";
                    }

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        connection.Open();

                        using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command))
                        {
                            dataAdapter.Fill(dsTimbrature);
                        }
                    }
                }
            }
            catch
            {
                return -2;
            }

            TimbraturaCollection timbrature = new TimbraturaCollection();

            foreach (DataRow row in dsTimbrature.Tables[0].Rows)
            {
                try
                {
                    Timbratura timbratura = new Timbratura
                                                {
                                                    CodiceFiscale = row["matr_cf"].ToString(),
                                                    CodiceRilevatore = row["term_id"].ToString(),
                                                    DataOra = DateTime.Parse(row["ty_timestamp"].ToString())
                                                };

                    if (row["ty_flag"].ToString().Trim() == "0")
                        timbratura.IngressoUscita = true;
                    else
                        timbratura.IngressoUscita = false;

                    timbratura.RagioneSociale = "Tymbro";
                    timbratura.Latitudine = 0; //Decimal.Parse(row["gps_lati"].ToString());
                    timbratura.Longitudine = 0; //Decimal.Parse(row["gps_longi"].ToString());

                    //return - 2;

                    timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.Tymbro, timbratura.CodiceRilevatore, timbratura.DataOra);

                    if (timbratura.IdCantiere.HasValue)
                    {
                        if (timbratura.IdCantiere != -1)
                            timbratura.PartitaIvaImpresa = _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere,
                                                                                                 timbratura.
                                                                                                     CodiceFiscale,
                                                                                                 timbratura.DataOra);
                    }

                    if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.Tymbro) == -1)
                        timbrature.Add(timbratura);
                }
                catch
                {
                    continue;
                }
            }

            if (timbrature.Count == 0)
                ret = -1;
            else
                ret = _biz.InsertTimbrature(timbrature, TipologiaFornitore.Tymbro);

            return ret;
        }

        public Int32 CaricaTimbratureNuovoServer()
        {
            Int32 ret;

            string connectionStringTymbroNuovoServer =
                ConfigurationManager.AppSettings["TymbroNuovoServer"];

            DataSet dsTimbrature = new DataSet();

            try
            {
                using (MySqlConnection connection = new MySqlConnection(connectionStringTymbroNuovoServer))
                {
                    string query;

                    if (ConfigurationManager.AppSettings["Test"] == "1")
                    {
                        query = "select * from tymbra where term_id = '044' order by Ty_timestamp desc limit 1000";
                    }
                    else
                    {
                        //query = "select * from tymbra where ty_flag != 9 order by Ty_timestamp desc limit 1000";
                        //query = "select * from tymbra where ty_flag != 9 and id_dth >= ADDDATE(NOW(), INTERVAL -2 DAY)";
                        query = "select * from tymbra where id_dth >= ADDDATE(NOW(), INTERVAL -2 DAY)";
                    }

                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        connection.Open();

                        using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter(command))
                        {
                            dataAdapter.Fill(dsTimbrature);
                        }
                    }
                }
            }
            catch
            {
                return -2;
            }

            TimbraturaCollection timbrature = new TimbraturaCollection();

            foreach (DataRow row in dsTimbrature.Tables[0].Rows)
            {
                try
                {
                    Timbratura timbratura = new Timbratura
                    {
                        CodiceFiscale = row["matr_cf"].ToString(),
                        CodiceRilevatore = row["term"].ToString().Substring(1, 3),
                        DataOra = DateTime.Parse(row["ty_timestamp"].ToString())
                    };

                    if (row["ty_flag"].ToString().Trim() == "0")
                        timbratura.IngressoUscita = true;
                    else
                        timbratura.IngressoUscita = false;

                    timbratura.RagioneSociale = "Tymbro";
                    timbratura.Latitudine = 0; //Decimal.Parse(row["gps_lati"].ToString());
                    timbratura.Longitudine = 0; //Decimal.Parse(row["gps_longi"].ToString());

                    //return - 2;

                    timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.Tymbro, timbratura.CodiceRilevatore, timbratura.DataOra);

                    if (timbratura.IdCantiere.HasValue)
                    {
                        if (timbratura.IdCantiere != -1)
                            timbratura.PartitaIvaImpresa = _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere,
                                                                                                 timbratura.
                                                                                                     CodiceFiscale,
                                                                                                 timbratura.DataOra);
                    }

                    if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.Tymbro) == -1)
                        timbrature.Add(timbratura);
                }
                catch
                {
                    continue;
                }
            }

            if (timbrature.Count == 0)
                ret = -1;
            else
                ret = _biz.InsertTimbrature(timbrature, TipologiaFornitore.Tymbro);

            return ret;
        }

        public void DeleteWhitelist(string codiceRilevatore)
        {
            String connectionStringTymbroNuovoServer =
               ConfigurationManager.AppSettings["TymbroNuovoServer"];

            using (MySqlConnection connection = new MySqlConnection(connectionStringTymbroNuovoServer))
            {
                String query = null;

                if (Int32.Parse(codiceRilevatore) > 100)
                    query = String.Format("INSERT INTO whitelist (term,term_id, matr_cf, ion_off,ion_dth) VALUES ('0{0}', '{1}', '', '9', NOW())",
                    codiceRilevatore, "001");
                else
                    query = String.Format("INSERT INTO whitelist (term,term_id, matr_cf, ion_off,ion_dth) VALUES ('0{0}', '{0}', '', '9', NOW())",
                    codiceRilevatore);

                connection.Open();
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    Console.WriteLine(command.CommandText);

                    command.ExecuteNonQuery();
                }
            }

        }

        public void CaricaWhitelist(Dictionary<String, Boolean> codiciFiscali, string codiceRilevatore)
        {
            string connectionStringTymbroNuovoServer =
                ConfigurationManager.AppSettings["TymbroNuovoServer"];

            using (MySqlConnection connection = new MySqlConnection(connectionStringTymbroNuovoServer))
            {
                connection.Open();

                foreach (KeyValuePair<String, Boolean> lavoratore in codiciFiscali)
                {
                    // Per ogni lavoratore (abilitato o meno) presente in white list
                    // recupero l'ultimo comando lanciato per la white list sul lettore

                    Boolean? ultimoComando = null;

                    using (MySqlCommand commandRec = connection.CreateCommand())
                    {
                        commandRec.CommandText =
                            //String.Format("SELECT ion_off FROM whitelist WHERE term_id = '{0}' and matr_cf = '{1}' order by ion_dth desc limit 1",
                            String.Format("SELECT wl.ion_off FROM whitelist wl WHERE right(wl.term, 3) = '{0}' and wl.matr_cf = '{1}' and not exists ( select wl2.id from whitelist wl2 where wl2.ion_off = '9' and right(wl2.term, 3) = '{0}' and wl2.ion_dth > wl.ion_dth) order by wl.ion_dth desc limit 1",
                            codiceRilevatore,
                            lavoratore.Key);

                        using (MySqlDataReader reader = commandRec.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                String flagAbilitazione = reader["ion_off"].ToString();
                                ultimoComando = (flagAbilitazione == "1");
                            }
                        }
                    }

                    // Inserisco un nuovo record solo se:
                    // - non era presente uno stato precedente e lo stato nuovo è presente
                    // - lo stato del lavoratore è cambiato
                    if ((!ultimoComando.HasValue && lavoratore.Value)
                        || (ultimoComando.HasValue && ultimoComando.Value != lavoratore.Value))
                    {
                        using (MySqlCommand command = connection.CreateCommand())
                        {

                            if (Int32.Parse(codiceRilevatore) > 100)
                            {
                                command.CommandText =
                                    String.Format(
                                        "INSERT INTO whitelist (term,term_id, matr_cf, ion_off,ion_dth) VALUES ('0{0}', '{1}', '{2}', '{3}', NOW())",
                                        codiceRilevatore,
                                        "001",
                                        lavoratore.Key,
                                        lavoratore.Value ? "1" : "0");
                            }
                            else
                            {
                                command.CommandText =
                                String.Format("INSERT INTO whitelist (term,term_id, matr_cf, ion_off,ion_dth) VALUES ('0{0}', '{0}', '{1}', '{2}', NOW())",
                                    codiceRilevatore,
                                    lavoratore.Key,
                                    lavoratore.Value ? "1" : "0");
                            }

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

        }
    }
}