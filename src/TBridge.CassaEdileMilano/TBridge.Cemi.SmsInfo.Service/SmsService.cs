using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

namespace TBridge.Cemi.SmsInfo.Service
{
    public partial class SmsService : ServiceBase
    {
        private readonly Timer timer1;

        public SmsService()
        {
            InitializeComponent();

            timer1 = new Timer();
            timer1.Elapsed += timer1_Elapsed;
        }

        protected override void OnStart(string[] args)
        {
            KillExistingProcesses();

            try
            {
                StartController();


                timer1.Interval = Int32.Parse(ConfigurationManager.AppSettings["timer"]);
                timer1.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void StartController()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.WorkingDirectory = ConfigurationManager.AppSettings["working_directory"];
            startInfo.FileName = ConfigurationManager.AppSettings["executable_name"];
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            Process.Start(startInfo);
        }

        protected override void OnStop()
        {
            KillExistingProcesses();
        }

        private void KillExistingProcesses()
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }

        private void timer1_Elapsed(object sender, ElapsedEventArgs e)
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            if (processes.Length == 0)
            {
                StartController();
            }
        }
    }
}