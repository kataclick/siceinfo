using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class Persona
    {
        private string fax;
        private int? idPersona;
        private string indirizzo;
        private string nominativo;
        private string ragioneSociale;
        private string telefono;

        public Persona()
        {
        }

        public Persona(int? idPersona, string nominativo, string ragioneSociale, string indirizzo,
                       string telefono, string fax)
        {
            this.idPersona = idPersona;
            this.nominativo = nominativo;
            this.ragioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.telefono = telefono;
            this.fax = fax;
        }

        public int? IdPersona
        {
            get { return idPersona; }
            set { idPersona = value; }
        }

        public string Nominativo
        {
            get { return nominativo; }
            set { nominativo = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }
    }
}