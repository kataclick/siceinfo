using System;
using TBridge.Cemi.Cpt.Type.Collections;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class NotificaTelematica : Notifica
    {
        public Int32? IdNotificaTemporanea
        {
            get;
            set;
        }

        public Boolean ResponsabileNonNominato
        {
            get;
            set;
        }

        public SubappaltoNotificheTelematicheCollection ImpreseAffidatarie
        {
            get;
            set;
        }

        public SubappaltoNotificheTelematicheCollection ImpreseEsecutrici
        {
            get;
            set;
        }

        public new CommittenteNotificheTelematiche Committente
        {
            get;
            set;
        }

        public new PersonaNotificheTelematiche CoordinatoreSicurezzaProgettazione
        {
            get;
            set;
        }

        public new PersonaNotificheTelematiche CoordinatoreSicurezzaRealizzazione
        {
            get;
            set;
        }

        public new PersonaNotificheTelematiche DirettoreLavori
        {
            get;
            set;
        }

        public Guid IdUtenteTelematiche
        {
            get;
            set;
        }

        public Guid Guid
        {
            get;
            set;
        }

        public Boolean CoordinatoreProgettazioneNonNominato
        {
            get;
            set;
        }

        public Boolean CoordinatoreEsecuzioneNonNominato
        {
            get;
            set;
        }

        public String Note
        {
            get;
            set;
        }
    }
}