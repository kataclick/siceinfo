using System;
using System.Collections.Generic;
using System.Text;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class Area
    {
        private Int16 idArea;
        public Int16 IdArea
        {
            get { return idArea; }
            set { idArea = value; }
        }

        private String descrizione;
        public String Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }
    }
}
