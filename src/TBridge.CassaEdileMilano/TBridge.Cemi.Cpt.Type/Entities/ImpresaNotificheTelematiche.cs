using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class ImpresaNotificheTelematiche
    {
        public Guid IdTemporaneo { get; set; }

        public Int32? IdImpresaTelematica { get; set; }

        public Int32? IdImpresaAnagrafica { get; set; }

        public String RagioneSociale { get; set; }

        public Boolean LavoratoreAutonomo { get; set; }

        public String PartitaIva { get; set; }

        public String CodiceFiscale { get; set; }

        public String AttivitaPrevalente { get; set; }

        public Int32? IdImpresa { get; set; }

        public String IdCassaEdile { get; set; }

        public String MatricolaINAIL { get; set; }

        public String MatricolaINPS { get; set; }

        public String MatricolaCCIAA { get; set; }

        public String Indirizzo { get; set; }

        public String Comune { get; set; }

        public String Provincia { get; set; }

        public String Cap { get; set; }

        public String Telefono { get; set; }

        public String Fax { get; set; }
    }
}