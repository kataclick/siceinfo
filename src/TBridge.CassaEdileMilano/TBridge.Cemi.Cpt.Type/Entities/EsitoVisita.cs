namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class EsitoVisita
    {
        public int IdEsitoVisita { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}