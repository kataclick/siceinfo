using TBridge.Cemi.Cpt.Type.Enums;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class LogRicerca
    {
        public int IdUtente { get; set; }

        public string XmlFiltro { get; set; }

        public SezioneLogRicerca Sezione { get; set; }
    }
}