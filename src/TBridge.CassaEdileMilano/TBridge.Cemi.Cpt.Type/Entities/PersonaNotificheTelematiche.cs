using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class PersonaNotificheTelematiche : Persona
    {
        public String Citta { get; set; }

        public String Provincia { get; set; }

        public String Cap { get; set; }

        public String PersonaCognome { get; set; }

        public String PersonaNome { get; set; }

        public String PersonaComune { get; set; }

        public String PersonaProvincia { get; set; }

        public String PersonaCap { get; set; }

        public String PersonaCellulare { get; set; }

        public String PersonaEmail { get; set; }

        public String PersonaCodiceFiscale { get; set; }

        public String EntePartitaIva { get; set; }

        public String EnteCodiceFiscale { get; set; }

        public String EnteIndirizzo { get; set; }

        public String EnteComune { get; set; }

        public String EnteProvincia { get; set; }

        public String EnteCap { get; set; }

        public String EnteTelefono { get; set; }

        public String EnteFax { get; set; }
    }
}