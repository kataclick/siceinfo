using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class UtenteNotificheTelematiche
    {
        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CartaIdentita { get; set; }

        public String Indirizzo { get; set; }

        public String Comune { get; set; }

        public String Provincia { get; set; }

        public String Cap { get; set; }

        public String Telefono { get; set; }

        public String Fax { get; set; }

        public Guid IdUtente { get; set; }

        public CommittenteNotificheTelematiche CommittenteTelematiche { get; set; }
    }
}