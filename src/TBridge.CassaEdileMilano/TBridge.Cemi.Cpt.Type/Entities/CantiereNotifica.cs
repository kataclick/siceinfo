using System;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class CantiereNotifica
    {
        private Committente committente;
        private DateTime data;
        private DateTime? dataFineLavori;
        private DateTime? dataInizioLavori;
        private DateTime dataInserimento;
        private int idIndirizzo;
        private int idNotifica;
        private Impresa impresaAppaltata;
        private Impresa impresaRicercata;
        private Impresa impresaSubappaltata;
        private Indirizzo indirizzo;
        private string naturaOpera;
        private int numeroVisiteASL;
        private int numeroVisiteASLERSLT;
        private int numeroVisiteCassaEdile;
        private int numeroVisiteCPT;
        private int numeroVisiteDPL;

        public int IdNotifica
        {
            get { return idNotifica; }
            set { idNotifica = value; }
        }

        public int IdIndirizzo
        {
            get { return idIndirizzo; }
            set { idIndirizzo = value; }
        }

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public string NaturaOpera
        {
            get { return naturaOpera; }
            set { naturaOpera = value; }
        }

        public Indirizzo Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string IndirizzoCompleto
        {
            get { return indirizzo.IndirizzoCompleto; }
        }

        public string IndirizzoDenominazione
        {
            get
            {
                if (this.Indirizzo != null)
                {
                    return String.Format("{0} {1}", this.Indirizzo.Indirizzo1, this.Indirizzo.Civico);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public string IndirizzoComune
        {
            get
            {
                if (this.Indirizzo != null)
                {
                    return this.Indirizzo.Comune;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public string IndirizzoProvincia
        {
            get
            {
                if (this.Indirizzo != null)
                {
                    return this.Indirizzo.Provincia;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public string IndirizzoCap
        {
            get
            {
                if (this.Indirizzo != null)
                {
                    return this.Indirizzo.Cap;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String Latitudine
        {
            get
            {
                return indirizzo.Latitudine.ToString();
            }
        }

        public String Longitudine
        {
            get
            {
                return indirizzo.Longitudine.ToString();
            }
        }

        public DateTime? DataInizioLavori
        {
            get { return dataInizioLavori; }
            set { dataInizioLavori = value; }
        }

        public DateTime? DataFineLavori
        {
            get { return dataFineLavori; }
            set { dataFineLavori = value; }
        }

        public DateTime DataInserimento
        {
            get { return dataInserimento; }
            set { dataInserimento = value; }
        }

        /// <summary>
        /// Ritorna il committente; null se non presente
        /// </summary>
        public Committente Committente
        {
            get { return committente; }
            set { committente = value; }
        }

        /// <summary>
        /// Ritorna la ragione sociale del committente
        /// </summary>
        public string CommittenteRagioneSociale
        {
            get
            {
                if (committente != null)
                    return committente.RagioneSociale;
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Impresa Appaltata; null se non presente
        /// </summary>
        public Impresa ImpresaAppaltata
        {
            get { return impresaAppaltata; }
            set { impresaAppaltata = value; }
        }

        /// <summary>
        /// Impresa Subappaltata; null se non presente
        /// </summary>
        public Impresa ImpresaSubappaltata
        {
            get { return impresaSubappaltata; }
            set { impresaSubappaltata = value; }
        }

        /// <summary>
        /// Ragione sociale dell' impresa appaltata
        /// </summary>
        public string ImpresaAppaltataRagioneSociale
        {
            get
            {
                if (impresaAppaltata != null)
                    return impresaAppaltata.RagioneSociale;
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Ragione sociale dell' impresa subappaltata
        /// </summary>
        public string ImpresaSubppaltataRagioneSociale
        {
            get
            {
                if (impresaSubappaltata != null)
                    return impresaSubappaltata.RagioneSociale;
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// L'oggetto � in realt� ImpresaAppaltata o SubAppaltata. Qui eien
        /// </summary>
        public Impresa ImpresaRicercata
        {
            get { return impresaRicercata; }
            set { impresaRicercata = value; }
        }

        /// <summary>
        /// Ragione sociale dell' impresa ricercata
        /// </summary>
        public string ImpresaRicercataRagioneSociale
        {
            get
            {
                if (impresaRicercata != null)
                    return impresaRicercata.RagioneSociale;
                else
                    return string.Empty;
            }
        }

        public string ImpresaRicercataPartitaIva
        {
            get
            {
                if (impresaRicercata != null)
                    return impresaRicercata.PartitaIva;
                else
                    return string.Empty;
            }
        }

        public string ImpresaRicercataCodiceFiscale
        {
            get
            {
                if (impresaRicercata != null)
                    return impresaRicercata.CodiceFiscale;
                else
                    return string.Empty;
            }
        }

        public int NumeroVisiteASL
        {
            get { return numeroVisiteASL; }
            set { numeroVisiteASL = value; }
        }

        public int NumeroVisiteCPT
        {
            get { return numeroVisiteCPT; }
            set { numeroVisiteCPT = value; }
        }

        public int NumeroVisiteDPL
        {
            get { return numeroVisiteDPL; }
            set { numeroVisiteDPL = value; }
        }

        public int NumeroVisiteASLERSLT
        {
            get { return numeroVisiteASLERSLT; }
            set { numeroVisiteASLERSLT = value; }
        }

        public int NumeroVisiteCassaEdile
        {
            get { return numeroVisiteCassaEdile; }
            set { numeroVisiteCassaEdile = value; }
        }

        public Int32? NumeroImprese { get; set; }

        public Int32? NumeroLavoratori { get; set; }

        public Decimal Ammontare { get; set; }

        public String ProtocolloRegioneNotifica { set; get; }
    }
}