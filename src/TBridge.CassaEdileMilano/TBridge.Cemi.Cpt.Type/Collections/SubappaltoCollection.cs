using System;
using System.Collections.Generic;
using TBridge.Cemi.Cpt.Type.Entities;

namespace TBridge.Cemi.Cpt.Type.Collections
{
    [Serializable]
    public class SubappaltoCollection : List<Subappalto>
    {
    }
}