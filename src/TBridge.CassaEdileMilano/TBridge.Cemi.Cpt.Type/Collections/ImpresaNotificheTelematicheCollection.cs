using System;
using System.Collections.Generic;
using System.Text;
using TBridge.Cemi.Cpt.Type.Entities;

namespace TBridge.Cemi.Cpt.Type.Collections
{
    [Serializable]
    public class ImpresaNotificheTelematicheCollection : List<ImpresaNotificheTelematiche>
    {
    }
}
