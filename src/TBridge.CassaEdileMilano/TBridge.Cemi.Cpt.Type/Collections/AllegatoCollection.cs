using System.Collections.Generic;
using TBridge.Cemi.Cpt.Type.Entities;

namespace TBridge.Cemi.Cpt.Type.Collections
{
    public class AllegatoCollection : List<Allegato>
    {
        public void AddUnico(Allegato allegato)
        {
            foreach (Allegato all in this)
            {
                if (all.IdAllegato == allegato.IdAllegato)
                    return;
            }

            base.Add(allegato);
        }
    }
}