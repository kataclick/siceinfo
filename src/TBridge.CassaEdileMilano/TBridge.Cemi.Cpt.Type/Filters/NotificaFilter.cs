using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    [Serializable]
    public class NotificaFilter
    {
        private decimal? ammontare;
        private string cap;
        private string committente;
        private DateTime? dataFine;
        private DateTime? dataInizio;
        private string fiscIva;
        private Int16 idArea;
        private int? idASL;
        private string impresa;
        private string indirizzo;
        private string indirizzoComune;
        private string naturaOpera;
        private string numeroAppalto;

        public Int16 IdArea
        {
            get { return idArea; }
            set { idArea = value; }
        }

        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public string Committente
        {
            get { return committente; }
            set { committente = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Impresa
        {
            get { return impresa; }
            set { impresa = value; }
        }

        public string NaturaOpera
        {
            get { return naturaOpera; }
            set { naturaOpera = value; }
        }

        public decimal? Ammontare
        {
            get { return ammontare; }
            set { ammontare = value; }
        }

        public DateTime? DataInizio
        {
            get { return dataInizio; }
            set { dataInizio = value; }
        }

        public DateTime? DataFine
        {
            get { return dataFine; }
            set { dataFine = value; }
        }

        public string FiscIva
        {
            get { return fiscIva; }
            set { fiscIva = value; }
        }

        public string IndirizzoComune
        {
            get { return indirizzoComune; }
            set { indirizzoComune = value; }
        }

        public string NumeroAppalto
        {
            get { return numeroAppalto; }
            set { numeroAppalto = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public int? IdASL
        {
            get { return idASL; }
            set { idASL = value; }
        }

        public Guid? IdUtenteTelematiche { get; set; }

        public Int32? IdNotifica { get; set; }

        public String ProvinciaImpresa { get; set; }

        public String CapImpresa { get; set; }

        public String IndirizzoProvincia { get; set; }

        public String ProtocolloRegione { get; set; }
    }
}