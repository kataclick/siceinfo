using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    public class UtenteNotificheTelematicheFilter
    {
        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String Login { get; set; }

        public String TipoUtenti { get; set; }
    }
}