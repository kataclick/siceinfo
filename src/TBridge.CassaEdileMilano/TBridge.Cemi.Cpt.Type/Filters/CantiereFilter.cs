using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    [Serializable]
    public class CantiereFilter
    {
        private decimal? ammontare;
        private string committente;
        private string comune;
        private DateTime? dataFine;
        private DateTime? dataInizio;
        private Int16 idArea;
        private int? idASL;
        private string impresa;
        private string indirizzo;
        private bool? localizzati;
        private string naturaOpera;

        public string Committente
        {
            get { return committente; }
            set { committente = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Impresa
        {
            get { return impresa; }
            set { impresa = value; }
        }

        public decimal? Ammontare
        {
            get { return ammontare; }
            set { ammontare = value; }
        }

        public string NaturaOpera
        {
            get { return naturaOpera; }
            set { naturaOpera = value; }
        }

        public DateTime? DataInizio
        {
            get { return dataInizio; }
            set { dataInizio = value; }
        }

        public DateTime? DataFine
        {
            get { return dataFine; }
            set { dataFine = value; }
        }

        public bool? Localizzati
        {
            get { return localizzati; }
            set { localizzati = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public int? IdASL
        {
            get { return idASL; }
            set { idASL = value; }
        }

        public Int16 IdArea
        {
            get { return idArea; }
            set { idArea = value; }
        }

        public Int32? IdIndirizzo { get; set; }

        public Guid? IdUtenteTelematiche { get; set; }

        public Int32? NumeroLavoratori { get; set; }
    }
}