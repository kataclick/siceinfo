using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    public class NotificaTelematicaAggiornamentoFilter
    {
        public Int32? IdNotifica { get; set; }

        public DateTime? DataInserimento { get; set; }

        public String NaturaOpera { get; set; }

        public String Indirizzo { get; set; }

        public String Comune { get; set; }

        public Guid? IdUtenteTelematiche { get; set; }
    }
}