using TBridge.Cemi.Cpt.Type.Entities;
using System;
using TBridge.Cemi.Cpt.Type.Collections;

namespace TBridge.Cemi.Cpt.Type.Delegates
{
    public delegate void ResponsabileDeiLavoriEventHandler(Boolean nonNominato);

    public delegate void CoordinatoreSicurezzaProgettazioneEventHandler(Boolean nonNominato);

    public delegate void CoordinatoreSicurezzaEsecuzioneEventHandler(Boolean nonNominato);

    public delegate void CoordinatoreSicurezzaProgettazioneComeResponsabileLavoriEventHandler(Boolean stato);

    public delegate void CoordinatoreSicurezzaEsecuzioneComeResponsabileLavoriEventHandler(Boolean stato);

    public delegate void CoordinatoreSicurezzaEsecuzioneComeCoordinatoreSicurezzaProgettazioneEventHandler(Boolean stato);

    public delegate void ImpresaSelectedEventHandler();

    public delegate void ImpresaNuovaEventHandler();

    public delegate void NotificaSelectedEventHandler(Int32 idNotifica, Int32 idNotificaRiferimento);

    public delegate void SubappaltoCreatedOrDeletedEventHandler(SubappaltoNotificheTelematicheCollection subappalti);
}