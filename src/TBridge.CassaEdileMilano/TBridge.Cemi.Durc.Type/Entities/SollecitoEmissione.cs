using System;

namespace TBridge.Cemi.Durc.Type.Entities
{
    public class SollecitoEmissione
    {
        public int? IdDurcSollecito { get; set; }

        public int IdUtente { get; set; }

        public string RagioneSocialeConsulente { get; set; }

        public string IdConsulente { get; set; }

        public string EMail { get; set; }

        public string Cip { get; set; }

        public string NumeroProtocollo { get; set; }

        public string MotivoRichiesta { get; set; }

        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public DateTime? DataRichiesta { get; set; }

        public bool RitiroSede { get; set; }

        public string Telefono { get; set; }
    }
}