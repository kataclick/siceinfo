using TBridge.Cemi.Durc.Type.Entities;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Durc.Type.Delegates
{
    public delegate void SollecitoEmissioneInsertedEventHandler(
        bool prerequisitiSoddisfatti, DurcSollecito sollecitoEmissione);
}