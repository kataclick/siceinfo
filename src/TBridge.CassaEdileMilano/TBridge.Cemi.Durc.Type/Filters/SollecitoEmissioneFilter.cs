﻿using System;

namespace TBridge.Cemi.Durc.Type.Filters
{
    public class SollecitoEmissioneFilter
    {
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public String RagioneSociale { get; set; }
        public Int32? CodiceImpresa { get; set; }
        public String NumeroProtocollo { get; set; }
        public String Cip { get; set; }
    }
}