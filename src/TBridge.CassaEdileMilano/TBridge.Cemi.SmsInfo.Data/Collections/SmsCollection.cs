using System;
using System.Collections.Generic;
using TBridge.Cemi.SmsInfo.Data.Entities;

namespace TBridge.Cemi.SmsInfo.Data.Collections
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public class SmsCollection : List<Sms>
    {
    }
}