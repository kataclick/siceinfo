using System;

namespace TBridge.Cemi.SmsInfo.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public class ReceivedSms : Sms
    {
        public string Mittente { get; set; }
    }
}