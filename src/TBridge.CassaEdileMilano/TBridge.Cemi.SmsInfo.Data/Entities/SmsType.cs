using System;

namespace TBridge.Cemi.SmsInfo.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public enum SmsType
    {
        NonDefinito = 0,
        Immediato = 1,
        AScadenza = 2
    }
}