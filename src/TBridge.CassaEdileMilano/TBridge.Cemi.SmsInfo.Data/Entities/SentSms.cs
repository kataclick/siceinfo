using System;
using TBridge.Cemi.SmsInfo.Data.Collections;

namespace TBridge.Cemi.SmsInfo.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public class SentSms : Sms
    {
        private int idInvioCarrier = -1;
        private int idInvioSistema = -1;
        public SmsType Tipo { get; set; }

        public SmsAddresseeCollection Destinatari { get; set; } //TODO: da correggere!!

        public int IdInvioCarrier
        {
            get { return idInvioCarrier; }
            set { idInvioCarrier = value; }
        }

        public int IdInvioSistema
        {
            get { return idInvioSistema; }
            set { idInvioSistema = value; }
        }
    }
}