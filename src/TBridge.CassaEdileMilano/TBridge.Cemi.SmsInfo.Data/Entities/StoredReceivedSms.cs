using System;

namespace TBridge.Cemi.SmsInfo.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public class StoredReceivedSms : ReceivedSms
    {
        public bool Corretto { get; set; }

        public bool PresoInCarico { get; set; }

        public string Commento { get; set; }

        public bool ErroreGestito { get; set; }
    }
}