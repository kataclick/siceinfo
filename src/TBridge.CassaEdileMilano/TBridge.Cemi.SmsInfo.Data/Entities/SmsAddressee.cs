using System;

namespace TBridge.Cemi.SmsInfo.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public class SmsAddressee
    {
        private string cognome = String.Empty;
        private int idLavoratore = -1;
        private int idSistema = -1;
        private int idUtente = -1;
        private string nome = String.Empty;
        public string Numero { get; set; }

        public bool Attivo { get; set; }

        public int IdSistema
        {
            get { return idSistema; }
            set { idSistema = value; }
        }

        public int IdUtente
        {
            get { return idUtente; }
            set { idUtente = value; }
        }

        public int IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
    }
}