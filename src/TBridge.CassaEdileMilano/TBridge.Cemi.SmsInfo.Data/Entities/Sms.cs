using System;

namespace TBridge.Cemi.SmsInfo.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.SmsInfo.Type")]
    public class Sms
    {
        private int idCarrier = -1;
        private int idSistema = -1;
        private string testo = String.Empty;

        public string Testo
        {
            get { return testo; }
            set { testo = value; }
        }

        public int IdCarrier
        {
            get { return idCarrier; }
            set { idCarrier = value; }
        }

        public DateTime Data { get; set; }

        //private SmsType tipo;

        //public SmsType Tipo
        //{
        //    get { return tipo; }
        //    set { tipo = value; }
        //}


        public int IdSistema
        {
            get { return idSistema; }
            set { idSistema = value; }
        }
    }
}