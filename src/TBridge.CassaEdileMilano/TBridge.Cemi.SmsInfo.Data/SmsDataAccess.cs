using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.SmsInfo.Type.Collections;
using TBridge.Cemi.SmsInfo.Type.Entities;

namespace TBridge.Cemi.SmsInfo.Data
{
    public class SmsDataAccess
    {
        public SmsDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        /// <summary>
        /// Restituisce l'id dell'ultimo sms ricevuto
        /// </summary>
        /// <returns>id dell'ultimo sms ricevuto; -1 se errore o non presente</returns>
        public int GetIdCarrierUltimoSmsRicevutoRegistrato()
        {
            int idCarrierMax;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiSelectMaxIdCarrier"))
            {
                idCarrierMax = (int) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return idCarrierMax;
        }

        /// <summary>
        /// Restituisce l'elenco degli sms che devono essere inviati
        /// </summary>
        /// <returns>Collection degli sms da inviare</returns>
        public SmsCollection GetSmsDaInviare()
        {
            SmsCollection smsDaInviare = new SmsCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsScadenzaSelectWithTesto"))
            {
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        SentSms sms = new SentSms();

                        sms.IdSistema = (int) dr["idSms"];
                        sms.Testo = (string) dr["testo"];
                        sms.Data = (DateTime) dr["dataInvio"];
                        sms.Tipo = SmsType.AScadenza;
                        sms.IdInvioSistema = (int) dr["idSmsScadenza"];

                        //recupero i destinatari
                        SmsAddresseeCollection destinatari = GetAllDestinatariAttivi(sms.IdInvioSistema);
                        sms.Destinatari = destinatari;

                        smsDaInviare.Add(sms);
                    }
                }
            }

            return smsDaInviare;
        }

        public StoredReceivedSmsCollection GetSmsRicevutiRegistrati()
        {
            StoredReceivedSmsCollection smsRicevutiRegistrati = new StoredReceivedSmsCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiSelectAll"))
            {
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        StoredReceivedSms sms = new StoredReceivedSms();

                        sms.IdSistema = (int) dr["idSmsRicevuto"];
                        sms.IdCarrier = (int) dr["idSmsCarrier"];
                        sms.Mittente = (string) dr["mittente"];
                        sms.Testo = (string) dr["testo"];
                        sms.Data = (DateTime) dr["data"];
                        sms.PresoInCarico = (bool) dr["presoInCarico"];
                        sms.Corretto = (bool) dr["corretto"];
                        sms.Commento = (string) dr["commento"];
                        sms.ErroreGestito = (bool) dr["erroreGestito"];

                        smsRicevutiRegistrati.Add(sms);
                    }
                }
            }

            return smsRicevutiRegistrati;
        }

        public StoredReceivedSmsCollection GetSmsRicevutiRegistratiNonGestiti()
        {
            StoredReceivedSmsCollection smsRicevutiRegistratiNonGestiti = new StoredReceivedSmsCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiSelectNonGestiti"))
            {
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        StoredReceivedSms sms = new StoredReceivedSms();

                        sms.IdSistema = (int) dr["idSmsRicevuto"];
                        sms.IdCarrier = (int) dr["idSmsCarrier"];
                        sms.Mittente = (string) dr["mittente"];
                        sms.Testo = (string) dr["testo"];
                        sms.Data = (DateTime) dr["data"];
                        sms.PresoInCarico = (bool) dr["presoInCarico"];
                        sms.Corretto = (bool) dr["corretto"];
                        sms.Commento = (string) dr["commento"];
                        sms.ErroreGestito = (bool) dr["erroreGestito"];

                        smsRicevutiRegistratiNonGestiti.Add(sms);
                    }
                }
            }

            return smsRicevutiRegistratiNonGestiti;
        }

        public SmsCollection GetSmsInviatiRegistrati()
        {
            SmsCollection smsInviatiRegistrati = new SmsCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsInviatiSelectAll"))
            {
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        SentSms sms = new SentSms();

                        sms.IdInvioSistema = (int) dr["idSmsInviato"];
                        sms.IdSistema = (int) dr["idSms"];
                        sms.Data = (DateTime) dr["dataInvio"];
                        sms.IdInvioCarrier = (int) dr["idInvioCarrier"];
                        SmsAddressee destinatario = new SmsAddressee();
                        SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                        destinatari.Add(destinatario);
                        sms.Destinatari = destinatari;

                        smsInviatiRegistrati.Add(sms);
                    }
                }
            }

            return smsInviatiRegistrati;
        }

        public StoredReceivedSmsCollection GetSmsRicevutiRegistratiNascosti()
        {
            StoredReceivedSmsCollection smsRicevutiNascosti = new StoredReceivedSmsCollection();

            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiSelectNonCorrettiNascosti"))
            {
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        StoredReceivedSms sms = new StoredReceivedSms();

                        sms.IdSistema = (int) dr["idSmsRicevuto"];
                        sms.IdCarrier = (int) dr["idSmsCarrier"];
                        sms.Mittente = (string) dr["mittente"];
                        sms.Testo = (string) dr["testo"];
                        sms.Data = (DateTime) dr["data"];
                        sms.PresoInCarico = (bool) dr["presoInCarico"];
                        sms.Corretto = (bool) dr["corretto"];
                        sms.Commento = (string) dr["commento"];
                        sms.ErroreGestito = (bool) dr["erroreGestito"];

                        smsRicevutiNascosti.Add(sms);
                    }
                }
            }

            return smsRicevutiNascosti;
        }

        /// <summary>
        /// Restituisce tutti i destinatari attivi di un dato sms a scadenza
        /// </summary>
        /// <param name="idSmsScadenza">Id dell'sms a scadenza</param>
        /// <returns>Collection di tutti i destinatari attivi per l'sms specificato</returns>
        public SmsAddresseeCollection GetAllDestinatariAttivi(int idSmsScadenza)
        {
            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();

            using (
                DbCommand dbCommandDestinatari =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsScadenzaDestinatariSelectDestByIdScad"))
            {
                DatabaseCemi.AddInParameter(dbCommandDestinatari, "@idSmsScadenza", DbType.Int32, idSmsScadenza);
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommandDestinatari))
                {
                    while (dr.Read())
                    {
                        SmsAddressee destinatario = new SmsAddressee();

                        destinatario.Numero = (string) dr["numeroTelefono"];
                        destinatario.Attivo = true;
                        destinatario.IdSistema = (int) dr["idSmsRecapito"];
                        destinatario.IdUtente = (int) dr["idUtente"];
                        destinatario.IdLavoratore = (int) dr["idLavoratore"];
                        destinatario.Cognome = (string) dr["cognome"];
                        destinatario.Nome = (string) dr["nome"];

                        destinatari.Add(destinatario);
                    }
                }
            }

            return destinatari;
        }

        public int RegistraSmsRicevuto(ReceivedSms smsRicevuto)
        {
            int idSmsRicevuto;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsCarrier", DbType.Int32, smsRicevuto.IdCarrier);
                DatabaseCemi.AddInParameter(dbCommand, "@mittente", DbType.String, smsRicevuto.Mittente);
                DatabaseCemi.AddInParameter(dbCommand, "@testo", DbType.String, smsRicevuto.Testo);
                DatabaseCemi.AddInParameter(dbCommand, "@data", DbType.DateTime, smsRicevuto.Data);
                DatabaseCemi.AddInParameter(dbCommand, "@corretto", DbType.Boolean, false);
                DatabaseCemi.AddInParameter(dbCommand, "@presoInCarico", DbType.Boolean, false);

                idSmsRicevuto = (int) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return idSmsRicevuto;
        }

        public void AggiornaSmsRicevuto(ReceivedSms smsRicevuto, bool inCarico, bool corretto, string commento)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiUpdateStato"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsRicevuto", DbType.Int32, smsRicevuto.IdSistema);
                DatabaseCemi.AddInParameter(dbCommand, "@corretto", DbType.Boolean, corretto);
                DatabaseCemi.AddInParameter(dbCommand, "@presoInCarico", DbType.Boolean, inCarico);
                DatabaseCemi.AddInParameter(dbCommand, "@commento", DbType.String, commento);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        /// <summary>
        /// Verifica la presenza di un lavoratore con il codice fiscale indicato e restituisce l'eventuale identificativo utente
        /// </summary>
        /// <param name="codiceFiscale">Codice fiscale del lavoratore</param>
        /// <returns>Id dell'utente del sistema associato</returns>
        public int EsisteLavoratore(string codiceFiscale)
        {
            int idUtente = -1;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriSelectIdUtenteByCF"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);

                try
                {
                    idUtente = (int) DatabaseCemi.ExecuteScalar(dbCommand);
                }
                catch
                {
                }
            }

            return idUtente;
        }

        public bool EsisteLavoratoreSms(int idUtente)
        {
            bool ret = false;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRecapitiSelectByIdUtenteAttivi"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                        ret = true;
                }
            }

            return ret;
        }

        public bool EsisteLavoratoreSmsNumero(int idUtente, ReceivedSms smsRicevuto)
        {
            bool ret = false;

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRecapitiSelectByIdUtenteNumeroAttivi"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(dbCommand, "@numeroTelefono", DbType.String, smsRicevuto.Mittente);
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                        ret = true;
                }
            }

            return ret;
        }

        public int EsisteLavoratoreRegistratoByNumero(string numeroTelefono)
        {
            int idUtente = -1;

            if (!numeroTelefono.StartsWith("+39"))
                numeroTelefono = "+39" + numeroTelefono;

            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRecapitiSelectIdUtenteByNumeroAttivo"))
            {
                DatabaseCemi.AddInParameter(command, "@numeroTelefono", DbType.String, numeroTelefono);

                try
                {
                    idUtente = (int)DatabaseCemi.ExecuteScalar(command);
                }
                catch
                {
                }
            }

            return idUtente;
        }

        /// <summary>
        /// Verifica che il numero di telefono non risulti gi� attivo
        /// </summary>
        /// <param name="mittente">Numero di telefono del lavoratore</param>
        /// <returns>true se il numero non risulta attivo</returns>
        public bool NumeroTelefonoDisponibile(string mittente)
        {
            bool ret = false;

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRecapitiSelectCountByNumeroTelefonoAttivo"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@numeroTelefono", DbType.String, mittente);

                try
                {
                    int num = (int) DatabaseCemi.ExecuteScalar(dbCommand);
                    if (num == 0)
                        ret = true;
                }
                catch (Exception exc)
                {
                    ret = false;
                }
            }

            return ret;
        }

        public int InserisciRecapito(int idUtente, ReceivedSms smsRicevuto)
        {
            int idRecapito;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRecapitiInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(dbCommand, "@numeroTelefono", DbType.String, smsRicevuto.Mittente);
                DatabaseCemi.AddInParameter(dbCommand, "@numeroAttivo", DbType.Boolean, true);

                idRecapito = (int) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return idRecapito;
        }

        public int RegistraSms(SentSms sms)
        {
            int idSistema;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@testo", DbType.String, sms.Testo);
                DatabaseCemi.AddInParameter(dbCommand, "@idTipoSms", DbType.Int32, sms.Tipo);
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsCarrier", DbType.Int32, sms.IdCarrier);

                idSistema = (int) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return idSistema;
        }

        public void AggiornaSmsAScadenza(SentSms sms)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsUpdateIdCarrier"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSms", DbType.Int32, sms.IdSistema);
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsCarrier", DbType.Int32, sms.IdCarrier);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public int RegistraSmsInviato(SentSms sms)
        {
            int idSistema = -1;

            if (sms.Tipo == SmsType.AScadenza)
            {
                EliminaSmsScadenza(sms);
            }

            foreach (SmsAddressee destinatario in sms.Destinatari)
            {
                using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsInviatiInsert"))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@idSms", DbType.Int32, sms.IdSistema);
                    DatabaseCemi.AddInParameter(dbCommand, "@numero", DbType.String, destinatario.Numero);
                    DatabaseCemi.AddInParameter(dbCommand, "@dataInvio", DbType.DateTime, sms.Data);
                    DatabaseCemi.AddInParameter(dbCommand, "@idInvioCarrier", DbType.Int32, sms.IdInvioCarrier);

                    idSistema = (int)DatabaseCemi.ExecuteScalar(dbCommand);
                }
            }

            return idSistema;
        }

        public void EliminaSmsScadenza(SentSms sms)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsScadenzaDeleteByIdSms"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSms", DbType.Int32, sms.IdSistema);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public void EliminaSms(Sms sms)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsDelete"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSms", DbType.Int32, sms.IdSistema);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public int GetIdLavoratore(string codiceFiscale)
        {
            int idLav = -1;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriSelectIdLavByCF"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);

                try
                {
                    idLav = (int) DatabaseCemi.ExecuteScalar(dbCommand);
                }
                catch (Exception exc)
                {
                }
            }

            return idLav;
        }

        /// <summary>
        /// Verifica la presenza di un lavoratore con l'id lavoratore indicato e restituisce l'eventuale identificativo utente
        /// </summary>
        /// <param name="idLavoratore">id del lavoratore</param>
        /// <returns>Id dell'utente del sistema associato</returns>
        public int EsisteLavoratore(int idLavoratore)
        {
            int idUtente = -1;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriSelectIdUtenteByIdLav"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idLav", DbType.Int32, idLavoratore);

                try
                {
                    idUtente = (int) DatabaseCemi.ExecuteScalar(dbCommand);
                }
                catch
                {
                }
            }

            return idUtente;
        }

        public IDictionary<int, string> GetLavoratoriBySentSms(SentSms sms)
        {
            IDictionary<int, string> ret = new Dictionary<int, string>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsInviatiSelectLavoratoriById"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsInviato", DbType.Int32, sms.IdInvioSistema);
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        int idLavoratore = (int) dr["idLavoratore"];
                        string numeroTelefono = (string) dr["numero"];
                        ret.Add(idLavoratore, numeroTelefono);
                    }
                }
            }

            return ret;
        }

        public void RegistraSmsRichiestaTs(ReceivedSms smsRicevuto, int idUtente, string testoTaglia)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiRichiesteTsInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsRicevuto", DbType.Int32, smsRicevuto.IdSistema);
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(dbCommand, "@testoTaglia", DbType.String, testoTaglia);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public void RegistraSmsFeedbackTs(ReceivedSms smsRicevuto, int idUtente, string testo)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiFeedbackTsInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idSmsRicevuto", DbType.Int32, smsRicevuto.IdSistema);
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(dbCommand, "@testo", DbType.String, testo);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }
    }
}