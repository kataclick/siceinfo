using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Deleghe.Type;
using TBridge.Cemi.Deleghe.Type.Collections;
using TBridge.Cemi.Deleghe.Type.Entities;
using TBridge.Cemi.Deleghe.Type.Enums;
using TBridge.Cemi.Deleghe.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Type;
using TBridge.Cemi.Type.Entities;
using Indirizzo = TBridge.Cemi.Deleghe.Type.Entities.Indirizzo;
using Lavoratore = TBridge.Cemi.Deleghe.Type.Entities.Lavoratore;

namespace TBridge.Cemi.Deleghe.Data
{
    public class DelegheDataAccess
    {
        public DelegheDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public LavoratoreCollection GetLavoratori(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectLavoratori"))
            {
                if (filtro.IdLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                if (!string.IsNullOrEmpty(filtro.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                if (!string.IsNullOrEmpty(filtro.Nome))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                if (filtro.DataNascita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                if (filtro.EscludiDeceduti.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@escludiDeceduti", DbType.Boolean, filtro.EscludiDeceduti);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);
                        lavoratore.Residenza = new Indirizzo();

                        // Dati generici
                        lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                        lavoratore.Cognome = reader["cognome"].ToString();
                        lavoratore.Nome = reader["nome"].ToString();
                        if (reader["dataNascita"] != DBNull.Value)
                            lavoratore.DataNascita = (DateTime) reader["dataNascita"];
                        if (reader["codiceFiscale"] != DBNull.Value)
                            lavoratore.CodiceFiscale = reader["codiceFiscale"].ToString();
                        if (reader["cellulare"] != DBNull.Value)
                            lavoratore.Cellulare = reader["cellulare"].ToString();

                        // Residenza
                        if (reader["indirizzoDenominazione"] != DBNull.Value)
                            lavoratore.Residenza.IndirizzoDenominazione = reader["indirizzoDenominazione"].ToString();
                        if (reader["indirizzoProvincia"] != DBNull.Value)
                            lavoratore.Residenza.Provincia = reader["indirizzoProvincia"].ToString();
                        if (reader["indirizzoComune"] != DBNull.Value)
                            lavoratore.Residenza.Comune = reader["indirizzoComune"].ToString();
                        if (reader["indirizzoCap"] != DBNull.Value)
                            lavoratore.Residenza.Cap = reader["indirizzoCap"].ToString();

                        // Ultimo rapporto
                        int? idAperto = null;
                        string aperto = null;
                        int? idChiuso = null;
                        string chiuso = null;
                        // Aperto
                        if (reader["ultimoRapportoAperto"] != DBNull.Value)
                        {
                            aperto = reader["ultimoRapportoAperto"].ToString();
                            idAperto = (int) reader["idUltimoRapportoAperto"];
                        }

                        // Chiuso
                        if (reader["ultimoRapportoChiuso"] != DBNull.Value)
                        {
                            chiuso = reader["ultimoRapportoChiuso"].ToString();
                            idChiuso = (int) reader["idUltimoRapportoChiuso"];
                        }

                        if (!string.IsNullOrEmpty(aperto))
                        {
                            lavoratore.Impresa = aperto;
                            lavoratore.IdImpresa = idAperto;
                        }
                        else
                        {
                            lavoratore.Impresa = chiuso;
                            lavoratore.IdImpresa = idChiuso;
                        }
                    }
                }
            }

            return lavoratori;
        }

        /// <summary>
        ///   Inserisce una delega
        /// </summary>
        /// <param name = "delega"></param>
        /// <returns></returns>
        public bool InsertDelega(Delega delega)
        {
            bool res = false;

            if (delega != null && delega.Lavoratore != null && delega.Lavoratore.Residenza != null &&
                delega.Cantiere != null)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, delega.Lavoratore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, delega.Lavoratore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                delega.Lavoratore.DataNascita.Value);

                    if (!string.IsNullOrEmpty(delega.Lavoratore.Cellulare))
                        DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String,
                                                    delega.Lavoratore.Cellulare);

                    if (delega.Lavoratore.IdImpresa.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                    delega.Lavoratore.IdImpresa.Value);

                    if (!string.IsNullOrEmpty(delega.Lavoratore.Impresa))
                        DatabaseCemi.AddInParameter(comando, "@impresa", DbType.String,
                                                    delega.Lavoratore.Impresa);

                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.IndirizzoDenominazione))
                        DatabaseCemi.AddInParameter(comando, "@residenzaIndirizzo", DbType.String,
                                                    delega.Lavoratore.Residenza.IndirizzoDenominazione);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Civico))
                        DatabaseCemi.AddInParameter(comando, "@residenzaCivico", DbType.String,
                                                    delega.Lavoratore.Residenza.Civico);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@residenzaProvincia", DbType.String,
                                                    delega.Lavoratore.Residenza.Provincia);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Comune))
                        DatabaseCemi.AddInParameter(comando, "@residenzaComune", DbType.String,
                                                    delega.Lavoratore.Residenza.Comune);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Cap))
                        DatabaseCemi.AddInParameter(comando, "@residenzaCap", DbType.String,
                                                    delega.Lavoratore.Residenza.Cap);
                    if (!string.IsNullOrEmpty(delega.Cantiere.IndirizzoDenominazione))
                        DatabaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String,
                                                    delega.Cantiere.IndirizzoDenominazione);
                    if (!string.IsNullOrEmpty(delega.Cantiere.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String,
                                                    delega.Cantiere.Provincia);
                    if (!string.IsNullOrEmpty(delega.Cantiere.Comune))
                        DatabaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, delega.Cantiere.Comune);
                    if (!string.IsNullOrEmpty(delega.Cantiere.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, delega.Cantiere.Cap);

                    DatabaseCemi.AddInParameter(comando, "@sindacato", DbType.String, delega.Sindacato.Id);
                    DatabaseCemi.AddInParameter(comando, "@comprensorioSindacale", DbType.String,
                                                delega.ComprensorioSindacale.Id);

                    if (!string.IsNullOrEmpty(delega.OperatoreTerritorio))
                        DatabaseCemi.AddInParameter(comando, "@operatoreTerritorio", DbType.String,
                                                    delega.OperatoreTerritorio);

                    DatabaseCemi.AddInParameter(comando, "@idOperatoreInserimento", DbType.Int32,
                                                delega.OperatoreInserimento.Id);

                    if (!string.IsNullOrEmpty(delega.CodiceSblocco))
                        DatabaseCemi.AddInParameter(comando, "@codiceSblocco", DbType.String,
                                                    delega.CodiceSblocco);

                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        public bool UpdateDelega(Delega delega)
        {
            bool res = false;

            if (delega != null && delega.Lavoratore != null && delega.Lavoratore.Residenza != null &&
                delega.Cantiere != null && delega.IdDelega.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, delega.IdDelega.Value);
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, delega.Lavoratore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, delega.Lavoratore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                delega.Lavoratore.DataNascita.Value);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Cellulare))
                        DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String,
                                                    delega.Lavoratore.Cellulare);
                    if (delega.Lavoratore.IdImpresa.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                    delega.Lavoratore.IdImpresa.Value);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Impresa))
                        DatabaseCemi.AddInParameter(comando, "@impresa", DbType.String, delega.Lavoratore.Impresa);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.IndirizzoDenominazione))
                        DatabaseCemi.AddInParameter(comando, "@residenzaIndirizzo", DbType.String,
                                                    delega.Lavoratore.Residenza.IndirizzoDenominazione);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Civico))
                        DatabaseCemi.AddInParameter(comando, "@residenzaCivico", DbType.String,
                                                    delega.Lavoratore.Residenza.Civico);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@residenzaProvincia", DbType.String,
                                                    delega.Lavoratore.Residenza.Provincia);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Comune))
                        DatabaseCemi.AddInParameter(comando, "@residenzaComune", DbType.String,
                                                    delega.Lavoratore.Residenza.Comune);
                    if (!string.IsNullOrEmpty(delega.Lavoratore.Residenza.Cap))
                        DatabaseCemi.AddInParameter(comando, "@residenzaCap", DbType.String,
                                                    delega.Lavoratore.Residenza.Cap);
                    if (!string.IsNullOrEmpty(delega.Cantiere.IndirizzoDenominazione))
                        DatabaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String,
                                                    delega.Cantiere.IndirizzoDenominazione);
                    if (!string.IsNullOrEmpty(delega.Cantiere.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String,
                                                    delega.Cantiere.Provincia);
                    if (!string.IsNullOrEmpty(delega.Cantiere.Comune))
                        DatabaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, delega.Cantiere.Comune);
                    if (!string.IsNullOrEmpty(delega.Cantiere.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, delega.Cantiere.Cap);

                    DatabaseCemi.AddInParameter(comando, "@sindacato", DbType.String, delega.Sindacato.Id);
                    DatabaseCemi.AddInParameter(comando, "@comprensorioSindacale", DbType.String,
                                                delega.ComprensorioSindacale.Id);

                    if (!string.IsNullOrEmpty(delega.OperatoreTerritorio))
                        DatabaseCemi.AddInParameter(comando, "@operatoreTerritorio", DbType.String,
                                                    delega.OperatoreTerritorio);

                    DatabaseCemi.AddInParameter(comando, "@idOperatoreInserimento", DbType.Int32,
                                                delega.OperatoreInserimento.Id);

                    if (!string.IsNullOrEmpty(delega.CodiceSblocco))
                        DatabaseCemi.AddInParameter(comando, "@codiceSblocco", DbType.String,
                                                    delega.CodiceSblocco);

                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        public bool ForzaUpdateDelegaBloccata(Delega delega, int idUtente)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheBloccateForzaUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, delega.IdDelega.Value);
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, delega.Lavoratore.Cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, delega.Lavoratore.Nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                            delega.Lavoratore.DataNascita.Value);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 2)
                    res = true;
            }

            return res;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public SindacalistiCollection GetSindacalisti(Sindacato _sindacato, ComprensorioSindacale _comprensorio)
        {
            SindacalistiCollection sindacalisti;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SindacalistiSelect"))
            {
                if (_sindacato != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@idSindacato", DbType.String, _sindacato.Id);
                if (_comprensorio != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@idComprensorioSindacale", DbType.String, _comprensorio.Id);

                using (DataSet dataSetSindacalisti = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    sindacalisti = new SindacalistiCollection();

                    Sindacalista sindacalista;

                    if (dataSetSindacalisti != null && dataSetSindacalisti.Tables.Count > 0 &&
                        dataSetSindacalisti.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetSindacalisti.Tables[0].Rows.Count; i++)
                        {
                            sindacalista = new Sindacalista();

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idSindacalista"]))
                                sindacalista.Id = (int) dataSetSindacalisti.Tables[0].Rows[i]["idSindacalista"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["nome"]))
                                sindacalista.Nome = (string) dataSetSindacalisti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["cognome"]))
                                sindacalista.Cognome = (string) dataSetSindacalisti.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idSindacato"]))
                            {
                                if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["descrizioneSindacato"]))
                                {
                                    Sindacato sindacato =
                                        new Sindacato((string) dataSetSindacalisti.Tables[0].Rows[i]["idSindacato"],
                                                      (string)
                                                      dataSetSindacalisti.Tables[0].Rows[i]["descrizioneSindacato"]);

                                    sindacalista.Sindacato = sindacato;
                                }
                            }

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idComprensorioSindacale"]))
                            {
                                ComprensorioSindacale comprensorio = null;

                                if (
                                    !Convert.IsDBNull(
                                        dataSetSindacalisti.Tables[0].Rows[i]["descrizioneComprensorioSindacale"]))
                                {
                                    comprensorio =
                                        new ComprensorioSindacale(
                                            (string) dataSetSindacalisti.Tables[0].Rows[i]["idComprensorioSindacale"],
                                            (string)
                                            dataSetSindacalisti.Tables[0].Rows[i]["descrizioneComprensorioSindacale"]);
                                }

                                sindacalista.ComprensorioSindacale = comprensorio;
                            }
                            //Non ci interessano in questo caso le informazioni legate all'utente

                            sindacalisti.Add(sindacalista);
                        }
                    }
                }
            }

            return sindacalisti;
        }

        public DelegaCollection GetDeleghe(DelegheFilter filtro)
        {
            DelegaCollection deleghe = new DelegaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectRicerca"))
            {
                if (!string.IsNullOrEmpty(filtro.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                if (!string.IsNullOrEmpty(filtro.Nome))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                if (filtro.DataNascita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita);
                if (!string.IsNullOrEmpty(filtro.ComprensorioSindacale))
                    DatabaseCemi.AddInParameter(comando, "@comprensorio", DbType.String, filtro.ComprensorioSindacale);
                if (!string.IsNullOrEmpty(filtro.Sindacato))
                    DatabaseCemi.AddInParameter(comando, "@sindacato", DbType.String, filtro.Sindacato);
                DatabaseCemi.AddInParameter(comando, "@confermate", DbType.Boolean, filtro.Confermate);
                if (filtro.Stato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, filtro.Stato);
                if (filtro.MeseConferma.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mese", DbType.DateTime, filtro.MeseConferma);
                if (filtro.DalMeseConferma.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dalMese", DbType.DateTime, filtro.DalMeseConferma);
                if (filtro.AlMeseConferma.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@alMese", DbType.DateTime, filtro.AlMeseConferma);
                if (!string.IsNullOrEmpty(filtro.OperatoreTerritorio))
                    DatabaseCemi.AddInParameter(comando, "@operatoreTerritorio", DbType.String,
                                                filtro.OperatoreTerritorio);
                if (filtro.MeseModificaStato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@meseModStato", DbType.DateTime, filtro.MeseModificaStato);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indexNomeAllegatoBusta = reader.GetOrdinal("nomeAllegatoBusta");
                    while (reader.Read())
                    {
                        Delega delega = new Delega();
                        deleghe.Add(delega);

                        delega.IdDelega = (int) reader["idDelega"];
                        delega.Lavoratore.Cognome = (string) reader["cognome"];
                        delega.Lavoratore.Nome = (string) reader["nome"];
                        delega.Lavoratore.DataNascita = (DateTime) reader["dataNascita"];

                        if (reader["cellulare"] != DBNull.Value)
                            delega.Lavoratore.Cellulare = (string) reader["cellulare"];

                        if (reader["idImpresa"] != DBNull.Value)
                            delega.Lavoratore.IdImpresa = (int) reader["idImpresa"];
                        if (reader["impresa"] != DBNull.Value)
                            delega.Lavoratore.Impresa = (string) reader["impresa"];

                        if (reader["residenzaIndirizzo"] != DBNull.Value)
                            delega.Lavoratore.Residenza.IndirizzoDenominazione = (string) reader["residenzaIndirizzo"];
                        if (reader["residenzaCivico"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Civico = (string)reader["residenzaCivico"];
                        if (reader["residenzaProvincia"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Provincia = (string) reader["residenzaProvincia"];
                        if (reader["residenzaComune"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Comune = (string) reader["residenzaComune"];
                        if (reader["residenzaCap"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Cap = (string) reader["residenzaCap"];
                        if (reader["cantiereIndirizzo"] != DBNull.Value)
                            delega.Cantiere.IndirizzoDenominazione = (string) reader["cantiereIndirizzo"];
                        if (reader["cantiereProvincia"] != DBNull.Value)
                            delega.Cantiere.Provincia = (string) reader["cantiereProvincia"];
                        if (reader["cantiereComune"] != DBNull.Value)
                            delega.Cantiere.Comune = (string) reader["cantiereComune"];
                        if (reader["cantiereCap"] != DBNull.Value)
                            delega.Cantiere.Cap = (string) reader["cantiereCap"];
                        delega.ComprensorioSindacale = new ComprensorioSindacale();
                        delega.ComprensorioSindacale.Id = (string) reader["idComprensorioSindacale"];
                        delega.ComprensorioSindacale.Descrizione = (string) reader["comprensorioDescrizione"];
                        if (reader["operatoreTerritorio"] != DBNull.Value)
                            delega.OperatoreTerritorio = (string) reader["operatoreTerritorio"];
                        delega.Sindacato = new Sindacato();
                        delega.Sindacato.Id = (string) reader["idSindacato"];
                        delega.Sindacato.Descrizione = (string) reader["sindacatoDescrizione"];
                        if (reader["stato"] != DBNull.Value)
                            delega.Stato = (StatoDelega) reader["stato"];
                        if (reader["dataConferma"] != DBNull.Value)
                            delega.DataConferma = (DateTime) reader["dataConferma"];

                        if (reader["idLavoratore"] != DBNull.Value)
                            delega.Lavoratore.IdLavoratore = (int) reader["idLavoratore"];

                        if (reader["dataAdesione"] != DBNull.Value)
                            delega.DataAdesione = (DateTime) reader["dataAdesione"];

                        if (reader["idArchidoc"] != DBNull.Value)
                            delega.IdArchidoc = (string) reader["idArchidoc"];
                        else
                        {
                            delega.IdArchidoc = null;
                        }

                        if (reader["nomeAllegatoLettera"] != DBNull.Value)
                            delega.NomeAllegatoLettera = (string) reader["nomeAllegatoLettera"];
                        else
                        {
                            delega.NomeAllegatoLettera = null;
                        }

                        delega.NomeAllegatoBusta = reader.IsDBNull(indexNomeAllegatoBusta) ? null :
                                                    reader.GetString(indexNomeAllegatoBusta);

                        if (reader["anagraficaResidenzaIndirizzo"] != DBNull.Value)
                        {
                            delega.AnagraficaResidenzaIndirizzo = reader["anagraficaResidenzaIndirizzo"] as String;
                        }
                        if (reader["anagraficaResidenzaComune"] != DBNull.Value)
                        {
                            delega.AnagraficaResidenzaComune = reader["anagraficaResidenzaComune"] as String;
                        }
                        if (reader["anagraficaResidenzaProvincia"] != DBNull.Value)
                        {
                            delega.AnagraficaResidenzaProvincia = reader["anagraficaResidenzaProvincia"] as String;
                        }
                        if (reader["anagraficaResidenzaCap"] != DBNull.Value)
                        {
                            delega.AnagraficaResidenzaCap = reader["anagraficaResidenzaCap"] as String;
                        }
                        if (reader["anagraficaResidenzaCivico"] != DBNull.Value)
                        {
                            delega.AnagraficaResidenzaCivico = reader["anagraficaResidenzaCivico"] as String;
                        }
                    }
                }
            }

            return deleghe;
        }

        public DelegaCollection GetDelegheBloccate()
        {
            DelegaCollection deleghe = new DelegaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectBloccate"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Delega delega = new Delega();
                        deleghe.Add(delega);

                        delega.IdDelega = (int) reader["idDelega"];
                        delega.Lavoratore.Cognome = (string) reader["cognome"];
                        delega.Lavoratore.Nome = (string) reader["nome"];
                        delega.Lavoratore.DataNascita = (DateTime) reader["dataNascita"];

                        if (reader["cellulare"] != DBNull.Value)
                            delega.Lavoratore.Cellulare = (string) reader["cellulare"];

                        if (reader["idImpresa"] != DBNull.Value)
                            delega.Lavoratore.IdImpresa = (int) reader["idImpresa"];
                        if (reader["impresa"] != DBNull.Value)
                            delega.Lavoratore.Impresa = (string) reader["impresa"];

                        if (reader["residenzaIndirizzo"] != DBNull.Value)
                            delega.Lavoratore.Residenza.IndirizzoDenominazione = (string) reader["residenzaIndirizzo"];
                        if (reader["residenzaCivico"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Civico = (string)reader["residenzaCivico"];
                        if (reader["residenzaProvincia"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Provincia = (string) reader["residenzaProvincia"];
                        if (reader["residenzaComune"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Comune = (string) reader["residenzaComune"];
                        if (reader["residenzaCap"] != DBNull.Value)
                            delega.Lavoratore.Residenza.Cap = (string) reader["residenzaCap"];
                        if (reader["cantiereIndirizzo"] != DBNull.Value)
                            delega.Cantiere.IndirizzoDenominazione = (string) reader["cantiereIndirizzo"];
                        if (reader["cantiereProvincia"] != DBNull.Value)
                            delega.Cantiere.Provincia = (string) reader["cantiereProvincia"];
                        if (reader["cantiereComune"] != DBNull.Value)
                            delega.Cantiere.Comune = (string) reader["cantiereComune"];
                        if (reader["cantiereCap"] != DBNull.Value)
                            delega.Cantiere.Cap = (string) reader["cantiereCap"];
                        delega.ComprensorioSindacale = new ComprensorioSindacale();
                        delega.ComprensorioSindacale.Id = (string) reader["idComprensorioSindacale"];
                        delega.ComprensorioSindacale.Descrizione = (string) reader["comprensorioDescrizione"];
                        if (reader["operatoreTerritorio"] != DBNull.Value)
                            delega.OperatoreTerritorio = (string) reader["operatoreTerritorio"];
                        delega.Sindacato = new Sindacato();
                        delega.Sindacato.Id = (string) reader["idSindacato"];
                        delega.Sindacato.Descrizione = (string) reader["sindacatoDescrizione"];
                        if (reader["stato"] != DBNull.Value)
                            delega.Stato = (StatoDelega) reader["stato"];
                        if (reader["dataConferma"] != DBNull.Value)
                            delega.DataConferma = (DateTime) reader["dataConferma"];
                        delega.CodiceSblocco = (string) reader["codiceSblocco"];
                        if (reader["dataInserimentoRecord"] != DBNull.Value)
                            delega.DataInserimento = (DateTime) reader["dataInserimentoRecord"];
                    }
                }
            }

            return deleghe;
        }

        public Delega GetDelega(string codiceSblocco, string idSindacato)
        {
            Delega delega = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectByCodiceSblocco"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceSblocco", DbType.String, codiceSblocco);
                DatabaseCemi.AddInParameter(comando, "@idSindacato", DbType.String, idSindacato);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        delega = GetDelegaDaReader(reader);
                    }
                }
            }

            return delega;
        }

        public Delega GetDelega(int idDelega)
        {
            Delega delega = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, idDelega);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        delega = GetDelegaDaReader(reader);
                    }
                }
            }

            return delega;
        }

        private static Delega GetDelegaDaReader(IDataRecord reader)
        {
            Delega delega = new Delega();

            delega.IdDelega = (int) reader["idDelega"];
            delega.Lavoratore.Cognome = (string) reader["cognome"];
            delega.Lavoratore.Nome = (string) reader["nome"];
            delega.Lavoratore.DataNascita = (DateTime) reader["dataNascita"];

            if (reader["cellulare"] != DBNull.Value)
                delega.Lavoratore.Cellulare = (string) reader["cellulare"];

            if (reader["idImpresa"] != DBNull.Value)
                delega.Lavoratore.IdImpresa = (int) reader["idImpresa"];
            if (reader["impresa"] != DBNull.Value)
                delega.Lavoratore.Impresa = (string) reader["impresa"];

            if (reader["residenzaIndirizzo"] != DBNull.Value)
                delega.Lavoratore.Residenza.IndirizzoDenominazione = (string) reader["residenzaIndirizzo"];
            if (reader["residenzaCivico"] != DBNull.Value)
                delega.Lavoratore.Residenza.Civico = (string)reader["residenzaCivico"];
            if (reader["residenzaProvincia"] != DBNull.Value)
                delega.Lavoratore.Residenza.Provincia = (string) reader["residenzaProvincia"];
            if (reader["residenzaComune"] != DBNull.Value)
                delega.Lavoratore.Residenza.Comune = (string) reader["residenzaComune"];
            if (reader["residenzaCap"] != DBNull.Value)
                delega.Lavoratore.Residenza.Cap = (string) reader["residenzaCap"];
            if (reader["cantiereIndirizzo"] != DBNull.Value)
                delega.Cantiere.IndirizzoDenominazione = (string) reader["cantiereIndirizzo"];
            if (reader["cantiereProvincia"] != DBNull.Value)
                delega.Cantiere.Provincia = (string) reader["cantiereProvincia"];
            if (reader["cantiereComune"] != DBNull.Value)
                delega.Cantiere.Comune = (string) reader["cantiereComune"];
            if (reader["cantiereCap"] != DBNull.Value)
                delega.Cantiere.Cap = (string) reader["cantiereCap"];
            delega.ComprensorioSindacale = new ComprensorioSindacale();
            delega.ComprensorioSindacale.Id = (string) reader["idComprensorioSindacale"];
            delega.ComprensorioSindacale.Descrizione = (string) reader["comprensorioDescrizione"];
            if (reader["operatoreTerritorio"] != DBNull.Value)
                delega.OperatoreTerritorio = (string) reader["operatoreTerritorio"];
            delega.Sindacato = new Sindacato();
            delega.Sindacato.Id = (string) reader["idSindacato"];
            delega.Sindacato.Descrizione = (string) reader["sindacatoDescrizione"];
            if (reader["stato"] != DBNull.Value)
                delega.Stato = (StatoDelega) reader["stato"];
            if (reader["dataConferma"] != DBNull.Value)
                delega.DataConferma = (DateTime) reader["dataConferma"];
            if (reader["dataAdesione"] != DBNull.Value)
                delega.DataAdesione = (DateTime) reader["dataAdesione"];
            delega.Stampata = (Boolean) reader["stampata"];

            if (reader["anagraficaResidenzaIndirizzo"] != DBNull.Value)
            {
                delega.AnagraficaResidenzaIndirizzo = reader["anagraficaResidenzaIndirizzo"] as String;
            }
            if (reader["anagraficaResidenzaComune"] != DBNull.Value)
            {
                delega.AnagraficaResidenzaComune = reader["anagraficaResidenzaComune"] as String;
            }
            if (reader["anagraficaResidenzaProvincia"] != DBNull.Value)
            {
                delega.AnagraficaResidenzaProvincia = reader["anagraficaResidenzaProvincia"] as String;
            }
            if (reader["anagraficaResidenzaCap"] != DBNull.Value)
            {
                delega.AnagraficaResidenzaCap = reader["anagraficaResidenzaCap"] as String;
            }
            if (reader["anagraficaResidenzaCivico"] != DBNull.Value)
            {
                delega.AnagraficaResidenzaCivico = reader["anagraficaResidenzaCivico"] as String;
            }

            return delega;
        }

        public bool DelegheConfermate(DateTime mese, Sindacato sindacato, ComprensorioSindacale comprensorioSindacale)
        {
            bool res;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheConfermate"))
            {
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.DateTime, mese);
                DatabaseCemi.AddInParameter(comando, "@sindacato", DbType.String, sindacato.Id);
                if (comprensorioSindacale != null)
                    DatabaseCemi.AddInParameter(comando, "@comprensorio", DbType.String, comprensorioSindacale.Id);
                DatabaseCemi.AddOutParameter(comando, "@delegheConfermate", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);

                res = (bool) comando.Parameters["@delegheConfermate"].Value;
            }

            return res;
        }

        public int EsistonoDelegheNonConfermate(Sindacato sindacato, ComprensorioSindacale comprensorioSindacale)
        {
            int res;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheNonConfermate"))
            {
                DatabaseCemi.AddInParameter(comando, "@sindacato", DbType.String, sindacato.Id);
                if (comprensorioSindacale != null)
                    DatabaseCemi.AddInParameter(comando, "@comprensorio", DbType.String, comprensorioSindacale.Id);
                DatabaseCemi.AddOutParameter(comando, "@delegheNonConfermate", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                res = (int) comando.Parameters["@delegheNonConfermate"].Value;
            }

            return res;
        }

        public bool ConfermaDeleghe(Sindacato sindacato, ComprensorioSindacale comprensorioSindacale,
                                    StatoDelega statoDelega)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheUpdateConferma"))
            {
                DatabaseCemi.AddInParameter(comando, "@sindacato", DbType.String, sindacato.Id);
                DatabaseCemi.AddInParameter(comando, "@comprensorio", DbType.String, comprensorioSindacale.Id);
                DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, statoDelega);

                if (DatabaseCemi.ExecuteNonQuery(comando) > 0)
                    res = true;
            }

            return res;
        }

        public bool CambiaStatoDelega(int idDelega, StatoDelega statoDelega)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheUpdateCambiaStato"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, idDelega);
                DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, statoDelega);

                if (DatabaseCemi.ExecuteNonQuery(comando) > 0)
                    res = true;
            }

            return res;
        }

        public bool DeleteDelega(int idDelega)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, idDelega);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool SbloccaDelega(int idDelega)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheUpdateSblocco"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, idDelega);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public List<WordField> GetWordDataSource(TipologiaLettera tipoLettera, LetteraParam param)
        {
            List<WordField> ds = new List<WordField>();

            string spName;

            switch (tipoLettera)
            {
                case TipologiaLettera.Scadute:
                    spName = "dbo.USP_DelegheLetteraScadute";
                    break;
                case TipologiaLettera.NonIscritti:
                    spName = "dbo.USP_DelegheLetteraNonIscritti";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("tipoLettera");
            }

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand(spName))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idComprensorioSindacale", DbType.String,
                                            param.ComprensorioSindacale);
                DatabaseCemi.AddInParameter(dbCommand, "@idSindacato", DbType.String, param.Sindacato);
                if (param.DataAdesione.HasValue)
                    DatabaseCemi.AddInParameter(dbCommand, "@dataAdesione", DbType.DateTime, param.DataAdesione);
                if (param.DataModificaStato.HasValue)
                    DatabaseCemi.AddInParameter(dbCommand, "@dataModificaStato", DbType.DateTime,
                                                param.DataModificaStato);

                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            WordField wf = new WordField
                                               {
                                                   Campo = dr.GetName(i),
                                                   Valore = dr[i].ToString()
                                               };

                            ds.Add(wf);
                        }
                    }
                }
            }

            return ds;
        }

        public DateTime? GetAperturaFaseInserimento()
        {
            DateTime? aperturaInserimento = null;

            using (DbCommand comando = DatabaseCemi.GetSqlStringCommand("SELECT dbo.UF_DelegheAperturaInserimento()"))
            {
                string tempData = DatabaseCemi.ExecuteScalar(comando) as string;

                if (!string.IsNullOrEmpty(tempData))
                    aperturaInserimento = DateTime.Parse(tempData);
            }

            return aperturaInserimento;
        }

        public DelegaCollection GetDeleghePerArchiDoc()
        {
            DelegaCollection deleghe = new DelegaCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectEstrazioneArchiDoc"))
            {
                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    int indiceIdDelega = dr.GetOrdinal("idDelega");
                    int indiceCognome = dr.GetOrdinal("cognome");
                    int indiceNome = dr.GetOrdinal("nome");
                    int indiceCodiceFiscale = dr.GetOrdinal("codiceFiscale");
                    int indiceDataNascita = dr.GetOrdinal("dataNascita");
                    int indiceIdSindacato = dr.GetOrdinal("idSindacato");
                    int indiceIdComprensorioSindacale = dr.GetOrdinal("idComprensorioSindacale");
                    int indiceDataConferma = dr.GetOrdinal("dataConferma");
                    int indiceIdLavoratore = dr.GetOrdinal("idLavoratore");
                    int indiceDataAdesione = dr.GetOrdinal("dataAdesione");

                    while (dr.Read())
                    {
                        Delega delega = new Delega();

                        deleghe.Add(delega);

                        delega.IdDelega = dr.GetInt32(indiceIdDelega);
                        delega.Lavoratore.Cognome = dr.GetString(indiceCognome);
                        delega.Lavoratore.Nome = dr.GetString(indiceNome);
                        if (!dr.IsDBNull(indiceCodiceFiscale))
                            delega.Lavoratore.CodiceFiscale = dr.GetString(indiceCodiceFiscale);
                        delega.Lavoratore.DataNascita = dr.GetDateTime(indiceDataNascita);
                        delega.Sindacato = new Sindacato();
                        delega.Sindacato.Id = dr.GetString(indiceIdSindacato);
                        delega.ComprensorioSindacale = new ComprensorioSindacale();
                        delega.ComprensorioSindacale.Id = dr.GetString(indiceIdComprensorioSindacale);
                        delega.DataConferma = dr.GetDateTime(indiceDataConferma);
                        if (!dr.IsDBNull(indiceIdLavoratore))
                            delega.Lavoratore.IdLavoratore = dr.GetInt32(indiceIdLavoratore);
                        delega.DataAdesione = dr.GetDateTime(indiceDataAdesione);
                    }
                }
            }

            return deleghe;
        }

        public List<StoricoDelegaBloccata> GetStoricoDelegaBloccata(int idDelega)
        {
            List<StoricoDelegaBloccata> storicoDelega = new List<StoricoDelegaBloccata>();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheStoricoModificheSelectByIdDelega"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idDelega", DbType.Int32, idDelega);

                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    int indiceIdDelega = dr.GetOrdinal("idDelega");
                    int indiceCognome = dr.GetOrdinal("cognome");
                    int indiceNome = dr.GetOrdinal("nome");
                    int indiceDataNascita = dr.GetOrdinal("dataNascita");
                    int indiceDataVariazione = dr.GetOrdinal("dataInserimentoRecord");
                    int indiceUtente = dr.GetOrdinal("login");

                    while (dr.Read())
                    {
                        StoricoDelegaBloccata delega = new StoricoDelegaBloccata();

                        delega.IdDelega = dr.GetInt32(indiceIdDelega);
                        delega.Cognome = dr.GetString(indiceCognome);
                        delega.Nome = dr.GetString(indiceNome);
                        delega.DataNascita = dr.GetDateTime(indiceDataNascita);
                        delega.DataVariazione = dr.GetDateTime(indiceDataVariazione);
                        delega.Utente = dr.GetString(indiceUtente);

                        storicoDelega.Add(delega);
                    }
                }
            }

            return storicoDelega;
        }

        #region Funzioni per i controlli

        public string DelegaAttiva(string cognome, string nome, DateTime dataNascita)
        {
            string sindacato = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheDelegaAttiva"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        sindacato = reader["sindacato"].ToString();
                    }
                }
            }

            return sindacato;
        }

        public bool DelegaGiaImmessa(string cognome, string nome, DateTime dataNascita, string sindacatoP)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheDelegaGiaImmessa"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
                DatabaseCemi.AddInParameter(comando, "@idSindacato", DbType.String, sindacatoP);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        if (reader["sindacato"] != DBNull.Value)
                            res = true;
                    }
                }
            }

            return res;
        }

        public string DelegaNonAttivaPrecedente(string cognome, string nome, DateTime dataNascita)
        {
            string sindacato = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheDelegaNonAttivaPrecedente"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        sindacato = reader["sindacato"].ToString();
                    }
                }
            }

            return sindacato;
        }

        #endregion

        public Boolean CheckModificaDelegaNonInseribile(Delega delega)
        {
            Boolean res = false;

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheCheckModificaDelegaNonInseribile"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idDelega", DbType.Int32, delega.IdDelega.Value);
                DatabaseCemi.AddInParameter(dbCommand, "@cognome", DbType.String, delega.LavoratoreCognome);
                DatabaseCemi.AddInParameter(dbCommand, "@nome", DbType.String, delega.LavoratoreNome);
                DatabaseCemi.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, delega.DataNascitaLavoratore);
                DatabaseCemi.AddInParameter(dbCommand, "@dataAdesione", DbType.DateTime, delega.DataAdesione.Value);
                DatabaseCemi.AddOutParameter(dbCommand, "@numeroDeleghePresenti", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(dbCommand);

                Int32 numeroDelegheTrovate = (Int32) DatabaseCemi.GetParameterValue(dbCommand, "@numeroDeleghePresenti");

                if (numeroDelegheTrovate > 0)
                {
                    res = true;
                }
            }

            return res;
        }

        public void ImpostaStampa(int idDelega)
        {
            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheUpdateStampa"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idDelega", DbType.Int32, idDelega);
                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }
    }
}