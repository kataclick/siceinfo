using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.SmsInfo.InvioMassimoSMS.SmsService;

namespace TBridge.Cemi.SmsInfo.InvioMassimoSMS
{
    public partial class MainForm : Form
    {
        private readonly Database database;
        private readonly List<string> numeriTelefono;
        private readonly List<string> numeriTelefonoError;
        private readonly SmsSenderSoapClient smsSender;

        public MainForm()
        {
            InitializeComponent();

            database = DatabaseFactory.CreateDatabase("CEMI");
            numeriTelefono = new List<string>();
            numeriTelefonoError = new List<string>();
            smsSender = new SmsSenderSoapClient();
            Username = ConfigurationManager.AppSettings["SmsUsername"];
            Password = ConfigurationManager.AppSettings["SmsPassword"];
            Testo = ConfigurationManager.AppSettings["Messaggio"];
            QueryRiempimento = ConfigurationManager.AppSettings["QueryRiempimentoDestinatari"];

            textBoxMessaggio.Text = string.Format("Testo: {0} - [{1}]{2}{3}Query: {4}", Testo, Testo.Length,
                                                  Environment.NewLine, Environment.NewLine, QueryRiempimento);
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Testo { get; set; }
        public string QueryRiempimento { get; set; }

        private void buttonGetNumbers_Click(object sender, EventArgs e)
        {
            //string query =
            //    "select RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.cellulare, '/', ''), ' ', ''), '-', ''))) as numero " +
            //    "from lavoratori l left outer join smsrecapiti s on l.idutente = s.idutente " +
            //    "where l.cellulare is not null and s.numeroTelefono is null";
            const string query = "select numero from _SmsMassiviTemp where inviato <> 1";

            // VALLA: pulita lista numeri telefoni ad ogni recupero.
            numeriTelefono.Clear();

            using (DbCommand command = database.GetSqlStringCommand(query))
            {
                using (IDataReader dr = database.ExecuteReader(command))
                {
                    while (dr.Read())
                    {
                        //TODO: eventuale pulizia aggiuntiva
                        numeriTelefono.Add(dr.GetString(0));
                    }
                }
            }

            richTextBoxLog.AppendText(string.Format("Numeri recuperati: {0}", numeriTelefono.Count));
            richTextBoxLog.AppendText(Environment.NewLine);
        }

        private void buttonInvia_Click(object sender, EventArgs e)
        {
            buttonInvia.Enabled = false;
            // VALLA: pulita lista dei numeri di telefono in errore prima di ogni invio
            numeriTelefonoError.Clear();

            foreach (string numero in numeriTelefono)
            {
                int idCarrier = RegistraSms(numero);
                if (idCarrier > 0)
                {
                    int idSistema = RegistraSmsDb(idCarrier);
                    int idInvioCarrier = InviaSms(idCarrier);
                    if (idInvioCarrier > 0)
                    {
                        RegistraSmsInviato(idSistema, numero, DateTime.Now, idInvioCarrier);

                        AggiornaTabellaTemp(numero);
                    }
                    else
                    {
                        richTextBoxLog.AppendText("Messaggio non inviato. Numero:" + numero);
                        richTextBoxLog.AppendText(Environment.NewLine);
                        numeriTelefonoError.Add(numero);
                    }
                }
                else
                {
                    richTextBoxLog.AppendText("Messaggio non registrato. Numero:" + numero);
                    richTextBoxLog.AppendText(Environment.NewLine);
                    numeriTelefonoError.Add(numero);
                }
            }
        }

        private void PreparaNumeri()
        {
            const string queryDelete = "delete from _SmsMassiviTemp";

            string querySelect = QueryRiempimento;
            //"insert into dbo._SmsMassiviTemp(numero, inviato) " +
            //"select s.numeroTelefono, 0 " +
            //"from dbo.__tmp_LavoratoriInvioSms lc inner join dbo.Lavoratori l on l.idLavoratore = lc.idLavoratore inner join dbo.SmsRecapiti s on l.idUtente = s.idUtente and numeroAttivo = 1";


            using (DbCommand commandDelete = database.GetSqlStringCommand(queryDelete))
            {
                database.ExecuteNonQuery(commandDelete);
            }

            richTextBoxLog.AppendText("Tabella d'appoggio svuotata.");
            richTextBoxLog.AppendText(Environment.NewLine);

            using (DbCommand command = database.GetSqlStringCommand(querySelect))
            {
                database.ExecuteNonQuery(command);
            }

            richTextBoxLog.AppendText("Tabella d'appoggio riempita.");
            richTextBoxLog.AppendText(Environment.NewLine);
        }

        private void buttonPrepara_Click(object sender, EventArgs e)
        {
            PreparaNumeri();
        }

        public int RegistraSms(string numero)
        {
            int idCarrier;

            if (!numero.StartsWith("+39"))
                numero = "+39" + numero;
            ArrayOfString destinatari = new ArrayOfString {numero};

            //var destinatari = new string[1];
            //destinatari[0] = numero;

            idCarrier = smsSender.InserisciMessaggio(Username, Password, 1, "Cassa Edile", Testo, null, null,
                                                     destinatari, false);

            return idCarrier;
        }

        private int RegistraSmsDb(int idCarrier)
        {
            int idSistema;

            using (DbCommand dbCommand = database.GetStoredProcCommand("dbo.USP_SmsInsert"))
            {
                database.AddInParameter(dbCommand, "@testo", DbType.String, Testo);
                database.AddInParameter(dbCommand, "@idTipoSms", DbType.Int32, 1);
                database.AddInParameter(dbCommand, "@idSmsCarrier", DbType.Int32, idCarrier);

                idSistema = (int) database.ExecuteScalar(dbCommand);
            }

            return idSistema;
        }

        private void RegistraSmsInviato(int idSistema, string numero, DateTime data, int idInvioCarrier)
        {
            using (DbCommand dbCommand = database.GetStoredProcCommand("dbo.USP_SmsInviatiInsert"))
            {
                database.AddInParameter(dbCommand, "@idSms", DbType.Int32, idSistema);
                database.AddInParameter(dbCommand, "@numero", DbType.String, numero);
                database.AddInParameter(dbCommand, "@dataInvio", DbType.DateTime, data);
                database.AddInParameter(dbCommand, "@idInvioCarrier", DbType.Int32, idInvioCarrier);

                database.ExecuteScalar(dbCommand);
            }
        }

        private int InviaSms(int idCarrier)
        {
            return smsSender.InviaMessaggio(Username, Password, idCarrier);
        }

        private void AggiornaTabellaTemp(string numero)
        {
            string query = "update _SmsMassiviTemp set inviato = 1 where numero = '" + numero + "'";

            using (DbCommand command = database.GetSqlStringCommand(query))
            {
                database.ExecuteNonQuery(command);
            }
        }
    }
}