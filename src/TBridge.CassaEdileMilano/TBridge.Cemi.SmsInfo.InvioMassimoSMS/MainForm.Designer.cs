﻿namespace TBridge.Cemi.SmsInfo.InvioMassimoSMS
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGetNumbers = new System.Windows.Forms.Button();
            this.buttonInvia = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.buttonPrepara = new System.Windows.Forms.Button();
            this.textBoxMessaggio = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonGetNumbers
            // 
            this.buttonGetNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGetNumbers.Location = new System.Drawing.Point(295, 12);
            this.buttonGetNumbers.Name = "buttonGetNumbers";
            this.buttonGetNumbers.Size = new System.Drawing.Size(88, 23);
            this.buttonGetNumbers.TabIndex = 1;
            this.buttonGetNumbers.Text = "2 - Recupera Numeri";
            this.buttonGetNumbers.UseVisualStyleBackColor = true;
            this.buttonGetNumbers.Click += new System.EventHandler(this.buttonGetNumbers_Click);
            // 
            // buttonInvia
            // 
            this.buttonInvia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInvia.Location = new System.Drawing.Point(390, 12);
            this.buttonInvia.Name = "buttonInvia";
            this.buttonInvia.Size = new System.Drawing.Size(75, 23);
            this.buttonInvia.TabIndex = 2;
            this.buttonInvia.Text = "3 - Invia";
            this.buttonInvia.UseVisualStyleBackColor = true;
            this.buttonInvia.Click += new System.EventHandler(this.buttonInvia_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxLog.Location = new System.Drawing.Point(12, 141);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(453, 207);
            this.richTextBoxLog.TabIndex = 2;
            this.richTextBoxLog.Text = "";
            // 
            // buttonPrepara
            // 
            this.buttonPrepara.Location = new System.Drawing.Point(12, 12);
            this.buttonPrepara.Name = "buttonPrepara";
            this.buttonPrepara.Size = new System.Drawing.Size(75, 23);
            this.buttonPrepara.TabIndex = 0;
            this.buttonPrepara.Text = "1 - Prepara";
            this.buttonPrepara.UseVisualStyleBackColor = true;
            this.buttonPrepara.Click += new System.EventHandler(this.buttonPrepara_Click);
            // 
            // textBoxMessaggio
            // 
            this.textBoxMessaggio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMessaggio.Location = new System.Drawing.Point(12, 42);
            this.textBoxMessaggio.Multiline = true;
            this.textBoxMessaggio.Name = "textBoxMessaggio";
            this.textBoxMessaggio.ReadOnly = true;
            this.textBoxMessaggio.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxMessaggio.Size = new System.Drawing.Size(453, 93);
            this.textBoxMessaggio.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 360);
            this.Controls.Add(this.textBoxMessaggio);
            this.Controls.Add(this.buttonPrepara);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.buttonInvia);
            this.Controls.Add(this.buttonGetNumbers);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGetNumbers;
        private System.Windows.Forms.Button buttonInvia;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Button buttonPrepara;
        private System.Windows.Forms.TextBox textBoxMessaggio;
    }
}