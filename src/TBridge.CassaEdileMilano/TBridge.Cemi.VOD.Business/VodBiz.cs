﻿using System.Collections.Generic;
using TBridge.Cemi.VOD.Data;
using TBridge.Cemi.VOD.Type.Entities;
using TBridge.Cemi.VOD.Type.Filters;

namespace TBridge.Cemi.VOD.Business
{
    public class VodBiz
    {
        public static List<ImpresaConFasciaIndirizzo> GetImprese(ImpreseFilter impreseFilter)
        {
            return Provider.GetImprese(impreseFilter);
        }
    }
}