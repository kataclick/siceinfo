﻿using Cemi.SiceInfo.Type.Enum;

namespace Cemi.SiceInfo.Type.Domain
{
    partial class ImpresaRichiestaVariazioneStato
    {
        public string StatoImpresaDesc => IdTipoStatoImpresa.Description();
    }
}
