﻿using System;
using Cemi.SiceInfo.Type.Domain;

namespace Cemi.SiceInfo.Type.Delegates
{
    public delegate void RichiestaDocumentoSelectedEventHandler(Int32 idDocumento, String DocumentaleId, String FileName);
}
