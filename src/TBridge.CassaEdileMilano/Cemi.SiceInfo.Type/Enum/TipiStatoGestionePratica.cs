﻿using System.ComponentModel;

namespace Cemi.SiceInfo.Type.Enum
{
    public enum TipiStatoGestionePratica
    {
        [Description("Da valutare")] DaValutare = 0,
        Approvata = 1,
        Rifiutata,
        [Description("In attesa di documentazione")] AttesaDocumentazione = 3
    }
}