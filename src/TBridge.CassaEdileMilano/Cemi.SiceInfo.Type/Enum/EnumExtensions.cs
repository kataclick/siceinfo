﻿using System.ComponentModel;
using System.Reflection;

namespace Cemi.SiceInfo.Type.Enum
{
    public static class EnumExtensions
    {
        public static string Description(this System.Enum e)
        {
            FieldInfo fi = e.GetType().GetField(e.ToString());
            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            string ret = attributes.Length > 0 ? attributes[0].Description : e.ToString();

            return ret;
        }
    }
}
