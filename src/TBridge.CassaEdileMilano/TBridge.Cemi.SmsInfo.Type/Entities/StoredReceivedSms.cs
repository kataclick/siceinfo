namespace TBridge.Cemi.SmsInfo.Type.Entities
{
    public class StoredReceivedSms : ReceivedSms
    {
        public bool Corretto { get; set; }

        public bool PresoInCarico { get; set; }

        public string Commento { get; set; }

        public bool ErroreGestito { get; set; }
    }
}