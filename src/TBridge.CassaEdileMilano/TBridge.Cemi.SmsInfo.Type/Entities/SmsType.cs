namespace TBridge.Cemi.SmsInfo.Type.Entities
{
    public enum SmsType
    {
        NonDefinito = 0,
        Immediato = 1,
        AScadenza = 2
    }
}