namespace TBridge.Cemi.SmsInfo.Type.Entities
{
    public class ReceivedSms : Sms
    {
        public string Mittente { get; set; }
    }
}