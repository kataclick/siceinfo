using System;
using TBridge.Cemi.Durc.Data;
using TBridge.Cemi.Durc.Type.Collections;
using TBridge.Cemi.Durc.Type.Entities;
using TBridge.Cemi.Durc.Type.Filters;

namespace TBridge.Cemi.Durc.Business
{
    public class SollecitiBusiness
    {
        private readonly DataProvider dataProvider;

        public SollecitiBusiness()
        {
            dataProvider = new DataProvider();
        }

        public SollecitoEmissione InserisciSollecito(SollecitoEmissione sollecitoEmissione)
        {
            return dataProvider.InserisciSollecito(sollecitoEmissione);
        }

        public string GetAddettoAreaImpresa(Int32 idImpresa)
        {
            return dataProvider.GetAddettoAreaImpresa(idImpresa);
        }

        public SollecitoEmissioneCollection GetRichiesteSollecito(SollecitoEmissioneFilter filter)
        {
            return dataProvider.GetRichiesteSollecito(filter);
        }
    }
}