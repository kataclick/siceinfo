﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Durc.Business
{
    public class SollecitiBizEf
    {
        public DurcSollecito InserisciSollecito(DurcSollecito sollecito)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.DurcSolleciti.AddObject(sollecito);
                context.SaveChanges();
            }

            return sollecito;
        }
    }
}
