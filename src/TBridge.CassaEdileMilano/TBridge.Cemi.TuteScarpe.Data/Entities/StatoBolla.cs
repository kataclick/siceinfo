using System;
using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public enum StatoBolla
    {
        Corretta,
        DifferenzaQuantita,
        OrdineNonPresente,
        GiaGestita,
        OrdineAltroFornitore,
        ImpresaNonPresente
    }

    [Serializable]
    public class StatoBollaClass
    {
        private readonly StatoBolla stato;

        public StatoBollaClass(StatoBolla stato)
        {
            this.stato = stato;
        }

        public StatoBolla Stato
        {
            get { return stato; }
        }

        public string GetTesto()
        {
            string res;

            switch (stato)
            {
                case StatoBolla.Corretta:
                    res = "Corretta";
                    break;
                case StatoBolla.DifferenzaQuantita:
                    res = "Differenze nelle quantit�";
                    break;
                case StatoBolla.GiaGestita:
                    res = "Bolla gi� gestita";
                    break;
                case StatoBolla.ImpresaNonPresente:
                    res = "Impresa non esistente";
                    break;
                case StatoBolla.OrdineAltroFornitore:
                    res = "Ordine gestito da un altro fornitore";
                    break;
                case StatoBolla.OrdineNonPresente:
                    res = "Ordine non esistente";
                    break;
                default:
                    res = string.Empty;
                    break;
            }

            return res;
        }
    }

    [Serializable]
    public class StatoBollaList : List<StatoBollaClass>
    {
    }
}