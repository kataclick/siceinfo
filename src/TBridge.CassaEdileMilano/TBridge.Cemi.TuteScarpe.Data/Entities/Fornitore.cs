namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Fornitore
    {
        private readonly string codiceFornitore;

        public Fornitore(string codiceFornitore)
        {
            this.codiceFornitore = codiceFornitore;
        }

        public string CodiceFornitore
        {
            get { return codiceFornitore; }
        }
    }
}