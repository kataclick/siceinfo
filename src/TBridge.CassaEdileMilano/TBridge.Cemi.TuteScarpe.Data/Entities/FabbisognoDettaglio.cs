using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoDettaglio
    {
        private readonly int idFabbisognoConfermatoDettaglio;
        private readonly int idLavoratore;
        private readonly string indumento;
        private readonly TagliaList listaTaglie;
        private readonly string nomeCompleto;
        private readonly int quantita;
        private string taglia;

        public FabbisognoDettaglio(int idFabbisognoConfermatoDettaglio, int idLavoratore, string nomeCompleto,
                                   string indumento, string taglia, int quantita, TagliaList listaTaglie)
        {
            this.idFabbisognoConfermatoDettaglio = idFabbisognoConfermatoDettaglio;
            this.idLavoratore = idLavoratore;
            this.nomeCompleto = nomeCompleto;
            this.indumento = indumento;
            this.taglia = taglia;
            this.quantita = quantita;
            this.listaTaglie = listaTaglie;
        }

        public int IdFabbisognoConfermatoDettaglio
        {
            get { return idFabbisognoConfermatoDettaglio; }
        }

        public int IdLavoratore
        {
            get { return idLavoratore; }
        }

        public string NomeCompleto
        {
            get { return nomeCompleto; }
        }

        public string Indumento
        {
            get { return indumento; }
        }

        public string Taglia
        {
            get { return taglia; }
            set { taglia = value; }
        }

        public int Quantita
        {
            get { return quantita; }
        }

        public TagliaList ListaTaglie
        {
            get { return listaTaglie; }
        }
    }
}