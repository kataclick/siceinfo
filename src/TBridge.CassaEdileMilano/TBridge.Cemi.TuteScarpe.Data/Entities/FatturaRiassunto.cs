using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FatturaRiassuntoVoce
    {
        private readonly string categoriaVestiario;
        private readonly int quantita;

        public FatturaRiassuntoVoce(string categoriaVestiario, int quantita)
        {
            this.categoriaVestiario = categoriaVestiario;
            this.quantita = quantita;
        }

        public string CategoriaVestiario
        {
            get { return categoriaVestiario; }
        }

        public int Quantita
        {
            get { return quantita; }
        }
    }

    public class FatturaRiassunto : List<FatturaRiassuntoVoce>
    {
    }
}