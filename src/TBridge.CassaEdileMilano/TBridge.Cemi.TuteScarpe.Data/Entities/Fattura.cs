using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class Fattura
    {
        private readonly string codiceFornitore;
        private readonly DateTime dataUpload;
        private readonly byte[] fileFattura;
        private readonly bool forzataCorrettezza;
        private readonly bool statoInserimento;
        private BollaList bolle;
        private int codiceFattura;
        private string descrizione;
        private int idFattura;

        public Fattura(string descrizione, DateTime dataUpload, string codiceFornitore, byte[] fileFattura)
        {
            this.descrizione = descrizione;
            this.dataUpload = dataUpload;
            this.codiceFornitore = codiceFornitore;
            this.fileFattura = fileFattura;
            statoInserimento = false;
            forzataCorrettezza = false;
            bolle = new BollaList();
        }

        public Fattura(int idFattura, int codiceFattura, string descrizione, DateTime dataUpload, string codiceFornitore,
                       bool statoInserimento, bool forzataCorrettezza)
        {
            this.idFattura = idFattura;
            this.codiceFattura = codiceFattura;
            this.descrizione = descrizione;
            this.dataUpload = dataUpload;
            this.codiceFornitore = codiceFornitore;
            this.statoInserimento = statoInserimento;
            this.forzataCorrettezza = forzataCorrettezza;
            bolle = new BollaList();
        }

        public int IdFattura
        {
            get { return idFattura; }
            set { idFattura = value; }
        }

        public int CodiceFattura
        {
            get { return codiceFattura; }
            set { codiceFattura = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        public byte[] FileFattura
        {
            get { return fileFattura; }
        }

        public DateTime DataUpload
        {
            get { return dataUpload; }
        }

        public string CodiceFornitore
        {
            get { return codiceFornitore; }
        }

        public bool StatoInserimento
        {
            get { return statoInserimento; }
        }

        public bool ForzataCorrettezza
        {
            get { return forzataCorrettezza; }
        }

        public BollaList Bolle
        {
            get { return bolle; }
            set { bolle = value; }
        }

        public bool Stato()
        {
            bool ret = true;

            if (Bolle != null)
            {
                foreach (Bolla b in Bolle)
                {
                    if (!(b.Stato.Count == 0))
                    {
                        ret = false;
                        break;
                    }
                }
            }

            return ret;
        }
    }
}