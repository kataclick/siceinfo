using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Ordine
    {
        private readonly string codiceFornitore;
        private readonly DateTime? dataConferma;
        private readonly DateTime dataCreazione;
        private readonly string descrizione;
        private readonly int idOrdine;
        private readonly FabbisognoList listaFabbisogni;
        private readonly DateTime? scaricatoFornitore;

        public Ordine(int idOrdine, DateTime dataCreazione, DateTime? dataConferma, string codiceFornitore,
                      string descrizione, DateTime? scaricatoFornitore)
        {
            this.idOrdine = idOrdine;
            this.dataCreazione = dataCreazione;
            this.dataConferma = dataConferma;
            this.codiceFornitore = codiceFornitore;
            this.descrizione = descrizione;
            this.scaricatoFornitore = scaricatoFornitore;
            listaFabbisogni = new FabbisognoList();
        }

        public Ordine(int idOrdine, DateTime dataCreazione, DateTime? dataConferma, string codiceFornitore,
                      string descrizione, FabbisognoList listaFabbisogni)
        {
            this.idOrdine = idOrdine;
            this.dataCreazione = dataCreazione;
            this.dataConferma = dataConferma;
            this.codiceFornitore = codiceFornitore;
            this.descrizione = descrizione;
            this.listaFabbisogni = listaFabbisogni;
        }

        public int IdOrdine
        {
            get { return idOrdine; }
        }

        public DateTime DataCreazione
        {
            get { return dataCreazione; }
        }

        public DateTime? DataConferma
        {
            get { return dataConferma; }
        }

        public string CodiceFornitore
        {
            get { return codiceFornitore; }
        }

        public string Descrizione
        {
            get { return descrizione; }
        }

        public DateTime? ScaricatoFornitore
        {
            get { return scaricatoFornitore; }
        }


        public FabbisognoList ListaFabbisogni
        {
            get { return listaFabbisogni; }
        }
    }
}