using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Fabbisogno
    {
        private readonly string codiceFornitore;
        private readonly DateTime dataConfermaImpresa;
        private readonly int idFabbisogno;
        private readonly int idImpresa;
        private readonly int progressivoFornitura;
        private readonly string ragioneSociale;

        public Fabbisogno(int idFabbisogno, string codiceFornitore, DateTime dataConfermaImpresa,
                          int progressivoFornitura)
        {
            this.idFabbisogno = idFabbisogno;
            this.codiceFornitore = codiceFornitore;
            this.dataConfermaImpresa = dataConfermaImpresa;
            this.progressivoFornitura = progressivoFornitura;
        }

        public Fabbisogno(int idFabbisogno, string codiceFornitore, DateTime dataConfermaImpresa, int idImpresa,
                          string ragioneSociale)
        {
            this.idFabbisogno = idFabbisogno;
            this.codiceFornitore = codiceFornitore;
            this.dataConfermaImpresa = dataConfermaImpresa;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
        }

        public Fabbisogno(int idFabbisogno, string codiceFornitore, DateTime dataConfermaImpresa, int idImpresa,
                          string ragioneSociale,
                          int progressivoFornitura)
        {
            this.idFabbisogno = idFabbisogno;
            this.codiceFornitore = codiceFornitore;
            this.dataConfermaImpresa = dataConfermaImpresa;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.progressivoFornitura = progressivoFornitura;
        }

        public int IdFabbisogno
        {
            get { return idFabbisogno; }
        }

        public string CodiceFornitore
        {
            get { return codiceFornitore; }
        }

        public DateTime DataConfermaImpresa
        {
            get { return dataConfermaImpresa; }
        }

        public int IdImpresa
        {
            get { return idImpresa; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
        }

        public int ProgressivoFornitura
        {
            get { return progressivoFornitura; }
        }
    }
}