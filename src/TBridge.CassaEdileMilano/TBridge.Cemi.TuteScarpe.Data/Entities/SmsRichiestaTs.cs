using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class SmsRichiestaTs
    {
        public SmsRichiestaTs()
        {
            IdImpresa = null;
        }

        public int IdSmsRicevuto { get; set; }
        public DateTime DataRicezione { get; set; }
        public int IdUtente { get; set; }
        public int IdLavoratore { get; set; }
        public string NumeroTelefono { get; set; }
        public string TestoTaglia { get; set; }
        public int? IdImpresa { get; set; }
    }
}