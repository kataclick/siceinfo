﻿using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoLavoratore
    {
        public FabbisognoLavoratore()
        {
            this.Gruppi = new GruppoFabbisognoLavoratoreCollection();
        }

        public Int32 IdImpresa { get; set; }

        public String RagioneSociale { get; set; }

        public Boolean ImpresaAsfaltista { get; set; }

        public Int32 IdLavoratore { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public GruppoFabbisognoLavoratoreCollection Gruppi { get; set; }

        public Boolean ForzatoAsfaltista
        {
            get 
            {
                if (this.Gruppi.Count > 0)
                {
                    if (this.Gruppi[0].Dettagli.Count > 0)
                    {
                        return this.Gruppi[0].Dettagli[0].ForzatoAsfaltista;
                    }
                }

                return false;
            }
        }
    }
}
