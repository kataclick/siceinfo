using System;
namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class VoceBolla
    {
        private readonly string tipoVestiario;
        private int idVoceBolla;
        private int quantita;
        private int quantitaFabbisogno;

        public VoceBolla(string tipoVestiario, int quantita)
        {
            idVoceBolla = -1;
            this.tipoVestiario = tipoVestiario;
            this.quantita = quantita;
            quantitaFabbisogno = 0;
        }

        public VoceBolla(string tipoVestiario, int quantita, int quantitaFabbisogno)
        {
            idVoceBolla = -1;
            this.tipoVestiario = tipoVestiario;
            this.quantita = quantita;
            this.quantitaFabbisogno = quantitaFabbisogno;
        }

        public int IdVoceBolla
        {
            get { return idVoceBolla; }
            set { idVoceBolla = value; }
        }

        public string TipoVestiario
        {
            get { return tipoVestiario; }
        }

        public int Quantita
        {
            get { return quantita; }
            set { quantita = value; }
        }

        public int QuantitaFabbisogno
        {
            get { return quantitaFabbisogno; }
            set { quantitaFabbisogno = value; }
        }

        public bool Stato()
        {
            if (Quantita == QuantitaFabbisogno)
                return true;
            else
                return false;
        }
    }
}