using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class Indirizzo
    {
        private string cap;
        private string comune;
        private bool corretto;
        private string email;
        private string fax;
        private string indirizzoVia;
        private string presso;
        private string provincia;
        private string telefono;

        public Indirizzo(string indirizzo, string provincia, string comune, string cap, bool corretto, string presso)
        {
            indirizzoVia = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.corretto = corretto;
            this.presso = presso;
        }

        public Indirizzo(string indirizzo, string provincia, string comune, string cap)
        {
            indirizzoVia = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
        }

        public Indirizzo()
        {
        }

        public string IndirizzoVia
        {
            get { return indirizzoVia; }
            set { indirizzoVia = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string Presso
        {
            get { return presso; }
            set { presso = value; }
        }

        public bool Corretto
        {
            get { return corretto; }
            set { corretto = value; }
        }

        public string IndirizzoCompleto
        {
            get { return IndirizzoVia + " - " + Comune + " " + Provincia + ", " + Cap; }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public String Cellulare { get; set; }
    }
}