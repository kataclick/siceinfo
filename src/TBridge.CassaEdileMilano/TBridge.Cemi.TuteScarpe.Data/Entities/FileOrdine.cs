using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FileOrdine
    {
        private readonly FileOrdineImpresaCollection listaOrdiniImprese = new FileOrdineImpresaCollection();

        public FileOrdineImpresaCollection ListaOrdiniImpresa
        {
            get { return listaOrdiniImprese; }
        }

        public void AggiungiOrdineImpresa(FileOrdineImpresa ordineImpresa)
        {
            listaOrdiniImprese.Add(ordineImpresa);
        }

        public string ScriviFile()
        {
            string lista = string.Empty;

            foreach (FileOrdineImpresa fileImpresa in listaOrdiniImprese)
                lista += fileImpresa.ScriviRecord();

            return lista;
        }
    }
}