﻿using System;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class OrdineAutomatico
    {
        public Int32 IdImpresa { get; set; }
        public String RagioneSociale { get; set; }
        public Int32 IdLavoratore { get; set; }
        public String Cognome { get; set; }
        public String Nome { get; set; }
        public String IdIndumento { get; set; }
        public String Descrizione { get; set; }
        public String Taglia { get; set; }
        public String TipologiaFornitura { get; set; }
    }
}