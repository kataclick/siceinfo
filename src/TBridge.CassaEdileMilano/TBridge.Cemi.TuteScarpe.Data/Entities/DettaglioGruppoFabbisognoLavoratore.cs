﻿using System;
using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class DettaglioGruppoFabbisognoLavoratore
    {
        public DettaglioGruppoFabbisognoLavoratore()
        {
            this.Taglie = new List<string>();
        }

        public String IdIndumento { get; set; }

        public String Descrizione { get; set; }

        public Int32 Quantita { get; set; }

        public List<String> Taglie { get; set; }

        public String TagliaSelezionata { get; set; }

        public Boolean ForzatoAsfaltista { get; set; }

        public Boolean Selezionato { get; set; }
    }
}
