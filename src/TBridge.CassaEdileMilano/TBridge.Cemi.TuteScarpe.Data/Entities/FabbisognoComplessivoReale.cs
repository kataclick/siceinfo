using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoComplessivoReale
    {
        private readonly int codiceFabbisognoComplessivo;
        private readonly DateTime dataGenerazione;
        private readonly DateTime? dataScadenza;

        public FabbisognoComplessivoReale(int codiceFabbisognoComplessivo, DateTime dataGenerazione,
                                          DateTime? dataScadenza)
        {
            this.codiceFabbisognoComplessivo = codiceFabbisognoComplessivo;
            this.dataGenerazione = dataGenerazione;
            this.dataScadenza = dataScadenza;
        }

        public int CodiceFabbisognoComplessivo
        {
            get { return codiceFabbisognoComplessivo; }
        }

        public DateTime DataGenerazione
        {
            get { return dataGenerazione; }
        }

        public DateTime? DataScadenza
        {
            get { return dataScadenza; }
        }

        public DateTime? DataConfermaAutomatica { get; set; }

        public DateTime? DataConfermaAutomaticaEffettuata { get; set; }

        public DateTime? DataInvioMailEffettuato { get; set; }

        public DateTime? DataInvioSmsEffettuato { get; set; }
    }
}