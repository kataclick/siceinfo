﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class TagliePerTipo
    {
        public TagliePerTipo()
        {
            this.Indumento = new IndumentoCollection();
            this.Taglie = new List<string>();
        }

        public String TipoTaglia { get; set; }

        public IndumentoCollection Indumento { get; set; }

        public List<String> Taglie { get; set; }

        public Int32 IdLavoratore { get; set; }

        public String TagliaScelta { get; set; }
    }
}
