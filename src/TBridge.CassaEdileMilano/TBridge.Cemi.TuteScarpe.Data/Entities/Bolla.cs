using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class Bolla
    {
        private readonly DateTime data;
        private readonly int idImpresa;
        private readonly int idOrdine;
        private readonly StatoBollaList stato;
        private readonly List<string> tipiVestiario;
        private int idBolla;
        private string ragioneSociale;
        private VoceBollaList vociBolla;

        public Bolla(int idImpresa, int idOrdine, DateTime data)
        {
            idBolla = -1;
            this.idImpresa = idImpresa;
            this.idOrdine = idOrdine;
            this.data = data;
            vociBolla = new VoceBollaList();
            tipiVestiario = new List<string>();
            stato = new StatoBollaList();
        }

        public int IdBolla
        {
            get { return idBolla; }
            set { idBolla = value; }
        }

        public int IdImpresa
        {
            get { return idImpresa; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public int IdOrdine
        {
            get { return idOrdine; }
        }

        public DateTime Data
        {
            get { return data; }
        }

        public List<string> TipiVestiario
        {
            get { return tipiVestiario; }
        }

        public VoceBollaList VociBolla
        {
            get { return vociBolla; }
            set { vociBolla = value; }
        }

        public StatoBollaList Stato
        {
            get { return stato; }
        }

        public void AggiornaStato()
        {
            if (VociBolla != null)
            {
                foreach (VoceBolla b in VociBolla)
                {
                    if (!b.Stato())
                    {
                        stato.Add(new StatoBollaClass(StatoBolla.DifferenzaQuantita));
                        break;
                    }
                }
            }
        }

        private void AggiungiTipoVestiario(string tipoVestiario)
        {
            if (!tipiVestiario.Contains(tipoVestiario))
                tipiVestiario.Add(tipoVestiario);
        }

        public void AggiungiVoceBolla(VoceBolla voce)
        {
            vociBolla.Add(voce);
            AggiungiTipoVestiario(voce.TipoVestiario);
        }
    }
}