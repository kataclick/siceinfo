using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class IndumentoRiassunto
    {
        private string indumento;
        private int totale;

        public IndumentoRiassunto()
        {
        }

        public IndumentoRiassunto(string indumento, int totale)
        {
            this.indumento = indumento;
            this.totale = totale;
        }

        public string Indumento
        {
            get { return indumento; }
            set { indumento = value; }
        }

        public int Totale
        {
            get { return totale; }
            set { totale = value; }
        }
    }

    public class OrdineRiassuntoIndumenti : List<IndumentoRiassunto>
    {
    }

    public class OrdineRiassunto
    {
        private readonly OrdineRiassuntoIndumenti listaIndumenti;

        public OrdineRiassunto()
        {
            listaIndumenti = new OrdineRiassuntoIndumenti();
        }

        public OrdineRiassuntoIndumenti ListaIndumenti
        {
            get { return listaIndumenti; }
        }

        public void AggiungiIndumentoRiassunto(string indumento, int totale)
        {
            listaIndumenti.Add(new IndumentoRiassunto(indumento, totale));
        }
    }

    public class OrdineRiassuntoImpLav
    {
        private ListaElementiRiassunti listaElementi;

        public OrdineRiassuntoImpLav()
        {
            listaElementi = new ListaElementiRiassunti();
        }

        public ListaElementiRiassunti ListaElementi
        {
            get { return listaElementi; }
            set { listaElementi = value; }
        }

        public void AggiungiElemento(string nome, int totale)
        {
            listaElementi.Add(new ElementoRiassunto(nome, totale));
        }
    }

    public class ElementoRiassunto
    {
        private string nomeElemento;

        private int totale;

        public ElementoRiassunto()
        {
        }

        public ElementoRiassunto(string nome, int totale)
        {
            nomeElemento = nome;
            this.totale = totale;
        }

        public string NomeElemento
        {
            get { return nomeElemento; }
            set { nomeElemento = value; }
        }

        public int Totale
        {
            get { return totale; }
            set { totale = value; }
        }
    }

    public class ListaElementiRiassunti : List<ElementoRiassunto>
    {
    }
}