namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class TipoVestiario
    {
        private readonly string nomeVestiario;
        private readonly int quantita;

        public TipoVestiario(string nomeVestiario, int quantita)
        {
            this.nomeVestiario = nomeVestiario;
            this.quantita = quantita;
        }

        public string NomeVestiario
        {
            get { return nomeVestiario; }
        }

        public int Quantita
        {
            get { return quantita; }
        }
    }
}