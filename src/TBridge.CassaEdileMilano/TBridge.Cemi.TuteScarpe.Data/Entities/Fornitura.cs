namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Fornitura
    {
        private readonly int idImpresa;
        private readonly int progressivoFornitura;

        public Fornitura(int idImpresa, int progressivoFornitura)
        {
            this.idImpresa = idImpresa;
            this.progressivoFornitura = progressivoFornitura;
        }

        public int IdImpresa
        {
            get { return idImpresa; }
        }

        public int ProgressivoFornitura
        {
            get { return progressivoFornitura; }
        }
    }
}