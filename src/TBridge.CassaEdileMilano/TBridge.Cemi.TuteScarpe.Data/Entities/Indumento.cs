﻿using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Indumento
    {
        public String IdIndumento { get; set; }

        public String Descrizione { get; set; }

        public override string ToString()
        {
            return this.Descrizione;
        }
    }
}
