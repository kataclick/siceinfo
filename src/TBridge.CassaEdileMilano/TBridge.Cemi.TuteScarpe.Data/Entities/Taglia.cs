namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Taglia
    {
        private readonly string descrizioneTaglia;
        private readonly int idTaglia;

        public Taglia(int idTaglia, string descrizioneTaglia)
        {
            this.idTaglia = idTaglia;
            this.descrizioneTaglia = descrizioneTaglia;
        }

        public int IdTaglia
        {
            get { return idTaglia; }
        }

        public string DescrizioneTaglia
        {
            get { return descrizioneTaglia; }
        }
    }
}