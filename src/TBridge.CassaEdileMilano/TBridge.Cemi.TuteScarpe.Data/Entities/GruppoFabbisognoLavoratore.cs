﻿using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class GruppoFabbisognoLavoratore : ICloneable
    {
        public GruppoFabbisognoLavoratore()
        {
            this.Dettagli = new DettaglioGruppoFabbisognoLavoratoreCollection();
        }

        public Int32 IdGruppo { get; set; }

        public String Descrizione { get; set; }

        public Int32 CodiceAlternative { get; set; }

        public Boolean GruppoPerAsfaltisti { get; set; }

        public Boolean Selezionato { get; set; }

        public DettaglioGruppoFabbisognoLavoratoreCollection Dettagli { get; set; }

        public object Clone()
        {
            GruppoFabbisognoLavoratore cloned = new GruppoFabbisognoLavoratore();

            cloned.IdGruppo = this.IdGruppo;
            cloned.Descrizione = this.Descrizione;
            cloned.CodiceAlternative = this.CodiceAlternative;
            cloned.GruppoPerAsfaltisti = this.GruppoPerAsfaltisti;
            cloned.Selezionato = this.Selezionato;

            cloned.Dettagli = new DettaglioGruppoFabbisognoLavoratoreCollection();
            foreach (DettaglioGruppoFabbisognoLavoratore dett in this.Dettagli)
            {
                DettaglioGruppoFabbisognoLavoratore clonedDett = new DettaglioGruppoFabbisognoLavoratore();
                cloned.Dettagli.Add(clonedDett);

                clonedDett.Descrizione = dett.Descrizione;
                clonedDett.ForzatoAsfaltista = dett.ForzatoAsfaltista;
                clonedDett.IdIndumento = dett.IdIndumento;
                clonedDett.Quantita = dett.Quantita;
                clonedDett.TagliaSelezionata = dett.TagliaSelezionata;
                clonedDett.Taglie = dett.Taglie;
                clonedDett.Selezionato = dett.Selezionato;
            }

            return cloned;
        }
    }
}
