using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class RapportoFornitoreImpresa
    {
        public int? IdRapportoFornitoreImpresa { get; set; }

        public int IdFornitore { get; set; }

        public int IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }

        public DateTime DataInizio { get; set; }

        public DateTime? DataFine { get; set; }

        public Indirizzo SedeLegale { get; set; }

        public Indirizzo SedeAmministrativa { get; set; }

        public Int32 NumeroLavoratori { get; set; }
    }
}