using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoComplessivo
    {
        private readonly DateTime dataConfermaImpresa;
        private readonly int idFabbisognoComplessivo;
        private readonly int idImpresa;
        private readonly FabbisognoList listaFabbisogni;
        private readonly string ragioneSocialeImpresa;
        private DateTime dataFabbisognoComplessivo;

        public FabbisognoComplessivo(int idFabbisognoComplessivo, DateTime dataFabbisognoComplessivo,
                                     FabbisognoList listaFabbisogni, int idImpresa, string ragioneSocialeImpresa,
                                     DateTime dataConfermaImpresa)
        {
            this.idFabbisognoComplessivo = idFabbisognoComplessivo;
            this.dataFabbisognoComplessivo = dataFabbisognoComplessivo;
            this.listaFabbisogni = listaFabbisogni;
            this.idImpresa = idImpresa;
            this.ragioneSocialeImpresa = ragioneSocialeImpresa;
            this.dataConfermaImpresa = dataConfermaImpresa;
        }

        public FabbisognoComplessivo(int idFabbisognoComplessivo, DateTime dataFabbisognoComplessivo)
        {
            this.idFabbisognoComplessivo = idFabbisognoComplessivo;
            this.dataFabbisognoComplessivo = dataFabbisognoComplessivo;
        }

        public int IdFabbisognoComplessivo
        {
            get { return idFabbisognoComplessivo; }
        }

        public DateTime DataFabbisognoComplessivo
        {
            get { return dataFabbisognoComplessivo; }
        }

        public FabbisognoList ListaFabbisogni
        {
            get { return listaFabbisogni; }
        }

        public int IdImpresa
        {
            get { return idImpresa; }
        }

        public string RagioneSocialeImpresa
        {
            get { return ragioneSocialeImpresa; }
        }

        public DateTime DataConfermaImpresa
        {
            get { return dataConfermaImpresa; }
        }

        public string Data
        {
            get { return dataFabbisognoComplessivo.ToShortDateString(); }
        }
    }
}