﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    public class IndumentoCollection : List<Indumento>
    {
        public Boolean PresenteIndumentoPerDescrizione(String descrizione)
        {
            foreach (Indumento indumento in this)
            {
                if (indumento.Descrizione == descrizione)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
