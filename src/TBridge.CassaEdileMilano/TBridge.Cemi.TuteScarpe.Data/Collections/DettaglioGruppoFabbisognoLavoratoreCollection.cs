﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    public class DettaglioGruppoFabbisognoLavoratoreCollection : List<DettaglioGruppoFabbisognoLavoratore>
    {
        public Boolean EliminaIndumento(String idIndumento)
        {
            Boolean res = false;

            for (Int32 i = 0; i < this.Count; i++)
            {
                if (this[i].IdIndumento == idIndumento)
                {
                    this.RemoveAt(i);
                    res = true;
                    break;
                }
            }

            return res;
        }

        public DettaglioGruppoFabbisognoLavoratore GetIndumento(String idIndumento)
        {
            DettaglioGruppoFabbisognoLavoratore res = null;

            foreach (DettaglioGruppoFabbisognoLavoratore dett in this)
            {
                if (dett.IdIndumento == idIndumento)
                {
                    res = dett;
                    break;
                }
            }

            return res;
        }
    }
}
