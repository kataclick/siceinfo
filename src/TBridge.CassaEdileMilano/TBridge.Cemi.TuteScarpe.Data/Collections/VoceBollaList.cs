using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    [Serializable]
    public class VoceBollaList : List<VoceBolla>
    {
        public new void Add(VoceBolla voce)
        {
            foreach (VoceBolla vb in this)
            {
                if (vb.TipoVestiario == voce.TipoVestiario)
                    vb.Quantita += voce.Quantita;
                else
                    base.Add(voce);
            }
        }
    }
}