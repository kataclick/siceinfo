using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    [Serializable]
    public class RapportoConsulenteImpresaCollection : List<RapportoConsulenteImpresa>
    {
        public RapportoConsulenteImpresa RapportoPresente(RapportoConsulenteImpresa rapporto)
        {
            foreach (RapportoConsulenteImpresa rap in this)
            {
                if (rap.IdImpresa == rapporto.IdImpresa)
                    return rap;
            }

            return null;
        }
    }
}