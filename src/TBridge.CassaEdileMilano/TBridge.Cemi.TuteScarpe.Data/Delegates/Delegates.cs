using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Delegates
{
    public delegate void ConsulenteSelectedEventHandler(Consulente consulente);
}