using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Business.SmsServiceReference;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using TBridge.Cemi.TuteScarpe.Type.Collections;
using TBridge.Cemi.TuteScarpe.Type.Entities;
using Fornitore = TBridge.Cemi.TuteScarpe.Data.Entities.Fornitore;
using Impresa = TBridge.Cemi.Type.Entities.Impresa;

namespace TBridge.Cemi.TuteScarpe.Data
{
    public class TSDataAccess
    {
        public TSDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        private Database DatabaseCemi { get; set; }

        #region Metodi per la gestione degli indirizzi-province-CAP

        public DataTable GetPreIndirizzi()
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PreIndirizzi_Select");
            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        public DataTable GetProvince()
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Comuni_Select_Province");
            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        public DataTable GetComuniDellaProvincia(int provincia)
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Comuni_Select_ComuniByProvincia");
            DatabaseCemi.AddInParameter(comando, "@idProvincia", DbType.Int32, provincia);
            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        public DataTable GetCAPDelComune(Int64 idComune)
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Comuni_Select_CAPByComune");
            DatabaseCemi.AddInParameter(comando, "@idComune", DbType.Int64, idComune);
            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        #endregion

        public DataTable GetLegendaTaglie()
        {
            DataTable ret = null;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_GetLegendaTaglie"))
            {
                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    if (ds != null && ds.Tables.Count > 0)
                        ret = ds.Tables[0];
                }
            }

            return ret;
        }

        /// <summary>
        ///   Otteniamo l'elenco dei lavoratori di uncerto fabbisogno a partire da un utente di tipo impresa
        /// </summary>
        /// <param name = "idUtente">idutente dell'impresa</param>
        /// <returns></returns>
        /// <param name = "completati"></param>
        public DataTable ElencoLavoratori(int idUtente, bool? completati)
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeElencoLavoratoriByIdUtenteImpresa");
            DatabaseCemi.AddInParameter(comando, "@idUtenteImpresa", DbType.Int32, idUtente);

            if (completati.HasValue)
                DatabaseCemi.AddInParameter(comando, "@completati", DbType.Boolean, completati);

            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);
            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        /// <summary>
        ///   Otteniamo l'elenco dei lavoratori di uncerto fabbisogno a partire da un utente di tipo impresa
        /// </summary>
        /// <param name = "idImpresa">idutente dell'impresa</param>
        /// <param name = "completati"></param>
        /// <returns></returns>
        public DataTable ElencoLavoratoriImpresa(int idImpresa, bool? completati)
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeElencoLavoratoriByIdUtenteImpresa");
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

            if (completati.HasValue)
                DatabaseCemi.AddInParameter(comando, "@completati", DbType.Boolean, completati);

            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);
            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        public bool EsisteFabbisogno(int idImpresa)
        {
            bool res;

            int iRes;
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineTuteScarpeSelectEsiste"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                iRes = (int) DatabaseCemi.ExecuteScalar(comando);
            }

            if (iRes > 0)
                res = true;
            else
                res = false;

            return res;
        }

        public void PulisciOrdineTemporaneo(int idUtente)
        {
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineTemporaneoDeleteByUtente")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

                DatabaseCemi.ExecuteScalar(comando);
            }
        }

        /// <summary>
        ///   Memorizziamo la dichiarazione di forzatura asfaltista
        /// </summary>
        /// <param name = "idUtente"></param>
        public int MemorizzaForzaAsfaltista(int idUtente)
        {
            int ret = -1;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeForzaAsfaltista"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtenteImpresa", DbType.Int32, idUtente);
                //Per il momento non prevediamo che si posso tornare indietro da quella dichiarazione, cmq la SP lo prevede
                DatabaseCemi.AddInParameter(comando, "@forzatura", DbType.Boolean, true);

                ret = DatabaseCemi.ExecuteNonQuery(comando);
            }

            return ret;
        }

        /// <summary>
        ///   Eliminiamo un lavoratore dal fabbisogno. In realt� si tratta di una disattivazione
        /// </summary>
        /// <param name = "idImpresa"></param>
        /// <param name = "idLavoratore"></param>
        /// <returns></returns>
        public int EliminaLavoratore(int idImpresa, int idLavoratore)
        {
            int ret;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ComposizioneOrdineTuteScarpeDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                ret = (int) DatabaseCemi.ExecuteScalar(comando);
            }

            return ret;
        }

        /// <summary>
        ///   Ripristianiamo un lavoratore nel fabbisogno
        /// </summary>
        /// <param name = "idImpresa"></param>
        /// <param name = "idLavoratore"></param>
        /// <returns></returns>
        public int RipristinaLavoratore(int idImpresa, int idLavoratore)
        {
            int ret = -1;

            try
            {
                using (
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_ComposizioneOrdineTuteScarpeUndelete"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                    ret = (int) DatabaseCemi.ExecuteScalar(comando);
                }
            }
            catch
            {
            }

            return ret;
        }

        public DataTable GetLavoratoriIncompleti(int idImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_OrdineTuteScarpeSelectLavoratoriIncompleti"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                // Ricavo il risultato da un dataset
                using (DataSet dsRes = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsRes != null && dsRes.Tables.Count > 0)
                    {
                        return dsRes.Tables[0];
                    }
                }
            }

            return null;
        }

        public DataTable GetStorico(int idImpresa)
        {
            DataTable dt = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeStoricoSelectByImpresa"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                using (DataSet ds = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (ds != null && ds.Tables.Count > 0)
                        dt = ds.Tables[0];
                }
            }

            return dt;
        }

        public DataTable GetFabbisogniNonGestiti(int idImpresa)
        {
            DataTable dt = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiSelectNonGestiti"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                using (DataSet ds = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (ds != null && ds.Tables.Count > 0)
                        dt = ds.Tables[0];
                }
            }

            return dt;
        }

        public DataTable GetFabbisogniConfermatiDettagliComeXML(int idFabbisognoComplessivo, int idImpresa)
        {
            DataTable dt = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiDettagliSelectComeXML"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                using (DataSet ds = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (ds != null && ds.Tables.Count > 0)
                        dt = ds.Tables[0];
                }
            }

            return dt;
        }

        public DataTable GetOrdine(int idStoricoTS, string inutile)
        {
            DataTable dt = null;

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeStoricoSelectById");
            DatabaseCemi.AddInParameter(comando, "@idStorico", DbType.Int32, idStoricoTS);
            using (DataSet ds = DatabaseCemi.ExecuteDataSet(comando))
            {
                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }

            return dt;
        }

        public Ordine GetOrdine(int idOrdine)
        {
            Ordine ordine = null;

            DataSet dsOrdine;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
                //DatabaseCemi.AddInParameter(comando, "@confermati", DbType.Boolean, null);
                dsOrdine = DatabaseCemi.ExecuteDataSet(comando);
            }

            if ((dsOrdine != null) && (dsOrdine.Tables.Count == 1) && (dsOrdine.Tables[0].Rows.Count == 1))
            {
                int idOrdineDb = -1;
                DateTime dataCreazione = new DateTime();
                DateTime? dataConferma = null;
                string codiceFornitore = string.Empty;
                string descrizione = string.Empty;
                DateTime? scaricatoFornitore = null;

                if (dsOrdine.Tables[0].Rows[0]["idTuteScarpeOrdine"] != DBNull.Value)
                    idOrdineDb = (int) dsOrdine.Tables[0].Rows[0]["idTuteScarpeOrdine"];

                if (dsOrdine.Tables[0].Rows[0]["dataCreazione"] != DBNull.Value)
                    dataCreazione = (DateTime) dsOrdine.Tables[0].Rows[0]["dataCreazione"];

                if (dsOrdine.Tables[0].Rows[0]["dataConferma"] != DBNull.Value)
                    dataConferma = (DateTime) dsOrdine.Tables[0].Rows[0]["dataConferma"];

                if (dsOrdine.Tables[0].Rows[0]["codiceFornitore"] != DBNull.Value)
                    codiceFornitore = (string) dsOrdine.Tables[0].Rows[0]["codiceFornitore"];

                if (dsOrdine.Tables[0].Rows[0]["descrizione"] != DBNull.Value)
                    descrizione = (string) dsOrdine.Tables[0].Rows[0]["descrizione"];

                if (dsOrdine.Tables[0].Rows[0]["scaricatoFornitore"] != DBNull.Value)
                    scaricatoFornitore = (DateTime) dsOrdine.Tables[0].Rows[0]["scaricatoFornitore"];

                ordine =
                    new Ordine(idOrdineDb, dataCreazione, dataConferma, codiceFornitore, descrizione, scaricatoFornitore);
            }

            return ordine;
        }

        private void PuliziaOrdini(Fornitura fornitura, DbTransaction transaction)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpePuliziaComposizioneOrdine"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, fornitura.IdImpresa);
                DatabaseCemi.ExecuteScalar(comando, transaction);
            }
        }

        public void ConfermaFabbisogno(int idImpresa, Indirizzo indirizzo, int? idConsulente, int? idFornitore)
        {
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        IEnumerable<Fornitura> forniture = GetListaForniture(idImpresa);
                        if (forniture != null)
                        {
                            foreach (Fornitura fornitura in forniture)
                            {
                                PuliziaOrdini(fornitura, transaction);

                                SaveStorico(fornitura, transaction, indirizzo, idConsulente, idFornitore);
                            }
                        }
                        else
                            throw new Exception();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw new Exception();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        private IEnumerable<Fornitura> GetListaForniture(int idImpresa)
        {
            FornituraList listaForniture = new FornituraList();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeSelectForniture"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                using (DataSet dsForniture = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsForniture != null) && (dsForniture.Tables.Count == 1))
                    {
                        for (int i = 0; i < dsForniture.Tables[0].Rows.Count; i++)
                        {
                            int progressivoFornitura = (int) dsForniture.Tables[0].Rows[i]["progressivoFornitura"];

                            listaForniture.Add(new Fornitura(idImpresa, progressivoFornitura));
                        }
                    }
                }
            }

            return listaForniture;
        }

        public void SaveStorico(Fornitura fornitura, DbTransaction transaction, Indirizzo indirizzo, int? idConsulente,
                                int? idFornitore)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConfermaFabbisogno"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, fornitura.IdImpresa);
                DatabaseCemi.AddInParameter(comando, "@progressivoFornitura", DbType.Int32,
                                            fornitura.ProgressivoFornitura);
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzo.IndirizzoVia);
                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, indirizzo.Provincia);
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, indirizzo.Comune);
                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, indirizzo.Cap);
                if (!String.IsNullOrEmpty(indirizzo.Presso))
                {
                    DatabaseCemi.AddInParameter(comando, "@presso", DbType.String, indirizzo.Presso);
                }
                if (!String.IsNullOrEmpty(indirizzo.Telefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, indirizzo.Telefono);
                }
                if (!String.IsNullOrEmpty(indirizzo.Cellulare))
                {
                    DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String, indirizzo.Cellulare);
                }
                if (idConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
                }
                if (idFornitore.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);
                }

                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        public int InserisciIndirizzoOrdine(string preIndirizzo, string indirizzo, string provincia, string comune,
                                            string cap)
        {
            try
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_IndirizzoOrdineTuteScarpeInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@preIndirizzo", DbType.String, preIndirizzo);
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzo);
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, provincia);
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comune);
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cap);

                    return (int) DatabaseCemi.ExecuteScalar(comando);
                }
            }
            catch
            {
                return -1;
            }
        }

        public Indirizzo GetIndirizzoAmministrativo(int idImpresa)
        {
            Indirizzo indirizzo = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
                using (DataSet dsIndirizzo = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsIndirizzo != null) && (dsIndirizzo.Tables.Count == 1) &&
                        (dsIndirizzo.Tables[0].Rows.Count == 1))
                    {
                        string ind = string.Empty;
                        string provincia = string.Empty;
                        string comune = string.Empty;
                        string cap = string.Empty;
                        bool corretto;

                        if (dsIndirizzo.Tables[0].Rows[0]["indirizzoSedeAmministrazione"] != DBNull.Value)
                            ind = (string) dsIndirizzo.Tables[0].Rows[0]["indirizzoSedeAmministrazione"];

                        if (dsIndirizzo.Tables[0].Rows[0]["provinciaSedeAmministrazione"] != DBNull.Value)
                            provincia = (string) dsIndirizzo.Tables[0].Rows[0]["provinciaSedeAmministrazione"];

                        if (dsIndirizzo.Tables[0].Rows[0]["localitaSedeAmministrazione"] != DBNull.Value)
                            comune = (string) dsIndirizzo.Tables[0].Rows[0]["localitaSedeAmministrazione"];

                        if (dsIndirizzo.Tables[0].Rows[0]["capSedeAmministrazione"] != DBNull.Value)
                            cap = (string) dsIndirizzo.Tables[0].Rows[0]["capSedeAmministrazione"];

                        if ((ind != string.Empty) && (provincia != string.Empty) && (comune != string.Empty) &&
                            (cap != string.Empty))
                            corretto = true;
                        else
                            corretto = false;

                        indirizzo = new Indirizzo(ind, provincia, comune, cap, corretto, null);
                    }
                }
            }

            return indirizzo;
        }

        public FabbisognoComplessivoList GetFabbisogniConfermati(int? idImpresa, string ragioneSocialeImpresa)
        {
            FabbisognoComplessivoList listaFabbisogniComplessivi = new FabbisognoComplessivoList();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiSelect")
                )
            {
                if (idImpresa != null)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                if (ragioneSocialeImpresa != null)
                    DatabaseCemi.AddInParameter(comando, "@ragioneSocialeImpresa", DbType.String, ragioneSocialeImpresa);

                using (DataSet dsFabbisogni = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsFabbisogni != null) && (dsFabbisogni.Tables.Count == 1))
                    {
                        FabbisognoComplessivo fabbisognoPrec = null;
                        for (int i = 0; i < dsFabbisogni.Tables[0].Rows.Count; i++)
                        {
                            int idFabbisogno = -1;
                            int id = -1;
                            string ragioneSociale = string.Empty;
                            DateTime dataConfermaImpresa = new DateTime();
                            int idFabbisognoComplessivo = -1;
                            DateTime dataFabbisognoComplessivo = new DateTime();
                            string codiceFornitore = string.Empty;
                            int progressivoFornitura = -1;

                            if (dsFabbisogni.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                                id = (int) dsFabbisogni.Tables[0].Rows[i]["idImpresa"];
                            if (dsFabbisogni.Tables[0].Rows[i]["idFabbisognoComplessivo"] != DBNull.Value)
                                idFabbisognoComplessivo =
                                    (int) dsFabbisogni.Tables[0].Rows[i]["idFabbisognoComplessivo"];
                            if (dsFabbisogni.Tables[0].Rows[i]["dataGenerazione"] != DBNull.Value)
                                dataFabbisognoComplessivo = (DateTime) dsFabbisogni.Tables[0].Rows[i]["dataGenerazione"];
                            if (dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"] != DBNull.Value)
                                ragioneSociale = (string) dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"];
                            if (dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"] != DBNull.Value)
                                dataConfermaImpresa = (DateTime) dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"];

                            if ((fabbisognoPrec == null) ||
                                (fabbisognoPrec.IdFabbisognoComplessivo != idFabbisognoComplessivo) ||
                                (fabbisognoPrec.IdImpresa != id))
                            {
                                fabbisognoPrec =
                                    new FabbisognoComplessivo(idFabbisognoComplessivo, dataFabbisognoComplessivo,
                                                              new FabbisognoList(), id, ragioneSociale,
                                                              dataConfermaImpresa);
                                listaFabbisogniComplessivi.Add(fabbisognoPrec);
                            }

                            if (dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"] != DBNull.Value)
                                idFabbisogno = (int) dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"];
                            //if (dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"] != DBNull.Value)
                            //    dataConfermaImpresa = (DateTime)dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"];
                            if (dsFabbisogni.Tables[0].Rows[i]["progressivoFornitura"] != DBNull.Value)
                                progressivoFornitura = (int) dsFabbisogni.Tables[0].Rows[i]["progressivoFornitura"];
                            if (dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                                codiceFornitore = (string) dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"];

                            fabbisognoPrec.ListaFabbisogni.Add(
                                new Fabbisogno(idFabbisogno, codiceFornitore, dataConfermaImpresa, progressivoFornitura));
                        }
                    }
                }
            }

            return listaFabbisogniComplessivi;
        }

        public FabbisognoList GetFabbisogniConfermati(int idFabbisognoComplessivo)
        {
            FabbisognoList listaFabbisogni = new FabbisognoList();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiSelect")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);

                using (DataSet dsFabbisogni = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsFabbisogni != null) && (dsFabbisogni.Tables.Count == 1))
                    {
                        for (int i = 0; i < dsFabbisogni.Tables[0].Rows.Count; i++)
                        {
                            int idFabbisogno = -1;
                            int id = -1;
                            string ragioneSociale = string.Empty;
                            DateTime dataConfermaImpresa = new DateTime();
                            string codiceFornitore = string.Empty;

                            if (dsFabbisogni.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                                id = (int) dsFabbisogni.Tables[0].Rows[i]["idImpresa"];
                            if (dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"] != DBNull.Value)
                                ragioneSociale = (string) dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"];
                            if (dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"] != DBNull.Value)
                                idFabbisogno = (int) dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"];
                            if (dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"] != DBNull.Value)
                                dataConfermaImpresa = (DateTime) dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"];
                            if (dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                                codiceFornitore = (string) dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"];

                            listaFabbisogni.Add(
                                new Fabbisogno(idFabbisogno, codiceFornitore, dataConfermaImpresa, id, ragioneSociale));
                        }
                    }
                }
            }

            return listaFabbisogni;
        }

        public FabbisognoDettaglioList GetFabbisogniConfermatiDettagli(Int32 idImpresa, Int32 idFabbisognoComplessivo)
        {
            FabbisognoDettaglioList listaDettagli = new FabbisognoDettaglioList();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiDettagliSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);
                using (DataSet dsDettagli = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsDettagli != null) && (dsDettagli.Tables.Count == 1))
                    {
                        for (int i = 0; i < dsDettagli.Tables[0].Rows.Count; i++)
                        {
                            int idFabbisognoConfermatoDettaglio = -1;
                            int idLavoratore = -1;
                            string nomeCompleto = string.Empty;
                            string idIndumento = string.Empty;
                            string indumento = string.Empty;
                            string taglia = string.Empty;
                            int quantita = -1;

                            if (dsDettagli.Tables[0].Rows[i]["idFabbisognoConfermatoDettaglio"] != DBNull.Value)
                                idFabbisognoConfermatoDettaglio =
                                    (int) dsDettagli.Tables[0].Rows[i]["idFabbisognoConfermatoDettaglio"];

                            if (dsDettagli.Tables[0].Rows[i]["idLavoratore"] != DBNull.Value)
                                idLavoratore = (int) dsDettagli.Tables[0].Rows[i]["idLavoratore"];

                            if (dsDettagli.Tables[0].Rows[i]["nomeCompleto"] != DBNull.Value)
                                nomeCompleto = (string) dsDettagli.Tables[0].Rows[i]["nomeCompleto"];

                            if (dsDettagli.Tables[0].Rows[i]["idIndumento"] != DBNull.Value)
                                idIndumento = ((string) dsDettagli.Tables[0].Rows[i]["idIndumento"]).Trim();

                            if (dsDettagli.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                                indumento = ((string) dsDettagli.Tables[0].Rows[i]["descrizione"]).Trim();

                            if (dsDettagli.Tables[0].Rows[i]["taglia"] != DBNull.Value)
                                taglia = (string) dsDettagli.Tables[0].Rows[i]["taglia"];

                            if (dsDettagli.Tables[0].Rows[i]["quantita"] != DBNull.Value)
                                quantita = int.Parse(dsDettagli.Tables[0].Rows[i]["quantita"].ToString());

                            TagliaList listaTaglie = GetTaglieArticolo(idIndumento);

                            if (listaTaglie != null)
                            {
                                listaDettagli.Add(
                                    new FabbisognoDettaglio(idFabbisognoConfermatoDettaglio, idLavoratore, nomeCompleto,
                                                            indumento, taglia, quantita, listaTaglie));
                            }
                        }
                    }
                }
            }

            return listaDettagli;
        }

        public FabbisognoDettaglioList GetFabbisogniConfermatiDettagli(int idFabbisognoConfermato)
        {
            FabbisognoDettaglioList listaDettagli = new FabbisognoDettaglioList();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiDettagliSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFabbisognoConfermato", DbType.Int32, idFabbisognoConfermato);
                using (DataSet dsDettagli = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsDettagli != null) && (dsDettagli.Tables.Count == 1))
                    {
                        for (int i = 0; i < dsDettagli.Tables[0].Rows.Count; i++)
                        {
                            int idFabbisognoConfermatoDettaglio = -1;
                            int idLavoratore = -1;
                            string nomeCompleto = string.Empty;
                            string idIndumento = string.Empty;
                            string indumento = string.Empty;
                            string taglia = string.Empty;
                            int quantita = -1;

                            if (dsDettagli.Tables[0].Rows[i]["idFabbisognoConfermatoDettaglio"] != DBNull.Value)
                                idFabbisognoConfermatoDettaglio =
                                    (int) dsDettagli.Tables[0].Rows[i]["idFabbisognoConfermatoDettaglio"];

                            if (dsDettagli.Tables[0].Rows[i]["idLavoratore"] != DBNull.Value)
                                idLavoratore = (int) dsDettagli.Tables[0].Rows[i]["idLavoratore"];

                            if (dsDettagli.Tables[0].Rows[i]["nomeCompleto"] != DBNull.Value)
                                nomeCompleto = (string) dsDettagli.Tables[0].Rows[i]["nomeCompleto"];

                            if (dsDettagli.Tables[0].Rows[i]["idIndumento"] != DBNull.Value)
                                idIndumento = ((string) dsDettagli.Tables[0].Rows[i]["idIndumento"]).Trim();

                            if (dsDettagli.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                                indumento = ((string) dsDettagli.Tables[0].Rows[i]["descrizione"]).Trim();

                            if (dsDettagli.Tables[0].Rows[i]["taglia"] != DBNull.Value)
                                taglia = (string) dsDettagli.Tables[0].Rows[i]["taglia"];

                            if (dsDettagli.Tables[0].Rows[i]["quantita"] != DBNull.Value)
                                quantita = int.Parse(dsDettagli.Tables[0].Rows[i]["quantita"].ToString());

                            TagliaList listaTaglie = GetTaglieArticolo(idIndumento);

                            if (listaTaglie != null)
                            {
                                listaDettagli.Add(
                                    new FabbisognoDettaglio(idFabbisognoConfermatoDettaglio, idLavoratore, nomeCompleto,
                                                            indumento, taglia, quantita, listaTaglie));
                            }
                        }
                    }
                }
            }

            return listaDettagli;
        }

        public TagliaList GetTaglieArticolo(string idIndumento)
        {
            DbCommand comando;
            TagliaList listaTaglie = new TagliaList();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeTaglieSelect");
            DatabaseCemi.AddInParameter(comando, "@idArticolo", DbType.String, idIndumento);
            DataSet dsTaglie = DatabaseCemi.ExecuteDataSet(comando);

            if ((dsTaglie != null) && (dsTaglie.Tables.Count == 1))
            {
                for (int i = 0; i < dsTaglie.Tables[0].Rows.Count; i++)
                {
                    int idTaglia = -1;
                    string descrizioneTaglia = string.Empty;

                    if (dsTaglie.Tables[0].Rows[i]["idTaglia"] != DBNull.Value)
                        idTaglia = (int) dsTaglie.Tables[0].Rows[i]["idTaglia"];

                    if (dsTaglie.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizioneTaglia = (string) dsTaglie.Tables[0].Rows[i]["descrizione"];

                    listaTaglie.Add(new Taglia(idTaglia, descrizioneTaglia));
                }
            }

            return listaTaglie;
        }

        public void AggiornaDettaglioFabbisogno(int idFabbisognoConfermatoDettaglio, string taglia)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiDettagliUpdate");
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoConfermatoDettaglio", DbType.Int32,
                                        idFabbisognoConfermatoDettaglio);
            DatabaseCemi.AddInParameter(comando, "@taglia", DbType.String, taglia);
            DatabaseCemi.ExecuteNonQuery(comando);
        }

        public FabbisognoComplessivoList GetFabbisogniComplessivi()
        {
            DbCommand comando;
            FabbisognoComplessivoList listaFabb = new FabbisognoComplessivoList();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniComplessiviSelect");
            DataSet dsFabb = DatabaseCemi.ExecuteDataSet(comando);

            if ((dsFabb != null) && (dsFabb.Tables.Count == 1))
            {
                for (int i = 0; i < dsFabb.Tables[0].Rows.Count; i++)
                {
                    int idFabbisognoComplessivo = -1;
                    DateTime dataGenerazione = new DateTime();

                    if (dsFabb.Tables[0].Rows[i]["idFabbisognoComplessivo"] != DBNull.Value)
                        idFabbisognoComplessivo = (int) dsFabb.Tables[0].Rows[i]["idFabbisognoComplessivo"];

                    if (dsFabb.Tables[0].Rows[i]["dataGenerazione"] != DBNull.Value)
                        dataGenerazione = (DateTime) dsFabb.Tables[0].Rows[i]["dataGenerazione"];

                    listaFabb.Add(new FabbisognoComplessivo(idFabbisognoComplessivo, dataGenerazione));
                }
            }

            return listaFabb;
        }

        public OrdineList GetOrdini(bool confermati, DateTime? dal, DateTime? al)
        {
            return GetOrdini(null, confermati, dal, al, null, null);
        }

        public OrdineList GetOrdini(string codiceFornitore, bool confermati, DateTime? dal, DateTime? al,
                                    string ragioneSociale, int? idImpresa)
        {
            DbCommand comando;
            OrdineList listaOrdini = new OrdineList();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniSelect");
            DatabaseCemi.AddInParameter(comando, "@confermati", DbType.Boolean, confermati);
            if (codiceFornitore != null)
                DatabaseCemi.AddInParameter(comando, "@codiceFornitore", DbType.String, codiceFornitore);
            if (dal.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, dal);
            if (al.HasValue)
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, al);
            if (ragioneSociale != null)
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSociale);
            if (idImpresa.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

            DataSet dsOrdini = DatabaseCemi.ExecuteDataSet(comando);

            if ((dsOrdini != null) && (dsOrdini.Tables.Count == 1))
            {
                for (int i = 0; i < dsOrdini.Tables[0].Rows.Count; i++)
                {
                    int idOrdine = -1;
                    DateTime dataCreazione = new DateTime();
                    DateTime? dataConferma = null;
                    string codice = string.Empty;
                    string descrizione = string.Empty;
                    DateTime? scaricatoFornitore = null;

                    if (dsOrdini.Tables[0].Rows[i]["idTuteScarpeOrdine"] != DBNull.Value)
                        idOrdine = (int) dsOrdini.Tables[0].Rows[i]["idTuteScarpeOrdine"];

                    if (dsOrdini.Tables[0].Rows[i]["dataCreazione"] != DBNull.Value)
                        dataCreazione = (DateTime) dsOrdini.Tables[0].Rows[i]["dataCreazione"];

                    if (dsOrdini.Tables[0].Rows[i]["dataConferma"] != DBNull.Value)
                        dataConferma = (DateTime) dsOrdini.Tables[0].Rows[i]["dataConferma"];

                    if (dsOrdini.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                        codice = (string) dsOrdini.Tables[0].Rows[i]["codiceFornitore"];

                    if (dsOrdini.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsOrdini.Tables[0].Rows[i]["descrizione"];

                    if (dsOrdini.Tables[0].Rows[i]["scaricatoFornitore"] != DBNull.Value)
                        scaricatoFornitore = (DateTime) dsOrdini.Tables[0].Rows[i]["scaricatoFornitore"];

                    listaOrdini.Add(
                        new Ordine(idOrdine, dataCreazione, dataConferma, codice, descrizione, scaricatoFornitore));
                }
            }

            return listaOrdini;
        }

        public bool InsertVoceOrdineTemporaneo(int idUtente, int idFabbisognoConfermato)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineTemporaneoInsert");
            DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoConfermato", DbType.Int32, idFabbisognoConfermato);
            DatabaseCemi.AddOutParameter(comando, "@risultato", DbType.Int32, 32);

            DatabaseCemi.ExecuteNonQuery(comando);

            int risultato = (int) DatabaseCemi.GetParameterValue(comando, "@risultato");

            if (risultato == 0)
                return true;
            else
                return false;
        }

        public void DeleteVoceOrdineTemporaneo(int idUtente, int idFabbisognoConfermato)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineTemporaneoDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@idFabbisognoConfermato", DbType.Int32, idFabbisognoConfermato);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public FabbisognoList SelectOrdineTemporaneo(int idUtente)
        {
            DbCommand comando;
            FabbisognoList listaFabbisogni = new FabbisognoList();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineTemporaneoSelect");
            DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

            DataSet dsFabbisogni = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsFabbisogni != null) && (dsFabbisogni.Tables.Count == 1))
            {
                for (int i = 0; i < dsFabbisogni.Tables[0].Rows.Count; i++)
                {
                    int idFabbisogno = -1;
                    int id = -1;
                    string ragioneSociale = string.Empty;
                    DateTime dataConfermaImpresa = new DateTime();
                    string codiceFornitore = string.Empty;

                    if (dsFabbisogni.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                        id = (int) dsFabbisogni.Tables[0].Rows[i]["idImpresa"];
                    if (dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"] != DBNull.Value)
                        ragioneSociale = (string) dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"];
                    if (dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"] != DBNull.Value)
                        idFabbisogno = (int) dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"];
                    if (dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"] != DBNull.Value)
                        dataConfermaImpresa = (DateTime) dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"];
                    if (dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                        codiceFornitore = (string) dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"];

                    listaFabbisogni.Add(
                        new Fabbisogno(idFabbisogno, codiceFornitore, dataConfermaImpresa, id, ragioneSociale));
                }
            }

            return listaFabbisogni;
        }

        public void CreaOrdine(string descrizione, FabbisognoList listaFabbisogni, bool confermato)
        {
            using (DbConnection conn = DatabaseCemi.CreateConnection())
            {
                conn.Open();
                DbTransaction transaction = conn.BeginTransaction();

                try
                {
                    DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniInsert");
                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, descrizione);
                    DatabaseCemi.AddInParameter(comando, "@confermato", DbType.Boolean, confermato);
                    int idOrdine = int.Parse(DatabaseCemi.ExecuteScalar(comando).ToString());

                    if (idOrdine <= 0)
                    {
                        transaction.Rollback();
                        return;
                    }
                    else
                    {
                        foreach (Fabbisogno fabbisogno in listaFabbisogni)
                        {
                            DbCommand comando2 =
                                DatabaseCemi.GetStoredProcCommand(
                                    "dbo.USP_TuteScarpeFabbisogniConfermatiUpdateSetOrdine");
                            DatabaseCemi.AddInParameter(comando2, "@idFabbisognoConfermato", DbType.Int32,
                                                        fabbisogno.IdFabbisogno);
                            DatabaseCemi.AddInParameter(comando2, "@idOrdine", DbType.Int32, idOrdine);
                            DatabaseCemi.ExecuteNonQuery(comando2);
                        }

                        transaction.Commit();
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }

        public FornitoreList GetFornitori()
        {
            DbCommand comando;
            FornitoreList listaFornitori = new FornitoreList();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniSelectFornitori");

            DataSet dsFornitori = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsFornitori != null) && (dsFornitori.Tables.Count == 1))
            {
                for (int i = 0; i < dsFornitori.Tables[0].Rows.Count; i++)
                {
                    string codiceFornitore = string.Empty;

                    if (dsFornitori.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                        codiceFornitore = (string) dsFornitori.Tables[0].Rows[i]["codiceFornitore"];

                    if (codiceFornitore != string.Empty)
                        listaFornitori.Add(new Fornitore(codiceFornitore));
                }
            }

            return listaFornitori;
        }

        public int ConfermaOrdine(int idOrdine, string codiceFornitore)
        {
            int risultato;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();

                try
                {
                    risultato = UpdateOrdine(idOrdine, codiceFornitore, transaction);
                    if (risultato == 0)
                    {
                        // Salvataggio XML
                        CreaXML(idOrdine, transaction);
                        transaction.Commit();
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return risultato;
        }

        private int UpdateOrdine(int idOrdine, string codiceFornitore, DbTransaction transaction)
        {
            int risultato;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConfermaOrdine"))
            {
                DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
                DatabaseCemi.AddInParameter(comando, "@codiceFornitore", DbType.String, codiceFornitore);
                DatabaseCemi.AddOutParameter(comando, "@risultato", DbType.String, 32);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);

                risultato = int.Parse(DatabaseCemi.GetParameterValue(comando, "@risultato").ToString());
            }

            return risultato;
        }

        private void CreaXML(int idOrdine, DbTransaction transaction)
        {
            using (DbCommand comando1 = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeImpreseNellOrdine"))
            {
                DatabaseCemi.AddInParameter(comando1, "@idOrdine", DbType.Int32, idOrdine);
                using (DataSet ds1 = DatabaseCemi.ExecuteDataSet(comando1))
                {
                    if ((ds1 != null) && (ds1.Tables.Count == 1))
                    {
                        for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                        {
                            int idImpresa = -1;
                            int idFabbisognoComplessivo = -1;
                            int? idConsulente = null;
                            int? idFornitore = null;

                            if (ds1.Tables[0].Rows[i]["idFabbisognoComplessivo"] != DBNull.Value)
                                idFabbisognoComplessivo = (int) ds1.Tables[0].Rows[i]["idFabbisognoComplessivo"];
                            if (ds1.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                                idImpresa = (int) ds1.Tables[0].Rows[i]["idImpresa"];
                            if (ds1.Tables[0].Rows[i]["idConsulente"] != DBNull.Value)
                                idConsulente = (int) ds1.Tables[0].Rows[i]["idConsulente"];
                            if (ds1.Tables[0].Rows[i]["idFornitore"] != DBNull.Value)
                                idFornitore = (int) ds1.Tables[0].Rows[i]["idFornitore"];

                            if ((idFabbisognoComplessivo != -1) && (idImpresa != -1))
                            {
                                DbCommand comando =
                                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiSelectXML");
                                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                                DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32,
                                                            idFabbisognoComplessivo);
                                DatabaseCemi.AddInParameter(comando, "@idTuteScarpeOrdine", DbType.Int32, idOrdine);
                                DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

                                if ((ds != null) && (ds.Tables.Count == 1))
                                {
                                    //ds.Tables[0].TableName = i.ToString();
                                    //dtRes.Tables.Add(ds.Tables[0].Copy());
                                    string xml = CreaXmlOrdine(ds.Tables[0]);

                                    InsertStorico(idFabbisognoComplessivo, idImpresa, idOrdine, xml, idConsulente,
                                                  idFornitore,
                                                  transaction);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return;
        }

        private void InsertStorico(int idFabbisognoComplessivo, int idImpresa, int idOrdine, string xml,
                                   int? idConsulente, int? idFornitore, DbTransaction transaction)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeStoricoInsert");
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.String, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@idTuteScarpeOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.AddInParameter(comando, "@stato", DbType.Boolean, true);
            DatabaseCemi.AddInParameter(comando, "@ordine", DbType.Xml, xml);

            if (idConsulente.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
            if (idFornitore.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);

            DatabaseCemi.ExecuteNonQuery(comando, transaction);
        }

        private static string CreaXmlOrdine(DataTable tabella)
        {
            string xmlOrdine;
            StringBuilder stringBuilder = new StringBuilder();

            using (XmlWriter writer = XmlWriter.Create(stringBuilder))
            {
                tabella.WriteXml(writer);

                xmlOrdine = stringBuilder.ToString();
            }

            return xmlOrdine;
        }

        public int DeleteOrdine(int idOrdine)
        {
            int risultato;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
                DatabaseCemi.AddOutParameter(comando, "@risultato", DbType.String, 32);
                DatabaseCemi.ExecuteNonQuery(comando);

                risultato = int.Parse(DatabaseCemi.GetParameterValue(comando, "@risultato").ToString());
            }

            return risultato;
        }

        public FabbisognoList GetDettagliOrdine(Ordine ordine)
        {
            return GetDettagliOrdine(ordine.IdOrdine);
        }

        public FabbisognoList GetDettagliOrdine(int idOrdine)
        {
            DbCommand comando;
            FabbisognoList listaFabbisogni = new FabbisognoList();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniGetDettagli");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);

            DataSet dsFabbisogni = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsFabbisogni != null) && (dsFabbisogni.Tables.Count == 1))
            {
                for (int i = 0; i < dsFabbisogni.Tables[0].Rows.Count; i++)
                {
                    int idFabbisogno = -1;
                    int id = -1;
                    string ragioneSociale = string.Empty;
                    DateTime dataConfermaImpresa = new DateTime();
                    string codiceFornitore = string.Empty;
                    int progressivoFornitura = 0;

                    if (dsFabbisogni.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                        id = (int) dsFabbisogni.Tables[0].Rows[i]["idImpresa"];
                    if (dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"] != DBNull.Value)
                        ragioneSociale = (string) dsFabbisogni.Tables[0].Rows[i]["ragioneSociale"];
                    if (dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"] != DBNull.Value)
                        idFabbisogno = (int) dsFabbisogni.Tables[0].Rows[i]["idFabbisognoConfermato"];
                    if (dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"] != DBNull.Value)
                        dataConfermaImpresa = (DateTime) dsFabbisogni.Tables[0].Rows[i]["dataConfermaImpresa"];
                    if (dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                        codiceFornitore = (string) dsFabbisogni.Tables[0].Rows[i]["codiceFornitore"];
                    if (dsFabbisogni.Tables[0].Rows[i]["progressivoFornitura"] != DBNull.Value)
                        progressivoFornitura = (int) dsFabbisogni.Tables[0].Rows[i]["progressivoFornitura"];

                    listaFabbisogni.Add(new Fabbisogno(idFabbisogno, codiceFornitore, dataConfermaImpresa, id,
                                                       ragioneSociale, progressivoFornitura));
                }
            }

            return listaFabbisogni;
        }

        public void OrdineScaricatoDaFornitore(int idOrdine)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniUpdateScaricatoFornitore");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.ExecuteNonQuery(comando);
        }

        public void RimuoviFabbisognoDaOrdine(int idFabbisogno)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiRimuoviOrdine");
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoConfermato", DbType.Int32, idFabbisogno);
            DatabaseCemi.ExecuteNonQuery(comando);
        }

        public void AggiungiFabbisognoAOrdine(int idFabbisogno, int idOrdine)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiUpdateSetOrdine");
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoConfermato", DbType.Int32, idFabbisogno);
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.ExecuteNonQuery(comando);
        }

        public int GetFabbisognoComplessivoOrdine(int idOrdine)
        {
            DbCommand comando;
            int idFabbisognoComplessivo;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniSelectFabbisognoComplessivo");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            idFabbisognoComplessivo = (int) DatabaseCemi.ExecuteScalar(comando);

            return idFabbisognoComplessivo;
        }

        public bool InserisciFattura(Fattura fattura)
        {
            bool res = true;

            // Fare insert di tutta la struttura della fattura
            DbConnection connessione = DatabaseCemi.CreateConnection();

            try
            {
                connessione.Open();
                DbTransaction transazione = connessione.BeginTransaction(IsolationLevel.ReadUncommitted);

                try
                {
                    // Inserimento in tabella TuteScarpeFatture
                    DbCommand comando1;
                    comando1 = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureInsert");
                    DatabaseCemi.AddInParameter(comando1, "@fileFattura", DbType.Binary, fattura.FileFattura);
                    DatabaseCemi.AddInParameter(comando1, "@descrizione", DbType.String, fattura.Descrizione);
                    DatabaseCemi.AddInParameter(comando1, "@codiceFornitore", DbType.String, fattura.CodiceFornitore);
                    DatabaseCemi.AddInParameter(comando1, "@codiceFattura", DbType.Int32, fattura.CodiceFattura);

                    bool stato = false;
                    if (fattura.Stato())
                        stato = true;
                    DatabaseCemi.AddInParameter(comando1, "@stato", DbType.Boolean, stato);

                    fattura.IdFattura = Int32.Parse(DatabaseCemi.ExecuteScalar(comando1, transazione).ToString());

                    if (fattura.Bolle != null)
                    {
                        foreach (Bolla bolla in fattura.Bolle)
                        {
                            // Inserimento in tabella TuteScarpeBolle
                            DbCommand comando2;
                            comando2 = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeBolleInsert");
                            DatabaseCemi.AddInParameter(comando2, "@idTuteScarpeFattura", DbType.Int32,
                                                        fattura.IdFattura);
                            DatabaseCemi.AddInParameter(comando2, "@idImpresa", DbType.Int32, bolla.IdImpresa);
                            DatabaseCemi.AddInParameter(comando2, "@idTuteScarpeOrdine", DbType.Int32, bolla.IdOrdine);
                            DatabaseCemi.AddInParameter(comando2, "@data", DbType.DateTime, DateTime.Now);
                            bolla.IdBolla = Int32.Parse(DatabaseCemi.ExecuteScalar(comando2, transazione).ToString());

                            if (bolla.VociBolla != null)
                            {
                                foreach (VoceBolla voce in bolla.VociBolla)
                                {
                                    // Inserimento in tabella TuteScarpeBollaVoci
                                    DbCommand comando3;
                                    comando3 = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeBollaVociInsert");
                                    DatabaseCemi.AddInParameter(comando3, "@idTuteScarpeBolla", DbType.Int32,
                                                                bolla.IdBolla);
                                    DatabaseCemi.AddInParameter(comando3, "@tipoVestiario", DbType.String,
                                                                voce.TipoVestiario);
                                    DatabaseCemi.AddInParameter(comando3, "@quantita", DbType.String, voce.Quantita);

                                    voce.IdVoceBolla =
                                        Int32.Parse(DatabaseCemi.ExecuteScalar(comando3, transazione).ToString());
                                }
                            }
                            else
                                throw new Exception();
                        }
                    }
                    else
                        throw new Exception();

                    transazione.Commit();
                }
                catch (Exception)
                {
                    transazione.Rollback();
                    throw;
                }
            }
            catch (Exception)
            {
                res = false;
            }
            finally
            {
                connessione.Close();
            }

            return res;
        }

        public void DeleteFattura(int idFattura)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureDelete");
            DatabaseCemi.AddInParameter(comando, "@idFattura", DbType.Int32, idFattura);
            DatabaseCemi.ExecuteNonQuery(comando);
        }


        public void ForzaFattura(int idFattura)
        {
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureUpdateForza");
            DatabaseCemi.AddInParameter(comando, "@idFattura", DbType.Int32, idFattura);
            DatabaseCemi.ExecuteNonQuery(comando);
        }

        public FatturaList GetFatture(DateTime dal, DateTime al, bool? corrette, bool? forzate)
        {
            FatturaList listaFatture = new FatturaList();
            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureSelect");
            DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, dal);
            DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, al);
            if (corrette.HasValue)
                DatabaseCemi.AddInParameter(comando, "@corrette", DbType.Boolean, corrette);
            if (forzate.HasValue)
                DatabaseCemi.AddInParameter(comando, "@forzate", DbType.Boolean, forzate);

            DataSet dsFattura = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsFattura != null) && (dsFattura.Tables.Count == 1))
            {
                int idFatturaDb = -1;
                int codiceFattura = -1;
                string descrizione = string.Empty;
                DateTime dataUpload = new DateTime();
                string codiceFornitore = string.Empty;
                bool statoInserimento = false;
                bool forzataCorrettezza = false;

                for (int i = 0; i < dsFattura.Tables[0].Rows.Count; i++)
                {
                    if (dsFattura.Tables[0].Rows[i]["idTuteScarpeFattura"] != DBNull.Value)
                        idFatturaDb = (int) dsFattura.Tables[0].Rows[i]["idTuteScarpeFattura"];
                    if (dsFattura.Tables[0].Rows[i]["codiceFattura"] != DBNull.Value)
                        codiceFattura = (int) dsFattura.Tables[0].Rows[i]["codiceFattura"];
                    if (dsFattura.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsFattura.Tables[0].Rows[i]["descrizione"];
                    if (dsFattura.Tables[0].Rows[i]["dataUploadFile"] != DBNull.Value)
                        dataUpload = (DateTime) dsFattura.Tables[0].Rows[i]["dataUploadFile"];
                    if (dsFattura.Tables[0].Rows[i]["codiceFornitore"] != DBNull.Value)
                        codiceFornitore = (string) dsFattura.Tables[0].Rows[i]["codiceFornitore"];
                    if (dsFattura.Tables[0].Rows[i]["statoInserimento"] != DBNull.Value)
                        statoInserimento = (bool) dsFattura.Tables[0].Rows[i]["statoInserimento"];
                    if (dsFattura.Tables[0].Rows[i]["forzataCorrettezza"] != DBNull.Value)
                        forzataCorrettezza = (bool) dsFattura.Tables[0].Rows[i]["forzataCorrettezza"];

                    listaFatture.Add(
                        new Fattura(idFatturaDb, codiceFattura, descrizione, dataUpload, codiceFornitore,
                                    statoInserimento, forzataCorrettezza));
                }
            }

            return listaFatture;
        }

        public Fattura GetFattura(int idFattura)
        {
            Fattura fattura = null;
            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureSelectById");
            DatabaseCemi.AddInParameter(comando, "@idFattura", DbType.Int32, idFattura);

            DataSet dsFattura = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsFattura != null) && (dsFattura.Tables.Count == 1) && ((dsFattura.Tables[0].Rows.Count == 1)))
            {
                int idFatturaDb = -1;
                int codiceFattura = -1;
                string descrizione = string.Empty;
                DateTime dataUpload = new DateTime();
                string codiceFornitore = string.Empty;
                bool statoInserimento = false;
                bool forzataCorrettezza = false;

                if (dsFattura.Tables[0].Rows[0]["idTuteScarpeFattura"] != DBNull.Value)
                    idFatturaDb = (int) dsFattura.Tables[0].Rows[0]["idTuteScarpeFattura"];
                if (dsFattura.Tables[0].Rows[0]["codiceFattura"] != DBNull.Value)
                    codiceFattura = (int) dsFattura.Tables[0].Rows[0]["codiceFattura"];
                if (dsFattura.Tables[0].Rows[0]["descrizione"] != DBNull.Value)
                    descrizione = (string) dsFattura.Tables[0].Rows[0]["descrizione"];
                if (dsFattura.Tables[0].Rows[0]["dataUploadFile"] != DBNull.Value)
                    dataUpload = (DateTime) dsFattura.Tables[0].Rows[0]["dataUploadFile"];
                if (dsFattura.Tables[0].Rows[0]["codiceFornitore"] != DBNull.Value)
                    codiceFornitore = (string) dsFattura.Tables[0].Rows[0]["codiceFornitore"];
                if (dsFattura.Tables[0].Rows[0]["statoInserimento"] != DBNull.Value)
                    statoInserimento = (bool) dsFattura.Tables[0].Rows[0]["statoInserimento"];
                if (dsFattura.Tables[0].Rows[0]["forzataCorrettezza"] != DBNull.Value)
                    forzataCorrettezza = (bool) dsFattura.Tables[0].Rows[0]["forzataCorrettezza"];

                fattura =
                    new Fattura(idFatturaDb, codiceFattura, descrizione, dataUpload, codiceFornitore, statoInserimento,
                                forzataCorrettezza);
            }

            return fattura;
        }

        public byte[] GetFileDellaFattura(int idFattura)
        {
            byte[] fileFattura = null;
            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureSelectFile");
            DatabaseCemi.AddInParameter(comando, "@idFattura", DbType.Int32, idFattura);

            DataSet dsFattura = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsFattura != null) && (dsFattura.Tables.Count == 1) && (dsFattura.Tables[0].Rows.Count == 1))
            {
                if (dsFattura.Tables[0].Rows[0]["fileFattura"] != DBNull.Value)
                    fileFattura = (byte[]) dsFattura.Tables[0].Rows[0]["fileFattura"];
            }

            return fileFattura;
        }

        public BollaList GetFatturaBolle(int idFattura)
        {
            BollaList listaBolle = new BollaList();
            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeBolleSelectByFattura");
            DatabaseCemi.AddInParameter(comando, "@idFattura", DbType.Int32, idFattura);

            DataSet dsBolle = DatabaseCemi.ExecuteDataSet(comando);
            if ((dsBolle != null) && (dsBolle.Tables.Count == 1))
            {
                int idBolla = -1;
                int idImpresa = -1;
                int idOrdine = -1;
                DateTime data = new DateTime();

                for (int i = 0; i < dsBolle.Tables[0].Rows.Count; i++)
                {
                    if (dsBolle.Tables[0].Rows[i]["idTuteScarpeBolla"] != DBNull.Value)
                        idBolla = (int) dsBolle.Tables[0].Rows[i]["idTuteScarpeBolla"];
                    if (dsBolle.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                        idImpresa = (int) dsBolle.Tables[0].Rows[i]["idImpresa"];
                    if (dsBolle.Tables[0].Rows[i]["idTuteScarpeOrdine"] != DBNull.Value)
                        idOrdine = (int) dsBolle.Tables[0].Rows[i]["idTuteScarpeOrdine"];
                    if (dsBolle.Tables[0].Rows[i]["data"] != DBNull.Value)
                        data = (DateTime) dsBolle.Tables[0].Rows[i]["data"];

                    Bolla bolla = new Bolla(idImpresa, idOrdine, data);
                    bolla.IdBolla = idBolla;
                    listaBolle.Add(bolla);
                }
            }

            return listaBolle;
        }

        public VoceBollaList GetVociBolla(int idBolla, int idOrdine, int idImpresa)
        {
            VoceBollaList listaVoci = new VoceBollaList();
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeBollaVociSelectByBolla"))
            {
                DatabaseCemi.AddInParameter(comando, "@idBolla", DbType.Int32, idBolla);

                using (DataSet dsVoci = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if ((dsVoci != null) && (dsVoci.Tables.Count == 1))
                    {
                        int idVoce = -1;
                        string tipoVestiario = string.Empty;
                        int quantita = -1;

                        for (int i = 0; i < dsVoci.Tables[0].Rows.Count; i++)
                        {
                            if (dsVoci.Tables[0].Rows[i]["idTuteScarpeBollaVoci"] != DBNull.Value)
                                idVoce = (int) dsVoci.Tables[0].Rows[i]["idTuteScarpeBollaVoci"];
                            if (dsVoci.Tables[0].Rows[i]["tipoVestiario"] != DBNull.Value)
                                tipoVestiario = (string) dsVoci.Tables[0].Rows[i]["tipoVestiario"];
                            if (dsVoci.Tables[0].Rows[i]["quantita"] != DBNull.Value)
                                quantita = (int) dsVoci.Tables[0].Rows[i]["quantita"];

                            int richiesta = GetRichiestaFabbisogno(idOrdine, idImpresa, tipoVestiario);

                            VoceBolla voce = new VoceBolla(tipoVestiario, quantita, richiesta);
                            voce.IdVoceBolla = idVoce;
                            listaVoci.Add(voce);
                        }
                    }
                }
            }

            return listaVoci;
        }


        public int GetRichiestaFabbisogno(int idOrdine, int idImpresa, string tipoVestiario)
        {
            DbCommand comando;

            comando =
                DatabaseCemi.GetStoredProcCommand(
                    "dbo.USP_TuteScarpeFabbisogniConfermatiDettagliSelectPerConfrontoFattura");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@tipoVestiario", DbType.String, tipoVestiario);
            int res = (int) DatabaseCemi.ExecuteScalar(comando);

            return res;
        }

        public TipoVestiarioList GetTipiVestiarioInFabbisogno(int idOrdine, int idImpresa)
        {
            DbCommand comando;
            TipoVestiarioList listaVestiario = new TipoVestiarioList();

            comando =
                DatabaseCemi.GetStoredProcCommand(
                    "dbo.USP_TuteScarpeFabbisogniConfermatiDettagliSelectTipiVestiarioInFabbisogno");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

            DataSet dsTipi = DatabaseCemi.ExecuteDataSet(comando);

            if ((dsTipi != null) && (dsTipi.Tables.Count == 1))
            {
                for (int i = 0; i < dsTipi.Tables[0].Rows.Count; i++)
                {
                    string tipoVestiario = null;
                    int numero = -1;

                    if (dsTipi.Tables[0].Rows[i]["tipoTaglia"] != DBNull.Value)
                        tipoVestiario = (string) dsTipi.Tables[0].Rows[i]["tipoTaglia"];
                    if (dsTipi.Tables[0].Rows[i]["numero"] != DBNull.Value)
                        numero = (int) dsTipi.Tables[0].Rows[i]["numero"];

                    if ((tipoVestiario != null) && (numero != -1))
                        listaVestiario.Add(new TipoVestiario(tipoVestiario, numero));
                }
            }

            return listaVestiario;
        }

        public string GetImpresaRagioneSociale(int idImpresa)
        {
            string ragioneSociale = string.Empty;
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectById");
            DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);

            DataSet drRes = DatabaseCemi.ExecuteDataSet(comando);

            if ((drRes != null) && (drRes.Tables.Count == 1) && (drRes.Tables[0].Rows.Count == 1))
                ragioneSociale = (string) drRes.Tables[0].Rows[0]["ragioneSociale"];

            return ragioneSociale;
        }

        public BitArray EsisteOrdine(int idOrdine, string codiceFornitore)
        {
            DbCommand comando;
            BitArray bit = new BitArray(2);

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniSelectEsiste");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            if (codiceFornitore != null)
                DatabaseCemi.AddInParameter(comando, "@codiceFornitore", DbType.String, codiceFornitore);
            DataSet dsRes = DatabaseCemi.ExecuteDataSet(comando);

            if ((dsRes != null) && (dsRes.Tables.Count == 2) && (dsRes.Tables[0].Rows.Count == 1) &&
                (dsRes.Tables[1].Rows.Count == 1))
            {
                if ((int) dsRes.Tables[0].Rows[0]["contatore"] == 0)
                    bit[0] = false;
                else
                    bit[0] = true;
                if ((int) dsRes.Tables[1].Rows[0]["contatore"] == 0)
                    bit[1] = false;
                else
                    bit[1] = true;
            }

            return bit;
        }

        public bool BollaGiaGestita(int idOrdine, int idImpresa, string codiceFornitore, DateTime dataUpload)
        {
            DbCommand comando;
            bool res = false;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeBolleSelectGiaGestita");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@codiceFornitore", DbType.String, codiceFornitore);
            DatabaseCemi.AddInParameter(comando, "@dataUpload", DbType.DateTime, dataUpload);
            int iRes = (int) DatabaseCemi.ExecuteScalar(comando);

            if (iRes != 0)
                res = true;

            return res;
        }

        public bool FatturaGiaPresente(int codiceFattura, string codiceFornitore)
        {
            DbCommand comando;
            bool res = false;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureSelectEsiste");
            DatabaseCemi.AddInParameter(comando, "@codiceFattura", DbType.Int32, codiceFattura);
            DatabaseCemi.AddInParameter(comando, "@codiceFornitore", DbType.String, codiceFornitore);
            int iRes = (int) DatabaseCemi.ExecuteScalar(comando);

            if (iRes != 0)
                res = true;

            return res;
        }

        public FatturaRiassunto GetRiassuntoFattura(int idFattura)
        {
            DbCommand comando;
            FatturaRiassunto riassunto = new FatturaRiassunto();

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFattureSelectRiassunto");
            DatabaseCemi.AddInParameter(comando, "@idFattura", DbType.Int32, idFattura);

            DataSet dsRiassunto = DatabaseCemi.ExecuteDataSet(comando);

            if ((dsRiassunto != null) && (dsRiassunto.Tables.Count == 1))
            {
                for (int i = 0; i < dsRiassunto.Tables[0].Rows.Count; i++)
                {
                    string tipoVestiario = null;
                    int quantita = -1;

                    if (dsRiassunto.Tables[0].Rows[i]["tipoVestiario"] != DBNull.Value)
                        tipoVestiario = (string) dsRiassunto.Tables[0].Rows[i]["tipoVestiario"];
                    if (dsRiassunto.Tables[0].Rows[i]["quantita"] != DBNull.Value)
                        quantita = (int) dsRiassunto.Tables[0].Rows[i]["quantita"];

                    if ((tipoVestiario != null) && (quantita != -1))
                        riassunto.Add(new FatturaRiassuntoVoce(tipoVestiario, quantita));
                }
            }

            return riassunto;
        }

        public Indirizzo GetIndirizzoSpedizione(int idImpresa, int idFabbisognoComplessivo)
        {
            DbCommand comando;
            Indirizzo indirizzo = null;

            comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiSelectIndirizzoSpedizione");
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);

            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1)
            {
                string indirizzoS = ds.Tables[0].Rows[0]["indirizzo"] as string;
                string provincia = ds.Tables[0].Rows[0]["provincia"] as string;
                string comune = ds.Tables[0].Rows[0]["comune"] as string;
                string cap = ds.Tables[0].Rows[0]["cap"] as string;
                string presso = ds.Tables[0].Rows[0]["presso"] as string;

                indirizzo = new Indirizzo(indirizzoS, provincia, comune, cap, false, presso);
                indirizzo.Telefono = ds.Tables[0].Rows[0]["telefono"] as String;
                indirizzo.Cellulare = ds.Tables[0].Rows[0]["cellulare"] as String;
            }

            return indirizzo;
        }

        public bool AggiornaIndirizzoSpedizione(int idImpresa, int idFabbisognoComplessivo, Indirizzo indirizzoNuovo)
        {
            DbCommand comando;
            bool res;

            comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniConfermatiUpdateIndirizzoSpedizione");
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);
            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoNuovo.IndirizzoVia);
            DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, indirizzoNuovo.Provincia);
            DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, indirizzoNuovo.Comune);
            DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, indirizzoNuovo.Cap);
            if (!String.IsNullOrEmpty(indirizzoNuovo.Presso))
            {
                DatabaseCemi.AddInParameter(comando, "@presso", DbType.String, indirizzoNuovo.Presso);
            }
            if (!String.IsNullOrEmpty(indirizzoNuovo.Telefono))
            {
                DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, indirizzoNuovo.Telefono);
            }
            if (!String.IsNullOrEmpty(indirizzoNuovo.Cellulare))
            {
                DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String, indirizzoNuovo.Cellulare);
            }

            int iRes = DatabaseCemi.ExecuteNonQuery(comando);

            if (iRes > 0)
                res = true;
            else
                res = false;


            return res;
        }

        public FabbisognoComplessivoReale GetUltimoFabbisognoComplessivo()
        {
            DbCommand comando;
            FabbisognoComplessivoReale fabbisognoComplessivo = null;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniComplessiviSelectUltimo");
            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1)
            {
                int codice = (int) ds.Tables[0].Rows[0]["idFabbisognoComplessivo"];
                DateTime dataGenerazione = (DateTime) ds.Tables[0].Rows[0]["dataGenerazione"];
                DateTime? dataScadenza = null;
                if (ds.Tables[0].Rows[0]["dataScadenza"] != DBNull.Value)
                    dataScadenza = (DateTime) ds.Tables[0].Rows[0]["dataScadenza"];
                fabbisognoComplessivo = new FabbisognoComplessivoReale(codice, dataGenerazione, dataScadenza);

                if (ds.Tables[0].Rows[0]["dataConfermaAutomatica"] != DBNull.Value)
                    fabbisognoComplessivo.DataConfermaAutomatica =
                        (DateTime) ds.Tables[0].Rows[0]["dataConfermaAutomatica"];
                if (ds.Tables[0].Rows[0]["dataConfermaAutomaticaEffettuata"] != DBNull.Value)
                    fabbisognoComplessivo.DataConfermaAutomaticaEffettuata =
                        (DateTime) ds.Tables[0].Rows[0]["dataConfermaAutomaticaEffettuata"];
                if (ds.Tables[0].Rows[0]["dataInvioMailEffettuato"] != DBNull.Value)
                    fabbisognoComplessivo.DataInvioMailEffettuato =
                        (DateTime) ds.Tables[0].Rows[0]["dataInvioMailEffettuato"];
                if (ds.Tables[0].Rows[0]["dataInvioSmsEffettuato"] != DBNull.Value)
                    fabbisognoComplessivo.DataInvioSmsEffettuato =
                        (DateTime) ds.Tables[0].Rows[0]["dataInvioSmsEffettuato"];
            }

            return fabbisognoComplessivo;
        }

        public bool AggiornaScadenzaFabbisogni(int idFabbisognoComplessivo, DateTime? dataScadenza,
                                               DateTime? dataConfermaAutomatica)
        {
            DbCommand comando;
            bool res;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniComplessiviUpdateScadenza");
            DatabaseCemi.AddInParameter(comando, "@idFabbisognoComplessivo", DbType.Int32, idFabbisognoComplessivo);
            if (dataScadenza.HasValue)
            {
                DatabaseCemi.AddInParameter(comando, "@dataScadenza", DbType.DateTime, dataScadenza.Value);
            }
            if (dataConfermaAutomatica.HasValue)
            {
                DatabaseCemi.AddInParameter(comando, "@dataConfermaAutomatica", DbType.DateTime,
                                            dataConfermaAutomatica.Value);
            }

            int iRes = DatabaseCemi.ExecuteNonQuery(comando);

            if (iRes == 1)
                res = true;
            else
                res = false;

            return res;
        }

        public DateTime? UltimoFabbisognoValido()
        {
            DbCommand comando;
            DateTime? dataScadenza = null;
            Int32? idFab = null;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeUltimoFabbisognoAncoraValido");

            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);
            if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1)
            {
                if (ds.Tables[0].Rows[0]["dataScadenza"] != DBNull.Value)
                    dataScadenza = (DateTime) ds.Tables[0].Rows[0]["dataScadenza"];

                if (ds.Tables[0].Rows[0]["idFabbisognoComplessivo"] != DBNull.Value)
                    idFab = (Int32) ds.Tables[0].Rows[0]["idFabbisognoComplessivo"];
            }

            //Questo "trucco" permette di distinguere il non avere una scadenza valorizzata dal non avere un fabbisogno attivo
            if (!idFab.HasValue)
                dataScadenza = new DateTime(1900, 1, 1);
            //con questa data il sistema "vede" un fabbisogno complessivo scaduto

            return dataScadenza;
        }

        public ConsulentiCollection GetConsulenti(int? idConsulente, string ragioneSociale, string comune,
                                                  string indirizzo, string codiceFiscale)
        {
            ConsulentiCollection consulenti = new ConsulentiCollection();
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ConsulentiSelect");
            if (idConsulente.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
            if (!string.IsNullOrEmpty(ragioneSociale))
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSociale);
            if (!string.IsNullOrEmpty(comune))
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comune);
            if (!string.IsNullOrEmpty(indirizzo))
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzo);
            if (!string.IsNullOrEmpty(codiceFiscale))
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

            using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
            {
                while (reader.Read())
                {
                    Consulente consulente = new Consulente();

                    consulente.IdConsulente = reader.GetInt32(reader.GetOrdinal("idConsulente"));

                    int tempOrdinal = reader.GetOrdinal("ragioneSociale");
                    if (!reader.IsDBNull(tempOrdinal))
                        consulente.RagioneSociale = reader.GetString(tempOrdinal);

                    tempOrdinal = reader.GetOrdinal("indirizzo");
                    if (!reader.IsDBNull(tempOrdinal))
                        consulente.Indirizzo = reader.GetString(tempOrdinal);

                    tempOrdinal = reader.GetOrdinal("localita");
                    if (!reader.IsDBNull(tempOrdinal))
                        consulente.Comune = reader.GetString(tempOrdinal);

                    tempOrdinal = reader.GetOrdinal("provincia");
                    if (!reader.IsDBNull(tempOrdinal))
                        consulente.Provincia = reader.GetString(tempOrdinal);

                    tempOrdinal = reader.GetOrdinal("codiceFiscale");
                    if (!reader.IsDBNull(tempOrdinal))
                        consulente.CodiceFiscale = reader.GetString(tempOrdinal);

                    tempOrdinal = reader.GetOrdinal("nTelefono");
                    if (!reader.IsDBNull(tempOrdinal))
                        consulente.Telefono = reader.GetString(tempOrdinal);

                    if (!Convert.IsDBNull(reader["eMail"]))
                        consulente.EMail = (String) reader["eMail"];

                    consulenti.Add(consulente);
                }
            }

            return consulenti;
        }

        public RapportoConsulenteImpresaCollection GetConsulenteRapportiImpresa(int idConsulente, bool? attivi)
        {
            RapportoConsulenteImpresaCollection rapporti = new RapportoConsulenteImpresaCollection();
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeRapportiConsulenteImpreseSelect");
            DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
            if (attivi.HasValue)
                DatabaseCemi.AddInParameter(comando, "@attivi", DbType.Boolean, attivi);

            using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
            {
                while (reader.Read())
                {
                    RapportoConsulenteImpresa rapporto = new RapportoConsulenteImpresa();

                    rapporto.IdRapportoConsulenteImpresa =
                        reader.GetInt32(reader.GetOrdinal("idTuteScarpeRapportoConsulenteImpresa"));
                    rapporto.IdConsulente = reader.GetInt32(reader.GetOrdinal("idConsulente"));
                    rapporto.IdImpresa = reader.GetInt32(reader.GetOrdinal("idImpresa"));
                    rapporto.RagioneSocialeImpresa = reader.GetString(reader.GetOrdinal("ragioneSociale"));
                    rapporto.DataInizio = reader.GetDateTime(reader.GetOrdinal("dataInizio"));

                    int tempOrdinal = reader.GetOrdinal("dataFine");
                    if (!reader.IsDBNull(tempOrdinal))
                        rapporto.DataFine = reader.GetDateTime(tempOrdinal);

                    if (reader["nota"] != DBNull.Value)
                        rapporto.Nota = (string) reader["nota"];
                    if (reader["numeroLavoratori"] != DBNull.Value)
                        rapporto.NumeroLavoratori = (Int32) reader["numeroLavoratori"];

                    rapporti.Add(rapporto);
                }
            }

            return rapporti;
        }

        public bool InsertConsulenteRapportoImpresa(RapportoConsulenteImpresa rapporto)
        {
            bool res = false;

            if (rapporto != null)
            {
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeRapportiConsulenteImpreseInsert");
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, rapporto.IdConsulente);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, rapporto.IdImpresa);
                if (!string.IsNullOrEmpty(rapporto.Nota))
                    DatabaseCemi.AddInParameter(comando, "@nota", DbType.String, rapporto.Nota);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public RapportoConsulenteImpresaCollection GetConsulenteRapportiImpresaDenunce(int idConsulente)
        {
            RapportoConsulenteImpresaCollection rapporti = new RapportoConsulenteImpresaCollection();
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Imprese_ConsulentiSelectPerTuteScarpe");
            DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);

            using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
            {
                while (reader.Read())
                {
                    RapportoConsulenteImpresa rapporto = new RapportoConsulenteImpresa();

                    rapporto.IdConsulente = idConsulente;
                    rapporto.IdImpresa = reader.GetInt32(reader.GetOrdinal("idImpresa"));
                    rapporto.RagioneSocialeImpresa = reader.GetString(reader.GetOrdinal("ragioneSociale"));

                    rapporti.Add(rapporto);
                }
            }

            return rapporti;
        }

        public bool UpdateRapportoConsulenteImpresa(int idRapporto, DateTime? dataFine, string nota)
        {
            bool res = false;
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeRapportiConsulenteImpreseUpdate");
            DatabaseCemi.AddInParameter(comando, "@idRapporto", DbType.Int32, idRapporto);
            if (dataFine.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataFine);
            if (!string.IsNullOrEmpty(nota))
                DatabaseCemi.AddInParameter(comando, "@nota", DbType.String, nota);

            if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                res = true;

            return res;
        }

        public int GetIdUtenteByIdImpresa(int idImpresa)
        {
            int idUtente;
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectIdUtenteById");
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

            idUtente = (int) DatabaseCemi.ExecuteScalar(comando);

            return idUtente;
        }

        public RapportoFornitoreImpresaCollection GetFornitoreRapportiImpresa(int idFornitore, bool? attivi,
                                                                              int? idImpresa, string ragioneSociale)
        {
            RapportoFornitoreImpresaCollection rapporti = new RapportoFornitoreImpresaCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeRapportiFornitoreImpreseSelect")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);
                if (attivi.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@attivi", DbType.Boolean, attivi);
                if (idImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                if (!string.IsNullOrEmpty(ragioneSociale))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSociale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        RapportoFornitoreImpresa rapporto = new RapportoFornitoreImpresa();
                        rapporti.Add(rapporto);

                        rapporto.IdRapportoFornitoreImpresa = (int) reader["idTuteScarpeRapportoFornitoreImpresa"];
                        rapporto.IdFornitore = (int) reader["idFornitore"];
                        rapporto.IdImpresa = (int) reader["idImpresa"];
                        rapporto.RagioneSocialeImpresa = reader.GetString(reader.GetOrdinal("ragioneSociale"));
                        rapporto.DataInizio = reader.GetDateTime(reader.GetOrdinal("dataInizio"));

                        int tempOrdinal = reader.GetOrdinal("dataFine");
                        if (!reader.IsDBNull(tempOrdinal))
                            rapporto.DataFine = reader.GetDateTime(tempOrdinal);

                        rapporto.SedeLegale = new Indirizzo();
                        rapporto.SedeAmministrativa = new Indirizzo();

                        // ???
                        // dataInizioValiditaIndirizzoSedeLegale,
                        // dataFineValiditaIndirizzoSedeLegale,
                        // dataInizioValiditaIndirizzoSedeAmministrativa,
                        // dataFineValiditaIndirizzoSedeAmministrativa

                        // Sede legale
                        rapporto.SedeLegale.IndirizzoVia = reader["indirizzoSedeLegale"] as string;
                        rapporto.SedeLegale.Comune = reader["localitaSedeLegale"] as string;
                        rapporto.SedeLegale.Provincia = reader["provinciaSedeLegale"] as string;
                        rapporto.SedeLegale.Cap = reader["capSedeLegale"] as string;
                        rapporto.SedeLegale.Telefono = reader["telefonoSedeLegale"] as string;
                        rapporto.SedeLegale.Fax = reader["faxSedeLegale"] as string;
                        rapporto.SedeLegale.Email = reader["emailSedeLegale"] as string;

                        // Sede amministrativa
                        rapporto.SedeAmministrativa.IndirizzoVia = reader["indirizzoSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Comune = reader["localitaSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Provincia = reader["provinciaSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Cap = reader["capSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Telefono = reader["telefonoSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Fax = reader["faxSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Email = reader["emailSedeAmministrazione"] as string;
                        rapporto.SedeAmministrativa.Presso = reader["pressoSedeAmministrazione"] as string;

                        rapporto.NumeroLavoratori = (int) reader["numeroLavoratori"];
                    }
                }
            }

            return rapporti;
        }

        public List<SmsRichiestaTs> GetElencoSmsRichiesteTs()
        {
            List<SmsRichiestaTs> ret = new List<SmsRichiestaTs>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiRichiesteTsSelectElenco")
                )
            {
                using (IDataReader dataReader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (dataReader != null)
                    {
                        int idSmsRicevutoIndex = dataReader.GetOrdinal("idSmsRicevuto");
                        int dataRicezioneIndex = dataReader.GetOrdinal("dataRicezione");
                        int idUtenteIndex = dataReader.GetOrdinal("idUtente");
                        int idLavoratoreIndex = dataReader.GetOrdinal("idLavoratore");
                        int numeroTelefonoIndex = dataReader.GetOrdinal("numeroTelefono");
                        int testoTagliaIndex = dataReader.GetOrdinal("testoTaglia");
                        int idImpresaIndex = dataReader.GetOrdinal("idImpresa");

                        while (dataReader.Read())
                        {
                            SmsRichiestaTs smsRichiestaTs = new SmsRichiestaTs();

                            smsRichiestaTs.IdSmsRicevuto = dataReader.GetInt32(idSmsRicevutoIndex);
                            smsRichiestaTs.DataRicezione = dataReader.GetDateTime(dataRicezioneIndex);
                            smsRichiestaTs.IdLavoratore = dataReader.GetInt32(idLavoratoreIndex);
                            smsRichiestaTs.IdUtente = dataReader.GetInt32(idUtenteIndex);
                            smsRichiestaTs.NumeroTelefono = dataReader.GetString(numeroTelefonoIndex);
                            smsRichiestaTs.TestoTaglia = dataReader.GetString(testoTagliaIndex);
                            if (dataReader[idImpresaIndex] != DBNull.Value)
                                smsRichiestaTs.IdImpresa = dataReader.GetInt32(idImpresaIndex);

                            ret.Add(smsRichiestaTs);
                        }
                    }
                }
            }

            return ret;
        }

        public List<SmsRichiestaTs> GetElencoSmsFeedbackTs()
        {
            List<SmsRichiestaTs> ret = new List<SmsRichiestaTs>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SmsRicevutiFeedbackTsSelectElenco")
                )
            {
                using (IDataReader dataReader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (dataReader != null)
                    {
                        int idSmsRicevutoIndex = dataReader.GetOrdinal("idSmsRicevuto");
                        int dataRicezioneIndex = dataReader.GetOrdinal("dataRicezione");
                        int idUtenteIndex = dataReader.GetOrdinal("idUtente");
                        int idLavoratoreIndex = dataReader.GetOrdinal("idLavoratore");
                        int numeroTelefonoIndex = dataReader.GetOrdinal("numeroTelefono");
                        int testoTagliaIndex = dataReader.GetOrdinal("testoTaglia");
                        int idImpresaIndex = dataReader.GetOrdinal("idImpresa");

                        while (dataReader.Read())
                        {
                            SmsRichiestaTs smsRichiestaTs = new SmsRichiestaTs();

                            smsRichiestaTs.IdSmsRicevuto = dataReader.GetInt32(idSmsRicevutoIndex);
                            smsRichiestaTs.DataRicezione = dataReader.GetDateTime(dataRicezioneIndex);
                            smsRichiestaTs.IdLavoratore = dataReader.GetInt32(idLavoratoreIndex);
                            smsRichiestaTs.IdUtente = dataReader.GetInt32(idUtenteIndex);
                            smsRichiestaTs.NumeroTelefono = dataReader.GetString(numeroTelefonoIndex);
                            smsRichiestaTs.TestoTaglia = dataReader.GetString(testoTagliaIndex);
                            if (dataReader[idImpresaIndex] != DBNull.Value)
                                smsRichiestaTs.IdImpresa = dataReader.GetInt32(idImpresaIndex);

                            ret.Add(smsRichiestaTs);
                        }
                    }
                }
            }

            return ret;
        }

        public ImpresaCollection GetImpreseConOrdiniAutomatici()
        {
            ImpresaCollection imprese = new ImpresaCollection();
            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniAutoImpreseCompilabiliSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Index

                    int idImpresaIndex = reader.GetOrdinal("idImpresa");
                    int ragioneSocialeIndex = reader.GetOrdinal("ragioneSociale");
                    int indirizzoSedeAmministrazioneIndex = reader.GetOrdinal("indirizzoSedeAmministrazione");
                    int capSedeAmministrazioneIndex = reader.GetOrdinal("capSedeAmministrazione");
                    int localitaSedeAmministrazioneIndex = reader.GetOrdinal("localitaSedeAmministrazione");
                    int provinciaSedeAmministrazioneIndex = reader.GetOrdinal("provinciaSedeAmministrazione");
                    int eMailImpresaIndex = reader.GetOrdinal("eMailImpresa");
                    int civicoSedeAmministrativaIndex = reader.GetOrdinal("civicoSedeAmministrativa");
                    int eMailConsulenteIndex = reader.GetOrdinal("eMailConsulente");
                    int denominazioneIndirizzoSedeAmministrativa =
                        reader.GetOrdinal("denominazioneIndirizzoSedeAmministrativa");
                    int tipoVia = reader.GetOrdinal("tipoVia");
                    int telefono = reader.GetOrdinal("telefono");

                    #endregion

                    while (reader.Read())
                    {
                        Type.Entities.Impresa impresa =
                            new Type.Entities.Impresa();

                        //Ragione Sociale e Codice Impresa
                        if (!reader.IsDBNull(idImpresaIndex))
                            impresa.IdImpresa = reader.GetInt32(idImpresaIndex);
                        if (!reader.IsDBNull(ragioneSocialeIndex))
                            impresa.RagioneSociale = reader.GetString(ragioneSocialeIndex);

                        //Indirizzo
                        impresa.Recapito = new Cemi.Type.Entities.Indirizzo();
                        if (!reader.IsDBNull(localitaSedeAmministrazioneIndex))
                            impresa.Recapito.Comune = reader.GetString(localitaSedeAmministrazioneIndex);
                        if (!reader.IsDBNull(provinciaSedeAmministrazioneIndex))
                            impresa.Recapito.Provincia = reader.GetString(provinciaSedeAmministrazioneIndex);
                        if (!reader.IsDBNull(capSedeAmministrazioneIndex))
                            impresa.Recapito.Cap = reader.GetString(capSedeAmministrazioneIndex);
                        if (!reader.IsDBNull(civicoSedeAmministrativaIndex))
                            impresa.Recapito.Civico = reader.GetString(civicoSedeAmministrativaIndex);
                        if (reader.IsDBNull(tipoVia) || reader.IsDBNull(denominazioneIndirizzoSedeAmministrativa))
                        {
                            impresa.Recapito.NomeVia = reader.GetString(indirizzoSedeAmministrazioneIndex);
                        }
                        else
                        {
                            impresa.Recapito.Qualificatore = reader.GetString(tipoVia);
                            impresa.Recapito.NomeVia = reader.GetString(denominazioneIndirizzoSedeAmministrativa);
                        }
                        //Emails
                        if (!reader.IsDBNull(eMailImpresaIndex))
                            impresa.EmailImpresa = reader.GetString(eMailImpresaIndex);
                        if (!reader.IsDBNull(eMailConsulenteIndex))
                            impresa.EmailConsulente = reader.GetString(eMailConsulenteIndex);
                        if (!reader.IsDBNull(telefono))
                        {
                            impresa.Telefono = reader.GetString(telefono);
                        }


                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        public OrdineAutomaticoCollection GetOrdiniAutomatici(int? idImpresa, int? idLavoratore)
        {
            OrdineAutomaticoCollection ordiniAutomatici = new OrdineAutomaticoCollection();

            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdiniAutoConfermatiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader dataReader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (dataReader.Read())
                    {
                        OrdineAutomatico ordineAutomatico = new OrdineAutomatico();

                        ordineAutomatico.IdImpresa = (int) dataReader["idImpresa"];
                        ordineAutomatico.RagioneSociale = (string) dataReader["ragioneSociale"];
                        ordineAutomatico.Taglia = (string) dataReader["taglia"];
                        ordineAutomatico.Descrizione = (string) dataReader["descrizione"];
                        ordineAutomatico.IdIndumento = (string) dataReader["idIndumento"];
                        ordineAutomatico.Nome = (string) dataReader["nome"];
                        ordineAutomatico.Cognome = (string) dataReader["cognome"];
                        ordineAutomatico.IdLavoratore = (int) dataReader["idLavoratore"];
                        ordineAutomatico.TipologiaFornitura = (string) dataReader["tipologiaFornitura"];

                        ordiniAutomatici.Add(ordineAutomatico);
                    }
                }
            }
            return ordiniAutomatici;
        }

        public ConsegnaCollection GetConsegneOrdini(int idImpresa, int idOrdine)
        {
            ConsegnaCollection consegneOrdini = new ConsegnaCollection();

            using (
                DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConsegneOrdiniSelectWithFilter")
                )
            {
                DatabaseCemi.AddInParameter(command, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(command, "@idTuteScarpeOrdine", DbType.Int32, idOrdine);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Consegna consegna = new Consegna();

                        consegna.CodiceImpresa = idImpresa;
                        consegna.NumeroOrdine = idOrdine;
                        consegna.DataEvento = (DateTime) reader["dataEvento"];
                        if (!Convert.IsDBNull(reader["idTuteScarpeConsegnaOrdineFile"]))
                        {
                            consegna.IdentificativoFile = (int) reader["idTuteScarpeConsegnaOrdineFile"];
                        }
                        consegna.NumeroSpedizione = (string) reader["numeroSpedizione"];
                        consegna.Stato = (string) reader["descrizioneEvento"];

                        consegneOrdini.Add(consegna);
                    }
                }
            }

            return consegneOrdini;
        }

        public bool InsertConsegneOrdini(Consegna ordine)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConsegneOrdiniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTuteScarpeOrdine", DbType.Int32, ordine.NumeroOrdine);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, ordine.CodiceImpresa);
                DatabaseCemi.AddInParameter(comando, "@numeroSpedizione", DbType.String, ordine.NumeroSpedizione);
                DatabaseCemi.AddInParameter(comando, "@dataEvento", DbType.Date, ordine.DataEvento);
                DatabaseCemi.AddInParameter(comando, "@descrizioneEvento", DbType.String, ordine.Stato);
                if (ordine.IdentificativoFile.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTuteScarpeConsegnaOrdineFile", DbType.Int32,
                                                ordine.IdentificativoFile);


                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool InsertFileConsegneOrdini(byte[] data, out Int32 idTuteScarpeConsegnaOrdineFile)
        {
            bool ret = false;

            idTuteScarpeConsegnaOrdineFile = 0;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConsegneOrdiniFileInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@fileConsegnaOrdine", DbType.Binary, data);
                DatabaseCemi.AddOutParameter(comando, "@idTuteScarpeConsegnaOrdineFile", DbType.Int32, 32);
                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    ret = true;
                    idTuteScarpeConsegnaOrdineFile =
                        (int) DatabaseCemi.GetParameterValue(comando, "@idTuteScarpeConsegnaOrdineFile");
                }
            }

            return ret;
        }

        public void MemorizzaDataInvioMailCompletamentoAutomatico()
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniComplessiviUpdateInvioMail"))
            {
                if (DatabaseCemi.ExecuteNonQuery(comando) < 1)
                {
                    throw new Exception(
                        "MemorizzaDataInvioMailCompletamentoAutomatico: Memorizzazione data invio mail non riuscita");
                }
            }
        }

        public void MemorizzaDataInvioSmsLavoratori()
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisogniComplessiviUpdateInvioSms"))
            {
                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("MemorizzaDataInvioSmsLavoratori: Memorizzazione data invio sms non riuscita");
                }
            }
        }

        public List<SmsDaInviare.Destinatario> GetRecapitiLavoratoriNonCompletabili()
        {
            List<SmsDaInviare.Destinatario> listaDestinatari = new List<SmsDaInviare.Destinatario>();

            using (
                DbCommand command =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeLavoratoriNonComfermabiliSelectRecapito"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        SmsDaInviare.Destinatario destinatario = new SmsDaInviare.Destinatario
                                                                     {
                                                                         Numero = (string) reader["numero"],
                                                                         IdUtenteSiceInfo = (int) reader["idUtente"]
                                                                     };

                        listaDestinatari.Add(destinatario);
                    }
                }
            }

            return listaDestinatari;
        }

        public void MemorizzaInvioMail(int idImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeImpreseConfermabiliUpdateInvioMail"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        #region Gestione impresa selezionata da consulente e fornitore

        public Impresa GetImpresaSelezionataConsulente(Int32 idConsulente)
        {
            Impresa impresa = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConsulentiImpresaSelezionataSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        if (!Convert.IsDBNull(reader["idImpresa"]))
                        {
                            impresa = new Impresa();
                            impresa.IdImpresa = (Int32) reader["idImpresa"];
                            impresa.IdUtente = (Int32) reader["idUtente"];
                            impresa.RagioneSociale = (String) reader["ragioneSociale"];
                            impresa.CodiceFiscale = reader["codiceFiscale"] as string;
                        }
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresaSelezionataFornitore(Int32 idFornitore)
        {
            Impresa impresa = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFornitoriImpresaSelezionataSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        if (!Convert.IsDBNull(reader["idImpresa"]))
                        {
                            impresa = new Impresa();
                            impresa.IdImpresa = (Int32) reader["idImpresa"];
                            impresa.IdUtente = (Int32) reader["idUtente"];
                            impresa.RagioneSociale = (String) reader["ragioneSociale"];
                            impresa.CodiceFiscale = reader["codiceFiscale"] as string;
                        }
                    }
                }
            }
            return impresa;
        }

        public void SetImpresaSelezionataConsulente(Int32 idConsulente, Int32? idImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConsulentiImpresaSelezionataInsertUpdate")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
                if (idImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void SetImpresaSelezionataFornitore(Int32 idConsulente, Int32? idImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFornitoriImpresaSelezionataInsertUpdate")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idConsulente);
                if (idImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        #endregion

        #region Creazione file ordini

        public FileOrdineImpresa GetOrdineFile(int idOrdine, int idimpresa)
        {
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineSelectForFile");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);
            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idimpresa);

            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            int codiceImpresa = 0;
            int protocolloOrdine = 0;
            string ragioneSociale1 = string.Empty;
            string ragioneSociale2 = string.Empty;
            string indirizzo = string.Empty;
            string cap = string.Empty;
            string citta = string.Empty;
            string provincia = string.Empty;
            string telefono = string.Empty;
            string cellulare = string.Empty;

            if (ds.Tables[0].Rows[0]["idimpresa"] != DBNull.Value)
                codiceImpresa = (int) ds.Tables[0].Rows[0]["idimpresa"];
            if (ds.Tables[0].Rows[0]["idTuteScarpeOrdine"] != DBNull.Value)
                protocolloOrdine = (int) ds.Tables[0].Rows[0]["idTuteScarpeOrdine"];
            if (ds.Tables[0].Rows[0]["ragioneSociale"] != DBNull.Value)
                ragioneSociale1 = (string) ds.Tables[0].Rows[0]["ragioneSociale"];
            if (ds.Tables[0].Rows[0]["presso"] != DBNull.Value)
                ragioneSociale2 = (string) ds.Tables[0].Rows[0]["presso"];
            if (ds.Tables[0].Rows[0]["indirizzo"] != DBNull.Value)
                indirizzo = (string) ds.Tables[0].Rows[0]["indirizzo"];
            if (ds.Tables[0].Rows[0]["cap"] != DBNull.Value)
                cap = (string) ds.Tables[0].Rows[0]["cap"];
            if (ds.Tables[0].Rows[0]["comune"] != DBNull.Value)
                citta = (string) ds.Tables[0].Rows[0]["comune"];
            if (ds.Tables[0].Rows[0]["provincia"] != DBNull.Value)
                provincia = (string) ds.Tables[0].Rows[0]["provincia"];
            if (ds.Tables[0].Rows[0]["telefono"] != DBNull.Value)
                telefono = (string) ds.Tables[0].Rows[0]["telefono"];
            if (ds.Tables[0].Rows[0]["cellulare"] != DBNull.Value)
                cellulare = (string) ds.Tables[0].Rows[0]["cellulare"];

            //
            FileOrdineImpresa ordineImpresa = new FileOrdineImpresa(codiceImpresa,
                                                                    protocolloOrdine, ragioneSociale1, ragioneSociale2,
                                                                    indirizzo, cap, citta, provincia, telefono,
                                                                    cellulare);

            DataTable dtDettaglio = ds.Tables[1];

            if (dtDettaglio != null)
            {
                string indumentoPrev = string.Empty;

                FileOrdineImpresaDettaglio dettaglio = null;
                for (int i = 0; i < dtDettaglio.Rows.Count; i++)
                {
                    DataRow row = dtDettaglio.Rows[i];
                    //
                    if (row["idindumento"].ToString() != indumentoPrev)
                    {
                        indumentoPrev = row["idindumento"].ToString();
                        dettaglio = new FileOrdineImpresaDettaglio();
                        ordineImpresa.AggiungiDettaglio(dettaglio);

                        dettaglio.TipoVestiario = row["tipoTaglia"].ToString();
                        dettaglio.CodiceRicevente = ordineImpresa.CodiceImpresa;
                        dettaglio.CodiceIndumento = row["idindumento"].ToString();
                        dettaglio.DescrizioneIndumento = row["descrizione"].ToString();
                    }

                    if (dettaglio != null)
                        dettaglio.AggiungiElemento(row["tipoTaglia"].ToString(), row["taglia"].ToString(),
                                                   (int) row["sommaTotale"]);
                }
            }

            //Carichiamo i dettagli del lavoratore di quell'impresa
            DataTable datatableDettaglioLavoratori = ds.Tables[3];

            FileOrdineLavoratoreCollection lavoratori = new FileOrdineLavoratoreCollection();
            if (datatableDettaglioLavoratori != null)
            {
                int idLavoratore = -1;
                string indumentoPrev = string.Empty;

                FileOrdineLavoratoreDettaglio dettaglio = null;
                FileOrdineLavoratore lavoratore = null;

                for (int i = 0; i < datatableDettaglioLavoratori.Rows.Count; i++)
                {
                    DataRow row = datatableDettaglioLavoratori.Rows[i];
                    if ((int) row["idLavoratore"] != idLavoratore)
                    {
                        lavoratore = new FileOrdineLavoratore();
                        lavoratore.IdLavoratore = (int) row["idLavoratore"];
                        lavoratore.IdImpresa = (int) row["idimpresa"];
                        lavoratore.Cognome = row["cognome"].ToString();
                        lavoratore.Nome = row["nome"].ToString();
                        lavoratore.DataNascita = (DateTime) row["dataNascita"];

                        lavoratori.Add(lavoratore);
                        idLavoratore = lavoratore.IdLavoratore;
                    }

                    //if (row["idindumento"].ToString() != indumentoPrev && (int)row["idLavoratore"] != idLavoratore)
                    {
                        indumentoPrev = row["idindumento"].ToString();
                        dettaglio = new FileOrdineLavoratoreDettaglio();
                        dettaglio.CodiceRicevente = (int) row["idLavoratore"];
                        dettaglio.TipoVestiario = row["tipoTaglia"].ToString();
                        dettaglio.IdLavoratore = idLavoratore;
                        dettaglio.CodiceIndumento = row["idindumento"].ToString();
                        dettaglio.DescrizioneIndumento = row["descrizione"].ToString();

                        lavoratore.AggiungiLavoratoreDettaglio(dettaglio);
                    }

                    dettaglio.AggiungiElemento(row["tipoTaglia"].ToString(), row["taglia"].ToString(),
                                               (int) row["sommaTotale"]);
                }
            }
            ordineImpresa.FileOrdineLavoratori = lavoratori;

            return ordineImpresa;
        }

        public FileOrdine GetOrdineFile(int idOrdine)
        {
            DataSet ds;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeImpreseNellOrdine");
            DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);

            ds = DatabaseCemi.ExecuteDataSet(comando);

            FileOrdine fileOrdine = new FileOrdine();

            if (ds != null && ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    FileOrdineImpresa ordineImpresa = GetOrdineFile(idOrdine, (int) row["idimpresa"]);
                    fileOrdine.AggiungiOrdineImpresa(ordineImpresa);
                }
            }

            return fileOrdine;
        }

        public OrdineRiassunto GetOrdineRiassunto(int idOrdine)
        {
            OrdineRiassunto ordineRiassunto;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineRiassunto"))
            {
                DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);

                using (DataSet ds = DatabaseCemi.ExecuteDataSet(comando))
                {
                    ordineRiassunto = new OrdineRiassunto();

                    try
                    {
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                ordineRiassunto.AggiungiIndumentoRiassunto((string) row["descrizione"],
                                                                           (int) row["sommaTotale"]);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }

            return ordineRiassunto;
        }

        public OrdineRiassuntoImpLav GetOrdineRiassuntoImpLav(int idOrdine)
        {
            OrdineRiassuntoImpLav ordineRiassuntoImpLav;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeOrdineRiassuntoImpLav"))
            {
                DatabaseCemi.AddInParameter(comando, "@idOrdine", DbType.Int32, idOrdine);

                using (DataSet ds = DatabaseCemi.ExecuteDataSet(comando))
                {
                    ordineRiassuntoImpLav = new OrdineRiassuntoImpLav();

                    try
                    {
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            ordineRiassuntoImpLav.AggiungiElemento("Totale lavoratori",
                                                                   (int) ds.Tables[0].Rows[0]["numeroLavoratori"]);
                            ordineRiassuntoImpLav.AggiungiElemento("Totale imprese",
                                                                   (int) ds.Tables[0].Rows[0]["numeroImprese"]);
                        }
                    }
                    catch
                    {
                    }
                }
            }

            return ordineRiassuntoImpLav;
        }

        #endregion

        #region Gestione vestiario e gruppi vestiario

        public DataTable GetSoluzioniVestiarioForPrint(int idImpresa)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_getAllGruppiForPrint"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                // Ricavo il risultato da un dataset
                using (DataSet dsRes = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsRes != null && dsRes.Tables.Count > 0)
                    {
                        return dsRes.Tables[0];
                    }
                }
            }
            return null;
        }

        public DataTable GetSoluzioniVestiarioSelezionato(int idImpresa)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_getAllGruppi2Sel"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                // Ricavo il risultato da un dataset
                using (DataSet dsRes = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsRes != null && dsRes.Tables.Count > 0)
                    {
                        return dsRes.Tables[0];
                    }
                }
            }
            return null;
        }

        #endregion

        #region Selezione taglie per il lavoratore

        public TagliePerTipoCollection GetTagliePerTipoTaglia()
        {
            TagliePerTipoCollection tagliePerTipo = new TagliePerTipoCollection();

            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeTagliePerTipoTagliaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per reader

                    Int32 indiceTipoTaglia = reader.GetOrdinal("tipoTaglia");
                    Int32 indiceIdIndumento = reader.GetOrdinal("idIndumento");
                    Int32 indiceIndumentoDescrizione = reader.GetOrdinal("indumentoDescrizione");
                    Int32 indiceTagliaDescrizione = reader.GetOrdinal("tagliaDescrizione");

                    #endregion

                    TagliePerTipo tagliaPerTipo = null;
                    String idIndumentoPrimoIndumento = null;
                    String idIndumentoCorrente = null;

                    while (reader.Read())
                    {
                        if (tagliaPerTipo == null
                            || tagliaPerTipo.TipoTaglia != reader.GetString(indiceTipoTaglia))
                        {
                            tagliaPerTipo = new TagliePerTipo();
                            tagliePerTipo.Add(tagliaPerTipo);

                            tagliaPerTipo.TipoTaglia = reader.GetString(indiceTipoTaglia);
                            idIndumentoPrimoIndumento = null;
                            if (String.IsNullOrEmpty(idIndumentoPrimoIndumento))
                            {
                                idIndumentoPrimoIndumento = reader.GetString(indiceIdIndumento);
                            }
                        }

                        if (String.IsNullOrEmpty(idIndumentoCorrente)
                            || idIndumentoCorrente != reader.GetString(indiceIdIndumento))
                        {
                            idIndumentoCorrente = reader.GetString(indiceIdIndumento);

                            Indumento indumento = new Indumento();
                            indumento.IdIndumento = reader.GetString(indiceIdIndumento);
                            indumento.Descrizione = reader.GetString(indiceIndumentoDescrizione);

                            if (!tagliaPerTipo.Indumento.PresenteIndumentoPerDescrizione(indumento.Descrizione))
                            {
                                tagliaPerTipo.Indumento.Add(indumento);
                            }
                        }

                        if (idIndumentoPrimoIndumento == reader.GetString(indiceIdIndumento))
                        {
                            tagliaPerTipo.Taglie.Add(reader.GetString(indiceTagliaDescrizione));
                        }
                    }
                }
            }

            return tagliePerTipo;
        }

        public TagliePerTipoCollection GetTaglieSelezionateLavoratore(Int32 idLavoratore)
        {
            TagliePerTipoCollection tagliePerTipo = new TagliePerTipoCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeTaglieLavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per reader

                    Int32 indiceTipoTaglia = reader.GetOrdinal("tipoTaglia");
                    Int32 indiceTagliaScelta = reader.GetOrdinal("tagliaScelta");

                    #endregion

                    while (reader.Read())
                    {
                        TagliePerTipo tagliaPerTipo = new TagliePerTipo();
                        tagliePerTipo.Add(tagliaPerTipo);

                        tagliaPerTipo.TipoTaglia = reader.GetString(indiceTipoTaglia);
                        tagliaPerTipo.TagliaScelta = reader.GetString(indiceTagliaScelta);
                    }
                }
            }

            return tagliePerTipo;
        }

        public void InserisciTaglieSelezionate(TagliePerTipoCollection taglieSelezionate)
        {
            if (taglieSelezionate != null && taglieSelezionate.Count > 0)
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();

                    using (DbTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            foreach (TagliePerTipo tpt in taglieSelezionate)
                            {
                                InserisciTagliaSelezionata(tpt, transaction);
                            }

                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }
        }

        private void InserisciTagliaSelezionata(TagliePerTipo tagliaSelezionata, DbTransaction transaction)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeTaglieLavoratoriInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@tipoTaglia", DbType.String, tagliaSelezionata.TipoTaglia);
                DatabaseCemi.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, tagliaSelezionata.IdLavoratore);
                DatabaseCemi.AddInParameter(dbCommand, "@tagliaScelta", DbType.String, tagliaSelezionata.TagliaScelta);

                if (DatabaseCemi.ExecuteNonQuery(dbCommand, transaction) != 1)
                {
                    throw new Exception(
                        "InserisciTagliaSelezionata: errore deurante l'inserimento/aggiornamento delle taglie.");
                }
            }
        }

        #endregion

        #region Nuovi metodi Tute & Scarpe

        public FabbisognoLavoratore GetFabbisognoLavoratore(Int32 idImpresa, Int32 idLavoratore)
        {
            FabbisognoLavoratore fabbisogno = null;

            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisognoLavoratoreSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        fabbisogno = new FabbisognoLavoratore();
                        fabbisogno.IdImpresa = idImpresa;
                        fabbisogno.IdLavoratore = idLavoratore;
                        fabbisogno.Cognome = reader["cognome"] as String;
                        fabbisogno.Nome = reader["nome"] as String;

                        reader.NextResult();

                        reader.Read();
                        fabbisogno.RagioneSociale = reader["ragioneSociale"] as String;
                        fabbisogno.ImpresaAsfaltista = (Boolean) reader["asfaltista"];

                        reader.NextResult();

                        #region Indici per reader

                        Int32 indiceIdGruppo = reader.GetOrdinal("idGruppoArticoli");
                        Int32 indiceDescrizioneGruppo = reader.GetOrdinal("descrizioneGruppo");
                        Int32 indiceCodiceAlternative = reader.GetOrdinal("codiceAlternative");
                        Int32 indiceGruppoAsfaltisti = reader.GetOrdinal("gruppoPerAsfaltisti");
                        Int32 indiceIdIndumento = reader.GetOrdinal("idIndumento");
                        Int32 indiceDescrizioneIndumento = reader.GetOrdinal("descrizioneIndumento");
                        Int32 indiceQuantita = reader.GetOrdinal("quantita");
                        Int32 indiceTaglia = reader.GetOrdinal("taglia");
                        Int32 indiceForzatoAsfaltista = reader.GetOrdinal("forzatoAsfaltista");
                        Int32 indiceTagliaArticolo = reader.GetOrdinal("tagliaArticolo");
                        Int32 indiceSelezionato = reader.GetOrdinal("selezionato");

                        #endregion

                        GruppoFabbisognoLavoratore gruppoCorrente = null;
                        DettaglioGruppoFabbisognoLavoratore dettaglioCorrente = null;
                        while (reader.Read())
                        {
                            if (gruppoCorrente == null
                                || gruppoCorrente.IdGruppo != reader.GetInt32(indiceIdGruppo))
                            {
                                gruppoCorrente = new GruppoFabbisognoLavoratore();
                                fabbisogno.Gruppi.Add(gruppoCorrente);
                                gruppoCorrente.IdGruppo = reader.GetInt32(indiceIdGruppo);
                                gruppoCorrente.Descrizione = reader.GetString(indiceDescrizioneGruppo);
                                gruppoCorrente.CodiceAlternative = reader.GetInt32(indiceCodiceAlternative);
                                gruppoCorrente.GruppoPerAsfaltisti = reader.GetBoolean(indiceGruppoAsfaltisti);
                                gruppoCorrente.Selezionato = gruppoCorrente.Selezionato || reader.GetBoolean(indiceSelezionato);

                                dettaglioCorrente = new DettaglioGruppoFabbisognoLavoratore();
                                gruppoCorrente.Dettagli.Add(dettaglioCorrente);
                                dettaglioCorrente.IdIndumento = reader.GetString(indiceIdIndumento);
                                dettaglioCorrente.Quantita = reader.GetInt32(indiceQuantita);
                                dettaglioCorrente.Descrizione = reader.GetString(indiceDescrizioneIndumento);
                                if (!reader.IsDBNull(indiceTaglia))
                                {
                                    dettaglioCorrente.TagliaSelezionata = reader.GetString(indiceTaglia);
                                }
                                dettaglioCorrente.ForzatoAsfaltista = reader.GetBoolean(indiceForzatoAsfaltista);
                                dettaglioCorrente.Selezionato = reader.GetBoolean(indiceSelezionato);
                            }
                            else
                            {
                                if (dettaglioCorrente.IdIndumento != reader.GetString(indiceIdIndumento))
                                {
                                    dettaglioCorrente = new DettaglioGruppoFabbisognoLavoratore();
                                    gruppoCorrente.Dettagli.Add(dettaglioCorrente);
                                    dettaglioCorrente.IdIndumento = reader.GetString(indiceIdIndumento);
                                    dettaglioCorrente.Quantita = reader.GetInt32(indiceQuantita);
                                    dettaglioCorrente.Descrizione = reader.GetString(indiceDescrizioneIndumento);
                                    if (!reader.IsDBNull(indiceTaglia))
                                    {
                                        dettaglioCorrente.TagliaSelezionata = reader.GetString(indiceTaglia);
                                    }
                                    dettaglioCorrente.ForzatoAsfaltista = reader.GetBoolean(indiceForzatoAsfaltista);
                                    dettaglioCorrente.Selezionato = reader.GetBoolean(indiceSelezionato);
                                }
                            }

                            if (!reader.IsDBNull(indiceTagliaArticolo))
                            {
                                dettaglioCorrente.Taglie.Add(reader.GetString(indiceTagliaArticolo));
                            }
                        }
                    }
                }
            }

            return fabbisogno;
        }

        public Int32 GetLavoratoreSuccessivo(Int32 idLavoratore, Int32 idImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisognoLavoratoreSelectNextLavoratore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddOutParameter(comando, "@idLavoratoreNext", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                return (Int32) DatabaseCemi.GetParameterValue(comando, "idLavoratoreNext");
            }
        }

        public Int32 GetLavoratorePrecedente(Int32 idLavoratore, Int32 idImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisognoLavoratoreSelectPrevLavoratore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddOutParameter(comando, "@idLavoratorePrev", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                return (Int32) DatabaseCemi.GetParameterValue(comando, "idLavoratorePrev");
            }
        }

        public void SalvaFabbisognoLavoratore(int idLavoratore, int idImpresa,
                                              DettaglioGruppoFabbisognoLavoratoreCollection dettagli)
        {
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        ResettaSelezioneFabbisognoLavoratore(idLavoratore, idImpresa, transaction);

                        foreach (DettaglioGruppoFabbisognoLavoratore dettaglio in dettagli)
                        {
                            InserisciTagliaSelezionata(idLavoratore, idImpresa, dettaglio.IdIndumento,
                                                       dettaglio.TagliaSelezionata, transaction);
                        }

                        ConfermaFabbisognoLavoratore(idLavoratore, transaction);

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            }
        }

        private void ResettaSelezioneFabbisognoLavoratore(Int32 idLavoratore, Int32 idImpresa, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisognoLavoratoreResettaSelezionati"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        private void InserisciTagliaSelezionata(Int32 idLavoratore, Int32 idImpresa, String articolo, String taglia,
                                                DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeFabbisognoLavoratoreUpdateTagliaSelezionata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idArticolo", DbType.String, articolo);
                DatabaseCemi.AddInParameter(comando, "@taglia", DbType.String, taglia);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) != 1)
                {
                    throw new Exception("InserisciTagliaSelezionata: taglia non aggiornata");
                }
            }
        }

        public void ConfermaFabbisognoLavoratore(Int32 idLavoratore, DbTransaction transaction)
        {
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TuteScarpeConfermaFabbisognoLavoratore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        #endregion
    }
}