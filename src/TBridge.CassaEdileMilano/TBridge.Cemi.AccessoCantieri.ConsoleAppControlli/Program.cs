﻿using System;
using System.Configuration;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.ConsoleAppControlli
{
    internal static class Program
    {
        private static readonly TBridge.Cemi.AccessoCantieri.Business.AccessoCantieriBusiness Biz = new TBridge.Cemi.AccessoCantieri.Business.AccessoCantieriBusiness();

        private static void Main()
        {
            try
            {
                #region denunce

                if (ConfigurationManager.AppSettings["ControlloDenunceAttivo"] == "1")
                {
                    {
                        Console.WriteLine("INIZIO controllo denunce");
                        ControlloDenunce();
                        Console.WriteLine("FINE controllo denunce");

                    }
                }

                #endregion

                # region Debiti

                if (ConfigurationManager.AppSettings["ControlloDebitiAttivo"] == "1")
                {
                    {
                        Console.WriteLine("INIZIO controllo debiti");
                        ControlloDebiti();
                        Console.WriteLine("FINE controllo debiti");
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        private static void ControlloDenunce()
        {
            TimbraturaCollection timbrature = Biz.GetTimbratureAnomalieDenunce();

            foreach (Timbratura timb in timbrature)
            {
                if (timb.IdImpresa != null)
                {
                    TipologiaAnomaliaDenuncia tipoAnomalia = Biz.ControlloDenunce(timb.DataOra.Year, timb.DataOra.Month,
                                                                                  timb.IdImpresa.Value,
                                                                                  timb.IdLavoratore);

                    Biz.UpdateTimbraturaAnomalieDenunce(timb.IdTimbratura, tipoAnomalia);
                }
            }
        }

        private static void ControlloDebiti()
        {
            TimbraturaCollection timbrature = Biz.GetTimbratureAnomalieDebiti();

            foreach (Timbratura timb in timbrature)
            {
                if (timb.IdImpresa != null)
                {
                    Boolean debitiOk = Biz.ControlloDebiti(timb.IdImpresa.Value, timb.DataOra.Year, timb.DataOra.Month);

                    Biz.UpdateTimbraturaAnomalieDebiti(timb.IdTimbratura, debitiOk);
                }
            }
        }
    }
}