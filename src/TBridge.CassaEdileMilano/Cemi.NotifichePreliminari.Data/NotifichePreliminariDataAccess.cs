﻿using System;
using System.Data;
using System.Data.Common;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.NotifichePreliminari.Types.Enums;
using Cemi.NotifichePreliminari.Types.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.NotifichePreliminari.Data
{
    public class NotifichePreliminariDataAccess
    {
        private Database _databaseCemi;

        public NotifichePreliminariDataAccess()
        {
            _databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public TipoOperaNotificaCollection GetTipiOpera()
        {
            TipoOperaNotificaCollection l = new TipoOperaNotificaCollection();

            l.Add(new Types.Entities.TipoOperaNotifica { Id = 1, Descrizione = "Pubblica" });

            return l;
        }

        public NotificaPreliminare NotificaSelectByNumero(String numeroNotifica)
        {
            NotificaPreliminare notifica = null;
            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariSelectByNumero"))
            {
                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, numeroNotifica);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index
                    Int32 indexIdNotifica = reader.GetOrdinal("idNotificaPreliminareCantieri");
                    Int32 indexNumeroNotfica = reader.GetOrdinal("numeroNotifica");
                    Int32 indexProtocolloNotifica = reader.GetOrdinal("protocolloNotifica");
                    Int32 indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    Int32 indexDataAggiornamento = reader.GetOrdinal("dataAggiornamentoNotifica");
                    Int32 indexNumeroLavoratoriAutonomi = reader.GetOrdinal("numeroLavoratoriAutonomi");
                    Int32 indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    Int32 indexIdTipoContratto = reader.GetOrdinal("idTipoContratto");
                    Int32 indexDescrizioneTipoContratto = reader.GetOrdinal("descrizioneTipoContratto");
                    Int32 indexIdContratto = reader.GetOrdinal("idContratto");
                    Int32 indexIdTipoOpera = reader.GetOrdinal("idTipoOpera");
                    Int32 indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    Int32 indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    Int32 indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    Int32 indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    Int32 indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    Int32 indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    Int32 indexArt9011 = reader.GetOrdinal("art9011");
                    Int32 indexArt9011B = reader.GetOrdinal("art9011B");
                    Int32 indexArt9011C = reader.GetOrdinal("art9011C");
                    Int32 indexIdCantiereRegione = reader.GetOrdinal("idCantiereRegione");

                    #endregion


                    if (reader.Read())
                    {

                        notifica = new NotificaPreliminare
                        {
                            IdNotificaCantiere = reader.GetInt32(indexIdNotifica),
                            AltraCategoriaDescrizione = reader.GetString(indexDescrizioneAltraCategoria),
                            AltraTipologiaDescrizione = reader.GetString(indexDescrizioneAltraTipologia),
                            AmmontareComplessivo = reader.GetDecimal(indexAmmontare),
                            Articolo9011 = reader.GetBoolean(indexArt9011),
                            Articolo9011B = reader.GetBoolean(indexArt9011B),
                            Articolo9011C = reader.GetBoolean(indexArt9011C),
                            ContrattoId = reader.GetString(indexIdContratto),
                            DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione),
                            DataAggiornamentoNotifica = reader.IsDBNull(indexDataAggiornamento) ? null as DateTime? : reader.GetDateTime(indexDataAggiornamento),
                            IdCantiereRegione = reader.IsDBNull(indexIdCantiereRegione) ? 0 : reader.GetInt32(indexIdCantiereRegione),
                            NumeroImprese = reader.IsDBNull(indexNumeroImprese) ? 0 : reader.GetInt32(indexNumeroImprese),
                            NumeroLavoratoriAutonomi = reader.IsDBNull(indexNumeroLavoratoriAutonomi) ? 0 : reader.GetInt32(indexNumeroLavoratoriAutonomi),
                            NumeroNotifica = reader.GetString(indexNumeroNotfica),
                            ProtocolloNotifica = reader.GetString(indexProtocolloNotifica),
                            TipoCategoriaDescrizione = reader.GetString(indexDescrizioneTipoCategoria),
                            TipoContrattoDescrizione = reader.GetString(indexDescrizioneTipoContratto),
                            TipoContrattoId = reader.GetString(indexIdTipoContratto),
                            TipoTipologiaDescrizione = reader.GetString(indexDescrizioneTipoTipologia),
                            TipoOperaDescrizione = reader.GetString(indexDescrizioneTipoOpera),
                            TipoOperaId = (IdTipoOperaEnum) (reader.IsDBNull(indexIdTipoOpera) ? 0 : reader.GetInt32(indexIdTipoOpera)),
                        };

                    }
                }
            }

            return notifica;
        }

        public NotificaPreliminareCollection NotificheSelectByFilter(NotificaPreliminareFilter filter)
        {
            NotificaPreliminareCollection notifiche = new NotificaPreliminareCollection();

            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariSelectByFilter"))
            {
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriDal", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriAl", DbType.DateTime, filter.DataInizioLavoriAl);
                _databaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filter.NaturaOpera);
                _databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                _databaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String, filter.CantiereIndirizzo);
                _databaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, filter.CantiereCap);
                _databaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String, filter.CantiereProvncia);
                _databaseCemi.AddInParameter(comando, "@committenteCodiceFiscale", DbType.String, filter.CommittenteCodiceFiscale);
                _databaseCemi.AddInParameter(comando, "@committenteCognome", DbType.String, filter.CommittenteCognome);
                _databaseCemi.AddInParameter(comando, "@committenteNome", DbType.String, filter.CommittenteNome);
                _databaseCemi.AddInParameter(comando, "@impresaRagioneSociale", DbType.String, filter.ImpresaRagioneSociale);
                _databaseCemi.AddInParameter(comando, "@impresaCF", DbType.String, filter.ImpresaCfPIva);
                _databaseCemi.AddInParameter(comando, "@impresaProvincia", DbType.String, filter.ImpresaProvincia);
                _databaseCemi.AddInParameter(comando, "@impresaCAP", DbType.String, filter.ImpresaCap);
                _databaseCemi.AddInParameter(comando, "@dataComunicazoneDa", DbType.DateTime, filter.DataComunicazioneDal);
                _databaseCemi.AddInParameter(comando, "@dataComunicazoneA", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, filter.NumeroNotifica);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index
                    Int32 indexIdNotifica = reader.GetOrdinal("idNotificaPreliminareCantieri");
                    Int32 indexNumeroNotfica = reader.GetOrdinal("numeroNotifica");
                    Int32 indexProtocolloNotifica = reader.GetOrdinal("protocolloNotifica");
                    Int32 indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    Int32 indexDataAggiornamento = reader.GetOrdinal("dataAggiornamentoNotifica");
                    Int32 indexNumeroLavoratoriAutonomi = reader.GetOrdinal("numeroLavoratoriAutonomi");
                    Int32 indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    Int32 indexIdTipoContratto = reader.GetOrdinal("idTipoContratto");
                    Int32 indexDescrizioneTipoContratto = reader.GetOrdinal("descrizioneTipoContratto");
                    Int32 indexIdContratto = reader.GetOrdinal("idContratto");
                    Int32 indexIdTipoOpera = reader.GetOrdinal("idTipoOpera");
                    Int32 indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    Int32 indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    Int32 indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    Int32 indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    Int32 indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    Int32 indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    Int32 indexArt9011 = reader.GetOrdinal("art9011");
                    Int32 indexArt9011B = reader.GetOrdinal("art9011B");
                    Int32 indexArt9011C = reader.GetOrdinal("art9011C");
                    Int32 indexIdCantiereRegione = reader.GetOrdinal("idCantiereRegione");
                    Int32 indexIDPersona = reader.GetOrdinal("idNotificaPreliminarePersone");
                    Int32 indexPersNome = reader.GetOrdinal("personaNome");
                    Int32 indexPersCognome = reader.GetOrdinal("personaCognome");
                    Int32 indexPersCf = reader.GetOrdinal("personaCf");
                    Int32 indexIdIndirizzo = reader.GetOrdinal("idNotificaPreliminareIndirizzi");
                    Int32 indexIndirizoComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indexIndirizzoIndirizzo = reader.GetOrdinal("indirizzoIndirizzo");
                    Int32 indexIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    Int32 indexIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    #endregion

                    //NotificaPreliminare notifica;
                    NotificaPreliminare notificaPrec = null;
                    // Indirizzo indirizzo;
                    // Persona comittente;
                    int idNotifica;
                    int idPersona;
                    int idIndirizzo;
                    while (reader.Read())
                    {
                        idNotifica = reader.IsDBNull(indexIdNotifica) ? -1 : reader.GetInt32(indexIdNotifica);
                        idPersona = reader.IsDBNull(indexIDPersona) ? -1 : reader.GetInt32(indexIDPersona);
                        idIndirizzo = reader.IsDBNull(indexIdIndirizzo) ? -1 : reader.GetInt32(indexIdIndirizzo);

                        if (idNotifica != -1 && (notificaPrec == null || notificaPrec.IdNotificaCantiere != idNotifica))
                        {
                            notificaPrec = new NotificaPreliminare
                                                {
                                                    IdNotificaCantiere = idNotifica,
                                                    AltraCategoriaDescrizione = reader.GetString(indexDescrizioneAltraCategoria),
                                                    AltraTipologiaDescrizione = reader.GetString(indexDescrizioneAltraTipologia),
                                                    AmmontareComplessivo = reader.GetDecimal(indexAmmontare),
                                                    Articolo9011 = reader.GetBoolean(indexArt9011),
                                                    Articolo9011B = reader.GetBoolean(indexArt9011B),
                                                    Articolo9011C = reader.GetBoolean(indexArt9011C),
                                                    ContrattoId = reader.GetString(indexIdContratto),
                                                    DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione),
                                                    DataAggiornamentoNotifica = reader.IsDBNull(indexDataAggiornamento) ? null as DateTime? : reader.GetDateTime(indexDataAggiornamento),
                                                    IdCantiereRegione = reader.IsDBNull(indexIdCantiereRegione) ? 0 : reader.GetInt32(indexIdCantiereRegione),
                                                    NumeroImprese = reader.IsDBNull(indexNumeroImprese) ? 0 : reader.GetInt32(indexNumeroImprese),
                                                    NumeroLavoratoriAutonomi = reader.IsDBNull(indexNumeroLavoratoriAutonomi) ? 0 : reader.GetInt32(indexNumeroLavoratoriAutonomi),
                                                    NumeroNotifica = reader.GetString(indexNumeroNotfica),
                                                    ProtocolloNotifica = reader.GetString(indexProtocolloNotifica),
                                                    TipoCategoriaDescrizione = reader.GetString(indexDescrizioneTipoCategoria),
                                                    TipoContrattoDescrizione = reader.GetString(indexDescrizioneTipoContratto),
                                                    TipoContrattoId = reader.GetString(indexIdTipoContratto),
                                                    TipoTipologiaDescrizione = reader.GetString(indexDescrizioneTipoTipologia),
                                                    TipoOperaDescrizione = reader.GetString(indexDescrizioneTipoOpera),
                                                    TipoOperaId = (IdTipoOperaEnum) (reader.IsDBNull(indexIdTipoOpera) ? 0 : reader.GetInt32(indexIdTipoOpera)),
                                                };
                            notifiche.Add(notificaPrec);
                        }

                        if (idIndirizzo != -1 && !notificaPrec.Indirizzi.ContainsId(idIndirizzo))
                        {

                            Indirizzo indirizzo = new Indirizzo();
                            indirizzo.IdIndirizzo = idIndirizzo;
                            indirizzo.Comune = reader.GetString(indexIndirizoComune);
                            indirizzo.NomeVia = reader.GetString(indexIndirizzoIndirizzo);
                            indirizzo.Provincia = reader.IsDBNull(indexIndirizzoProvincia) ? null : reader.GetString(indexIndirizzoProvincia);
                            indirizzo.Cap = reader.IsDBNull(indexIndirizzoCap) ? null : reader.GetString(indexIndirizzoCap);
                            notificaPrec.Indirizzi.Add(indirizzo);
                        }

                        if (idPersona != -1 && !notificaPrec.Committenti.ContainsId(idPersona))
                        {
                            Persona committente = new Persona
                                                        {
                                                            IdPersona = idPersona,
                                                            Nome = reader.GetString(indexPersNome),
                                                            CodiceFiscale = reader.GetString(indexPersCf),
                                                            Cognome = reader.GetString(indexPersCognome),
                                                        };
                            notificaPrec.Committenti.Add(committente);
                        }


                    }
                }
            }

            return notifiche;
        }


        public PersonaCollection PersoneSelectByNumeroNotificaRuolo(String numeroNotifica, IdRuoliPersone? ruolo)
        {
            PersonaCollection persone = new PersonaCollection();
            using (DbCommand command = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariPersoneSelectByNumeroNotifica"))
            {
                _databaseCemi.AddInParameter(command, "@numeroNotifica", DbType.String, numeroNotifica);

                //TODO RUOLO

                using (IDataReader reader = _databaseCemi.ExecuteReader(command))
                {
                    Int32 indexIDPersona = reader.GetOrdinal("idNotificaPreliminarePersone");
                    Int32 indexNome = reader.GetOrdinal("nome");
                    Int32 indexCognome = reader.GetOrdinal("cognome");
                    Int32 indexCf = reader.GetOrdinal("codiceFiscale");
                    Int32 indexDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indexRegione = reader.GetOrdinal("regione");
                    Int32 indexNazione = reader.GetOrdinal("nazione");
                    Int32 indexIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indexComune = reader.GetOrdinal("comune");
                    Int32 indexCap = reader.GetOrdinal("Cap");
                    Int32 indexRuolo = reader.GetOrdinal("ruolo");
                    Int32 indexProvincia = reader.GetOrdinal("provincia");

                    Persona persona;
                    String ruoloDescrizione;
                    while (reader.Read())
                    {
                        ruoloDescrizione = reader.GetString(indexRuolo);
                        persona = new Persona
                                        {
                                            CodiceFiscale = reader.GetString(indexCf),
                                            DataNascita = reader.GetDateTime(indexDataNascita),
                                            Cognome = reader.GetString(indexCognome),
                                            Nome = reader.GetString(indexNome),
                                            IdPersona = reader.GetInt32(indexIDPersona),

                                        };

                        persona.Ruolo = new RuoloPersona(ruoloDescrizione);
                        persona.Indirizzo = new TBridge.Cemi.Type.Entities.Indirizzo
                                                     {
                                                         Cap = reader.GetString(indexCap),
                                                         NomeVia = reader.GetString(indexIndirizzo),
                                                         Comune = reader.GetString(indexComune),
                                                         Provincia = reader.GetString(indexProvincia),
                                                         Stato = reader.GetString(indexNazione),
                                                     };


                        persone.Add(persona);


                    }
                }


            }
            return persone;
        }

        public ImpresaNotifichePreliminariCollection ImpreseSelectByNumeroNotificaIncarico(String numeroNotifica, IdTipoIncaricoImpresa? idIncarico)
        {
            ImpresaNotifichePreliminariCollection imprese = new ImpresaNotifichePreliminariCollection();

            using (DbCommand command = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariImpreseSelectByNumeroNotifica"))
            {
                _databaseCemi.AddInParameter(command, "@numeroNotifica", DbType.String, numeroNotifica);

                //TODO INCARICO

                using (IDataReader reader = _databaseCemi.ExecuteReader(command))
                {
                    Int32 indexIDImpresa = reader.GetOrdinal("idNotificaPreliminareImprese");
                    Int32 indexRagSoc = reader.GetOrdinal("ragioneSociale");
                    Int32 indexCf = reader.GetOrdinal("codiceFiscale");
                    Int32 indexPIva = reader.GetOrdinal("partitaIva");
                    Int32 indexNazione = reader.GetOrdinal("nazione");
                    Int32 indexIndirizzo = reader.GetOrdinal("indirizzoSede");
                    Int32 indexComune = reader.GetOrdinal("comune");
                    Int32 indexCap = reader.GetOrdinal("CAP");
                    Int32 indexProvincia = reader.GetOrdinal("provincia");
                    Int32 indexIncaricoId = reader.GetOrdinal("idTipoIncaricoAppalto");
                    Int32 indexIncaricoDescrione = reader.GetOrdinal("descrizioneTipoIncaricoAppalto");
                    ImpresaNotifichePreliminari impresa;
                    String incaricoId;
                    String incaricoDesc;
                    while (reader.Read())
                    {
                        impresa = new ImpresaNotifichePreliminari
                        {
                            CodiceFiscale = reader.GetString(indexCf),
                            PartitaIva = reader.IsDBNull(indexPIva) ? null : reader.GetString(indexPIva),
                            RagioneSociale = reader.GetString(indexRagSoc),
                            IdImpresa = reader.GetInt32(indexIDImpresa),

                        };

                        incaricoId = reader.GetString(indexIncaricoId);
                        incaricoDesc = reader.GetString(indexIncaricoDescrione);
                        impresa.IncaricoImpresa = new IncaricoImpresa(incaricoId, incaricoDesc);

                        impresa.Indirizzo = new TBridge.Cemi.Type.Entities.Indirizzo
                        {
                            Cap = reader.GetString(indexCap),
                            NomeVia = reader.GetString(indexIndirizzo),
                            Comune = reader.GetString(indexComune),
                            Provincia = reader.GetString(indexProvincia),
                            Stato = reader.GetString(indexNazione),
                        };


                        imprese.Add(impresa);


                    }
                }
            }

            return imprese;

        }

        public IndirizzoCollection IndirizziSelectByNumeroNotifica(String numeroNotifica)
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            using (DbCommand command = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariIndirizziSelectByNumeroNotifica"))
            {
                _databaseCemi.AddInParameter(command, "@numeroNotifica", DbType.String, numeroNotifica);


                using (IDataReader reader = _databaseCemi.ExecuteReader(command))
                {
                    Int32 indexIdIndirizzo = reader.GetOrdinal("idNotificaPreliminareIndirizzi");

                    Int32 indexIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indexComune = reader.GetOrdinal("comune");
                    Int32 indexDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
                    Int32 indexDurata = reader.GetOrdinal("durataLavori");
                    Int32 indexDescrizioneDurata = reader.GetOrdinal("descrizioneDurataLavori");
                    Int32 indexAreaId = reader.GetOrdinal("idArea");
                    Int32 indexAreaDescrizione = reader.GetOrdinal("descrizioneArea");
                    Int32 indexAreaNote = reader.GetOrdinal("noteArea");
                    Int32 indexNumLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");
                    Int32 indexProvincia = reader.GetOrdinal("provincia");
                    Int32 indexCap = reader.GetOrdinal("CAP");

                    Indirizzo indirizzo;
                    while (reader.Read())
                    {
                        indirizzo = new Indirizzo();
                        indirizzo.IdIndirizzo = reader.GetInt32(indexIdIndirizzo);
                        indirizzo.Comune = reader.GetString(indexComune);
                        indirizzo.NomeVia = reader.GetString(indexIndirizzo);
                        indirizzo.DataInizioLavori = reader.IsDBNull(indexDataInizioLavori) ? (DateTime?) null : reader.GetDateTime(indexDataInizioLavori);
                        indirizzo.NumeroDurata = reader.IsDBNull(indexDurata) ? (int?) null : reader.GetInt32(indexDurata);
                        indirizzo.DescrizioneDurata = reader.IsDBNull(indexDescrizioneDurata) ? null : reader.GetString(indexDescrizioneDurata);
                        indirizzo.NumeroMassimoLavoratori = reader.IsDBNull(indexNumLavoratori) ? (int?) null : reader.GetInt32(indexNumLavoratori);
                        indirizzo.Provincia = reader.IsDBNull(indexProvincia) ? null : reader.GetString(indexProvincia);
                        indirizzo.Cap = reader.IsDBNull(indexCap) ? null : reader.GetString(indexCap);

                        indirizzi.Add(indirizzo);
                    }
                }
            }

            return indirizzi;
        }

        public CantiereCollection CantieriPerCommittenteSelectByFilter(NotificaPreliminareFilter filter)
        {
            CantiereCollection cantieri = new CantiereCollection();

            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariCantieriSelectRicercaCantieriOrdPerCommittente"))
            {
                _databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                //   _databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filter.CantiereIndirizzo);
                _databaseCemi.AddInParameter(comando, "@comune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@provincia", DbType.String, filter.CantiereProvncia);
                _databaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filter.DataComunicazioneDal);
                _databaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@impresa", DbType.String, filter.ImpresaRagioneSociale);
                _databaseCemi.AddInParameter(comando, "@ivaFiscImpresa", DbType.String, filter.ImpresaCfPIva);
                _databaseCemi.AddInParameter(comando, "@provinciaImpresa", DbType.String, filter.ImpresaProvincia);
                _databaseCemi.AddInParameter(comando, "@capImpresa", DbType.String, filter.ImpresaCap);

                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, filter.NumeroNotifica);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriDal", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriAl", DbType.DateTime, filter.DataInizioLavoriAl);
                _databaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filter.NaturaOpera);
                //_databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                //_databaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String, filter.CantiereIndirizzo);
                //_databaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String, filter.CantiereProvncia);
                //_databaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, filter.CantiereCap);
                //_databaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@committenteCodiceFiscale", DbType.String, filter.CommittenteCodiceFiscale);
                _databaseCemi.AddInParameter(comando, "@committenteCognome", DbType.String, filter.CommittenteCognome);
                _databaseCemi.AddInParameter(comando, "@committenteNome", DbType.String, filter.CommittenteNome);
                //_databaseCemi.AddInParameter(comando, "@impresaRagioneSociale", DbType.String, filter.ImpresaRagioneSociale);
                //_databaseCemi.AddInParameter(comando, "@impresaCF", DbType.String, filter.ImpresaCfPIva);
                //_databaseCemi.AddInParameter(comando, "@impresaProvincia", DbType.String, filter.ImpresaProvincia);
                //_databaseCemi.AddInParameter(comando, "@impresaCAP", DbType.String, filter.ImpresaCap);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneDa", DbType.DateTime, filter.DataComunicazioneDal);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneA", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@indirizzoGeolocalizzato", DbType.Boolean, filter.CantiereIndirizzoGeolocalizzato);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index
                    Int32 indexNumeroNotifica = reader.GetOrdinal("numeroNotifica");
                    Int32 indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    Int32 indexIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indexComune = reader.GetOrdinal("comune");
                    Int32 indexDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
                    Int32 indexDurata = reader.GetOrdinal("durataLavori");
                    Int32 indexDescrizioneDurata = reader.GetOrdinal("descrizioneDurataLavori");
                    Int32 indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    Int32 indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    Int32 indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    Int32 indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    Int32 indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    Int32 indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    Int32 indexCommittenteCognome = reader.GetOrdinal("committenteCognome");
                    Int32 indexCommittenteNome = reader.GetOrdinal("committenteNome");
                    Int32 indexCommittenteCodiceFiscale = reader.GetOrdinal("committenteCodiceFiscale");
                    Int32 indexImpresaRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    Int32 indexImpresaCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    Int32 indexImpresaProvincia = reader.GetOrdinal("impresaProvincia");
                    Int32 indexImpresaCap = reader.GetOrdinal("impresaCap");
                    Int32 indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    Int32 indexNumeroLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");
                    Int32 indexLogitudine = reader.GetOrdinal("geolocalizzazioneLongitudine");
                    Int32 indexLatitudine = reader.GetOrdinal("geolocalizzazioneLatitudine");
                    Int32 indexIndirizzoProvincia = reader.GetOrdinal("provincia");
                    #endregion

                    while (reader.Read())
                    {
                        Cantiere cantiere = new Cantiere();
                        cantieri.Add(cantiere);

                        cantiere.NumeroNotifica = reader.GetString(indexNumeroNotifica);
                        cantiere.DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione);
                        //cantiere.Indirizzo = reader.GetString(indexIndirizzo);
                        //cantiere.Comune = reader.GetString(indexComune);
                        cantiere.DataInizio = !reader.IsDBNull(indexDataInizioLavori) ? (DateTime?) reader.GetDateTime(indexDataInizioLavori) : null;
                        cantiere.Durata = !reader.IsDBNull(indexDurata) ? reader.GetInt32(indexDurata) : 0;
                        cantiere.DescrizioneDurata = reader.IsDBNull(indexDescrizioneDurata) ? null : reader.GetString(indexDescrizioneDurata);

                        cantiere.TipoCategoriaDescrizione = reader.IsDBNull(indexDescrizioneDurata) ? null : reader.GetString(indexDescrizioneTipoCategoria);
                        cantiere.TipoOperaDescrizione = reader.IsDBNull(indexDescrizioneTipoOpera) ? null : reader.GetString(indexDescrizioneTipoOpera);
                        cantiere.TipoTipologiaDescrizione = reader.IsDBNull(indexDescrizioneTipoTipologia) ? null : reader.GetString(indexDescrizioneTipoTipologia);
                        cantiere.AltraCategoriaDescrizione = reader.IsDBNull(indexDescrizioneAltraCategoria) ? null : reader.GetString(indexDescrizioneAltraCategoria);
                        cantiere.AltraTipologiaDescrizione = reader.IsDBNull(indexDescrizioneAltraTipologia) ? null : reader.GetString(indexDescrizioneAltraTipologia);
                        cantiere.CommittenteCognome = reader.IsDBNull(indexCommittenteCognome) ? null : reader.GetString(indexCommittenteCognome);
                        cantiere.CommittenteNome = reader.IsDBNull(indexCommittenteNome) ? null : reader.GetString(indexCommittenteNome);
                        cantiere.CommittenteCodiceFiscale = reader.IsDBNull(indexCommittenteCodiceFiscale) ? null : reader.GetString(indexCommittenteCodiceFiscale);

                        cantiere.ImpresaRagioneSociale = !reader.IsDBNull(indexImpresaRagioneSociale) ? reader.GetString(indexImpresaRagioneSociale) : null;
                        cantiere.ImpresaCodiceFiscale = !reader.IsDBNull(indexImpresaCodiceFiscale) ? reader.GetString(indexImpresaCodiceFiscale) : null;
                        cantiere.ImpresaProvincia = !reader.IsDBNull(indexImpresaProvincia) ? reader.GetString(indexImpresaProvincia) : null;
                        cantiere.ImpresaCap = !reader.IsDBNull(indexImpresaCap) ? reader.GetString(indexImpresaCap) : null;
                        cantiere.NumeroImprese = !reader.IsDBNull(indexNumeroImprese) ? (Int32?) reader.GetInt32(indexNumeroImprese) : null;
                        cantiere.NumeroLavoratori = !reader.IsDBNull(indexNumeroLavoratori) ? (Int32?) reader.GetInt32(indexNumeroLavoratori) : null;
                        cantiere.AmmontareComplessivo = reader.IsDBNull(indexAmmontare) ? 0 : reader.GetDecimal(indexAmmontare);

                        cantiere.Indirizzo = new TBridge.Cemi.Geocode.Type.Entities.Indirizzo
                        {
                            NomeVia = reader.IsDBNull(indexIndirizzo) ? null : reader.GetString(indexIndirizzo),
                            Comune = reader.IsDBNull(indexComune) ? null : reader.GetString(indexComune),
                            Provincia = reader.IsDBNull(indexIndirizzoProvincia) ? null : reader.GetString(indexIndirizzoProvincia),
                            Latitudine = reader.IsDBNull(indexLatitudine) ? (Decimal?) null : reader.GetDecimal(indexLatitudine),
                            Longitudine = reader.IsDBNull(indexLogitudine) ? (Decimal?) null : reader.GetDecimal(indexLogitudine),
                        };
                    }
                }
            }

            return cantieri;
        }

        public CantiereCollection CantieriPerImpresaSelectByFilter(NotificaPreliminareFilter filter)
        {
            CantiereCollection cantieri = new CantiereCollection();

            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariCantieriSelectRicercaCantieriOrdPerImpresa"))
            {
                _databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                // _databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filter.CantiereIndirizzo);
                _databaseCemi.AddInParameter(comando, "@comune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@provincia", DbType.String, filter.CantiereProvncia);
                _databaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filter.DataComunicazioneDal);
                _databaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@impresa", DbType.String, filter.ImpresaRagioneSociale);
                _databaseCemi.AddInParameter(comando, "@ivaFiscImpresa", DbType.String, filter.ImpresaCfPIva);
                _databaseCemi.AddInParameter(comando, "@provinciaImpresa", DbType.String, filter.ImpresaProvincia);
                _databaseCemi.AddInParameter(comando, "@capImpresa", DbType.String, filter.ImpresaCap);

                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, filter.NumeroNotifica);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriDal", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriAl", DbType.DateTime, filter.DataInizioLavoriAl);
                _databaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filter.NaturaOpera);
                //_databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                //_databaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String, filter.CantiereIndirizzo);
                //_databaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String, filter.CantiereProvncia);
                //_databaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, filter.CantiereCap);
                //_databaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@committenteCodiceFiscale", DbType.String, filter.CommittenteCodiceFiscale);
                _databaseCemi.AddInParameter(comando, "@committenteCognome", DbType.String, filter.CommittenteCognome);
                _databaseCemi.AddInParameter(comando, "@committenteNome", DbType.String, filter.CommittenteNome);
                //_databaseCemi.AddInParameter(comando, "@impresaRagioneSociale", DbType.String, filter.ImpresaRagioneSociale);
                //_databaseCemi.AddInParameter(comando, "@impresaCF", DbType.String, filter.ImpresaCfPIva);
                //_databaseCemi.AddInParameter(comando, "@impresaProvincia", DbType.String, filter.ImpresaProvincia);
                //_databaseCemi.AddInParameter(comando, "@impresaCAP", DbType.String, filter.ImpresaCap);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneDa", DbType.DateTime, filter.DataComunicazioneDal);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneA", DbType.DateTime, filter.DataComunicazioneAl);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index
                    Int32 indexNumeroNotifica = reader.GetOrdinal("numeroNotifica");
                    Int32 indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    Int32 indexIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indexComune = reader.GetOrdinal("comune");
                    Int32 indexDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
                    Int32 indexDurata = reader.GetOrdinal("durataLavori");
                    Int32 indexDescrizioneDurata = reader.GetOrdinal("descrizioneDurataLavori");
                    Int32 indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    Int32 indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    Int32 indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    Int32 indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    Int32 indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    Int32 indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    Int32 indexCommittenteCognome = reader.GetOrdinal("committenteCognome");
                    Int32 indexCommittenteNome = reader.GetOrdinal("committenteNome");
                    Int32 indexCommittenteCodiceFiscale = reader.GetOrdinal("committenteCodiceFiscale");
                    Int32 indexImpresaRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    Int32 indexImpresaCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    Int32 indexImpresaProvincia = reader.GetOrdinal("impresaProvincia");
                    Int32 indexImpresaCap = reader.GetOrdinal("impresaCap");
                    Int32 indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    Int32 indexNumeroLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");
                    Int32 indexLogitudine = reader.GetOrdinal("geolocalizzazioneLongitudine");
                    Int32 indexLatitudine = reader.GetOrdinal("geolocalizzazioneLatitudine");
                    Int32 indexIndirizzoProvincia = reader.GetOrdinal("provincia");
                    #endregion

                    while (reader.Read())
                    {
                        Cantiere cantiere = new Cantiere();
                        cantieri.Add(cantiere);

                        cantiere.NumeroNotifica = reader.GetString(indexNumeroNotifica);
                        cantiere.DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione);
                        //cantiere.Indirizzo = reader.GetString(indexIndirizzo);
                        //cantiere.Comune = reader.GetString(indexComune);
                        cantiere.DataInizio = !reader.IsDBNull(indexDataInizioLavori) ? (DateTime?) reader.GetDateTime(indexDataInizioLavori) : null;
                        cantiere.Durata = !reader.IsDBNull(indexDurata) ? reader.GetInt32(indexDurata) : 0;
                        cantiere.DescrizioneDurata = reader.IsDBNull(indexDescrizioneDurata) ? null : reader.GetString(indexDescrizioneDurata);

                        cantiere.TipoCategoriaDescrizione = reader.IsDBNull(indexDescrizioneDurata) ? null : reader.GetString(indexDescrizioneTipoCategoria);
                        cantiere.TipoOperaDescrizione = reader.IsDBNull(indexDescrizioneTipoOpera) ? null : reader.GetString(indexDescrizioneTipoOpera);
                        cantiere.TipoTipologiaDescrizione = reader.IsDBNull(indexDescrizioneTipoTipologia) ? null : reader.GetString(indexDescrizioneTipoTipologia);
                        cantiere.AltraCategoriaDescrizione = reader.IsDBNull(indexDescrizioneAltraCategoria) ? null : reader.GetString(indexDescrizioneAltraCategoria);
                        cantiere.AltraTipologiaDescrizione = reader.IsDBNull(indexDescrizioneAltraTipologia) ? null : reader.GetString(indexDescrizioneAltraTipologia);
                        cantiere.CommittenteCognome = reader.IsDBNull(indexCommittenteCognome) ? null : reader.GetString(indexCommittenteCognome);
                        cantiere.CommittenteNome = reader.IsDBNull(indexCommittenteNome) ? null : reader.GetString(indexCommittenteNome);
                        cantiere.CommittenteCodiceFiscale = reader.IsDBNull(indexCommittenteCodiceFiscale) ? null : reader.GetString(indexCommittenteCodiceFiscale);

                        cantiere.ImpresaRagioneSociale = !reader.IsDBNull(indexImpresaRagioneSociale) ? reader.GetString(indexImpresaRagioneSociale) : null;
                        cantiere.ImpresaCodiceFiscale = !reader.IsDBNull(indexImpresaCodiceFiscale) ? reader.GetString(indexImpresaCodiceFiscale) : null;
                        cantiere.ImpresaProvincia = !reader.IsDBNull(indexImpresaProvincia) ? reader.GetString(indexImpresaProvincia) : null;
                        cantiere.ImpresaCap = !reader.IsDBNull(indexImpresaCap) ? reader.GetString(indexImpresaCap) : null;
                        cantiere.NumeroImprese = !reader.IsDBNull(indexNumeroImprese) ? (Int32?) reader.GetInt32(indexNumeroImprese) : null;
                        cantiere.NumeroLavoratori = !reader.IsDBNull(indexNumeroLavoratori) ? (Int32?) reader.GetInt32(indexNumeroLavoratori) : null;
                        cantiere.AmmontareComplessivo = reader.IsDBNull(indexAmmontare) ? 0 : reader.GetDecimal(indexAmmontare);

                        cantiere.Indirizzo = new TBridge.Cemi.Geocode.Type.Entities.Indirizzo
                        {
                            NomeVia = reader.IsDBNull(indexIndirizzo) ? null : reader.GetString(indexIndirizzo),
                            Comune = reader.IsDBNull(indexComune) ? null : reader.GetString(indexComune),
                            Provincia = reader.IsDBNull(indexIndirizzoProvincia) ? null : reader.GetString(indexIndirizzoProvincia),
                            Latitudine = reader.IsDBNull(indexLatitudine) ? (Decimal?) null : reader.GetDecimal(indexLatitudine),
                            Longitudine = reader.IsDBNull(indexLogitudine) ? (Decimal?) null : reader.GetDecimal(indexLogitudine),
                        };
                    }
                }
            }

            return cantieri;
        }

        public TBridge.Cemi.Cantieri.Type.Collections.CantiereCollection GetCantieriGenerati(String protocolloRegione)
        {
            TBridge.Cemi.Cantieri.Type.Collections.CantiereCollection cantieri = new TBridge.Cemi.Cantieri.Type.Collections.CantiereCollection();

            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariSelectCantieriGenerati"))
            {
                _databaseCemi.AddInParameter(comando, "@protocolloRegione", DbType.String, protocolloRegione);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader
                    Int32 indiceId = reader.GetOrdinal("idCantiere");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    #endregion

                    while (reader.Read())
                    {
                        TBridge.Cemi.Cantieri.Type.Entities.Cantiere indirizzo = new TBridge.Cemi.Cantieri.Type.Entities.Cantiere();
                        cantieri.Add(indirizzo);

                        indirizzo.IdCantiere = reader.GetInt32(indiceId);
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            indirizzo.Indirizzo = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            indirizzo.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            indirizzo.Provincia = reader.GetString(indiceProvincia);
                        }
                    }
                }
            }

            return cantieri;
        }
    }
}
