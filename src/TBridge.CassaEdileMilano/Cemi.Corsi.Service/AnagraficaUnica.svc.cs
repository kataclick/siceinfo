﻿using System;
using System.ServiceModel;
using Cemi.Corsi.Service.Interfaces;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.DataSets;
using TBridge.Cemi.Corsi.Type.Filters;

namespace Cemi.Corsi.Service
{
    [ServiceBehavior(Namespace = "http://www.cassaedilemilano.it/AnagraficaUnica")]
    public class AnagraficaUnica : IAnagraficaUnica
    {
        #region IAnagraficaUnica Members
        public TBridge.Cemi.Corsi.Type.DataSets.Corsi GetCorsi(Int32 idPianificazione)
        {
            CorsiBusiness corsiBiz = new CorsiBusiness();
            AnagraficaUnicaCorsiFilter filtro = new AnagraficaUnicaCorsiFilter();

            if (idPianificazione > 0)
            {
                filtro.IdPianificazione = idPianificazione;
            }

            return corsiBiz.AnagraficaUnicaRicercaCorsi(filtro);
        }

        public Iscrizioni GetIscrizioni(Int32 idPianificazione)
        {
            CorsiBusiness corsiBiz = new CorsiBusiness();
            AnagraficaUnicaIscrizioniFilter filtro = new AnagraficaUnicaIscrizioniFilter();

            if (idPianificazione > 0)
            {
                filtro.IdPianificazione = idPianificazione;
            }

            return corsiBiz.AnagraficaUnicaRicercaIscrizioni(filtro);
        }

        public Lavoratori RicercaLavoratori(String codiceFiscale)
        {
            CorsiBusiness corsiBiz = new CorsiBusiness();
            AnagraficaUnicaLavoratoriFilter filtro = new AnagraficaUnicaLavoratoriFilter();

            if (!String.IsNullOrEmpty(codiceFiscale))
            {
                filtro.CodiceFiscale = codiceFiscale;
            }

            return corsiBiz.AnagraficaUnicaRicercaLavoratori(filtro);
        }

        public Imprese RicercaImprese(String codiceFiscalePartitaIva)
        {
            CorsiBusiness corsiBiz = new CorsiBusiness();
            AnagraficaUnicaImpreseFilter filtro = new AnagraficaUnicaImpreseFilter();

            if (!String.IsNullOrEmpty(codiceFiscalePartitaIva))
            {
                filtro.CodiceFiscalePartitaIva = codiceFiscalePartitaIva;
            }

            return corsiBiz.AnagraficaUnicaRicercaImprese(filtro);
        }
        #endregion
    }
}
