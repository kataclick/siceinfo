﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using TBridge.Cemi.Corsi.Type.DataSets;

namespace Cemi.Corsi.Service.Interfaces
{
    [ServiceContract(Namespace = "http://www.cassaedilemilano.it/AnagraficaUnica")]
    public interface IAnagraficaUnica
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Xml, UriTemplate = "GetCorsi?idPianificazione={idPianificazione}")]
        TBridge.Cemi.Corsi.Type.DataSets.Corsi GetCorsi(Int32 idPianificazione);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Xml, UriTemplate = "GetIscrizioni?idPianificazione={idPianificazione}")]
        Iscrizioni GetIscrizioni(Int32 idPianificazione);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Xml, UriTemplate = "RicercaLavoratori?codiceFiscale={codiceFiscale}")]
        Lavoratori RicercaLavoratori(String codiceFiscale);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Xml, UriTemplate = "RicercaImprese?codiceFiscalePartitaIva={codiceFiscalePartitaIva}")]
        Imprese RicercaImprese(String codiceFiscalePartitaIva);
    }
}