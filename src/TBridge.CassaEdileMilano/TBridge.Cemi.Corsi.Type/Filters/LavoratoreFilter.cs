﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class LavoratoreFilter
    {
        public String Cognome { get; set; }

        public String Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public String CodiceFiscale { get; set; }

        public Boolean InForza { get; set; }

        public Int32? IdImpresa { get; set; }
    }
}