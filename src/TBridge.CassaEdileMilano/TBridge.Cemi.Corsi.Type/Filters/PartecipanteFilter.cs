﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class PartecipanteFilter
    {
        public Int32 IdProgrammazioneModulo { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CodiceFiscale { get; set; }

        public Boolean AttestatoRilasciato { get; set; }
    }
}