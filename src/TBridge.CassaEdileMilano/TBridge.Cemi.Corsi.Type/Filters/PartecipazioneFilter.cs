﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class PartecipazioneFilter
    {
        public Int32? IdLavoratore { get; set; }
    }
}