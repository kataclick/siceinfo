﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class ProgrammazioneModuloFilter
    {
        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public Int32? IdCorso { get; set; }

        public Int32? IdLocazione { get; set; }
    }
}