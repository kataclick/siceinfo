﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Corsi.Type.Filters.Esem
{
    public class CorsoFilter
    {
        public DateTime Dal { get; set; }

        public DateTime Al { get; set; }

        public String CodiceTipoCorso { get; set; }

        public String CodiceSede { get; set; }
    }
}
