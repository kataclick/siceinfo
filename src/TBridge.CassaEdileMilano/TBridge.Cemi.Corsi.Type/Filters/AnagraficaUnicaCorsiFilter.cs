﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    public class AnagraficaUnicaCorsiFilter
    {
        public Int32? IdPianificazione { get; set; }
    }
}
