﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class ImpresaFilter
    {
        public Int32? IdImpresa { get; set; }

        public String RagioneSociale { get; set; }

        public String IvaFisc { get; set; }

        public Int32? IdConsulente { get; set; }
    }
}