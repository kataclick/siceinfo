using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class PrestazioneDomandaCorsoFilter
    {
        public Int32? IdCorsiPrestazioneDomanda { get; set; }

        public String Cognome { get; set; }
        public String Nome { get; set; }
        public String CodiceFiscale { get; set; }

        public Int32? IdLavoratore { get; set; }

        public String IdTipoStatoPrestazione { get; set; }

        public Boolean? DenunciaPresente { get; set; }
        public Boolean? CodiceFiscalePresente { get; set; }
        public Boolean? CarpaPrepagataPresente { get; set; }
        public Boolean? Iscritto { get; set; }
    }
}