﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    public class AnagraficaUnicaIscrizioniFilter
    {
        public Int32? IdPianificazione { get; set; }
    }
}
