﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class DTAFilter
    {
        public String Cognome { get; set; }

        public String Corso { get; set; }

        public Boolean? Diritto { get; set; }
    }
}
