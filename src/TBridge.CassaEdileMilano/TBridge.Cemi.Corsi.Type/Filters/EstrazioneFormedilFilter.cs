﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class EstrazioneFormedilFilter
    {
        public DateTime Dal { get; set; }

        public DateTime Al { get; set; }
    }
}