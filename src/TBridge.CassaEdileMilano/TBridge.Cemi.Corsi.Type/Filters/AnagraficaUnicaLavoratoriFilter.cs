﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    public class AnagraficaUnicaLavoratoriFilter
    {
        public String CodiceFiscale { get; set; }
    }
}
