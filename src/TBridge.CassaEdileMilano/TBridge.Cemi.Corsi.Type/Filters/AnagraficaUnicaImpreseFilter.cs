﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    public class AnagraficaUnicaImpreseFilter
    {
        public String CodiceFiscalePartitaIva { get; set; }
    }
}
