﻿using System;

namespace TBridge.Cemi.Corsi.Type.Filters
{
    [Serializable]
    public class PartecipazioneLavoratoreImpresaFilter
    {
        public String LavoratoreCognome { get; set; }

        public String LavoratoreNome { get; set; }

        public String LavoratoreCodiceFiscale { get; set; }

        public Int32? IdImpresa { get; set; }

        public String ImpresaRagioneSociale { get; set; }

        public String ImpresaIvaFisc { get; set; }

        public Int32? IdConsulente { get; set; }
    }
}