using System;
using TBridge.Cemi.Corsi.Type.Entities;

namespace TBridge.Cemi.Corsi.Type.Delegates
{
    public delegate void ProgrammazioneSelectedEventHandler(Int32 idProgrammazione, Int32 idProgrammazioneModulo);

    public delegate void LavoratoreSelectedEventHandler(Lavoratore lavoratore);

    public delegate void LavoratoreNuovoEventHandler();

    public delegate void LavoratoreRicercaEventHandler();

    public delegate void ImpresaSelectedEventHandler(Impresa impresa);

    public delegate void ImpresaNuovaEventHandler();

    public delegate void RicercaLavoratoreSelectedEventHandler(Int32 idCorsiPrestazioneDomanda);
}