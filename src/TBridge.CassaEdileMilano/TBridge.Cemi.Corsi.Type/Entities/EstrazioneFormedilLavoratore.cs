﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class EstrazioneFormedilLavoratore
    {
        public String Corso { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public Int32? Sesso { get; set; }

        public String CodiceFiscale { get; set; }

        public String Cittadinanza { get; set; }

        public DateTime? DataNascita { get; set; }

        public String ProvinciaResidenza { get; set; }

        public Int32? TitoloDiStudio { get; set; }

        public String Azienda { get; set; }

        public String DateCorso { get; set; }
    }
}