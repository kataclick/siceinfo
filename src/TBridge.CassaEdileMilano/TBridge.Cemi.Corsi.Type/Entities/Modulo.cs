﻿using System;
using TBridge.Cemi.Corsi.Type.Collections;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class Modulo
    {
        public Int32 IdModulo { get; set; }

        public String Descrizione { get; set; }

        public Int16 OreDurata { get; set; }

        public Int16 Progressivo { get; set; }

        public ProgrammazioneModuloCollection Programmazioni { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}