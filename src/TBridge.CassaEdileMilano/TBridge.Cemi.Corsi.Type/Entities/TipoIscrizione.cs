﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class TipoIscrizione
    {
        public Int32 IdTipoIscrizione { get; set; }

        public String Descrizione { get; set; }
    }
}