﻿using System;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class Impresa : ImpresaAnagraficaComune
    {
        public TipologiaImpresa TipoImpresa { get; set; }

        public String CodiceERagioneSociale
        {
            get
            {
                if (IdImpresa.HasValue && TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    return String.Format("{0} {1}", IdImpresa, RagioneSociale);
                }
                else
                {
                    return RagioneSociale;
                }
            }
        }

        public TipoContratto TipoContratto { get; set; }

        public TipoIscrizione TipoIscrizione { get; set; }

        public Int32? NumeroDipendenti { get; set; }

        public String ProvinciaSedeLegale { get; set; }

        public String ComuneSedeLegale { get; set; }

        public Int32 IdAnagraficaCondivisa { get; set; }
    }
}