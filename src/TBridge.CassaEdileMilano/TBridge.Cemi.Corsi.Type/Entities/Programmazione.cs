﻿using System;
using TBridge.Cemi.Corsi.Type.Collections;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class Programmazione
    {
        public Int32? IdProgrammazione { get; set; }

        public Corso Corso { get; set; }

        public ProgrammazioneModuloCollection ProgrammazioniModuli { get; set; }
    }
}