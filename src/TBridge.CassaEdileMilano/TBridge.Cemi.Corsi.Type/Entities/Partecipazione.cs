﻿using System;
using TBridge.Cemi.Corsi.Type.Collections;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class Partecipazione
    {
        public Int32? IdPartecipazione { get; set; }

        public Corso Corso { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public Boolean AttestatoRilasciabile { get; set; }

        public PartecipazioneModuloCollection PartecipazioneModuli { get; set; }

        public Boolean PrenotazioneImpresa { get; set; }

        public Boolean PrenotazioneConsulente { get; set; }

        public Int32? IdConsulente { get; set; }
    }
}