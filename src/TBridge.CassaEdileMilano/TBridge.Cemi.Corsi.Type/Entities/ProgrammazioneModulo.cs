﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class ProgrammazioneModulo
    {
        private Modulo modulo;
        public Int32? IdProgrammazioneModulo { get; set; }

        public Corso Corso { get; set; }

        public Modulo Modulo
        {
            get { return modulo; }
            set { modulo = value; }
        }

        public Int32 IdModulo
        {
            get
            {
                if (modulo != null) return modulo.IdModulo;
                else return -1;
            }
        }

        public DateTime? Schedulazione { get; set; }

        public DateTime? SchedulazioneFine { get; set; }

        public String SchedulazioneData
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    return Schedulazione.Value.ToShortDateString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String SchedulazioneOra
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    return Schedulazione.Value.ToShortTimeString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String SchedulazioneStringa
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    if (Locazione.Latitudine.HasValue && Locazione.Longitudine.HasValue)
                    {
                        return
                            String.Format(
                                "<B>{0} - {1}</B> ({2})<BR />Posti occupati: <B>{3} / {4}</B><BR /><A href=\"{5}\"> Visualizza Mappa</A>",
                                Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                SchedulazioneFine.Value.ToString("HH:mm"),
                                Locazione.IndirizzoCompleto,
                                Partecipanti,
                                MaxPartecipanti,
                                String.Format("MappaLocazione.aspx?idLocazione={0}", Locazione.IdLocazione));
                    }
                    else
                    {
                        return String.Format("<B>{0} - {1}</B> ({2})<BR />Posti occupati: <B>{3} / {4}</B><BR />",
                                             Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                             Schedulazione.Value.ToString("HH:mm"),
                                             Locazione.IndirizzoCompleto,
                                             Partecipanti,
                                             MaxPartecipanti);
                    }
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String SchedulazioneStringaImpresa
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    if (Locazione.Latitudine.HasValue && Locazione.Longitudine.HasValue)
                    {
                        return
                            String.Format(
                                "<B>{0} - {1}</B> ({2})<BR />Posti residui: <B>{3}</B><BR /><A href=\"{4}\"> Visualizza Mappa</A>",
                                Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                SchedulazioneFine.Value.ToString("HH:mm"),
                                Locazione.IndirizzoCompleto,
                                (MaxPartecipanti - Partecipanti),
                                String.Format("MappaLocazione.aspx?idLocazione={0}", Locazione.IdLocazione));
                    }
                    else
                    {
                        return String.Format("<B>{0} - {1}</B> ({2})<BR />",
                                             Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                             SchedulazioneFine.Value.ToString("HH:mm"),
                                             Locazione.IndirizzoCompleto);
                    }
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public Locazione Locazione { get; set; }

        public Int32? IdProgrammazione { get; set; }

        public Int32 Partecipanti { get; set; }

        public Int32 MaxPartecipanti { get; set; }

        public Boolean PresenzeConfermate { get; set; }

        public Boolean Prenotabile { get; set; }

        public String IdComposto
        {
            get { return String.Format("{0}|{1}", IdProgrammazioneModulo, IdProgrammazione); }
        }

        public static Int32 IdProgrammazioneModuloDaIdComposto(String idComposto)
        {
            String[] idCompostoSplitted = idComposto.Split('|');
            return Int32.Parse(idCompostoSplitted[0]);
        }

        public static Int32 IdProgrammazioneDaIdComposto(String idComposto)
        {
            String[] idCompostoSplitted = idComposto.Split('|');
            return Int32.Parse(idCompostoSplitted[1]);
        }
    }
}