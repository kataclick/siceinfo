﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    public class PrestazioneDTA
    {
        public String CodiceFiscale { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public DateTime DataInizioCorso { get; set; }

        public DateTime? DataFineCorso { get; set; }

        public String CodiceCorso { get; set; }

        public Int32? IdLavoratore { get; set; }

        public DateTime? DataErogazione { get; set; }

        public Boolean DenunciaPresente { get; set; }

        public Boolean IbanPresente { get; set; }

        public Boolean Diritto
        {
            get
            {
                Boolean diritto = this.DenunciaPresente && this.IbanPresente;
                return diritto;
            }
        }

        public Boolean Selezionato { get; set; }

        public Int32? ProtocolloPrestazione { get; set; }

        public Int32? NumeroProtocolloPrestazione { get; set; }

        public String ProtocolloPrestazioneCompleto
        {
            get
            {
                String prot = null;

                if (this.ProtocolloPrestazione.HasValue
                    && this.NumeroProtocolloPrestazione.HasValue)
                {
                    prot = String.Format("{0}/{1}", this.ProtocolloPrestazione, this.NumeroProtocolloPrestazione);
                }

                return prot;
            }
        }

        public String IdStato { get; set; }

        public String DescrizioneStato { get; set; }
    }
}
