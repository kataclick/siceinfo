﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class TipoContratto
    {
        public Int32 IdTipoContratto { get; set; }

        public String Descrizione { get; set; }
    }
}