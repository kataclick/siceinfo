﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Corsi.Type.Entities.Esem
{
    [Serializable]
    public class Corso : IComparable
    {
        public String Codice { get; set; }

        public DateTime? DataInizio { get; set; }

        public String CodiceSede { get; set; }

        public String DescrizioneSede { get; set; }

        public String IndirizzoSede { get; set; }

        public Int32 Progressivo { get; set; }

        public String Descrizione { get; set; }

        public DateTime? DataFine { get; set; }

        public DateTime? DataPresuntaPerPrenotazione { get; set; }

        public Decimal Durata { get; set; }

        public Decimal Costo { get; set; }

        public Int32 PostiDisponibili { get; set; }

        public String ObbligoVisitaMedica { get; set; }

        public DateTime? DataScadenza { get; set; }

        public List<DateCorso> DateCorso { get; set; }

        public Decimal CostoCauzione { get; set; }

        public DateTime? DataPagamentoCauzione { get; set; } 

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(Corso))
            {
                if (this.DataInizio < ((Corso)obj).DataInizio)
                    return -1;
                if (this.DataInizio > ((Corso)obj).DataInizio)
                    return 1;
                if (this.DataInizio == ((Corso)obj).DataInizio)
                    return 0;
            }

            return 0;
        }
    }
}
