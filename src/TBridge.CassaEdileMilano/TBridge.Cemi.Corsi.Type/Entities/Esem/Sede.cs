﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Corsi.Type.Entities.Esem
{
    public class Sede : IComparable
    {
        public String Codice { get; set; }

        public String Descrizione { get; set; }

        public String Indirizzo { get; set; }

        public String Calendarizzato { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(Sede))
            {
                return this.Descrizione.CompareTo(((Sede)obj).Descrizione);
            }

            return 0;
        }
    }
}
