﻿using System;
using System.Collections.Generic;

namespace TBridge.Cemi.Corsi.Type.Entities.Esem
{
    [Serializable]
    public class DateCorso: IComparable
    {
        public DateTime GiornoCorso { get; set; }

        public String OraInizio { get; set; }

        public String OraFine { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(DateCorso))
            {
                if (this.GiornoCorso < ((DateCorso)obj).GiornoCorso)
                    return -1;
                if (this.GiornoCorso > ((DateCorso)obj).GiornoCorso)
                    return 1;
                if (this.GiornoCorso == ((DateCorso)obj).GiornoCorso)
                    return 0;
            }

            return 0;
        }

    }
}
