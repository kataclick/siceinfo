﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities.Esem
{
    [Serializable]
    public class Iscrizione
    {
        public Corso Corso { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public string Contatto { get; set; }

        public Boolean Gratuito { get; set; }

        public Boolean Ticket { get; set; }
    }
}
