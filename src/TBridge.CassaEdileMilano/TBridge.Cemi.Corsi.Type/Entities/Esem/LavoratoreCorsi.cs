﻿using System;
using System.Collections.Generic;

namespace TBridge.Cemi.Corsi.Type.Entities.Esem
{
    public class LavoratoreCorsi : IComparable
    {
        public Lavoratore Lavoratore { get; set; }

        public List<Corso> Corsi { get; set; }

        public LavoratoreCorsi()
        {
            this.Corsi = new List<Corso>();
        }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(LavoratoreCorsi))
            {
                if (this.Lavoratore != null && obj != null && ((LavoratoreCorsi) obj).Lavoratore != null)
                {
                    return this.Lavoratore.Cognome.CompareTo(((LavoratoreCorsi) obj).Lavoratore.Cognome);
                }
                else
                {
                    return 0;
                }
            }

            return 0;
        }
    }
}
