﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Corsi.Type.Entities.Esem
{
    public class TipoCorso : IComparable
    {
        public String Codice { get; set; }

        public String Descrizione { get; set; }


        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(TipoCorso))
            {
                return this.Descrizione.CompareTo(((TipoCorso)obj).Descrizione);
            }

            return 0;
        }
    }
}
