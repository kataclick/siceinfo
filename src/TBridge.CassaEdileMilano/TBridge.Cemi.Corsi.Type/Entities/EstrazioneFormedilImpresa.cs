﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class EstrazioneFormedilImpresa
    {
        public String Corso { get; set; }

        public String RagioneSociale { get; set; }

        public String CodiceFiscale { get; set; }

        public Int32? TipoContratto { get; set; }

        public Int32 TipoIscrizione { get; set; }

        public Int32? NumeroDipendenti { get; set; }

        public String Comune { get; set; }

        public String Provincia { get; set; }

        public Int32 NumeroLavoratori { get; set; }

        public String DateCorso { get; set; }
    }
}