using System;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class Lavoratore : LavoratoreAnagraficaComune
    {
        public Lavoratore()
            : base()
        {
            Fonte = FonteAnagraficheComuni.IscrizioneLavoratori;
        }

        public TipologiaLavoratore TipoLavoratore { get; set; }

        public Boolean Presente { get; set; }

        public Boolean Attestato { get; set; }

        public Boolean PrenotazioneImpresa { get; set; }

        public Boolean PrenotazioneConsulente { get; set; }

        public Int32 IdPartecipazione { get; set; }

        public Int32 IdPartecipazioneModulo { get; set; }

        public Impresa Impresa { get; set; }

        public Boolean RegistroConfermato { get; set; }

        public String Cittadinanza { get; set; }

        public String ProvinciaResidenza { get; set; }

        public TitoloDiStudio TitoloStudio { get; set; }

        public Int32 IdCorso { get; set; }

        public String CodiceCorso { get; set; }

        public String IndirizzoDenominazione { get; set; }

        public String IndirizzoComune { get; set; }

        public String IndirizzoProvincia { get; set; }

        public String IndirizzoCap { get; set; }

        public String IndirizzoCompleto
        {
            get
            {
                return String.Format("{0} {1} {2}",
                    this.IndirizzoDenominazione,
                    this.IndirizzoComune,
                    this.IndirizzoProvincia);
            }
        }

        public Boolean? PrimaEsperienza { get; set; }

        public DateTime? DataAssunzione { get; set; }

        public Int32 OreDenunciate { get; set; }

        public DateTime? DataInizioCorso { get; set; }

        public Boolean? Diritto { get; set; }

        public String Gestore { get; set; }

        public Int32 IdAnagraficaCondivisa { get; set; }

        public String CodiceCatastaleComuneNascita { get; set; }

        public String Telefono { get; set; }

        public String Cellulare { get; set; }

        public String Email { get; set; }
    }
}