﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class TitoloDiStudio
    {
        public Int32 IdTitoloStudio { get; set; }

        public String Descrizione { get; set; }
    }
}