﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    public class PrestazioneDTALavoratore
    {
        public Int32 Anno { get; set; }

        public Int32 IdLavoratore { get; set; }

        public Boolean Selezionato { get; set; }
    }
}
