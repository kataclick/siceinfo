﻿using System;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class PartecipazioneModulo
    {
        public Int32? IdPartecipazioneModulo { get; set; }

        public ProgrammazioneModulo Programmazione { get; set; }

        public DateTime? DataPianificazione
        {
            get
            {
                if (Programmazione == null)
                {
                    return null;
                }
                else
                {
                    return Programmazione.Schedulazione;
                }
            }
        }

        public Int32? IdCorso
        {
            get
            {
                if (Programmazione == null)
                {
                    return null;
                }
                else
                {
                    return Programmazione.Corso.IdCorso;
                }
            }
        }

        public Int32? IdLocazione
        {
            get
            {
                if (Programmazione == null)
                {
                    return null;
                }
                else
                {
                    return Programmazione.Locazione.IdLocazione;
                }
            }
        }

        public Boolean Presenza { get; set; }

        public Int32? IdPartecipazione { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }
    }
}