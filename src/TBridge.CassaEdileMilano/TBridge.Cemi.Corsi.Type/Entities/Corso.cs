﻿using System;
using TBridge.Cemi.Corsi.Type.Collections;

namespace TBridge.Cemi.Corsi.Type.Entities
{
    [Serializable]
    public class Corso
    {
        public Corso()
        {
            Moduli = new ModuloCollection();
        }

        public Int32? IdCorso { get; set; }

        public String Codice { get; set; }

        public String Descrizione { get; set; }

        public String Versione { get; set; }

        public Int16 DurataComplessiva { get; set; }

        public String NomeCompleto
        {
            get { return String.Format("{0} - {1}", Codice, Descrizione); }
        }

        public ModuloCollection Moduli { get; set; }

        public Int32 NumeroPianificazioni { get; set; }

        public Boolean IscrizioneLibera { get; set; }

        public override string ToString()
        {
            return String.Format("{0} - {1}", Codice, Descrizione);
        }

        public String ReportAttestato { get; set; }

        public String ReportAttestati { get; set; }
    }
}