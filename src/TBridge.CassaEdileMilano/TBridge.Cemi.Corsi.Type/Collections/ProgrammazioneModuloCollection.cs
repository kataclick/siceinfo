﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities;

namespace TBridge.Cemi.Corsi.Type.Collections
{
    [Serializable]
    public class ProgrammazioneModuloCollection : List<ProgrammazioneModulo>
    {
        public ProgrammazioneModulo GetProgrammazioneModulo(Int32 idProgrammazioneModulo)
        {
            foreach (ProgrammazioneModulo prog in this)
            {
                if (prog.IdProgrammazioneModulo.HasValue
                    && prog.IdProgrammazioneModulo.Value == idProgrammazioneModulo)
                {
                    return prog;
                }
            }

            return null;
        }

        public Boolean EsistonoIscritti()
        {
            foreach (ProgrammazioneModulo prog in this)
            {
                if (prog.Partecipanti > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public Boolean EsisteRegistroConfermato()
        {
            foreach (ProgrammazioneModulo prog in this)
            {
                if (prog.PresenzeConfermate)
                {
                    return true;
                }
            }

            return false;
        }
    }
}