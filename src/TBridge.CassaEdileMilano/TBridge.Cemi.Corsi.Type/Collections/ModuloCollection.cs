﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities;

namespace TBridge.Cemi.Corsi.Type.Collections
{
    [Serializable]
    public class ModuloCollection : List<Modulo>
    {
        public Int32 DurataComplessiva
        {
            get
            {
                Int32 durataComplessiva = 0;

                foreach (Modulo modulo in this)
                {
                    durataComplessiva += modulo.OreDurata;
                }

                return durataComplessiva;
            }
        }
    }
}