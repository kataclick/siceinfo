﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities;

namespace TBridge.Cemi.Corsi.Type.Collections
{
    [Serializable]
    public class EstrazioneFormedilLavoratoreCollection : List<EstrazioneFormedilLavoratore>
    {
    }
}