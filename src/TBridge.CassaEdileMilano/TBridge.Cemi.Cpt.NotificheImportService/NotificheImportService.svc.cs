﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using Elmah;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;

namespace TBridge.Cemi.Cpt.NotificheImportService
{
    public class NotificheImportService : INotificheImportService
    {
        #region INotificheImportService Members

        public Boolean ImportaNotifica(underground notificaXml)
        {
            Boolean res = false;

            try
            {
                // Conversione in notifica
                CptBusiness biz = new CptBusiness();
                NotificaTelematica notificaPreliminare = biz.ConvertiNotificaDaWebService(notificaXml);
                if (notificaPreliminare != null)
                {
                    if (!biz.EsisteNotificaRegione(notificaPreliminare.ProtocolloRegione))
                    {
                        // Inserimento 
                        if (biz.InserisciNotifica(notificaPreliminare))
                        {
                            res = true;
                        }
                        else
                        {
                            throw new Exception("Errore durante l'inserimento");
                        }
                    }
                    else
                    {
                        if (biz.NotificaRegioneDaAggiornare(notificaPreliminare.ProtocolloRegione, notificaPreliminare.Data))
                        {
                            // Aggiornamento
                            if (biz.UpdateNotifica(notificaPreliminare))
                            {
                                res = true;
                            }
                            else
                            {
                                throw new Exception("Errore durante l'aggiornamento");
                            }
                        }
                        else
                        {
                            res = true;
                        }
                    }
                }
                else
                {
                    throw new Exception("Non è riuscita la conversione della notifica");
                }
            }
            catch (Exception e)
            {
                Dictionary<String, String> config = new Dictionary<string, string>();
                config["connectionStringName"] = "ElmahConnection";
                config["applicationName"] = "WS ImportNotifiche";

                SqlErrorLog sqlErrorLog = new SqlErrorLog(config);
                Error err = new Error(e);

                if (notificaXml != null && notificaXml.NOTIFICA != null && notificaXml.NOTIFICA.Length == 1)
                {
                    undergroundNOTIFICA cantiere = notificaXml.NOTIFICA[0];
                    //e.Data.Add("ProtNotifica", cantiere.NR_NOTIFICA);
                    err.Message = String.Format("{0}: {1}", cantiere.NR_NOTIFICA, err.Message);
                }

                sqlErrorLog.Log(err);

                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione ImportaNotifica Strutturata: {0}", e.Message));
                }
            }

            return res;
        }

        public Boolean ImportaNotificaPlain(String plainNotificaXml)
        {
            Boolean res = false;

            try
            {
                if (String.IsNullOrWhiteSpace(plainNotificaXml))
                {
                    throw new ArgumentNullException("plainNotificaXml", "E' stata passata una stringa nulla o vuota");
                }

                StringReader sr = new StringReader(plainNotificaXml);

                //Tidy tidy = new Tidy();
                ///* Set the options you want */
                //tidy.Options.DocType = DocType.;
                //tidy.Options.DropFontTags = true;
                //tidy.Options.LogicalEmphasis = true;
                //tidy.Options.Xhtml = false;
                //tidy.Options.XmlOut = true;
                //tidy.Options.MakeClean = true;
                //tidy.Options.TidyMark = false;

                ///* Declare the parameters that is needed */
                //TidyMessageCollection tmc = new TidyMessageCollection();
                //MemoryStream input = new MemoryStream();
                //MemoryStream output = new MemoryStream();

                //Byte[] byteArray = Encoding.UTF8.GetBytes(plainNotificaXml);
                //input.Write(byteArray, 0, byteArray.Length);
                //input.Position = 0;
                //tidy.Parse(input, output, tmc);

                //String result = Encoding.UTF8.GetString(output.ToArray());

                // Deserializzazione dell'input
                underground notifica = null;
                XmlSerializer serializer = new XmlSerializer(typeof(underground));
                notifica = (underground) serializer.Deserialize(sr);

                return ImportaNotifica(notifica);
            }
            catch (Exception e)
            {
                String protNotifica = String.Empty;
                Dictionary<String, String> config = new Dictionary<string, string>();
                config["connectionStringName"] = "ElmahConnection";
                config["applicationName"] = "WS ImportNotifiche";

                SqlErrorLog sqlErrorLog = new SqlErrorLog(config);
                Error err = new Error(e);

                if (!String.IsNullOrWhiteSpace(plainNotificaXml))
                {
                    // <NR_NOTIFICA>57099/2012</NR_NOTIFICA>

                    try
                    {
                        protNotifica = ExtractString(plainNotificaXml, "NR_NOTIFICA");
                        //e.Data.Add("ProtNotifica", protNotifica);
                        err.Message = String.Format("{0}: {1}", protNotifica, err.Message);
                    }
                    catch
                    {
                        err.Message = String.Format("Prot. non presente: {0}", err.Message);
                    }
                }

                sqlErrorLog.Log(err);

                try
                {
                    Error errNoti = new Error(new Exception(plainNotificaXml));
                    sqlErrorLog.Log(errNoti);
                }
                catch { }

                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione ImportaNotifica Plain: {0}", e.Message));
                }
            }

            return res;
        }

        string ExtractString(string s, string tag)
        {
            // You should check for errors in real-world code, omitted for brevity
            var startTag = "<" + tag + ">";
            int startIndex = s.IndexOf(startTag) + startTag.Length;
            int endIndex = s.IndexOf("</" + tag + ">", startIndex);
            return s.Substring(startIndex, endIndex - startIndex);
        }

        public Boolean ImportaNotificaListPlain(List<String> plainNotificaListXml)
        {
            Boolean res = false;

            try
            {
                foreach (String notifica in plainNotificaListXml)
                {
                    ImportaNotificaPlain(notifica);
                }
            }
            catch
            {
            }

            return res;
        }

        public Boolean ImportaNotificaList(List<underground> notificaListXml)
        {
            Boolean res = false;

            try
            {
                foreach (underground notifica in notificaListXml)
                {
                    ImportaNotifica(notifica);
                }
            }
            catch
            {
            }

            return res;
        }

        #endregion
    }
}