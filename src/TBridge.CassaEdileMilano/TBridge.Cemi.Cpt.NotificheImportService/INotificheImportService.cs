﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TBridge.Cemi.Cpt.NotificheImportService
{
    [ServiceContract]
    public interface INotificheImportService
    {

        [OperationContract]
        Boolean ImportaNotificaPlain(String plainNotificaXml);

        [OperationContract]
        Boolean ImportaNotifica(underground notificaXml);

        [OperationContract]
        Boolean ImportaNotificaListPlain(List<String> plainNotificaXml);

        [OperationContract]
        Boolean ImportaNotificaList(List<underground> notificaXml);
    }
}
