using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Archidoc.Type.Collection;
using TBridge.Cemi.Archidoc.Type.Entities;
using TBridge.Cemi.Archidoc.Type.Exceptions;
using System.IO;

namespace TBridge.Cemi.Archidoc.Data
{
    public class ArchidocDataAccess
    {
        public ArchidocDataAccess()
        {
            DatabaseSice = DatabaseFactory.CreateDatabase("CEMI");
        }

        #region Documenti archidoc select insert metodi

        #region Select

        #region MalattiaTelematica
        public MalattiaTelematicaCollection GetMalattiaTelematicaCertificatiMedici(DateTime? dataDa, DateTime? dataA)
        {
            MalattiaTelematicaCollection certificati = new MalattiaTelematicaCollection();
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCertificatiMediciSelecByDate"))
            {

                DatabaseSice.AddInParameter(command, "@dateFrom", DbType.DateTime, dataDa);
                DatabaseSice.AddInParameter(command, "@dateTo", DbType.DateTime, dataA);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        #region Index
                        int indexIdMalattiaTelematica = reader.GetOrdinal("idMalattiaTelematicaCertificatoMedico");
                        int indexIdLavoratore = reader.GetOrdinal("idLavoratore");
                        int indexNomeLavoratore = reader.GetOrdinal("nome");
                        int indexCognomeLavoratore = reader.GetOrdinal("cognome");
                        int indexDataNescitaLavoratore = reader.GetOrdinal("dataNascita");
                        int indexCodiceFiscaleLavoratore = reader.GetOrdinal("codiceFiscale");
                        int indexIdImpresa = reader.GetOrdinal("idImpresa");
                        int indexImmagine = reader.GetOrdinal("immagine");
                        int indexNomeFile = reader.GetOrdinal("nomeFile");
                        int indexDataInsRecord = reader.GetOrdinal("dataInserimentoRecord");
                        #endregion


                        while (reader.Read())
                        {

                            MalattiaTelematica certificato = new MalattiaTelematica();
                            certificato.Tipo = "Certificato Medico";
                            certificato.IdMalattiaTelematica = reader.GetInt32(indexIdMalattiaTelematica);
                            certificato.IdLavoratore = reader.GetInt32(indexIdLavoratore);
                            certificato.IdImpresa = reader.GetInt32(indexIdImpresa);
                            certificato.Nome = reader.IsDBNull(indexNomeLavoratore) ? null : reader.GetString(indexNomeLavoratore);
                            certificato.Cognome = reader.IsDBNull(indexCognomeLavoratore) ? null : reader.GetString(indexCognomeLavoratore);
                            certificato.CodiceFiscale = reader.IsDBNull(indexCodiceFiscaleLavoratore) ? null : reader.GetString(indexCodiceFiscaleLavoratore);

                            certificato.DataNascita = reader.IsDBNull(indexDataNescitaLavoratore) ? (DateTime?) null : reader.GetDateTime(indexDataNescitaLavoratore);

                            certificato.DataInserimentoRecord = reader.IsDBNull(indexDataInsRecord)
                                                                    ? (DateTime?) null
                                                                    : reader.GetDateTime(indexDataInsRecord);

                            certificato.FileNome = !reader.IsDBNull(indexNomeFile)
                                                        ? reader.GetString(indexNomeFile)
                                                        : certificato.FileNome = String.Format("Certificato_Medico_{0}", certificato.IdMalattiaTelematica);

                            byte[] immagine = new byte[reader.GetBytes(indexImmagine, 0, null, 0, 0)];
                            reader.GetBytes(indexImmagine, 0, immagine, 0, immagine.Length);
                            certificato.FileByteArray = immagine;


                            certificati.Add(certificato);
                        }
                    }
                }
            }
            return certificati;
        }

        public MalattiaTelematicaCollection GetMalattiaTelematicaAltriDocumenti(DateTime? dataDa, DateTime? dataA)
        {

            MalattiaTelematicaCollection certificati = new MalattiaTelematicaCollection();
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_MalattiaTelematicaAltriDocumentiSelecByDate"))
            {

                DatabaseSice.AddInParameter(command, "@dateFrom", DbType.DateTime, dataDa);
                DatabaseSice.AddInParameter(command, "@dateTo", DbType.DateTime, dataA);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        #region Index
                        int indexIdMalattiaTelematica = reader.GetOrdinal("idMalattiaTelematicaAltroDocumento");
                        int indexDescrizione = reader.GetOrdinal("descrizione");
                        int indexImmagine = reader.GetOrdinal("immagine");
                        int indexNomeFile = reader.GetOrdinal("nomeFile");
                        int indexIdLavoratore = reader.GetOrdinal("idLavoratore");
                        int indexNomeLavoratore = reader.GetOrdinal("nome");
                        int indexCognomeLavoratore = reader.GetOrdinal("cognome");
                        int indexDataNescitaLavoratore = reader.GetOrdinal("dataNascita");
                        int indexCodiceFiscaleLavoratore = reader.GetOrdinal("codiceFiscale");
                        int indexDataInsRecord = reader.GetOrdinal("dataInserimentoRecord");
                        #endregion


                        while (reader.Read())
                        {

                            MalattiaTelematica certificato = new MalattiaTelematica();
                            certificato.Tipo = String.Format("Atro Documento {0}", reader.IsDBNull(indexDescrizione) ? null : reader.GetString(indexDescrizione));
                            certificato.IdMalattiaTelematica = reader.GetInt32(indexIdMalattiaTelematica);

                            certificato.DataInserimentoRecord = reader.IsDBNull(indexDataInsRecord)
                                                                    ? (DateTime?) null
                                                                    : reader.GetDateTime(indexDataInsRecord);

                            certificato.FileNome = !reader.IsDBNull(indexNomeFile)
                                                        ? reader.GetString(indexNomeFile)
                                                        : certificato.FileNome = String.Format("Altro_Documento_{0}", certificato.IdMalattiaTelematica);

                            certificato.IdLavoratore = reader.GetInt32(indexIdLavoratore);
                            certificato.Nome = reader.IsDBNull(indexNomeLavoratore) ? null : reader.GetString(indexNomeLavoratore);
                            certificato.Cognome = reader.IsDBNull(indexCognomeLavoratore) ? null : reader.GetString(indexCognomeLavoratore);
                            certificato.CodiceFiscale = reader.IsDBNull(indexCodiceFiscaleLavoratore) ? null : reader.GetString(indexCodiceFiscaleLavoratore);
                            certificato.DataNascita = reader.IsDBNull(indexDataNescitaLavoratore) ? (DateTime?) null : reader.GetDateTime(indexDataNescitaLavoratore);

                            byte[] immagine = new byte[reader.GetBytes(indexImmagine, 0, null, 0, 0)];
                            reader.GetBytes(indexImmagine, 0, immagine, 0, immagine.Length);
                            certificato.FileByteArray = immagine;


                            certificati.Add(certificato);
                        }
                    }
                }
            }
            return certificati;
        }
        #endregion

        #region Prestazioni

        public CertificatoCollection GetCertificati(DateTime? dataModificaRecord)
        {
            CertificatoCollection certificati = new CertificatoCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiCertificatiSelect"))
            {
                if (dataModificaRecord.HasValue)
                    DatabaseSice.AddInParameter(command, "@dataModificaRecord", DbType.DateTime, dataModificaRecord);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int idDocumentoIndex = reader.GetOrdinal("idDocumentoCertificato");

                        int idArchidocIndex = reader.GetOrdinal("idArchidoc");

                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = reader.GetOrdinal("idFamiliare");

                        int cognomeIndex = reader.GetOrdinal("cognome");
                        int nomeIndex = reader.GetOrdinal("nome");
                        int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                        int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");

                        int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                        int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                        int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                        int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");

                        int idTipoPrestazioneIndex = reader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = reader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = reader.GetOrdinal("numeroProtocolloPrestazione");
                        int idPrestazioniDomandaIndex = reader.GetOrdinal("idPrestazioniDomanda");
                        int idCertificatoArchidocIndex = reader.GetOrdinal("idTipoCertificatoArchidoc");
                        int originaleIndex = reader.GetOrdinal("originale");


                        while (reader.Read())
                        {
                            //if (!reader.IsDBNull(dataModificaRecordIndex))
                            //{
                            Certificato certificato = new Certificato();

                            certificato.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                            ? null
                                                            : reader.GetString(codiceFiscaleIndex);
                            certificato.Cognome = reader.IsDBNull(cognomeIndex)
                                                      ? null
                                                      : reader.GetString(cognomeIndex);
                            certificato.DataInserimentoRecord = reader.IsDBNull(dataInserimentoRecordIndex)
                                                                    ? (DateTime?) null
                                                                    : reader.GetDateTime(dataInserimentoRecordIndex);
                            certificato.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                                 ? (DateTime?) null
                                                                 : reader.GetDateTime(dataModificaRecordIndex);
                            certificato.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                          ? (DateTime?) null
                                                          : reader.GetDateTime(dataNascitaIndex);
                            certificato.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                            ? (DateTime?) null
                                                            : reader.GetDateTime(dataScansioneIndex);
                            certificato.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                         ? null
                                                         : reader.GetString(idArchidocIndex);
                            certificato.IdTipoCertificatoArchidoc = reader.IsDBNull(idCertificatoArchidocIndex)
                                                                    ? (int?) null
                                                                    : reader.GetInt32(idCertificatoArchidocIndex);
                            certificato.IdDocumento = reader.IsDBNull(idDocumentoIndex)
                                                          ? (int?) null
                                                          : reader.GetInt32(idDocumentoIndex);
                            certificato.IdFamiliare = reader.IsDBNull(idFamiliareIndex)
                                                          ? (int?) null
                                                          : reader.GetInt32(idFamiliareIndex);
                            certificato.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                           ? (int?) null
                                                           : reader.GetInt32(idLavoratoreIndex);
                            certificato.IdPrestazioniDomanda = reader.IsDBNull(idPrestazioniDomandaIndex)
                                                                   ? (int?) null
                                                                   : reader.GetInt32(idPrestazioniDomandaIndex);
                            certificato.IdTipoPrestazione = reader.IsDBNull(idTipoPrestazioneIndex)
                                                                ? null
                                                                : reader.GetString(idTipoPrestazioneIndex);
                            certificato.Nome = reader.IsDBNull(nomeIndex) ? null : reader.GetString(nomeIndex);
                            certificato.NumeroProtocolloPrestazione =
                                reader.IsDBNull(numeroProtocolloPrestazioneIndex)
                                    ? (int?) null
                                    : reader.GetInt32(numeroProtocolloPrestazioneIndex);
                            certificato.Originale = reader.IsDBNull(originaleIndex)
                                                        ? (bool?) null
                                                        : reader.GetBoolean(originaleIndex);
                            certificato.ProtocolloPrestazione = reader.IsDBNull(protocolloPrestazioneIndex)
                                                                    ? (int?) null
                                                                    : reader.GetInt32(protocolloPrestazioneIndex);
                            certificato.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                                  ? null
                                                                  : reader.GetString(protocolloScansioneIndex);

                            certificati.Add(certificato);
                            //}
                        }
                    }
                }
            }

            return certificati;
        }
        public CodiceFiscaleCollection GetCodiceFiscaleCollection(DateTime? dataModificaRecord)
        {
            CodiceFiscaleCollection codiceFiscaleCollection = new CodiceFiscaleCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiCodiceFiscaleSelect"))
            {
                if (dataModificaRecord.HasValue)
                    DatabaseSice.AddInParameter(command, "@dataModificaRecord", DbType.DateTime, dataModificaRecord);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int idDocumentoIndex = reader.GetOrdinal("idDocumentoCodiceFiscale");

                        int idArchidocIndex = reader.GetOrdinal("idArchidoc");

                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = reader.GetOrdinal("idFamiliare");

                        int cognomeIndex = reader.GetOrdinal("cognome");
                        int nomeIndex = reader.GetOrdinal("nome");
                        int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                        int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");

                        int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                        int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                        int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                        int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");
                        int indexIdPrestazione = reader.GetOrdinal("idPrestazioniDomanda");

                        while (reader.Read())
                        {
                            //if (!reader.IsDBNull(dataModificaRecordIndex))
                            //{
                            CodiceFiscale codiceFiscale = new CodiceFiscale();

                            codiceFiscale.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                              ? null
                                                              : reader.GetString(codiceFiscaleIndex);
                            codiceFiscale.Cognome = reader.IsDBNull(cognomeIndex)
                                                        ? null
                                                        : reader.GetString(cognomeIndex);
                            codiceFiscale.DataInserimentoRecord = reader.IsDBNull(dataInserimentoRecordIndex)
                                                                      ? (DateTime?) null
                                                                      : reader.GetDateTime(
                                                                            dataInserimentoRecordIndex);
                            codiceFiscale.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                                   ? (DateTime?) null
                                                                   : reader.GetDateTime(dataModificaRecordIndex);
                            codiceFiscale.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                            ? (DateTime?) null
                                                            : reader.GetDateTime(dataNascitaIndex);
                            codiceFiscale.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                              ? (DateTime?) null
                                                              : reader.GetDateTime(dataScansioneIndex);
                            codiceFiscale.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                           ? null
                                                           : reader.GetString(idArchidocIndex);
                            codiceFiscale.IdDocumento = reader.IsDBNull(idDocumentoIndex)
                                                            ? (int?) null
                                                            : reader.GetInt32(idDocumentoIndex);
                            codiceFiscale.IdFamiliare = reader.IsDBNull(idFamiliareIndex)
                                                            ? (int?) null
                                                            : reader.GetInt32(idFamiliareIndex);
                            codiceFiscale.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                             ? (int?) null
                                                             : reader.GetInt32(idLavoratoreIndex);
                            codiceFiscale.Nome = reader.IsDBNull(nomeIndex) ? null : reader.GetString(nomeIndex);
                            codiceFiscale.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                                    ? null
                                                                    : reader.GetString(protocolloScansioneIndex);
                            codiceFiscale.IdPrestazioniDomanda = reader.IsDBNull(indexIdPrestazione) ? (int?) null : reader.GetInt32(indexIdPrestazione);

                            codiceFiscaleCollection.Add(codiceFiscale);
                            //}
                        }
                    }
                }
            }

            return codiceFiscaleCollection;
        }
        public StatoFamigliaCollection GetStatoFamigliaCollection(DateTime? dataModificaRecord)
        {
            StatoFamigliaCollection statoFamigliaCollection = new StatoFamigliaCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiStatoFamigliaSelect"))
            {
                if (dataModificaRecord.HasValue)
                    DatabaseSice.AddInParameter(command, "@dataModificaRecord", DbType.DateTime, dataModificaRecord);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int idDocumentoIndex = reader.GetOrdinal("idDocumentoStatoFamiglia");
                        ;
                        int idArchidocIndex = reader.GetOrdinal("idArchidoc");

                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");

                        int cognomeIndex = reader.GetOrdinal("cognome");
                        int nomeIndex = reader.GetOrdinal("nome");
                        int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                        int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");

                        int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                        int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                        int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                        int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");
                        int indexIdPrestazione = reader.GetOrdinal("idPrestazioniDomanda");

                        while (reader.Read())
                        {
                            //if (!reader.IsDBNull(dataModificaRecordIndex))
                            //{
                            StatoFamiglia statoFamiglia = new StatoFamiglia();

                            statoFamiglia.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                              ? null
                                                              : reader.GetString(codiceFiscaleIndex);
                            statoFamiglia.Cognome = reader.IsDBNull(cognomeIndex)
                                                        ? null
                                                        : reader.GetString(cognomeIndex);
                            statoFamiglia.DataInserimentoRecord = reader.IsDBNull(dataInserimentoRecordIndex)
                                                                      ? (DateTime?) null
                                                                      : reader.GetDateTime(
                                                                            dataInserimentoRecordIndex);
                            statoFamiglia.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                                   ? (DateTime?) null
                                                                   : reader.GetDateTime(dataModificaRecordIndex);
                            statoFamiglia.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                            ? (DateTime?) null
                                                            : reader.GetDateTime(dataNascitaIndex);
                            statoFamiglia.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                              ? (DateTime?) null
                                                              : reader.GetDateTime(dataScansioneIndex);
                            statoFamiglia.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                           ? null
                                                           : reader.GetString(idArchidocIndex);
                            statoFamiglia.IdDocumento = reader.IsDBNull(idDocumentoIndex)
                                                            ? (int?) null
                                                            : reader.GetInt32(idDocumentoIndex);
                            statoFamiglia.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                             ? (int?) null
                                                             : reader.GetInt32(idLavoratoreIndex);
                            statoFamiglia.Nome = reader.IsDBNull(nomeIndex) ? null : reader.GetString(nomeIndex);
                            statoFamiglia.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                                    ? null
                                                                    : reader.GetString(protocolloScansioneIndex);
                            statoFamiglia.IdPrestazioniDomanda = reader.IsDBNull(indexIdPrestazione) ? (int?) null : reader.GetInt32(indexIdPrestazione);

                            statoFamigliaCollection.Add(statoFamiglia);
                            //}
                        }
                    }
                }
            }

            return statoFamigliaCollection;
        }
        public FatturaCollection GetPrestazioniFatture(DateTime? dataModificaRecord)
        {
            FatturaCollection fatturaCollection = new FatturaCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_PrestazioniFattureSelect"))
            {
                if (dataModificaRecord.HasValue)
                    DatabaseSice.AddInParameter(command, "@dataModificaRecord", DbType.DateTime, dataModificaRecord);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int idPrestazioniFatturaIndex = reader.GetOrdinal("idPrestazioniFattura");
                        int idArchidocIndex = reader.GetOrdinal("idArchidoc");
                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int cognomeIndex = reader.GetOrdinal("cognome");
                        int nomeIndex = reader.GetOrdinal("nome");
                        int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                        int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                        int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");
                        int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                        int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                        int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");
                        int idTipoPrestazioneIndex = reader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = reader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = reader.GetOrdinal("numeroProtocolloPrestazione");
                        int originaleIndex = reader.GetOrdinal("originale");
                        int validaIndex = reader.GetOrdinal("valida");
                        int totaleIndex = reader.GetOrdinal("totale");
                        int numeroIndex = reader.GetOrdinal("numero");
                        int idDocumentoIndex = reader.GetOrdinal("idPrestazioniFattura");
                        int dataValidazioneIndex = reader.GetOrdinal("dataValidazione");
                        int dataAssociazioneIndex = reader.GetOrdinal("dataAssociazione");
                        int dataAggiornamentoArchidocIndex = reader.GetOrdinal("dataAggiornamentoArchidoc");
                        int dataIndex = reader.GetOrdinal("data");
                        int indexIdPrestazione = reader.GetOrdinal("idPrestazioniDomanda");
                        int indexIdPrestazioniFatturaDichiarata = reader.GetOrdinal("idPrestazioniFatturaDichiarata");

                        while (reader.Read())
                        {
                            //if (!reader.IsDBNull(dataModificaRecordIndex))
                            //{
                            Fattura fattura = new Fattura();

                            fattura.IdPrestazioniFattura = reader.IsDBNull(idPrestazioniFatturaIndex)
                                                               ? (int?) null
                                                               : reader.GetInt32(idPrestazioniFatturaIndex);
                            fattura.Cognome = reader.IsDBNull(cognomeIndex) ? null : reader.GetString(cognomeIndex);
                            fattura.DataInserimentoRecord = reader.IsDBNull(dataInserimentoRecordIndex)
                                                                ? (DateTime?) null
                                                                : reader.GetDateTime(dataInserimentoRecordIndex);
                            fattura.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                             ? (DateTime?) null
                                                             : reader.GetDateTime(dataModificaRecordIndex);
                            fattura.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                        ? (DateTime?) null
                                                        : reader.GetDateTime(dataScansioneIndex);
                            fattura.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                     ? null
                                                     : reader.GetString(idArchidocIndex);
                            fattura.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                       ? (int?) null
                                                       : reader.GetInt32(idLavoratoreIndex);
                            fattura.IdTipoPrestazione = reader.IsDBNull(idTipoPrestazioneIndex)
                                                            ? null
                                                            : reader.GetString(idTipoPrestazioneIndex);
                            fattura.Nome = reader.IsDBNull(nomeIndex) ? null : reader.GetString(nomeIndex);
                            fattura.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                        ? null
                                                        : reader.GetString(codiceFiscaleIndex);
                            fattura.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                      ? (DateTime?) null
                                                      : reader.GetDateTime(dataNascitaIndex);
                            fattura.NumeroProtocolloPrestazione = reader.IsDBNull(numeroProtocolloPrestazioneIndex)
                                                                      ? (int?) null
                                                                      : reader.GetInt32(
                                                                            numeroProtocolloPrestazioneIndex);
                            fattura.Originale = reader.IsDBNull(originaleIndex)
                                                    ? (bool?) null
                                                    : reader.GetBoolean(originaleIndex);
                            fattura.ProtocolloPrestazione = reader.IsDBNull(protocolloPrestazioneIndex)
                                                                ? (int?) null
                                                                : reader.GetInt32(protocolloPrestazioneIndex);
                            fattura.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                              ? null
                                                              : reader.GetString(protocolloScansioneIndex);
                            fattura.Valida = reader.IsDBNull(validaIndex)
                                                 ? (bool?) null
                                                 : reader.GetBoolean(validaIndex);
                            fattura.Totale = reader.IsDBNull(totaleIndex)
                                                 ? (decimal?) null
                                                 : reader.GetDecimal(totaleIndex);
                            fattura.Numero = reader.IsDBNull(numeroIndex) ? null : reader.GetString(numeroIndex);
                            fattura.IdPrestazioniFattura = reader.IsDBNull(idDocumentoIndex)
                                                               ? (int?) null
                                                               : reader.GetInt32(idDocumentoIndex);
                            fattura.DataValidazione = reader.IsDBNull(dataValidazioneIndex)
                                                          ? (DateTime?) null
                                                          : reader.GetDateTime(dataValidazioneIndex);
                            fattura.DataAssociazione = reader.IsDBNull(dataAssociazioneIndex)
                                                           ? (DateTime?) null
                                                           : reader.GetDateTime(dataAssociazioneIndex);
                            fattura.DataAggiornamentoArchidoc = reader.IsDBNull(dataAggiornamentoArchidocIndex)
                                                                    ? (DateTime?) null
                                                                    : reader.GetDateTime(
                                                                          dataAggiornamentoArchidocIndex);
                            fattura.Data = reader.IsDBNull(dataIndex)
                                               ? (DateTime?) null
                                               : reader.GetDateTime(dataIndex);

                            fattura.IdPrestazioniDomanda = reader.IsDBNull(indexIdPrestazione) ? (int?) null : reader.GetInt32(indexIdPrestazione);
                            fattura.IdPrestazioniFatturaDichiarata = reader.IsDBNull(indexIdPrestazioniFatturaDichiarata) ? (int?) null : reader.GetInt32(indexIdPrestazioniFatturaDichiarata);

                            fatturaCollection.Add(fattura);
                            //}
                        }
                    }
                }
            }

            return fatturaCollection;
        }
        public DichiarazioneInterventoCollection GetDichiarazioneInterventoCollection(DateTime? dataModificaRecord)
        {
            DichiarazioneInterventoCollection dichiarazioneInterventoCollection =
                new DichiarazioneInterventoCollection();

            using (
                DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDichiarazioniInterventoSelect"))
            {
                if (dataModificaRecord.HasValue)
                    DatabaseSice.AddInParameter(command, "@dataModificaRecord", DbType.DateTime, dataModificaRecord);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int idDocumentoIndex = reader.GetOrdinal("idDocumentoDichiarazioneIntervento");
                        ;
                        int idArchidocIndex = reader.GetOrdinal("idArchidoc");

                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = reader.GetOrdinal("idFamiliare");

                        int cognomeIndex = reader.GetOrdinal("cognome");
                        int nomeIndex = reader.GetOrdinal("nome");
                        int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                        int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");

                        int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                        int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                        int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                        int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");

                        int idTipoPrestazioneIndex = reader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = reader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = reader.GetOrdinal("numeroProtocolloPrestazione");
                        int idPrestazioniDomandaIndex = reader.GetOrdinal("idPrestazioniDomanda");

                        while (reader.Read())
                        {
                            //if (!reader.IsDBNull(dataModificaRecordIndex))
                            //{
                            DichiarazioneIntervento dichiarazioneIntervento = new DichiarazioneIntervento();

                            dichiarazioneIntervento.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                                        ? null
                                                                        : reader.GetString(codiceFiscaleIndex);
                            dichiarazioneIntervento.Cognome = reader.IsDBNull(cognomeIndex)
                                                                  ? null
                                                                  : reader.GetString(cognomeIndex);
                            dichiarazioneIntervento.DataInserimentoRecord =
                                reader.IsDBNull(dataInserimentoRecordIndex)
                                    ? (DateTime?) null
                                    : reader.GetDateTime(dataInserimentoRecordIndex);
                            dichiarazioneIntervento.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                                             ? (DateTime?) null
                                                                             : reader.GetDateTime(
                                                                                   dataModificaRecordIndex);
                            dichiarazioneIntervento.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                                      ? (DateTime?) null
                                                                      : reader.GetDateTime(dataNascitaIndex);
                            dichiarazioneIntervento.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                                        ? (DateTime?) null
                                                                        : reader.GetDateTime(dataScansioneIndex);
                            dichiarazioneIntervento.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                                     ? null
                                                                     : reader.GetString(idArchidocIndex);
                            dichiarazioneIntervento.IdDocumento = reader.IsDBNull(idDocumentoIndex)
                                                                      ? (int?) null
                                                                      : reader.GetInt32(idDocumentoIndex);
                            dichiarazioneIntervento.IdFamiliare = reader.IsDBNull(idFamiliareIndex)
                                                                      ? (int?) null
                                                                      : reader.GetInt32(idFamiliareIndex);
                            dichiarazioneIntervento.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                                       ? (int?) null
                                                                       : reader.GetInt32(idLavoratoreIndex);
                            dichiarazioneIntervento.IdPrestazioniDomanda = reader.IsDBNull(idPrestazioniDomandaIndex)
                                                                               ? (int?) null
                                                                               : reader.GetInt32(
                                                                                     idPrestazioniDomandaIndex);
                            dichiarazioneIntervento.IdTipoPrestazione = reader.IsDBNull(idTipoPrestazioneIndex)
                                                                            ? null
                                                                            : reader.GetString(
                                                                                  idTipoPrestazioneIndex);
                            dichiarazioneIntervento.Nome = reader.IsDBNull(nomeIndex)
                                                               ? null
                                                               : reader.GetString(nomeIndex);
                            dichiarazioneIntervento.NumeroProtocolloPrestazione =
                                reader.IsDBNull(numeroProtocolloPrestazioneIndex)
                                    ? (int?) null
                                    : reader.GetInt32(numeroProtocolloPrestazioneIndex);
                            dichiarazioneIntervento.ProtocolloPrestazione =
                                reader.IsDBNull(protocolloPrestazioneIndex)
                                    ? (int?) null
                                    : reader.GetInt32(protocolloPrestazioneIndex);
                            dichiarazioneIntervento.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                                              ? null
                                                                              : reader.GetString(
                                                                                    protocolloScansioneIndex);


                            dichiarazioneInterventoCollection.Add(dichiarazioneIntervento);
                            //}
                        }
                    }
                }
            }

            return dichiarazioneInterventoCollection;
        }
        public ModuloCollection GetModuloPrestazioneCollection(DateTime? dataModificaRecord)
        {
            ModuloCollection moduloCollection = new ModuloCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiModuloPrestazioniSelect"))
            {
                if (dataModificaRecord.HasValue)
                    DatabaseSice.AddInParameter(command, "@dataModificaRecord", DbType.DateTime, dataModificaRecord);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int idDocumentoIndex = reader.GetOrdinal("idDocumentoModuloPrestazioni");
                        ;
                        int idArchidocIndex = reader.GetOrdinal("idArchidoc");

                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = reader.GetOrdinal("idFamiliare");

                        int cognomeIndex = reader.GetOrdinal("cognome");
                        int nomeIndex = reader.GetOrdinal("nome");
                        int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                        int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");

                        int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                        int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                        int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                        int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");

                        int idTipoPrestazioneIndex = reader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = reader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = reader.GetOrdinal("numeroProtocolloPrestazione");
                        int idPrestazioniDomandaIndex = reader.GetOrdinal("idPrestazioniDomanda");
                        int dataAggiornamentoArchidocIndex = reader.GetOrdinal("dataAggiornamentoArchidoc");
                        int originaleIndex = reader.GetOrdinal("originale");



                        while (reader.Read())
                        {
                            //gi� nelle SP vengono estratti i record che vogliamo, non c'� bisogno di un ulteriore filtro.
                            //if (!reader.IsDBNull(dataModificaRecordIndex))
                            //{
                            Modulo modulo = new Modulo();

                            modulo.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                                  ? null
                                                                  : reader.GetString(codiceFiscaleIndex);
                            modulo.Cognome = reader.IsDBNull(cognomeIndex)
                                                            ? null
                                                            : reader.GetString(cognomeIndex);
                            modulo.DataInserimentoRecord = reader.IsDBNull(dataInserimentoRecordIndex)
                                                                          ? (DateTime?) null
                                                                          : reader.GetDateTime(
                                                                                dataInserimentoRecordIndex);
                            modulo.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                                       ? (DateTime?) null
                                                                       : reader.GetDateTime(dataModificaRecordIndex);
                            modulo.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                                ? (DateTime?) null
                                                                : reader.GetDateTime(dataNascitaIndex);
                            modulo.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                                  ? (DateTime?) null
                                                                  : reader.GetDateTime(dataScansioneIndex);
                            modulo.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                               ? null
                                                               : reader.GetString(idArchidocIndex);
                            modulo.IdDocumento = reader.IsDBNull(idDocumentoIndex)
                                                                ? (int?) null
                                                                : reader.GetInt32(idDocumentoIndex);
                            modulo.IdFamiliare = reader.IsDBNull(idFamiliareIndex)
                                                                ? (int?) null
                                                                : reader.GetInt32(idFamiliareIndex);
                            modulo.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                                 ? (int?) null
                                                                 : reader.GetInt32(idLavoratoreIndex);
                            modulo.IdPrestazioniDomanda = reader.IsDBNull(idPrestazioniDomandaIndex)
                                                                         ? (int?) null
                                                                         : reader.GetInt32(idPrestazioniDomandaIndex);
                            modulo.IdTipoPrestazione = reader.IsDBNull(idTipoPrestazioneIndex)
                                                                      ? null
                                                                      : reader.GetString(idTipoPrestazioneIndex);
                            modulo.Nome = reader.IsDBNull(nomeIndex) ? null : reader.GetString(nomeIndex);
                            modulo.NumeroProtocolloPrestazione =
                                reader.IsDBNull(numeroProtocolloPrestazioneIndex)
                                    ? (int?) null
                                    : reader.GetInt32(numeroProtocolloPrestazioneIndex);
                            modulo.Originale = reader.IsDBNull(originaleIndex)
                                                              ? (bool?) null
                                                              : reader.GetBoolean(originaleIndex);
                            modulo.ProtocolloPrestazione = reader.IsDBNull(protocolloPrestazioneIndex)
                                                                          ? (int?) null
                                                                          : reader.GetInt32(
                                                                                protocolloPrestazioneIndex);
                            modulo.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                                        ? null
                                                                        : reader.GetString(protocolloScansioneIndex);
                            modulo.DataAggiornamentoArchidoc =
                                reader.IsDBNull(dataAggiornamentoArchidocIndex)
                                    ? (DateTime?) null
                                    : reader.GetDateTime(dataAggiornamentoArchidocIndex);

                            moduloCollection.Add(modulo);
                            //}
                        }
                    }
                }
            }

            return moduloCollection;
        }
        #endregion

        #region Variazioni anagrafiche imprese

        public LibroUnicoCollection GetDocumentiSospensioneImpresaLibroUnico()
        {
            LibroUnicoCollection documentiSospensioneLibroUnico = new LibroUnicoCollection();
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseRichiesteVariazioneStatoGetDocumentiLibroUnico"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        #region Index
                        int indexIdRichiesta = reader.GetOrdinal("idRichiesta");
                        int indexIdImpresa = reader.GetOrdinal("idImpresa");
                        int indexAllegatoNome = reader.GetOrdinal("allegatoNome");
                        int indexAllegato = reader.GetOrdinal("allegato");
                        int indexRagioneSociale = reader.GetOrdinal("ragioneSociale");
                        int indexCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                        int indexPartitaIVA = reader.GetOrdinal("partitaIVA");
                        #endregion

                        while (reader.Read())
                        {
                            LibroUnico sosp = new LibroUnico();
                            sosp.Tipo = "Libro Unico";
                            sosp.IdRichiesta = reader.GetInt32(indexIdRichiesta);
                            sosp.IdImpresa = reader.GetInt32(indexIdImpresa);
                            sosp.RagioneSocialeImpresa = reader.GetString(indexRagioneSociale);
                            sosp.CodiceFiscaleIVAImpresa = reader.IsDBNull(indexCodiceFiscale) ? reader.GetString(indexPartitaIVA) : reader.GetString(indexCodiceFiscale);
                            sosp.AllegatoNome = reader.GetString(indexAllegatoNome);
                            byte[] immagine = new byte[reader.GetBytes(indexAllegato, 0, null, 0, 0)];
                            reader.GetBytes(indexAllegato, 0, immagine, 0, immagine.Length);
                            sosp.FileByteArray = immagine;
                            sosp.FileNome =
                                $"Libro_Unico_{sosp.IdRichiesta}{Path.GetExtension(reader.GetString(indexAllegatoNome))}";
                            sosp.FileEstensione = Path.GetExtension(sosp.AllegatoNome).Substring(1);
                            sosp.DataInserimentoRecord = DateTime.Now.Date;

                            documentiSospensioneLibroUnico.Add(sosp);
                        }
                    }
                }
            }
            return documentiSospensioneLibroUnico;
        }

        public DenunciaACECollection GetDocumentiSospensioneImpresaDenunceACE()
        {
            DenunciaACECollection documentiSospensioneDenunceACE = new DenunciaACECollection();
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseRichiesteVariazioneStatoGetDocumentiDenunce"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        #region Index
                        int indexIdRichiesta = reader.GetOrdinal("idRichiesta");
                        int indexIdImpresa = reader.GetOrdinal("idImpresa");
                        int indexAllegatoNome = reader.GetOrdinal("allegatoNome");
                        int indexAllegato = reader.GetOrdinal("allegato");
                        int indexRagioneSociale = reader.GetOrdinal("ragioneSociale");
                        int indexCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                        int indexPartitaIVA = reader.GetOrdinal("partitaIVA");
                        #endregion

                        while (reader.Read())
                        {
                            DenunciaACE sosp = new DenunciaACE();
                            sosp.Tipo = "Denunce altre CE";
                            sosp.IdRichiesta = reader.GetInt32(indexIdRichiesta);
                            sosp.IdImpresa = reader.GetInt32(indexIdImpresa);
                            sosp.RagioneSocialeImpresa = reader.GetString(indexRagioneSociale);
                            sosp.CodiceFiscaleIVAImpresa = reader.IsDBNull(indexCodiceFiscale) ? reader.GetString(indexPartitaIVA) : reader.GetString(indexCodiceFiscale);
                            sosp.AllegatoNome = reader.GetString(indexAllegatoNome);
                            byte[] immagine = new byte[reader.GetBytes(indexAllegato, 0, null, 0, 0)];
                            reader.GetBytes(indexAllegato, 0, immagine, 0, immagine.Length);
                            sosp.FileByteArray = immagine;
                            sosp.FileEstensione = Path.GetExtension(sosp.AllegatoNome).Substring(1);
                            sosp.FileNome =
                                $"Denunce_ACE_{sosp.IdRichiesta}{Path.GetExtension(reader.GetString(indexAllegatoNome))}";
                            sosp.DataInserimentoRecord = DateTime.Now.Date;

                            documentiSospensioneDenunceACE.Add(sosp);
                        }
                    }
                }
            }
            return documentiSospensioneDenunceACE;
        }

        public UniemensCollection GetDocumentiSospensioneImpresaUniemens()
        {
            UniemensCollection documentiSospensioneUniemens = new UniemensCollection();
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseRichiesteVariazioneStatoGetDocumenti"))
            {
                DatabaseSice.AddInParameter(command, "@tipoDocumento", DbType.String, "Uniemens");
                
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        #region Index
                        int indexIdRichiesta = reader.GetOrdinal("idRichiesta");
                        int indexIdImpresa = reader.GetOrdinal("idImpresa");
                        int indexAllegatoNome = reader.GetOrdinal("allegatoNome");
                        int indexAllegato = reader.GetOrdinal("allegato");
                        int indexRagioneSociale = reader.GetOrdinal("ragioneSociale");
                        int indexCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                        int indexPartitaIVA = reader.GetOrdinal("partitaIVA");
                        #endregion

                        while (reader.Read())
                        {
                            Uniemens sosp = new Uniemens();
                            sosp.Tipo = "Uniemens";
                            sosp.IdRichiesta = reader.GetInt32(indexIdRichiesta);
                            sosp.IdImpresa = reader.GetInt32(indexIdImpresa);
                            sosp.RagioneSocialeImpresa = reader.GetString(indexRagioneSociale);
                            sosp.CodiceFiscaleIVAImpresa = reader.IsDBNull(indexCodiceFiscale) ? reader.GetString(indexPartitaIVA) : reader.GetString(indexCodiceFiscale);
                            sosp.AllegatoNome = reader.GetString(indexAllegatoNome);
                            byte[] immagine = new byte[reader.GetBytes(indexAllegato, 0, null, 0, 0)];
                            reader.GetBytes(indexAllegato, 0, immagine, 0, immagine.Length);
                            sosp.FileByteArray = immagine;
                            sosp.FileNome = $"Uniemens_{sosp.IdRichiesta}{Path.GetExtension(reader.GetString(indexAllegatoNome))}";
                            sosp.FileEstensione = Path.GetExtension(sosp.AllegatoNome).Substring(1);
                            sosp.DataInserimentoRecord = DateTime.Now.Date;

                            documentiSospensioneUniemens.Add(sosp);
                        }
                    }
                }
            }
            return documentiSospensioneUniemens;
        }

        #endregion

        #endregion

        #region Insert Update OLD

        //public void InsertUpdateCertificato(Certificato certificato)
        //{
        //    try
        //    {
        //        using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiCertificatiInsertUpdate"))
        //        {
        //            DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, certificato.IdArchidoc);
        //            //DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, certificato.IdLavoratore);
        //            //DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, certificato.IdFamiliare);
        //            //DatabaseSice.AddInParameter(command, "@cognome", DbType.String, certificato.Cognome);
        //            //DatabaseSice.AddInParameter(command, "@nome", DbType.String, certificato.Nome);
        //            //DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime,
        //            //                            certificato.ParseData(certificato.DataNascita));
        //            //DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String, certificato.CodiceFiscale);
        //            DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, certificato.DataScansione);
        //            DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
        //                                        certificato.ProtocolloScansione);
        //            DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
        //                                        certificato.ParseIdTipoPrestazione(certificato.IdTipoPrestazione));
        //            DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
        //                                        certificato.ProtocolloPrestazione);
        //            DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
        //                                        certificato.NumeroProtocolloPrestazione);
        //            DatabaseSice.AddInParameter(command, "@idTipoCertificatoArchidoc", DbType.Int32,
        //                                        certificato.IdTipoCertificatoArchidoc);
        //            DatabaseSice.AddInParameter(command, "@originale", DbType.Boolean, certificato.Originale);

        //            if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //            {

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertCertificato: errore durante l'inserimento di un certificato" + ex.Message);
        //    }
        //}
        //public void InsertUpdateCodiceFiscale(CodiceFiscale codiceFiscale)
        //{
        //    try
        //    {
        //        using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiCodiceFiscaleInsertUpdate"))
        //        {
        //            DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, codiceFiscale.IdArchidoc);
        //            DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, codiceFiscale.IdLavoratore);
        //            DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, codiceFiscale.IdFamiliare);
        //            //DatabaseSice.AddInParameter(command, "@cognome", DbType.String, codiceFiscale.Cognome);
        //            //DatabaseSice.AddInParameter(command, "@nome", DbType.String, codiceFiscale.Nome);
        //            //DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime,
        //            //                            codiceFiscale.ParseData(codiceFiscale.DataNascita));
        //            //DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String, codiceFiscale.CodiceFiscale);
        //            DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, codiceFiscale.DataScansione);
        //            DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
        //                                        codiceFiscale.ProtocolloScansione);
        //            if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //            {

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertCodiceFiscale: errore durante l'inserimento di un codice fiscale"+ex.Message);
        //    }
        //}
        //public void InsertUpdateStatoFamiglia(StatoFamiglia statoFamiglia)
        //{
        //    try
        //    {

        //        using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiStatoFamigliaInsertUpdate"))
        //        {
        //            DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, statoFamiglia.IdArchidoc);
        //            DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, statoFamiglia.IdLavoratore);
        //            //DatabaseSice.AddInParameter(command, "@cognome", DbType.String, statoFamiglia.Cognome);
        //            //DatabaseSice.AddInParameter(command, "@nome", DbType.String, statoFamiglia.Nome);
        //            //DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime, 
        //            //                                statoFamiglia.ParseData(statoFamiglia.DataNascita));
        //            //DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String, statoFamiglia.CodiceFiscale);
        //            DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, statoFamiglia.DataScansione);
        //            DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
        //                                        statoFamiglia.ProtocolloScansione);
        //            if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //            {

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertStatoFamiglia: errore durante l'inserimento di uno stato familiare"+ex.Message);
        //    }
        //}
        //public void InsertUpdateModuloPrestazioni(Modulo modulo)
        //{
        //    try
        //    {
        //    using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiModuloPrestazioniInsertUpdate"))
        //    {
        //        DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, modulo.IdArchidoc);
        //        //DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, modulo.IdLavoratore);
        //        //DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, modulo.IdFamiliare);
        //        //DatabaseSice.AddInParameter(command, "@cognome", DbType.String, modulo.Cognome);
        //        //DatabaseSice.AddInParameter(command, "@nome", DbType.String, modulo.Nome);
        //        //DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime, 
        //        //                            modulo.ParseData(modulo.DataNascita));
        //        //DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String, modulo.CodiceFiscale);
        //        DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, modulo.DataScansione);
        //        DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
        //                                    modulo.ProtocolloScansione);
        //        DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
        //                                    modulo.ParseIdTipoPrestazione(modulo.IdTipoPrestazione));
        //        DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
        //                                    modulo.ProtocolloPrestazione);
        //        DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
        //                                    modulo.NumeroProtocolloPrestazione);
        //        DatabaseSice.AddInParameter(command, "@originale", DbType.Boolean, modulo.Originale);

        //        if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //        {
        //        }
        //    }
        //        }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertModuloPrestazione: errore durante l'inserimento di un modulo prestazione"+ex.Message);
        //    }
        //}
        //public void InsertUpdateDichiarazioneIntervento(DichiarazioneIntervento dichiarazioneIntervento)
        //{
        //    try
        //    {
        //    using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDichiarazioniInterventoInsertUpdate"))
        //    {
        //        DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, dichiarazioneIntervento.IdArchidoc);
        //        //DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, dichiarazioneIntervento.IdLavoratore);
        //        //DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, dichiarazioneIntervento.IdFamiliare);
        //        //DatabaseSice.AddInParameter(command, "@cognome", DbType.String, dichiarazioneIntervento.Cognome);
        //        //DatabaseSice.AddInParameter(command, "@nome", DbType.String, dichiarazioneIntervento.Nome);
        //        //DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime,
        //        //                            dichiarazioneIntervento.ParseData(dichiarazioneIntervento.DataNascita));
        //        //DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String,
        //        //                            dichiarazioneIntervento.CodiceFiscale);
        //        DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime,
        //                                    dichiarazioneIntervento.DataScansione);
        //        DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
        //                                    dichiarazioneIntervento.ProtocolloScansione);
        //        DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
        //                                    dichiarazioneIntervento.ParseIdTipoPrestazione(dichiarazioneIntervento.IdTipoPrestazione));
        //        DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
        //                                    dichiarazioneIntervento.ProtocolloPrestazione);
        //        DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
        //                                    dichiarazioneIntervento.NumeroProtocolloPrestazione);               

        //        if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //        {
        //            //
        //        }
        //    }
        //        }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertDichiarazioneIntervento: errore durante l'inserimento di un intervento"+ex.Message);
        //    }
        //}
        //public void InsertUpdateFattura(Fattura fattura)
        //{
        //    try
        //    {
        //    using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_PrestazioniFattureInsertUpdateComplete"))
        //    {
        //        DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, fattura.IdArchidoc);
        //        DatabaseSice.AddInParameter(command, "@data", DbType.DateTime, 
        //                                    fattura.ParseData(fattura.Data));
        //        DatabaseSice.AddInParameter(command, "@totale ", DbType.Decimal, fattura.Totale);
        //        DatabaseSice.AddInParameter(command, "@numero", DbType.String, fattura.Numero);
        //        //DatabaseSice.AddInParameter(command, "@valida", DbType.Boolean, fattura.Valida);
        //        //DatabaseSice.AddInParameter(command, "@idLavoratore ", DbType.Int32, fattura.IdLavoratore);
        //        //DatabaseSice.AddInParameter(command, "@cognome", DbType.String, fattura.Cognome);
        //        //DatabaseSice.AddInParameter(command, "@nome", DbType.String, fattura.Nome);
        //        DatabaseSice.AddInParameter(command, "@originale", DbType.Boolean, fattura.Originale);
        //        //DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
        //        //                            fattura.NumeroProtocolloPrestazione);
        //        //DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
        //        //                            fattura.ProtocolloPrestazione);
        //        ////DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
        //        //                            fattura.ParseIdTipoPrestazione(fattura.IdTipoPrestazione));
        //        DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, fattura.DataScansione);
        //        DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String, fattura.ProtocolloScansione);

        //        if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //        {
        //            //
        //        }
        //    }
        //        }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertFattura: errore durante l'inserimento di una fattura"+ex.Message);
        //    }
        //}
        //public void InsertUpdateDeleghe(Delega delega)
        //{
        //    try
        //    {
        //        //InsertLog("InsertUpdateDeleghe", "idArchidoc: " + delega.IdArchidoc + " protocolloScansione: " + delega.ProtocolloScansione);
        //        using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDelegheInsertUpdate"))
        //        {
        //            DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, delega.IdArchidoc);
        //            DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, delega.IdLavoratore);
        //            DatabaseSice.AddInParameter(command, "@cognome", DbType.String, delega.Cognome);
        //            DatabaseSice.AddInParameter(command, "@nome", DbType.String, delega.Nome);
        //            DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime,
        //                                        delega.ParseData(delega.DataNascita));

        //            DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String, delega.CodiceFiscale);
        //            DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, delega.DataScansione);
        //            DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
        //                                        delega.ProtocolloScansione);

        //            DatabaseSice.AddInParameter(command, "@idDelega", DbType.Int32, delega.Barcode);

        //            DatabaseSice.AddInParameter(command, "@idSindacato", DbType.String, delega.CodiceSindacato);
        //            DatabaseSice.AddInParameter(command, "@idComprensorioSindacale", DbType.String, delega.CodiceComprensorio);

        //            DatabaseSice.AddInParameter(command, "@dataAdesione", DbType.DateTime,
        //                                       delega.ParseData(delega.DataAdesione));

        //            if (DatabaseSice.ExecuteNonQuery(command) != 1)
        //            {
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ArchidocUpdateSiceInfoException("InsertDelega: errore durante l'inserimento di una delega" + ex.Message);
        //    }
        //}

        //#region MalattiaTelematica
        //public void MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(MalattiaTelematica certificato)
        //{

        //    using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCertificatiMediciUpdateIdArchidoc"))
        //    {
        //        DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, certificato.IdArchidoc);
        //        DatabaseSice.AddInParameter(command, "@idMalattiaTelematica", DbType.Int32, certificato.IdMalattiaTelematica);

        //        DatabaseSice.ExecuteNonQuery(command); 
        //    }
        //}

        //public void MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(MalattiaTelematica certificato)
        //{

        //    using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_MalattiaTelematicaAltriDocumentiUpdateIdArchidoc"))
        //    {
        //        DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, certificato.IdArchidoc);
        //        DatabaseSice.AddInParameter(command, "@idMalattiaTelematica", DbType.Int32, certificato.IdMalattiaTelematica);

        //        DatabaseSice.ExecuteNonQuery(command);
        //    }
        //}
        //#endregion

        #endregion


        #region Insert Update New

        public void InsertUpdateCertificato(Certificato certificato)
        {
            try
            {
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiCertificatiInsertUpdateNew"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, certificato.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, certificato.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
                                                certificato.ProtocolloScansione);
                    DatabaseSice.AddInParameter(command, "@idPrestazioniDomanda", DbType.Int32, certificato.IdPrestazioniDomanda);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, certificato.IdLavoratore);
                    if (certificato.IdFamiliare.HasValue)
                    {
                        DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, certificato.IdFamiliare.Value);
                    }
                    else
                    {
                        DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, DBNull.Value);
                    }
                    DatabaseSice.AddInParameter(command, "@originale", DbType.Boolean, certificato.Originale);


                    //compatibilit� vecchio meccanismo

                    DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
                                               certificato.ParseIdTipoPrestazione(certificato.IdTipoPrestazione));
                    DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
                                                certificato.ProtocolloPrestazione);
                    DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
                                                certificato.NumeroProtocolloPrestazione);
                    DatabaseSice.AddInParameter(command, "@idTipoCertificatoArchidoc", DbType.Int32,
                                                certificato.IdTipoCertificatoArchidoc);
                    //fine

                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertCertificato: errore durante l'inserimento di un certificato" + ex.Message);
            }
        }

        public void InsertUpdateCodiceFiscale(CodiceFiscale codiceFiscale)
        {
            try
            {
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiCodiceFiscaleInsertUpdateNew"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, codiceFiscale.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, codiceFiscale.IdLavoratore);
                    DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, codiceFiscale.IdFamiliare);
                    DatabaseSice.AddInParameter(command, "@idPrestazioniDomanda", DbType.Int32, codiceFiscale.IdPrestazioniDomanda);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, codiceFiscale.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
                                                codiceFiscale.ProtocolloScansione);
                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertCodiceFiscale: errore durante l'inserimento di un codice fiscale" + ex.Message);
            }
        }

        public void InsertUpdateStatoFamiglia(StatoFamiglia statoFamiglia)
        {
            try
            {

                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiStatoFamigliaInsertUpdateNew"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, statoFamiglia.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, statoFamiglia.IdLavoratore);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, statoFamiglia.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
                                                statoFamiglia.ProtocolloScansione);
                    DatabaseSice.AddInParameter(command, "@idPrestazioniDomanda", DbType.Int32, statoFamiglia.IdPrestazioniDomanda);
                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertStatoFamiglia: errore durante l'inserimento di uno stato familiare" + ex.Message);
            }
        }

        public void InsertUpdateModuloPrestazioni(Modulo modulo)
        {
            try
            {
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiModuloPrestazioniInsertUpdateNew"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, modulo.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, modulo.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
                                                modulo.ProtocolloScansione);

                    DatabaseSice.AddInParameter(command, "@idPrestazioniDomanda", DbType.Int32, modulo.IdPrestazioniDomanda);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, modulo.IdLavoratore);
                    //if (fattura.IdFamiliare.HasValue)
                    //{
                    DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, modulo.IdFamiliare);
                    //}
                    //else
                    //{
                    //    DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, DBNull.Value);
                    //}
                    DatabaseSice.AddInParameter(command, "@originale", DbType.Boolean, modulo.Originale);

                    //old
                    DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
                                                modulo.ParseIdTipoPrestazione(modulo.IdTipoPrestazione));
                    DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
                                                modulo.ProtocolloPrestazione);
                    DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
                                                modulo.NumeroProtocolloPrestazione);
                    //old fine

                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertModuloPrestazione: errore durante l'inserimento di un modulo prestazione" + ex.Message);
            }
        }

        public void InsertUpdateDichiarazioneIntervento(DichiarazioneIntervento dichiarazioneIntervento)
        {
            try
            {
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDichiarazioniInterventoInsertUpdateNew"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, dichiarazioneIntervento.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime,
                                                dichiarazioneIntervento.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
                                                dichiarazioneIntervento.ProtocolloScansione);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, dichiarazioneIntervento.IdLavoratore);
                    DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, dichiarazioneIntervento.IdFamiliare);
                    DatabaseSice.AddInParameter(command, "@idPrestazioniDomanda", DbType.Int32, dichiarazioneIntervento.IdPrestazioniDomanda);

                    //old
                    DatabaseSice.AddInParameter(command, "@idTipoPrestazione", DbType.String,
                                                dichiarazioneIntervento.ParseIdTipoPrestazione(dichiarazioneIntervento.IdTipoPrestazione));
                    DatabaseSice.AddInParameter(command, "@protocolloPrestazione", DbType.Int32,
                                                dichiarazioneIntervento.ProtocolloPrestazione);
                    DatabaseSice.AddInParameter(command, "@numeroProtocolloPrestazione", DbType.Int32,
                                                dichiarazioneIntervento.NumeroProtocolloPrestazione);
                    //old fine


                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {
                        //
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertDichiarazioneIntervento: errore durante l'inserimento di un intervento" + ex.Message);
            }
        }

        public void InsertUpdateFattura(Fattura fattura)
        {
            try
            {
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_PrestazioniFattureInsertUpdateCompleteNew"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, fattura.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, fattura.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String, fattura.ProtocolloScansione);
                    DatabaseSice.AddInParameter(command, "@idPrestazioniFatturaDichiarata", DbType.Int32, fattura.IdPrestazioniFatturaDichiarata);
                    DatabaseSice.AddInParameter(command, "@idPrestazioniDomanda", DbType.Int32, fattura.IdPrestazioniDomanda);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, fattura.IdLavoratore);
                    if (fattura.IdFamiliare.HasValue)
                    {
                        DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, fattura.IdFamiliare.Value);
                    }
                    else
                    {
                        DatabaseSice.AddInParameter(command, "@idFamiliare", DbType.Int32, DBNull.Value);
                    }
                    DatabaseSice.AddInParameter(command, "@originale", DbType.Boolean, fattura.Originale);
                    //old
                    DatabaseSice.AddInParameter(command, "@data", DbType.DateTime,
                                                fattura.ParseData(fattura.Data));
                    DatabaseSice.AddInParameter(command, "@totale ", DbType.Decimal, fattura.Totale);
                    DatabaseSice.AddInParameter(command, "@numero", DbType.String, fattura.Numero);
                    //old fine

                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {
                        //
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertFattura: errore durante l'inserimento di una fattura" + ex.Message);
            }
        }

        public void InsertUpdateDeleghe(Delega delega)
        {
            try
            {
                //InsertLog("InsertUpdateDeleghe", "idArchidoc: " + delega.IdArchidoc + " protocolloScansione: " + delega.ProtocolloScansione);
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDelegheInsertUpdate"))
                {
                    DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, delega.IdArchidoc);
                    DatabaseSice.AddInParameter(command, "@idLavoratore", DbType.Int32, delega.IdLavoratore);
                    DatabaseSice.AddInParameter(command, "@cognome", DbType.String, delega.Cognome);
                    DatabaseSice.AddInParameter(command, "@nome", DbType.String, delega.Nome);
                    DatabaseSice.AddInParameter(command, "@dataNascita", DbType.DateTime,
                                                delega.ParseData(delega.DataNascita));

                    DatabaseSice.AddInParameter(command, "@codiceFiscale", DbType.String, delega.CodiceFiscale);
                    DatabaseSice.AddInParameter(command, "@dataScansione", DbType.DateTime, delega.DataScansione);
                    DatabaseSice.AddInParameter(command, "@protocolloScansione", DbType.String,
                                                delega.ProtocolloScansione);

                    DatabaseSice.AddInParameter(command, "@idDelega", DbType.Int32, delega.Barcode);

                    DatabaseSice.AddInParameter(command, "@idSindacato", DbType.String, delega.CodiceSindacato);
                    DatabaseSice.AddInParameter(command, "@idComprensorioSindacale", DbType.String, delega.CodiceComprensorio);

                    DatabaseSice.AddInParameter(command, "@dataAdesione", DbType.DateTime,
                                               delega.ParseData(delega.DataAdesione));

                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ArchidocUpdateSiceInfoException("InsertDelega: errore durante l'inserimento di una delega" + ex.Message);
            }
        }

        #region MalattiaTelematica
        public void MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(MalattiaTelematica certificato)
        {

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCertificatiMediciUpdateIdArchidoc"))
            {
                DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, certificato.IdArchidoc);
                DatabaseSice.AddInParameter(command, "@idMalattiaTelematica", DbType.Int32, certificato.IdMalattiaTelematica);

                DatabaseSice.ExecuteNonQuery(command);
            }
        }

        public void MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(MalattiaTelematica certificato)
        {

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_MalattiaTelematicaAltriDocumentiUpdateIdArchidoc"))
            {
                DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, certificato.IdArchidoc);
                DatabaseSice.AddInParameter(command, "@idMalattiaTelematica", DbType.Int32, certificato.IdMalattiaTelematica);

                DatabaseSice.ExecuteNonQuery(command);
            }
        }
        #endregion

        #region Variazioni anagrafiche imprese

        public void ImpreseRichiesteVariazioniStatoUpdateLibroUnicoIdArchidoc(LibroUnico documento)
        {
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseRichiesteVariazioneStatoUpdateLibroUnicoIdArchidoc"))
            {
                DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, documento.IdArchidoc);
                DatabaseSice.AddInParameter(command, "@idRichiesta", DbType.Int32, documento.IdRichiesta);

                DatabaseSice.ExecuteNonQuery(command);
            }
        }

        public void ImpreseRichiesteVariazioniStatoUpdateDenunceACEIdArchidoc(DenunciaACE documento)
        {
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseRichiesteVariazioneStatoUpdateDenunceACEIdArchidoc"))
            {
                DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, documento.IdArchidoc);
                DatabaseSice.AddInParameter(command, "@idRichiesta", DbType.Int32, documento.IdRichiesta);

                DatabaseSice.ExecuteNonQuery(command);
            }
        }

        public void ImpreseRichiesteVariazioniStatoUpdateUniemensIdArchidoc(Uniemens documento)
        {
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseRichiesteVariazioneStatoUpdateIdArchidoc"))
            {
                DatabaseSice.AddInParameter(command, "@idArchidoc", DbType.String, documento.IdArchidoc);
                DatabaseSice.AddInParameter(command, "@tipoDocumento", DbType.String, "Uniemens");
                DatabaseSice.AddInParameter(command, "@idRichiesta", DbType.Int32, documento.IdRichiesta);

                DatabaseSice.ExecuteNonQuery(command);
            }
        }

        #endregion

        #endregion

        #endregion

        public Database DatabaseSice { get; set; }

        public DateTime? GetDataElaborazione()
        {
            DateTime? dataElaborazione = null;

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ArchidocLogSelectLast"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        int dataElaborazioneIndex = reader.GetOrdinal("dataElaborazione");

                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(dataElaborazioneIndex))
                                dataElaborazione = reader.GetDateTime(dataElaborazioneIndex);
                        }
                    }
                }
            }

            return dataElaborazione;
        }

        /// <summary>
        /// Ritorna l'iddelega minore ovvero l'ultimo scansionato in archidoc
        /// </summary>
        /// <returns></returns>
        public Int32? GetMinDelega()
        {
            Int32? idDelega = null;

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DelegheSelectUltimaScansionata"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        Int32 idDelegaIndex = reader.GetOrdinal("ultimaDelegaScansionata");

                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(idDelegaIndex))
                                idDelega = reader.GetInt32(idDelegaIndex);
                        }
                    }
                }
            }

            return idDelega;
        }
        /// <summary>
        /// Ritorna l'iddelega minore ovvero l'ultimo inserito in archidoc
        /// </summary>
        /// <returns></returns>
        public Int32? GetMaxDelega()
        {
            Int32? idDelega = null;

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DelegheSelectUltimaInserita"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader != null)
                    {
                        Int32 idDelegaIndex = reader.GetOrdinal("ultimaDelegaInserita");

                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(idDelegaIndex))
                                idDelega = reader.GetInt32(idDelegaIndex);
                        }
                    }
                }
            }

            return idDelega;
        }

        public void InsertLog(string tipoElaborazione, string messaggio)
        {
            try
            {
                using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ArchidocLogInsert"))
                {
                    DatabaseSice.AddInParameter(command, "@tipoElaborazione", DbType.String, tipoElaborazione);
                    DatabaseSice.AddInParameter(command, "@messaggio", DbType.String, messaggio);

                    if (DatabaseSice.ExecuteNonQuery(command) != 1)
                    {
                        //TODO usare l'EVENT Log
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO usare l'EVENT Log
            }
        }

        #region Gestione allegati deleghe
        public Delega GetDelegaByProgressivoAnnuo(String progressivo)
        {
            Delega delega = null;
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDelegheSelectByProgressivoAnnuo"))
            {
                DatabaseSice.AddInParameter(command, "@progressivoAnnuo", DbType.String, progressivo);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    int idDocumentoIndex = reader.GetOrdinal("idDelega");

                    int idArchidocIndex = reader.GetOrdinal("idArchidoc");

                    int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");

                    int idComprensorioSindacaleIndex = reader.GetOrdinal("idComprensorioSindacale");

                    int idSindacatoIndex = reader.GetOrdinal("idSindacato");

                    int cognomeIndex = reader.GetOrdinal("cognome");
                    int nomeIndex = reader.GetOrdinal("nome");
                    int codiceFiscaleIndex = reader.GetOrdinal("codiceFiscale");
                    int protocolloScansioneIndex = reader.GetOrdinal("protocolloScansione");

                    int dataNascitaIndex = reader.GetOrdinal("dataNascita");
                    int dataScansioneIndex = reader.GetOrdinal("dataScansione");
                    int dataInserimentoRecordIndex = reader.GetOrdinal("dataInserimentoRecord");
                    int dataModificaRecordIndex = reader.GetOrdinal("dataModificaRecord");
                    int dataAdesioneIndex = reader.GetOrdinal("dataAdesione");
                    int indexNomeAllegatoLettera = reader.GetOrdinal("nomeAllegatoLettera");
                    int indexNomeAllegatoBusta = reader.GetOrdinal("nomeAllegatoBusta");

                    if (reader.Read())
                    {
                        delega = new Delega();

                        delega.CodiceSindacato = reader.IsDBNull(idSindacatoIndex) ? null : reader.GetString(idSindacatoIndex);

                        delega.CodiceComprensorio = reader.IsDBNull(idComprensorioSindacaleIndex) ? null : reader.GetString(idComprensorioSindacaleIndex);

                        delega.CodiceFiscale = reader.IsDBNull(codiceFiscaleIndex)
                                                          ? null
                                                          : reader.GetString(codiceFiscaleIndex);
                        delega.Cognome = reader.IsDBNull(cognomeIndex)
                                                    ? null
                                                    : reader.GetString(cognomeIndex);
                        delega.DataInserimentoRecord = reader.IsDBNull(dataInserimentoRecordIndex)
                                                                  ? (DateTime?) null
                                                                  : reader.GetDateTime(
                                                                        dataInserimentoRecordIndex);
                        delega.DataModificaRecord = reader.IsDBNull(dataModificaRecordIndex)
                                                               ? (DateTime?) null
                                                               : reader.GetDateTime(dataModificaRecordIndex);
                        delega.DataNascita = reader.IsDBNull(dataNascitaIndex)
                                                        ? (DateTime?) null
                                                        : reader.GetDateTime(dataNascitaIndex);
                        delega.DataScansione = reader.IsDBNull(dataScansioneIndex)
                                                          ? (DateTime?) null
                                                          : reader.GetDateTime(dataScansioneIndex);
                        delega.IdArchidoc = reader.IsDBNull(idArchidocIndex)
                                                       ? null
                                                       : reader.GetString(idArchidocIndex);
                        delega.IdDocumento = reader.IsDBNull(idDocumentoIndex)
                                                        ? (int?) null
                                                        : reader.GetInt32(idDocumentoIndex);

                        delega.IdLavoratore = reader.IsDBNull(idLavoratoreIndex)
                                                         ? (int?) null
                                                         : reader.GetInt32(idLavoratoreIndex);
                        delega.Nome = reader.IsDBNull(nomeIndex) ? null : reader.GetString(nomeIndex);
                        delega.ProtocolloScansione = reader.IsDBNull(protocolloScansioneIndex)
                                                                ? null
                                                                : reader.GetString(protocolloScansioneIndex);
                        delega.DataAdesione = reader.IsDBNull(dataAdesioneIndex)
                                                          ? (DateTime?) null
                                                          : reader.GetDateTime(dataAdesioneIndex);
                        delega.NomeAllegatoLettera = reader.IsDBNull(indexNomeAllegatoLettera) ? null : reader.GetString(indexNomeAllegatoLettera);

                        delega.NomeAllegatoBusta = reader.IsDBNull(indexNomeAllegatoBusta) ? null : reader.GetString(indexNomeAllegatoBusta);
                    }
                }
            }

            return delega;
        }

        public Boolean UpdateDelegaAllegatoBusta(Delega delega, String nomeAllegato)
        {
            Boolean result = true;
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DocumentiDelegheUpdateAllegatoBusta"))
            {
                DatabaseSice.AddInParameter(command, "@idDelega", DbType.Int32, delega.IdDocumento);
                if (nomeAllegato != null)
                {
                    DatabaseSice.AddInParameter(command, "@nomeAllegatoBusta", DbType.String, nomeAllegato);
                }
                else
                {
                    DatabaseSice.AddInParameter(command, "@nomeAllegatoBusta", DbType.String, DBNull.Value);
                }

                DatabaseSice.ExecuteNonQuery(command);

            }

            return result;
        }
        #endregion
    }
}