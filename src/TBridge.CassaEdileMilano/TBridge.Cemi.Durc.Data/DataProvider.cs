using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Durc.Type.Collections;
using TBridge.Cemi.Durc.Type.Entities;
using TBridge.Cemi.Durc.Type.Filters;

namespace TBridge.Cemi.Durc.Data
{
    public class DataProvider
    {
        public DataProvider()
        {
            DatabaseSice = DatabaseFactory.CreateDatabase("CEMI");
        }

        private Database DatabaseSice { get; set; }

        public SollecitoEmissione InserisciSollecito(SollecitoEmissione sollecitoEmissione)
        {
            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_DurcSollecitiInsert"))
            {
                DatabaseSice.AddInParameter(command, "@idUtente", DbType.Int32, sollecitoEmissione.IdUtente);
                DatabaseSice.AddInParameter(command, "@eMail", DbType.String, sollecitoEmissione.EMail);
                DatabaseSice.AddInParameter(command, "@cip", DbType.String, sollecitoEmissione.Cip);
                DatabaseSice.AddInParameter(command, "@numeroProtocollo", DbType.String,
                                            sollecitoEmissione.NumeroProtocollo);
                DatabaseSice.AddInParameter(command, "@motivoRichiesta", DbType.String,
                                            sollecitoEmissione.MotivoRichiesta);
                DatabaseSice.AddInParameter(command, "@idImpresa", DbType.Int32,
                                            sollecitoEmissione.IdImpresa);
                DatabaseSice.AddOutParameter(command, "@idSollecito", DbType.Int32, 4);

                if (DatabaseSice.ExecuteNonQuery(command) == 1)
                {
                    sollecitoEmissione.IdDurcSollecito = (int) DatabaseSice.GetParameterValue(command, "@idSollecito");
                }
            }

            return sollecitoEmissione;
        }

        public String GetAddettoAreaImpresa(Int32 idImpresa)
        {
            String addettoArea = String.Empty;

            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpresaSelectAddettoArea"))
            {
                DatabaseSice.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseSice.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        if (!Convert.IsDBNull(reader["addettoArea"]))
                        {
                            addettoArea = (String) reader["addettoArea"];
                        }
                    }
                }
            }

            return addettoArea;
        }

        public SollecitoEmissioneCollection GetRichiesteSollecito(SollecitoEmissioneFilter filter)
        {
            SollecitoEmissioneCollection richiesteSollecito = new SollecitoEmissioneCollection();

            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_DurcSollecitiSelectWithFilter"))
            {
                DatabaseSice.AddInParameter(comando, "@idImpresa", DbType.Int32, filter.CodiceImpresa);
                DatabaseSice.AddInParameter(comando, "@ragioneSociale", DbType.String, filter.RagioneSociale);
                DatabaseSice.AddInParameter(comando, "@cip", DbType.String, filter.Cip);
                DatabaseSice.AddInParameter(comando, "@numeroProtocollo", DbType.String, filter.NumeroProtocollo);
                DatabaseSice.AddInParameter(comando, "@dataInizio", DbType.DateTime, filter.DataInizio);
                DatabaseSice.AddInParameter(comando, "@dataFine", DbType.DateTime, filter.DataFine);

                using (IDataReader reader = DatabaseSice.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        SollecitoEmissione sollecito = new SollecitoEmissione();

                        sollecito.Cip = reader["cip"].ToString();
                        sollecito.IdImpresa = (int) reader["idImpresa"];
                        sollecito.RagioneSociale = reader["ragioneSociale"].ToString();
                        sollecito.IdDurcSollecito = (int?) reader["idDurcSollecito"];
                        sollecito.IdUtente = (int) reader["idUtente"];
                        sollecito.RagioneSocialeConsulente = reader["utenteInserimento"].ToString();
                        sollecito.IdConsulente = reader["idConsulente"].ToString();
                        sollecito.MotivoRichiesta = reader["motivoRichiesta"].ToString();
                        sollecito.NumeroProtocollo = reader["numeroProtocollo"].ToString();
                        sollecito.EMail = reader["eMail"].ToString();
                        sollecito.DataRichiesta = (DateTime?) reader["dataInserimentoRecord"];
                        sollecito.RitiroSede = (bool)reader["ritiroSede"];
                        sollecito.Telefono = reader["numeroTelefono"].ToString();

                        richiesteSollecito.Add(sollecito);
                    }
                }
            }

            return richiesteSollecito;
        }
    }
}