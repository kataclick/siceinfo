using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text.RegularExpressions;
using TBridge.Cemi.Business.Crm;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.SmsInfo.Business.Carrier;
using TBridge.Cemi.SmsInfo.Data;
using TBridge.Cemi.SmsInfo.Type.Collections;
using TBridge.Cemi.SmsInfo.Type.Entities;

namespace TBridge.Cemi.SmsInfo.Business
{
    [Obsolete("Utilizzre WS", true)]
    public class SmsBusiness
    {
        private readonly SmsCarrier _smsCarrier;

        private readonly SmsDataAccess _smsDataAccess;

        public SmsBusiness()
        {
            string username = ConfigurationManager.AppSettings["SmsUsername"];
            string password = ConfigurationManager.AppSettings["SmsPassword"];
            _smsCarrier = new SmsCarrier(username, password);
            _smsDataAccess = new SmsDataAccess();
        }

        #region Messaggi di errore

        private readonly string CANCELLAZIONE_ARRIVEDERCI =
            ConfigurationManager.AppSettings["CANCELLAZIONE_ARRIVEDERCI"];

        private readonly string CANCELLAZIONE_ERRORECF =
            ConfigurationManager.AppSettings["CANCELLAZIONE_ERRORECF"];

        private readonly string CANCELLAZIONE_ERROREFORMATO =
            ConfigurationManager.AppSettings["CANCELLAZIONE_ERROREFORMATO"];

        private readonly string CANCELLAZIONE_LAVORATORENONPRESENTE =
            ConfigurationManager.AppSettings["CANCELLAZIONE_LAVORATORENONPRESENTE"];

        private readonly string ERROREGENERICO =
            ConfigurationManager.AppSettings["ERROREGENERICO"];

        private readonly string LOGINLAVORATORE =
            ConfigurationManager.AppSettings["LOGINLAVORATORE"];

        private readonly string LOGINLAVORATOREASSENTE =
            ConfigurationManager.AppSettings["LOGINLAVORATOREASSENTE"];

        private readonly string PINLAVORATORE =
            ConfigurationManager.AppSettings["PINLAVORATORE"];

        private readonly string REGISTRAZIONE_AGGIORNATO =
            ConfigurationManager.AppSettings["REGISTRAZIONE_AGGIORNATO"];

        private readonly string REGISTRAZIONE_BENVENUTO =
            ConfigurationManager.AppSettings["REGISTRAZIONE_BENVENUTO"];

        private readonly string REGISTRAZIONE_ERRORECF =
            ConfigurationManager.AppSettings["REGISTRAZIONE_ERRORECF"];

        private readonly string REGISTRAZIONE_ERROREFORMATO =
            ConfigurationManager.AppSettings["REGISTRAZIONE_ERROREFORMATO"];

        private readonly string REGISTRAZIONE_LAVORATORENONPRESENTE =
            ConfigurationManager.AppSettings["REGISTRAZIONE_LAVORATORENONPRESENTE"];

        private readonly string REGISTRAZIONE_NUMEROGIAATTIVO =
            ConfigurationManager.AppSettings["REGISTRAZIONE_NUMEROGIAATTIVO"];

        private readonly string RICHIESTATS =
            ConfigurationManager.AppSettings["RICHIESTATS"];

        #endregion

        #region Gestione Richieste Registrazione/Cancellazione

        public void VerificaRichieste()
        {
            //interrogo il carrier e mi faccio restituire la lista di messaggi
            SmsCollection smsRicevuti = GetListaSmsRicevutiCarrier();
            if (smsRicevuti != null && smsRicevuti.Count > 0)
            {
                //recupero idCarrier ultimo messaggio gestito
                //int idCarrierUltimoSmsRicevutoRegistrato = GetIdCarrierUltimoSmsRicevutoRegistrato();
                foreach (ReceivedSms smsRicevuto in smsRicevuti)
                {
                    //if (smsRicevuto.IdCarrier > idCarrierUltimoSmsRicevutoRegistrato)
                    //{
                    //lo processo
                    try
                    {
                        //ripulisco il testo
                        smsRicevuto.Testo = smsRicevuto.Testo.Replace(" ", "").Trim();

                        //ora lo salvo su db in SmsRicevuti
                        smsRicevuto.IdSistema = RegistraSmsRicevuto(smsRicevuto);

                        if (smsRicevuto.IdSistema > 0)
                            MarcaComeLetto(smsRicevuto);

                        //lo gestisco
                        GestisciMessaggio(smsRicevuto);
                    }
                    catch (Exception ex)
                    {
                        //ACTIVITY TRACKING
                        //LogItemCollection logItemCollection = new LogItemCollection();
                        //logItemCollection.Add("Eccezione", ex.Message);
                        //Log.Write("Eccezione in: TBridge.Cemi.SmsInfo.Business.SmsBusiness.GestisciMessaggio", logItemCollection, Log.categorie.SMSINFO, Log.sezione.SYSTEMEXCEPTION);
                    }
                    //}
                }
            }
        }

        public void VerificaSmsRegistratiNonGestiti()
        {
            StoredReceivedSmsCollection smsRegistratiNonGestiti = GetListaSmsRicevutiRegistratiNonGestiti();
            if (smsRegistratiNonGestiti != null && smsRegistratiNonGestiti.Count > 0)
            {
                foreach (StoredReceivedSms smsRegistratoNonGestito in smsRegistratiNonGestiti)
                {
                    //lo processo
                    try
                    {
                        GestisciMessaggio(smsRegistratoNonGestito);
                    }
                    catch (Exception ex)
                    {
                        //ACTIVITY TRACKING
                        //LogItemCollection logItemCollection = new LogItemCollection();
                        //logItemCollection.Add("Eccezione", ex.Message);
                        //Log.Write("Eccezione in: TBridge.Cemi.SmsInfo.Business.SmsBusiness.GestisciMessaggio", logItemCollection, Log.categorie.SMSINFO, Log.sezione.SYSTEMEXCEPTION);
                    }
                }
            }
        }

        private void GestisciMessaggio(ReceivedSms smsRicevuto)
        {
            //ripulisco il testo
            smsRicevuto.Testo = smsRicevuto.Testo.Replace(" ", "").Trim();

            //ora lo salvo su db in SmsRicevuti
            //smsRicevuto.IdSistema = RegistraSmsRicevuto(smsRicevuto);

            // divido il testo in base al *
            // *codicefiscale = registrazione
            // *codicefiscale*stop = cancellazione
            // *codicefiscale*PIN = pin
            // *codicefiscale*LOGIN = credenziali
            // *TS*numero_taglia = richiesta scarpe - OLD: NON PIU' SUPPORTATO
            // *TS*SI o *TS*NO = feedback servizio TS
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());

            if (partiMessaggio.Length == 2)
            {
                //registrazione
                RegistraLavoratoreSms(smsRicevuto);
            }
            else if (partiMessaggio.Length == 3)
            {
                if (partiMessaggio[1].ToUpper() == "TS")
                {
                    //TS
                    RegistraRichiestaTs(smsRicevuto);
                }
                else if ((partiMessaggio[2]).ToUpper() == "STOP")
                {
                    //cancellazione
                    CancellaLavoratoreSms(smsRicevuto);
                }
                else if ((partiMessaggio[2]).ToUpper() == "PIN")
                {
                    //richiesta pin
                    FornisciPinLavoratore(smsRicevuto);
                }
                else if ((partiMessaggio[2]).ToUpper() == "LOGIN")
                {
                    //richiesta login
                    FornisciLoginLavoratore(smsRicevuto);
                }
            }
            else
            {
                //TODO: Avviso l'utente, mediante SMS, che vi � un errore nel formato
                SentSms sms = new SentSms();
                SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                SmsAddressee destinatario = new SmsAddressee();
                destinatario.Numero = smsRicevuto.Mittente;
                destinatari.Add(destinatario);
                sms.Destinatari = destinatari;
                sms.Tipo = SmsType.Immediato;
                sms.Data = DateTime.Now;
                sms.Testo = REGISTRAZIONE_ERROREFORMATO;
                InviaSms(sms);


                bool inCarico = true;
                bool corretto = false;
                string commento = REGISTRAZIONE_ERROREFORMATO;
                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);


                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("MessaggioInviato", REGISTRAZIONE_ERROREFORMATO);
                //Log.Write("Errore formato messaggio", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
            }
        }

        private void RegistraRichiestaTs(ReceivedSms smsRicevuto)
        {
            if (!smsRicevuto.Mittente.StartsWith("+39"))
                smsRicevuto.Mittente = "+39" + smsRicevuto.Mittente;

            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
            if ((partiMessaggio[1]).ToUpper() == "TS")
            {
                //Numero registrato al servizio?
                int idUtente = EsisteLavoratoreRegistratoByNumero(smsRicevuto.Mittente);
                if (idUtente > 0)
                {
                    //Registro richiesta - OLD
                    _smsDataAccess.RegistraSmsRichiestaTs(smsRicevuto, idUtente, partiMessaggio[2]);

                    //Registro feedback
                    //_smsDataAccess.RegistraSmsFeedbackTs(smsRicevuto, idUtente, partiMessaggio[2]);

                    //Inviare SMS??

                    //Aggiorno stato SMS ricevuto
                    bool inCarico = true;
                    bool corretto = true;
                    string commento = RICHIESTATS;
                    _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);
                }
                else
                {
                    //Lavoratore non registrato!!
                    SentSms sms = new SentSms();
                    SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                    SmsAddressee destinatario = new SmsAddressee();
                    destinatario.Numero = smsRicevuto.Mittente;
                    destinatari.Add(destinatario);
                    sms.Destinatari = destinatari;
                    sms.Tipo = SmsType.Immediato;
                    sms.Data = DateTime.Now;
                    sms.Testo = REGISTRAZIONE_LAVORATORENONPRESENTE;
                    InviaSms(sms);


                    bool inCarico = true;
                    bool corretto = false;
                    string commento = REGISTRAZIONE_LAVORATORENONPRESENTE;
                    _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);
                }
            }
            else
            {
                //Formato non corretto
                SentSms sms = new SentSms();
                SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                SmsAddressee destinatario = new SmsAddressee();
                destinatario.Numero = smsRicevuto.Mittente;
                destinatari.Add(destinatario);
                sms.Destinatari = destinatari;
                sms.Tipo = SmsType.Immediato;
                sms.Data = DateTime.Now;
                sms.Testo = REGISTRAZIONE_ERROREFORMATO;
                InviaSms(sms);


                bool inCarico = true;
                bool corretto = false;
                string commento = REGISTRAZIONE_ERROREFORMATO;
                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);
            }
        }

        private int EsisteLavoratoreRegistratoByNumero(string numeroMittente)
        {
            return _smsDataAccess.EsisteLavoratoreRegistratoByNumero(numeroMittente);
        }

        private void FornisciLoginLavoratore(ReceivedSms smsRicevuto)
        {
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
            if ((partiMessaggio[2]).ToUpper() == "LOGIN")
            {
                //Verifico prima CF
                if (CFValido(partiMessaggio[1], false))
                {
                    //Il CF sembra corretto. Faccio gli ulteriori controlli

                    //Utente presente come lavoratore?
                    int idUtente = EsisteLavoratore(partiMessaggio[1]);
                    if (idUtente > 0)
                    {
                        //Registrato al servizio?
                        if (EsisteLavoratoreSmsNumero(idUtente, smsRicevuto))
                        {
                            GestioneUtentiBiz gu = new GestioneUtentiBiz();
                            int idLavoratore = GetIdLavoratore(partiMessaggio[1]);

                            LavoratoreCredential lavoratoreCredential = gu.GetLavoratoreCredential(idLavoratore);

                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;

                            if (!String.IsNullOrEmpty(lavoratoreCredential.Password))
                            {
                                string username = lavoratoreCredential.Username;
                                string password = lavoratoreCredential.Password;
                                sms.Testo = String.Format(LOGINLAVORATORE, username, password);
                                InviaSms(sms);

                                bool inCarico = true;
                                bool corretto = true;
                                string commento = "Restituzione login";
                                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);
                            }
                            else
                            {
                                sms.Testo = String.Format(LOGINLAVORATOREASSENTE);
                                InviaSms(sms);

                                bool inCarico = true;
                                bool corretto = false;
                                string commento = "LOGINLAVORATOREASSENTE";
                                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);
                            }
                        }
                        else
                        {
                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = CANCELLAZIONE_LAVORATORENONPRESENTE;
                            InviaSms(sms);


                            bool inCarico = true;
                            bool corretto = false;
                            string commento = CANCELLAZIONE_LAVORATORENONPRESENTE;
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_LAVORATORENONPRESENTE);
                            //Log.Write("Generazione PIN Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                        }
                    }
                    else
                    {
                        SentSms sms = new SentSms();
                        SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                        SmsAddressee destinatario = new SmsAddressee();
                        destinatario.Numero = smsRicevuto.Mittente;
                        destinatari.Add(destinatario);
                        sms.Destinatari = destinatari;
                        sms.Tipo = SmsType.Immediato;
                        sms.Data = DateTime.Now;
                        sms.Testo = CANCELLAZIONE_LAVORATORENONPRESENTE;
                        InviaSms(sms);


                        bool inCarico = true;
                        bool corretto = false;
                        string commento = CANCELLAZIONE_LAVORATORENONPRESENTE;
                        _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                        //ACTIVITY TRACKING
                        //LogItemCollection logItemCollection = new LogItemCollection();
                        //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_LAVORATORENONPRESENTE);
                        //Log.Write("Generazione PIN Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                    }
                }
                else
                {
                    //Avviso l'utente, mediante SMS, che vi � un errore nel CF
                    SentSms sms = new SentSms();
                    SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                    SmsAddressee destinatario = new SmsAddressee();
                    destinatario.Numero = smsRicevuto.Mittente;
                    destinatari.Add(destinatario);
                    sms.Destinatari = destinatari;
                    sms.Tipo = SmsType.Immediato;
                    sms.Data = DateTime.Now;
                    sms.Testo = CANCELLAZIONE_ERRORECF;
                    InviaSms(sms);


                    bool inCarico = true;
                    bool corretto = false;
                    string commento = CANCELLAZIONE_ERRORECF;
                    _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                    //ACTIVITY TRACKING
                    //LogItemCollection logItemCollection = new LogItemCollection();
                    //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ERRORECF);
                    //Log.Write("Generazione PIN Lavoratore CF Errato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                }
            }
            else
            {
                //Avviso l'utente, mediante SMS, che vi � un errore nel formato
                SentSms sms = new SentSms();
                SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                SmsAddressee destinatario = new SmsAddressee();
                destinatario.Numero = smsRicevuto.Mittente;
                destinatari.Add(destinatario);
                sms.Destinatari = destinatari;
                sms.Tipo = SmsType.Immediato;
                sms.Data = DateTime.Now;
                sms.Testo = CANCELLAZIONE_ERROREFORMATO;
                InviaSms(sms);


                bool inCarico = true;
                bool corretto = false;
                string commento = CANCELLAZIONE_ERROREFORMATO;
                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ERROREFORMATO);
                //Log.Write("Generazione PIN errore formato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
            }
        }

        private int RegistraSmsRicevuto(ReceivedSms smsRicevuto)
        {
            int idSistema = _smsDataAccess.RegistraSmsRicevuto(smsRicevuto);
            return idSistema;
        }

        private bool MarcaComeLetto(ReceivedSms smsRicevuto)
        {
            return _smsCarrier.SetAck(smsRicevuto.IdCarrier);
        }

        private int GetIdCarrierUltimoSmsRicevutoRegistrato()
        {
            int idCarrierUltimoSmsGestito = _smsDataAccess.GetIdCarrierUltimoSmsRicevutoRegistrato();
            return idCarrierUltimoSmsGestito;
        }

        public void RegistraLavoratoreSms(ReceivedSms smsRicevuto)
        {
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
            if (CFValido(partiMessaggio[1], false))
            {
                //Il CF sembra corretto. Faccio gli ulteriori controlli

                //Utente presente come lavoratore?
                int idUtente = EsisteLavoratore(partiMessaggio[1]);
                if (idUtente > 0)
                {
                    //verifico che il numero non sia gi� attivo per altri...
                    if (NumeroTelefonoDisponibile(smsRicevuto.Mittente))
                    {
                        //Registrato al servizio?
                        if (EsisteLavoratoreSms(idUtente))
                        {
                            //TODO: Mandare sms a vecchio recapito???

                            //Vecchio recapito non pi� attivo, inserisco quello nuovo
                            //DisabilitaRecapiti(idUtente);  //credo non serva pi�

                            InserisciRecapito(idUtente, smsRicevuto);


                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = REGISTRAZIONE_AGGIORNATO;
                            InviaSms(sms);


                            bool inCarico = true;
                            bool corretto = true;
                            string commento = REGISTRAZIONE_AGGIORNATO;
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", REGISTRAZIONE_AGGIORNATO);
                            //Log.Write("Lavoratore SMS registrato/aggiornato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGGING);
                        }
                        else
                        {
                            //Inserisco recapito
                            InserisciRecapito(idUtente, smsRicevuto);

                            //Assegno ruolo
                            GestioneUtentiBiz.AssegnaRuoloLavoratoreSMS(idUtente);


                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = REGISTRAZIONE_BENVENUTO;
                            InviaSms(sms);


                            bool inCarico = true;
                            bool corretto = true;
                            string commento = REGISTRAZIONE_BENVENUTO;
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", REGISTRAZIONE_BENVENUTO);
                            //Log.Write("Lavoratore registrato al servizio SMS", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGGING);
                        }
                    }
                    else
                    {
                        SentSms sms = new SentSms();
                        SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                        SmsAddressee destinatario = new SmsAddressee();
                        destinatario.Numero = smsRicevuto.Mittente;
                        destinatari.Add(destinatario);
                        sms.Destinatari = destinatari;
                        sms.Tipo = SmsType.Immediato;
                        sms.Data = DateTime.Now;
                        sms.Testo = REGISTRAZIONE_NUMEROGIAATTIVO;
                        InviaSms(sms);


                        bool inCarico = true;
                        bool corretto = false;
                        string commento = REGISTRAZIONE_NUMEROGIAATTIVO;
                        _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);
                    }
                }
                else
                {
                    SentSms sms = new SentSms();
                    SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                    SmsAddressee destinatario = new SmsAddressee();
                    destinatario.Numero = smsRicevuto.Mittente;
                    destinatari.Add(destinatario);
                    sms.Destinatari = destinatari;
                    sms.Tipo = SmsType.Immediato;
                    sms.Data = DateTime.Now;
                    sms.Testo = REGISTRAZIONE_LAVORATORENONPRESENTE;
                    InviaSms(sms);


                    bool inCarico = true;
                    bool corretto = false;
                    string commento = REGISTRAZIONE_LAVORATORENONPRESENTE;
                    _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                    //ACTIVITY TRACKING
                    //LogItemCollection logItemCollection = new LogItemCollection();
                    //logItemCollection.Add("MessaggioInviato", REGISTRAZIONE_LAVORATORENONPRESENTE);
                    //Log.Write("Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                }
            }
            else
            {
                //CF non valido
                SentSms sms = new SentSms();
                SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                SmsAddressee destinatario = new SmsAddressee();
                destinatario.Numero = smsRicevuto.Mittente;
                destinatari.Add(destinatario);
                sms.Destinatari = destinatari;
                sms.Tipo = SmsType.Immediato;
                sms.Data = DateTime.Now;
                sms.Testo = REGISTRAZIONE_ERRORECF;
                InviaSms(sms);


                bool inCarico = true;
                bool corretto = false;
                string commento = REGISTRAZIONE_ERRORECF;
                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("MessaggioInviato", REGISTRAZIONE_ERRORECF);
                //Log.Write("Errore formato codice fiscale", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
            }
        }

        public void CancellaLavoratoreSms(ReceivedSms smsRicevuto)
        {
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
            if ((partiMessaggio[2]).ToUpper() == "STOP")
            {
                //Verifico prima CF
                if (CFValido(partiMessaggio[1], false))
                {
                    //Il CF sembra corretto. Faccio gli ulteriori controlli

                    //Utente presente come lavoratore?
                    int idUtente = EsisteLavoratore(partiMessaggio[1]);
                    if (idUtente > 0)
                    {
                        //Registrato al servizio?
                        if (EsisteLavoratoreSmsNumero(idUtente, smsRicevuto))
                        {
                            //DisabilitaRecapiti(idUtente);
                            LavoratoriManager lavoratoriManager = new LavoratoriManager();
                            lavoratoriManager.DisabilitaRecapito(idUtente);

                            GestioneUtentiBiz.DisassociaRuoloLavoratoreSMS(idUtente);


                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = CANCELLAZIONE_ARRIVEDERCI;
                            InviaSms(sms);

                            bool inCarico = true;
                            bool corretto = true;
                            string commento = CANCELLAZIONE_ARRIVEDERCI;
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ARRIVEDERCI);
                            //Log.Write("Lavoratore SMS Cancellato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGGING);
                        }
                        else
                        {
                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = CANCELLAZIONE_LAVORATORENONPRESENTE;
                            InviaSms(sms);


                            bool inCarico = true;
                            bool corretto = false;
                            string commento = CANCELLAZIONE_LAVORATORENONPRESENTE;
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_LAVORATORENONPRESENTE);
                            //Log.Write("Cancellazione Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                        }
                    }
                    else
                    {
                        SentSms sms = new SentSms();
                        SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                        SmsAddressee destinatario = new SmsAddressee();
                        destinatario.Numero = smsRicevuto.Mittente;
                        destinatari.Add(destinatario);
                        sms.Destinatari = destinatari;
                        sms.Tipo = SmsType.Immediato;
                        sms.Data = DateTime.Now;
                        sms.Testo = CANCELLAZIONE_LAVORATORENONPRESENTE;
                        InviaSms(sms);


                        bool inCarico = true;
                        bool corretto = false;
                        string commento = CANCELLAZIONE_LAVORATORENONPRESENTE;
                        _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                        //ACTIVITY TRACKING
                        //LogItemCollection logItemCollection = new LogItemCollection();
                        //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_LAVORATORENONPRESENTE);
                        //Log.Write("Cancellazione Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                    }
                }
                else
                {
                    //Avviso l'utente, mediante SMS, che vi � un errore nel CF
                    SentSms sms = new SentSms();
                    SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                    SmsAddressee destinatario = new SmsAddressee();
                    destinatario.Numero = smsRicevuto.Mittente;
                    destinatari.Add(destinatario);
                    sms.Destinatari = destinatari;
                    sms.Tipo = SmsType.Immediato;
                    sms.Data = DateTime.Now;
                    sms.Testo = CANCELLAZIONE_ERRORECF;
                    InviaSms(sms);


                    bool inCarico = true;
                    bool corretto = false;
                    string commento = CANCELLAZIONE_ERRORECF;
                    _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                    //ACTIVITY TRACKING
                    //LogItemCollection logItemCollection = new LogItemCollection();
                    //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ERRORECF);
                    //Log.Write("Cancellazione Lavoratore CF Errato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                }
            }
            else
            {
                //Avviso l'utente, mediante SMS, che vi � un errore nel formato
                SentSms sms = new SentSms();
                SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                SmsAddressee destinatario = new SmsAddressee();
                destinatario.Numero = smsRicevuto.Mittente;
                destinatari.Add(destinatario);
                sms.Destinatari = destinatari;
                sms.Tipo = SmsType.Immediato;
                sms.Data = DateTime.Now;
                sms.Testo = CANCELLAZIONE_ERROREFORMATO;
                InviaSms(sms);


                bool inCarico = true;
                bool corretto = false;
                string commento = CANCELLAZIONE_ERROREFORMATO;
                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ERROREFORMATO);
                //Log.Write("Cancellazione errore formato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
            }
        }

        public void FornisciPinLavoratore(ReceivedSms smsRicevuto)
        {
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
            if ((partiMessaggio[2]).ToUpper() == "PIN")
            {
                //Verifico prima CF
                if (CFValido(partiMessaggio[1], false))
                {
                    //Il CF sembra corretto. Faccio gli ulteriori controlli

                    //Utente presente come lavoratore?
                    int idUtente = EsisteLavoratore(partiMessaggio[1]);
                    if (idUtente > 0)
                    {
                        //Registrato al servizio?
                        if (EsisteLavoratoreSmsNumero(idUtente, smsRicevuto))
                        {
                            GestioneUtentiBiz gu = new GestioneUtentiBiz();
                            int idLavoratore = GetIdLavoratore(partiMessaggio[1]);
                            string pin = gu.GeneraPinLavoratore(idLavoratore);

                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = String.Format(PINLAVORATORE, pin);
                            InviaSms(sms);

                            bool inCarico = true;
                            bool corretto = true;
                            string commento = "Generazione pin";
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ARRIVEDERCI);
                            //Log.Write("Pin lavoratore generato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGGING);
                        }
                        else
                        {
                            SentSms sms = new SentSms();
                            SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                            SmsAddressee destinatario = new SmsAddressee();
                            destinatario.Numero = smsRicevuto.Mittente;
                            destinatari.Add(destinatario);
                            sms.Destinatari = destinatari;
                            sms.Tipo = SmsType.Immediato;
                            sms.Data = DateTime.Now;
                            sms.Testo = CANCELLAZIONE_LAVORATORENONPRESENTE;
                            InviaSms(sms);


                            bool inCarico = true;
                            bool corretto = false;
                            string commento = CANCELLAZIONE_LAVORATORENONPRESENTE;
                            _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                            //ACTIVITY TRACKING
                            //LogItemCollection logItemCollection = new LogItemCollection();
                            //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_LAVORATORENONPRESENTE);
                            //Log.Write("Generazione PIN Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                        }
                    }
                    else
                    {
                        SentSms sms = new SentSms();
                        SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                        SmsAddressee destinatario = new SmsAddressee();
                        destinatario.Numero = smsRicevuto.Mittente;
                        destinatari.Add(destinatario);
                        sms.Destinatari = destinatari;
                        sms.Tipo = SmsType.Immediato;
                        sms.Data = DateTime.Now;
                        sms.Testo = CANCELLAZIONE_LAVORATORENONPRESENTE;
                        InviaSms(sms);


                        bool inCarico = true;
                        bool corretto = false;
                        string commento = CANCELLAZIONE_LAVORATORENONPRESENTE;
                        _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                        //ACTIVITY TRACKING
                        //LogItemCollection logItemCollection = new LogItemCollection();
                        //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_LAVORATORENONPRESENTE);
                        //Log.Write("Generazione PIN Lavoratore non presente", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                    }
                }
                else
                {
                    //Avviso l'utente, mediante SMS, che vi � un errore nel CF
                    SentSms sms = new SentSms();
                    SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                    SmsAddressee destinatario = new SmsAddressee();
                    destinatario.Numero = smsRicevuto.Mittente;
                    destinatari.Add(destinatario);
                    sms.Destinatari = destinatari;
                    sms.Tipo = SmsType.Immediato;
                    sms.Data = DateTime.Now;
                    sms.Testo = CANCELLAZIONE_ERRORECF;
                    InviaSms(sms);


                    bool inCarico = true;
                    bool corretto = false;
                    string commento = CANCELLAZIONE_ERRORECF;
                    _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                    //ACTIVITY TRACKING
                    //LogItemCollection logItemCollection = new LogItemCollection();
                    //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ERRORECF);
                    //Log.Write("Generazione PIN Lavoratore CF Errato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
                }
            }
            else
            {
                //Avviso l'utente, mediante SMS, che vi � un errore nel formato
                SentSms sms = new SentSms();
                SmsAddresseeCollection destinatari = new SmsAddresseeCollection();
                SmsAddressee destinatario = new SmsAddressee();
                destinatario.Numero = smsRicevuto.Mittente;
                destinatari.Add(destinatario);
                sms.Destinatari = destinatari;
                sms.Tipo = SmsType.Immediato;
                sms.Data = DateTime.Now;
                sms.Testo = CANCELLAZIONE_ERROREFORMATO;
                InviaSms(sms);


                bool inCarico = true;
                bool corretto = false;
                string commento = CANCELLAZIONE_ERROREFORMATO;
                _smsDataAccess.AggiornaSmsRicevuto(smsRicevuto, inCarico, corretto, commento);

                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("MessaggioInviato", CANCELLAZIONE_ERROREFORMATO);
                //Log.Write("Generazione PIN errore formato", logItemCollection, Log.categorie.SMSINFO, Log.sezione.LOGICEXCEPTION);
            }
        }

        private bool EsisteLavoratoreSmsNumero(int idUtente, ReceivedSms smsRicevuto)
        {
            bool ret;

            ret = _smsDataAccess.EsisteLavoratoreSmsNumero(idUtente, smsRicevuto);

            return ret;
        }

        private int EsisteLavoratore(string codiceFiscale)
        {
            int idUtente;

            idUtente = _smsDataAccess.EsisteLavoratore(codiceFiscale);

            return idUtente;
        }

        private int GetIdLavoratore(string codiceFiscale)
        {
            int idLav;

            idLav = _smsDataAccess.GetIdLavoratore(codiceFiscale);

            return idLav;
        }

        private bool EsisteLavoratoreSms(int idUtente)
        {
            bool ret;

            ret = _smsDataAccess.EsisteLavoratoreSms(idUtente);

            return ret;
        }

        private bool NumeroTelefonoDisponibile(string mittente)
        {
            return _smsDataAccess.NumeroTelefonoDisponibile(mittente);
        }

        private void InserisciRecapito(int idUtente, ReceivedSms smsRicevuto)
        {
            _smsDataAccess.InserisciRecapito(idUtente, smsRicevuto);
        }

        #endregion

        #region Invio SMS

        public void InviaSms(SentSms sms)
        {
            try
            {
                sms.IdCarrier = _smsCarrier.RegistraSms(sms);

                if (sms.IdCarrier > 0)
                {
                    if (sms.Tipo == SmsType.Immediato)
                        sms.IdSistema = _smsDataAccess.RegistraSms(sms);
                    else if (sms.Tipo == SmsType.AScadenza)
                        _smsDataAccess.AggiornaSmsAScadenza(sms);

                    sms.IdInvioCarrier = _smsCarrier.InviaSms(sms);

                    if (sms.IdInvioCarrier > 0)
                    {
                        //registra il tutto su db
                        sms.IdInvioSistema = _smsDataAccess.RegistraSmsInviato(sms);


                        //per CRM
                        SmsManager.CreaSR(sms);
                    }
                    else
                    {
                        using (EventLog appLog = new EventLog())
                        {
                            appLog.Source = "SiceInfo";
                            appLog.WriteEntry("Messaggio NON inviato - Errore Carrier in fase di invio");
                        }
                    }
                }
                else
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry("Messaggio NON inviato - Errore Carrier in fase di registrazione SMS");
                    }
                }
            }
            catch (Exception e)
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione SmsInfo: {0}", e.Message));
                }
            }
        }

        public void VerificaSmsInInvio()
        {
            //Verifico se vi sono messaggi da inviare
            SmsCollection smsDaInviare = _smsDataAccess.GetSmsDaInviare();

            if (smsDaInviare != null && smsDaInviare.Count > 0)
            {
                foreach (SentSms sms in smsDaInviare)
                {
                    InviaSms(sms);
                }
            }
        }

        #endregion

        public SmsCollection GetListaSmsRicevutiCarrier()
        {
            return _smsCarrier.GetListaSmsRicevuti();
        }

        public StoredReceivedSmsCollection GetListaSmsRicevutiRegistrati()
        {
            return _smsDataAccess.GetSmsRicevutiRegistrati();
        }

        public SmsCollection GetListaSmsInviatiCarrier()
        {
            return _smsCarrier.GetListaSmsInviati();
        }

        public SmsCollection GetListaSmsInviatiRegistrati()
        {
            return _smsDataAccess.GetSmsInviatiRegistrati();
        }

        public StoredReceivedSmsCollection GetListaSmsRicevutiRegistratiNonGestiti()
        {
            return _smsDataAccess.GetSmsRicevutiRegistratiNonGestiti();
        }

        ///<summary>
        ///  CONTROLLO CODICE FISCALE
        ///</summary>
        ///<param name = "cfins">codice fiscale da validare</param>
        ///<param name = "p_strict">true attiva il controllo sull'ultimo carattere:
        ///  "dalle infromazioni che ho avuto il controllo pu� fallire quando l'ultima lettera
        ///  � stata usata per gestire i casi di omonimia
        ///  p_strict == false controlla solo la regolarit� dei codici
        ///  con una regular expression - in genere si tiene sempre a false per i CF</param>
        ///<returns></returns>
        [Obsolete("da eliminare")]
        public static bool CFValido(string cfins, bool p_strict)
        {
            string cf = cfins.ToUpper();
            string cfReg = @"^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]";

            if (Regex.Match(cf, cfReg).Success == false) return false;
            else if (p_strict == false) return true;

            string set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";

            int s = 0;
            for (int i = 1; i <= 13; i += 2)
                s += setpari.IndexOf(set2[set1.IndexOf(cf[i])]);

            for (int i = 0; i <= 14; i += 2)
                s += setdisp.IndexOf(set2[set1.IndexOf(cf[i])]);

            if (s%26 != cf[15] - 'A') return false;

            return true;
        }

        public StoredReceivedSmsCollection GetListaSmsRicevutiNascosti()
        {
            return _smsDataAccess.GetSmsRicevutiRegistratiNascosti();
        }
        
        #region AccessoCantieri

        public int InviaSmsAccessoCantieri(string testo, List<string> numeriDestinatari)
        {
            int ret = -1;

            //TODO: PORCATA!! servono poche info (testo, tipo, idcarrier, etc.) quindi uso il sentsms solo per questo, facendo attenzione!!!
            SentSms sms = new SentSms
                              {
                                  Testo = testo,
                                  Tipo = SmsType.Immediato,
                                  Data = DateTime.Now,
                                  Destinatari = new SmsAddresseeCollection()
                              };

            foreach (string destinatario in numeriDestinatari)
            {
                SmsAddressee addressee = new SmsAddressee();
                addressee.Numero = destinatario;
                sms.Destinatari.Add(addressee);
            }

            sms.IdCarrier = _smsCarrier.RegistraSms(sms);

            if (sms.IdCarrier > 0)
            {
                sms.IdSistema = _smsDataAccess.RegistraSms(sms);

                sms.IdInvioCarrier = _smsCarrier.InviaSms(sms);

                if (sms.IdInvioCarrier > 0)
                {
                    //registra il tutto su db
                    sms.IdInvioSistema = _smsDataAccess.RegistraSmsInviato(sms);

                    ret = sms.IdSistema;
                }
            }

            return ret;
        }

        #endregion
    }
}