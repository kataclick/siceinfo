﻿namespace TBridge.Cemi.Crm.WinApp
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerDataLiq = new System.Windows.Forms.DateTimePicker();
            this.buttonOk = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.textBoxQuery = new System.Windows.Forms.TextBox();
            this.checkBoxRicercaLibera = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // dateTimePickerDataLiq
            // 
            this.dateTimePickerDataLiq.Location = new System.Drawing.Point(12, 15);
            this.dateTimePickerDataLiq.Name = "dateTimePickerDataLiq";
            this.dateTimePickerDataLiq.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerDataLiq.TabIndex = 0;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(13, 143);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(199, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxLog.Location = new System.Drawing.Point(13, 172);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(521, 254);
            this.richTextBoxLog.TabIndex = 2;
            this.richTextBoxLog.Text = "";
            // 
            // textBoxQuery
            // 
            this.textBoxQuery.Enabled = false;
            this.textBoxQuery.Location = new System.Drawing.Point(13, 41);
            this.textBoxQuery.Multiline = true;
            this.textBoxQuery.Name = "textBoxQuery";
            this.textBoxQuery.Size = new System.Drawing.Size(521, 96);
            this.textBoxQuery.TabIndex = 3;
            // 
            // checkBoxRicercaLibera
            // 
            this.checkBoxRicercaLibera.AutoSize = true;
            this.checkBoxRicercaLibera.Location = new System.Drawing.Point(269, 15);
            this.checkBoxRicercaLibera.Name = "checkBoxRicercaLibera";
            this.checkBoxRicercaLibera.Size = new System.Drawing.Size(82, 17);
            this.checkBoxRicercaLibera.TabIndex = 4;
            this.checkBoxRicercaLibera.Text = "Query libera";
            this.checkBoxRicercaLibera.UseVisualStyleBackColor = true;
            this.checkBoxRicercaLibera.CheckedChanged += new System.EventHandler(this.checkBoxRicercaLibera_CheckedChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 438);
            this.Controls.Add(this.checkBoxRicercaLibera);
            this.Controls.Add(this.textBoxQuery);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.dateTimePickerDataLiq);
            this.MinimumSize = new System.Drawing.Size(326, 254);
            this.Name = "FormMain";
            this.Text = "CRM - Prestazioni";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerDataLiq;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.TextBox textBoxQuery;
        private System.Windows.Forms.CheckBox checkBoxRicercaLibera;
    }
}

