using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using TBridge.Cemi.Business.Crm;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Crm.WinApp
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();

            dateTimePickerDataLiq.Value = DateTime.Today;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            List<PrestazioneCrm> listaPrestazioni = new List<PrestazioneCrm>();

            string query;
            if (!this.checkBoxRicercaLibera.Checked)
                query = "SELECT * FROM dbo.crm_prestazioni WHERE dataLiquidazione = @dataLiquidazione";
            else
                query = textBoxQuery.Text;

            DateTime dataLiquidazione = dateTimePickerDataLiq.Value.Date;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    if (!this.checkBoxRicercaLibera.Checked)
                        command.Parameters.AddWithValue("@dataLiquidazione", dataLiquidazione);

                    connection.Open();
                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader != null)
                        {
                            int idTipoPrestazioneIndex = dataReader.GetOrdinal("idTipoPrestazione");
                            int protocolloPrestazioneIndex = dataReader.GetOrdinal("protocolloPrestazione");
                            int numeroProtocolloPrestazioneIndex = dataReader.GetOrdinal("numeroProtocolloPrestazione");
                            int descrizioneIndex = dataReader.GetOrdinal("descrizione");
                            int beneficiarioIndex = dataReader.GetOrdinal("beneficiario");
                            int dataDomandaIndex = dataReader.GetOrdinal("dataDomanda");
                            int dataRiferimentoIndex = dataReader.GetOrdinal("dataRiferimento");
                            int idLavoratoreIndex = dataReader.GetOrdinal("idLavoratore");
                            int idFamiliareIndex = dataReader.GetOrdinal("idFamiliare");
                            int statoIndex = dataReader.GetOrdinal("stato");
                            int causaleRespintaIndex = dataReader.GetOrdinal("causaleRespinta");
                            int lavoratoreIndirizzoIndex = dataReader.GetOrdinal("lavoratoreIndirizzo");
                            int lavoratoreProvinciaIndex = dataReader.GetOrdinal("lavoratoreProvincia");
                            int lavoratoreComuneIndex = dataReader.GetOrdinal("lavoratoreComune");
                            int lavoratoreFrazioneIndex = dataReader.GetOrdinal("lavoratoreFrazione");
                            int lavoratoreCapIndex = dataReader.GetOrdinal("lavoratoreCap");
                            int lavoratoreCellulareIndex = dataReader.GetOrdinal("lavoratoreCellulare");
                            int lavoratoreEmailIndex = dataReader.GetOrdinal("lavoratoreEmail");
                            int lavoratoreIdTipoPagamentoIndex = dataReader.GetOrdinal("lavoratoreIdTipoPagamento");
                            int familiareCognomeIndex = dataReader.GetOrdinal("familiareCognome");
                            int familiareNomeIndex = dataReader.GetOrdinal("familiareNome");
                            int familiareDataNascitaIndex = dataReader.GetOrdinal("familiareDataNascita");
                            int familiareCodiceFiscaleIndex = dataReader.GetOrdinal("familiareCodiceFiscale");
                            int gradoParentelaIndex = dataReader.GetOrdinal("gradoParentela");
                            int importoErogatoLordoIndex = dataReader.GetOrdinal("importoErogatoLordo");
                            int importoErogatoIndex = dataReader.GetOrdinal("importoErogato");
                            int modalitaPagamentoIndex = dataReader.GetOrdinal("modalitaPagamento");
                            int numeroMandatoIndex = dataReader.GetOrdinal("numeroMandato");
                            int statoAssegnoIndex = dataReader.GetOrdinal("statoAssegno");
                            int descrizioneStatoAssegnoIndex = dataReader.GetOrdinal("descrizioneStatoAssegno");
                            int dataLiquidazioneIndex = dataReader.GetOrdinal("dataLiquidazione");
                            int idTipoPagamentoIndex = dataReader.GetOrdinal("idTipoPagamento");
                            int ibanIndex = dataReader.GetOrdinal("IBAN");
                            int testoGruppoIndex = dataReader.GetOrdinal("testoGruppo");

                            while (dataReader.Read())
                            {
                                PrestazioneCrm prestazioneCrm = new PrestazioneCrm();

                                // chiavi
                                prestazioneCrm.IdTipoPrestazione = dataReader.GetString(idTipoPrestazioneIndex);
                                prestazioneCrm.ProtocolloPrestazione = dataReader.GetInt32(protocolloPrestazioneIndex);
                                prestazioneCrm.NumeroProtocolloPrestazione = dataReader.GetInt32(numeroProtocolloPrestazioneIndex);

                                if (dataReader[beneficiarioIndex] != DBNull.Value)
                                    prestazioneCrm.Beneficiario = dataReader.GetString(beneficiarioIndex);
                                if (dataReader[causaleRespintaIndex] != DBNull.Value)
                                    prestazioneCrm.CausaleRespinta = dataReader.GetString(causaleRespintaIndex);
                                if (dataReader[dataDomandaIndex] != DBNull.Value)
                                    prestazioneCrm.DataDomanda = dataReader.GetDateTime(dataDomandaIndex);
                                if (dataReader[dataLiquidazioneIndex] != DBNull.Value)
                                    prestazioneCrm.DataLiquidazione = dataReader.GetDateTime(dataLiquidazioneIndex);
                                if (dataReader[dataRiferimentoIndex] != DBNull.Value)
                                    prestazioneCrm.DataRiferimento = dataReader.GetDateTime(dataRiferimentoIndex);
                                if (dataReader[descrizioneIndex] != DBNull.Value)
                                    prestazioneCrm.Descrizione = dataReader.GetString(descrizioneIndex);
                                if (dataReader[descrizioneStatoAssegnoIndex] != DBNull.Value)
                                    prestazioneCrm.DescrizioneStatoAssegno = dataReader.GetString(descrizioneStatoAssegnoIndex);
                                if (dataReader[familiareCodiceFiscaleIndex] != DBNull.Value)
                                    prestazioneCrm.FamiliareCodiceFiscale = dataReader.GetString(familiareCodiceFiscaleIndex);
                                if (dataReader[familiareCognomeIndex] != DBNull.Value)
                                    prestazioneCrm.FamiliareCognome = dataReader.GetString(familiareCognomeIndex);
                                if (dataReader[familiareDataNascitaIndex] != DBNull.Value)
                                    prestazioneCrm.FamiliareDataNascita = dataReader.GetDateTime(familiareDataNascitaIndex);
                                if (dataReader[familiareNomeIndex] != DBNull.Value)
                                    prestazioneCrm.FamiliareNome = dataReader.GetString(familiareNomeIndex);
                                if (dataReader[gradoParentelaIndex] != DBNull.Value)
                                    prestazioneCrm.GradoParentela = dataReader.GetString(gradoParentelaIndex);
                                if (dataReader[ibanIndex] != DBNull.Value)
                                    prestazioneCrm.Iban = dataReader.GetString(ibanIndex);
                                if (dataReader[idFamiliareIndex] != DBNull.Value)
                                    prestazioneCrm.IdFamiliare = dataReader.GetInt32(idFamiliareIndex);
                                if (dataReader[idLavoratoreIndex] != DBNull.Value)
                                    prestazioneCrm.IdLavoratore = dataReader.GetInt32(idLavoratoreIndex);
                                if (dataReader[idTipoPagamentoIndex] != DBNull.Value)
                                    prestazioneCrm.IdTipoPagamento = dataReader.GetString(idTipoPagamentoIndex);
                                if (dataReader[importoErogatoIndex] != DBNull.Value)
                                    prestazioneCrm.ImportoErogato = dataReader.GetDecimal(importoErogatoIndex);
                                if (dataReader[importoErogatoLordoIndex] != DBNull.Value)
                                    prestazioneCrm.ImportoErogatoLordo = dataReader.GetDecimal(importoErogatoLordoIndex);
                                if (dataReader[lavoratoreCapIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreCap = dataReader.GetString(lavoratoreCapIndex);
                                if (dataReader[lavoratoreCellulareIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreCellulare = dataReader.GetString(lavoratoreCellulareIndex);
                                if (dataReader[lavoratoreComuneIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreComune = dataReader.GetString(lavoratoreComuneIndex);
                                if (dataReader[lavoratoreEmailIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreEmail = dataReader.GetString(lavoratoreEmailIndex);
                                if (dataReader[lavoratoreFrazioneIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreFrazione = dataReader.GetString(lavoratoreFrazioneIndex);
                                if (dataReader[lavoratoreIdTipoPagamentoIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreIdTipoPagamento = dataReader.GetString(lavoratoreIdTipoPagamentoIndex);
                                if (dataReader[lavoratoreIndirizzoIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreIndirizzo = dataReader.GetString(lavoratoreIndirizzoIndex);
                                if (dataReader[lavoratoreProvinciaIndex] != DBNull.Value)
                                    prestazioneCrm.LavoratoreProvincia = dataReader.GetString(lavoratoreProvinciaIndex);
                                if (dataReader[modalitaPagamentoIndex] != DBNull.Value)
                                    prestazioneCrm.ModalitaPagamento = dataReader.GetString(modalitaPagamentoIndex);
                                if (dataReader[numeroMandatoIndex] != DBNull.Value)
                                    prestazioneCrm.NumeroMandato = dataReader.GetString(numeroMandatoIndex);
                                if (dataReader[statoIndex] != DBNull.Value)
                                    prestazioneCrm.Stato = dataReader.GetString(statoIndex);
                                if (dataReader[statoAssegnoIndex] != DBNull.Value)
                                    prestazioneCrm.StatoAssegno = dataReader.GetString(statoAssegnoIndex);
                                if (dataReader[testoGruppoIndex] != DBNull.Value)
                                    prestazioneCrm.TestoGruppo = dataReader.GetString(testoGruppoIndex);

                                listaPrestazioni.Add(prestazioneCrm);
                                richTextBoxLog.AppendText(string.Format("{0} - {1}; {2}; {3} - caricato{4}", DateTime.Now,
                                                                        prestazioneCrm.IdTipoPrestazione,
                                                                        prestazioneCrm.NumeroProtocolloPrestazione,
                                                                        prestazioneCrm.ProtocolloPrestazione, Environment.NewLine));
                            }
                        }
                    }
                }
            }

            richTextBoxLog.AppendText(string.Format("{0} - Totale prestazioni caricate: {1}{2}", DateTime.Now,
                                                    listaPrestazioni.Count, Environment.NewLine));

            //chiamo il ws
            string result = PrestazioniManager.NotificaPrestazione(listaPrestazioni);
            richTextBoxLog.AppendText(string.Format("{0} - risposta: {1}{2}", DateTime.Now, result, Environment.NewLine));
        }

        private void checkBoxRicercaLibera_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxRicercaLibera.Checked)
            {
                this.dateTimePickerDataLiq.Enabled = false;
                this.textBoxQuery.Enabled = true;
            }
            else
            {
                this.dateTimePickerDataLiq.Enabled = true;
                this.textBoxQuery.Enabled = false;
            }
        }
    }
}