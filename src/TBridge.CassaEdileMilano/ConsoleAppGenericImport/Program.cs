﻿using System;
using System.Configuration;
using TBridge.Cemi.AccessoCantieri.Business;
//using System.Timers;

namespace TBridge.Cemi.AccessoCantieri.ConsoleAppGenericImport
{
    internal static class Program
    {
        private static readonly AccessoCantieriBusiness Biz = new AccessoCantieriBusiness();
        private static readonly GenericImportConnector GenericImportBiz = new GenericImportConnector();

        //private static Timer _timerAccessi;

        private static void Main()
        {
            //InizializzaTimer();

            CaricaAccessi();

            Console.ReadLine();
        }

        //private static void InizializzaTimer()
        //{
        //    _timerAccessi = new Timer
        //    {
        //        Interval =
        //            Int32.Parse(ConfigurationManager.AppSettings["VerificaAccessiTimerInterval"])
        //    };
        //    _timerAccessi.Elapsed += TimerAccessiElapsed;
        //}

        //private static void TimerAccessiElapsed(object sender, ElapsedEventArgs e)
        //{
        //    _timerAccessi.Stop();
        //    CaricaAccessi();
        //}

        private static void CaricaAccessi()
        {

            #region GenericImport

            if (ConfigurationManager.AppSettings["AcquisizioneAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura importazione generica" + DateTime.Now);
                    Int32 i = GenericImportBiz.CaricaDati();

                    if (i == -1)
                    {
                        Console.WriteLine(" Importazione generica: Nessun nuovo file");
                    }

                    else if (i > 0)
                        Console.WriteLine(string.Format(
                            " Importazione generica: Non lette {0} timbrature. Rilevatore non dichiarato?", i));
                    else
                        Console.WriteLine(" Importazione generica: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            # region Timbrature non gestite

            if (ConfigurationManager.AppSettings["RecuperoTimbratureNonGestiteAttivo"] == "1")
            {
                Console.WriteLine("INIZIO controllo timb");
                Biz.ControllaTimbratureNonGestite();
                Console.WriteLine("FINE controllo timb");
            }

            #endregion

            #region Timbrature non gestite senza comunicazioni

            ////Console.WriteLine("INIZIO controllo timb no comunicazioni");
            ////biz.ControllaTimbratureNonGestiteNoCom();
            ////Console.WriteLine("FINE controllo timb no comunicazioni");

            #endregion

            //_timerAccessi.Start();
        }

    }
}