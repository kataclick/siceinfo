﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using TBridge.Cemi.AccessoCantieri.Business;

namespace TBridge.Cemi.AccessoCantieri.EmailApp
{
    public class SslTcpClient
    {
        private static readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
        private TcpClient client;
        private StreamReader reader;
        private SslStream sslStream;
        private String success = "+OK";
        private StreamWriter writer;

        public SslTcpClient(string host, int port)
        {
            Host = host;
            Port = port;
        }

        protected int Port { get; set; }

        protected string Host { get; set; }
        protected String LastResponse { get; set; }
        protected bool Connected { get; set; }

        public static int Main(string[] args)
        {
            try
            {

                Console.WriteLine("INIZIO importazione file INPS");

                //biz.ImportaExcelINPS(ConfigurationManager.AppSettings["pathINPS"]);

                biz.ImportaExcelINPS2(ConfigurationManager.AppSettings["pathINPSScaricati"],
                                      ConfigurationManager.AppSettings["pathINPSElaborati"]);


                Console.WriteLine("FINE importazione file INPS");

                Console.Read();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.Read();
                return 10;
            }
        }

        private static void ExtractAttachment(Pop3Message message)
        {
            if (message.IsMultipart)
            {
                List<Pop3Component> attachment = message.GetAttachment();
                foreach (Pop3Component component in attachment)
                {
                    //if (component.FileExtension.Equals("xls"))
                    if (component.Name.EndsWith("xls"))
                    {
                        if (
                            !File.Exists(string.Format("{0}{1}", ConfigurationManager.AppSettings["pathINPSScaricati"],
                                                       component.Name)))
                        {
                            Base64Decoder decoder = new Base64Decoder(component.Data.ToCharArray());
                            byte[] bytes = decoder.GetDecoded();
                            BinaryWriter writer;

                            //FileStream romFileStream =
                            //    File.Open(String.Format(@"C:\Users\simone.vallarino\Desktop\{0}", component.Filename),
                            //              FileMode.OpenOrCreate,
                            //              FileAccess.ReadWrite);

                            FileStream romFileStream =
                                File.Open(
                                    String.Format("{0}{1}", ConfigurationManager.AppSettings["pathINPSScaricati"],
                                                  component.Name),
                                    FileMode.OpenOrCreate,
                                    FileAccess.ReadWrite);

                            writer = new BinaryWriter(romFileStream);
                            writer.Write(bytes);
                            writer.Close();
                            romFileStream.Close();
                        }
                    }
                }
            }
        }

        public void Login(String userName, String password)
        {
            client = new TcpClient(Host, Port);
            sslStream = new SslStream(client.GetStream(), false,
                                      ValidateServerCertificate,
                                      SelectLocalCertificate);

            sslStream.AuthenticateAsClient(Host);

            reader = new StreamReader(sslStream);
            writer = new StreamWriter(sslStream);
            writer.AutoFlush = true;

            String response = reader.ReadLine();
            if (!response.StartsWith(success))
                throw new Exception("Server response with an error");
            Connected = true;
            if (!User(userName))
                throw new Exception("Invalid username");
            if (!Password(password))
                throw new Exception("Invalid password");
        }

        private X509Certificate SelectLocalCertificate(object sender, string targethost,
                                                       X509CertificateCollection localcertificates,
                                                       X509Certificate remotecertificate, string[] acceptableissuers)
        {
            if (acceptableissuers != null && acceptableissuers.Length > 0 && localcertificates != null &&
                localcertificates.Count > 0)
            {
                foreach (X509Certificate certificate in localcertificates)
                {
                    if (Array.IndexOf(acceptableissuers, certificate.Issuer) != -1)
                        return certificate;
                }
            }
            if (localcertificates != null && localcertificates.Count > 0)
                return localcertificates[0];
            return null;
        }

        private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain,
                                               SslPolicyErrors sslpolicyerrors)
        {
            return sslpolicyerrors == SslPolicyErrors.None;
        }

        public void Disconnect()
        {
            if (Connected)
            {
                Quit();
                Connected = false;
                reader.Close();
                writer.Close();
                sslStream.Close();
                reader.Dispose();
                writer.Dispose();
                sslStream.Dispose();
                client.Close();
                client = null;
            }
        }


        private bool Password(string password)
        {
            if (!Connected)
                return false;
            return SendCommand(String.Format("PASS {0}", password));
        }

        private bool Quit()
        {
            if (!Connected)
                return false;
            return SendCommand("QUIT");
        }

        private bool Stat(out int messageCount, out int messageSize)
        {
            messageCount = -1;
            messageSize = -1;

            if (!Connected)
                return false;

            if (!SendCommand("STAT"))
                return false;

            String[] responseParameters = LastResponse.Split(' ');
            if (responseParameters.Length != 3)
                return false;
            Int32.TryParse(responseParameters[1], out messageCount);
            Int32.TryParse(responseParameters[2], out messageSize);

            return true;
        }

        private bool List(out List<String> headers)
        {
            headers = new List<string>();
            if (!Connected)
                return false;
            if (!SendCommand("LIST"))
                return false;

            String response = reader.ReadLine();

            while (!response.Equals("."))
            {
                headers.Add(response);
                response = reader.ReadLine();
            }

            return true;
        }

        private bool GetMessage(int idMessage, out List<String> headers)
        {
            headers = new List<string>();
            if (!Connected)
                return false;
            if (!SendCommand(String.Format("RETR {0}", idMessage)))
                return false;

            String response = reader.ReadLine();

            while (!response.Equals("."))
            {
                headers.Add(response);
                response = reader.ReadLine();
            }

            return true;
        }

        private bool Top(int messageId, int numLines, out List<string> headers, out List<string> lines)
        {
            headers = new List<string>();
            lines = new List<string>();

            if (!Connected)
                return false;
            if (!SendCommand(String.Format("TOP {0} {1}", messageId, numLines)))
                return false;

            String response = reader.ReadLine();

            while (!response.Equals("."))
            {
                headers.Add(response);
                response = reader.ReadLine();
            }

            //for(int i =0;i<numLines;i++)
            //{
            //    lines.Add(reader.ReadLine());
            //}
            return true;
        }


        private bool SendCommand(string command, params object[] commandParams)
        {
            if (writer == null || reader == null)
                return false;
            writer.WriteLine(String.Format(command, commandParams));
            LastResponse = reader.ReadLine();
            return LastResponse.StartsWith(success);
        }


        private bool User(string userName)
        {
            if (!Connected)
                return false;

            return SendCommand(String.Format("USER {0}", userName));
        }
    }
}