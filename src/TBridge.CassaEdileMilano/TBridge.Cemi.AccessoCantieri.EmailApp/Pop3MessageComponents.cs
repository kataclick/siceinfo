using System.Collections;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.EmailApp
{
    /// <summary>
    ///   Summary description for Pop3MessageBody.
    /// </summary>
    public class Pop3MessageComponents
    {
        private readonly ArrayList m_component = new ArrayList();

        public Pop3MessageComponents(string[] lines, long startOfBody
                                     , string multipartBoundary, string mainContentType)
        {
            long stopOfBody = lines.Length;

            // if this email is a mixture of message
            // and attachments ...

            if (multipartBoundary == null)
            {
                StringBuilder sbText = new StringBuilder();

                for (long i = startOfBody; i < stopOfBody; i++)
                {
                    sbText.Append(lines[i].Replace("\n", "").
                                      Replace("\r", ""));
                }

                // create a new component ...
                m_component.Add(
                    new Pop3Component(
                        mainContentType,
                        sbText.ToString()));
            }
            else
            {
                string boundary = multipartBoundary;

                bool firstComponent = true;

                // loop through whole of email ...
                for (long i = startOfBody; i < stopOfBody;)
                {
                    bool boundaryFound = true;

                    string contentType = null;
                    string name = null;
                    string filename = null;
                    string contentTransferEncoding = null;
                    string contentDescription = null;
                    string contentDisposition = null;
                    string data = null;

                    // if first block of multipart data ...
                    if (firstComponent)
                    {
                        boundaryFound = false;
                        firstComponent = false;

                        while (i < stopOfBody)
                        {
                            string line =
                                lines[i].Replace("\n", "").Replace("\r", "");

                            // if multipart boundary found then
                            // exit loop ...

                            if (Pop3Parse.GetSubHeaderLineType(line, boundary) ==
                                Pop3Parse.MultipartBoundaryFound)
                            {
                                boundaryFound = true;

                                break;
                            }
                            // ... else read next line ...
                            ++i;
                        }
                    }

                    // check to see whether multipart boundary
                    // was found ...


                    bool endOfHeader = false;

                    // read header information ...
                    while ((i < stopOfBody) && boundaryFound)
                    {
                        string line =
                            lines[i].Replace("\n", "").Replace("\r", "");

                        int lineType = Pop3Parse.GetSubHeaderLineType(line, boundary);

                        switch (lineType)
                        {
                            case Pop3Parse.ContentTypeType:
                                contentType =
                                    Pop3Parse.ContentType(line);
                                if (contentType.Contains("name"))
                                    name = Pop3Parse.Name(contentType.Substring(contentType.LastIndexOf(";") + 1));
                                break;

                            case Pop3Parse.ContentTransferEncodingType:
                                contentTransferEncoding =
                                    Pop3Parse
                                        .ContentTransferEncoding(line);
                                break;

                            case Pop3Parse.ContentDispositionType:
                                contentDisposition =
                                    Pop3Parse.ContentDisposition(line);
                                if (contentDisposition.Contains("filename"))
                                    filename =
                                        Pop3Parse.Filename(
                                            contentDisposition.Substring(contentDisposition.LastIndexOf(";") + 1));
                                break;

                            case Pop3Parse.ContentDescriptionType:
                                contentDescription =
                                    Pop3Parse
                                        .ContentDescription(line);
                                break;

                            case Pop3Parse.EndOfHeader:
                                endOfHeader = true;
                                break;
                        }

                        ++i;

                        if (endOfHeader)
                        {
                            break;
                        }
                    }

                    boundaryFound = false;

                    StringBuilder sbText = new StringBuilder();

                    bool emailComposed = false;

                    // store the actual data ...
                    while (i < stopOfBody)
                    {
                        // get the next line ...
                        string line = lines[i].Replace("\n", "");

                        // if we've found the boundary ...
                        if (Pop3Parse.GetSubHeaderLineType(line, boundary) ==
                            Pop3Parse.MultipartBoundaryFound)
                        {
                            boundaryFound = true;
                            ++i;
                            break;
                        }
                        else if (Pop3Parse.GetSubHeaderLineType(line, boundary) ==
                                 Pop3Parse.ComponetsDone)
                        {
                            emailComposed = true;
                            break;
                        }

                        // add this line to data ...
                        sbText.Append(lines[i]);
                        ++i;
                    }

                    if (sbText.Length > 0)
                    {
                        data = sbText.ToString();
                    }

                    // create a new component ...
                    m_component.Add(
                        new Pop3Component(
                            contentType,
                            name,
                            filename,
                            contentTransferEncoding,
                            contentDescription,
                            contentDisposition,
                            data));

                    // if all multiparts have been
                    // composed then exit ..

                    if (emailComposed)
                    {
                        break;
                    }
                }
            }
        }

        public IEnumerator ComponentEnumerator
        {
            get { return m_component.GetEnumerator(); }
        }

        public int NumberOfComponents
        {
            get { return m_component.Count; }
        }
    }
}