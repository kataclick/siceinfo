﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace TBridge.Cemi.AccessoCantieri.EmailApp
{
    /// <summary>
    ///   DLM: Stores the From:, To:, Subject:, body and attachments
    ///   within an email. Binary attachments are Base64-decoded
    /// </summary>
    public class Pop3Message
    {
        private const int m_contentTypeState = 3;
        private const int m_endOfHeader = -98;
        private const int m_fromState = 0;
        private const int m_notKnownState = -99;
        private const int m_subjectState = 2;
        private const int m_toState = 1;
        private readonly Socket m_client;

        // this array corresponds with above
        // enumerator ...

        private readonly string[] m_lineTypeString =
            {
                "From",
                "To",
                "Subject",
                "Content-Type"
            };

        private readonly ManualResetEvent m_manualEvent = new ManualResetEvent(false);
        private TcpClient client;
        private string m_body;
        private string m_contentType;
        private string m_from;

        private long m_inboxPosition;
        private bool m_isMultipart;
        private Pop3MessageComponents m_messageComponents;
        private long m_messageSize;
        private string m_multipartBoundary;

        private Pop3StateObject m_pop3State;
        private string m_subject;
        private string m_to;
        private StreamReader reader;
        private SslStream sslStream;
        private String success = "+OK";
        private StreamWriter writer;

        public Pop3Message(string host, string user, string password, int port)
        {
            Host = host;
            Port = port;

            Login(user, password);
        }

        public IEnumerator MultipartEnumerator
        {
            get { return m_messageComponents.ComponentEnumerator; }
        }

        public bool IsMultipart
        {
            get { return m_isMultipart; }
        }

        public string From
        {
            get { return m_from; }
        }

        public string To
        {
            get { return m_to; }
        }

        public string Subject
        {
            get { return m_subject; }
        }

        public string Body
        {
            get { return m_body; }
        }

        public long InboxPosition
        {
            get { return m_inboxPosition; }
        }

        //send the data to server

        protected int Port { get; set; }

        protected string Host { get; set; }
        protected String LastResponse { get; set; }
        protected bool Connected { get; set; }

        public void Login(String userName, String password)
        {
            client = new TcpClient(Host, Port);

            #region Connessione con POP3S tramite SSL/TSL da effettuare su porta 995

            //sslStream = new SslStream(client.GetStream(), false,
            //                          ValidateServerCertificate,
            //                          SelectLocalCertificate);

            //sslStream.AuthenticateAsClient(Host);

            //reader = new StreamReader(sslStream);
            //writer = new StreamWriter(sslStream);

            #endregion

            #region Connessione con POP3 normale da effettuare su porta 110

            reader = new StreamReader(client.GetStream());
            writer = new StreamWriter(client.GetStream());

            #endregion

            writer.AutoFlush = true;

            String response = reader.ReadLine();
            if (!response.StartsWith(success))
                throw new Exception("Server response with an error");
            Connected = true;
            if (!User(userName))
                throw new Exception("Invalid username");
            if (!Password(password))
                throw new Exception("Invalid password");
        }

        private X509Certificate SelectLocalCertificate(object sender, string targethost,
                                                       X509CertificateCollection localcertificates,
                                                       X509Certificate remotecertificate, string[] acceptableissuers)
        {
            if (acceptableissuers != null && acceptableissuers.Length > 0 && localcertificates != null &&
                localcertificates.Count > 0)
            {
                foreach (X509Certificate certificate in localcertificates)
                {
                    if (Array.IndexOf(acceptableissuers, certificate.Issuer) != -1)
                        return certificate;
                }
            }
            if (localcertificates != null && localcertificates.Count > 0)
                return localcertificates[0];
            return null;
        }

        private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain,
                                               SslPolicyErrors sslpolicyerrors)
        {
            return sslpolicyerrors == SslPolicyErrors.None;
        }

        public void Disconnect()
        {
            if (Connected)
            {
                Quit();
                Connected = false;
                reader.Close();
                writer.Close();
                sslStream.Close();
                reader.Dispose();
                writer.Dispose();
                sslStream.Dispose();
                client.Close();
                client = null;
            }
        }


        private bool Password(string password)
        {
            if (!Connected)
                return false;
            return SendCommand(String.Format("PASS {0}", password));
        }

        public bool Quit()
        {
            if (!Connected)
                return false;
            return SendCommand("QUIT");
        }

        public bool Stat(out int messageCount, out int messageSize)
        {
            messageCount = -1;
            messageSize = -1;

            if (!Connected)
                return false;

            if (!SendCommand("STAT"))
                return false;

            String[] responseParameters = LastResponse.Split(' ');
            if (responseParameters.Length != 3)
                return false;
            Int32.TryParse(responseParameters[1], out messageCount);
            Int32.TryParse(responseParameters[2], out messageSize);

            return true;
        }

        public bool List(out List<String> headers)
        {
            headers = new List<string>();
            if (!Connected)
                return false;
            if (!SendCommand("LIST"))
                return false;

            String response = reader.ReadLine();

            while (!response.Equals("."))
            {
                headers.Add(response);
                response = reader.ReadLine();
            }

            return true;
        }

        public bool GetMessage(int idMessage, out List<String> headers)
        {
            headers = new List<string>();
            if (!Connected)
                return false;
            if (!SendCommand(String.Format("RETR {0}", idMessage)))
                return false;

            String response = reader.ReadLine();

            while (!response.Equals("."))
            {
                headers.Add(response);
                response = reader.ReadLine();
            }

            return true;
        }

        public bool Top(int messageId, int numLines, out List<string> headers, out List<string> lines)
        {
            headers = new List<string>();
            lines = new List<string>();

            if (!Connected)
                return false;
            if (!SendCommand(String.Format("TOP {0} {1}", messageId, numLines)))
                return false;

            String response = reader.ReadLine();

            while (!response.Equals("."))
            {
                headers.Add(response);
                response = reader.ReadLine();
            }

            //for(int i =0;i<numLines;i++)
            //{
            //    lines.Add(reader.ReadLine());
            //}
            return true;
        }

        private bool SendCommand(string command, params object[] commandParams)
        {
            if (writer == null || reader == null)
                return false;
            writer.WriteLine(String.Format(command, commandParams));
            LastResponse = reader.ReadLine();
            return LastResponse.StartsWith(success);
        }

        private bool User(string userName)
        {
            if (!Connected)
                return false;

            return SendCommand(String.Format("USER {0}", userName));
        }

        //--------------------------

        private void StartReceiveAgain(string data)
        {
            // receive more data if we expect more.
            // note: a literal "." (or more) followed by
            // "\r\n" in an email is prefixed with "." ...

            if (!data.EndsWith("\r\n.\r\n"))
            {
                m_client.BeginReceive(m_pop3State.buffer, 0,
                                      Pop3StateObject.BufferSize, 0,
                                      new AsyncCallback(ReceiveCallback),
                                      m_pop3State);
            }
            else
            {
                // stop receiving data ...
                m_manualEvent.Set();
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket 
                // from the asynchronous state object.

                Pop3StateObject stateObj =
                    (Pop3StateObject) ar.AsyncState;

                Socket client = stateObj.workSocket;

                // Read data from the remote device.
                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, 
                    // so store the data received so far.

                    stateObj.sb.Append(
                        Encoding.ASCII.GetString(stateObj.buffer
                                                 , 0, bytesRead));

                    // read more data from pop3 server ...
                    StartReceiveAgain(stateObj.sb.ToString());
                }
            }
            catch (Exception e)
            {
                m_manualEvent.Set();

                throw new
                    Exception("RecieveCallback error" +
                              e);
            }
        }

        private int GetHeaderLineType(string line)
        {
            int lineType = m_notKnownState;

            for (int i = 0; i < m_lineTypeString.Length; i++)
            {
                string match = m_lineTypeString[i];

                if (Regex.Match(line, "^" + match + ":" + ".*$").Success)
                {
                    lineType = i;
                    break;
                }
                else if (line.Length == 0)
                {
                    lineType = m_endOfHeader;
                    break;
                }
            }

            return lineType;
        }

        private long ParseHeader(string[] lines)
        {
            int numberOfLines = lines.Length;
            long bodyStart = 0;

            for (int i = 0; i < numberOfLines; i++)
            {
                string currentLine = lines[i].Replace("\n", "");

                int lineType = GetHeaderLineType(currentLine);

                switch (lineType)
                {
                        // From:
                    case m_fromState:
                        m_from = Pop3Parse.From(currentLine);
                        break;

                        // Subject:
                    case m_subjectState:
                        m_subject = Pop3Parse.Subject(currentLine);
                        break;

                        // To:
                    case m_toState:
                        m_to = Pop3Parse.To(currentLine);
                        break;

                        // Content-Type
                    case m_contentTypeState:

                        m_contentType =
                            Pop3Parse.ContentType(currentLine);

                        m_isMultipart =
                            Pop3Parse.IsMultipart(m_contentType);

                        if (m_isMultipart)
                        {
                            // if boundary definition is on next
                            // line ...

                            if (m_contentType
                                .Substring(m_contentType.Length - 1, 1).
                                Equals(";"))
                            {
                                ++i;

                                m_multipartBoundary
                                    = Pop3Parse.
                                        MultipartBoundary(lines[i].
                                                              Replace("\n", ""));
                            }
                            else
                            {
                                // boundary definition is on same
                                // line as "Content-Type" ...

                                m_multipartBoundary =
                                    Pop3Parse
                                        .MultipartBoundary(m_contentType);
                            }
                        }

                        break;

                    case m_endOfHeader:
                        bodyStart = i + 1;
                        break;
                }

                if (bodyStart > 0)
                {
                    break;
                }
            }

            return (bodyStart);
        }

        private void ParseEmail(string[] lines)
        {
            long startOfBody = ParseHeader(lines);

            m_messageComponents =
                new Pop3MessageComponents(lines, startOfBody
                                          , m_multipartBoundary, m_contentType);
        }

        private void LoadEmail()
        {
            // tell pop3 server we want to start reading
            // email (m_inboxPosition) from inbox ...
            List<String> message;
            GetMessage((Int32) m_inboxPosition, out message);

            //Send("retr "+m_inboxPosition);

            foreach (string s in message)
            {
                m_pop3State.sb.Append(s + '\r');
            }

            // parse email ...
            ParseEmail(
                m_pop3State.sb.ToString().Split(new[] {'\r'}));

            // remove reading pop3State ...
            m_pop3State = null;
        }

        public void GetEmail(long position)
        {
            m_inboxPosition = position;
            //           m_messageSize = size;
            //         m_client = socketClient;

            m_pop3State = new Pop3StateObject();
            m_pop3State.workSocket = m_client;
            m_pop3State.sb = new StringBuilder();

            // load email ...
            LoadEmail();

            // get body (if it exists) ...
            IEnumerator multipartEnumerator =
                MultipartEnumerator;

            while (multipartEnumerator.MoveNext())
            {
                Pop3Component multipart = (Pop3Component)
                                          multipartEnumerator.Current;

                if (multipart.IsBody)
                {
                    m_body = multipart.Data;
                    break;
                }
            }
        }

        public override string ToString()
        {
            IEnumerator enumerator = MultipartEnumerator;

            string ret =
                "From    : " + From + "\r\n" +
                "To      : " + To + "\r\n" +
                "Subject : " + Subject + "\r\n";

            while (enumerator.MoveNext())
            {
                ret += enumerator.Current + "\r\n";
            }

            return ret;
        }

        public List<Pop3Component> GetAttachment()
        {
            IEnumerator enumerator = MultipartEnumerator;
            List<Pop3Component> attachment = new List<Pop3Component>();
            while (enumerator.MoveNext())
            {
                if (((Pop3Component) enumerator.Current).IsAttachment)
                    attachment.Add((Pop3Component) enumerator.Current);
            }
            return attachment;
        }
    }
}