﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Enums;
using TBridge.Cemi.Colonie.Type.Exceptions;
using TBridge.Cemi.Colonie.Type.Filters;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Colonie.Business
{
    public class ColonieBusinessEF
    {
        public List<ColoniePersonaleMansione> GetPersonaleMansioni()
        {
            List<ColoniePersonaleMansione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mansione in context.ColoniePersonaleMansioni
                            select mansione;
                ret = query.ToList();
            }

            return ret;
        }

        public List<ColoniePersonaleMansioniRetribuzione> GetPersonaleRetribuzioni(Int32 idMansione)
        {
            List<ColoniePersonaleMansioniRetribuzione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from retribuzione in context.ColoniePersonaleMansioniRetribuzioni
                            where retribuzione.IdMansione == idMansione
                            orderby retribuzione.Livello
                            select retribuzione;
                ret = query.ToList();
            }

            return ret;
        }

        public List<ColoniePersonaleValutazione> GetPersonaleValutazioni()
        {
            List<ColoniePersonaleValutazione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from valutazione in context.ColoniePersonaleValutazioni
                            select valutazione;
                ret = query.ToList();
            }

            return ret;
        }

        public List<ColoniePersonaleTitoloStudio> GetPersonaleTitoliStudio()
        {
            List<ColoniePersonaleTitoloStudio> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from titoloStudio in context.ColoniePersonaleTitoliStudio
                            select titoloStudio;
                ret = query.ToList();
            }

            return ret;
        }

        public List<ColoniePersonaleLuogoAppuntamento> GetLuoghiAppuntamento()
        {
            List<ColoniePersonaleLuogoAppuntamento> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from luogoAppuntamento in context.ColoniePersonaleLuoghiAppuntamento
                            select luogoAppuntamento;
                ret = query.ToList();
            }

            return ret;
        }

        public void InsertRichiesta(ColoniePersonaleRichiesta richiesta)
        {
            try
            {
                using (SICEEntities context = new SICEEntities())
                {
                    // Mansioni
                    List<ColoniePersonaleMansione> mansioni = new List<ColoniePersonaleMansione>();

                    foreach (ColoniePersonaleMansione mansione in richiesta.ColoniePersonaleMansioni)
                    {
                        var query = from mans in context.ColoniePersonaleMansioni
                                    where mans.Id == mansione.Id
                                    select mans;

                        mansioni.Add(query.Single());
                    }
                    richiesta.ColoniePersonaleMansioni = mansioni;

                    // Turni
                    List<ColonieTurno> turni = new List<ColonieTurno>();

                    foreach (ColonieTurno turno in richiesta.ColonieTurni)
                    {
                        var query = from turn in context.ColonieTurni
                                    where turn.Id == turno.Id
                                    select turn;

                        turni.Add(query.Single());
                    }
                    richiesta.ColonieTurni = turni;

                    context.ColoniePersonaleRichieste.AddObject(richiesta);
                    context.SaveChanges();
                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException is SqlException)
                {
                    // Nel caso in cui esista già una domanda con lo stesso
                    // codice fiscale genero la giusta eccezione
                    SqlException sqlExc = (SqlException) exc.InnerException;
                    if (sqlExc.Number == 2627)
                    {
                        throw new PersonaleRichiestaGiaInseritaException();
                    }
                }

                throw;
            }
        }

        public ColoniePersonaleRichiesta GetPersonaleRichiesta(Int32 id)
        {
            ColoniePersonaleRichiesta richiestaRet = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from richiesta in context.ColoniePersonaleRichieste.Include("ColoniePersonaleTitoloStudio").Include("ColoniePersonaleMansioni").Include("ColonieTurni").Include("ColonieTurni.ColonieDestinazione").Include("ColonieTurni.ColonieDestinazione.ColonieTipoDestinazione").Include("ColoniePersonaleProposte").Include("ColoniePersonaleColloquio").Include("ColoniePersonaleColloquio.ColoniePersonaleMansioni").Include("ColoniePersonaleColloquio.Valutazione").Include("ColoniePersonaleProposte.Mansione").Include("ColoniePersonaleProposte.Turni").Include("ColoniePersonaleRichiesteEsperienze").Include("ColoniePersonaleProposte.Retribuzione").Include("ColoniePersonaleProposte.Turni.ColonieDestinazione").Include("LuogoNascitaNav").Include("ResidenzaComuneNav").Include("DomicilioComuneNav").Include("ColoniePersonaleProposte.LuogoAppuntamento").Include("ColoniePersonaleProposte.Turni.ColonieDestinazione.ColonieTipoDestinazione").Include("ColonieVacanza")
                            where richiesta.Id == id
                            select richiesta;
                richiestaRet = query.Single();
            }

            return richiestaRet;
        }

        public byte[] GetCurriculumRichiesta(Int32 id, out String estensioneCurriculum)
        {
            byte[] curriculum = null;
            estensioneCurriculum = String.Empty;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from richiesta in context.ColoniePersonaleRichieste
                            where richiesta.Id == id
                            select new
                            {
                                richiesta.Curriculum,
                                richiesta.CurriculumEstensione
                            };
                curriculum = query.Single().Curriculum;
                estensioneCurriculum = query.Single().CurriculumEstensione;
            }

            return curriculum;
        }

        public List<ColoniePersonaleRichiesta> GetRichieste(RichiestaFilter filtro)
        {
            List<ColoniePersonaleRichiesta> ret;

            using (SICEEntities context = new SICEEntities())
            {
                Int32 stato = -1;
                if (filtro.Stato.HasValue)
                {
                    stato = (Int32) filtro.Stato.Value;
                }

                var query = from richiesta in context.ColoniePersonaleRichieste.Include("ColoniePersonaleColloquio").Include("ColoniePersonaleProposte").Include("ColoniePersonaleColloquio.ColoniePersonaleMansioni").Include("ColoniePersonaleColloquio.Valutazione").Include("ColoniePersonaleProposte.Mansione").Include("ColoniePersonaleProposte.Turni").Include("ColoniePersonaleProposte.Retribuzione").Include("LuogoNascitaNav").Include("ResidenzaComuneNav").Include("DomicilioComuneNav")
                            where (!filtro.IdRichiesta.HasValue || richiesta.Id == filtro.IdRichiesta.Value)
                                && (String.IsNullOrEmpty(filtro.Cognome) || richiesta.Cognome.Contains(filtro.Cognome))
                                && (String.IsNullOrEmpty(filtro.Nome) || richiesta.Nome.Contains(filtro.Nome))
                                && (String.IsNullOrEmpty(filtro.Note) || richiesta.RespintaNote.Contains(filtro.Note))
                                && (!filtro.IdMansione.HasValue || richiesta.ColoniePersonaleMansioni.Any(mansione => mansione.Id == filtro.IdMansione.Value))
                                && (!filtro.IdMansioneColloquio.HasValue || (richiesta.ColoniePersonaleColloquio.Any() && richiesta.ColoniePersonaleColloquio.All(colloquio => colloquio.ColoniePersonaleMansioni.Any(mansione => mansione.Id == filtro.IdMansioneColloquio.Value))))
                                && (!filtro.IdTurno.HasValue || richiesta.ColonieTurni.Any(turno => turno.Id == filtro.IdTurno.Value))
                                && (!filtro.IdMansioneAssegnata.HasValue || (richiesta.ColoniePersonaleProposte.Any() && richiesta.ColoniePersonaleProposte.All(proposta => proposta.IdMansione == filtro.IdMansioneAssegnata.Value)))
                                && (!filtro.IdTurnoAssegnato.HasValue || (richiesta.ColoniePersonaleProposte.Any() && richiesta.ColoniePersonaleProposte.All(proposta => proposta.Turni.Any(turno => turno.Id == filtro.IdTurnoAssegnato.Value))))
                                && (!filtro.EsperienzaCE.HasValue || richiesta.EsperienzaColonie == filtro.EsperienzaCE.Value)
                                && (!filtro.IdVacanza.HasValue || richiesta.IdColonieVacanza == filtro.IdVacanza.Value)
                            && (stato == -1
                                || (stato == (Int32) StatoRichiesta.RESPINTA && richiesta.Respinta)
                                || (stato == (Int32) StatoRichiesta.DAGESTIRE && !richiesta.Respinta && !richiesta.ColoniePersonaleColloquio.Any() && !richiesta.ColoniePersonaleProposte.Any())
                                || (stato == (Int32) StatoRichiesta.EFFETTUATAPROPOSTA && !richiesta.Respinta && richiesta.ColoniePersonaleProposte.Count == 1 && !richiesta.ColoniePersonaleProposte.All(proposta => proposta.Accettata.HasValue))
                                || (stato == (Int32) StatoRichiesta.FISSATOCOLLOQUIO && !richiesta.Respinta && richiesta.ColoniePersonaleProposte.Count == 0 && richiesta.ColoniePersonaleColloquio.Count == 1 && richiesta.ColoniePersonaleColloquio.All(colloquio => !colloquio.IdValutazione.HasValue))
                                || (stato == (Int32) StatoRichiesta.PROPOSTAACCETTATA && !richiesta.Respinta && richiesta.ColoniePersonaleProposte.Count == 1 && richiesta.ColoniePersonaleProposte.All(proposta => proposta.Accettata.HasValue && proposta.Accettata.Value))
                                || (stato == (Int32) StatoRichiesta.PROPOSTANONACCETTATA && !richiesta.Respinta && richiesta.ColoniePersonaleProposte.Count == 1 && richiesta.ColoniePersonaleProposte.All(proposta => proposta.Accettata.HasValue && !proposta.Accettata.Value))
                                || (stato == (Int32) StatoRichiesta.VALUTATO && !richiesta.Respinta && richiesta.ColoniePersonaleProposte.Count == 0 && richiesta.ColoniePersonaleColloquio.Count == 1 && richiesta.ColoniePersonaleColloquio.All(colloquio => colloquio.IdValutazione.HasValue))
                                )
                            && (!filtro.IdValutazione.HasValue || (richiesta.ColoniePersonaleColloquio.Any() && richiesta.ColoniePersonaleColloquio.All(colloquio => colloquio.IdValutazione == filtro.IdValutazione.Value)))
                            orderby richiesta.Cognome ascending, richiesta.Nome ascending
                            select richiesta;

                ret = query.ToList();
            }

            return ret;
        }

        public List<ColoniePersonaleRichiesta> GetRichiestePerEstrazione(EstrazioneFilter filtro)
        {
            List<ColoniePersonaleRichiesta> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from richiesta in context.ColoniePersonaleRichieste.Include("ColoniePersonaleColloquio").Include("ColoniePersonaleProposte").Include("ColoniePersonaleColloquio.ColoniePersonaleMansioni").Include("ColoniePersonaleColloquio.Valutazione").Include("ColoniePersonaleProposte.Mansione").Include("ColoniePersonaleProposte.Turni").Include("ColoniePersonaleProposte.Retribuzione").Include("LuogoNascitaNav").Include("ResidenzaComuneNav").Include("DomicilioComuneNav").Include("ColoniePersonaleTitoloStudio")
                            where (richiesta.IdColonieVacanza == filtro.IdVacanza)
                                && (!richiesta.Respinta)
                                && (richiesta.ColoniePersonaleProposte.Count == 1 && richiesta.ColoniePersonaleProposte.All(proposta => proposta.Accettata.HasValue && proposta.Accettata.Value))
                                && (!filtro.DataAssunzioneDal.HasValue || richiesta.ColoniePersonaleProposte.All(proposta => proposta.DataInizioRapporto >= filtro.DataAssunzioneDal.Value))
                                && (!filtro.DataAssunzioneAl.HasValue || richiesta.ColoniePersonaleProposte.All(proposta => proposta.DataInizioRapporto <= filtro.DataAssunzioneAl.Value))
                                && (!filtro.IdMansione.HasValue || richiesta.ColoniePersonaleProposte.All(proposta => proposta.IdMansione == filtro.IdMansione.Value))
                            orderby richiesta.Cognome ascending, richiesta.Nome ascending
                            select richiesta;

                ret = query.ToList();
            }

            return ret;
        }

        public List<ColoniePersonaleRichiesta> GetRichiesteConColloquio(Int32 idVacanza)
        {
            List<ColoniePersonaleRichiesta> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from richiesta in context.ColoniePersonaleRichieste.Include("ColoniePersonaleColloquio").Include("ColoniePersonaleColloquio.ColoniePersonaleMansioni").Include("ColoniePersonaleColloquio.Valutazione").Include("ColoniePersonaleColloquio.ColoniePersonaleMansioni")
                            where richiesta.ColoniePersonaleColloquio.Count == 1 && richiesta.ColonieVacanza.Id == idVacanza
                            select richiesta;
                ret = query.ToList();
            }

            return ret;
        }

        public StatoRichiesta GeneraStato(ColoniePersonaleRichiesta richiesta)
        {
            StatoRichiesta stato = StatoRichiesta.DAGESTIRE;

            if (richiesta.Respinta)
            {
                stato = StatoRichiesta.RESPINTA;
            }
            else
            {
                if (richiesta.ColoniePersonaleProposte != null && richiesta.ColoniePersonaleProposte.Count == 1)
                {
                    if (richiesta.ColoniePersonaleProposte.First().Accettata.HasValue)
                    {
                        if (richiesta.ColoniePersonaleProposte.First().Accettata.Value)
                        {
                            stato = StatoRichiesta.PROPOSTAACCETTATA;
                        }
                        else
                        {
                            stato = StatoRichiesta.PROPOSTANONACCETTATA;
                        }
                    }
                    else
                    {
                        stato = StatoRichiesta.EFFETTUATAPROPOSTA;
                    }
                }
                else
                {
                    if (richiesta.ColoniePersonaleColloquio != null && richiesta.ColoniePersonaleColloquio.Count == 1)
                    {
                        if (richiesta.ColoniePersonaleColloquio.First().IdValutazione.HasValue)
                        {
                            stato = StatoRichiesta.VALUTATO;
                        }
                        else
                        {
                            stato = StatoRichiesta.FISSATOCOLLOQUIO;
                        }
                    }
                }
            }

            return stato;
        }

        public String TraduciStato(StatoRichiesta stato)
        {
            switch (stato)
            {
                case StatoRichiesta.DAGESTIRE:
                    return "Da Gestire";
                case StatoRichiesta.EFFETTUATAPROPOSTA:
                    return "Effettuata Proposta";
                case StatoRichiesta.FISSATOCOLLOQUIO:
                    return "Fissato Colloquio";
                case StatoRichiesta.PROPOSTAACCETTATA:
                    return "Proposta Accettata";
                case StatoRichiesta.PROPOSTANONACCETTATA:
                    return "Proposta NON Accettata";
                case StatoRichiesta.VALUTATO:
                    return "Valutato";
                case StatoRichiesta.RESPINTA:
                    return "Respinta";
            }

            return String.Empty;
        }

        public ColoniePersonaleColloquio GetColloquio(Int32 idColloquio)
        {
            ColoniePersonaleColloquio colloquio = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from coll in context.ColoniePersonaleColloqui.Include("ColoniePersonaleMansioni").Include("Valutazione")
                            where coll.Id == idColloquio
                            select coll;
                colloquio = query.Single();
            }

            return colloquio;
        }

        public void InsertColloquio(ColoniePersonaleColloquio colloquio)
        {
            using (SICEEntities context = new SICEEntities())
            {
                List<ColoniePersonaleMansione> mansioni = new List<ColoniePersonaleMansione>();

                foreach (ColoniePersonaleMansione mansione in colloquio.ColoniePersonaleMansioni)
                {
                    var query = from mans in context.ColoniePersonaleMansioni
                                where mans.Id == mansione.Id
                                select mans;

                    mansioni.Add(query.Single());
                }
                colloquio.ColoniePersonaleMansioni = mansioni;

                context.ColoniePersonaleColloqui.AddObject(colloquio);
                context.SaveChanges();
            }
        }

        public void UpdateColloquio(ColoniePersonaleColloquio colloquio)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from coll in context.ColoniePersonaleColloqui
                            where coll.Id == colloquio.Id
                            select coll;
                ColoniePersonaleColloquio collToUpdate = query.Single();

                collToUpdate.Data = colloquio.Data;
                collToUpdate.ColoniePersonaleMansioni.Clear();
                foreach (ColoniePersonaleMansione mansione in colloquio.ColoniePersonaleMansioni)
                {
                    var queryMans = from mans in context.ColoniePersonaleMansioni
                                    where mans.Id == mansione.Id
                                    select mans;

                    collToUpdate.ColoniePersonaleMansioni.Add(queryMans.Single());
                }
                collToUpdate.IdValutazione = colloquio.IdValutazione;

                context.SaveChanges();
            }
        }

        public void UpdateColloquioInvio(Int32 idColloquio)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from colloquio in context.ColoniePersonaleColloqui
                            where colloquio.Id == idColloquio
                            select colloquio;

                ColoniePersonaleColloquio collToUpdate = query.Single();
                collToUpdate.Inviato = DateTime.Now;

                context.SaveChanges();
            }
        }

        public ColoniePersonaleProposta GetProposta(int idProposta)
        {
            ColoniePersonaleProposta proposta = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from prop in context.ColoniePersonaleProposte.Include("Turni").Include("Mansione").Include("Turni.ColonieDestinazione").Include("Turni.ColonieDestinazione.ColonieTipoDestinazione").Include("Retribuzione").Include("LuogoAppuntamento")
                            where prop.Id == idProposta
                            select prop;
                proposta = query.Single();
            }

            return proposta;
        }

        public void InsertProposta(ColoniePersonaleProposta proposta)
        {
            using (SICEEntities context = new SICEEntities())
            {
                List<ColonieTurno> turni = new List<ColonieTurno>();

                foreach (ColonieTurno turno in proposta.Turni)
                {
                    var query = from tur in context.ColonieTurni
                                where tur.Id == turno.Id
                                select tur;

                    turni.Add(query.Single());
                }
                proposta.Turni = turni;

                context.ColoniePersonaleProposte.AddObject(proposta);
                context.SaveChanges();
            }
        }

        public void UpdateProposta(ColoniePersonaleProposta proposta)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from prop in context.ColoniePersonaleProposte
                            where prop.Id == proposta.Id
                            select prop;
                ColoniePersonaleProposta propToUpdate = query.Single();

                propToUpdate.DataInizioRapporto = proposta.DataInizioRapporto;
                propToUpdate.DataFineRapporto = proposta.DataFineRapporto;
                propToUpdate.IdMansione = proposta.IdMansione;
                propToUpdate.IdRetribuzione = proposta.IdRetribuzione;
                propToUpdate.Accompagnatore = proposta.Accompagnatore;
                propToUpdate.Accettata = proposta.Accettata;
                propToUpdate.Protocollo = proposta.Protocollo;

                propToUpdate.Turni.Clear();
                foreach (ColonieTurno turno in proposta.Turni)
                {
                    var queryTurn = from turn in context.ColonieTurni
                                    where turn.Id == turno.Id
                                    select turn;

                    propToUpdate.Turni.Add(queryTurn.Single());
                }

                propToUpdate.IdColoniePersonaleLuogoAppuntamento = proposta.IdColoniePersonaleLuogoAppuntamento;
                propToUpdate.OraAppuntamento = proposta.OraAppuntamento;

                context.SaveChanges();
            }
        }

        public void UpdatePropostaInvio(Int32 idProposta)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from prop in context.ColoniePersonaleProposte
                            where prop.Id == idProposta
                            select prop;

                ColoniePersonaleProposta propToUpdate = query.Single();
                propToUpdate.Inviata = DateTime.Now;

                context.SaveChanges();
            }
        }

        public void UpdatePropostaConvocazione(int idProposta)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from prop in context.ColoniePersonaleProposte
                            where prop.Id == idProposta
                            select prop;

                ColoniePersonaleProposta propToUpdate = query.Single();
                propToUpdate.InviataConvocazione = DateTime.Now;

                context.SaveChanges();
            }
        }

        //public void RespingiRichiesta(Int32 idRichiesta, String note)
        public void RespingiRichiesta(Int32 idRichiesta)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from ric in context.ColoniePersonaleRichieste
                            where ric.Id == idRichiesta
                            select ric;

                ColoniePersonaleRichiesta richiesta = query.Single();
                richiesta.Respinta = true;
                //if (!String.IsNullOrWhiteSpace(note))
                //{
                //    richiesta.RespintaNote = note;
                //}

                context.SaveChanges();
            }
        }

        public void SalvaNote(Int32 idRichiesta, String note)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from ric in context.ColoniePersonaleRichieste
                            where ric.Id == idRichiesta
                            select ric;

                ColoniePersonaleRichiesta richiesta = query.Single();
                richiesta.RespintaNote = note;

                context.SaveChanges();
            }
        }

        public void UpdateRichiestaRifiutoInvio(Int32 idRichiesta)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from ric in context.ColoniePersonaleRichieste
                            where ric.Id == idRichiesta
                            select ric;

                ColoniePersonaleRichiesta richiesta = query.Single();
                richiesta.InviatoRifiuto = DateTime.Now;

                context.SaveChanges();
            }
        }

        public List<PrecedenteCandidatura> GetPrecedentiCandidature(String codiceFiscale, Int32 anno)
        {
            List<PrecedenteCandidatura> precedentiCandidature = new List<PrecedenteCandidatura>();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from ric in context.ColoniePersonaleRichieste
                            where ric.CodiceFiscale == codiceFiscale && ric.ColonieVacanza.Anno < anno
                            orderby ric.ColonieVacanza.Anno descending
                            select ric;

                var richieste = query.ToList();

                foreach (ColoniePersonaleRichiesta ric in richieste)
                {
                    PrecedenteCandidatura prec = new PrecedenteCandidatura();
                    precedentiCandidature.Add(prec);

                    prec.Anno = ric.ColonieVacanza.Anno;
                    prec.Stato = TraduciStato(GeneraStato(ric));
                    prec.Note = ric.RespintaNote;
                }
            }

            return precedentiCandidature;
        }

        public void UpdateRichiestaEmail(int idRichiesta, string email)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from ric in context.ColoniePersonaleRichieste
                            where ric.Id == idRichiesta
                            select ric;

                ColoniePersonaleRichiesta richiesta = query.Single();
                richiesta.Email = email;

                context.SaveChanges();
            }
        }
    }
}
