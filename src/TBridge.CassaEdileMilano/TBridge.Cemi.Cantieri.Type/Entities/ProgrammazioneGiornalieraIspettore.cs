using System;
using TBridge.Cemi.Cantieri.Type.Collections;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class ProgrammazioneGiornalieraIspettore
    {
        private readonly DateTime giorno;
        private readonly Ispettore ispettore;
        private readonly AttivitaCollection listaAttivita;

        public ProgrammazioneGiornalieraIspettore(Ispettore ispettore, DateTime giorno, AttivitaCollection listaAttivita)
        {
            this.ispettore = ispettore;
            this.giorno = giorno;
            this.listaAttivita = listaAttivita;
        }

        public Ispettore Ispettore
        {
            get { return ispettore; }
        }

        public DateTime Giorno
        {
            get { return giorno; }
        }

        public AttivitaCollection ListaAttivita
        {
            get { return listaAttivita; }
        }
    }
}