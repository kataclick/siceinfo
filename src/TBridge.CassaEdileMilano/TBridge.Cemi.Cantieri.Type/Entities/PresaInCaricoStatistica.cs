﻿using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class PresaInCaricoStatistica
    {
        public StatoIspezione? StatoIspezione { get; set; }

        public Int32 NumeroPreseInCarico { get; set; }

        public Int32 NumeroRapportiIspezione { get; set; }
    }
}
