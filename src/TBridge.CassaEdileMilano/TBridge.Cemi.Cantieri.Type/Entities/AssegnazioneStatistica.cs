﻿using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class AssegnazioneStatistica
    {
        public Boolean Assegnati { get; set; }

        public Int32 NumeroCantieri { get; set; }

        public Int32 NumeroPresiInCarico { get; set; }
    }
}
