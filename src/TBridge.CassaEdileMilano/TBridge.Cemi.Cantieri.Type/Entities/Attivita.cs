using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class Attivita
    {
        private readonly Cantiere cantiere;
        private readonly bool consuntivato;
        private readonly string descrizione;
        private readonly DateTime giorno;
        private readonly Ispettore ispettore;
        private readonly string nomeAttivita;
        private readonly DateTime ora;
        private readonly bool preventivato;
        private readonly bool programmato;
        private int? idAttivita;

        public Attivita(int? idAttivita, DateTime giorno, DateTime ora, string nomeAttivita, string descrizione,
                        Ispettore ispettore, Cantiere cantiere, bool programmato, bool preventivato, bool consuntivato)
        {
            this.idAttivita = idAttivita;
            this.giorno = giorno;
            this.ora = ora;
            this.nomeAttivita = nomeAttivita;
            this.descrizione = descrizione;
            this.ispettore = ispettore;
            this.cantiere = cantiere;
            this.programmato = programmato;
            this.preventivato = preventivato;
            this.consuntivato = consuntivato;
        }

        public int? IdAttivita
        {
            set { idAttivita = value; }
            get { return idAttivita; }
        }

        public DateTime Giorno
        {
            get { return giorno; }
        }

        public DateTime Ora
        {
            get { return ora; }
        }

        public string NomeAttivita
        {
            get { return nomeAttivita; }
        }

        public string Descrizione
        {
            get { return descrizione; }
        }

        public Ispettore Ispettore
        {
            get { return ispettore; }
        }

        public string NomeIspettore
        {
            get
            {
                if (ispettore != null) return ispettore.NomeCompleto;
                else return string.Empty;
            }
        }

        public Cantiere Cantiere
        {
            get { return cantiere; }
        }

        public bool Programmato
        {
            get { return programmato; }
        }

        public bool Preventivato
        {
            get { return preventivato; }
        }

        public bool Consuntivato
        {
            get { return consuntivato; }
        }

        public string NomeCompletoCantiere
        {
            get
            {
                if (cantiere != null)
                    return
                        cantiere.Indirizzo + " " + cantiere.Civico + " - " + cantiere.Comune + " " + cantiere.Provincia;
                else return string.Empty;
            }
        }
    }
}