using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class RAC
    {
        private string descrizione;
        private string idRAC;

        public RAC(string idRAC, string descrizione)
        {
            this.idRAC = idRAC;
            this.descrizione = descrizione;
        }

        public string IdRAC
        {
            get { return idRAC; }
            set { idRAC = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }
    }
}