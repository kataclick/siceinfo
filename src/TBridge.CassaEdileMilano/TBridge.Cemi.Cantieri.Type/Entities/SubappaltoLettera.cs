using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class SubappaltoLettera
    {
        private int? idCantieriImpresa;
        private int? idImpresaSubappaltata;

        public int? IdCantieriImpresa
        {
            get { return idCantieriImpresa; }
            set { idCantieriImpresa = value; }
        }

        public TipologiaImpresa TipoImpresa
        {
            get
            {
                if (idCantieriImpresa.HasValue) return TipologiaImpresa.Cantieri;
                else return TipologiaImpresa.SiceNew;
            }
        }

        public int IdImpresa
        {
            get
            {
                if (idCantieriImpresa.HasValue) return idCantieriImpresa.Value;
                else return idImpresaSubappaltata.Value;
            }
        }

        public string RagioneSocialeCantieriImp { get; set; }

        public int? IdImpresaSubappaltata
        {
            get { return idImpresaSubappaltata; }
            set { idImpresaSubappaltata = value; }
        }

        public string RagioneSocialeImp { get; set; }
    }
}