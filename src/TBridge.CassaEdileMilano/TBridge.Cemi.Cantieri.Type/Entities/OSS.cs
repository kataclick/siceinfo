using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class OSS
    {
        private string descrizione;
        private int idOSS;

        public OSS(int idOSS, string descrizione)
        {
            this.idOSS = idOSS;
            this.descrizione = descrizione;
        }

        public int IdOSS
        {
            get { return idOSS; }
            set { idOSS = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }
    }
}