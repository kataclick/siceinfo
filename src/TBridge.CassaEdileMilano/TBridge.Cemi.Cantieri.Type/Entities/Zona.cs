using System;
using TBridge.Cemi.GestioneUtenti.Type.Collections;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Zona
    {
        private readonly string descrizione;
        private readonly CAPCollection listaCap;
        private readonly string nome;
        private int? idZona;

        public Zona(int? idZona, string nome, string descrizione, CAPCollection listaCap)
        {
            this.idZona = idZona;
            this.nome = nome;
            this.descrizione = descrizione;

            if (listaCap != null)
                this.listaCap = listaCap;
            else
                this.listaCap = new CAPCollection();
        }

        public int? IdZona
        {
            set { idZona = value; }
            get { return idZona; }
        }

        public string Nome
        {
            get { return nome; }
        }

        public string Descrizione
        {
            get { return descrizione; }
        }

        public CAPCollection ListaCap
        {
            get { return listaCap; }
        }
    }
}