namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class CantiereFonte
    {
        public int Id { get; set; }

        public string Indirizzo { get; set; }

        public string Civico { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string IndirizzoCompleto
        {
            get { return Indirizzo + " " + Civico + " " + Comune + " " + Provincia; }
        }

        public string NomeFonte { get; set; }
    }
}