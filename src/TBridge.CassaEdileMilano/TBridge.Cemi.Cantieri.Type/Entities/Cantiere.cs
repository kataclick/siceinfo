using System;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Cantiere
    {
        #region ProprietÓ

        private bool attivo;
        private string cap;
        private string civico;
        private Committente committente;
        private string committenteTrovato;
        private string comune;
        private DateTime? dataFineLavori;
        private DateTime? dataInizioLavori;
        private string descrizioneLavori;
        private string direzioneLavori;
        private string fonti;
        private int? idCantiere;
        private double? importo;
        private Impresa impresaAppaltatrice;
        private string impresaAppaltatriceTrovata;
        private string indirizzo;
        private RapportoIspezioneCollection ispezioni;
        private double? latitudine;
        private double? longitudine;
        private string permessoCostruire;
        private string preIndirizzo;
        private string provincia;
        private string responsabileCantiere;
        private string responsabileProcedimento;
        private string tipoImpresaAppaltatrice;
        private TipologiaAppalto tipologiaAppalto;

        public int? IdCantiere
        {
            set { idCantiere = value; }
            get { return idCantiere; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Civico
        {
            get { return civico; }
            set { civico = value; }
        }

        public string IndirizzoMappa
        {
            get { return indirizzo + " " + civico + ", " + comune + " " + provincia; }
        }

        public string ImpresaAppaltatriceTrovata
        {
            get { return impresaAppaltatriceTrovata; }
            set { impresaAppaltatriceTrovata = value; }
        }

        public Impresa ImpresaAppaltatrice
        {
            get { return impresaAppaltatrice; }
            set { impresaAppaltatrice = value; }
        }

        public String ImpresaAppaltatriceStringa
        {
            get
            {
                if (this.ImpresaAppaltatrice != null)
                {
                    return this.ImpresaAppaltatrice.RagioneSociale;
                }
                else
                {
                    return this.ImpresaAppaltatriceTrovata;
                }
            }
        }

        public string NomeImpresa
        {
            get
            {
                if (impresaAppaltatrice != null)
                    return impresaAppaltatrice.RagioneSociale;
                else
                    return string.Empty;
            }
        }

        public string PermessoCostruire
        {
            get { return permessoCostruire; }
            set { permessoCostruire = value; }
        }

        public double? Importo
        {
            get { return importo; }
            set { importo = value; }
        }

        public DateTime? DataInizioLavori
        {
            get { return dataInizioLavori; }
            set { dataInizioLavori = value; }
        }

        public DateTime? DataFineLavori
        {
            get { return dataFineLavori; }
            set { dataFineLavori = value; }
        }

        public TipologiaAppalto TipologiaAppalto
        {
            get { return tipologiaAppalto; }
            set { tipologiaAppalto = value; }
        }

        public bool Attivo
        {
            get { return attivo; }
            set { attivo = value; }
        }

        public Committente Committente
        {
            get { return committente; }
            set { committente = value; }
        }

        public String CommittenteStringa
        {
            get
            {
                if (this.Committente != null)
                {
                    return this.Committente.RagioneSociale;
                }
                else
                {
                    return this.CommittenteTrovato;
                }
            }
        }

        public string CommittenteTrovato
        {
            get { return committenteTrovato; }
            set { committenteTrovato = value; }
        }

        public double? Latitudine
        {
            get { return latitudine; }
            set { latitudine = value; }
        }

        public double? Longitudine
        {
            get { return longitudine; }
            set { longitudine = value; }
        }

        public string DirezioneLavori
        {
            get { return direzioneLavori; }
            set { direzioneLavori = value; }
        }

        public string ResponsabileProcedimento
        {
            get { return responsabileProcedimento; }
            set { responsabileProcedimento = value; }
        }

        public string ResponsabileCantiere
        {
            get { return responsabileCantiere; }
            set { responsabileCantiere = value; }
        }

        public string DescrizioneLavori
        {
            get { return descrizioneLavori; }
            set { descrizioneLavori = value; }
        }

        public RapportoIspezioneCollection Ispezioni
        {
            get { return ispezioni; }
            set { ispezioni = value; }
        }

        public String IspezioneStringa
        {
            get
            {
                if (this.Ispezioni != null && this.Ispezioni.Count == 1)
                {
                    return this.Ispezioni[0].Giorno.ToShortDateString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public string TipoImpresaAppaltatrice
        {
            get { return tipoImpresaAppaltatrice; }
            set { tipoImpresaAppaltatrice = value; }
        }

        public string Fonti
        {
            get { return fonti; }
            set { fonti = value; }
        }

        // Utilizzato per Notifiche preliminari

        public string PreIndirizzo
        {
            get { return preIndirizzo; }
            set { preIndirizzo = value; }
        }

        #region ProprietÓ per Iscrizione CE

        private string frazione;

        public string Frazione
        {
            get { return frazione; }
            set { frazione = value; }
        }

        #endregion

        public Segnalazione Segnalazione { get; set; }
        public String SegnalazioneStringa
        {
            get
            {
                if (this.Segnalazione != null)
                {
                    return this.Segnalazione.Data.ToShortDateString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String SegnalazioneMotivazione
        {
            get
            {
                if (this.Segnalazione != null)
                {
                    return this.Segnalazione.Motivazione.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String SegnalazionePriorita
        {
            get
            {
                if (this.Segnalazione != null)
                {
                    return this.Segnalazione.Priorita.ToString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public String SegnalazioneNote
        {
            get
            {
                if (this.Segnalazione != null)
                {
                    return this.Segnalazione.Note;
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public Assegnazione Assegnazione { get; set; }
        public String AssegnazioneStringa
        {
            get
            {
                if (this.Assegnazione != null)
                {
                    return this.Assegnazione.Data.ToShortDateString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public TBridge.Cemi.Type.Domain.CantieriCalendarioAttivita PresaInCarico { get; set; }
        public String PresaInCaricoStringa
        {
            get
            {
                if (this.PresaInCarico != null)
                {
                    return this.PresaInCarico.Data.ToShortDateString();
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public Int32? NotificaRiferimento { get; set; }

        public StatoIspezione? StatoIspezione { get; set; }

        public String ProtocolloRegionaleNotifica { get; set; }

        #endregion

        #region Costruttori

        /// <summary>
        /// Costruttore per la serializzazione
        /// </summary>
        public Cantiere()
        {
        }

        public Cantiere(int? idCantiere, string provincia, string comune, string cap,
                        string indirizzo, string civico, string impresaAppaltatriceTrovata, string permessoCostruire,
                        double? importo, DateTime? dataInizioLavori, DateTime? dataFineLavori,
                        TipologiaAppalto tipologiaAppalto,
                        bool attivo, Committente committente, string committenteTrovato, Impresa impresa,
                        double? latitudine,
                        double? longitudine, string direzioneLavori, string responsabileProcedimento,
                        string responsabileCantiere,
                        string descrizioneLavori, string tipoImpresaAppaltatrice, string fonti)
        {
            this.idCantiere = idCantiere;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.indirizzo = indirizzo;
            this.civico = civico;
            this.impresaAppaltatriceTrovata = impresaAppaltatriceTrovata;
            this.permessoCostruire = permessoCostruire;
            this.importo = importo;
            this.dataInizioLavori = dataInizioLavori;
            this.dataFineLavori = dataFineLavori;
            this.tipologiaAppalto = tipologiaAppalto;
            this.attivo = attivo;
            this.committente = committente;
            this.committenteTrovato = committenteTrovato;
            impresaAppaltatrice = impresa;
            this.impresaAppaltatriceTrovata = impresaAppaltatriceTrovata;
            this.latitudine = latitudine;
            this.longitudine = longitudine;
            this.direzioneLavori = direzioneLavori;
            this.responsabileProcedimento = responsabileProcedimento;
            this.responsabileCantiere = responsabileCantiere;
            this.descrizioneLavori = descrizioneLavori;
            this.tipoImpresaAppaltatrice = tipoImpresaAppaltatrice;
            this.fonti = fonti;
        }

        #endregion
    }
}