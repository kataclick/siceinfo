﻿using TBridge.Cemi.Cantieri.Type.Collections;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class CantieriStatistiche
    {
        public CantieriStatistiche()
        {
            this.Segnalazioni = new SegnalazioneStatisticaCollection();
            this.Assegnazioni = new AssegnazioneStatisticaCollection();
            this.AssegnazioniPerIspettore = new AssegnazionePerIspettoreStatisticaCollection();
            this.PreseInCarico = new PresaInCaricoStatisticaCollection();
            this.PreseInCaricoPerIspettore = new PresaInCaricoPerIspettoreStatisticaCollection();
        }

        public SegnalazioneStatisticaCollection Segnalazioni { get; set; }

        public AssegnazioneStatisticaCollection Assegnazioni { get; set; }

        public AssegnazionePerIspettoreStatisticaCollection AssegnazioniPerIspettore { get; set; }

        public PresaInCaricoStatisticaCollection PreseInCarico { get; set; }

        public PresaInCaricoPerIspettoreStatisticaCollection PreseInCaricoPerIspettore { get; set; }
    }
}
