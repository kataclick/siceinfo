﻿using System;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Segnalazione
    {
        public Int32 IdSegnalazione { get; set; }

        public Int32 IdCantiere { get; set; }

        public DateTime Data { get; set; }

        public Boolean Ricorrente { get; set; }

        public CantiereSegnalazioneMotivazione Motivazione { get; set; }

        public CantiereSegnalazionePriorita Priorita { get; set; }

        public Int32 IdUtente { get; set; }

        public String NomeUtente { get; set; }

        public Int32? IdSegnalazioneSuccessiva { get; set; }

        public String Note { get; set; }
    }
}
