namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class TipologiaAttivita
    {
        private readonly string descrizione;
        private readonly string nome;

        public TipologiaAttivita(string nome, string descrizione)
        {
            this.nome = nome;
            this.descrizione = descrizione;
        }

        public string Nome
        {
            get { return nome; }
        }

        public string Descrizione
        {
            get { return descrizione; }
        }
    }
}