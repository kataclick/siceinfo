﻿using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class PresaInCaricoPerIspettoreStatistica
    {
        public Int32 IdIspettore { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CognomeNome
        {
            get
            {
                return String.Format("{0} {1}", this.Cognome, this.Nome);
            }
        }

        public Int32 NumeroPreseInCarico { get; set; }
    }
}
