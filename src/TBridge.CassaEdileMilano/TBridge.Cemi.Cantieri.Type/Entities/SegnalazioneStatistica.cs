﻿using System;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    public class SegnalazioneStatistica
    {
        public CantiereSegnalazionePriorita CantieriSegnalazionePriorita { get; set; }

        public Int32 NumeroCantieri { get; set; }

        public Int32 NumeroAssegnatiRifiutati { get; set; }
    }
}
