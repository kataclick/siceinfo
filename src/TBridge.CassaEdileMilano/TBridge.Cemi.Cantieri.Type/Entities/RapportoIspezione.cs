using System;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class RapportoIspezione
    {
        private DateTime? audit1Alle;
        private DateTime? audit1Dalle;
        private DateTime? audit1Data;
        private DateTime? audit2Alle;
        private DateTime? audit2Dalle;
        private DateTime? audit2Data;
        private string audit2Presso;
        private int? avanzamento;
        private Cantiere cantiere;
        private ComunicazioneRapporto comunicatoA;
        private DateTime? comunicatoIl;
        private EsitoIspezione esito;
        private DateTime giorno;
        private IspettoreCollection gruppoIspezione;
        private int? idAttivita;
        private int? idIspezione;
        private Ispettore ispettore;
        private RapportoIspezioneLavoratoreCollection lavoratori;
        private string note;
        private string noteIspettore1;
        private string noteIspettore2;
        private int? numOsservazioni;
        private int? numRAC;
        private int? numRilievi;
        private RapportoIspezioneImpresaCollection rapportiImpresa;
        private SegnalazionePervenuta? segnalazione;
        private StatoIspezione? stato;

        public RapportoIspezione()
        {
        }

        public RapportoIspezione(int idIspezione, DateTime giorno, Cantiere cantiere, Ispettore ispettore,
                                 EsitoIspezione esito,
                                 int? numRilievi, int? numOsservazioni, int? numRAC, string note,
                                 ComunicazioneRapporto comunicatoA, DateTime? comunicatoIl,
                                 SegnalazionePervenuta? segnalazione, StatoIspezione? stato, int? avanzamento,
                                 string noteIspettore1, DateTime? audit1Dalle,
                                 DateTime? audit1Alle, DateTime? audit1Data, string noteIspettore2,
                                 DateTime? audit2Dalle, DateTime? audit2Alle, DateTime? audit2Data,
                                 string audit2Presso, int? idAttivita)
        {
            this.idIspezione = idIspezione;
            this.giorno = giorno;
            this.cantiere = cantiere;
            this.ispettore = ispettore;
            this.esito = esito;
            this.numRilievi = numRilievi;
            this.numOsservazioni = numOsservazioni;
            this.numRAC = numRAC;
            this.note = note;
            this.comunicatoA = comunicatoA;
            this.comunicatoIl = comunicatoIl;
            this.segnalazione = segnalazione;
            this.stato = stato;
            this.avanzamento = avanzamento;
            this.noteIspettore1 = noteIspettore1;
            this.audit1Dalle = audit1Dalle;
            this.audit1Alle = audit1Alle;
            this.audit1Data = audit1Data;
            this.noteIspettore2 = noteIspettore2;
            this.audit2Dalle = audit2Dalle;
            this.audit2Alle = audit2Alle;
            this.audit2Data = audit2Data;
            this.audit2Presso = audit2Presso;
            this.idAttivita = idAttivita;
        }

        public RapportoIspezione(DateTime giorno, Cantiere cantiere, Ispettore ispettore, int? idAttivita)
        {
            idIspezione = null;
            this.giorno = giorno;
            this.cantiere = cantiere;
            this.ispettore = ispettore;
            this.idAttivita = idAttivita;
        }

        public int? IdIspezione
        {
            get { return idIspezione; }
            set { idIspezione = value; }
        }

        public DateTime Giorno
        {
            get { return giorno; }
            set { giorno = value; }
        }

        public Cantiere Cantiere
        {
            get { return cantiere; }
            set { cantiere = value; }
        }

        public string NomeCantiere
        {
            get
            {
                if (cantiere != null)
                    return
                        cantiere.Indirizzo + " " + cantiere.Civico + Environment.NewLine + cantiere.Comune + " " +
                        cantiere.Provincia;
                else
                    return string.Empty;
            }
        }

        public Ispettore Ispettore
        {
            get { return ispettore; }
            set { ispettore = value; }
        }

        public string NomeIspettore
        {
            get
            {
                if (ispettore != null)
                    return ispettore.Cognome + " " + ispettore.Nome;
                else
                    return string.Empty;
            }
        }

        public RapportoIspezioneImpresaCollection RapportiImpresa
        {
            get { return rapportiImpresa; }
            set { rapportiImpresa = value; }
        }

        public RapportoIspezioneLavoratoreCollection Lavoratori
        {
            get { return lavoratori; }
            set { lavoratori = value; }
        }

        public EsitoIspezione Esito
        {
            get { return esito; }
            set { esito = value; }
        }

        public int? NumRilievi
        {
            get { return numRilievi; }
            set { numRilievi = value; }
        }

        public int? NumOsservazioni
        {
            get { return numOsservazioni; }
            set { numOsservazioni = value; }
        }

        public int? NumRAC
        {
            get { return numRAC; }
            set { numRAC = value; }
        }

        public string Note
        {
            get { return note; }
            set { note = value; }
        }

        public ComunicazioneRapporto ComunicatoA
        {
            get { return comunicatoA; }
            set { comunicatoA = value; }
        }

        public DateTime? ComunicatoIl
        {
            get { return comunicatoIl; }
            set { comunicatoIl = value; }
        }

        public SegnalazionePervenuta? Segnalazione
        {
            get { return segnalazione; }
            set { segnalazione = value; }
        }

        public StatoIspezione? Stato
        {
            get { return stato; }
            set { stato = value; }
        }

        public int? Avanzamento
        {
            get { return avanzamento; }
            set { avanzamento = value; }
        }

        public string NoteIspettore1
        {
            get { return noteIspettore1; }
            set { noteIspettore1 = value; }
        }

        public DateTime? Audit1Dalle
        {
            get { return audit1Dalle; }
            set { audit1Dalle = value; }
        }

        public DateTime? Audit1Alle
        {
            get { return audit1Alle; }
            set { audit1Alle = value; }
        }

        public DateTime? Audit1Data
        {
            get { return audit1Data; }
            set { audit1Data = value; }
        }

        public string NoteIspettore2
        {
            get { return noteIspettore2; }
            set { noteIspettore2 = value; }
        }

        public DateTime? Audit2Dalle
        {
            get { return audit2Dalle; }
            set { audit2Dalle = value; }
        }

        public DateTime? Audit2Alle
        {
            get { return audit2Alle; }
            set { audit2Alle = value; }
        }

        public DateTime? Audit2Data
        {
            get { return audit2Data; }
            set { audit2Data = value; }
        }

        public string Audit2Presso
        {
            get { return audit2Presso; }
            set { audit2Presso = value; }
        }

        public int? IdAttivita
        {
            get { return idAttivita; }
            set { idAttivita = value; }
        }

        public IspettoreCollection GruppoIspezione
        {
            get { return gruppoIspezione; }
            set { gruppoIspezione = value; }
        }

        public String ProtocolloNotificaRegionale { get; set; }

        public DateTime? DataLetteraVerificaInCorso { get; set; }
    }
}