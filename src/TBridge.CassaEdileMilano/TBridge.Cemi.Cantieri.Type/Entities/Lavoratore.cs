using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Lavoratore
    {
        private string cognome;
        private DateTime? dataNascita;
        private int? idLavoratore;
        private string impresa;
        private string nome;
        private TipologiaLavoratore tipoLavoratore;

        public Lavoratore()
        {
        }

        public Lavoratore(TipologiaLavoratore tipoLavoratore, int? idLavoratore, string cognome, string nome,
                          DateTime? dataNascita)
        {
            this.tipoLavoratore = tipoLavoratore;
            this.idLavoratore = idLavoratore;
            this.cognome = cognome;
            this.nome = nome;
            this.dataNascita = dataNascita;
        }

        public TipologiaLavoratore TipoLavoratore
        {
            get { return tipoLavoratore; }
            set { tipoLavoratore = value; }
        }

        public int? IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public DateTime? DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }

        public string Impresa
        {
            get { return impresa; }
            set { impresa = value; }
        }
    }
}