using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Committente
    {
        private string cap;
        private string codiceFiscale;
        private string comune;
        private string fax;
        private bool fonteNotifica;
        private int? idCommittente;
        private string indirizzo;
        private bool modificato;
        private string partitaIva;
        private string personaRiferimento;
        private string provincia;
        private string ragioneSociale;
        private string telefono;

        public Committente()
        {
        }

        public Committente(int? idCommittente, string ragioneSociale, string partitaIva, string codiceFiscale,
                           string indirizzo, string comune, string provincia, string cap)
        {
            this.idCommittente = idCommittente;
            this.ragioneSociale = ragioneSociale;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;
            this.indirizzo = indirizzo;
            this.comune = comune;
            this.provincia = provincia;
            this.cap = cap;
        }

        public Committente(int? idCommittente, string ragioneSociale, string partitaIva, string codiceFiscale,
                           string indirizzo, string comune, string provincia, string cap, string telefono, string fax,
                           string personaRiferimento)
        {
            this.idCommittente = idCommittente;
            this.ragioneSociale = ragioneSociale;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;
            this.indirizzo = indirizzo;
            this.comune = comune;
            this.provincia = provincia;
            this.cap = cap;

            // Per parte Cpt
            this.telefono = telefono;
            this.fax = fax;
            this.personaRiferimento = personaRiferimento;
        }

        public int? IdCommittente
        {
            get { return idCommittente; }
            set { idCommittente = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string PartitaIva
        {
            get { return partitaIva; }
            set { partitaIva = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string NomeCompleto
        {
            get
            {
                return
                    RagioneSociale + Environment.NewLine + Indirizzo + Environment.NewLine + Comune + " - " + Provincia +
                    " " + Cap;
            }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public string PersonaRiferimento
        {
            get { return personaRiferimento; }
            set { personaRiferimento = value; }
        }

        public bool Modificato
        {
            get { return modificato; }
            set { modificato = value; }
        }

        public bool FonteNotifica
        {
            get { return fonteNotifica; }
            set { fonteNotifica = value; }
        }
    }
}