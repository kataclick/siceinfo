using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Subappalto
    {
        private int idCantiere;
        private int? idSubappalto;
        private Impresa subAppaltata;
        private Impresa subAppaltatrice;
        private TipologiaContratto tipoContratto;

        public Subappalto()
        {
        }

        public Subappalto(int? idSubappalto, int idCantiere, Impresa subAppaltatrice, Impresa subAppaltata,
                          TipologiaContratto tipoContratto)
        {
            this.idSubappalto = idSubappalto;
            this.idCantiere = idCantiere;
            this.subAppaltatrice = subAppaltatrice;
            this.subAppaltata = subAppaltata;
            this.tipoContratto = tipoContratto;
        }

        public int? IdSubappalto
        {
            get { return idSubappalto; }
            set { idSubappalto = value; }
        }

        public int IdCantiere
        {
            get { return idCantiere; }
            set { idCantiere = value; }
        }

        public Impresa SubAppaltatrice
        {
            get { return subAppaltatrice; }
            set { subAppaltatrice = value; }
        }

        public string NomeAppaltatrice
        {
            get
            {
                if (subAppaltatrice != null)
                {
                    if (subAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri) return subAppaltatrice.RagioneSociale;
                    else
                        return
                            String.Format("{0} - {1}", subAppaltatrice.IdImpresa,
                                          subAppaltatrice.RagioneSociale);
                }
                else return string.Empty;
            }
        }

        public Impresa SubAppaltata
        {
            get { return subAppaltata; }
            set { subAppaltata = value; }
        }

        public string NomeAppaltata
        {
            get
            {
                if (subAppaltata != null)
                {
                    if (subAppaltata.TipoImpresa == TipologiaImpresa.Cantieri) return subAppaltata.RagioneSociale;
                    else
                        return
                            String.Format("{0} - {1}", subAppaltata.IdImpresa, subAppaltata.RagioneSociale);
                }
                else return string.Empty;
            }
        }

        public TipologiaContratto TipoContratto
        {
            get { return tipoContratto; }
            set { tipoContratto = value; }
        }
    }
}