using System;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Type.Collections;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class RapportoIspezioneImpresa
    {
        private bool? caschiCE;
        private decimal? contributiRecuperati;
        private DateTime? daAttuare;
        private DateTime? dataChiusura;
        private int? dimensioneAziendale;
        private int? idRapportoIspezioneImpresa;
        private Impresa impresa;
        private bool nuovaIscritta;
        private int? operaiIntegrati;
        private int? operaiRegolari;
        private int? operaiRegolarizzati;
        private int? operaiTrasformati;
        private OSSCollection oss;
        private RACCollection rac;
        private bool? scarpeCE;
        private bool statistiche = true;
        private bool? tuteCE;

        public RapportoIspezioneImpresa()
        {
        }

        public RapportoIspezioneImpresa(int? idRapportoIspezioneImpresa, Impresa impresa, DateTime? daAttuare,
                                        DateTime? dataChiusura,
                                        int? operaiRegolari, int? operaiIntegrati, int? operaiTrasformati,
                                        int? operaiRegolarizzati, int? dimensioneAziendale,
                                        bool? caschiCE, bool? tuteCE, bool? scarpeCE)
        {
            this.idRapportoIspezioneImpresa = idRapportoIspezioneImpresa;
            this.impresa = impresa;
            this.daAttuare = daAttuare;
            this.dataChiusura = dataChiusura;
            this.operaiRegolari = operaiRegolari;
            this.operaiIntegrati = operaiIntegrati;
            this.operaiTrasformati = operaiTrasformati;
            this.operaiRegolarizzati = operaiRegolarizzati;
            this.dimensioneAziendale = dimensioneAziendale;
            this.caschiCE = caschiCE;
            this.tuteCE = tuteCE;
            this.scarpeCE = scarpeCE;
        }

        public int? IdRapportoIspezioneImpresa
        {
            get { return idRapportoIspezioneImpresa; }
            set { idRapportoIspezioneImpresa = value; }
        }

        public Impresa Impresa
        {
            get { return impresa; }
            set { impresa = value; }
        }

        public string NomeImpresa
        {
            get
            {
                if (impresa != null)
                {
                    if (impresa.TipoImpresa == TipologiaImpresa.Cantieri)
                        return impresa.RagioneSociale;
                    else
                        return String.Format("{0} - {1}", impresa.IdImpresa, impresa.RagioneSociale);
                }
                else
                    return string.Empty;
            }
        }

        public int? IdImpresa
        {
            get
            {
                if (impresa != null)
                    return impresa.IdImpresa;
                else
                    return null;
            }
        }

        public TipologiaImpresa TipoImpresa
        {
            get
            {
                if (impresa != null)
                    return impresa.TipoImpresa;
                else
                    throw new Exception("Impresa inesistente");
            }
        }

        public DateTime? DaAttuare
        {
            get { return daAttuare; }
            set { daAttuare = value; }
        }

        public DateTime? DataChiusura
        {
            get { return dataChiusura; }
            set { dataChiusura = value; }
        }

        public int? OperaiRegolari
        {
            get { return operaiRegolari; }
            set { operaiRegolari = value; }
        }

        public int? OperaiIntegrati
        {
            get { return operaiIntegrati; }
            set { operaiIntegrati = value; }
        }

        public int? OperaiTrasformati
        {
            get { return operaiTrasformati; }
            set { operaiTrasformati = value; }
        }

        public int? OperaiRegolarizzati
        {
            get { return operaiRegolarizzati; }
            set { operaiRegolarizzati = value; }
        }

        public int? DimensioneAziendale
        {
            get { return dimensioneAziendale; }
            set { dimensioneAziendale = value; }
        }

        public bool? CaschiCE
        {
            get { return caschiCE; }
            set { caschiCE = value; }
        }

        public bool? TuteCE
        {
            get { return tuteCE; }
            set { tuteCE = value; }
        }

        public bool? ScarpeCE
        {
            get { return scarpeCE; }
            set { scarpeCE = value; }
        }

        public OSSCollection Oss
        {
            get { return oss; }
            set { oss = value; }
        }

        public RACCollection Rac
        {
            get { return rac; }
            set { rac = value; }
        }

        public bool NuovaIscritta
        {
            get { return nuovaIscritta; }
            set { nuovaIscritta = value; }
        }

        public decimal? ContributiRecuperati
        {
            get { return contributiRecuperati; }
            set { contributiRecuperati = value; }
        }

        public bool Statistiche
        {
            get { return statistiche; }
            set { statistiche = value; }
        }

        public Decimal? ElenchiIntegrativi { get; set; }

        public Int32? OperaiTrasferta { get; set; }

        public Int32? OperaiNoAltreCE { get; set; }

        public Int32? OperaiDistacco { get; set; }

        public Int32? OperaiAltroCCNL { get; set; }

        public CassaEdileCollection CasseEdili { get; set; }

        public ContrattoCollection Contratti { get; set; }
    }
}