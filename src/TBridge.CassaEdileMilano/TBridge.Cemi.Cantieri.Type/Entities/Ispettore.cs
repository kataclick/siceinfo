using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Ispettore
    {
        private readonly bool operativo;
        private readonly Zona zona;
        private string cognome;
        private int? idIspettore;
        private string nome;

        public Ispettore()
        {
        }

        public Ispettore(int? idIspettore, string cognome, string nome, Zona zona, bool operativo)
        {
            this.idIspettore = idIspettore;
            this.cognome = cognome;
            this.nome = nome;
            this.zona = zona;
            this.operativo = operativo;
        }

        public Ispettore(int? idIspettore, string cognome, string nome, int idZona, string nomeZona,
                         string descrizioneZona, bool operativo)
        {
            this.idIspettore = idIspettore;
            this.cognome = cognome;
            this.nome = nome;
            this.operativo = operativo;

            zona = new Zona(idZona, nomeZona, descrizioneZona, null);
        }

        public int? IdIspettore
        {
            get { return idIspettore; }
            set { idIspettore = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string NomeCompleto
        {
            get { return cognome + " " + nome; }
        }

        public Zona Zona
        {
            get { return zona; }
        }

        public bool Operativo
        {
            get { return operativo; }
        }
    }
}