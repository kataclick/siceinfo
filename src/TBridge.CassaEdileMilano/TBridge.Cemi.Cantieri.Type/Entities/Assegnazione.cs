﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Assegnazione
    {
        public Int32 IdAssegnazione { get; set; }

        public Int32 IdCantiere { get; set; }

        public DateTime Data { get; set; }

        public CantieriAssegnazioneMotivazione MotivazioneRifiuto { get; set; }

        public CantieriAssegnazionePriorita Priorita { get; set; }

        public List<TBridge.Cemi.Type.Domain.Ispettore> Ispettori { get; set; }
    }
}
