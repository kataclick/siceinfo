﻿using System;

namespace TBridge.Cemi.Cantieri.Type.Entities
{
    [Serializable]
    public class Contratto
    {
        public Int32 Id { get; set; }

        public String Descrizione { get; set; }
    }
}
