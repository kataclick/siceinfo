using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type
{
    public class LetteraParam
    {
        public string Protocollo { get; set; }

        public int IdIspezione { get; set; }

        public GruppoLettera GruppoLettera { get; set; }

        public int? IdImpresa { get; set; }

        public int? IdCantieriImpresa { get; set; }

        public DateTime? DataAppuntamento { get; set; }
    }
}