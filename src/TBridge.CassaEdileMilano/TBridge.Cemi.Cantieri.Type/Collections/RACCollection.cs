using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    [Serializable]
    public class RACCollection : List<RAC>
    {
        public new bool Contains(RAC rac)
        {
            bool res = false;

            foreach (RAC r in this)
            {
                if (r.IdRAC == rac.IdRAC)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }
    }
}