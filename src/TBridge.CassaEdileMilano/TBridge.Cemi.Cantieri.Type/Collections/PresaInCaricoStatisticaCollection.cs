﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    public class PresaInCaricoStatisticaCollection : List<PresaInCaricoStatistica>
    {
        public Int32 TotalePresiInCarico
        {
            get
            {
                Int32 totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    totale += presInCarico.NumeroPreseInCarico;
                }

                return totale;
            }
        }

        public Int32 TotaleIspezioni
        {
            get
            {
                Int32 totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    if (presInCarico.StatoIspezione.HasValue)
                    {
                        totale += presInCarico.NumeroPreseInCarico;
                    }
                }

                return totale;
            }
        }

        public Int32 TotaleIspezioniInVerifica
        {
            get
            {
                Int32 totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    if (presInCarico.StatoIspezione.HasValue && presInCarico.StatoIspezione.Value == StatoIspezione.InVerifica)
                    {
                        totale += presInCarico.NumeroPreseInCarico;
                    }
                }

                return totale;
            }
        }

        public Int32 TotaleIspezioniConcluse
        {
            get
            {
                Int32 totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    if (presInCarico.StatoIspezione.HasValue && presInCarico.StatoIspezione.Value == StatoIspezione.Conclusa)
                    {
                        totale += presInCarico.NumeroPreseInCarico;
                    }
                }

                return totale;
            }
        }
    }
}
