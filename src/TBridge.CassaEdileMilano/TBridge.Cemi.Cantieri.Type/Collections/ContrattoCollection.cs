﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    [Serializable]
    public class ContrattoCollection : List<Contratto>
    {
        public Boolean PresenteNellaLista(Int32 idContratto)
        {
            foreach (Contratto contratto in this)
            {
                if (contratto.Id == idContratto)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
