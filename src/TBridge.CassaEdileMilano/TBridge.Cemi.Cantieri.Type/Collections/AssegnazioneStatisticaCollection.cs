﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    public class AssegnazioneStatisticaCollection : List<AssegnazioneStatistica>
    {
        public Int32 Totale
        {
            get
            {
                Int32 totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    totale += assStat.NumeroCantieri;
                }

                return totale;
            }
        }

        public Int32 TotaleAssegnati
        {
            get
            {
                Int32 totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    if (assStat.Assegnati)
                    {
                        totale += assStat.NumeroCantieri;
                    }
                }

                return totale;
            }
        }

        public Int32 TotaleRifiutati
        {
            get
            {
                Int32 totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    if (!assStat.Assegnati)
                    {
                        totale += assStat.NumeroCantieri;
                    }
                }

                return totale;
            }
        }

        public Int32 TotalePresiInCarico
        {
            get
            {
                Int32 totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    totale += assStat.NumeroPresiInCarico;
                }

                return totale;
            }
        }
    }
}
