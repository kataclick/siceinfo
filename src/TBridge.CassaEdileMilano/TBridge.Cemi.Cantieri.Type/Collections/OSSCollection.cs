using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    [Serializable]
    public class OSSCollection : List<OSS>
    {
        public new bool Contains(OSS oss)
        {
            bool res = false;

            foreach (OSS o in this)
            {
                if (o.IdOSS == oss.IdOSS)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }
    }
}