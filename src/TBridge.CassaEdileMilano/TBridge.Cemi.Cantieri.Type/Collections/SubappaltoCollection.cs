using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    [Serializable]
    public class SubappaltoCollection : List<Subappalto>
    {
        public Impresa FindImpresa(String codiceFiscale)
        {
            foreach (Subappalto subappalto in this)
            {
                if (subappalto.SubAppaltata != null
                    && subappalto.SubAppaltata.CodiceFiscale == codiceFiscale)
                {
                    return subappalto.SubAppaltata;
                }

                if (subappalto.SubAppaltatrice != null
                    && subappalto.SubAppaltatrice.CodiceFiscale == codiceFiscale)
                {
                    return subappalto.SubAppaltatrice;
                }
            }

            return null;
        }
    }
}