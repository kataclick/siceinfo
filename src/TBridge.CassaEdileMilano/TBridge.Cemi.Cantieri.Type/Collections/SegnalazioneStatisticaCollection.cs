﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    public class SegnalazioneStatisticaCollection : List<SegnalazioneStatistica>
    {
        public Int32 Totale
        {
            get
            {
                Int32 totale = 0;

                foreach (SegnalazioneStatistica segStat in this)
                {
                    totale += segStat.NumeroCantieri;
                }

                return totale;
            }
        }

        public Int32 TotaleAssegnazioniRifiuti
        {
            get
            {
                Int32 totale = 0;

                foreach (SegnalazioneStatistica segStat in this)
                {
                    totale += segStat.NumeroAssegnatiRifiutati;
                }

                return totale;
            }
        }
    }
}
