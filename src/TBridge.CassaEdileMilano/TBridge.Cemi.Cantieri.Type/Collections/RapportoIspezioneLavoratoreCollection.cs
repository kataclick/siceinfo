using System;
using System.Collections.Generic;
using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Collections
{
    [Serializable]
    public class RapportoIspezioneLavoratoreCollection : List<RapportoIspezioneLavoratore>
    {
    }
}