using TBridge.Cemi.Cantieri.Type.Entities;

namespace TBridge.Cemi.Cantieri.Type.Delegates
{
    public delegate void CantieriSelectedEventHandler(Cantiere cantiere);

    public delegate void CommittentiSelectedEventHandler(Committente committente);

    public delegate void ImpreseSelectedEventHandler(Impresa impresa);

    public delegate void LavoratoriSelectedEventHandler(Lavoratore lavoratore);

    public delegate void IspezioneSelectedEventHandler(RapportoIspezione ispezione);

    public delegate void CantiereGeneratoEventHandler();
}