using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Filters
{
    [Serializable]
    public class IspezioniFilter
    {
        private DateTime? al;
        private String appaltatrice;
        private DateTime? dal;
        private Int32? idAttivita;
        private Int32? idCantiere;
        private Int32? idIspettore;
        private Int32? idIspezione;
        private StatoIspezione? stato;
        private String subAppaltatrice;

        public Int32? IdIspezione
        {
            get { return idIspezione; }
            set { idIspezione = value; }
        }

        public DateTime? Dal
        {
            get { return dal; }
            set { dal = value; }
        }

        public DateTime? Al
        {
            get { return al; }
            set { al = value; }
        }

        public Int32? IdIspettore
        {
            get { return idIspettore; }
            set { idIspettore = value; }
        }

        public StatoIspezione? Stato
        {
            get { return stato; }
            set { stato = value; }
        }

        public Int32? IdCantiere
        {
            get { return idCantiere; }
            set { idCantiere = value; }
        }

        public String Appaltatrice
        {
            get { return appaltatrice; }
            set { appaltatrice = value; }
        }

        public String SubAppaltatrice
        {
            get { return subAppaltatrice; }
            set { subAppaltatrice = value; }
        }

        public Int32? IdAttivita
        {
            get { return idAttivita; }
            set { idAttivita = value; }
        }

        public String Provincia { get; set; }

        public Int32? IdImpresa { get; set; }
    }
}