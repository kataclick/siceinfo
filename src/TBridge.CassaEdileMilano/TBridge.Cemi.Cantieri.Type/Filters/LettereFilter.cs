﻿using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Filters
{
    public class LettereFilter
    {
        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public GruppoLettera? TipoLettera { get; set; }

        public String Protocollo { get; set; }

        public String IndirizzoCantiere { get; set; }

        public String ComuneCantiere { get; set; }

        public Int32? IdImpresa { get; set; }

        public String RagioneSocialeImpresa { get; set; }

        public String IvaFiscImpresa { get; set; }
    }
}
