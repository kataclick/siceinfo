﻿using System;
using TBridge.Cemi.Cantieri.Type.Enums;

namespace TBridge.Cemi.Cantieri.Type.Filters
{
    public class CantieriFilter
    {
        public Int32? IdCantiere { get; set; }

        public String Provincia { get; set; }

        public String Comune { get; set; }

        public String Indirizzo { get; set; }

        public Double? Importo { get; set; }

        public Int32? IdZona { get; set; }

        public Int32? IdImpresa { get; set; }

        public String Impresa { get; set; }

        public String CodiceFiscaleImpresa { get; set; }

        public String Committente { get; set; }

        public Boolean? Segnalati { get; set; }

        public StatoAssegnazioneFiltro? Assegnati { get; set; }

        public StatoPresaInCaricoFiltro? PresaInCarico { get; set; }

        public StatoRapportoIspezioneFiltro RapportoIspezione { get; set; }

        public Int32? IdIspettoreAssegnato { get; set; }

        public Int32? IdIspettorePresoInCarico { get; set; }

        public Boolean NonCollegatiAttivita { get; set; }

        public String Cap { get; set; }

        public DateTime? DataInserimentoDal { get; set; }

        public DateTime? DataInserimentoAl { get; set; }

        public DateTime? DataSegnalazioneDal { get; set; }

        public DateTime? DataSegnalazioneAl { get; set; }

        public DateTime? DataAssegnazioneDal { get; set; }

        public DateTime? DataAssegnazioneAl { get; set; }

        public DateTime? DataPresaCaricoDal { get; set; }

        public DateTime? DataPresaCaricoAl { get; set; }
    }
}
