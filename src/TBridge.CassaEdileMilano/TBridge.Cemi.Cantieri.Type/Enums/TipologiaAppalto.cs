namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum TipologiaAppalto
    {
        NonDefinito = 0,
        Pubblico,
        Privato
    }
}