namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum EsitoIspezione
    {
        Indefinita = 0,
        Positiva,
        Negativa
    }
}