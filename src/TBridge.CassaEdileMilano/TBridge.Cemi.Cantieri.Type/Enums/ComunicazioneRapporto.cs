namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum ComunicazioneRapporto
    {
        Archivio = 0,
        UfficioDatoriDiLavoro,
        UfficioCRC
    }
}