namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum GruppoLettera
    {
        VerificaInCorso = 1,
        IrregolaritaRiscontrate,
        IrregolaritaRiscontrateConAppuntamento,
        EsitoPositivo,
        EsitoPositivoConSubappaltatrici,
        IrregolaritaRiscontrateAlCommittente,
        Sindacati,
        BollinoBlu,
        IrregolaritaRiscontrateAlComune,
        IrregolaritaRiscontrateAlComune01,
        VerificaInCorsoComune
    }
}