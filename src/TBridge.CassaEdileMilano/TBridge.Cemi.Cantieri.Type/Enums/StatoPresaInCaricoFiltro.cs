namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum StatoPresaInCaricoFiltro
    {
        Tutti = 0,
        PresiInCarico,
        NonPresiInCarico,
        Rifiutati
    }
}