namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum StatoIspezione
    {
        InVerifica = 1,
        Conclusa
    }
}