namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum TipologiaLettera
    {
        M70505,
        M7050601,
        M7050602,
        M7050603,
        M70506,
        M7050901,
        M70509,
        M70510,
        M70511,
        M70501,
        M7050501,
        M70511Comune,
        M7051101Comune,
        M7050502Comune
    }
}