namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum TipologiaContratto
    {
        ContrattoEdile = 0,
        AltroContratto
    }
}