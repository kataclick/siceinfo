namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum StatoRapportoIspezioneFiltro
    {
        Tutti = 0,
        ConRapportoIspezione,
        SenzaRapportoIspezione
    }
}