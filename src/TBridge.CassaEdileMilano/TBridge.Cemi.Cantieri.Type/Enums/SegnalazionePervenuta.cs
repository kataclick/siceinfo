namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum SegnalazionePervenuta
    {
        CassaEdile = 1,
        Integrata
    }
}