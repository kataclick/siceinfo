namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum StatoAssegnazioneFiltro
    {
        Tutti = 0,
        Assegnati,
        Rifiutati,
        NonAssegnati
    }
}