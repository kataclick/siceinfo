namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum TipologiaImpresa
    {
        SiceNew = 0,
        Cantieri,
        TutteLeFonti
    }
}