namespace TBridge.Cemi.Cantieri.Type.Enums
{
    public enum TipologiaLavoratore
    {
        SiceNew = 0,
        Cantieri,
        Notifiche,
        TutteLeFonti
    }
}