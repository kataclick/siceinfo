﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Crm.ConsoleApp
{
    public class Prestazione
    {
        public int Id { get; set; }
        public string TipoPrestazione { get; set; }
        public string TipoCausale { get; set; }
        public string DescrizioneEstesaCausale { get; set; }
        public DateTime DataDomanda { get; set; }
        public int IdLavoratore { get; set; }
        public string Stato { get; set; }
    }
}
