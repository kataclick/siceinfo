﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using TBridge.Cemi.Business.Crm;
using TBridge.Cemi.Crm.ConsoleApp.EmailService;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Crm.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                if (args.Length > 0)
                {
                    switch (args[0])
                    {
                        case "Respinte":
                            DateTime dataRifiuto = DateTime.Today;
                            if (args.Length > 1)
                            {
                                DateTime date;
                                if (DateTime.TryParse(args[1], out date))
                                    dataRifiuto = date;
                            }
                            ComunicaRespinte(dataRifiuto);
                            break;
                        case "Sospese":
                            DateTime dataSospensione = DateTime.Today;
                            if (args.Length > 1)
                            {
                                DateTime date;
                                if (DateTime.TryParse(args[1], out date))
                                    dataSospensione = date;
                            }
                            ComunicaSospese(dataSospensione);
                            break;
                        case "Liquidate":
                            DateTime dataLiquidazione = DateTime.Today;
                            if (args.Length > 1)
                            {
                                DateTime date;
                                if (DateTime.TryParse(args[1], out date))
                                    dataLiquidazione = date;
                            }
                            ComunicaLiquidate(dataLiquidazione);
                            break;
                        case "Puntuale":
                            if (args.Length > 1)
                            {
                                int idPrestazione;
                                if (Int32.TryParse(args[1], out idPrestazione))
                                    ComunicaPrestazionePuntuale(idPrestazione);
                            }
                            break;
                        case "SP":
                            ComunicaRisultatiSp();
                            break;
                        default:
                            TracciaErrore(String.Format("Parametro non riconosciuto {0}", args[0]));
                            break;
                    }
                }
                else
                {
                    TracciaErrore("Parametri obbligatori");
                }
            }
            catch (Exception exception)
            {
                TracciaErrore(exception.Message);
            }
        }

        private static void ComunicaRisultatiSp()
        {
            PrestazioniBusiness prestazioniBusiness = new PrestazioniBusiness();
            List<int> listaIdPrestazioni = new List<int>();

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.USP_PrestazioniSelectPerNotificaCrm", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        int idPrestazioneDomandaIndex = reader.GetOrdinal("idPrestazioniDomanda");

                        while (reader.Read())
                        {
                            listaIdPrestazioni.Add(reader.GetInt32(idPrestazioneDomandaIndex));
                        }
                    }
                }
            }

            foreach (int id in listaIdPrestazioni)
            {
                Console.WriteLine(id);
                prestazioniBusiness.CallCrmWsPrestazioni(id);
            }
        }

        private static void ComunicaPrestazionePuntuale(int idPrestazione)
        {
            PrestazioniBusiness prestazioniBusiness = new PrestazioniBusiness();
            prestazioniBusiness.CallCrmWsPrestazioni(idPrestazione);
        }

        private static void ComunicaRespinte(DateTime dataRifiuto)
        {
            PrestazioniBusiness prestazioniBusiness = new PrestazioniBusiness();

            //Carico prestazioni respinte la sera prima (dopo le 19:50)
            List<Prestazione> listaPrestazioni = GetDomandeRespinte(dataRifiuto);

            List<Prestazione> listaPrestazioniConCausaleEstesa = (from prestazione in listaPrestazioni
                                                                  where
                                                                      !String.IsNullOrEmpty(
                                                                          prestazione.DescrizioneEstesaCausale)
                                                                  select prestazione).ToList();

            foreach (Prestazione prestazione in listaPrestazioniConCausaleEstesa)
            {
                Console.WriteLine(prestazione.Id);
                prestazioniBusiness.CallCrmWsPrestazioni(prestazione.Id);
            }

            List<Prestazione> listaPrestazioniSenzaCausaleEstesa = (from prestazione in listaPrestazioni
                                                                    where
                                                                        String.IsNullOrEmpty(
                                                                            prestazione.DescrizioneEstesaCausale)
                                                                    select prestazione).ToList();

            if (listaPrestazioniSenzaCausaleEstesa.Count > 0)
            {
                //Invio eMail
                InvioEMailPrestazioniSenzaCausale(listaPrestazioniSenzaCausaleEstesa);
            }

            #region Test di controllo

            bool inviaMailControllo = true;
            string inviaMailControlloString = ConfigurationManager.AppSettings["InviaMailControllo"];
            if (inviaMailControlloString == "false")
                inviaMailControllo = false;

            if (inviaMailControllo)
            {
                InviaMailControllo(listaPrestazioniConCausaleEstesa);
            }

            #endregion
        }

        private static List<Prestazione> GetDomandeRespinte(DateTime dataRifiuto)
        {
            List<Prestazione> listaPrestazioni = new List<Prestazione>();

            DateTime dataRifiutoDa = new DateTime(dataRifiuto.AddDays(-1).Year, dataRifiuto.AddDays(-1).Month,
                                                  dataRifiuto.AddDays(-1).Day, 19, 50, 0);
            DateTime dataRifiutoA = new DateTime(dataRifiuto.Year, dataRifiuto.Month, dataRifiuto.Day, 6, 00, 0);

            using (
                SqlConnection connection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                using (
                    SqlCommand command = new SqlCommand("dbo.USP_PrestazioniSelectRespintePerNotificaCrm", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@dataRifiutoDa", dataRifiutoDa);
                    command.Parameters.AddWithValue("@dataRifiutoA", dataRifiutoA);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        int idPrestazioneDomandaIndex = reader.GetOrdinal("idPrestazioniDomanda");
                        int idTipoPrestazioneIndex = reader.GetOrdinal("idTipoPrestazione");
                        int idTipoCausaleIndex = reader.GetOrdinal("idTipoCausale");
                        int descrizioneEstesaIndex = reader.GetOrdinal("descrizioneEstesa");
                        int dataDomandaIndex = reader.GetOrdinal("dataDomanda");
                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int statoIndex = reader.GetOrdinal("stato");

                        while (reader.Read())
                        {
                            Prestazione prestazione = new Prestazione {Id = reader.GetInt32(idPrestazioneDomandaIndex)};

                            if (reader[idTipoPrestazioneIndex] != DBNull.Value)
                                prestazione.TipoPrestazione = reader.GetString(idTipoPrestazioneIndex);
                            if (reader[idTipoCausaleIndex] != DBNull.Value)
                                prestazione.TipoCausale = reader.GetString(idTipoCausaleIndex);
                            if (reader[descrizioneEstesaIndex] != DBNull.Value)
                                prestazione.DescrizioneEstesaCausale = reader.GetString(descrizioneEstesaIndex);
                            if (reader[dataDomandaIndex] != DBNull.Value)
                                prestazione.DataDomanda = reader.GetDateTime(dataDomandaIndex);
                            if (reader[idLavoratoreIndex] != DBNull.Value)
                                prestazione.IdLavoratore = reader.GetInt32(idLavoratoreIndex);
                            if (reader[statoIndex] != DBNull.Value)
                                prestazione.Stato = reader.GetString(statoIndex);

                            listaPrestazioni.Add(prestazione);
                        }
                    }
                }
            }

            return listaPrestazioni;
        }

        private static void ComunicaSospese(DateTime dataSospensione)
        {
            PrestazioniBusiness prestazioniBusiness = new PrestazioniBusiness();

            //Carico prestazioni respinte la sera prima (dopo le 19:50)
            List<Prestazione> listaPrestazioni = GetDomandeSospese(dataSospensione);

            foreach (Prestazione prestazione in listaPrestazioni)
            {
                Console.WriteLine(prestazione.Id);
                prestazioniBusiness.CallCrmWsPrestazioni(prestazione.Id);
            }

            #region Test di controllo

            bool inviaMailControllo = true;
            string inviaMailControlloString = ConfigurationManager.AppSettings["InviaMailControllo"];
            if (inviaMailControlloString == "false")
                inviaMailControllo = false;

            if (inviaMailControllo)
            {
                InviaMailControllo(listaPrestazioni);
            }

            #endregion
        }

        private static List<Prestazione> GetDomandeSospese(DateTime dataSospensione)
        {
            List<Prestazione> listaPrestazioni = new List<Prestazione>();

            DateTime dataRifiutoDa = new DateTime(dataSospensione.AddDays(-1).Year, dataSospensione.AddDays(-1).Month,
                                                  dataSospensione.AddDays(-1).Day, 19, 50, 0);
            DateTime dataRifiutoA = new DateTime(dataSospensione.Year, dataSospensione.Month, dataSospensione.Day, 6, 00, 0);

            using (
                SqlConnection connection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                using (
                    SqlCommand command = new SqlCommand("dbo.USP_PrestazioniSelectSospesePerNotificaCrm", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@dataRifiutoDa", dataRifiutoDa);
                    command.Parameters.AddWithValue("@dataRifiutoA", dataRifiutoA);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        int idPrestazioneDomandaIndex = reader.GetOrdinal("idPrestazioniDomanda");
                        int idTipoPrestazioneIndex = reader.GetOrdinal("idTipoPrestazione");
                        int idTipoCausaleIndex = reader.GetOrdinal("idTipoCausale");
                        int descrizioneEstesaIndex = reader.GetOrdinal("descrizioneEstesa");
                        int dataDomandaIndex = reader.GetOrdinal("dataDomanda");
                        int idLavoratoreIndex = reader.GetOrdinal("idLavoratore");
                        int statoIndex = reader.GetOrdinal("stato");

                        while (reader.Read())
                        {
                            Prestazione prestazione = new Prestazione { Id = reader.GetInt32(idPrestazioneDomandaIndex) };

                            if (reader[idTipoPrestazioneIndex] != DBNull.Value)
                                prestazione.TipoPrestazione = reader.GetString(idTipoPrestazioneIndex);
                            if (reader[idTipoCausaleIndex] != DBNull.Value)
                                prestazione.TipoCausale = reader.GetString(idTipoCausaleIndex);
                            if (reader[descrizioneEstesaIndex] != DBNull.Value)
                                prestazione.DescrizioneEstesaCausale = reader.GetString(descrizioneEstesaIndex);
                            if (reader[dataDomandaIndex] != DBNull.Value)
                                prestazione.DataDomanda = reader.GetDateTime(dataDomandaIndex);
                            if (reader[idLavoratoreIndex] != DBNull.Value)
                                prestazione.IdLavoratore = reader.GetInt32(idLavoratoreIndex);
                            if (reader[statoIndex] != DBNull.Value)
                                prestazione.Stato = reader.GetString(statoIndex);

                            listaPrestazioni.Add(prestazione);
                        }
                    }
                }
            }

            return listaPrestazioni;
        }

        private static void InvioEMailPrestazioniSenzaCausale(IEnumerable<Prestazione> listaPrestazioniSenzaCausaleEstesa)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            bool usaMailTest = true;
            string usaMailTestString = ConfigurationManager.AppSettings["UsaMailTest"];
            if (usaMailTestString == "false")
                usaMailTest = false;

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            email.Mittente = new EmailAddress
                                 {
                                     Nome = "Support ITS",
                                     Indirizzo = "support.cemi@zenatek.it"
                                 };


            email.Oggetto = "Prestazioni non passate causa mancanza di descrizione estesa";

            #region Testo HTML

            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("<font face=\"Arial\" size=2>");

            sbTestoHtml.Append("Le seguenti prestazioni sono prive di descrizione estesa:");
            sbTestoHtml.Append("<br/>");
            sbTestoHtml.Append("<br/>");

            foreach (Prestazione prestazione in listaPrestazioniSenzaCausaleEstesa)
            {
                sbTestoHtml.Append(String.Format("Id: {0}<br/>", prestazione.Id));
                sbTestoHtml.Append(String.Format("Tipo Prestazione: {0}<br/>", prestazione.TipoPrestazione));
                sbTestoHtml.Append(String.Format("Tipo Causale: {0}<br/>", prestazione.TipoCausale));
                sbTestoHtml.Append(String.Format("Data domanda: {0}<br/>", prestazione.DataDomanda));
                sbTestoHtml.Append(String.Format("Cod. Lavoratore: {0}<br/><br/>", prestazione.IdLavoratore));
            }

            sbTestoHtml.Append("</font>");

            #endregion

            #region Testo Plain

            StringBuilder sbTestoPlain = sbTestoHtml;

            #endregion

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            List<EmailAddress> destinatari = new List<EmailAddress>();
            List<EmailAddress> destinatariBcc = new List<EmailAddress>();

            if (usaMailTest)
            {
                destinatari.Add(new EmailAddress
                                    {
                                        Indirizzo = "alessio.mosto@zenatek.it",
                                        Nome = "Alessio Mosto"
                                    });
            }
            else
            {
                destinatari.Add(new EmailAddress
                                    {
                                        Indirizzo = "nadia.manelli@cassaedilemilano.it",
                                        Nome = "Nadia Manelli"
                                    });

                destinatariBcc.Add(new EmailAddress
                                       {
                                           Indirizzo = "support.cemi@zenatek.it",
                                           Nome = "Support CEMI"
                                       });
                destinatariBcc.Add(new EmailAddress
                                        {
                                            Indirizzo = "marco.catalano@cassaedilemilano.it",
                                            Nome = "Marco Catalano"
                                        });
            }


            email.Destinatari = destinatari.ToArray();
            email.DestinatariBCC = destinatariBcc.ToArray();

            EmailInfoService client = new EmailInfoService();
            NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
            //credentials.Domain = "itsinfinity";
            client.Credentials = credentials;
            client.InviaEmail(email);
        }

        private static void InviaMailControllo(IEnumerable<Prestazione> listaPrestazioni)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            email.Mittente = new EmailAddress
                                 {
                                     Nome = "Support ITS",
                                     Indirizzo = "support.cemi@itsinfinity.com"
                                 };


            email.Oggetto = "Prestazioni passate con descrizione estesa";

            #region Testo HTML

            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("<font face=\"Arial\" size=2>");

            sbTestoHtml.Append("Le seguenti prestazioni sono passate - si spera ;) :");
            sbTestoHtml.Append("<br/>");
            sbTestoHtml.Append("<br/>");

            foreach (Prestazione prestazione in listaPrestazioni)
            {
                sbTestoHtml.Append(String.Format("Id: {0}<br/>", prestazione.Id));
                sbTestoHtml.Append(String.Format("Tipo Prestazione: {0}<br/>", prestazione.TipoPrestazione));
                sbTestoHtml.Append(String.Format("Stato: {0}<br/>", prestazione.Stato));
                sbTestoHtml.Append(String.Format("Tipo Causale: {0}<br/>", prestazione.TipoCausale));
                sbTestoHtml.Append(String.Format("Causale Estesa: {0}<br/>", prestazione.DescrizioneEstesaCausale));
                sbTestoHtml.Append(String.Format("Data domanda: {0}<br/>", prestazione.DataDomanda));
                sbTestoHtml.Append(String.Format("Cod. Lavoratore: {0}<br/><br/>", prestazione.IdLavoratore));
            }

            sbTestoHtml.Append("</font>");

            #endregion

            #region Testo Plain

            StringBuilder sbTestoPlain = sbTestoHtml;

            #endregion

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            List<EmailAddress> destinatari = new List<EmailAddress>();
            List<EmailAddress> destinatariBcc = new List<EmailAddress>();


            destinatari.Add(new EmailAddress
                                {
                                    Indirizzo = "alessio.mosto@itsinfinity.com",
                                    Nome = "Alessio Mosto"
                                });


            email.Destinatari = destinatari.ToArray();
            email.DestinatariBCC = destinatariBcc.ToArray();

            EmailInfoService client = new EmailInfoService();
            NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
            //credentials.Domain = "itsinfinity";
            client.Credentials = credentials;
            client.InviaEmail(email);
        }


        private static void ComunicaLiquidate(DateTime dataLiquidazione)
        {
            List<PrestazioneCrm> listaPrestazioni = new List<PrestazioneCrm>();

            const string query = "SELECT * FROM dbo.crm_PrestazioniLiquidate WHERE dataLiquidazione = @dataLiquidazione";

            using (
                SqlConnection connection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@dataLiquidazione", dataLiquidazione);
                    connection.Open();
                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        int idTipoPrestazioneIndex = dataReader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = dataReader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = dataReader.GetOrdinal("numeroProtocolloPrestazione");
                        int descrizioneIndex = dataReader.GetOrdinal("descrizione");
                        int beneficiarioIndex = dataReader.GetOrdinal("beneficiario");
                        int dataDomandaIndex = dataReader.GetOrdinal("dataDomanda");
                        int dataRiferimentoIndex = dataReader.GetOrdinal("dataRiferimento");
                        int idLavoratoreIndex = dataReader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = dataReader.GetOrdinal("idFamiliare");
                        int statoIndex = dataReader.GetOrdinal("stato");
                        int causaleRespintaIndex = dataReader.GetOrdinal("causaleRespinta");
                        int lavoratoreIndirizzoIndex = dataReader.GetOrdinal("lavoratoreIndirizzo");
                        int lavoratoreProvinciaIndex = dataReader.GetOrdinal("lavoratoreProvincia");
                        int lavoratoreComuneIndex = dataReader.GetOrdinal("lavoratoreComune");
                        int lavoratoreFrazioneIndex = dataReader.GetOrdinal("lavoratoreFrazione");
                        int lavoratoreCapIndex = dataReader.GetOrdinal("lavoratoreCap");
                        int lavoratoreCellulareIndex = dataReader.GetOrdinal("lavoratoreCellulare");
                        int lavoratoreEmailIndex = dataReader.GetOrdinal("lavoratoreEmail");
                        int lavoratoreIdTipoPagamentoIndex = dataReader.GetOrdinal("lavoratoreIdTipoPagamento");
                        int familiareCognomeIndex = dataReader.GetOrdinal("familiareCognome");
                        int familiareNomeIndex = dataReader.GetOrdinal("familiareNome");
                        int familiareDataNascitaIndex = dataReader.GetOrdinal("familiareDataNascita");
                        int familiareCodiceFiscaleIndex = dataReader.GetOrdinal("familiareCodiceFiscale");
                        int gradoParentelaIndex = dataReader.GetOrdinal("gradoParentela");
                        int importoErogatoLordoIndex = dataReader.GetOrdinal("importoErogatoLordo");
                        int importoErogatoIndex = dataReader.GetOrdinal("importoErogato");
                        int modalitaPagamentoIndex = dataReader.GetOrdinal("modalitaPagamento");
                        int numeroMandatoIndex = dataReader.GetOrdinal("numeroMandato");
                        int statoAssegnoIndex = dataReader.GetOrdinal("statoAssegno");
                        int descrizioneStatoAssegnoIndex = dataReader.GetOrdinal("descrizioneStatoAssegno");
                        int dataLiquidazioneIndex = dataReader.GetOrdinal("dataLiquidazione");
                        int idTipoPagamentoIndex = dataReader.GetOrdinal("idTipoPagamento");
                        int ibanIndex = dataReader.GetOrdinal("IBAN");
                        int testoGruppoIndex = dataReader.GetOrdinal("testoGruppo");

                        while (dataReader.Read())
                        {
                            PrestazioneCrm prestazioneCrm = new PrestazioneCrm();

                            // chiavi
                            prestazioneCrm.IdTipoPrestazione = dataReader.GetString(idTipoPrestazioneIndex);
                            prestazioneCrm.ProtocolloPrestazione = dataReader.GetInt32(protocolloPrestazioneIndex);
                            prestazioneCrm.NumeroProtocolloPrestazione =
                                dataReader.GetInt32(numeroProtocolloPrestazioneIndex);

                            if (dataReader[beneficiarioIndex] != DBNull.Value)
                                prestazioneCrm.Beneficiario = dataReader.GetString(beneficiarioIndex);
                            if (dataReader[causaleRespintaIndex] != DBNull.Value)
                                prestazioneCrm.CausaleRespinta = dataReader.GetString(causaleRespintaIndex);
                            if (dataReader[dataDomandaIndex] != DBNull.Value)
                                prestazioneCrm.DataDomanda = dataReader.GetDateTime(dataDomandaIndex);
                            if (dataReader[dataLiquidazioneIndex] != DBNull.Value)
                                prestazioneCrm.DataLiquidazione = dataReader.GetDateTime(dataLiquidazioneIndex);
                            if (dataReader[dataRiferimentoIndex] != DBNull.Value)
                                prestazioneCrm.DataRiferimento = dataReader.GetDateTime(dataRiferimentoIndex);
                            if (dataReader[descrizioneIndex] != DBNull.Value)
                                prestazioneCrm.Descrizione = dataReader.GetString(descrizioneIndex);
                            if (dataReader[descrizioneStatoAssegnoIndex] != DBNull.Value)
                                prestazioneCrm.DescrizioneStatoAssegno =
                                    dataReader.GetString(descrizioneStatoAssegnoIndex);
                            if (dataReader[familiareCodiceFiscaleIndex] != DBNull.Value)
                                prestazioneCrm.FamiliareCodiceFiscale =
                                    dataReader.GetString(familiareCodiceFiscaleIndex);
                            if (dataReader[familiareCognomeIndex] != DBNull.Value)
                                prestazioneCrm.FamiliareCognome = dataReader.GetString(familiareCognomeIndex);
                            if (dataReader[familiareDataNascitaIndex] != DBNull.Value)
                                prestazioneCrm.FamiliareDataNascita =
                                    dataReader.GetDateTime(familiareDataNascitaIndex);
                            if (dataReader[familiareNomeIndex] != DBNull.Value)
                                prestazioneCrm.FamiliareNome = dataReader.GetString(familiareNomeIndex);
                            if (dataReader[gradoParentelaIndex] != DBNull.Value)
                                prestazioneCrm.GradoParentela = dataReader.GetString(gradoParentelaIndex);
                            if (dataReader[ibanIndex] != DBNull.Value)
                                prestazioneCrm.Iban = dataReader.GetString(ibanIndex);
                            if (dataReader[idFamiliareIndex] != DBNull.Value)
                                prestazioneCrm.IdFamiliare = dataReader.GetInt32(idFamiliareIndex);
                            if (dataReader[idLavoratoreIndex] != DBNull.Value)
                                prestazioneCrm.IdLavoratore = dataReader.GetInt32(idLavoratoreIndex);
                            if (dataReader[idTipoPagamentoIndex] != DBNull.Value)
                                prestazioneCrm.IdTipoPagamento = dataReader.GetString(idTipoPagamentoIndex);
                            if (dataReader[importoErogatoIndex] != DBNull.Value)
                                prestazioneCrm.ImportoErogato = dataReader.GetDecimal(importoErogatoIndex);
                            if (dataReader[importoErogatoLordoIndex] != DBNull.Value)
                                prestazioneCrm.ImportoErogatoLordo = dataReader.GetDecimal(importoErogatoLordoIndex);
                            if (dataReader[lavoratoreCapIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreCap = dataReader.GetString(lavoratoreCapIndex);
                            if (dataReader[lavoratoreCellulareIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreCellulare = dataReader.GetString(lavoratoreCellulareIndex);
                            if (dataReader[lavoratoreComuneIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreComune = dataReader.GetString(lavoratoreComuneIndex);
                            if (dataReader[lavoratoreEmailIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreEmail = dataReader.GetString(lavoratoreEmailIndex);
                            if (dataReader[lavoratoreFrazioneIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreFrazione = dataReader.GetString(lavoratoreFrazioneIndex);
                            if (dataReader[lavoratoreIdTipoPagamentoIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreIdTipoPagamento =
                                    dataReader.GetString(lavoratoreIdTipoPagamentoIndex);
                            if (dataReader[lavoratoreIndirizzoIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreIndirizzo = dataReader.GetString(lavoratoreIndirizzoIndex);
                            if (dataReader[lavoratoreProvinciaIndex] != DBNull.Value)
                                prestazioneCrm.LavoratoreProvincia = dataReader.GetString(lavoratoreProvinciaIndex);
                            if (dataReader[modalitaPagamentoIndex] != DBNull.Value)
                                prestazioneCrm.ModalitaPagamento = dataReader.GetString(modalitaPagamentoIndex);
                            if (dataReader[numeroMandatoIndex] != DBNull.Value)
                                prestazioneCrm.NumeroMandato = dataReader.GetString(numeroMandatoIndex);
                            if (dataReader[statoIndex] != DBNull.Value)
                                prestazioneCrm.Stato = dataReader.GetString(statoIndex);
                            if (dataReader[statoAssegnoIndex] != DBNull.Value)
                                prestazioneCrm.StatoAssegno = dataReader.GetString(statoAssegnoIndex);
                            if (dataReader[testoGruppoIndex] != DBNull.Value)
                                prestazioneCrm.TestoGruppo = dataReader.GetString(testoGruppoIndex);

                            listaPrestazioni.Add(prestazioneCrm);
                        }
                    }
                }
            }

            //chiamo il ws
            string result = PrestazioniManager.NotificaPrestazione(listaPrestazioni);
        }


        private static void TracciaErrore(string messaggioErrore)
        {
            string logPrestazioni = ConfigurationManager.AppSettings["LogPrestazioni"];

            Console.WriteLine(messaggioErrore);

            if (logPrestazioni == "true")
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione WS CRM: {0}", messaggioErrore));
                }
            }
        }
    }
}