﻿using System;
using System.Configuration;
using TBridge.Cemi.AccessoCantieri.Business;

namespace TBridge.Cemi.AccessoCantieri.ConsoleAppImportManuale
{
    internal static class Program
    {
        private static readonly AccessoCantieriBusiness Biz = new AccessoCantieriBusiness();
        private static readonly ImportManualeConnector GenericImportBiz = new ImportManualeConnector();

        private static void Main()
        {

            CaricaAccessi();

            Console.ReadLine();
        }

        private static void CaricaAccessi()
        {

            #region GenericImport

                try
                {
                    Console.WriteLine("Importazione manuale" + DateTime.Now);
                    Int32 i = GenericImportBiz.CaricaDati();

                    if (i == -1)
                    {
                        Console.WriteLine(" Importazione manuale: Nessun file");
                    }

                    else if (i > 0)
                        Console.WriteLine(string.Format(
                            " Importazione manuale: Non lette {0} timbrature. Rilevatore non dichiarato?", i));
                    else if (i == -2)
                        Console.WriteLine(" Importazione manuale: Lettura con Problemi");
                    else
                        Console.WriteLine(" Importazione manuale: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            # region Timbrature non gestite

            if (ConfigurationManager.AppSettings["RecuperoTimbratureNonGestiteAttivo"] == "1")
            {
                Console.WriteLine("INIZIO controllo timb");
                Biz.ControllaTimbratureNonGestite();
                Console.WriteLine("FINE controllo timb");
            }

            #endregion

            #region Timbrature non gestite senza comunicazioni

            ////Console.WriteLine("INIZIO controllo timb no comunicazioni");
            ////biz.ControllaTimbratureNonGestiteNoCom();
            ////Console.WriteLine("FINE controllo timb no comunicazioni");

            #endregion

        }
    }
}
