using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.IscrizioneLavoratori.Data
{
    public class SintesiDataAccess
    {
        public SintesiDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public TipoFineRapporto GetTipoFineRapporto(string codiceCessazione)
        {
            TipoFineRapporto fineRapporto = new TipoFineRapporto();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCessazioniRLSelectByCodice"))
            {
                DatabaseCemi.AddInParameter(comando, "@codice", DbType.String, codiceCessazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        fineRapporto.Descrizione = reader["descrizione"].ToString();
                        fineRapporto.IdTipoFineRapporto = reader["idTipoFineRapporto"].ToString();
                    }
                }
            }

            return fineRapporto;
        }

        public Lavoratore GetLavoratore(string codiceFiscale, int idImpresa)
        {
            Lavoratore lavoratore = new Lavoratore();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriRicercaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, @idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        lavoratore.IdLavoratore = (int?) reader["idLavoratore"];
                        lavoratore.Cognome = reader["cognome"].ToString();
                        lavoratore.Nome = reader["nome"].ToString();
                        lavoratore.CodiceFiscale = reader["codiceFiscale"].ToString();
                        lavoratore.DataNascita = (DateTime) reader["dataNascita"];
                    }
                }
            }

            return lavoratore;
        }

        public RapportoDiLavoro GetRapportoLavoro(int? idLavoratore, int idImpresa)
        {
            RapportoDiLavoro rapportoDiLavoro = new RapportoDiLavoro();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriRapportoImpresaPersonaSelectWithFilter"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.String, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, @idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        rapportoDiLavoro.Qualifica = new TipoQualifica();
                        rapportoDiLavoro.Qualifica.IdQualifica = reader["idQualifica"].ToString();
                        rapportoDiLavoro.Qualifica.Descrizione = reader["descrizioneQualifica"].ToString();
                        rapportoDiLavoro.Mansione = new TipoMansione();
                        rapportoDiLavoro.Mansione.IdMansione = reader["idMansione"].ToString();
                        rapportoDiLavoro.Mansione.Descrizione = reader["descrizioneMansione"].ToString();
                        rapportoDiLavoro.Categoria = new TipoCategoria();
                        rapportoDiLavoro.Categoria.IdCategoria = reader["idCategoria"].ToString();
                        rapportoDiLavoro.Categoria.Descrizione = reader["descrizioneCategoria"].ToString();
                    }
                }
            }
            return rapportoDiLavoro;
        }

        public int SalvaDatiComunicazione(Guid? guid, string codiceComunicazione, string codiceComunicazionePrec, int idDichiarazione)
        {
            int rtn = 0;

            DbConnection connection = null;

            DbTransaction transaction = null;

            try
            {
                connection = DatabaseCemi.CreateConnection();

                connection.Open();

                transaction = connection.BeginTransaction();

                try
                {
                    bool inserimentoReportEffettuato = true;

                    object obj;
                    using (
                        DbCommand dbCommand =
                            DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDatiAggiuntiviSintesiInsert")
                        )
                    {
                        DatabaseCemi.AddInParameter(dbCommand, "@idComunicazione", DbType.String, codiceComunicazione);
                        DatabaseCemi.AddInParameter(dbCommand, "@idComunicazionePrecedente", DbType.String,
                                                    codiceComunicazionePrec);
                        DatabaseCemi.AddInParameter(dbCommand, "@idDichiarazione", DbType.Int32, idDichiarazione);
                        if (guid.HasValue)
                        {
                            DatabaseCemi.AddInParameter(dbCommand, "@guid", DbType.Guid, guid.Value);
                        }

                        obj = DatabaseCemi.ExecuteScalar(dbCommand, transaction);
                    }

                    if (obj != null)
                        rtn = (int) obj;
                    else rtn = -1;

                    if (rtn == -1)
                    {
                        inserimentoReportEffettuato = false;
                    }


                    if (inserimentoReportEffettuato)
                        transaction.Commit();
                    else
                        transaction.Rollback();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                if (connection != null)
                    connection.Close();
            }

            return rtn;
        }

        public Int32? GetIdDichiarazioneByIdComunicazoneSintesi(String idComunicazone)
        {
            Int32? id = null;

            using (
              DbCommand comando =
                  DatabaseCemi.GetStoredProcCommand(
                      "dbo.[USP_IscrizioneLavoratoriDichiarazioniSelectIdByIdComunicazioneSintesi]"))
            {

                DatabaseCemi.AddInParameter(comando, "@idComunicazione", DbType.String, idComunicazone);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indexId = reader.GetOrdinal("idDichiarazione");
                    if (reader.Read() && !reader.IsDBNull(indexId))
                    {
                        id = reader.GetInt32(indexId);
                    }
                }
            }

            return id;
        }


        public Boolean IsImpresaCompetenzaConsulente(Int32 idConsulente, String partitaIvaImpresa)
        {
            Boolean result = false;

            using (
              DbCommand comando =
                  DatabaseCemi.GetStoredProcCommand(
                      "dbo.[USP_Imprese_ConsulentiSelectByIdConsuletePivaImpSintesi]"))
            {

                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
                DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, partitaIvaImpresa);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    //int indexId = reader.GetOrdinal("idDichiarazione");
                    if (reader.Read())
                    {
                        result = true;
                    }
                }
            }

            return result;

        }
    }
}