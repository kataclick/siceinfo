using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Exceptions;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;
using TBridge.Cemi.Type.Entities;
using Impresa = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using Indirizzo = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo;

namespace TBridge.Cemi.IscrizioneLavoratori.Data
{
    public class IscrizioneLavoratoriDataAccess
    {
        public IscrizioneLavoratoriDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public DichiarazioneCollection GetDichiarazioni(DichiarazioneFilter filtro)
        {
            DichiarazioneCollection dichiarazioni = new DichiarazioneCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniRicercaSelect"))
            {
                if (filtro.DataInvioDa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInvioDa", DbType.DateTime, filtro.DataInvioDa.Value);
                }
                if (filtro.DataInvioA.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInvioA", DbType.DateTime, filtro.DataInvioA.Value);
                }

                if (filtro.TipoAttivita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@attivita", DbType.Int32, filtro.TipoAttivita.Value);
                }

                if (! string.IsNullOrEmpty(filtro.CodiceImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceImpresa", DbType.Int32, filtro.CodiceImpresa);
                }
                if (!string.IsNullOrEmpty(filtro.PartitaIVA))
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, filtro.PartitaIVA);
                }
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (filtro.StatoGestionePratica.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, filtro.StatoGestionePratica.Value);
                }
                if (filtro.IdDichiarazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, filtro.IdDichiarazione.Value);
                }
                if (!String.IsNullOrEmpty(filtro.IdNazionalita))
                {
                    DatabaseCemi.AddInParameter(comando, "@idNazionalita", DbType.String, filtro.IdNazionalita);
                }

                DatabaseCemi.AddInParameter(comando, "@cfDiversoDatiUguali", DbType.Boolean, filtro.DuplicazioneCfDiversoDatiUguali);
                DatabaseCemi.AddInParameter(comando, "@cfUgualeDatiDiversi", DbType.Boolean, filtro.DuplicazioneCfUgualeDatiDiversi);
                DatabaseCemi.AddInParameter(comando, "@cfUgualeDatiUguali", DbType.Boolean, filtro.DuplicazioneCfUgualeDatiUguali);
                DatabaseCemi.AddInParameter(comando, "@cfUgualeRapportoApeto", DbType.Boolean, filtro.DuplicazioneCfUgualeRapportoAperto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdIscrizioneLavoratoriDichiarazione =
                        reader.GetOrdinal("idIscrizioneLavoratoriDichiarazione");
                    Int32 indiceDataInserimentoRecord = reader.GetOrdinal("dataInserimentoRecord");
                    Int32 indiceAttivita = reader.GetOrdinal("attivita");
                    Int32 indiceIdStato = reader.GetOrdinal("idStato");
                    Int32 indiceIdUtente = reader.GetOrdinal("idUtente");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIVA");
                    Int32 indiceCodiceFiscaleImpresa = reader.GetOrdinal("codiceFiscaleImpresa");
                    Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("Provincia");
                    Int32 indiceCap = reader.GetOrdinal("CAP");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIdLavoratoreCe = reader.GetOrdinal("idLavoratoreCE");
                    Int32 indiceCognomeCe = reader.GetOrdinal("cognomeCE");
                    Int32 indiceNomeCe = reader.GetOrdinal("nomeCE");
                    Int32 indiceDataNascitaCe = reader.GetOrdinal("dataNascitaCE");
                    Int32 indiceCodiceFiscaleCe = reader.GetOrdinal("codiceFiscaleCE");
                    Int32 indiceIdDichiarazionePrecedente = reader.GetOrdinal("idDichiarazionePrecedente");
                    Int32 indiceIdDichiarazioneSuccessiva = reader.GetOrdinal("idDichiarazioneSuccessiva");
                    Int32 indiceIdComunicazionePrecedente = reader.GetOrdinal("idComunicazionePrecedente");
                    Int32 indiceIdComunicazioneSuccessiva = reader.GetOrdinal("idComunicazioneSuccessiva");

                    #endregion

                    while (reader.Read())
                    {
                        Dichiarazione dichiarazione = new Dichiarazione();
                        dichiarazioni.Add(dichiarazione);

                        dichiarazione.IdDichiarazione = reader.GetInt32(indiceIdIscrizioneLavoratoriDichiarazione);
                        //if (!reader.IsDBNull(indiceAttivita))
                        //{
                        //    dichiarazione.IdDichiarazione = reader.GetInt16(indiceIdAttivita);
                        //}
                        if (!reader.IsDBNull(indiceIdUtente))
                        {
                            dichiarazione.IdUtente = reader.GetInt32(indiceIdUtente);
                        }
                        if (!reader.IsDBNull(indiceDataInserimentoRecord))
                        {
                            dichiarazione.DataInvio = reader.GetDateTime(indiceDataInserimentoRecord);
                        }
                        if (!reader.IsDBNull(indiceAttivita))
                        {
                            dichiarazione.Attivita = (TipoAttivita) reader.GetInt16(indiceAttivita);
                        }
                        if (!reader.IsDBNull(indiceIdStato))
                        {
                            dichiarazione.Stato = (TipoStatoGestionePratica) reader.GetInt32(indiceIdStato);
                        }
                        dichiarazione.Impresa = new Impresa();
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            dichiarazione.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            dichiarazione.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        }
                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            dichiarazione.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            dichiarazione.Impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            dichiarazione.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceIdCantiere))
                        {
                            dichiarazione.IdCantiere = reader.GetInt32(indiceIdCantiere);
                        }
                        dichiarazione.Cantiere = new Indirizzo();
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            dichiarazione.Cantiere.Indirizzo1 = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceCivico))
                        {
                            dichiarazione.Cantiere.Civico = reader.GetString(indiceCivico);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            dichiarazione.Cantiere.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            dichiarazione.Cantiere.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            dichiarazione.Cantiere.Cap = reader.GetString(indiceCap);
                        }
                        dichiarazione.Lavoratore = new Lavoratore();
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIdLavoratoreCe))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratoreCe);
                        }
                        if (!reader.IsDBNull(indiceCognomeCe))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognomeCe);
                        }
                        if (!reader.IsDBNull(indiceNomeCe))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNomeCe);
                        }
                        if (!reader.IsDBNull(indiceDataNascitaCe))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascitaCe);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleCe))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscaleCe);
                        }
                        if (!reader.IsDBNull(indiceIdDichiarazionePrecedente))
                        {
                            dichiarazione.IdDichiarazionePrecedente = reader.GetInt32(indiceIdDichiarazionePrecedente);
                        }
                        if (!reader.IsDBNull(indiceIdDichiarazioneSuccessiva))
                        {
                            dichiarazione.IdDichiarazioneSuccessiva = reader.GetInt32(indiceIdDichiarazioneSuccessiva);
                        }
                        if (!reader.IsDBNull(indiceIdComunicazionePrecedente))
                        {
                            dichiarazione.IdComunicazionePrecedente = reader.GetString(indiceIdComunicazionePrecedente);
                        }
                        if (!reader.IsDBNull(indiceIdComunicazioneSuccessiva))
                        {
                            dichiarazione.IdComunicazioneSuccessiva = reader.GetString(indiceIdComunicazioneSuccessiva);
                        }
                    }
                }
            }


            return dichiarazioni;
        }

        public LavoratoreCollection GetLavoratori(LavoratoreFilter filtro, Int32 idImpresa)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriRicercaSelect"))
            {
                if (filtro.Codice.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.Codice.Value);
                }
                if (!String.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!String.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (!String.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public Lavoratore GetLavoratore(Int32 idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriSelect")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIban = reader.GetOrdinal("iban");
                    Int32 indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indiceIndirizzoDataInivioValidita = reader.GetOrdinal("dataInizioValiditaIndirizzo");

                    #endregion

                    if (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!Convert.IsDBNull(reader["sesso"]))
                        {
                            lavoratore.Sesso = ((String) reader["sesso"])[0];
                        }
                        else
                        {
                            lavoratore.Sesso = 'M';
                        }
                        if (!Convert.IsDBNull(reader["provinciaNascita"]))
                        {
                            lavoratore.ProvinciaNascita = (String) reader["provinciaNascita"];
                        }
                        if (!Convert.IsDBNull(reader["luogoNascita"]))
                        {
                            lavoratore.ComuneNascita = (String) reader["luogoNascita"];
                        }
                        if (!Convert.IsDBNull(reader["idStatoCivile"]))
                        {
                            lavoratore.IdStatoCivile = (String) reader["idStatoCivile"];
                        }
                        if (!Convert.IsDBNull(reader["idNazionalita"]))
                        {
                            lavoratore.NazioneNascita = (String) reader["idNazionalita"];
                        }

                        lavoratore.Indirizzo = new Indirizzo();
                        if (!Convert.IsDBNull(reader["indirizzoDenominazione"]))
                        {
                            lavoratore.Indirizzo.Indirizzo1 = (String) reader["indirizzoDenominazione"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoProvincia"]))
                        {
                            lavoratore.Indirizzo.Provincia = (String) reader["indirizzoProvincia"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoComuneCodiceCatastale"]))
                        {
                            lavoratore.Indirizzo.Comune = (String) reader["indirizzoComuneCodiceCatastale"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoCap"]))
                        {
                            lavoratore.Indirizzo.Cap = (String) reader["indirizzoCap"];
                        }
                        if (!Convert.IsDBNull(reader["civicoIndirizzo"]))
                        {
                            lavoratore.Indirizzo.Civico = (String) reader["civicoIndirizzo"];
                        }

                        lavoratore.Indirizzo.Comune = reader.IsDBNull(indiceIndirizzoComune) ? null : reader.GetString(indiceIndirizzoComune);
                        lavoratore.Indirizzo.ComuneDescrizione = lavoratore.Indirizzo.Comune;
                        lavoratore.Indirizzo.DataInizioValiditÓ = reader.IsDBNull(indiceIndirizzoDataInivioValidita) ? (DateTime?)null : reader.GetDateTime(indiceIndirizzoDataInivioValidita);

                        if (!Convert.IsDBNull(reader["telefono"]))
                        {
                            lavoratore.NumeroTelefono = (String) reader["telefono"];
                        }
                        if (!Convert.IsDBNull(reader["cellulare"]))
                        {
                            lavoratore.NumeroTelefonoCellulare = (String) reader["cellulare"];
                        }
                        if (!Convert.IsDBNull(reader["email"]))
                        {
                            lavoratore.Email = (String) reader["email"];
                        }
                        if (!reader.IsDBNull(indiceIban))
                        {
                            lavoratore.IBAN = reader.GetString(indiceIban);
                        }

                        
                    }
                }
            }

            return lavoratore;
        }

        public RapportoDiLavoro GetRapportoDiLavoro(Int32 idLavoratore, Int32 idImpresa)
        {
            RapportoDiLavoro rapporto = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriRapportoImpresaPersonaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        rapporto = new RapportoDiLavoro();

                        rapporto.IdImpresa = (Int32) reader["idImpresa"];
                        rapporto.IdLavoratore = (Int32) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["dataInizioValiditaRapporto"]))
                        {
                            rapporto.DataInizioValiditaRapporto = (DateTime) reader["dataInizioValiditaRapporto"];
                        }
                        rapporto.DataFineValiditaRapporto = (DateTime) reader["dataFineValiditaRapporto"];
                        if (!Convert.IsDBNull(reader["dataAssunzione"]))
                        {
                            rapporto.DataAssunzione = (DateTime) reader["dataAssunzione"];
                        }
                        if (!Convert.IsDBNull(reader["dataLicenziamento"]))
                        {
                            rapporto.DataLicenziamento = (DateTime) reader["dataLicenziamento"];
                        }
                        if (!Convert.IsDBNull(reader["idContratto"]))
                        {
                            rapporto.Contratto = new TipoContratto();
                            rapporto.Contratto.IdContratto = (String) reader["idContratto"];
                            rapporto.Contratto.Descrizione = (String) reader["descrizioneContratto"];
                        }
                        if (!Convert.IsDBNull(reader["idCategoria"]))
                        {
                            rapporto.Categoria = new TipoCategoria();
                            rapporto.Categoria.IdCategoria = (String) reader["idCategoria"];
                            rapporto.Categoria.Descrizione = (String) reader["descrizioneCategoria"];
                        }
                        if (!Convert.IsDBNull(reader["idQualifica"]))
                        {
                            rapporto.Qualifica = new TipoQualifica();
                            rapporto.Qualifica.IdQualifica = (String) reader["idQualifica"];
                            rapporto.Qualifica.Descrizione = (String) reader["descrizioneQualifica"];
                        }
                        if (!Convert.IsDBNull(reader["idMansione"]))
                        {
                            rapporto.Mansione = new TipoMansione();
                            rapporto.Mansione.IdMansione = (String) reader["idMansione"];
                            rapporto.Mansione.Descrizione = (String) reader["descrizioneMansione"];
                        }
                        if (!Convert.IsDBNull(reader["tipoInizioRapporto"]))
                        {
                            rapporto.TipoInizioRapporto = new TipoInizioRapporto();
                            rapporto.TipoInizioRapporto.IdTipoInizioRapporto = (String) reader["tipoInizioRapporto"];
                        }
                    }
                }
            }

            return rapporto;
        }

        public Impresa GetImpresa(String codiceFiscalePartitaIva)
        {
            Impresa impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_ImpreseSelectByPartitaIva]"))
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, codiceFiscalePartitaIva);
                DatabaseCemi.AddInParameter(comando, "@filtraCessate", DbType.Byte, 1);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region index
                    Int32 indexStato = reader.GetOrdinal("stato");
                    Int32 indexCodiceContratto = reader.GetOrdinal("codiceContratto");
                    Int32 indexDescrizioneContratto = reader.GetOrdinal("descrizioneContratto");
                    #endregion
                    if (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.IdImpresa = (Int32)reader["idImpresa"];
                        impresa.RagioneSociale = (String)reader["ragioneSociale"];

                        if (!Convert.IsDBNull(reader["partitaIva"]))
                        {
                            impresa.PartitaIva = (String)reader["partitaIva"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        {
                            impresa.CodiceFiscale = (String)reader["codiceFiscale"];
                        }
                        //if (!Convert.IsDBNull(reader["codiceContratto"]))
                        //{
                        //    impresa.CodiceContratto = (String)reader["codiceContratto"];
                        //}
                        if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                        {
                            impresa.SedeLegaleIndirizzo = (String)reader["indirizzoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                        {
                            impresa.SedeLegaleComune = (String)reader["localitaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                        {
                            impresa.SedeLegaleProvincia = (String)reader["provinciaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeLegale"]))
                        {
                            impresa.SedeLegaleCap = (String)reader["capSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeLegale"]))
                        {
                            impresa.SedeLegaleTelefono = (String)reader["telefonoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeLegale"]))
                        {
                            impresa.SedeLegaleFax = (String)reader["faxSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeLegale"]))
                        {
                            impresa.SedeLegaleEmail = (String)reader["emailSedeLegale"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaIndirizzo = (String)reader["indirizzoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaComune = (String)reader["localitaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaProvincia = (String)reader["provinciaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaCap = (String)reader["capSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaTelefono = (String)reader["telefonoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaFax = (String)reader["faxSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaEmail = (String)reader["emailSedeAmministrazione"];
                        }

                        impresa.Contratto = new TipoContratto
                                                    {
                                                        IdContratto = reader.IsDBNull(indexCodiceContratto) ? null : reader.GetString(indexCodiceContratto),
                                                        Descrizione = reader.IsDBNull(indexDescrizioneContratto) ? null : reader.GetString(indexDescrizioneContratto),
                                                    };

                        impresa.Stato = reader.IsDBNull(indexStato) ? null : reader.GetString(indexStato);
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresa(Int32 idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region index
                    Int32 indexStato = reader.GetOrdinal("stato");
                    Int32 indexCodiceContratto = reader.GetOrdinal("codiceContratto");
                    Int32 indexDescrizioneContratto = reader.GetOrdinal("descrizioneContratto");
                    #endregion

                    if (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.IdImpresa = (Int32) reader["idImpresa"];
                        impresa.RagioneSociale = (String) reader["ragioneSociale"];

                        if (!Convert.IsDBNull(reader["partitaIva"]))
                        {
                            impresa.PartitaIva = (String) reader["partitaIva"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        {
                            impresa.CodiceFiscale = (String) reader["codiceFiscale"];
                        }
                        //if (!Convert.IsDBNull(reader["codiceContratto"]))
                        //{
                        //    impresa.CodiceContratto = (String) reader["codiceContratto"];
                        //}
                        if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                        {
                            impresa.SedeLegaleIndirizzo = (String) reader["indirizzoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                        {
                            impresa.SedeLegaleComune = (String) reader["localitaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                        {
                            impresa.SedeLegaleProvincia = (String) reader["provinciaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeLegale"]))
                        {
                            impresa.SedeLegaleCap = (String) reader["capSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeLegale"]))
                        {
                            impresa.SedeLegaleTelefono = (String) reader["telefonoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeLegale"]))
                        {
                            impresa.SedeLegaleFax = (String) reader["faxSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeLegale"]))
                        {
                            impresa.SedeLegaleEmail = (String) reader["emailSedeLegale"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaIndirizzo = (String) reader["indirizzoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaComune = (String) reader["localitaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaProvincia = (String) reader["provinciaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaCap = (String) reader["capSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaTelefono = (String) reader["telefonoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaFax = (String) reader["faxSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaEmail = (String) reader["emailSedeAmministrazione"];
                        }

                        impresa.Contratto = new TipoContratto
                        {
                            IdContratto = reader.IsDBNull(indexCodiceContratto) ? null : reader.GetString(indexCodiceContratto),
                            Descrizione = reader.IsDBNull(indexDescrizioneContratto) ? null : reader.GetString(indexDescrizioneContratto),
                        };

                        impresa.Stato = reader.IsDBNull(indexStato) ? null : reader.GetString(indexStato);
                    }
                }
            }

            return impresa;
        }

        public RapportoDiLavoroCollection GetRapportiDiLavoroAperti(Int32 idLavoratore,
                                                                    DateTime dataInizioRapportoDiLavoro)
        {
            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriRapportiDiLavoroApertiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoLavoro", DbType.DateTime,
                                            dataInizioRapportoDiLavoro);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceTipoRapportoLavoro = reader.GetOrdinal("tipoRapporto");
                    Int32 indiceStato = reader.GetOrdinal("stato");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIvaImpresa = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscaleImpresa = reader.GetOrdinal("impCodiceFiscale");
                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizioValiditaRapporto");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFineValiditaRapporto");
                    Int32 indiceImpresaDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");

                    #endregion

                    while (reader.Read())
                    {
                        RapportoDiLavoro rapportoDiLavoro = new RapportoDiLavoro();
                        rapportiAperti.Add(rapportoDiLavoro);

                        rapportoDiLavoro.TipoRapportoLavoro =
                            (TipologiaRapportoLavoro) reader.GetInt32(indiceTipoRapportoLavoro);
                        if (!reader.IsDBNull(indiceStato))
                        {
                            rapportoDiLavoro.StatoPratica = (TipoStatoGestionePratica) reader.GetInt32(indiceStato);
                        }
                        rapportoDiLavoro.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.RagioneSocialeImpresa = reader.GetString(indiceRagioneSocialeImpresa);
                        rapportoDiLavoro.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                        rapportoDiLavoro.Lavoratore = new Lavoratore();
                        rapportoDiLavoro.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            rapportoDiLavoro.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            rapportoDiLavoro.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            rapportoDiLavoro.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            rapportoDiLavoro.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }

                        rapportoDiLavoro.Impresa = new Impresa();
                        rapportoDiLavoro.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.Impresa.RagioneSociale = reader.GetString(indiceRagioneSocialeImpresa);
                        if (!reader.IsDBNull(indicePartitaIvaImpresa))
                        {
                            rapportoDiLavoro.Impresa.PartitaIva = reader.GetString(indicePartitaIvaImpresa);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            rapportoDiLavoro.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceImpresaDataUltimaDenuncia))
                        {
                            rapportoDiLavoro.Impresa.DataUltimaDenuncia = reader.GetDateTime(indiceImpresaDataUltimaDenuncia);
                        }

                        if (!reader.IsDBNull(indiceDataInizio))
                        {
                            rapportoDiLavoro.DataInizioValiditaRapporto = reader.GetDateTime(indiceDataInizio);
                        }
                        if (!reader.IsDBNull(indiceDataFine))
                        {
                            rapportoDiLavoro.DataFineValiditaRapporto = reader.GetDateTime(indiceDataFine);
                        }

                    }
                }
            }

            return rapportiAperti;
        }

        public RapportoDiLavoroCollection GetDichiarazioniAperte(Int32 idDichiarazioneCorrente, String codiceFiscale,
                                                                 DateTime dataInizioRapportoDiLavoro)
        {
            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniAperteSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazioneCorrente);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoLavoro", DbType.DateTime,
                                            dataInizioRapportoDiLavoro);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceTipoRapportoLavoro = reader.GetOrdinal("tipoRapporto");
                    Int32 indiceStato = reader.GetOrdinal("stato");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIvaImpresa = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscaleImpresa = reader.GetOrdinal("impCodiceFiscale");
                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizioValiditaRapporto");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFineValiditaRapporto");
                    Int32 indiceImpresaDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");

                    #endregion

                    while (reader.Read())
                    {
                        RapportoDiLavoro rapportoDiLavoro = new RapportoDiLavoro();
                        rapportiAperti.Add(rapportoDiLavoro);

                        rapportoDiLavoro.TipoRapportoLavoro =
                            (TipologiaRapportoLavoro) reader.GetInt32(indiceTipoRapportoLavoro);
                        if (!reader.IsDBNull(indiceStato))
                        {
                            rapportoDiLavoro.StatoPratica = (TipoStatoGestionePratica) reader.GetInt32(indiceStato);
                        }
                        rapportoDiLavoro.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.RagioneSocialeImpresa = reader.GetString(indiceRagioneSocialeImpresa);
                        rapportoDiLavoro.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                        rapportoDiLavoro.Lavoratore = new Lavoratore();
                        rapportoDiLavoro.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            rapportoDiLavoro.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            rapportoDiLavoro.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            rapportoDiLavoro.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            rapportoDiLavoro.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }

                        rapportoDiLavoro.Impresa = new Impresa();
                        rapportoDiLavoro.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.Impresa.RagioneSociale = reader.GetString(indiceRagioneSocialeImpresa);
                        if (!reader.IsDBNull(indicePartitaIvaImpresa))
                        {
                            rapportoDiLavoro.Impresa.PartitaIva = reader.GetString(indicePartitaIvaImpresa);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            rapportoDiLavoro.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceImpresaDataUltimaDenuncia))
                        {
                            rapportoDiLavoro.Impresa.DataUltimaDenuncia = reader.GetDateTime(indiceImpresaDataUltimaDenuncia);
                        }

                        if (!reader.IsDBNull(indiceDataInizio))
                        {
                            rapportoDiLavoro.DataInizioValiditaRapporto = reader.GetDateTime(indiceDataInizio);
                        }
                        if (!reader.IsDBNull(indiceDataFine))
                        {
                            rapportoDiLavoro.DataFineValiditaRapporto = reader.GetDateTime(indiceDataFine);
                        }
                    }
                }
            }

            return rapportiAperti;
        }

        public Dichiarazione GetDichiarazione(int idDichiarazione)
        {
            Dichiarazione dichiarazione = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniSelectByID"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region index
                    int indexDataUltimoCambioStato = reader.GetOrdinal("dataUltimoCambioStato");
                    int indexImpresaDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");
                    int indexImpresaStato = reader.GetOrdinal("stato");
                    int indexIdUtenteUltimoCambioStato = reader.GetOrdinal("idUtenteUltimoCambioStato");
                    int indexIdComunicazionePrecedente = reader.GetOrdinal("idComunicazionePrecedente");
                    int indexIdComunicazioneSuccessiva = reader.GetOrdinal("idComunicazioneSuccessiva");
                    #endregion

                    if (reader.Read())
                    {
                        #region dati generali dichiarazione

                        dichiarazione = new Dichiarazione();

                        dichiarazione.IdDichiarazione = (Int32) reader["idDichiarazione"];

                        if (!Convert.IsDBNull(reader["dataInvio"]))
                        {
                            dichiarazione.DataInvio = (DateTime) reader["dataInvio"];
                        }

                        if (!Convert.IsDBNull(reader["attivita"]))
                        {
                            dichiarazione.Attivita = (TipoAttivita) (Int16) reader["attivita"];
                        }

                        if (!Convert.IsDBNull(reader["idUtente"]))
                        {
                            dichiarazione.IdUtente = (Int32)reader["idUtente"];
                        }

                        if (!Convert.IsDBNull(reader["idStato"]))
                        {
                            dichiarazione.Stato = (TipoStatoGestionePratica) (Int32) reader["idStato"];
                        }

                        dichiarazione.NuovoLavoratore = (Boolean) reader["nuovoLavoratore"];

                        if (!Convert.IsDBNull(reader["idLavoratoreSelezionato"]))
                        {
                            dichiarazione.IdLavoratoreSelezionato = (Int32) reader["idLavoratoreSelezionato"];
                        }

                        if (!Convert.IsDBNull(reader["mantieniIbanAnagrafica"]))
                        {
                            dichiarazione.MantieniIbanAnagrafica = (Boolean) reader["mantieniIbanAnagrafica"];
                        }

                        if (!Convert.IsDBNull(reader["mantieniIndirizzoAnagrafica"]))
                        {
                            dichiarazione.MantieniIndirizzoAnagrafica = (Boolean) reader["mantieniIndirizzoAnagrafica"];
                        }
                        dichiarazione.ControlloSdoppione = (Boolean) reader["controlloSdoppione"];

                        dichiarazione.RapportoAssociatoCessazione = (Boolean) reader["rapportoAssociatoCessazione"];

                        if (!Convert.IsDBNull(reader["idImpresaRapportoSelezionatoCessazione"]))
                        {
                            dichiarazione.IdImpresaRapportoSelezionatoCessazione =
                                (Int32) reader["idImpresaRapportoSelezionatoCessazione"];
                        }

                        if (!Convert.IsDBNull(reader["idLavoratoreRapportoSelezionatoCessazione"]))
                        {
                            dichiarazione.IdLavoratoreRapportoSelezionatoCessazione =
                                (Int32) reader["idLavoratoreRapportoSelezionatoCessazione"];
                        }

                        if (!Convert.IsDBNull(reader["dataFineRapportoSelezionatoCessazione"]))
                        {
                            dichiarazione.DataFineRapportoSelezionatoCessazione =
                                (DateTime) reader["dataFineRapportoSelezionatoCessazione"];
                        }

                        if (!Convert.IsDBNull(reader["idDichiarazionePrecedente"]))
                        {
                            dichiarazione.IdDichiarazionePrecedente = (Int32)reader["idDichiarazionePrecedente"];
                        }

                        if (!Convert.IsDBNull(reader["idDichiarazioneSuccessiva"]))
                        {
                            dichiarazione.IdDichiarazioneSuccessiva = (Int32)reader["idDichiarazioneSuccessiva"];
                        }

                        if (!Convert.IsDBNull(reader["cellulareSMSSiceInfo"]))
                        {
                            dichiarazione.CellulareSMSSiceInfo = (String)reader["cellulareSMSSiceInfo"];
                        }

                        dichiarazione.IdComunicazioneSuccessiva = reader.IsDBNull(indexIdComunicazioneSuccessiva) ? null : reader.GetString(indexIdComunicazioneSuccessiva);
                        dichiarazione.IdComunicazionePrecedente = reader.IsDBNull(indexIdComunicazionePrecedente) ? null : reader.GetString(indexIdComunicazionePrecedente);
                        dichiarazione.DataUltimoCambioStato = reader.IsDBNull(indexDataUltimoCambioStato) ? (DateTime?)null : reader.GetDateTime(indexDataUltimoCambioStato);
                        dichiarazione.UltimoCambioStatoIdUtente = reader.IsDBNull(indexIdUtenteUltimoCambioStato) ? (Int32?)null : reader.GetInt32(indexIdUtenteUltimoCambioStato);
                        #endregion

                        #region impresa

                        dichiarazione.Impresa = new Impresa();

                        if (!Convert.IsDBNull(reader["idImpresa"]))
                        {
                            dichiarazione.Impresa.IdImpresa = (Int32) reader["idImpresa"];
                        }
                        if (!Convert.IsDBNull(reader["ragioneSocialeImpresa"]))
                        {
                            dichiarazione.Impresa.RagioneSociale = (String) reader["ragioneSocialeImpresa"];
                        }
                        if (!Convert.IsDBNull(reader["partitaIVA"]))
                        {
                            dichiarazione.Impresa.PartitaIva = (String) reader["partitaIVA"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscaleImpresa"]))
                        {
                            dichiarazione.Impresa.CodiceFiscale = (String) reader["codiceFiscaleImpresa"];
                        }
                        if (!Convert.IsDBNull(reader["codiceContratto"]))
                        {
                            dichiarazione.Impresa.Contratto = new TipoContratto();
                            dichiarazione.Impresa.Contratto.IdContratto = (String) reader["codiceContratto"];
                            dichiarazione.Impresa.Contratto.Descrizione = (String) reader["contrattoImpresa"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleIndirizzo = (String) reader["indirizzoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleCap = (String) reader["capSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleComune = (String) reader["localitaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleProvincia = (String) reader["provinciaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleTelefono = (String) reader["telefonoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleFax = (String) reader["faxSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleEmail = (String) reader["emailSedeLegale"];
                        }

                        dichiarazione.Impresa.Stato = reader.IsDBNull(indexImpresaStato) ? null : reader.GetString(indexImpresaStato);
                        dichiarazione.Impresa.DataUltimaDenuncia = reader.IsDBNull(indexImpresaDataUltimaDenuncia) ? (DateTime?)null : reader.GetDateTime(indexImpresaDataUltimaDenuncia);

                        #endregion

                        #region cantiere

                        dichiarazione.Cantiere = new Indirizzo();
                        if (!Convert.IsDBNull(reader["idCantiere"]))
                        {
                            //dichiarazione.Cantiere.IdCantiere = (Int32)reader["idCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoCantiere"]))
                        {
                            dichiarazione.Cantiere.Indirizzo1 = (String) reader["indirizzoCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["comuneCantiere"]))
                        {
                            dichiarazione.Cantiere.Comune = (String) reader["comuneCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["comuneCantiereDescrizione"]))
                        {
                            dichiarazione.Cantiere.ComuneDescrizione = (String) reader["comuneCantiereDescrizione"];
                        }
                        if (!Convert.IsDBNull(reader["civicoCantiere"]))
                        {
                            dichiarazione.Cantiere.Civico = (String) reader["civicoCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaCantiere"]))
                        {
                            dichiarazione.Cantiere.Provincia = (String) reader["provinciaCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["capCantiere"]))
                        {
                            dichiarazione.Cantiere.Cap = (String) reader["capCantiere"];
                        }

                        #endregion

                        #region consulente

                        if (!Convert.IsDBNull(reader["idConsulente"]))
                        {
                            dichiarazione.Consulente = new Consulente();
                            dichiarazione.Consulente.IdConsulente = (Int32) reader["idConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["ragioneSocialeConsulente"]))
                        {
                            dichiarazione.Consulente.RagioneSociale = (String) reader["ragioneSocialeConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscaleConsulente"]))
                        {
                            dichiarazione.Consulente.CodiceFiscale = (String) reader["codiceFiscaleConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoConsulente"]))
                        {
                            //dichiarazione.Consulente.Indirizzo = (String)reader["indirizzoConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["capConsulente"]))
                        {
                            //dichiarazione.Consulente.Cap = (String)reader["capConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["localitaConsulente"]))
                        {
                            //dichiarazione.Consulente.Comune = (String)reader["localitaConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaConsulente"]))
                        {
                            //dichiarazione.Consulente.Provincia = (String)reader["provinciaConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoConsulente"]))
                        {
                            dichiarazione.Consulente.Telefono = (String) reader["telefonoConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["faxConsulente"]))
                        {
                            dichiarazione.Consulente.Fax = (String) reader["faxConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["emailConsulente"]))
                        {
                            dichiarazione.Consulente.Email = (String) reader["emailConsulente"];
                        }

                        #endregion

                        #region lavoratore

                        dichiarazione.Lavoratore = new Lavoratore();

                        //Lavoratore nuovo
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                            dichiarazione.Lavoratore.IdLavoratore = (Int32) reader["IdLavoratore"];

                            if (!Convert.IsDBNull(reader["cognomeLavoratore"]))
                            {
                                dichiarazione.Lavoratore.Cognome = (String) reader["cognomeLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["nomeLavoratore"]))
                            {
                                dichiarazione.Lavoratore.Nome = (String) reader["nomeLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["sessoLavoratore"]))
                            {
                                dichiarazione.Lavoratore.Sesso = ((String) reader["sessoLavoratore"])[0];
                            }
                            if (!Convert.IsDBNull(reader["dataNascitaLavoratore"]))
                            {
                                dichiarazione.Lavoratore.DataNascita = (DateTime) reader["dataNascitaLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["codiceFiscaleLavoratore"]))
                            {
                                dichiarazione.Lavoratore.CodiceFiscale = (String) reader["codiceFiscaleLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["comuneNascitaLavoratore"]))
                            {
                                dichiarazione.Lavoratore.ComuneNascita = (String) reader["comuneNascitaLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["provinciaNascitaLavoratore"]))
                            {
                                dichiarazione.Lavoratore.ProvinciaNascita =
                                    (String) reader["provinciaNascitaLavoratore"];
                            }
                        }

                        //Lavoratore CE
                        if (!Convert.IsDBNull(reader["idLavoratoreCE"]))
                        {
                            dichiarazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            dichiarazione.Lavoratore.IdLavoratore = (Int32) reader["IdLavoratoreCE"];

                            if (!Convert.IsDBNull(reader["cognomeLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.Cognome = (String) reader["cognomeLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["nomeLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.Nome = (String) reader["nomeLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["sessoLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.Sesso = ((String) reader["sessoLavoratoreCE"])[0];
                            }
                            if (!Convert.IsDBNull(reader["dataNascitaLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.DataNascita = (DateTime) reader["dataNascitaLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["codiceFiscaleLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.CodiceFiscale = (String) reader["codiceFiscaleLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["comuneNascitaLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.ComuneNascita = (String) reader["comuneNascitaLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["provinciaNascitaLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.ProvinciaNascita =
                                    (String) reader["provinciaNascitaLavoratoreCE"];
                            }
                        }

                        //Dati aggiuntivi
                        if (!Convert.IsDBNull(reader["italianoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Italiano = (Boolean) reader["italianoLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["statoCivileLavoratore"]))
                        {
                            dichiarazione.Lavoratore.IdStatoCivile = (String) reader["statoCivileLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["cittadinanzaLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Cittadinanza = (String) reader["cittadinanzaLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["livelloIstruzioneLavoratore"]))
                        {
                            dichiarazione.Lavoratore.LivelloIstruzione = (String) reader["livelloIstruzioneLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["linguaComunicazioniLavoratore"]))
                        {
                            dichiarazione.Lavoratore.LinguaComunicazione =
                                (String) reader["linguaComunicazioniLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["IBANLavoratore"]))
                        {
                            dichiarazione.Lavoratore.IBAN = (String) reader["IBANLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["cognomeCointestatarioLavoratore"]))
                        {
                            dichiarazione.Lavoratore.CognomeCointestatario =
                                (String) reader["cognomeCointestatarioLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["richiestaInfoCartaPrepagata"]))
                        {
                            dichiarazione.Lavoratore.RichiestaInfoCartaPrepagata =
                                (Boolean) reader["richiestaInfoCartaPrepagata"];
                        }


                        if (!Convert.IsDBNull(reader["idTipoCartaPrepagata"]))
                        {
                            dichiarazione.Lavoratore.TipoPrepagata = new TipoCartaPrepagata();
                            dichiarazione.Lavoratore.TipoPrepagata.IdTipoCartaPrepagata =
                                (String) reader["idTipoCartaPrepagata"];
                            dichiarazione.Lavoratore.TipoPrepagata.Descrizione =
                                (String) reader["tipoCartaPrepagataLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["tagliaFelpaHuskyLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TagliaFelpaHusky = (String) reader["tagliaFelpaHuskyLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["tagliaPantaloniLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TagliaPantaloni = (String) reader["tagliaPantaloniLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["tagliaScarpeLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TagliaScarpe = (String) reader["tagliaScarpeLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["richiestaIscrizione16OreLavoratore"]))
                        {
                            dichiarazione.Lavoratore.RichiestaIscrizioneCorsi16Ore =
                                (Boolean) reader["richiestaIscrizione16OreLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["descrizioneDocumento"]))
                        {
                            dichiarazione.Lavoratore.TipoDocumento = (String) reader["descrizioneDocumento"];
                        }

                        if (!Convert.IsDBNull(reader["numeroDocumento"]))
                        {
                            dichiarazione.Lavoratore.NumeroDocumento = (String) reader["numeroDocumento"];
                        }

                        if (!Convert.IsDBNull(reader["descrizioneMotivoPermesso"]))
                        {
                            dichiarazione.Lavoratore.MotivoPermesso = (String) reader["descrizioneMotivoPermesso"];
                        }

                        if (!Convert.IsDBNull(reader["dataRichiestaPermesso"]))
                        {
                            dichiarazione.Lavoratore.DataRichiestaPermesso = (DateTime) reader["dataRichiestaPermesso"];
                        }

                        if (!Convert.IsDBNull(reader["dataScadenzaPermesso"]))
                        {
                            dichiarazione.Lavoratore.DataScadenzaPermesso = (DateTime) reader["dataScadenzaPermesso"];
                        }

                        dichiarazione.Lavoratore.Indirizzo = new Indirizzo();

                        if (!Convert.IsDBNull(reader["indirizzoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Indirizzo1 = (String) reader["indirizzoLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["civicoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Civico = (String) reader["civicoLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["comuneLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Comune = (String) reader["comuneLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["comuneLavoratoreDescrizione"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.ComuneDescrizione =
                                (String) reader["comuneLavoratoreDescrizione"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Provincia = (String) reader["provinciaLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["capLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Cap = (String) reader["capLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.NumeroTelefono = (String) reader["telefonoLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["cellulareLavoratore"]))
                        {
                            dichiarazione.Lavoratore.NumeroTelefonoCellulare = (String) reader["cellulareLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["eMailLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Email = (String) reader["eMailLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["aderisceServizioSMS"]))
                        {
                            dichiarazione.Lavoratore.AderisceServizioSMS = (Boolean) reader["aderisceServizioSMS"];
                        }

                        if (!Convert.IsDBNull(reader["noteLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Note = (String) reader["noteLavoratore"];
                        }

                        #endregion

                        #region rapporto di lavoro

                        if (!Convert.IsDBNull(reader["idRapportoDiLavoro"]))
                        {
                            dichiarazione.RapportoDiLavoro = new RapportoDiLavoro();

                            if (!Convert.IsDBNull(reader["dataAssunzione"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataAssunzione = (DateTime) reader["dataAssunzione"];
                            }

                            if (!Convert.IsDBNull(reader["dataInizioRapportoDiLavoro"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto =
                                    (DateTime) reader["dataInizioRapportoDiLavoro"];
                            }

                            if (!Convert.IsDBNull(reader["dataFineRapportoDiLavoro"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataFineValiditaRapporto =
                                    (DateTime)reader["dataFineRapportoDiLavoro"];
                            }

                            dichiarazione.RapportoDiLavoro.Contratto = new TipoContratto();
                            if (!Convert.IsDBNull(reader["idContrattoApplicatoLavoratore"]))
                            {
                                dichiarazione.RapportoDiLavoro.Contratto.IdContratto =
                                    (String) reader["idContrattoApplicatoLavoratore"];
                            }

                            if (!Convert.IsDBNull(reader["descrizioneContrattoApplicato"]))
                            {
                                dichiarazione.RapportoDiLavoro.Contratto.Descrizione =
                                    (String) reader["descrizioneContrattoApplicato"];
                            }

                            dichiarazione.RapportoDiLavoro.TipoInizioRapporto = new TipoInizioRapporto();
                            if (!Convert.IsDBNull(reader["tipologiaContrattualeAssunzione"]))
                            {
                                dichiarazione.RapportoDiLavoro.TipoInizioRapporto.Descrizione =
                                    (String) reader["tipologiaContrattualeAssunzione"];
                            }

                            if (!Convert.IsDBNull(reader["partTime"]))
                            {
                                dichiarazione.RapportoDiLavoro.PartTime = (Boolean) reader["partTime"];
                            }

                            if (!Convert.IsDBNull(reader["percentualePartTime"]))
                            {
                                dichiarazione.RapportoDiLavoro.PercentualePartTime =
                                    (Decimal) reader["percentualePartTime"];
                            }

                            if (!Convert.IsDBNull(reader["livelloInquadramento"]))
                            {
                                dichiarazione.RapportoDiLavoro.LivelloInquadramento =
                                    (String) reader["livelloInquadramento"];
                            }

                            if (!Convert.IsDBNull(reader["idCategoria"]))
                            {
                                dichiarazione.RapportoDiLavoro.Categoria = new TipoCategoria();
                                dichiarazione.RapportoDiLavoro.Categoria.IdCategoria = (String) reader["idCategoria"];
                                dichiarazione.RapportoDiLavoro.Categoria.Descrizione = (String) reader["categoria"];
                            }

                            if (!Convert.IsDBNull(reader["idQualifica"]))
                            {
                                dichiarazione.RapportoDiLavoro.Qualifica = new TipoQualifica();
                                dichiarazione.RapportoDiLavoro.Qualifica.IdQualifica = (String) reader["idQualifica"];
                                dichiarazione.RapportoDiLavoro.Qualifica.Descrizione = (String) reader["qualifica"];
                            }

                            if (!Convert.IsDBNull(reader["idMansione"]))
                            {
                                dichiarazione.RapportoDiLavoro.Mansione = new TipoMansione();
                                dichiarazione.RapportoDiLavoro.Mansione.IdMansione = (String) reader["idMansione"];
                                dichiarazione.RapportoDiLavoro.Mansione.Descrizione = (String) reader["mansione"];
                            }

                            if (!Convert.IsDBNull(reader["dataCessazione"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataCessazione = (DateTime) reader["dataCessazione"];
                            }

                            dichiarazione.RapportoDiLavoro.TipoFineRapporto = new TipoFineRapporto();
                            if (!Convert.IsDBNull(reader["causaCessazione"]))
                            {
                                dichiarazione.RapportoDiLavoro.TipoFineRapporto.Descrizione =
                                    (String) reader["causaCessazione"];
                            }
                        }

                        #endregion

                        if (!Convert.IsDBNull(reader["posizioneValida"]))
                        {
                            dichiarazione.Lavoratore.PosizioneValida = (Boolean) reader["posizioneValida"];
                        }
                    }
                }

                return dichiarazione;
            }
        }

        public LavoratoreCollection GetLavoratoriConStessoCodiceFiscale(String codiceFiscale)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriLavoratoriRicercaPerCodiceFiscaleSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceSesso = reader.GetOrdinal("sesso");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLuogoNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagrafici(
            String cognome,
            String nome,
            Char sesso,
            DateTime dataNascita,
            String luogoNascita)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriLavoratoriRicercaPerDatiAnagraficiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, sesso.ToString());
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
                DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String, luogoNascita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceSesso = reader.GetOrdinal("sesso");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLuogoNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
            String cognome,
            String nome,
            Char sesso,
            DateTime dataNascita,
            String luogoNascita,
            String codiceFiscale)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriLavoratoriRicercaPerDatiAnagraficiECodiceFiscaleSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, sesso.ToString());
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
                DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String, luogoNascita);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceSesso = reader.GetOrdinal("sesso");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIban = reader.GetOrdinal("iban");
                    Int32 indiceCellulare = reader.GetOrdinal("cellulare");
                    Int32 indiceEmail = reader.GetOrdinal("email");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzoDenominazione");
                    Int32 indiceProvincia = reader.GetOrdinal("indirizzoProvincia");
                    Int32 indiceComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indiceCap = reader.GetOrdinal("indirizzoCAP");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLuogoNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIban))
                        {
                            lavoratore.IBAN = reader.GetString(indiceIban);
                        }
                        if (!reader.IsDBNull(indiceCellulare))
                        {
                            lavoratore.NumeroTelefonoCellulare = reader.GetString(indiceCellulare);
                        }
                        if (!reader.IsDBNull(indiceEmail))
                        {
                            lavoratore.Email = reader.GetString(indiceEmail);
                        }
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            lavoratore.Indirizzo = new Indirizzo();

                            lavoratore.Indirizzo.Indirizzo1 = reader.GetString(indiceIndirizzo);

                            if (!reader.IsDBNull(indiceProvincia))
                            {
                                lavoratore.Indirizzo.Provincia = reader.GetString(indiceProvincia);
                            }
                            if (!reader.IsDBNull(indiceComune))
                            {
                                lavoratore.Indirizzo.Comune = reader.GetString(indiceComune);
                            }
                            if (!reader.IsDBNull(indiceCap))
                            {
                                lavoratore.Indirizzo.Cap = reader.GetString(indiceCap);
                            }
                        }
                    }
                }
            }

            return lavoratori;
        }

        public void CambiaStatoDichiarazione(Int32 idDichiarazione, TipoStatoGestionePratica vecchioStato,
                                             TipoStatoGestionePratica nuovoStato, Int32 idUtenteCambioStato)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniCambioStateUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                DatabaseCemi.AddInParameter(comando, "@vecchioStato", DbType.Int32, vecchioStato);
                DatabaseCemi.AddInParameter(comando, "@nuovoStato", DbType.Int32, nuovoStato);
                DatabaseCemi.AddInParameter(comando, "@idUtenteCambioStato", DbType.Int32, idUtenteCambioStato);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new StatoGiaVariatoException();
                }
            }
        }

        public void CambiaLavoratoreSelezionato(Int32 idDichiarazione, Int32? idLavoratore, Boolean nuovoLavoratore)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniSelezioneLavoratoreUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (idLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@nuovoLavoratore", DbType.Boolean, nuovoLavoratore);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("CambiaLavoratoreSelezionato: cambio selezione lavoratore non riuscito");
                }
            }
        }

        public void ControlloSdoppioneEffettuato(int idDichiarazione, Boolean controlloEffettuato)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniControlloSdoppioneUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                DatabaseCemi.AddInParameter(comando, "@controlloEffettuato", DbType.Boolean, controlloEffettuato);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("ControlloSdoppioneEffettuato: aggiornamento controllo sdoppione non riuscito");
                }
            }
        }

        public void CambiaIBANDichiarato(Int32 idDichiarazione, Boolean? mantieni)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniCambioIBANUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (mantieni.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mantieni", DbType.Boolean, mantieni);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio selezione IBAN non effettuato");
                }
            }
        }

        public void CambiaIndirizzoDichiarato(Int32 idDichiarazione, Boolean? mantieni)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniCambioIndirizzoUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (mantieni.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mantieni", DbType.Boolean, mantieni);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio indirizzo dichiarato non effettuato");
                }
            }
        }

        public void CambiaSelezioneRapportoCessazione(Int32 idDichiarazione, Boolean? mantieni, Int32? idLavoratore,
                                                      Int32? idImpresa, DateTime? dataFineValiditaRapporto)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniCambioSelezioneRapportoCessazione"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (mantieni.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mantieni", DbType.Boolean, mantieni);
                if (idLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                if (idImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                if (dataFineValiditaRapporto.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataFineValiditaRapporto", DbType.DateTime,
                                                dataFineValiditaRapporto);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio selezione rapporto da cessare non effettuato");
                }
            }
        }

        public void CambiaPosizioneValida(Int32 idDichiarazione, bool? posizioneValida)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniCambioPosizioneValidaUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (posizioneValida.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@posizioneValida", DbType.Boolean, posizioneValida);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio posizione valida non effettuato");
                }
            }
        }

        #region inserimento

        private void InsertLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Common commonBiz = new Common();
            if (commonBiz.InsertLavoratore(lavoratore, transaction))
            {
                InsertLavoratoreDatiAggiuntivi(lavoratore, transaction);
            }
            else
            {
                throw new Exception("InsertLavoratore: errore durante l'inserimento nell'anagrafica comune");
            }
        }

        private Boolean InsertLavoratoreDatiAggiuntivi(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore.IdLavoratore.HasValue)
            {
                using (DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriDatiAggiuntiviInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);

                    if (!String.IsNullOrEmpty(lavoratore.IdStatoCivile))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idStatoCivile", DbType.String, lavoratore.IdStatoCivile);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.Cittadinanza))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, lavoratore.Cittadinanza);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.LivelloIstruzione))
                    {
                        DatabaseCemi.AddInParameter(comando, "@livelloIstruzione", DbType.String,
                                                    lavoratore.LivelloIstruzione);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.IdLingua))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idLingua", DbType.String, lavoratore.IdLingua);
                    }

                    if (lavoratore.DataDecesso.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime, lavoratore.DataDecesso);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.IdTipoDecesso))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoDecesso", DbType.String, lavoratore.IdTipoDecesso);
                    }

                    //if (lavoratore.Italiano)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@italiano", DbType.Boolean, lavoratore.Italiano);
                    //}

                    if (lavoratore.Indirizzo != null)
                    {
                        if (!String.IsNullOrEmpty(lavoratore.Indirizzo.Indirizzo1))
                        {
                            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String,
                                                        lavoratore.Indirizzo.Indirizzo1);
                        }
                        if (!String.IsNullOrEmpty(lavoratore.Indirizzo.Civico))
                        {
                            DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, lavoratore.Indirizzo.Civico);
                        }
                        if (!String.IsNullOrEmpty(lavoratore.Indirizzo.Comune))
                        {
                            DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, lavoratore.Indirizzo.Comune);
                        }
                        if (!String.IsNullOrEmpty(lavoratore.Indirizzo.Provincia))
                        {
                            DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String,
                                                        lavoratore.Indirizzo.Provincia);
                        }
                        if (!String.IsNullOrEmpty(lavoratore.Indirizzo.Cap))
                        {
                            DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, lavoratore.Indirizzo.Cap);
                        }
                        //if (!String.IsNullOrEmpty(lavoratore.Indirizzo.InfoAggiuntiva))
                        //{
                        //    //DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String, lavoratoreInfoAggiuntiva);
                        //}
                    }

                    if (!String.IsNullOrEmpty(lavoratore.NumeroTelefono))
                    {
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, lavoratore.NumeroTelefono);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.NumeroTelefonoCellulare))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String,
                                                    lavoratore.NumeroTelefonoCellulare);
                    }

                    //if (lavoratore.AderisceServizioSMS)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@aderisceServizioSMS", DbType.Boolean,
                                                lavoratore.AderisceServizioSMS);
                    //}

                    if (!String.IsNullOrEmpty(lavoratore.Email))
                    {
                        DatabaseCemi.AddInParameter(comando, "@email", DbType.String, lavoratore.Email);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.IBAN))
                    {
                        DatabaseCemi.AddInParameter(comando, "@IBAN", DbType.String, lavoratore.IBAN);
                    }

                    //if (lavoratore.CoIntestato)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@coIntestato", DbType.Boolean, lavoratore.CoIntestato);
                    //}

                    if (!String.IsNullOrEmpty(lavoratore.CognomeCointestatario))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cognomeCointestatario", DbType.String,
                                                    lavoratore.CognomeCointestatario);
                    }

                    if (lavoratore.IdTagliaFelpaHusky.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTagliaFelpaHusky", DbType.Int32,
                                                    lavoratore.IdTagliaFelpaHusky.Value);
                    }

                    if (lavoratore.IdTagliaPantaloni.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTagliaPantaloni", DbType.Int32,
                                                    lavoratore.IdTagliaPantaloni.Value);
                    }

                    if (lavoratore.IdTagliaScarpe.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTagliaScarpe", DbType.Int32,
                                                    lavoratore.IdTagliaScarpe.Value);
                    }

                    //if (lavoratore.RichiestaIscrizioneCorsi16Ore)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@richiestaIscrizione16Ore", DbType.Boolean,
                                                lavoratore.RichiestaIscrizioneCorsi16Ore);
                    //}

                    if (!String.IsNullOrEmpty(lavoratore.Note))
                    {
                        DatabaseCemi.AddInParameter(comando, "@note", DbType.String, lavoratore.Note);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.IdSindacato))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idSindacato", DbType.String, lavoratore.IdSindacato);
                    }

                    if (lavoratore.DataAdesione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataAdesione", DbType.DateTime, lavoratore.DataAdesione);
                    }

                    if (lavoratore.DataDisdetta.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataDisdetta", DbType.DateTime, lavoratore.DataDisdetta);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.NumeroDelega))
                    {
                        DatabaseCemi.AddInParameter(comando, "@numeroDelega", DbType.String, lavoratore.NumeroDelega);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.TipoDocumento))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.String, lavoratore.TipoDocumento);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.NumeroDocumento))
                    {
                        DatabaseCemi.AddInParameter(comando, "@numeroDocumento", DbType.String,
                                                    lavoratore.NumeroDocumento);
                    }

                    if (!String.IsNullOrEmpty(lavoratore.MotivoPermesso))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idMotivoPermesso", DbType.String,
                                                    lavoratore.MotivoPermesso);
                    }

                    if (lavoratore.DataRichiestaPermesso.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataRichiestaPermesso", DbType.DateTime,
                                                    lavoratore.DataRichiestaPermesso);
                    }

                    if (lavoratore.DataScadenzaPermesso.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataScadenzaPermesso", DbType.DateTime,
                                                    lavoratore.DataRichiestaPermesso);
                    }


                    //if (lavoratore.RapportoDiLavoro != null)
                    //{
                    //    if (lavoratore.RapportoDiLavoro.DataAssunzione.HasValue)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoDiLavoro", DbType.DateTime,
                    //                                    lavoratore.RapportoDiLavoro.DataInizioValiditaRapporto.Value);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Contratto.IdContratto))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idTipologiaContrattuale", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Contratto.IdContratto);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Categoria.IdCategoria))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idCategoria", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Categoria.IdCategoria);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Qualifica.IdQualifica))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idQualifica", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Qualifica.IdQualifica);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Mansione.IdMansione))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idMansione", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Mansione.IdMansione);
                    //    }

                    //    if (lavoratore.RapportoDiLavoro.DataCessazione.HasValue)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime,
                    //                                    lavoratore.RapportoDiLavoro.DataFineValiditaRapporto);
                    //    }

                    //    if (lavoratore.RapportoDiLavoro.TipoFineRapporto != null)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idCausaCessazione", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.TipoFineRapporto.IdTipoFineRapporto);
                    //    }

                    //    if (lavoratore.RapportoDiLavoro.DataTrasformazione.HasValue)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@dataTrasformazione", DbType.DateTime,
                    //                                    lavoratore.RapportoDiLavoro.DataTrasformazione.Value);
                    //    }


                    //    //TODO VALLA: rivedere specifiche
                    //    //if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.))
                    //    //{
                    //    //    DatabaseCemi.AddInParameter(comando, "@idCodiceTrasformazione", DbType.String, lavoratore.IdCodiceTrasformazione);
                    //    //}

                    //}

                    if (lavoratore.RichiestaInfoCartaPrepagata != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@richiestaInfoCartaPrepagata", DbType.Boolean,
                                                    lavoratore.RichiestaInfoCartaPrepagata);
                    }

                    if (lavoratore.TipoPrepagata != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoCartaPrepagata", DbType.String,
                                                    lavoratore.TipoPrepagata.IdTipoCartaPrepagata);
                    }

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }


        private Int32? InsertCantiere(Indirizzo cantiere, DbTransaction transaction)
        {
            Int32? idCantiere = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriCantieriInsert"))
            {
                if (cantiere != null)
                {
                    if (!String.IsNullOrEmpty(cantiere.Indirizzo1))
                    {
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String,
                                                    cantiere.Indirizzo1);
                    }
                    if (!String.IsNullOrEmpty(cantiere.Civico))
                    {
                        DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, cantiere.Civico);
                    }
                    if (!String.IsNullOrEmpty(cantiere.Comune))
                    {
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, cantiere.Comune);
                    }
                    if (!String.IsNullOrEmpty(cantiere.Provincia))
                    {
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, cantiere.Provincia);
                    }
                    if (!String.IsNullOrEmpty(cantiere.Cap))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cantiere.Cap);
                    }
                    if (cantiere.Latitudine.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal, cantiere.Latitudine);
                    }
                    if (cantiere.Longitudine.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal, cantiere.Longitudine);
                    }
                    //if (!String.IsNullOrEmpty(cantiere.InfoAggiuntiva))
                    //{
                    //    //DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String, cantiere.InfoAggiuntiva);
                    //}

                    DatabaseCemi.AddOutParameter(comando, "@idCantiere", DbType.Int32, 4);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    idCantiere = (Int32) DatabaseCemi.GetParameterValue(comando, "@idCantiere");
                }
            }

            return idCantiere;
        }

        private Int32? InsertRapportoDiLavoro(RapportoDiLavoro rapportoDiLavoro, DbTransaction transaction)
        {
            Int32? idRapportoDiLavoro = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriRapportiDiLavoroInsert"))
            {
                if (rapportoDiLavoro != null)
                {
                    if (rapportoDiLavoro.DataAssunzione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                                                    rapportoDiLavoro.DataAssunzione.Value);
                    }

                    if (rapportoDiLavoro.DataInizioValiditaRapporto.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoDiLavoro", DbType.DateTime,
                                                    rapportoDiLavoro.DataInizioValiditaRapporto.Value);
                    }

                    if (rapportoDiLavoro.DataFineValiditaRapporto.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataFineRapportoDiLavoro", DbType.DateTime,
                                                    rapportoDiLavoro.DataFineValiditaRapporto.Value);
                    }

                    if (rapportoDiLavoro.Contratto != null)
                    {
                        if (!String.IsNullOrEmpty(rapportoDiLavoro.Contratto.IdContratto))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idTipologiaContrattuale", DbType.String,
                                                        rapportoDiLavoro.Contratto.IdContratto);
                        }
                    }

                    if (rapportoDiLavoro.Categoria != null)
                    {
                        if (!String.IsNullOrEmpty(rapportoDiLavoro.Categoria.IdCategoria))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCategoria", DbType.String,
                                                        rapportoDiLavoro.Categoria.IdCategoria);
                        }
                    }

                    if (rapportoDiLavoro.Qualifica != null)
                    {
                        if (!String.IsNullOrEmpty(rapportoDiLavoro.Qualifica.IdQualifica))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idQualifica", DbType.String,
                                                        rapportoDiLavoro.Qualifica.IdQualifica);
                        }
                    }

                    if (rapportoDiLavoro.Mansione != null)
                    {
                        if (!String.IsNullOrEmpty(rapportoDiLavoro.Mansione.IdMansione))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idMansione", DbType.String,
                                                        rapportoDiLavoro.Mansione.IdMansione);
                        }
                    }

                    if (rapportoDiLavoro.DataCessazione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime,
                                                    rapportoDiLavoro.DataCessazione);
                    }

                    if (rapportoDiLavoro.TipoFineRapporto != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idCausaCessazione", DbType.String,
                                                    rapportoDiLavoro.TipoFineRapporto.IdTipoFineRapporto);
                    }

                    if (rapportoDiLavoro.DataTrasformazione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataTrasformazione", DbType.DateTime,
                                                    rapportoDiLavoro.DataTrasformazione.Value);
                    }

                    if (rapportoDiLavoro.TipoInizioRapporto != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoInizioRapporto", DbType.String,
                                                    rapportoDiLavoro.TipoInizioRapporto.IdTipoInizioRapporto);
                    }

                    DatabaseCemi.AddInParameter(comando, "@partTime", DbType.Boolean,
                                                rapportoDiLavoro.PartTime);

                    if (rapportoDiLavoro.PercentualePartTime.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@percentualePartTime", DbType.Decimal,
                                                    rapportoDiLavoro.PercentualePartTime);
                    }

                    if (!String.IsNullOrEmpty(rapportoDiLavoro.LivelloInquadramento))
                    {
                        DatabaseCemi.AddInParameter(comando, "@livelloInquadramento", DbType.String,
                                                    rapportoDiLavoro.LivelloInquadramento);
                    }

                    if (rapportoDiLavoro.ContrattoOriginaleSintesi !=null && !String.IsNullOrEmpty(rapportoDiLavoro.ContrattoOriginaleSintesi.IdContratto))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipologiaContrattualeOriginaleSintesi", DbType.String,
                                                    rapportoDiLavoro.ContrattoOriginaleSintesi.IdContratto);
                    }

                    //TODO VALLA: rivedere specifiche
                    //if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.))
                    //{
                    //    DatabaseCemi.AddInParameter(comando, "@idCodiceTrasformazione", DbType.String, lavoratore.IdCodiceTrasformazione);
                    //}


                    DatabaseCemi.AddOutParameter(comando, "@idRapportoDiLavoro", DbType.Int32, 4);
                }


                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    idRapportoDiLavoro = (Int32) DatabaseCemi.GetParameterValue(comando, "@idRapportoDiLavoro");
                }
            }
            return idRapportoDiLavoro;
        }


        public Boolean InsertDichiarazione(Dichiarazione dichiarazione)
        {
            Boolean res = false;
            Int32? idCantiere = null;
            Int32? idRapportoDiLavoro = null;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (!dichiarazione.Lavoratore.IdLavoratore.HasValue)
                        {
                            InsertLavoratore(dichiarazione.Lavoratore, transaction);
                        }

                        if (dichiarazione.Cantiere != null)
                        {
                            idCantiere = InsertCantiere(dichiarazione.Cantiere, transaction);
                        }

                        if (dichiarazione.RapportoDiLavoro != null)
                        {
                            idRapportoDiLavoro = InsertRapportoDiLavoro(dichiarazione.RapportoDiLavoro, transaction);
                        }

                        using (
                            DbCommand comando =
                                DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, dichiarazione.IdImpresa);
                            DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, dichiarazione.IdUtente);

                            if (dichiarazione.IdConsulente.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32,
                                                            dichiarazione.IdConsulente);
                            }

                            if (dichiarazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                            dichiarazione.Lavoratore.IdLavoratore);
                            }
                            else
                            {
                                if (dichiarazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idLavoratoreIscrizioneLavoratori",
                                                                DbType.Int32,
                                                                dichiarazione.Lavoratore.IdLavoratore);
                                }
                            }

                            if (idCantiere.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32,
                                                            idCantiere);
                            }

                            if (idRapportoDiLavoro.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idRapportoDiLavoro", DbType.Int32,
                                                            idRapportoDiLavoro);
                            }

                            DatabaseCemi.AddInParameter(comando, "@attivita", DbType.Int32, dichiarazione.Attivita);
                            DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, (Int32)dichiarazione.Stato);
                            
                            DatabaseCemi.AddOutParameter(comando, "@idDichiarazione", DbType.Int32, 32);

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                res = true;
                                dichiarazione.IdDichiarazione = (int?) DatabaseCemi.GetParameterValue(comando, "@idDichiarazione");
                            }
                        }

                        if (res)
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            throw new Exception(
                                "InsertDichiarazione: l'inserimento di una dichiarazione non Ŕ andato a buon fine");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        res = false;
                        throw;
                    }
                }
            }

            return res;
        }

        public DichiarazioneCollection GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(Int32 idImpresa,
                                                                                                string codiceFiscale,
                                                                                                DateTime?
                                                                                                    dataInizioValiditaRapporto,
                                                                                                DateTime? dataCessazione)
        {
            DichiarazioneCollection dichiarazioni = new DichiarazioneCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniSelectByImpresaCodiceFiscaleDataInizioDataCessazione")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@codiceImpresa", DbType.Int32, idImpresa);

                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);


                if (dataInizioValiditaRapporto.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInizioRapporto", DbType.DateTime,
                                                dataInizioValiditaRapporto.Value);
                }

                if (dataCessazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime, dataCessazione.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdIscrizioneLavoratoriDichiarazione =
                        reader.GetOrdinal("idIscrizioneLavoratoriDichiarazione");
                    Int32 indiceDataInserimentoRecord = reader.GetOrdinal("dataInserimentoRecord");
                    Int32 indiceAttivita = reader.GetOrdinal("attivita");
                    Int32 indiceIdStato = reader.GetOrdinal("idStato");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIVA = reader.GetOrdinal("partitaIVA");
                    Int32 indiceCodiceFiscaleImpresa = reader.GetOrdinal("codiceFiscaleImpresa");
                    Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("Provincia");
                    Int32 indiceCAP = reader.GetOrdinal("CAP");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIdLavoratoreCE = reader.GetOrdinal("idLavoratoreCE");
                    Int32 indiceCognomeCE = reader.GetOrdinal("cognomeCE");
                    Int32 indiceNomeCE = reader.GetOrdinal("nomeCE");
                    Int32 indiceDataNascitaCE = reader.GetOrdinal("dataNascitaCE");
                    Int32 indiceCodiceFiscaleCE = reader.GetOrdinal("codiceFiscaleCE");
                    Int32 indiceIdUtente = reader.GetOrdinal("idUtente");

                    #endregion

                    while (reader.Read())
                    {
                        Dichiarazione dichiarazione = new Dichiarazione();
                        dichiarazioni.Add(dichiarazione);

                        dichiarazione.IdDichiarazione = reader.GetInt32(indiceIdIscrizioneLavoratoriDichiarazione);
                        if (!reader.IsDBNull(indiceDataInserimentoRecord))
                        {
                            dichiarazione.DataInvio = reader.GetDateTime(indiceDataInserimentoRecord);
                        }
                        if (!reader.IsDBNull(indiceAttivita))
                        {
                            dichiarazione.Attivita = (TipoAttivita) reader.GetInt16(indiceAttivita);
                        }
                        if (!reader.IsDBNull(indiceIdStato))
                        {
                            dichiarazione.Stato = (TipoStatoGestionePratica) reader.GetInt32(indiceIdStato);
                        }
                        if (!reader.IsDBNull(indiceIdUtente))
                        {
                            dichiarazione.IdUtente = reader.GetInt32(indiceIdUtente);
                        }
                        dichiarazione.Impresa = new Impresa();
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            dichiarazione.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            dichiarazione.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        }
                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            dichiarazione.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        if (!reader.IsDBNull(indicePartitaIVA))
                        {
                            dichiarazione.Impresa.PartitaIva = reader.GetString(indicePartitaIVA);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            dichiarazione.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceIdCantiere))
                        {
                            dichiarazione.IdCantiere = reader.GetInt32(indiceIdCantiere);
                        }
                        dichiarazione.Cantiere = new Indirizzo();
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            dichiarazione.Cantiere.Indirizzo1 = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceCivico))
                        {
                            dichiarazione.Cantiere.Civico = reader.GetString(indiceCivico);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            dichiarazione.Cantiere.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            dichiarazione.Cantiere.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCAP))
                        {
                            dichiarazione.Cantiere.Cap = reader.GetString(indiceCAP);
                        }
                        dichiarazione.Lavoratore = new Lavoratore();
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIdLavoratoreCE))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratoreCE);
                        }
                        if (!reader.IsDBNull(indiceCognomeCE))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognomeCE);
                        }
                        if (!reader.IsDBNull(indiceNomeCE))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNomeCE);
                        }
                        if (!reader.IsDBNull(indiceDataNascitaCE))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascitaCE);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleCE))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscaleCE);
                        }
                    }
                }
            }


            return dichiarazioni;
        }

      
        #endregion
    }
}
