﻿using System;
using TBridge.Cemi.Sintesi.Data;
using TBridge.Cemi.Type.Collections.Sintesi;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using TipoContratto = TBridge.Cemi.Type.Entities.TipoContratto;


namespace TBridge.Cemi.Sintesi.Business
{
    public class SintesiBusiness
    {
        private readonly SintesiDataAccess _sintesiDataAccess = new SintesiDataAccess();

        public StatusStranieroCollection GetStatusStraniero()
        {
            return _sintesiDataAccess.GetStatusStraniero();
        }

        public MotivoPermessoCollection GetMotiviPermesso()
        {
            return _sintesiDataAccess.GetMotiviPermesso();
        }

        public TitoloDiStudioCollection GetTitoliDiStudio()
        {
            return _sintesiDataAccess.GetTitoliDiStudio();
        }

        public TipoComunicazioneCollection GetTipiComunicazione()
        {
            return _sintesiDataAccess.GetTipiComunicazione();
        }

        //NON USATO
        public TipoContrattoCollection GetTipiContratto()
        {
            return _sintesiDataAccess.GetTipiContratto();
        }

        public CessazioneRLCollection GetCessazioniRL()
        {
            return _sintesiDataAccess.GetCessazioniRL();
        }

        public TrasformazioneRLCollection GetTrasformazioniRL()
        {
            return _sintesiDataAccess.GetTrasformazioniRL();
        }

        public CCNLCollection GetCCNL()
        {
            return _sintesiDataAccess.GetCCNL();
        }

        public TipoOrarioCollection GetTipiOrario()
        {
            return _sintesiDataAccess.GetTipiOrario();
        }

        public TipoContratto GetTipoContratto(String idTipoContratto)
        {
            return _sintesiDataAccess.GetTipoContratto(idTipoContratto);
        }

        public TitoloDiStudio GetTitoloDiStudio(String codice)
        {
            return _sintesiDataAccess.GetTitoloDiStudio(codice);
        }

        public NazionalitaSiceNew GetNazionalita(String idNazione)
        {
            return _sintesiDataAccess.GetNazionalita(idNazione);
        }

        public TipoInizioRapporto GetTipoInizioRapporto(string idTipoInizioRapporto)
        {
            // TODO potrebbe essere necessaria modifica per Aggiormaneto Contr. Nazionale 10/1/2014
            return _sintesiDataAccess.GetTipoInizioRapporto(idTipoInizioRapporto);
        }

        public TipoQualifica GetTipoQualifica(string idQualifica)
        {
            return _sintesiDataAccess.GetTipoQualifica(idQualifica);
        }

        public TBridge.Cemi.Type.Collections.TipoQualificaCollection GetTipiQualifica(String tipoCategoria)
        {
            return _sintesiDataAccess.GetTipiQualifica(tipoCategoria);
        }

        public TBridge.Cemi.Type.Collections.TipoCategoriaCollection GetTipiCategoria()
        {
            return _sintesiDataAccess.GetTipiCategoria();
        }

        public TBridge.Cemi.Type.Collections.TipoCategoriaCollection GetTipiCategoria(String tipoContratto)
        {
            return _sintesiDataAccess.GetTipiCategoria(tipoContratto);
        }
    }
}