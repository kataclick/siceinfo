namespace TBridge.Cemi.IscrizioneCE.Type.Enums
{
    public enum StatoDomanda
    {
        Accettata = 0,
        Rifiutata,
        DaValutare,
        SospesaAttesaIntegrazione,
        SospesaDebiti
    }
}