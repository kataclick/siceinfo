﻿namespace TBridge.Cemi.IscrizioneCE.Type.Enums
{
    public enum TipoOrdinamento
    {
        DataRichiestaIscrizioneCrescente = 0,
        DataRichiestaIscrizioneDecrescente,
        DataInserimentoCrescente,
        DataInserimentoDecrescente,
        RagioneSocialeCrescente,
        RagioneSocialeDecrescente
    }
}