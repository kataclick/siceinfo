using System;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.IscrizioneCE.Type.Enums;
using Impresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using Lavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;

namespace TBridge.Cemi.IscrizioneCE.Type.Entities
{
    public class ModuloIscrizione
    {
        private bool compilazioneConsulente;
        private int? idConsulente;
        private int? idUtente;
        private Impresa impresa;
        private string ragioneSocialeConsulente;
        private string utente;

        public ModuloIscrizione()
        {
            LegaleRappresentante = new Lavoratore();
            Cantiere = new Cantiere();
            SedeLegale = new Sede();
            SedeAmministrativa = new Sede();
            Impresa = new Impresa();
        }

        public string IndirizzoConsulente { get; set; }

        public string ProvinciaConsulente { get; set; }

        public string CodiceCatastaleConsulente { get; set; }

        public string CapConsulente { get; set; }

        public string TelefonoConsulente { get; set; }

        public string FaxConsulente { get; set; }

        public string EmailConsulente { get; set; }

        public string PecConsulente { get; set; }

        public string CodiceFiscaleConsulente { get; set; }

        public string ComuneConsulente { get; set; }

        public int? CodiceConsulente { get; set; }

        public string RagSocConsulente { get; set; }

        public string IBAN { get; set; }

        public string IntestatarioConto { get; set; }

        public TipoModulo TipoModulo { get; set; }

        public string IdTipoInvioDenuncia { get; set; }

        public string IdTipoIscrizione { get; set; }

        public string DescrizioneTipoInvioDenuncia { get; set; }

        public string DescrizioneTipoIscrizione { get; set; }

        public string UtenteCompilazione
        {
            get
            {
                if (compilazioneConsulente && idConsulente.HasValue)
                    return ragioneSocialeConsulente;
                else if (idUtente.HasValue)
                    return utente;
                else
                    return "Impresa";
            }
        }

        public string RagioneSocialeConsulente
        {
            get { return ragioneSocialeConsulente; }
            set { ragioneSocialeConsulente = value; }
        }

        public bool CompilazioneConsulente
        {
            get { return compilazioneConsulente; }
            set { compilazioneConsulente = value; }
        }

        public int? IdUtente
        {
            get { return idUtente; }
            set { idUtente = value; }
        }

        public string Utente
        {
            get { return utente; }
            set { utente = value; }
        }

        public string PosizioneOrganizzazioneImprenditoriale { get; set; }

        public int? IdConsulente
        {
            get { return idConsulente; }
            set { idConsulente = value; }
        }

        public Consulente Consulente { get; set; }

        public string CodiceOrganizzazioneImprenditoriale { get; set; }

        public string DescrizioneOrganizzazioneImprenditoriale { get; set; }

        public string NumeroIscrizioneCameraCommercio { get; set; }

        public string NumeroIscrizioneImpreseArtigiane { get; set; }

        public DateTime? DataIscrizioneCommercioArtigiane { get; set; }

        public string CodiceTipoImpresa { get; set; }

        public string DescrizioneTipoImpresa { get; set; }

        public string CodiceNaturaGiuridica { get; set; }

        public string DescrizioneNaturaGiuridica { get; set; }

        public string CodiceAttivitaISTAT { get; set; }

        public string DescrizioneAttivitaISTAT { get; set; }

        public int? IdDomanda { get; set; }

        public string RagioneSociale
        {
            get
            {
                if (impresa != null)
                    return impresa.RagioneSociale;
                else
                    return string.Empty;
            }
        }

        public string PartitaIva
        {
            get
            {
                if (impresa != null)
                    return impresa.PartitaIVA;
                else
                    return string.Empty;
            }
        }

        public string CodiceFiscale
        {
            get
            {
                if (impresa != null)
                    return impresa.CodiceFiscale;
                else
                    return string.Empty;
            }
        }

        public string IdImpresa
        {
            get
            {
                if (impresa != null && impresa.IdImpresa > 0)
                    return impresa.IdImpresa.ToString();
                else
                    return string.Empty;
            }
        }

        public bool Confermata { get; set; }

        public StatoDomanda? Stato { get; set; }

        public Sede SedeLegale { get; set; }

        public Sede SedeAmministrativa { get; set; }

        public Impresa Impresa
        {
            get { return impresa; }
            set { impresa = value; }
        }

        public Lavoratore LegaleRappresentante { get; set; }

        //public Banca Banca
        //{
        //    get { return banca; }
        //    set { banca = value; }
        //}

        public Cantiere Cantiere { get; set; }

        public int? NumeroOpeImp { get; set; }

        public int? NumeroOpe { get; set; }

        public DateTime? DataRichiestaIscrizione { get; set; }

        public string CorrispondenzaPresso { get; set; }

        public bool GeocodificaOkSedeLegale { get; set; }

        public bool GeocodificaOkSedeAmministrativa { get; set; }

        public bool GeocodificaOkCantiere { get; set; }

        public bool CantiereInNotifiche { get; set; }

        public DateTime DataPresentazione { get; set; }

        public String IdCassaEdile { get; set; }

        public Guid guid { get; set; }

        public String PEC { get; set; }
    }
}