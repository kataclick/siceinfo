namespace TBridge.Cemi.IscrizioneCE.Type.Entities
{
    public class Sede
    {
        public string PreIndirizzo { get; set; }

        public int? IdIndirizzo { get; set; }

        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        public string Frazione { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string Telefono { get; set; }

        public string Fax { get; set; }

        public string EMail { get; set; }
    }
}