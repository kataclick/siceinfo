using System;

namespace TBridge.Cemi.IscrizioneCE.Type.Entities
{
    [Serializable]
    public class Banca
    {
        private string abi;
        private string agenzia;
        private string banca;
        private string cab;
        private string cap;
        private string cc;
        private string cin;
        private string cinPaese;
        private string civico;
        private string comune;
        private string comuneDescrizione;
        private string fax;
        private string frazione;
        private int? idBanca;
        private string indirizzo;
        private string intestatario;
        private string paese;
        private string preIndirizzo;
        private string preIndirizzoDescrizione;
        private string provincia;
        private string telefono;

        public int? IdBanca
        {
            get { return idBanca; }
            set { idBanca = value; }
        }

        public string Cin
        {
            get { return cin; }
            set { cin = value; }
        }

        public string Abi
        {
            get { return abi; }
            set { abi = value; }
        }

        public string Cab
        {
            get { return cab; }
            set { cab = value; }
        }

        public string Cc
        {
            get { return cc; }
            set { cc = value; }
        }

        public string Intestatario
        {
            get { return intestatario; }
            set { intestatario = value; }
        }

        public string Banca1
        {
            get { return banca; }
            set { banca = value; }
        }

        public string Agenzia
        {
            get { return agenzia; }
            set { agenzia = value; }
        }

        public string PreIndirizzo
        {
            get { return preIndirizzo; }
            set { preIndirizzo = value; }
        }

        public string PreIndirizzoDescrizione
        {
            get { return preIndirizzoDescrizione; }
            set { preIndirizzoDescrizione = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Civico
        {
            get { return civico; }
            set { civico = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string ComuneDescrizione
        {
            get { return comuneDescrizione; }
            set { comuneDescrizione = value; }
        }

        public string Frazione
        {
            get { return frazione; }
            set { frazione = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string IndirizzoCompleto
        {
            get { return String.Format("{0} {1} {2}", PreIndirizzoDescrizione, Indirizzo, Civico); }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public string CinPaese
        {
            get { return cinPaese; }
            set { cinPaese = value; }
        }

        public string Paese
        {
            get { return paese; }
            set { paese = value; }
        }
    }
}