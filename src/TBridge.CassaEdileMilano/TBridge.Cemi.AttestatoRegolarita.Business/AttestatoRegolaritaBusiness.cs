﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Data;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.AttestatoRegolarita.Type.Filters;
using TBridge.Cemi.Business;

namespace TBridge.Cemi.AttestatoRegolarita.Business
{
    public class AttestatoRegolaritaBusiness
    {
        private readonly Common _commonBiz = new Common();
        private readonly AttestatoRegolaritaDataAccess _dataAccess = new AttestatoRegolaritaDataAccess();

        public LavoratoreCollection GetLavoratoriOrdinati( /*TipologiaLavoratore tipoLavoratore,*/
            int? idLavoratore, string cognome, string nome, DateTime? dataNascita, string codiceFiscale,
            DateTime? dataAssunzione, DateTime? dataCessazione, string sortExpression,
            string direct, int? idImpresa)
        {
            LavoratoreCollection listaLavoratori = null;

            try
            {
                listaLavoratori =
                    _dataAccess.GetLavoratoriOrdinatiReader( /*tipoLavoratore,*/
                        idLavoratore, cognome, nome, dataNascita, codiceFiscale, dataAssunzione, dataCessazione,
                        sortExpression, direct, idImpresa);
            }
            catch
            {
            }

            return listaLavoratori;
        }

        /// <summary>
        ///   Restituisce la lista delle imprese presenti nel database ordinati per il criterio passato
        /// </summary>
        /// <returns></returns>
        public ImpresaCollection GetimpreseOrdinate(int? idImpresaParam,
                                                    string ragioneSocialeParam, string comuneParam,
                                                    string indirizzoParam, string ivaFiscale, string stringExpression,
                                                    string sortDirection)
        {
            ImpresaCollection listaImprese = null;

            try
            {
                listaImprese =
                    _dataAccess.GetImprese(idImpresaParam, ragioneSocialeParam, comuneParam, indirizzoParam, ivaFiscale,
                                          stringExpression, sortDirection);
            }
            catch
            {
            }

            return listaImprese;
        }

        public void CaricaProvinceInDropDown(DropDownList dropDownProvince)
        {
            DataTable dtProvince = _commonBiz.GetProvince();

            dropDownProvince.Items.Clear();
            dropDownProvince.Items.Add(new ListItem(string.Empty, null));

            dropDownProvince.DataSource = dtProvince;
            dropDownProvince.DataTextField = "sigla";
            dropDownProvince.DataValueField = "idProvincia";

            dropDownProvince.DataBind();
        }

        public void CaricaComuniInDropDown(DropDownList dropDownComuni, int idProvincia)
        {
            dropDownComuni.Items.Clear();
            dropDownComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idProvincia > 0)
            {
                DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

                dropDownComuni.DataSource = dtComuni;
                dropDownComuni.DataTextField = "denominazione";
                dropDownComuni.DataValueField = "idComune";
            }

            dropDownComuni.DataBind();
        }

        public void CaricaCapInDropDown(DropDownList dropDownCap, Int64 idComune)
        {
            dropDownCap.Items.Clear();
            dropDownCap.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idComune > 0)
            {
                DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

                dropDownCap.DataSource = dtCAP;
                dropDownCap.DataTextField = "cap";
                dropDownCap.DataValueField = "cap";
            }

            dropDownCap.DataBind();
        }

        public void CambioSelezioneDropDownProvincia(DropDownList ddlProvincia, DropDownList ddlComune,
                                                     DropDownList ddlCap)
        {
            int idProvincia = -1;

            if (ddlProvincia.SelectedValue != null)
                Int32.TryParse(ddlProvincia.SelectedValue, out idProvincia);

            CaricaComuniInDropDown(ddlComune, idProvincia);

            CaricaCapInDropDown(ddlCap, -1);
        }

        public void CambioSelezioneDropDownComune(DropDownList ddlComune, DropDownList ddlCap)
        {
            long idComune = -1;

            if (ddlComune.SelectedValue != null)
                Int64.TryParse(ddlComune.SelectedValue, out idComune);

            CaricaCapInDropDown(ddlCap, idComune);
        }


        public DomandaCollection GetDomandeByFilter(DomandaFilter filtro)
        {
            return _dataAccess.GetDomandeByFilter(filtro);
        }

        public Domanda GetDomandaByKey(int idDomanda)
        {
            return _dataAccess.GetDomandaByKey(idDomanda);
        }

        public DomandaImpresa GetLavoratoriInDomandaPerImpresa(Int32 idDomanda, Int32? idImpresa,
                                                               Int32? idAttestatoImpresa)
        {
            return _dataAccess.GetLavoratoriInDomandaPerImpresa(idDomanda, idImpresa, idAttestatoImpresa);
        }

        public DomandaImpresaCollection GetLavoratoriInDomanda(Int32 idDomanda)
        {
            return _dataAccess.GetLavoratoriInDomanda(idDomanda);
        }

        public ImpresaCollection GetImpreseSelezionateInSubappalto(Int32 idDomanda)
        {
            return _dataAccess.GetImpreseSelezionateInSubappalto(idDomanda);
        }

        public bool InsertDomanda(Domanda domanda)
        {
            return _dataAccess.InsertDomanda(domanda);
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIVA, string codiceFiscale)
        {
            bool[] res = null;

            try
            {
                res = _dataAccess.EsisteIvaFiscImpresa(partitaIVA, codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

        public bool UpdateDomanda(Domanda domanda)
        {
            return _dataAccess.UpdateDomanda(domanda);
        }

        public Int32 GetMesiAvvioControlli()
        {
            return _dataAccess.GetMesiAvvioControlli();
        }

        public Boolean SiPuoModificare(DateTime meseRichiesta, Int32 mesi)
        {
            Boolean res = true;

            if (DateTime.Now >= meseRichiesta.AddMonths(mesi))
                res = false;

            return res;
        }

        public void EffettuaControlli()
        {
            _dataAccess.EffettuaControlli();
        }

        public Domanda CaricaDomandaControlli(Int32 idDomanda)
        {
            return _dataAccess.GetDomandaControlli(idDomanda);
        }

        public ImpresaCollection CaricaImpreseEdiliControlli(Int32 idDomanda)
        {
            return _dataAccess.GetImpreseEdiliControlli(idDomanda);
        }

        public ImpresaCollection CaricaImpreseNonEdiliControlli(Int32 idDomanda)
        {
            return _dataAccess.GetImpreseNonEdiliControlli(idDomanda);
        }

        public LavoratoreCollection CaricaLavoratoriControlli(Int32 idDomanda)
        {
            return _dataAccess.GetLavoratoriControlli(idDomanda);
        }

        public DomandaCollection CaricaAttestati(DomandaFilter filtro)
        {
            return _dataAccess.GetRiepilogoAttestati(filtro);
        }

        public void LogSelezioneImpresaSubappalto(int idUtente, Subappalto subappalto)
        {
            if ((subappalto.Appaltante != null && subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                ||
                (subappalto.Appaltata != null && subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew))
            {
                _dataAccess.LogSelezioneImpresaSubappalto(idUtente, subappalto);
            }
        }
    }
}