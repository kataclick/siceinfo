using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.Geocode.Business;
using TBridge.Cemi.Geocode.Type.Entities;

namespace TBridge.Cemi.AttestatoRegolarita.Business
{
    public class AttestatoRegolaritaGeocoding
    {
        public static IndirizzoCollection GeoCodeGoogleMultiplo(string address)
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            List<Indirizzo> indirizziGeocodificati = GeocodeProvider.Geocode(address);

            if (indirizziGeocodificati != null)
            {
                foreach (Indirizzo indirizzo in indirizziGeocodificati)
                {
                    indirizzi.Add(new Type.Entities.Indirizzo(indirizzo.Via, indirizzo.Civico, indirizzo.Comune,
                                                              indirizzo.Provincia, indirizzo.Cap,
                                                              indirizzo.Latitudine, indirizzo.Longitudine));
                }
            }

            return indirizzi;
        }
    }
}