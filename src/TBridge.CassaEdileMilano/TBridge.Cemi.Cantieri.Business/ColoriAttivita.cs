﻿using System;
using System.Drawing;

namespace TBridge.Cemi.Cantieri.Business
{
    public static class ColoriAttivita
    {
        public const Int32 SFONDOISPEZIONER = 255;
        public const Int32 SFONDOISPEZIONEG = 200;
        public const Int32 SFONDOISPEZIONEB = 170;

        public const Int32 SFONDOISPEZIONECONSIDERATAR = 255;
        public const Int32 SFONDOISPEZIONECONSIDERATAG = 240;
        public const Int32 SFONDOISPEZIONECONSIDERATAB = 220;

        public const Int32 SFONDOISPEZIONEFANTASMAR = 250;
        public const Int32 SFONDOISPEZIONEFANTASMAG = 250;
        public const Int32 SFONDOISPEZIONEFANTASMAB = 250;

        public const Int32 SFONDOAPPUNTAMENTOR = 183;
        public const Int32 SFONDOAPPUNTAMENTOG = 255;
        public const Int32 SFONDOAPPUNTAMENTOB = 195;

        public const Int32 SFONDOBACKOFFICER = 251;
        public const Int32 SFONDOBACKOFFICEG = 255;
        public const Int32 SFONDOBACKOFFICEB = 186;

        public const Int32 SFONDOVARIER = 209;
        public const Int32 SFONDOVARIEG = 204;
        public const Int32 SFONDOVARIEB = 255;

        public static Color SfondoIspezione
        {
            get
            {
                return Color.FromArgb(SFONDOISPEZIONER, SFONDOISPEZIONEG, SFONDOISPEZIONEB);
            }
        }

        public static Color SfondoIspezioneConsiderata
        {
            get
            {
                return Color.FromArgb(SFONDOISPEZIONECONSIDERATAR, SFONDOISPEZIONECONSIDERATAG, SFONDOISPEZIONECONSIDERATAB);
            }
        }

        public static Color SfondoIspezioneFantasma
        {
            get
            {
                return Color.FromArgb(SFONDOISPEZIONEFANTASMAR, SFONDOISPEZIONEFANTASMAG, SFONDOISPEZIONEFANTASMAB);
            }
        }

        public static Color SfondoAppuntamento
        {
            get
            {
                return Color.FromArgb(SFONDOAPPUNTAMENTOR, SFONDOAPPUNTAMENTOG, SFONDOAPPUNTAMENTOB);
            }
        }

        public static Color SfondoBackOffice
        {
            get
            {
                return Color.FromArgb(SFONDOBACKOFFICER, SFONDOBACKOFFICEG, SFONDOBACKOFFICEB);
            }
        }

        public static Color SfondoVarie
        {
            get
            {
                return Color.FromArgb(SFONDOVARIER, SFONDOVARIEG, SFONDOVARIEB);
            }
        }
    }
}
