using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Type;

namespace TBridge.Cemi.Cantieri.Business.Office
{
    public class WordManager
    {
        private readonly string pathGenerazioneDocumenti = ConfigurationManager.AppSettings["PathGenerazioneDocumenti"];
        private readonly string pathM70501 = ConfigurationManager.AppSettings["M70501"];
        private readonly string pathM70505 = ConfigurationManager.AppSettings["M70505"];
        private readonly string pathM7050502Comune = ConfigurationManager.AppSettings["M7050502Comune"];
        private readonly string pathM7050501 = ConfigurationManager.AppSettings["M7050501"];
        private readonly string pathM70506 = ConfigurationManager.AppSettings["M70506"];
        private readonly string pathM7050601 = ConfigurationManager.AppSettings["M7050601"];
        private readonly string pathM7050602 = ConfigurationManager.AppSettings["M7050602"];
        private readonly string pathM7050603 = ConfigurationManager.AppSettings["M7050603"];
        private readonly string pathM70509 = ConfigurationManager.AppSettings["M70509"];
        private readonly string pathM7050901 = ConfigurationManager.AppSettings["M7050901"];
        private readonly string pathM70510 = ConfigurationManager.AppSettings["M70510"];
        private readonly string pathM70511 = ConfigurationManager.AppSettings["M70511"];
        private readonly string pathM70511Comune = ConfigurationManager.AppSettings["M70511Comune"];
        private readonly string pathM7051101Comune = ConfigurationManager.AppSettings["M7051101Comune"];

        public string CreateWord(TipologiaLettera tipoLettera, List<WordField> ds)
        {
            string ret;

            switch (tipoLettera)
            {
                case TipologiaLettera.M70505:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70505, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050502Comune:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7050502Comune, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050601:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7050601, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050602:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7050602, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050603:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7050603, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70506:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70506, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050901:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7050901, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70509:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70509, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70510:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70510, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70511:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70511, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70511Comune:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70511Comune, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7051101Comune:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7051101Comune, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70501:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM70501, ds, pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050501:
                    ret = Cemi.Business.Office.WordManager.CreateWord(pathM7050501, ds, pathGenerazioneDocumenti);
                    break;
                default:
                    //throw new ArgumentOutOfRangeException("tipoLettera");
                    ret = string.Empty;
                    break;
            }

            return ret;
        }
    }
}