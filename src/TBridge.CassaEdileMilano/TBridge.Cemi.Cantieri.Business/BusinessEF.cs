﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Cantieri.Business
{
    public class BusinessEF
    {
        public List<CantiereSegnalazioneMotivazione> GetSegnalazioneMotivazioni()
        {
            List<CantiereSegnalazioneMotivazione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from motivazione in context.CantieriSegnalazioneMotivazioni
                            orderby motivazione.Descrizione ascending
                            select motivazione;
                ret = query.ToList();
            }

            return ret;
        }

        public List<CantiereSegnalazionePriorita> GetSegnalazioniPriorita()
        {
            List<CantiereSegnalazionePriorita> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from priorita in context.CantieriSegnalazionePriorita
                            select priorita;
                ret = query.ToList();
            }

            return ret;
        }

        public List<CantieriAssegnazioneMotivazione> GetAssegnazioneMotivazioni()
        {
            List<CantieriAssegnazioneMotivazione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from motivazione in context.CantieriAssegnazioneMotivazioni
                            orderby motivazione.Descrizione ascending
                            select motivazione;
                ret = query.ToList();
            }

            return ret;
        }

        public List<CantieriAssegnazionePriorita> GetAssegnazionePriorita()
        {
            List<CantieriAssegnazionePriorita> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from priorita in context.CantieriAssegnazionePriorita
                            select priorita;
                ret = query.ToList();
            }

            return ret;
        }

        public List<CantieriCalendarioAttivitaRifiuti> GetRifiutiCalendarioAttivita()
        {
            List<CantieriCalendarioAttivitaRifiuti> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from rifiuto in context.CantieriCalendarioAttivitaRifiuti
                            orderby rifiuto.Descrizione ascending
                            select rifiuto;
                ret = query.ToList();
            }

            return ret;
        }

        public List<CantieriGruppoIspezione> GetGruppiIspezione()
        {
            List<CantieriGruppoIspezione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from gruppo in context.CantieriGruppiIspezione.Include("Ispettori")
                            select gruppo;
                ret = query.ToList();
            }

            return ret;
        }

        public List<Ispettore> GetIspettoriGruppo(Int32 idGruppoIspezione)
        {
            List<Ispettore> res;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from gruppo in context.CantieriGruppiIspezione.Include("Ispettori")
                            where gruppo.IdCantieriGruppoIspezione == idGruppoIspezione
                            select gruppo;

                res = query.Single().Ispettori.ToList();
            }

            return res;
        }

        public List<Ispettore> GetIspettori()
        {
            List<Ispettore> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from ispettore in context.Ispettori
                            orderby ispettore.Cognome ascending, ispettore.Nome ascending
                            select ispettore;
                ret = query.ToList();
            }

            return ret;
        }

        public Ispettore GetIspettore(Int32 idIspettore)
        {
            Ispettore res = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from ispettore in context.Ispettori
                            where ispettore.IdIspettore == idIspettore
                            select ispettore;
                res = query.Single();
            }

            return res;
        }

        public void InsertGruppoIspezione(CantieriGruppoIspezione gruppo)
        {
            using (SICEEntities context = new SICEEntities())
            {
                List<Ispettore> listaIspettori = new List<Ispettore>();

                foreach (Ispettore ispettore in gruppo.Ispettori)
                {
                    var query = from isp in context.Ispettori
                                where isp.IdIspettore == ispettore.IdIspettore
                                select isp;

                    listaIspettori.Add(query.Single());
                }
                gruppo.Ispettori = listaIspettori;

                context.CantieriGruppiIspezione.AddObject(gruppo);
                context.SaveChanges();
            }
        }

        public void DeleteGruppoIspezione(Int32 idGruppo)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from gruppo in context.CantieriGruppiIspezione.Include("Ispettori")
                            where gruppo.IdCantieriGruppoIspezione == idGruppo
                            select gruppo;

                context.CantieriGruppiIspezione.DeleteObject(query.Single());
                context.SaveChanges();
            }
        }

        public CantieriSegnalazione GetSegnalazione(Int32 idSegnalazione)
        {
            CantieriSegnalazione res = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from segnalazione in context.CantieriSegnalazioni
                            where segnalazione.IdCantieriSegnalazione == idSegnalazione
                            select segnalazione;

                res = query.Single();
            }

            return res;
        }

        public void UpdateSegnalazione(Type.Entities.Segnalazione segnalazioneU)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from segnalazione in context.CantieriSegnalazioni
                            where segnalazione.IdCantieriSegnalazione == segnalazioneU.IdSegnalazione
                            select segnalazione;

                CantieriSegnalazione segnalazioneO = query.Single();
                segnalazioneO.IdCantieriSegnalazioneMotivazione = segnalazioneU.Motivazione.Id;
                segnalazioneO.IdCantieriSegnalazionePriorita = segnalazioneU.Priorita.Id;
                segnalazioneO.IdSegnalazioneSuccessiva = segnalazioneU.IdSegnalazioneSuccessiva;
                segnalazioneO.Note = segnalazioneU.Note;

                context.SaveChanges();
            }
        }

        public void DeleteSegnalazione(int idSegnalazione)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from segnalazione in context.CantieriSegnalazioni
                            where segnalazione.IdCantieriSegnalazione == idSegnalazione
                            select segnalazione;

                CantieriSegnalazione segnalazioneD = query.Single();

                if (segnalazioneD.Cantieri.Count == 1)
                {
                    foreach (Cantiere cantiere in segnalazioneD.Cantieri)
                    {
                        cantiere.CantieriSegnalazione = null;
                        break;
                    }
                }
                context.CantieriSegnalazioni.DeleteObject(segnalazioneD);


                context.SaveChanges();
            }
        }

        public void DeleteAssegnazione(int idAssegnazione)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assegnazione in context.CantieriAssegnazioni.Include("Ispettori")
                            where assegnazione.Id == idAssegnazione
                            select assegnazione;

                CantieriAssegnazione assegnazioneD = query.Single();

                if (assegnazioneD.Cantiere.Count == 1)
                {
                    foreach (Cantiere cantiere in assegnazioneD.Cantiere)
                    {
                        cantiere.CantieriAssegnazione = null;
                        break;
                    }
                }
                context.CantieriAssegnazioni.DeleteObject(assegnazioneD);

                context.SaveChanges();
            }
        }

        public void UpdateAssegnazione(Type.Entities.Assegnazione assegnazioneU)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assegnazione in context.CantieriAssegnazioni
                            where assegnazione.Id == assegnazioneU.IdAssegnazione
                            select assegnazione;

                CantieriAssegnazione assegnazioneO = query.Single();
                if (assegnazioneU.Priorita != null)
                {
                    assegnazioneO.IdCantieriAssegnazionePriorita = assegnazioneU.Priorita.Id;
                }
                if (assegnazioneU.MotivazioneRifiuto != null)
                {
                    assegnazioneO.IdCantieriAssegnazioneMotivazione = assegnazioneU.MotivazioneRifiuto.Id;
                }

                assegnazioneO.Ispettori.Clear();
                foreach (Ispettore ispettore in assegnazioneU.Ispettori)
                {
                    var queryIsp = from isp in context.Ispettori
                                   where isp.IdIspettore == ispettore.IdIspettore
                                   select isp;

                    assegnazioneO.Ispettori.Add(queryIsp.Single());
                }

                context.SaveChanges();
            }
        }

        public CantieriAssegnazione GetAssegnazione(Int32 idAssegnazione)
        {
            CantieriAssegnazione res = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from assegnazione in context.CantieriAssegnazioni.Include("Ispettori")
                            where assegnazione.Id == idAssegnazione
                            select assegnazione;

                res = query.Single();
            }

            return res;
        }

        public List<CantieriTipologiaAttivita> GetTipologieAttivita()
        {
            List<CantieriTipologiaAttivita> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from tipologiaAttivita in context.CantieriCalendarioAttivitaTipologieAttivita
                            select tipologiaAttivita;
                ret = query.ToList();
            }

            return ret;
        }

        public List<CantieriCalendarioAttivita> GetCalendarioAttivita(Int32? idIspettore, DateTime inizio, DateTime fine)
        {
            List<CantieriCalendarioAttivita> ret;

            using (SICEEntities context = new SICEEntities())
            {
                if (idIspettore.HasValue)
                {
                    var query1 =
                        from calendarioAttivita in
                            context.CantieriCalendarioAttivita.Include("TipologiaAttivita").Include("CantieriCantieri").
                            Include("Ispettore").Include("IspettoriCorrelati").Include("Rifiuto")
                        where
                            (calendarioAttivita.IdIspettore == idIspettore ||
                                (from ispettore in calendarioAttivita.IspettoriCorrelati
                                 where (ispettore.IdIspettore == idIspettore)
                                 select ispettore).Count() > 0)
                             && (calendarioAttivita.Data > inizio && calendarioAttivita.Data < fine)
                        select calendarioAttivita;

                    var query2 =
                        from calendarioAttivita in
                            context.CantieriCalendarioAttivita.Include("TipologiaAttivita").Include("CantieriCantieri").
                            Include("Ispettore").Include("IspettoriCorrelati").Include("Rifiuto")
                        where
                            (calendarioAttivita.Data > inizio && calendarioAttivita.Data < fine)
                            && (from ispettore in calendarioAttivita.CantieriCantieri.FirstOrDefault().CantieriIspezioni.FirstOrDefault().IspezioneGruppo
                                where (ispettore.IdIspettore == idIspettore)
                                select ispettore).Count() > 0
                        select calendarioAttivita;

                    var query = query1.Union(query2);

                    ret = query.ToList();
                }
                else
                {
                    var query =
                        from calendarioAttivita in
                            context.CantieriCalendarioAttivita.Include("TipologiaAttivita").Include("CantieriCantieri").
                            Include("Ispettore").Include("IspettoriCorrelati").Include("Rifiuto")
                        where
                            (calendarioAttivita.Data > inizio && calendarioAttivita.Data < fine)
                        select calendarioAttivita;

                    ret = query.ToList();
                }
            }

            return ret;
        }

        public void DeleteAttivita(Int32 idAttivita)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from attivita in context.CantieriCalendarioAttivita.Include("IspettoriCorrelati")
                            where attivita.Id == idAttivita
                            select attivita;

                CantieriCalendarioAttivita attivitaD = query.Single();
                if (attivitaD.CantieriCantieri.Count == 1)
                {
                    foreach (Cantiere cantiere in attivitaD.CantieriCantieri)
                    {
                        cantiere.CantieriCalendarioAttivita = null;
                        break;
                    }
                }
                context.CantieriCalendarioAttivita.DeleteObject(attivitaD);
                context.SaveChanges();
            }
        }

        public void UpdateAttivita(Int32 idAttivita, DateTime inizio, Int32 durata)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from attivita in context.CantieriCalendarioAttivita
                            where attivita.Id == idAttivita
                            select attivita;

                CantieriCalendarioAttivita attivitaU = query.Single();
                attivitaU.Data = inizio;
                attivitaU.Durata = durata;

                context.SaveChanges();
            }
        }

        public void UpdateAttivita(Int32 idAttivita, Int32 idMotivoRifiuto)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from attivita in context.CantieriCalendarioAttivita
                            where attivita.Id == idAttivita
                            select attivita;

                CantieriCalendarioAttivita attivitaU = query.Single();
                attivitaU.IdRifiuto = idMotivoRifiuto;

                context.SaveChanges();
            }
        }

        public void InsertAttivita(CantieriCalendarioAttivita attivita)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.CantieriCalendarioAttivita.AddObject(attivita);

                if (attivita.IdCantiere.HasValue)
                {
                    var query = from cantiere in context.CantieriCantieri
                                where cantiere.IdCantiere == attivita.IdCantiere.Value
                                select cantiere;

                    Cantiere cantiereU = query.Single();
                    //cantiereU.CantieriCalendarioAttivita = attivita;
                    attivita.CantieriCantieri.Add(cantiereU);

                    Int32 idIspettorePresaInCarico = attivita.IdIspettore;
                    // Devo anche creare l'attività agli altri ispettori ai quali era stato assegnato il cantiere
                    if (cantiereU.CantieriAssegnazione != null && cantiereU.CantieriAssegnazione.Ispettori != null)
                    {
                        foreach (Ispettore ispettore in cantiereU.CantieriAssegnazione.Ispettori)
                        {
                            if (ispettore.IdIspettore != idIspettorePresaInCarico)
                            {
                                var queryUtente = from ispRec in context.Ispettori
                                                  where ispRec.IdIspettore == ispettore.IdIspettore
                                                  select ispRec;

                                attivita.IspettoriCorrelati.Add(queryUtente.Single());
                            }
                        }
                    }
                }

                context.SaveChanges();
            }
        }

        public CantieriCalendarioAttivita GetAttivita(Int32 idAttivita)
        {
            CantieriCalendarioAttivita res = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from attivita in context.CantieriCalendarioAttivita.Include("TipologiaAttivita").Include("Ispettore").Include("CantieriCantieri").Include("IspettoriCorrelati").Include("Rifiuto")
                            where attivita.Id == idAttivita
                            select attivita;
                res = query.Single();
            }

            return res;
        }

        public void UpdateAttivita(CantieriCalendarioAttivita attivita)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from attivitaO in context.CantieriCalendarioAttivita
                            where attivitaO.Id == attivita.Id
                            select attivitaO;

                CantieriCalendarioAttivita attivitaU = query.Single();
                attivitaU.Data = attivita.Data;
                attivitaU.Durata = attivita.Durata;
                attivitaU.IdTipologiaAttivita = attivita.IdTipologiaAttivita;
                attivitaU.Descrizione = attivita.Descrizione;

                context.SaveChanges();
            }
        }
    }
}