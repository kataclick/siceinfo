using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Xml;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Geocode.Business;
using TBridge.Cemi.Geocode.Type.Entities;

namespace TBridge.Cemi.Cantieri.Business
{
    [Obsolete("Utilizzare TBridge.Cemi.Geocode.Business")]
    public class CantieriGeocoding
    {
        public static IndirizzoCollection GeoCodeGoogleMultiplo(string address)
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            List<Indirizzo> indirizziGeocodificati = GeocodeProvider.Geocode(address);

            if (indirizziGeocodificati != null)
            {
                foreach (Indirizzo indirizzo in indirizziGeocodificati)
                {
                    indirizzi.Add(new Cpt.Type.Entities.Indirizzo(indirizzo.Via, indirizzo.Civico, indirizzo.Comune,
                                                                  indirizzo.Provincia, indirizzo.Cap,
                                                                  indirizzo.Latitudine, indirizzo.Longitudine));
                }
            }

            return indirizzi;
        }

        public static GeocodingResult GeoCodeGoogle(string address)
        {
            GeocodingResult geocodingResult = new GeocodingResult();
            geocodingResult.Result = false;

            WebClient webClient = new WebClient();
            string googleKey = ConfigurationManager.AppSettings["GoogleKey"];
            string urlRequest = "http://maps.google.com/maps/geo?q=" + address + "&output=xml&hl=it&key=" + googleKey;
            string page = webClient.DownloadString(urlRequest);

            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.LoadXml(page);
                XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDocument.NameTable);
                nsManager.AddNamespace("a", "http://earth.google.com/kml/2.0");
                XmlNode respCodeNode =
                    xmlDocument.SelectSingleNode("/a:kml/a:Response/a:Status/a:code/text()", nsManager);
                string respCode = respCodeNode.Value;
                geocodingResult.GeoStatusCode = Int32.Parse(respCode);
                if (respCode == "200")
                {
                    XmlNodeList coordNodeList = xmlDocument.SelectNodes("/a:kml/a:Response/a:Placemark", nsManager);
                    if (coordNodeList.Count == 1)
                    {
                        nsManager.AddNamespace("b", "urn:oasis:names:tc:ciq:xsdschema:xAL:2.0");

                        XmlNode indirizzoNode;
                        indirizzoNode =
                            xmlDocument.SelectSingleNode(
                                "/a:kml/a:Response/a:Placemark/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:Thoroughfare/b:ThoroughfareName/text()",
                                nsManager);
                        if (indirizzoNode == null)
                            indirizzoNode =
                                xmlDocument.SelectSingleNode(
                                    "/a:kml/a:Response/a:Placemark/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:DependentLocality/b:Thoroughfare/b:ThoroughfareName/text()",
                                    nsManager);
                        string indirizzo = indirizzoNode.Value;
                        string[] indArr = indirizzo.Split(',');
                        if (indArr.Length == 2)
                            geocodingResult.Civico = indArr[1].Trim();
                        geocodingResult.Indirizzo = indArr[0].Trim();

                        XmlNode capNode =
                            xmlDocument.SelectSingleNode(
                                "/a:kml/a:Response/a:Placemark/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:PostalCode/b:PostalCodeNumber/text()",
                                nsManager);
                        if (capNode != null)
                        {
                            string cap = capNode.Value;
                            geocodingResult.Cap = cap;
                        }

                        XmlNode comuneNode =
                            xmlDocument.SelectSingleNode(
                                "/a:kml/a:Response/a:Placemark/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:LocalityName/text()",
                                nsManager);
                        string comune = comuneNode.Value;
                        geocodingResult.Comune = comune;

                        XmlNode provinciaNode =
                            xmlDocument.SelectSingleNode(
                                "/a:kml/a:Response/a:Placemark/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:SubAdministrativeAreaName/text()",
                                nsManager);
                        string provincia = provinciaNode.Value;
                        geocodingResult.Provincia = provincia;

                        XmlNode regioneNode =
                            xmlDocument.SelectSingleNode(
                                "/a:kml/a:Response/a:Placemark/b:AddressDetails/b:Country/b:AdministrativeArea/b:AdministrativeAreaName/text()",
                                nsManager);
                        string regione = regioneNode.Value;
                        geocodingResult.Regione = regione;

                        XmlNode coordNode =
                            xmlDocument.SelectSingleNode("/a:kml/a:Response/a:Placemark/a:Point/a:coordinates/text()",
                                                         nsManager);
                        string coord = coordNode.Value;

                        string[] coordSplit = coord.Split(',');
                        string lat = coordSplit[1];
                        string lng = coordSplit[0];

                        try
                        {
                            NumberFormatInfo nfi = new NumberFormatInfo();
                            nfi.NumberDecimalSeparator = ".";

                            double latitude = double.Parse(lat, nfi);
                            double longitude = double.Parse(lng, nfi);

                            geocodingResult.Latitude = latitude;
                            geocodingResult.Longitude = longitude;

                            // Faccio un controllo per vedere se le cordinate trovate stanno nei dintorni di Milano
                            //if (latitude > 45.164044 && latitude < 45.864044 && longitude > 8.691567 && longitude < 9.691567)
                            if (geocodingResult.Regione.ToUpper() == "LOMBARDIA")
                                geocodingResult.Result = true;
                            else
                                geocodingResult.Result = false;
                        }
                        catch (Exception exc)
                        {
                        }
                    }
                    else
                    {
                        //throw new Exception("Pi� corrispondenze trovate");
                    }
                }
                else
                {
                    //throw new Exception(respCode);
                }
            }
            catch (XmlException xmlEx)
            {
                Exception ex = xmlEx;
            }
            catch (Exception ex)
            {
            }

            return geocodingResult;
        }
    }
}