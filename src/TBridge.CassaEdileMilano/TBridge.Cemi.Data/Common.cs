using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.Type.Filters;

namespace TBridge.Cemi.Data
{
    public class Common
    {
        public Common()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public string GetDataPremioFedelta()
        {
            string ret;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectDataPremioFedelta")
                )
            {
                ret = (string) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return ret;
        }

        public string GetDataImpreseRegolari()
        {
            string ret;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectDataImpreseRegolari")
                )
            {
                ret = (string) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return ret;
        }

        public int GetMesiDelegheScadute()
        {
            int ret;

            using (DbCommand dbCommand = DatabaseCemi.GetSqlStringCommand("SELECT dbo.UF_DelegheMesiScadenza()"))
            {
                ret = (int) DatabaseCemi.ExecuteScalar(dbCommand);
            }

            return ret;
        }

        public ListDictionary GetNatureGiuridiche()
        {
            ListDictionary natureGiuridiche = new ListDictionary();
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NaturaGiuridicaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idNaturaGiuridica"].ToString();
                        string descrizione = reader["descrizione"].ToString();

                        natureGiuridiche.Add(id, descrizione);
                    }
                }
            }

            return natureGiuridiche;
        }

        public ListDictionary GetAttivitaIstat()
        {
            ListDictionary attivitaIstat = new ListDictionary();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_AttivitaISTATSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idAttivitaISTAT"].ToString();
                        string descrizione = String.Format("{0} {1}", reader["idAttivitaISTAT"], reader["descrizione"]);

                        attivitaIstat.Add(id, descrizione);
                    }
                }
            }

            return attivitaIstat;
        }

        public ListDictionary GetTipiImpresa()
        {
            ListDictionary tipiImpresa = new ListDictionary();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiImpresaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idTipoImpresa"].ToString();
                        string descrizione = reader["descrizione"].ToString();

                        tipiImpresa.Add(id, descrizione);
                    }
                }
            }

            return tipiImpresa;
        }

        /// <summary>
        ///   Tipo invia denuncia
        /// </summary>
        /// <returns></returns>
        public ListDictionary GetTipiInvioDenuncia()
        {
            ListDictionary tipiInvioDenuncia = new ListDictionary();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiInvioDenunciaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idTipoInvioDenuncia"].ToString();
                        string descrizione = reader["descrizione"].ToString();

                        tipiInvioDenuncia.Add(id, descrizione);
                    }
                }
            }

            return tipiInvioDenuncia;
        }


        public StringCollection GetProvinceSiceNew()
        {
            StringCollection province = new StringCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ComuniSiceNewSelectProvince"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        province.Add(reader["provincia"].ToString());
                }
            }

            return province;
        }

        public ComuneSiceNewCollection GetComuniSiceNew(string provincia)
        {
            ComuneSiceNewCollection comuni = new ComuneSiceNewCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ComuniSiceNewSelect"))
            {
                if (!string.IsNullOrEmpty(provincia))
                    DatabaseCemi.AddInParameter(dbCommand, "@provincia", DbType.String, provincia);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ComuneSiceNew comune = new ComuneSiceNew();
                        comune.Provincia = reader["provincia"].ToString();
                        comune.CodiceCatastale = reader["codiceCatastale"].ToString();
                        comune.Comune = reader["comune"].ToString();

                        if (reader["cap"] != DBNull.Value)
                            comune.Cap = reader["cap"].ToString();

                        comuni.Add(comune);
                    }
                }
            }

            return comuni;
        }

        public ComuneSiceNew GetComuneSiceNew(string codiceCatastale)
        {
            ComuneSiceNew comune = new ComuneSiceNew();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ComuniSiceNewSelectByIdComune"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idComune", DbType.String, codiceCatastale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        comune.CodiceCatastale = reader["codiceCatastale"].ToString();
                        comune.Comune = reader["comune"].ToString();
                        comune.Provincia = reader["provincia"].ToString();
                        comune.Cap = reader["CAP"].ToString();
                    }
                }
            }
            return comune;
        }

        public ListDictionary GetNazioni()
        {
            ListDictionary nazioni = new ListDictionary();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NazionalitaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idNazione"].ToString();
                        string nome = reader["nomeNazione"].ToString();

                        nazioni.Add(id, nome);
                    }
                }
            }

            return nazioni;
        }

        public ListDictionary GetPreIndirizzi()
        {
            ListDictionary preIndirizzi = new ListDictionary();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiViaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idTipoVia"].ToString();
                        string nome = reader["descrizione"].ToString();

                        preIndirizzi.Add(id, nome);
                    }
                }
            }

            return preIndirizzi;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ListDictionary GetAssociazioniImprenditoriali()
        {
            ListDictionary associazioni = new ListDictionary();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_AssociazioniImprenditorialiSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        string id = reader["idEnte"].ToString();
                        string nome = reader["descrizione"].ToString();

                        associazioni.Add(id, nome);
                    }
                }
            }

            return associazioni;
        }

        /// <summary>
        /// </summary>
        /// <param name = "codiceCatastale"></param>
        /// <returns></returns>
        public FrazioneSiceNewCollection GetFrazioniSiceNew(string codiceCatastale)
        {
            FrazioneSiceNewCollection frazioni = new FrazioneSiceNewCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_FrazioniSiceNewSelect"))
            {
                if (!string.IsNullOrEmpty(codiceCatastale))
                    DatabaseCemi.AddInParameter(dbCommand, "@codiceCatastale", DbType.String, codiceCatastale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        FrazioneSiceNew frazione = new FrazioneSiceNew();
                        frazione.CodiceCatastale = reader["codiceCatastale"].ToString();
                        frazione.Frazione = reader["frazione"].ToString();

                        if (reader["cap"] != DBNull.Value)
                            frazione.Cap = reader["cap"].ToString();

                        frazioni.Add(frazione);
                    }
                }
            }

            return frazioni;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ComprensorioSindacaleCollection GetComprensori(bool selezionabili)
        {
            ComprensorioSindacaleCollection comprensori = new ComprensorioSindacaleCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ComprensoriSindacaliSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@selezionabili", DbType.Boolean, selezionabili);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        ComprensorioSindacale comprensorio = new ComprensorioSindacale();

                        // Dati generici
                        comprensorio.Id = reader["idComprensorioSindacale"].ToString();
                        comprensorio.Descrizione = reader["descrizione"].ToString();

                        comprensori.Add(comprensorio);
                    }
                }
            }

            return comprensori;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public SindacatoCollection GetSindacati()
        {
            SindacatoCollection sindacati = new SindacatoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_SindacatiSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Sindacato sindacato = new Sindacato();

                        // Dati generici
                        sindacato.Id = reader["idSindacato"].ToString();
                        sindacato.Descrizione = reader["descrizione"].ToString();

                        sindacati.Add(sindacato);
                    }
                }
            }

            return sindacati;
        }

        /// <summary>
        /// Ritorna le Casse Edili presenti in anagrafica
        /// </summary>
        /// <param name="conMilano">Indica se deve essere ritornata anche la Cassa Edile di Milano o meno</param>
        /// <returns></returns>
        public CassaEdileCollection GetCasseEdili(Boolean conMilano)
        {
            CassaEdileCollection casse = new CassaEdileCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CasseEdiliSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@conMilano", DbType.Boolean, conMilano);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile();

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        if (!Convert.IsDBNull(reader["descrizione"]))
                            cassaEdile.Descrizione = (string) reader["descrizione"];
                        if (!Convert.IsDBNull(reader["cnce"]))
                            cassaEdile.Cnce = true;

                        casse.Add(cassaEdile);
                    }
                }
            }

            return casse;
        }

        public StatoCivileCollection GetStatiCivili()
        {
            StatoCivileCollection statiCivili = new StatoCivileCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiStatiCiviliSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdStatoCivile = reader.GetOrdinal("idTipoStatoCivile");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        StatoCivile statoCivile = new StatoCivile();
                        statiCivili.Add(statoCivile);

                        statoCivile.IdStatoCivile = reader.GetString(indiceIdStatoCivile);
                        statoCivile.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return statiCivili;
        }

        public DataTable GetProvince()
        {
            DataTable dt = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Comuni_Select_Province"))
            {
                DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }

            return dt;
        }

        public DataTable GetComuniByProvincia(int provincia)
        {
            DataTable dt = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Comuni_Select_ComuniByProvincia"))
            {
                DatabaseCemi.AddInParameter(comando, "@idProvincia", DbType.Int32, provincia);
                DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }

            return dt;
        }

        public DataTable GetCapByComune(Int64 idComune)
        {
            DataTable dt = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Comuni_Select_CAPByComune"))
            {
                DatabaseCemi.AddInParameter(comando, "@idComune", DbType.Int64, idComune);
                DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

                if (ds != null && ds.Tables.Count > 0)
                    dt = ds.Tables[0];
            }

            return dt;
        }

        public ImpresaCollection GetImpreseConsulente(int idConsulente)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_Imprese_ConsulentiSelectPerNotifiche")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSociale");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();

                        impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSocialeImpresa);

                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        public void ConsulenteSelezionaImpresa(Int32 idConsulente, Int32? idImpresa)
        {
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ConsulentiImpresaSelezionataInsertUpdate")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);
                if (idImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
                //if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                //{
                //    throw new Exception("ConsulenteSelezionaImpresa: impossibile selezionare l'impresa.");
                //}
            }
        }

        public void ConsulenteImpresaSelezionata(Int32 idConsulente, out Int32 idImpresa, out String ragioneSociale)
        {
            idImpresa = -1;
            ragioneSociale = String.Empty;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ConsulentiImpresaSelezionataSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        if (!Convert.IsDBNull(reader["idImpresa"]))
                        {
                            idImpresa = (Int32) reader["idImpresa"];
                            ragioneSociale = (String) reader["ragioneSociale"];
                        }
                    }
                }
            }
        }

        public void ConsulenteImpresaSelezionata(Int32 idConsulente, out Int32 idImpresa, out String ragioneSociale,
                                                 out String codiceFiscale)
        {
            idImpresa = -1;
            ragioneSociale = String.Empty;
            codiceFiscale = String.Empty;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ConsulentiImpresaSelezionataSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        if (!Convert.IsDBNull(reader["idImpresa"]))
                        {
                            idImpresa = (Int32) reader["idImpresa"];
                            ragioneSociale = (String) reader["ragioneSociale"];
                            codiceFiscale = reader["codiceFiscale"] as string;
                        }
                    }
                }
            }
        }

        public TipoContrattoCollection GetTipiContratto()
        {
            TipoContrattoCollection tipiContratto = new TipoContrattoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiContrattoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdContratto = reader.GetOrdinal("idContratto");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoContratto tipoContratto = new TipoContratto();
                        tipiContratto.Add(tipoContratto);

                        tipoContratto.IdContratto = reader.GetString(indiceIdContratto);
                        tipoContratto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiContratto;
        }

        public TipoQualificaCollection GetTipiQualifica()
        {
            TipoQualificaCollection tipiQualifica = new TipoQualificaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiQualificaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdQualifica = reader.GetOrdinal("idQualifica");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoQualifica tipoQualifica = new TipoQualifica();
                        tipiQualifica.Add(tipoQualifica);

                        tipoQualifica.IdQualifica = reader.GetString(indiceIdQualifica);
                        tipoQualifica.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiQualifica;
        }

        public TipoQualificaCollection GetTipiQualifica(String tipoCategoria)
        {
            TipoQualificaCollection tipiQualifica = new TipoQualificaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiQualificaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoCategoria", DbType.String, tipoCategoria);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdQualifica = reader.GetOrdinal("idQualifica");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoQualifica tipoQualifica = new TipoQualifica();
                        tipiQualifica.Add(tipoQualifica);

                        tipoQualifica.IdQualifica = reader.GetString(indiceIdQualifica);
                        tipoQualifica.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiQualifica;
        }

        public TipoMansioneCollection GetTipiMansione()
        {
            TipoMansioneCollection tipiMansione = new TipoMansioneCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiMansioneSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdMansione = reader.GetOrdinal("idMansione");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoMansione tipoMansione = new TipoMansione();
                        tipiMansione.Add(tipoMansione);

                        tipoMansione.IdMansione = reader.GetString(indiceIdMansione);
                        tipoMansione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiMansione;
        }

        public TipoMansione GetTipoMansione(String codiceQualificaSiceNew)
        {
            TipoMansione tipoMansione = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipoMansioneSelectByCodiceQualifica"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceQualificaSiceNew", DbType.String, codiceQualificaSiceNew);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdMansione = reader.GetOrdinal("idMansione");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    if (reader.Read())
                    {
                        tipoMansione = new TipoMansione
                                            {
                                                IdMansione = reader.GetString(indiceIdMansione),
                                                Descrizione = reader.GetString(indiceDescrizione)
                                            };
                    }
                }
            }

            return tipoMansione;
        }

        public TipoCategoriaCollection GetTipiCategoria()
        {
            TipoCategoriaCollection tipiCategoria = new TipoCategoriaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCategoriaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCategoria = reader.GetOrdinal("idCategoria");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCategoria tipoCategoria = new TipoCategoria();
                        tipiCategoria.Add(tipoCategoria);

                        tipoCategoria.IdCategoria = reader.GetString(indiceIdCategoria);
                        tipoCategoria.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCategoria;
        }

        public TipoCategoriaCollection GetTipiCategoria(String tipoContratto)
        {
            TipoCategoriaCollection tipiCategoria = new TipoCategoriaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCategoriaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoContratto", DbType.String, tipoContratto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCategoria = reader.GetOrdinal("idCategoria");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCategoria tipoCategoria = new TipoCategoria();
                        tipiCategoria.Add(tipoCategoria);

                        tipoCategoria.IdCategoria = reader.GetString(indiceIdCategoria);
                        tipoCategoria.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCategoria;
        }

        public TipoFineRapportoCollection GetTipiFineRapporto()
        {
            TipoFineRapportoCollection tipiFineRapporto = new TipoFineRapportoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiFineRapportoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTipoFineRapporto = reader.GetOrdinal("idTipoFineRapporto");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoFineRapporto tipoFineRapporto = new TipoFineRapporto();
                        tipiFineRapporto.Add(tipoFineRapporto);

                        tipoFineRapporto.IdTipoFineRapporto = reader.GetString(indiceIdTipoFineRapporto);
                        tipoFineRapporto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiFineRapporto;
        }

        public TipoInizioRapportoCollection GetTipiInizioRapporto()
        {
            TipoInizioRapportoCollection tipiInizioRapporto = new TipoInizioRapportoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiInizioRapportoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTipoInizioRapporto = reader.GetOrdinal("idTipoInizioRapporto");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoInizioRapporto tipoInizioRapporto = new TipoInizioRapporto();
                        tipiInizioRapporto.Add(tipoInizioRapporto);

                        tipoInizioRapporto.IdTipoInizioRapporto = reader.GetString(indiceIdTipoInizioRapporto);
                        tipoInizioRapporto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiInizioRapporto;
        }

        public TipoCartaPrepagataCollection GetTipiCartaPrepagata()
        {
            TipoCartaPrepagataCollection tipiCartaPrepagata = new TipoCartaPrepagataCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCartaPrepagataSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTipoCartaPrepagata = reader.GetOrdinal("idTipoCartaPrepagata");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCartaPrepagata tipoCartaPrepagata = new TipoCartaPrepagata();
                        tipiCartaPrepagata.Add(tipoCartaPrepagata);

                        tipoCartaPrepagata.IdTipoCartaPrepagata = reader.GetString(indiceIdTipoCartaPrepagata);
                        tipoCartaPrepagata.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCartaPrepagata;
        }

        public TipoLinguaCollection GetLingue()
        {
            TipoLinguaCollection tipiLingua = new TipoLinguaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiLinguaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLingua = reader.GetOrdinal("idLingua");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoLingua lingua = new TipoLingua();
                        tipiLingua.Add(lingua);

                        lingua.IdLingua = reader.GetString(indiceIdLingua);
                        lingua.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiLingua;
        }


        public BollettiniFrecciaStatistichePagati GetBollettiniFrecciaStatistichePagati()
        {
            BollettiniFrecciaStatistichePagati statistichePagati = new BollettiniFrecciaStatistichePagati();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettinoFrecciaRichiesteSelectStatistichePagati"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        if (reader["numeroBollettiniUniciStampatiPagati"] != DBNull.Value)
                            statistichePagati.NumeroBollettiniUniciStampatiPagati =
                                Int32.Parse(reader["numeroBollettiniUniciStampatiPagati"].ToString());
                        if (reader["importoPagato"] != DBNull.Value)
                            statistichePagati.ImportoPagato = Decimal.Parse(reader["importoPagato"].ToString());
                        if (reader["numeroBollettiniUniciStampati"] != DBNull.Value)
                            statistichePagati.NumeroBollettiniUniciStampati =
                                Int32.Parse(reader["numeroBollettiniUniciStampati"].ToString());
                    }
                }
            }

            return statistichePagati;
        }

        public Impresa GetImpresa(string codiceFiscale)
        {
            Impresa impresa = new Impresa();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectByPartitaIva"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@partitaIva", DbType.String, codiceFiscale);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        impresa.IdImpresa = Int32.Parse(reader["idImpresa"].ToString());
                        impresa.RagioneSociale = reader["ragioneSociale"].ToString();
                    }
                }
            }
            return impresa;
        }

        public Impresa GetImpresaById(int idImpresa) //TODO
        {
            Impresa impresa = new Impresa();

            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectById"))
            {
                DatabaseCemi.AddInParameter(command, "IdImpresa", DbType.Int32, idImpresa);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    #region Indici
                    int indiceIdUtente = reader.GetOrdinal("idUtente");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzoSedeLegale");
                    int indiceLocalita = reader.GetOrdinal("localitaSedeLegale");
                    int indiceProvincia = reader.GetOrdinal("provinciaSedeLegale");
                    int indiceCap = reader.GetOrdinal("capSedeLegale");
                    int indiceEmail = reader.GetOrdinal("eMailSedeLegale");
                    #endregion
                    if (reader.Read())
                    {
                        impresa.IdImpresa = idImpresa;
                        if (!reader.IsDBNull(indiceIdUtente))
                        {
                            impresa.IdUtente = reader.GetInt32(indiceIdUtente);
                        }
                        impresa.RagioneSociale = reader.IsDBNull(indiceRagioneSociale) ? null : reader.GetString(indiceRagioneSociale);
                        impresa.CodiceFiscale = reader.IsDBNull(indiceCodiceFiscale) ? null : reader.GetString(indiceCodiceFiscale);
                        impresa.CapSedeLegale = reader.IsDBNull(indiceCap) ? null : reader.GetString(indiceCap);
                        impresa.IndirizzoSedeLegale = reader.IsDBNull(indiceIndirizzo) ? null : reader.GetString(indiceIndirizzo);
                        impresa.LocalitaSedeLegale = reader.IsDBNull(indiceLocalita) ? null : reader.GetString(indiceLocalita);
                        impresa.ProvinciaSedeLegale = reader.IsDBNull(indiceProvincia) ? null : reader.GetString(indiceProvincia);
                        impresa.EmailSedeLegale = reader.IsDBNull(indiceEmail) ? null : reader.GetString(indiceEmail);
                    }
                }
            }

            return impresa;
        }

        public BollettinoFrecciaStampabileCollection GetBollettiniFrecciaStampabili(
            BollettinoFrecciaStampabileFilter filtro)
        {
            BollettinoFrecciaStampabileCollection bollettini = new BollettinoFrecciaStampabileCollection();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniFrecciaStampabiliSelectWithFilter"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@anno", DbType.Int32, filtro.Anno);
                DatabaseCemi.AddInParameter(dbCommand, "@mese", DbType.Int32, filtro.Mese);
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filtro.IdImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filtro.RagioneSociale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BollettinoFrecciaStampabile bollettino = new BollettinoFrecciaStampabile();
                        bollettino.Anno = (int) reader["annoCompetenza"];
                        bollettino.Mese = (int) reader["meseCompetenza"];
                        bollettino.Importo = (decimal) reader["importo"];
                        bollettino.IdImpresa = (int) reader["idImpresa"];
                        bollettino.RagioneSociale = reader["ragioneSociale"].ToString();
                        bollettino.Cip = reader["cip"].ToString();

                        bollettini.Add(bollettino);
                    }
                }
            }

            return bollettini;
        }

        public FestivitaCollection GetFestivita(int anno)
        {
            FestivitaCollection festivita = new FestivitaCollection();

            festivita.Add(new Festivita { Giorno = new DateTime(anno, 1, 1) });     // Capodanno
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 1, 6) });     // Epifania
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 25) });    // Festa della liberazione
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 5, 1) });     // Festa del Lavoro
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 6, 2) });     // Festa della Repubblica
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 8, 15) });    // Ferragosto
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 11, 1) });    // Ognissanti
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 12, 7) });    // Sant'Ambrogio
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 12, 8) });    // Immacolata
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 12, 25) });   // Natale
            festivita.Add(new Festivita { Giorno = new DateTime(anno, 12, 26) });   // Santo Stefano

            festivita.Add(new Festivita { Giorno = GetPasqua(anno).AddDays(1) });   // Luned� dell'Angelo
            //switch (anno)
            //{
            //    case 2010:
            //        festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 5) });
            //        break;
            //    case 2011:
            //        festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 25) });
            //        break;
            //    case 2012:
            //        festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 9) });
            //        break;
            //    case 2013:
            //        festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 1) });
            //        break;
            //    case 2014:
            //        festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 21) });
            //        break;
            //    case 2015:
            //        festivita.Add(new Festivita { Giorno = new DateTime(anno, 4, 6) });
            //        break;
            //}

            return festivita;
        }

        public DateTime GetPasqua(Int32 anno)
        {

            int a;
            int b;
            int c;
            int d;
            int e;
            int M;
            int N;
            int da;
            int mo;

            if ((anno >= 1583) && (anno <= 1699))
            {
                M = 22;
                N = 2;
            }
            else if ((anno >= 1700) && (anno <= 1799))
            {
                M = 23;
                N = 3;
            }
            else if ((anno >= 1800) && (anno <= 1899))
            {
                M = 23;
                N = 4;
            }
            else if ((anno >= 1900) && (anno <= 2099))
            {
                M = 24;
                N = 5;
            }
            else if ((anno >= 2100) && (anno <= 2199))
            {
                M = 24;
                N = 6;
            }
            else if ((anno >= 2200) && (anno <= 2299))
            {
                M = 25;
                N = 0;
            }
            else if ((anno >= 2300) && (anno <= 2399))
            {
                M = 26;
                N = 1;
            }
            else if ((anno >= 2400) && (anno <= 2499))
            {
                M = 25;
                N = 1;
            }
            else
            {
                return new DateTime(1900, 1, 1);
            }

            a = anno % 19;
            b = anno % 4;
            c = anno % 7;
            d = ((19 * a) + M) % 30;
            e = ((2 * b) + (4 * c) + (6 * d) + N) % 7;


            if (d + e < 10)
            {
                da = d + e + 22;
                mo = 3;
            }
            else
            {
                da = d + e - 9;
                mo = 4;
            }

            if (da == 26 && mo == 4)
            {
                da = 19;
                mo = 4;
            }
            else if (da == 25 && mo == 4 && d == 28 && e == 6 && a > 10)
            {
                da = 18;
                mo = 4;
            }

            return new DateTime(anno, mo, da);
        }

        #region Lavoratore anagrafica comune

        public Boolean InsertLavoratore(LavoratoreAnagraficaComune lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            else
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriAnagraficaComuneInsert")
                    )
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                    DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, lavoratore.Sesso);
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);

                    DatabaseCemi.AddInParameter(comando, "@fonte", DbType.Int32, lavoratore.Fonte);
                    DatabaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);

                    DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String, lavoratore.PaeseNascita);

                    if (!String.IsNullOrEmpty(lavoratore.ProvinciaNascita))
                    {
                        DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                                                    lavoratore.ProvinciaNascita);
                    }
                    if (!String.IsNullOrEmpty(lavoratore.ComuneNascita))
                    {
                        DatabaseCemi.AddInParameter(comando, "@comuneNascita", DbType.String, lavoratore.ComuneNascita);
                    }

                    if (transaction == null)
                    {
                        DatabaseCemi.ExecuteNonQuery(comando);
                    }
                    else
                    {
                        DatabaseCemi.ExecuteNonQuery(comando, transaction);
                    }

                    Int32 idTemp = (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratore");
                    if (idTemp > 0)
                    {
                        lavoratore.IdLavoratore = idTemp;
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool UpdateLavoratore(LavoratoreAnagraficaComune lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            else
            {
                if (!lavoratore.IdLavoratore.HasValue)
                {
                    throw new ArgumentException("Non � presente l'ID", "lavoratore");
                }
                else
                {
                    using (
                        DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriAnagraficaComuneUpdate")
                        )
                    {
                        DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                    lavoratore.IdLavoratore.Value);
                        DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                        DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                        DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                        DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, lavoratore.Sesso);
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);

                        DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String, lavoratore.PaeseNascita);

                        if (!String.IsNullOrEmpty(lavoratore.ProvinciaNascita))
                        {
                            DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                                                        lavoratore.ProvinciaNascita);
                        }
                        if (!String.IsNullOrEmpty(lavoratore.ComuneNascita))
                        {
                            DatabaseCemi.AddInParameter(comando, "@comuneNascita", DbType.String,
                                                        lavoratore.ComuneNascita);
                        }

                        if (transaction == null)
                        {
                            if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                            {
                                res = true;
                            }
                        }
                        else
                        {
                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                res = true;
                            }
                        }
                    }
                }
            }

            return res;
        }

        #endregion

        #region Imprese anagrafica comune

        public Boolean InsertImpresa(ImpresaAnagraficaComune impresa, DbTransaction transaction)
        {
            Boolean res = false;

            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            else
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseAnagraficaComuneInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);

                    DatabaseCemi.AddInParameter(comando, "@fonte", DbType.Int32, impresa.Fonte);
                    DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                    if (transaction == null)
                    {
                        DatabaseCemi.ExecuteNonQuery(comando);
                    }
                    else
                    {
                        DatabaseCemi.ExecuteNonQuery(comando, transaction);
                    }

                    Int32 idTemp = (Int32) DatabaseCemi.GetParameterValue(comando, "@idImpresa");
                    if (idTemp > 0)
                    {
                        impresa.IdImpresa = idTemp;
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region Bollettino (Freccia o Mav)
        public BollettinoFrecciaCollection GetDatiBollettiniFreccia(int idImpresa)
        {
            BollettinoFrecciaCollection bollettini = new BollettinoFrecciaCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettinoFrecciaDatiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BollettinoFreccia bollettino = new BollettinoFreccia();
                        bollettino.IdImpresa = idImpresa;
                        bollettino.Importo = decimal.Parse((reader["importo"].ToString()));
                        bollettino.Anno = Int32.Parse(reader["anno"].ToString());
                        bollettino.Mese = Int32.Parse(reader["mese"].ToString());
                        bollettino.Cip = reader["CIP"].ToString();

                        bollettini.Add(bollettino);
                    }
                }
            }

            return bollettini;
        }

        public BollettinoCollection GetDatiBollettini(int idImpresa)
        {
            //Creo tutti gli oggetti della lista come bollettiniFreccia cos� posso assegnare anche la propriet� CIP

            BollettinoCollection bollettini = new BollettinoCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettinoFrecciaDatiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BollettinoFreccia bollettino = new BollettinoFreccia();
                        bollettino.IdImpresa = idImpresa;
                        bollettino.Importo = decimal.Parse((reader["importo"].ToString()));
                        bollettino.Anno = Int32.Parse(reader["anno"].ToString());
                        bollettino.Mese = Int32.Parse(reader["mese"].ToString());
                        bollettino.Sequenza = Int32.Parse(reader["sequenza"].ToString());
                        bollettino.Cip = reader["CIP"].ToString();

                        bollettini.Add(bollettino);
                    }
                }
            }

            return bollettini;
        }

        public BollettinoCollection GetDatiDenunce(int idImpresa)
        {
            //Creo tutti gli oggetti della lista come bollettiniFreccia cos� posso assegnare anche la propriet� CIP

            BollettinoCollection bollettini = new BollettinoCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_DenunciaDatiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BollettinoFreccia bollettino = new BollettinoFreccia();
                        bollettino.IdImpresa = idImpresa;
                        bollettino.Importo = decimal.Parse((reader["importo"].ToString()));
                        bollettino.Anno = Int32.Parse(reader["anno"].ToString());
                        bollettino.Mese = Int32.Parse(reader["mese"].ToString());
                        bollettino.Sequenza = Int32.Parse(reader["sequenza"].ToString());
                        bollettino.Cip = reader["CIP"].ToString();

                        bollettini.Add(bollettino);
                    }
                }
            }

            return bollettini;
        }


        public Bollettino GetDettaglioBollettino(Int32 idImpresa, Int32 anno, Int32 mese, Int32 sequenza)
        {
            Bollettino bollettino = null;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettinoFrecciaSelectDettaglio"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(dbCommand, "@mese", DbType.Int32, mese);
                DatabaseCemi.AddInParameter(dbCommand, "@sequenza", DbType.Int32, sequenza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bollettino = new BollettinoFreccia();
                        bollettino.IdImpresa = idImpresa;
                        bollettino.Anno = Int32.Parse(reader["anno"].ToString());
                        bollettino.Mese = Int32.Parse(reader["mese"].ToString());
                        bollettino.Sequenza = Int32.Parse(reader["sequenza"].ToString());

                        bollettino.Importo = Decimal.Parse(reader["importoVersato"].ToString());
                        bollettino.ImportoDovuto0 = Decimal.Parse(reader["importoDovuto00"].ToString());
                        bollettino.ImportoDovuto1 = Decimal.Parse(reader["importoDovuto01"].ToString());
                        bollettino.ImportoDovuto2 = Decimal.Parse(reader["importoDovuto02"].ToString());
                        bollettino.ImportoDovuto3 = Decimal.Parse(reader["importoDovuto03"].ToString());
                        bollettino.ImportoDovuto4 = Decimal.Parse(reader["importoDovuto04"].ToString());
                        bollettino.ImportoDovuto5 = Decimal.Parse(reader["importoDovuto05"].ToString());
                    }
                }
            }

            return bollettino;
        }

        public BollettinoFrecciaStampatoCollection GetBollettiniFrecciaStampati(BollettinoFrecciaStampatoFilter filtro)
        {
            BollettinoFrecciaStampatoCollection bollettini = new BollettinoFrecciaStampatoCollection();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettinoFrecciaRichiesteSelectWithFilter"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@dataInizio", DbType.Date, filtro.DataInizio);
                DatabaseCemi.AddInParameter(dbCommand, "@dataFine", DbType.Date, filtro.DataFine);

                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filtro.IdImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@anno", DbType.Int32, filtro.Anno);
                DatabaseCemi.AddInParameter(dbCommand, "@mese", DbType.Int32, filtro.Mese);
                DatabaseCemi.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filtro.RagioneSociale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BollettinoFrecciaStampato bollettino = new BollettinoFrecciaStampato();

                        bollettino.IdImpresa = Int32.Parse(reader["idImpresa"].ToString());
                        bollettino.RagioneSociale = reader["ragioneSociale"].ToString();
                        if (reader["importo"] != DBNull.Value)
                            bollettino.Importo = Decimal.Parse(reader["importo"].ToString());

                        bollettino.Anno = Int32.Parse(reader["anno"].ToString());
                        bollettino.Mese = Int32.Parse(reader["mese"].ToString());

                        if (reader["dataRichiesta"] != DBNull.Value)
                            bollettino.DataRichiesta = DateTime.Parse(reader["dataRichiesta"].ToString());

                        if (reader["dataVersamento"] != DBNull.Value)
                            bollettino.DataVersamento = DateTime.Parse(reader["dataVersamento"].ToString());

                        bollettino.Cip = reader["CIP"].ToString();

                        bollettini.Add(bollettino);
                    }
                }
            }

            return bollettini;
        }

        public void GetUtentiBollettiniFrecciaStampati(out int numeroImprese, out int numeroConsulenti)
        {
            int imprese = 0, consulenti = 0;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniFrecciaUtentiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@dataInizio", DbType.Date, DBNull.Value);
                DatabaseCemi.AddInParameter(dbCommand, "@dataFine", DbType.Date, DBNull.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        if (reader["tipoUtente"].ToString().Equals("Imprese"))
                        {
                            imprese++;
                        }
                        else if (reader["tipoUtente"].ToString().Equals("Consulenti"))
                        {
                            consulenti++;
                        }
                    }
                }
            }
            numeroConsulenti = consulenti;
            numeroImprese = imprese;
        }


        public int GetBollettiniFrecciaStampabili(DateTime? dataInizio, DateTime? dataFine)
        {
            int bollettini = 0;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniFrecciaStampabiliSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@dataInizio", DbType.Date, dataInizio);
                DatabaseCemi.AddInParameter(dbCommand, "@dataFine", DbType.Date, dataFine);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bollettini++;
                    }
                }
            }
            return bollettini;
        }

        public Boolean RegistraRichiesta(BollettinoFreccia bollettino, int idUtente)
        {
            Boolean res = false;

            if (bollettino == null)
            {
                throw new ArgumentNullException("bollettino");
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettinoFrecciaRichiesteInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, bollettino.IdImpresa);

                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, bollettino.Anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, bollettino.Mese);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

                DatabaseCemi.AddInParameter(comando, "@cip", DbType.String, bollettino.Cip);
                DatabaseCemi.AddInParameter(comando, "@importo", DbType.Decimal, bollettino.Importo);


                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public Boolean RegistraRichiestaBollettino(Bollettino bollettino, int idUtente)
        {
            Boolean res = false;

            if (bollettino == null)
            {
                throw new ArgumentNullException("bollettino");
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniRichiesteInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, bollettino.IdImpresa);

                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, bollettino.Anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, bollettino.Mese);
                DatabaseCemi.AddInParameter(comando, "@sequenza", DbType.Int32, bollettino.Sequenza);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@importo", DbType.Decimal, bollettino.Importo);
                DatabaseCemi.AddInParameter(comando, "@idTipoCanalePagamento", DbType.Int32, bollettino.TipoCanalePagamento);


                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public Boolean RegistraRichiestaBollettinoDenuncia(Bollettino bollettino, int idUtente)
        {
            Boolean res = false;

            if (bollettino == null)
            {
                throw new ArgumentNullException("bollettino");
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniDenunciaRichiesteInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, bollettino.IdImpresa);

                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, bollettino.Anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, bollettino.Mese);
                DatabaseCemi.AddInParameter(comando, "@sequenza", DbType.Int32, bollettino.Sequenza);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@importo", DbType.Decimal, bollettino.Importo);
                DatabaseCemi.AddInParameter(comando, "@idTipoCanalePagamento", DbType.Int32, bollettino.TipoCanalePagamento);


                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public BollettinoStampatoCollection GetBollettiniStampati(BollettinoStampatoFilter filtro)
        {
            BollettinoStampatoCollection bollettiniStampati = new BollettinoStampatoCollection();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniRichiesteSelectWithFilter"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@dataInizio", DbType.Date, filtro.DataInizio);
                DatabaseCemi.AddInParameter(dbCommand, "@dataFine", DbType.Date, filtro.DataFine);

                DatabaseCemi.AddInParameter(dbCommand, "@anno", DbType.Int32, filtro.Anno);
                DatabaseCemi.AddInParameter(dbCommand, "@mese", DbType.Int32, filtro.Mese);
                DatabaseCemi.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                DatabaseCemi.AddInParameter(dbCommand, "@tipoCanalePagamento", DbType.Int32, filtro.CanalePagamento);
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filtro.IdImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region indici
                    int indiceIdCanalePagamento = reader.GetOrdinal("idTipoCanalePagamento");
                    int indiceDescrizioneCanalePAgamento = reader.GetOrdinal("descrizioneCanalePagamento");
                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceImporto = reader.GetOrdinal("importo");
                    int indiceAnno = reader.GetOrdinal("anno");
                    int indiceMese = reader.GetOrdinal("mese");
                    int indiceSequenza = reader.GetOrdinal("sequenzaDenuncia");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indiceDataRichiesta = reader.GetOrdinal("dataRichiesta");
                    int indiceDataVersamento = reader.GetOrdinal("dataVersamento");
                    #endregion

                    while (reader.Read())
                    {
                        Bollettino bollettino;
                        TipiCanalePagamento tipoCanale = (TipiCanalePagamento) reader.GetInt32(indiceIdCanalePagamento);

                        switch (tipoCanale)
                        {
                            case TipiCanalePagamento.BollettinoFreccia:
                                bollettino = new BollettinoFreccia { TipoCanalePagamento = TipiCanalePagamento.BollettinoFreccia };
                                break;
                            case TipiCanalePagamento.MavPopolareSondrio:
                                bollettino = new BollettinoMav { TipoCanalePagamento = TipiCanalePagamento.MavPopolareSondrio };
                                break;
                            default:
                                bollettino = null;
                                break;
                        }

                        if (bollettino != null)
                        {
                            bollettino.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            if (!reader.IsDBNull(indiceImporto))
                            {
                                bollettino.Importo = reader.GetDecimal(indiceImporto);
                            }
                            bollettino.Anno = reader.GetInt32(indiceAnno);
                            bollettino.Mese = reader.GetInt32(indiceMese);
                            if (!reader.IsDBNull(indiceSequenza))
                            {
                                bollettino.Sequenza = reader.GetInt32(indiceSequenza);
                            }

                            BollettinoStampato bollettinoStampato = new BollettinoStampato(bollettino);
                            bollettinoStampato.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            bollettinoStampato.DescrizioneTipoCanalePagamento = reader.GetString(indiceDescrizioneCanalePAgamento);
                            if (!reader.IsDBNull(indiceDataRichiesta))
                            {
                                bollettinoStampato.DataRichiesta = reader.GetDateTime(indiceDataRichiesta);
                            }
                            if (!reader.IsDBNull(indiceDataVersamento))
                            {
                                bollettinoStampato.DataVersamento = reader.GetDateTime(indiceDataVersamento);
                            }

                            bollettiniStampati.Add(bollettinoStampato);
                        }

                    }
                }
            }

            return bollettiniStampati;
        }

        public void GetUtentiBollettiniStampati(out int numeroImprese, out int numeroConsulenti)
        {
            int imprese = 0, consulenti = 0;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniUtentiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@dataInizio", DbType.Date, DBNull.Value);
                DatabaseCemi.AddInParameter(dbCommand, "@dataFine", DbType.Date, DBNull.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        if (reader["tipoUtente"].ToString().Equals("Imprese"))
                        {
                            imprese++;
                        }
                        else if (reader["tipoUtente"].ToString().Equals("Consulenti"))
                        {
                            consulenti++;
                        }
                    }
                }
            }
            numeroConsulenti = consulenti;
            numeroImprese = imprese;
        }

        public BollettiniFrecciaStatistichePagati GetBollettiniStatistichePagati()
        {
            BollettiniFrecciaStatistichePagati statistichePagati = new BollettiniFrecciaStatistichePagati();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_BollettiniRichiesteSelectStatistichePagati"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        if (reader["numeroBollettiniUniciStampatiPagati"] != DBNull.Value)
                            statistichePagati.NumeroBollettiniUniciStampatiPagati =
                                Int32.Parse(reader["numeroBollettiniUniciStampatiPagati"].ToString());
                        if (reader["importoPagato"] != DBNull.Value)
                            statistichePagati.ImportoPagato = Decimal.Parse(reader["importoPagato"].ToString());
                        if (reader["numeroBollettiniUniciStampati"] != DBNull.Value)
                            statistichePagati.NumeroBollettiniUniciStampati =
                                Int32.Parse(reader["numeroBollettiniUniciStampati"].ToString());
                    }
                }
            }

            return statistichePagati;
        }

        public TipoCanalePagamentoBolletinoCollection GetTipiCanalePagamentoBollettino(bool conElementoVuoto)
        {
            TipoCanalePagamentoBolletinoCollection tipiCanale = new TipoCanalePagamentoBolletinoCollection();
            if (conElementoVuoto)
            {
                tipiCanale.Add(new TipoCanalePagamentoBolletino { Id = -1, Descrizione = String.Empty });
            }

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCanalePagamentoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region indici
                    int indiceId = reader.GetOrdinal("idTipoCanalePagamento");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");
                    #endregion

                    while (reader.Read())
                    {
                        TipoCanalePagamentoBolletino tipoCanale = new TipoCanalePagamentoBolletino
                        {
                            Id = reader.GetInt32(indiceId),
                            Descrizione = reader.GetString(indiceDescrizione)
                        };

                        tipiCanale.Add(tipoCanale);
                    }
                }
            }

            return tipiCanale;
        }
        #endregion

        #region OreCNCE
        public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        {
            bool res = false;
            oreDuplicate = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, ore.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, ore.IdCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, ore.Anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, ore.Mese);

                DatabaseCemi.AddInParameter(comando, "@oreLavorate", DbType.Int32, ore.OreLavorate);
                DatabaseCemi.AddInParameter(comando, "@oreFerie", DbType.Int32, ore.OreFerie);
                DatabaseCemi.AddInParameter(comando, "@oreInfortunio", DbType.Int32, ore.OreInfortunio);
                DatabaseCemi.AddInParameter(comando, "@oreMalattia", DbType.Int32, ore.OreMalattia);
                DatabaseCemi.AddInParameter(comando, "@oreCassaIntegrazione", DbType.Int32, ore.OreCassaIntegrazione);
                DatabaseCemi.AddInParameter(comando, "@orePermessoRetribuito", DbType.Int32, ore.OrePermessoRetribuito);
                DatabaseCemi.AddInParameter(comando, "@orePermessoNonRetribuito", DbType.Int32,
                                            ore.OrePermessoNonRetribuito);
                DatabaseCemi.AddInParameter(comando, "@oreAltro", DbType.Int32, ore.OreAltro);
                DatabaseCemi.AddInParameter(comando, "@livelloErogazione", DbType.Int32, ore.LivelloErogazione);
                DatabaseCemi.AddInParameter(comando, "@inseriteManualmente", DbType.Boolean, ore.InseriteManualmente);

                try
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
                catch (SqlException sqlExc)
                {
                    // Eccezione che viene generata se provo ad inserire due volte le ore per lo stesso mese
                    if (sqlExc.Number == 2627)
                        oreDuplicate = true;
                    else
                        throw;
                }
            }

            return res;
        }

        public OreMensiliCNCECollection GetOreMensiliCNCE(int idLavoratore)
        {
            OreMensiliCNCECollection ore = new OreMensiliCNCECollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CNCEOreSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        OreMensiliCNCE oreMese = TrasformaReaderInOreMensili(reader);
                        ore.Add(oreMese);
                    }
                }
            }

            return ore;
        }

        public OreMensiliCNCE GetOreMensiliCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        {
            OreMensiliCNCE ore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CNCEOreSelectSingolo"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        ore = TrasformaReaderInOreMensili(reader);
                    }
                }
            }

            return ore;
        }

        private OreMensiliCNCE TrasformaReaderInOreMensili(IDataReader reader)
        {
            OreMensiliCNCE oreMese = new OreMensiliCNCE();

            oreMese.IdCassaEdile = (string) reader["idCassaEdile"];
            oreMese.IdLavoratore = (int) reader["idLavoratore"];
            oreMese.Anno = (int) reader["anno"];
            oreMese.Mese = (int) reader["mese"];
            if (!Convert.IsDBNull(reader["oreLavorate"]))
                oreMese.OreLavorate = (int) reader["oreLavorate"];
            if (!Convert.IsDBNull(reader["oreFerie"]))
                oreMese.OreFerie = (int) reader["oreFerie"];
            if (!Convert.IsDBNull(reader["oreInfortunio"]))
                oreMese.OreInfortunio = (int) reader["oreInfortunio"];
            if (!Convert.IsDBNull(reader["oreMalattia"]))
                oreMese.OreMalattia = (int) reader["oreMalattia"];
            if (!Convert.IsDBNull(reader["oreCassaIntegrazione"]))
                oreMese.OreCassaIntegrazione = (int) reader["oreCassaIntegrazione"];
            if (!Convert.IsDBNull(reader["orePermessoRetribuito"]))
                oreMese.OrePermessoRetribuito = (int) reader["orePermessoRetribuito"];
            if (!Convert.IsDBNull(reader["orePermessoNonRetribuito"]))
                oreMese.OrePermessoNonRetribuito = (int) reader["orePermessoNonRetribuito"];
            if (!Convert.IsDBNull(reader["oreAltro"]))
                oreMese.OreAltro = (int) reader["oreAltro"];
            if (!Convert.IsDBNull(reader["livelloErogazione"]))
                oreMese.LivelloErogazione = (int) reader["livelloErogazione"];
            oreMese.InseriteManualmente = (bool) reader["inseriteManualmente"];

            return oreMese;
        }

        public bool DeleteOreCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }
        #endregion

        public void LogAccessEdilconnect(int idUtente, DateTime data, string url)
        {
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_EdilconnectAuditInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(dbCommand, "@data", DbType.DateTime, data);
                DatabaseCemi.AddInParameter(dbCommand, "@url", DbType.String, url);

                int ret = DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        #region Questionario Imprese
        public void InsertFiltroRicerca(Int32 idUtente, ConsulenteImpreseFilter filtro, Int32? pagina)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException();
            }

            XmlSerializer ser = new XmlSerializer(typeof(ConsulenteImpreseFilter));
            String filtroSerializzato = String.Empty;
            if (filtro != null)
            {
                using (StringWriter sw = new StringWriter())
                {
                    ser.Serialize(sw, filtro);
                    filtroSerializzato = sw.ToString();
                }
            }


            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseQuestionarioImpiegatiFiltriRicercaInsertUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                if (filtro != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@filtro", DbType.Xml, filtroSerializzato);
                    DatabaseCemi.AddInParameter(comando, "@pagina", DbType.Int32, pagina.Value);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Errore durante l'inserimento del filtro di ricerca");
                }
            }
        }

        public ConsulenteImpreseFilter GetFiltroRicerca(Int32 idUtente, out Int32 pagina)
        {
            ConsulenteImpreseFilter filtro = null;
            pagina = 0;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseQuestionarioImpiegatiFiltriRicercaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        #region Indici reader
                        Int32 indiceUserID = reader.GetOrdinal("idUtente");
                        Int32 indiceFiltro = reader.GetOrdinal("filtro");
                        Int32 indicePagina = reader.GetOrdinal("pagina");
                        #endregion

                        if (!reader.IsDBNull(indiceUserID))
                        {
                            String filtroSerializzato = null;

                            if (!reader.IsDBNull(indiceFiltro))
                            {
                                filtroSerializzato = reader.GetString(indiceFiltro);
                                pagina = reader.GetInt32(indicePagina);
                            }
                            if (!String.IsNullOrEmpty(filtroSerializzato))
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(ConsulenteImpreseFilter));
                                using (StringReader sr = new StringReader(filtroSerializzato))
                                {
                                    filtro = (ConsulenteImpreseFilter) ser.Deserialize(sr);
                                }
                            }
                        }
                    }
                }
            }

            return filtro;
        }

        public void DeleteFiltroRicerca(Int32 idUtente)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseQuestionarioImpiegatiFiltriRicercaDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }
        #endregion

        #region Cud
        public Cud GetCud(Int32 idLavoratore)
        {
            Cud cud = null;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_DocumentiCudSelectPresente"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        cud = new Cud();
                        cud.Anno = (Int32) reader["anno"];
                        cud.IdLavoratore = (Int32) reader["idLavoratore"];
                        if (reader["idArchidoc"] != DBNull.Value)
                        {
                            cud.IdArchidoc = (String) reader["idArchidoc"];
                        }
                    }
                }
            }

            return cud;
        }

        public Boolean CudScaricato(Int32 idUtente, Int32 annoRedditi)
        {
            Boolean inserito = false;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_DocumentiCudScaricatiInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@anno", DbType.Int32, annoRedditi);
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);

                try
                {
                    if (DatabaseCemi.ExecuteNonQuery(dbCommand) == 1)
                    {
                        inserito = true;
                    }
                }
                catch { }
            }

            return inserito;
        }
        #endregion

        public List<Int32> GetAnniTrattamentoCIGO()
        {
            List<Int32> anni = new List<Int32>();

            using (DbCommand dbCommand = DatabaseCemi.GetSqlStringCommand("SELECT DISTINCT annoProtocollo FROM dbo.Prestazioni WHERE idPrestazione = 'ATCIGO' AND Stato = 'L' AND descrizioneStatoAssegno = 'VALIDO' ORDER BY annoProtocollo DESC"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        Int32 anno = (Int32) reader[0];
                        anni.Add(anno);
                    }
                }
            }

            return anni;
        }

        public Boolean IsImpresaIrregolareBNI(Int32 idImpresa)
        {
            Boolean IrregolareBNI = false;

            using (DbCommand dbCommand = DatabaseCemi.GetSqlStringCommand("SELECT dbo.UF_BNIIsIrregolareByIdImpresa(@idImpresa)"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idimpresa", DbType.Int32, idImpresa);

                int ret = (int)DatabaseCemi.ExecuteScalar(dbCommand);
                if (ret == 0)
                    IrregolareBNI = true;
            }

            return IrregolareBNI;
        }
    }
}
