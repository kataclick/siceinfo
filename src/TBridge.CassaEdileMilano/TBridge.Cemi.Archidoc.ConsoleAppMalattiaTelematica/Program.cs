﻿using System;
using System.Configuration;
using TBridge.Cemi.Archidoc.Type.Entities;
using TBridge.Cemi.Archidoc.Type.Collection;
using TBridge.Cemi.Archidoc.Business;
using System.IO;


namespace TBridge.Cemi.Archidoc.ConsoleAppMalattiaTelematica
{
    class Program
    {
        private const string NOME_TIPO_DOCUMENTO = "Malattia Telematica";
        private static readonly Int16 giornoGiroCompleto = Int16.Parse(ConfigurationManager.AppSettings["GiornoGiroCompleto"]);
        private static readonly Int32 giorniRecupero = Int32.Parse(ConfigurationManager.AppSettings["GiorniRecupero"]);

        private static readonly DateTime recuperaDataDa = DateTime.Parse(ConfigurationManager.AppSettings["DataDa"]);
        private static readonly DateTime recuperaDataA = DateTime.Parse(ConfigurationManager.AppSettings["DataA"]);

        private static readonly String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
        private static readonly String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
        private static readonly String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
        private static readonly String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
        private static readonly String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
        private static readonly String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

        private static readonly String updateWorkFlow = (ConfigurationManager.AppSettings["UpdateWorkFlow"]);
        private static readonly String delegheWorkFlow = (ConfigurationManager.AppSettings["DelegheWorkFlow"]);

        static void Main(string[] args)
        {          
            ArchidocBusiness _biz = new ArchidocBusiness();
            DateTime _dataDa; 
            DateTime _dataA;
            DateTime _oggi = DateTime.Now;

            //Definiamo in base al file di configurazione quali sono gli estremi
            if (recuperaDataDa.Year == 1900)
            {
                _dataA = _oggi;
                _dataDa = _dataA.AddDays(-giorniRecupero);

                if ((Int16)_oggi.DayOfWeek == giornoGiroCompleto)
                {
                    _dataDa = _oggi.AddDays(-365);
                }
            }
            else
            {
                _dataA = recuperaDataA;
                _dataDa = recuperaDataDa;
            }

            _biz.InsertLog("START_MalattiaTelematica", String.Empty);

            try
            {
                ElaboraCertificatiMedici(_biz, _dataDa, _dataA);
            }
            catch (Exception ex)
            {
                _biz.InsertLog("ERROR_CertificatiMedici", ex.Message);
            }


            try
            {
                ElaboraAltriDocumenti(_biz, _dataDa, _dataA);
            }
            catch (Exception ex)
            {
                _biz.InsertLog("ERROR_AltriDocumenti", ex.Message);
            }

            _biz.InsertLog("END_MalattiaTelematica", String.Empty);
            
        }

        private static void ElaboraCertificatiMedici(ArchidocBusiness _biz, DateTime dataDa, DateTime dataA)
        {
            
            _biz.InsertLog("READ_CertificatiMedici", String.Empty);

            MalattiaTelematicaCollection documenti = _biz.GetMalattiaTelematicaCertificatiMedici(dataDa, dataA);

            if (documenti.Count > 0)
            {
                _biz.InsertLog("ARCHIDOC_INSERT_CertificatiMedici", String.Empty);

                _biz.InsertCardsWithDocumentsInArchidoc(NOME_TIPO_DOCUMENTO, documenti, true);

                _biz.InsertLog("UPDATE_CertificatiMedici", String.Empty);

                _biz.MalattiaTelematicaCertificatiMediciUpdateIdArchidoc(documenti);
            }
        }

        private static void ElaboraAltriDocumenti(ArchidocBusiness _biz, DateTime dataDa, DateTime dataA)
        {
            _biz.InsertLog("READ_AltriDocumenti", String.Empty);

            MalattiaTelematicaCollection documenti = _biz.GetMalattiaTelematicaAltriDocumenti(dataDa, dataA);

            if (documenti.Count > 0)
            {

                _biz.InsertLog("ARCHIDOC_INSERT_AltriDocumenti", String.Empty);

                _biz.InsertCardsWithDocumentsInArchidoc(NOME_TIPO_DOCUMENTO, documenti, true);

                _biz.InsertLog("UPDATE_AltriDocumenti", String.Empty);

                _biz.MalattiaTelematicaAltriDocumentiUpdateIdArchidoc(documenti);
            }
        }
    }
}
