﻿#region

using System;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Filters
{
    [Serializable]
    public class DomandaFilter
    {
        public String Indirizzo { get; set; }

        public String Comune { get; set; }

        public DateTime? Mese { get; set; }

        public Int32? IdUtente { get; set; }

        public String IvaCodFisc { get; set; }

        public String RagSocDenominazione { get; set; }
    }
}