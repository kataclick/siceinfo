﻿#region

using System;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    [Serializable]
    public class Domanda
    {
        private Impresa impresaRichiedente;
        private ImpresaCollection listaImprese;

        public Domanda()
        {
            Subappalti = new SubappaltoCollection();
        }

        public Int32? IdDomanda { get; set; }

        public String Indirizzo { get; set; }

        public String Civico { get; set; }

        public String Comune { get; set; }

        public String Provincia { get; set; }

        public String Cap { get; set; }

        public String IndirizzoCompletoCantiere
        {
            get
            {
                if (!String.IsNullOrEmpty(Provincia))
                {
                    return String.Format("{0} {1} {2} ({3})", Indirizzo, Civico, Comune, Provincia);
                }
                else
                {
                    return String.Format("{0} {1} {2}", Indirizzo, Civico, Comune);
                }
            }
        }

        public String InfoAggiuntiva { get; set; }

        public DateTime Mese { get; set; }

        public SubappaltoCollection Subappalti { get; set; }

        public DomandaImpresaCollection Lavoratori { get; set; }

        public Decimal? Latitudine { get; set; }

        public Decimal? Longitudine { get; set; }

        public Int32? IdImpresaRichiedente { get; set; }

        public Int32? IdCommittenteRichiedente { get; set; }

        public Decimal? Importo { get; set; }

        public string Descrizione { get; set; }

        public Decimal? PercentualeManodopera { get; set; }

        public Int32? NumeroLavoratori { get; set; }

        public Int32? NumeroGiorni { get; set; }

        public DateTime DataInizio { get; set; }

        public DateTime DataFine { get; set; }

        public Committente Committente { get; set; }

        public string Dia { get; set; }

        public string Concessione { get; set; }

        public string Autorizzazione { get; set; }

        public bool RichiestaVisitaIspettiva { get; set; }

        public DateTime? DataVisitaIspettiva { get; set; }

        public string OrarioVisitaIspettiva { get; set; }

        public string ContattoTelefonicoVisitaIspettiva { get; set; }

        public string MotivoVisitaIspettiva { get; set; }

        public Int32 IdUtente { get; set; }

        public String LoginUtente { get; set; }

        public DateTime? DataControllo { get; set; }

        public Boolean? AttestatoRilasciabile { get; set; }

        public Boolean? ControlloTutteImpreseAnagrafica { get; set; }

        public Boolean? ControlloTutteImpreseNoCessate { get; set; }

        public Boolean? ControlloTutteImpreseIscrittePrecedentemente { get; set; }

        public Boolean? ControlloTutteImpreseSospese { get; set; }

        public Boolean? ControlloTutteImpreseBNI { get; set; }

        public Boolean? ControlloTutteImpreseNonEdiliNoAnagrafica { get; set; }

        public Boolean? ControlloTuttiLavoratoriAnagrafica { get; set; }

        public Boolean? ControlloTuttiLavoratoriConRapporto { get; set; }

        public Boolean? ControlloTuttiLavoratoriOreLavorate { get; set; }

        public Boolean? ControlloTuttiLavoratoriPartTime { get; set; }

        public Boolean? ControlloAttestatoRilasciabile { get; set; }

        public Boolean? SegnalazioneCantiereInNotifiche { get; set; }

        public Guid GuidId { get; set; }

        public Impresa ImpresaRichiedente
        {
            get { return impresaRichiedente; }
            set { impresaRichiedente = value; }
        }

        public Committente CommittenteRichiedente { get; set; }

        public ImpresaCollection ListaImprese
        {
            get { return listaImprese; }
            set { listaImprese = value; }
        }
    }
}