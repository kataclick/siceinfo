#region

using System;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    [Serializable]
    public class Cantiere
    {
        #region ProprietÓ

        private string comune;
        private string indirizzo;
        private string provincia;

        public int? IdCantiere { set; get; }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap { get; set; }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string IndirizzoMappa
        {
            get { return indirizzo + " " + comune + " " + provincia; }
        }

        public double? Latitudine { get; set; }

        public double? Longitudine { get; set; }

        #endregion

        #region Costruttori

        /// <summary>
        /// Costruttore per la serializzazione
        /// </summary>
        public Cantiere()
        {
        }

        public Cantiere(int? idCantiere, string provincia, string comune, string cap,
                        string indirizzo)
        {
            IdCantiere = idCantiere;
            this.provincia = provincia;
            this.comune = comune;
            Cap = cap;
            this.indirizzo = indirizzo;
        }

        #endregion
    }
}