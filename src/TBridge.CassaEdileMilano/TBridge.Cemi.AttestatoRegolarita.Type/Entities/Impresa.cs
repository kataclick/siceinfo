#region

using System;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    [Serializable]
    public class Impresa
    {
        private const string SEPARATORE_CAMPO_COMPOSTO = "|";
        private string ammiCap;
        private string ammiComune;
        private string ammiIndirizzo;
        private string ammiProvincia;
        private string cap;
        private string comune;
        private string indirizzo;
        private string provincia;
        private TipologiaImpresa tipoImpresa;

        public Impresa()
        {
        }

        //tipoImpresa = 0 se presa da SiceNEW
        public Impresa(int? idImpresa, string ragioneSociale, string indirizzo,
                       string provincia, string comune, string cap, string partitaIva, string codiceFiscale,
                       TipologiaImpresa tipoImpresa, Guid? idTemporaneo, string idCassaEdileProvenienza)
        {
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            PartitaIva = partitaIva;
            CodiceFiscale = codiceFiscale;
            IdTemporaneo = idTemporaneo;
            this.tipoImpresa = tipoImpresa;
            IdCassaEdileProvenienza = idCassaEdileProvenienza;
        }

        public string CodiceFiscale { get; set; }

        public string AmmiIndirizzo
        {
            get { return ammiIndirizzo; }
            set { ammiIndirizzo = value; }
        }

        public string AmmiProvincia
        {
            get { return ammiProvincia; }
            set { ammiProvincia = value; }
        }

        public string AmmiComune
        {
            get { return ammiComune; }
            set { ammiComune = value; }
        }

        public string AmmiCap
        {
            get { return ammiCap; }
            set { ammiCap = value; }
        }

        public int? IdImpresa { get; set; }

        public Guid? IdTemporaneo { get; set; }

        public string RagioneSociale { get; set; }

        public String CodiceERagioneSociale
        {
            get
            {
                if (tipoImpresa == TipologiaImpresa.Nuova)
                {
                    return RagioneSociale;
                }
                else
                {
                    return String.Format("{0} - {1}", IdImpresa, RagioneSociale);
                }
            }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }


        public string PartitaIva { get; set; }

        public string AmmiIndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(ammiProvincia))
                    return String.Format("{0} {1} ({2}) {3}", ammiIndirizzo, ammiComune, ammiProvincia, ammiCap);
                else return String.Format("{0} {1} {2}", ammiIndirizzo, ammiComune, ammiCap);
            }
        }

        public string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(provincia))
                    return String.Format("{0} {1} ({2}) {3}", indirizzo, comune, provincia, cap);
                else return String.Format("{0} {1} {2}", indirizzo, comune, cap);
            }
        }

        public string NomeCompleto
        {
            get
            {
                return
                    RagioneSociale + Environment.NewLine + Indirizzo + Environment.NewLine + Comune + " - " + Provincia +
                    " " + Cap;
            }
        }

        /// <summary>
        /// Ritorna TipoImpresa + SEPARATORE + IdImpresa; per conoscere il separatore accedere alla proprietÓ SeparatoreCampoComposto
        /// </summary>
        public string IdImpresaComposto
        {
            //get { return ((int) TipoImpresa) + SEPARATORE_CAMPO_COMPOSTO + IdImpresa; }
            get { return String.Format("{0}|{1}|{2}", (Int32) TipoImpresa, IdImpresa, IdTemporaneo); }
        }

        /// <summary>
        /// Ritorna il valore del separatore usato nei campi composti (es IdImpresaComposto)
        /// </summary>
        public static string SeparatoreCampoComposto
        {
            get { return SEPARATORE_CAMPO_COMPOSTO; }
        }

        public string IdCassaEdileProvenienza { get; set; }

        public TipologiaContratto TipologiaContratto { get; set; }

        public TipologiaImpresa TipoImpresa
        {
            get { return tipoImpresa; }
            set { tipoImpresa = value; }
        }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string NumeroCameraCommercio { get; set; }

        public DateTime? DataNascita { get; set; }

        public bool LavoratoreAutonomo { get; set; }

        public Boolean? ControlloCessataAttivita { get; set; }

        public Boolean? ControlloBNI { get; set; }

        public Boolean? ControlloIscrittaPrecedentemente { get; set; }

        public Boolean? ControlloNonEdileNoAnagrafica { get; set; }

        public Boolean? ControlloSospesa { get; set; }

        public Int32? IdImpresaTrovata { get; set; }

        public Boolean ImpresaAssociata
        {
            get
            {
                if (TipoImpresa == TipologiaImpresa.SiceNew
                    ||
                    IdImpresaTrovata.HasValue
                    )
                {
                    return true;
                }

                return false;
            }
        }

        #region Controlli invertiti

        public Boolean ImpresaAssociataNegativo
        {
            get { return !ImpresaAssociata; }
        }

        public Boolean? ControlloCessataAttivitaNegativo
        {
            get { return !ControlloCessataAttivita; }
        }

        public Boolean? ControlloIscrittaPrecedentementeNegativo
        {
            get { return !ControlloIscrittaPrecedentemente; }
        }

        public Boolean? ControlloBNINegativo
        {
            get { return !ControlloBNI; }
        }

        public Boolean? ControlloNonEdileNoAnagraficaNegativo
        {
            get { return !ControlloNonEdileNoAnagrafica; }
        }

        public Boolean? ControlloSospesaNegativo
        {
            get { return !ControlloSospesa; }
        }

        #endregion

        /// <summary>
        /// Separa l'idcomposto
        /// </summary>
        /// <param name="idImpresaComposto">Id Impresa composto</param>
        /// <param name="idImpresa">valorizza idimpresa</param>
        /// <param name="tipoImpresa">valorizza tipoimpresa</param>
        public static void SplitIdImpresaComposto(string idImpresaComposto, out int idImpresa,
                                                  out TipologiaImpresa tipoImpresa)
        {
            string[] impresaIds = idImpresaComposto.Split(SeparatoreCampoComposto.ToCharArray());
            idImpresa = int.Parse(impresaIds[1]);
            tipoImpresa = (TipologiaImpresa) Int32.Parse(impresaIds[0]);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof (Impresa))
                return base.Equals(obj);
            else
            {
                Impresa impresa = (Impresa) obj;

                if (impresa.TipoImpresa == TipoImpresa
                    && impresa.IdImpresa == IdImpresa
                    )
                {
                    return true;
                }
                else
                {
                    if (impresa.TipoImpresa == TipologiaImpresa.Nuova
                        && TipoImpresa == TipologiaImpresa.Nuova
                        && !impresa.IdImpresa.HasValue
                        && !IdImpresa.HasValue
                        && IdTemporaneo == impresa.IdTemporaneo)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public override string ToString()
        {
            if (TipoImpresa == TipologiaImpresa.SiceNew)
            {
                return String.Format("{0} {1}", IdImpresa, RagioneSociale);
            }
            else
            {
                return RagioneSociale;
            }
        }
    }
}