using System;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    [Serializable]
    public class Subappalto
    {
        public Subappalto()
        {
        }

        public Subappalto(int? idSubappalto, int idCantiere, Impresa appaltante, Impresa appaltata,
                          TipologiaContratto tipoContratto)
        {
            IdSubappalto = idSubappalto;
            IdCantiere = idCantiere;
            Appaltante = appaltante;
            Appaltata = appaltata;
            TipoContratto = tipoContratto;
        }

        public int? IdSubappalto { get; set; }

        public int IdCantiere { get; set; }

        public Impresa Appaltante { get; set; }

        public Impresa Appaltata { get; set; }

        public TipologiaContratto TipoContratto { get; set; }

        #region ProprietÓ di appoggio per il Binding dei dati, solo lettura

        public string NomeAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    switch (Appaltante.TipoImpresa)
                    {
                        case TipologiaImpresa.SiceNew:
                            return String.Format("{0} - {1}", Appaltante.IdImpresa, Appaltante.RagioneSociale);
                        case TipologiaImpresa.Nuova:
                            return Appaltante.RagioneSociale;
                        default:
                            return String.Empty;
                    }
                }
                else return String.Empty;
            }
        }

        public string IndirizzoAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    return Appaltante.IndirizzoCompleto;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string CodiceFiscaleAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    return Appaltante.CodiceFiscale;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string PartitaIvaAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    return Appaltante.PartitaIva;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string NomeAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    switch (Appaltata.TipoImpresa)
                    {
                        case TipologiaImpresa.SiceNew:
                            return String.Format("{0} - {1}", Appaltata.IdImpresa, Appaltata.RagioneSociale);
                        case TipologiaImpresa.Nuova:
                            return Appaltata.RagioneSociale;
                        default:
                            return String.Empty;
                    }
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        public string IndirizzoAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    return Appaltata.IndirizzoCompleto;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string CodiceFiscaleAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    return Appaltata.CodiceFiscale;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string PartitaIvaAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    return Appaltata.PartitaIva;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        #endregion
    }
}