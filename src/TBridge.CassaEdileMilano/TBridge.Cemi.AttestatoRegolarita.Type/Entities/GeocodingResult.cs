namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    public class GeocodingResult
    {
        private int geoStatusCode;

        public bool Result { get; set; }

        public int GeoStatusCode
        {
            get { return geoStatusCode; }
            set { geoStatusCode = value; }
        }

        public string GeoStatusCodeResult
        {
            get
            {
                switch (geoStatusCode)
                {
                    case 200:
                        return "Success";
                    case 500:
                        return "Server Error";
                    case 601:
                        return "Missing Address";
                    case 602:
                        return "Unknown Address";
                    case 603:
                        return "Unavailable Address";
                    case 610:
                        return "Bad Key";
                    default:
                        return "Unknown status";
                }
            }
        }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string Regione { get; set; }

        public string Civico { get; set; }
    }
}