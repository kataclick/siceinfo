#region

using System;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    [Serializable]
    public class Lavoratore
    {
        private Int32? idLavoratoreTrovato;

        public Lavoratore()
        {
        }

        public Lavoratore( /*TipologiaLavoratore tipoLavoratore,*/
            int? idLavoratore, string cognome, string nome, DateTime? dataNascita, string codiceFiscale,
            DateTime? dataAssunzione, DateTime? dataCessazione)
        {
            //this.TipoLavoratore = tipoLavoratore;
            IdLavoratore = idLavoratore;
            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            CodiceFiscale = codiceFiscale;
            DataAssunzione = dataAssunzione;
            DataCessazione = dataCessazione;
        }

        public TipologiaLavoratore TipoLavoratore { get; set; }

        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string Impresa { get; set; }

        public string CodiceFiscale { get; set; }

        public DateTime? DataAssunzione { get; set; }

        public DateTime? DataCessazione { get; set; }

        public Int32? IdDomandaLavoratore { get; set; }

        public Int32? TipoAssunzione { get; set; }

        public String IdCassaEdileProvenienza { get; set; }

        public Boolean? ControlloOreLavorate { get; set; }

        public Boolean? ControlloPartTime { get; set; }

        public Boolean? ControlloPresenzaRapporto { get; set; }

        public Int32? IdLavoratoreTrovato
        {
            get { return idLavoratoreTrovato; }
            set { idLavoratoreTrovato = value; }
        }

        public Boolean LavoratoreAssociato
        {
            get
            {
                if (TipoLavoratore == TipologiaLavoratore.SiceNew
                    ||
                    idLavoratoreTrovato.HasValue
                    )
                {
                    return true;
                }

                return false;
            }
        }

        public TipologiaContratto TipoContrattoImpresa { get; set; }

        public Int32? IdImpresa { get; set; }

        public String CodiceERagioneSocialeImpresa
        {
            get
            {
                if (IdImpresa.HasValue)
                {
                    return String.Format("{0} - {1}", IdImpresa, Impresa);
                }
                else
                {
                    return Impresa;
                }
            }
        }

        #region Controlli invertiti

        public Boolean LavoratoreAssociatoNegativo
        {
            get { return !LavoratoreAssociato; }
        }

        public Boolean? ControlloOreLavorateNegativo
        {
            get { return !ControlloOreLavorate; }
        }

        public Boolean? ControlloPartTimeNegativo
        {
            get { return !ControlloPartTime; }
        }

        public Boolean? ControlloPresenzaRapportoNegativo
        {
            get { return !ControlloPresenzaRapporto; }
        }

        #endregion
    }
}