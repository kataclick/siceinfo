﻿#region

using System;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Entities
{
    [Serializable]
    public class DomandaImpresa
    {
        public DomandaImpresa()
        {
            Lavoratori = new LavoratoreCollection();
        }

        public Impresa Impresa { get; set; }

        public int IdDomanda { get; set; }

        public LavoratoreCollection Lavoratori { get; set; }
    }
}