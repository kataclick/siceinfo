#region

using System;
using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    /// <summary>
    /// Collezione di indirizzi
    /// </summary>
    [Serializable]
    public class IndirizzoCollection : List<Indirizzo>
    {
        public void AddUnico(Indirizzo indirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == indirizzo.IdIndirizzo)
                    return;
            }

            base.Add(indirizzo);
        }
    }
}