﻿#region

using System;
using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    [Serializable]
    public class DomandaImpresaCollection : List<DomandaImpresa>
    {
        public void AggiungiLavoratoreAImpresa(Int32? idImpresa, Int32? idAttestatoImpresa, Lavoratore lavoratore)
        {
            foreach (DomandaImpresa domImp in this)
            {
                if (
                    (idImpresa.HasValue && domImp.Impresa.TipoImpresa == TipologiaImpresa.SiceNew &&
                     domImp.Impresa.IdImpresa == idImpresa)
                    ||
                    (idAttestatoImpresa.HasValue && domImp.Impresa.TipoImpresa == TipologiaImpresa.Nuova &&
                     domImp.Impresa.IdImpresa == idImpresa)
                    )
                {
                    domImp.Lavoratori.Add(lavoratore);
                    break;
                }
            }
        }

        public DomandaImpresa TrovaLavoratoriDellImpresa(TipologiaImpresa tipoImpresa, Int32 idImpresa)
        {
            foreach (DomandaImpresa domImp in this)
            {
                if (domImp.Impresa.TipoImpresa == tipoImpresa
                    && domImp.Impresa.IdImpresa.Value == idImpresa)
                {
                    return domImp;
                }
            }

            return null;
        }

        public DomandaImpresa SelezionaDomandaImpresa(TipologiaImpresa tipoImpresa, int? idImpresa, Guid? idTemporaneo)
        {
            Impresa imp = new Impresa();
            imp.TipoImpresa = tipoImpresa;
            imp.IdImpresa = idImpresa;
            imp.IdTemporaneo = idTemporaneo;

            foreach (DomandaImpresa domImp in this)
            {
                if (domImp.Impresa.Equals(imp))
                {
                    return domImp;
                }
            }

            return null;
        }

        /// <summary>
        /// Controlla la presenza della domanda nella lista; l'uguaglianza si basa su idImpresa + idDomanda
        /// </summary>
        /// <param name="domandaImpresa"></param>
        /// <returns></returns>
        public new bool Contains(DomandaImpresa domandaImpresa)
        {
            foreach (DomandaImpresa domImp in this)
            {
                //controlliamo se la domanda è presente
                if ((domImp.Impresa.IdImpresa.HasValue && domandaImpresa.Impresa.IdImpresa.HasValue &&
                     domImp.Impresa.IdImpresa == domandaImpresa.Impresa.IdImpresa)
                    &&
                    (domImp.IdDomanda == domandaImpresa.IdDomanda))

                    return true;
            }
            return false;
        }


        public void Cancella(DomandaImpresa domandaImpresa)
        {
            //foreach (DomandaImpresa domImp in this)
            //{ 
            //    if ((domImp.Impresa.IdImpresa.HasValue && domandaImpresa.Impresa.IdImpresa.HasValue && domImp.Impresa.IdImpresa == domandaImpresa.Impresa.IdImpresa)
            //        &&
            //        (domImp.IdDomanda == domandaImpresa.IdDomanda))

            //        Remove(domImp);
            //}

            DomandaImpresa domImp;

            for (int i = 0; i < Count; i++)
            {
                domImp = this[i];

                if ((domImp.Impresa.IdImpresa.HasValue && domandaImpresa.Impresa.IdImpresa.HasValue &&
                     domImp.Impresa.IdImpresa == domandaImpresa.Impresa.IdImpresa)
                    &&
                    (domImp.IdDomanda == domandaImpresa.IdDomanda))

                    Remove(domImp);
            }
        }
    }
}