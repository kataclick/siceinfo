#region

using System;
using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    [Serializable]
    public class LavoratoreCollection : List<Lavoratore>
    {
        /// <summary>
        /// Controlla la presenza del lavoratore nella lista; l'uguaglianza si basa su idLavoratore 
        /// </summary>
        /// <param name="lavoratore"></param>
        /// <returns></returns>
        public new bool Contains(Lavoratore lavoratore)
        {
            foreach (Lavoratore lav in this)
            {
                //controlliamo se il lavoratore � presente
                if (lav.IdLavoratore.HasValue && lavoratore.IdLavoratore.HasValue &&
                    lav.IdLavoratore == lavoratore.IdLavoratore)
                    return true;
            }
            return false;
        }


        public bool ContainsCodFisc(Lavoratore lavoratore)
        {
            foreach (Lavoratore lav in this)
            {
                //controlliamo se il lavoratore con il dato codice fiscale � presente
                try
                {
                    if (lav.CodiceFiscale == lavoratore.CodiceFiscale)
                        return true;
                }
                catch
                {
                }
            }
            return false;
        }
    }
}