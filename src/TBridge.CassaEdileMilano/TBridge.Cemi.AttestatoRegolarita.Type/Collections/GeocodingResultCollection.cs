#region

using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    public class GeocodingResultCollection : List<GeocodingResult>
    {
    }
}