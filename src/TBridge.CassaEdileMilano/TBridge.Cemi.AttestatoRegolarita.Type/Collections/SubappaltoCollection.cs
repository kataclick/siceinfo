#region

using System;
using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    [Serializable]
    // Valla: usare una struttura ad albero, eliminare tutta questa roba
    public class SubappaltoCollection : List<Subappalto>
    {
        public Boolean Contains(Impresa appaltante, Impresa appaltata)
        {
            foreach (Subappalto sub in this)
            {
                if ((sub.Appaltante != null) && (sub.Appaltata != null))
                {
                    if ((sub.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew) &&
                        (sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew))
                    {
                        if (sub.Appaltante.IdImpresa == appaltante.IdImpresa &&
                            sub.Appaltata.IdImpresa == appaltata.IdImpresa)
                        {
                            return true;
                        }
                    }

                    if ((sub.Appaltante.TipoImpresa == TipologiaImpresa.Nuova) &&
                        (sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova))
                    {
                        if ((sub.Appaltante.PartitaIva == appaltante.PartitaIva ||
                             sub.Appaltante.CodiceFiscale == appaltante.CodiceFiscale)
                            &&
                            sub.Appaltata.PartitaIva == appaltata.PartitaIva ||
                            sub.Appaltata.CodiceFiscale == appaltata.CodiceFiscale)
                        {
                            return true;
                        }
                    }

                    if ((sub.Appaltante.TipoImpresa == TipologiaImpresa.Nuova) &&
                        (sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew))
                    {
                        if ((sub.Appaltante.PartitaIva == appaltante.PartitaIva ||
                             sub.Appaltante.CodiceFiscale == appaltante.CodiceFiscale)
                            &&
                            sub.Appaltata.IdImpresa == appaltata.IdImpresa)
                        {
                            return true;
                        }
                    }

                    if ((sub.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew) &&
                        (sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova))
                    {
                        if (sub.Appaltante.IdImpresa == appaltante.IdImpresa
                            &&
                            (sub.Appaltata.PartitaIva == appaltata.PartitaIva ||
                             sub.Appaltata.CodiceFiscale == appaltata.CodiceFiscale))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public Boolean ContainsAppaltata(Impresa appaltata)
        {
            foreach (Subappalto sub in this)
            {
                if (sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    if (sub.Appaltata.IdImpresa == appaltata.IdImpresa)
                    {
                        return true;
                    }
                }

                if (sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                {
                    if (sub.Appaltata.PartitaIva == appaltata.PartitaIva
                        ||
                        sub.Appaltata.CodiceFiscale == appaltata.CodiceFiscale)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}