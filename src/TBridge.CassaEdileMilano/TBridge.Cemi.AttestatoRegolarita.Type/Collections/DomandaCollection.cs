﻿#region

using System;
using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    [Serializable]
    public class DomandaCollection : List<Domanda>
    {
    }
}