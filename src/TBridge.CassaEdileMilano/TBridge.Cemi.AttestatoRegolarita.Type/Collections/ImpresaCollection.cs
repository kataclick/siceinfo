#region

using System;
using System.Collections.Generic;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Collections
{
    [Serializable]
    public class ImpresaCollection : List<Impresa>
    {
        /// <summary>
        /// Controlla la presenza dell'impresa nella lista; l'uguaglianza si basa su idImpresa 
        /// </summary>
        /// <param name="impresa"></param>
        /// <returns></returns>
        public new bool Contains(Impresa impresa)
        {
            foreach (Impresa imp in this)
            {
                //controlliamo se l'impresa � presente
                if (imp.TipoImpresa == TipologiaImpresa.SiceNew && impresa.TipoImpresa == TipologiaImpresa.SiceNew
                    && imp.IdImpresa == impresa.IdImpresa)
                {
                    return true;
                }

                if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                    && imp.IdImpresa.HasValue && impresa.IdImpresa.HasValue
                    && imp.IdImpresa == impresa.IdImpresa)
                {
                    return true;
                }

                if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                    && !imp.IdImpresa.HasValue && !impresa.IdImpresa.HasValue
                    && imp.IdTemporaneo == impresa.IdTemporaneo)
                {
                    return true;
                }
            }

            return false;
        }


        public Impresa Select(Impresa impresa)
        {
            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            else
            {
                foreach (Impresa imp in this)
                {
                    //controlliamo se l'impresa � presente
                    if (imp.TipoImpresa == TipologiaImpresa.SiceNew && impresa.TipoImpresa == TipologiaImpresa.SiceNew
                        && imp.IdImpresa == impresa.IdImpresa)
                    {
                        return imp;
                    }

                    if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                        && imp.IdImpresa.HasValue && impresa.IdImpresa.HasValue
                        && imp.IdImpresa == impresa.IdImpresa)
                    {
                        return imp;
                    }

                    if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                        && !imp.IdImpresa.HasValue && !impresa.IdImpresa.HasValue
                        && imp.IdTemporaneo == impresa.IdTemporaneo)
                    {
                        return imp;
                    }
                }
            }

            return null;
        }

        //public Impresa Select(int idImpresa)
        //{
        //    foreach (Impresa imp in this)
        //    {
        //        if (imp.IdImpresa.HasValue && imp.IdImpresa == idImpresa)
        //            return imp;
        //    }
        //    return null;
        //}

        //public Impresa Select(string ragioneSociale)
        //{
        //    foreach (Impresa imp in this)
        //    {
        //        if (imp.RagioneSociale == ragioneSociale)
        //            return imp;
        //    }
        //    return null;
        //}
    }
}