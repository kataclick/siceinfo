namespace TBridge.Cemi.AttestatoRegolarita.Type.Enums
{
    public enum TipologiaAppalto
    {
        NonDefinito = 0,
        Pubblico,
        Privato
    }
}