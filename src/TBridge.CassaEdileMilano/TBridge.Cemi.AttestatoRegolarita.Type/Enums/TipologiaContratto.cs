namespace TBridge.Cemi.AttestatoRegolarita.Type.Enums
{
    public enum TipologiaContratto
    {
        ContrattoEdile = 0,
        Agricoltura,
        AlimentaristiAgroindustriale,
        AltriVari,
        AmministrazionePubblica,
        AziendeServizi,
        Chimici,
        Commercio,
        CreditoAssicurazioni,
        EntiIstituzioniProvate,
        Meccanici,
        PoligraficiSpettacolo,
        Tessili,
        Trasporti
    }
}