﻿namespace TBridge.Cemi.AttestatoRegolarita.Type.Enums
{
    public enum TipologiaCommittente
    {
        Pubblico = 0,
        Privato
    }
}