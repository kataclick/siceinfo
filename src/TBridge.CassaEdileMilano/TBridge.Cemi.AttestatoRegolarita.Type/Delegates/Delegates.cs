#region

using TBridge.Cemi.AttestatoRegolarita.Type.Entities;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Type.Delegates
{
    public delegate void CantieriSelectedEventHandler(Cantiere cantiere);

    public delegate void ImpreseSelectedEventHandler(Impresa impresa);

    public delegate void LavoratoriSelectedEventHandler(Lavoratore lavoratore);
}