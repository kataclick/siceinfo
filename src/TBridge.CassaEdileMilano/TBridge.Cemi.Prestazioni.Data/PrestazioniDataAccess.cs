using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Enums;
using TBridge.Cemi.Prestazioni.Type.Exceptions;
using TBridge.Cemi.Prestazioni.Type.Filters;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using Indirizzo = TBridge.Cemi.Prestazioni.Type.Entities.Indirizzo;

namespace TBridge.Cemi.Prestazioni.Data
{
    public class PrestazioniDataAccess
    {
        private Database databaseCemi;

        public PrestazioniDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get
            {
                return databaseCemi;
            }
            set
            {
                databaseCemi = value;
            }
        }

        public bool IsLavoratoreInAnagrafica(Lavoratore lavoratore)
        {
            bool res;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniControlloLavoratoreInAnagrafica"))
            {
                databaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);
                databaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                databaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                databaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                databaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);
                databaseCemi.AddOutParameter(comando, "@numeroLavoratori", DbType.Int32, 4);
                databaseCemi.AddOutParameter(comando, "@idTipoPagamento", DbType.String, 1);

                databaseCemi.ExecuteNonQuery(comando);

                object idLavoratoreObject = databaseCemi.GetParameterValue(comando, "@idLavoratore");
                int righe = (int) databaseCemi.GetParameterValue(comando, "@numeroLavoratori");
                if (righe == 1 && !Convert.IsDBNull(idLavoratoreObject))
                {
                    lavoratore.IdLavoratore = (int) idLavoratoreObject;

                    object idTipoPagamentoObj = databaseCemi.GetParameterValue(comando, "@idTipoPagamento");
                    if (!Convert.IsDBNull(idTipoPagamentoObj))
                    {
                        lavoratore.IdTipoPagamento = (string) idTipoPagamentoObj;
                    }

                    res = true;
                }
                else
                {
                    res = false;
                }
            }

            return res;
        }

        public TipoPrestazioneCollection GetTipiPrestazionePerGradoParentela(string gradoParentela, bool soloAttive)
        {
            TipoPrestazioneCollection tipiPrestazione = new TipoPrestazioneCollection();

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniConfigurazioneSelectPerGrado")
                )
            {
                databaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, gradoParentela);
                databaseCemi.AddInParameter(comando, "@soloAttive", DbType.Boolean, soloAttive);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        TipoPrestazione tipoPrestazione = new TipoPrestazione();
                        tipiPrestazione.Add(tipoPrestazione);

                        tipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        tipoPrestazione.Descrizione = (string) reader["descrizione"];

                        //leggo anche il periodo di validit�
                        if (!Convert.IsDBNull(reader["periodoInserimentoInizio"]))
                            tipoPrestazione.ValidaDa = (DateTime) reader["periodoInserimentoInizio"];
                        if (!Convert.IsDBNull(reader["periodoInserimentoFine"]))
                            tipoPrestazione.ValidaA = (DateTime) reader["periodoInserimentoFine"];

                        tipoPrestazione.GradoParentela = gradoParentela;
                    }
                }
            }

            return tipiPrestazione;
        }

        public TipoPrestazioneCollection GetTipiPrestazionePerGradoParentela(string gradoParentela)
        {
            return GetTipiPrestazionePerGradoParentela(gradoParentela, true);
        }

        public TipoPrestazioneCollection GetTipiPrestazione()
        {
            TipoPrestazioneCollection tipiPrestazione = new TipoPrestazioneCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_TipiPrestazioneSelect"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici
                    Int32 indiceIdTipoPrestazione = reader.GetOrdinal("idTipoPrestazione");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");
                    Int32 indiceIdGruppo = reader.GetOrdinal("prestazioniGruppoId");
                    Int32 indiceDescrizioneGruppo = reader.GetOrdinal("prestazioniGruppoDescrizione");
                    #endregion

                    while (reader.Read())
                    {
                        TipoPrestazione tipoPrestazione = new TipoPrestazione();
                        tipiPrestazione.Add(tipoPrestazione);

                        tipoPrestazione.IdTipoPrestazione = reader.GetString(indiceIdTipoPrestazione);
                        tipoPrestazione.Descrizione = reader.GetString(indiceDescrizione);

                        tipoPrestazione.Gruppo = new Gruppo();
                        tipoPrestazione.Gruppo.Codice = reader.GetInt32(indiceIdGruppo);
                        tipoPrestazione.Gruppo.Descrizione = reader.GetString(indiceDescrizioneGruppo);
                    }
                }
            }

            return tipiPrestazione;
        }

        /// <summary>
        ///   Recupera i familiari del lavoratore filtrando per grado parentela
        /// </summary>
        /// <param name = "idLavoratore">id del lavoratore</param>
        /// <param name = "gradoParentela">Se grado parentela � null o empty non effettua filtro sul grado</param>
        /// <returns>lista dei familiari</returns>
        public FamiliareCollection GetFamiliariPerGradoParentela(int idLavoratore, string gradoParentela, bool conDataNascita)
        {
            FamiliareCollection familiari = new FamiliareCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFamiliariSelectPerGradoParentela"))
            {
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                if (!string.IsNullOrEmpty(gradoParentela))
                    databaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, gradoParentela);
                databaseCemi.AddInParameter(comando, "@conDataNascita", DbType.Boolean, conDataNascita);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Familiare familiare = RecuperaFamiliareDaReader(reader);
                        familiari.Add(familiare);
                    }
                }
            }

            return familiari;
        }

        public Familiare GetFamiliare(int idLavoratore, int idFamiliare)
        {
            Familiare familiare;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFamiliariSelectPerChiave"))
            {
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                databaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    familiare = RecuperaFamiliareDaReader(reader);
                }
            }

            return familiare;
        }

        private static Familiare RecuperaFamiliareDaReader(IDataReader reader)
        {
            Familiare familiare = new Familiare();

            familiare.IdLavoratore = (int) reader["idLavoratore"];
            familiare.IdFamiliare = (int) reader["idFamiliare"];
            familiare.Cognome = (string) reader["cognome"];
            familiare.Nome = (string) reader["nome"];
            if (!Convert.IsDBNull(reader["dataNascita"]))
                familiare.DataNascita = (DateTime) reader["dataNascita"];
            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                familiare.CodiceFiscale = (string) reader["codiceFiscale"];
            if (!Convert.IsDBNull(reader["gradoParentela"]))
                familiare.GradoParentela = (string) reader["gradoParentela"];
            if (!Convert.IsDBNull(reader["sesso"]))
                familiare.Sesso = ((string) reader["sesso"])[0];
            if (!Convert.IsDBNull(reader["segnaleSoggettoACarico"]))
                familiare.ACarico = (String) reader["segnaleSoggettoACarico"];

            return familiare;
        }

        public Configurazione GetConfigurazionePrestazione(string idTipoPrestazione, string beneficiario,
                                                           int idLavoratore)
        {
            Configurazione configurazione = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniConfigurazioneSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);
                databaseCemi.AddInParameter(comando, "@beneficiario", DbType.String, beneficiario);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        configurazione = new Configurazione();

                        configurazione.IdTipoPrestazione = idTipoPrestazione;
                        configurazione.GradoParentela = beneficiario;
                        configurazione.Reciprocita123 = (bool) reader["reciprocita123"];
                        configurazione.Reciprocita45 = (bool) reader["reciprocita45"];
                        configurazione.UnivocitaAnnuale = (bool) reader["univocitaAnnuale"];
                        configurazione.UnivocitaFamiliare = (bool) reader["univocitaFamiliare"];
                        configurazione.PeriodoRiferimentoInizio = (DateTime) reader["periodoRiferimentoInizio"];
                        configurazione.PeriodoRiferimentoFine = (DateTime) reader["periodoRiferimentoFine"];
                        configurazione.DifferenzaMesiFatturaDomanda = ((byte) reader["differenzaMesiFatturaDomanda"]);
                        configurazione.RichiestaFattura = (bool) reader["richiestaFattura"];
                        configurazione.RichiestaTipoScuola = (bool) reader["richiestaTipoScuola"];

                        configurazione.TipoMacroPrestazione = new TipoMacroPrestazione();
                        configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione =
                            (Int16) reader["idTipoMacroPrestazione"];
                        configurazione.TipoMacroPrestazione.Descrizione =
                            (String) reader["tipoMacroPrestazioneDescrizione"];

                        configurazione.TipoModulo = new TipoModulo();
                        configurazione.TipoModulo.IdTipoModulo = (Int16) reader["idTipoModulo"];
                        configurazione.TipoModulo.Descrizione = (String) reader["tipoModuloDescrizione"];
                        configurazione.TipoModulo.Modulo = (String) reader["modulo"];
                    }
                }
            }

            return configurazione;
        }

        public bool IsFamiliareInAnagrafica(Familiare familiare)
        {
            bool res;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniControlloFamiliareInAnagrafica"))
            {
                databaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, familiare.CodiceFiscale);
                databaseCemi.AddInParameter(comando, "@cognome", DbType.String, familiare.Cognome);
                databaseCemi.AddInParameter(comando, "@nome", DbType.String, familiare.Nome);
                databaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, familiare.DataNascita);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, familiare.IdLavoratore);
                databaseCemi.AddOutParameter(comando, "@idFamiliare", DbType.Int32, 4);
                databaseCemi.AddOutParameter(comando, "@numeroFamiliari", DbType.Int32, 4);

                databaseCemi.ExecuteNonQuery(comando);

                object idFamiliareObject = databaseCemi.GetParameterValue(comando, "@idFamiliare");
                int righe = (int) databaseCemi.GetParameterValue(comando, "@numeroFamiliari");
                if (righe == 1 && !Convert.IsDBNull(idFamiliareObject))
                {
                    familiare.IdFamiliare = (int) idFamiliareObject;
                    res = true;
                }
                else
                {
                    res = false;
                }
            }

            return res;
        }

        public bool InsertDomanda(Domanda domanda, int? idDomandaTemporanea, int idUtente, Boolean forzaNumeroProtocollo)
        {
            bool res;

            using (DbConnection connection = databaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (idDomandaTemporanea.HasValue
                            && !DeleteDomandaTemporanea(idDomandaTemporanea.Value, transaction))
                            throw new Exception("Inserimento domanda, fallita la cancellazione della domanda temporanea");

                        using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeInsert")
                            )
                        {
                            // Mettiamo come anno protocollo e numero quelli memorizzati
                            if (forzaNumeroProtocollo)
                            {
                                if (domanda.ProtocolloPrestazione <= 0)
                                    throw new ArgumentException("Valorizzare il protocollo prestazione", "domanda.ProtocolloPrestazione");
                                if (domanda.NumeroProtocolloPrestazione <= 0)
                                    throw new ArgumentException("Valorizzare il numero protocollo prestazione", "domanda.NumeroProtocolloPrestazione");

                                databaseCemi.AddInParameter(comando, "@protocolloPrestazione", DbType.Int16, domanda.ProtocolloPrestazione);
                                databaseCemi.AddInParameter(comando, "@numeroProtocolloPrestazione", DbType.Int32, domanda.NumeroProtocolloPrestazione);
                            }

                            databaseCemi.AddInParameter(comando, "@guid", DbType.Guid,
                                                        domanda.Guid);

                            databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String,
                                                        domanda.TipoPrestazione.IdTipoPrestazione);
                            databaseCemi.AddInParameter(comando, "@beneficiario", DbType.String, domanda.Beneficiario);
                            databaseCemi.AddInParameter(comando, "@stato", DbType.String, domanda.Stato.IdStato);
                            databaseCemi.AddInParameter(comando, "@dataRiferimento", DbType.Date,
                                                        domanda.DataRiferimento);
                            databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                        domanda.Lavoratore.IdLavoratore);

                            if (domanda.Lavoratore.Indirizzo != null)
                            {
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.IndirizzoVia))
                                    databaseCemi.AddInParameter(comando, "@lavoratoreIndirizzo", DbType.String,
                                                                domanda.Lavoratore.Indirizzo.IndirizzoVia);
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Provincia))
                                    databaseCemi.AddInParameter(comando, "@lavoratoreProvincia", DbType.String,
                                                                domanda.Lavoratore.Indirizzo.Provincia);
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Comune))
                                    databaseCemi.AddInParameter(comando, "@lavoratoreComuneCodiceCatastale",
                                                                DbType.String,
                                                                domanda.Lavoratore.Indirizzo.Comune);
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Cap))
                                    databaseCemi.AddInParameter(comando, "@lavoratoreCap", DbType.String,
                                                                domanda.Lavoratore.Indirizzo.Cap);

                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Frazione))
                                    databaseCemi.AddInParameter(comando, "@lavoratoreFrazione", DbType.String,
                                                                domanda.Lavoratore.Indirizzo.Frazione);
                            }
                            //il metodo getidutente della gestione utenti tipicametne ritorna -1 se l'utente nno � loggato; quindi se maggiore di 0 memorizziamo
                            //l'idutente che ha inserito la domanda, altrimenti non passiamo il parametro che verr� inizializzato dalla SP
                            if (idUtente > 0)
                                databaseCemi.AddInParameter(comando, "@idUtenteInserimento", DbType.String, idUtente);

                            if ((!String.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Cellulare)) &&
                                Common.NumeroCellulareValido(domanda.Lavoratore.Comunicazioni.Cellulare))
                            {
                                databaseCemi.AddInParameter(comando, "@lavoratoreCellulare", DbType.String,
                                                            Common.PulisciNumeroCellulare(
                                                                domanda.Lavoratore.Comunicazioni.Cellulare));
                            }
                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Email))
                            {
                                databaseCemi.AddInParameter(comando, "@lavoratoreEmail", DbType.String,
                                                            domanda.Lavoratore.Comunicazioni.Email);
                            }
                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.IdTipoPagamento))
                            {
                                databaseCemi.AddInParameter(comando, "@lavoratoreIdTipoPagamento", DbType.String,
                                                            domanda.Lavoratore.Comunicazioni.IdTipoPagamento);
                            }
                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Iban))
                            {
                                databaseCemi.AddInParameter(comando, "@lavoratoreIBAN", DbType.String,
                                                            domanda.Lavoratore.Comunicazioni.Iban);
                            }
                            if (domanda.Lavoratore.Comunicazioni.Appuntamento.HasValue)
                            {
                                databaseCemi.AddInParameter(comando, "@lavoratoreAppuntamento", DbType.DateTime,
                                                            domanda.Lavoratore.Comunicazioni.Appuntamento.Value);
                            }

                            if (domanda.Beneficiario != "L")
                            {
                                if (domanda.Familiare.IdFamiliare.HasValue)
                                    databaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32,
                                                                domanda.Familiare.IdFamiliare.Value);
                                databaseCemi.AddInParameter(comando, "@familiareCognome", DbType.String,
                                                            domanda.Familiare.Cognome);
                                databaseCemi.AddInParameter(comando, "@familiareNome", DbType.String,
                                                            domanda.Familiare.Nome);

                                databaseCemi.AddInParameter(comando, "@familiareDataNascita", DbType.DateTime,
                                                            domanda.Familiare.DataNascita);
                                databaseCemi.AddInParameter(comando, "@familiareCodiceFiscale", DbType.String,
                                                            domanda.Familiare.CodiceFiscale);
                            }

                            if (domanda.DatiAggiuntiviScolastiche != null)
                            {
                                if (domanda.DatiAggiuntiviScolastiche.TipoScuola != null)
                                    databaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32,
                                                                domanda.DatiAggiuntiviScolastiche.TipoScuola.
                                                                    IdTipoScuola);

                                if (domanda.DatiAggiuntiviScolastiche.TipoPromozione != null)
                                    databaseCemi.AddInParameter(comando, "@idTipoPromozione", DbType.Int32,
                                                                domanda.DatiAggiuntiviScolastiche.TipoPromozione.
                                                                    IdTipoPromozione);

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso))
                                    databaseCemi.AddInParameter(comando, "@idTipoPrestazioneDecesso", DbType.String,
                                                                domanda.DatiAggiuntiviScolastiche.
                                                                    IdTipoPrestazioneDecesso);

                                if (domanda.DatiAggiuntiviScolastiche.DataDecesso.HasValue)
                                    databaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime,
                                                                domanda.DatiAggiuntiviScolastiche.DataDecesso);

                                if (domanda.DatiAggiuntiviScolastiche.RedditoSi.HasValue)
                                {
                                    databaseCemi.AddInParameter(comando, "@redditoSi", DbType.Boolean,
                                                                domanda.DatiAggiuntiviScolastiche.RedditoSi);
                                }

                                if (!String.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.AnnoScolastico))
                                {
                                    databaseCemi.AddInParameter(comando, "@annoScolastico", DbType.String,
                                                                domanda.DatiAggiuntiviScolastiche.AnnoScolastico);
                                }

                                if (!String.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione))
                                {
                                    databaseCemi.AddInParameter(comando, "@istitutoScolDenominazione", DbType.String,
                                                                domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione);
                                }

                                if (!String.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo))
                                {
                                    databaseCemi.AddInParameter(comando, "@istitutoScolIndirizzo", DbType.String,
                                                                domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo);
                                }

                                if (!String.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoProvincia))
                                {
                                    databaseCemi.AddInParameter(comando, "@istitutoScolProvincia", DbType.String,
                                                                domanda.DatiAggiuntiviScolastiche.IstitutoProvincia);
                                }

                                if (!String.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoLocalita))
                                {
                                    databaseCemi.AddInParameter(comando, "@istitutoScolLocalita", DbType.String,
                                                                domanda.DatiAggiuntiviScolastiche.IstitutoLocalita);
                                }
                            }

                            databaseCemi.AddInParameter(comando, "@certificatoFamiglia", DbType.Boolean,
                                                        domanda.CertificatoFamiglia);
                            databaseCemi.AddInParameter(comando, "@moduloDentista", DbType.Boolean,
                                                        domanda.ModuloDentista);

                            databaseCemi.AddInParameter(comando, "@dataDomanda", DbType.DateTime,
                                                        domanda.DataDomanda);

                            databaseCemi.AddInParameter(comando, "@tipoInserimento", DbType.Int16,
                                                        (Int16) domanda.TipoInserimento);

                            databaseCemi.AddInParameter(comando, "@genitoriConviventi", DbType.Boolean,
                                                        (Boolean) domanda.GenitoriConviventi);

                            if (domanda.NumeroFatture.HasValue)
                            {
                                databaseCemi.AddInParameter(comando, "@numeroFatture", DbType.Int32,
                                                            domanda.NumeroFatture);
                            }
                            else if (domanda.FattureDichiarate != null)
                            {
                                databaseCemi.AddInParameter(comando, "@numeroFatture", DbType.Int32,
                                                            domanda.FattureDichiarate.Count);
                            }

                            if (domanda.TipoCausale != null &&
                                !string.IsNullOrEmpty((domanda.TipoCausale.IdTipoCausale)))
                            {
                                databaseCemi.AddInParameter(comando, "@idTipoCausale", DbType.String,
                                                            domanda.TipoCausale.IdTipoCausale);
                            }
                            databaseCemi.AddOutParameter(comando, "@idDomanda", DbType.Int32, 4);

                            if (databaseCemi.ExecuteNonQuery(comando, transaction) == 0)
                            {
                                object idDomandaObj = databaseCemi.GetParameterValue(comando, "@idDomanda");
                                if (idDomandaObj.GetType() == typeof(int) && ((int) idDomandaObj == -1))
                                    throw new UnivocitaViolataException();
                                else if (idDomandaObj.GetType() == typeof(int) && ((int) idDomandaObj == -2))
                                    throw new DomandaGiaInseritaException();

                                throw new Exception("Inserimento della domanda fallito");
                            }
                            else
                            {
                                domanda.IdDomanda = (int) databaseCemi.GetParameterValue(comando, "@idDomanda");

                                if (domanda.IdDomanda > 0)
                                {
                                    InserisciDocumentiConsegnati(transaction, domanda.IdDomanda.Value, domanda.Documenti);

                                    foreach (FatturaDichiarata fattura in domanda.FattureDichiarate)
                                    {
                                        if (!InsertFatturaDichiarata(fattura, domanda.IdDomanda.Value, transaction))
                                        {
                                            throw new Exception("Errore nell'inserimento di una Fattura Dichiarata");
                                        }
                                    }

                                    foreach (CassaEdile cassaEdile in domanda.CasseEdili)
                                    {
                                        if (!InsertDomandaCassaEdile(cassaEdile, domanda.IdDomanda.Value, transaction))
                                        {
                                            throw new Exception(
                                                "Errore nell'inserimento di una Cassa Edile legata alla domanda");
                                        }
                                    }
                                }
                                else
                                {
                                    if (domanda.IdDomanda == -2)
                                    {
                                        throw new DomandaGiaInseritaException();
                                    }
                                }
                            }
                        }

                        res = true;
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private void InserisciDocumentiConsegnati(DbTransaction transaction, Int32 idDomanda,
                                                  DocumentoCollection documenti)
        {
            if (documenti != null)
            {
                //inseriamo la lista
                foreach (Documento documento in documenti)
                {
                    if (!InsertDocumentoRichiestoConsegnato(documento, idDomanda, transaction))
                    {
                        throw new Exception("Errore nell'inserimento di un documento consegnato");
                    }
                }
            }
        }

        /// <summary>
        ///   Inserisce una nuova lista di documenti conegnati (quella precedente se presente viene cancellata)
        /// </summary>
        /// <param name = "idDomanda"></param>
        /// <param name = "documenti"></param>
        public void InserisciDocumentiConsegnati(Int32 idDomanda, DocumentoCollection documenti)
        {
            using (DbConnection connection = databaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (documenti != null)
                        {
                            //cancelliamo la lista
                            DeleteDocumentoRichiestoConsegnato(idDomanda, transaction);

                            //inseriamo la nuova lista
                            InserisciDocumentiConsegnati(transaction, idDomanda, documenti);
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        public bool InsertDomandaTemporanea(Domanda domanda)
        {
            bool res = false;

            StringBuilder domandaSerializzata = new StringBuilder();

            XmlSerializer serializer = new XmlSerializer(typeof(Domanda));

            using (TextWriter writer = new StringWriter(domandaSerializzata))
            {
                serializer.Serialize(writer, domanda);
            }

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, domanda.Lavoratore.IdLavoratore);
                databaseCemi.AddInParameter(comando, "@domanda", DbType.Xml, domandaSerializzata.ToString());

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool UpdateDomandaTemporanea(Domanda domanda)
        {
            bool res = false;

            StringBuilder domandaSerializzata = new StringBuilder();

            XmlSerializer serializer = new XmlSerializer(typeof(Domanda));

            using (TextWriter writer = new StringWriter(domandaSerializzata))
            {
                serializer.Serialize(writer, domanda);
            }

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                databaseCemi.AddInParameter(comando, "@domanda", DbType.Xml, domandaSerializzata.ToString());

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertDomandaCassaEdileTemporanea(CassaEdile cassaEdile, int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeCasseEdiliInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, cassaEdile.IdCassaEdile);

                if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertFatturaDichiarataTemporanea(FatturaDichiarata fattura, int idDomanda,
                                                       DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureTemporaneeDichiarateInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                databaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);

                if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertDomandaCassaEdile(CassaEdile cassaEdile, int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeCasseEdiliInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, cassaEdile.IdCassaEdile);

                if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertFatturaDichiarata(FatturaDichiarata fattura, int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                databaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);
                databaseCemi.AddInParameter(comando, "@aggiuntaOperatore", DbType.Boolean, fattura.AggiuntaOperatore);
                if (fattura.IdUtenteInserimento.HasValue)
                {
                    databaseCemi.AddInParameter(comando, "@idUtenteInserimento", DbType.Int32, fattura.IdUtenteInserimento);
                }

                if (transaction != null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        /// <summary>
        ///   inserimento di una fattura dichiarata
        /// </summary>
        /// <param name = "fattura"></param>
        /// <param name = "idDomanda"></param>
        /// <returns></returns>
        public bool InsertFatturaDichiarata(FatturaDichiarata fattura, int idDomanda)
        {
            //passiamo null come dbtransaction perch� questa chiamata prevede l'inserimento ad hoc di una fattura dic. in contesto non transazionale
            return InsertFatturaDichiarata(fattura, idDomanda, null);
        }

        public DomandaCollection GetDomande(DomandaFilter filtro)
        {
            DomandaCollection domande = new DomandaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeSelectRicerca"))
            {
                if (filtro.IdLavoratore.HasValue)
                    databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                if (filtro.Stato.HasValue)
                    databaseCemi.AddInParameter(comando, "@stato", DbType.String, filtro.Stato.Value);
                if (!String.IsNullOrEmpty(filtro.IdTipoPrestazione))
                {
                    databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, filtro.IdTipoPrestazione);
                }
                if (filtro.IdGruppo.HasValue)
                {
                    databaseCemi.AddInParameter(comando, "@idGruppo", DbType.Int32, filtro.IdGruppo.Value);
                }
                if (filtro.Anno.HasValue)
                    databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, filtro.Anno.Value);
                if (filtro.Protocollo.HasValue)
                    databaseCemi.AddInParameter(comando, "@protocollo", DbType.Int32, filtro.Protocollo.Value);
                if (filtro.IdUtente.HasValue)
                    databaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, filtro.IdUtente.Value);
                if (!String.IsNullOrEmpty(filtro.CognomeLavoratore))
                    databaseCemi.AddInParameter(comando, "@cognomeLavoratore", DbType.String, filtro.CognomeLavoratore);
                if (!String.IsNullOrEmpty(filtro.NomeLavoratore))
                    databaseCemi.AddInParameter(comando, "@nomeLavoratore", DbType.String, filtro.NomeLavoratore);
                if (filtro.DataNascitaLavoratore.HasValue)
                    databaseCemi.AddInParameter(comando, "@dataNascitaLavoratore", DbType.DateTime,
                                                filtro.DataNascitaLavoratore);
                if (filtro.TipologiaInserimento.HasValue)
                    databaseCemi.AddInParameter(comando, "@tipoInserimento", DbType.Int16, filtro.TipologiaInserimento);
                if (filtro.DataDomandaDal.HasValue)
                {
                    databaseCemi.AddInParameter(comando, "@dataDomandaDal", DbType.DateTime, filtro.DataDomandaDal.Value);
                }
                if (filtro.DataDomandaAl.HasValue)
                {
                    databaseCemi.AddInParameter(comando, "@dataDomandaAl", DbType.DateTime, filtro.DataDomandaAl.Value);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Domanda domanda = new Domanda();
                        domande.Add(domanda);

                        domanda.IdDomanda = (int) reader["idPrestazioniDomanda"];
                        domanda.TipoPrestazione = new TipoPrestazione();
                        domanda.TipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        domanda.ProtocolloPrestazione = (short) reader["protocolloPrestazione"];
                        domanda.NumeroProtocolloPrestazione = (int) reader["numeroProtocolloPrestazione"];
                        domanda.TipoPrestazione.Descrizione = (string) reader["tipoPrestazioneDescrizione"];
                        domanda.Stato = new StatoDomanda();
                        domanda.Stato.IdStato = (string) reader["stato"];
                        domanda.Stato.Descrizione = (string) reader["statoDescrizione"];
                        domanda.Beneficiario = (string) reader["beneficiario"];

                        //Gestione data domanda
                        if (!Convert.IsDBNull(reader["dataDomanda"]))
                        {
                            domanda.DataDomanda = (DateTime) reader["dataDomanda"];
                        }
                        else if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                        {
                            domanda.DataDomanda = (DateTime) reader["dataInserimentoRecord"];
                        }

                        if (!Convert.IsDBNull(reader["login"]))
                            domanda.LoginInCarico = (string) reader["login"];
                        if (!Convert.IsDBNull(reader["idUtente"]))
                            domanda.IdUtenteInCarico = (int) reader["idUtente"];
                        domanda.Lavoratore = new Lavoratore();
                        domanda.Lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                        domanda.Lavoratore.Cognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                        {
                            domanda.Lavoratore.Nome = (string) reader["nome"];
                        }
                        if (!Convert.IsDBNull(reader["dataNascita"]))
                            domanda.Lavoratore.DataNascita = (DateTime) reader["dataNascita"];
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            domanda.Lavoratore.CodiceFiscale = (string) reader["codiceFiscale"];

                        if (!Convert.IsDBNull(reader["idFamiliare"]))
                        {
                            domanda.Familiare = new Familiare();
                            domanda.Familiare.IdFamiliare = (int) reader["idFamiliare"];
                            domanda.Familiare.Cognome = (string) reader["familiareAnagraficaCognome"];
                            domanda.Familiare.Nome = (string) reader["familiareAnagraficaNome"];
                        }

                        if (!Convert.IsDBNull(reader["familiareDomandaCognome"]))
                        {
                            domanda.FamiliareFornito = new Familiare();
                            domanda.FamiliareFornito.Cognome = (string) reader["familiareDomandaCognome"];
                            domanda.FamiliareFornito.Nome = (string) reader["familiareDomandaNome"];
                        }
                        if (!Convert.IsDBNull(reader["tipoInserimento"]))
                            domanda.TipoInserimento = (TipoInserimento) ((Int16) reader["tipoInserimento"]);
                    }
                }
            }

            return domande;
        }

        public DomandaCollection GetDomandeTemporanee(DomandaFilter filtro)
        {
            DomandaCollection domande = new DomandaCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeSelectRicerca"))
            {
                if (filtro.IdLavoratore.HasValue)
                    databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Domanda domanda = null;

                        TextReader textReader = new StringReader((string) reader["domanda"]);
                        XmlSerializer serializer = new XmlSerializer(typeof(Domanda));
                        domanda = (Domanda) serializer.Deserialize(textReader);

                        domanda.IdDomanda = (int) reader["idPrestazioniDomanda"];

                        domande.Add(domanda);
                    }
                }
            }

            return domande;
        }

        public Domanda GetDomanda(int idDomanda)
        {
            Domanda domanda;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeSelectById"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    domanda = new Domanda();
                    domanda.IdDomanda = (int) reader["idPrestazioniDomanda"];
                    domanda.TipoPrestazione = new TipoPrestazione();
                    domanda.TipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                    domanda.TipoPrestazione.Descrizione = (string) reader["tipoPrestazioneDescrizione"];
                    domanda.Beneficiario = (string) reader["beneficiario"];

                    //domanda.DataConferma = (DateTime) reader["dataInserimentoRecord"];

                    domanda.DataRiferimento = (DateTime) reader["dataRiferimento"];
                    domanda.Stato = new StatoDomanda();
                    domanda.Stato.IdStato = (string) reader["stato"];
                    domanda.Stato.Descrizione = (string) reader["statoDescrizione"];
                    if (!Convert.IsDBNull(reader["idUtente"]))
                        domanda.IdUtenteInCarico = (int) reader["idUtente"];
                    if (!Convert.IsDBNull(reader["login"]))
                        domanda.LoginInCarico = (string) reader["login"];
                    domanda.ProtocolloPrestazione = (Int16) reader["protocolloPrestazione"];
                    domanda.NumeroProtocolloPrestazione = (Int32) reader["numeroProtocolloPrestazione"];
                    domanda.GenitoriConviventi = (Boolean) reader["genitoriConviventi"];

                    if (!Convert.IsDBNull(reader["nota"]))
                        domanda.Nota = (string) reader["nota"];

                    if (!Convert.IsDBNull(reader["tipoInserimento"]))
                        domanda.TipoInserimento = (TipoInserimento) ((Int16) reader["tipoInserimento"]);
                    else
                    {
                        //Compatibilit� con il passato
                        domanda.TipoInserimento = TipoInserimento.Normale;
                    }

                    if (!Convert.IsDBNull(reader["numeroFatture"]))
                        domanda.NumeroFatture = (Int32) reader["numeroFatture"];

                    if (!Convert.IsDBNull(reader["idTipoModulo"]))
                    {
                        domanda.TipoModulo = new TipoModulo();
                        domanda.TipoModulo.IdTipoModulo = (Int16) reader["idTipoModulo"];
                        domanda.TipoModulo.Descrizione = (String) reader["tipoModuloDescrizione"];
                        domanda.TipoModulo.Modulo = (String) reader["tipoModuloModulo"];
                    }

                    if (!Convert.IsDBNull(reader["idTipoCausale"]))
                    {
                        TipoCausale tc = new TipoCausale((string) reader["idTipoCausale"]);
                        if (!Convert.IsDBNull(reader["descrizioneTipoCausale"]))
                            tc.Descrizione = (string) reader["descrizioneTipoCausale"];
                        domanda.TipoCausale = tc;
                    }

                    //Gestione data domanda
                    if (!Convert.IsDBNull(reader["dataDomanda"]))
                    {
                        domanda.DataDomanda = (DateTime) reader["dataDomanda"];
                    }
                    else if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                    {
                        domanda.DataDomanda = (DateTime) reader["dataInserimentoRecord"];
                    }

                    #region Dati aggiuntivi della domanda

                    if (!Convert.IsDBNull(reader["idPrestazioniDomandeDatiAggiuntivi"]))
                    {
                        domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();
                        domanda.DatiAggiuntiviScolastiche.IdDatiScolastiche =
                            (Int32) reader["idPrestazioniDomandeDatiAggiuntivi"];

                        if (!Convert.IsDBNull(reader["idTipoScuola"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.TipoScuola = new TipoScuola();
                            domanda.DatiAggiuntiviScolastiche.TipoScuola.IdTipoScuola = (Int32) reader["idTipoScuola"];
                            domanda.DatiAggiuntiviScolastiche.TipoScuola.Descrizione =
                                (string) reader["tipoScuolaDescrizione"];
                        }

                        if (!Convert.IsDBNull(reader["login"]))
                            domanda.LoginInCarico = (string) reader["login"];

                        if (!Convert.IsDBNull(reader["idTipoPromozione"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.TipoPromozione = new TipoPromozione();
                            domanda.DatiAggiuntiviScolastiche.TipoPromozione.IdTipoPromozione =
                                (Int32) reader["idTipoPromozione"];
                            domanda.DatiAggiuntiviScolastiche.TipoPromozione.Descrizione =
                                (string) reader["tipoPromozioneDescrizione"];
                        }

                        if (!Convert.IsDBNull(reader["annoFrequenza"]))
                            domanda.DatiAggiuntiviScolastiche.AnnoFrequenza = (Int16) reader["annoFrequenza"];

                        if (!Convert.IsDBNull(reader["classeConclusa"]))
                            domanda.DatiAggiuntiviScolastiche.ClasseConclusa = (Int16) reader["classeConclusa"];

                        if (!Convert.IsDBNull(reader["numeroMesiFrequentati"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati =
                                (Int16) reader["numeroMesiFrequentati"];

                        if (!Convert.IsDBNull(reader["numeroMesiFrequentati"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati =
                                (Int16) reader["numeroMesiFrequentati"];

                        if (!Convert.IsDBNull(reader["annoImmatricolazione"]))
                            domanda.DatiAggiuntiviScolastiche.AnnoImmatricolazione =
                                (Int16) reader["annoImmatricolazione"];

                        if (!Convert.IsDBNull(reader["numeroAnniFrequentati"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroAnniFrequentati =
                                (Int16) reader["numeroAnniFrequentati"];

                        if (!Convert.IsDBNull(reader["numeroAnniFuoriCorso"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroAnniFuoriCorso =
                                (Int16) reader["numeroAnniFuoriCorso"];

                        if (!Convert.IsDBNull(reader["mediaVoti"]))
                            domanda.DatiAggiuntiviScolastiche.MediaVoti = (Decimal) reader["mediaVoti"];

                        if (!Convert.IsDBNull(reader["numeroEsamiSostenuti"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroEsamiSostenuti =
                                (Int16) reader["numeroEsamiSostenuti"];

                        if (!Convert.IsDBNull(reader["cfuConseguiti"]))
                            domanda.DatiAggiuntiviScolastiche.CfuConseguiti = (Int16) reader["cfuConseguiti"];

                        if (!Convert.IsDBNull(reader["cfuPrevisti"]))
                            domanda.DatiAggiuntiviScolastiche.CfuPrevisti = (Int16) reader["cfuPrevisti"];

                        if (!Convert.IsDBNull(reader["applicaDeduzione"]))
                            domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione = (string) reader["applicaDeduzione"];

                        //funerario
                        //dataDecesso � stata rinominata perch� non era corretto
                        if (!Convert.IsDBNull(reader["dataDecessoFunerario"]))
                            domanda.DatiAggiuntiviScolastiche.DataDecesso = (DateTime) reader["dataDecessoFunerario"];
                        if (!Convert.IsDBNull(reader["idFamiliareErede"]))
                            domanda.DatiAggiuntiviScolastiche.IdFamiliareErede = (int) reader["idFamiliareErede"];
                        if (!Convert.IsDBNull(reader["idTipoPrestazioneDecesso"]))
                            domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso =
                                (string) reader["idTipoPrestazioneDecesso"];

                        //handicap
                        if (!Convert.IsDBNull(reader["annoDaErogare"]))
                            domanda.DatiAggiuntiviScolastiche.AnnoDaErogare = (int) reader["annoDaErogare"];

                        if (!Convert.IsDBNull(reader["tipoDaErogare"]))
                            domanda.DatiAggiuntiviScolastiche.TipoDaErogare = (int) reader["tipoDaErogare"];

                        // asilo nido
                        if (!Convert.IsDBNull(reader["periodoFrequenzaDal"]))
                            domanda.DatiAggiuntiviScolastiche.PeriodoDal = (DateTime) reader["periodoFrequenzaDal"];

                        if (!Convert.IsDBNull(reader["periodoFrequenzaAl"]))
                            domanda.DatiAggiuntiviScolastiche.PeriodoAl = (DateTime) reader["periodoFrequenzaAl"];

                        if (!Convert.IsDBNull(reader["forzaturaScolastichePeriodoFrequenza180Giorni"]))
                            domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza180Giorni = (Boolean) reader["forzaturaScolastichePeriodoFrequenza180Giorni"];

                        if (!Convert.IsDBNull(reader["forzaturaScolastichePeriodoFrequenza3Anni"]))
                            domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza3Anni = (Boolean) reader["forzaturaScolastichePeriodoFrequenza3Anni"];

                        if (!Convert.IsDBNull(reader["forzaturaScolasticheAnnoFrequenza"]))
                            domanda.DatiAggiuntiviScolastiche.ForzaturaAnnoFrequenza = (Boolean) reader["forzaturaScolasticheAnnoFrequenza"];

                        if (!Convert.IsDBNull(reader["redditoSi"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.RedditoSi = (Boolean) reader["redditoSi"];
                        }

                        if (!Convert.IsDBNull(reader["annoScolastico"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.AnnoScolastico = (String) reader["annoScolastico"];
                        }

                        if (!Convert.IsDBNull(reader["denominazioneIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione = (String) reader["denominazioneIstituto"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo = (String) reader["indirizzoIstituto"];
                        }

                        if (!Convert.IsDBNull(reader["localitaIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoLocalita = (String) reader["localitaIstituto"];
                        }

                        if (!Convert.IsDBNull(reader["provinciaIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoProvincia = (String) reader["provinciaIstituto"];
                        }
                    }

                    #endregion

                    #region Dati del lavoratore

                    // Lavoratore
                    domanda.Lavoratore = new Lavoratore();
                    domanda.Lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                    domanda.Lavoratore.Cognome = (string) reader["lavoratoreCognome"];
                    domanda.Lavoratore.Nome = (string) reader["lavoratoreNome"];
                    if (!Convert.IsDBNull(reader["lavoratoreDataNascita"]))
                        domanda.Lavoratore.DataNascita = (DateTime) reader["lavoratoreDataNascita"];
                    if (!Convert.IsDBNull(reader["lavoratoreCodiceFiscale"]))
                        domanda.Lavoratore.CodiceFiscale = (string) reader["lavoratoreCodiceFiscale"];
                    domanda.Lavoratore.Indirizzo = new Indirizzo();
                    if (!Convert.IsDBNull(reader["lavoratoreIndirizzo"]))
                        domanda.Lavoratore.Indirizzo.IndirizzoVia = (string) reader["lavoratoreIndirizzo"];
                    if (!Convert.IsDBNull(reader["lavoratoreProvincia"]))
                        domanda.Lavoratore.Indirizzo.Provincia = (string) reader["lavoratoreProvincia"];
                    if (!Convert.IsDBNull(reader["lavoratoreComune"]))
                        domanda.Lavoratore.Indirizzo.Comune = (string) reader["lavoratoreComune"];
                    if (!Convert.IsDBNull(reader["lavoratoreCap"]))
                        domanda.Lavoratore.Indirizzo.Cap = (string) reader["lavoratoreCap"];
                    domanda.Lavoratore.Comunicazioni = new Comunicazioni();
                    if (!Convert.IsDBNull(reader["lavoratoreCellulare"]))
                        domanda.Lavoratore.Comunicazioni.Cellulare = (string) reader["lavoratoreCellulare"];
                    if (!Convert.IsDBNull(reader["lavoratoreEmail"]))
                        domanda.Lavoratore.Comunicazioni.Email = (string) reader["lavoratoreEmail"];
                    if (!Convert.IsDBNull(reader["lavoratoreIdTipoPagamento"]))
                        domanda.Lavoratore.Comunicazioni.IdTipoPagamento = (string) reader["lavoratoreIdTipoPagamento"];

                    #endregion

                    // Indirizzo fornito
                    domanda.Lavoratore.IndirizzoFornito = new Indirizzo();
                    if (!Convert.IsDBNull(reader["domandaLavoratoreIndirizzo"]))
                        domanda.Lavoratore.IndirizzoFornito.IndirizzoVia = (string) reader["domandaLavoratoreIndirizzo"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreProvincia"]))
                        domanda.Lavoratore.IndirizzoFornito.Provincia = (string) reader["domandaLavoratoreProvincia"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreComune"]))
                        domanda.Lavoratore.IndirizzoFornito.Comune = (string) reader["domandaLavoratoreComune"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreFrazione"]))
                        domanda.Lavoratore.IndirizzoFornito.Frazione = (string) reader["domandaLavoratoreFrazione"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreCap"]))
                        domanda.Lavoratore.IndirizzoFornito.Cap = (string) reader["domandaLavoratoreCap"];

                    // Comunicazioni fornite
                    domanda.Lavoratore.ComunicazioniFornite = new Comunicazioni();
                    if (!Convert.IsDBNull(reader["domandaLavoratoreCellulare"]))
                        domanda.Lavoratore.ComunicazioniFornite.Cellulare =
                            (string) reader["domandaLavoratoreCellulare"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreEmail"]))
                        domanda.Lavoratore.ComunicazioniFornite.Email = (string) reader["domandaLavoratoreEmail"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreIdTipoPagamento"]))
                        domanda.Lavoratore.ComunicazioniFornite.IdTipoPagamento =
                            (string) reader["domandaLavoratoreIdTipoPagamento"];

                    #region Dati familiare

                    // Familiare
                    if (!Convert.IsDBNull(reader["idFamiliare"]))
                    {
                        domanda.Familiare = new Familiare();
                        domanda.Familiare.IdLavoratore = (int) domanda.Lavoratore.IdLavoratore;
                        domanda.Familiare.IdFamiliare = (int) reader["idFamiliare"];
                        domanda.Familiare.Cognome = (string) reader["familiareCognome"];
                        domanda.Familiare.Nome = (string) reader["familiareNome"];
                        if (!Convert.IsDBNull(reader["familiareDataNascita"]))
                            domanda.Familiare.DataNascita = (DateTime) reader["familiareDataNascita"];
                        if (!Convert.IsDBNull(reader["familiareDataDecesso"]))
                            domanda.Familiare.DataDecesso = (DateTime) reader["familiareDataDecesso"];
                        if (!Convert.IsDBNull(reader["familiareCodiceFiscale"]))
                            domanda.Familiare.CodiceFiscale = (string) reader["familiareCodiceFiscale"];
                        if (!Convert.IsDBNull(reader["familiareGradoParentela"]))
                            domanda.Familiare.GradoParentela = (string) reader["familiareGradoParentela"];
                        if (!Convert.IsDBNull(reader["domandaFamiliareACarico"]))
                            domanda.Familiare.ACarico = (string) reader["domandaFamiliareACarico"];
                    }

                    // Familiare fornito
                    if (!Convert.IsDBNull(reader["domandaFamiliareCognome"]))
                    {
                        domanda.FamiliareFornito = new Familiare();
                        domanda.FamiliareFornito.Cognome = (string) reader["domandaFamiliareCognome"];
                        domanda.FamiliareFornito.Nome = (string) reader["domandaFamiliareNome"];
                        if (!Convert.IsDBNull(reader["domandaFamiliareDataNascita"]))
                            domanda.FamiliareFornito.DataNascita = (DateTime) reader["domandaFamiliareDataNascita"];
                        if (!Convert.IsDBNull(reader["domandaFamiliareCodiceFiscale"]))
                            domanda.FamiliareFornito.CodiceFiscale = (string) reader["domandaFamiliareCodiceFiscale"];
                    }

                    #endregion

                    if (!Convert.IsDBNull(reader["controlloDatiAnagraficiFamiliare"]))
                        domanda.ControlloFamiliare = (bool) reader["controlloDatiAnagraficiFamiliare"];
                    if (!Convert.IsDBNull(reader["controlloPresenzaDocumenti"]))
                        domanda.ControlloPresenzaDocumenti = (bool) reader["controlloPresenzaDocumenti"];
                    if (!Convert.IsDBNull(reader["controlloFatture"]))
                        domanda.ControlloFatture = (bool) reader["controlloFatture"];
                    if (!Convert.IsDBNull(reader["controlloUnivocitaPrestazione"]))
                        domanda.ControlloUnivocitaPrestazione = (bool) reader["controlloUnivocitaPrestazione"];
                    if (!Convert.IsDBNull(reader["controlloDatiAnagraficiLavoratore"]))
                        domanda.ControlloLavoratore = (bool) reader["controlloDatiAnagraficiLavoratore"];
                    if (!Convert.IsDBNull(reader["controlloOreCNCE"]))
                        domanda.ControlloOreCnce = (bool) reader["controlloOreCNCE"];
                    if (!Convert.IsDBNull(reader["controlloDatiScolastiche"]))
                        domanda.ControlloScolastiche = (bool) reader["controlloDatiScolastiche"];

                    #region Fatture

                    // Fatture fornite
                    reader.NextResult();
                    domanda.FattureDichiarate = new FatturaDichiarataCollection();
                    while (reader.Read())
                    {
                        FatturaDichiarata fattura = new FatturaDichiarata();

                        //fattura.IdPrestazioniFatturaDichiarata = (int)reader["idPrestazioniFatturaDichiarata"];
                        //fattura.Data = (DateTime)reader["data"];
                        //fattura.Numero = (string)reader["numero"];
                        //fattura.ImportoTotale = (decimal)reader["importoTotale"];
                        //fattura.Saldo = (bool)reader["saldo"];


                        fattura.IdPrestazioniFatturaDichiarata = (int) reader["idPrestazioniFatturaDichiarata"];
                        //fattura.IdPrestazioniDomanda = (int)reader["idPrestazioniDomanda"];
                        if (!Convert.IsDBNull(reader["idPrestazioniFattura"]))
                            fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            fattura.IdFatturaArchidoc = (string) reader["idArchidoc"];

                        fattura.Data = (DateTime) reader["data"];
                        fattura.Numero = (string) reader["numero"];
                        fattura.ImportoTotale = (decimal) reader["importoTotale"];
                        fattura.Saldo = (bool) reader["saldo"];
                        fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                        if (!Convert.IsDBNull(reader["idUtenteInserimento"]))
                            fattura.IdUtenteInserimento = (Int32) reader["idUtenteInserimento"];
                        if (!Convert.IsDBNull(reader["utenteInserimento"]))
                            fattura.UtenteInserimento = (String) reader["utenteInserimento"];

                        if (!Convert.IsDBNull(reader["dataAnnullamento"]))
                            fattura.DataAnnullamento = (DateTime) reader["dataAnnullamento"];
                        //fattura.DataInserimentoRecord = (DateTime)reader["dataInserimentoRecord"];
                        domanda.FattureDichiarate.Add(fattura);
                    }

                    #endregion

                    // Casse Edili
                    reader.NextResult();
                    domanda.CasseEdili = new CassaEdileCollection();
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile();
                        domanda.CasseEdili.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        cassaEdile.Descrizione = (string) reader["descrizione"];
                        cassaEdile.InseritaManualmente = (Boolean) reader["inserimentoManuale"];
                        cassaEdile.Cnce = !Convert.IsDBNull(reader["cnceIdCassaEdile"]);
                    }

                    // Importi Prestazione
                    reader.NextResult();
                    domanda.Importi = new PrestazioneImportoCollection();
                    while (reader.Read())
                    {
                        PrestazioneImporto importo = new PrestazioneImporto();
                        domanda.Importi.Add(importo);

                        importo.IdDomanda = idDomanda;
                        if (!Convert.IsDBNull(reader["idPrestazioniImportoPrestazione"]))
                            importo.IdImporto = (Int32) reader["idPrestazioniImportoPrestazione"];
                        importo.IdTipoImporto = (Int32) reader["idPrestazioniTipoImportoPrestazione"];
                        importo.Descrizione = (String) reader["descrizione"];
                        if (!Convert.IsDBNull(reader["valore"]))
                            importo.Valore = (Decimal) reader["valore"];
                    }
                }
            }

            return domanda;
        }

        public Domanda GetDomandaTemporanea(int idDomandaTemporanea)
        {
            Domanda domanda = null;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeSelectById"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomandaTemporanea);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        TextReader textReader = new StringReader((string) reader["domanda"]);
                        XmlSerializer serializer = new XmlSerializer(typeof(Domanda));
                        domanda = (Domanda) serializer.Deserialize(textReader);
                    }
                }
            }

            return domanda;
        }

        /// <summary>
        ///   Ritorna una collection di documenti a partire da un oggetto filtro
        /// </summary>
        /// <param name = "filtro"></param>
        /// <returns></returns>
        public DocumentoCollection GetDocumenti(DocumentoFilter filtro)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_DocumentiLavoratoreSelect"))
            {
                if (filtro.IdLavoratore.HasValue)
                    databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                if (filtro.IdTipoDocumento.HasValue)
                    databaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.String, filtro.IdTipoDocumento);
                if (!string.IsNullOrEmpty(filtro.Cognome))
                    databaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);

                if (filtro.DataScansioneDa.HasValue)
                    databaseCemi.AddInParameter(comando, "@dataScansioneDa", DbType.DateTime, filtro.DataScansioneDa);

                if (filtro.DataScansioneA.HasValue)
                    databaseCemi.AddInParameter(comando, "@dataScansioneA", DbType.DateTime, filtro.DataScansioneA);
                if (!string.IsNullOrEmpty(filtro.IdTipoPrestazione))
                    databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, filtro.IdTipoPrestazione);
                if (filtro.ProtocolloPrestazione.HasValue)
                    databaseCemi.AddInParameter(comando, "@protocolloPrestazione", DbType.Int32,
                                                filtro.ProtocolloPrestazione);
                if (filtro.NumeroProtocolloPrestazione.HasValue)
                    databaseCemi.AddInParameter(comando, "@numeroProtocolloPrestazione", DbType.Int32,
                                                filtro.NumeroProtocolloPrestazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();
                        documenti.Add(documento);

                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            documento.CodiceFiscale = (string) reader["codiceFiscale"];
                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["dataNascita"]))
                            documento.DataNascita = (DateTime) reader["dataNascita"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            documento.DataNascita = (DateTime) reader["dataScansione"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idFamiliare"]))
                            documento.IdFamiliare = (int) reader["idFamiliare"];
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                            documento.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["idPrestazioniDomanda"]))
                            documento.IdPrestazioniDomanda = (int) reader["idPrestazioniDomanda"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];

                        if (!Convert.IsDBNull(reader["idTipoPrestazione"]))
                            documento.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        if (!Convert.IsDBNull(reader["numeroProtocolloPrestazione"]))
                            documento.NumeroProtocolloPrestazione = (int) reader["numeroProtocolloPrestazione"];
                        if (!Convert.IsDBNull(reader["protocolloPrestazione"]))
                            documento.ProtocolloPrestazione = (int) reader["protocolloPrestazione"];

                        documento.IdDocumento = (int) reader["idDocumento"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];
                    }
                }
            }

            return documenti;
        }

        public Documento GetDocumento(short idTipoDocumento, int idDocumento)
        {
            Documento documento;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_DocumentiLavoratoreSelectByKey"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16, idTipoDocumento);
                databaseCemi.AddInParameter(comando, "@idDocumento", DbType.Int32, idDocumento);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();
                    documento = new Documento();

                    if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        documento.CodiceFiscale = (string) reader["codiceFiscale"];
                    if (!Convert.IsDBNull(reader["cognome"]))
                        documento.LavoratoreCognome = (string) reader["cognome"];
                    if (!Convert.IsDBNull(reader["nome"]))
                        documento.LavoratoreNome = (string) reader["nome"];
                    if (!Convert.IsDBNull(reader["dataNascita"]))
                        documento.DataNascita = (DateTime) reader["dataNascita"];
                    if (!Convert.IsDBNull(reader["dataScansione"]))
                        documento.DataNascita = (DateTime) reader["dataScansione"];
                    if (!Convert.IsDBNull(reader["idArchidoc"]))
                        documento.IdArchidoc = (string) reader["idArchidoc"];
                    if (!Convert.IsDBNull(reader["idFamiliare"]))
                        documento.IdFamiliare = (int) reader["idFamiliare"];
                    if (!Convert.IsDBNull(reader["idLavoratore"]))
                        documento.IdLavoratore = (int) reader["idLavoratore"];
                    if (!Convert.IsDBNull(reader["idPrestazioniDomanda"]))
                        documento.IdPrestazioniDomanda = (int) reader["idPrestazioniDomanda"];
                    if (!Convert.IsDBNull(reader["originale"]))
                        documento.Originale = (bool) reader["originale"];

                    documento.IdDocumento = (int) reader["idDocumento"];
                    documento.TipoDocumento = new TipoDocumento();
                    documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                    documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];
                }
            }

            return documento;
        }

        public DocumentoCollection GetDocumentiNecessari(int idDomanda)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumenti"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();
                        documenti.Add(documento);

                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idDocumento"]))
                            documento.IdDocumento = (int) reader["idDocumento"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];

                        documento.PerPrestazione = (bool) reader["univocoPrestazione"];
                        documento.RiferitoA = (string) reader["documentoRiferitoA"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];
                    }
                }
            }

            return documenti;
        }

        public DocumentoCollection GetDocumentiNecessariPiuRecenti(Int32 idDomanda)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumenti"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();

                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idDocumento"]))
                            documento.IdDocumento = (int) reader["idDocumento"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            documento.DataScansione = (DateTime) reader["dataScansione"];

                        documento.PerPrestazione = (bool) reader["univocoPrestazione"];
                        documento.RiferitoA = (string) reader["documentoRiferitoA"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];

                        if (!documenti.DocumentoGiaPresente(documento))
                        {
                            documenti.Add(documento);
                        }
                    }
                }
            }

            return documenti;
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario, int idLavoratore,
                                                         int? idFamiliare)
        {
            return GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, false, null);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario, int idLavoratore,
                                                         int? idFamiliare, Int32? idPrestazioniDomanda)
        {
            return GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, false,
                                         idPrestazioniDomanda);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario, int idLavoratore,
                                                         int? idFamiliare, bool operatore)
        {
            return GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, operatore);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario, int idLavoratore,
                                                         int? idFamiliare, bool operatore, Int32? idPrestazioniDomanda)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumentiGenerico"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);
                databaseCemi.AddInParameter(comando, "@beneficiario", DbType.String, beneficiario);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                if (idFamiliare.HasValue)
                    databaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare.Value);
                databaseCemi.AddInParameter(comando, "@operatore", DbType.Boolean, operatore);
                if (idPrestazioniDomanda.HasValue)
                    databaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32,
                                                idPrestazioniDomanda.Value);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();
                        documenti.Add(documento);

                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idDocumento"]))
                            documento.IdDocumento = (int) reader["idDocumento"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            documento.DataScansione = (DateTime) reader["dataScansione"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];

                        documento.PerPrestazione = (bool) reader["univocoPrestazione"];
                        documento.RiferitoA = (string) reader["documentoRiferitoA"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];

                        if (!Convert.IsDBNull(reader["consegnato"]))
                            documento.Consegnato = (bool) reader["consegnato"];
                    }
                }
            }

            return documenti;
        }

        public DocumentoCopertinaCollection GetDocumentiStampaCopertine(Int32 idDomanda)
        {
            DocumentoCopertinaCollection documenti = new DocumentoCopertinaCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumentiCopertine"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici
                    Int32 indiceIdDomanda = reader.GetOrdinal("idPrestazioniDomanda");
                    Int32 indiceIdTipoDocumento = reader.GetOrdinal("idTipoDocumento");
                    Int32 indiceIdTipoDocumentoArchidoc = reader.GetOrdinal("idTipoDocumentoArchidoc");
                    Int32 indiceDescrizioneDocumento = reader.GetOrdinal("documento");
                    Int32 indiceDescrizioneBeneficiario = reader.GetOrdinal("descrizioneBeneficiario");
                    Int32 indiceIdFatturaDichiarata = reader.GetOrdinal("idPrestazioniFatturaDichiarata");
                    Int32 indiceConsegnato = reader.GetOrdinal("consegnato");
                    Int32 indiceGradoParentela = reader.GetOrdinal("gradoParentela");
                    Int32 indiceGradoParentelaDocumento = reader.GetOrdinal("gradoParentelaDocumento");
                    Int32 indiceFatturaData = reader.GetOrdinal("fatturaData");
                    Int32 indiceFatturaNumero = reader.GetOrdinal("fatturaNumero");
                    #endregion

                    while (reader.Read())
                    {
                        DocumentoCopertina documento = new DocumentoCopertina();
                        documenti.Add(documento);

                        documento.IdDomanda = reader.GetInt32(indiceIdDomanda);
                        if (!reader.IsDBNull(indiceIdTipoDocumento))
                        {
                            documento.IdTipoDocumento = reader.GetInt16(indiceIdTipoDocumento);
                        }
                        documento.IdTipoDocumentoArchidoc = reader.GetInt32(indiceIdTipoDocumentoArchidoc);
                        documento.DescrizioneDocumento = reader.GetString(indiceDescrizioneDocumento);
                        if (!reader.IsDBNull(indiceDescrizioneBeneficiario))
                        {
                            documento.DescrizioneBeneficiario = reader.GetString(indiceDescrizioneBeneficiario);
                        }
                        if (!reader.IsDBNull(indiceIdFatturaDichiarata))
                        {
                            documento.IdFatturaDichiarata = reader.GetInt32(indiceIdFatturaDichiarata);
                        }
                        if (!reader.IsDBNull(indiceConsegnato))
                        {
                            documento.Consegnato = reader.GetBoolean(indiceConsegnato);
                        }
                        if (!reader.IsDBNull(indiceGradoParentela))
                        {
                            documento.GradoParentela = reader.GetString(indiceGradoParentela);
                        }
                        if (!reader.IsDBNull(indiceGradoParentelaDocumento))
                        {
                            documento.GradoParentelaDocumento = reader.GetString(indiceGradoParentelaDocumento);
                        }
                        if (!reader.IsDBNull(indiceFatturaData))
                        {
                            documento.FatturaData = reader.GetDateTime(indiceFatturaData);
                        }
                        if (!reader.IsDBNull(indiceFatturaNumero))
                        {
                            documento.FatturaNumero = reader.GetString(indiceFatturaNumero);
                        }
                    }
                }
            }

            return documenti;
        }

        public bool UpdateDomandaSetFamiliareDaAnagrafica(int idDomanda, int idFamiliare)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateSetFamiliare")
                )
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool UpdateDomandaSetPresaInCarico(int idDomanda, int idUtente, bool inCarico)
        {
            bool res = false;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdatePresaInCarico"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                databaseCemi.AddInParameter(comando, "@inCarico", DbType.Boolean, inCarico);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        //public OreMensiliCNCECollection GetOreMensiliCNCE(int idLavoratore)
        //{
        //    OreMensiliCNCECollection ore = new OreMensiliCNCECollection();

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCEOreSelect"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

        //        using (IDataReader reader = databaseCemi.ExecuteReader(comando))
        //        {
        //            while (reader.Read())
        //            {
        //                OreMensiliCNCE oreMese = TrasformaReaderInOreMensili(reader);
        //                ore.Add(oreMese);
        //            }
        //        }
        //    }

        //    return ore;
        //}

        //public OreMensiliCNCE GetOreMensiliCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        //{
        //    OreMensiliCNCE ore = null;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCEOreSelectSingolo"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

        //        using (IDataReader reader = databaseCemi.ExecuteReader(comando))
        //        {
        //            if (reader.Read())
        //            {
        //                ore = TrasformaReaderInOreMensili(reader);
        //            }
        //        }
        //    }

        //    return ore;
        //}

        //private OreMensiliCNCE TrasformaReaderInOreMensili(IDataReader reader)
        //{
        //    OreMensiliCNCE oreMese = new OreMensiliCNCE();

        //    oreMese.IdCassaEdile = (string) reader["idCassaEdile"];
        //    oreMese.IdLavoratore = (int) reader["idLavoratore"];
        //    oreMese.Anno = (int) reader["anno"];
        //    oreMese.Mese = (int) reader["mese"];
        //    if (!Convert.IsDBNull(reader["oreLavorate"]))
        //        oreMese.OreLavorate = (int) reader["oreLavorate"];
        //    if (!Convert.IsDBNull(reader["oreFerie"]))
        //        oreMese.OreFerie = (int) reader["oreFerie"];
        //    if (!Convert.IsDBNull(reader["oreInfortunio"]))
        //        oreMese.OreInfortunio = (int) reader["oreInfortunio"];
        //    if (!Convert.IsDBNull(reader["oreMalattia"]))
        //        oreMese.OreMalattia = (int) reader["oreMalattia"];
        //    if (!Convert.IsDBNull(reader["oreCassaIntegrazione"]))
        //        oreMese.OreCassaIntegrazione = (int) reader["oreCassaIntegrazione"];
        //    if (!Convert.IsDBNull(reader["orePermessoRetribuito"]))
        //        oreMese.OrePermessoRetribuito = (int) reader["orePermessoRetribuito"];
        //    if (!Convert.IsDBNull(reader["orePermessoNonRetribuito"]))
        //        oreMese.OrePermessoNonRetribuito = (int) reader["orePermessoNonRetribuito"];
        //    if (!Convert.IsDBNull(reader["oreAltro"]))
        //        oreMese.OreAltro = (int) reader["oreAltro"];
        //    if (!Convert.IsDBNull(reader["livelloErogazione"]))
        //        oreMese.LivelloErogazione = (int) reader["livelloErogazione"];
        //    oreMese.InseriteManualmente = (bool) reader["inseriteManualmente"];

        //    return oreMese;
        //}

        //public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        //{
        //    bool res = false;
        //    oreDuplicate = false;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertUpdate"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, ore.IdLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, ore.IdCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, ore.Anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, ore.Mese);

        //        databaseCemi.AddInParameter(comando, "@oreLavorate", DbType.Int32, ore.OreLavorate);
        //        databaseCemi.AddInParameter(comando, "@oreFerie", DbType.Int32, ore.OreFerie);
        //        databaseCemi.AddInParameter(comando, "@oreInfortunio", DbType.Int32, ore.OreInfortunio);
        //        databaseCemi.AddInParameter(comando, "@oreMalattia", DbType.Int32, ore.OreMalattia);
        //        databaseCemi.AddInParameter(comando, "@oreCassaIntegrazione", DbType.Int32, ore.OreCassaIntegrazione);
        //        databaseCemi.AddInParameter(comando, "@orePermessoRetribuito", DbType.Int32, ore.OrePermessoRetribuito);
        //        databaseCemi.AddInParameter(comando, "@orePermessoNonRetribuito", DbType.Int32,
        //                                    ore.OrePermessoNonRetribuito);
        //        databaseCemi.AddInParameter(comando, "@oreAltro", DbType.Int32, ore.OreAltro);
        //        databaseCemi.AddInParameter(comando, "@livelloErogazione", DbType.Int32, ore.LivelloErogazione);
        //        databaseCemi.AddInParameter(comando, "@inseriteManualmente", DbType.Boolean, ore.InseriteManualmente);

        //        try
        //        {
        //            if (databaseCemi.ExecuteNonQuery(comando) == 1)
        //                res = true;
        //        }
        //        catch (SqlException sqlExc)
        //        {
        //            // Eccezione che viene generata se provo ad inserire due volte le ore per lo stesso mese
        //            if (sqlExc.Number == 2627)
        //                oreDuplicate = true;
        //            else
        //                throw;
        //        }
        //    }

        //    return res;
        //}

        public void ControllaDomandaLavoratore(Domanda domanda)
        {
            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloLavoratore"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                databaseCemi.AddOutParameter(comando, "@indirizzo", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@email", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@cellulare", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@controlloLavoratore", DbType.Boolean, 1);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.Lavoratore.ControlloIndirizzo =
                        databaseCemi.GetParameterValue(comando, "@indirizzo") as bool?;
                    domanda.Lavoratore.ControlloEmail = databaseCemi.GetParameterValue(comando, "@email") as bool?;
                    domanda.Lavoratore.ControlloCellulare =
                        databaseCemi.GetParameterValue(comando, "@cellulare") as bool?;
                    domanda.ControlloLavoratore =
                        databaseCemi.GetParameterValue(comando, "@controlloLavoratore") as bool?;
                }
            }
        }

        public void ControllaDomandaFamiliare(Domanda domanda)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloFamiliare")
                )
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                databaseCemi.AddOutParameter(comando, "@familiareSelezionato", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@dataDecesso", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@gradoParentela", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@dataNascita", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@codiceFiscale", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@controlloFamiliare", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@aCarico", DbType.Boolean, 1);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    if (domanda.Familiare != null)
                    {
                        domanda.Familiare.ControlloFamiliareSelezionato =
                            databaseCemi.GetParameterValue(comando, "@familiareSelezionato") as bool?;
                        domanda.Familiare.ControlloDataDecesso =
                            databaseCemi.GetParameterValue(comando, "@dataDecesso") as bool?;
                        domanda.Familiare.ControlloGradoParentela =
                            databaseCemi.GetParameterValue(comando, "@gradoParentela") as bool?;
                        domanda.Familiare.ControlloDataNascita =
                            databaseCemi.GetParameterValue(comando, "@dataNascita") as bool?;
                        domanda.Familiare.ControlloCodiceFiscale =
                            databaseCemi.GetParameterValue(comando, "@codiceFiscale") as bool?;
                        domanda.Familiare.ControlloACarico =
                            databaseCemi.GetParameterValue(comando, "@aCarico") as bool?;
                        domanda.ControlloFamiliare =
                            databaseCemi.GetParameterValue(comando, "@controlloFamiliare") as bool?;
                    }
                    else if (domanda.FamiliareFornito != null)
                    {
                        domanda.FamiliareFornito.ControlloFamiliareSelezionato =
                            databaseCemi.GetParameterValue(comando, "@familiareSelezionato") as bool?;
                        domanda.FamiliareFornito.ControlloDataDecesso =
                            databaseCemi.GetParameterValue(comando, "@dataDecesso") as bool?;
                        domanda.FamiliareFornito.ControlloGradoParentela =
                            databaseCemi.GetParameterValue(comando, "@gradoParentela") as bool?;
                        domanda.FamiliareFornito.ControlloDataNascita =
                            databaseCemi.GetParameterValue(comando, "@dataNascita") as bool?;
                        domanda.FamiliareFornito.ControlloCodiceFiscale =
                            databaseCemi.GetParameterValue(comando, "@codiceFiscale") as bool?;
                        domanda.ControlloFamiliare =
                            databaseCemi.GetParameterValue(comando, "@controlloFamiliare") as bool?;
                    }
                }
            }
        }

        public void ControllaDomandaUnivocita(Domanda domanda)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloUnivocita")
                )
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                databaseCemi.AddOutParameter(comando, "@controlloUnivocita", DbType.Boolean, 1);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.ControlloUnivocitaPrestazione =
                        databaseCemi.GetParameterValue(comando, "@controlloUnivocita") as bool?;
                }
            }
        }

        public void ControllaDomandaDocumenti(Domanda domanda)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloDocumenti")
                )
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                databaseCemi.AddOutParameter(comando, "@controlloDocumenti", DbType.Boolean, 1);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.ControlloPresenzaDocumenti =
                        databaseCemi.GetParameterValue(comando, "@controlloDocumenti") as bool?;
                }
            }
        }

        public void ControllaDomandaFatture(Domanda domanda)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloFatture")
                )
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                databaseCemi.AddOutParameter(comando, "@controlloFatture", DbType.Boolean, 1);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.ControlloFatture =
                        databaseCemi.GetParameterValue(comando, "@controlloFatture") as bool?;
                }
            }
        }

        public void ControllaDomandaScolastiche(Domanda domanda)
        {
            if (domanda.IdTipoPrestazione != "C-ANID")
            {
                domanda.ControlloScolastiche = true;
            }
            else
            {
                using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloScolastiche")
                )
                {
                    databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                    databaseCemi.AddOutParameter(comando, "@numeroMesiFrequenza", DbType.Boolean, 1);
                    databaseCemi.AddOutParameter(comando, "@periodo180Giorni", DbType.Boolean, 1);
                    databaseCemi.AddOutParameter(comando, "@periodo3Anni", DbType.Boolean, 1);
                    databaseCemi.AddOutParameter(comando, "@annoFrequenza", DbType.Boolean, 1);
                    databaseCemi.AddOutParameter(comando, "@controlloScolastiche", DbType.Boolean, 1);

                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        //domanda.ControlloFatture =
                        //    databaseCemi.GetParameterValue(comando, "@controlloScolastiche") as bool?;
                        domanda.ControlloScolastiche =
                            databaseCemi.GetParameterValue(comando, "@controlloScolastiche") as bool?;
                    }
                }
            }
        }

        public bool UpdateDomandaAccogli(int idDomanda, int idUtente, String idTipoPrestazione)
        {
            bool res = false;

            // Se viene accolta una prestazione per cure e protesi dentarie
            // bisogna generare le due prestazioni per il calcolo:
            // - COO4CL per le cure
            // - C004PL per le protesi
            if (idTipoPrestazione == "C004CP")
            {
                Domanda domanda = GetDomanda(idDomanda);

                if (domanda.Importi != null && domanda.Importi.Count > 0)
                {
                    foreach (PrestazioneImporto importo in domanda.Importi)
                    {
                        // Cure
                        if (importo.IdTipoImporto == 19 && importo.Valore > 0)
                        {
                            Domanda domandaCure = domanda.CopiaBase();
                            domandaCure.TipoPrestazione.IdTipoPrestazione = "C004CL";
                            domandaCure.Stato.IdStato = "C";
                            domandaCure.Stato.Descrizione = "Accolta";

                            InsertDomanda(domandaCure, null, idUtente, true);
                        }

                        // Protesi
                        if (importo.IdTipoImporto == 18 && importo.Valore > 0)
                        {
                            Domanda domandaProtesi = domanda.CopiaBase();
                            domandaProtesi.TipoPrestazione.IdTipoPrestazione = "C004PL";
                            domandaProtesi.Stato.IdStato = "C";
                            domandaProtesi.Stato.Descrizione = "Accolta";

                            InsertDomanda(domandaProtesi, null, idUtente, true);
                        }
                    }
                }
            }

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAccogli"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (idUtente > 0)
                    databaseCemi.AddInParameter(comando, "@idUtenteGestione", DbType.Int32, idUtente);

                databaseCemi.AddOutParameter(comando, "@righeAggiornate", DbType.Int32, 4);

                databaseCemi.ExecuteNonQuery(comando);
                int righeAggiornate = (int) databaseCemi.GetParameterValue(comando, "@righeAggiornate");
                if (righeAggiornate == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public StatoDomandaCollection GetStatiDomanda()
        {
            StatoDomandaCollection stati = new StatoDomandaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniStatiDomandaSelect"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdStato = reader.GetOrdinal("idPrestazioniStatoDomanda");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        StatoDomanda stato = new StatoDomanda();
                        stati.Add(stato);

                        stato.IdStato = reader.GetString(indiceIdStato);
                        stato.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return stati;
        }

        public bool UpdateDomandaAnnulla(int idDomanda, int idUtente)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAnnulla"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                if (idUtente > 0)
                    databaseCemi.AddInParameter(comando, "@idUtenteGestione", DbType.Int32, idUtente);

                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaAttesa(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAttesa"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaAttesaDenuncia(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAttesaDenuncia"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaEsame(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateEsame"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaRespingi(int idDomanda, string idTipoCausale)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateRespingi"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idTipoCausale", DbType.String, idTipoCausale);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public TipoDocumentoCollection GetTipiDocumento()
        {
            TipoDocumentoCollection tipiDocumento = new TipoDocumentoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_TipiDocumentiSelect"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoDocumento = reader.GetOrdinal("idTipoDocumento");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoDocumento tipoDocumento = new TipoDocumento();
                        tipiDocumento.Add(tipoDocumento);

                        tipoDocumento.IdTipoDocumento = reader.GetInt16(indiceIdTipoDocumento);
                        tipoDocumento.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiDocumento;
        }

        /// <summary>
        ///   Ritorna i tipi causali finalizzati alla gestione fast delle prestazioni
        /// </summary>
        /// <returns>Elenco delle causali</returns>
        public TipoCausaleCollection GetTipiCausaliFast()
        {
            return GetTipiCausali(true, string.Empty);
        }

        /// <summary>
        /// </summary>
        /// <param name = "fast"></param>
        /// <param name = "idTipoPrestazione"></param>
        /// <returns></returns>
        public TipoCausaleCollection GetTipiCausali(bool? fast, String idTipoPrestazione)
        {
            TipoCausaleCollection tipiCausale = new TipoCausaleCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_TipiCausaliSelect"))
            {
                if (fast.HasValue)
                    databaseCemi.AddInParameter(comando, "@visualizzabiliFast", DbType.Int32, fast.Value);
                if (!string.IsNullOrEmpty(idTipoPrestazione))
                    databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoCausale = reader.GetOrdinal("idTipoCausale");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");
                    int indiceDescrizioneEstesa = reader.GetOrdinal("descrizioneEstesa");

                    while (reader.Read())
                    {
                        TipoCausale tipoCausale = new TipoCausale();
                        tipiCausale.Add(tipoCausale);

                        tipoCausale.IdTipoCausale = reader.GetString(indiceIdTipoCausale);
                        tipoCausale.Descrizione = reader.GetString(indiceDescrizione);
                        if (!Convert.IsDBNull(reader["descrizioneEstesa"]))
                            tipoCausale.DescrizioneEstesa = reader.GetString(indiceDescrizioneEstesa);
                    }
                }
            }

            return tipiCausale;
        }

        public bool AssociaPersonaDocumento(short idTipoDocumento, int idDocumento,
                                            Lavoratore lavoratore, Familiare familiare, int? idDomanda)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniAssociaDocumento"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16, idTipoDocumento);
                databaseCemi.AddInParameter(comando, "@idDocumento", DbType.Int32, idDocumento);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                if (familiare != null && familiare.IdFamiliare.HasValue)
                    databaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, familiare.IdFamiliare.Value);
                if (idDomanda.HasValue)
                    databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) > 0)
                    res = true;
            }

            return res;
        }

        public bool DisassociaDocumento(short idTipoDocumento, int idDocumento)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDisassociaDocumento"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16, idTipoDocumento);
                databaseCemi.AddInParameter(comando, "@idDocumento", DbType.Int32, idDocumento);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }
            return res;
        }

        public PrestazioneErogataCollection GetPrestazioniErogate(PrestazioneErogataFilter filtro)
        {
            PrestazioneErogataCollection prestazioni = new PrestazioneErogataCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniCompleteSelectByIdLavoratore"))
            {
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceIdTipoPrestazione = reader.GetOrdinal("idTipoPrestazione");
                    int indiceDescrizioneTipoPrestazione = reader.GetOrdinal("descrizioneTipoPrestazione");
                    int indiceIdFamiliare = reader.GetOrdinal("idFamiliare");
                    int indiceCognomeFamiliare = reader.GetOrdinal("familiareCognome");
                    int indiceNomeFamiliare = reader.GetOrdinal("familiareNome");
                    int indiceProtocolloPrestazione = reader.GetOrdinal("protocolloPrestazione");
                    int indiceNumeroProtocolloPrestazione = reader.GetOrdinal("numeroProtocolloPrestazione");
                    int indiceImportoLiquidabile = reader.GetOrdinal("importoLiquidabile");
                    int indiceIdStatoPrestazione = reader.GetOrdinal("idTipoStatoPrestazione");
                    int indiceDataDomanda = reader.GetOrdinal("dataDomanda");

                    #endregion

                    while (reader.Read())
                    {
                        PrestazioneErogata prestazione = new PrestazioneErogata();
                        prestazioni.Add(prestazione);

                        prestazione.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        prestazione.TipoPrestazione = new TipoPrestazione();
                        prestazione.TipoPrestazione.IdTipoPrestazione = reader.GetString(indiceIdTipoPrestazione);
                        prestazione.TipoPrestazione.Descrizione = reader.GetString(indiceDescrizioneTipoPrestazione);
                        prestazione.ProtocolloPrestazione = reader.GetInt32(indiceProtocolloPrestazione);
                        prestazione.NumeroProtocolloPrestazione = reader.GetInt32(indiceNumeroProtocolloPrestazione);
                        if (!reader.IsDBNull(indiceImportoLiquidabile))
                            prestazione.ImportoLiquidabile = reader.GetDecimal(indiceImportoLiquidabile);
                        if (!reader.IsDBNull(indiceIdStatoPrestazione))
                            prestazione.IdStatoPrestazione = reader.GetString(indiceIdStatoPrestazione);
                        if (!reader.IsDBNull(indiceDataDomanda))
                            prestazione.DataDomanda = reader.GetDateTime(indiceDataDomanda);

                        if (!reader.IsDBNull(indiceIdFamiliare))
                        {
                            prestazione.Familiare = new Familiare();
                            prestazione.Familiare.IdLavoratore = prestazione.IdLavoratore;
                            prestazione.Familiare.IdFamiliare = reader.GetInt32(indiceIdFamiliare);
                            prestazione.Familiare.Cognome = reader.GetString(indiceCognomeFamiliare);
                            prestazione.Familiare.Nome = reader.GetString(indiceNomeFamiliare);
                        }
                    }
                }
            }

            return prestazioni;
        }

        public bool FatturaGiaUtilizzata(FatturaDichiarata fattura, int idLavoratore)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_FattureDichiarateSelectFatturaGiaPresente"))
            {
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                databaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddOutParameter(comando, "@numeroFatture", DbType.Int32, 4);

                databaseCemi.ExecuteNonQuery(comando);

                if (((int) databaseCemi.GetParameterValue(comando, "@numeroFatture")) > 0)
                    res = true;
            }

            return res;
        }

        //public bool DeleteOreCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        //{
        //    bool res = false;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertDelete"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

        //        if (databaseCemi.ExecuteNonQuery(comando) == 1)
        //            res = true;
        //    }

        //    return res;
        //}

        public bool DeleteDomandaTemporanea(int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeDelete"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        public bool UpdateImportoPrestazione(PrestazioneImporto importo)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiPrestazioneUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@idImporto", DbType.Int32, importo.IdImporto);
                databaseCemi.AddInParameter(comando, "@valore", DbType.Currency, importo.Valore);

                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public bool InsertImportoPrestazione(PrestazioneImporto importo)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiPrestazioneInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoImporto", DbType.Int32, importo.IdTipoImporto);
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, importo.IdDomanda);
                databaseCemi.AddInParameter(comando, "@valore", DbType.Currency, importo.Valore);

                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public Boolean InsertCassaEdileInDomanda(int idDomanda, string idCassaEdile)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeCasseEdiliInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                databaseCemi.AddInParameter(comando, "@inseritaManualmente", DbType.Boolean, true);

                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniLavoratoriSiceNewSelect"))
            {
                if (filtro.IdLavoratore.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.String, filtro.IdLavoratore.Value);
                }
                if (!String.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!String.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (filtro.DataNascita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.i
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public Lavoratore GetLavoratoreSiceNewByKey(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniLavoratoreSiceNewSelectByKey")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    Int32 indiceComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indiceProvincia = reader.GetOrdinal("indirizzoProvincia");
                    Int32 indiceCap = reader.GetOrdinal("indirizzoCap");
                    Int32 indiceCellulare = reader.GetOrdinal("cellulare");
                    Int32 indiceEmail = reader.GetOrdinal("eMail");
                    Int32 indiceIdtipoPagamento = reader.GetOrdinal("idTipoPagamento");

                    #endregion

                    while (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.Indirizzo = new Indirizzo();

                            lavoratore.Indirizzo.IndirizzoVia = reader.GetString(indiceIndirizzoDenominazione);

                            if (!reader.IsDBNull(indiceComune))
                            {
                                lavoratore.Indirizzo.Comune = reader.GetString(indiceComune);
                            }
                            if (!reader.IsDBNull(indiceProvincia))
                            {
                                lavoratore.Indirizzo.Provincia = reader.GetString(indiceProvincia);
                            }
                            if (!reader.IsDBNull(indiceCap))
                            {
                                lavoratore.Indirizzo.Cap = reader.GetString(indiceCap);
                            }
                        }
                        if (!reader.IsDBNull(indiceCellulare))
                        {
                            lavoratore.Cellulare = reader.GetString(indiceCellulare);
                        }
                        if (!reader.IsDBNull(indiceEmail))
                        {
                            lavoratore.Email = reader.GetString(indiceEmail);
                        }
                        if (!reader.IsDBNull(indiceIdtipoPagamento))
                        {
                            lavoratore.IdTipoPagamento = reader.GetString(indiceIdtipoPagamento);
                        }
                    }
                }
            }

            return lavoratore;
        }

        public TipoPrestazione GetTipoPrestazione(string gradoParentela, string idTipoPrestazione)
        {
            TipoPrestazione tipoPrestazione = new TipoPrestazione();

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniConfigurazioneSelectSingle")
                )
            {
                databaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, gradoParentela);
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    tipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                    tipoPrestazione.Descrizione = (string) reader["descrizione"];

                    //leggo anche il periodo di validit�
                    if (!Convert.IsDBNull(reader["periodoInserimentoInizio"]))
                        tipoPrestazione.ValidaDa = (DateTime) reader["periodoInserimentoInizio"];
                    if (!Convert.IsDBNull(reader["periodoInserimentoFine"]))
                        tipoPrestazione.ValidaA = (DateTime) reader["periodoInserimentoFine"];

                    tipoPrestazione.GradoParentela = gradoParentela;
                }
            }

            return tipoPrestazione;
        }

        #region Metodi relativi a PrestazioniCRM

        /// <summary>
        ///   Permette di selezionare una prestazione a partire da una domanda di prestazione SICEINFO
        /// </summary>
        /// <param name = "idPrestazioneDomanda"></param>
        /// <returns></returns>
        public PrestazioneCRMCollection GetPrestazioniCRMColletcion(int idPrestazioneDomanda)
        {
            DateTime dataLiquidazione = DateTime.Today;
            PrestazioneCRMCollection listaPrestazioni = new PrestazioneCRMCollection();

            #region Query

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CRM_PrestazioniDomandeSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, idPrestazioneDomanda);

                using (IDataReader dataReader = databaseCemi.ExecuteReader(comando))
                {
                    if (dataReader != null)
                    {
                        #region Get ordinal index

                        int idTipoPrestazioneIndex = dataReader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = dataReader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = dataReader.GetOrdinal("numeroProtocolloPrestazione");
                        int descrizioneIndex = dataReader.GetOrdinal("descrizione");
                        int beneficiarioIndex = dataReader.GetOrdinal("beneficiario");
                        int dataDomandaIndex = dataReader.GetOrdinal("dataDomanda");
                        int dataRiferimentoIndex = dataReader.GetOrdinal("dataRiferimento");
                        int idLavoratoreIndex = dataReader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = dataReader.GetOrdinal("idFamiliare");
                        int statoIndex = dataReader.GetOrdinal("stato");
                        int causaleRespintaIndex = dataReader.GetOrdinal("causaleRespinta");
                        int lavoratoreIndirizzoIndex = dataReader.GetOrdinal("lavoratoreIndirizzo");
                        int lavoratoreProvinciaIndex = dataReader.GetOrdinal("lavoratoreProvincia");
                        int lavoratoreComuneIndex = dataReader.GetOrdinal("lavoratoreComune");
                        int lavoratoreFrazioneIndex = dataReader.GetOrdinal("lavoratoreFrazione");
                        int lavoratoreCapIndex = dataReader.GetOrdinal("lavoratoreCap");
                        int lavoratoreCellulareIndex = dataReader.GetOrdinal("lavoratoreCellulare");
                        int lavoratoreEmailIndex = dataReader.GetOrdinal("lavoratoreEmail");
                        int lavoratoreIdTipoPagamentoIndex = dataReader.GetOrdinal("lavoratoreIdTipoPagamento");
                        int familiareCognomeIndex = dataReader.GetOrdinal("familiareCognome");
                        int familiareNomeIndex = dataReader.GetOrdinal("familiareNome");
                        int familiareDataNascitaIndex = dataReader.GetOrdinal("familiareDataNascita");
                        int familiareCodiceFiscaleIndex = dataReader.GetOrdinal("familiareCodiceFiscale");
                        int gradoParentelaIndex = dataReader.GetOrdinal("gradoParentela");
                        int importoErogatoLordoIndex = dataReader.GetOrdinal("importoErogatoLordo");
                        int importoErogatoIndex = dataReader.GetOrdinal("importoErogato");
                        int modalitaPagamentoIndex = dataReader.GetOrdinal("modalitaPagamento");
                        int numeroMandatoIndex = dataReader.GetOrdinal("numeroMandato");
                        int statoAssegnoIndex = dataReader.GetOrdinal("statoAssegno");
                        int descrizioneStatoAssegnoIndex = dataReader.GetOrdinal("descrizioneStatoAssegno");
                        int dataLiquidazioneIndex = dataReader.GetOrdinal("dataLiquidazione");
                        int idTipoPagamentoIndex = dataReader.GetOrdinal("idTipoPagamento");
                        int ibanIndex = dataReader.GetOrdinal("IBAN");

                        #endregion

                        dataReader.Read();

                        PrestazioneCrm prestazioneCrm = new PrestazioneCrm();

                        // chiavi
                        prestazioneCrm.IdTipoPrestazione = dataReader.GetString(idTipoPrestazioneIndex);
                        prestazioneCrm.ProtocolloPrestazione = dataReader.GetInt32(protocolloPrestazioneIndex);
                        prestazioneCrm.NumeroProtocolloPrestazione =
                            dataReader.GetInt32(numeroProtocolloPrestazioneIndex);

                        if (dataReader[beneficiarioIndex] != DBNull.Value)
                            prestazioneCrm.Beneficiario = dataReader.GetString(beneficiarioIndex);

                        //Con questa property viene impostata anche la lista CausaliRespinta che pu� al massimo contenere un valore!
                        if (dataReader[causaleRespintaIndex] != DBNull.Value)
                            prestazioneCrm.CausaleRespinta = dataReader.GetString(causaleRespintaIndex);

                        if (dataReader[dataDomandaIndex] != DBNull.Value)
                            prestazioneCrm.DataDomanda = dataReader.GetDateTime(dataDomandaIndex);
                        if (dataReader[dataLiquidazioneIndex] != DBNull.Value)
                            prestazioneCrm.DataLiquidazione = dataReader.GetDateTime(dataLiquidazioneIndex);
                        if (dataReader[dataRiferimentoIndex] != DBNull.Value)
                            prestazioneCrm.DataRiferimento = dataReader.GetDateTime(dataRiferimentoIndex);
                        if (dataReader[descrizioneIndex] != DBNull.Value)
                            prestazioneCrm.Descrizione = dataReader.GetString(descrizioneIndex);
                        if (dataReader[descrizioneStatoAssegnoIndex] != DBNull.Value)
                            prestazioneCrm.DescrizioneStatoAssegno =
                                dataReader.GetString(descrizioneStatoAssegnoIndex);
                        if (dataReader[familiareCodiceFiscaleIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareCodiceFiscale =
                                dataReader.GetString(familiareCodiceFiscaleIndex);
                        if (dataReader[familiareCognomeIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareCognome = dataReader.GetString(familiareCognomeIndex);
                        if (dataReader[familiareDataNascitaIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareDataNascita =
                                dataReader.GetDateTime(familiareDataNascitaIndex);
                        if (dataReader[familiareNomeIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareNome = dataReader.GetString(familiareNomeIndex);
                        if (dataReader[gradoParentelaIndex] != DBNull.Value)
                            prestazioneCrm.GradoParentela = dataReader.GetString(gradoParentelaIndex);
                        if (dataReader[ibanIndex] != DBNull.Value)
                            prestazioneCrm.Iban = dataReader.GetString(ibanIndex);
                        if (dataReader[idFamiliareIndex] != DBNull.Value)
                            prestazioneCrm.IdFamiliare = dataReader.GetInt32(idFamiliareIndex);
                        if (dataReader[idLavoratoreIndex] != DBNull.Value)
                            prestazioneCrm.IdLavoratore = dataReader.GetInt32(idLavoratoreIndex);
                        if (dataReader[idTipoPagamentoIndex] != DBNull.Value)
                            prestazioneCrm.IdTipoPagamento = dataReader.GetString(idTipoPagamentoIndex);
                        if (dataReader[importoErogatoIndex] != DBNull.Value)
                            prestazioneCrm.ImportoErogato = dataReader.GetDecimal(importoErogatoIndex);
                        if (dataReader[importoErogatoLordoIndex] != DBNull.Value)
                            prestazioneCrm.ImportoErogatoLordo = dataReader.GetDecimal(importoErogatoLordoIndex);
                        if (dataReader[lavoratoreCapIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreCap = dataReader.GetString(lavoratoreCapIndex);
                        if (dataReader[lavoratoreCellulareIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreCellulare = dataReader.GetString(lavoratoreCellulareIndex);
                        if (dataReader[lavoratoreComuneIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreComune = dataReader.GetString(lavoratoreComuneIndex);
                        if (dataReader[lavoratoreEmailIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreEmail = dataReader.GetString(lavoratoreEmailIndex);
                        if (dataReader[lavoratoreFrazioneIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreFrazione = dataReader.GetString(lavoratoreFrazioneIndex);
                        if (dataReader[lavoratoreIdTipoPagamentoIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreIdTipoPagamento =
                                dataReader.GetString(lavoratoreIdTipoPagamentoIndex);
                        if (dataReader[lavoratoreIndirizzoIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreIndirizzo = dataReader.GetString(lavoratoreIndirizzoIndex);
                        if (dataReader[lavoratoreProvinciaIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreProvincia = dataReader.GetString(lavoratoreProvinciaIndex);
                        if (dataReader[modalitaPagamentoIndex] != DBNull.Value)
                            prestazioneCrm.ModalitaPagamento = dataReader.GetString(modalitaPagamentoIndex);
                        if (dataReader[numeroMandatoIndex] != DBNull.Value)
                            prestazioneCrm.NumeroMandato = dataReader.GetString(numeroMandatoIndex);
                        if (dataReader[statoIndex] != DBNull.Value)
                            prestazioneCrm.Stato = dataReader.GetString(statoIndex);
                        if (dataReader[statoAssegnoIndex] != DBNull.Value)
                            prestazioneCrm.StatoAssegno = dataReader.GetString(statoAssegnoIndex);

                        //TODO leggere il DS dei documenti mancanti
                        // Documenti in attesa
                        dataReader.NextResult();
                        DocumentoCollection documenti = new DocumentoCollection();
                        while (dataReader.Read())
                        {
                            Documento documento = new Documento();

                            if (!Convert.IsDBNull(dataReader["cognome"]))
                                documento.LavoratoreCognome = (string) dataReader["cognome"];
                            if (!Convert.IsDBNull(dataReader["nome"]))
                                documento.LavoratoreNome = (string) dataReader["nome"];
                            if (!Convert.IsDBNull(dataReader["idArchidoc"]))
                                documento.IdArchidoc = (string) dataReader["idArchidoc"];
                            if (!Convert.IsDBNull(dataReader["idDocumento"]))
                                documento.IdDocumento = (int) dataReader["idDocumento"];
                            if (!Convert.IsDBNull(dataReader["originale"]))
                                documento.Originale = (bool) dataReader["originale"];

                            if (!Convert.IsDBNull(dataReader["univocoPrestazione"]))
                                documento.PerPrestazione = Convert.ToBoolean((int) dataReader["univocoPrestazione"]);
                            documento.RiferitoA = (string) dataReader["documentoRiferitoA"];
                            documento.TipoDocumento = new TipoDocumento();
                            documento.TipoDocumento.IdTipoDocumento =
                                Convert.ToInt16((int) dataReader["idTipoDocumento"]);
                            documento.TipoDocumento.Descrizione = (string) dataReader["descrizioneDocumentoRichiesto"];

                            documenti.Add(documento);
                        }

                        prestazioneCrm.Documenti = documenti;

                        listaPrestazioni.Add(prestazioneCrm);
                    }
                }
            }

            #endregion

            return listaPrestazioni;
        }

        #endregion

        #region Get tipi

        /// <summary>
        ///   ritorna i tipi decesso (necessari alla prestazione funerario)
        /// </summary>
        /// <returns></returns>
        public TipoDecessoCollection GetTipiDecesso()
        {
            TipoDecessoCollection tipiDecesso = new TipoDecessoCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_TipiPrestazioniDecessiSelect"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoDecesso = reader.GetOrdinal("idTipoPrestazioniDecesso");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoDecesso tipoDecesso = new TipoDecesso();
                        tipiDecesso.Add(tipoDecesso);

                        tipoDecesso.IdTipoDecesso = reader.GetString(indiceIdTipoDecesso);
                        tipoDecesso.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiDecesso;
        }

        public TipoScuolaCollection GetTipiScuola(string idTipoPrestazione)
        {
            TipoScuolaCollection tipiScuola = new TipoScuolaCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniTipiScuolaSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoScuola = reader.GetOrdinal("idTipoScuola");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoScuola tipoScuola = new TipoScuola();
                        tipiScuola.Add(tipoScuola);

                        tipoScuola.IdTipoScuola = reader.GetInt32(indiceIdTipoScuola);
                        tipoScuola.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiScuola;
        }

        public TipoPromozioneCollection GetTipiPromozione(Int32 idTipoScuola)
        {
            TipoPromozioneCollection tipiPromozione = new TipoPromozioneCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniTipiPromozioneSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32, idTipoScuola);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoPromozione = reader.GetOrdinal("idTipoPromozione");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoPromozione tipoPromozione = new TipoPromozione();
                        tipiPromozione.Add(tipoPromozione);

                        tipoPromozione.IdTipoPromozione = reader.GetInt32(indiceIdTipoPromozione);
                        tipoPromozione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiPromozione;
        }

        #endregion

        #region Dati aggiuntivi insert update

        public bool UpdateDatiAggiuntiviScolastiche(Int32 idDomanda, DatiScolastiche datiScolastiche)
        {
            bool res = false;

            if (datiScolastiche.IdDatiScolastiche.HasValue)
            {
                using (
                    DbCommand comando =
                        databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDatiAggiuntiviUpdate"))
                {
                    databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                    databaseCemi.AddInParameter(comando, "@idDatiAggiuntivi", DbType.Int32,
                                                datiScolastiche.IdDatiScolastiche.Value);
                    databaseCemi.AddInParameter(comando, "@annoFrequenza", DbType.Int16, datiScolastiche.AnnoFrequenza);
                    if (datiScolastiche.ClasseConclusa.HasValue)
                        databaseCemi.AddInParameter(comando, "@classeConclusa", DbType.Int16,
                                                    datiScolastiche.ClasseConclusa.Value);
                    if (datiScolastiche.TipoScuola != null)
                        databaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32,
                                                    datiScolastiche.TipoScuola.IdTipoScuola);
                    if (datiScolastiche.TipoPromozione != null)
                        databaseCemi.AddInParameter(comando, "@idTipoPromozione", DbType.Int32,
                                                    datiScolastiche.TipoPromozione.IdTipoPromozione);
                    if (datiScolastiche.NumeroMesiFrequentati.HasValue)
                        databaseCemi.AddInParameter(comando, "@numeroMesiFrequentati", DbType.Int16,
                                                    datiScolastiche.NumeroMesiFrequentati.Value);
                    if (datiScolastiche.AnnoImmatricolazione.HasValue)
                        databaseCemi.AddInParameter(comando, "@annoImmatricolazione", DbType.Int16,
                                                    datiScolastiche.AnnoImmatricolazione.Value);
                    if (datiScolastiche.NumeroAnniFrequentati.HasValue)
                        databaseCemi.AddInParameter(comando, "@numeroAnniFrequentati", DbType.Int16,
                                                    datiScolastiche.NumeroAnniFrequentati.Value);
                    if (datiScolastiche.NumeroAnniFuoriCorso.HasValue)
                        databaseCemi.AddInParameter(comando, "@numeroAnniFuoriCorso", DbType.Int16,
                                                    datiScolastiche.NumeroAnniFuoriCorso.Value);
                    if (datiScolastiche.MediaVoti.HasValue)
                        databaseCemi.AddInParameter(comando, "@mediaVoti", DbType.Decimal,
                                                    datiScolastiche.MediaVoti.Value);
                    if (datiScolastiche.NumeroEsamiSostenuti.HasValue)
                        databaseCemi.AddInParameter(comando, "@numeroEsamiSostenuti", DbType.Int16,
                                                    datiScolastiche.NumeroEsamiSostenuti.Value);
                    if (datiScolastiche.CfuConseguiti.HasValue)
                        databaseCemi.AddInParameter(comando, "@cfuConseguiti", DbType.Int16,
                                                    datiScolastiche.CfuConseguiti.Value);
                    if (datiScolastiche.CfuPrevisti.HasValue)
                        databaseCemi.AddInParameter(comando, "@cfuPrevisti", DbType.Int16,
                                                    datiScolastiche.CfuPrevisti.Value);

                    //applica deduzione, vale per c003-123, C002-L
                    if (!string.IsNullOrEmpty(datiScolastiche.ApplicaDeduzione))
                        databaseCemi.AddInParameter(comando, "@applicaDeduzione", DbType.String,
                                                    datiScolastiche.ApplicaDeduzione);


                    //Funerario
                    if (datiScolastiche.DataDecesso.HasValue)
                        databaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime,
                                                    datiScolastiche.DataDecesso.Value);
                    if (!string.IsNullOrEmpty(datiScolastiche.IdTipoPrestazioneDecesso))
                        databaseCemi.AddInParameter(comando, "@idTipoPrestazioneDecesso", DbType.String,
                                                    datiScolastiche.IdTipoPrestazioneDecesso);
                    if (datiScolastiche.IdFamiliareErede.HasValue)
                        databaseCemi.AddInParameter(comando, "@idFamiliareErede", DbType.Int16,
                                                    datiScolastiche.IdFamiliareErede.Value);

                    //handicap
                    if (datiScolastiche.AnnoDaErogare.HasValue)
                        databaseCemi.AddInParameter(comando, "@annoDaErogare", DbType.Int16,
                                                    datiScolastiche.AnnoDaErogare.Value);
                    if (datiScolastiche.TipoDaErogare.HasValue)
                        databaseCemi.AddInParameter(comando, "@tipoDaErogare", DbType.Int16,
                                                    datiScolastiche.TipoDaErogare.Value);

                    // Asilo Nido
                    if (datiScolastiche.PeriodoDal.HasValue)
                        databaseCemi.AddInParameter(comando, "@periodoFrequenzaDal", DbType.DateTime,
                                                    datiScolastiche.PeriodoDal.Value);
                    if (datiScolastiche.PeriodoAl.HasValue)
                        databaseCemi.AddInParameter(comando, "@periodoFrequenzaAl", DbType.DateTime,
                                                    datiScolastiche.PeriodoAl.Value);

                    if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                        res = true;
                }
            }

            return res;
        }

        public bool InsertDatiAggiuntiviScolastiche(Int32 idDomanda, DatiScolastiche datiScolastiche)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDatiAggiuntiviInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@annoFrequenza", DbType.Int16, datiScolastiche.AnnoFrequenza);
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String,
                                            datiScolastiche.IdTipoPrestazione);

                if (datiScolastiche.ClasseConclusa.HasValue)
                    databaseCemi.AddInParameter(comando, "@classeConclusa", DbType.Int16,
                                                datiScolastiche.ClasseConclusa.Value);
                if (datiScolastiche.TipoScuola != null)
                    databaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32,
                                                datiScolastiche.TipoScuola.IdTipoScuola);
                if (datiScolastiche.TipoPromozione != null)
                    databaseCemi.AddInParameter(comando, "@idTipoPromozione", DbType.Int32,
                                                datiScolastiche.TipoPromozione.IdTipoPromozione);
                if (datiScolastiche.NumeroMesiFrequentati.HasValue)
                    databaseCemi.AddInParameter(comando, "@numeroMesiFrequentati", DbType.Int16,
                                                datiScolastiche.NumeroMesiFrequentati.Value);
                if (datiScolastiche.AnnoImmatricolazione.HasValue)
                    databaseCemi.AddInParameter(comando, "@annoImmatricolazione", DbType.Int16,
                                                datiScolastiche.AnnoImmatricolazione.Value);
                if (datiScolastiche.NumeroAnniFrequentati.HasValue)
                    databaseCemi.AddInParameter(comando, "@numeroAnniFrequentati", DbType.Int16,
                                                datiScolastiche.NumeroAnniFrequentati.Value);
                if (datiScolastiche.NumeroAnniFuoriCorso.HasValue)
                    databaseCemi.AddInParameter(comando, "@numeroAnniFuoriCorso", DbType.Int16,
                                                datiScolastiche.NumeroAnniFuoriCorso.Value);
                if (datiScolastiche.MediaVoti.HasValue)
                    databaseCemi.AddInParameter(comando, "@mediaVoti", DbType.Decimal, datiScolastiche.MediaVoti.Value);
                if (datiScolastiche.NumeroEsamiSostenuti.HasValue)
                    databaseCemi.AddInParameter(comando, "@numeroEsamiSostenuti", DbType.Int16,
                                                datiScolastiche.NumeroEsamiSostenuti.Value);
                if (datiScolastiche.CfuConseguiti.HasValue)
                    databaseCemi.AddInParameter(comando, "@cfuConseguiti", DbType.Int16,
                                                datiScolastiche.CfuConseguiti.Value);
                if (datiScolastiche.CfuPrevisti.HasValue)
                    databaseCemi.AddInParameter(comando, "@cfuPrevisti", DbType.Int16, datiScolastiche.CfuPrevisti.Value);

                //applica deduzione, vale per c003-123, C002-L
                if (!string.IsNullOrEmpty(datiScolastiche.ApplicaDeduzione))
                    databaseCemi.AddInParameter(comando, "@applicaDeduzione", DbType.String,
                                                datiScolastiche.ApplicaDeduzione);

                //Funerario
                if (datiScolastiche.DataDecesso.HasValue)
                    databaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime,
                                                datiScolastiche.DataDecesso.Value);
                if (!string.IsNullOrEmpty(datiScolastiche.IdTipoPrestazioneDecesso))
                    databaseCemi.AddInParameter(comando, "@idTipoPrestazioneDecesso", DbType.String,
                                                datiScolastiche.IdTipoPrestazioneDecesso);
                if (datiScolastiche.IdFamiliareErede.HasValue)
                    databaseCemi.AddInParameter(comando, "@idFamiliareErede", DbType.Int32,
                                                datiScolastiche.IdFamiliareErede.Value);

                //handicap
                if (datiScolastiche.AnnoDaErogare.HasValue)
                    databaseCemi.AddInParameter(comando, "@annoDaErogare", DbType.Int16,
                                                datiScolastiche.AnnoDaErogare.Value);
                if (datiScolastiche.TipoDaErogare.HasValue)
                    databaseCemi.AddInParameter(comando, "@tipoDaErogare", DbType.Int16,
                                                datiScolastiche.TipoDaErogare.Value);

                // Asilo Nido
                if (datiScolastiche.PeriodoDal.HasValue)
                    databaseCemi.AddInParameter(comando, "@periodoFrequenzaDal", DbType.DateTime,
                                                datiScolastiche.PeriodoDal.Value);
                if (datiScolastiche.PeriodoAl.HasValue)
                    databaseCemi.AddInParameter(comando, "@periodoFrequenzaAl", DbType.DateTime,
                                                datiScolastiche.PeriodoAl.Value);

                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Forzatura Controlli

        public bool ForzaLavoratoreIndirizzo(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloLavoratoreIndirizzo"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaPresenzaDocumenti(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloDocumenti"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaUnivocita(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaUnivocita"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaLavoratoreEmail(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloLavoratoreEmail"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaLavoratoreCellulare(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloLavoratoreCellulare"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareDataNascita(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareDataNascita"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareDataDecesso(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareDataDecesso"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareCodiceFiscale(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareCodiceFiscale"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareACarico(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareACarico"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloOreCNCE(int idDomanda, bool stato)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloOreCNCE"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@stato", DbType.Boolean, stato);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloPeriodoFrequenza180Giorni(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloScolastichePeriodoFrequenza180Giorni"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloPeriodoFrequenza3Anni(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloScolastichePeriodoFrequenza3Anni"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloAnnoFrequenza(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloScolasticheAnnoFrequenza"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region FATTURA

        /*
        USP_PrestazioniFatturaSelectPerIdFattura
        USP_PrestazioniFatturaSelectPerIdArchidoc
        USP_PrestazioniFattureInsert
        USP_PrestazioniFattureUpdatePerIdPrestazioniFattura
        USP_PrestazioniFattureUpdateSetValidaPerIdPrestazioniFattura
         
         */

        public Fattura getFatturaPerIdPrestazioniFattura(int idPrestazioniFattura)
        {
            Fattura fattura;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaSelectPerIdFattura")
                )
            {
                databaseCemi.AddInParameter(comando, "idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    fattura = new Fattura();
                    fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                    if (!Convert.IsDBNull(reader["idArchidoc"]))
                        fattura.IdArchidoc = (string) reader["idArchidoc"];
                    if (!Convert.IsDBNull(reader["idLavoratore"]))
                        fattura.IdLavoratore = (int) reader["idLavoratore"];
                    fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                    fattura.ImportoTotale = (decimal) reader["totale"];
                    fattura.Numero = (string) reader["numero"];
                    fattura.Data = (DateTime) reader["data"];
                    if (!Convert.IsDBNull(reader["dataAssociazione"]))
                        fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                    if (!Convert.IsDBNull(reader["dataValidazione"]))
                        fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                    if (!Convert.IsDBNull(reader["valida"]))
                        fattura.Valida = (bool) reader["valida"];
                }
            }
            return fattura;
        }

        public Fattura getFatturaPerIdArchidoc(int idArchidoc)
        {
            Fattura fattura;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaGetByIdArchidoc"))
            {
                databaseCemi.AddInParameter(comando, "idArchidoc", DbType.Int32, idArchidoc);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    fattura = new Fattura();
                    fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                    if (!Convert.IsDBNull(reader["idArchidoc"]))
                        fattura.IdArchidoc = (string) reader["idArchidoc"];
                    if (!Convert.IsDBNull(reader["idLavoratore"]))
                        fattura.IdLavoratore = (int) reader["idLavoratore"];
                    fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                    fattura.ImportoTotale = (decimal) reader["totale"];
                    fattura.Numero = (string) reader["numero"];
                    fattura.Data = (DateTime) reader["data"];
                    if (!Convert.IsDBNull(reader["dataAssociazione"]))
                        fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                    if (!Convert.IsDBNull(reader["dataValidazione"]))
                        fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                    if (!Convert.IsDBNull(reader["valida"]))
                        fattura.Valida = (bool) reader["valida"];
                }
            }
            return fattura;
        }

        public bool updateFatturaPerIdPrestazioniFattura(Fattura fattura, DbTransaction transaction)
        {
            bool res = false;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, fattura.IdPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@data", DbType.Date, fattura.Data);
                databaseCemi.AddInParameter(comando, "@totale", DbType.Currency, fattura.ImportoTotale);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddInParameter(comando, "@valida", DbType.Boolean, fattura.Valida);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, fattura.IdLavoratore);
                databaseCemi.AddInParameter(comando, "@idArchidoc", DbType.Int32, fattura.IdArchidoc);
                databaseCemi.AddInParameter(comando, "@dataAssociazione", DbType.DateTime, fattura.DataAssociazione);
                databaseCemi.AddInParameter(comando, "@dataValidazione", DbType.DateTime, fattura.DataValidazione);
                //databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool fatturaDichiarataAssociaFatturaFisica(int idPrestazioniFattura, int idPrestazioniFatturaDichiarata,
                                                          DbTransaction transaction)
        {
            bool res = false;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateAssociaIdPrestazioniFattura")
                )
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);


                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool fatturaDichiarataDisassociaFatturaFisica(int idPrestazioniFatturaDichiarata,
                                                             DbTransaction transaction)
        {
            bool res = false;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateDisassociaIdPrestazioniFattura"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool insertFattura(Fattura fattura, DbTransaction transaction)
        {
            bool res = false;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureInsert"))
            {
                //databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, fattura.IdPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@data", DbType.Date, fattura.Data);
                databaseCemi.AddInParameter(comando, "@totale", DbType.Currency, fattura.ImportoTotale);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddInParameter(comando, "@valida", DbType.Boolean, fattura.Valida);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, fattura.IdLavoratore);
                databaseCemi.AddInParameter(comando, "@idArchidoc", DbType.Int32, fattura.IdArchidoc);
                databaseCemi.AddInParameter(comando, "@dataAssociazione", DbType.DateTime, fattura.DataAssociazione);
                databaseCemi.AddInParameter(comando, "@dataValidazione", DbType.DateTime, fattura.DataValidazione);
                //databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public FatturaCollection getFatturePerIdFatturaDichiarata(int idPrestazioniFatturaDichiarata)
        {
            FatturaCollection fatture = new FatturaCollection();
            Fattura fattura;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaSelectPerIdFatturaDichiarata"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        fattura = new Fattura();
                        fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            fattura.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                            fattura.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["totale"]))
                            fattura.ImportoTotale = (decimal) reader["totale"];
                        if (!Convert.IsDBNull(reader["numero"]))
                            fattura.Numero = (string) reader["numero"];
                        if (!Convert.IsDBNull(reader["data"]))
                            fattura.Data = (DateTime) reader["data"];
                        if (!Convert.IsDBNull(reader["dataAssociazione"]))
                            fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                        if (!Convert.IsDBNull(reader["dataValidazione"]))
                            fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                        if (!Convert.IsDBNull(reader["valida"]))
                            fattura.Valida = (bool) reader["valida"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            fattura.Originale = (bool) reader["originale"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            fattura.DataScansione = (DateTime) reader["dataScansione"];

                        fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                        fatture.Add(fattura);
                    }
                }
            }
            return fatture;
        }

        public FatturaCollection getFattureFisicheAssociabili(DateTime? data, string numero, decimal? importo,
                                                              bool adaptSearch)
        {
            FatturaCollection fatture = new FatturaCollection();
            Fattura fattura;

            using
                (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaSelectNonAssociatePerDataNumeroImporto")
                )
            {
                if (data.HasValue)
                    databaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data.Value);
                if (!string.IsNullOrEmpty(numero))
                    databaseCemi.AddInParameter(comando, "@numero", DbType.String, numero);
                if (importo.HasValue)
                    databaseCemi.AddInParameter(comando, "@importo", DbType.Decimal, importo.Value);
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        fattura = new Fattura();

                        fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];

                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            fattura.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                            fattura.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                            fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];
                        if (!Convert.IsDBNull(reader["totale"]))
                            fattura.ImportoTotale = (decimal) reader["totale"];
                        if (!Convert.IsDBNull(reader["numero"]))
                            fattura.Numero = (string) reader["numero"];
                        if (!Convert.IsDBNull(reader["data"]))
                            fattura.Data = (DateTime) reader["data"];
                        if (!Convert.IsDBNull(reader["dataAssociazione"]))
                            fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                        if (!Convert.IsDBNull(reader["dataValidazione"]))
                            fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                        if (!Convert.IsDBNull(reader["valida"]))
                            fattura.Valida = (bool) reader["valida"];

                        fatture.Add(fattura);
                    }
                }
            }
            return fatture;
        }

        #endregion

        #region FATTURADICHIARATA

        /*
            USP_PrestazioniFattureDichiarateInsert
            USP_PrestazioniFattureDichiarateUpdate
            USP_PrestazioniFattureDichiarateSelectPerIdPrestazioniFatturaDichiarata
        
         */

        public FatturaDichiarata getFatturaDichiarataPerIdPrestazioniFatturaDichiarata(
            int idPrestazioniFatturaDichiarata, DbTransaction transaction)
        {
            FatturaDichiarata fattura;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateSelectPerIdPrestazioniFatturaDichiarata"))
            {
                databaseCemi.AddInParameter(comando, "idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    fattura = new FatturaDichiarata();

                    fattura.IdPrestazioniFatturaDichiarata = (int) reader["idPrestazioniFatturaDichiarata"];
                    fattura.IdPrestazioniDomanda = (int) reader["idPrestazioniDomanda"];
                    fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];

                    fattura.Data = (DateTime) reader["data"];
                    fattura.Numero = (string) reader["numero"];
                    fattura.ImportoTotale = (decimal) reader["importoTotale"];
                    fattura.Saldo = (bool) reader["saldo"];

                    fattura.DataAnnullamento = (DateTime) reader["dataAnnullamento"];
                    fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];
                }
            }
            return fattura;
        }

        public bool UpdateFatturaDichiarataPerIdPrestazioniFatturaDichiarata(FatturaDichiarata fattura,
                                                                             DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            fattura.IdPrestazioniFatturaDichiarata);
                databaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, fattura.IdPrestazioniDomanda);
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, fattura.IdPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                databaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);
                databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);
                //                databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool InsertFatturaDichiarataFULL(FatturaDichiarata fattura, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateInsert"))
            {
                //databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32, fattura.IdPrestazioneFatturaDichiarata);
                databaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, fattura.IdPrestazioniDomanda);
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, fattura.IdPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                databaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                databaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);
                databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);
                //                databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool AnnullaFatturaDichiarataPerIdFatturaDichiarata(int idPrestazioniFatturaDichiarata,
                                                                   DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateAnnullaPerIdPrestazioniFatturaDichiarata"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);
                //databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool RipristinaAnnullamentoFatturaDichiarataPerIdFatturaDichiarata(int idPrestazioniFatturaDichiarata,
                                                                                  DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateRipristinaAnnullamentoPerIdPrestazioniFatturaDichiarata"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);
                //databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool PrestazioniFatturaAggiornaValidazione(int idPrestazioniFattura, bool valida,
                                                          DbTransaction transaction)
        {
            bool res = false;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureUpdateValidazionePerIdPrestazioniFattura"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@valida", DbType.Boolean, valida);

                //databaseCemi.AddInParameter(comando, "@dataValidazione", DbType.DateTime, );
                //databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }
            return res;
        }

        #endregion

        #region ImportiFattura

        /*
        USP_GetFattureImportiPerIdPrestazioniFattura
        USP_PrestazioniImportiFattureUpdate
        USP_PrestazioniImportiFattureGeneraPerIdTipoPrestazione
        USP_PrestazioniImportiFattureInsert
        USP_PrestazioniImportiFattureInsertChecked
         */

        public FatturaImportoCollection getImportiFatturePerIdPrestazioniFattura(int idPrestazioniFattura)
        {
            FatturaImportoCollection importi = new FatturaImportoCollection();
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_FattureImportiSelectPerIdPrestazioniFattura"))
            {
                databaseCemi.AddInParameter(comando, "idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    FatturaImporto importo;
                    while (reader.Read())
                    {
                        importo = new FatturaImporto();

                        importo.IdPrestazioneImportoFattura = (int) reader["idPrestazioneImportoFattura"];
                        importo.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        importo.IdTipoPrestazioneImportoFattura = (int) reader["idTipoPrestazioneImportoFattura"];
                        importo.Descrizione = (string) reader["descrizione"];
                        //importo.DescrizioneTipoPrestazione = (string)reader["descrizioneTipoPrestazione"]; ;
                        importo.Valore = (decimal) reader["valore"];

                        importi.Add(importo);
                    }
                }
            }
            return importi;
        }

        public bool UpdateImportoFattura(FatturaImporto importo, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiFattureUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioneImportoFattura", DbType.Int32,
                                            importo.IdTipoPrestazioneImportoFattura);
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, importo.IdPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@idTipoPrestazioneImportoFattura", DbType.Int32,
                                            importo.IdTipoPrestazioneImportoFattura);
                databaseCemi.AddInParameter(comando, "@valore", DbType.Decimal, importo.Valore);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool fattureImportiUpdatePerIdPrestazioneImportoFattura(int idPrestazioneImportoFattura, decimal valore,
                                                                       DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniImportiFattureUpdateValorePeridPrestazioneImportoFattura"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioneImportoFattura", DbType.Int32,
                                            idPrestazioneImportoFattura);
                databaseCemi.AddInParameter(comando, "@valore", DbType.Decimal, valore);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool InsertImportoFatturaGeneraRecord(string idTipoPrestazione, int idPrestazioniFattura,
                                                     DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiFattureGeneraPerIdTipoPrestazione"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }


        public bool ImportoFatturaDeleteRecordPerIdPrestazioniFatturaDichiarata(int idPrestazioniFatturaDichiarata,
                                                                                DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniImportiFattureDeletePerIdPrestazioniFatturaDichiarata"))
            {
                databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                                            idPrestazioniFatturaDichiarata);

                if (transaction == null)
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region Gestione documenti richiesti

        private Boolean InsertDocumentoRichiestoConsegnato(Documento documento, Int32 idDomanda,
                                                           DbTransaction transaction)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDocumentiConsegnatiInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, documento.IdTipoPrestazione);
                databaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, documento.BeneficiarioPrestazione);
                databaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16,
                                            documento.TipoDocumento.IdTipoDocumento);
                databaseCemi.AddInParameter(comando, "@consegnato", DbType.Boolean, documento.Consegnato);

                if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private Boolean DeleteDocumentoRichiestoConsegnato(Int32 idDomanda, DbTransaction transaction)
        {
            //todo fare un controllo su come si comporta (true messo per test)
            Boolean res = true;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDocumentiConsegnatiDelete"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Metodi per filtri ricerca
        public void InsertFiltroRicerca(Int32 idUtente, DomandaFilter filtro, Int32? pagina)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException();
            }

            XmlSerializer ser = new XmlSerializer(typeof(DomandaFilter));
            String filtroSerializzato = String.Empty;
            if (filtro != null)
            {
                using (StringWriter sw = new StringWriter())
                {
                    ser.Serialize(sw, filtro);
                    filtroSerializzato = sw.ToString();
                }
            }


            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFiltriRicercaInsertUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                if (filtro != null)
                {
                    databaseCemi.AddInParameter(comando, "@filtro", DbType.Xml, filtroSerializzato);
                    databaseCemi.AddInParameter(comando, "@pagina", DbType.Int32, pagina.Value);
                }

                if (databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Errore durante l'inserimento del filtro di ricerca");
                }
            }
        }

        public DomandaFilter GetFiltroRicerca(Int32 idUtente, out Int32 pagina)
        {
            DomandaFilter filtro = null;
            pagina = 0;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFiltriRicercaSelect"))
            {
                databaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        #region Indici reader
                        Int32 indiceUserID = reader.GetOrdinal("idUtente");
                        Int32 indiceFiltro = reader.GetOrdinal("filtro");
                        Int32 indicePagina = reader.GetOrdinal("pagina");
                        #endregion

                        if (!reader.IsDBNull(indiceUserID))
                        {
                            String filtroSerializzato = null;

                            if (!reader.IsDBNull(indiceFiltro))
                            {
                                filtroSerializzato = reader.GetString(indiceFiltro);
                                pagina = reader.GetInt32(indicePagina);
                            }
                            if (!String.IsNullOrEmpty(filtroSerializzato))
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(DomandaFilter));
                                using (StringReader sr = new StringReader(filtroSerializzato))
                                {
                                    filtro = (DomandaFilter) ser.Deserialize(sr);
                                }
                            }
                        }
                    }
                }
            }

            return filtro;
        }

        public void DeleteFiltroRicerca(Int32 idUtente)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFiltriRicercaDelete"))
            {
                databaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }
        #endregion

        public void UpdateDomandaDataRiferimento(Int32 idDomanda, DateTime dataRiferimento)
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateDataRiferimento"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@dataRiferimento", DbType.DateTime, dataRiferimento);

                if (databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("UpdateDomandaDataRiferimento: non � stato possibile aggiornare la data di riferimento");
                }
            }
        }

        public void UpdateDomandaNota(Int32 idDomanda, Int32 idUtente, String nota)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateNota"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@nota", DbType.String, nota);
                databaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

                if (databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("UpdateDomandaNota: non � stato possibile aggiornare la nota");
                }
            }
        }

        public StatiCureProtesi GetStatiCureProtesi(int idDomanda)
        {
            StatiCureProtesi stati = new StatiCureProtesi();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniSelectStatiPerCureProtesiDentarie"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        String idTipoPrestazione = reader["idTipoPrestazione"] as String;
                        String stato = reader["stato"] as String;

                        switch (idTipoPrestazione)
                        {
                            case "C004CL":
                                stati.StatoCL = stato;
                                break;
                            case "C004PL":
                                stati.StatoPL = stato;
                                break;
                        }
                    }
                }
            }

            return stati;
        }
    }
}