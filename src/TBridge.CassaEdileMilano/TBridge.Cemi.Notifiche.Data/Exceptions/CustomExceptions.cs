using System;

namespace TBridge.Cemi.Notifiche.Data.Exceptions
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.Notifiche.Type")]
    public class NotificaPresenteException : Exception
    {
    }

    [Obsolete("Usare le classi presenti in TBridge.Cemi.Notifiche.Type")]
    public class DenunciaPresenteException : Exception
    {
    }
}