using System;
using System.Collections.Generic;
using TBridge.Cemi.Notifiche.Data.Entities;

namespace TBridge.Cemi.Notifiche.Data.Collections
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.Notifiche.Type")]
    public class ImpresaCollection : List<Impresa>
    {
    }
}