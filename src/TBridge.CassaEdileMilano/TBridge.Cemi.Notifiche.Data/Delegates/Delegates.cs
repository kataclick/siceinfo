using System;

namespace TBridge.Cemi.Notifiche.Data.Delegates
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.Notifiche.Type")]
    public delegate void ImpresaSelectedEventHandler();
}