using System;

namespace TBridge.Cemi.Notifiche.Data.Entities
{
    [Obsolete("Usare le classi presenti in TBridge.Cemi.Notifiche.Type")]
    public class Impresa
    {
        private int idImpresa;
        private string ragioneSociale;

        public int IdImpresa
        {
            get { return idImpresa; }
            set { idImpresa = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string NomeComposto
        {
            get { return string.Format("{0} - {1}", idImpresa, ragioneSociale); }
        }
    }
}