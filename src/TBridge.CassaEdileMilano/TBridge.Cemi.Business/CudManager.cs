﻿using System;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Business
{
    public class CudManager
    {
        private readonly TBridge.Cemi.Data.Common commonData = new TBridge.Cemi.Data.Common();

        public Cud GetCud(Int32 idLavoratore)
        {
            return commonData.GetCud(idLavoratore);
        }

        public Boolean CudScaricato(Int32 idUtente, Int32 annoRedditi)
        {
            return commonData.CudScaricato(idUtente, annoRedditi);
        }
    }
}
