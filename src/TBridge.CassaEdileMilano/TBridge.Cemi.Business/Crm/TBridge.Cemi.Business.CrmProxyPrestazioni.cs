#region WSCF
//------------------------------------------------------------------------------
// <autogenerated code>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated code>
//------------------------------------------------------------------------------
// File time 28-04-09 03.10 
//
// This source code was auto-generated by WsContractFirst, Version=0.7.6319.1
#endregion


namespace TBridge.Cemi.Business.CrmPrestazioni
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="WsPrestazioniSoap", Namespace="http://localhost/")]
    public partial class WsPrestazioni : System.Web.Services.Protocols.SoapHttpClientProtocol, IWsPrestazioni
    {
        
        private System.Threading.SendOrPostCallback ManagePrestazioniOperationCompleted;
        
        /// <remarks/>
        public WsPrestazioni()
        {
            string urlSetting = System.Configuration.ConfigurationManager.AppSettings["WsPrestazioni.ServiceEndpointURL"];
            if (((urlSetting != null) 
                        && (urlSetting != "")))
            {
                this.Url = urlSetting;
            }
            else
            {
                this.Url = "http://172.16.10.13/CEMI_CustomWSs/WsPrestazioni/WsPrestazioni.asmx";
            }
        }
        
        /// <remarks/>
        public event ManagePrestazioniCompletedEventHandler ManagePrestazioniCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://localhost/ManagePrestazioni", RequestNamespace="http://localhost/", ResponseNamespace="http://localhost/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string ManagePrestazioni([System.Xml.Serialization.XmlArrayAttribute(ElementName="lstPrestazione")] prestazioni[] lstPrestazione)
        {
            object[] results = this.Invoke("ManagePrestazioni", new object[] {
                        lstPrestazione});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void ManagePrestazioniAsync(prestazioni[] lstPrestazione)
        {
            this.ManagePrestazioniAsync(lstPrestazione, null);
        }
        
        /// <remarks/>
        public void ManagePrestazioniAsync(prestazioni[] lstPrestazione, object userState)
        {
            if ((this.ManagePrestazioniOperationCompleted == null))
            {
                this.ManagePrestazioniOperationCompleted = new System.Threading.SendOrPostCallback(this.OnManagePrestazioniOperationCompleted);
            }
            this.InvokeAsync("ManagePrestazioni", new object[] {
                        lstPrestazione}, this.ManagePrestazioniOperationCompleted, userState);
        }
        
        private void OnManagePrestazioniOperationCompleted(object arg)
        {
            if ((this.ManagePrestazioniCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.ManagePrestazioniCompleted(this, new ManagePrestazioniCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3074")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://localhost/", TypeName="prestazioni")]
    public partial class prestazioni
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(ElementName="codiceTipoPrestazione")]
        public string codiceTipoPrestazione;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(ElementName="numeroprotocollo")]
        public string numeroprotocollo;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(ElementName="protocollo")]
        public string protocollo;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(ElementName="dettaglio")]
        public System.Collections.Generic.List<dettaglioPrestazione> dettaglio;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(ElementName="causali")]
        public System.Collections.Generic.List<string> causali;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(ElementName="documenti")]
        public System.Collections.Generic.List<string> documenti;
        
        public prestazioni()
        {
        }
        
        public prestazioni(string codiceTipoPrestazione, string numeroprotocollo, string protocollo, System.Collections.Generic.List<dettaglioPrestazione> dettaglio, System.Collections.Generic.List<string> causali, System.Collections.Generic.List<string> documenti)
        {
            this.codiceTipoPrestazione = codiceTipoPrestazione;
            this.numeroprotocollo = numeroprotocollo;
            this.protocollo = protocollo;
            this.dettaglio = dettaglio;
            this.causali = causali;
            this.documenti = documenti;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.3074")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://localhost/", TypeName="dettaglioPrestazione")]
    public partial class dettaglioPrestazione
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(ElementName="chiave")]
        public int chiave;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(ElementName="valore")]
        public string valore;
        
        public dettaglioPrestazione()
        {
        }
        
        public dettaglioPrestazione(int chiave, string valore)
        {
            this.chiave = chiave;
            this.valore = valore;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053")]
    public delegate void ManagePrestazioniCompletedEventHandler(object sender, ManagePrestazioniCompletedEventArgs e);
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.3053")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class ManagePrestazioniCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        
        private object[] results;
        
        internal ManagePrestazioniCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState)
        {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}
