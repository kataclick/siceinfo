#region WSCF
//------------------------------------------------------------------------------
// <autogenerated code>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated code>
//------------------------------------------------------------------------------
// File time 13-02-09 09.39 
//
// This source code was auto-generated by WsContractFirst, Version=0.7.6319.1
#endregion


namespace TBridge.Cemi.Business.CrmIscrizione
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    public interface IIscrizioneImpresa
    {
        
        string ManageAccount(string name, string mail, string code, string iva, string idlead, string codelead, string namelead, string status);
    }
}
