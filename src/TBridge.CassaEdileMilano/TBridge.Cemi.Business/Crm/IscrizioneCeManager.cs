using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using TBridge.Cemi.Business.CrmIscrizione;
using TBridge.Cemi.IscrizioneCE.Type.Enums;

namespace TBridge.Cemi.Business.Crm
{
    public static class IscrizioneCeManager
    {
        private static readonly IscrizioneImpresa ws = new IscrizioneImpresa();
        private static readonly string logIscrizione = ConfigurationManager.AppSettings["LogIscrizione"];
        private static readonly string username = ConfigurationManager.AppSettings["UsernameWSCrm"];
        private static readonly string password = ConfigurationManager.AppSettings["PasswordWSCrm"];
        private static readonly string domain = ConfigurationManager.AppSettings["DomainWSCrm"];

        public static void GestisciSR(string ragioneSociale, string mail, string idImpresa, string pIva, int? idConsulente, string codiceFiscaleConsulente, string ragioneSocialeConsulente, StatoDomanda? statoDomanda)
        {
            try
            {
                int? stato = null;

                switch (statoDomanda)
                {
                    case StatoDomanda.Accettata:
                        stato = 8;
                        break;
                    case StatoDomanda.Rifiutata:
                        stato = 9;
                        break;
                    case StatoDomanda.DaValutare:
                        break;
                    case StatoDomanda.SospesaAttesaIntegrazione:
                        stato = 6;
                        break;
                    case StatoDomanda.SospesaDebiti:
                        stato = 7;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("statoDomanda");
                }

                NetworkCredential credential = new NetworkCredential(username, password) { Domain = domain };
                ws.Credentials = credential;
                String result = ws.ManageAccount(ragioneSociale, mail, idImpresa, pIva, idConsulente.ToString(),
                                                 codiceFiscaleConsulente, ragioneSocialeConsulente, stato.ToString());

                // Per il Log
                if (logIscrizione == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";

                        appLog.WriteEntry(
                            String.Format(
                                "{0} : ragioneSociale={1}; mail={2}; idImpresa={3}; pIva={4}; idConsulente={5}; codiceFiscaleConsulente={6}; ragioneSocialeConsulente={7}; stato={8}; risultato={9}",
                                DateTime.Now, ragioneSociale, mail, idImpresa, pIva, idConsulente,
                                codiceFiscaleConsulente, ragioneSocialeConsulente, stato, result));
                    }
                }

            }
            catch (Exception e)
            {
                // Per il Log
                if (logIscrizione == "true")
                {
                    /*if (!EventLog.Exists("SiceInfo", "Application"))
                        EventLog.CreateEventSource("SiceInfo", "Application");

                    using (EventLog appLog = new EventLog())
                    {                        
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione CRMIscrizioneCE: {0}", e.Message));
                    }
                    */
                }
            }
        }

        /*
        public static void CreaSR(string ragioneSociale, string mail, string idImpresa, string pIva, int? idConsulente,
                                  string codiceFiscaleConsulente, string ragioneSocialeConsulente)
        {
            try
            {
                NetworkCredential credential = new NetworkCredential(username, password) {Domain = domain};
                ws.Credentials = credential;
                String result = ws.CreateAccount(ragioneSociale, mail, idImpresa, pIva, idConsulente.ToString(),
                                                 codiceFiscaleConsulente, ragioneSocialeConsulente);

                // Per il Log
                if (logIscrizione == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";

                        appLog.WriteEntry(
                            String.Format(
                                "{0} : ragioneSociale={1} mail={2} idImpresa={3} pIva={4} idConsulente={5} codiceFiscaleConsulente={6} ragioneSocialeConsulente={7} risultato={8}",
                                DateTime.Now, ragioneSociale, mail, idImpresa, pIva, idConsulente,
                                codiceFiscaleConsulente,
                                ragioneSocialeConsulente, result));
                    }
                }
            }
            catch (Exception e)
            {
                // Per il Log
                if (logIscrizione == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione: {0}", e.Message));
                    }
                }
            }
        }

        public static void AggiornaSR(string pIva, StatoDomanda statoDomanda)
        {
            try
            {
                int stato = -1;

                switch (statoDomanda)
                {
                    case StatoDomanda.Accettata:
                        stato = 8;
                        break;
                    case StatoDomanda.Rifiutata:
                        stato = 9;
                        break;
                    case StatoDomanda.DaValutare:
                        break;
                    case StatoDomanda.SospesaAttesaIntegrazione:
                        stato = 6;
                        break;
                    case StatoDomanda.SospesaDebiti:
                        stato = 7;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("statoDomanda");
                }

                NetworkCredential credential = new NetworkCredential(username, password) { Domain = domain };
                ws.Credentials = credential;
                String result = ws.UpdateRS(pIva, stato);

                // Per il Log
                if (logIscrizione == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("{0} : pIva={1} stato={2} risultato={3}", DateTime.Now,
                                                        pIva, stato, result));
                    }
                }
            }
            catch (Exception e)
            {
                // Per il Log
                if (logIscrizione == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione: {0}", e.Message));
                    }
                }
            }
        }
        */
    }
}