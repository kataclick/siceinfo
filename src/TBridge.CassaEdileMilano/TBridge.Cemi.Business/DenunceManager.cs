﻿using System;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Business
{
    public class DenunceManager
    {
        public static OreDenunciateAggregateCompleteImpresa GetOreDenunciateAggregateCompleteImpresa(int idImpresa,
                                                                                                     DateTime
                                                                                                         dataUltimaDenuncia)
        {
            OreDenunciateAggregateCompleteImpresa oreUltimaDenuncia;

            using (SICEEntities context = new SICEEntities())
            {
                oreUltimaDenuncia = (from oreComplete in context.OreDenunciateAggregateCompleteImprese
                                     where
                                         oreComplete.IdImpresa == idImpresa &&
                                         oreComplete.Data == dataUltimaDenuncia
                                     select oreComplete).SingleOrDefault();
            }

            return oreUltimaDenuncia;
        }

        public static DateTime? GetDataUltimaDenunciaImpresa(int idImpresa)
        {
            DateTime? dataUltimaDenuncia;
            using (SICEEntities context = new SICEEntities())
            {
                dataUltimaDenuncia = (from denuncia in context.Denunce
                                      group denuncia by denuncia.IdImpresa
                                      into gd
                                      where gd.Key == idImpresa
                                      select gd.Max(d => d.Data)).SingleOrDefault();
            }
            return dataUltimaDenuncia;
        }
    }
}