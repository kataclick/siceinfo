﻿namespace TBridge.Cemi.Business
{
    public class PartitaIvaManager
    {
        public static bool VerificaPartitaIva(string partitaIva)
        {
            bool res = true;

            int i;
            int s = 0;
            for (i = 0; i <= 9; i += 2)
                s += partitaIva[i] - '0';
            for (i = 1; i <= 9; i += 2)
            {
                int c = 2*(partitaIva[i] - '0');
                if (c > 9) c = c - 9;
                s += c;
            }
            if ((10 - s%10)%10 != partitaIva[10] - '0')
                res = false;

            return res;
        }
    }
}