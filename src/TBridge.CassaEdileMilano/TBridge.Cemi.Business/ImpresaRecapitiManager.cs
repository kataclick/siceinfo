﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Business
{
    public class ImpresaRecapitiManager
    {
        public ImpresaRecapiti GetImpresaRecapitiById(int idImpresa)
        {
            ImpresaRecapiti ret;

            using (SICEEntities context = new SICEEntities())
            {
                var queryImpreseRecapiti = from impresa in context.Imprese
                                           where impresa.Id == idImpresa
                                           select
                                               new ImpresaRecapiti
                                                   {
                                                       IdImpresa = impresa.Id,
                                                       CodiceFiscale = impresa.CodiceFiscale,
                                                       RagioneSociale = impresa.RagioneSociale,
                                                       StatoImpresa = impresa.stato,
                                                       IndirizzoSedeLegale =
                                                           new Indirizzo
                                                               {
                                                                   Qualificatore = impresa.TipoViaSedeLegale.Descrizione,
                                                                   NomeVia = impresa.denominazioneIndirizzoSedeLegale,
                                                                   Civico = impresa.civicoSedeLegale,
                                                                   Cap = impresa.capSedeLegale,
                                                                   Comune = impresa.localitaSedeLegale,
                                                                   Provincia = impresa.provinciaSedeLegale
                                                               },
                                                       PressoSedeLegale = impresa.pressoSedeLegale,
                                                       TelefonoSedeLegale = impresa.telefonoSedeLegale,
                                                       FaxSedeLegale = impresa.faxSedeLegale,
                                                       EmailSedeLegale = impresa.eMailSedeLegale,
                                                       PECSedeLegale = impresa.pecSedeLegale,
                                                       IndirizzoSedeAmministrativa =
                                                           new Indirizzo
                                                               {
                                                                   Qualificatore =
                                                                       impresa.TipoViaSedeAmministrativa.Descrizione,
                                                                   NomeVia =
                                                                       impresa.denominazioneIndirizzoSedeAmministrativa,
                                                                   Civico = impresa.civicoSedeAmministrativa,
                                                                   Cap = impresa.capSedeAmministrazione,
                                                                   Comune = impresa.localitaSedeAmministrazione,
                                                                   Provincia = impresa.provinciaSedeAmministrazione
                                                               },
                                                       PressoSedeAmministrativa = impresa.pressoSedeAmministrazione,
                                                       TelefonoSedeAmministrativa = impresa.telefonosedeAmministrazione,
                                                       FaxSedeAmministrativa = impresa.faxSedeAmministrazione,
                                                       EmailSedeAmministrativa = impresa.eMailSedeAmministrazione,
                                                       IndirizzoCorrispondenza =
                                                           new Indirizzo
                                                               {
                                                                   Qualificatore =
                                                                       impresa.TipoViaCorrispondenza.Descrizione,
                                                                   NomeVia =
                                                                       impresa.denominazioneIndirizzoCorrispondenza,
                                                                   Civico = impresa.civicoCorrispondenza,
                                                                   Cap = impresa.capCorrispondenza,
                                                                   Comune = impresa.localitaCorrispondenza,
                                                                   Provincia = impresa.provinciaCorrispondenza
                                                               },
                                                       PressoCorrispondenza = impresa.pressoCorrispondenza,
                                                       TelefonoCorrispondenza = impresa.telefonoCorrispondenza,
                                                       FaxCorrispondenza = impresa.faxCorrispondenza,
                                                       EmailCorrispondenza = impresa.eMailCorrispondenza
                                                   };

                ret = queryImpreseRecapiti.SingleOrDefault();
            }

            return ret;
        }

        public bool SaveRichiestaVariazioneRecapiti(ImpresaRichiestaVariazioneRecapiti richiestaVariazioneRecapiti)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                context.ImpreseRichiesteVariazioneRecapiti.AddObject(richiestaVariazioneRecapiti);
                int numObj = context.SaveChanges();
                if (numObj > 0)
                    ret = true;
            }

            return ret;
        }

        public List<ImpresaRichiestaVariazioneRecapiti> GetRichiesteVariazioniPendenti(int idImpresa, int idTipoRecapito)
        {
            List<ImpresaRichiestaVariazioneRecapiti> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var queryRichiestePendenti =
                    from richiestaVariazioneRecapiti in context.ImpreseRichiesteVariazioneRecapiti.Include("ImpresaRecapito")
                    where
                        (richiestaVariazioneRecapiti.IdImpresa == idImpresa &&
                         richiestaVariazioneRecapiti.ImpresaRecapito.IdTipoRecapito == idTipoRecapito &&
                         richiestaVariazioneRecapiti.Gestito == false)
                    select richiestaVariazioneRecapiti;

                ret = queryRichiestePendenti.ToList();
            }

            return ret;
        }

        public Boolean EmailUgualeAConsulente(Int32 idImpresa, String email)
        {
            Boolean emailUguale = false;

            using (SICEEntities context = new SICEEntities())
            {
                var queryEmailConsulente =
                    from consulente in context.ImpreseConsulenti
                    where consulente.IdImpresa == idImpresa &&
                        !consulente.DataDisdetta.HasValue &&
                        consulente.Consulenti.eMail.ToUpper() == email.ToUpper()
                    select consulente;

                List<ImpresaConsulente> cons = queryEmailConsulente.ToList();
                if (cons.Count > 0)
                {
                    emailUguale = true;
                }
            }

            return emailUguale;
        }

        public TBridge.Cemi.Type.Domain.Impresa GetImpresa(Int32 idImpresa)
        {
            TBridge.Cemi.Type.Domain.Impresa impresa = null;

            using (SICEEntities context = new SICEEntities())
            {
                var queryImpresa =
                    from imp in context.Imprese
                    where imp.Id == idImpresa
                    select imp;

                impresa = queryImpresa.SingleOrDefault();
            }

            return impresa;
        }
    }
}