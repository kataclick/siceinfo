﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain.AnagraficaCondivisa;
using TBridge.Cemi.Type.Filters.AnagraficaCondivisa;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;

namespace TBridge.Cemi.Business
{
    public class AnagraficaCondivisaManager
    {
        public List<Lavoratore> GetLavoratoriByFilter(LavoratoreFilter filtro)
        {
            List<Lavoratore> lavoratori = null;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from lavoratore in context.Lavoratori//.Include("EnteGestore")
                            where
                                (!filtro.CodiceCassaEdile.HasValue || lavoratore.CodiceCassaEdile == filtro.CodiceCassaEdile.Value)
                                &&
                                (String.IsNullOrEmpty(filtro.Cognome) || lavoratore.Cognome.ToUpper().Contains(filtro.Cognome.ToUpper()))
                                &&
                                (String.IsNullOrEmpty(filtro.Nome) || lavoratore.Nome.ToUpper().Contains(filtro.Nome.ToUpper()))
                                &&
                                (!filtro.DataNascita.HasValue || lavoratore.DataNascita == filtro.DataNascita.Value)
                                &&
                                (String.IsNullOrEmpty(filtro.CodiceFiscale) || lavoratore.CodiceFiscale.ToUpper().Equals(filtro.CodiceFiscale.ToUpper()))
                            select lavoratore;

                lavoratori = query.ToList();
            }

            return lavoratori;
        }

        public List<Impresa> GetImpreseByFilter(ImpresaFilter filtro)
        {
            List<Impresa> imprese = null;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from impresa in context.Imprese.Include("Sedi")
                            where
                                (String.IsNullOrEmpty(filtro.RagioneSociale) || impresa.RagioneSociale.ToUpper().Contains(filtro.RagioneSociale.ToUpper()))
                                &&
                                (String.IsNullOrEmpty(filtro.CodiceFiscale) || impresa.CodiceFiscale.ToUpper().Contains(filtro.CodiceFiscale.ToUpper()))
                                &&
                                (String.IsNullOrEmpty(filtro.PartitaIva) || impresa.PartitaIVA.ToUpper().Contains(filtro.PartitaIva.ToUpper()))
                                &&
                                ((!filtro.CodiceCassaEdile.HasValue) || (impresa.CodiceCassaEdile.HasValue && impresa.CodiceCassaEdile.Value.Equals(filtro.CodiceCassaEdile.Value)))
                            select impresa;

                imprese = query.ToList();
            }

            return imprese;
        }

        public List<LavoratoreCorso> GetLavoratoriCorsi(Int32 idLavoratoreCassaEdile, FormazioneFilter filtro)
        {
            List<LavoratoreCorso> lavCor = null;

            if (filtro == null)
                filtro = new FormazioneFilter();

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var queryLav = from lavoratoreCorso in context.LavoratoriCorsi
                               where lavoratoreCorso.Lavoratore.CodiceCassaEdile == idLavoratoreCassaEdile
                               && (!filtro.Dal.HasValue || filtro.Dal.Value <= lavoratoreCorso.DataInizioCorso)
                               && (!filtro.Al.HasValue || filtro.Al.Value >= lavoratoreCorso.DataInizioCorso)
                               orderby lavoratoreCorso.DataInizioCorso descending
                               select lavoratoreCorso;

                lavCor = queryLav.ToList();
            }

            return lavCor;
        }
    }
}
