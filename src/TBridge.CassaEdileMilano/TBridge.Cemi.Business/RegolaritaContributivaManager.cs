﻿using System;
using System.Linq;
using TBridge.Cemi.Data;

namespace TBridge.Cemi.Business
{
    public class RegolaritaContributivaManager
    {
        public static decimal GetDebitoImpresa(int idImpresa, DateTime? dataInizio = null, DateTime? dataFine = null)
        {
            decimal debito;

            using (SICEEntities context = new SICEEntities())
            {
                var queryDebito = from situazioneContabileImpresa in context.SituazioniContabiliImprese
                                  where (!dataInizio.HasValue || (dataInizio.HasValue && situazioneContabileImpresa.Data >= dataInizio.Value))
                                        && (!dataFine.HasValue ||(dataFine.HasValue && situazioneContabileImpresa.Data <= dataFine.Value))
                                  group situazioneContabileImpresa by situazioneContabileImpresa.IdImpresa
                                  into g
                                  where g.Key == idImpresa
                                  select g.Sum(d => d.ImportoDebitoDenunciato) - g.Sum(v => v.ImportoDebitoVersato);

                debito = queryDebito.SingleOrDefault();
            }

            return debito;
        }
    }
}