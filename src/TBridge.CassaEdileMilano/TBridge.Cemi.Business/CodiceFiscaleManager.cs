﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Business
{
    public class CodiceFiscaleManager
    {
        public static string CalcolaCodiceFiscale(string nome, string cognome, string sesso, DateTime dataNascita,
                                                  string codiceIstat)
        {
            const string mesi = "ABCDEHLMPRST";
            const string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            int[,] matricecod = GetMatriceControlloCf();

            //converte tutto in maiuscolo
            string ccognome = cognome.ToUpper();
            string cnome = nome.ToUpper();

            string codiceFiscale = CalcolaLettereCognome(ccognome.Trim());
            codiceFiscale = codiceFiscale + CalcolaLettereNome(cnome.Trim());
            codiceFiscale = codiceFiscale + dataNascita.Year.ToString().Substring(2);
            codiceFiscale = codiceFiscale + mesi[dataNascita.Month - 1];

            if (sesso == "F")
                codiceFiscale = codiceFiscale + (dataNascita.Day + 40);
            else
            {
                string tmpGiorno = dataNascita.Day.ToString();
                if (tmpGiorno.Length == 1)
                    tmpGiorno = "0" + tmpGiorno;
                codiceFiscale = codiceFiscale + tmpGiorno;
            }

            codiceFiscale = codiceFiscale + codiceIstat;

            int codcontrollo = 0;
            ASCIIEncoding ascii = new ASCIIEncoding();
            for (int i = 0; i <= 14; i++)
            {
                string carattere = codiceFiscale[i].ToString();
                byte[] asciibytes = ascii.GetBytes(carattere);
                int asciicode = Int32.Parse(asciibytes[0].ToString());
                codcontrollo = codcontrollo + matricecod[asciicode, i%2];
            }

            codiceFiscale = codiceFiscale + alfabeto[codcontrollo%26];

            return codiceFiscale;
        }

        private static int[,] GetMatriceControlloCf()
        {
            int[,] matricecod = new int[100,2];
            //matrice per calcolare il carattere di controllo
            //il primo indice è il codice ascii del carattere il secondo indice la posizione pari o dispari
            // "A"
            matricecod[65, 0] = 1;
            matricecod[65, 1] = 0;
            // "B"
            matricecod[66, 0] = 0;
            matricecod[66, 1] = 1;
            // "C"
            matricecod[67, 0] = 5;
            matricecod[67, 1] = 2;
            // "D"
            matricecod[68, 0] = 7;
            matricecod[68, 1] = 3;
            // "E"
            matricecod[69, 0] = 9;
            matricecod[69, 1] = 4;
            // "F"
            matricecod[70, 0] = 13;
            matricecod[70, 1] = 5;
            // "G"
            matricecod[71, 0] = 15;
            matricecod[71, 1] = 6;
            // "H"
            matricecod[72, 0] = 17;
            matricecod[72, 1] = 7;
            // "I"
            matricecod[73, 0] = 19;
            matricecod[73, 1] = 8;
            // "J"
            matricecod[74, 0] = 21;
            matricecod[74, 1] = 9;
            // "K"
            matricecod[75, 0] = 2;
            matricecod[75, 1] = 10;
            // "L"
            matricecod[76, 0] = 4;
            matricecod[76, 1] = 11;
            // "M"
            matricecod[77, 0] = 18;
            matricecod[77, 1] = 12;
            // "N"
            matricecod[78, 0] = 20;
            matricecod[78, 1] = 13;
            // "O"
            matricecod[79, 0] = 11;
            matricecod[79, 1] = 14;
            // "P"
            matricecod[80, 0] = 3;
            matricecod[80, 1] = 15;
            // "Q"
            matricecod[81, 0] = 6;
            matricecod[81, 1] = 16;
            // "R"
            matricecod[82, 0] = 8;
            matricecod[82, 1] = 17;
            // "S"
            matricecod[83, 0] = 12;
            matricecod[83, 1] = 18;
            // "T"
            matricecod[84, 0] = 14;
            matricecod[84, 1] = 19;
            // "U"
            matricecod[85, 0] = 16;
            matricecod[85, 1] = 20;
            // "V"
            matricecod[86, 0] = 10;
            matricecod[86, 1] = 21;
            // "W"
            matricecod[87, 0] = 22;
            matricecod[87, 1] = 22;
            // "X"
            matricecod[88, 0] = 25;
            matricecod[88, 1] = 23;
            // "Y"
            matricecod[89, 0] = 24;
            matricecod[89, 1] = 24;
            // "Z"
            matricecod[90, 0] = 23;
            matricecod[90, 1] = 25;
            // "0"
            matricecod[48, 0] = 1;
            matricecod[48, 1] = 0;
            // "1"
            matricecod[49, 0] = 0;
            matricecod[49, 1] = 1;
            // "2"
            matricecod[50, 0] = 5;
            matricecod[50, 1] = 2;
            // "3"
            matricecod[51, 0] = 7;
            matricecod[51, 1] = 3;
            // "4"
            matricecod[52, 0] = 9;
            matricecod[52, 1] = 4;
            // "5"
            matricecod[53, 0] = 13;
            matricecod[53, 1] = 5;
            // "6"
            matricecod[54, 0] = 15;
            matricecod[54, 1] = 6;
            // "7"
            matricecod[55, 0] = 17;
            matricecod[55, 1] = 7;
            // "8"
            matricecod[56, 0] = 19;
            matricecod[56, 1] = 8;
            // "9"
            matricecod[57, 0] = 21;
            matricecod[57, 1] = 9;

            return matricecod;
        }

        public static bool VerificaCodiceFiscale(string nome, string cognome, string sesso, DateTime dataNascita,
                                                 string codiceIstat, string codiceFiscale)
        {
            return (codiceFiscale == CalcolaCodiceFiscale(nome, cognome, sesso, dataNascita, codiceIstat));
        }

        public static bool VerificaPrimi11CaratteriCodiceFiscale(string nome, string cognome, string sesso,
                                                                 DateTime dataNascita, string codiceFiscale)
        {
            return (VerificaCodiceControlloCodiceFiscale(codiceFiscale) &&
                    codiceFiscale.Substring(0, 11).ToUpper().Trim() ==
                    CalcolaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita).ToUpper().Trim());
        }

        public static bool VerificaPrimi12CaratteriCodiceFiscale(string provincia, string comune,
                                                                 string nome, string cognome, string sesso,
                                                                 DateTime dataNascita, string codiceFiscale)
        {
            ComuneSiceNewCollection comuniSiceNew = new Common().GetComuniSiceNew(provincia);
            String codiceCatastaleComune = String.Empty;
            foreach (ComuneSiceNew comuneSiceNew in comuniSiceNew)
            {
                if (comuneSiceNew.Comune.Equals(comune))
                    codiceCatastaleComune = comuneSiceNew.CodiceCatastale;
            }
            return (VerificaCodiceControlloCodiceFiscale(codiceFiscale) &&
                    VerificaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita, codiceFiscale) &&
                    (codiceFiscale[11] == codiceCatastaleComune[0]));
        }

        public static Boolean VerificaCodiceControlloCodiceFiscale(String codiceFiscale)
        {
            Boolean res = false;

            const string alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int[,] matricecod = GetMatriceControlloCf();

            codiceFiscale = codiceFiscale.ToUpper();
            int codcontrollo = 0;
            // richiede una using System.Text;
            ASCIIEncoding ascii = new ASCIIEncoding();
            for (int i = 0; i <= 14; i++)
            {
                string carattere = codiceFiscale[i].ToString();
                byte[] asciibytes = ascii.GetBytes(carattere);
                int asciicode = Int32.Parse(asciibytes[0].ToString());
                codcontrollo = codcontrollo + matricecod[asciicode, i%2];
            }

            if (alfabeto[codcontrollo%26] == codiceFiscale[15])
            {
                res = true;
            }

            return res;
        }

        public static ComuneSiceNew ComuneSiceNewDaCodiceFiscale(String codiceFiscale)
        {
            ComuneSiceNew cm = new ComuneSiceNew();

            if (codiceFiscale.Length == 16)
            {
                Common commonBiz = new Common();
                cm = commonBiz.GetComuneSiceNew(codiceFiscale.Substring(11, 4));
            }
            else
            {
                cm = null;
            }

            return cm;
        }

        public static string CalcolaPrimi11CaratteriCodiceFiscale(string nome, string cognome, string sesso,
                                                                  DateTime dataNascita)
        {
            const string mesi = "ABCDEHLMPRST";

            string ccognome = cognome.ToUpper();
            string cnome = nome.ToUpper();

            string codfisc = CalcolaLettereCognome(ccognome.Trim());
            codfisc = codfisc + CalcolaLettereNome(cnome.Trim());
            codfisc = codfisc + dataNascita.Year.ToString().Substring(2);
            codfisc = codfisc + mesi[dataNascita.Month - 1];
            if (sesso == "F")
                codfisc = codfisc + (dataNascita.Day + 40);
            else
            {
                string tmpGiorno = dataNascita.Day.ToString();
                if (tmpGiorno.Length == 1)
                    tmpGiorno = "0" + tmpGiorno;
                codfisc = codfisc + tmpGiorno;
            }

            return codfisc;
        }

        private static string CalcolaLettereCognome(string cognome)
        {
            // restituisce le 3 lettere relative al cognome

            string treLettereCognome = String.Empty;

            const string vocali = "AEIOU";
            const string consonanti = "BCDFGHJKLMNPQRSTVWXYZ";

            cognome = StripAccentate(cognome);
            int i = 0;
            while ((treLettereCognome.Length < 3) && (i < cognome.Length))
            {
                for (int j = 0; j < consonanti.Length; j++)
                {
                    if (cognome[i] == consonanti[j])
                    {
                        treLettereCognome = treLettereCognome + cognome[i];
                    }
                }
                i++;
            }

            i = 0;
            //se non ha ancora 3 consonanti sceglie fra le vocali
            while ((treLettereCognome.Length < 3) && (i < cognome.Length))
            {
                for (int j = 0; j < vocali.Length; j++)
                {
                    if (cognome[i] == vocali[j])
                    {
                        treLettereCognome = treLettereCognome + cognome[i];
                    }
                }
                i++;
            }

            //se non ha ancora 3 lettere aggiunge x per arrivare a 3
            if (treLettereCognome.Length < 3)
            {
                for (int k = treLettereCognome.Length; k <= 2; k++)
                    treLettereCognome = treLettereCognome + "X";
            }

            return treLettereCognome;
        }

        private static string CalcolaLettereNome(string nome)
        {
            // restituisce le 3 lettere relative al nome

            const string vocali = "AEIOU";
            const string consonanti = "BCDFGHJKLMNPQRSTVWXYZ";

            nome = StripAccentate(nome);
            int i = 0;
            string cons = "";
            string treLettereNome;
            while ((cons.Length < 4) && (i < nome.Length))
            {
                for (int j = 0; j < consonanti.Length; j++)
                {
                    if (nome[i] == consonanti[j])
                    {
                        cons = cons + nome[i];
                    }
                }
                i++;
            }

            if (cons.Length > 3)
            {
                //se ha 4 consonanti prende la 1a, 3a e 4a
                treLettereNome = cons[0] + cons[2].ToString() + cons[3];
            }
            else
                treLettereNome = cons;

            i = 0;
            //scorre il nome in cerca di vocali finchè $stringa non contiene 3 lettere
            while ((treLettereNome.Length < 3) && (i < nome.Length))
            {
                for (int j = 0; j < vocali.Length; j++)
                {
                    if (nome[i] == vocali[j])
                    {
                        treLettereNome = treLettereNome + nome[i];
                    }
                }
                i++;
            }

            //se non ha ancora 3 lettere aggiunge x per arrivare a 3
            if (treLettereNome.Length < 3)
            {
                for (int k = treLettereNome.Length; k <= 2; k++)
                    treLettereNome = treLettereNome + "X";
            }

            return treLettereNome;
        }
        
        private static string StripAccentate(string s)
        {
            const string accentate = "ÀÈÉÌÒÙàèéìòù";
            const string noaccento = "AEEIOUAEEIOU";
            string tmpString = "";

            for (int i = 0; i < s.Length; i++)
            {
                bool accentata = false;
                int tmpIndex = 0;
                for (int j = 0; j < accentate.Length; j++)
                {
                    if (s[i] == accentate[j])
                    {
                        accentata = true;
                        tmpIndex = j;
                    }
                }
                if (accentata)
                    tmpString = tmpString + noaccento[tmpIndex];
                else
                    tmpString = tmpString + s[i];
            }

            return tmpString;
        }

        public static DateTime? GetDataSpedizioneTesseraSanitariaByCf(string codiceFiscale)
        {
            DateTime? dataSpedizione = null;

            try
            {
                HttpWebRequest webRequest =
                    (HttpWebRequest)
                    WebRequest.Create(
                        "https://sistemats.sanita.finanze.it/simossInterrogazioneTSFree/interrogaSubmit.do");

                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";

                using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    requestWriter.Write("codiceFiscale=" + codiceFiscale);

                    requestWriter.Close();
                }

                ServicePointManager.ServerCertificateValidationCallback =
                    new RemoteCertificateValidationCallback(
                        delegate { return true; });

                string responseData;
                using (StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    responseData = reader.ReadToEnd();

                    reader.Close();
                }

                List<DateTime> dateCollection = new List<DateTime>();

                int iPos = 0;
                do
                {
                    iPos = responseData.IndexOf("<td class=\"centro\">", iPos + 1);
                    if (iPos > -1)
                    {
                        string data = responseData.Substring(iPos + 19, 10);
                        int anno;
                        int mese;
                        int giorno;

                        if (Int32.TryParse(data.Substring(6, 4), out anno) &&
                            Int32.TryParse(data.Substring(3, 2), out mese) &&
                            Int32.TryParse(data.Substring(0, 2), out giorno))
                        {
                            dateCollection.Add(new DateTime(anno, mese, giorno));
                        }
                    }
                } while (iPos != -1);

                if (responseData.Contains("Tessere Sanitarie di:"))
                {
                    if (dateCollection.Count > 0)
                    {
                        dateCollection.Sort();
                        dataSpedizione = dateCollection[0];
                    }
                }
            }
            catch (Exception exception)
            {
                //
            }

            return dataSpedizione;
        }
    }
}