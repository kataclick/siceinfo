﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using iTextSharp.text.pdf;
using TBridge.Cemi.Business.MavPoSoWebReference;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Business
{
    public class MavManager
    {
        private readonly Data.Common _dataProvider = new Data.Common();
        private readonly InformazioniBanca _informazioniBanca;
        private readonly String _nomeTemplateMav;
        private readonly String _pathRisorse;

        public MavManager(String pathApplicazione)
        {
            _pathRisorse = Path.Combine(pathApplicazione, "BollettiniFreccia");
            _nomeTemplateMav = ConfigurationManager.AppSettings["nomeTemplateMavPoSo"];

            _informazioniBanca = new InformazioniBanca
                                     {
                                         CodiceServizio = ConfigurationManager.AppSettings["codiceServizioMavPoSo"],
                                         CodiceSottoservizio =
                                             ConfigurationManager.AppSettings["codiceSottoservizioMavPoSo"],
                                         CodiceSia = ConfigurationManager.AppSettings["codiceSiaMavPoSo"],
                                         NumeroLista = ConfigurationManager.AppSettings["numeroListaMavPoSo"],
                                         NomeCertificato = ConfigurationManager.AppSettings["nomeCertMavPoSoWebRef"]
                                     };
        }

        public bool GetMavPdf(BollettinoMav mav, out byte[] pdfByteArray)
        {
            pdfByteArray = null;
            bool result = false;
            DateTime dataProcesso = DateTime.Now;
            //DateTime dataScadenza = DateTime.Today.AddDays(2);
            DateTime dataScadenza = DateTime.Today;
            Impresa impresa = _dataProvider.GetImpresaById(mav.IdImpresa);
            MavLocale mavLocale = new MavLocale(mav, impresa);

            try
            {
                acquisizione_avviso avviso = GetAcquisizioneAvviso(mavLocale, dataScadenza);
                ricevuta_acquisizione_avviso ricevuta = ChiamaWebService(avviso, mavLocale, dataProcesso);

                if (ricevuta.esito.Item is ricevuta_acquisizione_avvisoEsitoAcquisito)
                {
                    String identificativo =
                        ((ricevuta_acquisizione_avvisoEsitoAcquisito) (ricevuta.esito.Item)).
                            codice_identificativo_bollettino;
                    pdfByteArray = CreaPdf(mavLocale, identificativo, dataProcesso, dataScadenza);
                    result = true;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }

        public bool GetMavDenunciaPdf(BollettinoMav mav, out byte[] pdfByteArray)
        {
            pdfByteArray = null;
            bool result = false;
            DateTime dataProcesso = DateTime.Now;
            //DateTime dataScadenza = DateTime.Today.AddDays(2);
            DateTime dataScadenza = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month));
            Impresa impresa = _dataProvider.GetImpresaById(mav.IdImpresa);
            MavLocale mavLocale = new MavLocale(mav, impresa);

            try
            {
                acquisizione_avviso avviso = GetAcquisizioneAvviso(mavLocale, dataScadenza);
                ricevuta_acquisizione_avviso ricevuta = ChiamaWebService(avviso, mavLocale, dataProcesso);

                if (ricevuta.esito.Item is ricevuta_acquisizione_avvisoEsitoAcquisito)
                {
                    String identificativo =
                        ((ricevuta_acquisizione_avvisoEsitoAcquisito)(ricevuta.esito.Item)).
                            codice_identificativo_bollettino;
                    pdfByteArray = CreaPdf(mavLocale, identificativo, dataProcesso, dataScadenza);
                    result = true;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }

        private acquisizione_avviso GetAcquisizioneAvviso(MavLocale mav, DateTime dataScadenza)
        {
            acquisizione_avviso avviso = new acquisizione_avviso();

            avviso.informazioni_banca = new acquisizione_avvisoInformazioni_banca();
            avviso.informazioni_banca.codice_sia = _informazioniBanca.CodiceSia;
            avviso.informazioni_banca.codice_servizio = _informazioniBanca.CodiceServizio;
            avviso.informazioni_banca.codice_sottoservizio = _informazioniBanca.CodiceSottoservizio;
            avviso.informazioni_banca.numero_lista = new acquisizione_avvisoInformazioni_bancaNumero_lista();
            avviso.informazioni_banca.numero_lista.Value = _informazioniBanca.NumeroLista;
            avviso.informazioni_banca.numero_lista.modificabile = stringaModificabile.N;

            avviso.informazioni_debitore = new acquisizione_avvisoInformazioni_debitore();

            avviso.informazioni_debitore.anagrafica_debitore =
                new acquisizione_avvisoInformazioni_debitoreAnagrafica_debitore
                    {
                        Value = mav.RagioneSociale,
                        modificabile = stringaModificabile.N
                    };
            avviso.informazioni_debitore.cap_debitore = new acquisizione_avvisoInformazioni_debitoreCap_debitore
                                                            {
                                                                Value = mav.CapSedeLegale,
                                                                modificabile = stringaModificabile.N
                                                            };
            avviso.informazioni_debitore.codice_debitore =
                new acquisizione_avvisoInformazioni_debitoreCodice_debitore
                    {
                        Value = mav.IdImpresa,
                        modificabile = stringaModificabile.N
                    };

            avviso.informazioni_debitore.codice_fiscale_debitore =
                new acquisizione_avvisoInformazioni_debitoreCodice_fiscale_debitore
                    {
                        Value = mav.CodiceFiscale,
                        modificabile = stringaModificabile.N
                    };

            avviso.informazioni_debitore.email_debitore = new acquisizione_avvisoInformazioni_debitoreEmail_debitore
                                                              {
                                                                  Value = mav.EmailSedeLegale,
                                                                  modificabile = stringaModificabile.N
                                                              };

            avviso.informazioni_debitore.indirizzo_debitore =
                new acquisizione_avvisoInformazioni_debitoreIndirizzo_debitore
                    {
                        Value = mav.IndirizzoSedeLegale,
                        modificabile = stringaModificabile.N
                    };

            avviso.informazioni_debitore.localita_debitore =
                new acquisizione_avvisoInformazioni_debitoreLocalita_debitore
                    {
                        Value = mav.LocalitaSedeLegale,
                        modificabile = stringaModificabile.N
                    };

            avviso.informazioni_debitore.provincia_debitore =
                new acquisizione_avvisoInformazioni_debitoreProvincia_debitore
                    {
                        Value = mav.ProvinciaSedeLegale,
                        modificabile = stringaModificabile.N
                    };

            avviso.informazioni_pagamento = new acquisizione_avvisoInformazioni_pagamento();

            // Vecchia causale senza sequenza
            //string causalePerNI = mav.IdImpresa.PadLeft(6, '0') + mav.Anno.ToString().PadLeft(4, '0') +
            //                      mav.Mese.ToString().PadLeft(2, '0') + "00";
            string causalePerNI = mav.IdImpresa.PadLeft(6, '0') + mav.Anno.ToString().PadLeft(4, '0') +
                                  mav.Mese.ToString().PadLeft(2, '0') + mav.Sequenza.ToString().PadLeft(2, '0');
            avviso.informazioni_pagamento.causale_bollettino =
                new acquisizione_avvisoInformazioni_pagamentoCausale_bollettino
                    {
                        Value = causalePerNI,
                        modificabile = stringaModificabile.N
                    };

            // Vecchio identificativo senza sequenza
            //avviso.informazioni_pagamento.identificativo_disposizione = mav.IdImpresa.PadLeft(6, '0') +
            //                                                            mav.Anno.ToString().PadLeft(4, '0') +
            //                                                            mav.Mese.ToString().PadLeft(2, '0') + "00";
            avviso.informazioni_pagamento.identificativo_disposizione = mav.IdImpresa.PadLeft(6, '0') +
                                                                        mav.Anno.ToString().PadLeft(4, '0') +
                                                                        mav.Mese.ToString().PadLeft(2, '0') +
                                                                        mav.Sequenza.ToString().PadLeft(2, '0');

            avviso.informazioni_pagamento.importo = new acquisizione_avvisoInformazioni_pagamentoImporto
                                                        {
                                                            Value = mav.Importo,
                                                            modificabile = stringaModificabile.N
                                                        };

            avviso.informazioni_pagamento.scadenza = new data
                                                         {
                                                             Value = dataScadenza,
                                                             modificabile = stringaModificabile.N
                                                         };

            return avviso;
        }

        private ricevuta_acquisizione_avviso ChiamaWebService(acquisizione_avviso avv, MavLocale mav,
                                                              DateTime oraAppoggio)
        {
            byte[] bytes = AcquisizioneAvvisoToByteArray(avv);
            string appoggio = oraAppoggio.Hour.ToString(CultureInfo.InvariantCulture) + oraAppoggio.Minute +
                              oraAppoggio.Second + oraAppoggio.Millisecond + mav.IdImpresa + mav.Anno + mav.Mese +
                              mav.Sequenza + oraAppoggio.Year + oraAppoggio.Month + oraAppoggio.Day;
            string idTransazione = appoggio.PadLeft(20, '0').Substring(0, 20);

            ObjResponse response;
            using (MavFrecciaService client = new MavFrecciaService())
            {
                String pathCertificato = Path.Combine(_pathRisorse, _informazioniBanca.NomeCertificato);
                X509Certificate certificate = X509Certificate.CreateFromCertFile(pathCertificato);
                client.ClientCertificates.Add(certificate);
                //throw new Exception(certificate.GetName());
                response = client.pagamento(idTransazione, bytes);
            }

            string xmlRisposta = Encoding.UTF8.GetString(response.xmlresponse);

            XmlSerializer serializerRisposta = new XmlSerializer(typeof(ricevuta_acquisizione_avviso));
            ricevuta_acquisizione_avviso ricevuta =
                (ricevuta_acquisizione_avviso) serializerRisposta.Deserialize(new StringReader(xmlRisposta));

            return ricevuta;
        }

        private static byte[] AcquisizioneAvvisoToByteArray(acquisizione_avviso avv)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(acquisizione_avviso));
            MemoryStream ms = new MemoryStream();
            using (XmlTextWriterFull xmlTextWriter = new XmlTextWriterFull(ms, Encoding.GetEncoding("ISO-8859-15"))
                                                         {
                                                             Formatting = Formatting.Indented
                                                         })
            {
                serializer.Serialize(xmlTextWriter, avv);
                xmlTextWriter.Flush();
                ms = (MemoryStream) xmlTextWriter.BaseStream;
            }
            string xml = Encoding.UTF8.GetString(ms.ToArray());

            string encodeTo64 = EncodeTo64(xml);
            byte[] bytes = Convert.FromBase64String(encodeTo64);
            return bytes;
        }

        private Byte[] CreaPdf(MavLocale mav, String identificativo, DateTime dataAppoggio, DateTime dataScadenza)
        {
            Byte[] pdfByteArray = null;

            // Vecchia causale senza sequenza
            //String causale = mav.IdImpresa.PadLeft(6, '0') + mav.Anno.ToString().PadLeft(4, '0') +
            //                 mav.Mese.ToString().PadLeft(2, '0') + "00";
            String causale = mav.IdImpresa.PadLeft(6, '0') + mav.Anno.ToString().PadLeft(4, '0') +
                             mav.Mese.ToString().PadLeft(2, '0') + mav.Sequenza.ToString().PadLeft(2, '0');
            // Vecchia causale senza sequenza
            //String causaleStampa = String.Format("{0} {1}/{2} 00", mav.IdImpresa.PadLeft(6, '0'),
            //                                     mav.Anno.ToString().PadLeft(4, '0'),
            //                                     mav.Mese.ToString().PadLeft(2, '0'));
            String causaleStampa = String.Format("{0} {1}/{2} {3}", mav.IdImpresa.PadLeft(6, '0'),
                                                 mav.Anno.ToString().PadLeft(4, '0'),
                                                 mav.Mese.ToString().PadLeft(2, '0'),
                                                 mav.Sequenza.ToString().PadLeft(2, '0'));
            String pathSource = Path.Combine(_pathRisorse, _nomeTemplateMav);

            PdfReader pdfReader = new PdfReader(pathSource);
            MemoryStream memoryStrem = new MemoryStream();
            PdfStamper pdfStamper = new PdfStamper(pdfReader, memoryStrem);

            AcroFields pdfFormFields = pdfStamper.AcroFields;

            pdfFormFields.SetField("causaleA", causaleStampa);
            pdfFormFields.SetField("beneficiario", mav.Beneficiario);

            CultureInfo ci = CultureInfo.GetCultureInfo("it-IT").Clone() as CultureInfo;
            NumberFormatInfo numInfo = ci.NumberFormat;
            numInfo.CurrencyDecimalSeparator = ",";
            numInfo.CurrencySymbol = String.Empty;

            pdfFormFields.SetField("euro", mav.Importo.ToString("C", numInfo).Trim());
            pdfFormFields.SetField("scadenza", dataScadenza.ToString("dd/MM/yyyy"));
            pdfFormFields.SetField("codicedebitore", mav.IdImpresa);
            pdfFormFields.SetField("debitore",
                                   String.Format("{0}\n{1}\n{2} {3} {4}", mav.RagioneSociale, mav.IndirizzoSedeLegale,
                                                 mav.CapSedeLegale, mav.LocalitaSedeLegale, mav.ProvinciaSedeLegale));
            pdfFormFields.SetField("codiceidentificativoMAV", identificativo);
            pdfFormFields.SetField("luogoedata",
                                   String.Format("MILANO, {0}", dataAppoggio.ToString("dd MMMM yyyy").ToUpper()));
            pdfFormFields.SetField("codelineA", String.Format(">{0}<", identificativo));
            pdfFormFields.SetField("codelineB", ImportoCapolineaB(mav.Importo));

            pdfStamper.Writer.CloseStream = false;
            pdfStamper.FormFlattening = false;
            pdfStamper.Close();

            pdfByteArray = new Byte[memoryStrem.Length];
            Buffer.BlockCopy(memoryStrem.GetBuffer(), 0, pdfByteArray, 0, pdfByteArray.Length);
            memoryStrem.Close();

            return pdfByteArray;
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes
                = Encoding.ASCII.GetBytes(toEncode);
            string returnValue
                = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        private static string ImportoCapolineaB(decimal importo)
        {
            string capolinea;
            int intera;
            int decimale;
            intera = Decimal.ToInt32((Math.Floor(importo)));
            decimale = Convert.ToInt32((importo - intera) * 100);
            string valueIntera = Convert.ToString(intera);
            valueIntera = valueIntera.PadLeft(8, '0');
            string valueDecimale = Convert.ToString(decimale);
            valueDecimale = valueDecimale.PadLeft(2, '0');
            capolinea = String.Format("{0}+{1}<", valueIntera, valueDecimale);

            return capolinea;
        }

        #region Classi private

        #region Nested type: InformazioniBanca

        private class InformazioniBanca
        {
            public String CodiceSia { get; set; }
            public String CodiceServizio { get; set; }
            public String CodiceSottoservizio { get; set; }
            public String NumeroLista { get; set; }
            public String NomeCertificato { get; set; }
        }

        #endregion

        #region Nested type: MavLocale

        private class MavLocale
        {
            public MavLocale(BollettinoMav mav, Impresa impresa)
            {
                Anno = mav.Anno;
                Mese = mav.Mese;
                // La sequenza deve essere decrementata di 1 per un baco SiceNew
                Sequenza = mav.Sequenza - 1;
                Importo = mav.Importo;
                IdImpresa = impresa.IdImpresa.ToString().PadLeft(6, '0');
                if (impresa.RagioneSociale.Length <= 30)
                {
                    RagioneSociale = impresa.RagioneSociale;
                }
                else
                {
                    RagioneSociale = impresa.RagioneSociale.Substring(0, 30);
                }
                if (impresa.IndirizzoSedeLegale.Length <= 30)
                {
                    IndirizzoSedeLegale = impresa.IndirizzoSedeLegale;
                }
                else
                {
                    IndirizzoSedeLegale = impresa.IndirizzoSedeLegale.Substring(0, 30);
                }
                if (impresa.LocalitaSedeLegale.Length <= 30)
                {
                    LocalitaSedeLegale = impresa.LocalitaSedeLegale;
                }
                else
                {
                    LocalitaSedeLegale = impresa.LocalitaSedeLegale.Substring(0, 30);
                }
                ProvinciaSedeLegale = impresa.ProvinciaSedeLegale;
                CapSedeLegale = impresa.CapSedeLegale;
                CodiceFiscale = impresa.CodiceFiscale;
                EmailSedeLegale = impresa.EmailSedeLegale;
                String beneficiaroTemp = ConfigurationManager.AppSettings["beneficiarioMav"];
                Beneficiario = String.IsNullOrEmpty(beneficiaroTemp) ? beneficiaroTemp : beneficiaroTemp.Replace("\\n", "\n");
            }

            public int Anno { get; private set; }

            public int Mese { get; private set; }

            public int Sequenza { get; set; }

            public decimal Importo { get; private set; }

            public String IdImpresa { get; private set; }

            public String RagioneSociale { get; private set; }

            public String CodiceFiscale { get; private set; }

            public string IndirizzoSedeLegale { get; private set; }

            public string CapSedeLegale { get; private set; }

            public string LocalitaSedeLegale { get; private set; }

            public string ProvinciaSedeLegale { get; private set; }

            public string EmailSedeLegale { get; private set; }

            public string Beneficiario { get; private set; }
        }

        #endregion

        #region Nested type: XmlTextWriterFull

        private class XmlTextWriterFull : XmlTextWriter
        {
            public XmlTextWriterFull(Stream stream, Encoding encoding)
                : base(stream, encoding)
            {
            }

            public override void WriteEndElement()
            {
                base.WriteFullEndElement();
            }
        }

        #endregion

        #endregion
    }
}