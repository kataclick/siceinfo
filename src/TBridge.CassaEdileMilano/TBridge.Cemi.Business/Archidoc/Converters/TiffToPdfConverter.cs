﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Drawing;

namespace TBridge.Cemi.Business.Archidoc.Converters
{
    class TiffToPdfConverter
    {
        public Boolean IsTiff(Byte[] fileByteArray)
        {
            // http://www.fileformat.info/format/tiff/corion.htm
            const byte INTEL_BYTE_ORDER = 0x49;
            const byte MOTOROLA_BYTE_ORDER = 0x4D;
            const ushort TIFF_VERSION_NUMBER = 42;
            bool result = false;

            if (fileByteArray[0] == fileByteArray[1] && (fileByteArray[0] == INTEL_BYTE_ORDER || fileByteArray[0] == MOTOROLA_BYTE_ORDER))
            {
                ushort tiffVersion;
                if (fileByteArray[0] == INTEL_BYTE_ORDER)  
                {
                    //little endian
                    tiffVersion = (ushort)(((int)fileByteArray[3] << 8) | (int)fileByteArray[2]);
                }
                else
                {
                    //big endian
                    tiffVersion = (ushort)(((int)fileByteArray[2] << 8) | (int)fileByteArray[3]);
                }

                result = tiffVersion == TIFF_VERSION_NUMBER;
            }
           
            return result;
        }

        // Retrive PageCount of a multi-page tiff image
        public int GetPageCount(String fileName)
        {
            int pageCount = -1;
            try
            {
                Image img = Bitmap.FromFile(fileName);
                pageCount = img.GetFrameCount(FrameDimension.Page);
                img.Dispose();

            }
            catch (Exception ex)
            {
                pageCount = 0;
            }
            return pageCount;
        }

        // Retrive PageCount of a multi-page tiff image
        public int GetPageCount(Byte[] fileByteArray)
        {
            int pageCount = -1;
            try
            {
                using (MemoryStream memStream = new MemoryStream(fileByteArray))
                {
                    Image img = GetImageMemStream(memStream);
                    pageCount = img.GetFrameCount(FrameDimension.Page);
                    img.Dispose();
                }

            }
            catch (Exception ex)
            {
                pageCount = 0;
            }
            return pageCount;
        }

        // Retrive a specific Page from a multi-page tiff image
        public Image GetTiffPage(String sourceFile, int pageNumber)
        {
            Image returnImage = null;

            try
            {
                Image sourceIamge = Bitmap.FromFile(sourceFile);
                returnImage = GetTiffImage(sourceIamge, pageNumber);
                sourceIamge.Dispose();
            }
            catch (Exception ex)
            {
                returnImage = null;
            }

            //       String splittedImageSavePath = "X:\\CJT\\CJT-Docs\\CJT-Images\\result001.tif";
            //       returnImage.Save(splittedImageSavePath);

            return returnImage;
        }

        // Retrive a specific Page from a multi-page tiff image
        public Image GetTiffPage(Byte[] fileByteArray, int pageNumber)
        {
            Image returnImage = null;

            try
            {
                using (MemoryStream memStream = new MemoryStream(fileByteArray))
                {
                    Image sourceIamge = GetImageMemStream(memStream);
                    returnImage = GetTiffImage(sourceIamge, pageNumber);
                    sourceIamge.Dispose();
                }
            }
            catch (Exception ex)
            {
                returnImage = null;
            }

            //       String splittedImageSavePath = "X:\\CJT\\CJT-Docs\\CJT-Images\\result001.tif";
            //       returnImage.Save(splittedImageSavePath);

            return returnImage;
        }

        public Byte[] TiffToPdf(Byte[] tiffFileByteArray)
        {
            return TiffToPdf(tiffFileByteArray, 0, int.MaxValue);
        }

        public Byte[]  TiffToPdf(Byte[] tiffFileByteArray, int pageFrom, int pageTo)
        {
            Byte[] pdfByteArray = null;

            using (MemoryStream memStream = new MemoryStream(tiffFileByteArray))
            {
                using (Image image = GetImageMemStream(memStream))
                {
                    pdfByteArray = CreatePdfFRomTiff(image, pageFrom, pageTo);
                }
            }

            return pdfByteArray;
        }

        public Byte[] TiffToPdf(Stream tiffStream, int pageFrom, int pageTo)
        {
            Byte[] pdfByteArray = null;
            using (Image image = GetImageMemStream(tiffStream))
            {
                pdfByteArray = CreatePdfFRomTiff(image, pageFrom, pageTo);
            }

            return pdfByteArray;
        }

        public Byte[] TiffToPdf(String tiffFileName, int pageFrom, int pageTo)
        {
            Byte[] pdfByteArray = null;
            using (Image image = Bitmap.FromFile(tiffFileName))
            {
                pdfByteArray = CreatePdfFRomTiff(image, pageFrom, pageTo);
            }

            return pdfByteArray;
        }
      

        #region Private

        private Byte[] CreatePdfFRomTiff(Image image, int pageFrom, int pageTo)
        {
            Byte[] pdfByteArray;
            int pageCount = image.GetFrameCount(FrameDimension.Page);
            if (pageFrom > pageCount)
            {
                throw new ArgumentException("pageFrom must be <= pageCount", "pageFrom");
            }

            using (PdfDocument pdfDoc = new PdfDocument())
            {

                for (int i = Math.Max(0, pageFrom); i < Math.Min(pageCount, pageTo); i++)
                {
                    // AddPageToPdf(pdfDoc, GetTiffImage(image, i));

                    Guid objGuid = image.FrameDimensionsList[0];
                    FrameDimension objDimension = new FrameDimension(objGuid);
                    image.SelectActiveFrame(objDimension, i);
                    AddPageToPdf(pdfDoc, image);
                }

                using (MemoryStream outStream = new MemoryStream())
                {
                    pdfDoc.Save(outStream);
                    pdfByteArray = outStream.ToArray();
                }

            }
            return pdfByteArray;
        }

        private void AddPageToPdf(PdfDocument pdfDocument, Image pageImage)
        {
            PdfPage page = new PdfPage();

            XImage img = XImage.FromGdiPlusImage(pageImage);

            page.Width = img.PointWidth;
            page.Height = img.PointHeight;
            pdfDocument.Pages.Add(page);
            int newPageIndex = pdfDocument.Pages.Count - 1;
            XGraphics xgr = XGraphics.FromPdfPage(pdfDocument.Pages[newPageIndex]);

            xgr.DrawImage(img, 0, 0);
        }


        private Image GetTiffImage(Image sourceImage, int pageNumber)
        {
            MemoryStream ms = null;
            Image returnImage = null;

            //try
            //{
                ms = new MemoryStream();
                Guid objGuid = sourceImage.FrameDimensionsList[0];
                FrameDimension objDimension = new FrameDimension(objGuid);
                sourceImage.SelectActiveFrame(objDimension, pageNumber);
                sourceImage.Save(ms, ImageFormat.Tiff);
                returnImage = Image.FromStream(ms);
            //}
            //catch (Exception ex)
            //{
            //    returnImage = null;
            //}
            return returnImage;
        }

        private Image GetImageMemStream(Stream stream)
        {
            return  Bitmap.FromStream(stream);
        }
        #endregion
    }
}
