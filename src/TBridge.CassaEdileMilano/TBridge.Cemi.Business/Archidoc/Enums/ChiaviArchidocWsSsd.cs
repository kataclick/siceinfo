﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Business.Archidoc.Enums
{
    public enum ChiaviArchidocWsSsd
    {
        svIfReference,
        svIfDateReg,
        svIfProtocol,
        svIfDateDoc,
        svIfKey11,
        svIfKey12,
        svIfKey13,
        svIfKey14,
        svIfKey15,
        svIfKey21,
        svIfKey22,
        svIfKey23,
        svIfKey24,
        svIfKey25,
        svIfKey31,
        svIfKey32,
        svIfKey33,
        svIfKey34,
        svIfKey35,
        svIfKey41,
        svIfKey42,
        svIfKey43,
        svIfKey44,
        svIfKey45,
        svIfObj,
        GUID,
        NrPag,
        DocName,
        DocExtension,
        NrAttachments,
        FullPath
    }
}
