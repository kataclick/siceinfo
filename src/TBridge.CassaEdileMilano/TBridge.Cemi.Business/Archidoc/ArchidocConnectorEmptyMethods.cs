﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Archidoc.Type.Entities;

namespace TBridge.Cemi.Business.Archidoc
{
    internal partial class ArchidocConnectorStandard
    {
        #region Set method (empty e non)

        private svwsstCard SetCard(svwsstCard card)
        {
            svwsstCard stCard = new svwsstCard();

            // GuidCard
            stCard.GuidCard = card.GuidCard;
            // Archivio
            SetArchive(ref stCard, card.Archive.Id);
            //Tipo documento
            SetDocumentType(ref stCard, card.DocumentType.Id);
            // ProcWF
            stCard.ProcWF = svwsProcWF.svwsPWFNothing;
            // Fields
            SetCardFields(ref stCard, card.DocumentType.Id, card.Fields);
            // Visibility
            SetVisibility(ref stCard, card.Users, card.Offices, card.Groups);

            return stCard;
        }

        public svwsstGroups SetEmptyGroups()
        {
            int numeroGruppi = 0;
            svwsstGroups gruppi = new svwsstGroups();
            gruppi.Size = numeroGruppi;
            gruppi.Groups = new List<svwsstGroup>();
           // gruppi.Groups = new svwsstGroup[0];
            return gruppi;
        }

        public svwsstOffices SetEmptyOfficers()
        {
            int numeroUffici = 0;
            svwsstOffices uffici = new svwsstOffices();
            uffici.Size = numeroUffici;
            uffici.Offices = new List<svwsstOffice>();
            //uffici.Offices = new svwsstOffice[0];

            return uffici;
        }

        public void SetArchiveEmptyOfficers(ref svwsstCard card)
        {
            int numeroUffici = 0;
            card.Archive.OfficesRead = new svwsstOffices();
            card.Archive.OfficesRead.Size = numeroUffici;
            card.Archive.OfficesRead.Offices = new List<svwsstOffice>();
            //card.Archive.OfficesRead.Offices = new svwsstOffice[0];

            card.Archive.OfficesWrite = new svwsstOffices();
            card.Archive.OfficesWrite.Size = numeroUffici;
            card.Archive.OfficesWrite.Offices = new List<svwsstOffice>();
            //card.Archive.OfficesWrite.Offices = new svwsstOffice[0];
        }

        public svwsstUsers SetEmptyUsers()
        {
            int numeroUser = 0;
            svwsstUsers utenti = new svwsstUsers();
            utenti.Size = numeroUser;
            utenti.Users = new List<svwsstUser>();
            //utenti.Users = new svwsstUser[0];
            return utenti;
        }

        public void SetArchiveEmptyUsers(ref svwsstCard card)
        {
            int numeroUser = 0;
            card.Archive.UsersWrite = new svwsstUsers();
            card.Archive.UsersWrite.Size = numeroUser;
            card.Archive.UsersWrite.Users = new List<svwsstUser>();
            //card.Archive.UsersWrite.Users = new svwsstUser[0];

            card.Archive.UsersRead = new svwsstUsers();
            card.Archive.UsersRead.Size = numeroUser;
            card.Archive.UsersRead.Users = new List<svwsstUser>();
            //card.Archive.UsersRead.Users = new svwsstUser[0];
        }

        public svwsstFields SetEmptyCardFields()
        {
            svwsstFields cardFields = new svwsstFields();
            cardFields.Fields = new List<svwsstField>();
            cardFields.Size = 0;
            return cardFields;
        }

        public void SetArchive(ref svwsstCard stCard, short lId)
        {
            stCard.Archive = new svwsstArchive();
            stCard.Archive.Id = lId;

            SetArchiveEmptyUsers(ref stCard);
            //SetEmptyUsers(ref stCard.Archive.UsersWrite);
            SetArchiveEmptyOfficers(ref stCard);
            //SetEmptyOfficers(stCard.Archive.OfficesWrite);
        }

        public void SetDocumentType(ref svwsstCard stCard, short lId)
        {
            stCard.DocumentType = new svwsstDocumentType();

            stCard.DocumentType.Id = lId;

            //SetEmptyUsers(stDocumentType.UsersRead);
            //SetEmptyUsers(stDocumentType.UsersWrite);
            //SetEmptyOfficers(stDocumentType.OfficesRead);
            //SetEmptyOfficers(stDocumentType.OfficesWrite);

            stCard.DocumentType.UsersRead = SetEmptyUsers();
            stCard.DocumentType.UsersWrite = SetEmptyUsers();
            stCard.DocumentType.OfficesRead = SetEmptyOfficers();
            stCard.DocumentType.OfficesWrite = SetEmptyOfficers();
        }

        private void SetCardFields(ref svwsstCard stCard, short sCardDocumentTypeId, svwsstFields card)
        {
            svwsstFields stDocTypeFields;

            //SvAolWSService sws = new SvAolWSService();
            //stDocTypeFields = sws.DocumentType_Fields(sessioneGlobale, sCardDocumentTypeId);

            int i;
            int iSize = card.Size; //stDocTypeFields.Size;

            svwsstFields stCardFields = new svwsstFields();
            stCardFields.Size = iSize;
            stCardFields.Fields = new List<svwsstField>(iSize);
            //stCardFields.Fields = new svwsstField[iSize];

            for (i = 0; i < iSize; i++)
            {
                stCardFields.Fields.Add(new svwsstField());
                switch (card.Fields[i].Id)
                {
                    case svwsIdField.svwsIfDateDoc:
                    case svwsIdField.svwsIfDateReg:
                    case svwsIdField.svwsIfKey11:
                    case svwsIdField.svwsIfKey12:
                    case svwsIdField.svwsIfKey13:
                    case svwsIdField.svwsIfKey14:
                    case svwsIdField.svwsIfKey15:
                    case svwsIdField.svwsIfKey21:
                    case svwsIdField.svwsIfKey22:
                    case svwsIdField.svwsIfKey23:
                    case svwsIdField.svwsIfKey24:
                    case svwsIdField.svwsIfKey25:
                    case svwsIdField.svwsIfKey31:
                    case svwsIdField.svwsIfKey32:
                    case svwsIdField.svwsIfKey33:
                    case svwsIdField.svwsIfKey34:
                    case svwsIdField.svwsIfKey35:
                    case svwsIdField.svwsIfKey41:
                    case svwsIdField.svwsIfKey42:
                    case svwsIdField.svwsIfKey43:
                    case svwsIdField.svwsIfKey44:
                    case svwsIdField.svwsIfKey45:
                    case svwsIdField.svwsIfObj:
                        stCardFields.Fields[i] = new svwsstField();
                        stCardFields.Fields[i].Id = card.Fields[i].Id;
                        stCardFields.Fields[i].Description = card.Fields[i].Description;
                        stCardFields.Fields[i].OpF_ControlType = card.Fields[i].OpF_ControlType;
                        stCardFields.Fields[i].OpF_DataType = card.Fields[i].OpF_DataType;

                        switch (card.Fields[i].OpF_DataType)
                        {
                            case svwsDataType.svwsDtNumeric:
                                stCardFields.Fields[i].Value = card.Fields[i].Value;
                                break;
                            case svwsDataType.svwsDtString:
                                stCardFields.Fields[i].Value = card.Fields[i].Value;
                                break;
                            case svwsDataType.svwsDtDate:
                                stCardFields.Fields[i].Value = card.Fields[i].Value;
                                break;
                            default:
                                stCardFields.Fields[i].Value = card.Fields[i].Value;
                                break;
                        }
                        break;
                    case svwsIdField.svwsIfProtocol:
                    case svwsIdField.svwsIfReference:
                        stCardFields.Fields[i] = new svwsstField();
                        stCardFields.Fields[i].Id = card.Fields[i].Id;
                        stCardFields.Fields[i].Description = card.Fields[i].Description;
                        stCardFields.Fields[i].Value = card.Fields[i].Value;
                        stCardFields.Fields[i].OpF_ControlType = card.Fields[i].OpF_ControlType;
                        stCardFields.Fields[i].OpF_DataType = card.Fields[i].OpF_DataType;
                        break;
                }
            }

            stCard.Fields = stCardFields;
        }


        private void SetVisibility(ref svwsstCard stCard,
                                   svwsstUsers cardUsers, svwsstOffices cardOffices, svwsstGroups cardGroups)
        {
            //' USERS
            int lNumUsers = cardUsers.Size;
            svwsstUsers stUsers = new svwsstUsers();
            stUsers.Size = lNumUsers;
            stUsers.Users = new List<svwsstUser>();
            //stUsers.Users = new svwsstUser[lNumUsers];

            for (int l = 0; l < lNumUsers; l++)
            {
                svwsstUser utente = new svwsstUser();
                //' Code
                utente.Code = cardUsers.Users[l].Code;
                //' UserID
                utente.UserID = cardUsers.Users[l].UserID;
                //' Name
                utente.Name = cardUsers.Users[l].Name;
                //' UserType
                utente.UserType = cardUsers.Users[l].UserType;
                //' Email
                utente.Email = cardUsers.Users[l].Email;
                stUsers.Users.Add(utente);
                //stUsers.Users[l] = utente;

                #region Vecchio codice

                ////' Code
                //stUsers.Users[l].Code = cardUsers.Users[l].Code;
                ////' UserID
                //stUsers.Users[l].UserID = cardUsers.Users[l].UserID;
                ////' Name
                //stUsers.Users[l].Name = cardUsers.Users[l].Name;
                ////' UserType
                //stUsers.Users[l].UserType = cardUsers.Users[l].UserType;
                ////' Email
                //stUsers.Users[l].Email = cardUsers.Users[l].Email; 

                #endregion
            }

            //' OFFICES
            int lNumOffices = cardOffices.Size;
            svwsstOffices stOffices = new svwsstOffices();
            stOffices.Size = lNumOffices;
            stOffices.Offices = new List<svwsstOffice>(lNumOffices);
            //stOffices.Offices = new svwsstOffice[lNumOffices];

            for (int l = 0; l < lNumOffices; l++)
            {
                svwsstOffice ufficio = new svwsstOffice();
                //' Code
                ufficio.Code = cardOffices.Offices[l].Code;
                //' Name
                stOffices.Offices[l].Name = stOffices.Offices[l].Name;
                stOffices.Offices.Add(ufficio);
                //stOffices.Offices[l] = ufficio;

                #region Vecchio codice

                //stOffices.Offices[l] = new svwsstOffice();
                ////' Code
                //stOffices.Offices[l].Code = cardOffices.Offices[l].Code;
                ////' Name
                //stOffices.Offices[l].Name = stOffices.Offices[l].Name; 

                #endregion
            }

            //' GROUPS
            int lNumGroups = cardGroups.Size;
            svwsstGroups stGroups = new svwsstGroups();
            stGroups.Size = lNumGroups;
            stGroups.Groups = new List<svwsstGroup>(lNumGroups);
            //stGroups.Groups = new svwsstGroup[lNumGroups];

            for (int l = 0; l < lNumGroups - 1; l++)
            {
                svwsstGroup gruppo = new svwsstGroup();
                //' Code
                stGroups.Groups[l].Code = cardGroups.Groups[l].Code;
                //' Name
                stGroups.Groups[l].Name = stGroups.Groups[l].Name;
                stGroups.Groups.Add(gruppo);
                //stGroups.Groups[l] = gruppo;

                #region Vecchio codice

                //stGroups.Groups[l] = new svwsstGroup();
                ////' Code
                //stGroups.Groups[l].Code = cardGroups.Groups[l].Code;
                ////' Name
                //stGroups.Groups[l].Name = stGroups.Groups[l].Name; 

                #endregion
            }

            stCard.Users = stUsers;
            stCard.Offices = stOffices;
            stCard.Groups = stGroups;
        }

        #endregion

        #region Nuovi
        private void SetCarVisibility(svwsstCard card, svwsstDocumentType documentType)
        {
            SetCardOffices(card, documentType.OfficesWrite);
            SetCardOffices(card, documentType.OfficesRead);
            SetCardUsers(card, documentType.UsersWrite);
            SetCardUsers(card, documentType.UsersRead);
            card.Groups = SetEmptyGroups();
        }

        private void SetCardUsers(svwsstCard card, svwsstUsers users)
        {
            if (card.Users == null)
            {
                card.Users = SetEmptyUsers();
            }

            if (users.Size == 0)
            {
                return;
            }

            foreach (svwsstUser user in users.Users)
            {
                bool found = false;
                foreach (svwsstUser cardUser in card.Users.Users)
                {
                    if (user.UserID == cardUser.UserID)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    svwsstUser newUser = new svwsstUser
                    {
                        Code = user.Code,
                        UserID = user.UserID,
                        UserType = user.UserType,
                        Email = user.Email,
                        Name = user.Name
                    };
                    card.Users.Users.Add(newUser);
                    card.Users.Size++;

                }
            }
        }

        private void SetCardOffices(svwsstCard card, svwsstOffices offices)
        {
            if (card.Offices == null)
            {
                card.Offices = SetEmptyOfficers();
            }

            if (offices.Size == 0)
            {
                return;
            }

            foreach (svwsstOffice office in offices.Offices)
            {
                bool found = false;
                foreach (svwsstOffice cardOffice in card.Offices.Offices)
                {
                    if (office.Code == cardOffice.Code)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    svwsstOffice newOffice = new svwsstOffice
                    {
                        Code = office.Code,
                        Name = office.Name
                    };
                    card.Offices.Offices.Add(newOffice);
                    card.Offices.Size++;

                }
            }
        }

        private void SetCardFields(svwsstCard card, Documento documento)
        {
            List<svwsIdField> fieldsId = Enum.GetValues(typeof(svwsIdField)).Cast<svwsIdField>().ToList();
            if (card.Fields == null)
            {
                card.Fields = SetEmptyCardFields();
            }

            foreach (svwsIdField fieldId in fieldsId)
            {
                //if (fieldId == svwsIdField.svwsIfReference || fieldId == svwsIdField.svwsIfDateReg)
                //{
                //    continue;
                //}
                String fieldValue = documento.Get(fieldId.ToString(), "xxIGNORExx");
                if (fieldValue != "xxIGNORExx")
                {
                    svwsstField cardField = new svwsstField();
                    cardField.Id = fieldId;
                    cardField.Value = fieldValue;
                    card.Fields.Fields.Add(cardField);
                }
            }
        }

        private void SetCardFields(svwsstCard card, svwsstFields documentFields, Documento documento)
        {
            if (card.Fields == null)
            {
                card.Fields = SetEmptyCardFields();
            }

            if (documentFields.Size == 0)
            {
                return;
            }

            foreach (svwsstField field in documentFields.Fields)
            {
                if (!field.OpF_IsNotModify)
                {
                    String fieldValue = documento.Get(field.Id.ToString(), null);
                    if (fieldValue != null)
                    {
                        svwsstField cardField = new svwsstField();
                        cardField.Id = field.Id;
                        cardField.Value = fieldValue;
                        card.Fields.Fields.Add(cardField);
                        card.Fields.Size++;
                    }
                }
            }
        }

        private String TryGetExtension(String fileName)
        {
            String retval = null;
            if (!String.IsNullOrEmpty(fileName))
            {
                String[] parts = fileName.Split('.');
                if (parts.Length > 1)
                {
                    retval = parts[parts.Length - 1];
                }
            }

            return retval;
        }
        #endregion
    }
}
