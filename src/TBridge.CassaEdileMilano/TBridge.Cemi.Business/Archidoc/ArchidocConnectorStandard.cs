﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Archidoc.Type.Collection;
using TBridge.Cemi.Business.Archidoc.Enums;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Archidoc.Type.Entities;
using System.Web.Services.Protocols;
using System.Collections;
using System.Configuration;
using TBridge.Cemi.Archidoc.Type.Collections;

namespace TBridge.Cemi.Business.Archidoc
{
    internal partial class ArchidocConnectorStandard : IArchidocService
    {
        #region IArchidocService Members

        public byte[] GetDocument(string guid)
        {
            return GetDocument(guid, true);
        }

        public byte[] GetDocument(string guid, Boolean tiffToPdf)
        {
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            string sessione = string.Empty;

            try
            {
                #region LOGIN

                String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
                String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
                String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
                String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
                String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
                String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

                //archidocSession.Server = archidocServer;
                //archidocSession.MainEM = archidocMainEm;
                //archidocSession.GpEM = archidocGpEm;
                //archidocSession.Database = archidocDatabase;

                //archidocSession.OpenMode = svwsOpenDatabase.svwsOdShared;
                archidocSession.Language = svwsLanguage.svwsLgItalian;

                sessione = archidocService.Login(archidocUsername, archidocPassword, ref archidocSession);

                #endregion

                svwsstDocument doc = archidocService.Card_Document(sessione, guid, 0, true, tiffToPdf, true);

                archidocService.Logout(sessione);

                #region to file
                /*
                string fileName = "pippo.tif";
                string filePathComplete = @"c:\temp\" + fileName;

                System.IO.File.CreateText(filePathComplete).Close();

                FileStream fs = new FileStream(filePathComplete, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Write(doc.DocumentFile, 0, doc.DocumentFile.Length);
                fs.Close();
                
                //System.Diagnostics.Process.Start(filePathComplete);
                */

                #endregion

                return doc.DocumentFile;
            }
            catch (Exception ex)
            {
                archidocService.Logout(sessione);
                throw;
            }
        }

        public void UpdateCard(Documento documento, string nomeArchivio, string nomeTipoDocumento)
        {
            svwsstCards cards = null;
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            string sessione = string.Empty;

            try
            {
                #region LOGIN
                String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
                String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
                String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
                String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
                String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
                String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

                //archidocSession.Server = archidocServer;
                //archidocSession.MainEM = archidocMainEm;
                //archidocSession.GpEM = archidocGpEm;
                //archidocSession.Database = archidocDatabase;

                //archidocSession.OpenMode = svwsOpenDatabase.svwsOdShared;
                archidocSession.Language = svwsLanguage.svwsLgItalian;

                sessione = archidocService.Login(archidocUsername, archidocPassword, ref archidocSession);


                #endregion

                #region individuamo gli archivi ed i tipi documenti

                //carchiamo l'archivio generale e ne prendiamo l'ID
                svwsstArchive archivioGenerale = null;
                svwsstArchives archivi = archidocService.Archives_Load(sessione, svwsAccessLevel.svwsAlAny);

                for (int i = 0; i < archivi.Size; i++)
                {
                    if ((archivi.Archives[i]).Description == nomeArchivio)
                        archivioGenerale = archivi.Archives[i];
                }

                short idTipoDocumentoDaRicercare = 0;

                svwsstDocumentTypes tipiDocumento = archidocService.DocumentTypes_LoadFromArchives(sessione,
                                                                                                   svwsAccessLevel.
                                                                                                       svwsAlAny,
                                                                                                   new short[1]
                                                                                                       {
                                                                                                           archivioGenerale
                                                                                                               .Id
                                                                                                       });

                for (int j = 0; j < tipiDocumento.Size; j++)
                {
                    if (tipiDocumento.DocumentTypes[j].Description == nomeTipoDocumento)
                    {
                        idTipoDocumentoDaRicercare = tipiDocumento.DocumentTypes[j].Id;
                        break;
                    }
                }

                #endregion

                svwsstSearchCriteria searchCriteria = new svwsstSearchCriteria();

                #region settiamo l'archivio di ricerca

                svwsstArchives archiviRicerca = new svwsstArchives();
                svwsstArchive archivioRicerca = new svwsstArchive();
                List<svwsstArchive> archivioArrayRicerca = new List<svwsstArchive>();

                archivioRicerca.Id = archivioGenerale.Id;

                archivioRicerca.OfficesRead = SetEmptyOfficers();
                archivioRicerca.OfficesWrite = SetEmptyOfficers();
                archivioRicerca.UsersRead = SetEmptyUsers();
                archivioRicerca.UsersWrite = SetEmptyUsers();

                archivioArrayRicerca.Add(archivioRicerca);
                archiviRicerca.Archives = archivioArrayRicerca;
                searchCriteria.Archives = archiviRicerca;

                #endregion

                #region Settiamo il tipo documento di ricerca

                svwsstDocumentType tipoDocumentoRicerca = new svwsstDocumentType();

                tipoDocumentoRicerca.OfficesRead = SetEmptyOfficers();
                tipoDocumentoRicerca.OfficesWrite = SetEmptyOfficers();
                tipoDocumentoRicerca.UsersRead = SetEmptyUsers();
                tipoDocumentoRicerca.UsersWrite = SetEmptyUsers();
                tipoDocumentoRicerca.OpDT_AutoSend = true;

                tipoDocumentoRicerca.Id = idTipoDocumentoDaRicercare;

                searchCriteria.DocumentType = tipoDocumentoRicerca;

                #endregion

                #region Settiamo il tipo di documento esterno di ricerca

                searchCriteria.ExtDocumentTypes = new svwsstDocumentTypes();
                searchCriteria.ExtDocumentTypes.Size = 0;

                #endregion

                #region settiamo le annotazioni

                // Stringa da ricercare nelle annotazioni
                searchCriteria.AnnotationValue = "";

                #endregion

                #region settiamo i campi di ricerca ifKey

                ArrayList campiCollection = new ArrayList();

                svwsstFields campi = new svwsstFields();
                svwsstFields campo = new svwsstFields();
                svwsstFields campiRicerca = new svwsstFields();

                campi.Fields = new List<svwsstField>();
                campi.Fields.Add(new svwsstField());

                campi.Fields[0].Value = documento.ProtocolloScansione;
                campi.Fields[0].ValueTo = documento.ProtocolloScansione; // "67516/08";
                campi.Fields[0].Id = svwsIdField.svwsIfReference;

                campi.Size = campi.Fields.Count;
                for (int j = 0; j < campi.Fields.Count; j++)
                {
                    campiCollection.Add(campi.Fields[j]);
                }

                List<svwsstField> _campi = new List<svwsstField>(campiCollection.Count);
                for (int u = 0; u < campiCollection.Count; u++)
                {
                    _campi.Add(new svwsstField());
                    _campi[u] = (svwsstField)campiCollection[u];
                }
                campiRicerca.Size = campiCollection.Count;
                campiRicerca.Fields = _campi;
                searchCriteria.Fields = campiRicerca;

                #endregion

                #region Register operation type

                searchCriteria.RegisterOperationType = svwsRegisterOperationType.svwsRoSearchOr;

                searchCriteria.RegisterRecords = new svwsstRegisterRecords();
                searchCriteria.RegisterRecords.Size = 0;
                searchCriteria.RegisterRecords.RegisterRecords = new List<svwsstRegisterRecord>();

                #endregion

                #region parametri

                // Tipo di ricerca (Indici, Generica, Fulltext) 
                searchCriteria.SearchType = svwsSearchType.svwsSctIndexes; //.svwsSctGeneric;
                searchCriteria.ContextSearch = svwsContextSearch.svwsCxsArchive;
                // Indica se controllare il tempo di ricerca.
                searchCriteria.CheckSearchTooLong = false;
                // Limite di rintracciati per la ricerca
                searchCriteria.MaxFounded = 1000;
                // Limite di tempo per la ricerca
                searchCriteria.MaxTimeWait = 1000;
                // Dare priorità ai campi chiave in ricerca?
                searchCriteria.IsForcedIndex = false;
                // Dare priorità ai campi data in ricerca?
                searchCriteria.IsForcedDate = false;
                // Avvisare in caso di ricerca troppo lunga
                searchCriteria.AlertSearchTooLong = 1000;

                #endregion

                cards = archidocService.Cards_Search(sessione, ref searchCriteria);

                if (cards.Cards.Count == 1)
                {
                    for (int k = 0; k < cards.Size; k++)
                    {
                        string guidCard = cards.Cards[k].GuidCard;

                        string campostring = string.Empty;
                        for (int p = 0; p < cards.Cards[k].Fields.Fields.Count; p++)
                        {
                            cards.Cards[k].Fields.Fields[p].Value =
                                documento.Get(
                                    cards.Cards[k].Fields.Fields[p].Id.ToString(), //archidocKey
                                    cards.Cards[k].Fields.Fields[p].Value); //valoreoriginale                            
                        }
                    }

                    svwsstCard c = SetCard(cards.Cards[0]);

                    archidocService.Card_Modify(sessione, c, false, false);
                }

                #region LOGOUT

                archidocService.Logout(sessione);

                #endregion
            }
            catch (SoapException e)
            {
                string p = e.Detail.InnerText;
                archidocService.Logout(sessione);
            }
            catch (Exception e)
            {
                archidocService.Logout(sessione);
            }
            //return cards;
        }

        public void InsertCardWithDocument(string nomeArchivio, string nomeTipoDocumento, Documento documento)
        {
            svwsstArchive archiveUsed = null;
            svwsstArchives archives = null;
            svwsstDocumentTypes documentTypes = null;
            svwsstDocumentType documentTypeUsed = null;
            svwsstFields documentTypeFields = null;
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession;
            String sessionId = String.Empty;
            //String guidCard = null;

            try
            {
                #region LOGIN

                String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
                String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
                String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
                String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
                String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
                String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

                //archidocSession.Server = archidocServer;
                //archidocSession.MainEM = archidocMainEm;
                //archidocSession.GpEM = archidocGpEm;
                //archidocSession.Database = archidocDatabase;
             
                archidocSession = new svwsstSession();
                archidocSession.Language = svwsLanguage.svwsLgItalian;
                //archidocSession.OpenMode = svwsOpenDatabase.svwsOdShared;

                sessionId = archidocService.Login(archidocUsername, archidocPassword, ref archidocSession);

                #endregion

                #region individuamo gli archivi ed i tipi documenti
                //carchiamo l'archivio generale e ne prendiamo l'ID
                archives = archidocService.Archives_Load(sessionId, svwsAccessLevel.svwsAlAny);

                archiveUsed = archives.Archives.FirstOrDefault(arc => arc.Description == nomeArchivio);

                if (archiveUsed == null)
                {
                    throw new NullReferenceException(String.Format("Archive <{0}> not found.", nomeArchivio));
                }

                documentTypes = archidocService.DocumentTypes_LoadFromArchives(sessionId, svwsAccessLevel.svwsAlAny, new short[1] { archiveUsed.Id });

                documentTypeUsed = documentTypes.DocumentTypes.FirstOrDefault(type => type.Description == nomeTipoDocumento);

                if (documentTypeUsed == null)
                {
                    throw new NullReferenceException(String.Format("Document Type <{0}> not found.", nomeTipoDocumento));
                }

                documentTypeFields = archidocService.DocumentType_Fields(sessionId, documentTypeUsed.Id);
                #endregion

                #region InsertCard
                InsertCardAndDocument(archidocService, sessionId, nomeTipoDocumento, archiveUsed, documentTypeUsed, documentTypeFields, documento);
                #endregion
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                archidocService.Logout(sessionId);
                archidocService.Close();
            }
        }

        public void InsertCardsWithDocuments<T>(string nomeArchivio, string nomeTipoDocumento, List<T> documenti) where T : Documento
        {
            svwsstArchive archiveUsed = null;
            svwsstArchives archives = null;
            svwsstDocumentTypes documentTypes = null;
            svwsstDocumentType documentTypeUsed = null;
            svwsstFields documentTypeFields = null;
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession;
            String sessionId = String.Empty;
            //String guidCard = null;

            try
            {
                #region LOGIN

                String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
                String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
                String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
                String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
                String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
                String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

                //archidocSession.Server = archidocServer;
                //archidocSession.MainEM = archidocMainEm;
                //archidocSession.GpEM = archidocGpEm;
                //archidocSession.Database = archidocDatabase;

                archidocSession = new svwsstSession();
                archidocSession.Language = svwsLanguage.svwsLgItalian;
                //archidocSession.OpenMode = svwsOpenDatabase.svwsOdShared;

                sessionId = archidocService.Login(archidocUsername, archidocPassword, ref archidocSession);

                #endregion

                #region individuamo gli archivi ed i tipi documenti
                //carchiamo l'archivio generale e ne prendiamo l'ID
                archives = archidocService.Archives_Load(sessionId, svwsAccessLevel.svwsAlAny);

                archiveUsed = archives.Archives.FirstOrDefault(arc => arc.Description == nomeArchivio);

                if (archiveUsed == null)
                {
                    throw new NullReferenceException(String.Format("Archive <{0}> not found.", nomeArchivio));
                }

                documentTypes = archidocService.DocumentTypes_LoadFromArchives(sessionId, svwsAccessLevel.svwsAlAny, new short[1] { archiveUsed.Id });

                documentTypeUsed = documentTypes.DocumentTypes.FirstOrDefault(type => type.Description == nomeTipoDocumento);

                if (documentTypeUsed == null)
                {
                    throw new NullReferenceException(String.Format("Document Type <{0}> not found.", nomeTipoDocumento));
                }

                documentTypeFields = archidocService.DocumentType_Fields(sessionId, documentTypeUsed.Id);
                #endregion

                #region InsertCards
                foreach (Documento documento in documenti)
                {
                    InsertCardAndDocument(archidocService, sessionId, nomeTipoDocumento, archiveUsed, documentTypeUsed, documentTypeFields, documento);
                }

                #endregion
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                archidocService.Logout(sessionId);
                archidocService.Close();
            }
        }

        #region Allegati

        public void InsertAllegatoEsterno(String cardGuid, String note, String nomeFile, Byte[] fileByteArray)
        { 
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            string sessioneId = string.Empty;

            try
            {
                #region LOGIN
                sessioneId = this.LoginSesione(archidocService, archidocSession);
                #endregion
         
                svwsstAttachment allegato = new svwsstAttachment();
                allegato.GuidCard = cardGuid;
                allegato.AttachmentFile = fileByteArray;
                allegato.Note = note;
                allegato.Name = nomeFile;
                allegato.IsInternal = false;

                archidocService.Card_Attachment_InsertExternal(sessioneId, allegato, false, false);
                
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                archidocService.Logout(sessioneId);
                archidocService.Close();
            }
        }

        public void InsertAllegatoInterno(String cardGuid, String progressivoAnnuo, String note, String nomeArchivio)
        {
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            svwsstArchive archivioGenerale = null;
            string sessioneId = string.Empty;

            try
            {
                #region LOGIN
                sessioneId = this.LoginSesione(archidocService, archidocSession);
                #endregion

                //carchiamo l'archivio generale e ne prendiamo l'ID
                archivioGenerale = GetArchivioByNome(archidocService, sessioneId, nomeArchivio);
                if (archivioGenerale != null)
                {
                    svwsstAttachment allegato = new svwsstAttachment();
                    allegato.GuidCard = cardGuid;
                    allegato.ArchiveId = archivioGenerale.Id;
                    allegato.Note = note;
                    allegato.Name = progressivoAnnuo;
                    allegato.IsInternal = false;

                    archidocService.Card_Attachment_InsertInternal(sessioneId, allegato, false, false);
                }

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                archidocService.Logout(sessioneId);
                archidocService.Close();
            }
        }

        public AllegatoCollection GetAllegati(String cardGuid, Boolean getDocumenti)
        {
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            AllegatoCollection allegati = new AllegatoCollection();
            string sessioneId = string.Empty;

            try
            {
                #region LOGIN
                sessioneId = this.LoginSesione(archidocService, archidocSession);
                #endregion

                svwsstAttachments allegatiArchidoc = archidocService.Card_Attachments(sessioneId, cardGuid, getDocumenti, true);

                if (allegatiArchidoc.Size > 0)
                {
                   //allegati = (AllegatoCollection)allegatiArchidoc.Attachments.ConvertAll(new Converter<svwsstAttachment, Allegato>(ArchidocHelpers.SvwsstAttachmentToAllegato));
                    foreach ( svwsstAttachment  item in allegatiArchidoc.Attachments)
                    {
                        allegati.Add(ArchidocHelpers.SvwsstAttachmentToAllegato(item));
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                archidocService.Logout(sessioneId);
                archidocService.Close();
            }

            return allegati;
        }

        public Allegato GetAllegatoEsternoByNomeFile(String cardGuid, String nomeFile, Boolean getDocumento)
        {
            AllegatoCollection allegati = GetAllegati(cardGuid, getDocumento);
            //foreach (Allegato item in allegati)
            //{
            //    if (item.NomeFile.Equals(nomeFile, StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        return item;
            //    }
            //}

            return allegati.FirstOrDefault(x => x.NomeFile.Equals(nomeFile, StringComparison.CurrentCultureIgnoreCase));
        }

        
        public void DeleteAllegato(String cardGuid, String nomeAllegato)
        {
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            string sessioneId = string.Empty;

            try
            {
                #region LOGIN
                sessioneId = this.LoginSesione(archidocService, archidocSession);
                #endregion

                svwsstAttachment allegatoDaCancellare = null;
                svwsstAttachments allegatiArchidoc = archidocService.Card_Attachments(sessioneId, cardGuid, false, true);
                if (allegatiArchidoc.Size > 0)
                {
                    allegatoDaCancellare = allegatiArchidoc.Attachments.FirstOrDefault(x => x.Name.Equals(nomeAllegato, StringComparison.CurrentCultureIgnoreCase));
                    
                    if (allegatoDaCancellare != null)
                    {
                        archidocService.Card_Attachment_Delete(sessioneId, allegatoDaCancellare);
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                archidocService.Logout(sessioneId);
                archidocService.Close();
            }
        }

        #endregion

        #region Metodi Get
        public CodiceFiscaleCollection GetCodiciFiscali(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            CodiceFiscaleCollection codiciFiscali = new CodiceFiscaleCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Codice Fiscale";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             TipiRicercaArchidoc.DataScansione);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            CodiceFiscale codiceFiscale = new CodiceFiscale();
                            codiceFiscale.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campoArchidoc in card.Fields.Fields)
                            {
                                codiceFiscale.Set(
                                    campoArchidoc.Id.ToString(), //archidocKey
                                    campoArchidoc.Value //value
                                    );
                            }
                            codiciFiscali.Add(codiceFiscale);
                        }
            }

            return codiciFiscali;
        }

        public ModuloCollection GetModuliPrestazioni(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            ModuloCollection domandePrestazioni = new ModuloCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Prestazioni";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             TipiRicercaArchidoc.DataScansione);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            Modulo modulo = new Modulo();
                            modulo.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campoArchidoc in card.Fields.Fields)
                            {
                                modulo.Set(
                                    campoArchidoc.Id.ToString(), //archidocKey
                                    campoArchidoc.Value //value
                                    );
                            }
                            domandePrestazioni.Add(modulo);
                        }
            }

            return domandePrestazioni;
        }

        public StatoFamigliaCollection GetStatiDiFamiglia(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            StatoFamigliaCollection statiDiFamiglia = new StatoFamigliaCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Stato di Famiglia";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             TipiRicercaArchidoc.DataScansione);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            StatoFamiglia statoFamiglia = new StatoFamiglia();
                            statoFamiglia.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campoArchidoc in card.Fields.Fields)
                            {
                                statoFamiglia.Set(
                                    campoArchidoc.Id.ToString(), //archidocKey
                                    campoArchidoc.Value //value
                                    );
                            }
                            statiDiFamiglia.Add(statoFamiglia);
                        }
            }

            return statiDiFamiglia;
        }

        public CertificatoCollection GetCertificati(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            CertificatoCollection certificati = new CertificatoCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Certificati";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             TipiRicercaArchidoc.DataScansione);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            Certificato certificato = new Certificato();
                            certificato.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campo in card.Fields.Fields)
                            {
                                certificato.Set(
                                    campo.Id.ToString(), //archidocKey
                                    campo.Value //value
                                    );
                            }
                            certificati.Add(certificato);
                        }
            }

            return certificati;
        }

        public FatturaCollection GetFatture(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            FatturaCollection fatture = new FatturaCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Fatture";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             TipiRicercaArchidoc.DataScansione);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            Fattura fattura = new Fattura();
                            fattura.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campo in card.Fields.Fields)
                            {
                                fattura.Set(
                                    campo.Id.ToString(), //archidocKey
                                    campo.Value //value
                                    );
                            }
                            fatture.Add(fattura);
                        }
            }

            return fatture;
        }

        public DichiarazioneInterventoCollection GetDichiarazioniIntervento(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            DichiarazioneInterventoCollection dichiarazioni = new DichiarazioneInterventoCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Dettaglio Fatture";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             TipiRicercaArchidoc.DataScansione);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            DichiarazioneIntervento dichiarazione = new DichiarazioneIntervento();
                            dichiarazione.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campoArchidoc in card.Fields.Fields)
                            {
                                dichiarazione.Set(
                                    campoArchidoc.Id.ToString(), //archidocKey
                                    campoArchidoc.Value //value
                                    );
                            }
                            dichiarazioni.Add(dichiarazione);
                        }
            }

            return dichiarazioni;
        }

        public DelegaCollection GetDeleghe(Int32 delegaDa, Int32 delegaA)
        {
            DelegaCollection deleghe = new DelegaCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Deleghe";

            for (int delegaScansione = delegaDa; delegaScansione <= delegaA; delegaScansione++)
            {
                svwsstCards cards = GetCards(nomeArchivio, nomeTipoDocumento,
                                             delegaScansione.ToString(),
                                             delegaScansione.ToString(),
                                             TipiRicercaArchidoc.IdDelega);
                if (cards != null)
                    if (cards.Cards != null)
                        foreach (svwsstCard card in cards.Cards)
                        {
                            Delega delega = new Delega();
                            delega.SetIdArchidoc(card.GuidCard);
                            foreach (svwsstField campo in card.Fields.Fields)
                            {
                                delega.Set(
                                    campo.Id.ToString(), //archidocKey
                                    campo.Value //value
                                    );
                            }
                            deleghe.Add(delega);
                        }
            }

            return deleghe;
        }

        #endregion

        #endregion

        #region Private

        private String InsertCardAndDocument(SvAolWSServiceSoapClient archidocService, String sessionId, String nomeTipoDocumento, svwsstArchive archiveUsed, svwsstDocumentType documentTypeUsed, svwsstFields documentTypeFields, Documento documento)
        {
            String guidCard = null;
            svwsstCard card = new svwsstCard();
            card.ProcWF = svwsProcWF.svwsPWFNothing;
            card.Archive = archiveUsed;
            card.DocumentType = documentTypeUsed;
            SetCarVisibility(card, documentTypeUsed);
            SetCardFields(card, documentTypeFields, documento);

            guidCard = archidocService.Card_Insert(sessionId, false, card, SetEmptyUsers(), SetEmptyOfficers(), SetEmptyGroups(), null, null, true, false);
            
            if (!String.IsNullOrEmpty(guidCard) && documento.FileByteArray != null && documento.FileByteArray.Length > 0)
            {
                svwsstDocument cardDocument = new svwsstDocument();
                cardDocument.GuidCard = guidCard;
                cardDocument.SubNetId = 0;
                cardDocument.DocumentFile = documento.FileByteArray;
                cardDocument.FileSize = documento.FileByteArray.Length;
                cardDocument.Name = !String.IsNullOrEmpty(documento.FileNome) ? documento.FileNome : String.Format("{0}_{1}", nomeTipoDocumento, guidCard);
                cardDocument.Extension = !String.IsNullOrEmpty(documento.FileEstensione) ? documento.FileEstensione : this.TryGetExtension(documento.FileNome);

                archidocService.Card_Document_Insert(sessionId, cardDocument, true, false);
            }

            documento.IdArchidoc = guidCard;

            return guidCard;
        }

        private svwsstCards GetCards(string nomeArchivio, string nomeTipoDocumento, string da, string a,
                                    TipiRicercaArchidoc tipoRicerca)
        {
            svwsstCards cards = null;
            SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
            svwsstSession archidocSession = new svwsstSession();
            string sessione = string.Empty;

            try
            {
                #region LOGIN

                String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
                String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
                String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
                String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
                String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
                String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

                //archidocSession.Server = archidocServer;
                //archidocSession.MainEM = archidocMainEm;
                //archidocSession.GpEM = archidocGpEm;
                //archidocSession.Database = archidocDatabase;

                //archidocSession.OpenMode = svwsOpenDatabase.svwsOdShared;
                archidocSession.Language = svwsLanguage.svwsLgItalian;

                sessione = archidocService.Login(archidocUsername, archidocPassword, ref archidocSession);

                #endregion

                #region individuamo gli archivi ed i tipi documenti

                //carchiamo l'archivio generale e ne prendiamo l'ID
                svwsstArchive archivioGenerale = null;
                svwsstArchives archivi = archidocService.Archives_Load(sessione, svwsAccessLevel.svwsAlAny);

                for (int i = 0; i < archivi.Size; i++)
                {
                    if ((archivi.Archives[i]).Description == nomeArchivio)
                        archivioGenerale = archivi.Archives[i];
                }

                short idTipoDocumentoDaRicercare = 0;

                svwsstDocumentTypes tipiDocumento = archidocService.DocumentTypes_LoadFromArchives(sessione,
                                                                                                   svwsAccessLevel.
                                                                                                       svwsAlAny,
                                                                                                   new short[1]
                                                                                                       {
                                                                                                           archivioGenerale
                                                                                                               .Id
                                                                                                       });

                for (int j = 0; j < tipiDocumento.Size; j++)
                {
                    if (tipiDocumento.DocumentTypes[j].Description == nomeTipoDocumento)
                    {
                        idTipoDocumentoDaRicercare = tipiDocumento.DocumentTypes[j].Id;
                        break;
                    }
                }

                #endregion

                svwsstSearchCriteria searchCriteria = new svwsstSearchCriteria();

                #region settiamo l'archivio di ricerca

                svwsstArchives archiviRicerca = new svwsstArchives();
                svwsstArchive archivioRicerca = new svwsstArchive();
                List<svwsstArchive> archivioArrayRicerca = new List<svwsstArchive>();

                archivioRicerca.Id = archivioGenerale.Id;

                archivioRicerca.OfficesRead = SetEmptyOfficers();
                archivioRicerca.OfficesWrite = SetEmptyOfficers();
                archivioRicerca.UsersRead = SetEmptyUsers();
                archivioRicerca.UsersWrite = SetEmptyUsers();

                archivioArrayRicerca.Add(archivioRicerca);
                archiviRicerca.Archives = archivioArrayRicerca;
                searchCriteria.Archives = archiviRicerca;

                #endregion

                #region Settiamo il tipo documento di ricerca

                svwsstDocumentType tipoDocumentoRicerca = new svwsstDocumentType();

                tipoDocumentoRicerca.OfficesRead = SetEmptyOfficers();
                tipoDocumentoRicerca.OfficesWrite = SetEmptyOfficers();
                tipoDocumentoRicerca.UsersRead = SetEmptyUsers();
                tipoDocumentoRicerca.UsersWrite = SetEmptyUsers();
                tipoDocumentoRicerca.OpDT_AutoSend = true;

                tipoDocumentoRicerca.Id = idTipoDocumentoDaRicercare;

                searchCriteria.DocumentType = tipoDocumentoRicerca;

                #endregion

                #region Settiamo il tipo di documento esterno di ricerca

                searchCriteria.ExtDocumentTypes = new svwsstDocumentTypes();
                searchCriteria.ExtDocumentTypes.Size = 0;

                #endregion

                #region settiamo le annotazioni

                // Stringa da ricercare nelle annotazioni
                searchCriteria.AnnotationValue = "";

                #endregion

                #region settiamo i campi di ricerca ifKey

                ArrayList campiCollection = new ArrayList();

                svwsstFields campi = new svwsstFields();
                svwsstFields campo = new svwsstFields();
                svwsstFields campiRicerca = new svwsstFields();

                campi.Fields = new List<svwsstField>();
                campi.Fields.Add(new svwsstField());

                campi.Fields[0].Value = da;
                campi.Fields[0].ValueTo = a;

                switch (tipoRicerca)
                {
                    case TipiRicercaArchidoc.DataScansione:
                        campi.Fields[0].Id = svwsIdField.svwsIfDateReg;
                        break;
                    case TipiRicercaArchidoc.ProtocolloArchidoc:
                        campi.Fields[0].Id = svwsIdField.svwsIfReference;
                        break;
                    case TipiRicercaArchidoc.IdDelega:
                        campi.Fields[0].Id = svwsIdField.svwsIfKey22;
                        break;
                }

                campi.Size = campi.Fields.Count;
                for (int j = 0; j < campi.Fields.Count; j++)
                {
                    campiCollection.Add(campi.Fields[j]);
                }

                List<svwsstField> _campi = new List<svwsstField>(campiCollection.Count);
                for (int u = 0; u < campiCollection.Count; u++)
                {
                    _campi.Add(new svwsstField());
                    _campi[u] = (svwsstField)campiCollection[u];
                }
                campiRicerca.Size = campiCollection.Count;
                campiRicerca.Fields = _campi;
                searchCriteria.Fields = campiRicerca;

                #endregion

                #region Register operation type

                searchCriteria.RegisterOperationType = svwsRegisterOperationType.svwsRoSearchOr;

                searchCriteria.RegisterRecords = new svwsstRegisterRecords();
                searchCriteria.RegisterRecords.Size = 0;
                searchCriteria.RegisterRecords.RegisterRecords = new List<svwsstRegisterRecord>();

                #endregion

                #region parametri

                // Tipo di ricerca (Indici, Generica, Fulltext) 
                searchCriteria.SearchType = svwsSearchType.svwsSctIndexes; //.svwsSctGeneric;
                searchCriteria.ContextSearch = svwsContextSearch.svwsCxsArchive;
                // Indica se controllare il tempo di ricerca.
                searchCriteria.CheckSearchTooLong = false;
                // Limite di rintracciati per la ricerca
                searchCriteria.MaxFounded = 1000;
                // Limite di tempo per la ricerca
                searchCriteria.MaxTimeWait = 1000;
                // Dare priorità ai campi chiave in ricerca?
                searchCriteria.IsForcedIndex = false;
                // Dare priorità ai campi data in ricerca?
                searchCriteria.IsForcedDate = false;
                // Avvisare in caso di ricerca troppo lunga
                searchCriteria.AlertSearchTooLong = 1000;

                #endregion

                cards = archidocService.Cards_Search(sessione, ref searchCriteria);

                #region LOGOUT

                archidocService.Logout(sessione);

                #endregion
            }
            catch (SoapException e)
            {
                string p = e.Detail.InnerText;
                archidocService.Logout(sessione);
            }
            catch (Exception e)
            {
                archidocService.Logout(sessione);
            }

            return cards;
        }

        private String LoginSesione(SvAolWSServiceSoapClient archidocService, svwsstSession archidocSession)
        {
            String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
            String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
            String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
            String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
            String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
            String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

            //archidocSession.Server = archidocServer;
            //archidocSession.MainEM = archidocMainEm;
            //archidocSession.GpEM = archidocGpEm;
            //archidocSession.Database = archidocDatabase;

            //archidocSession.OpenMode = svwsOpenDatabase.svwsOdShared;
            archidocSession.Language = svwsLanguage.svwsLgItalian;

            return archidocService.Login(archidocUsername, archidocPassword, ref archidocSession);
        }

        private svwsstArchive GetArchivioByNome(SvAolWSServiceSoapClient archidocService, string sessione, String nomeArchivio)
        {
            svwsstArchives archivi = archidocService.Archives_Load(sessione, svwsAccessLevel.svwsAlAny);

            for (int i = 0; i < archivi.Size; i++)
            {
                if ((archivi.Archives[i]).Description == nomeArchivio)
                    return archivi.Archives[i];
            }
            return null;
        }

        #endregion
    }
}
