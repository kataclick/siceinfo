﻿using System;


namespace TBridge.Cemi.Business.Archidoc
{
    public class ArchidocLogin
    {
        public String ArchidocServer { get; set; }
        public String ArchidocMainEm { get; set; }
        public String ArchidocGpEm { get; set; }
        public String ArchidocDataBase { get; set; }
        public String ArchidocUsername { get; set; }
        public String ArchidocPassword { get; set; }
    }
}
