using TBridge.Cemi.Archidoc.Type.Entities;
using TBridge.Cemi.Archidoc.Type.Collection;
using System;
using System.Collections.Generic;
using TBridge.Cemi.Archidoc.Type.Collections;

namespace TBridge.Cemi.Business.Archidoc.Interfaces
{
    public interface IArchidocService
    {
        /// <summary>
        /// Restituisce i byte del documento ricercato tramite il suo guid
        /// </summary>
        /// <param name="guid">archidoc guid</param>
        /// <returns></returns>
        byte[] GetDocument(string guid);

        /// <summary>
        /// Restituisce i byte del documento ricercato tramite il suo guid
        /// </summary>
        /// <param name="guid">archidoc guid</param>
        /// <param name="tifToPdf">converte tif in pdf (funziona solo con file tif)</param>
        /// <returns></returns>
        byte[] GetDocument(string guid, bool tifToPdf);

        ///// <summary>
        ///// Restituisce i byte del documento ricercato tramite il suo guid
        ///// </summary>
        ///// <param name="guid">archidoc guid</param>
        ///// <param name="tiffToPdf">converte tiff in pdf (funziona solo per i tiff)</param>
        ///// <returns></returns>
        //byte[] GetDocument(string guid, int pageFrom, int pageTo);

        void UpdateCard(Documento documento, string nomeArchivio, string nomeTipoDocumento);

        void InsertCardWithDocument(string nomeArchivio, string nomeTipoDocumento, Documento documento);

        void InsertCardsWithDocuments<T>(string nomeArchivio, string nomeTipoDocumento, List<T> documenti) where T : Documento;

        #region Allegati

        void InsertAllegatoEsterno(String cardGuid, String note, String nomeFile, Byte[] fileByteArray);

        void InsertAllegatoInterno(String cardGuid, String progressivoAnnuo, String note, String nomeArchivio);

        AllegatoCollection GetAllegati(String cardGuid, Boolean getDocumenti);

        Allegato GetAllegatoEsternoByNomeFile(String cardGuid, String nomeFile, Boolean getDocumento);

        void DeleteAllegato(String cardGuid, String nomeAllegato);

        #endregion

        CodiceFiscaleCollection GetCodiciFiscali(DateTime dataScansioneDa, DateTime dataScansioneA);

        ModuloCollection GetModuliPrestazioni(DateTime dataScansioneDa, DateTime dataScansioneA);

        StatoFamigliaCollection GetStatiDiFamiglia(DateTime dataScansioneDa, DateTime dataScansioneA);

        CertificatoCollection GetCertificati(DateTime dataScansioneDa, DateTime dataScansioneA);

        FatturaCollection GetFatture(DateTime dataScansioneDa, DateTime dataScansioneA);

        DichiarazioneInterventoCollection GetDichiarazioniIntervento(DateTime dataScansioneDa, DateTime dataScansioneA);

        DelegaCollection GetDeleghe(Int32 delegaDa, Int32 delegaA);
    }
}