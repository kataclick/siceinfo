﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Filters;

namespace TBridge.Cemi.Business
{
    public class CommittentiManager
    {
        public List<Committente> GetCommittenti(CommittenteFilter filtro)
        {
            List<Committente> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from committente in context.Committenti
                            where (String.IsNullOrEmpty(filtro.Cognome) || committente.Cognome.Contains(filtro.Cognome))
                                && (String.IsNullOrEmpty(filtro.Nome) || committente.Nome.Contains(filtro.Nome))
                                && (String.IsNullOrEmpty(filtro.RagioneSociale) || committente.RagioneSociale.Contains(filtro.RagioneSociale))
                                && (String.IsNullOrEmpty(filtro.CodiceFiscale) || committente.CodiceFiscale.Contains(filtro.CodiceFiscale))
                            select committente;
                ret = query.ToList();
            }

            return ret;
        }

        public Committente GetCommittente(Int32 idCommittente)
        {
            Committente ret = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from committente in context.Committenti
                            where committente.IdCommittente == idCommittente
                            select committente;
                ret = query.Single();
            }

            return ret;
        }
    }
}
