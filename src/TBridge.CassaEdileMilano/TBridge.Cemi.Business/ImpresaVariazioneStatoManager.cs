﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cemi.SiceInfo.Data;
using Cemi.SiceInfo.Type.Enum;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;
using ImpresaRichiestaVariazioneStato = Cemi.SiceInfo.Type.Domain.ImpresaRichiestaVariazioneStato;
using Documento = Cemi.SiceInfo.Type.Domain.Documento;
using Cemi.SiceInfo.Business.Imp.Storage;

namespace TBridge.Cemi.Business
{
    public class ImpresaVariazioneStatoManager
    {
        public List<TipoStatoImpresa> GetTipiStatoImpresa()
        {
            using (SICEEntities context = new SICEEntities())
            {
                return context.TipiStatoImpresa.ToList();
            }
        }

        public List<TipoStatoGestionePratica> GetTipiStatoGestionePratica()
        {
            using (SICEEntities context = new SICEEntities())
            {
                return context.TipiStatoGestionePratica.ToList();
            }
        }

        public bool SaveRichiestaVariazioneStato(ImpresaRichiestaVariazioneStato richiestaVariazioneStato)
        {
            bool ret;

            using (var context = new SiceContext())
            {
                context.ImpreseRichiesteVariazioneStato.Add(richiestaVariazioneStato);
                ret = context.SaveChanges() > 0;
            }
            /*
            using (SICEEntities context = new SICEEntities())
            {
                context.ImpreseRichiesteVariazioneStato.AddObject(richiestaVariazioneStato);
                int numObj = context.SaveChanges();
                if (numObj > 0)
                    ret = true;
            }
            */
            return ret;
        }

        public List<ImpresaRichiestaVariazioneStato> GetImpreseRichiesteVariazioneStato()
        {
            List <ImpresaRichiestaVariazioneStato> ret;

            using (var context = new SiceContext())
            {
                var query = from richiestaVariazioneStato in context.ImpreseRichiesteVariazioneStato
                    select richiestaVariazioneStato;

                ret = query.ToList();
            }

            return ret;
            /*
            using (SICEEntities context = new SICEEntities())
            {
                var richiesteTutte = from richiestaVariazioneStato in context.ImpreseRichiesteVariazioneStato
                                     select richiestaVariazioneStato;

                return richiesteTutte.ToList();
            }
            */
        }

        public List<TipoCausaleRespintaVariazioneStatoImpresa> GetTipiCausaliRespinta()
        {
            using (SICEEntities context = new SICEEntities())
            {
                var tipiCausali = from tipoCausaliRespinta in context.TipiCausaliRespintaVariazioneStatoImpresa
                                  select tipoCausaliRespinta;

                return tipiCausali.ToList();
            }
        }

        public ImpresaVariazioneStato GetImpresaVariazioneStatoByIdRichiesta(int idRichiesta)
        {
            ImpresaVariazioneStato ret;
            //using (SICEEntities context = new SICEEntities())
            using (var context = new SiceContext())
            {
                string libroUnicoDesc = TipiDocumentiStored.LibroUnico.ToString();
                string denunceDesc = TipiDocumentiStored.Denunce.ToString();
                

                var richiesta = from richiestaVariazioneStato in context.ImpreseRichiesteVariazioneStato
                    where richiestaVariazioneStato.Id == idRichiesta
                    select new ImpresaVariazioneStato
                    {
                        IdRichiesta = richiestaVariazioneStato.Id,
                        IdImpresa = richiestaVariazioneStato.IdImpresa,
                        CodiceFiscale = richiestaVariazioneStato.Impresa.CodiceFiscale,
                        RagioneSociale = richiestaVariazioneStato.Impresa.RagioneSociale,
                        StatoImpresaAttuale = richiestaVariazioneStato.Impresa.Stato,
                        DataInizioCambioStato = richiestaVariazioneStato.DataInizioCambioStato,
                        DataRichiesta = richiestaVariazioneStato.DataRichiesta,
                        DataGestioneOperatore = richiestaVariazioneStato.DataGestioneOperatore,
                        IdTipoStatoImpresa = (int) richiestaVariazioneStato.IdTipoStatoImpresa,
                        IdTipoStatoPratica = (int) richiestaVariazioneStato.IdTipoStatoPratica,
                        IdUtente = richiestaVariazioneStato.IdUtente,
                        Login = richiestaVariazioneStato.Utente.Login,
                        //StatoImpresa = richiestaVariazioneStato.IdTipoStatoImpresa.Description(),
                        //StatoPratica = richiestaVariazioneStato.IdTipoStatoPratica.Description(),
                        Note = richiestaVariazioneStato.Note,
                        AllegatoLibroUnicoNome = richiestaVariazioneStato.Documenti.FirstOrDefault(x => x.TipoDocumento == libroUnicoDesc).Filename,
                        AllegatoLibroUnicoIdArchidoc = richiestaVariazioneStato.Documenti.FirstOrDefault(x => x.TipoDocumento == libroUnicoDesc).DocumentaleId,
                        AllegatoDenunceNome = richiestaVariazioneStato.Documenti.FirstOrDefault(x => x.TipoDocumento == denunceDesc).Filename,
                        AllegatoDenunceIdArchidoc = richiestaVariazioneStato.Documenti.FirstOrDefault(x => x.TipoDocumento == denunceDesc).DocumentaleId,
                        IdTipoCausaleRespinta = richiestaVariazioneStato.IdTipoCausaleRespinta,
                        AltriAllegati = (List<Documento>)richiestaVariazioneStato.Documenti
                    };

                ImpresaVariazioneStato variazione = richiesta.SingleOrDefault();
                

                if (variazione != null)
                {
                    variazione.StatoImpresa = ((TipiStatoImpresa)variazione.IdTipoStatoImpresa).Description();
                    variazione.StatoPratica = ((TipiStatoGestionePratica)variazione.IdTipoStatoPratica).Description();
                    variazione.InfoAggiuntiveAnalisi = GetInfoAggiuntiveImpresa(variazione.IdImpresa);
                }

                ret = variazione;
            }

            return ret;
        }

        public byte[] GetImpresaVariazioneStatoAllegatoLibroUnico(int idRichiesta)
        {
            byte[] allegato = GetAllegato(idRichiesta, TipiDocumentiStored.LibroUnico);
            return allegato;
        }

        public byte[] GetImpresaVariazioneStatoAllegatoDenunce(int idRichiesta)
        {
            byte[] allegato = GetAllegato(idRichiesta, TipiDocumentiStored.Denunce);
            return allegato;
        }

        public byte[] GetAllegato(int idRichiesta, TipiDocumentiStored tipoDocumento)
        {
            byte[] allegato = null;

            using (var context = new SiceContext())
            {
                string tipoDoc = tipoDocumento.ToString();

                int? storedDocumentId = (from richiestaVariazioneStato in context.ImpreseRichiesteVariazioneStato
                    where richiestaVariazioneStato.Id == idRichiesta
                    select richiestaVariazioneStato.Documenti.FirstOrDefault(x => x.TipoDocumento == tipoDoc).StoredDocumentId).SingleOrDefault();

                if (storedDocumentId.HasValue)
                {
                    //using (var storageContext = new StorageContext())
                    //{
                    //    allegato = (from document in storageContext.Documents
                    //        where document.Id == storedDocumentId
                    //        select document.File).Single();
                    //}
                    StorageManager storageManager = new StorageManager();
                    allegato = storageManager.Get(storedDocumentId.Value);
                }
            }

            return allegato;
        }

        public byte[] GetGenericAllegato(int idDocumento)
        {
            byte[] allegato = null;

            using (var context = new SiceContext())
            {
                //string tipoDoc = tipoDocumento.ToString();

                int? storedDocumentId = (from documento in context.Documenti
                                         where documento.Id == idDocumento
                                         select documento.StoredDocumentId).SingleOrDefault();

                if (storedDocumentId.HasValue)
                {
                    //using (var storageContext = new StorageContext())
                    //{
                    //    allegato = (from document in storageContext.Documents
                    //        where document.Id == storedDocumentId
                    //        select document.File).Single();
                    //}
                    StorageManager storageManager = new StorageManager();
                    allegato = storageManager.Get(storedDocumentId.Value);
                }
            }

            return allegato;
        }

        private ImpresaVariazioneStato.InfoAggiuntive GetInfoAggiuntiveImpresa(int idImpresa)
        {
            ImpresaVariazioneStato.InfoAggiuntive infoAggiuntive = new ImpresaVariazioneStato.InfoAggiuntive();

            DateTime? dataUltimaDenuncia = DenunceManager.GetDataUltimaDenunciaImpresa(idImpresa);

            if (dataUltimaDenuncia.HasValue)
            {
                infoAggiuntive.DataUltimaDenuncia = dataUltimaDenuncia;

                //dataUltimaDenuncia = dataUltimaDenuncia.Value.AddMonths(-2);
                OreDenunciateAggregateCompleteImpresa oreUltimaDenuncia =
                    DenunceManager.GetOreDenunciateAggregateCompleteImpresa(idImpresa, dataUltimaDenuncia.Value);

                if (oreUltimaDenuncia != null)
                {
                    infoAggiuntive.OreUltimaDenuncia = oreUltimaDenuncia.OreTotali;
                    infoAggiuntive.OreLavorateUltimaDenuncia = oreUltimaDenuncia.OreLavorate;
                }
            }

            using (SICEEntities context = new SICEEntities())
            {
                var queryImpresa = (from impresa in context.Imprese
                    where impresa.Id == idImpresa
                    select
                        new
                        {
                            impresa.DataIscrizione,
                            impresa.DataRipresa,
                            impresa.DataSospensione,
                            impresa.DataDisdetta
                        }).SingleOrDefault();

                if (queryImpresa != null)
                {
                    infoAggiuntive.DataIscrizione = queryImpresa.DataIscrizione;
                    infoAggiuntive.DataRiattivazione = queryImpresa.DataRipresa;
                    infoAggiuntive.DataSospensione = queryImpresa.DataSospensione;
                    infoAggiuntive.DataCessazione = queryImpresa.DataDisdetta;
                }
            }

            infoAggiuntive.Debito = RegolaritaContributivaManager.GetDebitoImpresa(idImpresa);

            return infoAggiuntive;
        }

        public List<ImpresaRichiestaVariazioneStato> GetImpreseRichiesteVariazioneStato(
            ImpresaRichiestaVariazioneStatoFilter filtro)
        {
            //using (SICEEntities context = new SICEEntities())
            using (var context = new SiceContext())
            {
                var richiesteTutte =
                    from richiestaVariazioneStato in
                        context.ImpreseRichiesteVariazioneStato.Include(x => x.Impresa)//.Include("StatoImpresa").Include("Impresa").Include("StatoGestionePratica")
                    select richiestaVariazioneStato;

                if (filtro.IdImpresa.HasValue)
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.IdImpresa == filtro.IdImpresa
                        select richiesta;

                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.Impresa.RagioneSociale.Contains(filtro.RagioneSociale)
                        select richiesta;

                if (filtro.DataRichiestaDa.HasValue)
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.DataRichiesta >= filtro.DataRichiestaDa.Value
                        select richiesta;

                if (filtro.DataRichiestaA.HasValue)
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.DataRichiesta <= filtro.DataRichiestaA.Value
                        select richiesta;

                if (filtro.IdTipoStatoImpresa.HasValue)
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.IdTipoStatoImpresa == (TipiStatoImpresa) filtro.IdTipoStatoImpresa
                        select richiesta;

                if (filtro.IdTipoStatoGestionePratica.HasValue)
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.IdTipoStatoPratica == (TipiStatoGestionePratica) filtro.IdTipoStatoGestionePratica
                        select richiesta;

                if (filtro.DataInizioCambioStato.HasValue)
                    richiesteTutte = from richiesta in richiesteTutte
                        where richiesta.DataInizioCambioStato == filtro.DataInizioCambioStato.Value
                        select richiesta;

                richiesteTutte = from richiesta in richiesteTutte
                    orderby richiesta.DataRichiesta descending
                    select richiesta;

                return richiesteTutte.ToList();
            }
        }

        public bool UpdateStatoPratica(int idRichiesta, int stato, int idUtenteGestione)
        {
            int num;

            using (var context = new SiceContext())
            {
                var richiesta = (from richiestaVariazioneStato in context.ImpreseRichiesteVariazioneStato
                    where richiestaVariazioneStato.Id == idRichiesta
                    select richiestaVariazioneStato).Single();

                richiesta.IdTipoStatoPratica = (TipiStatoGestionePratica) stato;
                richiesta.IdUtenteOperatore = idUtenteGestione;
                richiesta.DataGestioneOperatore = DateTime.Now;

                num = context.SaveChanges();
            }

            return num > 0;
        }

        public bool UpdateStatoPratica(int idRichiesta, int stato, int idUtenteGestione, int idCausaleRespinta)
        {
            int num;

            using (var context = new SiceContext())
            {
                var richiesta = (from richiestaVariazioneStato in context.ImpreseRichiesteVariazioneStato
                                 where richiestaVariazioneStato.Id == idRichiesta
                                 select richiestaVariazioneStato).Single();

                richiesta.IdTipoStatoPratica = (TipiStatoGestionePratica) stato;
                richiesta.IdUtenteOperatore = idUtenteGestione;
                richiesta.DataGestioneOperatore = DateTime.Now;
                richiesta.IdTipoCausaleRespinta = idCausaleRespinta;

                num = context.SaveChanges();
            }

            return num > 0;
        }
    }
}