#region WSCF
//------------------------------------------------------------------------------
// <autogenerated code>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated code>
//------------------------------------------------------------------------------
// File time 24-12-08 11.40 
//
// This source code was auto-generated by WsContractFirst, Version=0.7.6319.1
#endregion


namespace TBridge.Cemi.Business.Office
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    public interface IWordService
    {
        
        string CreateWord(string templatePath, WordField[] properties, string pathGenerazioneDocumenti);
    }
}
