﻿using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
//using Microsoft.Office.Interop.Word;

namespace TBridge.Cemi.Business
{
    public class PdfManager
    {
        //public Stream ConvertWord2Pdf(string wordFilePath)
        //{
        //    // Create a new Microsoft Word application object
        //    Application word = new Application();

        //    // C# doesn't have optional arguments so we'll need a dummy value
        //    object oMissing = Missing.Value;

        //    word.Visible = false;
        //    word.ScreenUpdating = false;

        //    // Cast as Object for word Open method
        //    Object filename = wordFilePath;

        //    // Use the dummy value as a placeholder for optional arguments
        //    Document doc = word.Documents.Open(ref filename, ref oMissing,
        //                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing);
        //    doc.Activate();

        //    object outputFileName = wordFilePath.Replace(".docx", ".pdf");
        //    object fileFormat = WdSaveFormat.wdFormatPDF;

        //    // Save document into PDF Format
        //    doc.SaveAs(ref outputFileName,
        //               ref fileFormat, ref oMissing, ref oMissing,
        //               ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //               ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //               ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        //    // Close the Word document, but leave the Word application open.
        //    // doc has to be cast to type _Document so that it will find the
        //    // correct Close method.
        //    object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
        //    (doc).Close(ref saveChanges, ref oMissing, ref oMissing);
        //    doc = null;


        //    // word has to be cast to type _Application so that it will find
        //    // the correct Quit method.
        //    word.NormalTemplate.Saved = true;
        //    (word).Quit(ref oMissing, ref oMissing, ref oMissing);
        //    word = null;

        //    Stream ms = GetFileStream((string)outputFileName);
        //    if (File.Exists((string)outputFileName))
        //        File.Delete((string)outputFileName);

        //    //Do il tempo a word di chiudersi...
        //    Thread.Sleep(100);

        //    return ms;


        //    //var wordApplication = new Microsoft.Office.Interop.Word.Application();
        //    //try
        //    //{
        //    //    Microsoft.Office.Interop.Word.Document wordDocument = wordApplication.Documents.Open(wordFilePath);

        //    //    if (wordDocument != null)
        //    //        wordDocument.SaveAs(string.Format(@"{0}\temp.pdf", Path.GetTempPath()), Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF);
        //    //    else
        //    //        throw new Exception("CUSTOM ERROR: Cannot Open Word Application");
        //    //    wordDocument.Close();
        //    //    wordApplication.Quit();

        //    //    Stream ms = GetFileStream(string.Format(@"{0}\temp.pdf", Path.GetTempPath()));
        //    //    if (File.Exists(string.Format(@"{0}\temp.pdf", Path.GetTempPath())))
        //    //        File.Delete(string.Format(@"{0}\temp.pdf", Path.GetTempPath()));
        //    //    return ms;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    wordApplication.Quit();
        //    //    throw;
        //    //}
        //}

        public Stream MergePdf(Stream mainPdf, Stream childPdf)
        {
            MemoryStream output = new MemoryStream();
            PdfDocument outputDocument = PdfReader.Open(mainPdf, PdfDocumentOpenMode.Modify);
            // Open the document to import pages from it.
            if (childPdf != null)
            {
                PdfDocument inputDocument = PdfReader.Open(childPdf, PdfDocumentOpenMode.Import);
                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            outputDocument.Save(output, false);
            byte[] buffer = new byte[output.Length];
            output.Seek(0, SeekOrigin.Begin);
            output.Flush();
            output.Read(buffer, 0, (int) output.Length);
            return output;
        }

        public void MergePdf(Stream mainPdf, Stream childPdf, string pathFileDest)
        {
            PdfDocument outputDocument = PdfReader.Open(mainPdf, PdfDocumentOpenMode.Modify);
            // Open the document to import pages from it.
            if (childPdf != null)
            {
                PdfDocument inputDocument = PdfReader.Open(childPdf, PdfDocumentOpenMode.Import);
                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            outputDocument.Save(pathFileDest);
        }

        public Stream MergePdf(string mainPdf, string childPdf)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            return MergePdf(main, child);
        }

        public void MergePdf(string mainPdf, string childPdf, string pathFileDest)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            MergePdf(main, child, pathFileDest);
        }

        public Stream GetPdfPage(Stream pdf, int pageIndex)
        {
            PdfDocument outputDocument = new PdfDocument();
            PdfDocument inputDocument = PdfReader.Open(pdf, PdfDocumentOpenMode.Import);
            if (inputDocument.PageCount < pageIndex || pageIndex < 0)
                return null;

            // Get the page from the external document...
            PdfPage page = inputDocument.Pages[pageIndex];
            // ...and add it to the output document.
            outputDocument.AddPage(page);

            MemoryStream output = new MemoryStream();
            outputDocument.Save(output, false);
            //byte[] buffer = new byte[output.Length];
            output.Seek(0, SeekOrigin.Begin);
            //output.Flush();
            //output.Read(buffer, 0, (int) output.Length);
            return output;
        }

        public Stream GetPdfPage(string pdf, int pageIndex)
        {
            Stream pdfStream = GetFileStream(pdf);
            return GetPdfPage(pdfStream, pageIndex);
        }

        public Stream GetFileStream(string filename)
        {
            MemoryStream ms = new MemoryStream();
            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] bytes = new byte[file.Length];
            file.Read(bytes, 0, (int) file.Length);
            ms.Write(bytes, 0, (int) file.Length);
            file.Close();
            return ms;
        }

        public Stream GetByteStream(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            return ms;
        }
    }
}