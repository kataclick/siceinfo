using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;

namespace TBridge.Cemi.Business
{
    public class Common
    {
        private readonly DateTime LIMITEINFERIORESMALLDATETIME = new DateTime(1900, 1, 1);
        private readonly DateTime LIMITESUPERIORESMALLDATETIME = new DateTime(2079, 6, 1);
        private readonly Data.Common commonData = new Data.Common();

        public string GetDataPremioFedelta()
        {
            return commonData.GetDataPremioFedelta();
        }

        public string GetDataImpreseRegolari()
        {
            return commonData.GetDataImpreseRegolari();
        }

        public int GetMesiDelegheScadute()
        {
            return commonData.GetMesiDelegheScadute();
        }

        public ListDictionary GetNatureGiuridiche()
        {
            return commonData.GetNatureGiuridiche();
        }

        public ListDictionary GetAttivitaIstat()
        {
            return commonData.GetAttivitaIstat();
        }

        public ListDictionary GetTipiImpresa()
        {
            return commonData.GetTipiImpresa();
        }

        /// <summary>
        ///   Ritorna i tipi invio denuncia
        /// </summary>
        /// <returns>(ListDictionary) tipi invio denuncia</returns>
        public ListDictionary GetTipiInvioDenuncia()
        {
            return commonData.GetTipiInvioDenuncia();
        }

        public StringCollection GetProvinceSiceNew()
        {
            return commonData.GetProvinceSiceNew();
        }

        public ComuneSiceNewCollection GetComuniSiceNew(string provincia)
        {
            return commonData.GetComuniSiceNew(provincia);
        }

        public ComuneSiceNew GetComuneSiceNew(String idComune)
        {
            return commonData.GetComuneSiceNew(idComune);
        }

        public ListDictionary GetNazioni()
        {
            return commonData.GetNazioni();
        }

        public ListDictionary GetPreIndirizzi()
        {
            return commonData.GetPreIndirizzi();
        }

        public ListDictionary GetAssociazioniImprenditoriali()
        {
            return commonData.GetAssociazioniImprenditoriali();
        }

        public FrazioneSiceNewCollection GetFrazioniSiceNew(string codiceCatastale)
        {
            return commonData.GetFrazioniSiceNew(codiceCatastale);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ComprensorioSindacaleCollection GetComprensori(bool selezionabili)
        {
            return commonData.GetComprensori(selezionabili);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public SindacatoCollection GetSindacati()
        {
            return commonData.GetSindacati();
        }

        public bool IsGrandeCitta(string codiceCatastale)
        {
            bool res = false;

            switch (codiceCatastale)
            {
                // Bari
                case "A662":
                    res = true;
                    break;
                // Bergamo
                case "A794":
                    res = true;
                    break;
                // Bologna
                case "A944":
                    res = true;
                    break;
                // Brescia
                case "B157":
                    res = true;
                    break;
                // Cagliari
                case "B354":
                    res = true;
                    break;
                // Catania
                case "C351":
                    res = true;
                    break;
                // Firenze
                case "D612":
                    res = true;
                    break;
                // Genova
                case "D969":
                    res = true;
                    break;
                // La Spezia
                case "E463":
                    res = true;
                    break;
                // Livorno
                case "E625":
                    res = true;
                    break;
                // Messina
                case "F158":
                    res = true;
                    break;
                // Milano
                case "F205":
                    res = true;
                    break;
                // Napoli
                case "F839":
                    res = true;
                    break;
                // Padova
                case "G224":
                    res = true;
                    break;
                // Palermo
                case "G273":
                    res = true;
                    break;
                // Perugia
                case "G478":
                    res = true;
                    break;
                // Pescara
                case "G482":
                    res = true;
                    break;
                // Pisa
                case "G702":
                    res = true;
                    break;
                // Reggio di Calabria
                case "H224":
                    res = true;
                    break;
                // Roma
                case "H501":
                    res = true;
                    break;
                // Salerno
                case "H703":
                    res = true;
                    break;
                // Torino
                case "L219":
                    res = true;
                    break;
                // Trieste
                case "L424":
                    res = true;
                    break;
                // Venezia
                case "L736":
                    res = true;
                    break;
                // Verbania
                case "L746":
                    res = true;
                    break;
                // Verona
                case "L781":
                    res = true;
                    break;
            }

            return res;
        }

        public CassaEdileCollection GetCasseEdili()
        {
            return commonData.GetCasseEdili(true);
        }

        public CassaEdileCollection GetCasseEdiliSenzaMilano()
        {
            return commonData.GetCasseEdili(false);
        }

        public StatoCivileCollection GetStatiCivili()
        {
            return commonData.GetStatiCivili();
        }

        public DataTable GetProvince()
        {
            return commonData.GetProvince();
        }

        public DataTable GetComuniDellaProvincia(int provincia)
        {
            return commonData.GetComuniByProvincia(provincia);
        }

        public DataTable GetCAPDelComune(Int64 idComune)
        {
            return commonData.GetCapByComune(idComune);
        }

        public ImpresaCollection ImpreseConsulente(int idConsulente)
        {
            return commonData.GetImpreseConsulente(idConsulente);
        }

        public void ConsulenteSelezionaImpresa(Int32 idConsulente, Int32? idImpresa)
        {
            commonData.ConsulenteSelezionaImpresa(idConsulente, idImpresa);
        }

        public void ConsulenteImpresaSelezionata(Int32 idConsulente, out Int32 idImpresa, out String ragioneSociale)
        {
            commonData.ConsulenteImpresaSelezionata(idConsulente, out idImpresa, out ragioneSociale);
        }

        public void ConsulenteImpresaSelezionata(Int32 idConsulente, out Int32 idImpresa, out String ragioneSociale,
                                                 out String codiceFiscale)
        {
            commonData.ConsulenteImpresaSelezionata(idConsulente, out idImpresa, out ragioneSociale, out codiceFiscale);
        }

        public TipoContrattoCollection GetTipiContratto()
        {
            return commonData.GetTipiContratto();
        }

        public TipoQualificaCollection GetTipiQualifica()
        {
            return commonData.GetTipiQualifica();
        }

        public TipoQualificaCollection GetTipiQualifica(String tipoCategoria)
        {
            return commonData.GetTipiQualifica(tipoCategoria);
        }

        public TipoMansioneCollection GetTipiMansione()
        {
            return commonData.GetTipiMansione();
        }

        public TipoMansione GetTipoMansione(String codiceQualificaSiceNew)
        {
            return commonData.GetTipoMansione(codiceQualificaSiceNew);
        }

        public TipoCategoriaCollection GetTipiCategoria()
        {
            return commonData.GetTipiCategoria();
        }

        public TipoCategoriaCollection GetTipiCategoria(String tipoContratto)
        {
            return commonData.GetTipiCategoria(tipoContratto);
        }

        public TipoFineRapportoCollection GetTipiFineRapporto()
        {
            return commonData.GetTipiFineRapporto();
        }

        public TipoInizioRapportoCollection GetTipiInizioRapporto()
        {
            return commonData.GetTipiInizioRapporto();
        }

        public TipoCartaPrepagataCollection GetTipiCartaPrepagata()
        {
            return commonData.GetTipiCartaPrepagata();
        }

        public TipoLinguaCollection GetLingue()
        {
            return commonData.GetLingue();
        }

        // SOSTITUITA DA GetBollettiniStatistichePagati
        public BollettiniFrecciaStatistichePagati GetBollettiniFrecciaStatistichePagati()
        {
            return commonData.GetBollettiniFrecciaStatistichePagati();
        }

        public Impresa GetImpresa(string codiceFiscale)
        {
            return commonData.GetImpresa(codiceFiscale);
        }

        public BollettinoFrecciaStampabileCollection GetBollettiniFrecciaStampabili(
            BollettinoFrecciaStampabileFilter filtro)
        {
            return commonData.GetBollettiniFrecciaStampabili(filtro);
        }

        #region Lavoratori anagrafica comune

        public Boolean InsertLavoratore(LavoratoreAnagraficaComune lavoratore, DbTransaction transaction)
        {
            return commonData.InsertLavoratore(lavoratore, transaction);
        }

        public Boolean UpdateLavoratore(LavoratoreAnagraficaComune lavoratore, DbTransaction transaction)
        {
            return commonData.UpdateLavoratore(lavoratore, transaction);
        }

        #endregion

        #region Imprese anagrafica comune

        public Boolean InsertImpresa(ImpresaAnagraficaComune impresa, DbTransaction transaction)
        {
            return commonData.InsertImpresa(impresa, transaction);
        }

        #endregion

        #region Bollettino (Freccia o Mav)

        // SOSTITUITA DA GetDatiBollettiniFrecciaMav
        public BollettinoFrecciaCollection GetDatiBollettini(int idImpresa)
        {
            return commonData.GetDatiBollettiniFreccia(idImpresa);
        }

        // SOSTITUITA DA GetBollettiniStampati
        public BollettinoFrecciaStampatoCollection GetBollettiniFrecciaStampati(BollettinoFrecciaStampatoFilter filtro)
        {
            return commonData.GetBollettiniFrecciaStampati(filtro);
        }

        // SOSTITUITA DA GetUtentiBollettiniStampati
        public void GetUtentiBollettiniFrecciaStampati(out int numeroImprese, out int numeroConsulenti)
        {
            commonData.GetUtentiBollettiniFrecciaStampati(out numeroImprese, out numeroConsulenti);
        }

        public int GetBollettiniFrecciaStampabili(DateTime? dataInizio, DateTime? dataFine)
        {
            return commonData.GetBollettiniFrecciaStampabili(dataInizio, dataFine);
        }

        // SOSTITUITA DA RegistraRichiestaBollettino
        public Boolean RegistraRichiesta(BollettinoFreccia bollettino, int idUtente)
        {
            return commonData.RegistraRichiesta(bollettino, idUtente);
        }

        #region Nuovi - Getstione MAV
        public BollettinoCollection GetDatiBollettiniFrecciaMav(int idImpresa)
        {
            return commonData.GetDatiBollettini(idImpresa);
        }
        
        public BollettinoCollection GetDatiDenunce(int idImpresa)
        {
            return commonData.GetDatiDenunce(idImpresa);
        }

        public Bollettino GetDettaglioBollettino(Int32 idImpresa, Int32 anno, Int32 mese, Int32 sequenza)
        {
            return commonData.GetDettaglioBollettino(idImpresa, anno, mese, sequenza);
        }

        public Boolean RegistraRichiestaBollettino(Bollettino bollettino, int idUtente)
        {
            return commonData.RegistraRichiestaBollettino(bollettino, idUtente);
        }

        public Boolean RegistraRichiestaBollettinoDenuncia(Bollettino bollettino, int idUtente)
        {
            return commonData.RegistraRichiestaBollettinoDenuncia(bollettino, idUtente);
        }

        public BollettinoStampatoCollection GetBollettiniStampati(BollettinoStampatoFilter filtro)
        {
            return commonData.GetBollettiniStampati(filtro);
        }

        public void GetUtentiBollettiniStampati(out int numeroImprese, out int numeroConsulenti)
        {
            commonData.GetUtentiBollettiniStampati(out numeroImprese, out numeroConsulenti);
        }

        public BollettiniFrecciaStatistichePagati GetBollettiniStatistichePagati()
        {
            return commonData.GetBollettiniStatistichePagati();
        }

        public TipoCanalePagamentoBolletinoCollection GetTipiCanalePagamentoBollettino(bool conElementoVuoto)
        {
            return commonData.GetTipiCanalePagamentoBollettino(conElementoVuoto);
        }
        #endregion
        #endregion

        #region Partita IVA

        [Obsolete("Utilizzare TBridge.Cemi.Business.PartitaIvaManager")]
        public bool VerificaPartitaIVA(string partitaIVA)
        {
            return PartitaIvaManager.VerificaPartitaIva(partitaIVA);
        }

        #endregion

        #region Codice Fiscale

        [Obsolete("Utilizzare TBridge.Cemi.Business.CodiceFiscaleManager")]
        public bool VerificaPrimi12CaratteriCodiceFiscale(string codiceFiscale, string provincia, string comune,
                                                          string nome, string cognome, string sesso,
                                                          DateTime dataNascita)
        {
            return CodiceFiscaleManager.VerificaPrimi12CaratteriCodiceFiscale(provincia, comune, nome, cognome, sesso,
                                                                              dataNascita, codiceFiscale);
        }

        [Obsolete("Utilizzare TBridge.Cemi.Business.CodiceFiscaleManager")]
        public bool VerificaCodiceFiscale(string nome, string cognome, string sesso, DateTime dataNascita,
                                          string codiceIstat, string codiceFiscale)
        {
            return CodiceFiscaleManager.VerificaCodiceFiscale(nome, codiceFiscale, sesso, dataNascita, codiceIstat,
                                                              codiceFiscale);
        }

        [Obsolete("Utilizzare TBridge.Cemi.Business.CodiceFiscaleManager")]
        public bool VerificaPrimi11CaratteriCodiceFiscale(string nome, string cognome, string sesso,
                                                          DateTime dataNascita, string codiceFiscalePrimi11)
        {
            return CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita,
                                                                              codiceFiscalePrimi11);
        }

        [Obsolete("Utilizzare TBridge.Cemi.Business.CodiceFiscaleManager")]
        public string CalcolaCodiceFiscale(string nome, string cognome, string sesso, DateTime dataNascita,
                                           string codiceIstat)
        {
            return CodiceFiscaleManager.CalcolaCodiceFiscale(nome, cognome, sesso, dataNascita, codiceIstat);
        }

        [Obsolete("Utilizzare TBridge.Cemi.Business.CodiceFiscaleManager")]
        public Boolean VerificaCodiceControlloCodiceFiscale(String codiceFiscale)
        {
            return CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codiceFiscale);
        }

        [Obsolete("Utilizzare TBridge.Cemi.Business.CodiceFiscaleManager")]
        public string CalcolaPrimi11CaratteriCodiceFiscale(string nome, string cognome, string sesso,
                                                           DateTime dataNascita)
        {
            return CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita);
        }

        #endregion

        #region IBAN

        [Obsolete("Utilizzare TBridge.Cemi.Business.IbanManager")]
        public bool VerificaCodiceIBAN(string IBAN)
        {
            return IbanManager.VerificaCodiceIban(IBAN);
        }

        #endregion

        #region Controlli per le date

        public bool IsDataSmallDateTime(DateTime data)
        {
            bool res = false;

            if (LIMITEINFERIORESMALLDATETIME <= data && data < LIMITESUPERIORESMALLDATETIME)
                res = true;

            return res;
        }

        public bool IsCampoDataSmallDateTime(string textData)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(textData))
            {
                DateTime dtParsato;

                if (DateTime.TryParse(textData, out dtParsato))
                {
                    if (IsDataSmallDateTime(dtParsato))
                        res = true;
                }
            }
            else
                res = true;

            return res;
        }

        public bool IsCampoDataSmallDateTimeMinoreDiOggi(string textData)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(textData))
            {
                DateTime dtParsato;

                if (DateTime.TryParse(textData, out dtParsato))
                {
                    if (IsDataSmallDateTime(dtParsato) && dtParsato < DateTime.Now)
                        res = true;
                }
            }
            else
                res = true;

            return res;
        }

        #endregion

        public static string PulisciNumeroCellulare(string numero)
        {
            string numeroPulito = numero.Trim();
            if (numeroPulito.StartsWith("00393"))
                numeroPulito = String.Format("+{0}", numeroPulito.Substring(2));
            if (!numeroPulito.StartsWith("+393"))
                numeroPulito = String.Format("+39{0}", numeroPulito);
            return numeroPulito;
        }

        public static bool NumeroCellulareValido(string numero)
        {
            //numero = PulisciNumeroCellulare(numero);
            return Regex.IsMatch(numero, @"^([+]39)?3\d{8,15}$");
        }

        public FestivitaCollection GetFestivita(int anno)
        {
            return commonData.GetFestivita(anno);
        }

        public static Boolean Sviluppo
        {
            get
            {
                Boolean svil = false;

                if (ConfigurationManager.AppSettings["Sviluppo"] != null)
                {
                    if (!Boolean.TryParse(ConfigurationManager.AppSettings["Sviluppo"], out svil))
                    {
                        svil = false;
                    }
                }

                return svil;
            }
        }

        public List<Int32> GetAnniTrattamentoCIGO()
        {
            return commonData.GetAnniTrattamentoCIGO();
        }

        public Boolean IsImpresaIrregolareBNI(int idImpresa)
        {
            return commonData.IsImpresaIrregolareBNI(idImpresa);
        }

        public Int32 GetIdUtenteByIdImpresa(Int32 idImpresa)
        {
            Int32 idUtente = -1;

            using (SICEEntities context = new SICEEntities())
            {
                var impresa = (from imp in context.Imprese
                               where imp.Id == idImpresa
                               select imp).FirstOrDefault();

                if (impresa != null)
                {
                    idUtente = impresa.IdUtente.Value;
                }
            }

            return idUtente;
        }

        public Int32 GetIdUtenteByIdConsulente(Int32 idConsulente)
        {
            Int32 idUtente = -1;

            using (SICEEntities context = new SICEEntities())
            {
                var consulente = (from con in context.Consulenti
                                  where con.Id == idConsulente
                                  select con).FirstOrDefault();

                if (consulente != null)
                {
                    idUtente = consulente.IdUtente.Value;
                }
            }

            return idUtente;
        }
    }
}
