﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class ContribuzioneINPSCollection : List<ContribuzioneINPS>
    {
        /// <summary>
        ///   Ritorna la partitaIva dell'impresa se risulta contribuzione per il mese dato
        /// </summary>
        public string ContainsContribuzione(int mese, int anno)
        {
            foreach (ContribuzioneINPS cont in this)
            {
                if (cont.DataUltimaContribuzioneINPS.HasValue)
                {
                    if (cont.DataUltimaContribuzioneINPS.Value.Month == mese &&
                        cont.DataUltimaContribuzioneINPS.Value.Year == anno)
                        return cont.PartitaIVAImpresaINPS;
                }
            }
            return null;
        }

        /// <summary>
        ///   Ritorna true se c'è contribuzione per quel mese con quell'impresa
        /// </summary>
        public bool ContainsImpresaAnnoMese(string partitaIVA, int mese, int anno)
        {
            foreach (ContribuzioneINPS cont in this)
            {
                if (cont.PartitaIVAImpresaINPS != null)
                {
                    if (cont.PartitaIVAImpresaINPS == partitaIVA)
                    {
                        if (cont.DataUltimaContribuzioneINPS.HasValue)
                        {
                            if (cont.DataUltimaContribuzioneINPS.Value.Month == mese &&
                                cont.DataUltimaContribuzioneINPS.Value.Year == anno)
                                return true;
                        }
                    }
                }

                if (cont.CodiceFiscaleImpresaINPS != null)
                {
                    if (cont.CodiceFiscaleImpresaINPS == partitaIVA)
                    {
                        if (cont.DataUltimaContribuzioneINPS.HasValue)
                        {
                            if (cont.DataUltimaContribuzioneINPS.Value.Month == mese &&
                                cont.DataUltimaContribuzioneINPS.Value.Year == anno)
                                return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        ///   Ritorna la data massima di ultima contribuzione con quella impresa
        /// </summary>
        public DateTime? ContainsImpresa(string partitaIVA, int mese, int anno)
        {
            DateTime? ret = null;
            foreach (ContribuzioneINPS cont in this)
            {
                if (cont.PartitaIVAImpresaINPS != null)
                {
                    if (cont.PartitaIVAImpresaINPS == partitaIVA)
                    {
                        if (cont.DataUltimaContribuzioneINPS.HasValue)
                        {
                            if (ret.HasValue)
                            {
                                if (cont.DataUltimaContribuzioneINPS.Value > ret.Value)
                                    ret = cont.DataUltimaContribuzioneINPS.Value;
                            }
                            else
                            {
                                ret = new DateTime(cont.DataUltimaContribuzioneINPS.Value.Year,
                                                   cont.DataUltimaContribuzioneINPS.Value.Month,
                                                   cont.DataUltimaContribuzioneINPS.Value.Day);
                            }
                        }
                    }
                }

                if (cont.CodiceFiscaleImpresaINPS != null)
                {
                    if (cont.CodiceFiscaleImpresaINPS == partitaIVA)
                    {
                        if (cont.DataUltimaContribuzioneINPS.HasValue)
                        {
                            if (ret.HasValue)
                            {
                                if (cont.DataUltimaContribuzioneINPS.Value > ret.Value)
                                    ret = cont.DataUltimaContribuzioneINPS.Value;
                            }
                            else
                            {
                                ret = new DateTime(cont.DataUltimaContribuzioneINPS.Value.Year,
                                                   cont.DataUltimaContribuzioneINPS.Value.Month,
                                                   cont.DataUltimaContribuzioneINPS.Value.Day);
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}