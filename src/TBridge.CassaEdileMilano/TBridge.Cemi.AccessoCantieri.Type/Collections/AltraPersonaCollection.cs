﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class AltraPersonaCollection : List<AltraPersona>
    {
        //public bool ContainsCodiceFiscale(string codFisc)
        //{
        //    foreach (AltraPersona alp in this)
        //    {
        //        try
        //        {
        //            if (alp.CodiceFiscale == codFisc)
        //                return true;
        //        }
        //        catch
        //        {
        //        }
        //    }
        //    return false;
        //}

        public TipologiaAnomalia ControlloTimbrature(string codFisc, DateTime dataOra)
        {
            foreach (AltraPersona alp in this)
            {
                if (alp.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                    return TipologiaAnomalia.No;
            }
            return TipologiaAnomalia.NonInLista;
        }

        public AltraPersona GetAltraPersonaCodFisc(string codFisc)
        {
            AltraPersona altrRet = null;

            try
            {
                foreach (AltraPersona altr in this)
                {
                    if (altr.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                    {
                        altrRet = altr;
                        break;
                    }
                }
            }
            catch
            {
            }
            return altrRet;
        }
    }
}