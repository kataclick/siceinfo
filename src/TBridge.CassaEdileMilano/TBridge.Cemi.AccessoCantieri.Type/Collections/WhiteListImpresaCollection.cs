using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class WhiteListImpresaCollection : List<WhiteListImpresa>
    {
        //public void AggiungiLavoratoreAImpresa(Int32? idImpresa, Int32? idAttestatoImpresa, Lavoratore lavoratore)
        //{
        //    foreach (WhiteListImpresa domImp in this)
        //    {
        //        if (
        //            (idImpresa.HasValue && domImp.Impresa.TipoImpresa == TipologiaImpresa.SiceNew &&
        //             domImp.Impresa.IdImpresa == idImpresa)
        //            ||
        //            (idAttestatoImpresa.HasValue && domImp.Impresa.TipoImpresa == TipologiaImpresa.Nuova &&
        //             domImp.Impresa.IdImpresa == idImpresa)
        //            )
        //        {
        //            domImp.Lavoratori.Add(lavoratore);
        //            break;
        //        }
        //    }
        //}

        //public WhiteListImpresa TrovaLavoratoriDellImpresa(TipologiaImpresa tipoImpresa, Int32 idImpresa)
        //{
        //    foreach (WhiteListImpresa domImp in this)
        //    {
        //        if (domImp.Impresa.TipoImpresa == tipoImpresa
        //            && domImp.Impresa.IdImpresa.Value == idImpresa)
        //        {
        //            return domImp;
        //        }
        //    }

        //    return null;
        //}

        public WhiteListImpresa SelezionaDomandaImpresa(TipologiaImpresa tipoImpresa, int? idImpresa, Guid? idTemporaneo,
                                                        string codiceFiscale)
        {
            Impresa imp = new Impresa { TipoImpresa = tipoImpresa, IdImpresa = idImpresa, IdTemporaneo = idTemporaneo };

            foreach (WhiteListImpresa domImp in this)
            {
                if (domImp.Impresa.Equals(imp))
                {
                    if (!String.IsNullOrEmpty(codiceFiscale))
                    {
                        domImp.Impresa.CodiceFiscale = codiceFiscale;
                    }
                    return domImp;
                }
            }

            return null;
        }


        /// <summary>
        ///   Controlla la presenza della domanda nella lista; l'uguaglianza si basa su idImpresa + idDomanda
        /// </summary>
        /// <param name = "domandaImpresa"></param>
        /// <returns></returns>
        public new bool Contains(WhiteListImpresa domandaImpresa)
        {
            foreach (WhiteListImpresa domImp in this)
            {
                //controlliamo se la domanda � presente
                if (domImp.IdDomanda.HasValue && domandaImpresa.IdDomanda.HasValue)
                {
                    if ((domImp.Impresa.IdImpresa.HasValue && domandaImpresa.Impresa.IdImpresa.HasValue &&
                         domImp.Impresa.IdImpresa == domandaImpresa.Impresa.IdImpresa)
                        &&
                        (domImp.IdDomanda == domandaImpresa.IdDomanda))
                    {
                        return true;
                    }
                }
                else
                {
                    if (!domImp.IdDomanda.HasValue && !domandaImpresa.IdDomanda.HasValue)
                    {
                        if ((domImp.Impresa.IdImpresa.HasValue && domandaImpresa.Impresa.IdImpresa.HasValue &&
                             domImp.Impresa.IdImpresa == domandaImpresa.Impresa.IdImpresa))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public void CancellaTutto(WhiteListImpresa domandaImpresa, string ragioneSociale)
        {
            WhiteListImpresa domImp;

            for (int i = 0; i < Count; i++)
            {
                domImp = this[i];

                if (domImp.Impresa.RagioneSociale == ragioneSociale)

                    Remove(domImp);
            }
        }

        public void Cancella(WhiteListImpresa domandaImpresa)
        {
            WhiteListImpresa domImp;

            for (int i = 0; i < Count; i++)
            {
                domImp = this[i];

                if ((domImp.Impresa.IdImpresa.HasValue && domandaImpresa.Impresa.IdImpresa.HasValue &&
                     domImp.Impresa.IdImpresa == domandaImpresa.Impresa.IdImpresa)
                    &&
                    (domImp.IdDomanda == domandaImpresa.IdDomanda))

                    Remove(domImp);
            }
        }

        private static DateTime OnlyDate(DateTime data)
        {
            return new DateTime(data.Year, data.Month, data.Day);
        }


        private int LavoratoreImpreseMultiple(string codFisc)
        {
            int ret = 0;
            foreach (WhiteListImpresa domImp in this)
            {
                foreach (Lavoratore lav in domImp.Lavoratori)
                {
                    if (lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                        ret++;
                }
            }

            return ret;
        }

        public TipologiaAnomalia ControlloTimbrature(string codFisc, DateTime dataOra)
        {
            TipologiaAnomalia tipoAnomaliaImpreseMultiple = TipologiaAnomalia.NonInLista;

            foreach (WhiteListImpresa domImp in this)
            {
                foreach (Lavoratore lav in domImp.Lavoratori)
                {
                    if (LavoratoreImpreseMultiple(codFisc) <= 1)
                    {
                        if ((lav.DataInizioAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataInizioAttivita.Value.Date) > OnlyDate(dataOra))))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        if ((lav.DataFineAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataFineAttivita.Value.Date) < OnlyDate(dataOra.Date))))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        if ((lav.DataInizioAttivita.HasValue) && (lav.DataFineAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataInizioAttivita.Value) <= OnlyDate(dataOra)) &&
                                 (OnlyDate(lav.DataFineAttivita.Value) >= OnlyDate(dataOra))))
                                return TipologiaAnomalia.No;
                        }
                        else
                        {
                            if (lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                                return TipologiaAnomalia.No;
                        }

                        if ((lav.DataInizioAttivita.HasValue) && (lav.DataFineAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataInizioAttivita.Value.Date) > OnlyDate(dataOra)) ||
                                 (OnlyDate(lav.DataFineAttivita.Value.Date) < OnlyDate(dataOra.Date))))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }
                    }
                    else
                    {
                        if ((lav.DataInizioAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataInizioAttivita.Value.Date) > OnlyDate(dataOra))))
                                if (tipoAnomaliaImpreseMultiple != TipologiaAnomalia.No)
                                    tipoAnomaliaImpreseMultiple = TipologiaAnomalia.PeriodoNonValido;
                        }

                        if ((lav.DataFineAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataFineAttivita.Value.Date) < OnlyDate(dataOra.Date))))
                                if (tipoAnomaliaImpreseMultiple != TipologiaAnomalia.No)
                                    tipoAnomaliaImpreseMultiple = TipologiaAnomalia.PeriodoNonValido;
                        }

                        if ((lav.DataInizioAttivita.HasValue) && (lav.DataFineAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataInizioAttivita.Value) <= OnlyDate(dataOra)) &&
                                 (OnlyDate(lav.DataFineAttivita.Value) >= OnlyDate(dataOra))))
                                tipoAnomaliaImpreseMultiple = TipologiaAnomalia.No;
                        }
                        else
                        {
                            if (lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                                tipoAnomaliaImpreseMultiple = TipologiaAnomalia.No;
                        }

                        if ((lav.DataInizioAttivita.HasValue) && (lav.DataFineAttivita.HasValue))
                        {
                            if ((lav.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim()) &&
                                ((OnlyDate(lav.DataInizioAttivita.Value.Date) > OnlyDate(dataOra)) ||
                                 (OnlyDate(lav.DataFineAttivita.Value.Date) < OnlyDate(dataOra.Date))))
                                if (tipoAnomaliaImpreseMultiple != TipologiaAnomalia.No)
                                    tipoAnomaliaImpreseMultiple = TipologiaAnomalia.PeriodoNonValido;
                        }
                    }
                }
            }
            if (tipoAnomaliaImpreseMultiple != TipologiaAnomalia.NonInLista)
                return tipoAnomaliaImpreseMultiple;
            return TipologiaAnomalia.NonInLista;
        }

        public Lavoratore GetLavoratoreCodFisc(String codFisc)
        {
            Lavoratore lavRet = null;

            foreach (WhiteListImpresa domImp in this)
            {
                foreach (Lavoratore lav in domImp.Lavoratori)
                {
                    if (lav.CodiceFiscale == codFisc)
                    {
                        lavRet = lav;
                        break;
                    }
                }
            }

            return lavRet;
        }

        public Lavoratore GetLavoratoreCodFisc(String codFisc, TipologiaImpresa tipoImpresa, Int32? idImpresa)
        {
            Lavoratore lavRet = null;

            foreach (WhiteListImpresa domImp in this)
            {
                if (idImpresa.HasValue
                    && domImp.Impresa.TipoImpresa == tipoImpresa
                    && domImp.Impresa.IdImpresa == idImpresa)
                {
                    foreach (Lavoratore lav in domImp.Lavoratori)
                    {
                        if (lav.CodiceFiscale == codFisc)
                        {
                            lavRet = lav;
                            break;
                        }
                    }
                }
            }

            return lavRet;
        }

        public Boolean EffettuaControlli(String codiceFiscaleLavoratore, String partitaIvaImpresa)
        {
            Boolean res = true;

            foreach (WhiteListImpresa wli in this)
            {
                if (wli.Impresa != null && wli.Impresa.PartitaIva == partitaIvaImpresa)
                {
                    foreach (Lavoratore lav in wli.Lavoratori)
                    {
                        if (lav.CodiceFiscale.ToUpper() == codiceFiscaleLavoratore.ToUpper() && !lav.EffettuaControlli)
                        {
                            res = false;
                            break;
                        }
                    }

                    if (!res)
                    {
                        break;
                    }
                }
            }

            return res;
        }
    }
}