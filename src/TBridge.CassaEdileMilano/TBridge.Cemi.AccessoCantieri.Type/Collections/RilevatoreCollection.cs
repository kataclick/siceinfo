﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class RilevatoreCollection : List<Rilevatore>
    {
        public RilevatoreCollection Select(int idWhiteList)
        {
            RilevatoreCollection ret = new RilevatoreCollection();
            foreach (Rilevatore r in this)
            {
                if (r.IdWhiteListRilevatore == idWhiteList)
                {
                    ret.Add(r);
                }
            }
            return ret;
        }
    }
}