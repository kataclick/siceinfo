﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class TimbraturaFilter
    {
        public DateTime? DataInizio
        {
            get;
            set;
        }

        public DateTime? DataFine
        {
            get;
            set;
        }

        public TipologiaRuoloTimbratura? TipoUtente
        {
            get;
            set;
        }

        public Int32? IdImpresa
        {
            get;
            set;
        }

        public Int32? IdAccessoCantieriWhiteList
        {
            get;
            set;
        }

        public String CodiceFiscale
        {
            get;
            set;
        }

        public Int32? IdCommittente
        {
            get;
            set;
        }
    }
}