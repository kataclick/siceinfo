﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class ControlloIdentitaFilter
    {
        public Int32? Mese { get; set; }

        public Int32? Anno { get; set; }

        public Int32? IdImpresa { get; set; }

        public Int32? IdCantiere { get; set; }

        public String CodiceFiscale { get; set; }

        public TipologiaAnomaliaControlli? TipologiaAnomaliaControlli { get; set; }

        public TipologiaRuoloTimbratura? TipologiaRuoloTimbratura { get; set; }

        public String PartitaIVA { get; set; }
    }
}