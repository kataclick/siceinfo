﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class PresenzeFilter
    {
        public Int32 IdCantiere { get; set; }

        public DateTime Dal { get; set; }

        public DateTime Al { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIvaImpresa { get; set; }
    }
}
