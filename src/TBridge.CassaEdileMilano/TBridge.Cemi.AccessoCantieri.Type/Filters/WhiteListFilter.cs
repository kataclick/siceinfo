﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class WhiteListFilter
    {
        public String Indirizzo
        {
            get;
            set;
        }

        public String Comune
        {
            get;
            set;
        }

        //public DateTime? Mese { get; set; }

        public Int32? IdUtente
        {
            get;
            set;
        }

        //public String IvaCodFisc { get; set; }

        //public String RagSocDenominazione { get; set; }

        public Int32? IdCommittente
        {
            get;
            set;
        }
    }
}