﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    public class LavoratoreFilter
    {
        public Int32 IdCantiere { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CodiceFiscale { get; set; }
    }
}
