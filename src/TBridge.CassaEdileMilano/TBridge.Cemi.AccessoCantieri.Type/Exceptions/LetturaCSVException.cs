﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Exceptions
{
    public class LetturaCSVException : Exception
    {
        public Int32 Line { get; set; }
        public Exception InnerException { get; set; }

        public LetturaCSVException(Int32 line, Exception innerException)
            : base()
        {
            this.Line = line;
            this.InnerException = innerException;
        }
    }
}
