﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Impresa
    {
        [DataMember]
        public String RagioneSociale { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String PartitaIVA { get; set; }

        [DataMember]
        public String Indirizzo { get; set; }

        [DataMember]
        public String Comune { get; set; }

        [DataMember]
        public String Provincia { get; set; }

        [DataMember]
        public String CAP { get; set; }

        [DataMember]
        public String ContrattoApplicato { get; set; }

        [DataMember]
        public List<Referente> Referenti { get; set; }

        [DataMember]
        public String CodiceFiscaleAppaltatrice { get; set; }

        [DataMember]
        public List<Lavoratore> Lavoratori { get; set; }
    }
}
