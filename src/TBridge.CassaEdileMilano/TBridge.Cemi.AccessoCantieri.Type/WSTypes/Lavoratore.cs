﻿using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Lavoratore
    {
        [DataMember]
        public String Cognome { get; set; }

        [DataMember]
        public String Nome { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public DateTime? DataNascita { get; set; }

        [DataMember]
        public String ContrattoApplicato { get; set; }

        [DataMember]
        public DateTime? Dal { get; set; }

        [DataMember]
        public DateTime? Al { get; set; }

        [DataMember]
        public String CodiceFiscaleImpresa { get; set; }
    }
}
