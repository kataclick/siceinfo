﻿using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Accesso
    {
        [DataMember]
        public DateTime DataOra { get; set; }

        [DataMember]
        public String CodiceFiscaleLavoratore { get; set; }

        [DataMember]
        public String CodiceFiscaleImpresa { get; set; }

        [DataMember]
        public Boolean Ingresso { get; set; }
    }
}
