﻿using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Referente
    {
        [DataMember]
        public String Cognome { get; set; }

        [DataMember]
        public String Nome { get; set; }

        [DataMember]
        public DateTime DataNascita { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String Telefono { get; set; }

        [DataMember]
        public String Cellulare { get; set; }

        [DataMember]
        public String EMail { get; set; }

        [DataMember]
        public String PEC { get; set; }
    }
}
