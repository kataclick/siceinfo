using System;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Delegates
{
    //public delegate void CantieriSelectedEventHandler(Cantiere cantiere);

    public delegate void ImpreseSelectedEventHandler(Impresa impresa);

    public delegate void LavoratoriSelectedEventHandler(Lavoratore lavoratore);

    public delegate void WhiteListSelectedEventHandler(Int32 idWhiteList);

    public delegate void WhiteListCompletaSelectedEventHandler(WhiteList whiteList);

    public delegate void CambiaCantiereSelectedEventHandler();

    public delegate void LavoratoriDeletedEventHandler(String codiceFiscale);
}