using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ControlloIdentita
    {
        public string CodiceFiscale { get; set; }

        public int Mese { get; set; }

        public int Anno { get; set; }

        public Int32? IdCantiere { get; set; }

        //CEMI

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public Boolean ControlliEffettuati { get; set; }

        public Boolean ControlloDenunciaSuperato { get; set; }

        public Boolean ControlloLavoratoreDenunciaSuperato { get; set; }

        public Boolean ControlloOreDenunciaSuperato { get; set; }

        public Boolean ControlloDebitiSuperato { get; set; }

        public Boolean ControlloDebitiEffettuato { get; set; }

        //CEXChange
        public DateTime? DataUltimaDenuncia { get; set; }

        public String CassaEdileUltimaDenuncia { get; set; }

        //CEXChange
        public DenunceOreCexChangeCollection DenunceOreCexChange { set; get; }

        //INPS
        //public ContribuzioneINPSCollection ContribuzioniINPS { get; set; }
        public ContribuzioneInps2Collection ContribuzioniInps { get; set; }

        public string PartitaIvaImpresa { get; set; }
    }
}