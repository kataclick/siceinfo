using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Rilevatore
    {
        public int IdRilevatore { get; set; }

        public string Codice { get; set; }

        public Decimal? Latitudine { get; set; }

        public Decimal? Longitudine { get; set; }

        public Int32? IdWhiteListRilevatore { get; set; }

        public Boolean InvioWhitelist { get; set; }
    }
}