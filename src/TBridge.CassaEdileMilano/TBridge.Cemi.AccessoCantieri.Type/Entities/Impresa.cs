using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Impresa : IComparable
    {
        //private const string SEPARATORE_CAMPO_COMPOSTO = "|";

        public Impresa()
        {
        }

        //tipoImpresa = 0 se presa da SiceNEW
        public Impresa(int? idImpresa, string ragioneSociale, string indirizzo,
                       string provincia, string comune, string cap, string partitaIva, string codiceFiscale,
                       TipologiaImpresa tipoImpresa, Guid? idTemporaneo, string telefonoSedeLegale,
                       string telefonoSedeAmministrativa, string emailSedeLegale, string emailSedeAmministrativa)
        {
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            Indirizzo = indirizzo;
            Provincia = provincia;
            Comune = comune;
            Cap = cap;
            PartitaIva = partitaIva;
            CodiceFiscale = codiceFiscale;
            IdTemporaneo = idTemporaneo;
            TipoImpresa = tipoImpresa;
            AmmiTelefono = telefonoSedeAmministrativa;
            AmmiEmail = emailSedeAmministrativa;
            LegaleTelefono = telefonoSedeLegale;
            LegaleEmail = emailSedeLegale;
        }

        public string CodiceFiscale { get; set; }

        //private string AmmiIndirizzo { get; set; }

        //private string AmmiProvincia { get; set; }

        //private string AmmiComune { get; set; }

        //private string AmmiCap { get; set; }

        public int? IdImpresa { get; set; }

        public Guid? IdTemporaneo { get; set; }

        public string RagioneSociale { get; set; }

        public DateTime? DataIscrizione { get; set; }

        public String CodiceERagioneSociale
        {
            get
            {
                if (TipoImpresa == TipologiaImpresa.Nuova)
                {
                    return RagioneSociale;
                }
                return String.Format("{0} - {1}", IdImpresa, RagioneSociale);
            }
        }

        public string Indirizzo { get; set; }

        public string Provincia { get; set; }

        public string Comune { get; set; }

        public string Cap { get; set; }

        public string PartitaIva { get; set; }

        //public string AmmiIndirizzoCompleto
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(AmmiProvincia))
        //            return String.Format("{0} {1} ({2}) {3}", AmmiIndirizzo, AmmiComune, AmmiProvincia, AmmiCap);
        //        return String.Format("{0} {1} {2}", AmmiIndirizzo, AmmiComune, AmmiCap);
        //    }
        //}

        public string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(Provincia))
                    return String.Format("{0} {1} ({2}) {3}", Indirizzo, Comune, Provincia, Cap);
                return String.Format("{0} {1} {2}", Indirizzo, Comune, Cap);
            }
        }

        public string NomeCompleto
        {
            get
            {
                return
                    RagioneSociale + Environment.NewLine + Indirizzo + Environment.NewLine + Comune + " - " + Provincia +
                    " " + Cap;
            }
        }

        /// <summary>
        ///   Ritorna TipoImpresa + SEPARATORE + IdImpresa; per conoscere il separatore accedere alla proprietÓ SeparatoreCampoComposto
        /// </summary>
        public string IdImpresaComposto
        {
            //get { return ((int) TipoImpresa) + SEPARATORE_CAMPO_COMPOSTO + IdImpresa; }
            get { return String.Format("{0}|{1}|{2}", (Int32) TipoImpresa, IdImpresa, IdTemporaneo); }
        }

        //private static string SeparatoreCampoComposto
        //{
        //    get { return SEPARATORE_CAMPO_COMPOSTO; }
        //}

        public TipologiaContratto TipologiaContratto { get; set; }

        public TipologiaImpresa TipoImpresa { get; set; }

        public string LegaleTelefono { get; set; }

        public string AmmiTelefono { get; set; }

        public string LegaleEmail { get; private set; }

        public string AmmiEmail { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string NumeroCameraCommercio { get; set; }

        public DateTime? DataNascita { get; set; }

        public bool LavoratoreAutonomo { get; set; }

        public string LuogoNascita { get; set; }

        public string PaeseNascita { get; set; }

        public string ProvinciaNascita { get; set; }

        //public DateTime? DataAssunzione { get; set; }

        public DateTime? DataStampaBadge { get; set; }

        public string Committente { get; set; }

        //public string AutorizzazioneAlSubappalto { get; set; }

        public byte[] Foto { get; set; }

        //private Int32? IdImpresaTrovata { get; set; }

        public string TipoAttivita { get; set; }

        //private Boolean ImpresaAssociata
        //{
        //    get
        //    {
        //        if (TipoImpresa == TipologiaImpresa.SiceNew
        //            ||
        //            IdImpresaTrovata.HasValue
        //            )
        //        {
        //            return true;
        //        }

        //        return false;
        //    }
        //}

        public string Stato { get; set; }
        public DateTime? DataSospensione { get; set; }
        public DateTime? DataCessazione { get; set; }

        #region Controlli invertiti

        //public Boolean ImpresaAssociataNegativo
        //{
        //    get { return !ImpresaAssociata; }
        //}

        #endregion

        //public static void SplitIdImpresaComposto(string idImpresaComposto, out int idImpresa,
        //                                          out TipologiaImpresa tipoImpresa)
        //{
        //    string[] impresaIds = idImpresaComposto.Split(SeparatoreCampoComposto.ToCharArray());
        //    idImpresa = int.Parse(impresaIds[1]);
        //    tipoImpresa = (TipologiaImpresa) Int32.Parse(impresaIds[0]);
        //}

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Impresa))
                return base.Equals(obj);
            Impresa impresa = (Impresa) obj;

            if (impresa.TipoImpresa == TipoImpresa
                && impresa.IdImpresa == IdImpresa
                )
            {
                return true;
            }
            if (impresa.TipoImpresa == TipologiaImpresa.Nuova
                && TipoImpresa == TipologiaImpresa.Nuova
                && !impresa.IdImpresa.HasValue
                && !IdImpresa.HasValue
                && IdTemporaneo == impresa.IdTemporaneo)
            {
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            if (TipoImpresa == TipologiaImpresa.SiceNew)
            {
                return String.Format("{0} {1}", IdImpresa, RagioneSociale);
            }
            return RagioneSociale;
        }

        public String ContrattoApplicato { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null
                && obj.GetType() == typeof(Impresa))
            {
                return this.RagioneSociale.CompareTo(((Impresa) obj).RagioneSociale);
            }

            return 0;
        }
    }
}