using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Timbratura
    {
        public int IdTimbratura { get; set; }

        public string CodiceRilevatore { get; set; }

        public string RagioneSociale { get; set; }

        public Decimal? Latitudine { get; set; }

        public Decimal? Longitudine { get; set; }

        public string CodiceFiscale { get; set; }

        public string CodiceFiscaleImpresa { get; set; }

        public string PartitaIvaImpresa { get; set; }

        public Boolean IngressoUscita { get; set; }

        public Boolean Gestito { get; set; }

        public TipologiaAnomalia Anomalia { get; set; }

        public DateTime DataOra { get; set; }

        public Int32? IdCantiere { get; set; }

        public Boolean ControlliEffettuati { get; set; }

        public Boolean ControlloDenunciaSuperato { get; set; }

        public Boolean ControlloLavoratoreDenunciaSuperato { get; set; }

        public Boolean ControlloOreDenunciaSuperato { get; set; }

        public Int32? IdLavoratore { get; set; }

        public Int32? IdImpresa { get; set; }

        public Boolean ControlloDebitiSuperato { get; set; }

        public Boolean ControlloDebitiEffettuato { get; set; }

        public TipologiaFornitore Fornitore { get; set; }
    }
}