using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Statistica
    {
        public ImpresaCollection ImpreseCeAppaltatrici { get; set; }

        public ImpresaCollection ImpreseCeSubappaltate { get; set; }

        public ImpresaCollection ImpreseNuoveAppaltatrici { get; set; }

        public ImpresaCollection ImpreseNuoveSubappaltate { get; set; }

        public ImpresaCollection ImpreseArtigianeAppaltatrici { get; set; }

        public ImpresaCollection ImpreseArtigianeSubappaltate { get; set; }

        public LavoratoreCollection LavoratoriCe { get; set; }

        public LavoratoreCollection LavoratoriNuovi { get; set; }

        public TimbraturaCollection Timbrature { get; set; }

        public TimbraturaCollection TimbratureFiltrate { get; set; }

        public Int32 TotaleImpreseCeAppaltatrici { get; set; }

        public Int32 TotaleImpreseCeSubappaltate { get; set; }

        public Int32 TotaleImpreseNuoveAppaltatrici { get; set; }

        public Int32 TotaleImpreseNuoveSubappaltate { get; set; }

        public Int32 TotaleImpreseArtigianeSubappaltate { get; set; }

        public Int32 TotaleImpreseArtigianeAppaltatrici { get; set; }

        public Int32 TotaleLavoratoriCe { get; set; }

        public Int32 TotaleLavoratoriNuovi { get; set; }

        public Int32 TotaleTimbrature { get; set; }

        public Int32 TotaleTimbratureFiltrate { get; set; }
    }
}