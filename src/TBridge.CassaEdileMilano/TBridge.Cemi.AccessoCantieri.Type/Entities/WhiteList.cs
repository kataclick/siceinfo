﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class WhiteList
    {
        public WhiteList()
        {
            Subappalti = new SubappaltoCollection();
        }

        public Int32? IdWhiteList
        {
            get;
            set;
        }

        public String Indirizzo
        {
            get;
            set;
        }

        public String Civico
        {
            get;
            set;
        }

        public String Comune
        {
            get;
            set;
        }

        public String Provincia
        {
            get;
            set;
        }

        public String Cap
        {
            get;
            set;
        }

        public String IndirizzoCompletoCantiere
        {
            get
            {
                if (!String.IsNullOrEmpty(Provincia))
                {
                    return String.Format("{0} {1} {2} ({3})", Indirizzo, Civico, Comune, Provincia);
                }
                return String.Format("{0} {1} {2}", Indirizzo, Civico, Comune);
            }
        }

        public String InfoAggiuntiva
        {
            get;
            set;
        }

        public SubappaltoCollection Subappalti
        {
            get;
            set;
        }

        public WhiteListImpresaCollection Lavoratori
        {
            get;
            set;
        }

        public Decimal? Latitudine
        {
            get;
            set;
        }

        public Decimal? Longitudine
        {
            get;
            set;
        }

        public string Descrizione
        {
            get;
            set;
        }

        public DateTime? DataInizio
        {
            get;
            set;
        }

        public DateTime? DataFine
        {
            get;
            set;
        }

        public Int32 IdUtente
        {
            get;
            set;
        }

        public String LoginUtente
        {
            get;
            set;
        }

        public Guid GuidId
        {
            get;
            set;
        }

        public ImpresaCollection ListaImprese
        {
            get;
            set;
        }

        public AltraPersonaCollection ListaAltrePersone
        {
            get;
            set;
        }

        public ReferenteCollection ListaReferenti
        {
            get;
            set;
        }

        public RilevatoreCantiereCollection Rilevatori
        {
            get;
            set;
        }

        public Committente Committente
        {
            get;
            set;
        }

        public string AutorizzazioneAlSubappalto
        {
            get;
            set;
        }

        public TBridge.Cemi.GestioneUtenti.Type.Entities.Committente CommittenteUtente
        {
            get;
            set;
        }
    }
}