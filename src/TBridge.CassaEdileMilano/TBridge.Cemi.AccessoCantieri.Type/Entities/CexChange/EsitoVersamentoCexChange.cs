﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class EsitoVersamentoCexChange
    {
        public int Id { set; get; }
        public String CodiceCexChange { set; get; }
        public String Descrizione { set; get; }
    }
}
