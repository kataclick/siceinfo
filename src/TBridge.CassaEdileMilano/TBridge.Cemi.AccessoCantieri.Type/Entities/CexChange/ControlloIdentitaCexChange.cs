﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.AccessoCantieri.Type.Collections.CexChange;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class ControlloIdentitaCexChange
    {
        public string CodiceFiscaleLavoratore { get; set; }

        public string PartitaIvaImpresa { get; set; }

        public int Mese { get; set; }

        public int Anno { get; set; }

        public Int32? IdCantiere { get; set; }

        public ControlloDenunciaOreCexChangeCollection ControlliDenunceCexChange { set; get; }

        //dati whitelist
        public Lavoratore LavoratoreTimbratura { get; set; }

        //dati whitelist
        public Impresa ImpresaTimbratura { get; set; }

        //INPS (basato su Milano)
        public ContribuzioneInps2Collection ContribuzioniInps { get; set; }
        
    }
}
