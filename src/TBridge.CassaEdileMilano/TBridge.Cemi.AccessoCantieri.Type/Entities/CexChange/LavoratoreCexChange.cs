﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class LavoratoreCexChange
    {
        public String CassaEdileCodice { set; get; }

        public String CassaEdileDescrizione { set; get; }

        public String CodiceCELavoratore { set; get; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public string LuogoNascita { get; set; }

        public string NazioneNascita { get; set; }

        public string ProvinciaNascita { get; set; }

        public String Sesso { get; set; }

        public int? UltimaDenunciaAnno { set; get; }

        public int? UltimaDenunciaMese { set; get; }

        public override string ToString()
        {
            return String.Format("{0} {1}", Nome, Cognome);
        }
    }
}
