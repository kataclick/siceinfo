﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class DenunciaOreCexChange
    {      
        public String CassaEdileCodice { set; get; }
        
        public String CassaEdileDescrizione { set; get; }

        public String CodiceCELavoratore { set; get; }

        public String CodiceCEImpresa { set; get; }
        
        public Int32 PeriodoMese { set; get; }
        
        public Int32 PeriodoAnno { set; get; }

        #region ore
        public Decimal OreOrdinarie { set; get; }

        public decimal ImponibileGNF { set; get; }
        #endregion

        public override string ToString()
        {
            return CassaEdileDescrizione;
        }
    }
}
