﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class ControlloDenunciaOreCexChange
    {
        public String CassaEdileCodice { set; get; }
       
        public String CassaEdileDescrizione { set; get; }

        public int PeriodoAnno { set; get; }

        public int PeriodoMese { set; get; }

        public Boolean ImpresaPresenzaDenunceInPeriodo { set; get; }

        public LavoratoreCexChange Lavoratore { set; get; }

        public ImpresaCexChange Impresa { set; get; }

        public DenunciaOreCexChange DeunciaOre { set; get; }

        public String Periodo
        {
            get { return String.Format("{0}/{1}", PeriodoMese, PeriodoAnno); }
        }

        public Boolean ImpresaIscrittaInPeriodo
        {
            get
            {
                if (Impresa != null && Impresa.DataIscrizione.HasValue)
                {

                    return ( new DateTime(Impresa.DataIscrizione.Value.Year, Impresa.DataIscrizione.Value.Month, 1) <
                                    new DateTime(PeriodoAnno, PeriodoMese, 1));
                }

                return false;
            }
        }

        public EsitoVersamentoCexChange ImpresaEsitoVersamentoInPeriodo { set; get; }

    }
}
