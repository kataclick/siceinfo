﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class ImpresaCexChange
    {
        public String CassaEdileCodice { set; get; }

        public String CassaEdileDescrizione { set; get; }

        public String CodiceCEImpresa { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }

        public string RagioneSociale { get; set; }

        public String Natura { set; get; }

        public String Stato { set; get;  }

        public DateTime? DataCessazione { set; get; }

        public DateTime? DataDecorrenza { set; get; }

        public DateTime? DataIscrizione { set; get; }

        public int? UltimaDenunciaAnno { set; get; }

        public int? UltimaDenunciaMese { set; get; }

        public String PosizioneBNI { set; get; }

        public Boolean? PosizioneBNIRegolre
        {
            get
            {
                return String.IsNullOrEmpty(PosizioneBNI) ? (Boolean?)null : (PosizioneBNI == "R");
            }
        }

        public override string ToString()
        {
            return RagioneSociale;
        }
    }
}
