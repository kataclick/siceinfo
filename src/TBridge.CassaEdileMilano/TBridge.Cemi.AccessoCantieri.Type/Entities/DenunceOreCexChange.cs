﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    public class DenunceOreCexChange
    {
        public String CodiceFiscaleLavoratore { set; get; }
        public String CassaEdileCodice { set; get; }
        public String CassaEdileDescrizione { set; get; }
        public Decimal OreOrdinarie { set; get; }
        public Int32 PeriodoMese { set; get; }
        public Int32 PeriodoAnno { set; get; }

        public override string ToString()
        {
            return CassaEdileDescrizione;
        }
    }
}
