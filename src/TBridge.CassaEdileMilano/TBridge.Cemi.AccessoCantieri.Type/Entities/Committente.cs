﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Committente
    {
        public Int32? IdCommittente { get; set; }

        public String RagioneSociale { get; set; }

        public TipologiaCommittente Tipologia { get; set; }

        public String Indirizzo { get; set; }

        public String Civico { get; set; }

        public Int32 Provincia { get; set; }

        public String ProvinciaDescrizione { get; set; }

        public Int64 Comune { get; set; }

        public String ComuneDescrizione { get; set; }

        public String Cap { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIva { get; set; }

        //public String Cognome { get; set; }

        //public String Nome { get; set; }
    }
}