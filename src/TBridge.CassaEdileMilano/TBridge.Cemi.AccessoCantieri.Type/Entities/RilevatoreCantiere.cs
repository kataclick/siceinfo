using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class RilevatoreCantiere
    {
        public Int32 IdRilevatore { get; set; }

        public String CodiceRilevatore { get; set; }

        public String RagioneSociale { get; set; }

        public Decimal? Latitudine { get; set; }

        public Decimal? Longitudine { get; set; }

        public Int32? IdCantiere { get; set; }

        public DateTime? DataInizio { get; set; }

        public DateTime? DataFine { get; set; }

        public String Indirizzo { get; set; }

        public String Civico { get; set; }

        public String Comune { get; set; }

        public String Provincia { get; set; }

        public String NomeReferente { get; set; }

        public String CognomeReferente { get; set; }

        public Boolean InvioWhitelist { get; set; }

        public Boolean Terminato { get; set; }
    }
}