using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ElementoInps
    {
        public String CodiceFiscaleLavoratoreInps { get; set; }

        public String PartitaIvaImpresaCemiInps { get; set; }
    }
}