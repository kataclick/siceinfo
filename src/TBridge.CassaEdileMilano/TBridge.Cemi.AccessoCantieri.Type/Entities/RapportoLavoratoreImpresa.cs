using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class RapportoLavoratoreImpresa
    {
        public int IdImpresa { get; set; }

        public int IdLavoratore { get; set; }

        public DateTime DataDenuncia { get; set; }

        public string Tipo { get; set; }
    }
}