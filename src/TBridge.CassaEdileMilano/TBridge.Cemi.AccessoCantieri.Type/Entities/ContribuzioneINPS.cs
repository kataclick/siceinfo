using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ContribuzioneINPS
    {
        public String RagioneSocialeImpresaINPS { get; set; }

        public String CodiceFiscaleImpresaINPS { get; set; }

        public String PartitaIVAImpresaINPS { get; set; }

        public DateTime? DataUltimaContribuzioneINPS { get; set; }
    }
}