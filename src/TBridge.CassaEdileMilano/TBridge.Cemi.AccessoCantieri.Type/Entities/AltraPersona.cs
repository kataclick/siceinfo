using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class AltraPersona
    {
        public int? IdAltraPersona { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public Boolean ModalitaContatto { get; set; }

        public Int32? IdWhiteListAltraPersona { get; set; }

        public TipologiaRuolo TipoRuolo { get; set; }
    }
}