﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    public class Presenze
    {
        public Int32 Anno { get; set; }

        public Int32 Mese { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CodiceFiscale { get; set; }

        public DateTime? DataNascita { get; set; }

        public DateTime? DataPrimoAccesso { get; set; }

        public DateTime? DataUltimoAccesso { get; set; }

        public Int32 NumeroPresenze { get; set; }

        public String ImpresaRagioneSociale { get; set; }

        public String ImpresaPartitaIva { get; set; }
    }
}
