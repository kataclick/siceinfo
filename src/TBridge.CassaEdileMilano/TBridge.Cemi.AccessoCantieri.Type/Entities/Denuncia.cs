using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Denuncia
    {
        public int AnnoDenuncia { get; set; }

        public int MeseDenuncia { get; set; }

        public int IdImpresa { get; set; }

        public String StatoDenuncia { get; set; }

        public DateTime DataDenuncia { get; set; }
    }
}