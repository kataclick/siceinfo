using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ContribuzioneINPS2
    {
        public String PartitaIVAImpresa { get; set; }

        public Int32 Mese { get; set; }

        public Int32 Anno { get; set; }

        public Boolean? Superato { get; set; }
    }
}