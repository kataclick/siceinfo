namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaAnomaliaDenuncia
    {
        No = 0,
        NonPresentata,
        LavoratoreNonPresente,
        OreNonCoerenti
    }
}