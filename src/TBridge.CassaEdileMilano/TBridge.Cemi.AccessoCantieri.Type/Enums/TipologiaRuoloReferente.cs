namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaRuoloReferente
    {
        Impresa = 0,
        Cantiere,
        Committente,
        Altro
    }
}