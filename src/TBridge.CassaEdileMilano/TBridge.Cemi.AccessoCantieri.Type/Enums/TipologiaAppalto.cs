namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaAppalto
    {
        NonDefinito = 0,
        Pubblico,
        Privato
    }
}