namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaAffiliazione
    {
        Impresa = 0,
        Comune,
        Provincia,
        Altro
    }
}