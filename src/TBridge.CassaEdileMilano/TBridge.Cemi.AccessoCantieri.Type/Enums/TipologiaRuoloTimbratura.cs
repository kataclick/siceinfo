namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaRuoloTimbratura
    {
        Lavoratore = 0,
        DittaArtigiana,
        PersonaAbilitata,
        Referente,
        Altro
    }
}