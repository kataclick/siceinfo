﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Dto
{
    public class AssegnazioneLettore
    {
        public int IdLettore { get; set; }
        public string Utente { get; set; }
        public String Indirizzo { get; set; }
        public String Comune { get; set; }
        public String Provincia { get; set; }
        public String Cap { get; set; }
        public string IndirizzoCantiere
        {
            get
            {
                String indCantiere = null;

                if (!String.IsNullOrEmpty(this.Provincia))
                {
                    indCantiere = String.Format("{0} {1} {2} ({3})", this.Indirizzo, this.Cap, this.Comune, this.Provincia);
                }
                else
                {
                    indCantiere = String.Format("{0} {1} {2}", this.Indirizzo, this.Cap, this.Comune);
                }

                return indCantiere;
            }
        }
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
    }
}