﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Dto
{
    public class Cantiere
    {
        public int Id { get; set; }
        public string Utente { get; set; }
        public string IndirizzoCantiere { get; set; }
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
    }
}