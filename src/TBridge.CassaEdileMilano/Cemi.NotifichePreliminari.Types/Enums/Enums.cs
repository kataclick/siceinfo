﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.NotifichePreliminari.Types.Enums
{
    public enum IdTipoIncaricoImpresa
    {
        NonDefinito = 4,
        NessunaRelazione = 0,
        Esecutrice = 1,
        Affidataria = 2,
        AffidatariaEsecutrice = 3,
    }

    public enum IdRuoliPersone
    {
        NonDefinito,
        Committente,
        CoordinatoreRealizzazione,
        CoordinatoreProgettazione,
        ResponsabileLavori,
    }

    public enum IdTipoOperaEnum
    {
        NonDefinito = 0,
        Pubblica = 1,
        Altro = 2,
    }
}
