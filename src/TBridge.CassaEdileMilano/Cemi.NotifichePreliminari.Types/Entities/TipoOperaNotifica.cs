﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class TipoOperaNotifica
    {
        public int Id { set; get; }

        public String Descrizione { set; get; }
    }
}
