﻿
using System;
namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class Cantiere
    {
        public String NumeroNotifica { set; get; }

        public DateTime DataComunicazioneNotifica { set; get; }

        //public String Indirizzo { get; set; }

        //public String Comune { get; set; }

        public TBridge.Cemi.Geocode.Type.Entities.Indirizzo Indirizzo { set; get; }

        public String Comune
        {
            get
            {
                return Indirizzo != null ? Indirizzo.Comune : null;
            }
        }

        public String IndirizzoBase
        {
            get
            {
                return Indirizzo != null ? Indirizzo.IndirizzoBase : null;
            }
        }

        public String Provincia
        {
            get
            {
                return Indirizzo != null ? Indirizzo.Provincia : null;
            }
        }

        public String TipoOperaDescrizione { set; get; }

        public String TipoCategoriaDescrizione { set; get; }

        public String TipoTipologiaDescrizione { set; get; }

        public String AltraCategoriaDescrizione { set; get; }

        public String AltraTipologiaDescrizione { set; get; }

        public String Categoria
        {
            get
            {
                return this.TipoCategoriaDescrizione.ToUpper() == "ALTRO..." ? this.AltraCategoriaDescrizione.ToUpper() : this.TipoCategoriaDescrizione.ToUpper();
            }
        }

        public String Tipologia
        {
            get
            {
                return this.TipoTipologiaDescrizione.ToUpper() == "ALTRO..." ? this.AltraTipologiaDescrizione.ToUpper() : this.TipoTipologiaDescrizione.ToUpper();
            }
        }

        public Decimal AmmontareComplessivo { set; get; }

        public DateTime? DataInizio { get; set; }

        public Int32 Durata { get; set; }

        public String DescrizioneDurata { get; set; }

        public String DurataCompleta
        {
            get
            {
                return String.Format("{0} {1}", this.Durata, this.DescrizioneDurata).Trim();
            }
        }

        public String CommittenteCognome { get; set; }

        public String CommittenteNome { get; set; }

        public String CommittenteCodiceFiscale { get; set; }

        public String Committente
        {
            get
            {
                return String.Format("{0} {1}", this.CommittenteCognome, this.CommittenteNome).Trim();
            }
        }

        public String ImpresaRagioneSociale { get; set; }

        public String ImpresaCodiceFiscale { get; set; }

        public String ImpresaProvincia { get; set; }

        public String ImpresaCap { get; set; }

        public Int32? NumeroImprese { get; set; }

        public Int32? NumeroLavoratori { get; set; }
    }
}
