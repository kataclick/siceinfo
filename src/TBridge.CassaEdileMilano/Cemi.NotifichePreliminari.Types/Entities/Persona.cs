using System;
using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    [Serializable]
    public class Persona
    {
       // private string fax;
        private int? idPersona;
       // private string nominativo;
       // private string ragioneSociale;
        //private string telefono;
        //private String _ruoloDescizione;

        public Persona()
        {
        }

        //public Persona(int? idPersona, string nominativo, string ragioneSociale, string indirizzo,
        //               string telefono, string fax)
        //{
        //    this.idPersona = idPersona;
        //    this.nominativo = nominativo;
        //    this.ragioneSociale = ragioneSociale;
        //    this.indirizzo = indirizzo;
        //    this.telefono = telefono;
        //    this.fax = fax;
        //}

        public int? IdPersona
        {
            get { return idPersona; }
            set { idPersona = value; }
        }

        public string Nominativo
        {
            get { return String.Format("{0} {1}", Nome, Cognome); }
            
        }

        //public string RagioneSociale
        //{
        //    get { return ragioneSociale; }
        //    set { ragioneSociale = value; }
        //}

        public string IndirizzoCompleto
        {
            get { return Indirizzo != null ? Helpers.HelperMethods.FormattaIndirizzo(Indirizzo) : null; }
            
        }

        //public string Telefono
        //{
        //    get { return telefono; }
        //    set { telefono = value; }
        //}

        //public string Fax
        //{
        //    get { return fax; }
        //    set { fax = value; }
        //}

        public String Nome { set; get; }

        public String Cognome { set; get; }

        public String CodiceFiscale { set; get; }

        public DateTime? DataNascita { set; get; }

        public TBridge.Cemi.Type.Entities.Indirizzo Indirizzo { set; get; }

        public RuoloPersona Ruolo { set; get; }

        public IdRuoliPersone? RuoloId 
        {
            get { return Ruolo != null ? Ruolo.Id : (IdRuoliPersone?)null; } 
        }

        public String RuoloDescrizione 
        {
            get
            {
                return Ruolo != null ? Ruolo.Descrizione : null;
            }
        }
    }
}