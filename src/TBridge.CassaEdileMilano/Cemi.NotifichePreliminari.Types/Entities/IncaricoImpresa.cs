﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class IncaricoImpresa
    {
        public IdTipoIncaricoImpresa Id { private set; get; }
        public String Descrizione { private set; get; }

        public IncaricoImpresa(String id, String descrizione)
        {
            Descrizione = descrizione.Trim();
            switch (id.Trim())
            {
                case "0":
                    Id = IdTipoIncaricoImpresa.NessunaRelazione;
                    break;
                case "1":
                    Id = IdTipoIncaricoImpresa.Esecutrice;
                    break;
                case "2":
                    Id = IdTipoIncaricoImpresa.Affidataria;
                    break;
                case "3":
                    Id = IdTipoIncaricoImpresa.AffidatariaEsecutrice;
                    break;
                default:
                    Descrizione = "NON DEFINITO";
                    Id = IdTipoIncaricoImpresa.NonDefinito;
                    break;
            }
        }


        public String IdTipoIncaricoEnumToString(IdTipoIncaricoImpresa id)
        {
            String retval;
            switch (id)
            {
                //case IdTipoIncaricoImpresa.NonDefinito:
                //    break;
                case IdTipoIncaricoImpresa.NessunaRelazione:
                    retval = "0";
                    break;
                case IdTipoIncaricoImpresa.Esecutrice:
                    retval = "1";
                    break;
                case IdTipoIncaricoImpresa.Affidataria:
                    retval = "2";
                    break;
                case IdTipoIncaricoImpresa.AffidatariaEsecutrice:
                    retval = "3";
                    break;
                default:
                    retval = "NON DEFINITO";
                    break;
            }

            return retval;
        }
        //0	NESSUNA RELAZIONE
        //1	ESECUTRICE
        //3	AFFIDATARIA/ESECUTRICE
        //2	AFFIDATARIA
        //3	AFFIDATARIA/ESECUTRICE
        //1	ESECUTRICE
        //3	AFFIDATARIA/ESECUTRICE
    }
}
