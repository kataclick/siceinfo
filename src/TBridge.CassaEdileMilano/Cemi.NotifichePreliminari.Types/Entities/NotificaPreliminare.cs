using System;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    [Serializable]
    public class NotificaPreliminare
    {

        public NotificaPreliminare()
        {
            Indirizzi = new IndirizzoCollection();

            Committenti = new PersonaCollection();
            AltrePersone = new PersonaCollection();
            Imprese = new ImpresaNotifichePreliminariCollection();
            //CoordinatoriProgettazione = new PersonaCollection();
            //ResponsabiliLavori = new PersonaCollection();
            //ImpreseAffidatarie = new ImpresaNotifichePreliminariCollection();
            //ImpreseEsecutrici = new ImpresaNotifichePreliminariCollection();
            //ImpreseSenzaRelazione = new ImpresaNotifichePreliminariCollection();



        }


        public int IdNotificaCantiere { set; get; }

        public String NumeroNotifica { set; get; }

        public String ProtocolloNotifica { set; get; }

        public DateTime DataComunicazioneNotifica { set; get; }

        public DateTime? DataAggiornamentoNotifica { get; set; }

        public Int32 NumeroLavoratoriAutonomi { set; get; }

        public Int32 NumeroImprese { set; get; }

        public String TipoContrattoId { set; get; }

        public String TipoContrattoDescrizione { set; get; }

        public String ContrattoId { set; get; }

        public IdTipoOperaEnum TipoOperaId { set; get; }

        public String TipoOperaDescrizione { set; get; }

        public String TipoCategoriaDescrizione { set; get; }

        public String TipoTipologiaDescrizione { set; get; }

        public String AltraCategoriaDescrizione { set; get; }

        public String AltraTipologiaDescrizione { set; get; }

        public Decimal AmmontareComplessivo { set; get; }

        public Boolean Articolo9011 { set; get; }

        public Boolean Articolo9011B { set; get; }

        public Boolean Articolo9011C { set; get; }

        public Int32 IdCantiereRegione { set; get; }


        public IndirizzoCollection Indirizzi { set; get; }

        public PersonaCollection Committenti { set; get; }

        public PersonaCollection AltrePersone { set; get; }

        public ImpresaNotifichePreliminariCollection Imprese { set; get; }

        public String TipoCategoriaDescrizioneVisualizzazione
        {
            get
            {
                return String.IsNullOrEmpty(AltraCategoriaDescrizione) ?
                TipoCategoriaDescrizione.ToUpper() : AltraCategoriaDescrizione.ToUpper();
            }
        }

        public String TipoTipologiaDescrizioneVisualizzazione
        {
            get
            {
                return String.IsNullOrEmpty(AltraTipologiaDescrizione) ?
                      TipoTipologiaDescrizione.ToUpper() : AltraTipologiaDescrizione.ToUpper();
            }
        }



        //public PersonaCollection CoordinatoriRealizazione { set; get; }

        //public PersonaCollection CoordinatoriProgettazione { set; get; }

        //public PersonaCollection ResponsabiliLavori { set; get; }

        //public ImpresaNotifichePreliminariCollection ImpreseEsecutrici { set; get; }

        //public ImpresaNotifichePreliminariCollection ImpreseAffidatarie { set; get; }

        //public ImpresaNotifichePreliminariCollection ImpreseSenzaRelazione { set; get; }


        //public void SetTipoOperaId(Int32 id)
        //{
        //    TipoOperaId = (IdTipoOperaEnum)id;
        //}

        public String Categoria
        {
            get
            {
                return this.TipoCategoriaDescrizione.ToUpper() == "ALTRO..." ? this.AltraCategoriaDescrizione.ToUpper() : this.TipoCategoriaDescrizione.ToUpper();
            }
        }

        public String Tipologia
        {
            get
            {
                return this.TipoTipologiaDescrizione.ToUpper() == "ALTRO..." ? this.AltraTipologiaDescrizione.ToUpper() : this.TipoTipologiaDescrizione.ToUpper();
            }
        }

    }
}
