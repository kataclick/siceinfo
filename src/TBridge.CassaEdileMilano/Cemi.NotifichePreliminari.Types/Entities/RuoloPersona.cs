﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class RuoloPersona
    {
        public IdRuoliPersone Id { private set; get; }
        public String Descrizione { private set; get; }


        public RuoloPersona (String descrizione)
        {
            IdRuoliPersone ruoloId;
            Descrizione = descrizione.Trim();

            switch (Descrizione)
            {
                case "COMMITTENTE":
                    ruoloId = IdRuoliPersone.Committente;
                    break;
                case "COORDINATORE REALIZZAZIONE":
                    ruoloId = IdRuoliPersone.CoordinatoreRealizzazione;
                    break;
                case "RESPONSABILE LAVORI":
                    ruoloId = IdRuoliPersone.ResponsabileLavori;
                    break;
                case "COORDINATORE PROGETTAZIONE":
                    ruoloId = IdRuoliPersone.CoordinatoreProgettazione;
                    break;
                default:
                    ruoloId = IdRuoliPersone.NonDefinito;
                    Descrizione = "NON DEFINITO";
                    break;
            }

            Id = ruoloId;
        }

        public String IdRuoloEnumToString(IdRuoliPersone id)
        {
            String retval;
            switch (id)
            {
                //case IdRuoliPersone.NonDefinito:
                //    break;
                case IdRuoliPersone.Committente:
                    retval = "COMMITTENTE";
                    break;
                case IdRuoliPersone.CoordinatoreRealizzazione:
                    retval = "COORDINATORE REALIZZAZIONE";
                    break;
                case IdRuoliPersone.CoordinatoreProgettazione:
                    retval = "COORDINATORE PROGETTAZIONE";
                    break;
                case IdRuoliPersone.ResponsabileLavori:
                    retval = "RESPONSABILE LAVORI";
                    break;
                default:
                    retval = "NON DEFINITO";
                    break;
            }

            return retval;
        }
    }
}
