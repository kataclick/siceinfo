﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Helpers
{
    public class HelperMethods
    {

        //public static IdRuoliPersone RuoloDescrizioneToEnumId(String descrizione)
        //{

        //    IdRuoliPersone ruoloId;


        //    switch (descrizione.Trim())
        //    {
        //        case "COMMITTENTE":
        //            ruoloId = IdRuoliPersone.Committente;
        //            break;
        //        case "COORDINATORE REALIZZAZIONE":
        //            ruoloId = IdRuoliPersone.CoordinatoreRealizzazione;
        //            break;
        //        case "RESPONSABILE LAVORI":
        //            ruoloId = IdRuoliPersone.ResponsabileLavori;
        //            break;
        //        case "COORDINATORE PROGETTAZIONE":
        //            ruoloId = IdRuoliPersone.CoordinatoreProgettazione;
        //            break;
        //        default:
        //            ruoloId = IdRuoliPersone.NonDefinito;
        //           // _ruoloDescizione = "SCONOSCIUTO";
        //            break;
        //    }


        //    return ruoloId;
        //}

        public static String FormattaIndirizzo(TBridge.Cemi.Type.Entities.Indirizzo indirizzo)
        {
            if (indirizzo == null)
            {
                return null;
            }

            return String.Format("{0}<br />{1}{2}{3}", indirizzo.NomeVia,
                                                        String.IsNullOrEmpty(indirizzo.Cap) ? null : String.Concat(indirizzo.Cap, " "),
                                                        indirizzo.Comune,
                                                         String.IsNullOrEmpty(indirizzo.Provincia) ? null : String.Format(" ({0})", indirizzo.Provincia));
        }
    }
}
