using System;
using System.Collections.Generic;
using System.Text;
using Cemi.NotifichePreliminari.Types.Entities;
using System.Linq;

namespace Cemi.NotifichePreliminari.Types.Collections
{
    [Serializable]
    public class ImpresaNotifichePreliminariCollection : List<ImpresaNotifichePreliminari>
    {

        public Boolean ContainsId(Int32 id)
        {
            return this.FirstOrDefault(x => x.IdImpresa == id) != null;
        }
    }
}
