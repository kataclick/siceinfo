﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.NotifichePreliminari.Types.Entities;

namespace Cemi.NotifichePreliminari.Types.Collections
{
    public class PersonaCollection :List<Persona>
    {
        public Boolean ContainsId(Int32 id)
        {
            return this.FirstOrDefault(x => x.IdPersona == id) != null;
        }
    }
}
