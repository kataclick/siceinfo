using System;

namespace Cemi.NotifichePreliminari.Types.Filters
{
    [Serializable]
    public class NotificaPreliminareFilter
    {

        private String _committenteCodiceFiscale;
        private String _committenteNome;
        private String _committenteCognome;
        private String _cantiereIndirizzo;
        private String _CantiereComune;
        private String _cantiereProvncia;
        private String _cantiereCap;
        private String _impresaRagioneSociale;
        private String _impresaCfPIva;
        private String _impresaProvincia;
        private String _impresaCap;
        private String _naturaOpera;
        private String _numeroNotifica;


        public String NumeroNotifica
        {
            set { _numeroNotifica = String.IsNullOrEmpty(value) ? null : value; }
            get { return _numeroNotifica; }
        }

        public String CommittenteCodiceFiscale 
        {
            set { _committenteCodiceFiscale = String.IsNullOrEmpty(value) ? null : value; }
            get { return _committenteCodiceFiscale; }
        }
        public String CommittenteCognome
        {
            set { _committenteCognome = String.IsNullOrEmpty(value) ? null : value; }
            get { return _committenteCognome; }
        }
        public String CommittenteNome
        {
            set { _committenteNome = String.IsNullOrEmpty(value) ? null : value; }
            get { return _committenteNome; }
        }

        public String NaturaOpera
        {
            set { _naturaOpera = String.IsNullOrEmpty(value) ? null : value.Trim(); }
            get { return _naturaOpera; }
        }

        public String CantiereIndirizzo
        {
            set { _cantiereIndirizzo = String.IsNullOrEmpty(value) ? null : value; }
            get { return _cantiereIndirizzo; }
        }
        public String CantiereComune
        {
            set { _CantiereComune = String.IsNullOrEmpty(value) ? null : value; }
            get { return _CantiereComune; }
        }
        public String CantiereProvncia
        {
            set {_cantiereProvncia = String.IsNullOrEmpty(value) ? null : value; }
            get { return _cantiereProvncia; }
        }
        public String CantiereCap
        {
            set { _cantiereCap = String.IsNullOrEmpty(value) ? null : value; }
            get { return _cantiereCap; }
        }

        public Boolean? CantiereIndirizzoGeolocalizzato { set; get; }

        public DateTime? DataComunicazioneDal { set; get; }
        public DateTime? DataComunicazioneAl { set; get; }
        public DateTime? DataInizioLavoriDal { set; get; }
        public DateTime? DataInizioLavoriAl { set; get; }
        public Decimal? Ammontare { set; get; }

        public String ImpresaRagioneSociale
        {
            set { _impresaRagioneSociale = String.IsNullOrEmpty(value) ? null : value; }
            get { return _impresaRagioneSociale; }
        }
        public String ImpresaCfPIva
        {
            set { _impresaCfPIva = String.IsNullOrEmpty(value) ? null : value; }
            get { return _impresaCfPIva; }
        }
        public String ImpresaProvincia
        {
            set { _impresaProvincia = String.IsNullOrEmpty(value) ? null : value; }
            get { return _impresaProvincia; }
        }
        public String ImpresaCap
        {
            set { _impresaCap = String.IsNullOrEmpty(value) ? null : value; }
            get { return _impresaCap; }
        }

    }
}