﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using Cemi.CigoTelematica.Type;
using Cemi.CigoTelematica.Type.Filters;
using Cemi.CigoTelematica.Data;
using Cemi.CigoTelematica.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;



namespace Cemi.CigoTelematica.Business
{
    public class Business
    {
        private readonly CigoTelematicaDataAccess _dataaccess = new CigoTelematicaDataAccess();


        //NManelli: da sistemare gli stati degli if e dell'else (stati domanda --> tabella tipo stato prestazione)
        public Boolean AbilitaControllo(String stato)
        {
            Boolean retVal = true;
            if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
            {
                if (stato == "I")
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                if (stato == "I" || stato == "T" || stato == "O" || stato == "S")
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }

            }

            return retVal;
        }


        ////NManelli: da sistemare gli stati degli if e dell'else (stati domanda --> tabella tipo stato prestazione)
        //public Boolean AbilitaDomanda(String stato)
        //{
        //    Boolean retVal = true;
        //    if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
        //    {
        //        if (stato == "I")
        //        {
        //            retVal = true;
        //        }
        //        else
        //        {
        //            retVal = false;
        //        }
        //    }
        //    else
        //    {
        //        if (stato == "I" || stato == "T" || stato == "O" || stato == "S")
        //        {
        //            retVal = true;
        //        }
        //        else
        //        {
        //            retVal = false;
        //        }

        //    }

        //    return retVal;
        //}



    }

    //public DomandeLavoratoriCollection GetDomandeLavoratori(EstrattoContoFilter estrattoContoFilter)
    //{
    //        return _dataAccess.GetDomandeLavoratori(estrattoContoFilter.PeriodoDa, estrattoContoFilter.PeriodoA, estrattoContoFilter.RagioneSocialeImpresa, estrattoContoFilter.CodiceFiscaleImpresa, estrattoContoFilter.IdImpresa);
    //}

      
    
}
