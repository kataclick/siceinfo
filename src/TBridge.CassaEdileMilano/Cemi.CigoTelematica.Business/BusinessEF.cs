﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
//using Cemi.CigoTelematica.Type;
using Cemi.CigoTelematica.Type.Filters;
using TBridge.Cemi.Data;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Collections;
using Cemi.CigoTelematica.Type.Collections;
using Cemi.CigoTelematica.Type.Entities;


namespace Cemi.CigoTelematica.Business
{
    public class BusinessEF
    {
        //per estratto conto
        //public List<CigoTelematicaDomanda> GetDomandeLavoratori(EstrattoContoFilter filter)
        //{
        //     if (filter.PeriodoA.HasValue)
        //        filter.PeriodoA = filter.PeriodoA.Value.AddMonths(1);

        //     List<CigoTelematicaDomanda> ret = new List<CigoTelematicaDomanda>();


        //    using (SICEEntities context = new SICEEntities())
        //    {
                
        //        var query = from dom in context.CigoTelematicaDomande.Include("CigoTelematicaLavoratori")
        //                    where
        //                    (!filter.IdImpresa.HasValue || dom.IdImpresa == filter.IdImpresa)
        //                    &&
        //                (String.IsNullOrEmpty(filter.RagioneSocialeImpresa) ||
        //                 dom.Impresa.RagioneSociale.ToUpper().Contains(filter.RagioneSocialeImpresa))
        //                &&
        //                (String.IsNullOrEmpty(filter.CodiceFiscaleImpresa) ||
        //                 dom.Impresa.CodiceFiscale.ToUpper() == filter.CodiceFiscaleImpresa ||
        //                 dom.Impresa.PartitaIVA.ToUpper() == filter.CodiceFiscaleImpresa)
        //                 &&
        //                 (((!filter.PeriodoDa.HasValue) || dom.dataDomanda >= filter.PeriodoDa)
        //                &&
        //                (!filter.PeriodoA.HasValue || dom.dataDomanda <= filter.PeriodoA))
                        
        //                    select dom;

        //        ret = query.ToList();

        //                    }

        //    return ret;
        //}


        public List<DomandeEstrattoConto> GetDomandeLavoratoriEstrattoConto(EstrattoContoFilter filter)
        {
            List<DomandeEstrattoConto> domande;
            if (filter.PeriodoA.HasValue)
                filter.PeriodoA = filter.PeriodoA.Value.AddMonths(1);

            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoDomandaSet
                            join prest in context.PrestazioniComplete
                             on new { annoProt = dom.AnnoProtocollo, numProt = dom.NumeroProtocollo , tipoProt = dom.TipoProtocollo, idTipo = dom.IdTipoPrestazione }
                             equals
                             new { annoProt = prest.annoProtocollo, numProt = prest.numeroProtocollo, tipoProt = prest.tipoProtocollo, idTipo = prest.idTipoPrestazione }
                             join imp in context.Imprese
                             on prest.idImpresa equals imp.Id
                             where
                            (prest.idTipoStatoPrestazione.ToUpper() == "L")
                            &&
                            (!filter.IdImpresa.HasValue || prest.idImpresa == filter.IdImpresa)
                            &&
                            (String.IsNullOrEmpty(filter.RagioneSocialeImpresa) ||
                            imp.RagioneSociale.ToUpper().Contains(filter.RagioneSocialeImpresa))
                            &&
                            (String.IsNullOrEmpty(filter.CodiceFiscaleImpresa) ||
                            imp.CodiceFiscale.ToUpper() == filter.CodiceFiscaleImpresa ||
                            imp.PartitaIVA.ToUpper() == filter.CodiceFiscaleImpresa)
                            &&
                            (((!filter.PeriodoDa.HasValue) || prest.dataDomanda >= filter.PeriodoDa)
                            &&
                            (!filter.PeriodoA.HasValue || prest.dataDomanda <= filter.PeriodoA))
                            select new DomandeEstrattoConto
                                                {
                                                    Anno = dom.Anno.Value,
                                                    Mese = dom.Mese.Value,
                                                    //ComuneCantiere = domtel.comuneCantiere,
                                                    //IndirizzoCantiere = domtel.indirizzoCantiere,
                                                    //ProvinciaCantiere = domtel.provinciaCantiere,
                                                    IdImpresa = prest.idImpresa.Value,
                                                    RagioneSocialeImpresa = imp.RagioneSociale,
                                                    Importo = dom.ImportoRiconosciuto ?? 0,
                                                    AnnoProtocolloDomanda = dom.AnnoProtocollo,
                                                    NumeroProtocolloDomanda = dom.NumeroProtocollo,
                                                    TipoProtocolloDomanda = dom.TipoProtocollo,
                                                };
                domande = query.ToList();

                //agggiumgere join con cigotelematicadomande per recuperare i dati del cantiere
            }
            return domande;     
        
        }

        public void InserisciProtocolloEstrattoConto(int annoProtocollo, int numeroProtocollo, string tipoProtocollo)
        {
            using (SICEEntities context = new SICEEntities())
            {
                bool exist = context.CigoTelematicaEstrattoContoProtocollo.Any(x => x.AnnoProtocolloDomanda == annoProtocollo &&
                                                                        x.NumeroProtocolloDomanda == numeroProtocollo &&
                                                                        x.TipoProtocolloDomanda == tipoProtocollo);

                if (!exist)
                {

                    var ultimoProtocollo = context.CigoTelematicaEstrattoContoProtocollo.Where(x => x.AnnoProtocollo == DateTime.Now.Year).OrderByDescending(x => x.NumeroProtocollo).FirstOrDefault();
                    context.CigoTelematicaEstrattoContoProtocollo.AddObject(new CigoTelematicaEstrattoContoProtocollo 
                                                                              {
                                                                                AnnoProtocolloDomanda = annoProtocollo,
                                                                                NumeroProtocolloDomanda = numeroProtocollo,
                                                                                TipoProtocolloDomanda = tipoProtocollo,
                                                                                AnnoProtocollo = DateTime.Now.Year,
                                                                                NumeroProtocollo = ultimoProtocollo != null ? ultimoProtocollo.NumeroProtocollo + 1 : 1,
                                                                                DataInserimentoRecord = DateTime.Now,
                                                                              });
                    context.SaveChanges();
                }
            }
        
        }

        //recupera domande da filtro per imprese/consulente
        public List<CigoTelematicaDomanda> GetDomandeImpresa(DomandeFilter filter)
        {
            List<CigoTelematicaDomanda> ret;


            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoTelematicaDomande.Include("TipoStatoPrestazione")
                        where
                        (!filter.IdImpresa.HasValue || dom.IdImpresa == filter.IdImpresa)
                            &&
                        (String.IsNullOrEmpty(filter.ComuneCantiere) ||
                         dom.ComuneCantiere.ToUpper().Contains(filter.ComuneCantiere.ToUpper()))
                         &&
                         (String.IsNullOrEmpty(filter.ProvinciaCantiere) ||
                         dom.ProvinciaCantiere.ToUpper().Contains(filter.ProvinciaCantiere.ToUpper()))
                         &&
                         (String.IsNullOrEmpty(filter.IndirizzoCantiere) ||
                         dom.IndirizzoCantiere.ToUpper().Contains(filter.IndirizzoCantiere.ToUpper()))
                         &&
                         (((!filter.PeriodoAnnoDa.HasValue) || dom.Anno >= filter.PeriodoAnnoDa)
                        &&
                        (!filter.PeriodoMeseDa.HasValue || dom.Mese >= filter.PeriodoMeseDa))
                        &&
                        (((!filter.PeriodoAnnoA.HasValue) || dom.Anno <= filter.PeriodoAnnoA)
                        &&
                        (!filter.PeriodoMeseA.HasValue || dom.Mese <= filter.PeriodoMeseA))

                        select dom;

                ret = query.ToList();
            
            }
            return ret;
        }



        public List<TipoStatoPrestazione> GetTipiStatoCigoTelematica()
        {
            List<TipoStatoPrestazione> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from stato in context.TipiStatoPrestazione
                            orderby stato.Descrizione
                            select stato
                            ;
                ret = query.ToList();
            }

            return ret;
        }




        // recupera da filtro le domande per il backoffice
        public List<CigoTelematicaDomanda> GetDomandeBackOffice(DomandeFilter filter)
        {

            List<CigoTelematicaDomanda> ret;

            using (SICEEntities context = new SICEEntities())
            {
                //if (filter.IdLavoratore.HasValue || !String.IsNullOrEmpty(filter.CognomeLavoratore) || !String.IsNullOrEmpty(filter.NomeLavoratore) || !String.IsNullOrEmpty(filter.CodiceFiscaleLavoratore))
                //{
                var query = from dom in context.CigoTelematicaDomande.Include("TipoStatoPrestazione").Include("UtenteInCarico")//.Include("Lavoratore")
                            //join lav in context.CigoTelematicaLavoratori on dom.Id equals lav.IdDomandaCigo
                                                        
                            where
                            (!filter.IdImpresa.HasValue || dom.IdImpresa == filter.IdImpresa)
                             &&
                             (String.IsNullOrEmpty(filter.StatoDomanda) || dom.IdStato == filter.StatoDomanda)
                             &&
                            (String.IsNullOrEmpty(filter.ComuneCantiere) ||
                             dom.ComuneCantiere.ToUpper().Contains(filter.ComuneCantiere.ToUpper()))
                             &&
                             (String.IsNullOrEmpty(filter.ProvinciaCantiere) ||
                             dom.ProvinciaCantiere.ToUpper().Contains(filter.ProvinciaCantiere.ToUpper()))
                             &&
                             (String.IsNullOrEmpty(filter.IndirizzoCantiere) ||
                             dom.IndirizzoCantiere.ToUpper().Contains(filter.IndirizzoCantiere.ToUpper()))
                             &&
                             (((!filter.PeriodoAnnoDa.HasValue) || dom.Anno >= filter.PeriodoAnnoDa)
                            &&
                            (!filter.PeriodoMeseDa.HasValue || dom.Mese >= filter.PeriodoMeseDa))
                            &&
                             (((!filter.PeriodoAnnoA.HasValue) || dom.Anno <= filter.PeriodoAnnoA)
                            &&
                            (!filter.PeriodoMeseA.HasValue || dom.Mese <= filter.PeriodoMeseA))
                            &&
                            //(dom.CigoTelematicaLavoratori.Any(x =>  (!filter.IdLavoratore.HasValue || x.Lavoratore.Id == filter.IdLavoratore)
                            //&&
                            //(String.IsNullOrEmpty(filter.CognomeLavoratore) ||
                            //x.Lavoratore.Cognome.ToUpper().Contains(filter.CognomeLavoratore.ToUpper()))
                            //&&
                            //(String.IsNullOrEmpty(filter.NomeLavoratore) ||
                            //x.Lavoratore.Nome.ToUpper().Contains(filter.NomeLavoratore.ToUpper()))
                            //&&
                            //(String.IsNullOrEmpty(filter.CodiceFiscaleLavoratore) ||
                            //x.Lavoratore.CodiceFiscale.Equals(filter.CodiceFiscaleLavoratore, StringComparison.CurrentCultureIgnoreCase))))
                            ((dom.CigoTelematicaLavoratori.Count == 0 && !filter.IdLavoratore.HasValue && String.IsNullOrEmpty(filter.CognomeLavoratore) && String.IsNullOrEmpty(filter.NomeLavoratore) && String.IsNullOrEmpty(filter.CodiceFiscaleLavoratore))
                              || dom.CigoTelematicaLavoratori.Any(x => (!filter.IdLavoratore.HasValue || x.Lavoratore.Id == filter.IdLavoratore)
                            &&
                            (String.IsNullOrEmpty(filter.CognomeLavoratore) ||
                            x.Lavoratore.Cognome.ToUpper().Contains(filter.CognomeLavoratore.ToUpper()))
                            &&
                            (String.IsNullOrEmpty(filter.NomeLavoratore) ||
                            x.Lavoratore.Nome.ToUpper().Contains(filter.NomeLavoratore.ToUpper()))
                            &&
                            (String.IsNullOrEmpty(filter.CodiceFiscaleLavoratore) ||
                            x.Lavoratore.CodiceFiscale.Equals(filter.CodiceFiscaleLavoratore, StringComparison.CurrentCultureIgnoreCase))))

                            select dom;

                    ret = query.ToList();
                    return ret;
                   

            }
                   
        }



        public void InsertNuovaDomanda(CigoTelematicaDomanda domanda)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.CigoTelematicaDomande.AddObject(domanda);
                context.SaveChanges();
            }

        }


        //recupera la domanda
        public CigoTelematicaDomanda GetDomandaById(int id)
        {
            CigoTelematicaDomanda ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoTelematicaDomande.Include("TipoStatoPrestazione")
                            where
                            dom.Id == id

                            select dom;

                ret = query.FirstOrDefault();

            }
            return ret;
        }


        // controllo dei check dei giorni
        public List<CigoTelematicaLavoratoreGiorno> GetLavoratoreGiorni(int idLavoratore, int idDomanda)
        {
            List<CigoTelematicaLavoratoreGiorno> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoTelematicaLavoratoriGiorni
                            where
                            dom.IdDomandaCigo == idDomanda && 
                            dom.IdLavoratore == idLavoratore
                            select dom;

                ret = query.ToList();

            }
            return ret;
        }

        
        //NM aggiunta
        public CigoTelematicaLavoratore OreRiconosciute(int idDomanda,int idLavoratore)
        {
            CigoTelematicaLavoratore ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoTelematicaLavoratori
                            where
                            dom.IdDomandaCigo == idDomanda &&
                            dom.IdLavoratore == idLavoratore
                            select dom;

                ret = query.FirstOrDefault();

            }
            return ret;
        }


        // possibili lavoratori con ore CIGO
        public LavoratoreCigoCollection GetLavoratoriConCigo(int idDomanda)
        {
            List<LavoratoreCigo> lavoratoriCigo;
            List<CigoTelematicaLavoratore> lavoratoriAggiunti;
            LavoratoreCigoCollection lavoratori = new LavoratoreCigoCollection();
            CigoTelematicaDomanda domanda;
            String[] filtroTipiOra;

            if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
            {
                filtroTipiOra = new String[] { "027" };
            }
            else
            {
                filtroTipiOra = new String[] { "027", "024", "025", "026" };
            }
            using (SICEEntities context = new SICEEntities())
            {

                //per imprese e consulenti considero solo lavoratori con ore 027 (cigo maltempo)
                //if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
                //{

                domanda = context.CigoTelematicaDomande.First(x => x.Id == idDomanda);
                    var query = from dom in context.OreDenunciateCompleteImprese.Include("Lavoratore").Include("TipiOra")
                                join lavrap in context.RapportiLavoratoreImpresaCompleti on
                                new
                                {
                                    IdImpresa = dom.IdImpresa,
                                    IdLavoratore = dom.IdLavoratore,
                                    Anno = dom.Data.Year,
                                    Mese = dom.Data.Month,

                                } equals new
                                {
                                    IdImpresa = lavrap.idImpresa,
                                    IdLavoratore = lavrap.idLavoratore,
                                    Anno = lavrap.dataDenuncia.Year,
                                    Mese = lavrap.dataDenuncia.Month,
                                }
                                
                                where
                                dom.Data.Month == domanda.Mese && dom.Data.Year == domanda.Anno && dom.IdImpresa == domanda.IdImpresa && filtroTipiOra.Contains( dom.IdTipoOra) 
                                && lavrap.categoria == "A"  // solo apprendisti
                                select new Type.Entities.LavoratoreCigo
                                                    {
                                                        Lavoratore = dom.Lavoratore,
                                                        OreCigo = (int)dom.OreDichiarate,
                                                        TipoOre = dom.IdTipoOra,
                                                        TipoOreDescrizione = dom.TipiOra.Descrizione,
                                                    };

                    lavoratoriCigo = query.ToList();

                var query1 = from dom in context.CigoTelematicaLavoratori
                            where
                            dom.IdDomandaCigo == idDomanda

                            select dom;

                lavoratoriAggiunti = query1.ToList();
            }

            foreach (var item in lavoratoriCigo)
            {
                bool aggiunto = lavoratoriAggiunti.Any(x => x.IdLavoratore == item.Lavoratore.Id);
                lavoratori.Add(new Type.Entities.LavoratoreCigo
                                                    {
                                                        Lavoratore = item.Lavoratore,
                                                        OreCigo = (int)item.OreCigo,
                                                        TipoOre = item.TipoOre,
                                                        TipoOreDescrizione = item.TipoOreDescrizione,
                                                        Aggiunto = aggiunto,
                                                    });
            }

            return lavoratori;
        }



        // controllo se ci sono solo apprendisti in denuncia o anche operai (per n°aut. e certificato inps)
        public bool OperaiPresentiInDenuncia(int anno, int mese, int idImpresa)
        {
            Boolean result;
            String[] filtroTipiOra;

            filtroTipiOra = new String[] { "027" };
            
            using (SICEEntities context = new SICEEntities())
            {

                //per imprese e consulenti considero solo lavoratori con ore 027 (cigo maltempo)
                var query = from dom in context.OreDenunciateCompleteImprese//.Include("Lavoratore").Include("TipiOra")
                            join lavrap in context.RapportiLavoratoreImpresaCompleti on
                            new
                            {
                                IdImpresa = dom.IdImpresa,
                                IdLavoratore = dom.IdLavoratore,
                                Anno = dom.Data.Year,
                                Mese = dom.Data.Month,

                            } equals new
                            {
                                IdImpresa = lavrap.idImpresa,
                                IdLavoratore = lavrap.idLavoratore,
                                Anno = lavrap.dataDenuncia.Year,
                                Mese = lavrap.dataDenuncia.Month,
                            }

                            where
                            dom.Data.Month == mese && dom.Data.Year == anno && dom.IdImpresa == idImpresa && filtroTipiOra.Contains(dom.IdTipoOra)
                            && lavrap.categoria == "O"// operai
                            select dom;
               result = query.Any();
            }

            return result;
        }





        //inserisce lavoratore in domanda da tasto aggiungi
        public void InsertLavoratoreInDomanda(int idLavoratore, int idDomanda, int oreCigo, String idtipoOre, int idUtente)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.CigoTelematicaLavoratori.AddObject(new CigoTelematicaLavoratore 
                                                                {IdDomandaCigo = idDomanda,
                                                                 IdLavoratore = idLavoratore, 
                                                                 IdStato = "L",
                                                                 OreRichieste = oreCigo,
                                                                 IdUtenteInserimento = idUtente,
                                                                 DataInserimentoRecord = DateTime.Now,
                                                                 IdTipoOreDenuncia = idtipoOre,
                                                                });


                context.SaveChanges(); 

            }
            

        }


        //cancellazione di un lavoratore dalla domanda (pulsante elimina delle griglia lavoratore)
        public void DeleteLavoratoreInDomanda(int idLavoratore, int idDomanda)
        {
            using (SICEEntities context = new SICEEntities())
            {
                foreach (var item in context.CigoTelematicaBustePaga.Where(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == idLavoratore))
	            {
                    context.CigoTelematicaBustePaga.DeleteObject(item);
	            }

                foreach (var item in context.CigoTelematicaLavoratoriGiorni.Where(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == idLavoratore))
                {
                    context.CigoTelematicaLavoratoriGiorni.DeleteObject(item);
                }

                context.CigoTelematicaLavoratori.DeleteObject(context.CigoTelematicaLavoratori.First(x => x.IdLavoratore == idLavoratore && x.IdDomandaCigo == idDomanda));
                context.SaveChanges();
            }
        
        }


        //cancellazione di una domanda (elimina anche i lavoratori)
        public void DeleteDomanda(int idDomanda)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var domanda = context.CigoTelematicaDomande.First(x => x.Id == idDomanda);

                foreach (var lavoratore in domanda.CigoTelematicaLavoratori)
                {
                    foreach (var item in context.CigoTelematicaBustePaga.Where(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == lavoratore.IdLavoratore))
	                {
                        context.CigoTelematicaBustePaga.DeleteObject(item);
	                }

                    foreach (var item in context.CigoTelematicaLavoratoriGiorni.Where(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == lavoratore.IdLavoratore))
                    {
                        context.CigoTelematicaLavoratoriGiorni.DeleteObject(item);
                    }

                    context.CigoTelematicaLavoratori.DeleteObject(lavoratore);
                }

                foreach (var item in context.CigoTelematicaDocumentiInps.Where(x => x.IdDomandaCigo == idDomanda))
                {
                    context.CigoTelematicaDocumentiInps.DeleteObject(item);
                }

                context.CigoTelematicaDomande.DeleteObject(domanda);

                context.SaveChanges();

             }
        }


        //restituisce tutti i lavoratori della domanda (griglia gestione domanda)
        public List<CigoTelematicaLavoratore> GetLavoratoriInDomanda(int idDomanda)
        {
            List<CigoTelematicaLavoratore> lavoratori;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoTelematicaLavoratori.Include("Lavoratore").Include("CigoTelematicaStatoLavoratore").Include("TipiOra")
                            where
                            dom.IdDomandaCigo == idDomanda
                            
                            select dom;

                lavoratori = query.ToList();
                
            }

            return lavoratori;
        }

        public List<LavoratoreDettagli> GetLavoratoriInDomanda(int idDomanda, Boolean eseguiControlli = false)
        {
            List<LavoratoreDettagli> lavoratori;
            using (SICEEntities context = new SICEEntities())
            {
                var query = from lav in context.CigoTelematicaLavoratori.Include("CigoTelematicaLavoratoriGiorni")
                                .Include("Lavoratore").Include("CigoTelematicaDomanda").Include("CigoTelematicaStatoLavoratore")
                            join dom in context.CigoTelematicaDomande on lav.IdDomandaCigo equals dom.Id

                            join lavrap in context.RapportiLavoratoreImpresaCompleti on
                            new
                            {
                                IdImpresa = dom.IdImpresa,
                                IdLavoratore = lav.IdLavoratore

                            } equals new
                            {
                                IdImpresa = lavrap.idImpresa,
                                IdLavoratore = lavrap.idLavoratore
                            }

                            join rip in context.RapportiImpresaPersona on
                    lav.IdLavoratore equals rip.IdLavoratore
                    into rapporto
                            from rip1 in
                                (from rip in rapporto
                                 where rip.IdImpresa == dom.IdImpresa &&
                                       dom.DataRichiesta >= rip.DataInizioValiditaRapporto &&
                                       dom.DataRichiesta < rip.DataFineValiditaRapporto
                                 select rip).DefaultIfEmpty()
                            join tc in context.TipiCategoria on
                                rip1.IdCategoria equals tc.Id
                                into cat
                            from tc in cat.DefaultIfEmpty()

                            where
                                        dom.Id == idDomanda
                                        && lavrap.dataDenuncia.Year == dom.Anno && lavrap.dataDenuncia.Month == dom.Mese
                            select new LavoratoreDettagli
                            {
                                IdDomanda = dom.Id,
                                IdStatoDomanda = dom.IdStato,
                                IdLavoratore = lav.Lavoratore.Id,
                                Cognome = lav.Lavoratore.Cognome,
                                Nome = lav.Lavoratore.Nome,
                                CodiceFiscale = lav.Lavoratore.CodiceFiscale,
                                IdStato = lav.IdStato,
                                Stato = lav.CigoTelematicaStatoLavoratore.Descrizione,
                                PercentualePT = rip1.PartTimePercentuale,
                                Categoria = tc.Descrizione,
                                PagaOrariaDenuncia = lavrap.pagaOrariaDichiarata,
                                PagaOrariaDichiarata = lav.PagaOrariaDichiarata,
                                OreRichieste = lav.OreRichieste,
                                IdImpresa = dom.IdImpresa,
                                Mese = dom.Mese.Value,
                                Anno = dom.Anno.Value,
                                DataInizio = rip1.DataInizioValiditaRapporto,
                                DataFine = rip1.DataFineValiditaRapporto,
                                DataAssunzione = rip1.DataAssunzione,
                                IdTipoOra = lav.IdTipoOreDenuncia,
                                DescrizioneTipoOra = lav.TipiOra.Descrizione,
                            };

                lavoratori = query.ToList();

                if (eseguiControlli)
                {
                    foreach (var lavoratore in lavoratori)
                    {

                        EseguiControlliLavoratore(idDomanda, lavoratore, context);

                    }

                }
            }

            return lavoratori;

        }


        // recupera i dati per il dettaglio lavoratore in domanda
        public LavoratoreDettagli GetLavoratoreInDomanda(int idDomanda, int idLavoratore, Boolean eseguiControlli = false)
        {
            LavoratoreDettagli lavoratore;
            using (SICEEntities context = new SICEEntities())
            {
                var query = from lav in context.CigoTelematicaLavoratori.Include("CigoTelematicaLavoratoriGiorni")
                                .Include("Lavoratore").Include("CigoTelematicaDomanda").Include("CigoTelematicaStatoLavoratore")
                            join dom in context.CigoTelematicaDomande on lav.IdDomandaCigo equals dom.Id

                            join lavrap in context.RapportiLavoratoreImpresaCompleti on 
                            new {
                                IdImpresa = dom.IdImpresa,
                                IdLavoratore = lav.IdLavoratore

                            } equals new {
                                IdImpresa = lavrap.idImpresa,
                                IdLavoratore = lavrap.idLavoratore
                            }

                            join rip in context.RapportiImpresaPersona on
                    lav.IdLavoratore equals rip.IdLavoratore 
                    into rapporto
                            from rip1 in
                                (from rip in rapporto
                                 where rip.IdImpresa == dom.IdImpresa &&
                                       dom.DataRichiesta >= rip.DataInizioValiditaRapporto &&
                                       dom.DataRichiesta < rip.DataFineValiditaRapporto
                                 select rip).DefaultIfEmpty()
                            join tc in context.TipiCategoria on
                                rip1.IdCategoria equals tc.Id
                                into cat
                            from tc in cat.DefaultIfEmpty()

                            where
                                        dom.Id == idDomanda
                                        && lav.IdLavoratore == idLavoratore
                                        && lavrap.dataDenuncia.Year == dom.Anno && lavrap.dataDenuncia.Month == dom.Mese
                            select new LavoratoreDettagli
                            {
                                IdDomanda = dom.Id,
                                IdStatoDomanda = dom.IdStato,
                                IdLavoratore = lav.Lavoratore.Id,
                                Cognome = lav.Lavoratore.Cognome,
                                Nome = lav.Lavoratore.Nome,
                                CodiceFiscale = lav.Lavoratore.CodiceFiscale,
                                IdStato = lav.IdStato,
                                Stato = lav.CigoTelematicaStatoLavoratore.Descrizione,
                                PercentualePT = rip1.PartTimePercentuale,
                                Categoria = tc.Descrizione,
                                PagaOrariaDenuncia = lavrap.pagaOrariaDichiarata,
                                PagaOrariaDichiarata = lav.PagaOrariaDichiarata,
                                OreRichieste = lav.OreRichieste,
                                IdImpresa = dom.IdImpresa,
                                Mese = dom.Mese.Value,
                                Anno = dom.Anno.Value,
                                DataInizio = rip1.DataInizioValiditaRapporto,
                                DataFine = rip1.DataFineValiditaRapporto,
                                DataAssunzione = rip1.DataAssunzione,
                            };

                lavoratore = query.FirstOrDefault();

                if (eseguiControlli)
                {
                    EseguiControlliLavoratore(idDomanda, lavoratore, context);
                    
                }
            }

            return lavoratore;

        }

        public CigoTelematicaLavoratore UpdateLavoratoreCigo(int idDomanda, int idLavoratore, GiornoCigoCollection lavoratoreGiorni, int idUtente, decimal pagaOraria, string stato, string oreRiconosciute)
        {
            CigoTelematicaLavoratore lavoratore;

            using (SICEEntities context = new SICEEntities())
            {
                CigoTelematicaDomanda domanda = context.CigoTelematicaDomande.First(x => x.Id == idDomanda);
                
                Int32 oreRic;

                lavoratore = context.CigoTelematicaLavoratori.First(x => x.IdLavoratore == idLavoratore && x.IdDomandaCigo == idDomanda);
                lavoratore.PagaOrariaDichiarata = pagaOraria;
                
                lavoratore.DataUltimoAggiornamento = DateTime.Now;
                
                lavoratore.IdStato = stato;
                //NM ??????????????????????????????????????????
                if (oreRiconosciute != "")
                {
                    oreRic = Convert.ToInt32(oreRiconosciute);
                }
                else 
                {
                    oreRic = 0;
                }
                lavoratore.OreRiconosciute = oreRic;

                foreach (var item in lavoratoreGiorni)
                {
                    var giorno = context.CigoTelematicaLavoratoriGiorni.FirstOrDefault(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == idLavoratore && x.DataAssenza.Day == item.Giorno);
                    if(item.Selected && giorno == null)
                    {
                        context.CigoTelematicaLavoratoriGiorni.AddObject(new CigoTelematicaLavoratoreGiorno
                                                                                {
                                                                                    IdDomandaCigo = idDomanda,
                                                                                    IdLavoratore = idLavoratore,
                                                                                    IdUtenteInserimento = idUtente,
                                                                                    DataInserimentoRecord = DateTime.Now,
                                                                                    DataAssenza = new DateTime(domanda.Anno.Value, domanda.Mese.Value, item.Giorno),

                                                                                });
                    }
                    else if (!item.Selected && giorno != null)
                    {                 
                        context.CigoTelematicaLavoratoriGiorni.DeleteObject(giorno);
                    }
                }

                context.SaveChanges();

                lavoratore = context.CigoTelematicaLavoratori.Include("CigoTelematicaStatoLavoratore").Include("CigoTelematicaDomanda").First(x => x.IdLavoratore == idLavoratore && x.IdDomandaCigo == idDomanda);

            }

            return lavoratore;
        }



        public CigoTelematicaDomanda ConfermaDomandaCigo(int idDomanda, DateTime? dataRilascio, string numeroAutorizzazione)
        {
            CigoTelematicaDomanda domanda;
            using (SICEEntities context = new SICEEntities())
            {
                domanda = context.CigoTelematicaDomande.First(x => x.Id == idDomanda);
                if (dataRilascio != null)
                {
                    domanda.DataRilascioAutorizzazione = dataRilascio;
                }
                domanda.NumeroAutorizzazione = numeroAutorizzazione;
                domanda.IdStato = "T"; //immessa pervenuta (domanda inviata dall'impresa a Cassa Edile)
                domanda.DataImmessaPervenuta = DateTime.Now;
                domanda.DataUltimoAggiornamento = DateTime.Now;
                context.SaveChanges();
                domanda = context.CigoTelematicaDomande.Include("TipoStatoPrestazione").First(x => x.Id == idDomanda);   
            }
            return domanda;
        }


        //salva sul db le note inserite dal backoffice
        public void ConfermaNote(int idDomanda, string note)
        {
            using (SICEEntities context = new SICEEntities())
            {
                CigoTelematicaDomanda domanda = context.CigoTelematicaDomande.First(x => x.Id == idDomanda);
                domanda.Note = note;
                context.SaveChanges();

            }
        }
        

        //recupera descrizione utente in carico
        public String DescrizioneUtenteInCarico(int idDomanda, int idUtente)
        {
            String ret = "";

            using (SICEEntities context = new SICEEntities())
            {
                var query = from dom in context.CigoTelematicaDomande
                            join ut in context.Utenti on dom.IdUtenteInCarico equals ut.Id
                            where
                                dom.Id == idDomanda
                                &&
                                ut.Id == idUtente
                                                                                                                          
                            select ut.Username;

                if (query.Count() > 0)
                    ret = query.FirstOrDefault();
            }

            return ret;
        }


        //aggiorna utente in carico sulla domanda
        public void UpdateCigoTelematicaDomandaUtenteInCarico(Int32 idDomanda, Int32? idUtente)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from domanda in context.CigoTelematicaDomande
                            where domanda.Id == idDomanda
                            select domanda;

                CigoTelematicaDomanda domandaU = query.Single();
                domandaU.IdUtenteInCarico = idUtente;
                domandaU.DataUltimoAggiornamento = DateTime.Now;

                context.SaveChanges();
            }
        }


        #region documenti inps
        public void InsertDocumentoInps(CigoTelematicaDocumentoInps documentoInps)
        {
           using (SICEEntities context = new SICEEntities())
            {
                context.CigoTelematicaDocumentiInps.AddObject(documentoInps);

                context.SaveChanges();
            }
        }

                
        public List<CigoTelematicaDocumentoInps> GetDocumentiInps(Int32 idDomanda)
        {
            List<CigoTelematicaDocumentoInps> ret = new List<CigoTelematicaDocumentoInps>();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from docinps in context.CigoTelematicaDocumentiInps
                            where
                                docinps.IdDomandaCigo == idDomanda
                            select docinps;
                
                ret = query.ToList();
            }

            return ret;
        }


        public Immagine GetImmagineDocumentoInps(Int32 idDocumentoInps)
        {
            Immagine ret = new Immagine();

            
            using (SICEEntities context = new SICEEntities())
            {
                var query = from docinps in context.CigoTelematicaDocumentiInps
                            where
                                docinps.Id == idDocumentoInps
                            select new Immagine { File = docinps.Immagine, NomeFile = docinps.NomeFile, IdArchidoc = docinps.IdArchidoc };

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public void DeleteDocumentoInps(Int32 idDocumentoInps)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from docinps in context.CigoTelematicaDocumentiInps
                            where docinps.Id == idDocumentoInps
                            select docinps;

                context.CigoTelematicaDocumentiInps.DeleteObject(query.Single());
                context.SaveChanges();
            }
        }
        #endregion


        #region busta paga

        public void InsertBustaPaga(CigoTelematicaBustaPaga bustaPaga)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.CigoTelematicaBustePaga.AddObject(bustaPaga);

                context.SaveChanges();
            }
        }


        public List<CigoTelematicaBustaPaga> GetBustaPaga(Int32 idDomanda, Int32 idLavoratore)
        {
            List<CigoTelematicaBustaPaga> ret = new List<CigoTelematicaBustaPaga>();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from busta in context.CigoTelematicaBustePaga
                            where
                                busta.IdDomandaCigo == idDomanda
                                && busta.IdLavoratore == idLavoratore
                            select busta;

                ret = query.ToList();
            }

            return ret;
        }


        public Immagine GetImmagineBustaPaga(Int32 idBustaPaga)
        {
            Immagine ret = new Immagine();


            using (SICEEntities context = new SICEEntities())
            {
                var query = from busta in context.CigoTelematicaBustePaga
                            where
                                busta.Id == idBustaPaga
                            select new Immagine { File = busta.Immagine, NomeFile = busta.NomeFile, IdArchidoc = busta.IdArchidoc };

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public void DeleteBustaPaga(Int32 idBustaPaga)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from busta in context.CigoTelematicaBustePaga
                            where busta.Id == idBustaPaga
                            select busta;

                context.CigoTelematicaBustePaga.DeleteObject(query.Single());
                context.SaveChanges();
            }
        }

        #endregion




        public CigoTelematicaDomanda AnnullaDomanda(CigoTelematicaDomanda domanda)
        {
            CigoTelematicaDomanda currentDomanda;
            using (SICEEntities context = new SICEEntities())
            {
                currentDomanda = context.CigoTelematicaDomande.First(x => x.Id == domanda.Id);
                currentDomanda.IdStato = "A"; //annullata 
                currentDomanda.DataUltimoAggiornamento = DateTime.Now;
                currentDomanda.DataRilascioAutorizzazione = domanda.DataRilascioAutorizzazione;
                currentDomanda.NumeroAutorizzazione = domanda.NumeroAutorizzazione;
                currentDomanda.Note = domanda.Note;
                context.SaveChanges();
                currentDomanda = context.CigoTelematicaDomande.Include("TipoStatoPrestazione").First(x => x.Id == domanda.Id);
            }
            return currentDomanda;
        }



        public CigoTelematicaDomanda InAttesaDomanda(CigoTelematicaDomanda domanda)
        {
            CigoTelematicaDomanda currentDomanda;
            using (SICEEntities context = new SICEEntities())
            {
                currentDomanda = context.CigoTelematicaDomande.First(x => x.Id == domanda.Id);
                currentDomanda.IdStato = "O"; //in attesa 
                currentDomanda.DataUltimoAggiornamento = DateTime.Now;
                currentDomanda.DataInAttesa = DateTime.Now;
                currentDomanda.DataRilascioAutorizzazione = domanda.DataRilascioAutorizzazione;
                currentDomanda.NumeroAutorizzazione = domanda.NumeroAutorizzazione;
                currentDomanda.Note = domanda.Note;
                context.SaveChanges();
                currentDomanda = context.CigoTelematicaDomande.Include("TipoStatoPrestazione").First(x => x.Id == domanda.Id);

            }

            return currentDomanda;
        }




        public CigoTelematicaDomanda RiportaImmessaPervenuta(CigoTelematicaDomanda domanda)
        {
            CigoTelematicaDomanda currentDomanda;
            using (SICEEntities context = new SICEEntities())
            {
                currentDomanda = context.CigoTelematicaDomande.First(x => x.Id == domanda.Id);
                currentDomanda.IdStato = "T"; //riporta in Immessa pervenuta 
                currentDomanda.DataUltimoAggiornamento = DateTime.Now;
                currentDomanda.DataImmessaPervenuta = DateTime.Now;
                currentDomanda.DataRilascioAutorizzazione = currentDomanda.DataRilascioAutorizzazione;
                currentDomanda.NumeroAutorizzazione = currentDomanda.NumeroAutorizzazione;
                currentDomanda.Note = currentDomanda.Note;
                context.SaveChanges();
                currentDomanda = context.CigoTelematicaDomande.Include("TipoStatoPrestazione").First(x => x.Id == domanda.Id);

            }
            return currentDomanda;
        }

        public CigoTelematicaDomanda RespingiDomanda(CigoTelematicaDomanda domanda)
        {
            CigoTelematicaDomanda currentDomanda;
            using (SICEEntities context = new SICEEntities())
            {
                currentDomanda = context.CigoTelematicaDomande.First(x => x.Id == domanda.Id);
                currentDomanda.IdStato = "R"; //respinta
                currentDomanda.DataUltimoAggiornamento = DateTime.Now;
                currentDomanda.DataRilascioAutorizzazione = currentDomanda.DataRilascioAutorizzazione;
                currentDomanda.NumeroAutorizzazione = currentDomanda.NumeroAutorizzazione;
                currentDomanda.Note = currentDomanda.Note;
                context.SaveChanges();
                currentDomanda = context.CigoTelematicaDomande.Include("TipoStatoPrestazione").First(x => x.Id == domanda.Id);
            }
            return currentDomanda;
        }


        public CigoTelematicaDomanda AccogliDomanda(CigoTelematicaDomanda domanda)
        {
            CigoTelematicaDomanda currentDomanda;
            using (SICEEntities context = new SICEEntities())
            {
                currentDomanda = context.CigoTelematicaDomande.First(x => x.Id == domanda.Id);
                currentDomanda.IdStato = "C"; //accolta
                currentDomanda.DataUltimoAggiornamento = DateTime.Now;
                currentDomanda.DataRilascioAutorizzazione = currentDomanda.DataRilascioAutorizzazione;
                currentDomanda.NumeroAutorizzazione = currentDomanda.NumeroAutorizzazione;
                currentDomanda.Note = currentDomanda.Note;
                context.SaveChanges();
                currentDomanda = context.CigoTelematicaDomande.Include("TipoStatoPrestazione").First(x => x.Id == domanda.Id);
            }
            return currentDomanda;
        }


        #region Private

        private static void EseguiControlliLavoratore(int idDomanda, LavoratoreDettagli lavoratore, SICEEntities context)
        {
            lavoratore.ControlliValidita = new ControlliLavoratore();
            var queryOre = from dom in context.OreDenunciateCompleteImprese
                           where
                           dom.Data.Month == lavoratore.Mese
                           && dom.Data.Year == lavoratore.Anno
                           && dom.IdImpresa == lavoratore.IdImpresa
                           && dom.IdLavoratore == lavoratore.IdLavoratore
                           && dom.IdTipoOra == "027"

                           select dom;

            OreDenunciateCompleteImpresa ore = queryOre.FirstOrDefault();


            Int32 giorniMeseDomanda = context.CigoTelematicaLavoratoriGiorni.Include("CigoTelematicaDomanda")
                                                         .Where(x => x.CigoTelematicaLavoratore.CigoTelematicaDomanda.Anno == lavoratore.Anno
                                                                && x.CigoTelematicaLavoratore.CigoTelematicaDomanda.Mese == lavoratore.Mese
                                                                && x.CigoTelematicaLavoratore.IdLavoratore == lavoratore.IdLavoratore
                                                                && x.CigoTelematicaLavoratore.IdStato != "R"
                                                                && x.CigoTelematicaLavoratore.CigoTelematicaDomanda.IdImpresa == lavoratore.IdImpresa).Count();

            //Int32 giorniAnnoDomanda = context.CigoTelematicaLavoratoriGiorni.Where(x => x.CigoTelematicaLavoratore.CigoTelematicaDomanda.anno == lavoratore.Anno
            //                                                    && x.CigoTelematicaLavoratore.IdLavoratore == lavoratore.IdLavoratore
            //                                                    && x.CigoTelematicaLavoratore.CigoTelematicaDomanda.IdImpresa == lavoratore.IdImpresa
            //                                                    && (x.CigoTelematicaLavoratore.CigoTelematicaDomanda.idStato == "C"
            //                                                        || x.CigoTelematicaLavoratore.CigoTelematicaDomanda.idStato == "D"
            //                                                        || x.CigoTelematicaLavoratore.CigoTelematicaDomanda.idStato == "L")
            //                                                    ).Count();
            Int32 oreApprovateAnnue = context.CigoTelematicaLavoratori.Include("CigoTelematicaDomanda")
                                                                    .Where(x => (x.IdLavoratore == lavoratore.IdLavoratore && x.IdStato != "R") && x.CigoTelematicaDomanda.IdImpresa == lavoratore.IdImpresa
                                                                                  && (x.CigoTelematicaDomanda.IdStato == "C"
                                                                                      || x.CigoTelematicaDomanda.IdStato == "D"
                                                                                      || x.CigoTelematicaDomanda.IdStato == "L"
                                                                                      || x.CigoTelematicaDomanda.IdStato == "T")).Select(x => x.OreRiconosciute ?? 0).DefaultIfEmpty(0).Sum();
            Int32 giornDomanda = context.CigoTelematicaLavoratoriGiorni.Where(x => x.IdDomandaCigo == lavoratore.IdDomanda && x.IdLavoratore == lavoratore.IdLavoratore).Count();

            //Nmanelli (27012015): recupero le ore riconosciute (sono quelle della domanda in esame)
            //Int32 oreRiconosciute = context.CigoTelematicaLavoratori.Where(x => x.IdDomandaCigo == lavoratore.IdDomanda && x.IdLavoratore == lavoratore.IdLavoratore).Select(x => x.OreRiconosciute ?? 0).DefaultIfEmpty(0).FirstOrDefault();

            //Nmanelli (23022015): recupero le ore riconosciute (sono quelle di tutte le domande per impresa, lavoratore del mese e anno in 
            //                     stato C D L T)
            Int32 oreMensiliRiconosciute = context.CigoTelematicaLavoratori.Include("CigoTelematicaDomanda")
                                                                    .Where(x => (x.IdLavoratore == lavoratore.IdLavoratore && x.IdStato != "R") && x.CigoTelematicaDomanda.IdImpresa == lavoratore.IdImpresa
                                                                                  && (x.CigoTelematicaDomanda.IdStato == "C"
                                                                                      || x.CigoTelematicaDomanda.IdStato == "D"
                                                                                      || x.CigoTelematicaDomanda.IdStato == "L"
                                                                                      || x.CigoTelematicaDomanda.IdStato == "T")
                                                                                      && x.CigoTelematicaDomanda.Anno == lavoratore.Anno
                                                                                      && x.CigoTelematicaDomanda.Mese == lavoratore.Mese)
                                                                                      .Select(x => x.OreRiconosciute ?? 0).DefaultIfEmpty(0).Sum();

            //Controlli eseguibili anche senza rapporto di lavoro
            lavoratore.ControlliValidita.LavoratoreInDenuncia = ore != null;
            lavoratore.ControlliValidita.PagaOrariaValida = lavoratore.PagaOrariaDenuncia == lavoratore.PagaOrariaDichiarata;
            lavoratore.ControlliValidita.MonteOreAnnuoValido = oreApprovateAnnue <= 150;
            lavoratore.ControlliValidita.TotaleOreAnnoRiconosciute = oreApprovateAnnue;
            
            if (String.IsNullOrEmpty(lavoratore.RapportoLavoro))
            {
                lavoratore.ControlliValidita.RapportoDiLavoroValido = false;
            }
            else
            {
                DateTime? primoGiornoCigo = null;
                DateTime? ultimoGiornoCigo = null;
                if (giornDomanda > 0)
                {
                    primoGiornoCigo = context.CigoTelematicaLavoratoriGiorni.Where(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == lavoratore.IdLavoratore).Min(x => x.DataAssenza);
                    ultimoGiornoCigo = context.CigoTelematicaLavoratoriGiorni.Where(x => x.IdDomandaCigo == idDomanda && x.IdLavoratore == lavoratore.IdLavoratore).Max(x => x.DataAssenza);
                }

                //se non vengono segnati giorni cigo nelle caselline il controllo torna false e mette il semaforo rosso
                lavoratore.ControlliValidita.RapportoDiLavoroValido = !lavoratore.DataAssunzione.HasValue ? false :
                    (primoGiornoCigo >= lavoratore.DataInizio && primoGiornoCigo <= lavoratore.DataFine &&
                     ultimoGiornoCigo >= lavoratore.DataInizio && ultimoGiornoCigo <= lavoratore.DataFine);

                Decimal oreGiornaliere = (lavoratore.PercentualePT.Value == 0 ? 8 : (lavoratore.PercentualePT.Value * 8 / 100));

                //NManelli (27012015): il controllo sul monte ore mensile va fatto sulle ore riconosciute e non richieste - cambio
                //lavoratore.ControlliValidita.MonteOreMensileValido = oreGiornaliere * giorniMeseDomanda <= lavoratore.OreRichieste;
                //lavoratore.ControlliValidita.MonteOreMensileValido = oreGiornaliere * giorniMeseDomanda <= oreRiconosciute;
                //NManelli (23022015): il controllo sul monte ore mensile va fatto sulle ore riconosciute e non richieste per tutti i cantieri inseriti per mese e anno
                lavoratore.ControlliValidita.MonteOreMensileValido = oreGiornaliere * giorniMeseDomanda <= oreMensiliRiconosciute;
                
                //lavoratore.ControlliValidita.MonteOreAnnuoValido = oreGiornaliere * giorniAnnoDomanda <= 150;
                                
                lavoratore.ControlliValidita.OreCigoValide = oreGiornaliere * giornDomanda <= lavoratore.OreRichieste;
                lavoratore.ControlliValidita.LavoratoreApprendista = String.Equals(lavoratore.Categoria, "apprendista", StringComparison.CurrentCultureIgnoreCase);
                lavoratore.ControlliValidita.TotaleOreMese = (int)oreGiornaliere * giorniMeseDomanda;

            }

          
        }
        #endregion

    }

}
