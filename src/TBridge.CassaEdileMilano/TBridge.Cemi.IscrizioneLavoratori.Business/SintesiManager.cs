using System;
using System.IO;
using System.Net;
using System.Xml.Serialization;
//using TBridge.Cemi.Business.SintesiServiceReference;
using TBridge.Cemi.IscrizioneLavoratori.Data;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.Sintesi.Business;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Sintesi.Business.ServiceReferenceProvinciaSintesi;
using System.Configuration;

namespace TBridge.Cemi.IscrizioneLavoratori.Business
{
    public class SintesiManager
    {
        private readonly SintesiDataAccess _dataAccess = new SintesiDataAccess();
        
        //public RapportoLavoro GetRapportoLavoro(String idComunicazione, String username, String password)
        //{
        //    RapportoLavoro rapportoLavoro = null;

            //try
            //{
            //    getXmlMinSoapClient client;

            //    string identificativoProvincia = idComunicazione.Substring(0, 5);
            //    switch (identificativoProvincia)
            //    {
            //        case "10015":
            //            //Milano
            //            client = new getXmlMinSoapClient("getXmlMinSoapMi");
            //            break;
            //        case "10108":
            //            //Monza-Brianza
            //            client = new getXmlMinSoapClient("getXmlMinSoapMb");
            //            break;
            //        case "10098":
            //            //Lodi
            //            client = new getXmlMinSoapClient("getXmlMinSoapLo");
            //            break;
            //        default:
            //            //Milano
            //            client = new getXmlMinSoapClient("getXmlMinSoapMi");
            //            break;
            //    }

            //    string xmlRisposta = client.getXmlMinFromCodAuth(idComunicazione, username, password);

            //    //GetXmlMin proxy = new GetXmlMin
            //    //                      {
            //    //                          Credentials = new NetworkCredential(username, password)
            //    //                      };

            //    //string xmlRisposta = proxy.getXmlMinFromIdAuth(idComunicazione, username, password);

            //    StringReader source = new StringReader(xmlRisposta);
            //    XmlSerializer serializer = new XmlSerializer(typeof (RapportoLavoro));
            //    rapportoLavoro = (RapportoLavoro) serializer.Deserialize(source);
            //}
            //catch (Exception)
            //{
            //    //
            //}

        //    return rapportoLavoro;
        //}


        //public SintesiServiceResult GetRapportoLavoro(String idComunicazione, String username, String password, out RapportoLavoro rapportoLavoro)
        //{
        //    string xmlRisposta = null;

        //    rapportoLavoro = null;

            //if (String.IsNullOrEmpty(idComunicazione))
            //{
            //    return new SintesiServiceResult { Error = Type.Enums.SintesiServiceErrors.InvalidId };
            //}

            //try
            //{
            //    getXmlMinSoapClient client;

            //    string identificativoProvincia = idComunicazione.Substring(0, 5);
            //    switch (identificativoProvincia)
            //    {
            //        case "10015":
            //            //Milano
            //            client = new getXmlMinSoapClient("getXmlMinSoapMi");
            //            break;
            //        case "10108":
            //            //Monza-Brianza
            //            client = new getXmlMinSoapClient("getXmlMinSoapMb");
            //            break;
            //        case "10098":
            //            //Lodi
            //            client = new getXmlMinSoapClient("getXmlMinSoapLo");
            //            break;
            //        default:
            //            //Milano
            //            client = new getXmlMinSoapClient("getXmlMinSoapMi");
            //            break;
            //    }

            //    xmlRisposta = client.getXmlMinFromCodAuth(idComunicazione, username, password);
            //}
            //catch (Exception)
            //{
            //    return new SintesiServiceResult { Error = Type.Enums.SintesiServiceErrors.ServiceCallFailed };
            //}

            //if (IsXml(xmlRisposta))
            //{
            //    SintesiServiceResult result = new SintesiServiceResult();
            //    try
            //    {
            //        StringReader source = new StringReader(xmlRisposta);
            //        XmlSerializer serializer = new XmlSerializer(typeof(RapportoLavoro));
            //        rapportoLavoro = (RapportoLavoro)serializer.Deserialize(source);

            //        result.Error = Type.Enums.SintesiServiceErrors.NoError;
            //    }
            //    catch (Exception)
            //    {

            //        result.Error = Type.Enums.SintesiServiceErrors.InvalidResponse;
            //    }

            //    return result;
            //}
            //else
            //{
            //    return new SintesiServiceResult { Error = Type.Enums.SintesiServiceErrors.ServiceErrorMessage, Message = xmlRisposta };

            //}


        //}

        public RapportoLavoro GetRapportoLavoro(String idComunicazione)
        {
            RapportoLavoro rapportoLavoro = null;

            try
            {

                string xmlRisposta = SintesiWsGetXmlRisposta(idComunicazione);

                //GetXmlMin proxy = new GetXmlMin
                //                      {
                //                          Credentials = new NetworkCredential(username, password)
                //                      };

                //string xmlRisposta = proxy.getXmlMinFromIdAuth(idComunicazione, username, password);

                StringReader source = new StringReader(xmlRisposta);
                XmlSerializer serializer = new XmlSerializer(typeof(RapportoLavoro));
                rapportoLavoro = (RapportoLavoro)serializer.Deserialize(source);
            }
            catch (Exception)
            {
                //
            }

            return rapportoLavoro;
        }


        public SintesiServiceResult GetRapportoLavoro(String idComunicazione, out RapportoLavoro rapportoLavoro)
        {
            String xmlRisposta = null;
            
            rapportoLavoro = null;

            if (String.IsNullOrEmpty(idComunicazione))
            {
                return new SintesiServiceResult { Error = Type.Enums.SintesiServiceErrors.InvalidId };
            }

            try
            {
                xmlRisposta = SintesiWsGetXmlRisposta(idComunicazione);
            }
            catch (Exception exc)
            {
                return new SintesiServiceResult { Error = Type.Enums.SintesiServiceErrors.ServiceCallFailed };
            }

            if (IsXml(xmlRisposta))
            {
                SintesiServiceResult result = new SintesiServiceResult();
                try
                {
                    StringReader source = new StringReader(xmlRisposta);
                    XmlSerializer serializer = new XmlSerializer(typeof(RapportoLavoro));
                    rapportoLavoro = (RapportoLavoro)serializer.Deserialize(source);

                    result.Error = Type.Enums.SintesiServiceErrors.NoError;
                }
                catch (Exception)
                {

                    result.Error = Type.Enums.SintesiServiceErrors.InvalidResponse;
                }

                return result;
            }
            else
            {
                return new SintesiServiceResult { Error = Type.Enums.SintesiServiceErrors.ServiceErrorMessage, Message = xmlRisposta };

            }

        }

        private static String SintesiWsGetXmlRisposta(String idComunicazione)
        {
            String xmlRisposta;
            String password = null;
            String username = null;
            String wsEndpoint = null;
            String identificativoProvincia = idComunicazione.Substring(0, 5);
            switch (identificativoProvincia)
            {
                case "10015":
                    //Milano
                    wsEndpoint = "getXmlMinSoapMi";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordMilano"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameMilano"];
                    break;
                case "10108":
                    //Monza-Brianza
                    wsEndpoint = "getXmlMinSoapMb";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordMonzaBrianza"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameMonzaBrianza"];
                    break;
                case "10098":
                    //Lodi
                    wsEndpoint = "getXmlMinSoapLo";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordLodi"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameLodi"];
                    break;
                default:
                    //Milano
                    wsEndpoint = "getXmlMinSoapMi";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordMilano"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameMilano"];
                    break;
            }

            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            using (getXmlMinSoapClient client = new getXmlMinSoapClient(wsEndpoint))
            {
                xmlRisposta = client.getXmlMinFromCodAuth(idComunicazione, username, password);
            }
                

            return xmlRisposta;
        }

        public void ImpostaTipoContratto(RapportoDiLavoro rapportoLavoro, TBridge.Cemi.Type.Entities.TipoContratto tipoContratto, int idImpresa)
        {
            IscrizioneLavoratoriDataAccess iscrizioneLavoratoriDataAccess = new IscrizioneLavoratoriDataAccess();
            IscrizioneLavoratori.Type.Entities.Impresa impresa = iscrizioneLavoratoriDataAccess.GetImpresa(idImpresa);

            if (tipoContratto.IdContratto != impresa.Contratto.IdContratto)
            {
                rapportoLavoro.Contratto = impresa.Contratto;
                rapportoLavoro.ContrattoOriginaleSintesi = tipoContratto;
            }
            else
            {
                rapportoLavoro.Contratto = tipoContratto;
            }
        }

        public TipoFineRapporto GetTipoFineRapporto(string codiceCessazione)
        {
            return _dataAccess.GetTipoFineRapporto(codiceCessazione);
        }

        public Lavoratore GetLavoratore(string codiceFiscale, int idImpresa)
        {
            return _dataAccess.GetLavoratore(codiceFiscale, idImpresa);
        }

        public RapportoDiLavoro GetRapportoLavoro(int? idLavoratore, int idImpresa)
        {
            return _dataAccess.GetRapportoLavoro(idLavoratore, idImpresa);
        }

        public int SalvaDatiComunicazione(Guid? guid, string codiceComunicazione, string codiceComunicazionePrec, int idDichiarazione)
        {
            return _dataAccess.SalvaDatiComunicazione(guid,codiceComunicazione,codiceComunicazionePrec,idDichiarazione);
        }

        public Boolean DichiarazioneExistByIdComunicazioneSintesi(String idComunicazione)
        {
            return _dataAccess.GetIdDichiarazioneByIdComunicazoneSintesi(idComunicazione).HasValue;
        }

        public Boolean IsImpresaCompetenzaConsulente(Int32 idConsulente, String impresaPartitaIvaCodiceFiscale)
        {
            return _dataAccess.IsImpresaCompetenzaConsulente(idConsulente, impresaPartitaIvaCodiceFiscale);
        }

        public Boolean IsDataAssunzioneValida(RapportoLavoro rapportoLavoro)
        {
            Boolean result = true;
            if (rapportoLavoro.Item is RapportoLavoroInizioRapporto)
            {
                RapportoLavoroInizioRapporto assunzione = (RapportoLavoroInizioRapporto)rapportoLavoro.Item;

                DateTime dataLimiteTmp = assunzione.dataInizio.AddMonths(1);
                //giorno 5 del mese successivo 
                DateTime dataLimite = new DateTime(dataLimiteTmp.Year, dataLimiteTmp.Month, 5);
                if (DateTime.Now.Date > dataLimite)
                {
                    result = false;
                }
            }
            return result;
        }

        private Boolean IsXml(String message)
        {
            //if (string.IsNullOrEmpty(message))
            //{
            //    return false;
            //}

            if (!string.IsNullOrEmpty(message) &&  message[0] == '<')
            {
                try
                {
                   System.Xml.Linq.XElement.Parse(message);
                   return true;
                }
                catch (System.Xml.XmlException)
                {
                    return false;
                }
            }

            return false;
        }
    }
}