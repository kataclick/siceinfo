using System;
using System.Collections.Generic;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.IscrizioneLavoratori.Data;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;

namespace TBridge.Cemi.IscrizioneLavoratori.Business
{
    public class IscrizioneLavoratoriManager
    {
        #region Costanti

        public const string URLSEMAFOROGIALLO = "~/images/semaforoGiallo.png";
        public const string URLSEMAFOROROSSO = "~/images/semaforoRosso.png";
        public const string URLSEMAFOROVERDE = "~/images/semaforoVerde.png";

        #endregion

        private readonly IscrizioneLavoratoriDataAccess dataAccess = new IscrizioneLavoratoriDataAccess();

        public DichiarazioneCollection GetDichiarazioni(DichiarazioneFilter filtro)
        {
            return dataAccess.GetDichiarazioni(filtro);
        }

        public LavoratoreCollection GetLavoratori(LavoratoreFilter filtro, Int32 idImpresa)
        {
            return dataAccess.GetLavoratori(filtro, idImpresa);
        }

        public Lavoratore GetLavoratore(Int32 idLavoratore)
        {
            return dataAccess.GetLavoratore(idLavoratore);
        }

        public Impresa GetImpresa(Int32 idImpresa)
        {
            return dataAccess.GetImpresa(idImpresa);
        }

        public Impresa GetImpresa(String codiceFiscalePartitaIva)
        {
            return dataAccess.GetImpresa(codiceFiscalePartitaIva);
        }

        public Boolean InsertDichiarazione(Dichiarazione dichiarazione)
        {
            return dataAccess.InsertDichiarazione(dichiarazione);
        }

        public RapportoDiLavoro GetRapportoDiLavoro(Int32 idLavoratore, Int32 idImpresa)
        {
            return dataAccess.GetRapportoDiLavoro(idLavoratore, idImpresa);
        }

        public RapportoDiLavoroCollection GetRapportiDiLavoroAperti(Int32 idLavoratore,
                                                                    DateTime dataInizioRapportoDiLavoro)
        {
            return dataAccess.GetRapportiDiLavoroAperti(idLavoratore, dataInizioRapportoDiLavoro);
        }

        public RapportoDiLavoroCollection GetDichiarazioniAperte(Int32 idDichiarazioneCorrente, String codiceFiscale,
                                                                 DateTime dataInizioRapportoDiLavoro)
        {
            return dataAccess.GetDichiarazioniAperte(idDichiarazioneCorrente, codiceFiscale, dataInizioRapportoDiLavoro);
        }

        public Dichiarazione GetDichiarazione(Int32 idDichiarazione)
        {
            return dataAccess.GetDichiarazione(idDichiarazione);
        }

        public LavoratoreCollection GetLavoratoriConStessoCodiceFiscale(String codiceFiscale)
        {
            return dataAccess.GetLavoratoriConStessoCodiceFiscale(codiceFiscale);
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagrafici(
            String cognome,
            String nome,
            Char sesso,
            DateTime dataNascita,
            String luogoNascita)
        {
            return dataAccess.GetLavoratoriConStessiDatiAnagrafici(
                cognome,
                nome,
                sesso,
                dataNascita,
                luogoNascita);
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
            String cognome,
            String nome,
            Char sesso,
            DateTime dataNascita,
            String luogoNascita,
            String codiceFiscale)
        {
            return dataAccess.GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
                cognome,
                nome,
                sesso,
                dataNascita,
                luogoNascita,
                codiceFiscale);
        }

        public void CambiaStatoDichiarazione(Int32 idDichiarazione, TipoStatoGestionePratica vecchioStato,
                                             TipoStatoGestionePratica nuovoStato, Int32 idUtenteCambioStato)
        {
            dataAccess.CambiaStatoDichiarazione(idDichiarazione, vecchioStato, nuovoStato, idUtenteCambioStato);
        }

        public void CambiaLavoratoreSelezionato(Int32 idDichiarazione, Int32? idLavoratore, Boolean nuovoLavoratore)
        {
            dataAccess.CambiaLavoratoreSelezionato(idDichiarazione, idLavoratore, nuovoLavoratore);
        }

        public void ControlloSdoppioneEffettuato(int idDichiarazione, Boolean controlloEffettuato)
        {
            dataAccess.ControlloSdoppioneEffettuato(idDichiarazione, controlloEffettuato);
        }

        public void CambiaIndirizzoDichiarato(int idDichiarazione, Boolean mantieni)
        {
            dataAccess.CambiaIndirizzoDichiarato(idDichiarazione, mantieni);
        }

        public void CambiaSelezioneRapportoCessazione(int idDichiarazione, Boolean mantieni, Int32? idLavoratore,
                                                      Int32? idImpresa, DateTime? dataFineValiditaRapporto)
        {
            dataAccess.CambiaSelezioneRapportoCessazione(idDichiarazione, mantieni, idLavoratore, idImpresa,
                                                         dataFineValiditaRapporto);
        }

        public void CambiaIBANDichiarato(int idDichiarazione, Boolean mantieni)
        {
            dataAccess.CambiaIBANDichiarato(idDichiarazione, mantieni);
        }

        public DichiarazioneCollection GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(Int32 idImpresa,
                                                                                                string codiceFiscale,
                                                                                                DateTime?
                                                                                                    dataInizioValiditaRapporto,
                                                                                                DateTime? dataCessazione)
        {
            return dataAccess.GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(idImpresa, codiceFiscale,
                                                                                       dataInizioValiditaRapporto,
                                                                                       dataCessazione);
        }


        public void CambiaPosizioneValida(Int32 idDichirazione, bool? posizioneValida)
        {
            dataAccess.CambiaPosizioneValida(idDichirazione, posizioneValida);
        }

        #region Metodi per i controlli

        public String LavoratoreSelezionatoONuovo(Dichiarazione dichiarazione)
        {
            String res = null;

            if (!dichiarazione.NuovoLavoratore
                && !dichiarazione.IdLavoratoreSelezionato.HasValue)
            {
                res = URLSEMAFOROROSSO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public String AnagraficaUgualeSenzaOmocodia(Dichiarazione dichiarazione,
                                                    LavoratoreCollection lavoratoriStessiDatiAnagrafici)
        {
            String res = null;

            if (lavoratoriStessiDatiAnagrafici.Count >= 1
                &&
                ConfrontaAnagraficaUgualeSenzaOmocodia(dichiarazione, lavoratoriStessiDatiAnagrafici))
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        private bool ConfrontaAnagraficaUgualeSenzaOmocodia(Dichiarazione dichiarazione, LavoratoreCollection lavoratori)
        {
            Boolean res = false;
            Lavoratore lavDichiarato = dichiarazione.Lavoratore;

            if (lavDichiarato.CodiceFiscale.Length == 16)  //Non eseguo il controllo per cf provvisori (11 caratteri es:23372119000)
            {
                foreach (Lavoratore lavTrovato in lavoratori)
                {
                    if (lavDichiarato.CodiceFiscale.ToUpper() != lavTrovato.CodiceFiscale)
                    {
                        String lavDichiaratoLuogoNascita = lavDichiarato.CodiceFiscale.Substring(12, 3);
                        String lavTrovatoLuogoNascita = lavTrovato.CodiceFiscale.Substring(12, 3);

                        Int32 temp1, temp2;
                        if (Int32.TryParse(lavDichiaratoLuogoNascita, out temp1)
                            && Int32.TryParse(lavTrovatoLuogoNascita, out temp2))
                        {
                            res = true;
                        }
                    }
                }
            }

            return res;
        }

        public String AnagraficaUgualeConOmocodia(Dichiarazione dichiarazione,
                                                  LavoratoreCollection lavoratoriStessiDatiAnagrafici)
        {
            String res = null;

            if (lavoratoriStessiDatiAnagrafici.Count >= 1
                &&
                ConfrontaAnagraficaUgualeConOmocodia(dichiarazione, lavoratoriStessiDatiAnagrafici))
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public String EmissioneTesseraSanitaria(Dichiarazione dichiarazione)
        {
            String res = null;
            if (CodiceFiscaleManager.GetDataSpedizioneTesseraSanitariaByCf(dichiarazione.Lavoratore.CodiceFiscale) !=
                null)
            {
                res = URLSEMAFOROVERDE;
            }
            else
            {
                res = URLSEMAFOROGIALLO;
            }

            return res;
        }

        private bool ConfrontaAnagraficaUgualeConOmocodia(Dichiarazione dichiarazione, LavoratoreCollection lavoratori)
        {
            Boolean res = false;

            Lavoratore lavDichiarato = dichiarazione.Lavoratore;
            if (lavDichiarato.CodiceFiscale.Length == 16)  //Non eseguo il controllo per cf provvisori (11 caratteri es:23372119000)
            {
                foreach (Lavoratore lavTrovato in lavoratori)
                {
                    if (lavDichiarato.CodiceFiscale.ToUpper() != lavTrovato.CodiceFiscale)
                    {
                        String lavDichiaratoLuogoNascita = lavDichiarato.CodiceFiscale.Substring(12, 3);
                        String lavTrovatoLuogoNascita = lavTrovato.CodiceFiscale.Substring(12, 3);

                        Int32 temp1, temp2;
                        if (!Int32.TryParse(lavDichiaratoLuogoNascita, out temp1)
                            || !Int32.TryParse(lavTrovatoLuogoNascita, out temp2))
                        {
                            res = true;
                        }
                    }
                }
            }

            return res;
        }

        public String AnagraficaDoppione(Dichiarazione dichiarazione,
                                         LavoratoreCollection lavoratoriStessiDatiAnagraficiECodiceFiscale)
        {
            String res = null;

            if (lavoratoriStessiDatiAnagraficiECodiceFiscale.Count > 1)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public String ControlloRapportoCessazione(Dichiarazione dichiarazione)
        {
            String res = null;

            if (!dichiarazione.RapportoAssociatoCessazione)
            {
                res = URLSEMAFOROROSSO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            if (dichiarazione.NuovoLavoratore)
                res = URLSEMAFOROVERDE;

            return res;
        }

        public Boolean ControlloContrattoImpresaTipoContratto(Dichiarazione dichiarazione)
        {
            Boolean result = false;
            if (dichiarazione.RapportoDiLavoro != null && dichiarazione.RapportoDiLavoro.Contratto != null
                && dichiarazione.Impresa !=null && dichiarazione.Impresa.Contratto != null
                && String.Equals(dichiarazione.RapportoDiLavoro.Contratto.IdContratto,dichiarazione.Impresa.Contratto.IdContratto))
            {
                result = true;
            }

            return result;
        }

        public String AnagraficaSdoppione(Dichiarazione dichiarazione, LavoratoreCollection lavoratoriCodFisc,
                                          LavoratoreCollection lavoratoriAnag)
        {
            String res = null;

            if (!dichiarazione.ControlloSdoppione
                && ConfrontaAnagraficaSdoppione(dichiarazione, lavoratoriCodFisc, lavoratoriAnag))
            {
                res = URLSEMAFOROROSSO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        private bool ConfrontaAnagraficaSdoppione(Dichiarazione dichiarazione, LavoratoreCollection lavoratoriCodFisc,
                                                  LavoratoreCollection lavoratoriAnag)
        {
            Boolean res = false;

            List<Int32> lavoratoriTrovati = new List<Int32>();
            foreach (Lavoratore lav in lavoratoriCodFisc)
            {
                Boolean inserisci = true;

                for (Int32 i = 0; i < lavoratoriTrovati.Count; i++)
                {
                    if (lav.IdLavoratore.Value == lavoratoriTrovati[i])
                    {
                        inserisci = false;
                        break;
                    }
                }

                if (inserisci)
                {
                    lavoratoriTrovati.Add(lav.IdLavoratore.Value);
                }
            }
            foreach (Lavoratore lav in lavoratoriAnag)
            {
                Boolean inserisci = true;

                for (Int32 i = 0; i < lavoratoriTrovati.Count; i++)
                {
                    if (lav.IdLavoratore.Value == lavoratoriTrovati[i])
                    {
                        inserisci = false;
                        break;
                    }
                }

                if (inserisci)
                {
                    lavoratoriTrovati.Add(lav.IdLavoratore.Value);
                }
            }

            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();
            foreach (Int32 idLav in lavoratoriTrovati)
            {
                rapportiAperti.AddRange(
                    GetRapportiDiLavoroAperti(idLav, dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto.Value));
            }
            rapportiAperti.AddRange(GetDichiarazioniAperte(dichiarazione.IdDichiarazione.Value,
                                                           dichiarazione.Lavoratore.CodiceFiscale,
                                                           dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto.
                                                               Value));

            if (rapportiAperti.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public String AnagraficaDubbia(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            String res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (!dichiarazione.NuovoLavoratore
                    && lavoratore != null
                    && ConfrontaAnagraficaDubbia(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROGIALLO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private Boolean ConfrontaAnagraficaDubbia(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            Boolean res = false;

            Lavoratore lavDichiarato = dichiarazione.Lavoratore;
            if (lavDichiarato.Cognome.ToUpper() != lavoratore.Cognome.ToUpper()
                || lavDichiarato.Nome.ToUpper() != lavoratore.Nome.ToUpper()
                || lavDichiarato.Sesso != lavoratore.Sesso
                || lavDichiarato.DataNascita != lavoratore.DataNascita
                || lavDichiarato.ComuneNascita != lavoratore.ComuneNascita)
            {
                res = true;
            }

            return res;
        }

        public String IbanDiverso(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            String res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (lavoratore != null
                    && !dichiarazione.MantieniIbanAnagrafica.HasValue
                    && ConfrontaIban(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROROSSO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaIban(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            Boolean res = false;

            if (!String.IsNullOrEmpty(dichiarazione.Lavoratore.IBAN)
                && !String.IsNullOrEmpty(lavoratore.IBAN)
                && dichiarazione.Lavoratore.IBAN != lavoratore.IBAN)
            {
                res = true;
            }

            return res;
        }

        public String InformazioniPrepagata(Dichiarazione dichiarazione)
        {
            String res = null;

            if (dichiarazione.Lavoratore.RichiestaInfoCartaPrepagata)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public String IndirizzoDiverso(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            String res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (lavoratore != null
                    && !dichiarazione.MantieniIndirizzoAnagrafica.HasValue
                    && ConfrontaIndirizzo(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROROSSO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaIndirizzo(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            Boolean res = false;

            if (!String.IsNullOrEmpty(dichiarazione.Lavoratore.Indirizzo.Indirizzo1)
                && !String.IsNullOrEmpty(lavoratore.Indirizzo.Indirizzo1)
                && (dichiarazione.Lavoratore.Indirizzo.Indirizzo1 != lavoratore.Indirizzo.Indirizzo1
                    || dichiarazione.Lavoratore.Indirizzo.Provincia != lavoratore.Indirizzo.Provincia
                    || dichiarazione.Lavoratore.Indirizzo.Comune != lavoratore.Indirizzo.Comune
                    || dichiarazione.Lavoratore.Indirizzo.Cap != lavoratore.Indirizzo.Cap))
            {
                res = true;
            }

            return res;
        }

        public String EmailDiversa(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            String res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (!dichiarazione.NuovoLavoratore
                    && lavoratore != null
                    && ConfrontaEmail(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROGIALLO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaEmail(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            Boolean res = false;

            if (!String.IsNullOrEmpty(lavoratore.Email)
                && lavoratore.Email != dichiarazione.Lavoratore.Email)
            {
                res = true;
            }

            return res;
        }

        public String CellulareDiverso(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            String res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (!dichiarazione.NuovoLavoratore
                    && lavoratore != null
                    && ConfrontaCellulare(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROGIALLO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaCellulare(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            Boolean res = false;

            if (!String.IsNullOrEmpty(lavoratore.NumeroTelefonoCellulare)
                && lavoratore.NumeroTelefonoCellulare != dichiarazione.Lavoratore.NumeroTelefonoCellulare)
            {
                res = true;
            }

            return res;
        }

        public String CantiereInNotifica(Dichiarazione dichiarazione)
        {
            String res = null;

            if (ControlloPresenzaInNotifiche(dichiarazione.Cantiere))
            {
                res = URLSEMAFOROVERDE;
            }
            else
            {
                res = URLSEMAFOROGIALLO;
            }

            return res;
        }

        private Boolean ControlloPresenzaInNotifiche(Indirizzo indirizzo)
        {
            Boolean ret = true;

            CptBusiness cptBiz = new CptBusiness();
            NotificaFilter filtro = new NotificaFilter();
            filtro.Indirizzo = indirizzo.Indirizzo1;
            filtro.IndirizzoComune = indirizzo.ComuneDescrizione;
            NotificaCollection notifiche =
                cptBiz.RicercaNotifiche(filtro);
            if (notifiche == null || notifiche.Count == 0)
            {
                ret = false;
            }

            return ret;
        }

        #endregion
    }
}