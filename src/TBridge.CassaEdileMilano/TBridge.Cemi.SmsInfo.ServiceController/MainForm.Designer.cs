namespace TBridge.Cemi.SmsInfo.ServiceController
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundWorkerVerificaRichieste = new System.ComponentModel.BackgroundWorker();
            this.tabControlSms = new System.Windows.Forms.TabControl();
            this.tabPageSmsRicevutiCarrier = new System.Windows.Forms.TabPage();
            this.buttonAggiornaSmsRicevutiCarrier = new System.Windows.Forms.Button();
            this.dataGridViewSmsRicevutiCarrier = new System.Windows.Forms.DataGridView();
            this.tabPageSmsRicevutiRegistrati = new System.Windows.Forms.TabPage();
            this.buttonAggiornaSmsRicevutiRegistrati = new System.Windows.Forms.Button();
            this.dataGridViewSmsRicevutiRegistrati = new System.Windows.Forms.DataGridView();
            this.tabPageSmsInviatiCarrier = new System.Windows.Forms.TabPage();
            this.buttonAggiornaSmsInviatiCarrier = new System.Windows.Forms.Button();
            this.dataGridViewSmsInviatiCarrier = new System.Windows.Forms.DataGridView();
            this.tabPageSmsInviatiRegistrati = new System.Windows.Forms.TabPage();
            this.buttonAggiornaSmsInviatiRegistrati = new System.Windows.Forms.Button();
            this.dataGridViewSmsInviatiRegistrati = new System.Windows.Forms.DataGridView();
            this.timerVerificaRichieste = new System.Windows.Forms.Timer(this.components);
            this.timerVerificaSmsInInvio = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerVerificaSmsInInvio = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerVerificaRichiesteNonGestite = new System.ComponentModel.BackgroundWorker();
            this.timerVerificaRichiesteNonGestite = new System.Windows.Forms.Timer(this.components);
            this.tabControlSms.SuspendLayout();
            this.tabPageSmsRicevutiCarrier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsRicevutiCarrier)).BeginInit();
            this.tabPageSmsRicevutiRegistrati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsRicevutiRegistrati)).BeginInit();
            this.tabPageSmsInviatiCarrier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsInviatiCarrier)).BeginInit();
            this.tabPageSmsInviatiRegistrati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsInviatiRegistrati)).BeginInit();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "SmsService";
            this.notifyIcon.Visible = true;
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // tabControlSms
            // 
            this.tabControlSms.Controls.Add(this.tabPageSmsRicevutiCarrier);
            this.tabControlSms.Controls.Add(this.tabPageSmsRicevutiRegistrati);
            this.tabControlSms.Controls.Add(this.tabPageSmsInviatiCarrier);
            this.tabControlSms.Controls.Add(this.tabPageSmsInviatiRegistrati);
            this.tabControlSms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSms.Location = new System.Drawing.Point(0, 0);
            this.tabControlSms.Name = "tabControlSms";
            this.tabControlSms.SelectedIndex = 0;
            this.tabControlSms.Size = new System.Drawing.Size(709, 466);
            this.tabControlSms.TabIndex = 0;
            // 
            // tabPageSmsRicevutiCarrier
            // 
            this.tabPageSmsRicevutiCarrier.Controls.Add(this.buttonAggiornaSmsRicevutiCarrier);
            this.tabPageSmsRicevutiCarrier.Controls.Add(this.dataGridViewSmsRicevutiCarrier);
            this.tabPageSmsRicevutiCarrier.Location = new System.Drawing.Point(4, 22);
            this.tabPageSmsRicevutiCarrier.Name = "tabPageSmsRicevutiCarrier";
            this.tabPageSmsRicevutiCarrier.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSmsRicevutiCarrier.Size = new System.Drawing.Size(701, 440);
            this.tabPageSmsRicevutiCarrier.TabIndex = 0;
            this.tabPageSmsRicevutiCarrier.Text = "Sms Ricevuti Carrier";
            this.tabPageSmsRicevutiCarrier.UseVisualStyleBackColor = true;
            // 
            // buttonAggiornaSmsRicevutiCarrier
            // 
            this.buttonAggiornaSmsRicevutiCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAggiornaSmsRicevutiCarrier.Location = new System.Drawing.Point(585, 411);
            this.buttonAggiornaSmsRicevutiCarrier.Name = "buttonAggiornaSmsRicevutiCarrier";
            this.buttonAggiornaSmsRicevutiCarrier.Size = new System.Drawing.Size(110, 23);
            this.buttonAggiornaSmsRicevutiCarrier.TabIndex = 1;
            this.buttonAggiornaSmsRicevutiCarrier.Text = "Aggiorna";
            this.buttonAggiornaSmsRicevutiCarrier.UseVisualStyleBackColor = true;
            this.buttonAggiornaSmsRicevutiCarrier.Click += new System.EventHandler(this.buttonAggiornaSmsRicevutiCarrier_Click);
            // 
            // dataGridViewSmsRicevutiCarrier
            // 
            this.dataGridViewSmsRicevutiCarrier.AllowUserToAddRows = false;
            this.dataGridViewSmsRicevutiCarrier.AllowUserToDeleteRows = false;
            this.dataGridViewSmsRicevutiCarrier.AllowUserToOrderColumns = true;
            this.dataGridViewSmsRicevutiCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSmsRicevutiCarrier.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSmsRicevutiCarrier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSmsRicevutiCarrier.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewSmsRicevutiCarrier.Name = "dataGridViewSmsRicevutiCarrier";
            this.dataGridViewSmsRicevutiCarrier.ReadOnly = true;
            this.dataGridViewSmsRicevutiCarrier.Size = new System.Drawing.Size(689, 399);
            this.dataGridViewSmsRicevutiCarrier.TabIndex = 0;
            // 
            // tabPageSmsRicevutiRegistrati
            // 
            this.tabPageSmsRicevutiRegistrati.Controls.Add(this.buttonAggiornaSmsRicevutiRegistrati);
            this.tabPageSmsRicevutiRegistrati.Controls.Add(this.dataGridViewSmsRicevutiRegistrati);
            this.tabPageSmsRicevutiRegistrati.Location = new System.Drawing.Point(4, 22);
            this.tabPageSmsRicevutiRegistrati.Name = "tabPageSmsRicevutiRegistrati";
            this.tabPageSmsRicevutiRegistrati.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSmsRicevutiRegistrati.Size = new System.Drawing.Size(701, 440);
            this.tabPageSmsRicevutiRegistrati.TabIndex = 1;
            this.tabPageSmsRicevutiRegistrati.Text = "Sms Ricevuti Registrati";
            this.tabPageSmsRicevutiRegistrati.UseVisualStyleBackColor = true;
            // 
            // buttonAggiornaSmsRicevutiRegistrati
            // 
            this.buttonAggiornaSmsRicevutiRegistrati.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAggiornaSmsRicevutiRegistrati.Location = new System.Drawing.Point(585, 411);
            this.buttonAggiornaSmsRicevutiRegistrati.Name = "buttonAggiornaSmsRicevutiRegistrati";
            this.buttonAggiornaSmsRicevutiRegistrati.Size = new System.Drawing.Size(110, 23);
            this.buttonAggiornaSmsRicevutiRegistrati.TabIndex = 2;
            this.buttonAggiornaSmsRicevutiRegistrati.Text = "Aggiorna";
            this.buttonAggiornaSmsRicevutiRegistrati.UseVisualStyleBackColor = true;
            this.buttonAggiornaSmsRicevutiRegistrati.Click += new System.EventHandler(this.buttonAggiornaSmsRicevutiRegistrati_Click);
            // 
            // dataGridViewSmsRicevutiRegistrati
            // 
            this.dataGridViewSmsRicevutiRegistrati.AllowUserToAddRows = false;
            this.dataGridViewSmsRicevutiRegistrati.AllowUserToDeleteRows = false;
            this.dataGridViewSmsRicevutiRegistrati.AllowUserToOrderColumns = true;
            this.dataGridViewSmsRicevutiRegistrati.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSmsRicevutiRegistrati.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSmsRicevutiRegistrati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSmsRicevutiRegistrati.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewSmsRicevutiRegistrati.Name = "dataGridViewSmsRicevutiRegistrati";
            this.dataGridViewSmsRicevutiRegistrati.ReadOnly = true;
            this.dataGridViewSmsRicevutiRegistrati.Size = new System.Drawing.Size(689, 399);
            this.dataGridViewSmsRicevutiRegistrati.TabIndex = 1;
            // 
            // tabPageSmsInviatiCarrier
            // 
            this.tabPageSmsInviatiCarrier.Controls.Add(this.buttonAggiornaSmsInviatiCarrier);
            this.tabPageSmsInviatiCarrier.Controls.Add(this.dataGridViewSmsInviatiCarrier);
            this.tabPageSmsInviatiCarrier.Location = new System.Drawing.Point(4, 22);
            this.tabPageSmsInviatiCarrier.Name = "tabPageSmsInviatiCarrier";
            this.tabPageSmsInviatiCarrier.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSmsInviatiCarrier.Size = new System.Drawing.Size(701, 440);
            this.tabPageSmsInviatiCarrier.TabIndex = 2;
            this.tabPageSmsInviatiCarrier.Text = "Sms Inviati Carrier";
            this.tabPageSmsInviatiCarrier.UseVisualStyleBackColor = true;
            // 
            // buttonAggiornaSmsInviatiCarrier
            // 
            this.buttonAggiornaSmsInviatiCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAggiornaSmsInviatiCarrier.Location = new System.Drawing.Point(585, 411);
            this.buttonAggiornaSmsInviatiCarrier.Name = "buttonAggiornaSmsInviatiCarrier";
            this.buttonAggiornaSmsInviatiCarrier.Size = new System.Drawing.Size(110, 23);
            this.buttonAggiornaSmsInviatiCarrier.TabIndex = 2;
            this.buttonAggiornaSmsInviatiCarrier.Text = "Aggiorna";
            this.buttonAggiornaSmsInviatiCarrier.UseVisualStyleBackColor = true;
            this.buttonAggiornaSmsInviatiCarrier.Click += new System.EventHandler(this.buttonAggiornaSmsInviatiCarrier_Click);
            // 
            // dataGridViewSmsInviatiCarrier
            // 
            this.dataGridViewSmsInviatiCarrier.AllowUserToAddRows = false;
            this.dataGridViewSmsInviatiCarrier.AllowUserToDeleteRows = false;
            this.dataGridViewSmsInviatiCarrier.AllowUserToOrderColumns = true;
            this.dataGridViewSmsInviatiCarrier.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSmsInviatiCarrier.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSmsInviatiCarrier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSmsInviatiCarrier.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewSmsInviatiCarrier.Name = "dataGridViewSmsInviatiCarrier";
            this.dataGridViewSmsInviatiCarrier.ReadOnly = true;
            this.dataGridViewSmsInviatiCarrier.Size = new System.Drawing.Size(689, 399);
            this.dataGridViewSmsInviatiCarrier.TabIndex = 1;
            // 
            // tabPageSmsInviatiRegistrati
            // 
            this.tabPageSmsInviatiRegistrati.Controls.Add(this.buttonAggiornaSmsInviatiRegistrati);
            this.tabPageSmsInviatiRegistrati.Controls.Add(this.dataGridViewSmsInviatiRegistrati);
            this.tabPageSmsInviatiRegistrati.Location = new System.Drawing.Point(4, 22);
            this.tabPageSmsInviatiRegistrati.Name = "tabPageSmsInviatiRegistrati";
            this.tabPageSmsInviatiRegistrati.Size = new System.Drawing.Size(701, 440);
            this.tabPageSmsInviatiRegistrati.TabIndex = 3;
            this.tabPageSmsInviatiRegistrati.Text = "Sms Inviati Registrati";
            this.tabPageSmsInviatiRegistrati.UseVisualStyleBackColor = true;
            // 
            // buttonAggiornaSmsInviatiRegistrati
            // 
            this.buttonAggiornaSmsInviatiRegistrati.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAggiornaSmsInviatiRegistrati.Location = new System.Drawing.Point(585, 411);
            this.buttonAggiornaSmsInviatiRegistrati.Name = "buttonAggiornaSmsInviatiRegistrati";
            this.buttonAggiornaSmsInviatiRegistrati.Size = new System.Drawing.Size(110, 23);
            this.buttonAggiornaSmsInviatiRegistrati.TabIndex = 2;
            this.buttonAggiornaSmsInviatiRegistrati.Text = "Aggiorna";
            this.buttonAggiornaSmsInviatiRegistrati.UseVisualStyleBackColor = true;
            this.buttonAggiornaSmsInviatiRegistrati.Click += new System.EventHandler(this.buttonAggiornaSmsInviatiRegistrati_Click);
            // 
            // dataGridViewSmsInviatiRegistrati
            // 
            this.dataGridViewSmsInviatiRegistrati.AllowUserToAddRows = false;
            this.dataGridViewSmsInviatiRegistrati.AllowUserToDeleteRows = false;
            this.dataGridViewSmsInviatiRegistrati.AllowUserToOrderColumns = true;
            this.dataGridViewSmsInviatiRegistrati.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSmsInviatiRegistrati.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSmsInviatiRegistrati.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSmsInviatiRegistrati.Location = new System.Drawing.Point(6, 6);
            this.dataGridViewSmsInviatiRegistrati.Name = "dataGridViewSmsInviatiRegistrati";
            this.dataGridViewSmsInviatiRegistrati.ReadOnly = true;
            this.dataGridViewSmsInviatiRegistrati.Size = new System.Drawing.Size(689, 399);
            this.dataGridViewSmsInviatiRegistrati.TabIndex = 1;
            // 
            // timerVerificaRichieste
            // 
            this.timerVerificaRichieste.Tick += new System.EventHandler(this.timerVerificaRichieste_Tick);
            // 
            // timerVerificaSmsInInvio
            // 
            this.timerVerificaSmsInInvio.Tick += new System.EventHandler(this.timerVerificaSmsInInvio_Tick);
            // 
            // timerVerificaRichiesteNonGestite
            // 
            this.timerVerificaRichiesteNonGestite.Tick += new System.EventHandler(this.timerVerificaRichiesteNonGestite_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 466);
            this.Controls.Add(this.tabControlSms);
            this.Name = "MainForm";
            this.Text = "SmsInfo Control";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.tabControlSms.ResumeLayout(false);
            this.tabPageSmsRicevutiCarrier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsRicevutiCarrier)).EndInit();
            this.tabPageSmsRicevutiRegistrati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsRicevutiRegistrati)).EndInit();
            this.tabPageSmsInviatiCarrier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsInviatiCarrier)).EndInit();
            this.tabPageSmsInviatiRegistrati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSmsInviatiRegistrati)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.ComponentModel.BackgroundWorker backgroundWorkerVerificaRichieste;
        private System.Windows.Forms.TabControl tabControlSms;
        private System.Windows.Forms.TabPage tabPageSmsRicevutiCarrier;
        private System.Windows.Forms.TabPage tabPageSmsRicevutiRegistrati;
        private System.Windows.Forms.TabPage tabPageSmsInviatiCarrier;
        private System.Windows.Forms.TabPage tabPageSmsInviatiRegistrati;
        private System.Windows.Forms.Button buttonAggiornaSmsRicevutiCarrier;
        private System.Windows.Forms.DataGridView dataGridViewSmsRicevutiCarrier;
        private System.Windows.Forms.Timer timerVerificaRichieste;
        private System.Windows.Forms.Timer timerVerificaSmsInInvio;
        private System.ComponentModel.BackgroundWorker backgroundWorkerVerificaSmsInInvio;
        private System.Windows.Forms.Button buttonAggiornaSmsRicevutiRegistrati;
        private System.Windows.Forms.DataGridView dataGridViewSmsRicevutiRegistrati;
        private System.Windows.Forms.Button buttonAggiornaSmsInviatiCarrier;
        private System.Windows.Forms.DataGridView dataGridViewSmsInviatiCarrier;
        private System.Windows.Forms.Button buttonAggiornaSmsInviatiRegistrati;
        private System.Windows.Forms.DataGridView dataGridViewSmsInviatiRegistrati;
        private System.ComponentModel.BackgroundWorker backgroundWorkerVerificaRichiesteNonGestite;
        private System.Windows.Forms.Timer timerVerificaRichiesteNonGestite;
    }
}