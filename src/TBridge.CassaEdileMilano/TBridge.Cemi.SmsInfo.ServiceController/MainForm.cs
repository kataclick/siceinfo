using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
using TBridge.Cemi.SmsInfo.Business;
using TBridge.Cemi.SmsInfo.Type.Collections;
using TBridge.Cemi.SmsInfo.Type.Entities;

namespace TBridge.Cemi.SmsInfo.ServiceController
{
    public partial class MainForm : BaseForm
    {
        //private readonly SmsBusiness smsBusiness;

        public MainForm()
        {
            InitializeComponent();

            /*
            smsBusiness = new SmsBusiness();

            InitializeBackgroudWorker();

            InitializeTimer();


            VerificaRichiesteSms();

            VerificaSmsInInvio();

            VerificaRichiesteNonGestite();


            LoadAllData();
             */
        }

        
        private void VerificaRichiesteSms()
        {
            try
            {
                backgroundWorkerVerificaRichieste.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.VerificaRichiesteSms", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            }
        }

        private void VerificaSmsInInvio()
        {
            try
            {
                backgroundWorkerVerificaSmsInInvio.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.VerificaSmsInInvio", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            }
        }

        private void VerificaRichiesteNonGestite()
        {
            try
            {
                backgroundWorkerVerificaRichiesteNonGestite.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.VerificaRichiesteNonGestite", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            }
        }

        private void InitializeTimer()
        {
            timerVerificaRichieste.Interval = Int32.Parse(ConfigurationManager.AppSettings["SmsVerificaRichiesteTimer"]);
            timerVerificaSmsInInvio.Interval =
                Int32.Parse(ConfigurationManager.AppSettings["SmsVerificaSmsInInvioTimer"]);
            timerVerificaRichiesteNonGestite.Interval =
                Int32.Parse(ConfigurationManager.AppSettings["SmsVerificaRichiesteNonGestiteTimer"]);
        }

        private void InitializeBackgroudWorker()
        {
            backgroundWorkerVerificaRichieste.DoWork += backgroundWorkerVerificaRichieste_DoWork;
            backgroundWorkerVerificaRichieste.RunWorkerCompleted += backgroundWorkerVerificaRichieste_RunWorkerCompleted;

            backgroundWorkerVerificaSmsInInvio.DoWork += backgroundWorkerVerificaSmsInInvio_DoWork;
            backgroundWorkerVerificaSmsInInvio.RunWorkerCompleted +=
                backgroundWorkerVerificaSmsInInvio_RunWorkerCompleted;

            backgroundWorkerVerificaRichiesteNonGestite.DoWork += backgroundWorkerVerificaRichiesteNonGestite_DoWork;
            backgroundWorkerVerificaRichiesteNonGestite.RunWorkerCompleted +=
                backgroundWorkerVerificaRichiesteNonGestite_RunWorkerCompleted;
        }

        private void backgroundWorkerVerificaRichiesteNonGestite_RunWorkerCompleted(object sender,
                                                                                    RunWorkerCompletedEventArgs e)
        {
            //lancio timer... al tick rilancio il worker...
            timerVerificaRichiesteNonGestite.Start();
        }

        private void backgroundWorkerVerificaRichiesteNonGestite_DoWork(object sender, DoWorkEventArgs e)
        {
            //smsBusiness.VerificaSmsRegistratiNonGestiti();
        }

        private void backgroundWorkerVerificaSmsInInvio_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //lancio timer... al tick rilancio il worker...
            timerVerificaSmsInInvio.Start();
        }

        private void backgroundWorkerVerificaSmsInInvio_DoWork(object sender, DoWorkEventArgs e)
        {
            //smsBusiness.VerificaSmsInInvio();
        }

        private void backgroundWorkerVerificaRichieste_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //lancio timer... al tick rilancio il worker...
            timerVerificaRichieste.Start();
        }

        private void backgroundWorkerVerificaRichieste_DoWork(object sender, DoWorkEventArgs e)
        {
            //controlle le richieste sul carrier...
            //smsBusiness.VerificaRichieste();
        }

        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
                Hide();
        }

        private void timerVerificaRichieste_Tick(object sender, EventArgs e)
        {
            timerVerificaRichieste.Stop();
            VerificaRichiesteSms();
        }

        private void timerVerificaSmsInInvio_Tick(object sender, EventArgs e)
        {
            timerVerificaSmsInInvio.Stop();
            VerificaSmsInInvio();
        }

        private void timerVerificaRichiesteNonGestite_Tick(object sender, EventArgs e)
        {
            timerVerificaRichiesteNonGestite.Stop();
            VerificaRichiesteNonGestite();
        }

        private void LoadAllData()
        {
            LoadSmsRicevutiCarrier();
            LoadSmsRicevutiRegistrati();
            LoadSmsInviatiCarrier();
            LoadSmsInviatiRegistrati();
        }

        private void LoadSmsInviatiRegistrati()
        {
            //try
            //{
            //    SmsCollection smsCollection = smsBusiness.GetListaSmsInviatiRegistrati();
            //    List<SentSms> lista = new List<SentSms>();
            //    foreach (SentSms sms in smsCollection)
            //    {
            //        lista.Add(sms);
            //    }

            //    dataGridViewSmsInviatiRegistrati.DataSource = lista;
            //    dataGridViewSmsInviatiRegistrati.Columns["Tipo"].Visible = false;
            //    dataGridViewSmsInviatiRegistrati.Columns["Testo"].Visible = false;
            //    dataGridViewSmsInviatiRegistrati.Columns["IdCarrier"].Visible = false;
            //}
            //catch (Exception ex)
            //{
            //    //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.LoadSmsInviatiRegistrati", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            //}
        }

        private void LoadSmsInviatiCarrier()
        {
            //try
            //{
            //    SmsCollection smsCollection = smsBusiness.GetListaSmsInviatiCarrier();
            //    List<SentSms> lista = new List<SentSms>();
            //    foreach (SentSms sms in smsCollection)
            //    {
            //        lista.Add(sms);
            //    }

            //    dataGridViewSmsInviatiCarrier.DataSource = lista;
            //    dataGridViewSmsInviatiCarrier.Columns["Tipo"].Visible = false;
            //    dataGridViewSmsInviatiCarrier.Columns["IdSistema"].Visible = false;
            //    dataGridViewSmsInviatiCarrier.Columns["IdInvioSistema"].Visible = false;
            //}
            //catch (Exception ex)
            //{
            //    //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.LoadSmsInviatiCarrier", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            //}
        }

        private void LoadSmsRicevutiRegistrati()
        {
            //try
            //{
            //    StoredReceivedSmsCollection smsCollection = smsBusiness.GetListaSmsRicevutiRegistrati();

            //    dataGridViewSmsRicevutiRegistrati.DataSource = smsCollection;
            //}
            //catch (Exception ex)
            //{
            //    //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.LoadSmsRicevutiRegistrati", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            //}
        }

        private void LoadSmsRicevutiCarrier()
        {
            //try
            //{
            //    SmsCollection smsCollection = smsBusiness.GetListaSmsRicevutiCarrier();
            //    List<ReceivedSms> lista = new List<ReceivedSms>();
            //    foreach (ReceivedSms sms in smsCollection)
            //    {
            //        lista.Add(sms);
            //    }

            //    dataGridViewSmsRicevutiCarrier.DataSource = lista;
            //    dataGridViewSmsRicevutiCarrier.Columns["IdSistema"].Visible = false;
            //}
            //catch (Exception ex)
            //{
            //    //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in ServiceController.LoadSmsRicevutiCarrier", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SMSINFO, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            //}
        }

        private void buttonAggiornaSmsRicevutiCarrier_Click(object sender, EventArgs e)
        {
            LoadSmsRicevutiCarrier();
        }

        private void buttonAggiornaSmsRicevutiRegistrati_Click(object sender, EventArgs e)
        {
            LoadSmsRicevutiRegistrati();
        }

        private void buttonAggiornaSmsInviatiCarrier_Click(object sender, EventArgs e)
        {
            LoadSmsInviatiCarrier();
        }

        private void buttonAggiornaSmsInviatiRegistrati_Click(object sender, EventArgs e)
        {
            LoadSmsInviatiRegistrati();
        }
    }
}