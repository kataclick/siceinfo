namespace TBridge.Cemi.GestioneUtenti.Type
{
    public class LavoratoreCredential
    {
        public int IdLavoratore { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}