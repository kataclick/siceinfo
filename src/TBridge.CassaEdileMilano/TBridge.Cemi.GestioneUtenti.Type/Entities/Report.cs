﻿using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class Report
    {
        public Int32 Id { get; set; }
        public String Nome { get; set; }
        public String Path { get; set; }
        public String Theme { get; set; }
    }
}