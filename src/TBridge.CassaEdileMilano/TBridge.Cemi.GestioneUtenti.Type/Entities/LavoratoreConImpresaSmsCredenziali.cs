namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class LavoratoreConImpresaSmsCredenziali : LavoratoreConImpresaSms
    {
        public string Username { get; set; }

        public new string Password { get; set; }
    }
}