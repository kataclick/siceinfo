using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class Fornitore : Utente
    {
        /// <summary>
        /// Costruttore pubblico
        /// </summary>
        public Fornitore()
        {
        }

        public Fornitore(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="ragioneSociale">ragione sociale</param>
        /// <param name="codice">codice sicenew</param>
        public Fornitore(string ragioneSociale, string codice)
        {
            Codice = codice;
            RagioneSociale = ragioneSociale;
        }

        /// <summary>
        /// Identificativo univoco del fornitore
        /// </summary>
        public int IdFornitore { get; set; }

        /// <summary>
        /// Ragione sociale del fornitore
        /// </summary>
        public string RagioneSociale { get; set; }

        /// <summary>
        /// Codice del foritore usato in SiceNew
        /// </summary>
        public string Codice { get; set; }

        public string EMail { get; set; }
    }
}