using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    [Serializable]
    public class CassaEdile : Utente
    {
        public CassaEdile()
        {
        }

        public CassaEdile(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        public CassaEdile(string idCassaEdile, string descrizione)
        {
            IdCassaEdile = idCassaEdile;
            Descrizione = descrizione;
        }

        public string IdCassaEdile { get; set; }

        public string Descrizione { get; set; }
    }
}