namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class LavoratoreConImpresaSms : LavoratoreConImpresa
    {
        public string NumeroTelefono { get; set; }
    }
}