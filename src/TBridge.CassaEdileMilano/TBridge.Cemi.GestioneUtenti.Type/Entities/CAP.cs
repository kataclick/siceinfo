using System;
using TBridge.Cemi.GestioneUtenti.Type.Collections;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    [Serializable]
    public class CAP
    {
        public CAP()
        {
        }

        public CAP(string cap)
        {
            Cap = cap;
            ListaComuni = new CAPCollection();
        }

        public CAP(string cap, string comune, string provincia)
        {
            Cap = cap;
            Comune = comune;
            Provincia = provincia;
        }

        public string Cap { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Testo
        {
            get { return Comune + " " + Provincia + " - " + Cap; }
        }

        public CAPCollection ListaComuni { get; set; }
    }
}