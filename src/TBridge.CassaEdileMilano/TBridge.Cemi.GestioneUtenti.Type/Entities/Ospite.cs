using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class Ospite : Utente
    {
        public Ospite()
        {
        }

        public Ospite(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        public Ospite(string nome, string cognome)
        {
            //
            Nome = nome;
            Cognome = cognome;
        }

        public int IdOspite { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }

        public string Ente { get; set; }

        public string EMail { get; set; }
    }
}