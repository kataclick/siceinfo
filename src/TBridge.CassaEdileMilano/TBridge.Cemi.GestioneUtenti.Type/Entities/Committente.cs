using System;
using TBridge.Cemi.GestioneUtenti.Type.Enums;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    [Serializable]
    public class Committente : Utente
    {
        private String cap;
        private String codiceFiscale;
        private String cognome;
        private Int64? comune;
        private String fax;
        private Int32 idCommittente;
        private String indirizzo;
        private String nome;
        private String partitaIva;
        private Int32? provincia;
        private String ragioneSociale;
        private String telefono;
        private TipologiaCommittente tipologia;

        public Committente()
        {
        }

        public Committente(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        public Int32 IdCommittente
        {
            get { return idCommittente; }
            set { idCommittente = value; }
        }

        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public String Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public String RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public String PartitaIva
        {
            get { return partitaIva; }
            set { partitaIva = value; }
        }

        public String CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public String Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public Int64? Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public Int32? Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public String Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public String Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public String Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        public TipologiaCommittente Tipologia
        {
            get { return tipologia; }
            set { tipologia = value; }
        }
    }
}