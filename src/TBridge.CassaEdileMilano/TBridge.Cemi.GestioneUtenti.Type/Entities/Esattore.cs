using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class Esattore : Utente
    {
        public Esattore()
        {
        }

        public Esattore(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        public string IdEsattore { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }
    }
}