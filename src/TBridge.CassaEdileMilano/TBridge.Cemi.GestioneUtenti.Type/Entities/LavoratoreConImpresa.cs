namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class LavoratoreConImpresa : Lavoratore
    {
        public int IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }
    }
}