namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class ImpresaConCredenziali : Impresa
    {
        public string Username { get; set; }

        public new string Password { get; set; }
    }
}