using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    [Serializable]
    public class Consulente : Utente
    {
        public Consulente()
        {
        }

        public Consulente(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        #region Aggiunta

        public string Cellulare { get; set; }

        public string Fax { get; set; }

        public string EMail { get; set; }

        #endregion

        //public Consulente(int idConsulente, string ragioneSociale)
        //{
        //    this.IdConsulente = idConsulente;
        //    this.RagioneSociale = ragioneSociale;
        //}

        public int IdConsulente { get; set; }

        public string RagioneSociale { get; set; }

        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string CodiceFiscale { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public string PIN { get; set; }

        public DateTime? DataGenerazionePIN { get; set; }

        public string GuidEdilconnect { get; set; }
    }
}