using System;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class Sindacalista : Utente
    {
        private string cognome;
        private string nome;

        public Sindacalista()
        {
        }

        public Sindacalista(int _id)
        {
            Id = _id;
        }

        public Sindacalista(Int32 idUtente, String userName)
            : base(idUtente, userName, null)
        {
        }

        public Sindacato Sindacato { get; set; }

        public ComprensorioSindacale ComprensorioSindacale { get; set; }

        public int Id { get; set; }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        /// <summary>
        /// Solo GET: ritorna "Nome Cognome"
        /// </summary>
        public string NomeCompleto
        {
            get { return String.Format("{0} {1}", cognome, nome); }
        }
    }
}