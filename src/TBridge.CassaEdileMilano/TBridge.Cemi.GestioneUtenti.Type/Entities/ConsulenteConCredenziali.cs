namespace TBridge.Cemi.GestioneUtenti.Type.Entities
{
    public class ConsulenteConCredenziali : Consulente
    {
        public string Username { get; set; }

        public new string Password { get; set; }
    }
}