using System;
using System.Collections.Generic;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.GestioneUtenti.Type.Collections
{
    [Serializable]
    public class FunzionalitaCollection : List<Funzionalita>
    {
        public bool Contains(string nomeFunzionalita)
        {
            foreach (Funzionalita funzionalita in this)
            {
                if (funzionalita.Nome == nomeFunzionalita)
                    return true;
            }

            return false;
        }

        public bool Contains(int idFunzionalita)
        {
            foreach (Funzionalita funzionalita in this)
            {
                if (funzionalita.IdFunzionalita == idFunzionalita)
                    return true;
            }

            return false;
        }

        public new string[] ToArray()
        {
            string[] funzionalitaArray = new string[Count];

            int i = 0;
            foreach (Funzionalita funzionalita in this)
            {
                funzionalitaArray[i] = funzionalita.Nome;
                i++;
            }

            return funzionalitaArray;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string funzS = string.Empty;

            foreach (Funzionalita funz in this)
            {
                funzS += funz.Nome + ",";
            }

            return funzS.Substring(0, funzS.Length - 1);
        }
    }
}