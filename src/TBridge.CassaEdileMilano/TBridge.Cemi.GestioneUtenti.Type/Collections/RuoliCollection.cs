using System;
using System.Collections.Generic;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.GestioneUtenti.Type.Collections
{
    [Serializable]
    public class RuoliCollection : List<Ruolo>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruolo"></param>
        /// <returns></returns>
        public bool Contains(RuoliPredefiniti ruolo)
        {
            return Contains(ruolo.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeRuolo"></param>
        /// <returns></returns>
        public bool Contains(string nomeRuolo)
        {
            foreach (Ruolo ruolo in this)
            {
                if (ruolo.Nome == nomeRuolo)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <returns></returns>
        public bool Contains(int idRuolo)
        {
            foreach (Ruolo ruolo in this)
            {
                if (ruolo.IdRuolo == idRuolo)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public new string[] ToArray()
        {
            string[] ruoliArray = new string[Count];

            int i = 0;
            foreach (Ruolo ruolo in this)
            {
                ruoliArray[i] = ruolo.Nome;
                i++;
            }

            return ruoliArray;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ruoloS = string.Empty;

            foreach (Ruolo ruolo in this)
            {
                ruoloS += ruolo.Nome + ",";
            }

            return ruoloS.Substring(0, ruoloS.Length - 1);
        }
    }
}