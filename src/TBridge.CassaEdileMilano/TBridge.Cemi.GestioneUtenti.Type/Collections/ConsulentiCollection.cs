using System;
using System.Collections.Generic;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.GestioneUtenti.Type.Collections
{
    [Serializable]
    public class ConsulentiCollection : List<Consulente>
    {
    }
}