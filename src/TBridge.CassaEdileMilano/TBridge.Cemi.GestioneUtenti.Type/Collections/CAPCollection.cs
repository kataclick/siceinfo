using System;
using System.Collections.Generic;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.GestioneUtenti.Type.Collections
{
    [Serializable]
    public class CAPCollection : List<CAP>
    {
        public new bool Contains(CAP capParam)
        {
            foreach (CAP cap in this)
            {
                if (cap.Cap == capParam.Cap)
                    return true;
            }

            return false;
        }
    }
}