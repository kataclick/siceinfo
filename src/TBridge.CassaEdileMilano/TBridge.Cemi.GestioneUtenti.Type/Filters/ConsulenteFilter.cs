using System;

namespace TBridge.Cemi.GestioneUtenti.Type.Filters
{
    public class ConsulenteFilter
    {
        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public bool? ConPIN { get; set; }

        public DateTime? PinGeneratoDal { get; set; }

        public DateTime? PinGeneratoAl { get; set; }

        public int? IdConsulente { get; set; }

        public bool? IscrittoSitoWeb { get; set; }
    }
}