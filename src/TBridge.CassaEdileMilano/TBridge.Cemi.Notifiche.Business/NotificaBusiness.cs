using System;
using TBridge.Cemi.Notifiche.Data;
using TBridge.Cemi.Notifiche.Type.Collections;
using TBridge.Cemi.Notifiche.Type.Entities;

//using TBridge.Cemi.ActivityTracking;

namespace TBridge.Cemi.Notifiche.Business
{
    public class NotificaBusiness
    {
        private readonly NotificaDataAccess notificaDataAccess;

        public NotificaBusiness()
        {
            notificaDataAccess = new NotificaDataAccess();
        }

        public void Assunzione(Notifica notifica)
        {
            notificaDataAccess.AssunzioneNotifica(notifica);
        }

        public void Cessazione(Notifica notifica)
        {
            try
            {
                switch (notifica.TipoNotifica)
                {
                    case TipoNotifica.CessazioneDenuncia:
                        notificaDataAccess.CessazioneDenuncia(notifica);
                        break;
                    case TipoNotifica.CessazioneNotifica:
                        notificaDataAccess.CessazioneNotifica(notifica);
                        break;
                    default:
                        throw new Exception("Notifica non valida");
                }
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.Cessazione", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);
                throw;
            }
        }

        public NotificaCollection Ricerca(string codiceFiscale, string cognome, string nome, DateTime? dataNascita,
                                          int idImpresa)
        {
            try
            {
                return notificaDataAccess.RicercaNotificheDenunce(codiceFiscale, cognome, nome, dataNascita, idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.Ricerca", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new NotificaCollection();
            }
        }

        public NotificaCollection RapportiCessazioni(int idImpresa)
        {
            try
            {
                return notificaDataAccess.RicercaRapportiCessazioni(idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.RapportiCessazioni", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new NotificaCollection();
            }
        }

        public NotificaCollection RapportiAssunzioni(int idImpresa)
        {
            try
            {
                return notificaDataAccess.RicercaRapportiAssunzioni(idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.RapportiAssunzioni", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new NotificaCollection();
            }
        }

        public NotificaCollection NotificheNonDenunciate()
        {
            try
            {
                return notificaDataAccess.RicercaNotificheNonDenunciate();
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.NotificheNonDenunciate", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new NotificaCollection();
            }
        }

        public NotificaCollection NotificheRapportiCessatiDenunciati()
        {
            try
            {
                return notificaDataAccess.RicercaNotificheRapportiCessatiDenunciati();
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.NotificheRapportiCessatiDenunciati", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new NotificaCollection();
            }
        }

        public NotificaCollection RicercaPerSubappalti(string cognome, string nome, int idImpresa)
        {
            try
            {
                return notificaDataAccess.RicercaLavoratoriImpresa(cognome, nome, idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.RicercaPerSubappalti", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new NotificaCollection();
            }
        }

        public int MesiRitardoDati()
        {
            int res = 0;

            try
            {
                res = notificaDataAccess.MesiRitardoDati();
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.MesiRitardoDati", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);
            }

            return res;
        }

        public ImpresaCollection ImpreseConsulente(int idConsulente)
        {
            return notificaDataAccess.GetImpreseConsulente(idConsulente);
        }
    }
}