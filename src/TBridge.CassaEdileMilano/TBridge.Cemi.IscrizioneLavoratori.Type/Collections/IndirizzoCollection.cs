using System;
using System.Collections.Generic;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Collections
{
    /// <summary>
    ///   Collezione di indirizzi
    /// </summary>
    [Serializable]
    public class IndirizzoCollection : List<Indirizzo>
    {
    }
}