﻿using System;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Enums
{
    [Obsolete("Utilizzare l'enum TBridge.Cemi.Type.Enums.TipoStatoGestionePratica")]
    public enum TipoStatoGestionePratica
    {
        DaValutare = 0,
        Approvata = 1,
        Rifiutata = 2,
        InAttesaDiDocumentazione = 3,
    }
}