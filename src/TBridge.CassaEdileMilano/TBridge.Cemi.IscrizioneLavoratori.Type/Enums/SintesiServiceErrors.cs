﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Enums
{
    public enum SintesiServiceErrors
    {
        NoError,
        InvalidId,
        ServiceCallFailed,
        InvalidResponse,
        ServiceErrorMessage,
    }
}
