using System;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    [Serializable]
    public class Impresa
    {
        public Int32 IdImpresa { get; set; }

        public String RagioneSociale { get; set; }

        public String PartitaIva { get; set; }

        public String CodiceFiscale { get; set; }

        public String CodiceContratto 
        {
            get
            {
                return Contratto != null ? Contratto.IdContratto : null;
            }
        }

        public String SedeLegaleIndirizzo { get; set; }

        public String SedeLegaleComune { get; set; }

        public String SedeLegaleProvincia { get; set; }

        public String SedeLegaleCap { get; set; }

        public String SedeLegaleTelefono { get; set; }

        public String SedeLegaleFax { get; set; }

        public String SedeLegaleEmail { get; set; }

        public String SedeLegale
        {
            get
            {
                return String.Format("{0} {1} ({2}) {3}",
                                     SedeLegaleIndirizzo,
                                     SedeLegaleComune,
                                     SedeLegaleProvincia,
                                     SedeLegaleCap);
            }
        }

        public String SedeAmministrativaIndirizzo { get; set; }

        public String SedeAmministrativaComune { get; set; }

        public String SedeAmministrativaProvincia { get; set; }

        public String SedeAmministrativaCap { get; set; }

        public String SedeAmministrativaTelefono { get; set; }

        public String SedeAmministrativaFax { get; set; }

        public String SedeAmministrativaEmail { get; set; }

        public String SedeAmministrativa
        {
            get
            {
                return String.Format("{0} {1} ({2}) {3}",
                                     SedeAmministrativaIndirizzo,
                                     SedeAmministrativaComune,
                                     SedeAmministrativaProvincia,
                                     SedeAmministrativaCap);
            }
        }

        public TipoContratto Contratto { get; set; }

        public DateTime? DataUltimaDenuncia { set; get; }

        public String Stato { set; get; }
    }
}