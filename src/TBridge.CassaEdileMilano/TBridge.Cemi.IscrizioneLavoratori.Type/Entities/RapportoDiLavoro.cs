using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class RapportoDiLavoro
    {
        public TipologiaRapportoLavoro TipoRapportoLavoro { get; set; }

        public Int32 IdImpresa { get; set; }

        public String RagioneSocialeImpresa { get; set; }

        public Int32 IdLavoratore { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public TipoStatoGestionePratica? StatoPratica { get; set; }

        public DateTime? DataInizioValiditaRapporto { get; set; }

        public DateTime? DataFineValiditaRapporto { get; set; }

        public DateTime? DataAssunzione { get; set; }

        public DateTime? DataLicenziamento { get; set; }

        public TipoContratto Contratto { get; set; }

        public TipoContratto ContrattoOriginaleSintesi { get; set; }

        public TipoCategoria Categoria { get; set; }

        public TipoQualifica Qualifica { get; set; }

        public TipoMansione Mansione { get; set; }

        public DateTime? DataCessazione { get; set; }

        public DateTime? DataTrasformazione { get; set; }

        public TipoFineRapporto TipoFineRapporto { get; set; }

        public TipoInizioRapporto TipoInizioRapporto { get; set; }

        public Boolean PartTime { get; set; }

        public Decimal? PercentualePartTime { get; set; }

        public String LivelloInquadramento { get; set; }
    }
}