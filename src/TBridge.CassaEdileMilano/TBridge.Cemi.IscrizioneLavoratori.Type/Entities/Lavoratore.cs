using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class Lavoratore : LavoratoreAnagraficaComune
    {
        public Lavoratore()
        {
            base.Fonte = FonteAnagraficheComuni.IscrizioneLavoratori;
        }

        #region anagrafica

        public String IdStatoCivile { get; set; }

        public String Cittadinanza { get; set; }

        public String LivelloIstruzione { get; set; }

        public String IdLingua { get; set; }

        public DateTime? DataDecesso { get; set; }

        public String IdTipoDecesso { get; set; }

        public Boolean Italiano { get; set; }

        public String NazioneNascita { get; set; }

        public TipologiaLavoratore TipoLavoratore { get; set; }

        //foto

        #endregion anagrafica

        #region Documenti

        public String TipoDocumento { get; set; }

        public String NumeroDocumento { get; set; }

        public String MotivoPermesso { get; set; }

        public DateTime? DataRichiestaPermesso { get; set; }

        public DateTime? DataScadenzaPermesso { get; set; }

        #endregion documenti

        #region indirizzo

        public Indirizzo Indirizzo { get; set; }

        public String NumeroTelefono { get; set; }

        public String NumeroTelefonoCellulare { get; set; }

        public Boolean AderisceServizioSMS { get; set; }

        public String Email { get; set; }

        public String IndirizzoCompleto
        {
            get
            {
                if (Indirizzo == null)
                {
                    return String.Empty;
                }
                else
                {
                    return Indirizzo.IndirizzoCompleto;
                }
            }
        }

        #endregion indirizzo

        #region info aggiuntive

        public String IBAN { get; set; }

        public Boolean CoIntestato { get; set; }

        public String CognomeCointestatario { get; set; }

        public Boolean RichiestaInfoCartaPrepagata { get; set; }

        public TipoCartaPrepagata TipoPrepagata { get; set; }

        public Int32? IdTagliaFelpaHusky { get; set; }

        public Int32? IdTagliaPantaloni { get; set; }

        public Int32? IdTagliaScarpe { get; set; }

        public String TagliaFelpaHusky { get; set; }

        public String TagliaPantaloni { get; set; }

        public String TagliaScarpe { get; set; }

        public Boolean RichiestaIscrizioneCorsi16Ore { get; set; }

        public String Note { get; set; }

        public String IdSindacato { get; set; }

        public DateTime? DataAdesione { get; set; }

        public DateTime? DataDisdetta { get; set; }

        public String NumeroDelega { get; set; }

        public String LinguaComunicazione { get; set; }

        public Boolean? PosizioneValida { get; set; }

        #endregion info aggiuntive
    }
}