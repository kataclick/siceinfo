using System;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    [Serializable]
    public class Indirizzo
    {
        public string Indirizzo1 { get; set; }

        public string Civico { get; set; }

        public string Comune { get; set; }

        public String ComuneDescrizione { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public String IndirizzoCompleto
        {
            get
            {
                return String.Format("{0} {1} ({2}) {3}",
                                     Indirizzo1,
                                     ComuneDescrizione,
                                     Provincia,
                                     Cap);
            }
        }

        public DateTime? DataInizioValiditÓ { set; get; }
    }
}