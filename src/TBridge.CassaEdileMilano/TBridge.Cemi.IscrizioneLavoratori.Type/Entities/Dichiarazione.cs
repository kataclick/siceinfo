using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class Dichiarazione
    {
        public Int32? IdDichiarazione { get; set; }

        public Int32? IdDichiarazionePrecedente { get; set; }

        public String IdComunicazionePrecedente { get; set; }

        public Int32? IdDichiarazioneSuccessiva { get; set; }

        public String IdComunicazioneSuccessiva { get; set; }

        public Int32? IdConsulente { get; set; }

        public Int32 IdImpresa { get; set; }

        public Impresa Impresa { get; set; }

        public Consulente Consulente { get; set; }

        public TipoAttivita Attivita { get; set; }

        public Indirizzo Cantiere { get; set; }

        public Int32 IdCantiere { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public RapportoDiLavoro RapportoDiLavoro { get; set; }

        public DateTime DataInvio { get; set; }

        public TipoStatoGestionePratica Stato { get; set; }

        public Boolean NuovoLavoratore { get; set; }

        public Int32? IdLavoratoreSelezionato { get; set; }

        public Boolean? MantieniIbanAnagrafica { get; set; }

        public Boolean? MantieniIndirizzoAnagrafica { get; set; }

        public Boolean ControlloSdoppione { get; set; }

        public Boolean RapportoAssociatoCessazione { get; set; }

        public Int32? IdLavoratoreRapportoSelezionatoCessazione { get; set; }

        public Int32? IdImpresaRapportoSelezionatoCessazione { get; set; }

        public DateTime? DataFineRapportoSelezionatoCessazione { get; set; }

        public Int32 IdUtente { get; set;}

        public String CellulareSMSSiceInfo { get; set; }

        public DateTime? DataUltimoCambioStato { get; set; }

        public Int32? UltimoCambioStatoIdUtente { get; set; }

        public Boolean IsRettifica
        {
            get { return !String.IsNullOrEmpty(IdComunicazionePrecedente) || IdDichiarazionePrecedente.HasValue; }
        }

        public Boolean IsRettificata
        {
            get { return  !String.IsNullOrEmpty(IdComunicazioneSuccessiva) || IdDichiarazioneSuccessiva.HasValue; }
        }

    }
}