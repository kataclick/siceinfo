using System;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class Consulente
    {
        public Int32 IdConsulente { get; set; }

        public String RagioneSociale { get; set; }

        public String CodiceFiscale { get; set; }

        public String Telefono { get; set; }

        public String Fax { get; set; }

        public String Email { get; set; }
    }
}