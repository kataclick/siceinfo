﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class SintesiServiceResult
    {
        public SintesiServiceErrors Error {set; get;}

        public String Message { set; get; }
    }
}
