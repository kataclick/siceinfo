using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Filters
{
    public class DichiarazioneFilter
    {
        public DateTime? DataInvioDa { get; set; }

        public DateTime? DataInvioA { get; set; }

        public TipoAttivita? TipoAttivita { get; set; }

        public String CodiceImpresa { get; set; }

        public String PartitaIVA { get; set; }

        public String Cognome { get; set; }

        public String CodiceFiscale { get; set; }

        public TipoStatoGestionePratica? StatoGestionePratica { get; set; }

        public Int32? IdDichiarazione { get; set; }

        public String IdNazionalita { set; get; }

        public Boolean DuplicazioneCfUgualeDatiDiversi { set; get; }

        public Boolean DuplicazioneCfUgualeDatiUguali { set; get; }

        public Boolean DuplicazioneCfDiversoDatiUguali { set; get; }

        public Boolean DuplicazioneCfUgualeRapportoAperto { set; get; }
    }
}