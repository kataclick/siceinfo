using System;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Filters
{
    public class LavoratoreFilter
    {
        public Int32? Codice { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CodiceFiscale { get; set; }
    }
}