using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Cantieri.Type;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cantieri.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Type;
using CassaEdile = TBridge.Cemi.Type.Entities.CassaEdile;
using CassaEdileCollection = TBridge.Cemi.Type.Collections.CassaEdileCollection;
using Committente = TBridge.Cemi.Cantieri.Type.Entities.Committente;
using Impresa = TBridge.Cemi.Cantieri.Type.Entities.Impresa;
using Ispettore = TBridge.Cemi.Cantieri.Type.Entities.Ispettore;
using Lavoratore = TBridge.Cemi.Cantieri.Type.Entities.Lavoratore;

namespace TBridge.Cemi.Cantieri.Data
{
    public class CantieriDataAccess
    {
        private Database databaseCemi;

        public CantieriDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        #region GetCantieri con DataSet

        ///// <summary>
        ///// Restituisce la lista dei cantieri presenti nel database
        ///// </summary>
        ///// <returns></returns>
        //public CantiereCollection GetCantieri(int? idCantiereParam, string provinciaParam, string comuneParam,
        //                                      string indirizzoParam, double? importoParam, string stringExpression,
        //                                      string sortDirection, int? idZonaParam, bool senzaCoordinate,
        //                                      string impresa, string committente)
        //{
        //    CantiereCollection listaCantieri;
        //    DbCommand comando;

        //    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelect");
        //    if (idCantiereParam.HasValue)
        //        DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiereParam.Value);
        //    if (!string.IsNullOrEmpty(provinciaParam))
        //        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, provinciaParam);
        //    if (!string.IsNullOrEmpty(comuneParam))
        //        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comuneParam);
        //    if (!string.IsNullOrEmpty(indirizzoParam))
        //        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoParam);
        //    if (importoParam.HasValue)
        //        DatabaseCemi.AddInParameter(comando, "@importo", DbType.Double, importoParam.Value);
        //    if (idZonaParam.HasValue)
        //        DatabaseCemi.AddInParameter(comando, "@idZona", DbType.Int32, idZonaParam.Value);
        //    if (senzaCoordinate)
        //        DatabaseCemi.AddInParameter(comando, "@senzaCoordinate", DbType.Boolean, true);
        //    if (!string.IsNullOrEmpty(impresa))
        //        DatabaseCemi.AddInParameter(comando, "@impresa", DbType.String, impresa);
        //    if (!string.IsNullOrEmpty(committente))
        //        DatabaseCemi.AddInParameter(comando, "@committente", DbType.String, committente);

        //    DataSet dsCantieri = databaseCemi.ExecuteDataSet(comando);
        //    listaCantieri = CreaListaDaDataSetCantieri(dsCantieri, stringExpression, sortDirection);

        //    return listaCantieri;
        //}

        //private CantiereCollection CreaListaDaDataSetCantieri(DataSet dsCantieri, string stringExpression,
        //                                                      string sortDirection)
        //{
        //    CantiereCollection listaCantieri = new CantiereCollection();

        //    if ((dsCantieri != null) && (dsCantieri.Tables.Count == 1))
        //    {
        //        if (stringExpression != null && sortDirection != null)
        //        {
        //            dsCantieri.Tables[0].DefaultView.Sort = stringExpression + " " + sortDirection;
        //        }

        //        for (int i = 0; i < dsCantieri.Tables[0].DefaultView.Count; i++)
        //        {
        //            int idCantiere;
        //            string provincia = null;
        //            string comune = null;
        //            string cap = null;
        //            string indirizzo;
        //            string civico = null;
        //            string impresaAppaltatriceTrovata = null;
        //            string permessoCostruire = null;
        //            double? importo = null;
        //            DateTime? dataInizioLavori = null;
        //            DateTime? dataFineLavori = null;
        //            TipologiaAppalto tipologiaAppalto;
        //            bool attivo;
        //            int? idCommittente = null;
        //            string committenteTrovato = null;
        //            int? idImpresa = null;
        //            int? idCantieriImpresa = null;
        //            string tipoImpresaAppaltatrice = null;
        //            double? latitudine = null;
        //            double? longitudine = null;
        //            string direzioneLavori = null;
        //            string responsabileProcedimento = null;
        //            string responsabileCantiere = null;
        //            string descrizioneLavori = null;
        //            int fonteAEM;
        //            int fonteASLE;
        //            int fonteCPT;
        //            string fonti = string.Empty;

        //            idCantiere = (int) dsCantieri.Tables[0].DefaultView[i]["idCantiere"];
        //            indirizzo = (string) dsCantieri.Tables[0].DefaultView[i]["indirizzo"];

        //            if (dsCantieri.Tables[0].DefaultView[i]["numero"] != DBNull.Value)
        //                civico = (string) dsCantieri.Tables[0].DefaultView[i]["numero"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["comune"] != DBNull.Value)
        //                comune = (string) dsCantieri.Tables[0].DefaultView[i]["comune"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["provincia"] != DBNull.Value)
        //                provincia = (string) dsCantieri.Tables[0].DefaultView[i]["provincia"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["cap"] != DBNull.Value)
        //                cap = (string) dsCantieri.Tables[0].DefaultView[i]["cap"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["impresaAppaltatrice"] != DBNull.Value)
        //                impresaAppaltatriceTrovata = (string) dsCantieri.Tables[0].DefaultView[i]["impresaAppaltatrice"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["dataFineLavori"] != DBNull.Value)
        //                dataFineLavori = (DateTime) dsCantieri.Tables[0].DefaultView[i]["dataFineLavori"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["dataInizioLavori"] != DBNull.Value)
        //                dataInizioLavori = (DateTime) dsCantieri.Tables[0].DefaultView[i]["dataInizioLavori"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["permessoCostruire"] != DBNull.Value)
        //                permessoCostruire = (string) dsCantieri.Tables[0].DefaultView[i]["permessoCostruire"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["importo"] != DBNull.Value)
        //                importo = decimal.ToDouble((decimal) dsCantieri.Tables[0].DefaultView[i]["importo"]);
        //            if (dsCantieri.Tables[0].DefaultView[i]["permessoCostruire"] != DBNull.Value)
        //                permessoCostruire = (string) dsCantieri.Tables[0].DefaultView[i]["permessoCostruire"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["idCantieriCommittente"] != DBNull.Value)
        //                idCommittente = (int) dsCantieri.Tables[0].DefaultView[i]["idCantieriCommittente"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["committente"] != DBNull.Value)
        //                committenteTrovato = (string) dsCantieri.Tables[0].DefaultView[i]["committente"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["idImpresa"] != DBNull.Value)
        //                idImpresa = (int) dsCantieri.Tables[0].DefaultView[i]["idImpresa"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["idCantieriImpresa"] != DBNull.Value)
        //                idCantieriImpresa = (int) dsCantieri.Tables[0].DefaultView[i]["idCantieriImpresa"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["tipoImpresaAppaltatrice"] != DBNull.Value)
        //                tipoImpresaAppaltatrice =
        //                    (string) dsCantieri.Tables[0].DefaultView[i]["tipoImpresaAppaltatrice"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["latitudine"] != DBNull.Value)
        //                latitudine = decimal.ToDouble((decimal) dsCantieri.Tables[0].DefaultView[i]["latitudine"]);
        //            if (dsCantieri.Tables[0].DefaultView[i]["longitudine"] != DBNull.Value)
        //                longitudine = decimal.ToDouble((decimal) dsCantieri.Tables[0].DefaultView[i]["longitudine"]);
        //            if (dsCantieri.Tables[0].DefaultView[i]["direzioneLavori"] != DBNull.Value)
        //                direzioneLavori = (string) dsCantieri.Tables[0].DefaultView[i]["direzioneLavori"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["responsabileProcedimento"] != DBNull.Value)
        //                responsabileProcedimento =
        //                    (string) dsCantieri.Tables[0].DefaultView[i]["responsabileProcedimento"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["responsabileCantiere"] != DBNull.Value)
        //                responsabileCantiere = (string) dsCantieri.Tables[0].DefaultView[i]["responsabileCantiere"];
        //            if (dsCantieri.Tables[0].DefaultView[i]["descrizioneLavori"] != DBNull.Value)
        //                descrizioneLavori = (string) dsCantieri.Tables[0].DefaultView[i]["descrizioneLavori"];

        //            tipologiaAppalto = (TipologiaAppalto) dsCantieri.Tables[0].DefaultView[i]["tipologiaAppalto"];
        //            attivo = (bool) dsCantieri.Tables[0].DefaultView[i]["attivo"];

        //            fonteAEM = (int) dsCantieri.Tables[0].DefaultView[i]["fonteAEM"];
        //            fonteASLE = (int) dsCantieri.Tables[0].DefaultView[i]["fonteASLE"];
        //            fonteCPT = (int) dsCantieri.Tables[0].DefaultView[i]["fonteCPT"];

        //            if (fonteAEM > 0)
        //                if (fonti.Length == 0)
        //                    fonti += "AEM";
        //                else
        //                    fonti += ", AEM";
        //            if (fonteASLE > 0)
        //                if (fonti.Length == 0)
        //                    fonti += "ASLE";
        //                else
        //                    fonti += ", ASLE";
        //            if (fonteCPT > 0)
        //                if (fonti.Length == 0)
        //                    fonti += "CPT";
        //                else
        //                    fonti += ", CPT";
        //            if (fonti.Length == 0)
        //                fonti = "CE";

        //            Committente committente = null;
        //            if (idCommittente.HasValue)
        //            {
        //                // E' presente un committente.. Lo recupero
        //                string ragioneSocialeCom;
        //                string partitaIvaCom = null;
        //                string codiceFiscaleCom = null;
        //                string indirizzoCom = null;
        //                string provinciaCom = null;
        //                string comuneCom = null;
        //                string capCom = null;

        //                ragioneSocialeCom = (string) dsCantieri.Tables[0].DefaultView[i]["committenteRagioneSociale"];

        //                if (dsCantieri.Tables[0].DefaultView[i]["committentePartitaIva"] != DBNull.Value)
        //                    partitaIvaCom = (string) dsCantieri.Tables[0].DefaultView[i]["committentePartitaIva"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["committenteCodiceFiscale"] != DBNull.Value)
        //                    codiceFiscaleCom = (string) dsCantieri.Tables[0].DefaultView[i]["committenteCodiceFiscale"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["committenteIndirizzo"] != DBNull.Value)
        //                    indirizzoCom = (string) dsCantieri.Tables[0].DefaultView[i]["committenteIndirizzo"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["committenteProvincia"] != DBNull.Value)
        //                    provinciaCom = (string) dsCantieri.Tables[0].DefaultView[i]["committenteProvincia"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["committenteComune"] != DBNull.Value)
        //                    comuneCom = (string) dsCantieri.Tables[0].DefaultView[i]["committenteComune"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["committenteCap"] != DBNull.Value)
        //                    capCom = (string) dsCantieri.Tables[0].DefaultView[i]["committenteCap"];

        //                committente = new Committente(idCommittente, ragioneSocialeCom, partitaIvaCom,
        //                                              codiceFiscaleCom, indirizzoCom, comuneCom, provinciaCom, capCom);
        //            }

        //            Impresa impresaAppaltatrice = null;
        //            if (idImpresa.HasValue)
        //            {
        //                // E' presente un'impresa SiceNew. La recupero
        //                string ragioneSocialeImp;
        //                string partitaIvaImp = null;
        //                string codiceFiscaleImp = null;
        //                string indirizzoImp = null;
        //                string provinciaImp = null;
        //                string comuneImp = null;
        //                string capImp = null;
        //                TipologiaImpresa tipoImpresa = TipologiaImpresa.SiceNew;

        //                ragioneSocialeImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaRagioneSociale"];

        //                if (dsCantieri.Tables[0].DefaultView[i]["impresaPartitaIva"] != DBNull.Value)
        //                    partitaIvaImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaPartitaIva"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["impresaCodiceFiscale"] != DBNull.Value)
        //                    codiceFiscaleImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaCodiceFiscale"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["impresaIndirizzo"] != DBNull.Value)
        //                    indirizzoImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaIndirizzo"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["impresaProvincia"] != DBNull.Value)
        //                    provinciaImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaProvincia"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["impresaComune"] != DBNull.Value)
        //                    comuneImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaComune"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["impresaCap"] != DBNull.Value)
        //                    capImp = (string) dsCantieri.Tables[0].DefaultView[i]["impresaCap"];

        //                impresaAppaltatrice = new Impresa(tipoImpresa, idImpresa, ragioneSocialeImp, indirizzoImp,
        //                                                  provinciaImp, comuneImp, capImp, partitaIvaImp,
        //                                                  codiceFiscaleImp);

        //                //if (impresaAppaltatrice != null && impresaAppaltatrice.IdImpresa.HasValue && conLavoratori)
        //                //{
        //                //    // Recupero anche i lavoratori
        //                //    impresaAppaltatrice.Lavoratori = GetLavoratori(impresaAppaltatrice);
        //                //}
        //            }
        //            else if (idCantieriImpresa.HasValue)
        //            {
        //                // E' presente un'impresa Cantieri. La recupero
        //                string ragioneSocialeImp;
        //                string partitaIvaImp = null;
        //                string codiceFiscaleImp = null;
        //                string indirizzoImp = null;
        //                string provinciaImp = null;
        //                string comuneImp = null;
        //                string capImp = null;
        //                TipologiaImpresa tipoImpresa = TipologiaImpresa.Cantieri;

        //                ragioneSocialeImp =
        //                    (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaRagioneSociale"];

        //                if (dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaPartitaIva"] != DBNull.Value)
        //                    partitaIvaImp = (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaPartitaIva"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaCodiceFiscale"] != DBNull.Value)
        //                    codiceFiscaleImp =
        //                        (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaCodiceFiscale"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaIndirizzo"] != DBNull.Value)
        //                    indirizzoImp = (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaIndirizzo"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaProvincia"] != DBNull.Value)
        //                    provinciaImp = (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaProvincia"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaComune"] != DBNull.Value)
        //                    comuneImp = (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaComune"];
        //                if (dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaCap"] != DBNull.Value)
        //                    capImp = (string) dsCantieri.Tables[0].DefaultView[i]["cantieriImpresaCap"];

        //                impresaAppaltatrice =
        //                    new Impresa(tipoImpresa, idCantieriImpresa, ragioneSocialeImp, indirizzoImp,
        //                                provinciaImp, comuneImp, capImp, partitaIvaImp, codiceFiscaleImp);
        //            }

        //            listaCantieri.Add(
        //                new Cantiere(idCantiere, provincia, comune, cap, indirizzo, civico, impresaAppaltatriceTrovata,
        //                             permessoCostruire, importo, dataInizioLavori, dataFineLavori, tipologiaAppalto,
        //                             attivo, committente,
        //                             committenteTrovato, impresaAppaltatrice, latitudine, longitudine, direzioneLavori,
        //                             responsabileProcedimento,
        //                             responsabileCantiere, descrizioneLavori, tipoImpresaAppaltatrice, fonti));
        //        }
        //    }

        //    return listaCantieri;
        //}

        #endregion

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        /// <summary>
        /// Restituisce la lista dei cantieri presenti nel database
        /// </summary>
        /// <returns></returns>
        public CantiereCollection GetCantieriReader(CantieriFilter filtro, string stringExpression,
                                                    string sortDirection, bool senzaCoordinate)
        {
            CantiereCollection listaCantieri;
            DbCommand comando;

            switch (stringExpression)
            {
                case "Provincia":
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelectOrderProvincia");
                    if (sortDirection == "DESC")
                        DatabaseCemi.AddInParameter(comando, "@asc", DbType.Boolean, false);
                    break;
                case "Comune":
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelectOrderComune");
                    if (sortDirection == "DESC")
                        DatabaseCemi.AddInParameter(comando, "@asc", DbType.Boolean, false);
                    break;
                case "Indirizzo":
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelectOrderIndirizzo");
                    if (sortDirection == "DESC")
                        DatabaseCemi.AddInParameter(comando, "@asc", DbType.Boolean, false);
                    break;
                case "Importo":
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelectOrderImporto");
                    if (sortDirection == "DESC")
                        DatabaseCemi.AddInParameter(comando, "@asc", DbType.Boolean, false);
                    break;
                default:
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelect");
                    break;
            }

            if (filtro.IdCantiere.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filtro.IdCantiere.Value);
            if (!string.IsNullOrEmpty(filtro.Provincia))
                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, filtro.Provincia);
            if (!string.IsNullOrEmpty(filtro.Comune))
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.Comune);
            if (!string.IsNullOrEmpty(filtro.Indirizzo))
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filtro.Indirizzo);
            if (filtro.Importo.HasValue)
                DatabaseCemi.AddInParameter(comando, "@importo", DbType.Double, filtro.Importo.Value);
            if (filtro.IdZona.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idZona", DbType.Int32, filtro.IdZona.Value);
            if (senzaCoordinate)
                DatabaseCemi.AddInParameter(comando, "@senzaCoordinate", DbType.Boolean, true);
            if (filtro.IdImpresa.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
            if (!string.IsNullOrEmpty(filtro.Impresa))
                DatabaseCemi.AddInParameter(comando, "@impresa", DbType.String, filtro.Impresa);
            if (!string.IsNullOrEmpty(filtro.CodiceFiscaleImpresa))
                DatabaseCemi.AddInParameter(comando, "@codFiscImpresa", DbType.String, filtro.CodiceFiscaleImpresa);
            if (!string.IsNullOrEmpty(filtro.Committente))
                DatabaseCemi.AddInParameter(comando, "@committente", DbType.String, filtro.Committente);
            //if (!string.IsNullOrEmpty(filtro.ImpresaSubappalto))
            //    DatabaseCemi.AddInParameter(comando, "@subappalto", DbType.String, filtro.ImpresaSubappalto);
            if (filtro.Segnalati.HasValue)
                DatabaseCemi.AddInParameter(comando, "@segnalati", DbType.Boolean, filtro.Segnalati.Value);
            if (filtro.Assegnati.HasValue)
                DatabaseCemi.AddInParameter(comando, "@assegnati", DbType.Int32, (Int32) filtro.Assegnati.Value);
            if (filtro.PresaInCarico.HasValue)
                DatabaseCemi.AddInParameter(comando, "@presoInCarico", DbType.Int32, (Int32) filtro.PresaInCarico.Value);
            if (filtro.IdIspettoreAssegnato.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idIspettoreAssegnato", DbType.Int32, (Int32) filtro.IdIspettoreAssegnato.Value);
            if (filtro.IdIspettorePresoInCarico.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idIspettorePresoInCarico", DbType.Int32, (Int32) filtro.IdIspettorePresoInCarico.Value);
            DatabaseCemi.AddInParameter(comando, "@rapportoIspezione", DbType.Int32, filtro.RapportoIspezione);
            DatabaseCemi.AddInParameter(comando, "@nonCollegatiAttivita", DbType.Boolean, filtro.NonCollegatiAttivita);
            if (!String.IsNullOrEmpty(filtro.Cap))
                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, filtro.Cap);
            if (filtro.DataInserimentoDal.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataInserimentoDal", DbType.DateTime, filtro.DataInserimentoDal.Value);
            if (filtro.DataInserimentoAl.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataInserimentoAl", DbType.DateTime, filtro.DataInserimentoAl.Value);
            if (filtro.DataSegnalazioneDal.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataSegnalazioneDal", DbType.DateTime, filtro.DataSegnalazioneDal.Value);
            if (filtro.DataSegnalazioneAl.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataSegnalazioneAl", DbType.DateTime, filtro.DataSegnalazioneAl.Value);
            if (filtro.DataAssegnazioneDal.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataAssegnazioneDal", DbType.DateTime, filtro.DataAssegnazioneDal.Value);
            if (filtro.DataAssegnazioneAl.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataAssegnazioneAl", DbType.DateTime, filtro.DataAssegnazioneAl.Value);
            if (filtro.DataPresaCaricoDal.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataPresaCaricoDal", DbType.DateTime, filtro.DataPresaCaricoDal.Value);
            if (filtro.DataPresaCaricoAl.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataPresaCaricoAl", DbType.DateTime, filtro.DataPresaCaricoAl.Value);

            using (IDataReader reader = databaseCemi.ExecuteReader(comando))
            {
                listaCantieri = CreaListaDaReaderCantieri(reader);
            }

            return listaCantieri;
        }

        private static CantiereCollection CreaListaDaReaderCantieri(IDataReader reader)
        {
            CantiereCollection listaCantieri = new CantiereCollection();

            if (reader != null)
            {
                #region Indici per reader
                Int32 indiceSegnalazioneId = reader.GetOrdinal("idCantieriSegnalazione");
                Int32 indiceSegnalazioneData = reader.GetOrdinal("segnalazioneData");
                Int32 indiceSegnalazioneRicorrente = reader.GetOrdinal("segnalazioneRicorrente");
                Int32 indiceSegnalazioneMotivazioneId = reader.GetOrdinal("idCantieriSegnalazioneMotivazione");
                Int32 indiceSegnalazioneMotivazioneDescrizione = reader.GetOrdinal("segnalazioneMotivazioneDescrizione");
                Int32 indiceSegnalazionePrioritaId = reader.GetOrdinal("idCantieriSegnalazionePriorita");
                Int32 indiceSegnalazionePrioritaDescrizione = reader.GetOrdinal("segnalazionePrioritaDescrizione");
                Int32 indiceSegnalazioneIdUtente = reader.GetOrdinal("segnalazioneIdUtente");
                Int32 indiceSegnalazioneNomeUtente = reader.GetOrdinal("segnalazioneLogin");
                Int32 indiceIdSegnalazioneSuccessiva = reader.GetOrdinal("idSegnalazioneSuccessiva");
                Int32 indiceSegnalazioneNote = reader.GetOrdinal("segnalazioneNote");

                Int32 indiceAssegnazioneId = reader.GetOrdinal("idCantieriAssegnazione");
                Int32 indiceAssegnazioneData = reader.GetOrdinal("assegnazioneData");
                Int32 indiceAssegnazioneMotivazioneId = reader.GetOrdinal("idCantieriAssegnazioneMotivazione");
                Int32 indiceAssegnazioneMotivazioneDescrizione = reader.GetOrdinal("assegnazioneMotivazioneDescrizione");
                Int32 indiceAssegnazionePrioritaId = reader.GetOrdinal("idCantieriAssegnazionePriorita");
                Int32 indiceAssegnazionePrioritaDescrizione = reader.GetOrdinal("assegnazionePrioritaDescrizione");

                Int32 indicePresaInCaricoId = reader.GetOrdinal("idCantieriCalendarioAttivita");
                Int32 indicePresaInCaricoData = reader.GetOrdinal("presaInCaricoData");
                Int32 indicePresaInCaricoDurata = reader.GetOrdinal("presaInCaricoDurata");
                Int32 indicePresaInCaricoDescrizione = reader.GetOrdinal("presaInCaricoDescrizione");
                Int32 indicePresaInCaricoIdIspettore = reader.GetOrdinal("presaInCaricoIdIspettore");
                Int32 indicePresaInCaricoIspettoreCognome = reader.GetOrdinal("presaInCaricoIspettoreCognome");
                Int32 indicePresaInCaricoIspettoreNome = reader.GetOrdinal("presaInCaricoIspettoreNome");
                Int32 indicePresaInCaricoDataPresaInCarico = reader.GetOrdinal("presaInCaricoDataPresaInCarico");
                Int32 indicePresaInCaricoRifiutoId = reader.GetOrdinal("idCantieriCalendarioAttivitaRifiuto");
                Int32 indicePresaInCaricoRifiutoDescrizione = reader.GetOrdinal("presaInCaricoRifiutoDescrizione");

                Int32 indiceIspezioneStato = reader.GetOrdinal("ispezioneStato");

                Int32 indiceNotificaRiferimento = reader.GetOrdinal("notificaRiferimento");
                Int32 indiceProtocolloRegionaleNotifica = reader.GetOrdinal("protocolloRegionaleNotifica");
                #endregion

                while (reader.Read())
                {
                    int idCantiere;
                    string provincia = null;
                    string comune = null;
                    string cap = null;
                    string indirizzo;
                    string civico = null;
                    string impresaAppaltatriceTrovata = null;
                    string permessoCostruire = null;
                    double? importo = null;
                    DateTime? dataInizioLavori = null;
                    DateTime? dataFineLavori = null;
                    TipologiaAppalto tipologiaAppalto;
                    bool attivo;
                    int? idCommittente = null;
                    string committenteTrovato = null;
                    int? idImpresa = null;
                    int? idCantieriImpresa = null;
                    string tipoImpresaAppaltatrice = null;
                    double? latitudine = null;
                    double? longitudine = null;
                    string direzioneLavori = null;
                    string responsabileProcedimento = null;
                    string responsabileCantiere = null;
                    string descrizioneLavori = null;
                    int fonteAEM;
                    int fonteASLE;
                    int fonteCPT;
                    int fonteNotifiche;
                    string fonti = string.Empty;
                    int tempOrdinal;

                    idCantiere = reader.GetInt32(reader.GetOrdinal("idCantiere"));
                    indirizzo = reader.GetString(reader.GetOrdinal("indirizzo"));

                    // Numero
                    tempOrdinal = reader.GetOrdinal("numero");
                    if (!reader.IsDBNull(tempOrdinal))
                        civico = reader.GetString(tempOrdinal);

                    // Comune
                    tempOrdinal = reader.GetOrdinal("comune");
                    if (!reader.IsDBNull(tempOrdinal))
                        comune = reader.GetString(tempOrdinal);

                    // Provincia
                    tempOrdinal = reader.GetOrdinal("provincia");
                    if (!reader.IsDBNull(tempOrdinal))
                        provincia = reader.GetString(tempOrdinal);

                    // Cap
                    tempOrdinal = reader.GetOrdinal("cap");
                    if (!reader.IsDBNull(tempOrdinal))
                        cap = reader.GetString(tempOrdinal);

                    // Impresa appaltatrice
                    tempOrdinal = reader.GetOrdinal("impresaAppaltatrice");
                    if (!reader.IsDBNull(tempOrdinal))
                        impresaAppaltatriceTrovata = reader.GetString(tempOrdinal);

                    // Data fine lavori
                    tempOrdinal = reader.GetOrdinal("dataFineLavori");
                    if (!reader.IsDBNull(tempOrdinal))
                        dataFineLavori = reader.GetDateTime(tempOrdinal);

                    // Data inizio lavori
                    tempOrdinal = reader.GetOrdinal("dataInizioLavori");
                    if (!reader.IsDBNull(tempOrdinal))
                        dataInizioLavori = reader.GetDateTime(tempOrdinal);

                    // Permesso costruire
                    tempOrdinal = reader.GetOrdinal("permessoCostruire");
                    if (!reader.IsDBNull(tempOrdinal))
                        permessoCostruire = reader.GetString(tempOrdinal);

                    // Importo
                    tempOrdinal = reader.GetOrdinal("importo");
                    if (!reader.IsDBNull(tempOrdinal))
                        importo = decimal.ToDouble(reader.GetDecimal(tempOrdinal));

                    // Permesso costruire
                    tempOrdinal = reader.GetOrdinal("permessoCostruire");
                    if (!reader.IsDBNull(tempOrdinal))
                        permessoCostruire = reader.GetString(tempOrdinal);

                    // Id committente
                    tempOrdinal = reader.GetOrdinal("idCantieriCommittente");
                    if (!reader.IsDBNull(tempOrdinal))
                        idCommittente = reader.GetInt32(tempOrdinal);

                    // Committente
                    tempOrdinal = reader.GetOrdinal("committente");
                    if (!reader.IsDBNull(tempOrdinal))
                        committenteTrovato = reader.GetString(tempOrdinal);

                    // Id Impresa
                    tempOrdinal = reader.GetOrdinal("idImpresa");
                    if (!reader.IsDBNull(tempOrdinal))
                        idImpresa = reader.GetInt32(tempOrdinal);

                    // Id Cantieri Impresa
                    tempOrdinal = reader.GetOrdinal("idCantieriImpresa");
                    if (!reader.IsDBNull(tempOrdinal))
                        idCantieriImpresa = reader.GetInt32(tempOrdinal);

                    // Tipo impresa appaltatrice
                    tempOrdinal = reader.GetOrdinal("tipoImpresaAppaltatrice");
                    if (!reader.IsDBNull(tempOrdinal))
                        tipoImpresaAppaltatrice = reader.GetString(tempOrdinal);

                    // Latitudine
                    tempOrdinal = reader.GetOrdinal("latitudine");
                    if (!reader.IsDBNull(tempOrdinal))
                        latitudine = decimal.ToDouble(reader.GetDecimal(tempOrdinal));

                    // Longitudine
                    tempOrdinal = reader.GetOrdinal("longitudine");
                    if (!reader.IsDBNull(tempOrdinal))
                        longitudine = decimal.ToDouble(reader.GetDecimal(tempOrdinal));

                    // Direzione lavori
                    tempOrdinal = reader.GetOrdinal("direzioneLavori");
                    if (!reader.IsDBNull(tempOrdinal))
                        direzioneLavori = reader.GetString(tempOrdinal);

                    // Responsabile procedimento
                    tempOrdinal = reader.GetOrdinal("responsabileProcedimento");
                    if (!reader.IsDBNull(tempOrdinal))
                        responsabileProcedimento = reader.GetString(tempOrdinal);

                    // Responsabile cantiere
                    tempOrdinal = reader.GetOrdinal("responsabileCantiere");
                    if (!reader.IsDBNull(tempOrdinal))
                        responsabileCantiere = reader.GetString(tempOrdinal);

                    // Descrizione lavori
                    tempOrdinal = reader.GetOrdinal("descrizioneLavori");
                    if (!reader.IsDBNull(tempOrdinal))
                        descrizioneLavori = reader.GetString(tempOrdinal);

                    tipologiaAppalto = (TipologiaAppalto) reader.GetInt32(reader.GetOrdinal("tipologiaAppalto"));
                    attivo = reader.GetBoolean(reader.GetOrdinal("attivo"));

                    fonteAEM = reader.GetInt32(reader.GetOrdinal("fonteAEM"));
                    fonteASLE = reader.GetInt32(reader.GetOrdinal("fonteASLE"));
                    fonteCPT = reader.GetInt32(reader.GetOrdinal("fonteCPT"));
                    fonteNotifiche = reader.GetInt32(reader.GetOrdinal("fonteNotifiche"));

                    if (fonteAEM > 0)
                        if (fonti.Length == 0)
                            fonti += "AEM";
                        else
                            fonti += ", AEM";
                    if (fonteASLE > 0)
                        if (fonti.Length == 0)
                            fonti += "ASLE";
                        else
                            fonti += ", ASLE";
                    if (fonteCPT > 0)
                        if (fonti.Length == 0)
                            fonti += "CPT";
                        else
                            fonti += ", CPT";
                    if (fonteNotifiche > 0)
                        if (fonti.Length == 0)
                            fonti += "Notifiche";
                        else
                            fonti += ", Notifiche";
                    if (fonti.Length == 0)
                        fonti = "CE";

                    Committente committente = null;
                    if (idCommittente.HasValue)
                    {
                        // E' presente un committente.. Lo recupero
                        committente = new Committente();
                        committente.IdCommittente = idCommittente;
                        committente.RagioneSociale = reader.GetString(reader.GetOrdinal("committenteRagioneSociale"));

                        // Fonte notifica
                        committente.FonteNotifica = reader.GetBoolean(reader.GetOrdinal("committenteFonteNotifica"));
                        committente.Modificato = reader.GetBoolean(reader.GetOrdinal("committenteModificato"));

                        // Partita IVA
                        tempOrdinal = reader.GetOrdinal("committentePartitaIva");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.PartitaIva = reader.GetString(tempOrdinal);

                        // Codice fiscale
                        tempOrdinal = reader.GetOrdinal("committenteCodiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.CodiceFiscale = reader.GetString(tempOrdinal);

                        // Indirizzo
                        tempOrdinal = reader.GetOrdinal("committenteIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.Indirizzo = reader.GetString(tempOrdinal);

                        // Provincia
                        tempOrdinal = reader.GetOrdinal("committenteProvincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.Provincia = reader.GetString(tempOrdinal);

                        // Comune
                        tempOrdinal = reader.GetOrdinal("committenteComune");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.Comune = reader.GetString(tempOrdinal);

                        // Cap
                        tempOrdinal = reader.GetOrdinal("committenteCap");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.Cap = reader.GetString(tempOrdinal);

                        // Persona riferimento
                        tempOrdinal = reader.GetOrdinal("committentePersonaRiferimento");
                        if (!reader.IsDBNull(tempOrdinal))
                            committente.PersonaRiferimento = reader.GetString(tempOrdinal);
                    }

                    Impresa impresaAppaltatrice = null;
                    if (idImpresa.HasValue)
                    {
                        // E' presente un'impresa SiceNew. La recupero
                        string ragioneSocialeImp;
                        string partitaIvaImp = null;
                        string codiceFiscaleImp = null;
                        string indirizzoImp = null;
                        string provinciaImp = null;
                        string comuneImp = null;
                        string capImp = null;
                        TipologiaImpresa tipoImpresa = TipologiaImpresa.SiceNew;

                        ragioneSocialeImp = reader.GetString(reader.GetOrdinal("impresaRagioneSociale"));

                        // Partita IVA
                        tempOrdinal = reader.GetOrdinal("impresaPartitaIva");
                        if (!reader.IsDBNull(tempOrdinal))
                            partitaIvaImp = reader.GetString(tempOrdinal);

                        // Codice fiscale
                        tempOrdinal = reader.GetOrdinal("impresaCodiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            codiceFiscaleImp = reader.GetString(tempOrdinal);

                        // Indirizzo
                        tempOrdinal = reader.GetOrdinal("impresaIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzoImp = reader.GetString(tempOrdinal);

                        // Provincia
                        tempOrdinal = reader.GetOrdinal("impresaProvincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            provinciaImp = reader.GetString(tempOrdinal);

                        // Comune
                        tempOrdinal = reader.GetOrdinal("impresaComune");
                        if (!reader.IsDBNull(tempOrdinal))
                            comuneImp = reader.GetString(tempOrdinal);

                        // Cap
                        tempOrdinal = reader.GetOrdinal("impresaCap");
                        if (!reader.IsDBNull(tempOrdinal))
                            capImp = reader.GetString(tempOrdinal);

                        impresaAppaltatrice = new Impresa(tipoImpresa, idImpresa, ragioneSocialeImp, indirizzoImp,
                                                          provinciaImp, comuneImp, capImp, partitaIvaImp,
                                                          codiceFiscaleImp);

                        // Indirizzo
                        tempOrdinal = reader.GetOrdinal("impresaAmmiIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.AmmiIndirizzo = reader.GetString(tempOrdinal);

                        // Provincia
                        tempOrdinal = reader.GetOrdinal("impresaAmmiProvincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.AmmiProvincia = reader.GetString(tempOrdinal);

                        // Comune
                        tempOrdinal = reader.GetOrdinal("impresaAmmiComune");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.AmmiComune = reader.GetString(tempOrdinal);

                        // Cap
                        tempOrdinal = reader.GetOrdinal("impresaAmmiCap");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.AmmiCap = reader.GetString(tempOrdinal);
                    }
                    else if (idCantieriImpresa.HasValue)
                    {
                        // E' presente un'impresa Cantieri. La recupero
                        string ragioneSocialeImp;
                        string partitaIvaImp = null;
                        string codiceFiscaleImp = null;
                        string indirizzoImp = null;
                        string provinciaImp = null;
                        string comuneImp = null;
                        string capImp = null;
                        TipologiaImpresa tipoImpresa = TipologiaImpresa.Cantieri;

                        ragioneSocialeImp = reader.GetString(reader.GetOrdinal("cantieriImpresaRagioneSociale"));

                        // Partita IVA
                        tempOrdinal = reader.GetOrdinal("cantieriImpresaPartitaIva");
                        if (!reader.IsDBNull(tempOrdinal))
                            partitaIvaImp = reader.GetString(tempOrdinal);

                        // Codice fiscale
                        tempOrdinal = reader.GetOrdinal("cantieriImpresaCodiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            codiceFiscaleImp = reader.GetString(tempOrdinal);

                        // Indirizzo
                        tempOrdinal = reader.GetOrdinal("cantieriImpresaIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzoImp = reader.GetString(tempOrdinal);

                        // Provincia
                        tempOrdinal = reader.GetOrdinal("cantieriImpresaProvincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            provinciaImp = reader.GetString(tempOrdinal);

                        // Comune
                        tempOrdinal = reader.GetOrdinal("cantieriImpresaComune");
                        if (!reader.IsDBNull(tempOrdinal))
                            comuneImp = reader.GetString(tempOrdinal);

                        // Cap
                        tempOrdinal = reader.GetOrdinal("cantieriImpresaCap");
                        if (!reader.IsDBNull(tempOrdinal))
                            capImp = reader.GetString(tempOrdinal);

                        impresaAppaltatrice = new Impresa(tipoImpresa, idCantieriImpresa, ragioneSocialeImp,
                                                          indirizzoImp,
                                                          provinciaImp, comuneImp, capImp, partitaIvaImp,
                                                          codiceFiscaleImp);

                        tempOrdinal = reader.GetOrdinal("cantieriImpresaTelefono");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.Telefono = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("cantieriImpresaFax");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.Fax = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("cantieriImpresaPersonaRiferimento");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.PersonaRiferimento = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("cantieriImpresaTipoAttivita");
                        if (!reader.IsDBNull(tempOrdinal))
                            impresaAppaltatrice.TipoAttivita = reader.GetString(tempOrdinal);

                        impresaAppaltatrice.FonteNotifica =
                            reader.GetBoolean(reader.GetOrdinal("cantieriImpresaFonteNotifica"));
                        impresaAppaltatrice.Modificato =
                            reader.GetBoolean(reader.GetOrdinal("cantieriImpresaModificato"));
                    }

                    Cantiere cantiere = new Cantiere(idCantiere, provincia, comune, cap, indirizzo, civico, impresaAppaltatriceTrovata,
                                     permessoCostruire, importo, dataInizioLavori, dataFineLavori, tipologiaAppalto,
                                     attivo, committente,
                                     committenteTrovato, impresaAppaltatrice, latitudine, longitudine, direzioneLavori,
                                     responsabileProcedimento,
                                     responsabileCantiere, descrizioneLavori, tipoImpresaAppaltatrice, fonti);

                    if (!reader.IsDBNull(indiceNotificaRiferimento))
                    {
                        cantiere.NotificaRiferimento = reader.GetInt32(indiceNotificaRiferimento);
                    }

                    if (!reader.IsDBNull(indiceProtocolloRegionaleNotifica))
                    {
                        cantiere.ProtocolloRegionaleNotifica = reader.GetString(indiceProtocolloRegionaleNotifica);
                    }

                    listaCantieri.Add(cantiere);

                    if (!reader.IsDBNull(indiceIspezioneStato))
                    {
                        cantiere.StatoIspezione = (StatoIspezione) reader.GetInt32(indiceIspezioneStato);
                    }

                    // Segnalazione
                    if (!reader.IsDBNull(indiceSegnalazioneId))
                    {
                        cantiere.Segnalazione = new Segnalazione();

                        cantiere.Segnalazione.IdSegnalazione = reader.GetInt32(indiceSegnalazioneId);
                        cantiere.Segnalazione.IdCantiere = cantiere.IdCantiere.Value;
                        cantiere.Segnalazione.Data = reader.GetDateTime(indiceSegnalazioneData);
                        cantiere.Segnalazione.Ricorrente = reader.GetBoolean(indiceSegnalazioneRicorrente);
                        cantiere.Segnalazione.Motivazione = new Cemi.Type.Domain.CantiereSegnalazioneMotivazione();
                        cantiere.Segnalazione.Motivazione.Id = reader.GetInt32(indiceSegnalazioneMotivazioneId);
                        cantiere.Segnalazione.Motivazione.Descrizione = reader.GetString(indiceSegnalazioneMotivazioneDescrizione);
                        cantiere.Segnalazione.Priorita = new Cemi.Type.Domain.CantiereSegnalazionePriorita();
                        cantiere.Segnalazione.Priorita.Id = reader.GetInt32(indiceSegnalazionePrioritaId);
                        cantiere.Segnalazione.Priorita.Descrizione = reader.GetString(indiceSegnalazionePrioritaDescrizione);
                        cantiere.Segnalazione.IdUtente = reader.GetInt32(indiceSegnalazioneIdUtente);
                        cantiere.Segnalazione.NomeUtente = reader.GetString(indiceSegnalazioneNomeUtente);
                        if (!reader.IsDBNull(indiceIdSegnalazioneSuccessiva))
                        {
                            cantiere.Segnalazione.IdSegnalazioneSuccessiva = reader.GetInt32(indiceIdSegnalazioneSuccessiva);
                        }
                        if (!reader.IsDBNull(indiceSegnalazioneNote))
                        {
                            cantiere.Segnalazione.Note = reader.GetString(indiceSegnalazioneNote);
                        }
                    }

                    // Assegnazione
                    if (!reader.IsDBNull(indiceAssegnazioneId))
                    {
                        cantiere.Assegnazione = new Assegnazione();

                        cantiere.Assegnazione.IdAssegnazione = reader.GetInt32(indiceAssegnazioneId);
                        cantiere.Assegnazione.IdCantiere = cantiere.IdCantiere.Value;
                        cantiere.Assegnazione.Data = reader.GetDateTime(indiceAssegnazioneData);
                        if (!reader.IsDBNull(indiceAssegnazioneMotivazioneId))
                        {
                            cantiere.Assegnazione.MotivazioneRifiuto = new Cemi.Type.Domain.CantieriAssegnazioneMotivazione();
                            cantiere.Assegnazione.MotivazioneRifiuto.Id = reader.GetInt32(indiceAssegnazioneMotivazioneId);
                            cantiere.Assegnazione.MotivazioneRifiuto.Descrizione =
                                reader.GetString(indiceAssegnazioneMotivazioneDescrizione);
                        }
                        if (!reader.IsDBNull(indiceAssegnazionePrioritaId))
                        {
                            cantiere.Assegnazione.Priorita = new Cemi.Type.Domain.CantieriAssegnazionePriorita();
                            cantiere.Assegnazione.Priorita.Id = reader.GetInt32(indiceAssegnazionePrioritaId);
                            cantiere.Assegnazione.Priorita.Descrizione =
                                reader.GetString(indiceAssegnazionePrioritaDescrizione);
                        }
                    }

                    // Presa in carico
                    if (!reader.IsDBNull(indicePresaInCaricoId))
                    {
                        cantiere.PresaInCarico = new Cemi.Type.Domain.CantieriCalendarioAttivita();

                        cantiere.PresaInCarico.Id = reader.GetInt32(indicePresaInCaricoId);
                        cantiere.PresaInCarico.Data = reader.GetDateTime(indicePresaInCaricoData);
                        cantiere.PresaInCarico.Durata = reader.GetInt32(indicePresaInCaricoDurata);
                        cantiere.PresaInCarico.DataPresaInCarico = reader.GetDateTime(indicePresaInCaricoDataPresaInCarico);
                        if (!reader.IsDBNull(indicePresaInCaricoDescrizione))
                        {
                            cantiere.PresaInCarico.Descrizione = reader.GetString(indicePresaInCaricoDescrizione);
                        }

                        cantiere.PresaInCarico.Ispettore = new Cemi.Type.Domain.Ispettore();
                        cantiere.PresaInCarico.Ispettore.IdIspettore = reader.GetInt32(indicePresaInCaricoIdIspettore);
                        cantiere.PresaInCarico.Ispettore.Cognome = reader.GetString(indicePresaInCaricoIspettoreCognome);
                        cantiere.PresaInCarico.Ispettore.Nome = reader.GetString(indicePresaInCaricoIspettoreNome);

                        if (!reader.IsDBNull(indicePresaInCaricoRifiutoId))
                        {
                            cantiere.PresaInCarico.Rifiuto = new Cemi.Type.Domain.CantieriCalendarioAttivitaRifiuti();
                            cantiere.PresaInCarico.Rifiuto.Id = reader.GetInt32(indicePresaInCaricoRifiutoId);
                            cantiere.PresaInCarico.Rifiuto.Descrizione = reader.GetString(indicePresaInCaricoRifiutoDescrizione);
                        }
                    }
                }
            }

            return listaCantieri;
        }

        /*
                private LavoratoreCollection GetLavoratori(Impresa impresaAppaltatrice)
                {
                    LavoratoreCollection listaLavoratori = new LavoratoreCollection();
                    DbCommand comando;

                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelect");
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresaAppaltatrice.IdImpresa.Value);

                    DataSet dsLavoratori = databaseCemi.ExecuteDataSet(comando);
                    if ((dsLavoratori != null) && (dsLavoratori.Tables.Count == 1))
                    {
                        for (int i = 0; i < dsLavoratori.Tables[0].Rows.Count; i++)
                        {
                            TipologiaLavoratore tipoLavoratore = TipologiaLavoratore.SiceNew;
                            int idLavoratore;
                            string cognome;
                            string nome;
                            DateTime? dataNascita = null;

                            idLavoratore = (int) dsLavoratori.Tables[0].Rows[i]["idLavoratore"];
                            cognome = (string) dsLavoratori.Tables[0].Rows[i]["cognome"];
                            nome = (string) dsLavoratori.Tables[0].Rows[i]["nome"];

                            if (dsLavoratori.Tables[0].Rows[i]["dataNascita"] != DBNull.Value)
                                dataNascita = (DateTime) dsLavoratori.Tables[0].Rows[i]["dataNascita"];

                            listaLavoratori.Add(new Lavoratore(tipoLavoratore, idLavoratore, cognome, nome, dataNascita));
                        }
                    }

                    return listaLavoratori;
                }
        */

        /// <summary>
        /// Inserisce un cantiere nel database. In caso di inserimento corretto il campo IdCantiere risulter� valorizzato
        /// </summary>
        /// <param name="cantiere"></param>
        public void InsertCantiere(Cantiere cantiere, DbTransaction transaction)
        {
            if (cantiere != null)
            {
                if (cantiere.IdCantiere == null && cantiere.Indirizzo != null)
                {
                    // Proseguo con l'inserimento
                    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriInsert"))
                    {

                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, cantiere.Indirizzo);
                        if (!String.IsNullOrEmpty(cantiere.Provincia))
                            DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, cantiere.Provincia);
                        if (!String.IsNullOrEmpty(cantiere.Comune))
                            DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, cantiere.Comune);
                        if (!String.IsNullOrEmpty(cantiere.Cap))
                            DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cantiere.Cap);
                        if (!String.IsNullOrEmpty(cantiere.Civico))
                            DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, cantiere.Civico);
                        if (cantiere.PermessoCostruire != null)
                            DatabaseCemi.AddInParameter(comando, "@permessoCostruire", DbType.String,
                                                        cantiere.PermessoCostruire);
                        if (cantiere.Importo != null)
                            DatabaseCemi.AddInParameter(comando, "@importo", DbType.Double, cantiere.Importo);
                        if (cantiere.DataInizioLavori.HasValue)
                            DatabaseCemi.AddInParameter(comando, "@dataInizioLavori", DbType.DateTime,
                                                        cantiere.DataInizioLavori.Value);
                        if (cantiere.DataFineLavori.HasValue)
                            DatabaseCemi.AddInParameter(comando, "@dataFineLavori", DbType.DateTime,
                                                        cantiere.DataFineLavori.Value);
                        if (cantiere.DirezioneLavori != null)
                            DatabaseCemi.AddInParameter(comando, "@direzioneLavori", DbType.String, cantiere.DirezioneLavori);
                        if (cantiere.ResponsabileProcedimento != null)
                            DatabaseCemi.AddInParameter(comando, "@responsabileProcedimento", DbType.String,
                                                        cantiere.ResponsabileProcedimento);
                        if (cantiere.ResponsabileCantiere != null)
                            DatabaseCemi.AddInParameter(comando, "@responsabileCantiere", DbType.String,
                                                        cantiere.ResponsabileCantiere);
                        if (cantiere.DescrizioneLavori != null)
                            DatabaseCemi.AddInParameter(comando, "@descrizioneLavori", DbType.String,
                                                        cantiere.DescrizioneLavori);
                        if (cantiere.TipoImpresaAppaltatrice != null)
                            DatabaseCemi.AddInParameter(comando, "@tipoImpresaAppaltatrice", DbType.String,
                                                        cantiere.TipoImpresaAppaltatrice);

                        DatabaseCemi.AddInParameter(comando, "@tipologiaAppalto", DbType.Int32, cantiere.TipologiaAppalto);
                        DatabaseCemi.AddInParameter(comando, "@attivo", DbType.Boolean, cantiere.Attivo);

                        if (cantiere.Latitudine.HasValue)
                        {
                            DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Double, cantiere.Latitudine);
                        }
                        if (cantiere.Longitudine.HasValue)
                        {
                            DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Double, cantiere.Longitudine);
                        }

                        if (cantiere.NotificaRiferimento.HasValue)
                        {
                            DatabaseCemi.AddInParameter(comando, "@notificaRiferimento", DbType.Int32, cantiere.NotificaRiferimento);
                        }

                        if (!String.IsNullOrWhiteSpace(cantiere.ProtocolloRegionaleNotifica))
                        {
                            DatabaseCemi.AddInParameter(comando, "@protocolloRegionale", DbType.String, cantiere.ProtocolloRegionaleNotifica);
                        }

                        // Committente
                        if (cantiere.Committente != null)
                            DatabaseCemi.AddInParameter(comando, "@idCantieriCommittente", DbType.Int32,
                                                        cantiere.Committente.IdCommittente.Value);

                        // Impresa appaltatrice
                        if (cantiere.ImpresaAppaltatrice != null)
                        {
                            if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.SiceNew)
                                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                            cantiere.ImpresaAppaltatrice.IdImpresa.Value);
                            else if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                                DatabaseCemi.AddInParameter(comando, "@idCantieriImpresa", DbType.Int32,
                                                            cantiere.ImpresaAppaltatrice.IdImpresa.Value);
                        }

                        decimal res = -1;
                        if (transaction == null)
                        {
                            res = (decimal) databaseCemi.ExecuteScalar(comando);
                        }
                        else
                        {
                            res = (decimal) databaseCemi.ExecuteScalar(comando, transaction);
                        }
                        if (res > 0)
                            cantiere.IdCantiere = decimal.ToInt32(res);
                    }
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();
        }

        /// <summary>
        /// Aggiorna un cantiere gi� presente nel database
        /// </summary>
        /// <param name="cantiere"></param>
        /// <returns></returns>
        public bool UpdateCantiere(Cantiere cantiere)
        {
            bool res = false;

            if (cantiere != null)
            {
                if (cantiere.IdCantiere.HasValue && cantiere.Indirizzo != null)
                {
                    // Proseguo con l'aggiornamento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriUpdate");

                    DatabaseCemi.AddInParameter(comando, "@idCantieriCantiere", DbType.Int32, cantiere.IdCantiere);
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, cantiere.Indirizzo);
                    if (!String.IsNullOrEmpty(cantiere.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, cantiere.Provincia);
                    if (!String.IsNullOrEmpty(cantiere.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, cantiere.Comune);
                    if (!String.IsNullOrEmpty(cantiere.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cantiere.Cap);
                    if (!String.IsNullOrEmpty(cantiere.Civico))
                        DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, cantiere.Civico);
                    if (cantiere.PermessoCostruire != null)
                        DatabaseCemi.AddInParameter(comando, "@permessoCostruire", DbType.String,
                                                    cantiere.PermessoCostruire);
                    if (cantiere.Importo != null)
                        DatabaseCemi.AddInParameter(comando, "@importo", DbType.Double, cantiere.Importo);
                    if (cantiere.DataInizioLavori.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioLavori", DbType.DateTime,
                                                    cantiere.DataInizioLavori.Value);
                    if (cantiere.DataFineLavori.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFineLavori", DbType.DateTime,
                                                    cantiere.DataFineLavori.Value);
                    if (cantiere.DirezioneLavori != null)
                        DatabaseCemi.AddInParameter(comando, "@direzioneLavori", DbType.String, cantiere.DirezioneLavori);
                    if (cantiere.ResponsabileProcedimento != null)
                        DatabaseCemi.AddInParameter(comando, "@responsabileProcedimento", DbType.String,
                                                    cantiere.ResponsabileProcedimento);
                    if (cantiere.ResponsabileCantiere != null)
                        DatabaseCemi.AddInParameter(comando, "@responsabileCantiere", DbType.String,
                                                    cantiere.ResponsabileCantiere);
                    if (cantiere.DescrizioneLavori != null)
                        DatabaseCemi.AddInParameter(comando, "@descrizioneLavori", DbType.String,
                                                    cantiere.DescrizioneLavori);
                    if (cantiere.TipoImpresaAppaltatrice != null)
                        DatabaseCemi.AddInParameter(comando, "@tipoImpresaAppaltatrice", DbType.String,
                                                    cantiere.TipoImpresaAppaltatrice);

                    DatabaseCemi.AddInParameter(comando, "@tipologiaAppalto", DbType.Int32, cantiere.TipologiaAppalto);
                    DatabaseCemi.AddInParameter(comando, "@attivo", DbType.Boolean, cantiere.Attivo);

                    if (cantiere.Latitudine.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Double, cantiere.Latitudine);
                    }
                    if (cantiere.Longitudine.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Double, cantiere.Longitudine);
                    }

                    if (cantiere.Committente != null && cantiere.Committente.IdCommittente.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idCantieriCommittente", DbType.Int32,
                                                    cantiere.Committente.IdCommittente);
                    }

                    if (cantiere.ImpresaAppaltatrice != null && cantiere.ImpresaAppaltatrice.IdImpresa.HasValue)
                    {
                        if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                            DatabaseCemi.AddInParameter(comando, "@idCantieriImpresa", DbType.Int32,
                                                        cantiere.ImpresaAppaltatrice.IdImpresa);
                        else
                            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                        cantiere.ImpresaAppaltatrice.IdImpresa);
                    }

                    int resQuery = databaseCemi.ExecuteNonQuery(comando);
                    if (resQuery == 1)
                        res = true;
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();

            return res;
        }

        /// <summary>
        /// Restituisce la lista degli ispettori con le relative zone (senza i cap) presenti nel database
        /// </summary>
        /// <returns></returns>
        public IspettoreCollection GetIspettori(int? idIspettoreParam, bool? operativi)
        {
            IspettoreCollection listaIspettori = new IspettoreCollection();

            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IspettoriSelect");
            if (idIspettoreParam.HasValue)
            {
                DatabaseCemi.AddInParameter(comando, "@idIspettore", DbType.Int32, idIspettoreParam.Value);
            }
            if (!operativi.HasValue || operativi.Value == false)
            {
                DatabaseCemi.AddInParameter(comando, "@soloOperativi", DbType.Boolean, false);
            }

            DataSet dsIspettori = databaseCemi.ExecuteDataSet(comando);
            if ((dsIspettori != null) && (dsIspettori.Tables.Count == 1))
            {
                for (int i = 0; i < dsIspettori.Tables[0].Rows.Count; i++)
                {
                    int idIspettore;
                    string cognome;
                    string nome;
                    int idZona;
                    string nomeZona;
                    string descrizioneZona = null;
                    bool operativo;

                    idIspettore = (int) dsIspettori.Tables[0].Rows[i]["idIspettore"];
                    cognome = (string) dsIspettori.Tables[0].Rows[i]["cognome"];
                    nome = (string) dsIspettori.Tables[0].Rows[i]["nome"];
                    idZona = (int) dsIspettori.Tables[0].Rows[i]["idCantieriZona"];
                    nomeZona = (string) dsIspettori.Tables[0].Rows[i]["nomeZona"];

                    if (dsIspettori.Tables[0].Rows[i]["descrizioneZona"] != DBNull.Value)
                        descrizioneZona = (string) dsIspettori.Tables[0].Rows[i]["descrizioneZona"];

                    operativo = (bool) dsIspettori.Tables[0].Rows[i]["operativo"];

                    listaIspettori.Add(
                        new Ispettore(idIspettore, cognome, nome, idZona, nomeZona, descrizioneZona, operativo));
                }
            }

            return listaIspettori;
        }

        /// <summary>
        /// Restituisce la lista delle zone presenti nel database
        /// </summary>
        /// <returns></returns>
        public ZonaCollection GetZone(int? idZonaParam)
        {
            ZonaCollection listaZone = new ZonaCollection();

            DbCommand comando;

            comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZoneSelect");
            if (idZonaParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idCantieriZona", DbType.Int32, idZonaParam.Value);

            DataSet dsZone = databaseCemi.ExecuteDataSet(comando);
            if ((dsZone != null) && (dsZone.Tables.Count == 1))
            {
                Zona zonaCorr = null;
                CAP capCorr = null;

                for (int i = 0; i < dsZone.Tables[0].Rows.Count; i++)
                {
                    int idZona;
                    string nome;
                    string descrizione = null;

                    idZona = (int) dsZone.Tables[0].Rows[i]["idCantieriZona"];
                    nome = (string) dsZone.Tables[0].Rows[i]["nome"];
                    if (dsZone.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsZone.Tables[0].Rows[i]["descrizione"];

                    if (zonaCorr == null || zonaCorr.IdZona != idZona)
                    {
                        zonaCorr = new Zona(idZona, nome, descrizione, null);
                        listaZone.Add(zonaCorr);
                        capCorr = null;
                    }

                    if (dsZone.Tables[0].Rows[i]["cap"] != DBNull.Value)
                    {
                        string comune = null;
                        string provincia = null;

                        if (dsZone.Tables[0].Rows[i]["comune"] != DBNull.Value)
                            comune = (string) dsZone.Tables[0].Rows[i]["comune"];
                        if (dsZone.Tables[0].Rows[i]["provincia"] != DBNull.Value)
                            provincia = (string) dsZone.Tables[0].Rows[i]["provincia"];

                        string cap = (string) dsZone.Tables[0].Rows[i]["cap"];

                        if (capCorr == null || capCorr.Cap != cap)
                        {
                            capCorr = new CAP(cap);
                            zonaCorr.ListaCap.Add(capCorr);
                        }

                        capCorr.ListaComuni.Add(new CAP(cap, comune, provincia));
                    }
                }
            }

            return listaZone;
        }

        /// <summary>
        /// Inserisce una zona nel database. In caso di inserimento corretto il campo IdZona risulter� valorizzato
        /// </summary>
        /// <param name="zona"></param>
        public bool InsertZona(Zona zona)
        {
            bool doppia = false;

            if (zona != null)
            {
                if (zona.IdZona == null && !string.IsNullOrEmpty(zona.Nome))
                {
                    using (DbConnection connection = databaseCemi.CreateConnection())
                    {
                        connection.Open();

                        using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                        {
                            try
                            {
                                // Proseguo con l'inserimento
                                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZoneInsert");

                                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, zona.Nome);
                                if (zona.Descrizione != null)
                                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, zona.Descrizione);

                                decimal resZona = (decimal) databaseCemi.ExecuteScalar(comando, transaction);
                                int idZonaTemp = decimal.ToInt32(resZona);

                                if (idZonaTemp > 0)
                                {
                                    if (zona.ListaCap != null)
                                    {
                                        foreach (CAP cap in zona.ListaCap)
                                        {
                                            // Inserisco i cap
                                            DbCommand comandoCap =
                                                databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZonaCapInsert");

                                            DatabaseCemi.AddInParameter(comandoCap, "@idCantieriZona", DbType.Int32,
                                                                        idZonaTemp);
                                            DatabaseCemi.AddInParameter(comandoCap, "@cap", DbType.String, cap.Cap);

                                            int numRes = databaseCemi.ExecuteNonQuery(comandoCap, transaction);

                                            if (numRes != 1)
                                                throw new Exception();
                                        }
                                    }

                                    zona.IdZona = decimal.ToInt32(resZona);
                                    transaction.Commit();
                                }
                                else
                                    throw new Exception();
                            }
                            catch (SqlException sqlExc)
                            {
                                // Violazione di nome doppio
                                if (sqlExc.ErrorCode == -2146232060)
                                    doppia = true;
                                transaction.Rollback();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();
                            }
                        }

                        connection.Close();
                    }
                }
            }

            return doppia;
        }

        /// <summary>
        /// Aggiorna una zona gi� presente nel database.
        /// </summary>
        /// <param name="zona"></param>
        public bool UpdateZona(Zona zona)
        {
            bool res = false;

            if (zona != null)
            {
                if (zona.IdZona != null && !string.IsNullOrEmpty(zona.Nome))
                {
                    using (DbConnection connection = databaseCemi.CreateConnection())
                    {
                        connection.Open();

                        using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                        {
                            try
                            {
                                // Proseguo con l'inserimento
                                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZoneUpdate");

                                DatabaseCemi.AddInParameter(comando, "@idCantieriZona", DbType.Int32, zona.IdZona);
                                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, zona.Nome);
                                if (zona.Descrizione != null)
                                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, zona.Descrizione);

                                int nRes = databaseCemi.ExecuteNonQuery(comando, transaction);

                                if (nRes == 1)
                                {
                                    // Cancello i vecchi cap
                                    DbCommand comandoDel =
                                        databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZonaCapDelete");
                                    DatabaseCemi.AddInParameter(comandoDel, "@idCantieriZona", DbType.Int32, zona.IdZona);
                                    databaseCemi.ExecuteNonQuery(comandoDel, transaction);

                                    if (zona.ListaCap != null)
                                    {
                                        foreach (CAP cap in zona.ListaCap)
                                        {
                                            // Aggiorno i cap
                                            DbCommand comandoCap =
                                                databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZonaCapInsert");

                                            DatabaseCemi.AddInParameter(comandoCap, "@idCantieriZona", DbType.Int32,
                                                                        zona.IdZona);
                                            DatabaseCemi.AddInParameter(comandoCap, "@cap", DbType.String, cap.Cap);

                                            int numRes = databaseCemi.ExecuteNonQuery(comandoCap, transaction);

                                            if (numRes != 1)
                                                throw new Exception();
                                        }
                                    }

                                    transaction.Commit();
                                    res = true;
                                }
                                else
                                    throw new Exception();
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();
                            }
                        }
                        connection.Close();
                    }
                }
            }

            return res;
        }

        /// <summary>
        /// Restituisce la programmazione settimanale di un ispettore
        /// </summary>
        /// <param name="dal"></param>
        /// <param name="al"></param>
        /// <param name="idIspettore"></param>
        /// <returns></returns>
        /// <param name="programmatoParam"></param>
        /// <param name="preventivatoParam"></param>
        /// <param name="consuntivatoParam"></param>
        public ProgrammazioneGiornaliereIspettoreCollection GetProgrammazioneSettimanale(DateTime dal, DateTime al,
                                                                                         int idIspettore,
                                                                                         bool programmatoParam,
                                                                                         bool preventivatoParam,
                                                                                         bool consuntivatoParam)
        {
            ProgrammazioneGiornaliereIspettoreCollection programmazione =
                new ProgrammazioneGiornaliereIspettoreCollection();

            DbCommand comando;

            Ispettore ispettore = (GetIspettori(idIspettore, false))[0];
            for (DateTime i = dal; i <= al; i = i.AddDays(1))
            {
                //if (i.DayOfWeek != DayOfWeek.Saturday && i.DayOfWeek != DayOfWeek.Sunday)
                programmazione.Add(new ProgrammazioneGiornalieraIspettore(ispettore, i, new AttivitaCollection()));
            }

            comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriProgrammazioneSelect");
            DatabaseCemi.AddInParameter(comando, "@idIspettore", DbType.Int32, idIspettore);
            DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, dal);
            DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, al);
            DatabaseCemi.AddInParameter(comando, "@programmato", DbType.Boolean, programmatoParam);
            DatabaseCemi.AddInParameter(comando, "@preventivato", DbType.Boolean, preventivatoParam);
            DatabaseCemi.AddInParameter(comando, "@consuntivato", DbType.Boolean, consuntivatoParam);

            DataSet dsProgrammazione = databaseCemi.ExecuteDataSet(comando);
            if ((dsProgrammazione != null) && (dsProgrammazione.Tables.Count == 1))
            {
                //ProgrammazioneGiornalieraIspettore prog = null;

                for (int i = 0; i < dsProgrammazione.Tables[0].Rows.Count; i++)
                {
                    int idAttivita;
                    DateTime giorno;
                    string nomeAttivita;
                    string descrizione = null;
                    Cantiere cantiere = null;
                    bool programmato;
                    bool preventivato;
                    bool consuntivato;

                    idAttivita = (int) dsProgrammazione.Tables[0].Rows[i]["idCantieriProgrammazione"];
                    giorno = (DateTime) dsProgrammazione.Tables[0].Rows[i]["giorno"];
                    TimeSpan ts = giorno - dal;
                    int indice = ts.Days;

                    if (dsProgrammazione.Tables[0].Rows[i]["idCantieriCantiere"] != DBNull.Value)
                    {
                        int idCantiere = (int) dsProgrammazione.Tables[0].Rows[i]["idCantieriCantiere"];
                        CantieriFilter filtro = new CantieriFilter();
                        filtro.IdCantiere = idCantiere;
                        cantiere =
                            GetCantieriReader(filtro, null, null, false)[0];
                    }

                    nomeAttivita = (string) dsProgrammazione.Tables[0].Rows[i]["nomeAttivita"];

                    if (dsProgrammazione.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsProgrammazione.Tables[0].Rows[i]["descrizione"];

                    programmato = (bool) dsProgrammazione.Tables[0].Rows[i]["programmato"];
                    preventivato = (bool) dsProgrammazione.Tables[0].Rows[i]["preventivato"];
                    consuntivato = (bool) dsProgrammazione.Tables[0].Rows[i]["consuntivato"];

                    programmazione[indice].ListaAttivita.Add(
                        new Attivita(idAttivita, giorno, DateTime.Now, nomeAttivita, descrizione, ispettore, cantiere,
                                     programmato, preventivato, consuntivato));
                }
            }

            // Elimino il sabato e la domenica
            int j = 0;
            while (j < programmazione.Count)
                if (programmazione[j].Giorno.DayOfWeek == DayOfWeek.Saturday ||
                    programmazione[j].Giorno.DayOfWeek == DayOfWeek.Sunday)
                    programmazione.RemoveAt(j);
                else
                    j++;

            return programmazione;
        }

        public AttivitaCollection GetProgrammazionePerCantiere(int idCantiere)
        {
            AttivitaCollection programmazione = new AttivitaCollection();
            DbCommand comando;

            comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriProgrammazioneSelectByCantiere");
            DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

            DataSet dsProgrammazione = databaseCemi.ExecuteDataSet(comando);
            if ((dsProgrammazione != null) && (dsProgrammazione.Tables.Count == 1))
            {
                for (int i = 0; i < dsProgrammazione.Tables[0].Rows.Count; i++)
                {
                    int idAttivita;
                    DateTime giorno;
                    string nomeAttivita;
                    string descrizione = null;
                    bool programmato;
                    bool preventivato;
                    bool consuntivato;
                    int idIspettore;
                    Ispettore ispettore;

                    idAttivita = (int) dsProgrammazione.Tables[0].Rows[i]["idCantieriProgrammazione"];
                    giorno = (DateTime) dsProgrammazione.Tables[0].Rows[i]["giorno"];
                    idIspettore = (int) dsProgrammazione.Tables[0].Rows[i]["idIspettore"];
                    ispettore = GetIspettori(idIspettore, null)[0];

                    nomeAttivita = (string) dsProgrammazione.Tables[0].Rows[i]["nomeAttivita"];

                    if (dsProgrammazione.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsProgrammazione.Tables[0].Rows[i]["descrizione"];

                    programmato = (bool) dsProgrammazione.Tables[0].Rows[i]["programmato"];
                    preventivato = (bool) dsProgrammazione.Tables[0].Rows[i]["preventivato"];
                    consuntivato = (bool) dsProgrammazione.Tables[0].Rows[i]["consuntivato"];

                    programmazione.Add(
                        new Attivita(idAttivita, giorno, DateTime.Now, nomeAttivita, descrizione, ispettore, null,
                                     programmato, preventivato, consuntivato));
                }
            }

            return programmazione;
        }

        /// <summary>
        /// Restituisce la lista delle attivita presenti nel database
        /// </summary>
        /// <returns></returns>
        public TipologiaAttivitaCollection GetTipologieAttivita()
        {
            TipologiaAttivitaCollection listaAttivita = new TipologiaAttivitaCollection();

            DbCommand comando;

            comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriTipologieAttivitaSelect");

            DataSet dsAttivita = databaseCemi.ExecuteDataSet(comando);
            if ((dsAttivita != null) && (dsAttivita.Tables.Count == 1))
            {
                for (int i = 0; i < dsAttivita.Tables[0].Rows.Count; i++)
                {
                    string nomeAttivita;
                    string descrizione = null;

                    nomeAttivita = (string) dsAttivita.Tables[0].Rows[i]["nomeAttivita"];
                    if (dsAttivita.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsAttivita.Tables[0].Rows[i]["descrizione"];

                    listaAttivita.Add(new TipologiaAttivita(nomeAttivita, descrizione));
                }
            }

            return listaAttivita;
        }

        /// <summary>
        /// Inserisce una attivita nel database. In caso di inserimento corretto il campo IdAttivita risulter� valorizzato
        /// </summary>
        /// <param name="attivita"></param>
        public void InsertAttivita(Attivita attivita)
        {
            if (attivita != null)
            {
                if (!string.IsNullOrEmpty(attivita.NomeAttivita) && attivita.Ispettore != null &&
                    attivita.Ispettore.IdIspettore.HasValue && !string.IsNullOrEmpty(attivita.NomeAttivita))
                {
                    // Proseguo con l'inserimento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriProgrammazioneInsert");

                    DatabaseCemi.AddInParameter(comando, "@giorno", DbType.DateTime, attivita.Giorno);
                    DatabaseCemi.AddInParameter(comando, "@ora", DbType.DateTime, attivita.Ora);
                    DatabaseCemi.AddInParameter(comando, "@idIspettore", DbType.Int32, attivita.Ispettore.IdIspettore);

                    if (attivita.Cantiere != null && attivita.Cantiere.IdCantiere.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idCantieriCantiere", DbType.Int32,
                                                    attivita.Cantiere.IdCantiere);

                    DatabaseCemi.AddInParameter(comando, "@nomeAttivita", DbType.String, attivita.NomeAttivita);

                    if (!string.IsNullOrEmpty(attivita.Descrizione))
                        DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, attivita.Descrizione);

                    DatabaseCemi.AddInParameter(comando, "@programmato", DbType.Boolean, attivita.Programmato);
                    DatabaseCemi.AddInParameter(comando, "@preventivato", DbType.Boolean, attivita.Preventivato);
                    DatabaseCemi.AddInParameter(comando, "@consuntivato", DbType.Boolean, attivita.Consuntivato);

                    decimal resQuery = (decimal) databaseCemi.ExecuteScalar(comando);
                    if (resQuery > 0)
                        attivita.IdAttivita = decimal.ToInt32(resQuery);
                }
            }
        }

        /// <summary>
        /// Cancella una voce della tabella CantieriProgrammazione nel database
        /// </summary>
        /// <param name="idAttivita"></param>
        /// <returns></returns>
        /// <param name="tran"></param>
        public bool DeleteAttivita(int idAttivita, DbTransaction tran)
        {
            bool res = false;

            if (idAttivita > 0)
            {
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriProgrammazioneDelete");

                DatabaseCemi.AddInParameter(comando, "@idCantieriProgrammazione", DbType.Int32, idAttivita);

                if (tran == null)
                {
                    int resI = databaseCemi.ExecuteNonQuery(comando);
                    if (resI == 1)
                        res = true;
                }
                else
                {
                    int resI = databaseCemi.ExecuteNonQuery(comando, tran);
                    if (resI == 1)
                        res = true;
                }
            }

            return res;
        }

        /// <summary>
        /// Restituisce la lista dei committenti presenti nel database
        /// </summary>
        /// <returns></returns>
        public CommittenteCollection GetCommittenti(int? idCommittenteParam, string ragioneSocialeParam,
                                                    string comuneParam, string indirizzoParam, string ivaFiscale,
                                                    string stringExpression, string sortDirection,
                                                    bool modalitaNotifiche)
        {
            CommittenteCollection listaCommittenti = new CommittenteCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCommittentiSelect"))
            {
                if (idCommittenteParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32, idCommittenteParam.Value);
                if (!string.IsNullOrEmpty(ragioneSocialeParam))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSocialeParam);
                if (!string.IsNullOrEmpty(comuneParam))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comuneParam);
                if (!string.IsNullOrEmpty(indirizzoParam))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoParam);
                if (!string.IsNullOrEmpty(ivaFiscale))
                    DatabaseCemi.AddInParameter(comando, "@ivaFiscale", DbType.String, ivaFiscale);
                DatabaseCemi.AddInParameter(comando, "@modalitaNotifiche", DbType.Boolean, modalitaNotifiche);

                DataSet dsCommittenti = databaseCemi.ExecuteDataSet(comando);

                if ((dsCommittenti != null) && (dsCommittenti.Tables.Count == 1))
                {
                    if (stringExpression != null && sortDirection != null)
                    {
                        dsCommittenti.Tables[0].DefaultView.Sort = stringExpression + " " + sortDirection;
                    }

                    for (int i = 0; i < dsCommittenti.Tables[0].DefaultView.Count; i++)
                    {
                        // E' presente un committente.. Lo recupero
                        Committente committente = new Committente();
                        listaCommittenti.Add(committente);

                        committente.IdCommittente =
                            (int) dsCommittenti.Tables[0].DefaultView[i]["idCantieriCommittente"];
                        committente.FonteNotifica = (bool) dsCommittenti.Tables[0].DefaultView[i]["fonteNotifica"];

                        committente.RagioneSociale = (string) dsCommittenti.Tables[0].DefaultView[i]["ragioneSociale"];
                        committente.Modificato = (bool) dsCommittenti.Tables[0].DefaultView[i]["modificato"];

                        if (dsCommittenti.Tables[0].DefaultView[i]["indirizzo"] != DBNull.Value)
                            committente.Indirizzo = (string) dsCommittenti.Tables[0].DefaultView[i]["indirizzo"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["comune"] != DBNull.Value)
                            committente.Comune = (string) dsCommittenti.Tables[0].DefaultView[i]["comune"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["provincia"] != DBNull.Value)
                            committente.Provincia = (string) dsCommittenti.Tables[0].DefaultView[i]["provincia"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["cap"] != DBNull.Value)
                            committente.Cap = (string) dsCommittenti.Tables[0].DefaultView[i]["cap"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["partitaIva"] != DBNull.Value)
                            committente.PartitaIva = (string) dsCommittenti.Tables[0].DefaultView[i]["partitaIva"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["codiceFiscale"] != DBNull.Value)
                            committente.CodiceFiscale = (string) dsCommittenti.Tables[0].DefaultView[i]["codiceFiscale"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["personaRiferimento"] != DBNull.Value)
                            committente.PersonaRiferimento =
                                (string) dsCommittenti.Tables[0].DefaultView[i]["personaRiferimento"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["telefono"] != DBNull.Value)
                            committente.Telefono = (string) dsCommittenti.Tables[0].DefaultView[i]["telefono"];
                        if (dsCommittenti.Tables[0].DefaultView[i]["fax"] != DBNull.Value)
                            committente.Fax = (string) dsCommittenti.Tables[0].DefaultView[i]["fax"];
                    }
                }
            }

            return listaCommittenti;
        }

        /// <summary>
        /// Restituisce la lista delle imprese presenti nel database
        /// </summary>
        /// <returns></returns>
        public ImpresaCollection GetImprese(TipologiaImpresa tipoImpresa, int? idImpresaParam,
                                            string ragioneSocialeParam, string comuneParam, string indirizzoParam,
                                            string ivaFiscale, string stringExpression, string sortDirection,
                                            bool modalitaNotifiche)
        {
            ImpresaCollection listaImprese = new ImpresaCollection();

            DbCommand comando;

            switch (tipoImpresa)
            {
                case TipologiaImpresa.TutteLeFonti:
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseSelectTutteLeFonti");
                    break;
                default:
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseSelect");
                    DatabaseCemi.AddInParameter(comando, "@tipoImpresa", DbType.Int32, tipoImpresa);
                    break;
            }

            if (idImpresaParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresaParam.Value);
            if (!string.IsNullOrEmpty(ragioneSocialeParam))
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSocialeParam);
            if (!string.IsNullOrEmpty(comuneParam))
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comuneParam);
            if (!string.IsNullOrEmpty(indirizzoParam))
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoParam);
            if (!string.IsNullOrEmpty(ivaFiscale))
                DatabaseCemi.AddInParameter(comando, "@ivaFiscale", DbType.String, ivaFiscale);
            DatabaseCemi.AddInParameter(comando, "@modalitaNotifiche", DbType.Boolean, modalitaNotifiche);

            DataSet dsImprese = databaseCemi.ExecuteDataSet(comando);

            if ((dsImprese != null) && (dsImprese.Tables.Count == 1))
            {
                if (!String.IsNullOrEmpty(stringExpression) && !String.IsNullOrEmpty(sortDirection))
                {
                    dsImprese.Tables[0].DefaultView.Sort = stringExpression + " " + sortDirection;
                }

                for (int i = 0; i < dsImprese.Tables[0].DefaultView.Count; i++)
                {
                    int idImpresa;
                    string ragioneSociale;
                    string provincia = null;
                    string comune = null;
                    string cap = null;
                    string indirizzo = null;
                    string partitaIva = null;
                    string codiceFiscale = null;

                    idImpresa = (int) dsImprese.Tables[0].DefaultView[i]["idImpresa"];
                    ragioneSociale = (string) dsImprese.Tables[0].DefaultView[i]["ragioneSociale"];

                    if (dsImprese.Tables[0].DefaultView[i]["indirizzo"] != DBNull.Value)
                        indirizzo = (string) dsImprese.Tables[0].DefaultView[i]["indirizzo"];
                    if (dsImprese.Tables[0].DefaultView[i]["comune"] != DBNull.Value)
                        comune = (string) dsImprese.Tables[0].DefaultView[i]["comune"];
                    if (dsImprese.Tables[0].DefaultView[i]["provincia"] != DBNull.Value)
                        provincia = (string) dsImprese.Tables[0].DefaultView[i]["provincia"];
                    if (dsImprese.Tables[0].DefaultView[i]["cap"] != DBNull.Value)
                        cap = (string) dsImprese.Tables[0].DefaultView[i]["cap"];
                    if (dsImprese.Tables[0].DefaultView[i]["partitaIva"] != DBNull.Value)
                        partitaIva = (string) dsImprese.Tables[0].DefaultView[i]["partitaIva"];
                    if (dsImprese.Tables[0].DefaultView[i]["codiceFiscale"] != DBNull.Value)
                        codiceFiscale = (string) dsImprese.Tables[0].DefaultView[i]["codiceFiscale"];

                    TipologiaImpresa tipImp = tipoImpresa;
                    if (dsImprese.Tables[0].DefaultView[i]["fonte"] != DBNull.Value)
                        tipImp = (TipologiaImpresa) dsImprese.Tables[0].DefaultView[i]["fonte"];

                    Impresa impresa = new Impresa(tipImp, idImpresa, ragioneSociale, indirizzo,
                                                  provincia, comune, cap, partitaIva, codiceFiscale);
                    listaImprese.Add(impresa);

                    if (dsImprese.Tables[0].DefaultView[i]["fonteNotifica"] != DBNull.Value)
                        impresa.FonteNotifica = (bool) dsImprese.Tables[0].DefaultView[i]["fonteNotifica"];

                    if (dsImprese.Tables[0].DefaultView[i]["tipoAttivita"] != DBNull.Value)
                        impresa.TipoAttivita = (string) dsImprese.Tables[0].DefaultView[i]["tipoAttivita"];
                    if (dsImprese.Tables[0].DefaultView[i]["telefono"] != DBNull.Value)
                        impresa.Telefono = (string) dsImprese.Tables[0].DefaultView[i]["telefono"];
                    if (dsImprese.Tables[0].DefaultView[i]["fax"] != DBNull.Value)
                        impresa.Fax = (string) dsImprese.Tables[0].DefaultView[i]["fax"];
                    if (dsImprese.Tables[0].DefaultView[i]["personaRiferimento"] != DBNull.Value)
                        impresa.PersonaRiferimento = (string) dsImprese.Tables[0].DefaultView[i]["personaRiferimento"];
                    if (dsImprese.Tables[0].DefaultView[i]["lavoratoreAutonomo"] != DBNull.Value)
                        impresa.LavoratoreAutonomo = (bool) dsImprese.Tables[0].DefaultView[i]["lavoratoreAutonomo"];

                    if (dsImprese.Tables[0].DefaultView[i]["ammiIndirizzo"] != DBNull.Value)
                        impresa.AmmiIndirizzo = (string) dsImprese.Tables[0].DefaultView[i]["ammiIndirizzo"];
                    if (dsImprese.Tables[0].DefaultView[i]["ammiComune"] != DBNull.Value)
                        impresa.AmmiComune = (string) dsImprese.Tables[0].DefaultView[i]["ammiComune"];
                    if (dsImprese.Tables[0].DefaultView[i]["ammiProvincia"] != DBNull.Value)
                        impresa.AmmiProvincia = (string) dsImprese.Tables[0].DefaultView[i]["ammiProvincia"];
                    if (dsImprese.Tables[0].DefaultView[i]["ammiCap"] != DBNull.Value)
                        impresa.AmmiCap = (string) dsImprese.Tables[0].DefaultView[i]["ammiCap"];

                    if (dsImprese.Tables[0].DefaultView[i]["dataInizioLegale"] != DBNull.Value)
                        impresa.DataInizioValiditaSedeLegale =
                            (DateTime) dsImprese.Tables[0].DefaultView[i]["dataInizioLegale"];
                    if (dsImprese.Tables[0].DefaultView[i]["dataFineLegale"] != DBNull.Value)
                        impresa.DataFineValiditaSedeLegale =
                            (DateTime) dsImprese.Tables[0].DefaultView[i]["dataFineLegale"];
                    if (dsImprese.Tables[0].DefaultView[i]["dataInizioAmmi"] != DBNull.Value)
                        impresa.DataInizioValiditaSedeAmministrativa =
                            (DateTime) dsImprese.Tables[0].DefaultView[i]["dataInizioAmmi"];
                    if (dsImprese.Tables[0].DefaultView[i]["dataFineAmmi"] != DBNull.Value)
                        impresa.DataFineValiditaSedeAmministrativa =
                            (DateTime) dsImprese.Tables[0].DefaultView[i]["dataFineAmmi"];

                    if (dsImprese.Tables[0].DefaultView[i]["modificato"] != DBNull.Value)
                        impresa.Modificato = (bool) dsImprese.Tables[0].DefaultView[i]["modificato"];
                }
            }

            return listaImprese;
        }

        public bool UpdateCantiereCoordinate(int idCantiere, GeocodingResult geoResult)
        {
            bool res = false;

            if (idCantiere > 0)
            {
                DbCommand comando;
                comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriUpdateCoordinate");
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Double, geoResult.Latitude);
                DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Double, geoResult.Longitude);
                DatabaseCemi.AddInParameter(comando, "@indirizzoGeocoder", DbType.String, geoResult.Indirizzo);
                DatabaseCemi.AddInParameter(comando, "@civicoGeocoder", DbType.String, geoResult.Civico);
                DatabaseCemi.AddInParameter(comando, "@provinciaGeocoder", DbType.String, geoResult.Provincia);
                DatabaseCemi.AddInParameter(comando, "@comuneGeocoder", DbType.String, geoResult.Comune);
                DatabaseCemi.AddInParameter(comando, "@capGeocoder", DbType.String, geoResult.Cap);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool UpdateAttivitaPrevCons(int idAttivita, bool? preventivata, bool? consuntivata)
        {
            bool res = false;

            if ((idAttivita > 0) && (preventivata.HasValue || consuntivata.HasValue) &&
                !(preventivata.HasValue && consuntivata.HasValue))
            {
                DbCommand comando;
                comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriProgrammazioneUpdatePrevCons");
                DatabaseCemi.AddInParameter(comando, "@idProgrammazione", DbType.Int32, idAttivita);
                if (preventivata.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@preventivata", DbType.Boolean, preventivata);
                if (consuntivata.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@consuntivata", DbType.Boolean, consuntivata);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public RapportoIspezioneCollection GetIspezione(IspezioniFilter filtro, Boolean conGruppo)
        {
            RapportoIspezioneCollection listaIspezioni = new RapportoIspezioneCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniSelect"))
            {
                if (filtro.IdIspezione.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32, filtro.IdIspezione.Value);
                if (filtro.Dal.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal.Value);
                if (filtro.Al.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al.Value);
                if (filtro.IdCantiere.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filtro.IdCantiere.Value);
                if (filtro.IdIspettore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idIspettore", DbType.Int32, filtro.IdIspettore.Value);
                if (filtro.Stato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, filtro.Stato.Value);
                if (!string.IsNullOrEmpty(filtro.Appaltatrice))
                    DatabaseCemi.AddInParameter(comando, "@appaltatrice", DbType.String, filtro.Appaltatrice);
                if (filtro.IdAttivita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idProgrammazione", DbType.Int32, filtro.IdAttivita.Value);
                if (!string.IsNullOrEmpty(filtro.SubAppaltatrice))
                    DatabaseCemi.AddInParameter(comando, "@subAppaltatrice", DbType.String, filtro.SubAppaltatrice);
                if (!string.IsNullOrEmpty(filtro.Provincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaCantiere", DbType.String, filtro.Provincia);
                }
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader
                    Int32 iIdIspezione = reader.GetOrdinal("idCantieriIspezione");
                    Int32 iGiorno = reader.GetOrdinal("giorno");
                    Int32 iIdCantiere = reader.GetOrdinal("idCantieriCantiere");
                    Int32 iIdIspettore = reader.GetOrdinal("idIspettore");
                    Int32 iEsito = reader.GetOrdinal("esito");
                    Int32 iComunicatoA = reader.GetOrdinal("comunicatoA");
                    Int32 iNumRilievi = reader.GetOrdinal("numRilievi");
                    Int32 iNumOsservazioni = reader.GetOrdinal("numOsservazioni");
                    Int32 iNumRAC = reader.GetOrdinal("numRAC");
                    Int32 iNote = reader.GetOrdinal("note");
                    Int32 iComunicatoIl = reader.GetOrdinal("comunicatoIl");
                    Int32 iSegnalazione = reader.GetOrdinal("segnalazione");
                    Int32 iStato = reader.GetOrdinal("stato");
                    Int32 iAvanzamento = reader.GetOrdinal("avanzamento");
                    Int32 iNoteIspettore1 = reader.GetOrdinal("noteIspettore1");
                    Int32 iAudit1Dalle = reader.GetOrdinal("audit1Dalle");
                    Int32 iAudit1Alle = reader.GetOrdinal("audit1Alle");
                    Int32 iAudit1Data = reader.GetOrdinal("audit1Data");
                    Int32 iNoteIspettore2 = reader.GetOrdinal("noteIspettore2");
                    Int32 iAudit2Dalle = reader.GetOrdinal("audit2Dalle");
                    Int32 iAudit2Alle = reader.GetOrdinal("audit2Alle");
                    Int32 iAudit2Data = reader.GetOrdinal("audit2Data");
                    Int32 iAudit2Presso = reader.GetOrdinal("audit2Presso");
                    Int32 iIdCantieriProgrammazione = reader.GetOrdinal("idCantieriProgrammazione");
                    Int32 iProtocolloNotifica = reader.GetOrdinal("protocolloRegione");
                    Int32 idataLetteraVerificaInCorso = reader.GetOrdinal("dataLetteraVerificaInCorso");
                    #endregion

                    while (reader.Read())
                    {
                        RapportoIspezione rappIsp = new RapportoIspezione();
                        listaIspezioni.Add(rappIsp);

                        rappIsp.IdIspezione = reader.GetInt32(iIdIspezione);
                        rappIsp.Giorno = reader.GetDateTime(iGiorno);

                        if (!reader.IsDBNull(idataLetteraVerificaInCorso))
                        {
                            rappIsp.DataLetteraVerificaInCorso = reader.GetDateTime(idataLetteraVerificaInCorso);
                        }

                        // Cantiere (non sarebbe da recuperare cos�)
                        Int32 idCantiere = reader.GetInt32(iIdCantiere);
                        CantieriFilter filtroCantieri = new CantieriFilter();
                        filtroCantieri.IdCantiere = idCantiere;
                        rappIsp.Cantiere =
                            GetCantieriReader(filtroCantieri, null, null, false)
                                [0];

                        if (!reader.IsDBNull(iProtocolloNotifica))
                        {
                            rappIsp.ProtocolloNotificaRegionale = reader.GetString(iProtocolloNotifica);
                        }

                        // Ispettore (stesso discorso del cantiere)
                        Int32 idIspettore = reader.GetInt32(iIdIspettore);
                        rappIsp.Ispettore = GetIspettori(idIspettore, null)[0];

                        rappIsp.Esito = (EsitoIspezione) reader.GetInt32(iEsito);
                        rappIsp.ComunicatoA = (ComunicazioneRapporto) reader.GetInt32(iComunicatoA);

                        if (!reader.IsDBNull(iNumRilievi))
                        {
                            rappIsp.NumRilievi = reader.GetInt32(iNumRilievi);
                        }
                        if (!reader.IsDBNull(iNumOsservazioni))
                        {
                            rappIsp.NumOsservazioni = reader.GetInt32(iNumOsservazioni);
                        }
                        if (!reader.IsDBNull(iNumRAC))
                        {
                            rappIsp.NumRAC = reader.GetInt32(iNumRAC);
                        }
                        if (!reader.IsDBNull(iNote))
                        {
                            rappIsp.Note = reader.GetString(iNote);
                        }
                        if (!reader.IsDBNull(iComunicatoIl))
                        {
                            rappIsp.ComunicatoIl = reader.GetDateTime(iComunicatoIl);
                        }
                        if (!reader.IsDBNull(iSegnalazione))
                        {
                            rappIsp.Segnalazione = (SegnalazionePervenuta) reader.GetInt32(iSegnalazione);
                        }
                        if (!reader.IsDBNull(iStato))
                        {
                            rappIsp.Stato = (StatoIspezione) reader.GetInt32(iStato);
                        }
                        if (!reader.IsDBNull(iAvanzamento))
                        {
                            rappIsp.Avanzamento = reader.GetInt32(iAvanzamento);
                        }
                        if (!reader.IsDBNull(iNoteIspettore1))
                        {
                            rappIsp.NoteIspettore1 = reader.GetString(iNoteIspettore1);
                        }
                        if (!reader.IsDBNull(iAudit1Dalle))
                        {
                            rappIsp.Audit1Dalle = reader.GetDateTime(iAudit1Dalle);
                        }
                        if (!reader.IsDBNull(iAudit1Alle))
                        {
                            rappIsp.Audit1Alle = reader.GetDateTime(iAudit1Alle);
                        }
                        if (!reader.IsDBNull(iAudit1Data))
                        {
                            rappIsp.Audit1Data = reader.GetDateTime(iAudit1Data);
                        }
                        if (!reader.IsDBNull(iNoteIspettore2))
                        {
                            rappIsp.NoteIspettore2 = reader.GetString(iNoteIspettore2);
                        }
                        if (!reader.IsDBNull(iAudit2Dalle))
                        {
                            rappIsp.Audit2Dalle = reader.GetDateTime(iAudit2Dalle);
                        }
                        if (!reader.IsDBNull(iAudit2Alle))
                        {
                            rappIsp.Audit2Alle = reader.GetDateTime(iAudit2Alle);
                        }
                        if (!reader.IsDBNull(iAudit2Data))
                        {
                            rappIsp.Audit2Data = reader.GetDateTime(iAudit2Data);
                        }
                        if (!reader.IsDBNull(iAudit2Presso))
                        {
                            rappIsp.Audit2Presso = reader.GetString(iAudit2Presso);
                        }
                        if (!reader.IsDBNull(iIdCantieriProgrammazione))
                        {
                            rappIsp.IdAttivita = reader.GetInt32(iIdCantieriProgrammazione);
                        }

                        // Gruppo ispezione
                        if (conGruppo)
                        {
                            rappIsp.GruppoIspezione = GetGruppoIspezione(rappIsp.IdIspezione);
                        }
                    }
                }
            }

            return listaIspezioni;
        }

        public RapportoIspezioneImpresaCollection GetIspezioniImprese(int idIspezioneParam, int idCantiereParam)
        {
            RapportoIspezioneImpresaCollection rapporti = new RapportoIspezioneImpresaCollection();
            DbCommand comando;

            comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseSelect");
            DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32, idIspezioneParam);
            DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiereParam);

            DataSet dsIspezione = databaseCemi.ExecuteDataSet(comando);

            if ((dsIspezione != null) && (dsIspezione.Tables.Count == 1))
            {
                for (int i = 0; i < dsIspezione.Tables[0].Rows.Count; i++)
                {
                    int? idRapportoIspezioneImpresa = null;
                    int idImpresa;
                    Impresa impresa = null;
                    DateTime? daAttuare = null;
                    DateTime? dataChiusura = null;
                    int? operaiRegolari = null;
                    int? operaiIntegrati = null;
                    int? operaiTrasformati = null;
                    int? operaiRegolarizzati = null;
                    int? dimensioneAziendale = null;
                    bool? caschiCE = null;
                    bool? tuteCE = null;
                    bool? scarpeCE = null;
                    bool nuovaIscritta = false;
                    decimal? contributiRecuperati = null;
                    bool statistiche = true;
                    decimal? elenchiIntegrativi = null;

                    int? operaiTrasferta = null;
                    int? operaiNoAltreCE = null;
                    int? operaiDistacco = null;
                    int? operaiAltroCCNL = null;

                    if (dsIspezione.Tables[0].Rows[i]["idCantieriIspezioniImpresa"] != DBNull.Value)
                    {
                        idRapportoIspezioneImpresa = (int) dsIspezione.Tables[0].Rows[i]["idCantieriIspezioniImpresa"];

                        if (dsIspezione.Tables[0].Rows[i]["daAttuare"] != DBNull.Value)
                            daAttuare = (DateTime) dsIspezione.Tables[0].Rows[i]["daAttuare"];
                        if (dsIspezione.Tables[0].Rows[i]["dataChiusura"] != DBNull.Value)
                            dataChiusura = (DateTime) dsIspezione.Tables[0].Rows[i]["dataChiusura"];
                        if (dsIspezione.Tables[0].Rows[i]["operaiRegolari"] != DBNull.Value)
                            operaiRegolari = (int) dsIspezione.Tables[0].Rows[i]["operaiRegolari"];
                        if (dsIspezione.Tables[0].Rows[i]["operaiIntegrati"] != DBNull.Value)
                            operaiIntegrati = (int) dsIspezione.Tables[0].Rows[i]["operaiIntegrati"];
                        if (dsIspezione.Tables[0].Rows[i]["operaiTrasformati"] != DBNull.Value)
                            operaiTrasformati = (int) dsIspezione.Tables[0].Rows[i]["operaiTrasformati"];
                        if (dsIspezione.Tables[0].Rows[i]["operaiRegolarizzati"] != DBNull.Value)
                            operaiRegolarizzati = (int) dsIspezione.Tables[0].Rows[i]["operaiRegolarizzati"];
                        if (dsIspezione.Tables[0].Rows[i]["dimensioneAziendale"] != DBNull.Value)
                            dimensioneAziendale = (int) dsIspezione.Tables[0].Rows[i]["dimensioneAziendale"];
                        if (dsIspezione.Tables[0].Rows[i]["caschiCE"] != DBNull.Value)
                            caschiCE = (bool) dsIspezione.Tables[0].Rows[i]["caschiCE"];
                        if (dsIspezione.Tables[0].Rows[i]["tuteCE"] != DBNull.Value)
                            tuteCE = (bool) dsIspezione.Tables[0].Rows[i]["tuteCE"];
                        if (dsIspezione.Tables[0].Rows[i]["scarpeCE"] != DBNull.Value)
                            scarpeCE = (bool) dsIspezione.Tables[0].Rows[i]["scarpeCE"];

                        nuovaIscritta = (bool) dsIspezione.Tables[0].Rows[i]["nuovaIscritta"];
                        if (dsIspezione.Tables[0].Rows[i]["contributiRecuperati"] != DBNull.Value)
                            contributiRecuperati = (decimal) dsIspezione.Tables[0].Rows[i]["contributiRecuperati"];
                        if (dsIspezione.Tables[0].Rows[i]["statistiche"] != DBNull.Value)
                            statistiche = (bool) dsIspezione.Tables[0].Rows[i]["statistiche"];
                        if (dsIspezione.Tables[0].Rows[i]["elenchiIntegrativi"] != DBNull.Value)
                        {
                            elenchiIntegrativi = (decimal) dsIspezione.Tables[0].Rows[i]["elenchiIntegrativi"];
                        }

                        if (dsIspezione.Tables[0].Rows[i]["operaiTrasferta"] != DBNull.Value)
                        {
                            operaiTrasferta = (Int32) dsIspezione.Tables[0].Rows[i]["operaiTrasferta"];
                        }
                        if (dsIspezione.Tables[0].Rows[i]["operaiNoAltreCE"] != DBNull.Value)
                        {
                            operaiNoAltreCE = (Int32) dsIspezione.Tables[0].Rows[i]["operaiNoAltreCE"];
                        }
                        if (dsIspezione.Tables[0].Rows[i]["operaiDistacco"] != DBNull.Value)
                        {
                            operaiDistacco = (Int32) dsIspezione.Tables[0].Rows[i]["operaiDistacco"];
                        }
                        if (dsIspezione.Tables[0].Rows[i]["operaiAltroCCNL"] != DBNull.Value)
                        {
                            operaiAltroCCNL = (Int32) dsIspezione.Tables[0].Rows[i]["operaiAltroCCNL"];
                        }
                    }

                    if (dsIspezione.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                    {
                        idImpresa = (int) dsIspezione.Tables[0].Rows[i]["idImpresa"];
                        impresa =
                            GetImprese(TipologiaImpresa.SiceNew, idImpresa, null, null, null, null, null, null, false)[0
                                ];
                    }
                    else if (dsIspezione.Tables[0].Rows[i]["idCantieriImpresa"] != DBNull.Value)
                    {
                        idImpresa = (int) dsIspezione.Tables[0].Rows[i]["idCantieriImpresa"];
                        impresa =
                            GetImprese(TipologiaImpresa.Cantieri, idImpresa, null, null, null, null, null, null, false)[
                                0];
                    }

                    RapportoIspezioneImpresa rapp =
                        new RapportoIspezioneImpresa(idRapportoIspezioneImpresa, impresa, daAttuare, dataChiusura,
                                                     operaiRegolari,
                                                     operaiIntegrati, operaiTrasformati, operaiRegolarizzati,
                                                     dimensioneAziendale, caschiCE, tuteCE, scarpeCE);

                    rapp.NuovaIscritta = nuovaIscritta;
                    rapp.ContributiRecuperati = contributiRecuperati;
                    rapp.Statistiche = statistiche;
                    rapp.ElenchiIntegrativi = elenchiIntegrativi;
                    rapp.OperaiTrasferta = operaiTrasferta;
                    rapp.OperaiNoAltreCE = operaiNoAltreCE;
                    rapp.OperaiDistacco = operaiDistacco;
                    rapp.OperaiAltroCCNL = operaiAltroCCNL;

                    if (rapp.IdRapportoIspezioneImpresa.HasValue)
                    {
                        // Recupero RAC e OSS
                        rapp.Oss = GetIspezioneImpresaOSS(rapp.IdRapportoIspezioneImpresa.Value);
                        rapp.Rac = GetIspezioneImpresaRAC(rapp.IdRapportoIspezioneImpresa.Value);
                        rapp.Contratti = GetIspezioneImpresaContratti(rapp.IdRapportoIspezioneImpresa.Value);
                        rapp.CasseEdili = GetIspezioneImpresaCasseEdili(rapp.IdRapportoIspezioneImpresa.Value);
                    }
                    else
                    {
                        rapp.Oss = new OSSCollection();
                        rapp.Rac = new RACCollection();
                        rapp.Contratti = new ContrattoCollection();
                        rapp.CasseEdili = new CassaEdileCollection();
                    }

                    rapporti.Add(rapp);
                }
            }

            return rapporti;
        }

        private RACCollection GetIspezioneImpresaRAC(int idIspezioneImpresa)
        {
            RACCollection listaRAC = new RACCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseRACSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);

                DataSet dsRAC = databaseCemi.ExecuteDataSet(comando);

                if ((dsRAC != null) && (dsRAC.Tables.Count == 1))
                {
                    for (int i = 0; i < dsRAC.Tables[0].Rows.Count; i++)
                    {
                        string idRAC;
                        string descrizione;

                        idRAC = (string) dsRAC.Tables[0].Rows[i]["idCantieriRAC"];
                        descrizione = (string) dsRAC.Tables[0].Rows[i]["descrizione"];

                        listaRAC.Add(new RAC(idRAC, descrizione));
                    }
                }
            }

            return listaRAC;
        }

        private OSSCollection GetIspezioneImpresaOSS(int idIspezioneImpresa)
        {
            OSSCollection listaOSS = new OSSCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseOSSSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);

                DataSet dsOSS = databaseCemi.ExecuteDataSet(comando);

                if ((dsOSS != null) && (dsOSS.Tables.Count == 1))
                {
                    for (int i = 0; i < dsOSS.Tables[0].Rows.Count; i++)
                    {
                        int idOSS;
                        string descrizione;

                        idOSS = (int) dsOSS.Tables[0].Rows[i]["idCantieriOSS"];
                        descrizione = (string) dsOSS.Tables[0].Rows[i]["descrizione"];

                        listaOSS.Add(new OSS(idOSS, descrizione));
                    }
                }
            }

            return listaOSS;
        }

        public SubappaltoCollection GetSubappalti(int idCantiereParam)
        {
            SubappaltoCollection listaSubappalti = new SubappaltoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiereParam);

                DataSet dsSubappalti = databaseCemi.ExecuteDataSet(comando);

                if ((dsSubappalti != null) && (dsSubappalti.Tables.Count == 1))
                {
                    for (int i = 0; i < dsSubappalti.Tables[0].Rows.Count; i++)
                    {
                        int idSubappalto;
                        int idCantiere;
                        int idImpresaIce = -1;
                        int idCantieriImpresaIce = -1;
                        int idImpresaAta = -1;
                        int idCantieriImpresaAta = -1;
                        Impresa subappaltatrice = null;
                        Impresa subappaltata;
                        TipologiaContratto tipoContratto;

                        idSubappalto = (int) dsSubappalti.Tables[0].Rows[i]["idCantieriSubappalto"];
                        idCantiere = (int) dsSubappalti.Tables[0].Rows[i]["idCantieriCantiere"];
                        if (dsSubappalti.Tables[0].Rows[i]["idCantieriImpresaSubappaltatrice"] != DBNull.Value)
                            idCantieriImpresaIce = (int) dsSubappalti.Tables[0].Rows[i]["idCantieriImpresaSubappaltatrice"];
                        if (dsSubappalti.Tables[0].Rows[i]["idCantieriImpresaSubappaltata"] != DBNull.Value)
                            idCantieriImpresaAta = (int) dsSubappalti.Tables[0].Rows[i]["idCantieriImpresaSubappaltata"];
                        if (dsSubappalti.Tables[0].Rows[i]["idImpresaSubappaltatrice"] != DBNull.Value)
                            idImpresaIce = (int) dsSubappalti.Tables[0].Rows[i]["idImpresaSubappaltatrice"];
                        if (dsSubappalti.Tables[0].Rows[i]["idImpresaSubappaltata"] != DBNull.Value)
                            idImpresaAta = (int) dsSubappalti.Tables[0].Rows[i]["idImpresaSubappaltata"];
                        tipoContratto = (TipologiaContratto) dsSubappalti.Tables[0].Rows[i]["tipologiaContratto"];

                        // Subappaltatrice
                        if (idCantieriImpresaIce != -1)
                            subappaltatrice =
                                GetImprese(TipologiaImpresa.Cantieri, idCantieriImpresaIce, null, null, null, null, null,
                                           null, false)[0];
                        else if (idImpresaIce != -1)
                            subappaltatrice =
                                GetImprese(TipologiaImpresa.SiceNew, idImpresaIce, null, null, null, null, null, null, false)
                                    [0];

                        // Subappaltata
                        if (idCantieriImpresaAta != -1)
                            subappaltata =
                                GetImprese(TipologiaImpresa.Cantieri, idCantieriImpresaAta, null, null, null, null, null,
                                           null, false)[0];
                        else
                            subappaltata =
                                GetImprese(TipologiaImpresa.SiceNew, idImpresaAta, null, null, null, null, null, null, false)
                                    [0];

                        listaSubappalti.Add(
                            new Subappalto(idSubappalto, idCantiere, subappaltatrice, subappaltata, tipoContratto));
                    }
                }
            }

            return listaSubappalti;
        }

        public bool InsertSubappalti(SubappaltoCollection subappalti, DbTransaction transaction)
        {
            bool res = false;

            if (transaction == null)
            {
                using (DbConnection conn = databaseCemi.CreateConnection())
                {
                    conn.Open();
                    using (DbTransaction tran = conn.BeginTransaction())
                    {
                        try
                        {
                            foreach (Subappalto subappalto in subappalti)
                            {
                                if (!subappalto.IdSubappalto.HasValue)
                                {
                                    // Memorizzo la "foto" delle imprese, se necessario
                                    if (subappalto.SubAppaltata.FonteNotifica && !subappalto.SubAppaltata.Modificato)
                                    {
                                        res = UpdateImpresaIspettore(subappalto.SubAppaltata, tran);
                                    }

                                    if (subappalto.SubAppaltatrice != null && subappalto.SubAppaltatrice.FonteNotifica &&
                                        !subappalto.SubAppaltatrice.Modificato)
                                    {
                                        res = UpdateImpresaIspettore(subappalto.SubAppaltatrice, tran);
                                    }

                                    DbCommand comando;

                                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiInsert");
                                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, subappalto.IdCantiere);
                                    DatabaseCemi.AddInParameter(comando, "@tipologiaContratto", DbType.Int32,
                                                                subappalto.TipoContratto);

                                    if (subappalto.SubAppaltata.TipoImpresa == TipologiaImpresa.Cantieri)
                                        DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaSubappaltata", DbType.Int32,
                                                                    subappalto.SubAppaltata.IdImpresa.Value);
                                    else if (subappalto.SubAppaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                                        DatabaseCemi.AddInParameter(comando, "@idImpresaSubappaltata", DbType.Int32,
                                                                    subappalto.SubAppaltata.IdImpresa.Value);

                                    if (subappalto.SubAppaltatrice != null)
                                    {
                                        if (subappalto.SubAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                                            DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaSubappaltatrice",
                                                                        DbType.Int32,
                                                                        subappalto.SubAppaltatrice.IdImpresa.Value);
                                        else if (subappalto.SubAppaltatrice.TipoImpresa == TipologiaImpresa.SiceNew)
                                            DatabaseCemi.AddInParameter(comando, "@idImpresaSubappaltatrice", DbType.Int32,
                                                                        subappalto.SubAppaltatrice.IdImpresa.Value);
                                    }

                                    if (databaseCemi.ExecuteNonQuery(comando, tran) != 1)
                                        throw new Exception();
                                }
                                else
                                {
                                    DbCommand comando;

                                    comando =
                                        databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiUpdate");
                                    DatabaseCemi.AddInParameter(comando, "@idSubappalto", DbType.Int32,
                                                                subappalto.IdSubappalto.Value);
                                    DatabaseCemi.AddInParameter(comando, "@tipoContratto", DbType.Int32,
                                                                subappalto.TipoContratto);

                                    if (subappalto.SubAppaltata.TipoImpresa == TipologiaImpresa.Cantieri)
                                        DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaSubappaltata", DbType.Int32,
                                                                    subappalto.SubAppaltata.IdImpresa.Value);
                                    else if (subappalto.SubAppaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                                        DatabaseCemi.AddInParameter(comando, "@idImpresaSubappaltata", DbType.Int32,
                                                                    subappalto.SubAppaltata.IdImpresa.Value);

                                    if (subappalto.SubAppaltatrice != null)
                                    {
                                        if (subappalto.SubAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                                            DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaSubappaltatrice",
                                                                        DbType.Int32,
                                                                        subappalto.SubAppaltatrice.IdImpresa.Value);
                                        else if (subappalto.SubAppaltatrice.TipoImpresa == TipologiaImpresa.SiceNew)
                                            DatabaseCemi.AddInParameter(comando, "@idImpresaSubappaltatrice", DbType.Int32,
                                                                        subappalto.SubAppaltatrice.IdImpresa.Value);
                                    }

                                    if (databaseCemi.ExecuteNonQuery(comando, tran) != 1)
                                        throw new Exception();
                                }
                            }

                            res = true;
                            tran.Commit();
                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }
            else
            {
                foreach (Subappalto subappalto in subappalti)
                {
                    if (!subappalto.IdSubappalto.HasValue)
                    {
                        // Memorizzo la "foto" delle imprese, se necessario
                        if (subappalto.SubAppaltata.FonteNotifica && !subappalto.SubAppaltata.Modificato)
                        {
                            res = UpdateImpresaIspettore(subappalto.SubAppaltata, transaction);
                        }

                        if (subappalto.SubAppaltatrice != null && subappalto.SubAppaltatrice.FonteNotifica &&
                            !subappalto.SubAppaltatrice.Modificato)
                        {
                            res = UpdateImpresaIspettore(subappalto.SubAppaltatrice, transaction);
                        }

                        DbCommand comando;

                        comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiInsert");
                        DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, subappalto.IdCantiere);
                        DatabaseCemi.AddInParameter(comando, "@tipologiaContratto", DbType.Int32,
                                                    subappalto.TipoContratto);

                        if (subappalto.SubAppaltata.TipoImpresa == TipologiaImpresa.Cantieri)
                            DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaSubappaltata", DbType.Int32,
                                                        subappalto.SubAppaltata.IdImpresa.Value);
                        else if (subappalto.SubAppaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                            DatabaseCemi.AddInParameter(comando, "@idImpresaSubappaltata", DbType.Int32,
                                                        subappalto.SubAppaltata.IdImpresa.Value);

                        if (subappalto.SubAppaltatrice != null)
                        {
                            if (subappalto.SubAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                                DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaSubappaltatrice",
                                                            DbType.Int32,
                                                            subappalto.SubAppaltatrice.IdImpresa.Value);
                            else if (subappalto.SubAppaltatrice.TipoImpresa == TipologiaImpresa.SiceNew)
                                DatabaseCemi.AddInParameter(comando, "@idImpresaSubappaltatrice", DbType.Int32,
                                                            subappalto.SubAppaltatrice.IdImpresa.Value);
                        }

                        if (databaseCemi.ExecuteNonQuery(comando, transaction) != 1)
                            throw new Exception();
                    }
                    else
                    {
                        DbCommand comando;

                        comando =
                            databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiUpdateTipoContratto");
                        DatabaseCemi.AddInParameter(comando, "@idSubappalto", DbType.Int32,
                                                    subappalto.IdSubappalto.Value);
                        DatabaseCemi.AddInParameter(comando, "@tipoContratto", DbType.Int32,
                                                    subappalto.TipoContratto);

                        if (databaseCemi.ExecuteNonQuery(comando, transaction) != 1)
                            throw new Exception();
                    }
                }
            }

            return res;
        }

        public bool InsertUpdateIspezione(RapportoIspezione ispezione)
        {
            bool res = false;

            if (ispezione != null)
            {
                using (DbConnection conn = databaseCemi.CreateConnection())
                {
                    conn.Open();
                    using (DbTransaction tran = conn.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            // Se l'ispezione � in stato conclusa e il cantiere era stato segnalato con
                            // una segnalazione ricorrente verifico se non � presente gi� una nuova segnalazione
                            // altrimenti la inserisco copiando anche il cantiere
                            if (ispezione.Stato.HasValue
                                && ispezione.Stato.Value == StatoIspezione.Conclusa)
                            {
                                if (ispezione.Cantiere != null
                                    && ispezione.Cantiere.Segnalazione != null
                                    && ispezione.Cantiere.Segnalazione.Ricorrente
                                    && !ispezione.Cantiere.Segnalazione.IdSegnalazioneSuccessiva.HasValue)
                                {
                                    Cantiere cantiereCopia = CopiaCantiere(ispezione.Cantiere);
                                    InsertCantiere(cantiereCopia, tran);
                                    SubappaltoCollection subappaltiCopia = CopiaSubappalti(GetSubappalti(ispezione.Cantiere.IdCantiere.Value), cantiereCopia.IdCantiere.Value);
                                    InsertSubappalti(subappaltiCopia, tran);

                                    Segnalazione segnalazioneCopia = CopiaSegnalazione(ispezione.Cantiere.Segnalazione);
                                    segnalazioneCopia.IdCantiere = cantiereCopia.IdCantiere.Value;
                                    InsertSegnalazione(segnalazioneCopia, tran);
                                    UpdateCantiereConSegnalazione(segnalazioneCopia, tran);

                                    UpdateSegnalazionePerSuccessiva(ispezione.Cantiere.Segnalazione.IdSegnalazione, segnalazioneCopia.IdSegnalazione, tran);
                                }
                            }

                            // Verifico se si tratta di insert o update
                            if (!ispezione.IdIspezione.HasValue)
                            {
                                // Insert
                                if (!InsertIspezione(ispezione, tran))
                                    throw new Exception("Inserimento ispezione fallita");
                            }
                            else
                            {
                                // Update
                                if (!UpdateIspezione(ispezione, tran))
                                    throw new Exception("Aggiornamento ispezione fallita");
                            }

                            if (ispezione.GruppoIspezione != null)
                            {
                                if (!InsertGruppoIspezione(ispezione.GruppoIspezione, ispezione.IdIspezione.Value, tran))
                                    throw new Exception("Inserimento gruppo ispezione fallito");
                            }

                            if (ispezione.RapportiImpresa != null)
                            {
                                foreach (RapportoIspezioneImpresa rapImp in ispezione.RapportiImpresa)
                                {
                                    if (!rapImp.IdRapportoIspezioneImpresa.HasValue)
                                    {
                                        // Insert
                                        if (!InsertIspezioneImpresa(rapImp, ispezione.IdIspezione.Value, tran))
                                            throw new Exception("Inserimento ispezione impresa fallito");
                                    }
                                    else
                                    {
                                        // Update
                                        if (!UpdateIspezioneImpresa(rapImp, tran))
                                            throw new Exception("Aggiornamento ispezione impresa fallito");
                                    }
                                }
                            }

                            tran.Commit();
                            res = true;
                        }
                        catch
                        {
                            tran.Rollback();
                            throw;
                        }
                    }
                }
            }

            return res;
        }

        private bool InsertContratti(Int32 idIspezioneImpresa, ContrattoCollection contratti, DbTransaction tran)
        {
            Boolean res = true;

            using (DbCommand comando1 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseContrattiDelete"))
            {
                DatabaseCemi.AddInParameter(comando1, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                databaseCemi.ExecuteNonQuery(comando1, tran);
            }

            using (DbCommand comando2 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseContrattiInsert"))
            {
                DatabaseCemi.AddInParameter(comando2, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                DatabaseCemi.AddInParameter(comando2, "@idContratto", DbType.Int32);

                foreach (Contratto co in contratti)
                {
                    comando2.Parameters["@idContratto"].Value = co.Id;

                    if (databaseCemi.ExecuteNonQuery(comando2, tran) != 1)
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        private bool InsertCasseEdili(Int32 idIspezioneImpresa, Cemi.Type.Collections.CassaEdileCollection casseEdili, DbTransaction tran)
        {
            Boolean res = true;

            using (DbCommand comando1 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseCasseEdiliDelete"))
            {
                DatabaseCemi.AddInParameter(comando1, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                databaseCemi.ExecuteNonQuery(comando1, tran);
            }

            using (DbCommand comando2 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseCasseEdiliInsert"))
            {
                DatabaseCemi.AddInParameter(comando2, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                DatabaseCemi.AddInParameter(comando2, "@idCassaEdile", DbType.String);

                foreach (TBridge.Cemi.Type.Entities.CassaEdile ce in casseEdili)
                {
                    comando2.Parameters["@idCassaEdile"].Value = ce.IdCassaEdile;

                    if (databaseCemi.ExecuteNonQuery(comando2, tran) != 1)
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        private SubappaltoCollection CopiaSubappalti(SubappaltoCollection subappalti, Int32 idCantiere)
        {
            SubappaltoCollection subappaltiCopia = new SubappaltoCollection();

            if (subappalti != null)
            {
                foreach (Subappalto sub in subappalti)
                {
                    sub.IdCantiere = idCantiere;
                    subappaltiCopia.Add(sub);
                }
            }

            return subappaltiCopia;
        }

        private void UpdateSegnalazionePerSuccessiva(Int32 idSegnalazione, Int32 idSegnalazioneSuccessiva, DbTransaction transaction)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSegnalazioniUpdateSegnalazioneSuccessiva"))
            {
                DatabaseCemi.AddInParameter(comando, "@idSegnalazione", DbType.Int32, idSegnalazione);
                DatabaseCemi.AddInParameter(comando, "@idSegnalazioneSuccessiva", DbType.Int32, idSegnalazioneSuccessiva);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) != 1)
                {
                    throw new Exception("UpdateSegnalazionePerSuccessiva: errore durante l'aggiornamento della segnalazione");
                }
            }
        }

        private Segnalazione CopiaSegnalazione(Segnalazione segnalazione)
        {
            Segnalazione segnalazioneCopia = new Segnalazione();

            segnalazioneCopia.Data = segnalazione.Data;
            segnalazioneCopia.IdUtente = segnalazione.IdUtente;
            segnalazioneCopia.Motivazione = segnalazione.Motivazione;
            segnalazioneCopia.NomeUtente = segnalazione.NomeUtente;
            segnalazioneCopia.Priorita = segnalazione.Priorita;
            segnalazioneCopia.Ricorrente = segnalazione.Ricorrente;

            return segnalazioneCopia;
        }

        private Cantiere CopiaCantiere(Cantiere cantiere)
        {
            Cantiere cantiereCopia = new Cantiere();

            cantiereCopia.Attivo = cantiere.Attivo;
            cantiereCopia.Cap = cantiere.Cap;
            cantiereCopia.Civico = cantiere.Civico;
            cantiereCopia.Committente = cantiere.Committente;
            cantiereCopia.Comune = cantiere.Comune;
            cantiereCopia.DataFineLavori = cantiere.DataFineLavori;
            cantiereCopia.DataInizioLavori = cantiere.DataInizioLavori;
            cantiereCopia.DescrizioneLavori = cantiere.DescrizioneLavori;
            cantiereCopia.DirezioneLavori = cantiere.DirezioneLavori;
            cantiereCopia.Frazione = cantiere.Frazione;
            cantiereCopia.Importo = cantiere.Importo;
            cantiereCopia.ImpresaAppaltatrice = cantiere.ImpresaAppaltatrice;
            cantiereCopia.Indirizzo = cantiere.Indirizzo;
            cantiereCopia.Latitudine = cantiere.Latitudine;
            cantiereCopia.Longitudine = cantiere.Longitudine;
            cantiereCopia.PermessoCostruire = cantiere.PermessoCostruire;
            cantiereCopia.PreIndirizzo = cantiere.PreIndirizzo;
            cantiereCopia.Provincia = cantiere.Provincia;
            cantiereCopia.ResponsabileCantiere = cantiere.ResponsabileCantiere;
            cantiereCopia.ResponsabileProcedimento = cantiere.ResponsabileProcedimento;
            cantiereCopia.TipoImpresaAppaltatrice = cantiere.TipoImpresaAppaltatrice;
            cantiereCopia.TipologiaAppalto = cantiere.TipologiaAppalto;

            return cantiereCopia;
        }

        private bool InsertGruppoIspezione(IspettoreCollection ispettoreCollection, int idIspezione, DbTransaction tran)
        {
            bool res = true;

            using (DbCommand comando1 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniGruppiDelete"))
            {
                DatabaseCemi.AddInParameter(comando1, "@idIspezione", DbType.Int32, idIspezione);
                databaseCemi.ExecuteNonQuery(comando1, tran);
            }

            using (DbCommand comando2 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniGruppiInsert"))
            {
                DatabaseCemi.AddInParameter(comando2, "@idIspezione", DbType.Int32);
                DatabaseCemi.AddInParameter(comando2, "@idIspettore", DbType.Int32);
                foreach (Ispettore ispettore in ispettoreCollection)
                {
                    comando2.Parameters["@idIspezione"].Value = idIspezione;
                    comando2.Parameters["@idIspettore"].Value = ispettore.IdIspettore;

                    if (databaseCemi.ExecuteNonQuery(comando2, tran) != 1)
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        private bool UpdateIspezione(RapportoIspezione ispezione, DbTransaction transaction)
        {
            bool res = false;

            if (ispezione != null && ispezione.IdIspezione.HasValue)
            {
                using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32, ispezione.IdIspezione.Value);
                    DatabaseCemi.AddInParameter(comando, "@esito", DbType.Int32, ispezione.Esito);
                    if (ispezione.NumRilievi.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numRilievi", DbType.Int32, ispezione.NumRilievi.Value);
                    if (ispezione.NumOsservazioni.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numOsservazioni", DbType.Int32,
                                                    ispezione.NumOsservazioni.Value);
                    if (ispezione.NumRAC.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numRAC", DbType.Int32, ispezione.NumRAC.Value);
                    if (ispezione.Note != null)
                        DatabaseCemi.AddInParameter(comando, "@note", DbType.String, ispezione.Note);
                    DatabaseCemi.AddInParameter(comando, "@comunicatoA", DbType.Int32, ispezione.ComunicatoA);
                    if (ispezione.ComunicatoIl.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@comunicatoIl", DbType.DateTime,
                                                    ispezione.ComunicatoIl.Value);
                    if (ispezione.Segnalazione.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@segnalazione", DbType.Int32, ispezione.Segnalazione.Value);
                    if (ispezione.Stato.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, ispezione.Stato.Value);
                    if (ispezione.Avanzamento.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@avanzamento", DbType.Int32, ispezione.Avanzamento.Value);

                    if (ispezione.NoteIspettore1 != null)
                        DatabaseCemi.AddInParameter(comando, "@noteIspettore1", DbType.String, ispezione.NoteIspettore1);
                    if (ispezione.Audit1Dalle.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@audit1Dalle", DbType.DateTime,
                                                    ispezione.Audit1Dalle.Value);
                    if (ispezione.Audit1Alle.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@audit1Alle", DbType.DateTime, ispezione.Audit1Alle.Value);
                    if (ispezione.Audit1Data.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@audit1Data", DbType.DateTime, ispezione.Audit1Data.Value);

                    if (ispezione.NoteIspettore2 != null)
                        DatabaseCemi.AddInParameter(comando, "@noteIspettore2", DbType.String, ispezione.NoteIspettore2);
                    if (ispezione.Audit2Dalle.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@audit2Dalle", DbType.DateTime,
                                                    ispezione.Audit2Dalle.Value);
                    if (ispezione.Audit2Alle.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@audit2Alle", DbType.DateTime, ispezione.Audit2Alle.Value);
                    if (ispezione.Audit2Data.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@audit2Data", DbType.DateTime, ispezione.Audit2Data.Value);
                    if (ispezione.Audit2Presso != null)
                        DatabaseCemi.AddInParameter(comando, "@audit2Presso", DbType.String, ispezione.Audit2Presso);

                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        private bool InsertIspezione(RapportoIspezione ispezione, DbTransaction transaction)
        {
            bool res = false;

            if (ispezione != null && ispezione.Cantiere.IdCantiere.HasValue && ispezione.Ispettore.IdIspettore.HasValue)
            {
                using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@giorno", DbType.DateTime, ispezione.Giorno);
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32,
                                                ispezione.Cantiere.IdCantiere.Value);
                    DatabaseCemi.AddInParameter(comando, "@idIspettore", DbType.Int32,
                                                ispezione.Ispettore.IdIspettore.Value);
                    DatabaseCemi.AddInParameter(comando, "@esito", DbType.Int32, ispezione.Esito);

                    if (ispezione.IdAttivita.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idProgrammazione", DbType.Int32, ispezione.IdAttivita);
                    }
                    if (ispezione.NumRilievi.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numRilievi", DbType.Int32, ispezione.NumRilievi.Value);
                    if (ispezione.NumOsservazioni.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numOsservazioni", DbType.Int32,
                                                    ispezione.NumOsservazioni.Value);
                    if (ispezione.NumRAC.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numRAC", DbType.Int32, ispezione.NumRAC.Value);
                    if (ispezione.Note != null)
                        DatabaseCemi.AddInParameter(comando, "@note", DbType.String, ispezione.Note);
                    DatabaseCemi.AddInParameter(comando, "@comunicatoA", DbType.Int32, ispezione.ComunicatoA);
                    if (ispezione.ComunicatoIl.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@comunicatoIl", DbType.DateTime,
                                                    ispezione.ComunicatoIl.Value);
                    if (ispezione.Segnalazione.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@segnalazione", DbType.Int32, ispezione.Segnalazione.Value);
                    if (ispezione.Stato.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, ispezione.Stato.Value);
                    if (ispezione.Avanzamento.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@avanzamento", DbType.Int32, ispezione.Avanzamento.Value);

                    int resI = decimal.ToInt32((decimal) databaseCemi.ExecuteScalar(comando, transaction));
                    if (resI > 0)
                    {
                        res = true;
                        ispezione.IdIspezione = resI;
                    }
                }
            }

            return res;
        }

        private bool InsertIspezioneImpresa(RapportoIspezioneImpresa ispezioneImpresa, int idIspezione,
                                            DbTransaction transaction)
        {
            bool res = false;

            if (ispezioneImpresa != null && ispezioneImpresa.Impresa != null &&
                ispezioneImpresa.Impresa.IdImpresa.HasValue)
            {
                using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32, idIspezione);

                    if (ispezioneImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                        DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                    ispezioneImpresa.Impresa.IdImpresa);
                    else
                        DatabaseCemi.AddInParameter(comando, "@idCantieriImpresa", DbType.Int32,
                                                    ispezioneImpresa.Impresa.IdImpresa);

                    if (ispezioneImpresa.DaAttuare.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@daAttuare", DbType.DateTime,
                                                    ispezioneImpresa.DaAttuare.Value);
                    if (ispezioneImpresa.DataChiusura.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataChiusura", DbType.DateTime,
                                                    ispezioneImpresa.DataChiusura.Value);
                    if (ispezioneImpresa.OperaiRegolari.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiRegolari", DbType.Int32,
                                                    ispezioneImpresa.OperaiRegolari.Value);
                    if (ispezioneImpresa.OperaiIntegrati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiIntegrati", DbType.Int32,
                                                    ispezioneImpresa.OperaiIntegrati.Value);
                    if (ispezioneImpresa.OperaiTrasformati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiTrasformati", DbType.Int32,
                                                    ispezioneImpresa.OperaiTrasformati.Value);
                    if (ispezioneImpresa.OperaiRegolarizzati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiRegolarizzati", DbType.Int32,
                                                    ispezioneImpresa.OperaiRegolarizzati.Value);
                    if (ispezioneImpresa.DimensioneAziendale.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dimensioneAziendale", DbType.Int32,
                                                    ispezioneImpresa.DimensioneAziendale.Value);
                    if (ispezioneImpresa.CaschiCE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@caschi", DbType.Boolean, ispezioneImpresa.CaschiCE.Value);
                    if (ispezioneImpresa.TuteCE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@tute", DbType.Boolean, ispezioneImpresa.TuteCE.Value);
                    if (ispezioneImpresa.ScarpeCE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@scarpe", DbType.Boolean, ispezioneImpresa.ScarpeCE.Value);

                    DatabaseCemi.AddInParameter(comando, "@nuovaIscritta", DbType.Boolean,
                                                ispezioneImpresa.NuovaIscritta);
                    if (ispezioneImpresa.ContributiRecuperati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@contributiRecuperati", DbType.Double,
                                                    ispezioneImpresa.ContributiRecuperati.Value);
                    DatabaseCemi.AddInParameter(comando, "@statistiche", DbType.Boolean, ispezioneImpresa.Statistiche);
                    if (ispezioneImpresa.ElenchiIntegrativi.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@elenchiIntegrativi", DbType.Double,
                                                    ispezioneImpresa.ElenchiIntegrativi.Value);
                    }
                    if (ispezioneImpresa.OperaiTrasferta.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiTrasferta", DbType.Int32,
                                                    ispezioneImpresa.OperaiTrasferta.Value);
                    }
                    if (ispezioneImpresa.OperaiNoAltreCE.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiNoAltreCE", DbType.Int32,
                                                    ispezioneImpresa.OperaiNoAltreCE.Value);
                    }
                    if (ispezioneImpresa.OperaiDistacco.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiDistacco", DbType.Int32,
                                                    ispezioneImpresa.OperaiDistacco.Value);
                    }
                    if (ispezioneImpresa.OperaiAltroCCNL.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiAltroCCNL", DbType.Int32,
                                                    ispezioneImpresa.OperaiAltroCCNL.Value);
                    }

                    int progr = decimal.ToInt32((decimal) databaseCemi.ExecuteScalar(comando, transaction));
                    if (progr != -1)
                        res = true;

                    // Aggiorno OSS
                    if (res)
                        res = UpdateIspezioneImpresaOSS(progr, ispezioneImpresa.Oss, transaction);

                    // Aggiorno RAC
                    if (res)
                        res = UpdateIspezioneImpresaRAC(progr, ispezioneImpresa.Rac, transaction);

                    if (res && ispezioneImpresa.CasseEdili != null)
                    {
                        res = InsertCasseEdili(progr, ispezioneImpresa.CasseEdili, transaction);
                    }

                    if (res && ispezioneImpresa.Contratti != null)
                    {
                        res = InsertContratti(progr, ispezioneImpresa.Contratti, transaction);
                    }
                }
            }

            return res;
        }

        private bool UpdateIspezioneImpresa(RapportoIspezioneImpresa ispezioneImpresa, DbTransaction transaction)
        {
            bool res = false;

            if (ispezioneImpresa != null && ispezioneImpresa.IdRapportoIspezioneImpresa.HasValue)
            {
                using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32,
                                                ispezioneImpresa.IdRapportoIspezioneImpresa.Value);

                    if (ispezioneImpresa.DaAttuare.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@daAttuare", DbType.DateTime,
                                                    ispezioneImpresa.DaAttuare.Value);
                    if (ispezioneImpresa.DataChiusura.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataChiusura", DbType.DateTime,
                                                    ispezioneImpresa.DataChiusura.Value);
                    if (ispezioneImpresa.OperaiRegolari.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiRegolari", DbType.Int32,
                                                    ispezioneImpresa.OperaiRegolari.Value);
                    if (ispezioneImpresa.OperaiIntegrati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiIntegrati", DbType.Int32,
                                                    ispezioneImpresa.OperaiIntegrati.Value);
                    if (ispezioneImpresa.OperaiTrasformati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiTrasformati", DbType.Int32,
                                                    ispezioneImpresa.OperaiTrasformati.Value);
                    if (ispezioneImpresa.OperaiRegolarizzati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@operaiRegolarizzati", DbType.Int32,
                                                    ispezioneImpresa.OperaiRegolarizzati.Value);
                    if (ispezioneImpresa.DimensioneAziendale.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dimensioneAziendale", DbType.Int32,
                                                    ispezioneImpresa.DimensioneAziendale.Value);
                    if (ispezioneImpresa.CaschiCE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@caschi", DbType.Boolean, ispezioneImpresa.CaschiCE.Value);
                    if (ispezioneImpresa.TuteCE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@tute", DbType.Boolean, ispezioneImpresa.TuteCE.Value);
                    if (ispezioneImpresa.ScarpeCE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@scarpe", DbType.Boolean, ispezioneImpresa.ScarpeCE.Value);

                    DatabaseCemi.AddInParameter(comando, "@nuovaIscritta", DbType.Boolean,
                                                ispezioneImpresa.NuovaIscritta);
                    if (ispezioneImpresa.ContributiRecuperati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@contributiRecuperati", DbType.Decimal,
                                                    ispezioneImpresa.ContributiRecuperati.Value);
                    DatabaseCemi.AddInParameter(comando, "@statistiche", DbType.Boolean, ispezioneImpresa.Statistiche);
                    if (ispezioneImpresa.ElenchiIntegrativi.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@elenchiIntegrativi", DbType.Double,
                                                    ispezioneImpresa.ElenchiIntegrativi.Value);
                    }

                    if (ispezioneImpresa.OperaiTrasferta.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiTrasferta", DbType.Int32,
                                                    ispezioneImpresa.OperaiTrasferta.Value);
                    }
                    if (ispezioneImpresa.OperaiNoAltreCE.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiNoAltreCE", DbType.Int32,
                                                    ispezioneImpresa.OperaiNoAltreCE.Value);
                    }
                    if (ispezioneImpresa.OperaiDistacco.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiDistacco", DbType.Int32,
                                                    ispezioneImpresa.OperaiDistacco.Value);
                    }
                    if (ispezioneImpresa.OperaiAltroCCNL.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@operaiAltroCCNL", DbType.Int32,
                                                    ispezioneImpresa.OperaiAltroCCNL.Value);
                    }

                    databaseCemi.ExecuteNonQuery(comando, transaction);

                    // Aggiorno OSS
                    res =
                        UpdateIspezioneImpresaOSS(ispezioneImpresa.IdRapportoIspezioneImpresa.Value,
                                                  ispezioneImpresa.Oss,
                                                  transaction);

                    // Aggiorno RAC
                    if (res)
                        res =
                            UpdateIspezioneImpresaRAC(ispezioneImpresa.IdRapportoIspezioneImpresa.Value,
                                                      ispezioneImpresa.Rac, transaction);

                    if (res && ispezioneImpresa.CasseEdili != null)
                    {
                        res = InsertCasseEdili(ispezioneImpresa.IdRapportoIspezioneImpresa.Value, ispezioneImpresa.CasseEdili, transaction);
                    }

                    if (res && ispezioneImpresa.Contratti != null)
                    {
                        res = InsertContratti(ispezioneImpresa.IdRapportoIspezioneImpresa.Value, ispezioneImpresa.Contratti, transaction);
                    }
                }
            }

            return res;
        }

        private bool UpdateIspezioneImpresaOSS(int idIspezioneImpresa, IEnumerable<OSS> oss, DbTransaction transaction)
        {
            bool res = true;

            DeleteIspezioneImpresaOSS(idIspezioneImpresa, transaction);
            if (oss != null)
            {
                foreach (OSS os in oss)
                {
                    using (
                        DbCommand comando =
                            databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseOSSInsert"))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                        DatabaseCemi.AddInParameter(comando, "@idOSS", DbType.Int32, os.IdOSS);
                        if (databaseCemi.ExecuteNonQuery(comando, transaction) != 1)
                        {
                            res = false;
                            break;
                        }
                    }
                }
            }

            return res;
        }

        private void DeleteIspezioneImpresaOSS(int idIspezioneImpresa, DbTransaction transaction)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseOSSDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                databaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        private bool UpdateIspezioneImpresaRAC(int idIspezioneImpresa, IEnumerable<RAC> rac, DbTransaction transaction)
        {
            bool res = true;

            DeleteIspezioneImpresaRAC(idIspezioneImpresa, transaction);
            if (rac != null)
            {
                foreach (RAC ra in rac)
                {
                    using (
                        DbCommand comando =
                            databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseRACInsert"))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                        DatabaseCemi.AddInParameter(comando, "@idRAC", DbType.String, ra.IdRAC);
                        if (databaseCemi.ExecuteNonQuery(comando, transaction) != 1)
                        {
                            res = false;
                            break;
                        }
                    }
                }
            }

            return res;
        }

        private void DeleteIspezioneImpresaRAC(int idIspezioneImpresa, DbTransaction transaction)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseRACDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                databaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        public OSSCollection GetOSS()
        {
            OSSCollection listaOSS = new OSSCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriOSSSelect"))
            {
                DataSet dsOSS = databaseCemi.ExecuteDataSet(comando);

                if ((dsOSS != null) && (dsOSS.Tables.Count == 1))
                {
                    for (int i = 0; i < dsOSS.Tables[0].Rows.Count; i++)
                    {
                        int idOSS;
                        string descrizione;

                        idOSS = (int) dsOSS.Tables[0].Rows[i]["idCantieriOSS"];
                        descrizione = (string) dsOSS.Tables[0].Rows[i]["descrizione"];

                        listaOSS.Add(new OSS(idOSS, descrizione));
                    }
                }
            }

            return listaOSS;
        }

        public RACCollection GetRAC()
        {
            RACCollection listaRAC = new RACCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriRACSelect"))
            {
                DataSet dsRAC = databaseCemi.ExecuteDataSet(comando);

                if ((dsRAC != null) && (dsRAC.Tables.Count == 1))
                {
                    for (int i = 0; i < dsRAC.Tables[0].Rows.Count; i++)
                    {
                        string idRAC;
                        string descrizione;

                        idRAC = (string) dsRAC.Tables[0].Rows[i]["idCantieriRAC"];
                        descrizione = (string) dsRAC.Tables[0].Rows[i]["descrizione"];

                        listaRAC.Add(new RAC(idRAC, descrizione));
                    }
                }
            }

            return listaRAC;
        }

        public LavoratoreCollection GetLavoratoriOrdinati(TipologiaLavoratore tipoLavoratore, int? idLavoratoreParam,
                                                          string cognomeParam, string nomeParam,
                                                          DateTime? dataNascitaParam, string stringExpression,
                                                          string sortDirection)
        {
            LavoratoreCollection listaLavoratori = new LavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoLavoratore", DbType.Int32, tipoLavoratore);

                if (idLavoratoreParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratoreParam.Value);
                if (!string.IsNullOrEmpty(cognomeParam))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognomeParam);
                if (!string.IsNullOrEmpty(nomeParam))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nomeParam);
                if (dataNascitaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascitaParam);

                DataSet dsLavoratori = databaseCemi.ExecuteDataSet(comando);

                if ((dsLavoratori != null) && (dsLavoratori.Tables.Count == 1))
                {
                    if (stringExpression != null && sortDirection != null)
                    {
                        dsLavoratori.Tables[0].DefaultView.Sort = stringExpression + " " + sortDirection;
                    }

                    for (int i = 0; i < dsLavoratori.Tables[0].DefaultView.Count; i++)
                    {
                        int idLavoratore;
                        string cognome;
                        string nome = null;
                        DateTime? dataNascita = null;

                        idLavoratore = (int) dsLavoratori.Tables[0].DefaultView[i]["idLavoratore"];
                        cognome = (string) dsLavoratori.Tables[0].DefaultView[i]["cognome"];

                        if (dsLavoratori.Tables[0].DefaultView[i]["nome"] != DBNull.Value)
                            nome = (string) dsLavoratori.Tables[0].DefaultView[i]["nome"];
                        if (dsLavoratori.Tables[0].DefaultView[i]["dataNascita"] != DBNull.Value)
                            dataNascita = (DateTime) dsLavoratori.Tables[0].DefaultView[i]["dataNascita"];

                        listaLavoratori.Add(new Lavoratore(tipoLavoratore, idLavoratore, cognome, nome, dataNascita));
                    }
                }
            }

            return listaLavoratori;
        }

        public LavoratoreCollection GetLavoratoriOrdinatiReader(TipologiaLavoratore tipoLavoratore,
                                                                int? idLavoratoreParam, string cognomeParam,
                                                                string nomeParam, DateTime? dataNascitaParam,
                                                                string stringExpression, string sortDirection)
        {
            LavoratoreCollection listaLavoratori = new LavoratoreCollection();
            DbCommand comando;

            switch (tipoLavoratore)
            {
                case TipologiaLavoratore.SiceNew:
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelect");
                    DatabaseCemi.AddInParameter(comando, "@tipoLavoratore", DbType.Int32, tipoLavoratore);
                    break;
                case TipologiaLavoratore.Cantieri:
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelect");
                    DatabaseCemi.AddInParameter(comando, "@tipoLavoratore", DbType.Int32, tipoLavoratore);
                    break;
                case TipologiaLavoratore.Notifiche:
                    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelectNotifiche");
                    break;
                case TipologiaLavoratore.TutteLeFonti:
                    comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelectTutteFonti");
                    break;
                default:
                    comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelectTutteFonti");
                    break;
            }

            //if (tipoLavoratore != TipologiaLavoratore.Notifiche)
            //{
            //    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelect");
            //    DatabaseCemi.AddInParameter(comando, "@tipoLavoratore", DbType.Int32, tipoLavoratore);
            //}
            //else
            //    comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelectNotifiche");

            if (idLavoratoreParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratoreParam.Value);
            if (!string.IsNullOrEmpty(cognomeParam))
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognomeParam);
            if (!string.IsNullOrEmpty(nomeParam))
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nomeParam);
            if (dataNascitaParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascitaParam);

            using (IDataReader reader = databaseCemi.ExecuteReader(comando))
            {
                while (reader.Read())
                {
                    int idLavoratore = -1;
                    string cognome = String.Empty;
                    string nome = String.Empty;
                    DateTime? dataNascita = null;
                    TipologiaLavoratore tipoLav;

                    if (reader["idLavoratore"] != DBNull.Value)
                        idLavoratore = (int) reader["idLavoratore"];
                    if (reader["cognome"] != DBNull.Value)
                        cognome = (string) reader["cognome"];
                    if (reader["nome"] != DBNull.Value)
                        nome = (string) reader["nome"];
                    if (reader["dataNascita"] != DBNull.Value)
                        dataNascita = (DateTime) reader["dataNascita"];
                    if (tipoLavoratore == TipologiaLavoratore.TutteLeFonti && reader["fonte"] != DBNull.Value)
                        tipoLav =
                            (TipologiaLavoratore) Enum.ToObject(typeof(TipologiaLavoratore), (int) reader["fonte"]);
                    else
                        tipoLav = tipoLavoratore;

                    listaLavoratori.Add(new Lavoratore(tipoLav, idLavoratore, cognome, nome, dataNascita));
                }
            }

            // Prove ritardo...
            //System.Threading.Thread.Sleep(100000);

            return listaLavoratori;
        }

        public LavoratoreCollection GetLavoratoriConUltimoImpiego(string cognome, string nome, DateTime dataNascita)
        {
            LavoratoreCollection listaLavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriSelectControlloPresenzaSiceNew"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        listaLavoratori.Add(lavoratore);

                        if (reader["idLavoratore"] != DBNull.Value)
                            lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                        if (reader["cognome"] != DBNull.Value)
                            lavoratore.Cognome = (string) reader["cognome"];
                        if (reader["nome"] != DBNull.Value)
                            lavoratore.Nome = (string) reader["nome"];
                        if (reader["dataNascita"] != DBNull.Value)
                            lavoratore.DataNascita = (DateTime) reader["dataNascita"];

                        // Ultimo rapporto
                        string aperto = null;
                        string chiuso = null;
                        // Aperto
                        if (reader["ultimoRapportoAperto"] != DBNull.Value)
                            aperto = reader["ultimoRapportoAperto"].ToString();

                        // Chiuso
                        if (reader["ultimoRapportoChiuso"] != DBNull.Value)
                            chiuso = reader["ultimoRapportoChiuso"].ToString();

                        if (!string.IsNullOrEmpty(aperto))
                            lavoratore.Impresa = aperto;
                        else
                            lavoratore.Impresa = chiuso;
                    }
                }
            }

            return listaLavoratori;
        }

        public RapportoIspezioneLavoratoreCollection GetLavoratoriIspezione(int? idIspezione)
        {
            RapportoIspezioneLavoratoreCollection listaLavoratori = new RapportoIspezioneLavoratoreCollection();

            if (idIspezione.HasValue)
            {
                DbCommand comando;

                comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniLavoratori");
                DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32, idIspezione.Value);

                DataSet dsLavoratori = databaseCemi.ExecuteDataSet(comando);

                if ((dsLavoratori != null) && (dsLavoratori.Tables.Count == 1))
                {
                    for (int i = 0; i < dsLavoratori.Tables[0].DefaultView.Count; i++)
                    {
                        int? idIspezioniLavoratore;
                        int? numeroLavoratori = null;
                        Lavoratore lavoratore = null;
                        Impresa impresa;
                        int? idLavoratore = null;
                        int? idCantieriLavoratore = null;
                        int? idNotifica = null;
                        int? idImpresa = null;
                        int? idCantieriImpresa = null;

                        idIspezioniLavoratore = (int) dsLavoratori.Tables[0].Rows[i]["idCantieriIspezioniLavoratori"];
                        if (dsLavoratori.Tables[0].Rows[i]["idLavoratore"] != DBNull.Value)
                            idLavoratore = (int) dsLavoratori.Tables[0].Rows[i]["idLavoratore"];
                        if (dsLavoratori.Tables[0].Rows[i]["idCantieriLavoratore"] != DBNull.Value)
                            idCantieriLavoratore = (int) dsLavoratori.Tables[0].Rows[i]["idCantieriLavoratore"];
                        if (dsLavoratori.Tables[0].Rows[i]["idNotifica"] != DBNull.Value)
                            idNotifica = (int) dsLavoratori.Tables[0].Rows[i]["idNotifica"];
                        if (dsLavoratori.Tables[0].Rows[i]["numeroLavoratori"] != DBNull.Value)
                            numeroLavoratori = (int) dsLavoratori.Tables[0].Rows[i]["numeroLavoratori"];
                        if (dsLavoratori.Tables[0].Rows[i]["idImpresa"] != DBNull.Value)
                            idImpresa = (int) dsLavoratori.Tables[0].Rows[i]["idImpresa"];
                        if (dsLavoratori.Tables[0].Rows[i]["idCantieriImpresa"] != DBNull.Value)
                            idCantieriImpresa = (int) dsLavoratori.Tables[0].Rows[i]["idCantieriImpresa"];

                        // Carico il lavoratore dall'anagrafica corretta
                        if (idLavoratore.HasValue)
                            lavoratore =
                                GetLavoratoriOrdinati(TipologiaLavoratore.SiceNew, idLavoratore.Value, null, null, null,
                                                      null, null)[0];
                        else if (idCantieriLavoratore.HasValue)
                            lavoratore =
                                GetLavoratoriOrdinati(TipologiaLavoratore.Cantieri, idCantieriLavoratore.Value, null,
                                                      null, null, null, null)[0];
                        else if (idNotifica.HasValue)
                            lavoratore =
                                GetLavoratoriOrdinati(TipologiaLavoratore.Notifiche, idNotifica.Value, null, null, null,
                                                      null, null)[0];

                        // Carico l'impresa dall'anagrafica corretta
                        if (idImpresa.HasValue)
                            impresa =
                                GetImprese(TipologiaImpresa.SiceNew, idImpresa.Value, null, null, null, null, null, null,
                                           false)[0];
                        else
                            impresa =
                                GetImprese(TipologiaImpresa.Cantieri, idCantieriImpresa.Value, null, null, null, null,
                                           null, null, false)[0];

                        if (lavoratore != null)
                            listaLavoratori.Add(
                                new RapportoIspezioneLavoratore(idIspezioniLavoratore, lavoratore, impresa));
                        else
                            listaLavoratori.Add(
                                new RapportoIspezioneLavoratore(idIspezioniLavoratore, numeroLavoratori.Value, impresa));
                    }
                }
            }

            return listaLavoratori;
        }

        public bool InsertIspezioneLavoratori(RapportoIspezione ispezione)
        {
            bool res = false;

            if (ispezione != null && ispezione.IdIspezione.HasValue && ispezione.Lavoratori != null)
            {
                using (DbConnection conn = databaseCemi.CreateConnection())
                {
                    conn.Open();
                    using (DbTransaction tran = conn.BeginTransaction())
                    {
                        try
                        {
                            foreach (RapportoIspezioneLavoratore ispLav in ispezione.Lavoratori)
                            {
                                if (!ispLav.IdIspezioneLavoratore.HasValue)
                                {
                                    DbCommand comando;

                                    comando =
                                        databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniLavoratoriInsert");
                                    DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32,
                                                                ispezione.IdIspezione);

                                    // Lavoratore
                                    if (ispLav.Lavoratore != null)
                                    {
                                        if (ispLav.Lavoratore.TipoLavoratore == TipologiaLavoratore.Cantieri)
                                            DatabaseCemi.AddInParameter(comando, "@idCantieriLavoratore", DbType.Int32,
                                                                        ispLav.Lavoratore.IdLavoratore.Value);
                                        else if (ispLav.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                                            DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                                        ispLav.Lavoratore.IdLavoratore.Value);
                                        else if (ispLav.Lavoratore.TipoLavoratore == TipologiaLavoratore.Notifiche)
                                            DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32,
                                                                        ispLav.Lavoratore.IdLavoratore.Value);
                                    }
                                    else
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@numeroLavoratori", DbType.Int32,
                                                                    ispLav.NumeroLavoratori.Value);
                                    }

                                    // Impresa
                                    if (ispLav.Impresa.TipoImpresa == TipologiaImpresa.Cantieri)
                                        DatabaseCemi.AddInParameter(comando, "@idCantieriImpresa", DbType.Int32,
                                                                    ispLav.Impresa.IdImpresa.Value);
                                    else if (ispLav.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                                        DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                                    ispLav.Impresa.IdImpresa.Value);

                                    if (databaseCemi.ExecuteNonQuery(comando, tran) != 1)
                                        throw new Exception();
                                }
                            }

                            res = true;
                            tran.Commit();
                        }
                        catch
                        {
                            tran.Rollback();
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
            }

            return res;
        }

        public bool DeleteIspezioneLavoratore(int idIspezioneLavoratore)
        {
            bool res = false;

            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniLavoratoriDelete");
            DatabaseCemi.AddInParameter(comando, "@idIspezioneLavoratore", DbType.Int32, idIspezioneLavoratore);
            if (databaseCemi.ExecuteNonQuery(comando) == 1)
                res = true;

            return res;
        }

        public bool UpdateIspezioneLavoratore(int idIspezioneLavoratore, int idLavoratoreSiceNew)
        {
            bool res = false;

            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniLavoratoriUpdate");
            DatabaseCemi.AddInParameter(comando, "@idCantieriIspezioniLavoratori", DbType.Int32, idIspezioneLavoratore);
            DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratoreSiceNew);
            if (databaseCemi.ExecuteNonQuery(comando) == 1)
                res = true;

            return res;
        }

        public bool DeleteSubappalto(int idSubappalto)
        {
            bool res = false;

            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiDelete");
            DatabaseCemi.AddInParameter(comando, "@idSubappalto", DbType.Int32, idSubappalto);
            if (databaseCemi.ExecuteNonQuery(comando) == 1)
                res = true;

            return res;
        }

        public bool UpdateCommittente(Committente committente)
        {
            bool res = false;

            if (committente != null)
            {
                if (committente.IdCommittente.HasValue && committente.RagioneSociale != null)
                {
                    // Proseguo con l'aggiornamento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCommittentiUpdate");

                    DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32, committente.IdCommittente.Value);
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, committente.RagioneSociale);
                    if (!string.IsNullOrEmpty(committente.PartitaIva))
                        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, committente.PartitaIva);
                    if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, committente.CodiceFiscale);
                    if (!string.IsNullOrEmpty(committente.Indirizzo))
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                    if (!string.IsNullOrEmpty(committente.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                    if (!string.IsNullOrEmpty(committente.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                    if (!string.IsNullOrEmpty(committente.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                    if (!string.IsNullOrEmpty(committente.Telefono))
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, committente.Telefono);
                    if (!string.IsNullOrEmpty(committente.Fax))
                        DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, committente.Fax);
                    if (!string.IsNullOrEmpty(committente.Cap))
                        DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                                                    committente.PersonaRiferimento);

                    int resQuery = databaseCemi.ExecuteNonQuery(comando);
                    if (resQuery == 1)
                        res = true;
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();

            return res;
        }

        public void InsertCommittente(Committente committente)
        {
            if (committente != null)
            {
                if (!committente.IdCommittente.HasValue && !string.IsNullOrEmpty(committente.RagioneSociale))
                {
                    // Proseguo con l'inserimento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCommittentiInsert");

                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, committente.RagioneSociale);
                    if (!string.IsNullOrEmpty(committente.PartitaIva))
                        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, committente.PartitaIva);
                    if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, committente.CodiceFiscale);
                    if (!string.IsNullOrEmpty(committente.Indirizzo))
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                    if (!string.IsNullOrEmpty(committente.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                    if (!string.IsNullOrEmpty(committente.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                    if (!string.IsNullOrEmpty(committente.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                    if (!string.IsNullOrEmpty(committente.Telefono))
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, committente.Telefono);
                    if (!string.IsNullOrEmpty(committente.Fax))
                        DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, committente.Fax);
                    if (!string.IsNullOrEmpty(committente.PersonaRiferimento))
                        DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                                                    committente.PersonaRiferimento);

                    decimal res = (decimal) databaseCemi.ExecuteScalar(comando);
                    if (res > 0)
                        committente.IdCommittente = decimal.ToInt32(res);
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();
        }

        public bool UpdateImpresa(Impresa impresa)
        {
            bool res = false;

            if (impresa != null)
            {
                if (impresa.IdImpresa.HasValue && impresa.RagioneSociale != null)
                {
                    // Proseguo con l'aggiornamento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseUpdate");

                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                    if (!string.IsNullOrEmpty(impresa.PartitaIva))
                        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                    if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                    if (!string.IsNullOrEmpty(impresa.Indirizzo))
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                    if (!string.IsNullOrEmpty(impresa.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                    if (!string.IsNullOrEmpty(impresa.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                    if (!string.IsNullOrEmpty(impresa.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);

                    int resQuery = databaseCemi.ExecuteNonQuery(comando);
                    if (resQuery == 1)
                        res = true;
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();

            return res;
        }

        public bool UpdateImpresaIspettore(Impresa impresa, DbTransaction transaction)
        {
            bool res = false;

            if (impresa != null)
            {
                if (impresa.IdImpresa.HasValue && impresa.RagioneSociale != null)
                {
                    // Proseguo con l'aggiornamento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseUpdatePerIspettori");

                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                    if (!string.IsNullOrEmpty(impresa.PartitaIva))
                        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                    if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                    if (!string.IsNullOrEmpty(impresa.Indirizzo))
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                    if (!string.IsNullOrEmpty(impresa.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                    if (!string.IsNullOrEmpty(impresa.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                    if (!string.IsNullOrEmpty(impresa.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                    if (!string.IsNullOrEmpty(impresa.Telefono))
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, impresa.Telefono);
                    if (!string.IsNullOrEmpty(impresa.Fax))
                        DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, impresa.Fax);
                    if (!string.IsNullOrEmpty(impresa.PersonaRiferimento))
                        DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                                                    impresa.PersonaRiferimento);
                    if (!string.IsNullOrEmpty(impresa.TipoAttivita))
                        DatabaseCemi.AddInParameter(comando, "@tipoAttivita", DbType.String, impresa.TipoAttivita);
                    DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean, impresa.LavoratoreAutonomo);

                    int resQuery;
                    if (transaction != null)
                        resQuery = databaseCemi.ExecuteNonQuery(comando, transaction);
                    else
                        resQuery = databaseCemi.ExecuteNonQuery(comando);

                    if (resQuery == 1)
                        res = true;
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();

            return res;
        }

        public void InsertImpresa(Impresa impresa)
        {
            if (impresa != null)
            {
                if (!impresa.IdImpresa.HasValue && !string.IsNullOrEmpty(impresa.RagioneSociale))
                {
                    // Proseguo con l'inserimento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseInsert");

                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                    if (!string.IsNullOrEmpty(impresa.PartitaIva))
                        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                    if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                    if (!string.IsNullOrEmpty(impresa.Indirizzo))
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                    if (!string.IsNullOrEmpty(impresa.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                    if (!string.IsNullOrEmpty(impresa.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                    if (!string.IsNullOrEmpty(impresa.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                    if (!string.IsNullOrEmpty(impresa.TipoAttivita))
                        DatabaseCemi.AddInParameter(comando, "@tipoAttivita", DbType.String, impresa.TipoAttivita);
                    if (!string.IsNullOrEmpty(impresa.Telefono))
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, impresa.Telefono);
                    if (!string.IsNullOrEmpty(impresa.Fax))
                        DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, impresa.Fax);
                    if (!string.IsNullOrEmpty(impresa.PersonaRiferimento))
                        DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                                                    impresa.PersonaRiferimento);
                    DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean, impresa.LavoratoreAutonomo);

                    decimal res = (decimal) databaseCemi.ExecuteScalar(comando);
                    if (res > 0)
                        impresa.IdImpresa = decimal.ToInt32(res);
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();
        }

        public bool DeleteZona(int idZona)
        {
            bool res = false;

            using (DbConnection connection = databaseCemi.CreateConnection())
            {
                connection.Open();
                using (DbTransaction tran = connection.BeginTransaction())
                {
                    try
                    {
                        // Cancello i vecchi cap
                        DbCommand comandoDel1 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZonaCapDelete");
                        DatabaseCemi.AddInParameter(comandoDel1, "@idCantieriZona", DbType.Int32, idZona);
                        databaseCemi.ExecuteNonQuery(comandoDel1, tran);

                        // Cancello i vecchi cap
                        DbCommand comandoDel2 = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriZoneDelete");
                        DatabaseCemi.AddInParameter(comandoDel2, "@idZona", DbType.Int32, idZona);
                        if (databaseCemi.ExecuteNonQuery(comandoDel2, tran) == 1)
                        {
                            tran.Commit();
                            res = true;
                        }
                        else
                            throw new Exception();
                    }
                    catch
                    {
                        tran.Rollback();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return res;
        }

        public bool ZonaAssociata(int idZona)
        {
            bool res = true;

            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IspettoriSelectZonaPresente");
            DatabaseCemi.AddInParameter(comando, "@idZona", DbType.Int32, idZona);
            if (((int) databaseCemi.ExecuteScalar(comando)) == 0)
                res = false;

            return res;
        }

        public bool DeleteIspezione(Int32? idAttivita, Int32? idIspezione, DbTransaction tran)
        {
            bool res = false;


            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniDelete");
            if (idAttivita.HasValue)
            {
                DatabaseCemi.AddInParameter(comando, "@idProgrammazione", DbType.Int32, idAttivita.Value);
            }
            if (idIspezione.HasValue)
            {
                DatabaseCemi.AddInParameter(comando, "@idIspezione", DbType.Int32, idIspezione.Value);
            }

            if (tran == null)
            {
                if ((int) databaseCemi.ExecuteScalar(comando) == 1)
                    res = true;
            }
            else
            {
                if ((int) databaseCemi.ExecuteScalar(comando, tran) == 1)
                    res = true;
            }

            return res;
        }

        public void InsertLavoratore(Lavoratore lavoratore)
        {
            if (lavoratore != null)
            {
                if (!lavoratore.IdLavoratore.HasValue && !string.IsNullOrEmpty(lavoratore.Cognome) &&
                    !string.IsNullOrEmpty(lavoratore.Nome) && lavoratore.DataNascita.HasValue)
                {
                    // Proseguo con l'inserimento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriInsert");

                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita.Value);

                    decimal res = (decimal) databaseCemi.ExecuteScalar(comando);
                    if (res > 0)
                        lavoratore.IdLavoratore = decimal.ToInt32(res);
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();
        }

        public bool UpdateLavoratore(Lavoratore lavoratore)
        {
            bool res = false;

            if (lavoratore != null)
            {
                if (lavoratore.IdLavoratore.HasValue && !string.IsNullOrEmpty(lavoratore.Cognome) &&
                    !string.IsNullOrEmpty(lavoratore.Nome) && lavoratore.DataNascita.HasValue)
                {
                    // Proseguo con l'aggiornamento
                    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLavoratoriUpdate");

                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita.Value);

                    int resQuery = databaseCemi.ExecuteNonQuery(comando);
                    if (resQuery == 1)
                        res = true;
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();

            return res;
        }

        /// <summary>
        /// Cancella il cantiere e le attivita associate
        /// </summary>
        /// <param name="idCantiere"></param>
        /// <returns></returns>
        public bool DeleteCantiere(int idCantiere)
        {
            bool res = true;

            AttivitaCollection attivita = GetProgrammazionePerCantiere(idCantiere);

            using (DbConnection conn = databaseCemi.CreateConnection())
            {
                conn.Open();
                using (DbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        for (int i = 0; i < attivita.Count; i++)
                        {
                            DeleteIspezione(attivita[i].IdAttivita.Value, null, tran);

                            if (DeleteAttivita(attivita[i].IdAttivita.Value, tran))
                                res = true;
                            else
                            {
                                res = false;
                                break;
                            }
                        }

                        if (res)
                        {
                            DbCommand comando1 =
                                databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiDeletePerCantiere");

                            DatabaseCemi.AddInParameter(comando1, "@idCantiere", DbType.Int32, idCantiere);
                            if (databaseCemi.ExecuteNonQuery(comando1, tran) >= 0)
                                res = true;
                            else
                                res = false;

                            if (res)
                            {
                                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriDelete");

                                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                                if (databaseCemi.ExecuteNonQuery(comando, tran) >= 1)
                                    res = true;
                                else
                                    res = false;
                            }
                        }

                        if (res)
                            tran.Commit();
                        else
                            tran.Rollback();
                    }
                    catch
                    {
                        tran.Rollback();
                        res = false;
                    }
                }
            }

            return res;
        }

        public bool FusioneCantieri(Cantiere cantiere, CantiereCollection cantieriAssimilabili)
        {
            bool res = true;

            using (DbConnection conn = databaseCemi.CreateConnection())
            {
                conn.Open();
                using (DbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (Cantiere cantiereLista in cantieriAssimilabili)
                        {
                            if (!FondiCantieri(cantiere, cantiereLista, tran))
                                throw new Exception();
                        }

                        tran.Commit();
                    }
                    catch
                    {
                        res = false;
                        tran.Rollback();
                    }
                }
            }

            return res;
        }

        private bool FondiCantieri(Cantiere principale, Cantiere secondario, DbTransaction transaction)
        {
            bool res = false;

            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriFusioneCantieri");

            DatabaseCemi.AddInParameter(comando, "@idCantierePrincipale", DbType.Int32, principale.IdCantiere.Value);
            DatabaseCemi.AddInParameter(comando, "@idCantiereSecondario", DbType.Int32, secondario.IdCantiere.Value);

            if ((int) databaseCemi.ExecuteScalar(comando, transaction) == 2)
                res = true;

            return res;
        }

        public List<CantiereFonte> GetCantieriFontiAem(bool soloNonGeoreferenziati)
        {
            List<CantiereFonte> listaCantieri = new List<CantiereFonte>();

            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriFonteAemSelect");
            DatabaseCemi.AddInParameter(dbCommand, "@soloNonGeoreferenziati", DbType.Boolean, soloNonGeoreferenziati);

            using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
            {
                while (reader.Read())
                {
                    CantiereFonte cantiere = new CantiereFonte();
                    cantiere.NomeFonte = "Aem";

                    cantiere.Id = (int) reader["id"];
                    if (reader["indirizzo"] != DBNull.Value)
                        cantiere.Indirizzo = (string) reader["indirizzo"];
                    if (reader["civico"] != DBNull.Value)
                        cantiere.Civico = (string) reader["civico"];
                    if (reader["comune"] != DBNull.Value)
                        cantiere.Comune = (string) reader["comune"];
                    if (reader["cap"] != DBNull.Value)
                        cantiere.Cap = (string) reader["cap"];
                    if (reader["provincia"] != DBNull.Value)
                        cantiere.Provincia = (string) reader["provincia"];

                    listaCantieri.Add(cantiere);
                }
            }

            return listaCantieri;
        }

        public List<CantiereFonte> GetCantieriFontiAsle(bool soloNonGeoreferenziati)
        {
            List<CantiereFonte> listaCantieri = new List<CantiereFonte>();

            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriFonteAsleSelect");
            DatabaseCemi.AddInParameter(dbCommand, "@soloNonGeoreferenziati", DbType.Boolean, soloNonGeoreferenziati);

            using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
            {
                while (reader.Read())
                {
                    CantiereFonte cantiere = new CantiereFonte();
                    cantiere.NomeFonte = "Asle";

                    cantiere.Id = (int) reader["id"];
                    if (reader["indirizzo"] != DBNull.Value)
                        cantiere.Indirizzo = (string) reader["indirizzo"];
                    if (reader["civico"] != DBNull.Value)
                        cantiere.Civico = (string) reader["civico"];
                    if (reader["comune"] != DBNull.Value)
                        cantiere.Comune = (string) reader["comune"];
                    if (reader["cap"] != DBNull.Value)
                        cantiere.Cap = (string) reader["cap"];
                    if (reader["provincia"] != DBNull.Value)
                        cantiere.Provincia = (string) reader["provincia"];

                    listaCantieri.Add(cantiere);
                }
            }

            return listaCantieri;
        }

        public List<CantiereFonte> getCantieriFontiCpt(bool soloNonGeoreferenziati)
        {
            List<CantiereFonte> listaCantieri = new List<CantiereFonte>();

            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriFonteCptSelect");
            DatabaseCemi.AddInParameter(dbCommand, "@soloNonGeoreferenziati", DbType.Boolean, soloNonGeoreferenziati);

            using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
            {
                while (reader.Read())
                {
                    CantiereFonte cantiere = new CantiereFonte();
                    cantiere.NomeFonte = "Cpt";

                    cantiere.Id = (int) reader["id"];
                    if (reader["indirizzo"] != DBNull.Value)
                        cantiere.Indirizzo = (string) reader["indirizzo"];
                    if (reader["civico"] != DBNull.Value)
                        cantiere.Civico = (string) reader["civico"];
                    if (reader["comune"] != DBNull.Value)
                        cantiere.Comune = (string) reader["comune"];
                    if (reader["cap"] != DBNull.Value)
                        cantiere.Cap = (string) reader["cap"];
                    if (reader["provincia"] != DBNull.Value)
                        cantiere.Provincia = (string) reader["provincia"];

                    listaCantieri.Add(cantiere);
                }
            }

            return listaCantieri;
        }

        public void UpdateCantiereCoordinateFonteAem(object idCantiere, GeocodingResult geoResult)
        {
            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriFonteAemUpdateDatiGoogle");
            DatabaseCemi.AddInParameter(dbCommand, "@idCantiere", DbType.Int32, idCantiere);
            DatabaseCemi.AddInParameter(dbCommand, "@latitudine", DbType.Double, geoResult.Latitude);
            DatabaseCemi.AddInParameter(dbCommand, "@longitudine", DbType.Double, geoResult.Longitude);
            DatabaseCemi.AddInParameter(dbCommand, "@indirizzoGeocoder", DbType.String, geoResult.Indirizzo);
            DatabaseCemi.AddInParameter(dbCommand, "@civicoGeocoder", DbType.String, geoResult.Civico);
            DatabaseCemi.AddInParameter(dbCommand, "@provinciaGeocoder", DbType.String, geoResult.Provincia);
            DatabaseCemi.AddInParameter(dbCommand, "@comuneGeocoder", DbType.String, geoResult.Comune);
            DatabaseCemi.AddInParameter(dbCommand, "@capGeocoder", DbType.String, geoResult.Cap);

            databaseCemi.ExecuteNonQuery(dbCommand);
        }

        public void UpdateCantiereCoordinateFonteAsle(object idCantiere, GeocodingResult geoResult)
        {
            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriFonteAsleUpdateDatiGoogle");
            DatabaseCemi.AddInParameter(dbCommand, "@idCantiere", DbType.Int32, idCantiere);
            DatabaseCemi.AddInParameter(dbCommand, "@latitudine", DbType.Double, geoResult.Latitude);
            DatabaseCemi.AddInParameter(dbCommand, "@longitudine", DbType.Double, geoResult.Longitude);
            DatabaseCemi.AddInParameter(dbCommand, "@indirizzoGeocoder", DbType.String, geoResult.Indirizzo);
            DatabaseCemi.AddInParameter(dbCommand, "@civicoGeocoder", DbType.String, geoResult.Civico);
            DatabaseCemi.AddInParameter(dbCommand, "@provinciaGeocoder", DbType.String, geoResult.Provincia);
            DatabaseCemi.AddInParameter(dbCommand, "@comuneGeocoder", DbType.String, geoResult.Comune);
            DatabaseCemi.AddInParameter(dbCommand, "@capGeocoder", DbType.String, geoResult.Cap);

            databaseCemi.ExecuteNonQuery(dbCommand);
        }

        public void UpdateCantiereCoordinateFonteCpt(object idCantiere, GeocodingResult geoResult)
        {
            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriFonteCptUpdateDatiGoogle");
            DatabaseCemi.AddInParameter(dbCommand, "@idCantiere", DbType.Int32, idCantiere);
            DatabaseCemi.AddInParameter(dbCommand, "@latitudine", DbType.Double, geoResult.Latitude);
            DatabaseCemi.AddInParameter(dbCommand, "@longitudine", DbType.Double, geoResult.Longitude);
            DatabaseCemi.AddInParameter(dbCommand, "@indirizzoGeocoder", DbType.String, geoResult.Indirizzo);
            DatabaseCemi.AddInParameter(dbCommand, "@civicoGeocoder", DbType.String, geoResult.Civico);
            DatabaseCemi.AddInParameter(dbCommand, "@provinciaGeocoder", DbType.String, geoResult.Provincia);
            DatabaseCemi.AddInParameter(dbCommand, "@comuneGeocoder", DbType.String, geoResult.Comune);
            DatabaseCemi.AddInParameter(dbCommand, "@capGeocoder", DbType.String, geoResult.Cap);

            databaseCemi.ExecuteNonQuery(dbCommand);
        }

        public int NumeroDiProgrammazioniPerCantiere(int idCantiere)
        {
            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriProgrammazioneNumeroPerCantiere");
            DatabaseCemi.AddInParameter(dbCommand, "@idCantiere", DbType.Int32, idCantiere);

            return (int) databaseCemi.ExecuteScalar(dbCommand);
        }

        public bool DeleteIspezioneImpresa(int idRapportoIspezioneImpresa)
        {
            bool res = false;

            DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseDelete");
            DatabaseCemi.AddInParameter(dbCommand, "@idIspezioneImpresa", DbType.Int32, idRapportoIspezioneImpresa);

            if (((int) databaseCemi.ExecuteScalar(dbCommand)) > 0)
                res = true;

            return res;
        }

        public bool UpdateCommittenteIspettore(Committente committente)
        {
            bool res = false;

            if (committente != null)
            {
                if (committente.IdCommittente.HasValue && committente.RagioneSociale != null)
                {
                    // Proseguo con l'aggiornamento
                    DbCommand comando =
                        databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCommittentiUpdatePerIspettori");

                    DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32, committente.IdCommittente.Value);
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, committente.RagioneSociale);
                    if (!string.IsNullOrEmpty(committente.PartitaIva))
                        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, committente.PartitaIva);
                    if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, committente.CodiceFiscale);
                    if (!string.IsNullOrEmpty(committente.Indirizzo))
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                    if (!string.IsNullOrEmpty(committente.Provincia))
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                    if (!string.IsNullOrEmpty(committente.Comune))
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                    if (!string.IsNullOrEmpty(committente.Cap))
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                    if (!string.IsNullOrEmpty(committente.Telefono))
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, committente.Telefono);
                    if (!string.IsNullOrEmpty(committente.Fax))
                        DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, committente.Fax);
                    if (!string.IsNullOrEmpty(committente.Cap))
                        DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                                                    committente.PersonaRiferimento);

                    int resQuery = databaseCemi.ExecuteNonQuery(comando);
                    if (resQuery == 1)
                    {
                        res = true;
                        committente.Modificato = true;
                    }
                }
                else
                    throw new ArgumentException();
            }
            else
                throw new ArgumentNullException();

            return res;
        }

        public IspettoreCollection GetGruppoIspezione(int? idIspezione)
        {
            IspettoreCollection gruppoIspezione = new IspettoreCollection();

            if (idIspezione.HasValue)
            {
                using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniGruppiSelect"))
                {
                    databaseCemi.AddInParameter(dbCommand, "@idIspezione", DbType.Int32, idIspezione.Value);

                    using (IDataReader reader = databaseCemi.ExecuteReader(dbCommand))
                    {
                        while (reader.Read())
                        {
                            Ispettore ispettore = new Ispettore();
                            gruppoIspezione.Add(ispettore);

                            ispettore.IdIspettore = (int) reader["idIspettore"];
                            ispettore.Cognome = (string) reader["cognome"];
                            ispettore.Nome = (string) reader["nome"];
                        }
                    }
                }
            }

            return gruppoIspezione;
        }

        #region Lettere

        public List<WordField> GetWordDataSource(TipologiaLettera tipoLettera, LetteraParam param)
        {
            List<WordField> ds = new List<WordField>();

            string spName;

            switch (tipoLettera)
            {
                case TipologiaLettera.M70505:
                    spName = "dbo.USP_CantieriLetteraVerifica";
                    break;
                case TipologiaLettera.M7050502Comune:
                    spName = "dbo.USP_CantieriLetteraVerificaComune";
                    break;
                case TipologiaLettera.M7050601:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaPubblico";
                    break;
                case TipologiaLettera.M7050602:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaPrivati";
                    break;
                case TipologiaLettera.M7050603:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaPubblico";
                    break;
                case TipologiaLettera.M70506:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaPrivati";
                    break;
                case TipologiaLettera.M7050901:
                    spName = "dbo.USP_CantieriLetteraEsitoPositivo";
                    break;
                case TipologiaLettera.M70509:
                    spName = "dbo.USP_CantieriLetteraEsitoPositivo";
                    break;
                case TipologiaLettera.M70510:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaCommittente";
                    break;
                case TipologiaLettera.M70511:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaCommittente";
                    break;
                case TipologiaLettera.M70501:
                    spName = "dbo.USP_CantieriLetteraSindacati";
                    break;
                case TipologiaLettera.M7050501:
                    spName = "dbo.USP_CantieriLetteraBollinoBlu";
                    break;
                case TipologiaLettera.M70511Comune:
                case TipologiaLettera.M7051101Comune:
                    spName = "dbo.USP_CantieriLetteraIrregolaritaComune";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("tipoLettera");
            }

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand(spName))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idIspezione", DbType.Int32, param.IdIspezione);
                if (param.IdImpresa.HasValue)
                    DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, param.IdImpresa);
                if (param.IdCantieriImpresa.HasValue)
                    DatabaseCemi.AddInParameter(dbCommand, "@idCantieriImpresa", DbType.Int32, param.IdCantieriImpresa);

                using (IDataReader dr = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            WordField wf = new WordField();
                            wf.Campo = dr.GetName(i);
                            if (dr[i] is DateTime)
                            {
                                wf.Valore = ((DateTime) dr[i]).ToShortDateString();
                            }
                            else
                            {
                                wf.Valore = dr[i].ToString();
                            }

                            ds.Add(wf);
                        }
                    }
                }
            }

            return ds;
        }

        public void LogLettera(DateTime data, string protocollo, string fileLettera, int idIspezione,
                               GruppoLettera gruppoLettera,
                               int? idImpresa, int? idCantieriImpresa)
        {
            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLogLettereInsertUpdate"))
            {
                databaseCemi.AddInParameter(dbCommand, "@protocollo", DbType.String, protocollo);
                databaseCemi.AddInParameter(dbCommand, "@data", DbType.DateTime, data);

                byte[] byteLettera = null;
                using (FileStream lettera = new FileStream(fileLettera, FileMode.Open))
                {
                    byteLettera = new byte[lettera.Length];
                    lettera.Read(byteLettera, 0, byteLettera.Length);
                }

                databaseCemi.AddInParameter(dbCommand, "@lettera", DbType.Binary, byteLettera);
                databaseCemi.AddInParameter(dbCommand, "@idIspezione", DbType.Int32, idIspezione);
                databaseCemi.AddInParameter(dbCommand, "@idTipoLettera", DbType.Int32, gruppoLettera);
                if (idImpresa.HasValue)
                    databaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa.Value);
                if (idCantieriImpresa.HasValue)
                    databaseCemi.AddInParameter(dbCommand, "@idCantieriImpresa", DbType.Int32, idCantieriImpresa.Value);

                databaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public TipologiaAppalto GetTipologiaAppalto(int idIspezione)
        {
            TipologiaAppalto tipoAppalto;

            using (
                DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriSelectTipoAppaltoById")
                )
            {
                databaseCemi.AddInParameter(dbCommand, "@idIspezione", DbType.Int32, idIspezione);

                int tipoAppaltoInt = (int) databaseCemi.ExecuteScalar(dbCommand);
                tipoAppalto = (TipologiaAppalto) Enum.Parse(typeof(TipologiaAppalto), tipoAppaltoInt.ToString());
            }

            return tipoAppalto;
        }

        public List<LogLettera> GetLogLettere(LettereFilter filtro)
        {
            List<LogLettera> ret = new List<LogLettera>();

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLogLettereSelectWithFilter"))
            {
                if (filtro.Dal.HasValue)
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@dal", DbType.DateTime, filtro.Dal.Value);
                }
                if (filtro.Al.HasValue)
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@al", DbType.DateTime, filtro.Al.Value);
                }
                if (filtro.TipoLettera.HasValue)
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@tipoLettera", DbType.Int32, filtro.TipoLettera.Value);
                }
                if (!String.IsNullOrEmpty(filtro.Protocollo))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@protocollo", DbType.String, filtro.Protocollo);
                }
                if (!String.IsNullOrEmpty(filtro.IndirizzoCantiere))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@indirizzoCantiere", DbType.String, filtro.IndirizzoCantiere);
                }
                if (!String.IsNullOrEmpty(filtro.ComuneCantiere))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@comuneCantiere", DbType.String, filtro.ComuneCantiere);
                }
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!String.IsNullOrEmpty(filtro.RagioneSocialeImpresa))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@ragioneSocialeImpresa", DbType.String, filtro.RagioneSocialeImpresa);
                }
                if (!String.IsNullOrEmpty(filtro.IvaFiscImpresa))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@ivaFiscImpresa", DbType.String, filtro.IvaFiscImpresa);
                }

                using (IDataReader dr = databaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per reader
                    Int32 indiceProtocollo = dr.GetOrdinal("protocollo");
                    Int32 indiceGiorno = dr.GetOrdinal("data");
                    Int32 indiceIspezioneGiorno = dr.GetOrdinal("giorno");
                    Int32 indiceTipoLettera = dr.GetOrdinal("idCantieriTipologiaLettera");
                    Int32 indiceCantiereIndirizzo = dr.GetOrdinal("indirizzo");
                    Int32 indiceComune = dr.GetOrdinal("comune");
                    Int32 indiceProvincia = dr.GetOrdinal("provincia");
                    #endregion

                    while (dr.Read())
                    {
                        LogLettera lettera = new LogLettera();
                        lettera.Protocollo = dr.GetString(indiceProtocollo);
                        ;
                        lettera.Giorno = dr.GetDateTime(indiceGiorno);
                        lettera.IspezioneGiorno = dr.GetDateTime(indiceIspezioneGiorno);
                        lettera.TipoLettera = (GruppoLettera) dr.GetInt32(indiceTipoLettera);
                        lettera.CantiereIndirizzo = dr.GetString(indiceCantiereIndirizzo);
                        if (!dr.IsDBNull(indiceComune))
                        {
                            lettera.CantiereComune = dr.GetString(indiceComune);
                        }
                        if (!dr.IsDBNull(indiceProvincia))
                        {
                            lettera.CantiereProvincia = dr.GetString(indiceProvincia);
                        }

                        ret.Add(lettera);
                    }
                }
            }

            return ret;
        }

        public List<LogLettera> GetLogLetterePerIspezione(int idIspezione)
        {
            List<LogLettera> ret = new List<LogLettera>();

            using (
                DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLogLettereSelectByIspezione"))
            {
                databaseCemi.AddInParameter(dbCommand, "@idIspezione", DbType.Int32, idIspezione);

                using (IDataReader dr = databaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        LogLettera lettera = new LogLettera();
                        lettera.Protocollo = (string) dr["protocollo"];
                        lettera.Giorno = (DateTime) dr["data"];
                        lettera.TipoLettera = (GruppoLettera) dr["idCantieriTipologiaLettera"];

                        if (dr["idCantieriImpresa"] != DBNull.Value)
                        {
                            lettera.TipoImpresa = TipologiaImpresa.Cantieri;
                            lettera.IdImpresa = (int) dr["idCantieriImpresa"];
                        }
                        if (dr["idImpresa"] != DBNull.Value)
                        {
                            lettera.TipoImpresa = TipologiaImpresa.SiceNew;
                            lettera.IdImpresa = (int) dr["idImpresa"];
                        }
                        if (dr["ragioneSociale"] != DBNull.Value)
                        {
                            lettera.RagioneSocialeImpresa = (string) dr["ragioneSociale"];
                        }

                        ret.Add(lettera);
                    }
                }
            }

            return ret;
        }

        public byte[] GetLettera(string protocollo)
        {
            byte[] ret = null;

            using (
                DbCommand dbCommand =
                    databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLogLettereSelectLetteraByProtocollo"))
            {
                databaseCemi.AddInParameter(dbCommand, "@protocollo", DbType.String, protocollo);

                using (IDataReader dr = databaseCemi.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        ret = (byte[]) dr["lettera"];
                    }
                }
            }

            return ret;
        }

        public List<SubappaltoLettera> GetSubappaltiLettera(int idIspezione)
        {
            List<SubappaltoLettera> ret = new List<SubappaltoLettera>();

            using (
                DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSubappaltiSelectByIspezione"))
            {
                databaseCemi.AddInParameter(dbCommand, "@idIspezione", DbType.Int32, idIspezione);
                using (IDataReader dr = databaseCemi.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        SubappaltoLettera sub = new SubappaltoLettera();
                        if (dr["idCantieriImpresaSubappaltata"] != DBNull.Value)
                            sub.IdCantieriImpresa = (int) dr["idCantieriImpresaSubappaltata"];
                        if (dr["idImpresaSubappaltata"] != DBNull.Value)
                            sub.IdImpresaSubappaltata = (int) dr["idImpresaSubappaltata"];
                        sub.RagioneSocialeCantieriImp = dr["ragioneSocialeCantieriImp"] as string;
                        sub.RagioneSocialeImp = dr["ragioneSocialeImp"] as string;
                        ret.Add(sub);
                    }
                }
            }

            return ret;
        }

        public bool DeleteLettera(string protocollo)
        {
            bool res = false;

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriLogLettereDelete"))
            {
                databaseCemi.AddInParameter(dbCommand, "@protocollo", DbType.String, protocollo);

                if (databaseCemi.ExecuteNonQuery(dbCommand) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Gestione scadenza

        /// <summary>
        /// Aggiorna il numero di giorni oltre i quali un ispettore non pu� pi� modificare un rapporto d'ispezione
        /// </summary>
        /// <returns></returns>
        public void AggiornaScadenza(int scadenza)
        {
            DbCommand comando =
                databaseCemi.GetStoredProcCommand("dbo.USP_ParametriUpdateCantieriProgrammazioneScadenza");
            DatabaseCemi.AddInParameter(comando, "@programmazione scadenza", DbType.Int32, scadenza);

            databaseCemi.ExecuteScalar(comando);
        }

        /// <summary>
        /// Ritorna il numero di giorni oltre i quali un ispettore non pu� pi� modificare un rapporto d'ispezione
        /// </summary>
        /// <returns></returns>
        public int GetScadenza()
        {
            DbCommand comando =
                databaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectCantieriProgrammazioneScadenza");
            int scadenza = (int) databaseCemi.ExecuteScalar(comando);

            return scadenza;
        }

        #endregion

        public Boolean InsertSegnalazione(Segnalazione segnalazione)
        {
            Boolean res = false;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (InsertSegnalazione(segnalazione, transaction))
                        {
                            if (UpdateCantiereConSegnalazione(segnalazione, transaction))
                            {
                                res = true;
                                transaction.Commit();
                            }
                            else
                            {
                                throw new Exception("InsertSegnalazione: aggiornamento del cantiere non andato a buon fine");
                            }
                        }
                        else
                        {
                            throw new Exception("InsertSegnalazione: inserimento della segnalazione non andato a buon fine");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private Boolean InsertSegnalazione(Segnalazione segnalazione, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriSegnalazioniInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@data", DbType.DateTime, segnalazione.Data);
                DatabaseCemi.AddInParameter(dbCommand, "@ricorrente", DbType.Boolean, segnalazione.Ricorrente);
                DatabaseCemi.AddInParameter(dbCommand, "@idMotivazione", DbType.Int32, segnalazione.Motivazione.Id);
                DatabaseCemi.AddInParameter(dbCommand, "@idPriorita", DbType.Int32, segnalazione.Priorita.Id);
                DatabaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, segnalazione.IdUtente);
                if (!String.IsNullOrEmpty(segnalazione.Note))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@note", DbType.String, segnalazione.Note);
                }
                DatabaseCemi.AddOutParameter(dbCommand, "@idSegnalazione", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(dbCommand, transaction);

                segnalazione.IdSegnalazione = (Int32) DatabaseCemi.GetParameterValue(dbCommand, "@idSegnalazione");

                if (segnalazione.IdSegnalazione > 0)
                {
                    res = true;
                }
            }

            return res;
        }

        private Boolean UpdateCantiereConSegnalazione(Segnalazione segnalazione, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriUpdateSegnalazione"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idCantiere", DbType.Int32, segnalazione.IdCantiere);
                DatabaseCemi.AddInParameter(dbCommand, "@idSegnalazione", DbType.Int32, segnalazione.IdSegnalazione);

                if (DatabaseCemi.ExecuteNonQuery(dbCommand, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean InsertAssegnazione(Assegnazione assegnazione)
        {
            Boolean res = false;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (InsertAssegnazione(assegnazione, transaction))
                        {
                            if (UpdateCantiereConAssegnazione(assegnazione, transaction))
                            {
                                res = true;
                                transaction.Commit();
                            }
                            else
                            {
                                throw new Exception("InsertAssegnazione: aggiornamento del cantiere");
                            }
                        }
                        else
                        {
                            throw new Exception("InsertAssegnazione: inserimento dell'assegnazione non andato a buon fine");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private bool UpdateCantiereConAssegnazione(Assegnazione assegnazione, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCantieriUpdateAssegnazione"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idCantiere", DbType.Int32, assegnazione.IdCantiere);
                DatabaseCemi.AddInParameter(dbCommand, "@idAssegnazione", DbType.Int32, assegnazione.IdAssegnazione);

                if (DatabaseCemi.ExecuteNonQuery(dbCommand, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private bool InsertAssegnazione(Assegnazione assegnazione, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriAssegnazioniInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@data", DbType.DateTime, assegnazione.Data);
                if (assegnazione.MotivazioneRifiuto != null)
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@idMotivazione", DbType.Int32,
                                                assegnazione.MotivazioneRifiuto.Id);
                }
                if (assegnazione.Priorita != null)
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@idPriorita", DbType.Int32,
                                                assegnazione.Priorita.Id);
                }
                DatabaseCemi.AddOutParameter(dbCommand, "@idAssegnazione", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(dbCommand, transaction);

                assegnazione.IdAssegnazione = (Int32) DatabaseCemi.GetParameterValue(dbCommand, "@idAssegnazione");

                if (assegnazione.IdAssegnazione > 0)
                {
                    if (InsertAssegnazioneIspettori(assegnazione, transaction))
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        private Boolean InsertAssegnazioneIspettori(Assegnazione assegnazione, DbTransaction transaction)
        {
            Boolean res = true;

            foreach (TBridge.Cemi.Type.Domain.Ispettore ispettore in assegnazione.Ispettori)
            {
                using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriAssegnazioneComposizioneInsert"))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@idAssegnazione", DbType.Int32, assegnazione.IdAssegnazione);
                    DatabaseCemi.AddInParameter(dbCommand, "@idIspettore", DbType.Int32, ispettore.IdIspettore);

                    if (DatabaseCemi.ExecuteNonQuery(dbCommand, transaction) != 1)
                    {
                        res = false;
                    }
                }
            }

            return res;
        }

        public void CambioPresaInCarico(Int32 idAttivita, Int32 idIspettorePromosso)
        {
            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriCambioPresaInCaricoUpdate"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idAttivita", DbType.Int32, idAttivita);
                DatabaseCemi.AddInParameter(dbCommand, "@idIspettore", DbType.Int32, idIspettorePromosso);
                DatabaseCemi.AddOutParameter(dbCommand, "@errore", DbType.String, 200);

                DatabaseCemi.ExecuteNonQuery(dbCommand);

                String errore = DatabaseCemi.GetParameterValue(dbCommand, "@errore").ToString();
                if (!String.IsNullOrEmpty(errore))
                {
                    throw new Exception(errore);
                }
            }
        }

        public CantieriStatistiche GetStatisticheCantieri(DateTime dal, DateTime al)
        {
            CantieriStatistiche statistiche = null;

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriStatisticheNumeriIspezioni"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@dal", DbType.DateTime, dal);
                DatabaseCemi.AddInParameter(dbCommand, "@al", DbType.DateTime, al);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    statistiche = new CantieriStatistiche();

                    // Segnalazioni
                    while (reader.Read())
                    {
                        SegnalazioneStatistica segStat = new SegnalazioneStatistica();
                        statistiche.Segnalazioni.Add(segStat);

                        segStat.CantieriSegnalazionePriorita = new Cemi.Type.Domain.CantiereSegnalazionePriorita();
                        segStat.CantieriSegnalazionePriorita.Id = (Int32) reader["idCantieriSegnalazionePriorita"];
                        segStat.CantieriSegnalazionePriorita.Descrizione = (String) reader["descrizione"];
                        segStat.NumeroCantieri = (Int32) reader["numeroCantieriSegnalati"];
                        segStat.NumeroAssegnatiRifiutati = (Int32) reader["numeroCantieriAssegnatiRifiutati"];
                    }

                    // Assegnazioni
                    reader.NextResult();
                    // Assegnazioni Totali
                    while (reader.Read())
                    {
                        AssegnazioneStatistica assStat = new AssegnazioneStatistica();
                        statistiche.Assegnazioni.Add(assStat);

                        assStat.Assegnati = (String) reader["statoAssegnazione"] == "ASS";
                        assStat.NumeroCantieri = (Int32) reader["numeroCantieriAssegnati"];
                        assStat.NumeroPresiInCarico = (Int32) reader["numeroPresiInCarico"];
                    }

                    // Assegnazioni Per Ispettore
                    reader.NextResult();
                    while (reader.Read())
                    {
                        AssegnazionePerIspettoreStatistica assPerIspStat = new AssegnazionePerIspettoreStatistica();
                        statistiche.AssegnazioniPerIspettore.Add(assPerIspStat);

                        assPerIspStat.IdIspettore = (Int32) reader["idIspettore"];
                        assPerIspStat.Cognome = (String) reader["cognome"];
                        assPerIspStat.Nome = (String) reader["nome"];
                        assPerIspStat.NumeroAssegnazioni = (Int32) reader["numeroCantieriAssegnati"];
                    }

                    // Prese in carico
                    reader.NextResult();
                    while (reader.Read())
                    {
                        PresaInCaricoStatistica presInCar = new PresaInCaricoStatistica();
                        statistiche.PreseInCarico.Add(presInCar);

                        Int32 statoIspezione;
                        statoIspezione = (Int32) reader["statoIspezione"];
                        if (statoIspezione != -1)
                        {
                            presInCar.StatoIspezione = (StatoIspezione) statoIspezione;
                        }
                        presInCar.NumeroPreseInCarico = (Int32) reader["numeroPreseInCarico"];
                        presInCar.NumeroRapportiIspezione = (Int32) reader["numeroRapportiIspezione"];
                    }

                    // Prese in carico Per Ispettore
                    reader.NextResult();
                    while (reader.Read())
                    {
                        PresaInCaricoPerIspettoreStatistica presInCarPerIsp = new PresaInCaricoPerIspettoreStatistica();
                        statistiche.PreseInCaricoPerIspettore.Add(presInCarPerIsp);

                        presInCarPerIsp.IdIspettore = (Int32) reader["idIspettore"];
                        presInCarPerIsp.Cognome = (String) reader["cognome"];
                        presInCarPerIsp.Nome = (String) reader["nome"];
                        presInCarPerIsp.NumeroPreseInCarico = (Int32) reader["numeroPreseInCarico"];
                    }
                }
            }

            return statistiche;
        }

        public Int32? GetIdImpresaDaCodiceFiscale(String codiceFiscale)
        {
            Int32? idImpresa = null;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriRecuperaCodiceImpresaDaCodiceFiscale"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                idImpresa = DatabaseCemi.GetParameterValue(comando, "@idImpresa") as Int32?;
            }

            return idImpresa;
        }

        public ContrattoCollection GetContratti()
        {
            ContrattoCollection contratti = new ContrattoCollection();

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriContrattiSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    Int32 indiceId = reader.GetOrdinal("idCantieriContratto");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    // Segnalazioni
                    while (reader.Read())
                    {
                        Contratto contratto = new Contratto();
                        contratti.Add(contratto);

                        contratto.Id = reader.GetInt32(indiceId);
                        contratto.Descrizione = reader.GetString(indiceDescrizione);
                    }

                    return contratti;
                }
            }
        }

        public CassaEdileCollection GetIspezioneImpresaCasseEdili(Int32 idIspezioneImpresa)
        {
            CassaEdileCollection casseEdili = new CassaEdileCollection();

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseCasseEdiliSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    Int32 indiceIdCassaEdile = reader.GetOrdinal("idCassaEdile");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    // Segnalazioni
                    while (reader.Read())
                    {
                        CassaEdile ce = new CassaEdile();
                        casseEdili.Add(ce);

                        ce.IdCassaEdile = reader.GetString(indiceIdCassaEdile);
                        ce.Descrizione = reader.GetString(indiceDescrizione);
                    }

                    return casseEdili;
                }
            }
        }

        public ContrattoCollection GetIspezioneImpresaContratti(Int32 idIspezioneImpresa)
        {
            ContrattoCollection contratti = new ContrattoCollection();

            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseContrattiSelect"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    Int32 indiceIdContratto = reader.GetOrdinal("idCantieriContratto");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    // Segnalazioni
                    while (reader.Read())
                    {
                        Contratto co = new Contratto();
                        contratti.Add(co);

                        co.Id = reader.GetInt32(indiceIdContratto);
                        co.Descrizione = reader.GetString(indiceDescrizione);
                    }

                    return contratti;
                }
            }
        }

        public void DeleteCassaEdile(Int32 idIspezioneImpresa, String idCassaEdile)
        {
            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseCasseEdiliDeleteSingle"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@idCassaEdile", DbType.String, idCassaEdile);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public void DeleteContratto(Int32 idIspezioneImpresa, String idContratto)
        {
            using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CantieriIspezioniImpreseContrattiDeleteSingle"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idIspezioneImpresa", DbType.Int32, idIspezioneImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@idContratto", DbType.Int32, idContratto);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }
    }
}