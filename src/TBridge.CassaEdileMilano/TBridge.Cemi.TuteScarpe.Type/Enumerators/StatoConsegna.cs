﻿namespace TBridge.Cemi.TuteScarpe.Type.Enumerators
{
    public enum StatoConsegna
    {
        Consegnato,
        Avvisato,
        ConcordatoConsegna,
        Giacenza
    }
}