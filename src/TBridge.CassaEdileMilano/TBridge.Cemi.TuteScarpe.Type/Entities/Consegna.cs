﻿using System;

namespace TBridge.Cemi.TuteScarpe.Type.Entities
{
    public class Consegna
    {
        public Int32 NumeroOrdine { get; set; }
        public Int32 CodiceImpresa { get; set; }
        public String NumeroSpedizione { get; set; }
        public DateTime DataEvento { get; set; }
        public String Stato { get; set; }
        public Int32? IdentificativoFile { get; set; }
    }
}