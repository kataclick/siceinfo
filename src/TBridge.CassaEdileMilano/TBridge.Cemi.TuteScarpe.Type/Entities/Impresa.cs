﻿using System;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.TuteScarpe.Type.Entities
{
    public class Impresa : TBridge.Cemi.Type.Entities.Impresa
    {
        public String EmailConsulente { get; set; }
        public String EmailImpresa { get; set; }
        public Indirizzo Recapito { get; set; }
        public String Telefono { get; set; }
    }
}