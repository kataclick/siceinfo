namespace TBridge.Cemi.ImpreseAdemp.Type.Filters
{
    public class EccezioneListaFilter
    {
        public int? IdImpresa { get; set; }

        public string RagioneSociale { get; set; }
    }
}