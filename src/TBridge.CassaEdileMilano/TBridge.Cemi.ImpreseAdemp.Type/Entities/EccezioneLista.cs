namespace TBridge.Cemi.ImpreseAdemp.Type.Entities
{
    public class EccezioneLista
    {
        private Impresa impresa;

        public EccezioneLista()
        {
            impresa = new Impresa();
        }

        public Impresa Impresa
        {
            get { return impresa; }
            set { impresa = value; }
        }

        public int? ImpresaId
        {
            get
            {
                if (impresa != null) return impresa.IdImpresa;
                else return null;
            }
        }

        public string ImpresaRagioneSociale
        {
            get
            {
                if (impresa != null) return impresa.RagioneSociale;
                else return null;
            }
        }

        public bool Stato { get; set; }
    }
}