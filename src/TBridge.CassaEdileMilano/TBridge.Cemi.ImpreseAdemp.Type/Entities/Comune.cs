﻿using System;

namespace TBridge.Cemi.ImpreseAdemp.Type.Entities
{
    public class Comune
    {
        public String CodiceCatastale { get; set; }
        public String Nome { get; set; }
    }
}