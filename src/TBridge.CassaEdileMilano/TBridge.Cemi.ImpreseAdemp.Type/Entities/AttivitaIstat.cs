﻿using System;

namespace TBridge.Cemi.ImpreseAdemp.Type.Entities
{
    public class AttivitaIstat
    {
        public String CodiceAttivita { get; set; }
        public String Descrizione { get; set; }
    }
}