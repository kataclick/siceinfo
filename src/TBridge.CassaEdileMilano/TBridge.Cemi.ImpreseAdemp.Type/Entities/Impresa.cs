namespace TBridge.Cemi.ImpreseAdemp.Type.Entities
{
    public class Impresa
    {
        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }
    }
}