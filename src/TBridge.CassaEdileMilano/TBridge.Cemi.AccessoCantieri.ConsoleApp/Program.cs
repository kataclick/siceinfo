using System;
using System.Configuration;
using System.Timers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.SelestaProvider;
using TBridge.Cemi.AccessoCantieri.TTProvider;
using TimbraturaSelesta = TBridge.Cemi.AccessoCantieri.SelestaProvider.Timbratura;
using TimbraturaTT = TBridge.Cemi.AccessoCantieri.TTProvider.Timbratura;

namespace TBridge.Cemi.AccessoCantieri.ConsoleApp
{
    internal static class Program
    {
        private static readonly AccessoCantieriBusiness Biz = new AccessoCantieriBusiness();
        private static readonly ProgettiESoluzioniConnector ProgettiSoluzioniBiz = new ProgettiESoluzioniConnector();
        private static readonly SelestaConnector SelestaBiz = new SelestaConnector();
        private static readonly TymbroConnector TymbroBiz = new TymbroConnector();
        private static readonly TrexomConnector TrexomBiz = new TrexomConnector();
        private static readonly TTConnector TtBiz = new TTConnector();

        private static Timer _timerAccessi;

        private static void Main()
        {
            InizializzaTimer();

            CaricaAccessi();

            Console.ReadLine();
        }

        private static void InizializzaTimer()
        {
            _timerAccessi = new Timer
                                {
                                    Interval =
                                        Int32.Parse(ConfigurationManager.AppSettings["VerificaAccessiTimerInterval"])
                                };
            _timerAccessi.Elapsed += TimerAccessiElapsed;
        }

        private static void TimerAccessiElapsed(object sender, ElapsedEventArgs e)
        {
            _timerAccessi.Stop();
            CaricaAccessi();
        }

        private static void CaricaAccessi()
        {
            #region TreTorri

            if (ConfigurationManager.AppSettings["AcquisizioneTreTorriAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura Tre Torri" + DateTime.Now);

                    TimbratureService ws = new TimbratureService();

                    TimbraturaTT[] timbrature = ws.GetTimbrature();
                    //TimbraturaTT[] timbratureRecovery = ws.GetTimbraturePeriodo(DateTime.Now.ToString("dd-MM-yyyy"),
                    //DateTime.Now.AddDays(1).ToString(
                    //    "dd-MM-yyyy");

                    //Int32 iS = TtBiz.CaricaTimbrature(timbrature);
                    Int32 iS = 0;
                    //Int32 iSRecovery = 0;


                    iS = TtBiz.CaricaTimbrature(timbrature);
                    //if (timbratureRecovery != null)
                    //    iSRecovery = TtBiz.CaricaTimbrature(timbratureRecovery);


                    Int32 isTot = iS /*+ iSRecovery*/;

                    if (isTot == -1)
                    {
                        Console.WriteLine(" Tre Torri: Nessuna nuova timbratura");
                    }

                    else if (isTot > 0)
                        Console.WriteLine(string.Format(
                            " Tre Torri: Non lette {0} timbrature. Rilevatore non dichiarato?",
                            iS));
                    else
                        Console.WriteLine(" Tre Torri: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            #region Click&find

            if (ConfigurationManager.AppSettings["AcquisizioneSelestaAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura Click&Find" + DateTime.Now);

                    WSTimbrature ws = new WSTimbrature();

                    TimbraturaSelesta[] timbrature = ws.getTimbrature();

                    TimbraturaSelesta[] timbratureRecovery = ws.getTimbraturePeriodo(
                        DateTime.Now.ToString("yyyy-MM-dd"),
                        DateTime.Now.AddDays(1).ToString(
                            "yyyy-MM-dd"));
                    Int32 iS = 0;
                    if (timbrature[0] != null)
                        iS = SelestaBiz.CaricaTimbrature(timbrature);

                    //Int32 iSRecovery = 0;
                    Int32 iSRecovery = SelestaBiz.CaricaTimbrature(timbratureRecovery);

                    Int32 isTot = iS + iSRecovery;

                    if (isTot == -1)
                    {
                        Console.WriteLine(" Selesta Click&Find: Nessuna nuova timbratura");
                    }

                    else if (isTot > 0)
                        Console.WriteLine(string.Format(
                            " Selesta Click&Find: Non lette {0} timbrature. Rilevatore non dichiarato?",
                            isTot));
                    else
                        Console.WriteLine(" Selesta Click&Find: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            #region ProgettiESoluzioni

            if (ConfigurationManager.AppSettings["AcquisizioneProgettiSoluzioniAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura Progetti e soluzioni" + DateTime.Now);
                    Int32 i = ProgettiSoluzioniBiz.CaricaDati();

                    if (i == -1)
                    {
                        Console.WriteLine(" Progetti: Nessun nuovo file");
                    }

                    else if (i > 0)
                        Console.WriteLine(string.Format(
                            " Progetti: Non lette {0} timbrature. Rilevatore non dichiarato?", i));
                    else
                        Console.WriteLine(" Progetti: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            #region Tymbro

            if (ConfigurationManager.AppSettings["AcquisizioneTymbroAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura Tymbro" + DateTime.Now);
                    Int32 iT = TymbroBiz.CaricaTimbrature();

                    if (iT == -1)
                    {
                        Console.WriteLine(" Tymbro: Nessuna nuova timbratura");
                    }

                    else if (iT > 0)
                        Console.WriteLine(string.Format(
                            " Tymbro: Non lette {0} timbrature. Rilevatore non dichiarato?", iT));
                    else
                        Console.WriteLine(" Tymbro: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            #region Trexom

            if (ConfigurationManager.AppSettings["AcquisizioneTrexomAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura Trexom" + DateTime.Now);
                    Int32 iT = TrexomBiz.CaricaTimbrature();

                    if (iT == -1)
                    {
                        Console.WriteLine(" Trexom: Nessuna nuova timbratura");
                    }

                    if (iT == -2)
                    {
                        Console.WriteLine(" Trexom: Errore nella lettura da DB");
                    }

                    else if (iT > 0)
                        Console.WriteLine(string.Format(
                            " Trexom: Non lette {0} timbrature. Rilevatore non dichiarato?", iT));
                    else
                        Console.WriteLine(" Trexom: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            #region TymbroNuovoServer

            if (ConfigurationManager.AppSettings["AcquisizioneTymbroNuovoServerAttiva"] == "1")
                try
                {
                    Console.WriteLine("Lettura Tymbro Nuovo Server" + DateTime.Now);
                    Int32 iT = TymbroBiz.CaricaTimbratureNuovoServer();

                    if (iT == -1)
                    {
                        Console.WriteLine(" Tymbro Nuovo Server: Nessuna nuova timbratura");
                    }

                    else if (iT > 0)
                        Console.WriteLine(string.Format(
                            " Tymbro Nuovo Server: Non lette {0} timbrature. Rilevatore non dichiarato?", iT));
                    else
                        Console.WriteLine(" Tymbro Nuovo Server: Lettura OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            #endregion

            #region INPS

            ////Console.WriteLine("INIZIO import INPS");
            ////biz.ImportaExcelINPS(ConfigurationManager.AppSettings["pathINPS"]);
            ////biz.ImportaExcelINPS2(ConfigurationManager.AppSettings["pathINPSScaricati"], ConfigurationManager.AppSettings["pathINPSElaborati"]);
            ////Console.WriteLine("FINE import INPS");


            ////Console.WriteLine("INIZIO export INPS");
            ////try
            ////{
            ////    biz.CreaExcelINPS(ConfigurationManager.AppSettings["pathINPSCreazione"]);
            ////}
            ////catch (Exception ex)
            ////{
            ////    Console.WriteLine(ex.Message);
            ////    Console.Read();
            ////}
            ////Console.WriteLine("FINE export INPS");

            #endregion

            #region denunce

            //if (ConfigurationManager.AppSettings["ControlloDenunceAttivo"] == "1")
            //{
            //    if (Int32.Parse(DateTime.Now.Hour.ToString("HH")) >= Int32.Parse(ConfigurationManager.AppSettings["OraInizioControlloDenunce"]) &&
            //        Int32.Parse(DateTime.Now.Hour.ToString("HH")) < Int32.Parse(ConfigurationManager.AppSettings["OraFineControlloDenunce"]))
            //    {
            //        Console.WriteLine("INIZIO controllo denunce");
            //        ControlloDenunce();
            //        Console.WriteLine("FINE controllo denunce");
            //    }
            //}

            #endregion

            # region Debiti

            //if (ConfigurationManager.AppSettings["ControlloDebitiAttivo"] == "1")
            //{
            //    if (Int32.Parse(DateTime.Now.Hour.ToString("HH")) >= Int32.Parse(ConfigurationManager.AppSettings["OraInizioControlloDebiti"]) &&
            //        Int32.Parse(DateTime.Now.Hour.ToString("HH")) < Int32.Parse(ConfigurationManager.AppSettings["OraFineControlloDebiti"]))
            //    {
            //        Console.WriteLine("INIZIO controllo debiti");
            //        ControlloDebiti();
            //        Console.WriteLine("FINE controllo debiti");
            //    }
            //}

            #endregion

            # region Timbrature non gestite

            if (ConfigurationManager.AppSettings["RecuperoTimbratureNonGestiteAttivo"] == "1")
            {
                Console.WriteLine("INIZIO controllo timb");
                Biz.ControllaTimbratureNonGestite();
                Console.WriteLine("FINE controllo timb");
            }

            #endregion

            #region Timbrature non gestite senza comunicazioni

            ////Console.WriteLine("INIZIO controllo timb no comunicazioni");
            ////biz.ControllaTimbratureNonGestiteNoCom();
            ////Console.WriteLine("FINE controllo timb no comunicazioni");

            #endregion

            _timerAccessi.Start();
        }

        //private static void ControlloDenunce()
        //{
        //    TimbraturaCollection timbrature = Biz.GetTimbratureAnomalieDenunce();

        //    foreach (Timbratura timb in timbrature)
        //    {
        //        if (timb.IdImpresa != null)
        //        {
        //            TipologiaAnomaliaDenuncia tipoAnomalia = Biz.ControlloDenunce(timb.DataOra.Year, timb.DataOra.Month,
        //                                                                          timb.IdImpresa.Value,
        //                                                                          timb.IdLavoratore);

        //            Biz.UpdateTimbraturaAnomalieDenunce(timb.IdTimbratura, tipoAnomalia);
        //        }
        //    }
        //}

        //private static void ControlloDebiti()
        //{
        //    TimbraturaCollection timbrature = Biz.GetTimbratureAnomalieDebiti();

        //    foreach (Timbratura timb in timbrature)
        //    {
        //        if (timb.IdImpresa != null)
        //        {
        //            Boolean debitiOk = Biz.ControlloDebiti(timb.IdImpresa.Value, timb.DataOra.Year, timb.DataOra.Month);

        //            Biz.UpdateTimbraturaAnomalieDebiti(timb.IdTimbratura, debitiOk);
        //        }
        //    }
        //}
    }
}