using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.ImpreseAdemp.Type.Collections;
using TBridge.Cemi.ImpreseAdemp.Type.Entities;
using TBridge.Cemi.ImpreseAdemp.Type.Filters;

namespace TBridge.Cemi.ImpreseAdemp.Data
{
    public class ImpreseAdemDataAccess
    {
        private Database databaseCemi;

        public ImpreseAdemDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        public EccezioneListaCollection GetEccezioniListaImpreseAdempienti(EccezioneListaFilter filtro)
        {
            EccezioneListaCollection listaEccezioni = new EccezioneListaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_ListaImpreseRegolariEccezioniSelect"))
            {
                if (filtro.IdImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa);
                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        EccezioneLista eccezione = new EccezioneLista();
                        listaEccezioni.Add(eccezione);

                        eccezione.Impresa.IdImpresa = (int) reader["idImpresa"];
                        eccezione.Impresa.RagioneSociale = (string) reader["ragioneSociale"];
                        eccezione.Stato = (bool) reader["stato"];
                    }
                }
            }

            return listaEccezioni;
        }

        public bool InsertEccezioneListaImpreseAdempienti(EccezioneLista eccezione, out bool giaPresente)
        {
            bool res = false;
            giaPresente = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_ListaImpreseRegolariEccezioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, eccezione.Impresa.IdImpresa);
                DatabaseCemi.AddInParameter(comando, "@stato", DbType.String, eccezione.Stato);

                try
                {
                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
                catch (DbException dbExcp)
                {
                    if (dbExcp.ErrorCode == -2146232060)
                        giaPresente = true;
                    else
                        throw;
                }
            }

            return res;
        }

        public bool DeleteEccezioneListaImpreseAdempienti(int idImpresa)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_ListaImpreseRegolariEccezioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public void UpdateParametriImpreseRegolari(string singolo, string totale)
        {
            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_UpdateParametriImpreseRegolari"))
            {
                command.CommandText = "dbo.USP_UpdateParametriImpreseRegolari";
                DatabaseCemi.AddInParameter(command, "@singolo", DbType.String, singolo);
                DatabaseCemi.AddInParameter(command, "@totale", DbType.String, totale);
                DatabaseCemi.ExecuteNonQuery(command);
            }
        }

        public void GetParametriImpreseRegolari(out string singolo, out string totale)
        {
            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_GetParametriImpreseRegolari"))
            {
                using (IDataReader rs = DatabaseCemi.ExecuteReader(command))
                {
                    rs.Read();
                    singolo = rs.GetValue(0).ToString();
                    totale = rs.GetValue(1).ToString();
                    rs.Close();
                }
            }
        }

        public AttivitaIstatCollection GetAttivitaIstatImpreseAdempienti()
        {
            AttivitaIstatCollection listAttivitaIstat = new AttivitaIstatCollection();

            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRegolariAttivitaISTATSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        AttivitaIstat attivitaIstat = new AttivitaIstat();
                        attivitaIstat.CodiceAttivita = reader["idattivitaistat"].ToString();
                        attivitaIstat.Descrizione = reader["descrizione"].ToString();

                        listAttivitaIstat.Add(attivitaIstat);
                    }
                }
            }

            return listAttivitaIstat;
        }

        public ComuniCollection GetComuniSedeAmministrativaImpreseRegolari()
        {
            ComuniCollection listAttivitaIstat = new ComuniCollection();

            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRegolariComuniSedeAmministrativaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Comune attivitaIstat = new Comune();
                        attivitaIstat.Nome = reader["comune"].ToString();
                        attivitaIstat.CodiceCatastale = reader["codiceCatastale"].ToString();

                        listAttivitaIstat.Add(attivitaIstat);
                    }
                }
            }

            return listAttivitaIstat;
        }

        public ComuniCollection GetComuniSedeLegaleImpreseRegolari()
        {
            ComuniCollection listAttivitaIstat = new ComuniCollection();

            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRegolariComuniSedeLegaleSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Comune attivitaIstat = new Comune();
                        attivitaIstat.Nome = reader["comune"].ToString();
                        attivitaIstat.CodiceCatastale = reader["codiceCatastale"].ToString();

                        listAttivitaIstat.Add(attivitaIstat);
                    }
                }
            }

            return listAttivitaIstat;
        }
    }
}