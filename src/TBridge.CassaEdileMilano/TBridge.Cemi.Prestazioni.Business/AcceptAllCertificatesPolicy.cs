using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace TBridge.Cemi.Prestazioni.Business
{
    public class AcceptAllCertificatesPolicy : ICertificatePolicy
    {
        #region ICertificatePolicy Members

        public bool CheckValidationResult(
            ServicePoint srvPoint,
            X509Certificate certificate,
            WebRequest request, int certificateProblem)
        {
            // TODO:  Add AcceptAllCertificatesPolicy.CheckValidationResult
            // implementation
            return true;
        }

        #endregion
    }
}