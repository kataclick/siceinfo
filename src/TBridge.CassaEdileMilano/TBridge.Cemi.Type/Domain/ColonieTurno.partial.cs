﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class ColonieTurno
    {
        private String descrizioneAppoggio;

        public String Descrizione
        {
            get
            {
                return String.Format("{0} {1}", this.ColonieDestinazione.ColonieTipoDestinazione.Descrizione, this.Progressivo);
            }
        }

        public String DescrizioneAppoggio
        {
            get
            {
                if (this.ColonieDestinazione != null && this.ColonieDestinazione.ColonieTipoDestinazione != null)
                {
                    return String.Format("{0} {1}", this.ColonieDestinazione.ColonieTipoDestinazione.Descrizione, this.Progressivo);
                }

                return this.descrizioneAppoggio;
            }
            set
            {
                this.descrizioneAppoggio = value;
            }
        }
    }
}
