﻿using System;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantieriCalendarioAttivita
    {
        public Int32? IdCantiere { get; set; }
        private const Int32 LUNGHEZZASTRINGADESCRIZIONE = 50;

        public String CognomeIspettore
        {
            get
            {
                if (this.Ispettore != null)
                {
                    return this.Ispettore.Cognome;
                }
                else
                {
                    return null;
                }
            }
        }

        public String NomeCompletoIspettore
        {
            get
            {
                if (this.Ispettore != null)
                {
                    return this.Ispettore.NomeCompleto;
                }
                else
                {
                    return null;
                }
            }
        }

        public String DescrizioneAttivita
        {
            get
            {
                if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine("<b>");
                    sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                    sbDescrizione.AppendLine("</b>");
                    if (!String.IsNullOrEmpty(this.Descrizione))
                    {
                        sbDescrizione.AppendLine("<br />");
                        sbDescrizione.AppendLine("<br />");
                        if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                        {
                            sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                        }
                        else
                        {
                            sbDescrizione.AppendLine(this.Descrizione);
                        }
                    }
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine(String.Format("Titolare: <b>{0}</b>", this.Ispettore.NomeCompleto));
                    sbDescrizione.AppendLine("<br />");

                    foreach (Ispettore ispettore in this.IspettoriCorrelati)
                    {
                        sbDescrizione.AppendLine(ispettore.NomeCompleto);
                        sbDescrizione.AppendLine("<br />");
                    }

                    return sbDescrizione.ToString();
                }
                else
                {
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        return String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3));
                    }
                    else
                    {
                        return this.Descrizione;
                    }
                }
            }
        }

        public String DescrizioneAttivitaConRapportoIspezione(String statoIspezione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
            {
                sbDescrizione.AppendLine(String.Format("Stato: <b>{0}</b>", statoIspezione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<b>");
                sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                sbDescrizione.AppendLine("</b>");
                if (!String.IsNullOrEmpty(this.Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(this.Descrizione);
                    }
                }
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(String.Format("Titolare: <b>{0}</b>", this.Ispettore.NomeCompleto));
                sbDescrizione.AppendLine("<br />");

                foreach (Ispettore ispettore in this.IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.NomeCompleto);
                    sbDescrizione.AppendLine("<br />");
                }

                return sbDescrizione.ToString();
            }
            else
            {
                if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                }
                else
                {
                    sbDescrizione.AppendLine(this.Descrizione);
                }
            }

            return sbDescrizione.ToString();
        }

        public String DescrizioneAttivitaConRifiuto(String motivazione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
            {
                sbDescrizione.AppendLine(String.Format("Non isp: <b>{0}</b>", motivazione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<b>");
                sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                sbDescrizione.AppendLine("</b>");
                if (!String.IsNullOrEmpty(this.Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(this.Descrizione);
                    }
                }
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(String.Format("Titolare: <b>{0}</b>", this.Ispettore.NomeCompleto));
                sbDescrizione.AppendLine("<br />");

                foreach (Ispettore ispettore in this.IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.NomeCompleto);
                    sbDescrizione.AppendLine("<br />");
                }

                return sbDescrizione.ToString();
            }
            else
            {
                if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                }
                else
                {
                    sbDescrizione.AppendLine(this.Descrizione);
                }
            }

            return sbDescrizione.ToString();
        }

        public String DescrizioneAttivitaPerElencoCompleto
        {
            get
            {
                if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    foreach (Ispettore ispettore in this.IspettoriCorrelati)
                    {
                        sbDescrizione.AppendLine(ispettore.Cognome);
                        sbDescrizione.AppendLine("<br />");
                    }

                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine(String.Format("<b>{0}</b>", this.TipologiaAttivita.ToString()));
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                    if (!String.IsNullOrEmpty(this.Descrizione))
                    {
                        sbDescrizione.AppendLine("<br />");
                        sbDescrizione.AppendLine("<br />");
                        if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                        {
                            sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                        }
                        else
                        {
                            sbDescrizione.AppendLine(this.Descrizione);
                        }
                    }

                    return sbDescrizione.ToString();
                }
                else
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine(String.Format("<b>{0}</b>", this.TipologiaAttivita.ToString()));
                    sbDescrizione.AppendLine("<br />");
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(this.Descrizione);
                    }

                    return sbDescrizione.ToString();
                }
            }
        }

        public String DescrizioneAttivitaPerElencoCompletoConRapportoIspezione(String statoIspezione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
            {
                foreach (Ispettore ispettore in this.IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.Cognome);
                    sbDescrizione.AppendLine("<br />");
                }

                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(String.Format("<b>{0}</b>", this.TipologiaAttivita.ToString()));
                sbDescrizione.AppendLine(String.Format("<br />Stato: <b>{0}</b>", statoIspezione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                if (!String.IsNullOrEmpty(this.Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(this.Descrizione);
                    }
                }
            }
            else
            {
                sbDescrizione.AppendLine(String.Format("<b>{0}</b>", this.TipologiaAttivita.ToString()));
                sbDescrizione.AppendLine("<br />");
                if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                }
                else
                {
                    sbDescrizione.AppendLine(this.Descrizione);
                }
            }

            return sbDescrizione.ToString();
        }

        public String DescrizioneAttivitaPerElencoCompletoConRifiuto(String motivazione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
            {
                foreach (Ispettore ispettore in this.IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.Cognome);
                    sbDescrizione.AppendLine("<br />");
                }

                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(String.Format("<b>{0}</b>", this.TipologiaAttivita.ToString()));
                sbDescrizione.AppendLine(String.Format("<br />Non isp: <b>{0}</b>", motivazione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                if (!String.IsNullOrEmpty(this.Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(this.Descrizione);
                    }
                }
            }
            else
            {
                sbDescrizione.AppendLine(String.Format("<b>{0}</b>", this.TipologiaAttivita.ToString()));
                sbDescrizione.AppendLine("<br />");
                if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                }
                else
                {
                    sbDescrizione.AppendLine(this.Descrizione);
                }
            }

            return sbDescrizione.ToString();
        }

        public String DescrizioneAttivitaPerElencoCompletoNonHtml
        {
            get
            {
                if (this.CantieriCantieri != null && this.CantieriCantieri.Count > 0)
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine(this.CognomeIspettore);
                    foreach (Ispettore ispettore in this.IspettoriCorrelati)
                    {
                        sbDescrizione.AppendLine(ispettore.Cognome);
                    }

                    sbDescrizione.AppendLine();
                    sbDescrizione.AppendLine(this.CantieriCantieri.First().IndirizzoCompletoLimitato);
                    if (!String.IsNullOrEmpty(this.Descrizione))
                    {
                        sbDescrizione.AppendLine();
                        sbDescrizione.AppendLine();
                        if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                        {
                            sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                        }
                        else
                        {
                            sbDescrizione.AppendLine(this.Descrizione);
                        }
                    }

                    return sbDescrizione.ToString();
                }
                else
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine(this.CognomeIspettore);
                    sbDescrizione.AppendLine();
                    sbDescrizione.AppendLine(this.TipologiaAttivita.ToString());
                    if (this.Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(String.Format("{0}...", this.Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(this.Descrizione);
                    }

                    return sbDescrizione.ToString();
                }
            }
        }

        public DateTime DataFineAttivita
        {
            get
            {
                return this.Data.AddHours(this.Durata);
            }
        }

        public Boolean InCarico(Int32 idIspettore)
        {
            if (this.IdIspettore == idIspettore)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
