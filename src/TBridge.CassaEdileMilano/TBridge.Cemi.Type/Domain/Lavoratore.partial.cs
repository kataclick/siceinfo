﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    public partial class Lavoratore
    {
        public Boolean ServizioSMS { get; set; }
        public String IBAN 
        {
            get
            {
                String iban = String.Empty;
                if (!String.IsNullOrEmpty(this.codicePaese))
                {
                    iban = String.Format("{0}{1}{2}{3}{4}{5}", this.codicePaese, this.codiceCINPaese, this.codiceCIN, this.codiceABI, this.codiceCAB, this.numeroCC);
                }
                return iban;
            }
        }
    }
}
