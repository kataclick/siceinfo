﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    public partial class ColoniePersonaleRichiesteEsperienza
    {
        public String Descrizione
        {
            get
            {
                return String.Format("Dal {2} al {3} {0} presso {1}", this.Mansione, this.Impresa, this.Dal.ToString("dd/MM/yyyy"), this.Al.HasValue ? this.Al.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
        }
    }
}
