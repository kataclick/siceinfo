﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class Cantiere
    {
        private const Int32 LUNGHEZZAINDIRIZZOLIMITATO = 50;

        public String IndirizzoCompleto
        {
            get 
            {
                return String.Format("{0} {1}, {2} {3} {4}", 
                    this.Indirizzo, 
                    this.Numero, 
                    this.Cap, 
                    this.Comune, 
                    this.Provincia);
            }
        }

        public String IndirizzoCompletoLimitato
        {
            get
            {
                String indirizzoCompleto = String.Format("{0} {1}, {2} {3} {4}",
                    this.Indirizzo,
                    this.Numero,
                    this.Cap,
                    this.Comune,
                    this.Provincia);

                if (indirizzoCompleto.Length > LUNGHEZZAINDIRIZZOLIMITATO)
                {
                    indirizzoCompleto = indirizzoCompleto.Substring(0, LUNGHEZZAINDIRIZZOLIMITATO - 3);
                    return String.Format("{0}...", indirizzoCompleto);
                }
                else
                {
                    return indirizzoCompleto;
                }
            }
        }
    }
}
