﻿using System;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class ColoniePersonaleRichiesta
    {
        public String NomeCompleto
        {
            get
            {
                return String.Format("{0} {1}", this.Cognome, this.Nome);
            }
        }

        public DateTime? DataColloquioInizio
        {
            get
            {
                foreach (ColoniePersonaleColloquio coll in this.ColoniePersonaleColloquio)
                {
                    return coll.Data;
                }

                return null;
            }
        }

        public DateTime? DataColloquioFine
        {
            get
            {
                foreach (ColoniePersonaleColloquio coll in this.ColoniePersonaleColloquio)
                {
                    return coll.DataFine;
                }

                return null;
            }
        }

        public String ElencoMansioni
        {
            get
            {
                StringBuilder mansioni = new StringBuilder();

                foreach (ColoniePersonaleColloquio coll in this.ColoniePersonaleColloquio)
                {
                    foreach (ColoniePersonaleMansione mans in coll.ColoniePersonaleMansioni)
                    {
                        mansioni.AppendLine(mans.Descrizione);
                    }
                }

                return mansioni.ToString();
            }
        }
    }
}
