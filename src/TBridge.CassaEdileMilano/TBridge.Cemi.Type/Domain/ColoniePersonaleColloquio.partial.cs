﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class ColoniePersonaleColloquio
    {
        public DateTime DataFine
        {
            get
            {
                return this.Data.AddMinutes(30);
            }
        }
    }
}
