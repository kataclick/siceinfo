﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class Ispettore
    {
        public String NomeCompleto 
        {
            get { return String.Format("{0} {1}", this.Cognome, this.Nome); }
        }
    }
}
