﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Domain
{
    public partial class ImpresaQuestionarioImpiegati
    {
        public List<Durata> ScadenzeParziali
        {
            get
            {
                List<Durata> scadenze = new List<Durata>();

                foreach (ImpresaQuestionarioImpiegatiDeterminatiScadenza scad in this.Scadenze)
                {
                    if ((Int32) scad.TipoOrario == (Int32) ImpresaQuestionarioTipoScadenza.Parziali)
                    {
                        Durata scadenza = new Durata();
                        scadenze.Add(scadenza);

                        scadenza.Inizio = scad.DataInizio;
                        scadenza.Fine = scad.DataScadenza;
                    }
                }

                return scadenze;
            }
        }

        public List<Durata> ScadenzePiene
        {
            get
            {
                List<Durata> scadenze = new List<Durata>();

                foreach (ImpresaQuestionarioImpiegatiDeterminatiScadenza scad in this.Scadenze)
                {
                    if ((Int32) scad.TipoOrario == (Int32) ImpresaQuestionarioTipoScadenza.Piene)
                    {
                        Durata scadenza = new Durata();
                        scadenze.Add(scadenza);

                        scadenza.Inizio = scad.DataInizio;
                        scadenza.Fine = scad.DataScadenza;
                    }
                }

                return scadenze;
            }
        }
    }
}
