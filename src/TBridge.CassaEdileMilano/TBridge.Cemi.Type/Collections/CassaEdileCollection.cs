using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Type.Collections
{
    [Serializable]
    public class CassaEdileCollection : List<CassaEdile>
    {
        public bool SonoTutteNelCircuitoCNCE()
        {
            foreach (CassaEdile cassa in this)
            {
                if (!cassa.Cnce)
                    return false;
            }

            return true;
        }

        public Boolean PresenteNellaLista(String idCassaEdile)
        {
            foreach (CassaEdile cassa in this)
            {
                if (cassa.IdCassaEdile == idCassaEdile)
                    return true;
            }

            return false;
        }
    }
}