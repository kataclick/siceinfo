﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Filters.AnagraficaCondivisa
{
    public class ImpresaFilter
    {
        public Int32? CodiceCassaEdile { get; set; }

        public String RagioneSociale { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIva { get; set; }

    }
}
