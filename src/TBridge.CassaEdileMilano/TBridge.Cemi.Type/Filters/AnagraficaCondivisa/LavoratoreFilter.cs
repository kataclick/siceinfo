﻿using System;

namespace TBridge.Cemi.Type.Filters.AnagraficaCondivisa
{
    public class LavoratoreFilter
    {
        public Int32? CodiceCassaEdile { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public String CodiceFiscale { get; set; }
    }
}
