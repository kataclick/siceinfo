﻿using System;

namespace TBridge.Cemi.Type.Filters
{
    [Serializable]
    public class ConsulenteImpreseFilter
    {
        public Int32? IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String CodiceFiscaleImpresa { get; set; }
        public Boolean? Questionario { get; set; }
    }
}
