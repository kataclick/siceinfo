﻿using System;

namespace TBridge.Cemi.Type.Filters
{
    public class CommittenteFilter
    {
        public String Cognome
        {
            get;
            set;
        }

        public String Nome
        {
            get;
            set;
        }

        public String RagioneSociale
        {
            get;
            set;
        }

        public String CodiceFiscale
        {
            get;
            set;
        }
    }
}
