using System;

namespace TBridge.Cemi.Type.Filters
{
    public class BollettinoFrecciaStampatoFilter : BollettinoFrecciaStampabileFilter
    {
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }

    }
}