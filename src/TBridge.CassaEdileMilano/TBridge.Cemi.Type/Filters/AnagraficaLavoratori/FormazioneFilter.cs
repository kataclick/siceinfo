﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Filters.AnagraficaLavoratori
{
    public class FormazioneFilter
    {
        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }
    }
}
