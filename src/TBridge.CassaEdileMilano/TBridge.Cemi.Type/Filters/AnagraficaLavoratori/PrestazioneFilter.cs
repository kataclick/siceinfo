﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Filters.AnagraficaLavoratori
{
    public class PrestazioneFilter
    {
        public DateTime? Dal { get; set; }
        public DateTime? Al { get; set; }
        public String IdTipoPrestazione { get; set; }
    }
}
