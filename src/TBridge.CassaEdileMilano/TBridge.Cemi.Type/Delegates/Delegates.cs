using System;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Type.Delegates
{
    public delegate void ImpresaSelectedEventHandler(Int32 idImpresa, String codiceRagioneSociale);

    public delegate void RichiestaVariazioneStatoImpresaSelectedEventHandler(Int32 idRichiestaVariazioneStatoImpresa);

    public delegate void RicercaRichiesteVariazioneStatoImpresaSelectedEventHandler();

    public delegate void CommittenteSelectedEventHandler(Committente committente);
}