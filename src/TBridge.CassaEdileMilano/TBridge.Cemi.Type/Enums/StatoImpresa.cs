﻿namespace TBridge.Cemi.Type.Enums
{
    public enum StatoImpresa
    {
        ATTIVA = 1,
        CESSATA,
        SOSPESA,
        SOSP_UFF
    }
}