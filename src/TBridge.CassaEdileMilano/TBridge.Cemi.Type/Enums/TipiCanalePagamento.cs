﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Enums
{
    public enum TipiCanalePagamento : int
    {
        BollettinoFreccia = 1,
        MavPopolareSondrio = 2
    }
}
