﻿namespace TBridge.Cemi.Type.Enums
{
    public enum TipoStatoGestionePratica
    {
        DaValutare = 0,
        Approvata = 1,
        Rifiutata = 2,
        InAttesaDiDocumentazione = 3,
    }
}
