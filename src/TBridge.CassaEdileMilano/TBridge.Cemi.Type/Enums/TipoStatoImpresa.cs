﻿namespace TBridge.Cemi.Type.Enums
{
    public enum TipoStatoImpresa
    {
        Sospensione = 1,
        Riattivazione,
        Cessazione
    }
}
