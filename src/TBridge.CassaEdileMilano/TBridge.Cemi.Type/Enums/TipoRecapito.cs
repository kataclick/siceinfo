﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Enums
{
    public enum TipoRecapito
    {
        Legale = 1,
        Amministrativo = 2,
        Corrispondenza = 3
    }
}
