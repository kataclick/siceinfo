using System;

namespace TBridge.Cemi.Type.Entities
{
    public class Impresa
    {
        public Int32 IdImpresa { get; set; }

        public Int32 IdUtente { get; set; }

        public String RagioneSociale { get; set; }

        public String CodiceFiscale { get; set; }


        /// <summary>
        /// Indirizzo della sede legale dell'impresa
        /// </summary>
        public string IndirizzoSedeLegale { get; set; }

        /// <summary>
        /// CAP della sede legale dell'impresa
        /// </summary>
        public string CapSedeLegale { get; set; }

        /// <summary>
        /// Localit� della sede legale dell'impresa
        /// </summary>
        public string LocalitaSedeLegale { get; set; }

        /// <summary>
        /// Provincia della sede legale dell'impresa
        /// </summary>
        public string ProvinciaSedeLegale { get; set; }

        /// <summary>
        /// Email della sede legale dell'impresa
        /// </summary>
        public string EmailSedeLegale { get; set; }



        public string NomeComposto
        {
            get { return string.Format("{0} - {1}", IdImpresa, RagioneSociale); }
        }

        public override string ToString()
        {
            return NomeComposto;
        }
    }
}