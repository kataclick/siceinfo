﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class Indirizzo
    {
        /// <summary>
        /// Identificativo: codice numerico
        /// </summary>
        /// <example>
        /// 3243
        /// </example>
        public Int32 Identificativo { get; set; }

        /// <summary>
        /// Tipologia: Via, Piazza, etc.
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: Piazza
        /// </example>
        public String Qualificatore { get; set; }

        /// <summary>
        /// Nome della via
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: della Vittoria
        /// </example>
        public String NomeVia { get; set; }

        /// <summary>
        /// Civico
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: 11A
        /// </example>
        public String Civico { get; set; }

        /// <summary>
        /// Comune
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: Genova
        /// </example>
        public String Comune { get; set; }

        /// <summary>
        /// Codice Catastale del Comune
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: D969
        /// </example>
        public String ComuneCodiceCatastale { get; set; }

        /// <summary>
        /// CAP
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: 16121
        /// </example>
        public String Cap { get; set; }

        /// <summary>
        /// Provincia
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: GE
        /// </example>
        public String Provincia { get; set; }

        /// <summary>
        /// Stato
        /// </summary>
        /// <example>
        /// Per la sede di T Bridge sarebbe: Italia/Italy
        /// </example>
        public String Stato { get; set; }

        public String InformazioniAggiuntive { get; set; }

        /// <summary>
        /// Qualificatore + NomeVia: ex. Piazza della Vittoria
        /// </summary>
        /// <example>Per la sede di T Bridge sarebbe: Piazza della Vittoria</example>
        public String Via
        {
            get { return !String.IsNullOrEmpty(Qualificatore) ? String.Format("{0} {1}", Qualificatore, NomeVia) : NomeVia; }
        }

        /// <summary>
        /// Qualificatore + NomeVia + Civico: ex. Piazza della Vittoria 11A
        /// </summary>
        /// <example>Per la sede di T Bridge sarebbe: Piazza della Vittoria 11A</example>
        public String IndirizzoBase
        {
            get { return !String.IsNullOrEmpty(Civico) ? String.Format("{0} {1}", Via, Civico) : Via; }
        }

        /// <summary>
        /// CAP + Comune + Provincia: ex. 16121 Genova GE
        /// </summary>
        /// <example>Per la sede di T Bridge sarebbe: 16121 Genova GE</example>
        public String Localita
        {
            get { return String.Format("{0} {1} {2}", Cap, Comune, Provincia); }
        }

        public String IndirizzoCompleto
        {
            get
            {
                return !String.IsNullOrEmpty(InformazioniAggiuntive)
                           ? String.Format("{0}{1}{2} ({3})", IndirizzoBase, Environment.NewLine, Localita,
                                           InformazioniAggiuntive)
                           : String.Format("{0}{1}{2}", IndirizzoBase, Environment.NewLine, Localita);
            }
        }

        public override string ToString()
        {
            return IndirizzoCompleto;
        }
    }
}