using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoMansione
    {
        public String IdMansione { get; set; }

        public String Descrizione { get; set; }
    }
}