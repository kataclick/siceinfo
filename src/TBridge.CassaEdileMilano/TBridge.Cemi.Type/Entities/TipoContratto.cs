using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoContratto
    {
        public String IdContratto { get; set; }

        public String Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}