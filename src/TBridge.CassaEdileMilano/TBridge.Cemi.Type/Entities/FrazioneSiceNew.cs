using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class FrazioneSiceNew
    {
        public string CodiceCatastale { get; set; }

        public string Frazione { get; set; }

        public string Cap { get; set; }

        public string CodicePerCombo
        {
            get { return String.Format("{0}|{1}", Frazione, Cap); }
        }
    }
}