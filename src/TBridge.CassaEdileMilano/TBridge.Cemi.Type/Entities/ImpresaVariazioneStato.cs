﻿using System;
using System.Collections.Generic;
using Cemi.SiceInfo.Type.Domain;

namespace TBridge.Cemi.Type.Entities
{
    public class ImpresaVariazioneStato
    {
        //public ImpresaVariazioneStato()
        //{
        //    InfoAggiuntiveAnalisi = new InfoAggiuntive();
        //}

        public int IdRichiesta { get; set; }
        public int IdImpresa { get; set; }
        public string CodiceFiscale { get; set; }
        public string RagioneSociale { get; set; }
        public string StatoImpresaAttuale { get; set; }
        public DateTime DataRichiesta { get; set; }
        public int IdUtente { get; set; }
        public string Login { get; set; }
        public int IdTipoStatoImpresa { get; set; }
        public string StatoImpresa { get; set; }
        public DateTime DataInizioCambioStato { get; set; }
        public string Note { get; set; }
        public int IdTipoStatoPratica { get; set; }
        public string StatoPratica { get; set; }
        public InfoAggiuntive InfoAggiuntiveAnalisi { get; set; }
        public DateTime? DataGestioneOperatore { get; set; }
        public string AllegatoLibroUnicoNome { get; set; }
        public string AllegatoLibroUnicoIdArchidoc { get; set; }
        public string AllegatoDenunceNome { get; set; }
        public string AllegatoDenunceIdArchidoc { get; set; }
        public int? IdTipoCausaleRespinta { get; set; }

        public List<Documento> AltriAllegati { get; set; } = new List<Documento>();

        public string NomeComposto => $"{IdImpresa} - {RagioneSociale}";

        public class InfoAggiuntive
        {
            public DateTime? DataUltimaDenuncia { get; set; }
            public decimal? OreUltimaDenuncia { get; set; }
            public decimal? OreLavorateUltimaDenuncia { get; set; }
            public DateTime? DataIscrizione { get; set; }
            public DateTime? DataSospensione { get; set; }
            public DateTime? DataRiattivazione { get; set; }
            public DateTime? DataCessazione { get; set; }
            public decimal? Debito { get; set; }
        }
    }
}