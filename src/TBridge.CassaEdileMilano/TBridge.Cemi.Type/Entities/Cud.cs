﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class Cud
    {
        public Int32 Anno { get; set; }

        public Int32 IdLavoratore { get; set; }

        public String IdArchidoc { get; set; }
    }
}
