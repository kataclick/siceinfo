using System;


namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class ImpreseConsulente
    {
        
        public Int32 IdImpresa { get; set; }

        public String RagioneSociale { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIva { get; set; }

        public Boolean QuestionarioPresente { get; set; }
    }
}