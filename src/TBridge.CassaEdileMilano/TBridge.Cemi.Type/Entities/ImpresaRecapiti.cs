﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class ImpresaRecapiti
    {
        public Int32 IdImpresa
        {
            get;
            set;
        }
        public String CodiceFiscale
        {
            get;
            set;
        }
        public String RagioneSociale
        {
            get;
            set;
        }
        public String StatoImpresa
        {
            get;
            set;
        }
        public Indirizzo IndirizzoSedeLegale
        {
            get;
            set;
        }
        public String PressoSedeLegale
        {
            get;
            set;
        }
        public String TelefonoSedeLegale
        {
            get;
            set;
        }
        public String FaxSedeLegale
        {
            get;
            set;
        }
        public String EmailSedeLegale
        {
            get;
            set;
        }
        public String PECSedeLegale
        {
            get;
            set;
        }
        public Indirizzo IndirizzoSedeAmministrativa
        {
            get;
            set;
        }
        public String PressoSedeAmministrativa
        {
            get;
            set;
        }
        public String TelefonoSedeAmministrativa
        {
            get;
            set;
        }
        public String FaxSedeAmministrativa
        {
            get;
            set;
        }
        public String EmailSedeAmministrativa
        {
            get;
            set;
        }
        public Indirizzo IndirizzoCorrispondenza
        {
            get;
            set;
        }
        public String PressoCorrispondenza
        {
            get;
            set;
        }
        public String TelefonoCorrispondenza
        {
            get;
            set;
        }
        public String FaxCorrispondenza
        {
            get;
            set;
        }
        public String EmailCorrispondenza
        {
            get;
            set;
        }
    }
}
