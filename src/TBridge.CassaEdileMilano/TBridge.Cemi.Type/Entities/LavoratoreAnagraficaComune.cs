using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class LavoratoreAnagraficaComune
    {
        public FonteAnagraficheComuni Fonte { get; set; }

        public Int32? IdLavoratore { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CognomeNome
        {
            get { return String.Format("{0} {1}", Cognome, Nome); }
        }

        public Char Sesso { get; set; }

        public DateTime DataNascita { get; set; }

        public String CodiceFiscale { get; set; }

        public String PaeseNascita { get; set; }

        public String ProvinciaNascita { get; set; }

        public String ComuneNascita { get; set; }
    }
}