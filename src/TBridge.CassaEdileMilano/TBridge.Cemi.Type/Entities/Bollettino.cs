﻿using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    public class Bollettino
    {
        public Int32 IdImpresa { get; set; }
        public Int32 Anno { get; set; }
        public Int32 Mese { get; set; }
        public Int32 Sequenza { get; set; }
        public Decimal Importo { get; set; }
        public virtual TipiCanalePagamento TipoCanalePagamento { set; get; }

        public Decimal ImportoDovuto0 { get; set; }
        public Decimal ImportoDovuto1 { get; set; }
        public Decimal ImportoDovuto2 { get; set; }
        public Decimal ImportoDovuto3 { get; set; }
        public Decimal ImportoDovuto4 { get; set; }
        public Decimal ImportoDovuto5 { get; set; }
    }
}
