namespace TBridge.Cemi.Type.Entities
{
    public class TipiDeleghe<TKey, TDescription>
    {
        protected TDescription descrizione;
        protected TKey id;

        public TKey Id
        {
            get { return id; }
            set { id = value; }
        }

        public TDescription Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }
    }
}