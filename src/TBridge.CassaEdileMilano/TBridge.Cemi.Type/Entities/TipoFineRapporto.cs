using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoFineRapporto
    {
        public String IdTipoFineRapporto { get; set; }

        public String Descrizione { get; set; }
    }
}