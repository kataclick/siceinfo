﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Entities
{
    public class DenunciaLavoratore
    {
        public Int32 IdLavoratore { get; set; }

        public Int32 IdImpresa { get; set; }

        public String RagioneSocialeImpresa { get; set; }

        public DateTime Data { get; set; }
    }
}
