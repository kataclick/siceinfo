using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoQualifica
    {
        public String IdQualifica { get; set; }

        public String Descrizione { get; set; }
    }
}