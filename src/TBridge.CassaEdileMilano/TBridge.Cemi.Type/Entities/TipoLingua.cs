using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoLingua
    {
        public String IdLingua { get; set; }

        public String Descrizione { get; set; }
    }
}