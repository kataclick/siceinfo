using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class CassaEdile
    {
        public static char IDCOMPOSTOSEPARATOR = '|';
        private bool cnce;
        private string descrizione;

        private string idCassaEdile;

        public CassaEdile()
        {
        }

        public CassaEdile(string idCassaEdile, string descrizione)
        {
            this.idCassaEdile = idCassaEdile;
            this.descrizione = descrizione;
        }

        public string IdCassaEdile
        {
            get { return idCassaEdile; }
            set { idCassaEdile = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        public bool Cnce
        {
            get { return cnce; }
            set { cnce = value; }
        }

        public string IdComposito
        {
            get { return String.Format("{0}{1}{2}", idCassaEdile, IDCOMPOSTOSEPARATOR, cnce); }
        }

        #region Attributo necessario per prestazioni

        private Boolean inseritaManualmente;

        public Boolean InseritaManualmente
        {
            get { return inseritaManualmente; }
            set { inseritaManualmente = value; }
        }

        #endregion
    }
}