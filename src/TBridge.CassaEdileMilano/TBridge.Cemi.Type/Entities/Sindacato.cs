namespace TBridge.Cemi.Type.Entities
{
    public class Sindacato : TipiDeleghe<string, string>
    {
        public Sindacato()
        {
        }

        public Sindacato(string _id, string _descrizione)
        {
            id = _id;
            descrizione = _descrizione;
        }

        public override string ToString()
        {
            return descrizione;
        }
    }
}