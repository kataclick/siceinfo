﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class BollettinoFrecciaStampabile: BollettinoFreccia
    {
        public String RagioneSociale { get; set; }
    }
}
