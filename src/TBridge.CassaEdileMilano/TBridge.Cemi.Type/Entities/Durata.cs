﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class Durata
    {
        public DateTime? Inizio { get; set; }

        public DateTime Fine { get; set; }
    }
}
