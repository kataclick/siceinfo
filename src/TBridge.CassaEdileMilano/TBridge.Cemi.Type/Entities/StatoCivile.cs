using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class StatoCivile
    {
        public String IdStatoCivile { get; set; }

        public String Descrizione { get; set; }
    }
}