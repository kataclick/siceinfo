﻿using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    public class BollettinoStampato
    {

        public Bollettino Bollettino { private set; get; }

        public string RagioneSociale { get; set; }

        public DateTime? DataVersamento { get; set; }

        public DateTime DataRichiesta { get; set; }

        public String DescrizioneTipoCanalePagamento { set; get; }

        public TipiCanalePagamento TipoCanalePagamento
        {
            get
            {
                return Bollettino.TipoCanalePagamento;
            }
        }

        public int IdImpresa
        {
            get
            {
                return Bollettino.IdImpresa;
            }
        }

        public int Anno
        {
            get
            {
                return Bollettino.Anno;
            }
        }

        public int Mese
        {
            get
            {
                return Bollettino.Mese;
            }
        }

        public Int32 Sequenza
        {
            get
            {
                return Bollettino.Sequenza;
            }
        }

        public decimal Importo
        {
            get
            {
                return Bollettino.Importo;
            }
        }

        public BollettinoStampato(Bollettino bollettino)
        {
            if (bollettino == null)
            {
                throw new ArgumentNullException("bollettino");
            }

            Bollettino = bollettino;
        }
    }
}
