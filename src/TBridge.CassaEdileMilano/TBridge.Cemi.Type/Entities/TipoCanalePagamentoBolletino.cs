﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Entities
{
    public class TipoCanalePagamentoBolletino
    {
        public int Id { get; set; }
        public String Descrizione { get; set; }
    }
}
