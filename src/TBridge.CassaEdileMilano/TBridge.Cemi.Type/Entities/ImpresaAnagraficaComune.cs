using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class ImpresaAnagraficaComune
    {
        public FonteAnagraficheComuni Fonte { get; set; }

        public Int32? IdImpresa { get; set; }

        public String RagioneSociale { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIva { get; set; }
    }
}