using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoCategoria
    {
        public String IdCategoria { get; set; }

        public String Descrizione { get; set; }
    }
}