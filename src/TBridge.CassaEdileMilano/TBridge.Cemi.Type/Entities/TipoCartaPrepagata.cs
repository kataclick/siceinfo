using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoCartaPrepagata
    {
        public String IdTipoCartaPrepagata { get; set; }

        public String Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}