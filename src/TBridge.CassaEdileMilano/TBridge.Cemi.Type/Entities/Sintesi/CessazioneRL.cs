using System;

namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class CessazioneRL
    {
        public String Codice { get; set; }

        public String Descrizione { get; set; }
    }
}