﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4927
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.3038.
// 

namespace TBridge.Cemi.Type.Entities.Sintesi.Original
{
    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    [XmlRoot("UniLav", Namespace = "http://servizi.lavoro.gov.it/unilav", IsNullable = false)]
    [Obsolete]
    public class RapportoLavoro
    {
        private SiNo assunzioneForzaMaggioreField;
        private string codiceComunicazioneField;

        private string codiceComunicazionePrecField;
        private DatiLavoratore coobbligatoField;

        private DateTime dataInvioField;
        private RapportoLavoroDatoreLavoro datoreLavoroField;

        private string delegatoField;

        private string descrizioneForzaMaggioreField;
        private string emailDelegatoField;
        private object itemField;
        private DatiLavoratore lavoratoreField;

        private string protocolloField;
        private string tipoComunicazioneField;
        private string tipoDelegatoField;

        /// <remarks/>
        public RapportoLavoroDatoreLavoro DatoreLavoro
        {
            get { return datoreLavoroField; }
            set { datoreLavoroField = value; }
        }

        /// <remarks/>
        public DatiLavoratore Lavoratore
        {
            get { return lavoratoreField; }
            set { lavoratoreField = value; }
        }

        /// <remarks/>
        public DatiLavoratore Coobbligato
        {
            get { return coobbligatoField; }
            set { coobbligatoField = value; }
        }

        /// <remarks/>
        [XmlElement("Cessazione", typeof (Cessazione))]
        [XmlElement("InizioRapporto", typeof (RapportoLavoroInizioRapporto))]
        [XmlElement("Proroga", typeof (Proroga))]
        [XmlElement("Trasformazione", typeof (RapportoLavoroTrasformazione))]
        public object Item
        {
            get { return itemField; }
            set { itemField = value; }
        }

        /// <remarks/>
        public string TipoComunicazione
        {
            get { return tipoComunicazioneField; }
            set { tipoComunicazioneField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceComunicazione
        {
            get { return codiceComunicazioneField; }
            set { codiceComunicazioneField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceComunicazionePrec
        {
            get { return codiceComunicazionePrecField; }
            set { codiceComunicazionePrecField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public DateTime dataInvio
        {
            get { return dataInvioField; }
            set { dataInvioField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string delegato
        {
            get { return delegatoField; }
            set { delegatoField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string tipoDelegato
        {
            get { return tipoDelegatoField; }
            set { tipoDelegatoField = value; }
        }

        /// <remarks/>
        [XmlAttribute("e-mailDelegato")]
        public string emailDelegato
        {
            get { return emailDelegatoField; }
            set { emailDelegatoField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public SiNo assunzioneForzaMaggiore
        {
            get { return assunzioneForzaMaggioreField; }
            set { assunzioneForzaMaggioreField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string descrizioneForzaMaggiore
        {
            get { return descrizioneForzaMaggioreField; }
            set { descrizioneForzaMaggioreField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string protocollo
        {
            get { return protocolloField; }
            set { protocolloField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class RapportoLavoroDatoreLavoro : DatoreSedi
    {
        private string settoreField;

        /// <remarks/>
        public string Settore
        {
            get { return settoreField; }
            set { settoreField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class DatoreSedi
    {
        private string codiceFiscaleField;

        private string denominazioneField;
        private IndirizzoConRecapitiItaliano sedeLavoroField;
        private IndirizzoConRecapitiItaliano sedeLegaleField;

        /// <remarks/>
        public IndirizzoConRecapitiItaliano SedeLegale
        {
            get { return sedeLegaleField; }
            set { sedeLegaleField = value; }
        }

        /// <remarks/>
        public IndirizzoConRecapitiItaliano SedeLavoro
        {
            get { return sedeLavoroField; }
            set { sedeLavoroField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceFiscale
        {
            get { return codiceFiscaleField; }
            set { codiceFiscaleField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string denominazione
        {
            get { return denominazioneField; }
            set { denominazioneField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class IndirizzoConRecapitiItaliano : IndirizzoItaliano
    {
        private string emailField;

        private string faxField;
        private string telefonoField;

        /// <remarks/>
        [XmlElement("e-mail")]
        public string email
        {
            get { return emailField; }
            set { emailField = value; }
        }

        /// <remarks/>
        public string telefono
        {
            get { return telefonoField; }
            set { telefonoField = value; }
        }

        /// <remarks/>
        public string fax
        {
            get { return faxField; }
            set { faxField = value; }
        }
    }

    /// <remarks/>
    [XmlInclude(typeof (IndirizzoConRecapitiItaliano))]
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class IndirizzoItaliano
    {
        private string capField;
        private string comuneField;

        private string indirizzoField;

        /// <remarks/>
        public string Comune
        {
            get { return comuneField; }
            set { comuneField = value; }
        }

        /// <remarks/>
        public string cap
        {
            get { return capField; }
            set { capField = value; }
        }

        /// <remarks/>
        public string Indirizzo
        {
            get { return indirizzoField; }
            set { indirizzoField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class Cessazione
    {
        private string codiceCausaField;
        private Contratto contrattoField;

        private DateTime dataField;

        /// <remarks/>
        public Contratto Contratto
        {
            get { return contrattoField; }
            set { contrattoField = value; }
        }

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime data
        {
            get { return dataField; }
            set { dataField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceCausa
        {
            get { return codiceCausaField; }
            set { codiceCausaField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class Contratto
    {
        private string agevolazioniField;

        private string ccnlField;
        private string codiceEntePrevidenzialeField;

        private DateTime dataFineField;

        private bool dataFineFieldSpecified;
        private DateTime dataInizioField;
        private string entePrevidenzialeField;

        private string ggLavorativePrevisteField;
        private SiNo lavoroInAgricolturaField;

        private bool lavoroInAgricolturaFieldSpecified;
        private Legge68 legge68Field;
        private string livelloInquadramentoField;
        private string patINAILField;
        private string qualificaProfessionaleField;

        private string retribuzioneCompensoField;
        private SiNo socioLavoratoreField;

        private string tipoLavorazioneField;

        private string tipologiaContrattualeField;
        private TipoOrario tipoOrarioField;

        /// <remarks/>
        public string agevolazioni
        {
            get { return agevolazioniField; }
            set { agevolazioniField = value; }
        }

        /// <remarks/>
        public string ccnl
        {
            get { return ccnlField; }
            set { ccnlField = value; }
        }

        /// <remarks/>
        public string livelloInquadramento
        {
            get { return livelloInquadramentoField; }
            set { livelloInquadramentoField = value; }
        }

        /// <remarks/>
        public Legge68 legge68
        {
            get { return legge68Field; }
            set { legge68Field = value; }
        }

        /// <remarks/>
        public TipoOrario tipoOrario
        {
            get { return tipoOrarioField; }
            set { tipoOrarioField = value; }
        }

        /// <remarks/>
        public string qualificaProfessionale
        {
            get { return qualificaProfessionaleField; }
            set { qualificaProfessionaleField = value; }
        }

        /// <remarks/>
        public string RetribuzioneCompenso
        {
            get { return retribuzioneCompensoField; }
            set { retribuzioneCompensoField = value; }
        }

        /// <remarks/>
        public string PatINAIL
        {
            get { return patINAILField; }
            set { patINAILField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public SiNo lavoroInAgricoltura
        {
            get { return lavoroInAgricolturaField; }
            set { lavoroInAgricolturaField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool lavoroInAgricolturaSpecified
        {
            get { return lavoroInAgricolturaFieldSpecified; }
            set { lavoroInAgricolturaFieldSpecified = value; }
        }

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime dataInizio
        {
            get { return dataInizioField; }
            set { dataInizioField = value; }
        }

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime dataFine
        {
            get { return dataFineField; }
            set { dataFineField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool dataFineSpecified
        {
            get { return dataFineFieldSpecified; }
            set { dataFineFieldSpecified = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string ggLavorativePreviste
        {
            get { return ggLavorativePrevisteField; }
            set { ggLavorativePrevisteField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string tipoLavorazione
        {
            get { return tipoLavorazioneField; }
            set { tipoLavorazioneField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string entePrevidenziale
        {
            get { return entePrevidenzialeField; }
            set { entePrevidenzialeField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceEntePrevidenziale
        {
            get { return codiceEntePrevidenzialeField; }
            set { codiceEntePrevidenzialeField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string TipologiaContrattuale
        {
            get { return tipologiaContrattualeField; }
            set { tipologiaContrattualeField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public SiNo socioLavoratore
        {
            get { return socioLavoratoreField; }
            set { socioLavoratoreField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class Legge68
    {
        private DateTime dataNullaOstaField;

        private bool dataNullaOstaFieldSpecified;

        private string numeroAttoField;

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime dataNullaOsta
        {
            get { return dataNullaOstaField; }
            set { dataNullaOstaField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool dataNullaOstaSpecified
        {
            get { return dataNullaOstaFieldSpecified; }
            set { dataNullaOstaFieldSpecified = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string numeroAtto
        {
            get { return numeroAttoField; }
            set { numeroAttoField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class TipoOrario
    {
        private string codiceField;

        private string oreSettimanaliMedieField;

        /// <remarks/>
        [XmlAttribute]
        public string codice
        {
            get { return codiceField; }
            set { codiceField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string oreSettimanaliMedie
        {
            get { return oreSettimanaliMedieField; }
            set { oreSettimanaliMedieField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [XmlType(TypeName = "Si-No", Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public enum SiNo
    {
        /// <remarks/>
        SI,

        /// <remarks/>
        NO,
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class DatoreSede
    {
        private string codiceFiscaleField;

        private string denominazioneField;
        private IndirizzoConRecapitiItaliano sedeField;

        /// <remarks/>
        public IndirizzoConRecapitiItaliano Sede
        {
            get { return sedeField; }
            set { sedeField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceFiscale
        {
            get { return codiceFiscaleField; }
            set { codiceFiscaleField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string denominazione
        {
            get { return denominazioneField; }
            set { denominazioneField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class IndirizzoSenzaCAP
    {
        private string comuneField;

        private string indirizzoField;

        /// <remarks/>
        public string Comune
        {
            get { return comuneField; }
            set { comuneField = value; }
        }

        /// <remarks/>
        public string Indirizzo
        {
            get { return indirizzoField; }
            set { indirizzoField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class Trasformazione
    {
        private string codiceTrasformazioneField;
        private Contratto contrattoField;
        private DateTime dataTrasformazioneField;
        private TrasformazioneDatoreDistaccatario datoreDistaccatarioField;

        private SiNo distaccoAziendaEsteraField;

        private bool distaccoAziendaEsteraFieldSpecified;
        private SiNo distaccoParzialeField;

        private bool distaccoParzialeFieldSpecified;
        private IndirizzoSenzaCAP sedeLavoroPrecField;

        /// <remarks/>
        public Contratto Contratto
        {
            get { return contrattoField; }
            set { contrattoField = value; }
        }

        /// <remarks/>
        public IndirizzoSenzaCAP SedeLavoroPrec
        {
            get { return sedeLavoroPrecField; }
            set { sedeLavoroPrecField = value; }
        }

        /// <remarks/>
        public SiNo DistaccoParziale
        {
            get { return distaccoParzialeField; }
            set { distaccoParzialeField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool DistaccoParzialeSpecified
        {
            get { return distaccoParzialeFieldSpecified; }
            set { distaccoParzialeFieldSpecified = value; }
        }

        /// <remarks/>
        public SiNo DistaccoAziendaEstera
        {
            get { return distaccoAziendaEsteraField; }
            set { distaccoAziendaEsteraField = value; }
        }

        /// <remarks/>
        [XmlIgnore]
        public bool DistaccoAziendaEsteraSpecified
        {
            get { return distaccoAziendaEsteraFieldSpecified; }
            set { distaccoAziendaEsteraFieldSpecified = value; }
        }

        /// <remarks/>
        public TrasformazioneDatoreDistaccatario DatoreDistaccatario
        {
            get { return datoreDistaccatarioField; }
            set { datoreDistaccatarioField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string codiceTrasformazione
        {
            get { return codiceTrasformazioneField; }
            set { codiceTrasformazioneField = value; }
        }

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime dataTrasformazione
        {
            get { return dataTrasformazioneField; }
            set { dataTrasformazioneField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class TrasformazioneDatoreDistaccatario : DatoreSede
    {
        private string patINAILField;
        private string settoreField;

        /// <remarks/>
        public string Settore
        {
            get { return settoreField; }
            set { settoreField = value; }
        }

        /// <remarks/>
        public string PatINAIL
        {
            get { return patINAILField; }
            set { patINAILField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class Proroga
    {
        private ProrogaContratto contrattoField;

        private DateTime dataFineProrogaField;

        /// <remarks/>
        public ProrogaContratto Contratto
        {
            get { return contrattoField; }
            set { contrattoField = value; }
        }

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime dataFineProroga
        {
            get { return dataFineProrogaField; }
            set { dataFineProrogaField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class ProrogaContratto : Contratto
    {
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class DatiExtraCE
    {
        private DateTime dataScadenzaPSField;

        private string motivoPermessoField;

        private string numeroDocumentoField;
        private string tipoDocumentoField;

        /// <remarks/>
        [XmlAttribute(DataType = "date")]
        public DateTime dataScadenzaPS
        {
            get { return dataScadenzaPSField; }
            set { dataScadenzaPSField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string motivoPermesso
        {
            get { return motivoPermessoField; }
            set { motivoPermessoField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string tipoDocumento
        {
            get { return tipoDocumentoField; }
            set { tipoDocumentoField = value; }
        }

        /// <remarks/>
        [XmlAttribute]
        public string numeroDocumento
        {
            get { return numeroDocumentoField; }
            set { numeroDocumentoField = value; }
        }
    }

    /// <remarks/>
    [XmlInclude(typeof (AnagraficaCompleta))]
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class Anagrafica
    {
        private string cittadinanzaField;
        private string codicefiscaleField;
        private string cognomeField;

        private string nomeField;

        /// <remarks/>
        public string cognome
        {
            get { return cognomeField; }
            set { cognomeField = value; }
        }

        /// <remarks/>
        public string nome
        {
            get { return nomeField; }
            set { nomeField = value; }
        }

        /// <remarks/>
        [XmlElement("codice-fiscale")]
        public string codicefiscale
        {
            get { return codicefiscaleField; }
            set { codicefiscaleField = value; }
        }

        /// <remarks/>
        public string cittadinanza
        {
            get { return cittadinanzaField; }
            set { cittadinanzaField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class AnagraficaCompleta : Anagrafica
    {
        private AnagraficaCompletaNascita nascitaField;
        private Sesso sessoField;

        /// <remarks/>
        public Sesso sesso
        {
            get { return sessoField; }
            set { sessoField = value; }
        }

        /// <remarks/>
        public AnagraficaCompletaNascita nascita
        {
            get { return nascitaField; }
            set { nascitaField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public enum Sesso
    {
        /// <remarks/>
        M,

        /// <remarks/>
        F,
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class AnagraficaCompletaNascita
    {
        private string comuneField;

        private DateTime dataField;

        /// <remarks/>
        public string comune
        {
            get { return comuneField; }
            set { comuneField = value; }
        }

        /// <remarks/>
        [XmlElement(DataType = "date")]
        public DateTime data
        {
            get { return dataField; }
            set { dataField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class DatiLavoratore
    {
        private AnagraficaCompleta anagraficaCompletaField;

        private DatiExtraCE extraCEField;

        private IndirizzoItaliano indirizzoLavoratoreField;

        private string livelloIstruzioneField;

        /// <remarks/>
        public AnagraficaCompleta AnagraficaCompleta
        {
            get { return anagraficaCompletaField; }
            set { anagraficaCompletaField = value; }
        }

        /// <remarks/>
        public DatiExtraCE extraCE
        {
            get { return extraCEField; }
            set { extraCEField = value; }
        }

        /// <remarks/>
        public IndirizzoItaliano IndirizzoLavoratore
        {
            get { return indirizzoLavoratoreField; }
            set { indirizzoLavoratoreField = value; }
        }

        /// <remarks/>
        public string LivelloIstruzione
        {
            get { return livelloIstruzioneField; }
            set { livelloIstruzioneField = value; }
        }
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class RapportoLavoroInizioRapporto : Contratto
    {
    }

    /// <remarks/>
    [GeneratedCode("xsd", "2.0.50727.3038")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://servizi.lavoro.gov.it/unilav")]
    public class RapportoLavoroTrasformazione : Trasformazione
    {
    }
}