using System;

namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class TipoContratto
    {
        public String Codice { get; set; }

        public String Descrizione { get; set; }
    }
}