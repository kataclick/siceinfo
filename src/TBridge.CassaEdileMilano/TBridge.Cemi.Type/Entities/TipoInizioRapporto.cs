using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoInizioRapporto
    {
        public String IdTipoInizioRapporto { get; set; }

        public String Descrizione { get; set; }
    }
}