using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class ComuneSiceNew
    {
        private string cap;
        private string codiceCatastale;
        private string comune;
        private string provincia;

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string CodiceCatastale
        {
            get { return codiceCatastale; }
            set { codiceCatastale = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string CodicePerCombo
        {
            get { return String.Format("{0}|{1}", codiceCatastale, cap); }
        }
    }
}