﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class BollettiniFrecciaStatistichePagati
    {
        public Int32 NumeroBollettiniUniciStampati { get; set; }
        public Int32 NumeroBollettiniUniciStampatiPagati { get; set; }
        public Decimal ImportoPagato { get; set; }
    }
}