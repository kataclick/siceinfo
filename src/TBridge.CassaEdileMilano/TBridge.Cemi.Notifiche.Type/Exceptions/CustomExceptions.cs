using System;

namespace TBridge.Cemi.Notifiche.Type.Exceptions
{
    public class NotificaPresenteException : Exception
    {
    }

    public class DenunciaPresenteException : Exception
    {
    }
}