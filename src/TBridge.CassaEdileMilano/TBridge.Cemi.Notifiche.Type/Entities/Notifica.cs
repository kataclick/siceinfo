using System;

namespace TBridge.Cemi.Notifiche.Type.Entities
{
    public enum TipoNotifica
    {
        Assunzione,
        CessazioneNotifica,
        CessazioneDenuncia,
        Visualizzazione
    }

    public enum TipoErrore
    {
        DenunciaPresente,
        NotificaPresente,
        NonPresente
    }

    public class Notifica
    {
        #region Propriet�

        private readonly string codiceFiscale;
        private readonly string cognome;
        private readonly DateTime? dataAssunzione;
        private readonly DateTime dataNascita;
        private readonly int idImpresa;
        private readonly int idLavoratore;
        private readonly string luogoNascita;
        private readonly string nome;
        private readonly string ragioneSociale;
        private readonly string sesso;
        private readonly TipoNotifica tipoNotifica;
        private DateTime? dataCessazione;

        private int? idNotifica;

        public TipoNotifica TipoNotifica
        {
            get { return tipoNotifica; }
        }

        public int? IdNotifica
        {
            get { return idNotifica; }
            set { if (idNotifica == null) idNotifica = value; }
        }

        public int IdLavoratore
        {
            get { return idLavoratore; }
        }

        public string Cognome
        {
            get { return cognome; }
        }

        public string Nome
        {
            get { return nome; }
        }

        public DateTime DataNascita
        {
            get { return dataNascita; }
        }

        public string LuogoNascita
        {
            get { return luogoNascita; }
        }

        public string Sesso
        {
            get { return sesso; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
        }

        public int IdImpresa
        {
            get { return idImpresa; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
        }


        public DateTime? DataAssunzione
        {
            get { return dataAssunzione; }
        }

        public DateTime? DataCessazione
        {
            get { return dataCessazione; }
            set
            {
                if (dataCessazione == null) dataCessazione = value;
                else throw new Exception("Data cessazione gi� impostata");
            }
        }

        #endregion

        #region Costruttori

        public Notifica(int idLavoratore, int idImpresa, DateTime dataCessazione)
        {
            this.idLavoratore = idLavoratore;
            this.idImpresa = idImpresa;
            this.dataCessazione = dataCessazione;
            tipoNotifica = TipoNotifica.CessazioneDenuncia;
        }

        public Notifica(string cognome, string nome, DateTime dataNascita, string luogoNascita, string sesso,
                        string codiceFiscale, DateTime dataAssunzione, int idImpresa)
        {
            this.cognome = cognome;
            this.nome = nome;
            this.dataNascita = dataNascita;
            this.luogoNascita = luogoNascita;
            this.sesso = sesso;
            this.codiceFiscale = codiceFiscale;
            this.dataAssunzione = dataAssunzione;
            this.idImpresa = idImpresa;
            dataCessazione = null;
            tipoNotifica = TipoNotifica.Assunzione;
        }

        public Notifica(TipoNotifica tipo, int idNotLav, string cognome, string nome, DateTime dataNascita,
                        string luogoNascita, string sesso, string codiceFiscale, DateTime? dataAssunzione,
                        DateTime? dataCessazione, int idImpresa, string ragioneSociale)
        {
            switch (tipo)
            {
                case TipoNotifica.CessazioneDenuncia:
                    idLavoratore = idNotLav;
                    break;
                case TipoNotifica.CessazioneNotifica:
                    idNotifica = idNotLav;
                    break;
            }

            this.cognome = cognome;
            this.nome = nome;
            this.dataNascita = dataNascita;
            this.luogoNascita = luogoNascita;
            this.sesso = sesso;
            this.codiceFiscale = codiceFiscale;
            this.dataAssunzione = dataAssunzione;
            this.dataCessazione = dataCessazione;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            tipoNotifica = tipo;
        }

        public Notifica(string cognome, string nome, string codiceFiscale, DateTime? dataAssunzione)
        {
            tipoNotifica = TipoNotifica.Visualizzazione;
            this.cognome = cognome;
            this.nome = nome;
            this.codiceFiscale = codiceFiscale;
            this.dataAssunzione = dataAssunzione;
        }

        #endregion
    }
}