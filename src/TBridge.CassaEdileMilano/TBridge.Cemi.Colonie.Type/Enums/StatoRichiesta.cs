﻿
namespace TBridge.Cemi.Colonie.Type.Enums
{
    public enum StatoRichiesta
    {
        DAGESTIRE = 0,
        FISSATOCOLLOQUIO,
        VALUTATO,
        EFFETTUATAPROPOSTA,
        PROPOSTAACCETTATA,
        PROPOSTANONACCETTATA,
        RESPINTA
    }
}
