namespace TBridge.Cemi.Colonie.Type.Enums
{
    public enum StatoDomandaACE
    {
        DaValutare = 0,
        Confermata,
        Rifiutata
    }
}