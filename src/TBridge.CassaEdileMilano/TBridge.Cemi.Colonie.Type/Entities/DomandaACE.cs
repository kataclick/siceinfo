using TBridge.Cemi.Colonie.Type.Enums;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    public class DomandaACE
    {
        private FamiliareACE bambino;

        public DomandaACE()
        {
            bambino = new FamiliareACE();
        }

        public int? IdDomanda { get; set; }

        public int IdTurno { get; set; }

        public int? IdTaglia { get; set; }

        public string StringaLavoratore
        {
            get
            {
                if (bambino != null && bambino.Parente != null) return bambino.Parente.NomeCompleto;
                else return string.Empty;
            }
        }

        public string CodiceFiscaleLavoratore
        {
            get
            {
                if (bambino != null && bambino.Parente != null) return bambino.Parente.CodiceFiscale;
                else return string.Empty;
            }
        }

        public string IndirizzoLavoratore
        {
            get
            {
                if (bambino != null && bambino.Parente != null) return bambino.Parente.IndirizzoCompleto;
                else return string.Empty;
            }
        }

        public FamiliareACE Bambino
        {
            get { return bambino; }
            set { bambino = value; }
        }

        public string StringaPartecipante
        {
            get
            {
                if (bambino != null) return bambino.NomeCompleto;
                else return string.Empty;
            }
        }

        public string CodiceFiscalePartecipante
        {
            get
            {
                if (bambino != null) return bambino.CodiceFiscale;
                else return string.Empty;
            }
        }

        public string StringaCassaEdile
        {
            get
            {
                if (bambino != null && bambino.Parente != null) return bambino.Parente.IdCassaEdile;
                else return string.Empty;
            }
        }

        public Accompagnatore Accompagnatore { get; set; }

        public StatoDomandaACE StatoDomanda { get; set; }

        public string MotivazioneRifiuto { get; set; }

        public int? NumeroCorredo { get; set; }
    }
}