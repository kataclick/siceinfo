namespace TBridge.Cemi.Colonie.Type.Entities
{
    public class SituazioneACE
    {
        private int accompagnatoriAccettati;
        private int accompagnatoriInCarico;
        private int domandeAccettate;
        private int domandeInCarico;

        public int IdTurno { get; set; }

        public string Turno { get; set; }

        public int PostiDisponibili { get; set; }

        public int PostiPrenotati { get; set; }

        public int DomandeAccettate
        {
            get { return domandeAccettate; }
            set { domandeAccettate = value; }
        }

        public int AccompagnatoriAccettati
        {
            get { return accompagnatoriAccettati; }
            set { accompagnatoriAccettati = value; }
        }

        public int DomandeInCarico
        {
            get { return domandeInCarico; }
            set { domandeInCarico = value; }
        }

        public int AccompagnatoriInCarico
        {
            get { return accompagnatoriInCarico; }
            set { accompagnatoriInCarico = value; }
        }

        public int DomandeRifiutate { get; set; }

        public int PostiAccettatiRichiesti
        {
            get { return domandeAccettate + accompagnatoriAccettati + domandeInCarico + accompagnatoriInCarico; }
        }
    }
}