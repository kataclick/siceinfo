using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class DomandeStato
    {
        public DomandeStato()
        {
        }

        public DomandeStato(string stato, string indirizzoSesso, string indirizzoCognome, string indirizzoNome,
                            string indirizzoDenominazione, string indirizzoCAP, string indirizzoComune,
                            string indirizzoProvincia,
                            string cognomePartecipante, string nomePartecipante, string tipoVacanza, string luogoVacanza,
                            DateTime? dataInizio, DateTime? dataFine, int? numeroCorredo)
        {
            Stato = stato;
            IndirizzoSesso = indirizzoSesso;
            IndirizzoCognome = indirizzoCognome;
            IndirizzoNome = indirizzoNome;
            IndirizzoDenominazione = indirizzoDenominazione;
            IndirizzoCAP = indirizzoCAP;
            IndirizzoComune = indirizzoComune;
            IndirizzoProvincia = indirizzoProvincia;
            CognomePartecipante = cognomePartecipante;
            NomePartecipante = nomePartecipante;
            TipoVacanza = tipoVacanza;
            LuogoVacanza = luogoVacanza;
            DataInizio = dataInizio;
            DataFine = dataFine;
            NumeroCorredo = numeroCorredo;
            Causale = null;
        }

        public DomandeStato(string stato, string indirizzoSesso, string indirizzoCognome, string indirizzoNome,
                            string indirizzoDenominazione, string indirizzoCAP, string indirizzoComune,
                            string indirizzoProvincia,
                            string causale)
        {
            Stato = stato;
            IndirizzoSesso = indirizzoSesso;
            IndirizzoCognome = indirizzoCognome;
            IndirizzoNome = indirizzoNome;
            IndirizzoDenominazione = indirizzoDenominazione;
            IndirizzoCAP = indirizzoCAP;
            IndirizzoComune = indirizzoComune;
            IndirizzoProvincia = indirizzoProvincia;
            CognomePartecipante = null;
            NomePartecipante = null;
            TipoVacanza = null;
            LuogoVacanza = null;
            DataInizio = null;
            DataFine = null;
            NumeroCorredo = null;
            Causale = causale;
        }

        public string Stato { get; set; }

        public string IndirizzoCognome { get; set; }

        public string IndirizzoNome { get; set; }

        public string IndirizzoSesso { get; set; }

        public string IndirizzoDenominazione { get; set; }

        public string IndirizzoCAP { get; set; }

        public string IndirizzoComune { get; set; }

        public string IndirizzoProvincia { get; set; }

        public string CognomePartecipante { get; set; }

        public string NomePartecipante { get; set; }

        public string TipoVacanza { get; set; }

        public string LuogoVacanza { get; set; }

        public DateTime? DataInizio { get; set; }

        public DateTime? DataFine { get; set; }

        public int? NumeroCorredo { get; set; }

        public string Causale { get; set; }
    }
}