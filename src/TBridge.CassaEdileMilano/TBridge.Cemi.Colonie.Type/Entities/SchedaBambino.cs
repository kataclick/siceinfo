using System;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class SchedaBambino
    {
        public SchedaBambino()
        {
        }

        public SchedaBambino(Partecipante partecipante, Lavoratore lavoratore, int numeroCorredo,
                             string tipoDestinazione,
                             int progressivoTurno, int anno, string tipoVacanza, string cognomePar, string nomePar,
                             int? numeroCorredoPar)
        {
            Lavoratore = lavoratore;
            Partecipante = partecipante;
            NumeroCorredo = numeroCorredo;
            TipoDestinazione = tipoDestinazione;
            ProgressivoTurno = progressivoTurno;
            Anno = anno;
            TipoVacanza = tipoVacanza;
            CognomeParente = cognomePar;
            NomeParente = nomePar;
            NumeroCorredoParente = numeroCorredoPar;
        }

        public Partecipante Partecipante { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public int NumeroCorredo { get; set; }

        public string TipoDestinazione { get; set; }

        public int ProgressivoTurno { get; set; }

        public int Anno { get; set; }

        public string TipoVacanza { get; set; }

        public string CognomeParente { get; set; }

        public string NomeParente { get; set; }

        public int? NumeroCorredoParente { get; set; }

        public Accompagnatore Accompagnatore { get; set; }

        public String Taglia { get; set; }
    }
}