using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class Turno
    {
        private DateTime al;
        private decimal costoGiornaliero;
        private decimal costoGiornalieroAccompagnatore;
        private DateTime dal;
        private Destinazione destinazione;
        private int? idTurno;
        private int progressivoTurno;

        public Turno()
        {
        }

        public Turno(int? idTurno, int progressivoTurno, DateTime dal, DateTime al, Destinazione destinazione,
                     int idDestinazione, int idVacanza, int postiDisponibili, decimal costoGiornaliero)
        {
            IdTurno = idTurno;
            this.progressivoTurno = progressivoTurno;
            this.dal = dal;
            this.al = al;
            this.destinazione = destinazione;
            IdDestinazione = idDestinazione;
            IdVacanza = idVacanza;
            PostiDisponibili = postiDisponibili;
            CostoGiornaliero = costoGiornaliero;
        }

        public Turno(int? idTurno, int progressivoTurno, DateTime dal, DateTime al, Destinazione destinazione,
                     int idDestinazione, int idVacanza, int postiDisponibili, int postiOccupati,
                     decimal costoGiornaliero
            )
        {
            IdTurno = idTurno;
            this.progressivoTurno = progressivoTurno;
            this.dal = dal;
            this.al = al;
            this.destinazione = destinazione;
            IdDestinazione = idDestinazione;
            IdVacanza = idVacanza;
            PostiDisponibili = postiDisponibili;
            PostiOccupati = postiOccupati;
            CostoGiornaliero = costoGiornaliero;
        }

        public int? IdTurno
        {
            get
            {
                return idTurno;
            }
            set
            {
                idTurno = value;
            }
        }

        public int IdCombo
        {
            get
            {
                if (idTurno.HasValue)
                    return idTurno.Value;
                else
                    return -1;
            }
        }

        public int ProgressivoTurno
        {
            get
            {
                return progressivoTurno;
            }
            set
            {
                progressivoTurno = value;
            }
        }

        public string DescrizioneTurno
        {
            get
            {
                if (destinazione != null)
                {
                    return String.Format("{0} {1}", destinazione.DescrizioneTD, progressivoTurno);
                }
                else
                {
                    return progressivoTurno.ToString();
                }
            }
        }

        public string DescrizioneTurnoEstesa
        {
            get
            {
                if (destinazione != null)
                {
                    return String.Format("{0} {1} (dal {2:dd/MM/yyyy} al {3:dd/MM/yyyy})", destinazione.DescrizioneTD, progressivoTurno, dal, al);
                }
                else
                {
                    return progressivoTurno.ToString();
                }
            }
        }

        public DateTime Dal
        {
            get
            {
                return dal;
            }
            set
            {
                dal = value;
            }
        }

        public DateTime Al
        {
            get
            {
                return al;
            }
            set
            {
                al = value;
            }
        }

        public int PostiDisponibili
        {
            get;
            set;
        }

        public int IdVacanza
        {
            get;
            set;
        }

        public int IdDestinazione
        {
            get;
            set;
        }


        public Destinazione Destinazione
        {
            get
            {
                return destinazione;
            }
            set
            {
                destinazione = value;
            }
        }

        public decimal CostoGiornaliero
        {
            get
            {
                return costoGiornaliero;
            }
            set
            {
                costoGiornaliero = value;
            }
        }

        public decimal CostoGiornalieroAccompagnatore
        {
            get
            {
                return costoGiornalieroAccompagnatore;
            }
            set
            {
                costoGiornalieroAccompagnatore = value;
            }
        }

        public string StringaDestinazione
        {
            get
            {
                if (destinazione != null)
                    return destinazione.Luogo;
                else
                    return string.Empty;
            }
        }

        public int PostiOccupati
        {
            get;
            set;
        }

        public string DettaglioTurno
        {
            get
            {
                return String.Format("{0} {1}-{2}", progressivoTurno, dal.ToShortDateString(), al.ToShortDateString());
            }
        }
    }
}