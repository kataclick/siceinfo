using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class TurnoPresenza
    {
        private Destinazione destinazione;
        private int progressivoTurno;

        public TurnoPresenza()
        {
        }

        public TurnoPresenza(int idDomanda, Destinazione destinazione, int progressivoTurno, int idTurno, bool presenza)
        {
            IdDomanda = idDomanda;
            this.destinazione = destinazione;
            this.progressivoTurno = progressivoTurno;
            IdTurno = idTurno;
            Presenza = presenza;
        }

        public int IdDomanda { get; set; }

        public Destinazione Destinazione
        {
            get { return destinazione; }
            set { destinazione = value; }
        }

        public string DescrizioneTurno
        {
            get { return destinazione.DescrizioneTD + " " + progressivoTurno; }
        }

        public int ProgressivoTurno
        {
            get { return progressivoTurno; }
            set { progressivoTurno = value; }
        }

        public int IdTurno { get; set; }

        public bool Presenza { get; set; }
    }
}