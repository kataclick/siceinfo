using System;
using TBridge.Cemi.Colonie.Type.Collections;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class CostoPerCassa
    {
        private CostoPerTipologiaCollection costi;

        public CostoPerCassa(string cassaEdile)
        {
            CassaEdile = cassaEdile;

            costi = new CostoPerTipologiaCollection();
            costi.Add(new CostoPerTipologia("Rinunciatari"));
            costi.Add(new CostoPerTipologia("Disabili"));
            costi.Add(new CostoPerTipologia("Totale a saldo"));
            //costi.Add(new CostoPerTipologia("Ospiti"));
        }

        public string CassaEdile { get; set; }

        public CostoPerTipologiaCollection Costi
        {
            get { return costi; }
            set { costi = value; }
        }

        public CostoPerTipologia GetOspitiByDurataSoggiornoECosto(Int32 durata, Decimal costoGiornaliero)
        {
            foreach (CostoPerTipologia costoTemp in costi)
            {
                if (costoTemp.TipoCosto == "Ospiti"
                    && costoTemp.PeriodoPermanenza == durata
                    && costoTemp.QuotaIndividuale == costoGiornaliero)
                    return costoTemp;
            }

            return null;
        }

        public CostoPerTipologia GetAccompagnatoriByDurataSoggiornoECosto(Int32 durata,
                                                                          Decimal costoGiornalieroAccompagnatore)
        {
            foreach (CostoPerTipologia costoTemp in costi)
            {
                if (costoTemp.TipoCosto == "Accompagnatori"
                    && costoTemp.PeriodoPermanenza == durata
                    && costoTemp.QuotaIndividuale == costoGiornalieroAccompagnatore)
                    return costoTemp;
            }

            return null;
        }

        public CostoPerTipologia GetByTipologia(string tipologia)
        {
            foreach (CostoPerTipologia costoTemp in costi)
            {
                if (costoTemp.TipoCosto == tipologia)
                    return costoTemp;
            }

            return null;
        }

        public void RemoveTotaleSaldo()
        {
            for (int i = 0; i < costi.Count; i++)
            {
                if (costi[i].TipoCosto == "Totale a saldo")
                {
                    costi.RemoveAt(i);
                    break;
                }
            }
        }
    }
}