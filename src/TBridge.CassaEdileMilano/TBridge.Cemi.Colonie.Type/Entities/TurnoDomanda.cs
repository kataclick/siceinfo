using System;
using TBridge.Cemi.Colonie.Type.Enums;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class TurnoDomanda
    {
        private Autobus autobus;
        private DateTime? richiestaAnnullamento;
        private DateTime? rientroAnticipato;

        public TurnoDomanda()
        {
        }

        public TurnoDomanda(int idTurno, int idDomanda, Autobus autobus, bool? imbarcato, DateTime? rientroAnticipato,
                            DateTime? richiestaAnnullamento, string mancatoImbarco, bool? presenteColonia,
                            bool portatoreHandicap,
                            bool intolleranzeAlimentari, DateTime? partenzaPosticipata)
        {
            IdTurno = idTurno;
            IdDomanda = idDomanda;
            Imbarcato = imbarcato;
            this.autobus = autobus;
            this.rientroAnticipato = rientroAnticipato;
            this.richiestaAnnullamento = richiestaAnnullamento;
            MancatoImbarco = mancatoImbarco;
            PresenteColonia = presenteColonia;
            PortatoreHandicap = portatoreHandicap;
            IntolleranzeAlimentari = intolleranzeAlimentari;
            PartenzaPosticipata = partenzaPosticipata;
        }

        public TurnoDomanda(int idTurno, int idDomanda, Autobus autobus, bool? imbarcato, DateTime? rientroAnticipato,
                            DateTime? richiestaAnnullamento, string mancatoImbarco, bool? presenteColonia,
                            bool portatoreHandicap,
                            bool intolleranzeAlimentari, DateTime? partenzaPosticipata, Int32 progressivo,
                            String descrizione)
        {
            IdTurno = idTurno;
            IdDomanda = idDomanda;
            Imbarcato = imbarcato;
            this.autobus = autobus;
            this.rientroAnticipato = rientroAnticipato;
            this.richiestaAnnullamento = richiestaAnnullamento;
            MancatoImbarco = mancatoImbarco;
            PresenteColonia = presenteColonia;
            PortatoreHandicap = portatoreHandicap;
            IntolleranzeAlimentari = intolleranzeAlimentari;
            PartenzaPosticipata = partenzaPosticipata;
            Progressivo = progressivo;
            Descrizione = descrizione;
        }

        public int IdTurno { get; set; }

        public int IdDomanda { get; set; }

        public bool? Imbarcato { get; set; }

        public Autobus Autobus
        {
            get { return autobus; }
            set { autobus = value; }
        }

        public string MancatoImbarco { get; set; }

        public bool? PresenteColonia { get; set; }

        public DateTime? RientroAnticipato
        {
            get { return rientroAnticipato; }
            set { rientroAnticipato = value; }
        }

        public string RientroAnticipatoStringa
        {
            get
            {
                if (rientroAnticipato.HasValue)
                    return rientroAnticipato.Value.ToShortDateString();
                else
                    return string.Empty;
            }
        }

        public DateTime? RichiestaAnnullamento
        {
            get { return richiestaAnnullamento; }
            set { richiestaAnnullamento = value; }
        }

        public Boolean RichiestaAnnullamentoEsplicita
        {
            get;
            set;
        }

        public string RichiestaAnnullamentoStringa
        {
            get
            {
                if (richiestaAnnullamento.HasValue)
                    return richiestaAnnullamento.Value.ToShortDateString();
                else
                    return string.Empty;
            }
        }

        public string TargaAutobus
        {
            get
            {
                if (autobus != null)
                    return autobus.Targa;
                else
                    return string.Empty;
            }
        }

        public bool PortatoreHandicap { get; set; }

        public bool IntolleranzeAlimentari { get; set; }

        public DateTime? PartenzaPosticipata { get; set; }

        public TipoDomanda TipoDomanda { get; set; }

        #region Triffo: aggiunte due proprietÓ per descrivere il turno

        public String Descrizione { get; set; }
        public Int32 Progressivo { get; set; }

        public String DescrizioneCompleta
        {
            get { return Descrizione + " " + Progressivo.ToString(); }
        }

        #endregion
    }
}