using System;
using TBridge.Cemi.Colonie.Type.Collections;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class PrenotazioniTurno
    {
        private PrenotazioniCassaCollection prenotazioni;
        private Turno turno;

        /// <summary>
        /// 
        /// </summary>
        public PrenotazioniTurno()
        {
        }

        public PrenotazioniTurno(Turno turno, PrenotazioniCassaCollection prenotazioni)
        {
            this.turno = turno;
            Prenotazioni = prenotazioni;
        }

        public Turno Turno
        {
            get { return turno; }
            set { turno = value; }
        }

        public string DescrizioneTurno
        {
            get
            {
                if (turno != null) return turno.DescrizioneTurno;
                else return string.Empty;
            }
        }

        public int PostiTurno
        {
            get
            {
                if (turno != null) return turno.PostiDisponibili;
                else return 0;
            }
        }

        public PrenotazioniCassaCollection Prenotazioni
        {
            get { return prenotazioni; }
            set { prenotazioni = value; }
        }

        /// <summary>
        /// Posti prenotati e che sono stati concessi
        /// </summary>
        public int PostiConcessi
        {
            get { return GetPostiConcessi(); }
        }

        /// <summary>
        /// Posti prenotati che devono anche essere vagliati e concessi
        /// </summary>
        public int PostiRichiestiDaVerificare
        {
            get { return GetPostiRichiestiDaVerificare(); }
        }

        /// <summary>
        /// Ritorna il numero di posti che restano a disposizione per un dato turno
        /// </summary>
        public int PostiDisponibili
        {
            get { return PostiTurno - PostiConcessi; }
        }

        private int GetPostiConcessi()
        {
            int posti = 0;

            foreach (PrenotazioniCassa pren in prenotazioni)
            {
                if (pren.Prenotazioni != null && pren.Prenotazioni.Count == 1)
                {
                    if (pren.Prenotazioni[0].Accettata.HasValue && pren.Prenotazioni[0].Accettata.Value)
                    {
                        if (pren.Prenotazioni[0].PostiConcessi.HasValue)
                            posti += pren.Prenotazioni[0].PostiConcessi.Value;
                        else
                            posti += pren.Prenotazioni[0].PostiRichiesti.Value;
                    }
                }
            }

            return posti;
        }

        private int GetPostiRichiestiDaVerificare()
        {
            int posti = 0;

            foreach (PrenotazioniCassa pren in Prenotazioni)
            {
                if (pren.Prenotazioni != null && pren.Prenotazioni.Count == 1)
                {
                    if (!pren.Prenotazioni[0].Accettata.HasValue)
                    {
                        posti += pren.Prenotazioni[0].PostiRichiesti.Value;
                    }
                }
            }

            return posti;
        }
    }
}