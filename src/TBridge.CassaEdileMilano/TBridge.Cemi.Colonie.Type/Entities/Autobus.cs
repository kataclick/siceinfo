using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class Autobus
    {
        private string codiceAutobus;
        private int? idAutobus;
        private string targa;

        public Autobus()
        {
        }

        public Autobus(int? idAutobus, string codiceAutobus, string targa, int idTurno)
        {
            this.idAutobus = idAutobus;
            this.codiceAutobus = codiceAutobus;
            this.targa = targa;
            IdTurno = idTurno;
        }

        public int? IdAutobus
        {
            get { return idAutobus; }
            set { idAutobus = value; }
        }

        public int IdCombo
        {
            get
            {
                if (idAutobus.HasValue) return idAutobus.Value;
                else return -1;
            }
        }

        public string CodiceAutobus
        {
            get { return codiceAutobus; }
            set { codiceAutobus = value; }
        }

        public string Targa
        {
            get { return targa; }
            set { targa = value; }
        }

        public string TestoCombo
        {
            get { return codiceAutobus + " - " + targa; }
        }

        public int IdTurno { get; set; }
    }
}