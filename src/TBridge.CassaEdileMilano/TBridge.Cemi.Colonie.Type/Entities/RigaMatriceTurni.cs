using System;
using TBridge.Cemi.Colonie.Type.Collections;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class RigaMatriceTurni
    {
        private Destinazione destinazione;

        public RigaMatriceTurni()
        {
        }

        public RigaMatriceTurni(Destinazione destinazione, TurnoCollection turni)
        {
            this.destinazione = destinazione;
            Turni = turni;
        }

        public Destinazione Destinazione
        {
            get { return destinazione; }
            set { destinazione = value; }
        }

        public string StringaDestinazione
        {
            get
            {
                if (destinazione != null) return destinazione.Luogo;
                else return string.Empty;
            }
        }

        public TurnoCollection Turni { get; set; }
    }
}