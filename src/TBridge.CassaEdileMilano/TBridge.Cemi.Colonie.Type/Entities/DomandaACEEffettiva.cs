using System;
using TBridge.Cemi.Colonie.Type.Collections;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class DomandaACEEffettiva
    {
        public DomandaACEEffettiva()
        {
        }

        public DomandaACEEffettiva(int idDomanda, string cognome, string nome, DateTime? dataNascita, string stato
            )
        {
            IdDomanda = idDomanda;
            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            Stato = stato;

            Turni = new TurnoDomandaCollection();
        }

        public Int32 IdDomanda { get; set; }
        public Int32 NumeroCorredo { get; set; }
        public String Cognome { get; set; }
        public String Nome { get; set; }

        public DateTime? DataNascita { get; set; }
        public String Sesso { get; set; }
        public Boolean? PortatoreHandicap { get; set; }

        //public DateTime? RichiestaAnnullamento { get; set; }
        //public DateTime? RientroAnticipato{ get; set; }
        //public DateTime? PartenzaPosticipata{ get; set; }
        //public Boolean? PresenteColonia { get; set; }

        public String Descrizione { get; set; }
        public Int32 Progressivo { get; set; }
        public Boolean? IntolleranzeAlimentari { get; set; }

        public String Accompagnatore { get; set; }
        public String Stato { get; set; }

        public TurnoDomandaCollection Turni { get; set; }
    }
}