using System;
using TBridge.Cemi.Colonie.Type.Collections;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class PrenotazioniCassa
    {
        private PrenotazioneCollection prenotazioni;

        public PrenotazioniCassa()
        {
        }

        public PrenotazioniCassa(string idCassaEdile, PrenotazioneCollection prenotazioni)
        {
            IdCassaEdile = idCassaEdile;
            this.prenotazioni = prenotazioni;
        }

        public string IdCassaEdile { get; set; }

        public PrenotazioneCollection Prenotazioni
        {
            get { return prenotazioni; }
            set { prenotazioni = value; }
        }

        public int IdPrenotazione
        {
            get
            {
                if (prenotazioni != null && prenotazioni.Count == 1 && prenotazioni[0].IdPrenotazione.HasValue)
                    return prenotazioni[0].IdPrenotazione.Value;
                else return -1;
            }
        }
    }
}