using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class Destinazione
    {
        private int? idDestinazione;
        private string luogo;
        private TipoDestinazione tipoDestinazione;

        public Destinazione()
        {
        }

        public Destinazione(int? idDestinazione, TipoDestinazione tipoDestinazione, string luogo, bool attiva)
        {
            this.idDestinazione = idDestinazione;
            this.tipoDestinazione = tipoDestinazione;
            this.luogo = luogo;
            Attiva = attiva;
        }

        public int? IdDestinazione
        {
            get { return idDestinazione; }
            set { idDestinazione = value; }
        }

        public int IdCombo
        {
            get
            {
                if (idDestinazione.HasValue) return idDestinazione.Value;
                else return -1;
            }
        }

        public TipoDestinazione TipoDestinazione
        {
            get { return tipoDestinazione; }
            set { tipoDestinazione = value; }
        }

        public string DescrizioneTD
        {
            get
            {
                if (tipoDestinazione != null) return tipoDestinazione.Descrizione;
                else return string.Empty;
            }
        }

        public string DescrizioneTV
        {
            get
            {
                if (tipoDestinazione != null && tipoDestinazione.TipoVacanza != null)
                    return tipoDestinazione.TipoVacanza.Descrizione;
                else return string.Empty;
            }
        }

        public string Luogo
        {
            get { return luogo; }
            set { luogo = value; }
        }

        public string DestinazioneLuogo
        {
            get
            {
                if (tipoDestinazione != null) return tipoDestinazione.Descrizione + " - " + luogo;
                else return luogo;
            }
        }

        public bool Attiva { get; set; }
    }
}