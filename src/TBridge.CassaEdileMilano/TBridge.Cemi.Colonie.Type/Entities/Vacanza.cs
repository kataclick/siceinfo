using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class Vacanza
    {
        private int anno;
        private TipoVacanza tipoVacanza;

        public Vacanza()
        {
        }

        public Vacanza(int? idVacanza, int anno, TipoVacanza tipoVacanza, decimal costoGiornaliero, decimal penalita,
                       decimal costoDisabile, int limitePenalita)
        {
            IdVacanza = idVacanza;
            this.anno = anno;
            this.tipoVacanza = tipoVacanza;
            CostoGiornaliero = costoGiornaliero;
            Penalita = penalita;
            CostoDisabile = costoDisabile;
            LimitePenalita = limitePenalita;
        }

        public int? IdVacanza
        {
            get;
            set;
        }

        public int Anno
        {
            get
            {
                return anno;
            }
            set
            {
                anno = value;
            }
        }

        public TipoVacanza TipoVacanza
        {
            get
            {
                return tipoVacanza;
            }
            set
            {
                tipoVacanza = value;
            }
        }

        public string StringaTipoVacanza
        {
            get
            {
                if (tipoVacanza != null)
                    return tipoVacanza.Descrizione;
                else
                    return string.Empty;
            }
        }

        public decimal CostoGiornaliero
        {
            get;
            set;
        }

        public decimal CostoGiornalieroAccompagnatore
        {
            get;
            set;
        }

        public decimal Penalita
        {
            get;
            set;
        }

        public decimal CostoDisabile
        {
            get;
            set;
        }

        public int LimitePenalita
        {
            get;
            set;
        }

        public DateTime? DataInizioPrenotazioni
        {
            get;
            set;
        }

        public DateTime? DataFinePrenotazioni
        {
            get;
            set;
        }

        public DateTime? DataInizioDomandeACE
        {
            get;
            set;
        }

        public DateTime? DataFineDomandeACE
        {
            get;
            set;
        }

        public DateTime LimiteInferioreBimbi
        {
            get;
            set;
        }

        public DateTime LimiteSuperioreBimbi
        {
            get;
            set;
        }

        public bool Attiva
        {
            get;
            set;
        }

        public DateTime? DataInizioRichieste
        {
            get;
            set;
        }

        public DateTime? DataFineRichieste
        {
            get;
            set;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", StringaTipoVacanza, anno);
        }
    }
}