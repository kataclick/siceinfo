﻿using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    public class PrecedenteCandidatura
    {
        public Int32 Anno { get; set; }

        public String Stato { get; set; }

        public String Note { get; set; }

        public override string ToString()
        {
            return String.Format("{0}: {1} {2}", this.Anno, this.Stato, String.IsNullOrWhiteSpace(this.Note) ? "" : String.Format("({0})", this.Note));
        }
    }
}
