using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class LavoratoreACE
    {
        private string cap;
        private string cognome;
        private string comune;
        private string indirizzo;
        private string nome;
        private string provincia;

        public int? IdLavoratore { get; set; }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string NomeCompleto
        {
            get { return String.Format("{0} {1}", cognome, nome); }
        }

        public DateTime DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public char Sesso { get; set; }

        public string Telefono { get; set; }

        public string Cellulare { get; set; }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Comune
        {
            get { return comune; }
            set { comune = value; }
        }

        public string Cap
        {
            get { return cap; }
            set { cap = value; }
        }

        public string IndirizzoCompleto
        {
            get { return string.Format("{0} {1} ({2}) {3}", indirizzo, comune, provincia, cap); }
        }

        public string IdCassaEdile { get; set; }

        public bool Modificabile { get; set; }
    }
}