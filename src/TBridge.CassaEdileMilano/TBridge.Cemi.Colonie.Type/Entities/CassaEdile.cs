using System;

namespace TBridge.Cemi.Colonie.Type.Entities
{
    [Serializable]
    public class CassaEdile
    {
        public CassaEdile()
        {
        }

        public CassaEdile(string idCassaEdile, string descrizione)
        {
            IdCassaEdile = idCassaEdile;
            Descrizione = descrizione;
        }

        public string IdCassaEdile { get; set; }

        public string Descrizione { get; set; }

        public String Email { get; set; }
    }
}