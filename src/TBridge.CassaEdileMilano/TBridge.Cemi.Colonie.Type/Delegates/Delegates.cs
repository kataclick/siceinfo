using TBridge.Cemi.Colonie.Type.Entities;

namespace TBridge.Cemi.Colonie.Type.Delegates
{
    public delegate void DomandaACESelectedEventHandler(DomandaACE domanda);

    public delegate void LavoratoreACESelectedEventHandler(LavoratoreACE lavoratore);

    public delegate void LavoratoreACEConfirmedEventHandler(LavoratoreACE lavoratore);

    public delegate void LavoratoreACEModifiedEventHandler(LavoratoreACE lavoratore);

    public delegate void FamiliareACEModifiedEventHandler(FamiliareACE familiare);

    public delegate void FamiliareACEConfirmedEventHandler(FamiliareACE lavoratore);

    public delegate void CancelledEventHandler();

    public delegate void CassaEdileSelectEventHandler(CassaEdile cassaEdile);
}