namespace TBridge.Cemi.Colonie.Type.Filters
{
    public class CassaEdileFilter
    {
        public string IdCassaEdile { get; set; }

        public string Descrizione { get; set; }

        public int? IdVacanza { get; set; }
    }
}