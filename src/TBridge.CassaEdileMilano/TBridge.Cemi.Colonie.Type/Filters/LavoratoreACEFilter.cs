using System;

namespace TBridge.Cemi.Colonie.Type.Filters
{
    public class LavoratoreACEFilter
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string IdCassaEdile { get; set; }
    }
}