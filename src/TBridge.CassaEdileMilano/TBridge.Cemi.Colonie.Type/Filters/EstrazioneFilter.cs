﻿using System;

namespace TBridge.Cemi.Colonie.Type.Filters
{
    public class EstrazioneFilter
    {
        public Int32 IdVacanza
        {
            get;
            set;
        }

        public DateTime? DataAssunzioneDal
        {
            get;
            set;
        }

        public DateTime? DataAssunzioneAl
        {
            get;
            set;
        }

        public Int32? IdMansione
        {
            get;
            set;
        }
    }
}
