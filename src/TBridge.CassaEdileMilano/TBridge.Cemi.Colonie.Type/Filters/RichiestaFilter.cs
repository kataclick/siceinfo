﻿using System;
using TBridge.Cemi.Colonie.Type.Enums;

namespace TBridge.Cemi.Colonie.Type.Filters
{
    public class RichiestaFilter
    {
        public Int32? IdRichiesta
        {
            get;
            set;
        }

        public String Cognome
        {
            get;
            set;
        }

        public String Nome
        {
            get;
            set;
        }

        public Int32? IdMansione
        {
            get;
            set;
        }

        public Int32? IdMansioneAssegnata
        {
            get;
            set;
        }

        public Int32? IdMansioneColloquio
        {
            get;
            set;
        }

        public Int32? IdTurno
        {
            get;
            set;
        }

        public Int32? IdTurnoAssegnato
        {
            get;
            set;
        }

        public StatoRichiesta? Stato
        {
            get;
            set;
        }

        public Int32? IdValutazione
        {
            get;
            set;
        }

        public Boolean? EsperienzaCE
        {
            get;
            set;
        }

        public String Note
        {
            get;
            set;
        }

        public Int32? IdVacanza { get; set; }
    }
}
