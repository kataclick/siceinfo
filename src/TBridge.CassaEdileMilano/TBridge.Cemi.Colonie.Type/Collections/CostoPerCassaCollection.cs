using System;
using System.Collections.Generic;
using TBridge.Cemi.Colonie.Type.Entities;

namespace TBridge.Cemi.Colonie.Type.Collections
{
    [Serializable]
    public class CostoPerCassaCollection : List<CostoPerCassa>
    {
        public CostoPerCassa GetByCassaEdile(string cassaEdile)
        {
            foreach (CostoPerCassa costoTemp in this)
            {
                if (costoTemp.CassaEdile == cassaEdile)
                    return costoTemp;
            }

            return null;
        }
    }
}