using System;
using System.Collections.Generic;
using TBridge.Cemi.Colonie.Type.Entities;

namespace TBridge.Cemi.Colonie.Type.Collections
{
    [Serializable]
    public class CostoPerTipologiaCollection : List<CostoPerTipologia>
    {
    }
}