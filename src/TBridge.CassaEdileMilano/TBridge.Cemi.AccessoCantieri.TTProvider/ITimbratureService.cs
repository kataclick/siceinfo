namespace TBridge.Cemi.AccessoCantieri.TTProvider
{
    public interface ITimbratureService
    {
        Timbratura[] GetTimbrature();

        Timbratura[] GetTimbraturePeriodo(string dataInizio, string dataFine);
    }
}