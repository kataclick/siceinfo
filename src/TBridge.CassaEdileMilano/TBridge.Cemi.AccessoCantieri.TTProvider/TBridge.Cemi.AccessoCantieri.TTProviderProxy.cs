using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.AccessoCantieri.TTProvider
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "TimbratureServiceSoap", Namespace = "http://www.cassaedilemilano.it/WebService/")]
    public class TimbratureService : SoapHttpClientProtocol, ITimbratureService
    {
        private SendOrPostCallback GetTimbratureOperationCompleted;

        private SendOrPostCallback GetTimbraturePeriodoOperationCompleted;

        /// <remarks />
        public TimbratureService()
        {
            string urlSetting = ConfigurationManager.AppSettings["TimbratureService.ServiceEndpointURL"];
            if (((urlSetting != null)
                 && (urlSetting != "")))
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://93.62.222.187/server.php";
            }
        }

        #region ITimbratureService Members

        /// <remarks />
        [SoapDocumentMethod("http://www.cassaedilemilano.it/WebService/GetTimbrature",
            RequestNamespace = "http://www.cassaedilemilano.it/WebService/",
            ResponseNamespace = "http://www.cassaedilemilano.it/WebService/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public Timbratura[] GetTimbrature()
        {
            object[] results = Invoke("GetTimbrature", new object[0]);
            return ((Timbratura[]) (results[0]));
        }

        /// <remarks />
        [SoapDocumentMethod("http://www.cassaedilemilano.it/WebService/GetTimbraturePeriodo",
            RequestNamespace = "http://www.cassaedilemilano.it/WebService/",
            ResponseNamespace = "http://www.cassaedilemilano.it/WebService/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public Timbratura[] GetTimbraturePeriodo([XmlElement(ElementName = "dataInizio")] string dataInizio,
                                                 [XmlElement(ElementName = "dataFine")] string dataFine)
        {
            object[] results = Invoke("GetTimbraturePeriodo", new object[]
                                                                  {
                                                                      dataInizio,
                                                                      dataFine
                                                                  });
            return ((Timbratura[]) (results[0]));
        }

        #endregion

        /// <remarks />
        public event GetTimbratureCompletedEventHandler GetTimbratureCompleted;

        /// <remarks />
        public event GetTimbraturePeriodoCompletedEventHandler GetTimbraturePeriodoCompleted;

        /// <remarks />
        public void GetTimbratureAsync()
        {
            GetTimbratureAsync(null);
        }

        /// <remarks />
        public void GetTimbratureAsync(object userState)
        {
            if ((GetTimbratureOperationCompleted == null))
            {
                GetTimbratureOperationCompleted = new SendOrPostCallback(OnGetTimbratureOperationCompleted);
            }
            InvokeAsync("GetTimbrature", new object[0], GetTimbratureOperationCompleted, userState);
        }

        private void OnGetTimbratureOperationCompleted(object arg)
        {
            if ((GetTimbratureCompleted != null))
            {
                InvokeCompletedEventArgs invokeArgs = ((InvokeCompletedEventArgs) (arg));
                GetTimbratureCompleted(this,
                                       new GetTimbratureCompletedEventArgs(invokeArgs.Results, invokeArgs.Error,
                                                                           invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void GetTimbraturePeriodoAsync(string dataInizio, string dataFine)
        {
            GetTimbraturePeriodoAsync(dataInizio, dataFine, null);
        }

        /// <remarks />
        public void GetTimbraturePeriodoAsync(string dataInizio, string dataFine, object userState)
        {
            if ((GetTimbraturePeriodoOperationCompleted == null))
            {
                GetTimbraturePeriodoOperationCompleted = new SendOrPostCallback(OnGetTimbraturePeriodoOperationCompleted);
            }
            InvokeAsync("GetTimbraturePeriodo", new object[]
                                                    {
                                                        dataInizio,
                                                        dataFine
                                                    }, GetTimbraturePeriodoOperationCompleted, userState);
        }

        private void OnGetTimbraturePeriodoOperationCompleted(object arg)
        {
            if ((GetTimbraturePeriodoCompleted != null))
            {
                InvokeCompletedEventArgs invokeArgs = ((InvokeCompletedEventArgs) (arg));
                GetTimbraturePeriodoCompleted(this,
                                              new GetTimbraturePeriodoCompletedEventArgs(invokeArgs.Results,
                                                                                         invokeArgs.Error,
                                                                                         invokeArgs.Cancelled,
                                                                                         invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "4.0.30319.1")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/WebService/", TypeName = "Timbratura")]
    public class Timbratura
    {
        /// <remarks />
        [XmlElement(ElementName = "CodiceFiscaleImpresa")] public string CodiceFiscaleImpresa;

        /// <remarks />
        [XmlElement(ElementName = "CodiceFiscaleLavoratore")] public string CodiceFiscaleLavoratore;

        /// <remarks />
        [XmlElement(ElementName = "DataOra")] public string DataOra;

        /// <remarks />
        [XmlElement(ElementName = "IdentificativoLettore")] public string IdentificativoLettore;

        /// <remarks />
        [XmlElement(ElementName = "Ingresso")] public int Ingresso;

        public Timbratura()
        {
        }

        public Timbratura(string identificativoLettore, string dataOra, string codiceFiscaleLavoratore,
                          string codiceFiscaleImpresa, int ingresso)
        {
            IdentificativoLettore = identificativoLettore;
            DataOra = dataOra;
            CodiceFiscaleLavoratore = codiceFiscaleLavoratore;
            CodiceFiscaleImpresa = codiceFiscaleImpresa;
            Ingresso = ingresso;
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void GetTimbratureCompletedEventHandler(object sender, GetTimbratureCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class GetTimbratureCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal GetTimbratureCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public Timbratura[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return ((Timbratura[]) (results[0]));
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void GetTimbraturePeriodoCompletedEventHandler(
        object sender, GetTimbraturePeriodoCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class GetTimbraturePeriodoCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal GetTimbraturePeriodoCompletedEventArgs(object[] results, Exception exception, bool cancelled,
                                                        object userState) :
                                                            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public Timbratura[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return ((Timbratura[]) (results[0]));
            }
        }
    }
}