﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Collections.Sintesi;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using TipoContratto = TBridge.Cemi.Type.Entities.TipoContratto;

namespace TBridge.Cemi.Sintesi.Data
{
    public class SintesiDataAccess
    {
        public SintesiDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public StatusStranieroCollection GetStatusStraniero()
        {
            StatusStranieroCollection status = new StatusStranieroCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiStatusStranieroSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        StatusStraniero statusS = new StatusStraniero();
                        status.Add(statusS);

                        statusS.Codice = reader.GetString(indiceCodice);
                        statusS.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return status;
        }

        public MotivoPermessoCollection GetMotiviPermesso()
        {
            MotivoPermessoCollection motivi = new MotivoPermessoCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiMotiviPermessoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        MotivoPermesso motivo = new MotivoPermesso();
                        motivi.Add(motivo);

                        motivo.Codice = reader.GetString(indiceCodice);
                        motivo.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return motivi;
        }

        public TitoloDiStudioCollection GetTitoliDiStudio()
        {
            TitoloDiStudioCollection titoli = new TitoloDiStudioCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTitoliDiStudioSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TitoloDiStudio titolo = new TitoloDiStudio();
                        titoli.Add(titolo);

                        titolo.Codice = reader.GetString(indiceCodice);
                        titolo.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return titoli;
        }

        public TipoComunicazioneCollection GetTipiComunicazione()
        {
            TipoComunicazioneCollection tipiComunicazione = new TipoComunicazioneCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTipiComunicazioneSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoComunicazione tipoComunicazione = new TipoComunicazione();
                        tipiComunicazione.Add(tipoComunicazione);

                        tipoComunicazione.Codice = reader.GetString(indiceCodice);
                        tipoComunicazione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiComunicazione;
        }

        //NON USATO
        public TipoContrattoCollection GetTipiContratto()
        {
            TipoContrattoCollection tipiContratto = new TipoContrattoCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTipiContrattoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        Type.Entities.Sintesi.TipoContratto tipoContratto = new Type.Entities.Sintesi.TipoContratto();
                        tipiContratto.Add(tipoContratto);

                        tipoContratto.Codice = reader.GetString(indiceCodice);
                        tipoContratto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiContratto;
        }

        public CessazioneRLCollection GetCessazioniRL()
        {
            CessazioneRLCollection cessazioni = new CessazioneRLCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCessazioniRLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        CessazioneRL cessazione = new CessazioneRL();
                        cessazioni.Add(cessazione);

                        cessazione.Codice = reader.GetString(indiceCodice);
                        cessazione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return cessazioni;
        }

        public TrasformazioneRLCollection GetTrasformazioniRL()
        {
            TrasformazioneRLCollection trasformazioni = new TrasformazioneRLCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTrasformazioniRLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TrasformazioneRL trasformazione = new TrasformazioneRL();
                        trasformazioni.Add(trasformazione);

                        trasformazione.Codice = reader.GetString(indiceCodice);
                        trasformazione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return trasformazioni;
        }

        public CCNLCollection GetCCNL()
        {
            CCNLCollection ccnls = new CCNLCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCCNLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        CCNL ccnl = new CCNL();
                        ccnls.Add(ccnl);

                        ccnl.Codice = reader.GetString(indiceCodice);
                        ccnl.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return ccnls;
        }

        public TipoOrarioCollection GetTipiOrario()
        {
            TipoOrarioCollection tipiOrario = new TipoOrarioCollection();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCCNLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoOrario tipoOrario = new TipoOrario();
                        tipiOrario.Add(tipoOrario);

                        tipoOrario.Codice = reader.GetString(indiceCodice);
                        tipoOrario.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiOrario;
        }

        public TipoContratto GetTipoContratto(string idTipocontratto)
        {
            TipoContratto contratto = new TipoContratto();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiContrattoSintesiSelectByTipoContratto"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idTipocontratto", DbType.String, idTipocontratto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        contratto.IdContratto = reader["idContratto"].ToString();
                        contratto.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }
            return contratto;
        }

        public TitoloDiStudio GetTitoloDiStudio(string codice)
        {
            TitoloDiStudio titoloDiStudio = new TitoloDiStudio();

            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTitoliDiStudioSelectByCodice"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codice", DbType.String, codice);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        titoloDiStudio.Codice = reader["codice"].ToString();
                        titoloDiStudio.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }

            return titoloDiStudio;
        }

        public NazionalitaSiceNew GetNazionalita(String idNazione)
        {
            NazionalitaSiceNew nazionalita = new NazionalitaSiceNew();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiNazioniSelectByIdNazione"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idNazione", DbType.String, idNazione);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        nazionalita.IdNazione = reader["idNazioneSiceNew"].ToString();
                        nazionalita.Nazionalita = reader["nomeNazione"].ToString();
                    }
                }
            }

            return nazionalita;
        }

        public TipoInizioRapporto GetTipoInizioRapporto(string idTipoInizioRapporto)
        {
            TipoInizioRapporto tipoInizioRapporto = new TipoInizioRapporto();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiInizioRapportoSintesiSelectByTipoInizioRapporto"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idTipoInizioRapporto", DbType.String, idTipoInizioRapporto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        tipoInizioRapporto.IdTipoInizioRapporto = reader["idTipoInizioRapporto"].ToString();
                        tipoInizioRapporto.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }
            return tipoInizioRapporto;
        }

        public TipoQualifica GetTipoQualifica(string idQualifica)
        {
            TipoQualifica tipoQualifica = new TipoQualifica();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTipiQualificaSelectByTipoQualifica"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idQualifica", DbType.String, idQualifica);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        tipoQualifica.IdQualifica = reader["idQualifica"].ToString();
                        tipoQualifica.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }
            return tipoQualifica;
        }

        public TBridge.Cemi.Type.Collections.TipoCategoriaCollection GetTipiCategoria()
        {
            TBridge.Cemi.Type.Collections.TipoCategoriaCollection tipiCategoria = new TBridge.Cemi.Type.Collections.TipoCategoriaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCategoriaSelectSintesi"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCategoria = reader.GetOrdinal("idCategoria");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCategoria tipoCategoria = new TipoCategoria();
                        tipiCategoria.Add(tipoCategoria);

                        tipoCategoria.IdCategoria = reader.GetString(indiceIdCategoria);
                        tipoCategoria.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCategoria;
        }

        public TBridge.Cemi.Type.Collections.TipoCategoriaCollection GetTipiCategoria(String tipoContratto)
        {
            TBridge.Cemi.Type.Collections.TipoCategoriaCollection tipiCategoria = new TBridge.Cemi.Type.Collections.TipoCategoriaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCategoriaSelectSintesi"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoContratto", DbType.String, tipoContratto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCategoria = reader.GetOrdinal("idCategoria");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCategoria tipoCategoria = new TipoCategoria();
                        tipiCategoria.Add(tipoCategoria);

                        tipoCategoria.IdCategoria = reader.GetString(indiceIdCategoria);
                        tipoCategoria.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCategoria;
        }


        public TBridge.Cemi.Type.Collections.TipoQualificaCollection GetTipiQualifica(String tipoCategoria)
        {
            TBridge.Cemi.Type.Collections.TipoQualificaCollection tipiQualifica = new TBridge.Cemi.Type.Collections.TipoQualificaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_SintesiTipiQualificaSelect]"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoCategoria", DbType.String, tipoCategoria);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdQualifica = reader.GetOrdinal("idQualifica");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoQualifica tipoQualifica = new TipoQualifica();
                        tipiQualifica.Add(tipoQualifica);

                        tipoQualifica.IdQualifica = reader.GetString(indiceIdQualifica);
                        tipoQualifica.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiQualifica;
        }
    }
}