using TBridge.Cemi.Geocode.Type.Entities;

namespace TBridge.Cemi.Geocode.Type.Delegates
{
    public delegate void IndirizzoSelectedEventHandler(Indirizzo indirizzo);
}