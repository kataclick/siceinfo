﻿using System;

namespace TBridge.Cemi.Geocode.Type.Entities
{
    [Serializable]
    public class Indirizzo : Cemi.Type.Entities.Indirizzo
    {
        public Decimal? Latitudine { get; set; }
        public Decimal? Longitudine { get; set; }

        public String IndirizzoCompletoGeocode
        {
            get
            {
                return String.Format("{0} {1} {2}", IndirizzoBase, Localita, Stato);
            }
        }

        public Boolean Georeferenziato
        {
            get { return Latitudine.HasValue && Longitudine.HasValue; }
        }
    }
}