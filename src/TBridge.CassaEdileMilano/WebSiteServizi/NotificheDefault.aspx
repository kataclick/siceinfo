<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="NotificheDefault.aspx.cs" Inherits="NotificheDefault" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc1" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche"
        sottoTitolo="Benvenuto nella sezione notifiche" />
    <br />
    <p class="DefaultPage">
        In questa sezione le imprese danno comunicazione dell�instaurazione di rapporti
        di lavoro e della cessazione dei medesimi.
        <br />
        Trattasi di comunicazioni similari a quelle da effettuarsi ai sensi della legge
        248/2006 nei confronti di INAIL e dei Centri per l�Impiego, che consentono di fornire
        a Cassa Edile tempestive informazioni sul proprio organico, anche al fine di aggiornare
        simultaneamente il programma "Subappalti".
        <br />
        <br />
        Si rende noto che le notifiche delle variazioni effettuate con questo programma
        non sono sostitutive delle dovute corrispondenti comunicazioni da effettuarsi in
        sede di denuncia mensile.
        <br />
        <br />
        Per procedere alla segnalazione di variazioni di rapporti di lavoro, servirsi del
        men� verticale collocato sulla sinistra dello schermo.
    </p>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuNotifiche ID="MenuNotifiche1" runat="server" />
</asp:Content>
