<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportImpreseIscritte.aspx.cs" Inherits="ReportImpreseIscritte" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu" TagPrefix="uc1" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Report imprese iscritte"
        titolo="Iscrizione CE" />
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td width="17%">
                    Codice Impresa:
                </td>
                <td width="30%">
                    <telerik:RadTextBox ID="RadTextBoxCodiceImpresa" runat="server" Width="100%" />
                </td>
                <td width="5%">
                </td>
                <td width="18%">
                    Ragione sociale:
                </td>
                <td width="30%">
                    <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>
                    Dal:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDa" Width="100%" runat="server" />
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidatorData" ControlToValidate="RadDatePickerDataA"
                        ControlToCompare="RadDatePickerDataDa" Operator="GreaterThanEqual" ErrorMessage="La data di fine non pu� essere precedente a quella di inizio"
                        ValidationGroup="VisualizzazioneReport" runat="server" Type="Date">*</asp:CompareValidator>
                </td>
                <td>
                    Al:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataA" Width="100%" runat="server" />
                </td>
            </tr>
        </table>
        <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
            Text="Visualizza report" ValidationGroup="VisualizzazioneReport" />
        <asp:ValidationSummary ID="ValidationSummaryData" runat="server" ValidationGroup="VisualizzazioneReport" />
    </div>
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerImpreseIscritte" runat="server" ProcessingMode="Remote"
                    ShowZoomControl="False" OnInit="ReportViewerImpreseIscritte_Init" Height="550pt"
                    Width="550pt">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
