#region

using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Notifiche.Business;
using TBridge.Cemi.Notifiche.Type.Entities;
using TBridge.Cemi.Notifiche.Type.Exceptions;
using Telerik.Web.UI;

#endregion

public partial class NotificheAssunzione : Page
{
    private readonly Common bizCommon = new Common();
    private int idImpresa;
    //private int idUtente;
    private NotificaBusiness notificaBiz;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.NNotificheGestione);

        notificaBiz = new NotificaBusiness();

        // Imposto il limite per la data assunzione a tre giorni dopo la data di oggi
        CompareValidatorDataAssunzione.ValueToCompare = DateTime.Now.AddDays(3).ToShortDateString();

        if (Session["idImpresa"] != null)
            idImpresa = (int) Session["idImpresa"];

        #region Click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonOk, null) + ";");
        sb.Append("return true;");
        ButtonOk.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonOk);

        #endregion

        NotificheImpresaSelezionata1.OnImpresaSelected += NotificheImpresaSelezionata1_OnImpresaSelected;

        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();


                int idConsulente = consulente.IdConsulente;

                PanelImprese.Visible = true;
            }
            else
            {
                PanelImprese.Visible = false;
            }
        }
    }

    private void NotificheImpresaSelezionata1_OnImpresaSelected()
    {
        Page.Validate("selezioneImpresa");
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Page.Validate("selezioneImpresa");

            if (Page.IsValid)
            {
                if (CheckData())
                {
                    Notifica notifica =
                        new Notifica(TextBoxCognome.Text.Trim().ToUpper(), TextBoxNome.Text.Trim().ToUpper(),
                                     DateTime.Parse(TextBoxDataNascita.Text),
                                     TextBoxComuneNascita.Text.Trim().ToUpper(), RadioButtonListSesso.SelectedValue,
                                     TextBoxCF.Text.ToUpper(),
                                     DateTime.Parse(TextBoxDataAssunzione.Text), idImpresa);
                    try
                    {
                        notificaBiz.Assunzione(notifica);

                        LabelErrore.Text = "Notifica inserita correttamente";
                        AzzeraCampi();
                    }
                    catch (NotificaPresenteException npEx)
                    {
                        LabelErrore.Text = "E' gi� presente una notifica associata al lavoratore";
                    }
                    catch (DenunciaPresenteException dpEx)
                    {
                        int mesiRitardo = notificaBiz.MesiRitardoDati();
                        LabelErrore.Text = "E' gi� presente una denuncia del lavoratore nel mese di " +
                                           DateTime.Now.AddMonths(-mesiRitardo).ToString("MMMM yyyy");
                    }
                    catch (Exception ex)
                    {
                        LabelErrore.Text = "Errore generico durante l'inserimento";
                    }
                }
            }
        }
    }


    /// <summary>
    /// Controlla che la data di assunzione inserita sia successiva all'ultimo mese di cui si conoscono le denunce
    /// </summary>
    /// <returns></returns>
    protected bool CheckData()
    {
        bool res = false;

        try
        {
            DateTime dataAssunzione = DateTime.Parse(TextBoxDataAssunzione.Text);
            DateTime dataLimiteTemp =
                DateTime.Now.AddDays(-DateTime.Now.Day + 1).AddMonths(-notificaBiz.MesiRitardoDati() + 1);
            DateTime dataLimite = new DateTime(dataLimiteTemp.Year, dataLimiteTemp.Month, dataLimiteTemp.Day);
            dataLimite = dataLimite.AddSeconds(-1);

            if (dataAssunzione < dataLimite)
            {
                LabelErrore.Text = "La data di assunzione non puo' essere precedente al " +
                                   dataLimite.AddSeconds(1).ToShortDateString();
            }
            else
                res = true;
        }
        catch
        {
        }

        return res;
    }

    private void AzzeraCampi()
    {
        TextBoxCognome.Text = string.Empty;
        TextBoxNome.Text = string.Empty;
        TextBoxDataNascita.Text = string.Empty;
        TextBoxComuneNascita.Text = string.Empty;
        TextBoxCF.Text = string.Empty;
        TextBoxDataAssunzione.Text = string.Empty;
    }

    protected void CustomValidatorImpresaSelezionata_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (Session["idImpresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // Controllo la correttezza del codice fiscale (primi 11 caratteri, bloccante)

        if (!string.IsNullOrEmpty(TextBoxCF.Text))
        {
            string sesso;
            if (RadioButtonListSesso.Items[0].Selected)
                sesso = "M";
            else
                sesso = "F";

            args.IsValid = bizCommon.VerificaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text.Trim().ToUpper(),
                                                                           TextBoxCognome.Text.Trim().ToUpper(),
                                                                           sesso,
                                                                           DateTime.Parse(TextBoxDataNascita.Text),
                                                                           TextBoxCF.Text.Trim().ToUpper());
        }
    }
}