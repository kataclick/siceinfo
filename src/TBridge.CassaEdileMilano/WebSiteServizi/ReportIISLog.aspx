<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportIISLog.aspx.cs" Inherits="ReportIISLog" %>

<%@ Register Src="WebControls/IISLogMenu.ascx" TagName="IISLogMenu" TagPrefix="uc2" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Report Log IIS"
        Visible="true" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerIISLog" runat="server" OnInit="ReportViewerIISLog_Init"
        ProcessingMode="Remote" width="100%" Height="490px">
    </rsweb:ReportViewer>
    </td></tr></table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:IISLogMenu ID="IISLogMenu1" runat="server" />
</asp:Content>
