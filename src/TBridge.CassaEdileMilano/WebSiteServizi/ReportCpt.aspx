<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportCpt.aspx.cs" Inherits="ReportCpt" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Report notifica"
        titolo="Notifiche" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerCpt" runat="server" ProcessingMode="Remote" ShowParameterPrompts="False"
                    Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
