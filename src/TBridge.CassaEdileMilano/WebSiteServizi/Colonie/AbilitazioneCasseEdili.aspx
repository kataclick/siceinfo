<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AbilitazioneCasseEdili.aspx.cs" Inherits="Colonie_AbilitazioneCasseEdili" %>

<%@ Register Src="../WebControls/ColonieRicercaCassaEdile.ascx" TagName="ColonieRicercaCassaEdile"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Abilitazione casse edili"
        titolo="Colonie" />
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Casse Edili abilitate"></asp:Label>
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Operazione non riuscita"
        Visible="False"></asp:Label>
    <asp:GridView ID="GridViewCasseEdiliAbilitate" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="IdCassaEdile" OnRowDeleting="GridViewCasseEdiliAbilitate_RowDeleting" Width="100%" OnPageIndexChanging="GridViewCasseEdiliAbilitate_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="IdCassaEdile" HeaderText="Codice">
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rimuovi" ShowDeleteButton="True">
                <ItemStyle Width="50px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna abilitazione presente
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonAggiungiCassaEdile" runat="server" Text="Autorizza cassa edile" OnClick="ButtonAggiungiCassaEdile_Click" />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelAggiungi" runat="server" Width="100%" Visible="False">
        <uc3:ColonieRicercaCassaEdile ID="ColonieRicercaCassaEdile1" runat="server" />
    </asp:Panel>
</asp:Content>

