﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Colonie_CandidaturaPersonaleConferma : System.Web.UI.Page
{
    private readonly ColonieBusinessEF efBiz = new ColonieBusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieRichiestePersonale);
        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdRichiesta"] != null)
            {
                Int32 idRichiesta = (Int32) Context.Items["IdRichiesta"];
                CaricaRichiesta(idRichiesta);
            }
            else
            {
                Response.Redirect("~/Colonie/CandidaturaPersonale.aspx");
            }
        }
    }

    private void CaricaRichiesta(Int32 idRichiesta)
    {
        ColoniePersonaleRichiesta richiesta = efBiz.GetPersonaleRichiesta(idRichiesta);

        LabelId.Text = richiesta.Id.ToString();
        LabelNomeCognome.Text = String.Format("{0} {1}", richiesta.Nome, richiesta.Cognome);
        LabelEmail.Text = richiesta.Email;
    }
}