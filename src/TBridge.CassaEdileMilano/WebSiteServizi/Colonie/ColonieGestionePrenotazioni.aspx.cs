using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ColonieGestionePrenotazioni : Page
{
    private const string PRENOTABILE = "Prenotabile";
    private readonly ColonieBusiness biz = new ColonieBusiness();
    private string visualizza = "TUTTI";

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestionePrenotazioni,
                                              "ColonieGestionePrenotazioni.aspx");

        if (!Page.IsPostBack)
        {
            TextBoxAnno.Text = DateTime.Now.Year.ToString();
            CaricaTipiVacanza();
        }
    }

    private void CaricaTipiVacanza()
    {
        TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

        DropDownListTipiVacanza.DataSource = tipiVacanza;
        DropDownListTipiVacanza.DataTextField = "Descrizione";
        DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
        DropDownListTipiVacanza.DataBind();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaMatrice();
        }
    }

    private void CaricaMatrice()
    {
        int anno = Int32.Parse(TextBoxAnno.Text);
        int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);
        visualizza = DropDownListVisualizza.SelectedValue;

        PanelVisualizza.Visible = true;
        PrenotazioniTurnoCollection prenotazioni = biz.GetPrenotazioniPerTurno(anno, idTipoVacanza);
        GridViewPrenotazioni.DataSource = prenotazioni;
        GridViewPrenotazioni.DataBind();
    }

    protected void GridViewPrenotazioni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PrenotazioniTurno prenotazioni = (PrenotazioniTurno) e.Row.DataItem;
            GridView gvPren = (GridView) e.Row.FindControl("GridViewPerCassa");
            Panel pTotali = (Panel) e.Row.FindControl("PanelTotali");
            Label labelPostiConcessi = (Label) e.Row.FindControl("LabelPostiConcessi");
            Label labelPostiRichiestiDaVerificare = (Label) e.Row.FindControl("LabelPostiRichiestiDaVerificare");

            gvPren.DataSource = prenotazioni.Prenotazioni;
            gvPren.DataBind();

            if (prenotazioni.Prenotazioni != null && prenotazioni.Prenotazioni.Count > 0)
            {
                pTotali.Visible = true;

                int postiPren = prenotazioni.PostiConcessi;
                labelPostiConcessi.Text = postiPren.ToString();
                if (postiPren > prenotazioni.PostiTurno)
                    labelPostiConcessi.ForeColor = Color.Red;

                labelPostiRichiestiDaVerificare.Text = prenotazioni.PostiRichiestiDaVerificare.ToString();
            }
        }
    }

    protected void GridViewPerCassa_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PrenotazioniCassa prenotazioneCassa = (PrenotazioniCassa) e.Row.DataItem;
            Label lStato = (Label) e.Row.FindControl("LabelStato");
            Label labelPostiRichiesti = (Label) e.Row.FindControl("LabelPostiRichiesti");
            Label labelPostiConcessi = (Label) e.Row.FindControl("LabelPostiConcessi");
            Button bOperazione = (Button) e.Row.FindControl("ButtonOperazione");

            if (prenotazioneCassa.Prenotazioni.Count == 1)
            {
                Prenotazione prenotazione = prenotazioneCassa.Prenotazioni[0];

                if (prenotazione.IdPrenotazione.HasValue)
                {
                    labelPostiRichiesti.Text = prenotazione.PostiRichiesti.ToString();

                    if (prenotazione.Accettata.HasValue)
                    {
                        if (!prenotazione.Accettata.Value)
                        {
                            e.Row.ForeColor = Color.Gray;
                            lStato.Text = "Rifiutata";
                            //bOperazione.Enabled = false;
                            bOperazione.Text = "Mod.";

                            if (visualizza != "NONACCETTATE" && visualizza != "TUTTE")
                                e.Row.Visible = false;
                        }
                        else
                        {
                            if (prenotazione.PostiConcessi.HasValue &&
                                prenotazione.PostiConcessi.Value != prenotazione.PostiRichiesti)
                            {
                                lStato.Text = "Variata";
                                labelPostiRichiesti.ForeColor = Color.Gray;
                            }
                            else
                                lStato.Text = "Confermata";

                            bOperazione.Text = "Mod.";
                            if (visualizza != "ACCETTATE" && visualizza != "TUTTE")
                                e.Row.Visible = false;
                        }

                        if (prenotazione.PostiConcessi.HasValue)
                            labelPostiConcessi.Text = prenotazione.PostiConcessi.ToString();
                        else if (prenotazione.Accettata.Value)
                            labelPostiConcessi.Text = prenotazione.PostiRichiesti.ToString();
                    }
                    else
                    {
                        lStato.Text = "Da verif.";
                        e.Row.ForeColor = Color.Red;
                        bOperazione.Text = "Ver.";

                        if (visualizza != "NONVERIFICATE" && visualizza != "TUTTE")
                            e.Row.Visible = false;
                    }
                }
                else
                {
                    lStato.Text = PRENOTABILE;
                }
            }
        }
    }

    protected void GridViewPerCassa_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvPren = (GridView) sender;
        int idPrenotazione = (int) gvPren.DataKeys[e.NewSelectedIndex].Value;

        CaricaPrenotazione(idPrenotazione);
    }

    private void CaricaPrenotazione(int idPrenotazione)
    {
        PanelDettagli.Visible = true;

        ViewState["IdPrenotazione"] = idPrenotazione;
        Prenotazione prenotazione = biz.GetPrenotazione(idPrenotazione);

        LabelTurno.Text = prenotazione.DescrizioneTurno;
        LabelCassaEdile.Text = prenotazione.IdCassaEdile;
        LabelPostiRichiesti.Text = prenotazione.PostiRichiesti.ToString();

        if (prenotazione.Accettata.HasValue)
        {
            CheckBoxAccettata.Checked = prenotazione.Accettata.Value;

            GestisciPostiConcessi();
        }

        TextBoxPostiConcessi.Text = prenotazione.PostiConcessi.ToString();
    }

    protected void CheckBoxAccettata_CheckedChanged(object sender, EventArgs e)
    {
        GestisciPostiConcessi();
    }

    private void GestisciPostiConcessi()
    {
        if (CheckBoxAccettata.Checked)
            TextBoxPostiConcessi.Enabled = true;
        else
        {
            TextBoxPostiConcessi.Enabled = false;
            TextBoxPostiConcessi.Text = string.Empty;
        }
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            Prenotazione prenotazione = CreaPrenotazione();

            if (biz.UpdatePrenotazione(prenotazione))
            {
                SvuotaCampi();
                PanelDettagli.Visible = false;
                LabelErroreModifica.Text = string.Empty;

                CaricaMatrice();
            }
            else
            {
                LabelErroreModifica.Text = "Errore durante il salvataggio";
            }
        }
    }

    private void SvuotaCampi()
    {
        ViewState["IdPrenotazione"] = null;

        LabelTurno.Text = string.Empty;
        LabelCassaEdile.Text = string.Empty;
        CheckBoxAccettata.Checked = false;
        LabelPostiRichiesti.Text = string.Empty;
        TextBoxPostiConcessi.Text = string.Empty;
    }

    private Prenotazione CreaPrenotazione()
    {
        Prenotazione prenotazione = new Prenotazione();

        int idPrenotazione = (int) ViewState["IdPrenotazione"];
        prenotazione.IdPrenotazione = idPrenotazione;
        prenotazione.Accettata = CheckBoxAccettata.Checked;

        if (CheckBoxAccettata.Checked)
            if (!string.IsNullOrEmpty(TextBoxPostiConcessi.Text))
                prenotazione.PostiConcessi = Int32.Parse(TextBoxPostiConcessi.Text);

        return prenotazione;
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaMatrice();
    }

    protected void GridViewPrenotazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewPrenotazioni.PageIndex = e.NewPageIndex;
        CaricaMatrice();
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        SvuotaCampi();
        PanelDettagli.Visible = false;
    }
}