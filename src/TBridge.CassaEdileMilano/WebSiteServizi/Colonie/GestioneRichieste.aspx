﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneRichieste.aspx.cs" Inherits="Colonie_GestioneRichieste" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="../WebControls/MenuColonie.ascx" tagname="MenuColonie" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/RicercaRichieste.ascx" tagname="RicercaRichieste" tagprefix="uc3" %>
<%@ Register src="WebControls/VacanzaAttivaRiassunto.ascx" tagname="VacanzaAttivaRiassunto" tagprefix="uc4" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Villaggi Vacanze" sottoTitolo="Gestione Richieste" />
    <br />
    <uc4:VacanzaAttivaRiassunto ID="VacanzaAttivaRiassunto1" runat="server" />
    <br />
    <uc3:RicercaRichieste ID="RicercaRichieste1" runat="server" />
</asp:Content>


