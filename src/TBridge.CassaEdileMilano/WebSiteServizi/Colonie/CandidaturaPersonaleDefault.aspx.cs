﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Business;

public partial class Colonie_CandidaturaPersonaleDefault : System.Web.UI.Page
{
    ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();
            DateTime adesso = DateTime.Now;

            if (vacanzaAttiva.DataInizioRichieste.HasValue
                && vacanzaAttiva.DataFineRichieste.HasValue
                && vacanzaAttiva.DataInizioRichieste.Value <= adesso
                && adesso < vacanzaAttiva.DataFineRichieste.Value.AddDays(1))
            {
                PanelAttivo.Visible = true;
                PanelNonAttivo.Visible = false;
            }
            else
            {
                PanelAttivo.Visible = false;
                PanelNonAttivo.Visible = true;
            }
        }
    }

    protected void ButtonProsegui_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Colonie/CandidaturaPersonale.aspx");
    }
}