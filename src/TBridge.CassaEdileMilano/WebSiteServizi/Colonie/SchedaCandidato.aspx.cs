﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Colonie_SchedaCandidato : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale);
        #endregion

        if (Context.Items["IdRichiesta"] != null)
        {
            Int32 idRichiesta = (Int32) Context.Items["IdRichiesta"];

            ReportViewerSchedaCandidato.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerSchedaCandidato.ServerReport.ReportPath = "/ReportColonie/ReportSchedaCandidato";

            ReportParameter[] listaParam = new ReportParameter[1];
            listaParam[0] = new ReportParameter("id", idRichiesta.ToString());
            ReportViewerSchedaCandidato.ServerReport.SetParameters(listaParam);

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            //PDF

            byte[] bytes = ReportViewerSchedaCandidato.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", idRichiesta));
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }
}