<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ColonieDettagliPerTurno.aspx.cs" Inherits="ColonieDettagliPerTurno" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Dettagli turno"
            titolo="Colonie" />
        <br />
        <asp:Label ID="LabelTurno" runat="server" Font-Bold="True"></asp:Label><br />
        <asp:GridView ID="GridViewDettaglioPerTurno" runat="server" AutoGenerateColumns="False"
            OnRowDataBound="GridViewDettaglioPerTurno_RowDataBound" Width="200px">
            <Columns>
                <asp:BoundField HeaderText="Cassa edile" />
                <asp:BoundField HeaderText="Partecipanti" />
            </Columns>
        </asp:GridView>
        <br />
        &nbsp;<asp:Button ID="ButtonChiudi" runat="server" Text="Chiudi" /></div>
    </form>
</body>
</html>
