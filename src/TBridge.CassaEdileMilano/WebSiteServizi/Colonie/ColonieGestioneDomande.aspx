<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieGestioneDomande.aspx.cs" Inherits="ColonieGestioneDomande" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione domande"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxAnno"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="ButtonVisualizza" runat="server" Text="Visualizza turni" ValidationGroup="visualizzazione"
                    OnClick="ButtonVisualizza_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;<asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Elenco turni"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Turno"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTurno" runat="server" Width="300px" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownListTurno_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="ButtonAutobus" runat="server" Enabled="False" OnClick="ButtonAutobus_Click"
                    Text="Gestisci autobus" />
            </td>
        </tr>
    </table>
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="LabelNonAssegnate" runat="server" Font-Bold="True" Text="Domande non assegnate"
                    Visible="False"></asp:Label><br />
                <asp:GridView ID="GridViewDomandeNonAssegnate" runat="server" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="Cognome,Nome" AllowPaging="True" OnPageIndexChanging="GridViewDomandeNonAssegnate_PageIndexChanging"
                    PageSize="3" OnRowDataBound="GridViewDomandeNonAssegnate_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                            HtmlEncode="False" />
                        <asp:BoundField DataField="CassaEdile" HeaderText="Provenienza" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonAssegna" runat="server" Text="Assegna" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <asp:Label ID="LabelPartecipanti" runat="server" Font-Bold="True" Text="Elenco Partecipanti turno selezionato"
                    Visible="False"></asp:Label>&nbsp;
                <asp:Panel ID="PanelFiltro" runat="server" Visible="False" Width="100%">
                    <table class="standardTable">
                        <tr>
                            <td>
                                Cognome
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCognome" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cassa Edile
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListCassaEdile" runat="server" Width="200px" AppendDataBoundItems="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Stato
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListStatoDomande" runat="server" Width="200px" AppendDataBoundItems="True">
                                    <asp:ListItem Value="TUTTE">Tutte</asp:ListItem>
                                    <asp:ListItem Value="NOANNULLATE">Non annullate</asp:ListItem>
                                    <asp:ListItem Value="ANNULLATE">Annullate</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="ButtonFiltro" runat="server" OnClick="ButtonFiltro_Click" Text="Cerca"
                                    Width="75px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataKeyNames="IdDomanda,IdTurno,Cognome,Nome" OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging"
                    OnRowDeleting="GridViewDomande_RowDeleting" AllowPaging="True" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                    OnRowDataBound="GridViewDomande_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:TemplateField HeaderText="P. hand.">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxPortatoreHandicap" runat="server" Enabled="False" />
                            </ItemTemplate>
                            <ItemStyle Width="30px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Accompagnatore" HeaderText="Acc." />
                        <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                            HtmlEncode="False" />
                        <asp:BoundField DataField="TargaAutobus" HeaderText="Autobus" />
                        <asp:BoundField DataField="AnnullamentoStringa" HeaderText="Annullata" />
                        <asp:BoundField DataField="CassaEdile" HeaderText="Provenienza" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Modifica"
                            ShowSelectButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonScheda" runat="server" CommandName="delete" Text="Scheda" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelDettagli" runat="server" Width="100%" Visible="False">
        <table class="standardTable">
            <tr>
                <td colspan="3">
                    <asp:Label ID="Label3" runat="server" Text="Dettagli" Font-Bold="True"></asp:Label>
                    <asp:Label ID="LabelDomanda" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="rigaHandicap" runat="server">
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Portatore handicap" Font-Bold="False"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxHandicap" runat="server" />
                </td>
            </tr>
            <tr id="rigaIntolleranze" runat="server">
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Intolleranze alimentari" Font-Bold="False"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxIntolleranze" runat="server" />
                </td>
            </tr>
            <tr id="rigaDivisorio" runat="server">
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Richiesta annullamento (gg/mm/aaaa)"
                        Font-Bold="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRichiestaAnnullamento" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Formato data errato"
                        ControlToValidate="TextBoxRichiestaAnnullamento" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="inserimento"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Imbarcato" Font-Bold="False"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxImbarcato" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxImbarcato_CheckedChanged" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Panel ID="PanelImbarcato" runat="server" Width="100%">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    <asp:Label ID="Label5" runat="server" Text="Autobus" Font-Bold="False"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListAutobus" runat="server" Width="300px" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelNonImbarcato" runat="server" Width="710px">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    <asp:Label ID="Label9" runat="server" Text="Motivo mancato imbarco" Font-Bold="False"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListMancatoImbarco" runat="server" Width="300px" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label13" runat="server" Text="Partenza posticipata (gg/mm/aaaa)" Font-Bold="False"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxPartenzaPosticipata" runat="server" Width="300px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Formato data errato"
                                        ControlToValidate="TextBoxPartenzaPosticipata" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                                        ValidationGroup="inserimento"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Presente in colonia" Font-Bold="False"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxPresenteColonia" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxPresenteColonia_CheckedChanged" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Rientro anticipato (gg/mm/aaaa)" Font-Bold="False"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRientroAnticipato" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Formato data errato"
                        ControlToValidate="TextBoxRientroAnticipato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="inserimento"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <asp:Button ID="ButtonAggiorna" runat="server" Text="Salva" ValidationGroup="inserimento"
                        OnClick="ButtonAggiorna_Click" Height="32px" Width="158px" Font-Size="Medium" />
                    <br />
                    <asp:Label ID="LabelErroreInserimento" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
