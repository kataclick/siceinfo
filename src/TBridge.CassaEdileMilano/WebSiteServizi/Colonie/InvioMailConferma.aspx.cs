﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using TBridge.Cemi.Type.Domain;
using System.Configuration;
using System.Net;
using TBridge.Cemi.Colonie.Business;

public partial class Colonie_InvioMailConferma : System.Web.UI.Page
{
    private const Int32 DA = 634;
    private const Int32 A = 680;
    //private const Int32 DA = 29;
    //private const Int32 A = 30;

    private ColonieBusinessEF biz = new ColonieBusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonInvia_Click(object sender, EventArgs e)
    {
        //Int32 mi = 0;

        //for (Int32 i = DA; i <= A; i++)
        //{
        //    try
        //    {
        //        ColoniePersonaleRichiesta ric = biz.GetPersonaleRichiesta(i);
        //        InviaEmailConferma(ric);
        //        mi++;
        //    }
        //    catch { }
        //}

        //LabelMessaggio.Text = String.Format("Inviate {0} mail", mi);
    }

    private void InviaEmailConferma(ColoniePersonaleRichiesta richiesta)
    {
        EmailMessageSerializzabile email = new EmailMessageSerializzabile();

        email.Destinatari = new List<EmailAddress>();
        //email.Destinatari.Add(new EmailAddress("alessio.mura@itsinfinity.com", "alessio.mura@itsinfinity.com"));
        email.Destinatari.Add(new EmailAddress(richiesta.Email, richiesta.Email));

        email.DestinatariBCC = new List<EmailAddress>();
        email.DestinatariBCC.Add(new EmailAddress("personale.colonie@cassaedilemilano.it", "personale.colonie@cassaedilemilano.it"));

        var sbPlain = new StringBuilder();
        var sbHtml = new StringBuilder();

        #region Creiamo il plaintext della mail
        sbPlain.AppendLine(String.Format("Gentile {0} {1} (richiesta n° {2}),", richiesta.Nome, richiesta.Cognome, richiesta.Id));
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("La ringraziamo per averci inviato la Sua candidatura per i Villaggi Vacanze di Cassa Edile Milano.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Confermiamo di averla ricevuta correttamente e che sarà presa in attenta considerazione nel più breve tempo possibile.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine(String.Format("Sarà contattato all’indirizzo di posta elettronica {0} qualora il Suo profilo risulti in linea con i requisiti ricercati.", richiesta.Email));
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Con i migliori saluti,");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Ufficio Personale");
        sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");

        #endregion
        email.BodyPlain = sbPlain.ToString();

        #region Creiamo l'htlm text della mail
        sbHtml.Append(String.Format("<span style=\"font-family: Calibri; font-size: 11pt\">Gentile <b>{0} {1}</b> (richiesta n° <b>{2}</b>),", richiesta.Nome, richiesta.Cognome, richiesta.Id));
        sbHtml.Append("<br /><br />");
        sbHtml.Append("La ringraziamo per averci inviato la Sua candidatura per i Villaggi Vacanze di Cassa Edile Milano.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Confermiamo di averla ricevuta correttamente e che sarà presa in attenta considerazione nel più breve tempo possibile.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append(String.Format("Sarà contattato <b>all’indirizzo di posta elettronica {0}</b> qualora il Suo profilo risulti in linea con i requisiti ricercati.", richiesta.Email));
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Con i migliori saluti,</span>");
        sbHtml.Append("<br /><br />");
        /*
        sbHtml.Append("Ufficio Personale");
        sbHtml.Append("<br />");
        sbHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
        */
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Ufficio Personale</strong></span>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Cassa Edile di Milano, Lodi, Monza e Brianza</strong><br />Via S. Luca, 6<br />20122 Milano (Mi)</span>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\">Web <a href=\"http://www.cassaedilemilano.it\">www.cassaedilemilano.it</a>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<img src=\"http://ww2.cassaedilemilano.it/Portals/0/images/Logo%20mail%20aziendale.jpg\" />"); 

        #endregion
        email.BodyHTML = sbHtml.ToString();

        email.Mittente = new EmailAddress();
        email.Mittente.Indirizzo = "personale.colonie@cassaedilemilano.it";
        email.Mittente.Nome = "Ufficio Personale";

        email.Oggetto = "Conferma ricezione richiesta di assunzione ";

        email.DataSchedulata = DateTime.Now;
        email.Priorita = MailPriority.Normal;

        string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
        string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

        var service = new EmailInfoService();
        var credentials = new NetworkCredential(emailUserName, emailPassword);
        service.Credentials = credentials;
        service.InviaEmail(email);
    }
}