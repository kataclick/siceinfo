using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using CassaEdile=TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;

public partial class Colonie_IscrizioneAltreCasseEdiliGestione : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();
    private string idCassaEdile;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieInserimentoDomandeACE,
                                              "IscrizioneAltreCasseEdili.aspx");
        ColonieRicercaDomandeACE1.OnDomandaSelected += ColonieRicercaDomandeACE1_OnDomandaSelected;

        if (GestioneUtentiBiz.IsCassaEdile())
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile cassaEdile =
            //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.CassaEdile)HttpContext.Current.User.Identity).
            //            Entity;
            CassaEdile cassaEdile =
                (CassaEdile) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idCassaEdile = cassaEdile.IdCassaEdile;
        }

        if (!Page.IsPostBack)
        {
            Vacanza vacanza = biz.GetVacanzaAttiva();
            ViewState["IdVacanzaAttiva"] = vacanza.IdVacanza.Value;

            CaricaTurni(vacanza.IdVacanza.Value);

            GridViewSituazione.DataSource = biz.GetSituazioneDomande(idCassaEdile, vacanza.IdVacanza.Value);
            GridViewSituazione.DataBind();

            if (vacanza != null)
            {
                if (biz.CassaEdileAbilitata(vacanza.IdVacanza.Value, idCassaEdile))
                {
                    DateTime ora = DateTime.Now;
                    LabelVacanzaAnno.Text = vacanza.Anno.ToString();
                    LabelVacanzaTipo.Text = vacanza.TipoVacanza.Descrizione;

                    if (vacanza.DataInizioDomandeACE.HasValue && vacanza.DataFineDomandeACE.HasValue &&
                        vacanza.DataInizioDomandeACE.Value <= ora && ora <= vacanza.DataFineDomandeACE.Value.AddDays(1))
                    {
                        PanelRicerca.Visible = true;
                        PanelNessunaVacanzaAttiva.Visible = false;
                    }
                    else
                    {
                        PanelRicerca.Visible = false;
                        PanelNessunaVacanzaAttiva.Visible = true;

                        if (vacanza.DataInizioDomandeACE.HasValue && vacanza.DataFineDomandeACE.HasValue)
                        {
                            if (vacanza.DataInizioDomandeACE > ora)
                                LabelMessaggio.Text =
                                    String.Format("Sar� possibile inserire le domande a partire dal {0}",
                                                  vacanza.DataInizioDomandeACE.Value.ToShortDateString());
                            else
                            {
                                LabelMessaggio.Text =
                                    String.Format("Il periodo per l'inserimento delle domande � scaduto il {0}",
                                                  vacanza.DataFineDomandeACE.Value.ToShortDateString());
                                PanelRicerca.Visible = true;
                                ViewState["ReadOnly"] = true;
                            }
                        }
                        else
                            LabelMessaggio.Text = "Non � ancora possibile inserire le domande";
                    }
                }
                else
                {
                    PanelAbilitata.Visible = false;
                    LabelNonAbilitata.Visible = true;
                }
            }
            else
            {
                PanelRicerca.Visible = false;
                PanelNessunaVacanzaAttiva.Visible = true;
                LabelMessaggio.Text = "Non esistono vacanze attive, non � possibile inserire le domanda.";
            }
        }

        if (ViewState["ReadOnly"] != null && ((bool) ViewState["ReadOnly"]))
        {
            ColonieRicercaDomandeACE1.ModalitaReadOnly();
        }

        if (ViewState["IdVacanzaAttiva"] != null)
        {
            ColonieRicercaDomandeACE1.IdVacanza = (int) ViewState["IdVacanzaAttiva"];
        }
    }

    private void CaricaTurni(int idVacanza)
    {
        TurnoCollection turni = biz.GetTurni(null, idVacanza, null, null, null);

        ColonieRicercaDomandeACE1.CaricaTurni(turni);
    }

    private void ColonieRicercaDomandeACE1_OnDomandaSelected(DomandaACE domanda)
    {
        ColonieDatiDomanda1.Visible = true;
        ColonieDatiDomanda1.CaricaDomanda(domanda, true, idCassaEdile);
    }

    protected void GridViewSituazione_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SituazioneACE situazione = (SituazioneACE) e.Row.DataItem;

            if (situazione.DomandeInCarico > 0)
            {
                if (situazione.PostiAccettatiRichiesti > situazione.PostiPrenotati)
                    e.Row.ForeColor = Color.Red;
            }
        }
    }
}