using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;

public partial class ColonieDettagliPerTurno : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonChiudi.Attributes.Add("onclick", "window.close(); return false;");

        if (Request.QueryString["idTurno"] != null)
        {
            int idTurno = Int32.Parse(Request.QueryString["idTurno"]);
            LabelTurno.Text = Request.QueryString["descTurno"];

            Dictionary<string, int> part = biz.GetPartecipantiTurnoPerCassa(idTurno);
            GridViewDettaglioPerTurno.DataSource = part;
            GridViewDettaglioPerTurno.DataBind();
        }
    }

    protected void GridViewDettaglioPerTurno_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            KeyValuePair<string, int> riga = (KeyValuePair<string, int>) e.Row.DataItem;
            e.Row.Cells[0].Text = riga.Key;
            e.Row.Cells[1].Text = riga.Value.ToString();
        }
    }
}