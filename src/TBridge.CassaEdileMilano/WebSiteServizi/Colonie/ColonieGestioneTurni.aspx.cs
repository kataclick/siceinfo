using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ColonieGestioneTurni : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneVacanze,
                                              "ColonieGestioneTurni.aspx");

        if (!Page.IsPostBack)
        {
            CaricaTipiVacanza();

            int idVacanza = Int32.Parse(Request.QueryString["idVacanza"]);
            Vacanza vacanza = biz.GetVacanze(null, idVacanza)[0];
            ViewState["Vacanza"] = vacanza;

            DropDownListTipiVacanza.SelectedValue = vacanza.TipoVacanza.IdTipoVacanza.ToString();
            CaricaDestinazioni(vacanza.TipoVacanza.IdTipoVacanza);
            TextBoxAnno.Text = vacanza.Anno.ToString();
        }
    }

    private void CaricaTipiVacanza()
    {
        TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

        DropDownListTipiVacanza.DataSource = tipiVacanza;
        DropDownListTipiVacanza.DataTextField = "Descrizione";
        DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
        DropDownListTipiVacanza.DataBind();

        if (DropDownListTipiVacanza.SelectedIndex != -1)
        {
            int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);
            CaricaDestinazioni(idTipoVacanza);
        }
    }

    private void CaricaDestinazioni(int idTipoVacanza)
    {
        DestinazioneCollection destinazioni = biz.GetDestinazioni(idTipoVacanza, null);
        DropDownListDestinazione.DataSource = destinazioni;
        DropDownListDestinazione.DataTextField = "DestinazioneLuogo";
        DropDownListDestinazione.DataValueField = "IdDestinazione";
        DropDownListDestinazione.DataBind();
    }

    protected void DropDownListTipiVacanza_SelectedIndexChanged(object sender, EventArgs e)
    {
        SvuotaTurni();

        if (DropDownListTipiVacanza.SelectedIndex != -1)
        {
            int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);
            CaricaDestinazioni(idTipoVacanza);
        }
    }

    protected void DropDownListDestinazione_SelectedIndexChanged(object sender, EventArgs e)
    {
        SvuotaTurni();
    }

    private void SvuotaTurni()
    {
        GridViewTurni.DataSource = null;
        GridViewTurni.DataBind();
        PanelVisualizzazione.Visible = false;
        PanelNuovo.Visible = false;
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        //resettiamo il messaggio di errore in cancellazione
        LabelRes.Text = string.Empty;

        if (DropDownListDestinazione.SelectedIndex != -1)
        {
            PanelVisualizzazione.Visible = true;
            LabelErroreVisualizzazione.Visible = false;

            CaricaTurni();
        }
        else
            LabelErroreVisualizzazione.Visible = true;
    }

    private void CaricaTurni()
    {
        if (DropDownListDestinazione.SelectedIndex != -1)
        {
            Vacanza vacanza = (Vacanza) ViewState["Vacanza"];

            int idDestinazione = Int32.Parse(DropDownListDestinazione.SelectedItem.Value);

            TurnoCollection turni = biz.GetTurni(null, vacanza.IdVacanza.Value, idDestinazione, null, null);
            GridViewTurni.DataSource = turni;
            GridViewTurni.DataBind();
        }
    }

    protected void GridViewTurni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Turno turno = (Turno) e.Row.DataItem;
            Label labelDurata = (Label) e.Row.FindControl("LabelDurata");
            labelDurata.Text = (((turno.Al - turno.Dal)).Days + 1).ToString();
        }
    }

    protected void ButtonNuovoTurno_Click(object sender, EventArgs e)
    {
        //resettiamo il messaggio di errore in cancellazione
        LabelRes.Text = string.Empty;

        PanelNuovo.Visible = true;
    }

    protected void ButtonAggiungi_Click(object sender, EventArgs e)
    {
        if (ControlloCampiServer())
        {
            Turno turno = CreaTurno();

            bool? risultato = biz.InsertTurno(turno);

            if (!risultato.HasValue)
            {
                LabelErroreInserimento.Text = "Il progressivo turno � gi� stato utilizzato";
            }
            else
            {
                if (risultato.Value)
                {
                    SvuotaCampiInserimento();
                    CaricaTurni();
                }
                else
                    LabelErroreInserimento.Text = "Errore durante l'inserimento";
            }
        }
    }

    private void SvuotaCampiInserimento()
    {
        TextBoxProgressivo.Text = string.Empty;
        TextBoxDal.Text = string.Empty;
        TextBoxAl.Text = string.Empty;
        TextBoxCostoGiornaliero.Text = string.Empty;
        TextBoxCostoGiornalieroAccompagnatore.Text = string.Empty;
        TextBoxDisponibilita.Text = string.Empty;
        LabelErroreInserimento.Text = string.Empty;
        PanelNuovo.Visible = false;
    }

    private Turno CreaTurno()
    {
        Turno turno = null;

        int idDestinazione = -1;
        int idVacanza = -1;
        int progressivo = -1;
        DateTime dal;
        DateTime al;
        int postiDisponibili = -1;
        Vacanza vacanza = (Vacanza) ViewState["Vacanza"];
        decimal costoGiornaliero;

        idVacanza = vacanza.IdVacanza.Value;
        idDestinazione = Int32.Parse(DropDownListDestinazione.SelectedItem.Value);
        progressivo = Int32.Parse(TextBoxProgressivo.Text);
        dal = DateTime.Parse(TextBoxDal.Text);
        al = DateTime.Parse(TextBoxAl.Text);
        postiDisponibili = Int32.Parse(TextBoxDisponibilita.Text);
        costoGiornaliero = Decimal.Parse(TextBoxCostoGiornaliero.Text);

        turno = new Turno(null, progressivo, dal, al, null, idDestinazione, idVacanza, postiDisponibili,
                          costoGiornaliero);
        turno.CostoGiornalieroAccompagnatore = Decimal.Parse(TextBoxCostoGiornalieroAccompagnatore.Text);

        return turno;
    }

    private bool ControlloCampiServer()
    {
        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (DropDownListDestinazione.SelectedIndex == -1)
        {
            res = false;
            errori.Append("Selezionare una destinazione" + Environment.NewLine);
        }

        int progressivo;
        if (string.IsNullOrEmpty(TextBoxProgressivo.Text) || !Int32.TryParse(TextBoxProgressivo.Text, out progressivo))
        {
            res = false;
            errori.Append("Progressivo errato" + Environment.NewLine);
        }

        DateTime dal = DateTime.Now;
        if (string.IsNullOrEmpty(TextBoxDal.Text) || !DateTime.TryParse(TextBoxDal.Text, out dal))
        {
            res = false;
            errori.Append("Formato data dal errato" + Environment.NewLine);
        }

        DateTime al = DateTime.Now;
        if (string.IsNullOrEmpty(TextBoxAl.Text) || !DateTime.TryParse(TextBoxAl.Text, out al))
        {
            res = false;
            errori.Append("Formato data al errato" + Environment.NewLine);
        }

        if (dal > al)
        {
            res = false;
            errori.Append("Periodo non valido" + Environment.NewLine);
        }

        decimal costoGiornaliero;
        if (string.IsNullOrEmpty(TextBoxCostoGiornaliero.Text) ||
            !decimal.TryParse(TextBoxCostoGiornaliero.Text, out costoGiornaliero))
        {
            res = false;
            errori.Append("Digitare un costo giornaliero o formato errato" + Environment.NewLine);
        }

        if (!res)
            LabelErroreInserimento.Text = errori.ToString();
        return res;
    }

    protected void GridViewTurni_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idTurno = (int) GridViewTurni.DataKeys[e.RowIndex].Value;

        if (biz.DeleteTurno(idTurno))
            LabelRes.Text = "Il turno � stato eliminato correttamente.";
        else
            LabelRes.Text =
                "Non � stato possibile eliminare il turno. Vi sono ancora delle domande o delle prenotazioni associate.";

        CaricaTurni();
    }
}