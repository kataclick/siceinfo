<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieMatriceRichiestaDisponibilita.aspx.cs" Inherits="ColonieMatriceRichiestaDisponibilita"
    EnableEventValidation="false" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Matrice richieste disponibilitÓ"
        titolo="Colonie" />

    <script type="text/javascript">
        //To cause postback "as" the Button
        function PostBackOnMainPage()
        {
            <%=Page.GetPostBackEventReference(ButtonAggiorna)%>
        }
    </script>

    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxAnno"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Visualizza" ValidationGroup="visualizzazione" />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonAggiorna" runat="server" Text="Aggiorna" OnClick="ButtonVisualizza_Click"
        Visible="false" /><br />
    <asp:GridView ID="GridViewMatrice" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewMatrice_RowDataBound"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="StringaDestinazione" HeaderText="Destinazione">
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:GridView ID="GridViewTurni" runat="server" AutoGenerateColumns="False" DataKeyNames="IdTurno"
                        OnRowDataBound="GridViewTurni_RowDataBound" Width="620px">
                        <Columns>
                            <asp:TemplateField HeaderText="Progr.">
                                <ItemTemplate>
                                    <asp:Label ID="LabelProgressivo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dal">
                                <ItemTemplate>
                                    <asp:Label ID="LabelDal" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Al">
                                <ItemTemplate>
                                    <asp:Label ID="LabelAl" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PostiDisponibili" HeaderText="Posti disponibili">
                                <ItemStyle Width="30px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Posti occupati">
                                <ItemTemplate>
                                    <asp:Label ID="LabelPostiOccupati" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="ButtonDettagli" runat="server" CommandName="dettagliProvenienza"
                                        Text="Per prov." />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="ButtonModifica" runat="server" Text="Modifica" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="ButtonCheckList" runat="server" OnClick="ButtonCheckList_Click" Text="Check List" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ItemTemplate>
                <ItemStyle Width="620px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessun turno disponibile
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
</asp:Content>
