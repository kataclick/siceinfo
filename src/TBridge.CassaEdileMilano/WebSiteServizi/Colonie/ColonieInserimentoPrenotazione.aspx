<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieInserimentoPrenotazione.aspx.cs" Inherits="ColonieInserimentoPrenotazione" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento prenotazione"
        titolo="Villaggi vacanza" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxAnno"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Visualizza" ValidationGroup="visualizzazione" />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelEstremiVacanza" runat="server"></asp:Label><br />
    <asp:Label ID="LabelNonAttiva" runat="server"></asp:Label><br />
    <asp:Panel ID="PanelDettagli" runat="server" Visible="False" Width="100%">
        <asp:GridView ID="GridViewPrenotazioni" runat="server" AutoGenerateColumns="False"
            DataKeyNames="IdTurno,IdPrenotazione" OnRowDataBound="GridViewPrenotazioni_RowDataBound"
            Width="100%">
            <Columns>
                <asp:TemplateField HeaderText="Stato">
                    <ItemTemplate>
                        <asp:Label ID="LabelStato" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DescrizioneTurno" HeaderText="Turno" />
                <asp:BoundField DataField="StringaDestinazione" HeaderText="Destinazione" />
                <asp:BoundField DataField="DalTurno" HeaderText="Dal" />
                <asp:BoundField DataField="AlTurno" HeaderText="Al" />
                <asp:TemplateField HeaderText="Posti richiesti">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBoxPostiRichiesti" runat="server" Width="50px" ValidationGroup="postiPrenotati"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxPostiRichiesti"
                            ErrorMessage="*" ValidationExpression="^\d+$" ValidationGroup="postiPrenotati"></asp:RegularExpressionValidator>
                    </ItemTemplate>
                    <ItemStyle Width="100px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Posti confermati">
                    <ItemTemplate>
                        <asp:Label ID="LabelPostiConcessi" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                Nessuna prenotazione possibile
            </EmptyDataTemplate>
        </asp:GridView>
        Digitare il numero di posti richiesti nelle omonime caselle ("Posti richiesti")
        e cliccare sul tasto "Prenota".
        <br />
        Una volta premuto il tasto "Prenota" non sar� pi� possibile modificare il dato inserito.
        <br />
        L'indicazione del numero di partecipanti riferito a ciascun turno pu� essere effettuata
        in momenti diversi entro la data di scadenza sopra indicata.
        <br />
        <asp:Button ID="ButtonSalva" runat="server" OnClick="ButtonSalva_Click" Text="Prenota"
            Enabled="False" Width="131px" ValidationGroup="postiPrenotati" /><asp:Label ID="LabelErroreAgg"
                runat="server" ForeColor="Red"></asp:Label></asp:Panel>
    <br />
    &nbsp;
    <br />
</asp:Content>
