using System;
using System.Web.UI;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ColonieSchedaBambino : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieSchedaPartecipante,
                                              "ColonieSchedaBambibno.aspx");

        if (Request.QueryString["idDomanda"] != null && Request.QueryString["idTurno"] != null)
        {
            int idDomanda = Int32.Parse(Request.QueryString["idDomanda"]);
            int idTurno = Int32.Parse(Request.QueryString["idTurno"]);

            CaricaSchedaBambino(idDomanda, idTurno);
        }
    }

    private void CaricaSchedaBambino(int idDomanda, int idTurno)
    {
        SchedaBambinoCollection schedeBambino = biz.GetSchedaBambino(idDomanda, idTurno);
        SchedaBambino scheda = null;

        if (schedeBambino != null && schedeBambino.Count != 0)
            scheda = schedeBambino[0];

        // Dati partecipante
        LabelCognomeP.Text = scheda.Partecipante.Cognome;
        LabelNomeP.Text = scheda.Partecipante.Nome;
        LabelSessoP.Text = scheda.Partecipante.Sesso;
        if (scheda.Partecipante.DataNascita.HasValue)
            LabelDataNascitaP.Text = scheda.Partecipante.DataNascita.Value.ToShortDateString();
        LabelTesseraSanitaria.Text = scheda.Partecipante.NumeroTesseraSanitaria;
        LabelIndirizzoP.Text = scheda.Lavoratore.IndirizzoDenominazione;
        LabelCAPP.Text = scheda.Lavoratore.IndirizzoCAP;
        LabelComuneP.Text = scheda.Lavoratore.IndirizzoComune;
        LabelProvinciaP.Text = scheda.Lavoratore.IndirizzoProvincia;
        CheckBoxHandicap.Checked = scheda.Partecipante.PortatoreHandicap;
        CheckBoxIntolleranze.Checked = scheda.Partecipante.IntolleranzeAlimentari;
        LabelDieta.Text = scheda.Partecipante.Dieta;
        LabelAllergie.Text = scheda.Partecipante.Allergie;
        LabelProtesi.Text = scheda.Partecipante.ProtesiAusili;
        LabelTerapie.Text = scheda.Partecipante.TerapieInCorso;

        // Parente partecipante
        LabelParenteNominativo.Text = scheda.CognomeParente + " " + scheda.NomeParente;
        LabelParenteCorredo.Text = scheda.NumeroCorredoParente.ToString();

        //Dati lavoratore
        if (String.IsNullOrEmpty(scheda.Lavoratore.IdCassaEdile))
        {
            LabelIdLavoratore.Text = scheda.Lavoratore.IdLavoratore.ToString();
        }
        LabelCognomeL.Text = scheda.Lavoratore.Cognome;
        LabelNomeL.Text = scheda.Lavoratore.Nome;
        LabelIndirizzoL.Text = scheda.Lavoratore.IndirizzoDenominazione;
        LabelCAPL.Text = scheda.Lavoratore.IndirizzoCAP;
        LabelComuneL.Text = scheda.Lavoratore.IndirizzoComune;
        LabelProvinciaL.Text = scheda.Lavoratore.IndirizzoProvincia;
        LabelTelefonoL.Text = scheda.Lavoratore.Telefono;
        LabelCellulareL.Text = scheda.Lavoratore.Cellulare;
        LabelCassa.Text = scheda.Lavoratore.IdCassaEdile;

        // Accompagnatore
        if (scheda.Accompagnatore != null)
        {
            LabelAccompagnatoreCognome.Text = scheda.Accompagnatore.Cognome;
            LabelAccompagnatoreNome.Text = scheda.Accompagnatore.Nome;
            LabelAccompagnatoreDataNascita.Text = scheda.Accompagnatore.DataNascita.ToShortDateString();
            LabelAccompagnatoreSesso.Text = scheda.Accompagnatore.Sesso.ToString();
        }

        // Altro
        LabelNumeroCorredo.Text = scheda.NumeroCorredo.ToString();
        LabelLocalita.Text = scheda.TipoDestinazione;
        LabelTurno.Text = scheda.ProgressivoTurno.ToString();
        LabelAnno.Text = scheda.Anno.ToString();
        LabelTipoVacanza.Text = scheda.TipoVacanza;
    }
}