﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CandidaturaPersonaleDefault.aspx.cs" Inherits="Colonie_CandidaturaPersonaleDefault" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Villaggi Vacanze" sottoTitolo="Candidatura Personale" />
    <br />
    <asp:Panel
        ID="PanelAttivo"
        runat="server"
        Visible="true">
        Benvenuto nella sezione Villaggi Vacanze
        <br />
        <p>
            Tra le prestazioni erogate da Cassa Edile di Milano rientra il soggiorno estivo gratuito dei figli dei lavoratori aventi diritto di età compresa tra i 5 anni e i 15 anni, aperto anche ai figli dei lavoratori iscritti presso altre Casse.<br />
			I tre turni, della durata di 14 giorni ciascuno, iniziano alla fine di giugno e terminano entro la prima metà del mese di agosto.
        </p>
        N° 100 posizioni aperte per i profili di seguito elencati:
        <ul>
            <li>
                <b>Educatori</b>
                <br />
                Personale dedicato all’organizzazione e gestione delle attività ricreative. Il profilo si suddivide tra “educatori per adolescenti” che si occuperanno dei ragazzi tra i 12 e i 15 anni ed “educatori per bambini” che seguiranno i bimbi di età compresa tra i 5 e i 11 anni.
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di studio: diploma di maturità rilasciato da un istituto secondario superiore o diploma di laurea (preferibilmente nel settore socio-pedagogico);
                    </li>
                    <li>
                        Titolo preferenziale: attività di insegnamento, animazione, accompagnamento bambini/ragazzi e la conoscenza di una o più lingue straniere, esperienze in società sportive agonistiche (ad esempio, pallavolo, pallacanestro, calcio, danza, ecc.) o diploma ISEF o titolo di laurea in scienze motorie.
                    </li>
                </ol>
            </li>
            <li>
                <b>Assistenti per Diversamente Abili</b>
                <br />
                L’educatore di sostegno è specializzato nell'assistenza e nella cura di ospiti diversamente abili o appartenenti a categorie di disagio sociale, culturale o familiare, favorendone l’integrazione nei gruppi. 
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di studio: diploma di maturità rilasciato da un istituto secondario superiore o diploma di laurea (preferibilmente nel settore socio-pedagogico); 
                    </li>
                    <li>
                        Titolo preferenziale: percorsi di formazione per il conseguimento della specializzazione per le attività di sostegno didattico agli alunni con disabilità.
                    </li>
                </ol>
            </li>
            <li>
                <b>Bagnini</b>
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di studio: scuola dell&#39;obbligo;
                    </li>
                    <li>
                        Con brevetto di salvataggio in mare aperto e piscina (M.I.P.).</li>
                </ol>
            </li>
            <li>
                <b>Infermieri Professionali</b>
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di Studio: infermiere professionale o laurea in scienze infermieristiche.
                    </li>
                </ol>
            </li>
            <li>
                <b>Vigilatori/trici Notturni/e</b>
                <br />
                <ol>
                    <li>
                        Età minima: 30 anni;
                    </li>
                    <li>
                        Titolo di Studio: scuola dell&#39;obbligo.
                    </li>
                </ol>
            </li>
            <li>
                <b>Cuochi</b>
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di Studio: diploma alberghiero;
                    </li>
                    <li>
                        esperienza maturata nella ristorazione.
                    </li>
                </ol>
            </li>
            <li>
                <b>Aiuto Cuochi</b>
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di Studio: scuola dell&#39;obbligo.</li>
                </ol>
            </li>
            <li>
                <b>Inservienti di Cucina</b>
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di Studio: scuola dell&#39;obbligo.</li>
                </ol>
            </li>
            <li>
                <b>Inservienti</b>
                <br />
                <ol>
                    <li>
                        Età minima: 18 anni;
                    </li>
                    <li>
                        Titolo di Studio: scuola dell&#39;obbligo.
                    </li>
                </ol>
            </li>
        </ul>
        <br />
        <asp:Button
            ID="ButtonProsegui"
            runat="server"
            Text="Comunica i tuoi dati"
            Width="200px" onclick="ButtonProsegui_Click" />
    </asp:Panel>

    <asp:Panel
        ID="PanelNonAttivo"
        runat="server"
        Visible="false">
        La candidatura per i Villaggi Vacanze di Cassa Edile Milano, Lodi, Monza e Brianza <b>non è al momento attiva</b>.
    </asp:Panel>
</asp:Content>


