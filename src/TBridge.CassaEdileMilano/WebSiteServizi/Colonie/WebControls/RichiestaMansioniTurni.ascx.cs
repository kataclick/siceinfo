﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Collections;

public partial class Colonie_WebControls_RichiestaMansioniTurni : System.Web.UI.UserControl
{
    private readonly ColonieBusinessEF efBiz = new ColonieBusinessEF();
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaMansioni();
            CaricaTurni();
        }
    }

    private void CaricaTurni()
    {
        Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();
        TurnoCollection turni = biz.GetTurni(null, vacanzaAttiva.IdVacanza, null, null, null);

        Presenter.CaricaElementiInCheckBoxList(
            CheckBoxListTurni,
            turni,
            "DescrizioneTurnoEstesa",
            "IdTurno");
    }

    private void CaricaMansioni()
    {
        List<ColoniePersonaleMansione> mansioni = efBiz.GetPersonaleMansioni();

        Presenter.CaricaElementiInCheckBoxList(
            CheckBoxListMansioni,
            mansioni,
            "Descrizione",
            "Id");
    }

    public void GetMansioniTurni(ColoniePersonaleRichiesta richiesta)
    {
        // Mansioni
        foreach (ListItem li in CheckBoxListMansioni.Items)
        {
            if (li.Selected)
            {
                ColoniePersonaleMansione mansione = new ColoniePersonaleMansione()
                {
                    Id = Int32.Parse(li.Value),
                    Descrizione = li.Text
                };
                richiesta.ColoniePersonaleMansioni.Add(mansione);
            }
        }

        // Turni
        foreach (ListItem li in CheckBoxListTurni.Items)
        {
            if (li.Selected)
            {
                ColonieTurno turno = new ColonieTurno()
                {
                    Id = Int32.Parse(li.Value)
                };
                richiesta.ColonieTurni.Add(turno);
            }
        }
    }

    #region Custom Validators
    protected void CustomValidatorMansioni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        foreach (ListItem li in CheckBoxListMansioni.Items)
        {
            if (li.Selected)
            {
                args.IsValid = true;
                break;
            }
        }
    }

    protected void CustomValidatorTurni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        foreach (ListItem li in CheckBoxListTurni.Items)
        {
            if (li.Selected)
            {
                args.IsValid = true;
                break;
            }
        }
    }
    #endregion
}