﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Enums;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using System.Configuration;
using System.Net;
using TBridge.Cemi.Business;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class Colonie_WebControls_RiassuntoRichiesta : System.Web.UI.UserControl
{
    private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();
    protected String PostBackString;

    protected void Page_Init(object sender, EventArgs e)
    {
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, "Refresh");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        TextBoxEmail.Attributes.Add("onkeypress", "emailKeyPressed();");
        
        if (!Page.IsPostBack)
        {
        }
        else
        {
            String eventArgs = Request["__EVENTARGUMENT"];
            if (eventArgs == "Refresh")
            {
                Refresh();
            }
        }
    }

    private void Refresh()
    {
        Int32 idRichiesta = (Int32) ViewState["IdRichiesta"];
        ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);
        CaricaRichiesta(richiesta);
    }

    public void CaricaRichiesta(ColoniePersonaleRichiesta richiesta)
    {
        //string comando = String.Format(
        //            "openRadWindowRespingi({0}); return false;",
        //            richiesta.Id);

        //ButtonRespingi.Attributes.Add("OnClick", comando);

        string comando = String.Format(
                    "openRadWindowNote({0}, '{1}'); return false;",
                    richiesta.Id,
                    richiesta.RespintaNote);

        ButtonNote.Attributes.Add("OnClick", comando);

        if (richiesta == null)
        {
            throw new ArgumentNullException("richiesta");
        }

        LabelStato.Text = bizEF.TraduciStato(bizEF.GeneraStato(richiesta));

        if (!String.IsNullOrEmpty(richiesta.RespintaNote))
        {
            LabelRespintaNote.Text = richiesta.RespintaNote;
        }
        else
        {
            LabelRespintaNote.Text = "Nessuna";
        }

        ViewState["IdRichiesta"] = richiesta.Id;

        LabelCognomeNome.Text = String.Format("{0} {1}", richiesta.Cognome, richiesta.Nome);
        LabelSesso.Text = richiesta.Sesso;
        LabelCodiceFiscale.Text = richiesta.CodiceFiscale;
        LabelNascita.Text = String.Format("{0} {1} ({2})", richiesta.DataNascita.ToString("dd/MM/yyyy"), richiesta.LuogoNascitaNav.Comune, richiesta.ProvinciaNascita);
        LabelComunitario.Text = richiesta.Comunitario ? "Sì" : "No";
        LabelTelefono.Text = richiesta.Telefono;
        //LabelEmail.Text = richiesta.Email;
        TextBoxEmail.Text = richiesta.Email;
        LabelTitoloStudio.Text = richiesta.ColoniePersonaleTitoloStudio.Descrizione;
        LabelNote.Text = richiesta.Note;
        LabelEsperienzaCEMI.Text = richiesta.EsperienzaColonie ? "Sì" : "No";
        LabelResidenza.Text = String.Format("{0} {1}<br />{2} ({3})<br />{4}", richiesta.ResidenzaIndirizzo, richiesta.ResidenzaCivico, richiesta.ResidenzaComuneNav.Comune, richiesta.ResidenzaProvincia, richiesta.ResidenzaCap);
        if (!String.IsNullOrEmpty(richiesta.DomicilioIndirizzo))
        {
            LabelDomicilio.Text = String.Format("{0} {1}<br />{2} ({3})<br />{4}", richiesta.DomicilioIndirizzo, richiesta.DomicilioCivico, richiesta.DomicilioComuneNav.Comune, richiesta.DomicilioProvincia, richiesta.DomicilioCap);
        }

        RadBinaryImageFoto.DataValue = richiesta.Fotografia;
        if (richiesta.Curriculum != null)
        {
            ImageButtonCurriculum.Enabled = true;
            ImageButtonCurriculum.ImageUrl = "~/images/pdf24.png";
        }

        if (richiesta.InviatoRifiuto.HasValue)
        {
            LabelInviatoRifiuto.Text = String.Format("Inviato il: {0}", richiesta.InviatoRifiuto.Value.ToString("dd/MM/yyyy"));
        }

        Presenter.CaricaElementiInBulletedList(
            BulletedListMansioni,
            richiesta.ColoniePersonaleMansioni,
            "Descrizione",
            "Id");

        Presenter.CaricaElementiInBulletedList(
            BulletedListTurni,
            richiesta.ColonieTurni,
            "Descrizione",
            "Id");

        Presenter.CaricaElementiInBulletedList(
            BulletedListEsperienze,
            richiesta.ColoniePersonaleRichiesteEsperienze,
            "Descrizione",
            "Id");

        ColoniePersonaleColloquio colloquio = null;
        if (richiesta.ColoniePersonaleColloquio.Count == 1)
        {
            colloquio = richiesta.ColoniePersonaleColloquio.First();
        }
        RiassuntoColloquio1.CaricaColloquio(richiesta.Id, colloquio);

        ColoniePersonaleProposta proposta = null;
        if (richiesta.ColoniePersonaleProposte.Count == 1)
        {
            proposta = richiesta.ColoniePersonaleProposte.First();
        }
        RiassuntoProposta1.CaricaProposta(richiesta.Id, proposta);

        StatoRichiesta stato = bizEF.GeneraStato(richiesta);
        switch (stato)
        {
            case StatoRichiesta.DAGESTIRE:
            case StatoRichiesta.EFFETTUATAPROPOSTA:
            case StatoRichiesta.FISSATOCOLLOQUIO:
            case StatoRichiesta.VALUTATO:
                ButtonRespingi.Enabled = true;
                break;
            default:
                ButtonRespingi.Enabled = false;
                break;
        }

        List<PrecedenteCandidatura> precCand = bizEF.GetPrecedentiCandidature(richiesta.CodiceFiscale, richiesta.ColonieVacanza.Anno);
        if (precCand != null && precCand.Count > 0)
        {
            LabelNessunaCandidaturaPrecedente.Visible = false;
            BulletedListPrecedentiCandidature.Visible = true;
            Presenter.CaricaElementiInBulletedList(
                BulletedListPrecedentiCandidature,
                precCand,
                "",
                "");
        }
        else
        {
            LabelNessunaCandidaturaPrecedente.Visible = true;
            BulletedListPrecedentiCandidature.Visible = false;
        }

        if (richiesta.Respinta)
        {
            RiassuntoColloquio1.DisabilitaTutto();
            RiassuntoProposta1.DisabilitaTutto();

            ButtonInviaRifiuto.Enabled = true;
        }
    }

    protected void ImageButtonCurriculum_Click(object sender, ImageClickEventArgs e)
    {
        if (ViewState["IdRichiesta"] != null)
        {
            Int32 idRichiesta = (Int32) ViewState["IdRichiesta"];
            String estensioneCurriculum;
            byte[] curriculum = bizEF.GetCurriculumRichiesta(idRichiesta, out estensioneCurriculum);

            //Set the appropriate ContentType.
            switch(estensioneCurriculum)
            {
                case ".PDF":
                    Response.AddHeader("content-disposition", "attachment; filename=curriculum.pdf");
                    Response.ContentType = "application/pdf";
                    break;
                case ".DOC":
                    Response.AddHeader("content-disposition", "attachment; filename=curriculum.doc");
                    Response.ContentType = "application/word";
                    break;
                case ".DOCX":
                    Response.AddHeader("content-disposition", "attachment; filename=curriculum.docx");
                    Response.ContentType = "application/word";
                    break;
            }
            //Write the file directly to the HTTP content output stream.
            Response.BinaryWrite(curriculum);
            Response.Flush();
            Response.End();
        }
    }

    protected void ImageButtonScheda_Click(object sender, ImageClickEventArgs e)
    {
        if (ViewState["IdRichiesta"] != null)
        {
            Int32 idRichiesta = (Int32) ViewState["IdRichiesta"];
            Context.Items["IdRichiesta"] = idRichiesta;
            Server.Transfer("~/Colonie/SchedaCandidato.aspx");
        }
    }

    protected void ButtonRespingi_Click(object sender, EventArgs e)
    {
        if (ViewState["IdRichiesta"] != null)
        {
            Int32 idRichiesta = (Int32) ViewState["IdRichiesta"];
            bizEF.RespingiRichiesta(idRichiesta);

            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);
            CaricaRichiesta(richiesta);
        }
    }

    protected void ButtonInviaRifiuto_Click(object sender, EventArgs e)
    {
        if (ViewState["IdRichiesta"] != null)
        {
            Int32 idRichiesta = (Int32) ViewState["IdRichiesta"];
            InviaEmailRifiuto(idRichiesta);
            bizEF.UpdateRichiestaRifiutoInvio(idRichiesta);

            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);
            CaricaRichiesta(richiesta);
        }
    }

    private void InviaEmailRifiuto(int idRichiesta)
    {
        ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);

        EmailMessageSerializzabile email = new EmailMessageSerializzabile();

        email.Destinatari = new List<EmailAddress>();
        
        email.Destinatari.Add(new EmailAddress(richiesta.Email, richiesta.Email));
        //email.Destinatari.Add(new EmailAddress("alessio.mura@itsinfinity.com", "alessio.mura@itsinfinity.com"));

        email.DestinatariBCC = new List<EmailAddress>();
        if (!Common.Sviluppo)
        {
            email.DestinatariBCC.Add(new EmailAddress("personale.colonie@cassaedilemilano.it", "personale.colonie@cassaedilemilano.it"));
        }

        var sbPlain = new StringBuilder();
        var sbHtml = new StringBuilder();

        #region Creiamo il plaintext della mail
        sbPlain.AppendLine(String.Format("Gentile {0} {1} (richiesta n° {2}),", richiesta.Nome, richiesta.Cognome, richiesta.Id));
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Con riferimento alla Sua candidatura, abbiamo attentamente valutato il curriculum vitae inviatoci e le esperienze lavorative maturate negli anni precedenti presso i Villaggi Vacanze del nostro Ente.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Pur avendo apprezzato il suo profilo e la sua preparazione, al momento non ci è possibile prospettarle un inserimento nel nostro organico che sia di reciproca soddisfazione.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("La ringraziamo comunque per l’interesse dimostrato verso Cassa Edile di Milano e le porgiamo i nostri migliori auguri per il suo futuro professionale.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Cordiali saluti.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Ufficio Personale");
        sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");

        #endregion
        email.BodyPlain = sbPlain.ToString();

        #region Creiamo l'htlm text della mail
        sbHtml.Append(String.Format("<span style=\"font-family: Calibri; font-size: 11pt\">Gentile <b>{0} {1}</b> (richiesta n° {2}),", richiesta.Nome, richiesta.Cognome, richiesta.Id));
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Con riferimento alla Sua candidatura, abbiamo attentamente valutato il curriculum vitae inviatoci e le esperienze lavorative maturate negli anni precedenti presso i Villaggi Vacanze del nostro Ente.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Pur avendo apprezzato il suo profilo e la sua preparazione, al momento non ci è possibile prospettarle un inserimento nel nostro organico che sia di reciproca soddisfazione.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("La ringraziamo comunque per l’interesse dimostrato verso Cassa Edile di Milano e le porgiamo i nostri migliori auguri per il suo futuro professionale.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Con i migliori saluti,</span>");
        sbHtml.Append("<br /><br />");
        /*
        sbHtml.Append("Ufficio Personale");
        sbHtml.Append("<br />");
        sbHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
        */
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Ufficio Personale</strong></span>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Cassa Edile di Milano, Lodi, Monza e Brianza</strong><br />Via S. Luca, 6<br />20122 Milano (Mi)</span>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\">Web <a href=\"http://www.cassaedilemilano.it\">www.cassaedilemilano.it</a>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<img src=\"http://ww2.cassaedilemilano.it/Portals/0/images/Logo%20mail%20aziendale.jpg\" />"); 


        #endregion
        email.BodyHTML = sbHtml.ToString();

        email.Mittente = new EmailAddress();
        email.Mittente.Indirizzo = "personale.colonie@cassaedilemilano.it";
        email.Mittente.Nome = "Ufficio Personale";

        email.Oggetto = String.Format("Candidatura soggiorni estivi (Prot. Num. {0})", richiesta.Id);

        email.DataSchedulata = DateTime.Now;
        email.Priorita = MailPriority.Normal;

        String emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
        String emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

        var service = new EmailInfoService();
        var credentials = new NetworkCredential(emailUserName, emailPassword);
        service.Credentials = credentials;
        service.InviaEmail(email);
    }

    protected void ButtonSalvaEmail_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["IdRichiesta"] != null)
            {
                String email = TextBoxEmail.Text;
                Int32 idRichiesta = (Int32) ViewState["IdRichiesta"];

                bizEF.UpdateRichiestaEmail(idRichiesta, email);
                Refresh();
            }
        }
    }
}