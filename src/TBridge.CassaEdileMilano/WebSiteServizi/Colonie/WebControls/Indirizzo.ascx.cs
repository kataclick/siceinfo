﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;

public partial class Colonie_WebControls_Indirizzo : System.Web.UI.UserControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RequiredFieldValidatorIndirizzo.ValidationGroup = this.ID;
            RequiredFieldValidatorProvincia.ValidationGroup = this.ID;
            RequiredFieldValidatorComune.ValidationGroup = this.ID;
            RequiredFieldValidatorCap.ValidationGroup = this.ID;
            RegularExpressionValidatorCap.ValidationGroup = this.ID;
        }
    }

    public void GetIndirizzo(out String indirizzo, out String provincia, out String comune, out String civico, out String cap)
    {
        indirizzo = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text);
        provincia = RadComboBoxProvincia.SelectedValue;
        comune = RadComboBoxComune.SelectedValue;
        civico = Presenter.NormalizzaCampoTesto(RadTextBoxCivico.Text);
        cap = RadTextBoxCap.Text;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Presenter.CaricaProvinceConElementoVuoto(RadComboBoxProvincia);
        }
    }

    protected void RadComboBoxProvincia_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBoxComune.ClearSelection();
        Presenter.CaricaComuni(RadComboBoxComune, RadComboBoxProvincia.SelectedValue);
    }
}