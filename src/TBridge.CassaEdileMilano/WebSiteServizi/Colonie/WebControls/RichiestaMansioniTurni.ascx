﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RichiestaMansioniTurni.ascx.cs" Inherits="Colonie_WebControls_RichiestaMansioniTurni" %>

<table class="filledtable">
    <tr>
        <td>
            Mansioni e Periodi
        </td>
    </tr>
</table>
<table class="borderedTable">
    <colgroup>
        <col width="200px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Mansioni
            <br />
            <small>
                (selezionare tutte le mansioni di interesse)
            </small>
        </td>
        <td>
            <asp:CheckBoxList
                ID="CheckBoxListMansioni"
                runat="server">
            </asp:CheckBoxList>
            <asp:CustomValidator
                ID="CustomValidatorMansioni"
                runat="server"
                ValidationGroup="stop"
                ErrorMessage="Selezionare almeno una mansione"
                ForeColor="Red" 
                onservervalidate="CustomValidatorMansioni_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            Turni
            <br />
            <small>
                (le date potrebbero subire variazioni in base alla mansione scelta)
            </small>
        </td>
        <td>
            <asp:CheckBoxList
                ID="CheckBoxListTurni"
                runat="server">
            </asp:CheckBoxList>
            <asp:CustomValidator
                ID="CustomValidatorTurni"
                runat="server"
                ValidationGroup="stop"
                ErrorMessage="Selezionare almeno un turno"
                ForeColor="Red" onservervalidate="CustomValidatorTurni_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>