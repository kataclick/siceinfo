﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;

public partial class Colonie_WebControls_RichiestaEsperienze : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Creo tre righe sul RadGrid
            List<Int32> lista = new List<Int32>();
            lista.Add(1);
            lista.Add(2);
            lista.Add(3);

            Presenter.CaricaElementiInGridView(
                RadGridEsperienze,
                lista);
        }
    }

    public void GetEsperienze(ColoniePersonaleRichiesta richiesta)
    {
        richiesta.EsperienzaColonie = RadioButtonSi.Checked;

        foreach (GridDataItem item in RadGridEsperienze.MasterTableView.Items)
        {
            RadTextBox radTBImp = (RadTextBox) item.FindControl("RadTextBoxImpresa");
            RadDatePicker radDPDal = (RadDatePicker) item.FindControl("RadDatePickerDal");
            RadDatePicker radDPAl = (RadDatePicker) item.FindControl("RadDatePickerAl");
            RadTextBox radTBMan = (RadTextBox) item.FindControl("RadTextBoxMansione");

            if (!String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(radTBImp.Text)))
            {
                ColoniePersonaleRichiesteEsperienza esperienza = new ColoniePersonaleRichiesteEsperienza();
                richiesta.ColoniePersonaleRichiesteEsperienze.Add(esperienza);

                esperienza.Impresa = Presenter.NormalizzaCampoTesto(radTBImp.Text);
                esperienza.Dal = radDPDal.SelectedDate.Value;
                if (radDPAl.SelectedDate.HasValue)
                {
                    esperienza.Al = radDPAl.SelectedDate.Value;
                }
                esperienza.Mansione = Presenter.NormalizzaCampoTesto(radTBMan.Text);
            }
        }
    }

    protected void RadGridEsperienze_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Label lContatore = (Label) e.Item.FindControl("LabelContatore");
            CustomValidator cvTuttiDati = (CustomValidator) e.Item.FindControl("CustomValidatorTuttiDati");
            CustomValidator cvDate = (CustomValidator) e.Item.FindControl("CustomValidatorOrdineDate");

            Int32 contatore = e.Item.ItemIndex + 1;
            
            lContatore.Text = String.Format("{0}.", contatore);
            cvTuttiDati.ErrorMessage = String.Format("Indicare tutti i dati dell'esperienza {0}", contatore);
            cvDate.ErrorMessage = String.Format("L'inizio dell'esperienza {0} deve essere precedente alla fine", contatore);
        }
    }

    #region Custom Validators
    protected void CustomValidatorTuttiDati_ServerValidate(object source, ServerValidateEventArgs args)
    {
        RadTextBox radTBImp = (RadTextBox) ((CustomValidator) source).FindControl("RadTextBoxImpresa");
        RadDatePicker radDPDal = (RadDatePicker) ((CustomValidator) source).FindControl("RadDatePickerDal");
        RadDatePicker radDPAl = (RadDatePicker) ((CustomValidator) source).FindControl("RadDatePickerAl");
        RadTextBox radTBMan = (RadTextBox) ((CustomValidator) source).FindControl("RadTextBoxMansione");

        if (String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(radTBImp.Text))
            || !radDPDal.SelectedDate.HasValue
            || String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(radTBMan.Text)))
        {
            if (String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(radTBImp.Text))
                && !radDPDal.SelectedDate.HasValue
                && String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(radTBMan.Text)))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorOrdineDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        RadDatePicker radDPDal = (RadDatePicker) ((CustomValidator) source).FindControl("RadDatePickerDal");
        RadDatePicker radDPAl = (RadDatePicker) ((CustomValidator) source).FindControl("RadDatePickerAl");

        if (radDPDal.SelectedDate.HasValue && radDPAl.SelectedDate.HasValue
            && radDPDal.SelectedDate.Value > radDPAl.SelectedDate.Value)
        {
            args.IsValid = false;
        }
    }
    #endregion
}