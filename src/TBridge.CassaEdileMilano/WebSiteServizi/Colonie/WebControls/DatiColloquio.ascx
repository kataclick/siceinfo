﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiColloquio.ascx.cs" Inherits="Colonie_WebControls_DatiColloquio" %>

<style type="text/css">
    .primaColonnaColloquio
    {
        width: 130px;
    }
</style>

<table class="standardTable">
    <tr>
        <td colspan="2">
            <table class="borderedTable">
                <tr>
                    <td class="primaColonnaColloquio">
                        Mansioni richieste
                    </td>
                    <td colspan="2">
                        <asp:BulletedList
                            ID="BulletedListMansioniRichieste"
                            runat="server">
                        </asp:BulletedList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
            Data e Ora<b>*</b>:
        </td>
        <td>
            <telerik:RadDateTimePicker
                ID="RadDateTimePickerData"
                runat="server"
                Width="200px">
            </telerik:RadDateTimePicker>
            <asp:CustomValidator
                ID="CustomValidatorData"
                runat="server"
                ValidationGroup="colloquio"
                ControlToValidate="RadDateTimePickerData"
                ErrorMessage="Inserire una data e ora valida"
                ForeColor="Red" 
                onservervalidate="CustomValidatorData_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
            Mansioni<b>*</b>
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxMansione"
                runat="server"
                Width="200px"
                EmptyMessage="Selezionare la Mansione">
            </telerik:RadComboBox>
            <br />
            <asp:Button
                ID="ButtonAggiungiMansione"
                runat="server"
                Text="Aggiungi" 
                onclick="ButtonAggiungiMansione_Click" 
                Height="18px"
                Width="100%" />
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
        </td>
        <td>
            <asp:ListBox
                ID="ListBoxMansioni"
                runat="server"
                Width="200px">
            </asp:ListBox>
            <asp:CustomValidator
                ID="CustomValidatorValidatorMansioni"
                runat="server"
                ValidationGroup="colloquio"
                ErrorMessage="Selezionare almeno una mansione"
                ForeColor="Red" 
                onservervalidate="CustomValidatorValidatorMansioni_ServerValidate">
                *
            </asp:CustomValidator>
            <br />
            <asp:Button
                ID="ButtonEliminaSelezionato"
                runat="server"
                Text="Canc. selez." 
                Height="18px"
                Width="100%" onclick="ButtonEliminaSelezionato_Click" />
        
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
            Valutazione
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxValutazione"
                runat="server"
                AppendDataBoundItems="true"
                Width="200px">
            </telerik:RadComboBox>
        </td>
    </tr>
</table>