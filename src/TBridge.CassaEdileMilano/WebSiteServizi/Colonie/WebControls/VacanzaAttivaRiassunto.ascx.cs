﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class Colonie_WebControls_VacanzaAttivaRiassunto : System.Web.UI.UserControl
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Vacanza vacanza = biz.GetVacanzaAttiva();

            LabelAnno.Text = vacanza.Anno.ToString();
            LabelTipo.Text = vacanza.TipoVacanza.Descrizione;
        }
    }
}