﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicercaRichieste.ascx.cs" Inherits="Colonie_WebControls_RicercaRichieste" %>

<asp:Panel ID="PanelRicercaCantieri" runat="server" DefaultButton="ButtonVisualizza" Width="100%">
<table class="filledtable" width="200px">
    <tr>
        <td>
            Ricerca Richieste
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            Cognome
        </td>
        <td>
            Nome
        </td>
        <td>
            Note
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" MaxLength="50" Width="180px"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNome" runat="server" MaxLength="50" Width="180px"></telerik:RadTextBox>
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNote" runat="server" MaxLength="200" Width="180px"></telerik:RadTextBox>
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            Stato
        </td>
        <td>
            Codice richiesta
        </td>
        <td>
            Esperienza CEMI
        </td>
        <td>
            Mansione colloquio
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxStato"
                runat="server"
                Width="180px" AutoPostBack="True" 
                onselectedindexchanged="RadComboBoxStato_SelectedIndexChanged">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="" Value="TUTTI" />
                    <telerik:RadComboBoxItem runat="server" Text="Da Gestire" Value="DAGESTIRE" />
                    <telerik:RadComboBoxItem runat="server" Text="Fissato Colloquio" Value="FISSATOCOLLOQUIO" />
                    <telerik:RadComboBoxItem runat="server" Text="Valutato" Value="VALUTATO" />
                    <telerik:RadComboBoxItem runat="server" Text="Effettuata Proposta" Value="EFFETTUATAPROPOSTA" />
                    <telerik:RadComboBoxItem runat="server" Text="Proposta Accettata" Value="PROPOSTAACCETTATA" />
                    <telerik:RadComboBoxItem runat="server" Text="Proposta Non Accettata" Value="PROPOSTANONACCETTATA" />
                    <telerik:RadComboBoxItem runat="server" Text="Respinta" Value="RESPINTA" />
                </Items>
            </telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxIdRichiesta" runat="server" Width="180px" Type="Number" MinValue="1" MaxValue="2147483648" MaxLength="10">
                <NumberFormat DecimalDigits="0" />
            </telerik:RadNumericTextBox>
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxEsperienzeCEMI"
                runat="server"
                Width="180px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" />
                    <telerik:RadComboBoxItem runat="server" Text="Sì" Value="SI" />
                    <telerik:RadComboBoxItem runat="server" Text="No" Value="NO" />
                </Items>
            </telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxMansioneColloquio" runat="server" AppendDataBoundItems="true" Width="180px"></telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxValutazione"
                runat="server"
                Width="180px"
                Enabled="false"
                AppendDataBoundItems="true">
            </telerik:RadComboBox>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Mansione richiesta
        </td>
        <td>
            Turno richiesto
        </td>
        <td>
            Mansione assegnata
        </td>
        <td>
            Turno assegnato
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadComboBox ID="RadComboBoxMansione" runat="server" AppendDataBoundItems="true" Width="180px"></telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTurno" runat="server" AppendDataBoundItems="true" Width="180px"></telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxMansioneAssegnata" runat="server" AppendDataBoundItems="true" Width="180px"></telerik:RadComboBox>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTurnoAssegnato" runat="server" AppendDataBoundItems="true" Width="180px"></telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td align="right" colspan="3">
            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaCantieri"
                OnClick="ButtonVisualizza_Click" />
        </td>
    </tr>
</table>
<br />
<telerik:RadGrid ID="RadGridRichieste" runat="server" AllowPaging="True" PageSize="20"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Visible="False" 
        onitemdatabound="RadGridRichieste_ItemDataBound" 
        onitemcommand="RadGridRichieste_ItemCommand" AllowSorting="True" 
        onneeddatasource="RadGridRichieste_NeedDataSource">
    <ClientSettings>
        <Selecting CellSelectionMode="None" />
    </ClientSettings>
    <MasterTableView DataKeyNames="Id">
        <CommandItemSettings ExportToPdfText="Export to PDF" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter id column" 
                HeaderText="Prot." UniqueName="id">
                <ItemStyle VerticalAlign="Top" Width="30px" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn FilterControlAltText="Filter Persona column" 
                HeaderText="Persona" UniqueName="Persona">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <b>
                                <asp:Label ID="LabelCognome" runat="server">
                                    </asp:Label>
                                <asp:Label ID="LabelNome" runat="server">
                                    </asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelDataNascita" runat="server">
                                </asp:Label>
                                <asp:Image ID="ImageControlloEta" runat="server" 
                                    ImageUrl="~/images/semaforoRosso.png" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelLuogoNascita" runat="server">
                                </asp:Label>
                                (
                                <asp:Label ID="LabelProvinciaNascita" runat="server">
                                </asp:Label>
                                )
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                <asp:Label ID="LabelStato" runat="server" Text="Da Valutare">
                                    </asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelNote" runat="server">
                                    </asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" Width="120px" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn FilterControlAltText="Filter Persona column" 
                HeaderText="Colloquio" UniqueName="Colloquio">
                <ItemTemplate>
                    <asp:MultiView ID="MultiViewColloquio" runat="server" ActiveViewIndex="1">
                        <asp:View ID="ViewColloquio" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        Data e Ora:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelColloquioData" runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:BulletedList ID="BulletedListMansioni" runat="server" Width="100%">
                                        </asp:BulletedList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inviata conv.:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelInviatoColloquio" runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Valutazione:
                                    </td>
                                    <td>
                                        <b>
                                        <asp:Label ID="LabelValutazione" runat="server">
                                        </asp:Label>
                                        </b>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="ViewNoColloquio" runat="server">
                            Non è stato programmato un colloquio
                        </asp:View>
                    </asp:MultiView>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" Width="180px" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn FilterControlAltText="Filter Persona column" 
                HeaderText="Proposta" UniqueName="Proposta">
                <ItemTemplate>
                    <asp:MultiView ID="MultiViewProposta" runat="server" ActiveViewIndex="1">
                        <asp:View ID="ViewProposta" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        Mansione
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelPropostaMansione" runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Periodo
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelPropostaPeriodo" runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inviata proposta
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelInviataProposta" runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Accettata
                                    </td>
                                    <td>
                                        <b>
                                        <asp:Label ID="LabelAccettata" runat="server" Enabled="false" />
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inviata convocazione
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelInviataConvocazione" runat="server">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="ViewNoProposta" runat="server">
                            Non è stata effettuata una proposta
                        </asp:View>
                    </asp:MultiView>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" Width="180px" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn>
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td align="center">
                                <asp:ImageButton ID="ImageButtonSeleziona" runat="server" 
                                    CommandName="SELEZIONA" ImageUrl="~/images/lente.png" ToolTip="Seleziona" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <small>Seleziona </small>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" Width="10px" />
            </telerik:GridTemplateColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
</asp:Panel>