﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using System.IO;

public partial class Colonie_WebControls_RichiestaUpload : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public void GetUpload(ColoniePersonaleRichiesta richiesta)
    {
        Stream fsFotografia = RadUploadFotografia.UploadedFiles[0].InputStream;
        Stream fsCurriculum = null;

        // Fotografia
        byte[] fotografia = new byte[fsFotografia.Length];
        fsFotografia.Read(fotografia, 0, (Int32) fsFotografia.Length);
        richiesta.Fotografia = fotografia;

        // Curriculum
        if (RadUploadCurriculum.UploadedFiles.Count == 1)
        {
            fsCurriculum = RadUploadCurriculum.UploadedFiles[0].InputStream;
            byte[] curriculum = new byte[fsCurriculum.Length];
            fsCurriculum.Read(curriculum, 0, (Int32) fsCurriculum.Length);
            richiesta.Curriculum = curriculum;
            richiesta.CurriculumEstensione = RadUploadCurriculum.UploadedFiles[0].GetExtension().ToUpper();
        }
    }

    #region Custom Validators
    protected void CustomValidatorFotografia_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadUploadFotografia.UploadedFiles.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorCurriculum_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadUploadCurriculum.InvalidFiles.Count == 1)
        {
            args.IsValid = false;
        }
    }
    #endregion
}