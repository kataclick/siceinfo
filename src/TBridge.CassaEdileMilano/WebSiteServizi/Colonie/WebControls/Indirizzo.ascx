﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Indirizzo.ascx.cs" Inherits="Colonie_WebControls_Indirizzo" %>

<table class="standardTable">
    <colgroup>
        <col width="130px" />
        <col width="270px" />
        <col width="45px" />
        <col />
    </colgroup>
    <tr>
        <td>
            <small>
                Indirizzo:
            </small>
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxIndirizzo"
                runat="server"
                MaxLength="50"
                Width="250px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorIndirizzo"
                runat="server"
                ValidationGroup="indirizzo"
                ControlToValidate="RadTextBoxIndirizzo"
                ErrorMessage="Indirizzo mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
        <td>
            <small>
                Civico:
            </small>
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxCivico"
                runat="server"
                MaxLength="10"
                Width="40px">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <small>
                Provincia e Luogo:
            </small>
        </td>
        <td colspan="3">
            <telerik:RadAjaxLoadingPanel
                ID="RadAjaxLoadingPanelAttesa"
                runat="server"
                Width="100%">
            </telerik:RadAjaxLoadingPanel>
            <telerik:RadAjaxPanel
                ID="RadAjaxPanel"
                runat="server"
                LoadingPanelID="RadAjaxLoadingPanelAttesa"
                Width="100%">
                <telerik:RadComboBox
                    ID="RadComboBoxProvincia"
                    runat="server"
                    Width="50px" 
                    AutoPostBack="True"
                    EmptyMessage=" "
                    onselectedindexchanged="RadComboBoxProvincia_SelectedIndexChanged">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorProvincia"
                    runat="server"
                    ValidationGroup="indirizzo"
                    ControlToValidate="RadComboBoxProvincia"
                    ErrorMessage="Provincia mancante"
                    ForeColor="Red">
                    *
                </asp:RequiredFieldValidator>
                &nbsp;
                <telerik:RadComboBox
                    ID="RadComboBoxComune"
                    runat="server"
                    EmptyMessage=" "
                    Width="300px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorComune"
                    runat="server"
                    ValidationGroup="indirizzo"
                    ControlToValidate="RadComboBoxComune"
                    ErrorMessage="Comune mancante"
                    ForeColor="Red">
                    *
                </asp:RequiredFieldValidator>
            </telerik:RadAjaxPanel>
        </td>
    </tr>
    <tr>
        <td>
            <small>
                Cap:
            </small>
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxCap"
                runat="server"
                MaxLength="5"
                Width="60px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorCap"
                runat="server"
                ValidationGroup="indirizzo"
                ControlToValidate="RadTextBoxCap"
                ErrorMessage="Cap mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidatorCap"
                runat="server"
                ValidationGroup="indirizzo"
                ControlToValidate="RadTextBoxCap"
                ErrorMessage="Cap non valido"
                ValidationExpression="^\d{5}$"
                ForeColor="Red">
                *
            </asp:RegularExpressionValidator>
        </td>
    </tr>
</table>