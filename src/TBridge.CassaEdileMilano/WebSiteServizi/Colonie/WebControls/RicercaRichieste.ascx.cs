﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Filters;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class Colonie_WebControls_RicercaRichieste : System.Web.UI.UserControl
{
    private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();
    private readonly ColonieBusiness biz = new ColonieBusiness();

    private const Int32 LIMITEINFERIOREETA = 18;
    private const Int32 LIMITESUPERIOREETA = 67;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaFiltri();

            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            if (idUtente > 0)
            {
                Int32 pagina;
                RichiestaFilter filtro = biz.GetFiltroRicercaRichieste(idUtente, out pagina);
                if (filtro != null)
                {
                    CaricaFiltro(filtro, pagina);
                    RadGridRichieste.Visible = true;
                    CaricaRichieste(true);
                }
            }
        }
    }

    private void CaricaFiltro(RichiestaFilter filtro, Int32 pagina)
    {
        RadGridRichieste.CurrentPageIndex = pagina;

        RadTextBoxCognome.Text = filtro.Cognome;
        RadTextBoxNome.Text = filtro.Nome;
        if (filtro.Stato.HasValue)
        {
            RadComboBoxStato.SelectedValue = filtro.Stato.ToString();
            GestisciCambioStato();
        }
        if (filtro.IdRichiesta.HasValue)
        {
            RadNumericTextBoxIdRichiesta.Value = (double)filtro.IdRichiesta;
        }
        if (filtro.EsperienzaCE.HasValue)
        {
            if (filtro.EsperienzaCE.Value)
            {
                RadComboBoxEsperienzeCEMI.SelectedValue = "SI";
            }
            else
            {
                RadComboBoxEsperienzeCEMI.SelectedValue = "NO";
            }
        }
        if (filtro.IdMansione.HasValue)
        {
            RadComboBoxMansione.SelectedValue = filtro.IdMansione.ToString();
        }
        if (filtro.IdTurno.HasValue)
        {
            RadComboBoxTurno.SelectedValue = filtro.IdTurno.ToString();
        }
        if (filtro.IdMansioneAssegnata.HasValue)
        {
            RadComboBoxMansioneAssegnata.SelectedValue = filtro.IdMansioneAssegnata.ToString();
        }
        if (filtro.IdTurnoAssegnato.HasValue)
        {
            RadComboBoxTurnoAssegnato.SelectedValue = filtro.IdTurnoAssegnato.ToString();
        }
    }

    private void CaricaFiltri()
    {
        CaricaMansioni();
        CaricaTurni();
        CaricaValutazioni();
    }

    private void CaricaTurni()
    {
        Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();
        TurnoCollection turni = biz.GetTurni(null, vacanzaAttiva.IdVacanza, null, null, null);

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxTurno,
            turni,
            "DescrizioneTurno",
            "IdTurno");
        //RadComboBoxTurno.EmptyMessage = "Selezionare un turno";

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxTurnoAssegnato,
            turni,
            "DescrizioneTurno",
            "IdTurno");
        //RadComboBoxTurnoAssegnato.EmptyMessage = "Selezionare un turno";
    }

    private void CaricaMansioni()
    {
        List<ColoniePersonaleMansione> mansioni = bizEF.GetPersonaleMansioni();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxMansione,
            mansioni,
            "Descrizione",
            "Id");

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxMansioneAssegnata,
            mansioni,
            "Descrizione",
            "Id");

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxMansioneColloquio,
            mansioni,
            "Descrizione",
            "Id");
    }

    private void CaricaValutazioni()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxValutazione,
            bizEF.GetPersonaleValutazioni(),
            "Descrizione",
            "Id");
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        CaricaRichieste(true);
        RadGridRichieste.Visible = true;
    }

    private void CaricaRichieste(Boolean needDataBind)
    {
        RichiestaFilter filtro = CreaFiltro();

        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
        if (idUtente > 0)
        {
            biz.InsertFiltroRicercaRichieste(idUtente, filtro, RadGridRichieste.CurrentPageIndex);
        }

        List<ColoniePersonaleRichiesta> richieste = null;

        try
        {
            richieste = bizEF.GetRichieste(filtro);
        }
        catch
        {
            biz.DeleteFiltroRicercaRichieste(idUtente);
            throw;
        }

        if (needDataBind)
        {
            Presenter.CaricaElementiInGridView(
                RadGridRichieste,
                richieste);
        }
        else
        {
            RadGridRichieste.DataSource = richieste;
        }
    }

    private RichiestaFilter CreaFiltro()
    {
        RichiestaFilter filtro = new RichiestaFilter();

        if (RadNumericTextBoxIdRichiesta.Value.HasValue)
        {
            filtro.IdRichiesta = Convert.ToInt32(RadNumericTextBoxIdRichiesta.Value.Value);
        }

        String cognomeNorm = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
        if (!String.IsNullOrEmpty(cognomeNorm))
        {
            filtro.Cognome = cognomeNorm;
        }

        String nomeNorm = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
        if (!String.IsNullOrEmpty(nomeNorm))
        {
            filtro.Nome = nomeNorm;
        }

        String noteNorm = Presenter.NormalizzaCampoTesto(RadTextBoxNote.Text);
        if (!String.IsNullOrEmpty(noteNorm))
        {
            filtro.Note = noteNorm;
        }

        if (!String.IsNullOrEmpty(RadComboBoxMansione.SelectedValue))
        {
            filtro.IdMansione = Int32.Parse(RadComboBoxMansione.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadComboBoxMansioneAssegnata.SelectedValue))
        {
            filtro.IdMansioneAssegnata = Int32.Parse(RadComboBoxMansioneAssegnata.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadComboBoxMansioneColloquio.SelectedValue))
        {
            filtro.IdMansioneColloquio = Int32.Parse(RadComboBoxMansioneColloquio.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadComboBoxTurno.SelectedValue))
        {
            filtro.IdTurno = Int32.Parse(RadComboBoxTurno.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadComboBoxTurnoAssegnato.SelectedValue))
        {
            filtro.IdTurnoAssegnato = Int32.Parse(RadComboBoxTurnoAssegnato.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadComboBoxValutazione.SelectedValue))
        {
            filtro.IdValutazione = Int32.Parse(RadComboBoxValutazione.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadComboBoxStato.SelectedValue))
        {
            if (RadComboBoxStato.SelectedValue != "TUTTI")
            {
                filtro.Stato = (StatoRichiesta) Enum.Parse(typeof(StatoRichiesta), RadComboBoxStato.SelectedValue);
            }
        }

        if (!String.IsNullOrEmpty(RadComboBoxEsperienzeCEMI.SelectedValue))
        {
            filtro.EsperienzaCE = RadComboBoxEsperienzeCEMI.SelectedValue == "SI";
        }

        Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();
        filtro.IdVacanza = vacanzaAttiva.IdVacanza;
        
        return filtro;
    }

    protected void RadGridRichieste_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            ColoniePersonaleRichiesta richiesta = (ColoniePersonaleRichiesta) e.Item.DataItem;

            // Dati richiesta
            Label lCognome = (Label) e.Item.FindControl("LabelCognome");
            Label lNome = (Label) e.Item.FindControl("LabelNome");
            Label lDataNascita = (Label) e.Item.FindControl("LabelDataNascita");
            Label lLuogoNascita = (Label) e.Item.FindControl("LabelLuogoNascita");
            Label lProvinciaNascita = (Label) e.Item.FindControl("LabelProvinciaNascita");
            Image iEta = (Image) e.Item.FindControl("ImageControlloEta");
            Label lStato = (Label) e.Item.FindControl("LabelStato");
            Label lNote = (Label) e.Item.FindControl("LabelNote");

            // Colloquio
            MultiView mvColloquio = (MultiView) e.Item.FindControl("MultiViewColloquio");
            Label lColloquioData = (Label) e.Item.FindControl("LabelColloquioData");
            BulletedList blMansioni = (BulletedList) e.Item.FindControl("BulletedListMansioni");
            Label lInviatoColloquio = (Label) e.Item.FindControl("LabelInviatoColloquio");
            Label lValutazione = (Label) e.Item.FindControl("LabelValutazione");
            
            // Proposta
            MultiView mvProposta = (MultiView) e.Item.FindControl("MultiViewProposta");
            Label lPropostaMansione = (Label) e.Item.FindControl("LabelPropostaMansione");
            Label lPropostaPeriodo = (Label) e.Item.FindControl("LabelPropostaPeriodo");
            Label lAccettata = (Label) e.Item.FindControl("LabelAccettata");
            Label lInviataProposta = (Label) e.Item.FindControl("LabelInviataProposta");
            Label lInviataConvocazione = (Label) e.Item.FindControl("LabelInviataConvocazione");

            // Dati della richiesta
            lCognome.Text = richiesta.Cognome;
            lNome.Text = richiesta.Nome;
            lDataNascita.Text = richiesta.DataNascita.ToString("dd/MM/yyyy");
            lLuogoNascita.Text = richiesta.LuogoNascitaNav.Comune;
            lProvinciaNascita.Text = richiesta.ProvinciaNascita;

            if (richiesta.DataNascita <= DateTime.Today.AddYears(-LIMITEINFERIOREETA)
                && richiesta.DataNascita >= DateTime.Today.AddYears(-LIMITESUPERIOREETA))
            {
                iEta.ImageUrl = "~/images/semaforoVerde.png";
                iEta.ToolTip = String.Format("Età compresa tra {0} e {1} anni", LIMITEINFERIOREETA, LIMITESUPERIOREETA);
            }
            else
            {
                iEta.ImageUrl = "~/images/semaforoRosso.png";
                iEta.ToolTip = String.Format("Età NON compresa tra {0} e {1} anni", LIMITEINFERIOREETA, LIMITESUPERIOREETA);
            }

            lStato.Text = bizEF.TraduciStato(bizEF.GeneraStato(richiesta));
            lNote.Text = richiesta.RespintaNote;

            // Colloquio
            if (richiesta.ColoniePersonaleColloquio != null && richiesta.ColoniePersonaleColloquio.Count == 1)
            {
                mvColloquio.ActiveViewIndex = 0;
                lColloquioData.Text = richiesta.ColoniePersonaleColloquio.First().Data.ToString("dd/MM/yyyy HH:mm");
                Presenter.CaricaElementiInBulletedList(
                    blMansioni,
                    richiesta.ColoniePersonaleColloquio.First().ColoniePersonaleMansioni,
                    "Descrizione",
                    "Id");
                lInviatoColloquio.Text = !richiesta.ColoniePersonaleColloquio.First().Inviato.HasValue ? "No" : String.Format("In data {0:dd/MM/yyyy}", richiesta.ColoniePersonaleColloquio.First().Inviato.Value);
                lValutazione.Text = richiesta.ColoniePersonaleColloquio.First().Valutazione != null ? richiesta.ColoniePersonaleColloquio.First().Valutazione.Descrizione : "Da Valutare";
            }

            // Proposta
            if (richiesta.ColoniePersonaleProposte != null && richiesta.ColoniePersonaleProposte.Count == 1)
            {
                mvProposta.ActiveViewIndex = 0;
                lPropostaMansione.Text = richiesta.ColoniePersonaleProposte.First().Mansione.Descrizione;
                lPropostaPeriodo.Text = String.Format("{0:dd/MM/yyyy} - {1:dd/MM/yyyy}", richiesta.ColoniePersonaleProposte.First().DataInizioRapporto, richiesta.ColoniePersonaleProposte.First().DataFineRapporto);
                lAccettata.Text = !richiesta.ColoniePersonaleProposte.First().Accettata.HasValue ? "In attesa" : (richiesta.ColoniePersonaleProposte.First().Accettata.Value ? "Sì" : "No");
                lInviataProposta.Text = !richiesta.ColoniePersonaleProposte.First().Inviata.HasValue ? "No" : String.Format("In data {0:dd/MM/yyyy}", richiesta.ColoniePersonaleProposte.First().Inviata.Value);
                lInviataConvocazione.Text = !richiesta.ColoniePersonaleProposte.First().InviataConvocazione.HasValue ? "No" : String.Format("In data {0:dd/MM/yyyy}", richiesta.ColoniePersonaleProposte.First().InviataConvocazione.Value);
            }
        }
    }

    protected void RadGridRichieste_ItemCommand(object sender, GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "SELEZIONA":
                Int32 idRichiesta = (Int32) e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"];
                Context.Items["IdRichiesta"] = idRichiesta;
                Server.Transfer("~/Colonie/GestioneRichiesta.aspx");
                break;
        }
    }

    //protected void RadGridRichieste_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    //{
    //    CaricaRichieste();
    //}

    protected void RadComboBoxStato_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        GestisciCambioStato();
    }

    private void GestisciCambioStato()
    {
        RadComboBoxValutazione.Enabled = (RadComboBoxStato.SelectedValue == "VALUTATO");
        RadComboBoxValutazione.ClearSelection();
    }

    protected void RadGridRichieste_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        CaricaRichieste(false);
    }
}