﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Collections;

public partial class Colonie_WebControls_DatiProposta : System.Web.UI.UserControl
{
    private readonly ColonieBusiness biz = new ColonieBusiness();
    private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaMansioni();
            CaricaTurni();
            CaricaLuoghiAppuntamento();

            if (!RadTimePickerOraAppuntamento.SelectedDate.HasValue)
            {
                RadTimePickerOraAppuntamento.SelectedDate = new DateTime(2000, 1, 1, 11, 0, 0);
            }

            if (ViewState["Turni"] == null)
            {
                ViewState["Turni"] = new List<ColonieTurno>();
            }
            CaricaTurniSelezionati();
        }
    }

    private void CaricaLuoghiAppuntamento()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxLuogoAppuntamento,
            bizEF.GetLuoghiAppuntamento(),
            "Descrizione",
            "Id");
    }

    private void CaricaTurniSelezionati()
    {
        Presenter.CaricaElementiInListBox(
            ListBoxTurni,
            (List<ColonieTurno>) ViewState["Turni"],
            "DescrizioneAppoggio",
            "Id");
    }

    private void CaricaTurni()
    {
        if (RadComboBoxTurno.Items.Count == 0)
        {
            Vacanza vacanza = biz.GetVacanzaAttiva();
            TurnoCollection turni = biz.GetTurni(null, vacanza.IdVacanza, null, null, null);

            Presenter.CaricaElementiInDropDown(
                RadComboBoxTurno,
                turni,
                "DescrizioneTurno",
                "IdTurno");
        }
    }

    private void CaricaMansioni()
    {
        if (RadComboBoxMansione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxMansione,
                bizEF.GetPersonaleMansioni(),
                "Descrizione",
                "Id");
        }
    }

    public void CaricaProposta(ColoniePersonaleProposta proposta)
    {
        CaricaMansioni();

        ViewState["IdProposta"] = proposta.Id;
        RadDateTimePickerDataInizio.SelectedDate = proposta.DataInizioRapporto;
        RadDateTimePickerDataFine.SelectedDate = proposta.DataFineRapporto;
        RadComboBoxMansione.SelectedValue = proposta.IdMansione.ToString();
        Presenter.CaricaElementiInDropDown(
            RadComboBoxLivello,
            bizEF.GetPersonaleRetribuzioni(proposta.IdMansione),
            "Livello",
            "Id");
        RadComboBoxLivello.SelectedValue = proposta.Retribuzione.Id.ToString();
        AbilitaSceltaLivello();

        CheckBoxAccompagnatore.Checked = proposta.Accompagnatore;
        TextBoxProtocollo.Text = proposta.Protocollo;
        RadioButtonInAttesa.Checked = !proposta.Accettata.HasValue;
        RadioButtonSi.Checked = proposta.Accettata.HasValue && proposta.Accettata.Value;
        RadioButtonNo.Checked = proposta.Accettata.HasValue && !proposta.Accettata.Value;

        List<ColonieTurno> turni = new List<ColonieTurno>();
        foreach (ColonieTurno tur in proposta.Turni)
        {
            turni.Add(tur);
        }
        ViewState["Turni"] = turni;
        CaricaTurniSelezionati();

        if (proposta.LuogoAppuntamento != null)
        {
            RadComboBoxLuogoAppuntamento.SelectedValue = proposta.LuogoAppuntamento.Id.ToString();
        }
        RadTimePickerOraAppuntamento.SelectedDate = proposta.OraAppuntamento;
    }

    public ColoniePersonaleProposta GetProposta()
    {
        ColoniePersonaleProposta proposta = new ColoniePersonaleProposta();

        if (ViewState["IdProposta"] != null)
        {
            proposta.Id = (Int32) ViewState["IdProposta"];
        }
        proposta.DataInizioRapporto = RadDateTimePickerDataInizio.SelectedDate.Value;
        proposta.DataFineRapporto = RadDateTimePickerDataFine.SelectedDate.Value;
        proposta.IdMansione = Int32.Parse(RadComboBoxMansione.SelectedValue);
        proposta.IdRetribuzione = Int32.Parse(RadComboBoxLivello.SelectedValue);
        proposta.Accompagnatore = CheckBoxAccompagnatore.Checked;
        proposta.Protocollo = Presenter.NormalizzaCampoTesto(TextBoxProtocollo.Text);
        Boolean? accettata = null;
        if (RadioButtonSi.Checked)
        {
            accettata = true;
        }
        else
        {
            if (RadioButtonNo.Checked)
            {
                accettata = false;
            }
        }
        proposta.Accettata = accettata;
        proposta.Turni = (List<ColonieTurno>) ViewState["Turni"];
        if (!String.IsNullOrEmpty(RadComboBoxLuogoAppuntamento.SelectedValue))
        {
            proposta.IdColoniePersonaleLuogoAppuntamento = Int32.Parse(RadComboBoxLuogoAppuntamento.SelectedValue);
        }
        DateTime oraAppuntamento = new DateTime(2000, 1, 1, RadTimePickerOraAppuntamento.SelectedDate.Value.Hour, RadTimePickerOraAppuntamento.SelectedDate.Value.Minute, 0);
        proposta.OraAppuntamento = oraAppuntamento;

        return proposta;
    }

    protected void ButtonAggiungiTurno_Click(object sender, EventArgs e)
    {
        if (RadComboBoxTurno.SelectedItem != null)
        {
            ColonieTurno turno = new ColonieTurno()
            {
                Id = Int32.Parse(RadComboBoxTurno.SelectedItem.Value),
                DescrizioneAppoggio = RadComboBoxTurno.SelectedItem.Text
            };

            AggiungiTurno(turno);
        }
    }

    private void AggiungiTurno(ColonieTurno turno)
    {
        Boolean inserisci = true;

        List<ColonieTurno> turni = (List<ColonieTurno>) ViewState["Turni"];
        foreach (ColonieTurno tur in turni)
        {
            if (turno.Id == tur.Id)
            {
                inserisci = false;
                break;
            }
        }

        if (inserisci)
        {
            turni.Add(turno);
            ViewState["Turni"] = turni;

            CaricaTurniSelezionati();
        }
    }

    protected void ButtonEliminaSelezionato_Click(object sender, EventArgs e)
    {
        if (ListBoxTurni.SelectedIndex >= 0)
        {
            List<ColonieTurno> turni = (List<ColonieTurno>) ViewState["Turni"];
            turni.RemoveAt(ListBoxTurni.SelectedIndex);
            ViewState["Turni"] = turni;
            CaricaTurniSelezionati();
        }
    }

    #region Custom Validators
    protected void CustomValidatorDataInizio_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime? dataSelezionata = RadDateTimePickerDataInizio.SelectedDate;

        if (!dataSelezionata.HasValue
            || dataSelezionata.Value < new DateTime(2012, 1, 1)
            || dataSelezionata.Value > new DateTime(2050, 1, 1))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDataFine_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime? dataSelezionata = RadDateTimePickerDataFine.SelectedDate;

        if (!dataSelezionata.HasValue
            || dataSelezionata.Value < new DateTime(2012, 1, 1)
            || dataSelezionata.Value > new DateTime(2050, 1, 1))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorValidatorTurni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        List<ColonieTurno> turni = (List<ColonieTurno>) ViewState["Turni"];

        if (turni.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorValidatorOraAppuntamento_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!RadTimePickerOraAppuntamento.SelectedDate.HasValue)
        {
            args.IsValid = false;
        }
    }
    #endregion

    protected void RadComboBoxMansione_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (!String.IsNullOrEmpty(RadComboBoxMansione.SelectedValue))
        {
            Int32 idMansione = Int32.Parse(RadComboBoxMansione.SelectedValue);

            Presenter.CaricaElementiInDropDown(
                RadComboBoxLivello,
                bizEF.GetPersonaleRetribuzioni(idMansione),
                "Livello",
                "Id");

            RadComboBoxLivello.ClearSelection();
            RadComboBoxLivello.Text = null;
            AbilitaSceltaLivello();
        }
    }

    private void AbilitaSceltaLivello()
    {
        // Seleziono automaticamente l'unico livello disponibile
        if (RadComboBoxLivello.Items.Count == 1)
        {
            RadComboBoxLivello.SelectedIndex = 0;
            RadComboBoxLivello.Enabled = false;
        }
        else
        {
            RadComboBoxLivello.Enabled = true;
        }
    }
}