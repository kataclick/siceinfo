﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RichiestaEsperienze.ascx.cs" Inherits="Colonie_WebControls_RichiestaEsperienze" %>

<table class="filledtable">
    <tr>
        <td>
            Esperienze Pregresse
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            <b>
                Esperienza precedente in Villaggio Vacanze Cassa Edile
                <asp:RadioButton
                    ID="RadioButtonSi"
                    runat="server"
                    GroupName="esperienza"
                    Text="Sì" />
                <asp:RadioButton
                    ID="RadioButtonNo"
                    runat="server"
                    GroupName="esperienza"
                    Text="No"
                    Checked="true" />
            </b>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <small>Indicare per prime le esperienze più recenti, anche se di volontariato. Se l'esperienza è ancora in corso non indicare la fine.</small>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadGrid
                ID="RadGridEsperienze"
                runat="server"
                Width="100%" CellSpacing="0" GridLines="None" 
                onitemdatabound="RadGridEsperienze_ItemDataBound">
                <MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
                <Columns>
                    <telerik:GridTemplateColumn 
                        UniqueName="ContatoreColumn">
                        <ItemTemplate>
                            <asp:Label
                                ID="LabelContatore"
                                runat="server">
                            </asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="20px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn 
                        UniqueName="ImpresaColumn"
                        HeaderText="Datore di Lavoro">
                        <ItemTemplate>
                            <telerik:RadTextBox
                                ID="RadTextBoxImpresa"
                                runat="server"
                                MaxLength="50"
                                Width="100%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                        <ItemStyle Width="150px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn 
                        UniqueName="DalColumn"
                        HeaderText="Dal">
                        <ItemTemplate>
                            <telerik:RadDatePicker
                                ID="RadDatePickerDal"
                                runat="server"
                                Width="100%">
                            </telerik:RadDatePicker>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn 
                        UniqueName="AlColumn"
                        HeaderText="Al">
                        <ItemTemplate>
                            <telerik:RadDatePicker
                                ID="RadDatePickerAl"
                                runat="server"
                                Width="100%">
                            </telerik:RadDatePicker>
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn 
                        UniqueName="MansioneColumn"
                        HeaderText="Mansione">
                        <ItemTemplate>
                            <telerik:RadTextBox
                                ID="RadTextBoxMansione"
                                runat="server"
                                MaxLength="100"
                                Width="250px">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn 
                        UniqueName="ControlliColumn"
                        HeaderText="">
                        <ItemTemplate>
                            <asp:CustomValidator
                                ID="CustomValidatorTuttiDati"
                                runat="server"
                                ValidationGroup="stop"
                                ErrorMessage="Indicare tutti i dati dell'esperienza"
                                ForeColor="Red" onservervalidate="CustomValidatorTuttiDati_ServerValidate">
                                *
                            </asp:CustomValidator>
                            <asp:CustomValidator
                                ID="CustomValidatorOrdineDate"
                                runat="server"
                                ValidationGroup="stop"
                                ErrorMessage="L'inizio dell'esperienza deve essere precedente alla fine"
                                ForeColor="Red" 
                                onservervalidate="CustomValidatorOrdineDate_ServerValidate">
                                *
                            </asp:CustomValidator>
                        </ItemTemplate>
                        <ItemStyle Width="20px" />
                    </telerik:GridTemplateColumn>
                </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                </MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
            </telerik:RadGrid>
        </td>
    </tr>
</table>