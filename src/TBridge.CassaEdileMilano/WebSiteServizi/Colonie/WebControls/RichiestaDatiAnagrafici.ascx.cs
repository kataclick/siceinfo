﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Colonie.Business;

public partial class Colonie_WebControls_RichiestaDatiAnagrafici : System.Web.UI.UserControl
{
    private readonly Common commonBiz = new Common();
    private readonly ColonieBusinessEF efBiz = new ColonieBusinessEF();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime adesso = DateTime.Now;

            // Imposto le date limite per la data di nascita
            CompareValidatorDataNascitaDal.ValueToCompare = adesso.AddYears(-80).ToString("dd/MM/yyyy");
            CompareValidatorDataNascitaAl.ValueToCompare = adesso.AddYears(-15).ToString("dd/MM/yyyy");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTitoliStudio();
            CaricaProvince();
        }
    }

    private void CaricaProvince()
    {
        if (RadComboBoxLuogoNascitaProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(RadComboBoxLuogoNascitaProvincia);
        }
    }

    protected void CaricaTitoliStudio()
    {
        if (RadComboBoxTitoloStudio.Items.Count == 0)
        {
            List<ColoniePersonaleTitoloStudio> titoliStudio = efBiz.GetPersonaleTitoliStudio();
            Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxTitoloStudio,
                titoliStudio,
                "Descrizione",
                "Id");
        }
    }

    public void GetDatiAnagrafici(ColoniePersonaleRichiesta richiesta)
    {
        if (richiesta == null)
        {
            throw new ArgumentNullException("richiesta");
        }

        richiesta.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
        richiesta.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
        richiesta.Sesso = RadioButtonListSesso.SelectedValue;
        richiesta.DataNascita = RadDateTimePickerDataNascita.SelectedDate.Value;
        if (CheckBoxLuogoNascitaItalia.Checked)
        {
            richiesta.ProvinciaNascita = RadComboBoxLuogoNascitaProvincia.SelectedValue;
        }
        else
        {
            richiesta.ProvinciaNascita = "EE";
        }
        richiesta.LuogoNascita = RadComboBoxLuogoNascitaComune.SelectedValue;
        richiesta.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);
        richiesta.Telefono = RadTextBoxTelefono.Text;
        richiesta.Email = RadTextBoxEmail.Text;
        richiesta.IdColoniePersonaleTitoloStudio = Int32.Parse(RadComboBoxTitoloStudio.SelectedValue);
        richiesta.Note = Presenter.NormalizzaCampoTesto(RadTextBoxNote.Text);
        richiesta.Comunitario = RadioButtonListComunitario.SelectedValue == "S";

        // Residenza
        String residenzaIndirizzo;
        String residenzaProvincia;
        String residenzaComune;
        String residenzaCivico;
        String residenzaCap;
        IndirizzoResidenza.GetIndirizzo(out residenzaIndirizzo, out residenzaProvincia, out residenzaComune, out residenzaCivico, out residenzaCap);
        richiesta.ResidenzaIndirizzo = residenzaIndirizzo;
        richiesta.ResidenzaProvincia = residenzaProvincia;
        richiesta.ResidenzaComune = residenzaComune;
        richiesta.ResidenzaCivico = residenzaCivico;
        richiesta.ResidenzaCap = residenzaCap;

        // Domicilio
        String domicilioIndirizzo;
        String domicilioProvincia;
        String domicilioComune;
        String domicilioCivico;
        String domicilioCap;
        IndirizzoDomicilio.GetIndirizzo(out domicilioIndirizzo, out domicilioProvincia, out domicilioComune, out domicilioCivico, out domicilioCap);
        if (!String.IsNullOrEmpty(domicilioIndirizzo))
        {
            richiesta.DomicilioIndirizzo = domicilioIndirizzo;
            richiesta.DomicilioProvincia = domicilioProvincia;
            richiesta.DomicilioComune = domicilioComune;
            richiesta.DomicilioCivico = domicilioCivico;
            richiesta.DomicilioCap = domicilioCap;
        }
    }

    #region Custom Validators
    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // Controllo la correttezza del codice fiscale del legale rappresentante (primi 11 caratteri, bloccante)
        try
        {
            if (RadDateTimePickerDataNascita.SelectedDate.HasValue)
            {
                if (!CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text),
                                                                     Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text),
                                                                     RadioButtonListSesso.SelectedValue,
                                                                     RadDateTimePickerDataNascita.SelectedDate.Value,
                                                                     Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        catch
        {
        }
    }

    protected void CustomValidatorResidenza_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Page.Validate(IndirizzoResidenza.ID);
        args.IsValid = Page.IsValid;
    }

    protected void CustomValidatorDomicilio_ServerValidate(object source, ServerValidateEventArgs args)
    {
        String indirizzo = null;
        String provincia = null;
        String comune = null;
        String civico = null;
        String cap = null;

        IndirizzoDomicilio.GetIndirizzo(out indirizzo, out provincia, out comune, out civico, out cap);
        if (!String.IsNullOrEmpty(indirizzo)
            ^ !String.IsNullOrEmpty(provincia)
            ^ !String.IsNullOrEmpty(comune)
            ^ !String.IsNullOrEmpty(cap))
        {
            Page.Validate(IndirizzoDomicilio.ID);
            args.IsValid = Page.IsValid;
        }
    }
    #endregion

    protected void CheckBoxLuogoNascitaItalia_CheckedChanged(object sender, EventArgs e)
    {
        RadComboBoxLuogoNascitaProvincia.Enabled = CheckBoxLuogoNascitaItalia.Checked;

        if (RadComboBoxLuogoNascitaProvincia.Enabled)
        {
            RadComboBoxLuogoNascitaProvincia.CssClass = null;
            RadComboBoxLuogoNascitaProvincia.ClearSelection();
            RadComboBoxLuogoNascitaComune.Text = null;
            Presenter.CaricaComuni(RadComboBoxLuogoNascitaComune, RadComboBoxLuogoNascitaProvincia.SelectedValue);
        }
        else
        {
            RadComboBoxLuogoNascitaProvincia.CssClass = "campoDisabilitato";
            RadComboBoxLuogoNascitaProvincia.ClearSelection();
            RadComboBoxLuogoNascitaComune.Text = null;
            Presenter.CaricaComuni(RadComboBoxLuogoNascitaComune, "EE");
        }
    }

    protected void RadComboBoxLuogoNascitaProvincia_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        RadComboBoxLuogoNascitaComune.ClearSelection();
        Presenter.CaricaComuni(RadComboBoxLuogoNascitaComune, RadComboBoxLuogoNascitaProvincia.SelectedValue);
    }
}