﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiassuntoProposta.ascx.cs" Inherits="Colonie_WebControls_RiassuntoProposta" %>

<style type="text/css">
    .primaColonnaProposta
    {
        width: 170px;
    }
</style>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openRadWindowProposta(idRichiesta, idProposta) {
            var oWindow = radopen("../Colonie/GestioneProposta.aspx?idRichiesta=" + idRichiesta + "&idProposta=" + idProposta, null);
            oWindow.set_title("Proposta");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(500, 500);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />
<asp:MultiView
    ID="MultiViewProposta"
    runat="server">
    <asp:View
        ID="ViewNoProposta"
        runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    Nessuna proposta effettuata
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button
                        ID="ButtonPropostaInserisci"
                        runat="server"
                        Width="120px"
                        Text="Effettua proposta" />
                </td>
            </tr>
        </table>
    </asp:View>
    <asp:View
        ID="ViewProposta"
        runat="server">
        <table class="standardTable">
            <tr>
                <td class="primaColonnaProposta">
                    Data Inizio Rap.:
                </td>
                <td>
                    <asp:Label
                        ID="LabelDataInizioRapporto"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Data Fine Rap.:
                </td>
                <td>
                    <asp:Label
                        ID="LabelDataFineRapporto"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Mansione:
                </td>
                <td>
                    <asp:Label
                        ID="LabelMansione"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Livello:
                </td>
                <td>
                    <asp:Label
                        ID="LabelLivello"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Accompagnatore:
                </td>
                <td>
                    <asp:Label
                        ID="LabelAccompagnatore"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Accettazione:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelAccettata"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Inviata:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelInviata"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Inviata convocazione:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelInviataConvocazione"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Luogo Appuntamento:
                </td>
                <td>
                    <asp:Label
                        ID="LabelLuogoAppuntamento"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaProposta">
                    Ora Appuntamento:
                </td>
                <td>
                    <asp:Label
                        ID="LabelOraAppuntamento"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:BulletedList
                        ID="BulletedListTurni"
                        runat="server"
                        Width="100%">
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button
                        ID="ButtonPropostaModifica"
                        runat="server"
                        Width="120px"
                        Text="Modifica Proposta" />
                    &nbsp;
                    <asp:Button
                        ID="ButtonPropostaVisualizza"
                        runat="server"
                        Width="120px"
                        Text="Visualizza Proposta" onclick="ButtonPropostaVisualizza_Click" />
                    &nbsp;
                    <asp:Button
                        ID="ButtonPropostaInvia"
                        runat="server"
                        Width="120px"
                        Text="Invia Proposta" onclick="ButtonPropostaInvia_Click" 
                        style="height: 21px" />
                    &nbsp;
                    <asp:Button
                        ID="ButtonPropostaInviaConvocazione"
                        runat="server"
                        Width="120px"
                        Text="Invia Convocazione" 
                        onclick="ButtonPropostaInviaConvocazione_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <rsweb:ReportViewer ID="ReportViewerProposta" runat="server" Width="100%"
                        ProcessingMode="Remote" 
                        Visible="false" ShowBackButton="False" ShowExportControls="False" 
                        ShowFindControls="False" ShowPageNavigationControls="False" 
                        ShowPrintButton="False" ShowRefreshButton="False" ShowZoomControl="False">
                    </rsweb:ReportViewer>
                </td>
            </tr>
        </table>
    </asp:View>
</asp:MultiView>