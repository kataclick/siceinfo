﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiassuntoColloquio.ascx.cs" Inherits="Colonie_WebControls_RiassuntoColloquio" %>

<style type="text/css">
    .primaColonnaColloquio
    {
        width: 170px;
    }
</style>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openRadWindowColloquio(idRichiesta, idColloquio) {
            var oWindow = radopen("../Colonie/GestioneColloquio.aspx?idRichiesta=" + idRichiesta + "&idColloquio=" + idColloquio, null);
            oWindow.set_title("Colloquio");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(400, 450);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />
<asp:MultiView
    ID="MultiViewColloquio"
    runat="server">
    <asp:View
        ID="ViewNoColloquio"
        runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    Nessun colloquio fissato
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button
                        ID="ButtonColloquioInserisci"
                        runat="server"
                        Width="120px"
                        Text="Fissa Colloquio" />
                </td>
            </tr>
        </table>
    </asp:View>
    <asp:View
        ID="ViewColloquio"
        runat="server">
        <table class="standardTable">
            <tr>
                <td class="primaColonnaColloquio">
                    Data e Ora:
                </td>
                <td>
                    <asp:Label
                        ID="LabelData"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:BulletedList
                        ID="BulletedListMansioni"
                        runat="server"
                        Width="100%">
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td>
                    Inviata conv.:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelInviato"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="primaColonnaColloquio">
                    Valutazione:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelValutazione"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button
                        ID="ButtonColloquioModifica"
                        runat="server"
                        Width="120px"
                        Text="Modifica Colloquio" />
                    &nbsp;
                    <asp:Button
                        ID="ButtonPropostaInvia"
                        runat="server"
                        Width="120px"
                        Text="Invia convocazione" onclick="ButtonPropostaInvia_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <rsweb:ReportViewer ID="ReportViewerPrivacy" runat="server" Width="100%"
                        ProcessingMode="Remote" 
                        Visible="false" ShowBackButton="False" ShowExportControls="False" 
                        ShowFindControls="False" ShowPageNavigationControls="False" 
                        ShowPrintButton="False" ShowRefreshButton="False" ShowZoomControl="False">
                    </rsweb:ReportViewer>
                    <rsweb:ReportViewer ID="ReportViewerQuestionario" runat="server" Width="100%"
                        ProcessingMode="Remote" 
                        Visible="false" ShowBackButton="False" ShowExportControls="False" 
                        ShowFindControls="False" ShowPageNavigationControls="False" 
                        ShowPrintButton="False" ShowRefreshButton="False" ShowZoomControl="False">
                    </rsweb:ReportViewer>
                </td>
            </tr>
        </table>
    </asp:View>
</asp:MultiView>