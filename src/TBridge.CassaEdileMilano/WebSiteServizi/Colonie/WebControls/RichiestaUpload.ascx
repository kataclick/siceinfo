﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RichiestaUpload.ascx.cs" Inherits="Colonie_WebControls_RichiestaUpload" %>

<table class="filledtable">
    <tr>
        <td>
            Upload
        </td>
    </tr>
</table>
<table class="borderedTable">
    <colgroup>
        <col width="200px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Fotografia <small>(fototessera, JPG, max 500KB)</small><b>*</b>:
        </td>
        <td>
            <telerik:RadUpload ID="RadUploadFotografia" Runat="server" AllowedFileExtensions="JPG" 
                ControlObjectsVisibility="None" Culture="it" MaxFileInputsCount="1" 
                MaxFileSize="512000" OverwriteExistingFiles="True" Width="100%">
            </telerik:RadUpload>
            <asp:CustomValidator
                ID="CustomValidatorFotografia"
                runat="server"
                ValidationGroup="stop"
                ErrorMessage="Selezionare una fotografia in formato JPG di dimensione massima di 500KB"
                ForeColor="Red" 
                onservervalidate="CustomValidatorFotografia_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Curriculum Vitae <small>(PDF,DOC,DOCX max 1MB)</small>:
        </td>
        <td>
            <telerik:RadUpload ID="RadUploadCurriculum" Runat="server" AllowedFileExtensions="PDF,DOC,DOCX" 
                ControlObjectsVisibility="None" Culture="it" MaxFileInputsCount="1" 
                MaxFileSize="1024000" OverwriteExistingFiles="True" Width="100%">
            </telerik:RadUpload>
            <asp:CustomValidator
                ID="CustomValidatorCurriculum"
                runat="server"
                ValidationGroup="stop"
                ErrorMessage="Selezionare il curriculum nel formato PDF, DOC, DOCX di dimensione massima di 1MB"
                ForeColor="Red" 
                onservervalidate="CustomValidatorCurriculum_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>