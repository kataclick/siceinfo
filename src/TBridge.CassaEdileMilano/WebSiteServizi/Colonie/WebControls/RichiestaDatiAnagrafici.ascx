﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RichiestaDatiAnagrafici.ascx.cs" Inherits="Colonie_WebControls_RichiestaDatiAnagrafici" %>

<%@ Register src="Indirizzo.ascx" tagname="Indirizzo" tagprefix="uc1" %>

<table class="filledtable">
    <tr>
        <td>
            Richiedente
        </td>
    </tr>
</table>
<table class="borderedTable">
    <colgroup>
        <col width="200px" />
        <col />
        <col />
    </colgroup>
    <tr>
        <td colspan="2">
            <b>
                Dati Anagrafici
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Cognome<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxCognome"
                runat="server"
                MaxLength="50"
                Width="250px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorCognome"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadTextBoxCognome"
                ErrorMessage="Cognome mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxNome"
                runat="server"
                MaxLength="50"
                Width="250px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNome"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadTextBoxNome"
                ErrorMessage="Nome mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Sesso<b>*</b>:
        </td>
        <td>
            <asp:RadioButtonList
                ID="RadioButtonListSesso"
                runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True">M</asp:ListItem>
                <asp:ListItem>F</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            Codice Fiscale<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxCodiceFiscale"
                runat="server"
                MaxLength="16"
                Width="250px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorCodiceFiscale"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadTextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
            <asp:CustomValidator
                ID="CustomValidatorCodiceFiscale"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadTextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non valido in base ai dati forniti" 
                onservervalidate="CustomValidatorCodiceFiscale_ServerValidate"
                ForeColor="Red">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Cittadinanza comunitaria<b>*</b>:
        </td>
        <td>
            <asp:RadioButtonList
                ID="RadioButtonListComunitario"
                runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="S">Sì</asp:ListItem>
                <asp:ListItem Value="N">No</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>
                Data e Luogo di Nascita
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Data<b>*</b>:
        </td>
        <td>
            <telerik:RadDatePicker
                ID="RadDateTimePickerDataNascita"
                runat="server"
                Width="250px">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorDataNascita"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadDateTimePickerDataNascita"
                ErrorMessage="Data di nascita mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator
                ID="CompareValidatorDataNascitaDal"
                runat="server"
                ValidationGroup="stop"
                Operator="GreaterThanEqual"
                Type="Date"
                ControlToValidate="RadDateTimePickerDataNascita"
                ValueToCompare="1/1/1920"
                ErrorMessage="Data di nascita non valida"
                ForeColor="Red">
                *
            </asp:CompareValidator>
            <asp:CompareValidator
                ID="CompareValidatorDataNascitaAl"
                runat="server"
                ValidationGroup="stop"
                Operator="LessThanEqual"
                Type="Date"
                ControlToValidate="RadDateTimePickerDataNascita"
                ValueToCompare="1/1/2100"
                ErrorMessage="Data di nascita non valida"
                ForeColor="Red">
                *
            </asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Luogo<b>*</b>:
        </td>
        <td>
            <telerik:RadAjaxLoadingPanel
                ID="RadAjaxLoadingPanelAttesa"
                runat="server"
                Width="100%">
            </telerik:RadAjaxLoadingPanel>
            <telerik:RadAjaxPanel
                ID="RadAjaxPanelLuogoNascita"
                runat="server"
                LoadingPanelID="RadAjaxLoadingPanelAttesa"
                Width="100%">
                <small>
                    <table class="standardTable">
                        <colgroup>
                            <col width="80px" />
                            <col />
                        </colgroup>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox
                                    ID="CheckBoxLuogoNascitaItalia"
                                    runat="server"
                                    Text="Italia"
                                    Checked="true" AutoPostBack="True" 
                                    oncheckedchanged="CheckBoxLuogoNascitaItalia_CheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Provincia            
                            </td>
                            <td>
                                <telerik:RadComboBox
                                    ID="RadComboBoxLuogoNascitaProvincia"
                                    runat="server"
                                    EmptyMessage=" "
                                    Width="200px" AutoPostBack="True" 
                                    onselectedindexchanged="RadComboBoxLuogoNascitaProvincia_SelectedIndexChanged">
                                </telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Luogo            
                            </td>
                            <td>
                                <telerik:RadComboBox
                                    ID="RadComboBoxLuogoNascitaComune"
                                    runat="server"
                                    EmptyMessage=" "
                                    Width="200px">
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidatorLuogoNascitaComune"
                                    runat="server"
                                    ValidationGroup="stop"
                                    ControlToValidate="RadComboBoxLuogoNascitaComune"
                                    ErrorMessage="Selezionare il luogo di nascita"
                                    ForeColor="Red">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </small>
            </telerik:RadAjaxPanel>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>
                Recapiti
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Telefono/Cellulare<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxTelefono"
                runat="server"
                MaxLength="50"
                Width="250px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorTelefono"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadTextBoxTelefono"
                ErrorMessage="Telefono/Cellulare mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator 
                ID="RegularExpressionValidatorTelefono"
                runat="server" 
                ControlToValidate="RadTextBoxTelefono" 
                ErrorMessage="Telefono/Cellulare non valido"
                ValidationExpression="^(\+39)?[\d]{4,19}$" 
                ValidationGroup="stop"
                ForeColor="Red">
                *
            </asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Email<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxEmail"
                runat="server"
                MaxLength="255"
                Width="250px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEmail"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadTextBoxEmail"
                ErrorMessage="Email mancante"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator 
                ID="RegularExpressionValidatorEmail"
                runat="server" 
                ControlToValidate="RadTextBoxEmail" 
                ErrorMessage="Email non valida"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                ValidationGroup="stop"
                ForeColor="Red">
                *
            </asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Residenza<b>*</b>:
            <asp:CustomValidator
                ID="CustomValidatorResidenza"
                runat="server"
                ValidationGroup="stop"
                ErrorMessage="Residenza mancante o parzialmente indicata"
                ForeColor="Red" 
                onservervalidate="CustomValidatorResidenza_ServerValidate">
                &nbsp;
            </asp:CustomValidator>
        </td>
        <td>
            <uc1:Indirizzo ID="IndirizzoResidenza" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            Domicilio <small>(da compilare solamente se diverso dalla Residenza)</small>:
            <asp:CustomValidator
                ID="CustomValidatorDomicilio"
                runat="server"
                ValidationGroup="stop"
                ErrorMessage="Il domicilio non può essere indicato parzialmente"
                ForeColor="Red" 
                onservervalidate="CustomValidatorDomicilio_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
        <td>
            <uc1:Indirizzo ID="IndirizzoDomicilio" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>
                Titolo di Studio
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Titolo di Studio<b>*</b>:
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxTitoloStudio"
                runat="server"
                EmptyMessage=" "
                Width="250px">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorTitoloStudio"
                runat="server"
                ValidationGroup="stop"
                ControlToValidate="RadComboBoxTitoloStudio"
                ErrorMessage="Selezionare un Titolo di Studio"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Specificare <small>(titoli di studio/corsi frequentati/brevetti conseguiti)</small>:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxNote"
                runat="server"
                Width="250px"
                Height="50px"
                MaxLength="256" 
                TextMode="MultiLine">
            </telerik:RadTextBox>
        </td>
    </tr>
</table>