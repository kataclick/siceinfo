<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IscrizioneAltreCasseEdiliGestione.aspx.cs" Inherits="Colonie_IscrizioneAltreCasseEdiliGestione" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ColonieRicercaDomandeACE.ascx" TagName="ColonieRicercaDomandeACE"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ColonieDatiDomanda.ascx" TagName="ColonieDatiDomanda"
    TagPrefix="uc4" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione richieste"
        titolo="Villaggi vacanze" />
    <br />
    <asp:Panel ID="PanelAbilitata" runat="server" Visible="true">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Vacanza attiva"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Anno
                </td>
                <td>
                    <asp:Label ID="LabelVacanzaAnno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo
                </td>
                <td>
                    <asp:Label ID="LabelVacanzaTipo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Stato domande"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridViewSituazione" runat="server" AutoGenerateColumns="False"
                        Width="100%" OnRowDataBound="GridViewSituazione_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="Turno" HeaderText="Turno">
                                <ItemStyle Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PostiPrenotati" HeaderText="Posti confermati (a seguito prenotazione)" />
                            <asp:BoundField DataField="DomandeAccettate" HeaderText="Domande confermate" />
                            <asp:BoundField DataField="AccompagnatoriAccettati" HeaderText="Accompagnatori confermati" />
                            <asp:BoundField DataField="DomandeInCarico" HeaderText="Domande da valutare">
                                <ItemStyle ForeColor="Gray" />
                            </asp:BoundField>
                            <asp:BoundField DataField="AccompagnatoriInCarico" HeaderText="Accompagnatori da valutare">
                                <ItemStyle ForeColor="Gray" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DomandeRifiutate" HeaderText="Domande rifiutate" />
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun dato estratto
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Ricerca domande"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    E' possibile effettuare la ricerca per turno, stato domanda, cognome lavoratore
                    e figlio/a (partecipante).
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="PanelRicerca" runat="server" Visible="false">
                        <uc3:ColonieRicercaDomandeACE ID="ColonieRicercaDomandeACE1" runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PanelNessunaVacanzaAttiva" runat="server" Visible="false">
                        <asp:Label ID="LabelMessaggio" runat="server" ForeColor="red"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="LabelNonAbilitata" runat="server" ForeColor="red" Visible="false"
        Text="La cassa edile non � abilitata alla gestione delle domande. Contattare Cassa Edile di Milano."></asp:Label>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
    <uc4:ColonieDatiDomanda ID="ColonieDatiDomanda1" runat="server" Visible="false">
    </uc4:ColonieDatiDomanda>
</asp:Content>
