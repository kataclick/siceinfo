﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CandidaturaPersonaleConferma.aspx.cs" Inherits="Colonie_CandidaturaPersonaleConferma" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Colonie" sottoTitolo="Conferma Candidatura Personale" />
    <br />
    Gentile <b><asp:Label ID="LabelNomeCognome" runat="server"></asp:Label></b> (richiesta n° <b><asp:Label ID="LabelId" runat="server"></asp:Label></b>),
    <br />
    <br />
    La ringraziamo per averci inviato la Sua candidatura per i Villaggi Vacanze di Cassa Edile Milano.
    <br />
    <br />
    Confermiamo di averla ricevuta correttamente e che sarà presa in attenta considerazione nel più breve tempo possibile.
    <br />
    <br />
    Sarà contattato <b>all’indirizzo di posta elettronica</b> <asp:Label ID="LabelEmail" runat="server"></asp:Label> qualora il Suo profilo risulti in linea con i requisiti ricercati.
    <br />
    <br />
    Con i migliori saluti,
    <br />
    <br />
    Ufficio Personale
    <br />
    Cassa Edile di Milano, Lodi, Monza e Brianza
</asp:Content>


