<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieDefault.aspx.cs" Inherits="ColonieDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Villaggi vacanza"
        titolo="Villaggi vacanza" />
    <br />
    Benvenuto nella sezione Colonie
    <p class="DefaultPage">
        Tra le prestazioni erogate da Cassa Edile di Milano rientra il soggiorno estivo
        gratuito dei figli dei lavoratori aventi diritto di et� compresa tra i 5 anni e
        i 15 anni, aperto anche ai figli dei lavoratori iscritti presso altre Casse.<br />
        La struttura di Pinarella di Cervia (Ra) ospita ogni
        anno dei turni di 14 giorni ciascuno dalla fine di giugno alla fine di agosto.<br />
        La permanenza ed il viaggio sono completamente gratuiti.
    </p>
    <p class="DefaultPage">
        Utilizzando il men� verticale sulla sinistra del video � possibile indicare il numero
        preventivato di partecipanti tramite l'apposita funzione "Prenotazioni", inserire
        le domande del numero di partecipanti confermato da Cassa Edile di Milano (Inserimento
        domande) e gestire le stesse (Gestione domande).
    </p>
</asp:Content>
