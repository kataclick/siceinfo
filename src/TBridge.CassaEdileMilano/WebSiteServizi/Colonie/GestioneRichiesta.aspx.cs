﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Colonie_GestioneRichiesta : System.Web.UI.Page
{
    private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale);
        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdRichiesta"] != null)
            {
                Int32 idRichiesta = (Int32) Context.Items["IdRichiesta"];
                ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);
                RiassuntoRichiesta1.CaricaRichiesta(richiesta);
            }
        }
    }
}