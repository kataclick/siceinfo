﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CandidaturaPersonale.aspx.cs" Inherits="Colonie_CandidaturaPersonale" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="WebControls/RichiestaDatiAnagrafici.ascx" tagname="RichiestaDatiAnagrafici" tagprefix="uc2" %>
<%@ Register src="WebControls/VacanzaAttivaRiassunto.ascx" tagname="VacanzaAttivaRiassunto" tagprefix="uc3" %>
<%@ Register src="WebControls/RichiestaMansioniTurni.ascx" tagname="RichiestaMansioniTurni" tagprefix="uc4" %>
<%@ Register src="WebControls/RichiestaEsperienze.ascx" tagname="RichiestaEsperienze" tagprefix="uc5" %>
<%@ Register src="WebControls/RichiestaUpload.ascx" tagname="RichiestaUpload" tagprefix="uc6" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Villaggi vacanza" sottoTitolo="Candidatura Personale" />
    <br />
    <asp:Panel
        ID="PanelAttivo"
        runat="server"
        Visible="true">
        <uc3:VacanzaAttivaRiassunto ID="VacanzaAttivaRiassunto1" runat="server" />
        <br />
        Tutti i campi contrassegnati da <b>*</b> sono obbligatori
        <br />
        <br />
        <uc2:RichiestaDatiAnagrafici ID="RichiestaDatiAnagrafici1" runat="server" />
        <br />
        <uc4:RichiestaMansioniTurni ID="RichiestaMansioniTurni1" runat="server" />
        <br />
        <uc5:RichiestaEsperienze ID="RichiestaEsperienze1" runat="server" />
        <br />
        <uc6:RichiestaUpload ID="RichiestaUpload1" runat="server" />
        <br />
        <asp:CheckBox
            ID="CheckBoxConsensoPrivacy"
            runat="server" />
        <a href="~/Colonie/Static/Privacy.pdf" onclick="window.open('<%= ResolveUrl("~/Colonie/Static/Privacy.pdf") %>'); return false;" >Autorizzo il trattamento dei dati personali ai sensi del D. lgs. 196/03</a>
        <b>*</b>
        <asp:CustomValidator
            ID="CustomValidatorPrivacy"
            runat="server"
            ValidationGroup="stop"
            ErrorMessage="Per inoltrare la richiesta è obbligatorio acconsentire il trattamento dei dati"
            ForeColor="Red" onservervalidate="CustomValidatorPrivacy_ServerValidate" >
            *
        </asp:CustomValidator>
        <br />
        <br />
        <asp:Button
            ID="ButtonConferma"
            runat="server"
            Text="Invia la richiesta"
            Width="150px"
            CausesValidation="true"
            ValidationGroup="stop"
            onclick="ButtonConferma_Click" />
        <br />
        <asp:Label
            ID="LabelRichiestaGiaInserita"
            runat="server"
            ForeColor="Red"
            Text="Risulta già una richiesta per il codice fiscale fornito per la vacanza in corso. In caso di problemi contattare la Cassa Edile."
            Visible="false">
        </asp:Label>
        <asp:ValidationSummary
            ID="ValidationSummaryDatiAnagrafici"
            runat="server"
            CssClass="messaggiErrore"
            ValidationGroup="stop" />
    </asp:Panel>
    <asp:Panel
        ID="PanelNonAttivo"
        runat="server"
        Visible="false">
        La candidatura per i Villaggi Vacanze di Cassa Edile Milano, Lodi, Monza e Brianza <b>non è al momento attiva</b>.
    </asp:Panel>
</asp:Content>


