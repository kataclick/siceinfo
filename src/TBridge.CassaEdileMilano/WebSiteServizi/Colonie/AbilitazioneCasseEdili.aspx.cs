using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Colonie_AbilitazioneCasseEdili : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();
    private int idVacanza;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneMatrice,
                                              "AbilitazioneCasseEdili.aspx");

        idVacanza = Int32.Parse(Request.QueryString["idVacanza"]);
        ColonieRicercaCassaEdile1.IdVacanza = idVacanza;
        ColonieRicercaCassaEdile1.OnCassaEdileSelected += ColonieRicercaCassaEdile1_OnCassaEdileSelected;

        if (!Page.IsPostBack)
        {
            CaricaCasseEdiliAbilitate();
        }
    }

    protected void GridViewCasseEdiliAbilitate_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string idCassaEdile = (string) GridViewCasseEdiliAbilitate.DataKeys[e.RowIndex].Value;

        if (biz.DeleteAbilitazioneVacanza(idVacanza, idCassaEdile))
        {
            LabelErrore.Visible = false;
            CaricaCasseEdiliAbilitate();
            ColonieRicercaCassaEdile1.ForzaRicerca();
        }
        else
        {
            LabelErrore.Visible = true;
        }
    }

    private void CaricaCasseEdiliAbilitate()
    {
        CassaEdileCollection casseEdili = biz.GetAbilitazioniVacanza(idVacanza);
        GridViewCasseEdiliAbilitate.DataSource = casseEdili;
        GridViewCasseEdiliAbilitate.DataBind();
    }

    protected void ButtonAggiungiCassaEdile_Click(object sender, EventArgs e)
    {
        PanelAggiungi.Visible = true;
    }

    private void ColonieRicercaCassaEdile1_OnCassaEdileSelected(CassaEdile cassaEdile)
    {
        if (biz.InsertAbilitazioneVacanza(idVacanza, cassaEdile.IdCassaEdile))
        {
            LabelErrore.Visible = false;
            CaricaCasseEdiliAbilitate();
            ColonieRicercaCassaEdile1.ForzaRicerca();
        }
        else
        {
            LabelErrore.Visible = true;
        }
    }

    protected void GridViewCasseEdiliAbilitate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewCasseEdiliAbilitate.PageIndex = e.NewPageIndex;
        CaricaCasseEdiliAbilitate();
    }
}