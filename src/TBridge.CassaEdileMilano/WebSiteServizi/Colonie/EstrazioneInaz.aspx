﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EstrazioneInaz.aspx.cs" Inherits="Colonie_EstrazioneInaz" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/VacanzaAttivaRiassunto.ascx" TagName="VacanzaAttivaRiassunto"
    TagPrefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Colonie" sottoTitolo="Estrazione INAZ" />
    <br />
    <uc3:VacanzaAttivaRiassunto ID="VacanzaAttivaRiassunto1" runat="server" />
    <br />
    <asp:Panel ID="PanelEstrazioneInaz" runat="server" DefaultButton="ButtonEstrazioneInaz"
        Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    Data Assunzione - Dal
                </td>
                <td>
                    Data Assunzione - Al
                    <asp:CustomValidator ID="CustomValidatorDalAl" runat="server" ValidationGroup="estrazioneInaz"
                        ErrorMessage="Dal deve essere precedente ad Al" ForeColor="Red" 
                        onservervalidate="CustomValidatorDalAl_ServerValidate">
                        *
                    </asp:CustomValidator>
                </td>
                <td>
                    Mansione
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDal" runat="server" Width="180px">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerAl" runat="server" Width="180px">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxMansione" runat="server" AppendDataBoundItems="true"
                        Width="180px">
                    </telerik:RadComboBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td align="right" colspan="3">
                    <asp:Button ID="ButtonEstrazioneInaz" runat="server" Text="Estrai" Width="100px"
                        ValidationGroup="estrazioneInaz" OnClick="ButtonEstrazioneInaz_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:ValidationSummary
                        ID="ValidationSummaryEstrazione"
                        runat="server"
                        ValidationGroup="estrazioneInaz"
                        CssClass="messaggiErrore" />
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="LabelRisultato" runat="server">
        </asp:Label>
    </asp:Panel>
</asp:Content>
