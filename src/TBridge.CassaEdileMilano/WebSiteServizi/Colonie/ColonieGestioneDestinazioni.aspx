<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieGestioneDestinazioni.aspx.cs" Inherits="ColonieGestioneDestinazioni" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione Destinazioni"
        titolo="Villaggi vacanza" />
    <br />
    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Destinazioni disponibili"></asp:Label><br />
    <asp:GridView ID="GridViewDestinazioni" runat="server" AutoGenerateColumns="False"
        Width="100%" DataKeyNames="IdDestinazione">
        <Columns>
            <asp:BoundField DataField="DescrizioneTV" HeaderText="Tipo vacanza" />
            <asp:BoundField DataField="DescrizioneTD" HeaderText="Tipo destinazione" />
            <asp:BoundField DataField="Luogo" HeaderText="Luogo" />
            <asp:CheckBoxField DataField="Attiva" HeaderText="Attiva" />
        </Columns>
        <EmptyDataTemplate>
            Nessuna destinazione presente
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonNuovo" runat="server" Text="Nuova destinazione" OnClick="ButtonNuovo_Click"
        CausesValidation="False" /><br />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
    <br />
    <asp:Panel ID="PanelInserisciModifica" runat="server" Width="100%"
        Visible="False">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Codice destinazione" Visible="false"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="TextBoxIdDestinazione" runat="server" Width="300px" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="DropDownListTipiVacanza_SelectedIndexChanged" Width="300px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Selezionare un tipo vacanza"
                        ControlToValidate="DropDownListTipiVacanza"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Tipo destinazione"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTipiDestinazione" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Selezionare un tipo destinazione"
                        ControlToValidate="DropDownListTipiDestinazione"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Luogo"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLuogo" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Il campo non pu� essere vuoto"
                        ControlToValidate="TextBoxLuogo"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Attiva"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxAttiva" runat="server" Checked="True" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonOperazione" runat="server" Text="Inserisci/Modifica" OnClick="ButtonOperazione_Click" />
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
