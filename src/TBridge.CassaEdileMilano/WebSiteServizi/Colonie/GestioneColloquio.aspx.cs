﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using System.Configuration;
using System.Net;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Microsoft.Reporting.WebForms;
using System.IO;

public partial class Colonie_GestioneColloquio : System.Web.UI.Page
{
    private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale);
        #endregion

        #region Click multipli
        StringBuilder sbProp = new StringBuilder();
        sbProp.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbProp.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbProp.Append(
            "if (Page_ClientValidate('colloquio') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbProp.Append("this.value = 'Attendere...';");
        sbProp.Append("this.disabled = true;");
        sbProp.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null));
        sbProp.Append(";");
        sbProp.Append("return true;");
        ButtonConferma.Attributes.Add("onclick", sbProp.ToString());
        #endregion

        if (!Page.IsPostBack)
        {
            Int32 idRichiesta = Int32.Parse(Request.QueryString["idRichiesta"]);
            Int32 idColloquio = Int32.Parse(Request.QueryString["idColloquio"]);

            ViewState["IdRichiesta"] = idRichiesta;
            DatiColloquio1.CaricaRichiesta(idRichiesta);
            if (idColloquio > 0)
            {
                ViewState["IdColloquio"] = idColloquio;
                ColoniePersonaleColloquio colloquio = bizEF.GetColloquio(idColloquio);
                DatiColloquio1.CaricaColloquio(colloquio);
            }
        }
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Int32 idRichiesta = (Int32)ViewState["IdRichiesta"];
            Int32? idColloquio = ViewState["IdColloquio"] as Int32?;

            ColoniePersonaleColloquio colloquio = DatiColloquio1.GetColloquio();
            colloquio.IdRichiesta = idRichiesta;
            if (!idColloquio.HasValue)
            {
                colloquio.DataInserimentoRecord = DateTime.Now;
            }

            if (!idColloquio.HasValue)
            {
                bizEF.InsertColloquio(colloquio);
            }
            else
            {
                bizEF.UpdateColloquio(colloquio);
            }

            body1.Attributes.Add("onload", "CloseAndRebind();");
        }
    }
}