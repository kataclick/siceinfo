using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ColonieGestioneAutobus : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneDomande,
                                              "ColonieGestioneAutobus.aspx");

        if (!Page.IsPostBack)
        {
            ViewState["IdTurno"] = Int32.Parse(Request.QueryString["idTurno"]);
            CaricaAutobus();
        }
    }

    private void CaricaAutobus()
    {
        int idTurno = (int) ViewState["IdTurno"];

        AutobusCollection autobus = biz.GetAutobus(idTurno);
        GridViewAutobus.DataSource = autobus;
        GridViewAutobus.DataBind();
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        PanelInserimento.Visible = true;
    }

    private void SvuotaCampi()
    {
        TextBoxCodice.Text = string.Empty;
        TextBoxTarga.Text = string.Empty;
        LabelErroreInserimento.Text = string.Empty;
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Colonie/ColonieGestioneDomande.aspx");
    }

    protected void ButtonInserimento_Click(object sender, EventArgs e)
    {
        if (ControlloCampiServer())
        {
            Autobus autobus = CreaAutobus();

            if (biz.InsertAutobus(autobus))
            {
                SvuotaCampi();
                PanelInserimento.Visible = false;
                CaricaAutobus();
            }
            else
                LabelErroreInserimento.Text = "Errore durante l'inserimento";
        }
    }

    private Autobus CreaAutobus()
    {
        string codice = TextBoxCodice.Text.ToUpper();
        string targa = TextBoxTarga.Text.ToUpper();
        int idTurno = (int) ViewState["IdTurno"];

        return new Autobus(null, codice, targa, idTurno);
    }

    private bool ControlloCampiServer()
    {
        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxCodice.Text))
        {
            res = false;
            errori.Append("Codice non presente" + Environment.NewLine);
        }

        if (string.IsNullOrEmpty(TextBoxTarga.Text))
        {
            res = false;
            errori.Append("Targa non presente" + Environment.NewLine);
        }

        if (!res)
            LabelErroreInserimento.Text = errori.ToString();
        return res;
    }

    protected void GridViewAutobus_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int indice = e.RowIndex;
        int idAutobus = (int) GridViewAutobus.DataKeys[indice].Value;

        if (!biz.DeleteAutobus(idAutobus))
            LabelErrore.Text = "Errore nella cancellazione";
        else
            Response.Redirect(Page.Request.Url.ToString());
    }
}