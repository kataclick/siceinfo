﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class Colonie_CalendarioColloqui : System.Web.UI.Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();
    private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale);
        #endregion

        if (!Page.IsPostBack)
        {
            Vacanza vacanza = biz.GetVacanzaAttiva();

            List<ColoniePersonaleRichiesta> richieste = bizEF.GetRichiesteConColloquio(vacanza.IdVacanza.Value);
            RadSchedulerColloqui.DataSource = richieste;
            RadSchedulerColloqui.DataBind();
        }
    }

    protected void RadSchedulerColloqui_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
    {
        ColoniePersonaleRichiesta richiesta = (ColoniePersonaleRichiesta) e.Appointment.DataItem;
        e.Appointment.ToolTip = String.Format("{0}\n\n{1}", richiesta.NomeCompleto, richiesta.ElencoMansioni);
    }

    protected void ImageButtonPDF_Click(object sender, ImageClickEventArgs e)
    {
        RadSchedulerColloqui.ExportToPdf();
    }
}