﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RespingiRichiesta.aspx.cs" Inherits="Colonie_RespingiRichiesta" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body id="body1" runat="server">
    <form id="form1" runat="server">

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refresh(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function Cancel() {
            GetRadWindow().close();
        }
    </script>

    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>

        <table>
            <tr>
                <td>
                    Note:
                </td>
                <td>
                    <asp:TextBox
                        ID="TextBoxNote"
                        runat="server"
                        Width="200px"
                        Height="50px"
                        TextMode="MultiLine"
                        MaxLength="200">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button
                        ID="ButtonRespingi"
                        runat="server"
                        Text="Salva"
                        Width="100px" onclick="ButtonRespingi_Click" />
                    &nbsp;
                    <asp:Button
                        ID="ButtonAnnulla"
                        runat="server"
                        Text="Chiudi"
                        Width="100px"
                        OnClientClick="Cancel();" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
