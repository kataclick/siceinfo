<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieGestioneAutobus.aspx.cs" Inherits="ColonieGestioneAutobus" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione autobus"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Autobus" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewAutobus" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataKeyNames="IdAutobus" OnRowDeleting="GridViewAutobus_RowDeleting">
                    <Columns>
                        <asp:BoundField DataField="CodiceAutobus" HeaderText="Codice" />
                        <asp:BoundField DataField="Targa" HeaderText="Targa" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="DeleteButton" CommandName="Delete" ImageUrl="~/images/pallinoX.png"
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun autobus associato al turno
                    </EmptyDataTemplate>
                </asp:GridView>
                La cancellazione � possibile solamente se l'autobus non � ancora stato associato
                a nessun partecipante&nbsp;<br />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonNuovo" runat="server" Text="Nuovo autobus" OnClick="ButtonNuovo_Click" />&nbsp;
                <asp:Button ID="ButtonIndietro" runat="server" Text="Torna indietro" OnClick="ButtonIndietro_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    &nbsp;<asp:Panel ID="PanelInserimento" runat="server" Width="100%" Visible="False">
        <table class="standardTable">
            <tr>
                <td>
                    Codice
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodice" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="TextBoxCodice"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Targa
                </td>
                <td>
                    <asp:TextBox ID="TextBoxTarga" runat="server" Width="300px" MaxLength="8"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                        ControlToValidate="TextBoxTarga"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button ID="ButtonInserimento" runat="server" Text="Inserisci" OnClick="ButtonInserimento_Click" />
                    <asp:Label ID="LabelErroreInserimento" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
