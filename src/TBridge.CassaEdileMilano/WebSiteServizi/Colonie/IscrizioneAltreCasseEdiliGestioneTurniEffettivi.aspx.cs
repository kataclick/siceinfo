﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using CassaEdile = TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;

public partial class Colonie_IscrizioneAltreCasseEdiliGestioneTurniEffettivi : System.Web.UI.Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    private const Int32 GIORNITOLLERANZA = 30;
    private DateTime dataPresenzeOk;

    protected void Page_Load(object sender, EventArgs e)
    {
        String idCassaEdile = string.Empty;
        
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieInserimentoDomandeACE,
                                              "IscrizioneAltreCasseEdili.aspx");

        Vacanza vacanza = null;
        CassaEdile cassaEdile = null;

        if (!Page.IsPostBack)
        {
            //Otteniamo la vacanza attiva
            vacanza = biz.GetVacanzaAttiva();

            if (vacanza != null)
            {
                ViewState["IdVacanzaAttiva"] = vacanza.IdVacanza.Value;

                //Visualizziamo la vacanza attiva
                LabelVacanzaAnno.Text = vacanza.Anno.ToString();
                LabelVacanzaTipo.Text = vacanza.TipoVacanza.Descrizione;

                //Se l'utente è di ACE
                if (GestioneUtentiBiz.IsCassaEdile())
                {
                    cassaEdile = (CassaEdile) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    ViewState["IdCassaEdile"] = cassaEdile.IdCassaEdile;

                    //settiamo i parametri al controllo
                    this.ColonieRicercaDomandeACEEffettive1.SetParametri(vacanza.IdVacanza.Value, cassaEdile.IdCassaEdile);

                    #region Carichiamo le parmanenze dei partecipanti
                    //L'unico metodo fornito presente è il calcolo costi per vacanza (o turno). Noi usiamo il calcolocosti per vacanza e poi 
                    //visualizziamo i dati solamente per la cassa che si è loggata. E' uno spreco di risorse ma è il metodo più rapido per sviluppare questa parte
                    //Mura è d'accordo [giuggi: 20/10/2009].
                    CostoPerCassaCollection costi = biz.CalcolaCosti(vacanza.IdVacanza.Value);

                    if (costi != null)
                    {
                        CostoPerCassaCollection costoACE = new CostoPerCassaCollection();

                        CostoPerCassa costo = costi.GetByCassaEdile(cassaEdile.IdCassaEdile);
                        if (costo != null)
                        {
                            costo.RemoveTotaleSaldo();
                            costoACE.Add(costo);
                        }

                        GridViewCosti.DataSource = costoACE;
                        GridViewCosti.DataBind();
                    }
                    else
                    {
                        GridViewCosti.DataSource = null;
                        GridViewCosti.DataBind();
                    }

                    CaricaDataPresenzeOk();
                    if (DateTime.Now < dataPresenzeOk.AddDays(GIORNITOLLERANZA))
                    {
                        GridViewCosti.Visible = false;
                        LabelMessaggio.Text = String.Format("Il riassunto sulla situazione della vacanza sarà disponibile dal {0:dd/MM/yyyy}", dataPresenzeOk.AddDays(GIORNITOLLERANZA + 1));
                    }
                    #endregion
                }
            }
        }
    }

    private void CaricaDataPresenzeOk()
    {
        Vacanza vacanza = biz.GetVacanzaAttiva();
        TurnoCollection turni = biz.GetTurni(null, vacanza.IdVacanza, null, null, null);

        foreach (Turno turno in turni)
        {
            if (turno.Al > dataPresenzeOk)
            {
                dataPresenzeOk = turno.Al;
            }
        }
    }

    #region Rowdatabound per visualizzare i giorni utilizzati (i costi non vengono visualizzati
    protected void GridViewCosti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView gvCosto = (GridView)e.Row.FindControl("GridViewCosto");

            CostoPerCassa costo = (CostoPerCassa)e.Row.DataItem;
            gvCosto.DataSource = costo.Costi;
            gvCosto.DataBind();
        }
    }

    protected void GridViewCosto_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CostoPerTipologia costo = (CostoPerTipologia)e.Row.DataItem;

            Label lNumeroPersone = (Label)e.Row.FindControl("LabelNumeroPersone");
            Label lPeriodoPermanenza = (Label)e.Row.FindControl("LabelPeriodoPermanenza");
            Label lTotaleGiorni = (Label)e.Row.FindControl("LabelTotaleGiorni");
            //Label lQuotaIndividuale = (Label)e.Row.FindControl("LabelQuotaIndividuale");
            //Label lTotale = (Label)e.Row.FindControl("LabelTotale");

            if (costo.NumeroPersone != 0)
                lNumeroPersone.Text = costo.NumeroPersone.ToString();
            if (costo.PeriodoPermanenza != 0)
                lPeriodoPermanenza.Text = costo.PeriodoPermanenza.ToString();
            if (costo.TotaleGiorni != 0)
                lTotaleGiorni.Text = costo.TotaleGiorni.ToString();
            //if (costo.QuotaIndividuale != 0)
            //    lQuotaIndividuale.Text = costo.QuotaIndividuale.ToString();
            //if (costo.Totale != 0)
            //    lTotale.Text = costo.Totale.ToString();
        }
    }

    #endregion
}
