using System;
using System.Web.UI;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ColonieModificaPartecipantiTurno : Page
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneMatrice,
                                              "ColonieGestionePartecipantiTurno.aspx");

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["nonAssegnate"] != null)
            {
                ColonieRicercaDomande1.ImpostaFiltri(true, -1, -1);
            }

            if (Request.QueryString["idTurno"] != null)
            {
                int idTurno = Int32.Parse(Request.QueryString["idTurno"]);

                if (idTurno != -1)
                {
                    Turno turno = biz.GetTurni(idTurno, null, null, null, null)[0];
                    ColonieRicercaDomande1.ImpostaFiltri(false, turno.Destinazione.IdDestinazione.Value, turno.IdTurno.Value);
                }
            }
        }
    }
}