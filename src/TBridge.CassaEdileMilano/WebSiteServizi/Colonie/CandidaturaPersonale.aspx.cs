﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Exceptions;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using System.Configuration;
using System.Net;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Business;

public partial class Colonie_CandidaturaPersonale : System.Web.UI.Page
{
    ColonieBusinessEF efBiz = new ColonieBusinessEF();
    ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieRichiestePersonale);
        #endregion

        #region Per prevenire click multipli

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('datiAnagrafici') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null));
        sb.Append(";");
        sb.Append("return true;");
        ButtonConferma.Attributes.Add("onclick", sb.ToString());

        #endregion

        if (!Page.IsPostBack)
        {
            Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();
            DateTime adesso = DateTime.Now;

            if (vacanzaAttiva.DataInizioRichieste.HasValue
                && vacanzaAttiva.DataFineRichieste.HasValue
                && vacanzaAttiva.DataInizioRichieste.Value <= adesso
                && adesso < vacanzaAttiva.DataFineRichieste.Value.AddDays(1))
            {
                PanelAttivo.Visible = true;
                PanelNonAttivo.Visible = false;
            }
            else
            {
                PanelAttivo.Visible = false;
                PanelNonAttivo.Visible = true;
            }
        }
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        LabelRichiestaGiaInserita.Visible = false;

        if (Page.IsValid)
        {
            ConfermaRichiesta();
        }
    }

    private void ConfermaRichiesta()
    {
        ColoniePersonaleRichiesta richiesta = CreaRichiesta();
        try
        {
            efBiz.InsertRichiesta(richiesta);
            InviaEmailConferma(richiesta);
            Context.Items["IdRichiesta"] = richiesta.Id;
            Server.Transfer("~/Colonie/CandidaturaPersonaleConferma.aspx");
        }
        catch (PersonaleRichiestaGiaInseritaException)
        {
            LabelRichiestaGiaInserita.Visible = true;
        }
    }

    private void InviaEmailConferma(ColoniePersonaleRichiesta richiesta)
    {
        EmailMessageSerializzabile email = new EmailMessageSerializzabile();

        email.Destinatari = new List<EmailAddress>();
        //email.Destinatari.Add(new EmailAddress("alessio.mura@itsinfinity.com", "alessio.mura@itsinfinity.com"));
        email.Destinatari.Add(new EmailAddress(richiesta.Email, richiesta.Email));

        email.DestinatariBCC = new List<EmailAddress>();
        if (!Common.Sviluppo)
        {
            email.DestinatariBCC.Add(new EmailAddress("personale.colonie@cassaedilemilano.it", "personale.colonie@cassaedilemilano.it"));
        }

        var sbPlain = new StringBuilder();
        var sbHtml = new StringBuilder();

        #region Creiamo il plaintext della mail
        sbPlain.AppendLine(String.Format("Gentile {0} {1} (richiesta n° {2}),", richiesta.Nome, richiesta.Cognome, richiesta.Id));
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("La ringraziamo per averci inviato la Sua candidatura per i Villaggi Vacanze di Cassa Edile Milano.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Confermiamo di averla ricevuta correttamente e che sarà presa in attenta considerazione nel più breve tempo possibile.");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine(String.Format("Sarà contattato/a all’indirizzo di posta elettronica {0} qualora il Suo profilo risulti in linea con i requisiti ricercati.", richiesta.Email));
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Con i migliori saluti,");
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Ufficio Personale");
        sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");

        #endregion
        email.BodyPlain = sbPlain.ToString();

        #region Creiamo l'htlm text della mail
        sbHtml.Append(String.Format("<span style=\"font-family: Calibri; font-size: 11pt\">Gentile <b>{0} {1}</b> (richiesta n° <b>{2}</b>),", richiesta.Nome, richiesta.Cognome, richiesta.Id));
        sbHtml.Append("<br /><br />");
        sbHtml.Append("La ringraziamo per averci inviato la Sua candidatura per i Villaggi Vacanze di Cassa Edile Milano.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Confermiamo di averla ricevuta correttamente e che sarà presa in attenta considerazione nel più breve tempo possibile.");
        sbHtml.Append("<br /><br />");
        sbHtml.Append(String.Format("Sarà contattato/a <b>all’indirizzo di posta elettronica {0}</b> qualora il Suo profilo risulti in linea con i requisiti ricercati.", richiesta.Email));
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Con i migliori saluti,</span>");
        sbHtml.Append("<br /><br />");
        /*
        sbHtml.Append("Ufficio Personale");
        sbHtml.Append("<br />");
        sbHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
        */
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Ufficio Personale</strong></span>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Cassa Edile di Milano, Lodi, Monza e Brianza</strong><br />Via S. Luca, 6<br />20122 Milano (Mi)</span>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\">Web <a href=\"http://www.cassaedilemilano.it\">www.cassaedilemilano.it</a>");
        sbHtml.Append("<br /><br />");
        sbHtml.Append("<img src=\"http://ww2.cassaedilemilano.it/Portals/0/images/Logo%20mail%20aziendale.jpg\" />");

        #endregion
        email.BodyHTML = sbHtml.ToString();

        email.Mittente = new EmailAddress();
        email.Mittente.Indirizzo = "personale.colonie@cassaedilemilano.it";
        email.Mittente.Nome = "Ufficio Personale";

        email.Oggetto = "Conferma ricezione richiesta di assunzione ";

        email.DataSchedulata = DateTime.Now;
        email.Priorita = MailPriority.Normal;

        string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
        string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

        var service = new EmailInfoService();
        var credentials = new NetworkCredential(emailUserName, emailPassword);
        service.Credentials = credentials;
        service.InviaEmail(email);
    }

    private ColoniePersonaleRichiesta CreaRichiesta()
    {
        ColoniePersonaleRichiesta richiesta = new ColoniePersonaleRichiesta();

        Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();

        richiesta.IdColonieVacanza = vacanzaAttiva.IdVacanza.Value;
        RichiestaDatiAnagrafici1.GetDatiAnagrafici(richiesta);
        RichiestaMansioniTurni1.GetMansioniTurni(richiesta);
        RichiestaEsperienze1.GetEsperienze(richiesta);
        RichiestaUpload1.GetUpload(richiesta);

        richiesta.DataInserimentoRecord = DateTime.Now;

        return richiesta;
    }

    #region Custom Validators
    protected void CustomValidatorPrivacy_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!CheckBoxConsensoPrivacy.Checked)
        {
            args.IsValid = false;
        }
    }
    #endregion
}