<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ColonieGestioneTurni.aspx.cs" Inherits="ColonieGestioneTurni" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione turni"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownListTipiVacanza_SelectedIndexChanged" Enabled="False">
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Destinazione"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListDestinazione" runat="server" Width="300px" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownListDestinazione_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Visualizza" ValidationGroup="visualizzazione" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;<asp:Label ID="LabelErroreVisualizzazione" runat="server" ForeColor="Red" Text="Selezionare una destinazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="PanelVisualizzazione" runat="server" Visible="False" Width="100%">
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Turni presenti"></asp:Label>
        <br />
        <asp:GridView ID="GridViewTurni" runat="server" Width="100%" AutoGenerateColumns="False"
            OnRowDataBound="GridViewTurni_RowDataBound" DataKeyNames="IdTurno" OnRowDeleting="GridViewTurni_RowDeleting">
            <Columns>
                <asp:BoundField DataField="ProgressivoTurno" HeaderText="Progressivo">
                    <ItemStyle Width="40px" />
                </asp:BoundField>
                <asp:BoundField DataField="Dal" HeaderText="Dal" DataFormatString="{0:dd/MM/yyyy}"
                    HtmlEncode="False">
                    <ItemStyle Width="60px" />
                </asp:BoundField>
                <asp:BoundField DataField="Al" HeaderText="Al" DataFormatString="{0:dd/MM/yyyy}"
                    HtmlEncode="False">
                    <ItemStyle Width="60px" />
                </asp:BoundField>
                <asp:BoundField DataField="PostiDisponibili" HeaderText="Disponibilit&#224;">
                    <ItemStyle Width="40px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Durata (giorni)">
                    <ItemStyle Width="40px" />
                    <ItemTemplate>
                        <asp:Label ID="LabelDurata" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CostoGiornaliero" HeaderText="Costo giornaliero" DataFormatString="{0:C}" />
                <asp:BoundField DataField="CostoGiornalieroAccompagnatore" HeaderText="Costo giornaliero acc."
                    DataFormatString="{0:C}" />
                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteImageUrl="~/images/pallinoX.png"
                    DeleteText="" ShowDeleteButton="True" />
            </Columns>
            <EmptyDataTemplate>
                Nessun turno presente<br />
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:Label ID="LabelRes" runat="server" Text="Label"></asp:Label><br />
        <asp:Button ID="ButtonNuovoTurno" runat="server" OnClick="ButtonNuovoTurno_Click"
            Text="Nuovo turno" /></asp:Panel>
    <br />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
    <asp:Panel ID="PanelNuovo" runat="server" Visible="False" Width="100%">
        <table class="standardTable">
            <tr>
                <td colspan="3">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Nuovo turno"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Progressivo"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxProgressivo" runat="server" Width="300px" MaxLength="2"></asp:TextBox>
                </td>
                <td style="width: 86px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxProgressivo"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="inserimento">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxProgressivo"
                        ErrorMessage="Valore numerico" ValidationExpression="^\d{1,2}$" ValidationGroup="inserimento"></asp:RegularExpressionValidator>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Dal (gg/mm/aaaa)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td style="width: 86px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxDal"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="inserimento">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Formato data errato"
                        ControlToValidate="TextBoxDal" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="inserimento"></asp:RegularExpressionValidator>&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Al (gg/mm/aaaa)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td style="width: 86px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxAl"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="inserimento">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxAl"
                        ErrorMessage="Formato data errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="inserimento"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="DisponibilitÓ"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDisponibilita" runat="server" Width="300px" MaxLength="4"></asp:TextBox>
                </td>
                <td style="width: 86px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxDisponibilita"
                        ErrorMessage="RequiredFieldValidator" ValidationGroup="inserimento">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBoxDisponibilita"
                        ErrorMessage="Formato data errato" ValidationExpression="^\d{1,4}$" ValidationGroup="inserimento"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Costo giornaliero
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCostoGiornaliero" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxCostoGiornaliero"
                        ErrorMessage="Digitare un costo giornaliero" ValidationGroup="inserimento">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBoxCostoGiornaliero"
                        ErrorMessage="Formato errato" ValidationExpression="^\d+(\,\d\d)?$" ValidationGroup="inserimento">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Costo giornaliero accompagnatore
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCostoGiornalieroAccompagnatore" runat="server" Width="300px"
                        MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxCostoGiornalieroAccompagnatore"
                        ErrorMessage="Digitare un costo giornaliero per l'accompagnatore" ValidationGroup="inserimento">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TextBoxCostoGiornalieroAccompagnatore"
                        ErrorMessage="Formato errato" ValidationExpression="^\d+(\,\d\d)?$" ValidationGroup="inserimento">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 26px">
                    <asp:Button ID="ButtonAggiungi" runat="server" Text="Crea turno" OnClick="ButtonAggiungi_Click"
                        ValidationGroup="inserimento" />
                    <br />
                    <asp:Label ID="LabelErroreInserimento" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
