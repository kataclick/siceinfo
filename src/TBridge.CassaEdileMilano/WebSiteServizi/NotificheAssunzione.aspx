<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="NotificheAssunzione.aspx.cs" Inherits="NotificheAssunzione" %>

<%@ Register Src="WebControls/NotificheImpresaSelezionata.ascx" TagName="NotificheImpresaSelezionata"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche"
        sottoTitolo="Gestione notifiche di assunzione" />
    <br />
    <asp:Panel ID="PanelImprese" runat="server"  Width="519px">
        <uc3:NotificheImpresaSelezionata ID="NotificheImpresaSelezionata1" runat="server" />
        <br />
        <asp:CustomValidator ID="CustomValidatorImpresaSelezionata" runat="server" ErrorMessage="Selezionare una impresa"
            OnServerValidate="CustomValidatorImpresaSelezionata_ServerValidate" ValidationGroup="selezioneImpresa"></asp:CustomValidator></asp:Panel>
    <br />
    Per registrare un nuovo rapporto di lavoro, compilare i campi obbligatori sotto
    riportati e premere OK per conferma. I campi contrassegnati con * sono obbligatori.<br />
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 190px">
            </td>
            <td style="width: 191px">
            </td>
            <td align="left" style="width: 169px">
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Cognome*:
            </td>
            <td style="width: 191px">
                <asp:TextBox ID="TextBoxCognome" runat="server"></asp:TextBox>
            </td>
            <td align="left" style="width: 169px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxCognome"
                    ErrorMessage="*" ValidationGroup="inserimentoNotifica"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Nome*:
            </td>
            <td style="width: 191px">
                <asp:TextBox ID="TextBoxNome" runat="server"></asp:TextBox>
            </td>
            <td align="left" style="width: 169px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNome"
                    ErrorMessage="*" ValidationGroup="inserimentoNotifica"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Data nascita (gg/mm/aaaa)*:
            </td>
            <td style="width: 191px">
                <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td align="left" style="width: 169px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxDataNascita"
                    ErrorMessage="*" ValidationGroup="inserimentoNotifica"></asp:RequiredFieldValidator>&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxDataNascita"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                    ValidationGroup="inserimentoNotifica"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Comune nascita*:
            </td>
            <td style="width: 191px">
                <asp:TextBox ID="TextBoxComuneNascita" runat="server"></asp:TextBox>
            </td>
            <td align="left" style="width: 169px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxComuneNascita"
                    ErrorMessage="*" ValidationGroup="inserimentoNotifica"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Sesso*:
            </td>
            <td align="left" style="width: 191px">
                <asp:RadioButtonList ID="RadioButtonListSesso" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True">M</asp:ListItem>
                    <asp:ListItem>F</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Codice fiscale*:
            </td>
            <td style="width: 191px">
                <asp:TextBox ID="TextBoxCF" runat="server" MaxLength="16"></asp:TextBox>
            </td>
            <td align="left" style="width: 169px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxCF"
                    ErrorMessage="*" ValidationGroup="inserimentoNotifica"></asp:RequiredFieldValidator>&nbsp;
                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Codice fiscale non valido"
                    OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="inserimentoNotifica"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 190px">
                Data assunzione (gg/mm/aaaa)*:
            </td>
            <td style="width: 191px">
                <asp:TextBox ID="TextBoxDataAssunzione" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td align="left" style="width: 169px">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxDataAssunzione"
                    ErrorMessage="*" ValidationGroup="inserimentoNotifica"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidatorDataAssunzioneTipo" runat="server" ControlToValidate="TextBoxDataAssunzione"
                    ErrorMessage="Formato data non valido." Operator="DataTypeCheck" Type="Date"
                    ValidationGroup="inserimentoNotifica"></asp:CompareValidator><br />
                <asp:CompareValidator ID="CompareValidatorDataAssunzione" runat="server" ErrorMessage="La data di assunzione non pu� essere successiva a tre giorni dopo la data odierna"
                    Operator="LessThanEqual" Type="Date" ControlToValidate="TextBoxDataAssunzione"
                    ValidationGroup="inserimentoNotifica"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="ButtonOk" runat="server" Text="Ok" OnClick="ButtonOk_Click" Width="107px"
                    ValidationGroup="inserimentoNotifica" />
            </td>
            <td style="width: 169px">
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Width="157px"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuNotifiche ID="MenuNotifiche1" runat="server" />
</asp:Content>
