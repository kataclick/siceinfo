<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DurcReport.aspx.cs" Inherits="Durc_DurcReport" Theme="CETheme2009" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <div>
        <table class="standardTable">
            <tr align="left" valign="top">
                <td align="left" valign="top" width="0">
                </td>
                <td align="left" valign="top">
                    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Visualizza lista"
                        titolo="Elenco DURC regolari" />
                    <br />
                    Il report consente la visualizzazione dei DURC regolari emessi da Cassa Edile in
                    ordine di data richiesta (dalla pi� recente alla pi� datata). L�utente pu� eseguire
                    una ricerca mirata sulla base dei campi selezionati e compilati sotto riportati.
                    Una volta definiti i parametri (ricerca per ragione sociale impresa, codice impresa,
                    numero protocollo del documento, C.I.P. � Codice Identificativo Pratica � data di
                    emissione del documento, data di richiesta del documento) � possibile visualizzare
                    l'esito della ricerca premendo il tasto "Visualizza". Il report viene aggiornato
                    quotidianamente e tiene traccia dei documenti rilasciati dal 30 giugno 2006.
                </td>
            </tr>
        </table>
        <br />
        <div class="borderedDiv">
            <table class="standardTable">
                <tr>
                    <td width="17%">
                        Ragione sociale:
                    </td>
                    <td width="30%">
                        <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="100%" />
                    </td>
                    <td width="5%">
                    </td>
                    <td width="18%">
                        Protocollo:
                    </td>
                    <td width="30%">
                        <asp:TextBox ID="TextBoxProtocollo" runat="server" Width="100%" />
                    </td>
                </tr>
                <tr>
                    <td width="17%">
                        Cod.Fisc./P.IVA:
                    </td>
                    <td width="30%">
                        <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%" />
                    </td>
                    <td width="5%">
                    </td>
                    <td width="18%">
                    </td>
                    <td width="30%">
                    </td>
                </tr>
                <tr>
                    <td>
                        Data emissione da:
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerDataEmissioneDa" runat="server" Width="100%" />
                    </td>
                    <td>
                    <asp:CompareValidator ID="CompareValidatorData" ControlToValidate="RadDatePickerDataEmissioneA"
                            ControlToCompare="RadDatePickerDataEmissioneDa" Operator="GreaterThanEqual" ErrorMessage="La data di inizio deve essere precedente alla data di fine per il parametro Data Emissione"
                            ValidationGroup="filtro" runat="server" Type="Date">*</asp:CompareValidator>
                    </td>
                    <td>
                        Data emissione a:
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerDataEmissioneA" runat="server" Width="100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Data richiesta da:
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerDataRichiestaDa" runat="server" Width="100%" />
                    </td>
                    <td>
                    <asp:CompareValidator ID="CompareValidator1" ControlToValidate="RadDatePickerDataRichiestaA"
                            ControlToCompare="RadDatePickerDataRichiestaDa" Operator="GreaterThanEqual" ErrorMessage="La data di inizio deve essere precedente alla data di fine per il parametro Data Richiesta"
                            ValidationGroup="filtro" runat="server" Type="Date">*</asp:CompareValidator>
                    </td>
                    <td>
                        Data richiesta a:
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerDataRichiestaA" runat="server" Width="100%" />
                    </td>
                </tr>
            </table>
            <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
                Text="Visualizza" Width="137px" ValidationGroup="filtro"/>
            <asp:ValidationSummary ID="ValidationSummaryDate" runat="server" ValidationGroup="filtro" />
        </div>
        <table style="height: 600pt; width: 550pt;">
            <tr>
                <td>
                    <rsweb:ReportViewer ID="ReportViewerDurc" runat="server" Font-Names="Verdana" Font-Size="8pt"
                        OnInit="ReportViewerDurc_Init" ProcessingMode="Remote" ShowExportControls="False"
                        ShowPrintButton="False" SizeToReportContent="True" DocumentMapCollapsed="True"
                        Height="550pt" Width="550pt" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
