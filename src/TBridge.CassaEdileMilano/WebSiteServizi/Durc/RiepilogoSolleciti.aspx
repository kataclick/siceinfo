<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RiepilogoSolleciti.aspx.cs" Inherits="Durc_RiepilogoSolleciti" Theme="CETheme2009Wide" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/RichiesteSollecitoRicerca.ascx" TagName="RichiesteSollecitoRicerca"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Richieste DURC"
        sottoTitolo="Riepilogo richieste pervenute" />
    <br />
    <uc2:RichiesteSollecitoRicerca ID="RichiesteSollecitoRicerca1" runat="server" />
</asp:Content>
