﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Presenter;

public partial class Durc_DurcLuccaReport : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Per evitare SessionExpired su Explorer
        Response.AppendHeader("P3P", @"CP=""CAO PSA OUR""");
    }

    protected void ReportViewerDurc_Init(object sender, EventArgs e)
    {
        ReportViewerDurc.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        ReportViewerDurc.ServerReport.ReportPath = "/ReportDURC/DURCLucca";
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        Page.Validate("durc");

        if (Page.IsValid)
        {
            ReportViewerDurc.Reset();

            ReportViewerDurc.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerDurc.ServerReport.ReportPath = "/ReportDURC/DURCLucca";

            List<ReportParameter> listaParam = new List<ReportParameter>();
            if (!string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
                listaParam.Add(new ReportParameter("ragioneSociale", TextBoxRagioneSociale.Text));

            if (!string.IsNullOrEmpty(TextBoxDataEmissioneDa.Text))
            {
                listaParam.Add(new ReportParameter("dataEmissioneDa",
                                                   DateTime.Parse(TextBoxDataEmissioneDa.Text.Replace('.', '/')).ToShortDateString()));
            }
            //else
            //    listaParam.Add(new ReportParameter("dataEmissioneDa"));

            if (!string.IsNullOrEmpty(TextBoxDataEmissioneA.Text))
            {
                listaParam.Add(new ReportParameter("dataEmissioneA",
                                                   DateTime.Parse(TextBoxDataEmissioneA.Text.Replace('.', '/')).ToShortDateString()));
            }
            //else
            //    listaParam.Add(new ReportParameter("dataEmissioneA"));

            //if (!string.IsNullOrEmpty(TextBoxDataRichiestaDa.Text))
            //{
            //    listaParam.Add(new ReportParameter("dataRichiestaDa",
            //                                       DateTime.Parse(TextBoxDataRichiestaDa.Text.Replace('.', '/')).ToShortDateString()));
            //}
            //else
            //    listaParam.Add(new ReportParameter("dataRichiestaDa"));

            if (!String.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
            {
                listaParam.Add(new ReportParameter("codiceFiscale",
                                                   Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text)));
            }

            //if (!string.IsNullOrEmpty(TextBoxDataRichiestaA.Text))
            //{
            //    listaParam.Add(new ReportParameter("dataRichiestaA",
            //                                       DateTime.Parse(TextBoxDataRichiestaA.Text.Replace('.', '/')).ToShortDateString()));
            //}
            //else
            //    listaParam.Add(new ReportParameter("dataRichiestaA"));

            if (!String.IsNullOrEmpty(DropDownListPrivatoPubblico.SelectedValue))
            {
                listaParam.Add(new ReportParameter("privato", DropDownListPrivatoPubblico.SelectedValue == "PR" ? "true" : "false"));
            }

            if (!string.IsNullOrEmpty(TextBoxProtocollo.Text))
                listaParam.Add(new ReportParameter("protocollo", TextBoxProtocollo.Text));

            ReportViewerDurc.ServerReport.SetParameters(listaParam);

            ReportViewerDurc.Visible = true;
        }
        else
        {
            ReportViewerDurc.Visible = false;
        }
    }
}