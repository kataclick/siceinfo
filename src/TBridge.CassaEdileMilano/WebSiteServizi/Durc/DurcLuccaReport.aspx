﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DurcLuccaReport.aspx.cs"
    Inherits="Durc_DurcLuccaReport"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width:665px; background-color:#2A58A4; color:White;">
            <tr>
                <td>
                    <asp:Label ID="LabelRagioneSociale" runat="server" Text="Ragione sociale" Font-Names="Tahoma"
                        ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Label ID="LabelDataEmissioneDa" runat="server" 
                        Text="Data emissione da (gg/mm/aaaa)"
                        Font-Names="Tahoma" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataEmissioneDa" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBoxDataEmissioneDa"
                        ErrorMessage="Data emissione da: formato non corretto." Operator="DataTypeCheck"
                        Type="Date" ValidationGroup="durc">*</asp:CompareValidator>
                </td>
                <td>
                    <asp:Label ID="LabelDataEmissioneA" runat="server" 
                        Text="Data emissione a (gg/mm/aaaa)"
                        Font-Names="Tahoma" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataEmissioneA" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Data emissione a: formato non corretto."
                        Operator="DataTypeCheck" Type="Date" ControlToValidate="TextBoxDataEmissioneA"
                        ValidationGroup="durc">*</asp:CompareValidator>
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToCompare="TextBoxDataEmissioneDa"
                        ControlToValidate="TextBoxDataEmissioneA" ErrorMessage="Data emissione a minore di data emissione da"
                        Operator="GreaterThanEqual" ValidationGroup="durc">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelProtocollo" runat="server" Text="Protocollo" Font-Names="Tahoma"
                        ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxProtocollo" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                </td>
                <td>
                    <%--<asp:Label ID="LabelDataRichiestaDa" runat="server" 
                        Text="Data richiesta da (gg/mm/aaaa)"
                        Font-Names="Tahoma" ></asp:Label>--%>
                    <asp:Label ID="LabelCodiceFiscale" runat="server" 
                        Text="Codice fiscale"
                        Font-Names="Tahoma" ></asp:Label>
                </td>
                <td>
                    <%--<asp:TextBox ID="TextBoxDataRichiestaDa" runat="server" Width="100px"></asp:TextBox>--%>
                    <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <%--<asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="TextBoxDataRichiestaDa"
                        ErrorMessage="Data richiesta da: formato non corretto." Operator="DataTypeCheck"
                        Type="Date" ValidationGroup="durc">*</asp:CompareValidator>--%>
                </td>
                <td>
                    <%--<asp:Label ID="LabelDataRichiestaA" runat="server" 
                        Text="Data richiesta a (gg/mm/aaaa)"
                        Font-Names="Tahoma" ></asp:Label>--%>
                    <asp:Label ID="LabelPrivatoPubblico" runat="server" 
                        Text="Tipologia lavori"
                        Font-Names="Tahoma" ></asp:Label>
                </td>
                <td>
                    <%--<asp:TextBox ID="TextBoxDataRichiestaA" runat="server" Width="100px"></asp:TextBox>--%>
                    <asp:DropDownList ID="DropDownListPrivatoPubblico" runat="server" Width="100px">
                        <asp:ListItem Selected="True" Value="">Tutti</asp:ListItem>
                        <asp:ListItem Value="PR">Privati</asp:ListItem>
                        <asp:ListItem Value="PU">Pubblici</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <%--<asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="TextBoxDataRichiestaA"
                        ErrorMessage="Data richiesta a: formato non corretto." Operator="DataTypeCheck"
                        Type="Date" ValidationGroup="durc">*</asp:CompareValidator>
                    <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToCompare="TextBoxDataRichiestaDa"
                        ControlToValidate="TextBoxDataRichiestaA" ErrorMessage="Data richiesta a minore di data emissione da"
                        Operator="GreaterThanEqual" ValidationGroup="durc" Type="Date">*</asp:CompareValidator>--%>
                </td>
            </tr>
        </table>
        <table class="standardTable">
            <tr>
                <td width="100">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="durc"
                        Font-Names="Tahoma"  Width="300px" />
                    <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                        Text="Visualizza" />
                </td>
            </tr>
        </table>
        <table style="width:665px">
            <tr>
                <td width="100">
                    <rsweb:ReportViewer ID="ReportViewerDurc" runat="server" ProcessingMode="Remote"
                        Width="665px" ShowDocumentMapButton="false" ShowFindControls="False" ShowRefreshButton="False"
                        ShowZoomControl="False" Height="660px" oninit="ReportViewerDurc_Init" 
                        ShowExportControls="False" ShowPrintButton="False" >
                    </rsweb:ReportViewer>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>