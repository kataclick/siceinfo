using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;

public partial class Durc_DurcReport : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DURCVisualizzaLista);
    }

    protected void ReportViewerDurc_Init(object sender, EventArgs e)
    {
        ReportViewerDurc.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        if (Request.QueryString["path"] == null)
            ReportViewerDurc.ServerReport.ReportPath = "/ReportDURC/DURC";
        else
            ReportViewerDurc.ServerReport.ReportPath = Request.QueryString["path"];
    }

    protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
    {
        const string nothing = null;

        if (Page.IsValid)
        {
            List<ReportParameter> listaParam = new List<ReportParameter>();
            listaParam.Add(new ReportParameter("RagioneSociale", Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text)));
            listaParam.Add(new ReportParameter("CodiceFiscale", Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text)));

            
            if (RadDatePickerDataEmissioneDa.SelectedDate != null)
            {
                listaParam.Add(new ReportParameter("DataEmissioneDa",
                                                   RadDatePickerDataEmissioneDa.SelectedDate.Value.ToShortDateString()));
            }
            else listaParam.Add(new ReportParameter("DataEmissioneDa", nothing)); 

            if (RadDatePickerDataEmissioneA.SelectedDate != null)
            {
                listaParam.Add(new ReportParameter("DataEmissioneA",
                                                   RadDatePickerDataEmissioneA.SelectedDate.Value.ToShortDateString()));
            }
            else listaParam.Add(new ReportParameter("DataEmissioneA", nothing)); 

            if (RadDatePickerDataRichiestaDa.SelectedDate != null)
            {
                listaParam.Add(new ReportParameter("DataRichiestaDa",
                                                   RadDatePickerDataRichiestaDa.SelectedDate.Value.ToShortDateString()));
            }
            else listaParam.Add(new ReportParameter("DataRichiestaDa", nothing)); 
            if (RadDatePickerDataRichiestaA.SelectedDate != null)
            {
                listaParam.Add(new ReportParameter("DataRichiestaA",
                                                   RadDatePickerDataRichiestaA.SelectedDate.Value.ToShortDateString()));
            }
            else listaParam.Add(new ReportParameter("DataRichiestaA", nothing)); 

            listaParam.Add(new ReportParameter("Protocollo", TextBoxProtocollo.Text));

            ReportViewerDurc.ServerReport.SetParameters(listaParam);
        }
    }
}