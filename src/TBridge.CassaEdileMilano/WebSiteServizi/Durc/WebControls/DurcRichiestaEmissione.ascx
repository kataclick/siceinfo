﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DurcRichiestaEmissione.ascx.cs"
    Inherits="Durc_WebControls_DurcRichiestaEmissione" %>
<script type="text/javascript">
    var datePicker;

    function onLoadRadDatePickerDataInserimento(sender, args) {
        datePicker = sender;
    }

    function validate(sender, eventArgs) {
        var data = datePicker.get_selectedDate();

        var dataMinima = new Date();
        dataMinima.setDate(dataMinima.getDate() - 3);

        if (data > dataMinima) {
            eventArgs.IsValid = false;
        }
        else {
            eventArgs.IsValid = true;
        }
    }

    function ValidateTelefono(source, args) {
        var chkRitiro = document.getElementById('<%= CheckBoxRitiroCe.ClientID %>');
        var txtTelefono = document.getElementById('<%= RadTextBoxTelefono.ClientID %>');

        if (chkRitiro.checked != false) {
            if (txtTelefono.value == "")
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }
</script>
<table class="standardTable">
    <tr>
        <td class="SollecitoDurcTd">
            Codice di iscrizione impresa in Cassa Edile
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxIdImpresa" runat="server" EmptyMessage="Seleziona un'impresa"
                Width="280px" Enabled="False">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxIdImpresa" ErrorMessage="Seleziona un'impresa" ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Codice Fiscale/P. IVA impresa
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxCf" runat="server" EmptyMessage="Seleziona un'impresa"
                Width="280px" Enabled="False">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxCf" ErrorMessage="Seleziona un'impresa" ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Ragione Sociale impresa
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxRagionesociale" runat="server" EmptyMessage="Seleziona un'impresa"
                Width="280px" Enabled="False">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxRagioneSociale" ErrorMessage="Seleziona un'impresa"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo e-mail (*)
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxEmail" runat="server" EmptyMessage="Inserisci l'indirizzo e-mail"
                Width="280px">
            </telerik:RadTextBox>
            <asp:RegularExpressionValidator ID="emailValidator" runat="server" Display="Dynamic"
                ErrorMessage="Inserisci un indirizzo e-mail valido." ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,4}){1,2}$"
                ControlToValidate="RadTextBoxEmail" ForeColor="Red" />
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxEmail" ErrorMessage="Inserisci un indirizzo e-mail."
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Numero C.I.P. (Codice Identificativo Pratica DURC) (*)
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxCIP" runat="server" EmptyMessage="Inserisci il C.I.P."
                Width="280px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxCIP" ErrorMessage="Inserisci il C.I.P." ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Numero Protocollo (DURC) (*)
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxProtocollo" runat="server" EmptyMessage="Inserisci il numero protocollo"
                Width="280px">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxProtocollo" ErrorMessage="Inserisci il numero protocollo"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Data inserimento pratica DURC sul sito dello Sportello Unico Previdenziale (*)
        </td>
        <td colspan="2">
            <telerik:RadDatePicker ID="RadDatePickerDataInserimento" runat="server" Width="280px">
                <DateInput ID="DateInputDataInserimento" runat="server">
                    <ClientEvents OnLoad="onLoadRadDatePickerDataInserimento" />
                </DateInput>
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredfieldValidatorData" runat="server" Display="Dynamic"
                ControlToValidate="RadDatePickerDataInserimento" ErrorMessage="Inserisci la data della richiesta"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
            <asp:CustomValidator ID="CustomValidatorData" EnableClientScript="true" runat="server"
                Display="Static" ControlToValidate="RadDatePickerDataInserimento" ErrorMessage="Deve essere inserita da almeno 3 giorni"
                ClientValidationFunction="validate" ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <%--<tr>
        <td  >
            L’impresa è regolare presso Cassa Edile di Milano?
        </td>
        <td colspan="2">
            <telerik:RadComboBox ID="RadComboBoxRegolarita" runat="server" Width="280px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Sì" Value="Si" />
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="No" Value="No" />
                </Items>
            </telerik:RadComboBox>
            <asp:CompareValidator ValueToCompare="No" Operator="NotEqual" Display="Dynamic" ControlToValidate="RadComboBoxRegolarita"
                ErrorMessage="Non potrai inviare il sollecito" runat="server" ID="Comparevalidator3"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td  >
            L’impresa è regolare presso le altre Casse Edili ?
        </td>
        <td colspan="2">
            <telerik:RadComboBox ID="RadComboBoxRegolaritaAltreCE" runat="server" Width="280px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Sì" Value="Si" />
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="No" Value="No" />
                    <telerik:RadComboBoxItem runat="server" Text="Non Iscritta" Value="Non Iscritta" />
                </Items>
            </telerik:RadComboBox>
            <asp:CompareValidator ValueToCompare="No" Operator="NotEqual" Display="Dynamic" ControlToValidate="RadComboBoxRegolaritaAltreCE"
                ErrorMessage="Non potrai inviare il sollecito" runat="server" ID="Comparevalidator2"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>--%>
    <tr>
        <td>
            Validato INAIL (*)
        </td>
        <td colspan="2">
            <telerik:RadComboBox ID="RadComboBoxValidatoInail" runat="server" Width="280px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Sì" Value="Si" />
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="No" Value="No" />
                </Items>
            </telerik:RadComboBox>
            <asp:CompareValidator ValueToCompare="No" Operator="NotEqual" Display="Dynamic" ControlToValidate="RadComboBoxValidatoInail"
                ErrorMessage="Non potrai inviare la richiesta" runat="server" ID="Comparevalidator4"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Validato INPS (*)
        </td>
        <td colspan="2">
            <telerik:RadComboBox ID="RadComboBoxValidatoInps" runat="server" Width="280px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Sì" Value="Si" />
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="No" Value="No" />
                </Items>
            </telerik:RadComboBox>
            <asp:CompareValidator ValueToCompare="No" Operator="NotEqual" Display="Dynamic" ControlToValidate="RadComboBoxValidatoInps"
                ErrorMessage="Non potrai inviare la richiesta" runat="server" ID="Comparevalidator1"
                ValidationGroup="ButtonConferma" ForeColor="Red" />
        </td>
    </tr>
    <%--<tr>
        <td  >
            Sono trascorsi almeno 3 giorni dalla richiesta?
        </td>
        <td colspan="2">
            <telerik:RadComboBox ID="RadComboRitardoEmissione" runat="server" Width="280px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="Sì" Value="Si" />
                    <telerik:RadComboBoxItem runat="server" Selected="True" Text="No" Value="No" />
                </Items>
            </telerik:RadComboBox>
        </td>
    </tr>--%>
    <tr id="Tr1" runat="server" style="display:none">
        <td>
            Ritiro in sede Cassa Edile a Milano
        </td>
        <td colspan="2">
            <asp:CheckBox ID="CheckBoxRitiroCe" runat="server" Checked="false"/>
        </td>
    </tr>
    <tr id="Tr2" runat="server">
        <td>
            Recapito telefonico
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxTelefono" runat="server" Width="280px" />
            <asp:CustomValidator ID="CustomValidatorTelefono" runat="server" ClientValidationFunction="ValidateTelefono"
                ErrorMessage="Fornire un recapito telefonico" ForeColor="Red" />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="RadTextBoxTelefono"
                                                    ErrorMessage="Il telefono inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliSedeLegale"
                                                    CssClass="messaggiErrore" ForeColor="Red" />
        </td>
    </tr>
    <tr>
        <td>
            Descrizione del motivo della richiesta
        </td>
        <td colspan="2">
            <telerik:RadTextBox ID="RadTextBoxMotiviSollecito" runat="server" EmptyMessage="Inserisci le motivazioni della richiesta."
                Height="158px" Width="280px" TextMode="MultiLine">
            </telerik:RadTextBox>
            <%--<asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                ControlToValidate="RadTextBoxMotiviSollecito" ErrorMessage="Inserisci la motivazione"
                ValidationGroup="ButtonConferma" ForeColor="Red" />--%>
        </td>
    </tr>
    <tr>
        <td />
        <td colspan="2">
            <asp:Button ID="ButtonConferma" runat="server" Text="Invia richiesta" OnClick="ButtonConferma_Click"
                CausesValidation="true" ValidationGroup="ButtonConferma" />
        </td>
    </tr>
</table>