﻿using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Durc.Business;
using TBridge.Cemi.Durc.Type.Delegates;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;

public partial class Durc_WebControls_DurcRichiestaEmissione : UserControl
{
    private readonly SollecitiBizEf _sollecitiBiz = new SollecitiBizEf();

    public event SollecitoEmissioneInsertedEventHandler OnSollecitoEmissioneInserted;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null) + ";");
        sb.Append("return true;");
        ButtonConferma.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonConferma);
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        DurcSollecito sollecitoEmissione = new DurcSollecito();
        bool prerequisitiSoddisfatti = PrerequisitiSoddisfatti();
        if (prerequisitiSoddisfatti)
        {
            sollecitoEmissione = InserisciSollecitoEmissione();
        }
        else
            RadDatePickerDataInserimento.SelectedDate = null;
        if (OnSollecitoEmissioneInserted != null)
            OnSollecitoEmissioneInserted(prerequisitiSoddisfatti, sollecitoEmissione);
    }

    private bool PrerequisitiSoddisfatti()
    {
        return Page.IsValid;
    }

    private DurcSollecito InserisciSollecitoEmissione()
    {
        DurcSollecito sollecitoDurc = new DurcSollecito
                                          {
                                              Cip = RadTextBoxCIP.Text,
                                              EMail = RadTextBoxEmail.Text,
                                              IdUtente = GestioneUtentiBiz.GetIdUtente(),
                                              MotivoRichiesta = RadTextBoxMotiviSollecito.Text,
                                              NumeroProtocollo = RadTextBoxProtocollo.Text,
                                              IdImpresa = Convert.ToInt32(RadTextBoxIdImpresa.Text),
                                              DataInserimentoRecord = DateTime.Now,
                                              RitiroSede = CheckBoxRitiroCe.Checked,
                                              NumeroTelefono = !String.IsNullOrEmpty(RadTextBoxTelefono.Text) ? RadTextBoxTelefono.Text : null
                                          };

        return _sollecitiBiz.InserisciSollecito(sollecitoDurc);
    }
}