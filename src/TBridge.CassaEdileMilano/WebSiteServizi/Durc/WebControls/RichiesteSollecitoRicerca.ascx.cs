﻿using System;
using System.Web.UI;
using TBridge.Cemi.Durc.Business;
using TBridge.Cemi.Durc.Type.Collections;
using TBridge.Cemi.Durc.Type.Filters;
using Telerik.Web.UI;

public partial class Durc_WebControls_RichiesteSollecitoRicerca : UserControl
{
    private readonly SollecitiBusiness sollecitiBiz = new SollecitiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RadDatePickerDataDa.SelectedDate = DateTime.Today.AddMonths(-1);
        }
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        Page.Validate("filtro");
        if (Page.IsValid)
        {
            RadGridRichiesteSollecito.Rebind();
        }
    }

    protected void RadGridRichiesteSollecito_ExcelExportCellFormatting(object source,
                                                                       ExcelExportCellFormattingEventArgs e)
    {
        switch (e.FormattedColumn.HeaderText)
        {
            default:
                e.Cell.Style["mso-number-format"] = @"\@";
                break;
            case "DataRichiesta":
                e.Cell.Style["mso-number-format"] = @"mm\/dd\/yyyy";
                break;
        }
    }
    
    protected void ButtonExportExcel_Click(object sender, EventArgs e)
    {
        RadGridRichiesteSollecito.ExportSettings.IgnorePaging = true;
        RadGridRichiesteSollecito.ExportSettings.ExportOnlyData = false;
        RadGridRichiesteSollecito.ExportSettings.FileName = "Durc_Riepilogo_Richieste";
        RadGridRichiesteSollecito.MasterTableView.ExportToExcel();
    }

    protected void RadGridRichiesteSollecito_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        SollecitoEmissioneFilter filtro = new SollecitoEmissioneFilter
        {
            CodiceImpresa = (int?)RadTextBoxIdImpresa.Value,
            RagioneSociale = RadTextBoxRagioneSociale.Text,
            DataInizio = RadDatePickerDataDa.SelectedDate,
            DataFine = RadDatePickerDataA.SelectedDate,
            Cip = RadTextBoxCip.Text,
            NumeroProtocollo = RadTextBoxNumeroProtocollo.Text
        };

        SollecitoEmissioneCollection solleciti = sollecitiBiz.GetRichiesteSollecito(filtro);

        RadGridRichiesteSollecito.DataSource = solleciti;
    }
}