﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RichiesteSollecitoRicerca.ascx.cs"
    Inherits="Durc_WebControls_RichiesteSollecitoRicerca" %>
<asp:Panel ID="ListaBollettini" runat="server" DefaultButton="ButtonFiltra">
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td width="15%">
                    Codice Impresa:
                </td>
                <td width="30%">
                    <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                        ID="RadTextBoxIdImpresa" runat="server" Width="100%">
                    </telerik:RadNumericTextBox>
                </td>
                <td width="7%">
                </td>
                <td width="18%">
                    Data richiesta da:
                </td>
                <td width="30%">
                    <telerik:RadDatePicker ID="RadDatePickerDataDa" Width="100%" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Ragione sociale:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="100%">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
                <td>
                    Data richiesta a:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataA" runat="server" Width="100%" />
                </td>
            </tr>
            <tr>
                <td >
                    CIP:
                </td>
                <td >
                    <telerik:RadTextBox ID="RadTextBoxCip" runat="server" Width="100%" />
                </td>
                <td >
                </td>
                <td >
                    Numero Protocollo:
                </td>
                <td >
                    <telerik:RadTextBox ID="RadTextBoxNumeroProtocollo" runat="server" Width="100%" />
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right">
                    <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Filtra"
                        ValidationGroup="filtro" />
                    <br />
                    <asp:CompareValidator ID="CompareValidatorData" ControlToValidate="RadDatePickerDataA"
                        ControlToCompare="RadDatePickerDataDa" Operator="GreaterThanEqual" ErrorMessage="La data di fine non può essere precedente a quella di inizio"
                        ValidationGroup="filtro" runat="server" Type="Date" />
                </td>
            </tr>
        </table>
        <div style="padding: 5px;">
            <telerik:RadGrid ID="RadGridRichiesteSollecito" runat="server" AllowPaging="true"
                OnNeedDataSource="RadGridRichiesteSollecito_NeedDataSource"
                OnExcelExportCellFormatting="RadGridRichiesteSollecito_ExcelExportCellFormatting" >
                <ExportSettings ExportOnlyData="true" IgnorePaging="true" />
                <MasterTableView>
                    <Columns>
                        <telerik:GridDateTimeColumn DataField="DataRichiesta" HeaderText="Data Richiesta"
                            DataFormatString="{0:dd/MM/yyyy}" />
                        <telerik:GridBoundColumn DataField="Cip" HeaderText="CIP" />
                        <telerik:GridBoundColumn DataField="NumeroProtocollo" HeaderText="Numero Protocollo" />
                        <telerik:GridBoundColumn DataField="IdImpresa" HeaderText="Codice Impresa" />
                        <telerik:GridBoundColumn DataField="RagioneSociale" HeaderText="Ragione Sociale Impresa" />
                        <telerik:GridBoundColumn DataField="Email" HeaderText="E-mail" />
                        <telerik:GridBoundColumn DataField="Telefono" HeaderText="Telefono" />
                        <telerik:GridBoundColumn DataField="MotivoRichiesta" HeaderText="Motivo" />
                        <telerik:GridBoundColumn DataField="IdConsulente" HeaderText="Codice Consulente" />
                        <telerik:GridBoundColumn DataField="RagioneSocialeConsulente" HeaderText="Ragione Sociale Consulente" />
                        <telerik:GridCheckBoxColumn DataField="RitiroSede" HeaderText="Ritiro in sede" />
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
        <asp:Button ID="ButtonExportExcel" Width="150px" Text="Esporta" OnClick="ButtonExportExcel_Click"
            runat="server" />
    </div>
</asp:Panel>
