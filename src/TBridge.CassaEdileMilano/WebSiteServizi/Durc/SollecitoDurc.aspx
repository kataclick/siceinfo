﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SollecitoDurc.aspx.cs" Inherits="Durc_SollecitoDurc" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/DurcRichiestaEmissione.ascx" TagName="DurcRichiestaEmissione"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Richiesta emissione urgente di DURC"
        sottoTitolo="Richiesta emissione urgente di DURC" />
    <br />
    <asp:Label ID="LabelIntestazione" runat="server" Text="Nel ricordare che i tempi previsti dalla normativa vigente per il rilascio del Documento Unico di Regolarità Contributiva (DURC) 
    sono di <b>30 giorni</b> dalla data della richiesta di emissione (articolo 6 Decreto Ministeriale 24.10.2007), si precisa che questa funzione consente alle <b>SOLE IMPRESE REGOLARI</b> 
    di poter attivare una procedura di emissione urgente del certificato di regolarità contributiva.<br />
    La procedura consente all’impresa, che deve motivare l’urgenza della richiesta, di ottenere il tempestivo rilascio del documento di regolarità contributiva 
    (dopo 5 giorni lavorativi dalla data dell’inserimento della richiesta tramite lo Sportello Unico Previdenziale <a href='http://www.sportellounicoprevidenziale.it'>www.sportellounicoprevidenziale.it</a>) al verificarsi delle seguenti condizioni:
    <ul>
    <li>la data della richiesta effettuata tramite il sito internet www.sportellounicoprevidenziale.it deve essere di tre giorni precedente l’utilizzo della presente funzione;</li>
    <li>l’impresa deve essere regolare per il sistema delle Casse Edili;</li>
    <li>l’INPS e l’INAIL devono aver già istruito e validato la pratica.</li>
    </ul>
    Per maggiori informazioni consulta le relative <a href='http://ww2.cassaedilemilano.it/ImpreseeConsulenti/FAQDomandePi%C3%B9Frequenti/FAQDURC/tabid/189/language/it-IT/Default.aspx'>FAQ</a>.
	<br />
    <br />
    Per poter procedere con la richiesta di emissione urgente, completare i dati richiesti nel format sotto riportato:" />
    <br />
    <br />
    <uc3:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa" runat="server" />
    <br />
    <uc2:DurcRichiestaEmissione ID="DurcRichiestaEmissione1" runat="server" Visible="false" />
</asp:Content>
