using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.EmailClient;
using TBridge.Cemi.Durc.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using Consulente = TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente;
using Impresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;

//using TBridge.Cemi.Durc.Business;

public partial class Durc_SollecitoDurc : Page
{
    private readonly Common commonBiz = new Common();
    private string codiceFiscale;
    private string eMail;
    private string telefono;
    //private readonly SollecitiBusiness durcBiz = new SollecitiBusiness();
    private int idImpresa;
    private string ragioneSociale;
    private Consulente consulente;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DurcSollecito);

        DurcRichiestaEmissione1.OnSollecitoEmissioneInserted += OnSollecitoEmissioneInsert;
        ConsulenteSelezioneImpresa.OnImpresaSelected += OnImpresaSelected;

        ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxIdImpresa")).Enabled = false;
        ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxRagionesociale")).Enabled = false;
        ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxCf")).Enabled = false;

        idImpresa = 0;
        ragioneSociale = String.Empty;
        eMail = String.Empty;
        telefono = String.Empty;
        codiceFiscale = String.Empty;

        if (GestioneUtentiBiz.IsImpresa())
        {
            ConsulenteSelezioneImpresa.Visible = false;
            Impresa impresa = (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            idImpresa = impresa.IdImpresa;
            ragioneSociale = impresa.RagioneSociale;
            eMail = impresa.Email;
            telefono = impresa.TelefonoSedeAmministrazione;
            codiceFiscale = impresa.CodiceFiscale;

            if (idImpresa != -1)
                DurcRichiestaEmissione1.Visible = true;
        }

        if (GestioneUtentiBiz.IsConsulente())
        {
            consulente = (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            eMail = consulente.EMail;
            telefono = consulente.Telefono;
            commonBiz.ConsulenteImpresaSelezionata(consulente.IdConsulente, out idImpresa, out ragioneSociale,
                                                   out codiceFiscale);

            if (idImpresa != -1)
                DurcRichiestaEmissione1.Visible = true;
        }

        if (idImpresa != -1 && !Page.IsPostBack)
        {
            ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxIdImpresa")).Text = idImpresa.ToString();
            ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxRagionesociale")).Text = ragioneSociale;
            ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxCf")).Text = codiceFiscale;
            ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxEmail")).Text = eMail;
            ((RadTextBox) DurcRichiestaEmissione1.FindControl("RadTextBoxTelefono")).Text = telefono;
        }
    }

    private void OnImpresaSelected(int idImpresa, string ragioneSociale)
    {
        
        commonBiz.ConsulenteImpresaSelezionata(consulente.IdConsulente, out idImpresa, out ragioneSociale,
                                                   out codiceFiscale);
    
        if (idImpresa != -1)
        {
            DurcRichiestaEmissione1.Visible = true;
            ((RadTextBox)DurcRichiestaEmissione1.FindControl("RadTextBoxIdImpresa")).Text = idImpresa.ToString();
            ((RadTextBox)DurcRichiestaEmissione1.FindControl("RadTextBoxRagionesociale")).Text = ragioneSociale;
            ((RadTextBox)DurcRichiestaEmissione1.FindControl("RadTextBoxCf")).Text = codiceFiscale;
            ((RadTextBox)DurcRichiestaEmissione1.FindControl("RadTextBoxEmail")).Text = eMail;
        }
        else
        {
            DurcRichiestaEmissione1.Visible = false;
        }
    }

    public void OnSollecitoEmissioneInsert(bool prerequisitiSoddisfatti, DurcSollecito sollecitoEmissione)
    {
        if (!prerequisitiSoddisfatti)
        {
            //LabelIntestazione.Text = "Non si dispone dei requisiti necessari ad inoltrare la richiesta.";
            //ConsulenteSelezioneImpresa.Visible = false;
            //DurcRichiestaEmissione1.Visible = false;
        }
        else
        {
            if (sollecitoEmissione.Id > 0)
            {
                LabelIntestazione.Text =
                    "Gentile Utente la Sua richiesta � stata inoltrata correttamente; in caso di mancato accoglimento della stessa ricever� entro 2 giorni lavorativi un avviso via e-mail, all�indirizzo di posta elettronica segnalato. Diversamente la richiesta si intender� accolta e verr� evasa.";
                ConsulenteSelezioneImpresa.Visible = false;
                DurcRichiestaEmissione1.Visible = false;

                //String addettoArea = durcBiz.GetAddettoAreaImpresa(idImpresa);
                //InviaMail(sollecitoEmissione, addettoArea);
                //InviaMail(sollecitoEmissione, null);
            }
            else
            {
                DurcRichiestaEmissione1.Visible = false;
                ConsulenteSelezioneImpresa.Visible = false;
                LabelIntestazione.Text = "Operazione non andata a buon fine. Si prega di contattare l'ufficio.";
            }
        }
    }

    private void InviaMail(SollecitoEmissione sollecitoEmissione, string addettoArea)
    {
        EmailMessageSerializzabile message = new EmailMessageSerializzabile();
        message.DataInvio = DateTime.Now;
        message.DataSchedulata = DateTime.Now;
        message.Mittente = new EmailAddress(ConfigurationManager.AppSettings["MittenteName"],
                                            ConfigurationManager.AppSettings["MittenteMail"]);
        if (GestioneUtentiBiz.IsImpresa())
        {
            message.Oggetto = String.Format("Richiesta emissione DURC urgente - Impresa: {0} - {1}", idImpresa,
                                            ragioneSociale);

            message.BodyPlain =
                String.Format(
                    "Identificativo sollecito: {0}{1}CIP: {2}{3}Numero Protocollo: {4}{5}Impresa per cui viene richiesto: {6} - {7}{8}E-mail: {9}{10}Motivo richiesta: {11}",
                    sollecitoEmissione.IdDurcSollecito, Environment.NewLine, sollecitoEmissione.Cip, Environment.NewLine,
                    sollecitoEmissione.NumeroProtocollo, Environment.NewLine, idImpresa, ragioneSociale,
                    Environment.NewLine, sollecitoEmissione.EMail, Environment.NewLine,
                    sollecitoEmissione.MotivoRichiesta);
        }
        else
        {
            Consulente consulente = (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            message.Oggetto = String.Format("Richiesta emissione DURC urgente - Richiedente: {0} - Impresa: {1} - {2}",
                                            consulente.RagioneSociale, idImpresa, ragioneSociale);

            message.BodyPlain =
                String.Format(
                    "Identificativo sollecito: {0}{1}CIP: {2}{3}Numero Protocollo: {4}{5}Impresa per cui viene richiesto: {6} - {7}{8}Consulente richiedente: {9} - {10}{11}E-mail: {12}{13}Motivo richiesta: {14}",
                    sollecitoEmissione.IdDurcSollecito, Environment.NewLine, sollecitoEmissione.Cip, Environment.NewLine,
                    sollecitoEmissione.NumeroProtocollo, Environment.NewLine, idImpresa, ragioneSociale,
                    Environment.NewLine, consulente.IdConsulente, consulente.RagioneSociale, Environment.NewLine,
                    sollecitoEmissione.EMail, Environment.NewLine, sollecitoEmissione.MotivoRichiesta);
        }

        message.Destinatari = new List<EmailAddress>();

        string destinatarioName;
        string destinatarioMail;
        switch (addettoArea)
        {
            case "000":
                destinatarioName = "Area 000";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto000Mail"];
                break;
            case "001":
                destinatarioName = "Area 001";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto001Mail"];
                break;
            case "002":
                destinatarioName = "Area 002";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto002Mail"];
                break;
            case "003":
                destinatarioName = "Area 003";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto003Mail"];
                break;
            case "004":
                destinatarioName = "Area 004";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto004Mail"];
                break;
            case "005":
                destinatarioName = "Area 005";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto005Mail"];
                break;
            case "006":
                destinatarioName = "Area 006";
                destinatarioMail = ConfigurationManager.AppSettings["Addetto006Mail"];
                break;
            default:
                destinatarioName = "Ufficio DURC";
                destinatarioMail = ConfigurationManager.AppSettings["AddettoDefaultMail"];
                break;
        }

        message.Destinatari.Add(new EmailAddress(destinatarioName, destinatarioMail));

        EmailInfoService emailInfoService = new EmailInfoService();

        string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
        string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];
        emailInfoService.Credentials = new NetworkCredential(emailUserName, emailPassword);
        emailInfoService.InviaEmail(message);
    }
}