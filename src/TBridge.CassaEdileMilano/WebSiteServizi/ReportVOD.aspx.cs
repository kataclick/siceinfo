using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.VOD.Business;
using TBridge.Cemi.VOD.Type.Entities;
using TBridge.Cemi.VOD.Type.Filters;
using Telerik.Web.UI;
using System.Web;

public partial class ReportVOD : Page
{
    private string _idEsattore;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime dataFine = DateTime.Today;
            DateTime dataInizio = new DateTime(dataFine.Year, dataFine.Month, 1);
            dataInizio = dataInizio.AddMonths(-6);

            RadDatePickerDataDaElencoImprese.SelectedDate = dataInizio;
            RadDatePickerDataAElencoImprese.SelectedDate = dataFine;

            //if (GestioneUtentiBiz.IsEsattore())
            //{
            //    _idEsattore = ((Esattore)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdEsattore;
            //    // ViewState["idEsattore"] = _idEsattore;
            //    RadComboBoxIdEsattoreElencoImprese.SelectedValue = _idEsattore;
            //    RadComboBoxIdEsattoreSchedaImpresa.SelectedValue = _idEsattore;
            //    RadComboBoxIdEsattoreSchedaVCOD.SelectedValue = _idEsattore;
            //}

            ReloadFiltersFromCookie();

            #region Impostiamo i valori dei filtri leggendoli da querystring

            if (!String.IsNullOrEmpty(Request.QueryString["dataDa"]))
            {
                RadDatePickerDataDaElencoImprese.SelectedDate = DateTime.Parse(Request.QueryString["dataDa"]);
                RadDatePickerDataDaSchedaImpresa.SelectedDate = DateTime.Parse(Request.QueryString["dataDa"]);
                RadDatePickerDataDaSchedaVCOD.SelectedDate = DateTime.Parse(Request.QueryString["dataDa"]);
            }

            if (!String.IsNullOrEmpty(Request.QueryString["dataA"]))
            {
                RadDatePickerDataAElencoImprese.SelectedDate = DateTime.Parse(Request.QueryString["dataA"]);
                RadDatePickerDataASchedaImpresa.SelectedDate = DateTime.Parse(Request.QueryString["dataA"]);
                RadDatePickerDataASchedaVCOD.SelectedDate = DateTime.Parse(Request.QueryString["dataA"]);
            }

            if (!String.IsNullOrEmpty(Request.QueryString["idImpresa"]))
            {
                //RadNumericTextBoxIdImpresaElencoImprese.Text = (Request.QueryString["idImpresa"]);
                RadNumericTextBoxIdImpresaSchedaImpresa.Text = (Request.QueryString["idImpresa"]);
                RadNumericTextBoxIdImpresaSchedaVCOD.Text = (Request.QueryString["idImpresa"]);
            }

            //if (!String.IsNullOrEmpty(Request.QueryString["idEsattore"]))
            //{
            //    RadComboBoxIdEsattoreElencoImprese.SelectedValue = (Request.QueryString["idEsattore"]);
            //    RadComboBoxIdEsattoreSchedaImpresa.SelectedValue = (Request.QueryString["idEsattore"]);
            //    RadComboBoxIdEsattoreSchedaVCOD.SelectedValue = (Request.QueryString["idEsattore"]);
            //}

            #endregion
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.VodElencoImpreseConsulenti,
                                                             FunzionalitaPredefinite.VodElencoImpreseMedie
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        //if (GestioneUtentiBiz.IsEsattore())
        //{
        //    _idEsattore = ((Esattore) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdEsattore;
        //   // ViewState["idEsattore"] = _idEsattore;
        //    RadComboBoxIdEsattoreElencoImprese.SelectedValue = _idEsattore;
        //    RadComboBoxIdEsattoreSchedaImpresa.SelectedValue = _idEsattore;
        //    RadComboBoxIdEsattoreSchedaVCOD.SelectedValue = _idEsattore;
        //}
    }

    protected void ReportViewerVOD_Init(object sender, EventArgs e)
    {
        ReportViewerVOD.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        //con la gestione dei controlli esterni al report vi sono delle fasi di postback in pi�, 
        //queste fanno perdere il parametro in querystring
        string tipoReport;

        if (!String.IsNullOrEmpty(Request.QueryString["tipo"]))
        {
            tipoReport = Request.QueryString["tipo"];
            ViewState["tipo"] = tipoReport;
        }
        else
        {
            tipoReport = (String) ViewState["tipo"];
        }

        switch (tipoReport)
        {
            case "ImpreseEsattori":
                PanelElencoImpreseFascia.Visible = false;
                PanelSchedaImpresa.Visible = false;
                PanelSchedaVCOD.Visible = false;
                ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportImpreseConsulenti";
                TitoloSottotitolo1.sottoTitolo = "Elenco Imprese - Consulenti - Esattori";
                ImpostaVisualizzazionePortrait();
                break;
            case "scheda":
                PanelElencoImpreseFascia.Visible = false;
                PanelSchedaImpresa.Visible = true;
                PanelSchedaVCOD.Visible = false;
                TitoloSottotitolo1.sottoTitolo = "Scheda impresa";
                ImpostaVisualizzazioneLandscape();
                break;
            case "ImpreseMedie":
                PanelElencoImpreseFascia.Visible = true;
                PanelSchedaImpresa.Visible = false;
                PanelSchedaVCOD.Visible = false;
                TitoloSottotitolo1.sottoTitolo = "Elenco Imprese ordinate per fascia";
                ImpostaVisualizzazionePortrait();
                break;
            case "reportVCOD":
                PanelElencoImpreseFascia.Visible = false;
                PanelSchedaImpresa.Visible = false;
                PanelSchedaVCOD.Visible = true;
                TitoloSottotitolo1.sottoTitolo = "VCOD";
                ImpostaVisualizzazioneLandscape();
                break;
            default:
                PanelElencoImpreseFascia.Visible = false;
                PanelSchedaImpresa.Visible = false;
                PanelSchedaVCOD.Visible = false;
                break;
        }
    }

    private void ImpostaVisualizzazioneLandscape()
    {
        //Impostiamo la visualizzazione dei menu
        ReportViewerVOD.Width = new Unit(1150);
    }

    private void ImpostaVisualizzazionePortrait()
    {
        //Impostiamo la visualizzazione dei menu
        ReportViewerVOD.Width = new Unit(950);
    }

    //creato per risolvere problema "ReportViewer HyperLinkTarget property problem"
    //http://social.msdn.microsoft.com/forums/en-US/vsreportcontrols/thread/d6eb7629-d250-4bdb-ae48-7dae6c3f03db/
    public void void_name(object sender, PageNavigationEventArgs e)
    {
        ReportViewerVOD.HyperlinkTarget = "_top";
    }

    protected void ButtonTornaElencoImprese_Click(object sender, EventArgs e)
    {
        string url = Request.Url.ToString().Replace("scheda", "ImpreseMedie");
        
        Response.Redirect(url);
    }

    protected void ButtonTornaSchedaImpresa_Click(object sender, EventArgs e)
    {
        string url = Request.Url.ToString().Replace("reportVCOD", "scheda");
        Response.Redirect(url);
    }

    protected void ButtonEsportaConIndirizzi_Click(object sender, EventArgs e)
    {
        Stampa();
        //string stampa = PreparaStampa(imprese);

        //Response.ClearContent();
        //Response.AddHeader("content-disposition", "attachment; filename=Imprese.xls");
        //Response.ContentType = "application/vnd.ms-excel";
        //Response.Write(stampa);
        //Response.End();
    }

    private void Stampa()
    {
        RadGrid radGridVcod = new RadGrid();

        radGridVcod.NeedDataSource += RadGridVcod_NeedDataSource;

        radGridVcod.AutoGenerateColumns = false;

        GridBoundColumn bc1 = new GridBoundColumn {HeaderText = "Codice Impresa", DataField = "IdImpresa"};
        GridBoundColumn bc2 = new GridBoundColumn {HeaderText = "Ragione Sociale", DataField = "RagioneSociale"};

        GridBoundColumn bc3 = new GridBoundColumn {HeaderText = "Via", DataField = "IndirizzoContatto.IndirizzoBase"};
        GridBoundColumn bc4 = new GridBoundColumn {HeaderText = "Comune", DataField = "IndirizzoContatto.Comune"};
        GridBoundColumn bc5 = new GridBoundColumn
                                  {
                                      HeaderText = "Provincia",
                                      DataField = "IndirizzoContatto.Provincia"
                                  };
        GridBoundColumn bc6 = new GridBoundColumn {HeaderText = "Cap", DataField = "IndirizzoContatto.Cap"};

        GridBoundColumn bc7 = new GridBoundColumn
                                  {
                                      HeaderText = "Via (Consulente)",
                                      DataField = "IndirizzoConsulente.IndirizzoBase"
                                  };
        GridBoundColumn bc8 = new GridBoundColumn
                                  {
                                      HeaderText = "Comune (Consulente)",
                                      DataField = "IndirizzoConsulente.Comune"
                                  };
        GridBoundColumn bc9 = new GridBoundColumn
                                  {
                                      HeaderText = "Provincia (Consulente)",
                                      DataField = "IndirizzoConsulente.Provincia"
                                  };
        GridBoundColumn bc10 = new GridBoundColumn
                                   {
                                       HeaderText = "Cap (Consulente)",
                                       DataField = "IndirizzoConsulente.Cap"
                                   };


        radGridVcod.MasterTableView.Columns.Add(bc1);
        radGridVcod.MasterTableView.Columns.Add(bc2);
        radGridVcod.MasterTableView.Columns.Add(bc3);
        radGridVcod.MasterTableView.Columns.Add(bc4);
        radGridVcod.MasterTableView.Columns.Add(bc5);
        radGridVcod.MasterTableView.Columns.Add(bc6);
        radGridVcod.MasterTableView.Columns.Add(bc7);
        radGridVcod.MasterTableView.Columns.Add(bc8);
        radGridVcod.MasterTableView.Columns.Add(bc9);
        radGridVcod.MasterTableView.Columns.Add(bc10);

        PlaceHolder1.Controls.Clear();
        PlaceHolder1.Controls.Add(radGridVcod);

        radGridVcod.ExportSettings.IgnorePaging = true;
        radGridVcod.ExportSettings.FileName = "Report_VCOD";
        radGridVcod.ExportSettings.ExportOnlyData = true;
        radGridVcod.MasterTableView.ExportToExcel();
    }

    private void RadGridVcod_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        ImpreseFilter impreseFilter = GetImpreseFilter();

        List<ImpresaConFasciaIndirizzo> imprese = VodBiz.GetImprese(impreseFilter);
        ((RadGrid) source).DataSource = imprese;
    }

    private ImpreseFilter GetImpreseFilter()
    {
        ImpreseFilter impreseFilter = new ImpreseFilter
        {
            DataInizio = RadDatePickerDataDaElencoImprese.SelectedDate.Value,
            DataFine = RadDatePickerDataAElencoImprese.SelectedDate.Value
        };

        if (RadNumericTextBoxIdImpresaElencoImprese.Value.HasValue)
            impreseFilter.IdImpresa = (Int32)RadNumericTextBoxIdImpresaElencoImprese.Value.Value;
        if (!String.IsNullOrEmpty(RadComboBoxIdEsattoreElencoImprese.SelectedValue))
            impreseFilter.IdEsattore = RadComboBoxIdEsattoreElencoImprese.SelectedValue;
        if (!string.IsNullOrEmpty(RadComboBoxFascia.SelectedValue))
            impreseFilter.Fascia = RadComboBoxFascia.SelectedValue;
        if (RadNumericTextBoxOperai.Value.HasValue)
            impreseFilter.NumeroOperai = (int)RadNumericTextBoxOperai.Value;
        if (RadNumericTextBoxIdConsulente.Value.HasValue)
            impreseFilter.IdConsulente = (int)RadNumericTextBoxIdConsulente.Value;
        if (RadNumericTextBoxPartTime.Value.HasValue)
            impreseFilter.MediaPartTime = (int)RadNumericTextBoxPartTime.Value;
        if (!String.IsNullOrEmpty(RadComboBoxStato.SelectedValue))
            impreseFilter.Stato = RadComboBoxStato.SelectedValue;
        if (!String.IsNullOrEmpty(RadComboBoxEsitoControllo.SelectedValue))
            impreseFilter.EsitoControlloDenuncia = RadComboBoxEsitoControllo.SelectedValue;
        impreseFilter.OreFerie = RadNumericTextBoxOreFerie.Value;
        impreseFilter.OreCigStraordinaria = RadNumericTextBoxOreCigStraordinaria.Value;
        impreseFilter.OrePermessiRetribuiti = RadNumericTextBoxOrePermessiRetribuiti.Value;
        impreseFilter.OreCigo = RadNumericTextBoxOreCigo.Value;
        impreseFilter.OreCigoMaltempo = RadNumericTextBoxOraCigoMaltempo.Value;
        return impreseFilter;
    }

    private void ReloadFiltersFromCookie()
    {
        String tipoReport;
        if (!String.IsNullOrEmpty(Request.QueryString["tipo"]))
        {
            tipoReport = Request.QueryString["tipo"];
            ViewState["tipo"] = tipoReport;
        }
        else
        {
            tipoReport = (String)ViewState["tipo"];
        }

        switch (tipoReport)
        {
            case "ImpreseEsattori":
                break;
            case "scheda":
                ReloadIdEsattoreFromFilterCookie(RadComboBoxIdEsattoreSchedaImpresa);
                break;
            case "ImpreseMedie":
                ReloadImpreseFilter();
                break;
            case "reportVCOD":
                ReloadIdEsattoreFromFilterCookie(RadComboBoxIdEsattoreSchedaVCOD);
                break;
            default:
                break;
        }


    }

    private void ReloadImpreseFilter()
    {
        HttpCookie aCookie = Request.Cookies["impreseFilter"];
        if (aCookie != null)
        {
            System.Collections.Specialized.NameValueCollection
                userInfoCookieCollection = aCookie.Values;

            String tmp = Server.HtmlEncode(userInfoCookieCollection["utente"]);
            if (!String.IsNullOrEmpty(tmp) && Int32.Parse(tmp) == GestioneUtentiBiz.GetIdUtente())
            {
                LoadNumeriTextBox(RadNumericTextBoxOperai,userInfoCookieCollection["numOperai"]);
                LoadNumeriTextBox(RadNumericTextBoxOreFerie, userInfoCookieCollection["oreFerie"]);
                LoadNumeriTextBox(RadNumericTextBoxPartTime, userInfoCookieCollection["partTime"]);
                LoadNumeriTextBox(RadNumericTextBoxIdImpresaElencoImprese, userInfoCookieCollection["idImpresa"]);
                LoadNumeriTextBox(RadNumericTextBoxIdConsulente, userInfoCookieCollection["idConsulente"]);

                LoadNumeriTextBox(RadNumericTextBoxOrePermessiRetribuiti, userInfoCookieCollection["orePermRetrib"]);
                LoadNumeriTextBox(RadNumericTextBoxOreCigStraordinaria, userInfoCookieCollection["oreCigStraord"]);
                LoadNumeriTextBox(RadNumericTextBoxOreCigDeroga, userInfoCookieCollection["oreCigDeroga"]);
                LoadNumeriTextBox(RadNumericTextBoxOreCigo, userInfoCookieCollection["oreCigo"]);
                LoadNumeriTextBox(RadNumericTextBoxOraCigoMaltempo, userInfoCookieCollection["oreCigoMaltempo"]);

                LoadComboBoxByIndex(RadComboBoxEsitoControllo, userInfoCookieCollection["indexEsitoControllo"]);
                LoadComboBoxByIndex(RadComboBoxFascia, userInfoCookieCollection["indexFascia"]);
                LoadComboBoxByIndex(RadComboBoxIdEsattoreElencoImprese, userInfoCookieCollection["indexIdEsattore"]);
                LoadComboBoxByIndex(RadComboBoxStato, userInfoCookieCollection["indexStato"]);
            }

            //Delete cookie
            HttpCookie rCookie = new HttpCookie(aCookie.Name);
            rCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(rCookie);
        }
    }

    private void ReloadIdEsattoreFromFilterCookie(RadComboBox comboBox)
    {
        HttpCookie aCookie = Request.Cookies["impreseFilter"];
        if (aCookie != null)
        {
            System.Collections.Specialized.NameValueCollection
                userInfoCookieCollection = aCookie.Values;

            String tmp = Server.HtmlEncode(userInfoCookieCollection["utente"]);
            if (!String.IsNullOrEmpty(tmp) && Int32.Parse(tmp) == GestioneUtentiBiz.GetIdUtente())
            {
                LoadComboBoxByIndex(comboBox, userInfoCookieCollection["indexIdEsattore"]);
            }
        }
    }

    private void LoadNumeriTextBox(RadNumericTextBox textBox, String cookieValue)
    {
        String tmp = Server.HtmlEncode(cookieValue);
        if (!String.IsNullOrEmpty(tmp))
        {
            textBox.Value = Double.Parse(tmp);
        }
    }

    private void LoadComboBoxByIndex(RadComboBox comboBox, String cookieValue)
    {
        String tmp = Server.HtmlEncode(cookieValue);
        if (!String.IsNullOrEmpty(tmp))
        {
            comboBox.SelectedIndex = Int32.Parse(cookieValue);
        }
    }

    private void SaveCurrentImpreseFilter()
    {
        HttpCookie aCookie = new HttpCookie("impreseFilter");
        aCookie.Values["utente"] = GestioneUtentiBiz.GetIdUtente().ToString();
        aCookie.Values["oreFerie"] = RadNumericTextBoxOreFerie.Value.HasValue ? RadNumericTextBoxOreFerie.Value.ToString() : String.Empty;
        aCookie.Values["numOperai"] = RadNumericTextBoxOperai.Value.HasValue ? RadNumericTextBoxOperai.Value.ToString() : String.Empty;  
        aCookie.Values["partTime"] = RadNumericTextBoxPartTime.Value.HasValue ? RadNumericTextBoxPartTime.Value.ToString() : String.Empty;
        aCookie.Values["orePermRetrib"] = RadNumericTextBoxOrePermessiRetribuiti.Value.HasValue ? RadNumericTextBoxOrePermessiRetribuiti.Value.ToString() : String.Empty;
        aCookie.Values["oreCigStraord"] = RadNumericTextBoxOreCigStraordinaria.Value.HasValue ? RadNumericTextBoxOreCigStraordinaria.Value.ToString() : String.Empty;
        aCookie.Values["oreCigDeroga"] = RadNumericTextBoxOreCigDeroga.Value.HasValue ? RadNumericTextBoxOreCigDeroga.Value.ToString() : String.Empty;
        aCookie.Values["oreCigo"] = RadNumericTextBoxOreCigo.Value.HasValue ? RadNumericTextBoxOreCigo.Value.ToString() : String.Empty;
        aCookie.Values["oreCigoMaltempo"] = RadNumericTextBoxOraCigoMaltempo.Value.HasValue ? RadNumericTextBoxOraCigoMaltempo.Value.ToString() : String.Empty;
        
         aCookie.Values["idImpresa"] = RadNumericTextBoxIdImpresaElencoImprese.Value.HasValue ? RadNumericTextBoxIdImpresaElencoImprese.Value.ToString() : String.Empty;
        
        aCookie.Values["idConsulente"] = RadNumericTextBoxIdConsulente.Value.HasValue ? RadNumericTextBoxIdConsulente.Value.ToString() : String.Empty;
        aCookie.Values["indexEsitoControllo"] = RadComboBoxEsitoControllo.SelectedIndex.ToString();
        aCookie.Values["indexFascia"] = RadComboBoxFascia.SelectedIndex.ToString();
        aCookie.Values["indexStato"] = RadComboBoxStato.SelectedIndex.ToString();
        aCookie.Values["indexIdEsattore"] = RadComboBoxIdEsattoreElencoImprese.SelectedIndex.ToString();
        aCookie.Expires = DateTime.Now.AddMinutes(30);
       // aCookie.Path="/WebSiteServizi";
        Response.Cookies.Add(aCookie);

    }

    #region Eventi bottoni

    protected void ButtonVisualizzaElencoImprese_Click(object sender, EventArgs e)
    {
        ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportElencoImpreseWithMedia";

        //ViewState["dataDa"] = RadDatePickerDataDaElencoImprese.SelectedDate.Value;
        //ViewState["dataA"] = RadDatePickerDataAElencoImprese.SelectedDate.Value;
        //if (RadNumericTextBoxIdImpresaElencoImprese.Value.HasValue)
        //    ViewState["idImpresa"] = (Int32) RadNumericTextBoxIdImpresaElencoImprese.Value.Value;
        //else
        //{
        //    ViewState["idImpresa"] = null;
        //}
        //ViewState["idEsattore"] = RadComboBoxIdEsattoreElencoImprese.SelectedValue;

        SaveCurrentImpreseFilter();

        VisualizzaReportElencoImprese();
    }

    protected void ButtonVisualizzaSchedaImpresa_Click(object sender, EventArgs e)
    {
        ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportSchedaImpresa";

        //ViewState["dataDa"] = RadDatePickerDataDaSchedaImpresa.SelectedDate.Value;
        //ViewState["dataA"] = RadDatePickerDataASchedaImpresa.SelectedDate.Value;

        //if (RadNumericTextBoxIdImpresaSchedaImpresa.Value.HasValue)
        //    ViewState["idImpresa"] = (Int32) RadNumericTextBoxIdImpresaSchedaImpresa.Value.Value;
        //else
        //{
        //    ViewState["idImpresa"] = null;
        //}
        VisualizzaReportSchedaImpresa();
    }

    protected void ButtonVisualizzaVCOD_Click(object sender, EventArgs e)
    {
        ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportOreLavoratore";

        //ViewState["dataDa"] = RadDatePickerDataDaSchedaVCOD.SelectedDate.Value;
        //ViewState["dataA"] = RadDatePickerDataASchedaVCOD.SelectedDate.Value;
        //if (RadNumericTextBoxIdImpresaSchedaVCOD.Value.HasValue)
        //    ViewState["idImpresa"] = (Int32) RadNumericTextBoxIdImpresaSchedaVCOD.Value.Value;
        //else
        //{
        //    ViewState["idImpresa"] = null;
        //}
        //ViewState["idEsattore"] = RadComboBoxIdEsattoreSchedaVCOD.SelectedValue;

        VisualizzaReportVcod();
    }

    #endregion

    #region Settaggio parametri dei report

    /// <summary>
    /// </summary>
    private void VisualizzaReportElencoImprese()
    {
        const string nothing = null;

        List<ReportParameter> listaParam = new List<ReportParameter>();
        ReportParameter param1 = new ReportParameter("dataDa", RadDatePickerDataDaElencoImprese.SelectedDate.Value.ToShortDateString());
        listaParam.Add(param1);
        ReportParameter param2 = new ReportParameter("dataA", RadDatePickerDataAElencoImprese.SelectedDate.Value.ToShortDateString());
        listaParam.Add(param2);
        if (!String.IsNullOrEmpty(RadComboBoxIdEsattoreElencoImprese.SelectedValue))
        {
            ReportParameter param3 = new ReportParameter("idEsattore", (RadComboBoxIdEsattoreElencoImprese.SelectedValue));
            listaParam.Add(param3);
        }
        else
        {
            ReportParameter param3 = new ReportParameter("idEsattore", nothing);
            listaParam.Add(param3);
        }
        if (RadNumericTextBoxIdImpresaElencoImprese.Value.HasValue)
        {
            ReportParameter param8 = new ReportParameter("idImpresa", ((Int32)RadNumericTextBoxIdImpresaElencoImprese.Value.Value).ToString());
            listaParam.Add(param8);
        }
        else
        {
            ReportParameter param8 = new ReportParameter("idImpresa", nothing);
            listaParam.Add(param8);
        }

        //ReportParameter param1 = new ReportParameter("dataDa", ((DateTime) ViewState["dataDa"]).ToShortDateString());
        //listaParam.Add(param1);
        //ReportParameter param2 = new ReportParameter("dataA", ((DateTime) ViewState["dataA"]).ToShortDateString());
        //listaParam.Add(param2);
        //if (ViewState["idEsattore"] != null && ViewState["idEsattore"].ToString() != "")
        //{
        //    ReportParameter param3 = new ReportParameter("idEsattore", (ViewState["idEsattore"]).ToString());
        //    listaParam.Add(param3);
        //}
        //else
        //{
        //    ReportParameter param3 = new ReportParameter("idEsattore", nothing);
        //    listaParam.Add(param3);
        //}
        //if (ViewState["idImpresa"] != null)
        //{
        //    ReportParameter param8 = new ReportParameter("idImpresa", ((Int32)ViewState["idImpresa"]).ToString());
        //    listaParam.Add(param8);
        //}
        //else
        //{
        //    ReportParameter param8 = new ReportParameter("idImpresa", nothing);
        //    listaParam.Add(param8);
        //}

        if (!string.IsNullOrEmpty(RadComboBoxFascia.SelectedValue))
        {
            ReportParameter param4 = new ReportParameter("fascia", RadComboBoxFascia.SelectedValue);
            listaParam.Add(param4);
        }
        else
        {
            ReportParameter param4 = new ReportParameter("fascia", nothing);
            listaParam.Add(param4);
        }

        if (RadNumericTextBoxOperai.Value.HasValue)
        {
            ReportParameter param5 = new ReportParameter("operai", RadNumericTextBoxOperai.Value.Value.ToString());
            listaParam.Add(param5);
        }
        else
        {
            ReportParameter param5 = new ReportParameter("operai", nothing);
            listaParam.Add(param5);
        }

        if (RadNumericTextBoxIdConsulente.Value.HasValue)
        {
            ReportParameter param6 = new ReportParameter("idConsulente",
                                                         RadNumericTextBoxIdConsulente.Value.Value.ToString());
            listaParam.Add(param6);
        }
        else
        {
            ReportParameter param6 = new ReportParameter("idConsulente", nothing);
            listaParam.Add(param6);
        }

        if (RadNumericTextBoxPartTime.Value.HasValue)
        {
            ReportParameter param7 = new ReportParameter("mediaPartTime", RadNumericTextBoxPartTime.Value.ToString());
            listaParam.Add(param7);
        }
        else
        {
            ReportParameter param7 = new ReportParameter("mediaPartTime", nothing);
            listaParam.Add(param7);
        }

      

        if (!String.IsNullOrEmpty(RadComboBoxStato.SelectedValue))
        {
            ReportParameter param9 = new ReportParameter("stato", RadComboBoxStato.SelectedValue);
            listaParam.Add(param9);
        }
        else
        {
            ReportParameter param9 = new ReportParameter("stato", nothing);
            listaParam.Add(param9);
        }

        if (!String.IsNullOrEmpty(RadComboBoxEsitoControllo.SelectedValue))
        {
            ReportParameter param = new ReportParameter("esitoControlloDenuncia",
                                                        RadComboBoxEsitoControllo.SelectedValue);
            listaParam.Add(param);
        }
        else
        {
            ReportParameter param = new ReportParameter("esitoControlloDenuncia", nothing);
            listaParam.Add(param);
        }

        listaParam.Add(new ReportParameter("oreFerie", RadNumericTextBoxOreFerie.Value.HasValue ? RadNumericTextBoxOreFerie.Value.ToString() : null));
        listaParam.Add(new ReportParameter("orePermessiRetribuiti",  RadNumericTextBoxOrePermessiRetribuiti.Value.HasValue ? RadNumericTextBoxOrePermessiRetribuiti.Value.ToString() : null));
        listaParam.Add(new ReportParameter("oreCigStraordinaria", RadNumericTextBoxOreCigStraordinaria.Value.HasValue ? RadNumericTextBoxOreCigStraordinaria.Value.ToString() : null));
        listaParam.Add(new ReportParameter("oreCigDeroga", RadNumericTextBoxOreCigDeroga.Value.HasValue ? RadNumericTextBoxOreCigDeroga.Value.ToString() : null));
        listaParam.Add(new ReportParameter("oreCigo", RadNumericTextBoxOreCigo.Value.HasValue ? RadNumericTextBoxOreCigo.Value.ToString() : null));
        listaParam.Add(new ReportParameter("oreCigoMaltempo", RadNumericTextBoxOraCigoMaltempo.Value.HasValue ? RadNumericTextBoxOraCigoMaltempo.Value.ToString() : null));

        ReportViewerVOD.ServerReport.SetParameters(listaParam.ToArray());
    }

    private void VisualizzaReportSchedaImpresa()
    {
        const string nothing = null;

        List<ReportParameter> listaParam = new List<ReportParameter>();
        ReportParameter param1 = new ReportParameter("dataDa", RadDatePickerDataDaSchedaImpresa.SelectedDate.Value.ToShortDateString());
        listaParam.Add(param1);
        ReportParameter param2 = new ReportParameter("dataA", RadDatePickerDataASchedaImpresa.SelectedDate.Value.ToShortDateString());
        listaParam.Add(param2);
        if (RadNumericTextBoxIdImpresaSchedaImpresa.Value.HasValue)
        {
            ReportParameter param8 = new ReportParameter("idImpresa", ((Int32)RadNumericTextBoxIdImpresaSchedaImpresa.Value.Value).ToString());
            listaParam.Add(param8);
        }
        else
        {
            ReportParameter param8 = new ReportParameter("idImpresa", nothing);
            listaParam.Add(param8);
        }

        //ReportParameter param1 = new ReportParameter("dataDa", ((DateTime)ViewState["dataDa"]).ToShortDateString());
        //listaParam.Add(param1);
        //ReportParameter param2 = new ReportParameter("dataA", ((DateTime)ViewState["dataA"]).ToShortDateString());
        //listaParam.Add(param2);
        //if (ViewState["idImpresa"] != null)
        //{
        //    ReportParameter param8 = new ReportParameter("idImpresa", ((Int32)ViewState["idImpresa"]).ToString());
        //    listaParam.Add(param8);
        //}
        //else
        //{
        //    ReportParameter param8 = new ReportParameter("idImpresa", nothing);
        //    listaParam.Add(param8);
        //}
        ReportViewerVOD.ServerReport.SetParameters(listaParam.ToArray());
    }

    private void VisualizzaReportVcod()
    {
        const string nothing = null;
        List<ReportParameter> listaParam = new List<ReportParameter>();
        ReportParameter param1 = new ReportParameter("dataDa", RadDatePickerDataDaSchedaVCOD.SelectedDate.Value.ToShortDateString());
        listaParam.Add(param1);
        ReportParameter param2 = new ReportParameter("dataA", RadDatePickerDataASchedaVCOD.SelectedDate.Value.ToShortDateString());
        listaParam.Add(param2);
        //if (ViewState["idEsattore"] != null && ViewState["idEsattore"].ToString() != "")
        //{
        //    ReportParameter param3 = new ReportParameter("idEsattore", (ViewState["idEsattore"]).ToString());
        //    listaParam.Add(param3);
        //}
        //else
        //{
        //    ReportParameter param3 = new ReportParameter("idEsattore", nothing);
        //    listaParam.Add(param3);
        //}
        if (RadNumericTextBoxIdImpresaSchedaVCOD.Value.HasValue)
        {
            ReportParameter param4 = new ReportParameter("idImpresa", ((Int32)RadNumericTextBoxIdImpresaSchedaVCOD.Value.Value).ToString());
            listaParam.Add(param4);
        }
        else
        {
            ReportParameter param4 = new ReportParameter("idImpresa", nothing);
            listaParam.Add(param4);
        }

        
        //ReportParameter param1 = new ReportParameter("dataDa", ((DateTime) ViewState["dataDa"]).ToShortDateString());
        //listaParam.Add(param1);
        //ReportParameter param2 = new ReportParameter("dataA", ((DateTime) ViewState["dataA"]).ToShortDateString());
        //listaParam.Add(param2);
        ////if (ViewState["idEsattore"] != null && ViewState["idEsattore"].ToString() != "")
        ////{
        ////    ReportParameter param3 = new ReportParameter("idEsattore", (ViewState["idEsattore"]).ToString());
        ////    listaParam.Add(param3);
        ////}
        ////else
        ////{
        ////    ReportParameter param3 = new ReportParameter("idEsattore", nothing);
        ////    listaParam.Add(param3);
        ////}
        //if (ViewState["idImpresa"] != null)
        //{
        //    ReportParameter param4 = new ReportParameter("idImpresa", ((Int32) ViewState["idImpresa"]).ToString());
        //    listaParam.Add(param4);
        //}
        //else
        //{
        //    ReportParameter param4 = new ReportParameter("idImpresa", nothing);
        //    listaParam.Add(param4);
        //}

        ReportParameter param9 = new ReportParameter("maxOrePermessiIndividuali",
                                                     RadNumericTextBoxMaxOrePermessiIndividuali.Value.ToString());
        listaParam.Add(param9);
        ReportParameter param5 = new ReportParameter("maxOreFerie", RadNumericTextBoxMaxOreFerie.Value.ToString());
        listaParam.Add(param5);
        ReportParameter param6 = new ReportParameter("maxOreCIG", RadNumericTextBoxMaxOreCIG.Value.ToString());
        listaParam.Add(param6);
        ReportParameter param7 = new ReportParameter("maxOreAssenzeGiustificate",
                                                     RadNumericTextBoxMaOreAssGiust.Value.ToString());
        listaParam.Add(param7);
        ReportParameter param8 = new ReportParameter("maxOreAssenzeNonMotivate",
                                                     RadNumericTextBoxMaxOreAssNonGiust.Value.ToString());
        listaParam.Add(param8);

        listaParam.Add(new ReportParameter("maxOreCIGDeroga", RadNumericTextBoxMaxOreCIGDeroga.Value.ToString()));
        listaParam.Add(new ReportParameter("maxOreCIGStraordinaria", RadNumericTextBoxMaxOreCIGStraord.Value.ToString()));
        listaParam.Add(new ReportParameter("maxOreCIGO", RadNumericTextBoxMaxOreCIGO.Value.ToString()));
        listaParam.Add(new ReportParameter("maxOreCIGOMaltempo", RadNumericTextBoxMaxOreCIGOMaltempo.Value.ToString()));
        ReportViewerVOD.ServerReport.SetParameters(listaParam.ToArray());
    }

    #endregion

/*
    private static string PreparaStampa(List<ImpresaConFasciaIndirizzo> imprese)
    {
        GridView gv = new GridView();
        gv.ID = "gvImprese";
        gv.AutoGenerateColumns = false;

        BoundField bc1 = new BoundField();
        bc1.HeaderText = "Codice Impresa";
        bc1.DataField = "IdImpresa";
        BoundField bc2 = new BoundField();
        bc2.HeaderText = "Ragione Sociale";
        bc2.DataField = "RagioneSociale";
        BoundField bc3 = new BoundField();
        bc3.HeaderText = "Indirizzo Impresa";
        bc3.DataField = "IndirizzoContatto";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Indirizzo Consulente";
        bc4.DataField = "IndirizzoConsulente";
        //BoundField bc4 = new BoundField();
        //bc4.HeaderText = "Data nascita";
        //bc4.DataField = "DataNascitaLavoratore";
        //bc4.DataFormatString = "{0:dd/MM/yyyy}";
        //bc4.HtmlEncode = false;

        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc4);
        //gv.Columns.Add(bc4);

        gv.DataSource = imprese;
        gv.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw.ToString();
    }
*/
}