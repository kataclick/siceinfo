using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class NotificheDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.NNotificheAnomalie);
        funzionalita.Add(FunzionalitaPredefinite.NNotificheGestione);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "NotificheDefault.aspx");

        if (GestioneUtentiBiz.IsImpresa())
        {
            GestioneUtentiBiz gu =
                new GestioneUtentiBiz();

            //Impresa impresaEntity =
            //    gu.GetUtenteImpresa(GestioneUtentiBiz.GetIdUtente());
            Impresa impresaEntity =
                (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            if (impresaEntity != null)
            {
                Session["idImpresa"] = impresaEntity.IdImpresa;
            }
        }
        else
        {
            //Se l'utente non � un'impresa indirizziamo verso una pagina
            //nella quale in fututo qlc potr� scegliere un impresa e gestirne le notifiche
            //al momento la pagina non ha funzionalit�
            Response.Redirect("NotificheDefaultAmministrativo.aspx");
        }
    }
}