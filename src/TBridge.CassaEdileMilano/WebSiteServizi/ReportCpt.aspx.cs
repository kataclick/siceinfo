using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ReportCpt : Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);
        funzionalita.Add(FunzionalitaPredefinite.CantieriGestione);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        if (Request.QueryString["idNotifica"] != null)
        {
            int idNotifica = Int32.Parse(Request.QueryString["idNotifica"]);

            ReportViewerCpt.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            Boolean isNotificaTelematica = biz.IsNotificaTelematica(idNotifica);
            
            if (!isNotificaTelematica)
            {
                ReportViewerCpt.ServerReport.ReportPath = "/ReportCpt/ReportNotifica";
            }
            else
            {
                ReportViewerCpt.ServerReport.ReportPath = "/ReportCpt/ReportNotificaTelematica";
            }
            ReportParameter[] listaParam = new ReportParameter[1];
            listaParam[0] = new ReportParameter("idNotifica", idNotifica.ToString());

            ReportViewerCpt.ServerReport.SetParameters(listaParam);

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            byte[] bytes = null;

            //PDF
            bytes = ReportViewerCpt.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Notifica.pdf", idNotifica));
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }
}