using System;
using System.Configuration;
using System.Web.UI;

public partial class ReportNotificheCantiere : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //barbirato / damicco
        if ((TextBox2.Text == "report" && TextBox1.Text == "report1"))
        {
            Label3.Text = "Accesso consentito";
            ReportViewerColonie.Visible = true;
            Panel1.Visible = false;

            ReportViewerColonie.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            ReportViewerColonie.ServerReport.ReportPath = "/Statistiche/NotificheCantiere";
        }
        else Label3.Text = "Accesso negato";
    }
}