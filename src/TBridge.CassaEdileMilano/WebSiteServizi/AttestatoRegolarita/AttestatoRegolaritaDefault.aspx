﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AttestatoRegolaritaDefault.aspx.cs" Inherits="AttestatoRegolarita_AttestatoRegolaritaDefault" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuAttestatoRegolarita.ascx" tagname="MenuAttestatoRegolarita" tagprefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" 
    sottoTitolo="Benvenuto nella sezione Verifica Regolarità per Cantiere"
        titolo="Attestato di regolarità" />
    <br />
    In questa pagina è possibile richiedere/gestire l'attestato di Verifica Regolarità per Cantiere.
</asp:Content>


