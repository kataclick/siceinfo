using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AttestatoRegolarita_GestioneAttestati : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.AttestatoRegolaritaRichiesta);
        funzionalita.Add(FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "~/AttestatoRegolarita/GestioneAttestati.aspx");

        #endregion
    }
}