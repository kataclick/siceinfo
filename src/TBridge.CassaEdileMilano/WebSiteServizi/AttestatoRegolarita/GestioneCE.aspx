﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneCE.aspx.cs" Inherits="AttestatoRegolarita_GestioneCE" %>

<%@ Register src="../WebControls/MenuAttestatoRegolarita.ascx" tagname="MenuAttestatoRegolarita" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" 
        titolo="Verifica Regolarità per Cantiere" sottoTitolo="Gestione per la Cassa Edile" />
    <br />
    <asp:Button ID="ButtonLanciaControlli" runat="server" Text="Lancia controlli" 
        onclick="ButtonLanciaControlli_Click" />
</asp:Content>


