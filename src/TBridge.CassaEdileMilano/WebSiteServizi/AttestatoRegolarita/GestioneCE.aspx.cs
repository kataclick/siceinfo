﻿using System;
using System.Web.UI;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AttestatoRegolarita_GestioneCE : Page
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE,
                                              "~/AttestatoRegolarita/GestioneCE.aspx");

        #endregion
    }

    protected void ButtonLanciaControlli_Click(object sender, EventArgs e)
    {
        biz.EffettuaControlli();
    }
}