﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaInserimento.aspx.cs" Inherits="AttestatoRegolarita_ConfermaInserimento" %>

<%@ Register src="../WebControls/MenuAttestatoRegolarita.ascx" tagname="MenuAttestatoRegolarita" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Regolarità per Cantiere" sottoTitolo="Conferma richiesta" />
    <br />
    <p>
        La richiesta è stata correttamente inserita/modificata.
    </p>
    <p>
        E' possibile controllare lo stato della domanda tramite la funzionalità "Gestione richieste" nel menu laterale sinistro.
    </p>
</asp:Content>


