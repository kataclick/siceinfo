﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VisualizzaDomanda.aspx.cs" Inherits="AttestatoRegolarita_VisualizzaDomanda" %>

<%@ Register Src="../WebControls/MenuAttestatoRegolarita.ascx" TagName="MenuAttestatoRegolarita"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaDatiDomanda.ascx" TagName="AttestatoRegolaritaDatiDomanda"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaControlli.ascx" TagName="AttestatoRegolaritaControlli"
    TagPrefix="uc4" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Regolarità per Cantiere"
        sottoTitolo="Dettaglio domanda" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:AttestatoRegolaritaDatiDomanda ID="AttestatoRegolaritaDatiDomanda1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <uc4:AttestatoRegolaritaControlli ID="AttestatoRegolaritaControlli1" runat="server"
                    Visible="False" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
