﻿#region

using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

#endregion

public partial class AttestatoRegolarita_ReportAttestatoRegolarita : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AttestatoRegolaritaRichiesta,
                                              "~/AttestatoRegolarita/ReportAttestatoRegolarita.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                int idDomanda = (int) Context.Items["IdDomanda"];

                ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                ReportViewer1.ServerReport.ReportPath = "/ReportAttestatoRegolarita/ReportModuloAttestatoRegolarita";
                ReportParameter[] listaParam = new ReportParameter[1];
                listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());

                ReportViewer1.ServerReport.SetParameters(listaParam);
            }
        }
    }
}