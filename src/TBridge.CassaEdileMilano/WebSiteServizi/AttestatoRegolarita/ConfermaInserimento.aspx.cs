﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AttestatoRegolarita_ConfermaInserimento : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AttestatoRegolaritaRichiesta,
                                              "~/AttestatoRegolarita/ConfermaInserimento.aspx");

        #endregion
    }
}