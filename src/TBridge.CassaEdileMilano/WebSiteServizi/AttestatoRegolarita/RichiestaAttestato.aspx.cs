#region

using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;
using Committente=TBridge.Cemi.GestioneUtenti.Type.Entities.Committente;
using Impresa=TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;

#endregion

public partial class AttestatoRegolarita_RichiestaAttestato : Page
{
    private const Int32 INDICECONFERMA = 3;
    private const Int32 INDICEDATIGENERALI = 0;
    private const Int32 INDICEIMPRESE = 1;
    private const Int32 INDICELAVORATORI = 2;
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AttestatoRegolaritaRichiesta,
                                              "~/AttestatoRegolarita/RichiestaAttestato.aspx");

        #endregion

        #region Cambio colore link della sidebar

        // Per far funzionare il cambiamento di colore dei link della sidebar
        //Hashtable buttonsColl = new Hashtable();
        Control lst1 = Wizard1.FindControl("SideBarContainer");
        DataList lst = (DataList) lst1.FindControl("SideBarList");
        lst.ItemCreated += lst_ItemCreated;

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                ViewState["IdDomanda"] = Context.Items["IdDomanda"];

                CaricaDomanda((Int32) ViewState["IdDomanda"]);
            }

            ViewState["indiceWizard"] = 0;
            ViewState["GuidId"] = Guid.NewGuid();
        }

        #region Click multipli

        // Per prevenire click multipli
        Button bConferma =
            (Button)
            Wizard1.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(bConferma, null) + ";");
        sb.Append("return true;");
        bConferma.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(bConferma);

        #endregion
    }

    private void CaricaDomanda(int idDomanda)
    {
        Domanda domanda = biz.GetDomandaByKey(idDomanda);
        Boolean copia = false;

        if (Request.QueryString["modalita"] == "copia")
        {
            ViewState["IdDomanda"] = null;
            copia = true;
        }

        AttestatoRegolaritaCantiere1.CaricaDati(domanda, copia);
        AttestatoRegolaritaImprese1.CaricaImprese(domanda.Subappalti);
        ImpresaCollection imprese = biz.GetImpreseSelezionateInSubappalto(domanda.IdDomanda.Value);
        //AttestatoRegolaritaImprese1.GetImprese();
        AttestatoRegolaritaLavoratore1.CaricaImprese(imprese);
        DomandaImpresaCollection domImpColl = biz.GetLavoratoriInDomanda(idDomanda);
        AttestatoRegolaritaLavoratore1.CaricaLavoratori(domImpColl);
        AttestatoRegolaritaConferma1.CaricaDatiVisita(domanda);
    }

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (AttestatoRegolaritaCantiere1.GetIndirizzi().Count == 0)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
        else
        {
            ViewState["indiceWizard"] = e.NextStepIndex;

            if (e.NextStepIndex == INDICELAVORATORI)
            {
                ImpresaCollection imprese = AttestatoRegolaritaImprese1.GetImprese();
                AttestatoRegolaritaLavoratore1.CaricaImprese(imprese);

                AttestatoRegolaritaLavoratore1.DeselezionaImpresa();
            }

            if (e.NextStepIndex == INDICECONFERMA)
            {
                ImpresaCollection imprese = AttestatoRegolaritaImprese1.GetImprese();
                AttestatoRegolaritaConferma1.CaricaImprese(imprese);
                Domanda domanda = CreaDomanda();
                AttestatoRegolaritaConferma1.CaricaDomanda(domanda);
            }
        }
    }

    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Page.Validate("stopConferma");

        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
        else
        {
            Domanda domanda = CreaDomanda();

            if (domanda.IdDomanda.HasValue)
            {
                if (biz.UpdateDomanda(domanda))
                {
                    Response.Redirect("~/AttestatoRegolarita/ConfermaInserimento.aspx");
                }
            }
            else
            {
                try
                {
                    if (biz.InsertDomanda(domanda))
                    {
                        LabelGiaInserita.Visible = false;
                        Response.Redirect("~/AttestatoRegolarita/ConfermaInserimento.aspx");
                    }
                }
                catch (RichiestaGiaInseritaException exc)
                {
                    LabelGiaInserita.Visible = true;
                }
            }
        }
    }

    private Domanda CreaDomanda()
    {
        Domanda domanda = new Domanda();

        if (ViewState["IdDomanda"] != null)
        {
            domanda.IdDomanda = (Int32) ViewState["IdDomanda"];
        }

        domanda.GuidId = (Guid) ViewState["GuidId"];

        #region Riconoscimento dell'utente

        domanda.IdUtente = GestioneUtentiBiz.GetIdUtente();

        if (GestioneUtentiBiz.IsCommittente())
        {
            Committente committente =
                (Committente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            domanda.IdCommittenteRichiedente = committente.IdCommittente;
        }

        if (GestioneUtentiBiz.IsImpresa())
        {
            Impresa impresa =
                (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            domanda.IdImpresaRichiedente = impresa.IdImpresa;
        }

        #endregion

        #region Dati Generali

        IndirizzoCollection indirizzi = AttestatoRegolaritaCantiere1.GetIndirizzi();
        DateTime meseAnno = AttestatoRegolaritaCantiere1.GetDataRichiesta();
        Decimal? importo = AttestatoRegolaritaCantiere1.GetImporto();
        Decimal? percentualeManodopera = AttestatoRegolaritaCantiere1.GetPercentualeManodopera();
        Int32? numeroLavoratori = AttestatoRegolaritaCantiere1.GetNumeroLavoratori();
        Int32? numeroGiorni = AttestatoRegolaritaCantiere1.GetNumeroGiorni();
        string descrizione = AttestatoRegolaritaCantiere1.GetDescrizione();
        DateTime dataInizio = AttestatoRegolaritaCantiere1.GetDataInizio();
        DateTime dataFine = AttestatoRegolaritaCantiere1.GetDataFine();
        string DIA = AttestatoRegolaritaCantiere1.GetDIA();
        string concessione = AttestatoRegolaritaCantiere1.GetConcessione();
        string autorizzazione = AttestatoRegolaritaCantiere1.GetAutorizzazione();
        bool richiestaVisitaIspettiva = AttestatoRegolaritaConferma1.GetRichiestaVisitaIspettiva();
        DateTime? dataVisitaIspettiva = AttestatoRegolaritaConferma1.GetDataVisitaIspettiva();
        string orarioVisitaIspettiva = AttestatoRegolaritaConferma1.GetOrarioVisitaIspettiva();
        string contattoTelefonicoVisitaIspettiva = AttestatoRegolaritaConferma1.GetContattoTelefonicoVisitaIspettiva();
        string motivoVisitaIspettiva = AttestatoRegolaritaConferma1.GetMotivoVisitaIspettiva();

        domanda.Mese = meseAnno;

        domanda.DataInizio = dataInizio;
        domanda.DataFine = dataFine;

        domanda.Importo = importo;
        domanda.PercentualeManodopera = percentualeManodopera;
        domanda.NumeroLavoratori = numeroLavoratori;
        domanda.NumeroGiorni = numeroGiorni;
        domanda.Descrizione = descrizione;

        domanda.Dia = DIA;
        domanda.Concessione = concessione;
        domanda.Autorizzazione = autorizzazione;

        domanda.RichiestaVisitaIspettiva = richiestaVisitaIspettiva;
        domanda.DataVisitaIspettiva = dataVisitaIspettiva;
        domanda.OrarioVisitaIspettiva = orarioVisitaIspettiva;
        domanda.ContattoTelefonicoVisitaIspettiva = contattoTelefonicoVisitaIspettiva;
        domanda.MotivoVisitaIspettiva = motivoVisitaIspettiva;

        domanda.Cap = indirizzi[0].Cap;
        domanda.Civico = indirizzi[0].Civico;
        domanda.Comune = indirizzi[0].Comune;
        domanda.Provincia = indirizzi[0].Provincia;
        domanda.Indirizzo = indirizzi[0].Indirizzo1;
        domanda.InfoAggiuntiva = indirizzi[0].InfoAggiuntiva;
        domanda.Latitudine = indirizzi[0].Latitudine;
        domanda.Longitudine = indirizzi[0].Longitudine;

        domanda.Committente = AttestatoRegolaritaCantiere1.GetCommittente();

        #endregion

        #region Subappalti

        SubappaltoCollection subappalti = AttestatoRegolaritaImprese1.GetSubappalti();
        domanda.Subappalti = subappalti;

        #endregion

        #region Lavoratori

        DomandaImpresaCollection domandeImprese = AttestatoRegolaritaLavoratore1.GetDomandeImprese();
        domanda.Lavoratori = domandeImprese;

        #endregion

        return domanda;
    }

    protected void Wizard1_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Page.Validate("stop");

        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
        else
        {
            if (e.CurrentStepIndex >= e.NextStepIndex)
            {
                ViewState["indiceWizard"] = e.NextStepIndex;
            }
            else
            {
                e.Cancel = true;
            }
        }
    }

    protected void Wizard1_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
    {
        ViewState["indiceWizard"] = e.NextStepIndex;
    }

    #region Cambio colore link della sidebar

    private void lst_ItemCreated(object sender, DataListItemEventArgs e)
    {
        // Per far funzionare il cambiamento di colore dei link della sidebar
        int indiceWizard = (int) ViewState["indiceWizard"];
        LinkButton button = (LinkButton) e.Item.FindControl("SideBarButton");

        if (e.Item.ItemIndex > indiceWizard)
            button.ForeColor = Color.LightGray;
    }

    #endregion
}