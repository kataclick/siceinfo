<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RichiestaAttestato.aspx.cs" Inherits="AttestatoRegolarita_RichiestaAttestato"
    EnableEventValidation="true" %>

<%@ Register Src="../WebControls/MenuAttestatoRegolarita.ascx" TagName="MenuAttestatoRegolarita"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaCantiere.ascx" TagName="AttestatoRegolaritaCantiere"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaImprese.ascx" TagName="AttestatoRegolaritaImprese"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaLavoratore.ascx" TagName="AttestatoRegolaritaLavoratore"
    TagPrefix="uc5" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaDatiDomanda.ascx" TagName="AttestatoRegolaritaDatiDomanda"
    TagPrefix="uc6" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaConferma.ascx" TagName="AttestatoRegolaritaConferma"
    TagPrefix="uc7" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Regolarit� per Cantiere"
        sottoTitolo="Richiesta" />
    <br />
    <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" BorderWidth="1px" Font-Names="Verdana"
        Height="200px" Width="100%" SideBarStyle-Width="120px" OnNextButtonClick="Wizard1_NextButtonClick"
        OnFinishButtonClick="Wizard1_FinishButtonClick" OnSideBarButtonClick="Wizard1_SideBarButtonClick"
        OnPreviousButtonClick="Wizard1_PreviousButtonClick">
        <StepStyle VerticalAlign="Top" />
        <SideBarStyle Width="120px"></SideBarStyle>
        <StartNavigationTemplate>
            <asp:Button ID="btnSuccessivoStart" runat="server" Text="Avanti" CommandName="MoveNext"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StepNavigationTemplate>
        <FinishNavigationTemplate>
            <asp:Button ID="btnPrecedenteFinish" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Conferma"
                Width="150px" TabIndex="1" ValidationGroup="stop" />
        </FinishNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep ID="WizardStepCantiere" runat="server" Title="- Cantiere">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Cantiere </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorIndirizzo" runat="server" ErrorMessage="Non � stato comunicato l'indirizzo del cantiere"
                                OnServerValidate="CustomValidatorIndirizzo_ServerValidate" ValidationGroup="stop">&nbsp;</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc3:AttestatoRegolaritaCantiere ID="AttestatoRegolaritaCantiere1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErroriCantieri" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepImprese" runat="server" Title="- Imprese">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Imprese </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc4:AttestatoRegolaritaImprese ID="AttestatoRegolaritaImprese1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErroriImprese" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepLavoratori" runat="server" Title="- Lavoratori">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Lavoratori </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc5:AttestatoRegolaritaLavoratore ID="AttestatoRegolaritaLavoratore1" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepConferma" runat="server" Title="- Conferma">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Conferma </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc7:AttestatoRegolaritaConferma ID="AttestatoRegolaritaConferma1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelGiaInserita" runat="server" ForeColor="Red" Text="La richiesta � gi� stata inserita. Per inserirne una nuova cliccare sulla voce &quot;Richiesta&quot; nel menu laterale sinistro."
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
        </WizardSteps>
        <HeaderStyle BorderStyle="Solid" BorderWidth="2px" HorizontalAlign="Center" />
    </asp:Wizard>
    <br />
</asp:Content>
