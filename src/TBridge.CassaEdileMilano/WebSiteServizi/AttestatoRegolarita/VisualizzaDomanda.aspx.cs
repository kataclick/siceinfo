﻿#region

using System;
using System.Web.UI;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

#endregion

public partial class AttestatoRegolarita_VisualizzaDomanda : Page
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AttestatoRegolaritaRichiesta,
                                              "~/AttestatoRegolarita/VisualizzaDomanda.aspx");

        #endregion

        if (Context.Items["IdDomanda"] != null)
        {
            ViewState["idDomanda"] = (Int32) Context.Items["IdDomanda"];
            Int32 idDomanda = (Int32) Context.Items["IdDomanda"];
            CaricaDomanda(idDomanda);
        }
    }

    private void CaricaDomanda(int idDomanda)
    {
        Domanda domanda = biz.GetDomandaByKey(idDomanda);
        AttestatoRegolaritaDatiDomanda1.CaricaDomanda(domanda);

        if (domanda.DataControllo.HasValue)
        {
            AttestatoRegolaritaControlli1.Visible = true;
            AttestatoRegolaritaControlli1.CaricaControlli(domanda.IdDomanda.Value);
        }
    }
}