﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RiepilogoAttestati.aspx.cs" Inherits="AttestatoRegolarita_RiepilogoAttestati" %>

<%@ Register Src="../WebControls/MenuAttestatoRegolarita.ascx" TagName="MenuAttestatoRegolarita"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaRiepilogoAttestati.ascx" TagName="AttestatoRegolaritaRiepilogoAttestati"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Regolarità per Cantiere"
        sottoTitolo="Riepilogo attestati" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:AttestatoRegolaritaRiepilogoAttestati ID="AttestatoRegolaritaRiepilogoAttestati1"
                    runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
