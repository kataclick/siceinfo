<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneAttestati.aspx.cs" Inherits="AttestatoRegolarita_GestioneAttestati" %>

<%@ Register Src="../WebControls/MenuAttestatoRegolarita.ascx" TagName="MenuAttestatoRegolarita"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AttestatoRegolaritaRicerca.ascx" TagName="AttestatoRegolaritaRicerca"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAttestatoRegolarita ID="MenuAttestatoRegolarita1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica RegolaritÓ per Cantiere"
        sottoTitolo="Gestione richieste" />
    <br />
    <uc3:AttestatoRegolaritaRicerca ID="AttestatoRegolaritaRicerca1" runat="server" />
    <br />
</asp:Content>
