using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AttestatoRegolarita_AttestatoRegolaritaDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AttestatoRegolaritaRichiesta,
                                                             FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE,
                                                             FunzionalitaPredefinite.AttestatoRegolaritaRiepilogoAttestati
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}