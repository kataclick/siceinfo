using System;
using System.Web.UI;
using TBridge.Cemi.Subappalti.Business;

public partial class SchedaImpresa : UserControl
{
    private bool usaControlli = true; // Valore di default

    /// <summary>
    /// Imposta o ricava se il controllo usa i controlli o la detailsView 
    /// </summary>
    public bool UsaControlli
    {
        get { return usaControlli; }
        set
        {
            usaControlli = value;
            pnlControlli.Visible = usaControlli;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CaricaSchedaImpresa();
    }

    /// <summary>
    /// Carica la scheda impresa da database e la binda ai controlli
    /// </summary>
    public void CaricaSchedaImpresa()
    {
        if (Session["Subappalti.IdImpresa"] != null)
        {
            int idImpresa = int.Parse(Session["Subappalti.IdImpresa"].ToString());

            // Usando questo costruttore carico direttamente da database
            ImpresaEntity impresa = new ImpresaEntity(idImpresa);

            lblTxtCodice.Text = impresa.Id.ToString();
            lblTxtRagioneSociale.Text = impresa.RagioneSociale;
            if (impresa.CodiceFiscale != null)
                lblTxtCodiceFiscale.Text = impresa.CodiceFiscale;
            else
                lblCodiceFiscale.Visible = false;
            if (impresa.PartitaIVA != null)
                lblTxtPartitaIva.Text = impresa.PartitaIVA;
            else
                lblPartitaIVA.Visible = false;
            if (impresa.TipoImpresa != null)
                lblTxtTipoImpresa.Text = impresa.TipoImpresa;
            else
                lblTipoImpresa.Visible = false;
            //if (impresa.CodiceContratto != null)
            //    lblTxtCodiceContratto.Text = impresa.CodiceContratto;
            //else
            //    lblCodiceContratto.Visible = false;
            if (impresa.CodiceINAIL != null)
                lblTxtCodiceInail.Text = impresa.CodiceINAIL;
            else
                lblCodiceINAIL.Visible = false;
            if (impresa.CodiceINPS != null)
                lblTxtCodiceInps.Text = impresa.CodiceINPS;
            else
                lblCodiceINPS.Visible = false;

            if (impresa.NumeroIscrizioneCCIAA != -1)
            {
                if (impresa.NumeroIscrizioneCCIAA != 0)
                    lblTxtNrIscrizCCIAA.Text = impresa.NumeroIscrizioneCCIAA.ToString();
                else
                    lblTxtNrIscrizCCIAA.Text = "-";
            }
            else
                lblNumeroIscrizioneCCIAA.Visible = false;

            if (impresa.AttivitaISTAT != null)
                lblTxtAttivitaIstat.Text = impresa.AttivitaISTAT;
            else
                lblAttivitaISTAT.Visible = false;
            //if (impresa.NaturaGiuridica != null)
            //    lblTxtNaturaGiuridica.Text = impresa.NaturaGiuridica;
            //else
            //    lblNaturaGiuridica.Visible = false;
            if (impresa.IndirizzoSedeLegale != null)
                lblTxtIndirizzoSedeLegale.Text = impresa.IndirizzoSedeLegale;
            if (impresa.CapSedeLegale != null)
                lblTxtCapSedeLegale.Text = impresa.CapSedeLegale;
            if (impresa.LocalitaSedeLegale != null)
                lblTxtLocalitaSedeLegale.Text = impresa.LocalitaSedeLegale;
            if (impresa.ProvinciaSedeLegale != null)
                lblTxtProvinciaSedeLegale.Text = "(" + impresa.ProvinciaSedeLegale + ")";
            if (impresa.IndirizzoSedeAmministrazione != null)
                lblTxtIndirizzoSedeAmministrazione.Text = impresa.IndirizzoSedeAmministrazione;
            if (impresa.CapSedeAmministrazione != null)
                lblTxtCapSedeAmministrazione.Text = impresa.CapSedeAmministrazione;
            if (impresa.LocalitaSedeAmministrazione != null)
                lblTxtLocalitaSedeAmministrazione.Text = impresa.LocalitaSedeAmministrazione;
            if (impresa.ProvinciaSedeAmministrazione != null)
                lblTxtProvinciaSedeAmministrazione.Text = "(" + impresa.ProvinciaSedeAmministrazione + ")";
            if (impresa.PressoSedeAmministrazione != null)
                lblTxtPressoSedeAmministrazione.Text = impresa.PressoSedeAmministrazione;
            else
                lblPressoSedeAmministrazione.Visible = false;
            if (impresa.Telefono != null)
                lblTxtTelefono.Text = impresa.Telefono;
            else
                lblTelefono.Visible = false;
            if (impresa.FaxSedeAmministrazione != null)
                lblTxtFax.Text = impresa.FaxSedeAmministrazione;
            else
                lblFax.Visible = false;
            if (impresa.Email != null)
                lblTxtEmail.Text = impresa.Email;
            else
                lblEmail.Visible = false;
            if (impresa.Cellulare != null)
                lblTxtCellulare.Text = impresa.Cellulare;
            else
                lblCellulare.Visible = false;
            if (impresa.SitoWeb != null)
                lblTxtSito.Text = impresa.SitoWeb;
            else
                lblSitoWeb.Visible = false;
        }
    } // CaricaSchedaImpresa
} // Class