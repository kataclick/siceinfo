<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    StylesheetTheme="CETheme2009Wide" CodeFile="ReportLavoratoriPINSMS.aspx.cs" Inherits="ReportLavoratoriPINSMS" %>


<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <table class="standardTable">
        <tr align="left" valign="top">
            <td align="left" valign="top" width="0">
            </td>
            <td align="left" valign="top">
                <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Report Lavoratori PIN & SMS"
                    sottoTitolo="Lavoratori PIN SMS" runat="server" />
                Per accedere al report � necessario fornire un'autenticazione.
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="Panel1" runat="server" Width="960px">
        <asp:Label ID="Label2" runat="server" Text="Username:"></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><br />
        <asp:Label ID="Label1" runat="server" Text="Password:"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" TextMode="Password"></asp:TextBox><br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Accedi al report" /><br />
        <asp:Label ID="Label3" runat="server"></asp:Label></asp:Panel>
    <br />
    <table height="600px" width="940px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerColonie" runat="server" ProcessingMode="Remote"
        Width="940px" Visible="False">
    </rsweb:ReportViewer>
    </td></tr></table>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MenuDettaglio">
    &nbsp;</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    &nbsp;</asp:Content>
