<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportImprese.aspx.cs" Inherits="ReportImprese" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<%@ Register Src="WebControls/MenuImprese.ascx" TagName="MenuImprese" TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <%--<uc2:MenuImprese ID="MenuImprese1" runat="server" />--%>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Reportistica imprese" />
    <br />
    <table class="borderedTable" id="TableLabelCartella" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelCartella" runat="server">Riportiamo di seguito l�elenco dei Vostri dipendenti che hanno percepito il trattamento economico per ferie/la gratifica natalizia per l�anno in corso con i relativi importi. Nel ricordarvi che abbiamo provveduto a far pervenire direttamente ai lavoratori quanto di loro spettanza tramite bonifico su conto corrente o carta prepagata intestati ai beneficiari stessi, sottolineiamo che gli importi erogati devono corrispondere a quelli effettivamente accantonati per il periodo in oggetto. Provvediamo, altres�, ad elencare i nominativi dei Vostri dipendenti che, per ragioni varie a noi non note, non hanno incassato quanto di loro competenza. Vi saremmo, pertanto, grati se voleste avvertirli di prendere contatto con i nostri uffici. La situazione � aggiornata alla data riportata nel report. </asp:Label>--%>
                <asp:Label ID="LabelCartella" runat="server">Riportiamo di seguito l�elenco dei vostri dipendenti che hanno percepito il trattamento economico per ferie/la gratifica natalizia per l�anno in corso con i relativi importi. Nel ricordarvi che abbiamo provveduto a far pervenire direttamente ai lavoratori quanto di loro spettanza tramite bonifico su conto corrente o carta prepagata intestati ai beneficiari stessi, certifichiamo che gli importi erogati sono corrispondenti a quelli effettivamente accantonati per il periodo in oggetto. Provvediamo, altres�, ad elencare i nominativi dei Vostri dipendenti che non hanno incassato quanto di loro competenza in quanto non ci � stata fornita indicazione del mezzo di pagamento elettronico prescelto (conto corrente o carta prepagata). Vi saremmo, pertanto, grati se voleste avvertirli di prendere contatto con i nostri uffici. I dati riportati potranno subire variazioni nel corso delle successive consultazioni recependo gli aggiornamenti di volta in volta intercorsi. La situazione � aggiornata alla data riportata nel prospetto sottostante. </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelNoDati" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelNoDati" runat="server">I periodi di consultazione dei report sono: <ul><li><b>report</b> relativo al trattamento economico per ferie (<b>�cartella� di luglio</b>): visualizzabile dall�inizio dell�erogazione del pagamento <b>fino a fine settembre</b>;</li><li><b>report</b> relativo alla gratifica natalizia (<b>�cartella� di dicembre</b>): visualizzabile dall�inizio dell�erogazione del pagamento <b>fino a fine gennaio.</b></li></ul>Ne approfittiamo per precisare che i periodi di contribuzione delle �cartelle� sono:<ul><li>�cartella� di luglio: da ottobre a marzo</li><li>�cartella� di dicembre: da aprile a settembre</li></ul>Per ulteriori informazioni l�impresa pu� contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 <b>�Servizi ai Lavoratori�</b>, tasto 1 <b>�Cartella e Anzianit� Professionale Edile�.</b></asp:Label>--%>
                <asp:Label ID="LabelNoDati" runat="server">I periodi di consultazione dei report sono: <ul><li><b>riepilogo</b> relativo al trattamento economico per ferie (<b>�cartella� di luglio</b>): visualizzabile dall�inizio dell�erogazione del pagamento <b>fino a fine settembre</b>;</li><li><b>riepilogo</b> relativo alla gratifica natalizia (<b>�cartella� di dicembre</b>): visualizzabile dall�inizio dell�erogazione del pagamento <b>fino a fine gennaio.</b></li></ul>Ne approfittiamo per precisare che i periodi di contribuzione delle �cartelle� sono:<ul><li>�cartella� di luglio: da ottobre a marzo</li><li>�cartella� di dicembre: da aprile a settembre</li></ul>Per ulteriori informazioni l�impresa pu� contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 <b>�Servizi ai Lavoratori�</b>, tasto 1 <b>�Cartella e Anzianit� Professionale Edile�.</b></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelPremioFedelta" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelCartella" runat="server">Riportiamo di seguito l�elenco dei Vostri dipendenti che hanno percepito il trattamento economico per ferie/la gratifica natalizia per l�anno in corso con i relativi importi. Nel ricordarvi che abbiamo provveduto a far pervenire direttamente ai lavoratori quanto di loro spettanza tramite bonifico su conto corrente o carta prepagata intestati ai beneficiari stessi, sottolineiamo che gli importi erogati devono corrispondere a quelli effettivamente accantonati per il periodo in oggetto. Provvediamo, altres�, ad elencare i nominativi dei Vostri dipendenti che, per ragioni varie a noi non note, non hanno incassato quanto di loro competenza. Vi saremmo, pertanto, grati se voleste avvertirli di prendere contatto con i nostri uffici. La situazione � aggiornata alla data riportata nel report. </asp:Label>--%>
                <asp:Label ID="LabelPremioFedelta" runat="server">
                Riportiamo di seguito l�elenco dei nominativi dei Vostri dipendenti assegnatari del Premio di Fedelt� relativo all�anno corrente.
                <br /><br />
                Vi ricordiamo che il Premio Fedelt� consiste in un�erogazione annuale dell�importo fisso pari a � 250,00 lordi (� 192,50 netti) per beneficiario da destinare ai lavoratori dipendenti da almeno otto anni da impresa iscritta per lo stesso periodo alla Cassa Edile di Milano.
                <br /><br />
                L�individuazione dei nominativi � stata effettuata sulla base dei requisiti di accantonamento orario necessari per la fruizione della prestazione in oggetto, riportati in dettaglio sul nostro sito internet nella sezione <b>�Lavoratori�</b> alla voce <b>"Informazioni Operative�</b>, <b>�Prestazioni e Modulistica�</b>, <b>�Normativa e Modulistica�</b>.
                </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelNoDatiPremioFedelta" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelNoDati" runat="server">I periodi di consultazione dei report sono: <ul><li><b>report</b> relativo al trattamento economico per ferie (<b>�cartella� di luglio</b>): visualizzabile dall�inizio dell�erogazione del pagamento <b>fino a fine settembre</b>;</li><li><b>report</b> relativo alla gratifica natalizia (<b>�cartella� di dicembre</b>): visualizzabile dall�inizio dell�erogazione del pagamento <b>fino a fine gennaio.</b></li></ul>Ne approfittiamo per precisare che i periodi di contribuzione delle �cartelle� sono:<ul><li>�cartella� di luglio: da ottobre a marzo</li><li>�cartella� di dicembre: da aprile a settembre</li></ul>Per ulteriori informazioni l�impresa pu� contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 <b>�Servizi ai Lavoratori�</b>, tasto 1 <b>�Cartella e Anzianit� Professionale Edile�.</b></asp:Label>--%>
                <asp:Label ID="LabelLabelNoDatiPremioFedelta" runat="server">Per ulteriori informazioni l�impresa pu� contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 �<b>Servizi ai Lavoratori</b>�, tasto 9 �<b>Prestazioni</b>�.</asp:Label>
            </td>
        </tr>
    </table>
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerImprese" runat="server" Visible="false" OnInit="ReportViewerImprese_Init"
                    ProcessingMode="Remote" Font-Names="Verdana" Font-Size="8pt" SizeToReportContent="True"
                    DocumentMapCollapsed="True" Height="550pt" Width="550pt">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
