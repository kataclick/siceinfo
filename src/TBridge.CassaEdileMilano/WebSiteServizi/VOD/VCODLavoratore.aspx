﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VCODLavoratore.aspx.cs"Inherits="VOD_VCODLavoratore" Title="VCOD" %>

<%@ Register Src="../WebControls/MenuVOD.ascx" TagName="MenuVOD" TagPrefix="uc2" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Ore Denunciate"
        Visible="true" />
    <br />
    <br />
    <table Height="490px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerVOD" runat="server" OnInit="ReportViewerVOD_Init"
        ProcessingMode="Remote" Width="750px" Height="490px" OnPageNavigation="void_name">
    </rsweb:ReportViewer>
    </td></tr></table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuVOD ID="MenuVOD1" runat="server"></uc2:MenuVOD>
</asp:Content>
