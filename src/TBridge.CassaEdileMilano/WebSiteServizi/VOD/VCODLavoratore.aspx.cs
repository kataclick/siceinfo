﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class VOD_VCODLavoratore : Page
{
    private string idEsattore;

    protected void Page_Load(object sender, EventArgs e)
    {
        //
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.VodElencoImpreseConsulenti);
        funzionalita.Add(FunzionalitaPredefinite.VodElencoImpreseMedie);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "ReportVOD.aspx");

        if (GestioneUtentiBiz.IsEsattore())
        {
            //idEsattore = ((Esattore) (HttpContext.Current.User).Identity).Entity.IdEsattore;
            idEsattore = ((Esattore) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdEsattore;

            //TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz gu = new TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz();

            //TBridge.Cemi.GestioneUtentiBiz.Data.Entities.Esattore
            //    esattoreEntity = gu.GetUtenteEsattore(TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente());

            //if (esattoreEntity != null)
            //{
            //    idEsattore = esattoreEntity.IdEsattore;
            //}
        }
    }

    protected void ReportViewerVOD_Init(object sender, EventArgs e)
    {
        ReportViewerVOD.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        string tipoReport = Request.QueryString["tipo"];
        if (tipoReport == "ImpreseEsattori")
        {
            ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportImpreseConsulenti";
            TitoloSottotitolo1.sottoTitolo = "Elenco Imprese - Consulenti - Esattori";
        }
        else if (tipoReport == "ImpreseMedie")
        {
            ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportElencoImpreseWithMedia";
            TitoloSottotitolo1.sottoTitolo = "Elenco Imprese ordinate per fascia";

            DateTime dataFine = DateTime.Today;
            DateTime dataInizio = new DateTime(dataFine.Year, dataFine.Month, 1);
            dataInizio = dataInizio.AddMonths(-13);

            ReportParameter[] listaParam;
            ReportParameter param1 = new ReportParameter("dataInizio", dataInizio.ToShortDateString());
            ReportParameter param2 = new ReportParameter("dataFine", dataFine.ToShortDateString());

            if (idEsattore != null)
            {
                ReportParameter param3 = new ReportParameter("idEsattore", idEsattore);
                listaParam = new ReportParameter[3];
                listaParam[0] = param1;
                listaParam[1] = param2;
                listaParam[2] = param3;
            }
            else
            {
                listaParam = new ReportParameter[2];
                listaParam[0] = param1;
                listaParam[1] = param2;
            }

            ReportViewerVOD.ServerReport.SetParameters(listaParam);
        }
        else if (tipoReport == "scheda")
        {
            ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportSchedaImpresa";
            ReportViewerVOD.Width = new Unit(1050);
            TitoloSottotitolo1.sottoTitolo = "Scheda impresa";
            Theme = "CETheme2009Wide";

            try
            {
                DateTime dataDa = DateTime.Parse(Request.QueryString["dataDa"]);
                DateTime dataA = DateTime.Parse(Request.QueryString["dataA"]);
                string idEsattoreParam = Request.QueryString["idEsattore"];
                string idImpParam = Request.QueryString["idImpresa"];

                ReportParameter[] listaParam = new ReportParameter[4];
                ReportParameter param1 = new ReportParameter("dataDa", dataDa.ToShortDateString());
                ReportParameter param2 = new ReportParameter("dataA", dataA.ToShortDateString());
                ReportParameter param3 = new ReportParameter("idEsattore", idEsattoreParam);
                ReportParameter param4 = new ReportParameter("idImpresa", idImpParam);

                listaParam[0] = param1;
                listaParam[1] = param2;
                listaParam[2] = param3;
                listaParam[3] = param4;

                ReportViewerVOD.ServerReport.SetParameters(listaParam);
            }
            catch
            {
                //
            }
        }
        else if (tipoReport == "dettaglioOreLavoratore")
        {
            ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportDettaglioOrePerLavoratore";
            TitoloSottotitolo1.sottoTitolo = "Dettaglio ore";

            try
            {
                DateTime dataDa = DateTime.Parse(Request.QueryString["dataDa"]);
                DateTime dataA = DateTime.Parse(Request.QueryString["dataA"]);
                string idImpParam = Request.QueryString["idImpresa"];

                ReportParameter[] listaParam = new ReportParameter[3];
                ReportParameter param1 = new ReportParameter("dataDa", dataDa.ToShortDateString());
                ReportParameter param2 = new ReportParameter("dataA", dataA.ToShortDateString());
                ReportParameter param3 = new ReportParameter("idImpresa", idImpParam);

                listaParam[0] = param1;
                listaParam[1] = param2;
                listaParam[2] = param3;

                ReportViewerVOD.ServerReport.SetParameters(listaParam);
            }
            catch
            {
                //
            }
        }
        else if (tipoReport == "dettaglioOrePeriodo")
        {
            ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportDettaglioOrePerPeriodo";
            TitoloSottotitolo1.sottoTitolo = "Dettaglio ore";

            try
            {
                DateTime dataDa = DateTime.Parse(Request.QueryString["dataDa"]);
                DateTime dataA = DateTime.Parse(Request.QueryString["dataA"]);
                string idImpParam = Request.QueryString["idImpresa"];

                ReportParameter[] listaParam = new ReportParameter[3];
                ReportParameter param1 = new ReportParameter("dataDa", dataDa.ToShortDateString());
                ReportParameter param2 = new ReportParameter("dataA", dataA.ToShortDateString());
                ReportParameter param3 = new ReportParameter("idImpresa", idImpParam);

                listaParam[0] = param1;
                listaParam[1] = param2;
                listaParam[2] = param3;

                ReportViewerVOD.ServerReport.SetParameters(listaParam);
            }
            catch
            {
                //
            }
        }
        else if (tipoReport == "reportVCOD")
        {
            ReportViewerVOD.ServerReport.ReportPath = "/ReportVOD/ReportVCOD2";
            TitoloSottotitolo1.sottoTitolo = "VCOD";
            ReportViewerVOD.Width = new Unit(1050);
            Theme = "CETheme2009Wide";

            try
            {
                DateTime dataDa = DateTime.Parse(Request.QueryString["dataDa"]);
                DateTime dataA = DateTime.Parse(Request.QueryString["dataA"]);
                string idImpParam = Request.QueryString["idImpresa"];

                ReportParameter[] listaParam = new ReportParameter[3];
                ReportParameter param1 = new ReportParameter("dataDa", dataDa.ToShortDateString());
                ReportParameter param2 = new ReportParameter("dataA", dataA.ToShortDateString());
                ReportParameter param3 = new ReportParameter("idImpresa", idImpParam);

                listaParam[0] = param1;
                listaParam[1] = param2;
                listaParam[2] = param3;

                ReportViewerVOD.ServerReport.SetParameters(listaParam);
            }
            catch
            {
                //
            }
        }
    }

    public void void_name(object sender, PageNavigationEventArgs e)
    {
        ReportViewerVOD.HyperlinkTarget = "_top";
    }
}