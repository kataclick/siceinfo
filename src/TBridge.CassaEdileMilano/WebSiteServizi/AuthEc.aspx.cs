﻿using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class AuthEc : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        /*
        codiceCe: codice cassa edile di impresa/consulente
        guid: quello che ha generato Edilconnect
        partitaiva: ...
        tipoUtente: 1=consulente; 2=impresa
        loginEc: MI00
        passwordEc: 1qazxsw2
         */

        try
        {
            string usernameAuthEc = ConfigurationManager.AppSettings["AuthEcUsername"];
            string passwordAuthEc = ConfigurationManager.AppSettings["AuthEcPassword"];

            int codiceCe = Convert.ToInt32(Request["codiceCe"]);
            string guidEc = Request["guid"];
            string partitaIva = Request["partitaIva"];
            int tipoUtente = Convert.ToInt32(Request["tipoUtente"]);
            string usernameEc = Request["loginEc"];
            string passwordEcCriptata = Request["passwordEc"];

            String passwordEcCriptataDecriptata = CEXChangeCryptography.cxAESDecryptToString(passwordEcCriptata,
                                                                                             passwordAuthEc);
            String[] splitted = passwordEcCriptataDecriptata.Split(' ');

            if (splitted.Length == 2)
            {
                String pIvaCript = splitted[0];
                String timeUtcStr = splitted[1];

                DateTime timeUtc = DateTime.FromFileTimeUtc(long.Parse(timeUtcStr));
                DateTime adesso = DateTime.UtcNow;

                if (usernameEc == usernameAuthEc && pIvaCript == partitaIva && !String.IsNullOrWhiteSpace(guidEc)
                    && timeUtc.Day == adesso.Day && timeUtc.Month == adesso.Month && timeUtc.Year == adesso.Year)
                {
                    String userName = GestioneUtentiBiz.GetUserNamePerEdilConnect(tipoUtente, codiceCe, guidEc,
                                                                                  partitaIva);

                    if (!String.IsNullOrEmpty(userName))
                    {
                        ImpersonateManager.ImpersonateEdilConnect(userName);
                    }
                    else
                    {
                        switch (tipoUtente)
                        {
                            case 1:
                                // Consulente
                                Server.Transfer("~/GestioneUtentiRegistrazioneConsulente.aspx");
                                break;
                            case 2:
                                // Impresa
                                Server.Transfer("~/GestioneUtentiRegistrazioneImpresa.aspx");
                                break;
                        }
                    }
                }
            }
        }
        catch
        {
            throw;
        }

        Server.Transfer("~/Default.aspx");
    }
}