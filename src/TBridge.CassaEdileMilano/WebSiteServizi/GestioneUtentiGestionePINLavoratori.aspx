<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneUtentiGestionePINLavoratori.aspx.cs" Inherits="GestioneUtentiGestionePINLavoratori" Theme="CETheme2009Wide" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register src="WebControls/MenuGestionePin.ascx" tagname="MenuGestionePin" tagprefix="uc3" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuGestionePin ID="MenuGestionePin1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">      
        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openRadWindowSms(idLavoratore) {
            var oWindow = radopen("GestioneSmsLavoratore.aspx?idLavoratore=" + idLavoratore, null);
            oWindow.set_title("Gestione SMS");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(460, 360);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }        
    </script>
</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.ico" />
    
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione utenti"
        sottoTitolo="Gestione PIN per i lavoratori" />
    <br />
    <table width="90%">
        <tr>
            <td style="width: 119px; text-align:left">
                Codice lavoratore:
            </td>
            <td style="width: 380px; text-align:left">
                <asp:TextBox ID="TextBoxCodLavoratore" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Formato non valido"
                    ControlToValidate="TextBoxCodLavoratore" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 119px; text-align:left">
                Cognome:
            </td>
            <td style="width: 380px; text-align:left">
                <asp:TextBox ID="TextBoxCognome" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 119px;; text-align:left">
                Nome:
            </td>
            <td style="width: 380px; text-align:left">
                <asp:TextBox ID="TextBoxNome" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                Codice Fiscale:
            </td>
            <td style="width: 380px; text-align:left">
                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 119px; text-align:left">
                Codice impresa:
            </td>
            <td style="width: 380px; text-align:left">
                <asp:TextBox ID="TextBoxCodImpresa" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Formato non valido"
                    ControlToValidate="TextBoxCodImpresa" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                Ragione sociale:
            </td>
            <td align="left" style="width: 380px">
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                Con PIN:
            </td>
            <td align="left" style="width: 380px">
                <asp:DropDownList ID="DropDownListTipoRicerca" runat="server" Width="306px">
                    <asp:ListItem Value="NONDEF">Entrambi</asp:ListItem>
                    <asp:ListItem Value="SENZAPIN">Lavoratori senza PIN</asp:ListItem>
                    <asp:ListItem Value="CONPIN">Lavoratori con PIN</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                PIN generato
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 119px">
                Dal (gg/mm/aaaa):
            </td>
            <td style="width: 380px" align="left">
                <asp:TextBox ID="TextBoxDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDal"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 119px">
                Al (gg/mm/aaaa):
            </td>
            <td style="width: 380px" align="left">
                <asp:TextBox ID="TextBoxAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxAl"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Iscritto al sito web
            </td>
            <td align="left" style="width: 380px">
                <asp:DropDownList ID="DropDownListIscrittoWeb" runat="server" Width="306px">
                    <asp:ListItem Value="NONDEF">Tutti</asp:ListItem>
                    <asp:ListItem Value="ISCRITTI">Iscritti</asp:ListItem>
                    <asp:ListItem Value="NONISCRITTI">Non iscritti</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                Num. Cellulare:
            </td>
            <td align="left" style="width: 380px">
                <asp:TextBox ID="TextBoxCell" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:CustomValidator ID="CellValidator" runat="server" ControlToValidate ="TextBoxCell" ErrorMessage="Formato non valido" OnServerValidate="CellValidator_ServerValidate"></asp:CustomValidator>
                <% //<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCell" ErrorMessage="Formato non valido" ValidationExpression="^(\+39)?\d{15}$"></asp:RegularExpressionValidator> %>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" style="width: 380px">
                <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Ricerca" />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelError" runat="server" Font-Bold="True" ForeColor="#FF0000"></asp:Label><br />
    <br />
    <strong>Risultati della ricerca</strong>&nbsp;<br />
    <asp:GridView ID="GridViewLavoratori" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="idLavoratore,IndirizzoDenominazione,IndirizzoComune,IndirizzoCap,IndirizzoProvincia"
        OnPageIndexChanging="GridViewLavoratori_PageIndexChanging" PageSize="20" Width="90%"
        OnRowDataBound="GridViewLavoratori_RowDataBound" OnRowCommand="GridViewLavoratori_RowCommand">
        <Columns>
            <asp:BoundField DataField="idLavoratore" HeaderText="Cod Lav" />
            <asp:BoundField DataField="cognome" HeaderText="Cognome">
                <ItemStyle Width="30%" />
                <HeaderStyle Width="30%" />
            </asp:BoundField>
            <asp:BoundField DataField="nome" HeaderText="Nome" />
            <asp:BoundField DataField="codiceFiscale" HeaderText="Codice Fiscale">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField DataField="idImpresa" HeaderText="Cod Imp" />
            <asp:BoundField DataField="RagioneSocialeImpresa" HeaderText="Ragione Sociale" />
            <asp:BoundField HeaderText="Username" DataField="Username" />
            <asp:BoundField DataField="PIN" HeaderText="PIN" SortExpression="PIN">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField DataField="dataGenerazionePIN" HeaderText="Data generazione PIN"
                DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:BoundField DataField="DataInvioSmsPin" HeaderText="Data Invio SMS PIN"
                DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:TemplateField HeaderText="Numero Telefono">
                <ItemTemplate>
                    <asp:TextBox ID="TextBoxNumeroTel" runat="server" Text='<%# Bind("NumeroTelefono") %>' ReadOnly="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Inserisci nel file">
                <ItemStyle Width="5%" />
                <HeaderStyle Width="5%" />
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxNelFile" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" Text="Genera PIN"
                CommandName="Genera" Visible="false">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="15%" />
            </asp:ButtonField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="ButtonSms" runat="server" CausesValidation="false" 
                        CommandName="Sms" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" Text="Gestisci" />
                </ItemTemplate>
                <ControlStyle CssClass="bottoneGriglia" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>                  
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="ButtonImpersona" runat="server" CausesValidation="false" 
                        CommandName="Impersona" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" Text="Impersona" />
                </ItemTemplate>
                <ControlStyle CssClass="bottoneGriglia" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>            
        </Columns>
        <EmptyDataTemplate>
            Nessun lavoratore trovato
        </EmptyDataTemplate>
    </asp:GridView>
    <table width="80%">
        <tr>
            <td align="right" style="width: 517px">
                <asp:LinkButton ID="LinkButtonSelezionaTutti" runat="server" OnClick="LinkButtonSelezionaTutti_Click"
                    Visible="False">Seleziona tutti</asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="LinkButtonDeselezionaTutti" runat="server" OnClick="LinkButtonDeselezionaTutti_Click"
                    Visible="False">Deseleziona tutti</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="width: 517px">
                <table class="standardTable">
                    <tr>
                        <td align="left">
                            <asp:Label ID="LabelEsporta" runat="server" Text="Esporta Excel" Visible="False"></asp:Label>&nbsp;
                        </td>
                        <td style="width: 141px">
                        </td>
                        <td align="left">
                            <asp:Label ID="LabelGenera" runat="server" Text="Genera PIN" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="ButtonEsportaExcel" runat="server" OnClick="ButtonEsportaExcel_Click"
                                Text="Solo selezionati" Visible="False" Width="120px" />
                        </td>
                        <td align="left" style="width: 141px">
                        </td>
                        <td>
                            <asp:Button ID="ButtonGeneraPIN" runat="server" OnClick="ButtonGeneraPIN_Click" Text="Nella pagina"
                                Visible="False" Width="120px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="ButtonFileTutti" runat="server" OnClick="ButtonFileTutti_Click" Text="Tutti"
                                Visible="False" Width="120px" />
                        </td>
                        <td align="left" style="width: 141px">
                        </td>
                        <td>
                            <asp:Button ID="ButtonGeneraTutti" runat="server" OnClick="ButtonGeneraTutti_Click"
                                Text="Tutti" Visible="False" Width="120px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
