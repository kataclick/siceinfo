﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneCommittente.aspx.cs" Inherits="GestioneUtentiRegistrazioneCommittente" %>

<%@ Register src="WebControls/MenuGestioneUtenti.ascx" tagname="MenuGestioneUtenti" tagprefix="uc1" %>
<%@ Register src="WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<%@ Register src="WebControls/RegistrazioneCommittente.ascx" tagname="RegistrazioneCommittente" tagprefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione utenti" sottoTitolo="Registrazione committente" />
    <br />
    <uc3:RegistrazioneCommittente ID="RegistrazioneCommittente1" runat="server" />
</asp:Content>


