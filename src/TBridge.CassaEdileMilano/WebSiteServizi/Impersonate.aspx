<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Impersonate.aspx.cs" Inherits="Impersonate" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="MainPage">
    <asp:Panel
        ID="PanelImpersona"
        runat="server"
        DefaultButton="ButtonImpersonifica">
        <telerik:RadTextBox ID="RadTextBoxUsername" runat="server" Width="220px" />
        <asp:Button ID="ButtonImpersonifica" runat="server" Text="Impersona" 
            onclick="ButtonImpersonifica_Click" />
    </asp:Panel>
</asp:Content>
