using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ReportIscrizioneCE : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEIscrizione,
                                              "~/ReportIscrizioneCE.aspx.aspx");

        if (Session["IdDomanda"] != null)
        {
            int? idDomanda = (int?) Session["IdDomanda"];
            string tipoModulo = Request.QueryString["tipoModulo"];

            if (idDomanda.HasValue)
            {
                ReportViewerIscrizioneCE.ServerReport.ReportServerUrl =
                    new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                if (tipoModulo == "prevedi" || tipoModulo == "impiegati" || tipoModulo == "unico")
                    ReportViewerIscrizioneCE.ServerReport.ReportPath =
                        "/ReportIscrizioneCE/ReportModuloIscrizionePrevedi";
                else
                    ReportViewerIscrizioneCE.ServerReport.ReportPath = "/ReportIscrizioneCE/ReportModuloIscrizione";

                ReportParameter[] listaParam = new ReportParameter[1];
                listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());

                ReportViewerIscrizioneCE.ServerReport.SetParameters(listaParam);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                //PDF

                byte[] bytes = ReportViewerIscrizioneCE.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", "attachment;filename=IscrizioneCE.pdf");
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }
    }
}