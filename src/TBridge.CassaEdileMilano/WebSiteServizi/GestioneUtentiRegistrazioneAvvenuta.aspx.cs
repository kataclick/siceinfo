using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
//using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class GestioneUtentiRegistrazioneAvvenuta : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.GestioneUtentiRegistrazione,
                                                             FunzionalitaPredefinite.GestioneUtentiGestisciUtenti
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        //GestioneUtentiBiz gu = new GestioneUtentiBiz();

        //if (Context.Items["idUtente"] != null)
        //{
        //    int idUtente = Int32.Parse(Context.Items["idUtente"].ToString());

        //    Utente ut = new Utente();
        //    ut = gu.GetUtenteById(idUtente);

        //    LabelUsername.Text = ut.UserName;
        //    LabelPassword.Text = ut.Password;
        //}
    }
}