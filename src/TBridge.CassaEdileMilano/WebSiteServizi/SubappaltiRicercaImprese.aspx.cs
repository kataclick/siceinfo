using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Subappalti.Business;

public partial class SubappaltiRicercaImprese : Page
{
    /// <summary>
    /// Utente che sta utilizzando la pagina
    /// </summary>
    private int idUtente;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Controllo l'autorizzazione dell'utente a vedere la pagina ...
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.SubappaltiRicerca,
                                              "SubappaltiRicercaImprese.aspx");

        // Ricavo l'utente che sta utilizzando la pagina
        //IUtente utente = ApplicationInstance.GetUtenteSistema();

        // Va settato sempre
        //idUtente = utente.IdUtente;

        idUtente = GestioneUtentiBiz.GetIdUtente();

        if (!IsPostBack)
        {
            // Inizializzazioni
        }

        // TODO: try catch
        SubappaltiBusiness business = new SubappaltiBusiness();

        if (!business.RicercaPossibile(idUtente))
        {
            // TODO: redirect?
            lblErrMessage.Text = "Sono gi� state effettuate 10 ricerche nella giornata";
            lblErrMessage.Visible = true;
            btnRicerca.Enabled = false;
        }
    }

    protected void btnRicerca_Click(object sender, EventArgs e)
    {
        // TODO: try catch
        CaricaListaImprese();
    }

    private void CaricaListaImprese()
    {
        // Controllo validit� campi, o almeno uno obbligatorio
        SubappaltiBusiness business = new SubappaltiBusiness();

        if (string.IsNullOrEmpty(txtCodice.Text) && string.IsNullOrEmpty(txtRagioneSociale.Text))
        {
            // Segnalo obbligatorio almeno uno dei due campi ... 
            // purtroppo lo devo far fare al server
            lblErrMessage.Visible = true;
        }
        else if (!string.IsNullOrEmpty(txtCodice.Text))
        {
            // Caso in cui sia stato valorizzato il codice

            // Resetto il messaggio di errore
            lblErrMessage.Visible = false;

            int idImpresa;
            if (int.TryParse(txtCodice.Text, out idImpresa))
            {
                gvListaImprese.DataSource = business.RicercaImpresa(idImpresa);
            }
        }
        else
        {
            // Caso in cui sia stato valorizzata la ragione sociale

            // Resetto il messaggio di errore
            lblErrMessage.Visible = false;

            // Salvo la datasource nella session perch� mi serve per il paging
            Session["Subappalti.Datasource"] = business.RicercaImpresa(txtRagioneSociale.Text);

            gvListaImprese.DataSource = Session["Subappalti.Datasource"];
        }

        LabelRicerca.Visible = true;

        gvListaImprese.DataBind();

        //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
        //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
        //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
        //TBridge.Cemi.ActivityTracking.Log.Write("Subappalti Visualizza lista imprese", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);
    } // CaricaListaImprese

    private void CaricaSchedaImpresa()
    {
        // TODO: problema. nei button fields cells[0].text non � il valore => non funziona
        // La soluzione pi� semplice � che Codice sia un boundField

        Session["Subappalti.IdImpresa"] = gvListaImprese.SelectedRow.Cells[0].Text;
        //Per accedere ad un'impresa prima passiamo attraverso il disclaimer

        Response.Redirect("./SubappaltiLiberatoria.aspx");
        //Response.Redirect("./SubappaltiSchedaImpresa.aspx");
    }

    protected void gvListaImprese_SelectedIndexChanged(object sender, EventArgs e)
    {
        //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
        //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
        //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
        //TBridge.Cemi.ActivityTracking.Log.Write("Subappalti carica scheda impresa", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);

        CaricaSchedaImpresa();
    }

    protected void gvListaImprese_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // A sto punto la datasource � nulla ... la devo ricaricare
        gvListaImprese.DataSource = Session["Subappalti.Datasource"];

        gvListaImprese.PageIndex = e.NewPageIndex;
        gvListaImprese.DataBind();
    }
} // Class