﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RiepilogoStampabili.aspx.cs" Inherits="BollettiniFreccia_RiepilogoStampabili" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <telerik:RadAjaxManagerProxy runat="server" ID="RadAjaxManagerProxy1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ButtonFiltra">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewBollettiniFreccia" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CustomValidatorStampa" />
                    <telerik:AjaxUpdatedControl ControlID="CustomValidatorStampa1" />
                    <telerik:AjaxUpdatedControl ControlID="LabelFiltro" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Riepilogo Bollettini Freccia"
        titolo="Bollettino Freccia" />
    <br />
    <asp:Panel ID="ListaBollettini" runat="server" DefaultButton="ButtonFiltra">
        <div class="borderedDiv">
            <table class="standardTable">
                <tr>
                    <td class="centermain">
                        Codice:
                    </td>
                    <td>
                        <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                            ID="RadTextBoxIdImpresa" runat="server" Width="220px">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td class="centermain">
                        Ragione sociale:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="220px">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="centermain">
                        Anno:
                    </td>
                    <td>
                        <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                            ID="RadTextBoxAnno" runat="server" Width="220px" MinValue="1900">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td class="centermain">
                        Mese:
                    </td>
                    <td>
                        <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" MinValue="1" MaxValue="12"
                            NumberFormat-GroupSeparator="" ID="RadTextBoxMese" runat="server" Width="220px">
                        </telerik:RadNumericTextBox>
                    </td>
                </tr>
            </table>
            <div style="padding: 10px;">
                <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Filtra" />
                <br />
            </div>
            <div style="padding: 10px;">
                <telerik:RadGrid ID="GridViewBollettiniFreccia" runat="server" Width="100%" AllowPaging="True"
                    OnPageIndexChanged="GridViewBollettiniFreccia_PageIndexChanged" OnExcelExportCellFormatting="GridViewBollettiniFreccia_ExcelExportCellFormatting">
                    <MasterTableView DataKeyNames="idImpresa,ragioneSociale,anno,mese,importo,CIP">
                        <Columns>
                            <telerik:GridClientSelectColumn />
                            <telerik:GridBoundColumn DataField="IdImpresa" HeaderText="Codice" />
                            <telerik:GridBoundColumn DataField="RagioneSociale" HeaderText="Ragione Sociale" />
                            <telerik:GridBoundColumn DataField="Anno" HeaderText="Anno" />
                            <telerik:GridBoundColumn DataField="Mese" HeaderText="Mese" />
                            <telerik:GridBoundColumn ItemStyle-Width="80px" DataField="Importo" HeaderText="Importo"
                                DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" />
                            <telerik:GridBoundColumn DataField="CIP" HeaderText="Identificativo del bollettino" />
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnableRowHoverStyle="true">
                        <Selecting AllowRowSelect="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <asp:Label ID="CustomValidatorStampa" runat="server" Text="Selezionare un bollettino da stampare"
                    Visible="false" ForeColor="Red" />
                <asp:Label ID="CustomValidatorStampa1" runat="server" ForeColor="Red" Text="Impossibile generare il bollettino. Selezionare un bollettino con codice identificativo valorizzato."
                    Visible="false" />
                <br />
                <asp:Button ID="ButtonStampa" runat="server" Text="Stampa selezionato" Width="150px"
                    Visible="true" OnClick="ButtonStampa_Click" />
                    <asp:Button ID="ButtonExportExcel" Width="150px" Text="Esporta" OnClick="ButtonExportExcel_Click"
            runat="server" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
