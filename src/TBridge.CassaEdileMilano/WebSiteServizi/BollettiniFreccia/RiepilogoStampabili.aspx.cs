﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;
using Telerik.Web.UI;
using UtenteImpresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
public partial class BollettiniFreccia_RiepilogoStampabili : System.Web.UI.Page
{
    private readonly Common bizCommon = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Pagina disabilitata
        throw new Exception("Pagina non più valida");

        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaStatistiche,
                                              "~/BollettiniFreccia/RiepilogoStampati.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            RadTextBoxAnno.MaxValue = DateTime.Today.Year;
            CaricaBollettiniStampabili();
            ButtonFiltra.Focus();
        }
    }

    private void CaricaBollettiniStampabili()
    {
        int? anno = (Int32?)RadTextBoxAnno.Value;
        int? mese = (Int32?)RadTextBoxMese.Value;

        BollettinoFrecciaStampabileFilter filtro = new BollettinoFrecciaStampabileFilter();
        filtro.Anno = anno;
        filtro.Mese = mese;
        filtro.IdImpresa = (Int32?)RadTextBoxIdImpresa.Value;
        filtro.RagioneSociale = RadTextBoxRagioneSociale.Text;

        BollettinoFrecciaStampabileCollection bollettini = bizCommon.GetBollettiniFrecciaStampabili(filtro);

        GridViewBollettiniFreccia.DataSource = bollettini;
        GridViewBollettiniFreccia.DataBind();
    }


    protected void ButtonStampa_Click(object sender, EventArgs e)
    {
        CustomValidatorStampa.Visible = true;
        CustomValidatorStampa1.Visible = false;
        
        if (GridViewBollettiniFreccia.SelectedIndexes.Count > 0)
        {
            CustomValidatorStampa.Visible = false;

            BollettinoFrecciaStampabile bollettino = new BollettinoFrecciaStampabile();

            bollettino.IdImpresa =
                (Int32)
                GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
                    Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["idImpresa"];
            bollettino.RagioneSociale =
                GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
                    Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["ragioneSociale"].ToString();

            bollettino.Anno =
                (Int32)
                GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
                    Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["anno"];
            bollettino.Mese =
                (Int32)
                GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
                    Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["mese"];
            bollettino.Cip =
                GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
                    Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["CIP"].ToString();
            bollettino.Importo =
                (Decimal)
                GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
                    Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["importo"];

            CustomValidatorStampa1.Visible = true;
            if (!String.IsNullOrEmpty(bollettino.Cip))
            {
                //Context.Items["idUtente"] = idUtente;
                Context.Items["bollettino"] = bollettino;

                //bizCommon.RegistraRichiesta(bollettino, idUtente);
                CustomValidatorStampa1.Visible = false;

                Server.Transfer("~/ReportBollettinoFreccia.aspx");
            }
        }
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        CustomValidatorStampa.Visible = false;
        CustomValidatorStampa1.Visible = false;
        CaricaBollettiniStampabili();
        
    }

    protected void GridViewBollettiniFreccia_ExcelExportCellFormatting(object source, ExcelExportCellFormattingEventArgs e)
    {
        switch (e.FormattedColumn.HeaderText)
        {
            default: e.Cell.Style["mso-number-format"] = @"\@"; break;
            case "DataRichiesta": e.Cell.Style["mso-number-format"] = @"mm\/dd\/yyyy"; break; break;
        }
    }

    protected void ButtonExportExcel_Click(object sender, EventArgs e)
    {
        CaricaBollettiniStampabili();
        GridViewBollettiniFreccia.ExportSettings.IgnorePaging = true;
        GridViewBollettiniFreccia.ExportSettings.FileName = "Riepilogo_Bollettini_Stampabili";
        GridViewBollettiniFreccia.MasterTableView.ExportToExcel();
    }

    protected void GridViewBollettiniFreccia_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaBollettiniStampabili();
    }
}