﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RiepilogoStampati.aspx.cs" Inherits="BollettiniFreccia_RiepilogoStampati" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <telerik:RadAjaxManagerProxy runat="server" ID="RadAjaxManagerProxy1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ButtonFiltra">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="GridViewBollettiniFreccia" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CustomValidatorStampa" />
                    <telerik:AjaxUpdatedControl ControlID="CustomValidatorStampa1" />
                    <telerik:AjaxUpdatedControl ControlID="LabelFiltro" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Riepilogo Pagamenti"
        titolo="Pagamenti" />
    <br />
    <asp:Panel ID="InformazioniGenerali" runat="server" Visible="true">
        <div class="borderedDiv">
            <table class="standardTable">
                <tr>
                    <td class="centermain" style="width: 320px;">
                        <b>Bollettini Stampabili:</b>
                    </td>
                    <td class="centermain">
                        <asp:Label ID="LabelBolletiniStampabili" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="centermain">
                        <b>Bollettini Stampati (*):</b>
                    </td>
                    <td class="centermain">
                        <asp:Label ID="LabelBolletiniStampati" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="centermain">
                        <b>Utenti che hanno stampato (*):</b>
                    </td>
                    <td class="centermain">
                        <asp:Label ID="LabelNumeroImprese" runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="centermain">
                        <asp:Label ID="LabelNumeroConsulenti" runat="server">
                        </asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Label ID="LabelNota" runat="server" ForeColor="Gray" Font-Size="Smaller" Text="(*) Dati conteggiati dal 02/02/2010" />
        </div>
    </asp:Panel>
    <br />
    <asp:Panel ID="ListaBollettini" runat="server" DefaultButton="ButtonFiltra">
        <div class="borderedDiv">
            <table class="standardTable">
                <tr>
                    <td class="centermain">
                        Ragione sociale:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="220px">
                        </telerik:RadTextBox>
                    </td>
                    <td class="centermain">
                        Data Stampa Inizio:
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerDataInizio" runat="server" Width="220px" />
                    </td>
                </tr>
                <tr>
                    <td class="centermain">
                        Anno:
                    </td>
                    <td>
                        <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                            ID="RadTextBoxAnno" runat="server" Width="220px" MinValue="1900">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td class="centermain">
                        Data Stampa Fine:
                    </td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerDataFine" runat="server" Width="220px" />
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="RadDatePickerDataFine"
                            ControlToCompare="RadDatePickerDataInizio" Operator="GreaterThanEqual" ValidationGroup="filtro"
                            runat="server" Type="Date">*</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td class="centermain">
                        Mese:
                    </td>
                    <td>
                        <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" MinValue="1" MaxValue="12"
                            NumberFormat-GroupSeparator="" ID="RadTextBoxMese" runat="server" Width="220px">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td class="centermain">
                        Canale Pagamento:
                    </td>
                    <td>
                        <telerik:RadComboBox ID="RadCombBoxTipoCanalePagamento" runat="server" Width="220px"
                            DataTextField="Descrizione" DataValueField="Id">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td class="centermain">
                        Codice Impresa:
                    </td>
                    <td>
                        <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                            ID="RadNumericTextBoxIdImpresa" runat="server" Width="220px">
                        </telerik:RadNumericTextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Filtra" />
                    </td>
                </tr>
            </table>
            <div style="padding: 10px;">
                <asp:Label ID="LabelFiltro" runat="server" Text="La data di fine non può essere antecedente a quella di inizio"
                    Visible="false" ForeColor="Red" />
            </div>
            <div style="padding: 5px;">
                <telerik:RadGrid ID="GridViewBollettiniFreccia" runat="server" Width="100%" AllowPaging="True"
                    OnPageIndexChanged="GridViewBollettiniFreccia_PageIndexChanged">
                    <MasterTableView DataKeyNames="idImpresa,ragioneSociale,anno,mese,importo">
                        <Columns>
                            <telerik:GridClientSelectColumn Visible="false" />
                            <telerik:GridBoundColumn DataField="IdImpresa" HeaderText="Codice" />
                            <telerik:GridBoundColumn DataField="RagioneSociale" HeaderText="Ragione Sociale" />
                            <telerik:GridBoundColumn DataField="Anno" HeaderText="Anno" />
                            <telerik:GridBoundColumn DataField="Mese" HeaderText="Mese" />
                            <telerik:GridBoundColumn DataField="Sequenza" HeaderText="Seq.Den." />
                            <telerik:GridDateTimeColumn DataField="DataRichiesta" HeaderText="Data Stampa" DataFormatString="{0:dd/MM/yyyy}" />
                            <telerik:GridBoundColumn ItemStyle-Width="80px" DataField="Importo" HeaderText="Importo"
                                DataFormatString="{0:C}" ItemStyle-HorizontalAlign="Right" />
                            <telerik:GridBoundColumn DataField="DescrizioneTipoCanalePagamento" HeaderText="Canale Pagamento" />
                            <telerik:GridDateTimeColumn DataField="DataVersamento" HeaderText="Data Versamento"
                                DataFormatString="{0:dd/MM/yyyy}" />
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnableRowHoverStyle="true">
                        <Selecting AllowRowSelect="True" />
                    </ClientSettings>
                </telerik:RadGrid>
                <br />
                <asp:Label ID="CustomValidatorStampa" runat="server" Text="Selezionare un bollettino da stampare"
                    Visible="false" ForeColor="Red" />
                <asp:Label ID="CustomValidatorStampa1" runat="server" ForeColor="Red" Text="Impossibile generare il bollettino. Selezionare un bollettino con codice identificativo valorizzato."
                    Visible="false" />
                <br />
                <asp:Button ID="ButtonStampa" runat="server" Text="Stampa selezionato" Width="150px"
                    Visible="false" OnClick="ButtonStampa_Click" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
