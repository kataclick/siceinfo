﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;
using Telerik.Web.UI;
using UtenteImpresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using TBridge.Cemi.Type.Enums;

public partial class BollettiniFreccia_RiepilogoStampati : Page
{
    private readonly Common bizCommon = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaStatistiche);

        #endregion

        if (!Page.IsPostBack)
        {
            RadTextBoxAnno.MaxValue = DateTime.Today.Year;
            RadDatePickerDataInizio.SelectedDate = DateTime.Today.AddMonths(-1);
            CaricaStatisticheBollettiniStampatiPagati();
            CaricaDati();
            ButtonFiltra.Focus();
        }
    }

    private void CaricaDati()
    {
        CaricaTipiCanalePagamento();
        CaricaBollettiniStampati();
    }

    private void CaricaTipiCanalePagamento()
    {
        TipoCanalePagamentoBolletinoCollection tipiPagmento = bizCommon.GetTipiCanalePagamentoBollettino(true);
        RadCombBoxTipoCanalePagamento.DataSource = tipiPagmento;
        RadCombBoxTipoCanalePagamento.DataBind();
    }

    private void CaricaBollettiniStampati()
    {
        BollettinoStampatoFilter filtro = new BollettinoStampatoFilter
                                              {
                                                  Anno = (Int32?) RadTextBoxAnno.Value,
                                                  Mese = (Int32?) RadTextBoxMese.Value,
                                                  DataInizio = RadDatePickerDataInizio.SelectedDate,
                                                  DataFine = RadDatePickerDataFine.SelectedDate,
                                                  RagioneSociale = RadTextBoxRagioneSociale.Text,
                                                  CanalePagamento = GetFiltroTipoCanalePagamento(),
                                                  IdImpresa = (Int32?) RadNumericTextBoxIdImpresa.Value
                                              };

        BollettinoStampatoCollection bollettini = bizCommon.GetBollettiniStampati(filtro);
        GridViewBollettiniFreccia.DataSource = bollettini;
        GridViewBollettiniFreccia.DataBind();
    }

    private void CaricaStatisticheBollettiniStampatiPagati() 
    {
        int numeroImprese;
        int numeroConsulenti;
        bizCommon.GetUtentiBollettiniStampati(out numeroImprese, out numeroConsulenti);
        LabelNumeroImprese.Text = String.Format("Imprese: {0}", numeroImprese);
        LabelNumeroConsulenti.Text = String.Format("Consulenti: {0}", numeroConsulenti);

        LabelBolletiniStampabili.Text = bizCommon.GetBollettiniFrecciaStampabili(null, null).ToString();

        //BollettinoFrecciaRichiesteSelectStatistichePagati
        BollettiniFrecciaStatistichePagati bollettiniFrecciaStatistichePagati =
            bizCommon.GetBollettiniStatistichePagati();

        LabelBolletiniStampati.Text = String.Format("{0} di cui {1} pagati per Tot. {2:C}",
                                                    bollettiniFrecciaStatistichePagati.NumeroBollettiniUniciStampati,
                                                    bollettiniFrecciaStatistichePagati.
                                                        NumeroBollettiniUniciStampatiPagati,
                                                    bollettiniFrecciaStatistichePagati.ImportoPagato);
    }

    private TipiCanalePagamento? GetFiltroTipoCanalePagamento()
    {
        TipiCanalePagamento? tipo = null;
        int id;
        if (int.TryParse(RadCombBoxTipoCanalePagamento.SelectedValue, out id) && id > 0)
        {
            tipo = (TipiCanalePagamento)id;
        }

        return tipo;
    }

    protected void ButtonStampa_Click(object sender, EventArgs e)
    {
        //CustomValidatorStampa.Visible = true;
        //CustomValidatorStampa1.Visible = false;
        //LabelFiltro.Visible = false;

        //if (GridViewBollettiniFreccia.SelectedIndexes.Count > 0)
        //{
        //    CustomValidatorStampa.Visible = false;

        //    BollettinoFrecciaStampato bollettino = new BollettinoFrecciaStampato();

        //    bollettino.IdImpresa =
        //        (Int32)
        //        GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
        //            Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["idImpresa"];
        //    bollettino.RagioneSociale =
        //        GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
        //            Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["ragioneSociale"].ToString();

        //    bollettino.Anno =
        //        (Int32)
        //        GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
        //            Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["anno"];
        //    bollettino.Mese =
        //        (Int32)
        //        GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
        //            Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["mese"];
        //    bollettino.Cip =
        //        GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
        //            Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["CIP"].ToString();
        //    bollettino.Importo =
        //        (Decimal)
        //        GridViewBollettiniFreccia.MasterTableView.DataKeyValues[
        //            Int32.Parse(GridViewBollettiniFreccia.SelectedIndexes[0])]["importo"];

        //    CustomValidatorStampa1.Visible = true;
        //    if (!String.IsNullOrEmpty(bollettino.Cip))
        //    {
        //        //Context.Items["idUtente"] = idUtente;
        //        Context.Items["bollettino"] = bollettino;

        //        //bizCommon.RegistraRichiesta(bollettino, idUtente);
        //        CustomValidatorStampa1.Visible = false;

        //        Server.Transfer("~/ReportBollettinoFreccia.aspx");
        //    }
        //}
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        CustomValidatorStampa.Visible = false;
        CustomValidatorStampa1.Visible = false;
        LabelFiltro.Visible = true;
        Page.Validate("filtro");
        if (Page.IsValid)
        {
            LabelFiltro.Visible = false;
            CaricaBollettiniStampati();
        }
    }

    protected void GridViewBollettiniFreccia_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaBollettiniStampati();
    }
}