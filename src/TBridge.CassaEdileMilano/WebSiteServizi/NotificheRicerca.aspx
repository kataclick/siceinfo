<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NotificheRicerca.aspx.cs" Inherits="NotificheRicerca" %>

<%@ Register Src="WebControls/NotificheImpresaSelezionata.ascx" TagName="NotificheImpresaSelezionata"
    TagPrefix="uc3" %>

<%@ Register Src="WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc1" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
<uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche" sottoTitolo="Ricerche "/>
    <br />
    <asp:Panel ID="PanelImprese" runat="server" Width="100%">
        <uc3:NotificheImpresaSelezionata ID="NotificheImpresaSelezionata1" runat="server" />
        <asp:CustomValidator ID="CustomValidatorImprese" runat="server" ErrorMessage="Selezionare una impresa"
            OnServerValidate="CustomValidatorImprese_ServerValidate" ValidationGroup="selezioneImpresa"></asp:CustomValidator></asp:Panel>
    <br />
    <br />
    Con questa funzione � possibile eseguire una ricerca relativa alle variazioni di
    rapporti di lavoro notificate nei due mesi precedenti.<br />
    <br />
    Per conoscere e visualizzare il numero di rapporti cessati o il numero di rapporti
    instaurati selezionare la voce interessata.<br />
    <br />
    <table class="standardTable">
        <tr>
            <td align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonAssunzioni" runat="server" GroupName="scelta"
                    Text="Rapporti di assunzione notificati" Checked="True" /></td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonCessazioni" runat="server" GroupName="scelta"
                    Text="Rapporti di cessazione notificati" /></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 24px" align="left">
                <br />
                <asp:Button ID="ButtonOk" runat="server" Text="Cerca" OnClick="ButtonOk_Click" Width="89px" ValidationGroup="selezioneImpresa" />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Label ID="LabelTitoloElenco" runat="server" Font-Bold="True" Text="Elenco notifiche valide"
        Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewLavoratori" runat="server" Width="100%" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging" PageSize="8">
        <Columns>
            <asp:TemplateField HeaderText="Fonte">
                <ItemTemplate>
                    <%#StringaTipo((TBridge.Cemi.Notifiche.Type.Entities.TipoNotifica)DataBinder.Eval(Container.DataItem, "TipoNotifica"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Cognome" DataField="Cognome" />
            <asp:BoundField HeaderText="Nome" DataField="Nome" />
            <asp:BoundField HeaderText="Data nascita" DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:BoundField HeaderText="Comune nascita" DataField="LuogoNascita" />
            <asp:BoundField HeaderText="Sesso" DataField="Sesso" />
            <asp:BoundField HeaderText="Codice fiscale" DataField="CodiceFiscale" />
            <asp:BoundField HeaderText="Data assunzione" DataField="DataAssunzione" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:BoundField DataField="DataCessazione" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data cessazione"
                HtmlEncode="False" />
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:Button ID="ButtonStampa" runat="server" OnClick="ButtonStampa_Click" Text="Stampa"
        Visible="False" Width="150px" />&nbsp;<asp:Button ID="ButtonExcel" runat="server" OnClick="ButtonExcel_Click"
            Text="Esporta in Excel" Visible="False" Width="150px" />&nbsp;<asp:Button ID="ButtonWord" runat="server"
                OnClick="ButtonWord_Click" Text="Esporta in Word" Visible="False" Width="150px" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuNotifiche ID="MenuNotifiche1" runat="server" />
</asp:Content>
