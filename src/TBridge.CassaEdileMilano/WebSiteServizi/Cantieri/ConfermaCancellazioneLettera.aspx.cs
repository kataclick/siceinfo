using System;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Cantieri_ConfermaCancellazioneLettera : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriLettere, "CantieriLogLettere.aspx");
        ButtonAnnulla.Attributes.Add("onclick", "hystory.back();");

        if (!Page.IsPostBack)
        {
            LabelProtocollo.Text = Context.Items["protocollo"].ToString();
        }
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        string protocollo = LabelProtocollo.Text;

        if (biz.DeleteLettera(protocollo))
        {
            LabelErrore.Visible = false;
            Server.Transfer("~/Cantieri/CantieriLogLettere.aspx");
        }
        else
            LabelErrore.Visible = true;
    }
}