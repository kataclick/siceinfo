﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ElencoAttivita.aspx.cs" Inherits="Cantieri_ElencoAttivita" Theme="CETheme2009Wide" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/LegendaColoriAttivita.ascx" tagname="LegendaColoriAttivita" tagprefix="uc3" %>
<%@ Register src="../WebControls/MenuCantieriStatistiche.ascx" tagname="MenuCantieriStatistiche" tagprefix="uc4" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">

    <telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function openRadWindowModificaAttivita(sender, eventArgs) {
                var apt = eventArgs.get_appointment();
                var oWindow = radopen("../Cantieri/Attivita.aspx?modalita=modifica&idAttivita=" + apt.get_id() , null);
                oWindow.set_title("Attività");
                oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                oWindow.setSize(600, 450);
                oWindow.center();
            }
        </script>
    </telerik:RadScriptBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
        VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />

    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Elenco Attività" />
    <br />
    Ispettore
    <telerik:RadComboBox
        ID="RadComboBoxIspettore"
        runat="server"
        Width="250px" AppendDataBoundItems="True" AutoPostBack="True" 
        onselectedindexchanged="RadComboBoxIspettore_SelectedIndexChanged">
    </telerik:RadComboBox>
    <br />
    <telerik:RadScheduler ID="RadSchedulerAttivita" runat="server" 
        AppointmentStyleMode="Default" Culture="it-IT" FirstDayOfWeek="Monday" 
        LastDayOfWeek="Friday" SelectedView="WeekView" Width="100%"
        DataSubjectField="CognomeIspettore" DataDescriptionField="DescrizioneAttivitaPerElencoCompleto"
        DataStartField="Data" DataEndField="DataFineAttivita" DataKeyField="Id" Height="600px" 
        MinutesPerRow="60" OverflowBehavior="Expand" 
        onappointmentdatabound="RadSchedulerAttivita_AppointmentDataBound" 
        Localization-ConfirmDeleteText="Sei sicuro di voler eliminare quest'attività?" 
        Localization-ConfirmDeleteTitle="Conferma cancellazione" 
        Localization-ConfirmCancel="Annulla" Localization-Cancel="Elimina" 
        DayEndTime="19:00:00" DayStartTime="07:00:00" ShowFooter="False" 
        WorkDayEndTime="19:00:00" WorkDayStartTime="07:00:00"
        OnClientAppointmentClick="openRadWindowModificaAttivita"
        OnClientAppointmentDoubleClick="openRadWindowModificaAttivita" 
        AllowDelete="False" AllowEdit="False" AllowInsert="False" 
        HoursPanelTimeFormat="HH:mmtt" 
        onnavigationcomplete="RadSchedulerAttivita_NavigationComplete">
        <AdvancedForm Modal="true" />
        <Localization Cancel="Elimina" ConfirmDeleteTitle="Conferma cancellazione" 
            ConfirmDeleteText="Sei sicuro di voler eliminare quest&#39;attivit&#224;?" 
            ConfirmCancel="Annulla" ShowMore="altro..."></Localization>
        <TimelineView UserSelectable="false" />
        <WeekView DayEndTime="19:00:00" DayStartTime="07:00:00" />
        <AppointmentTemplate>
            <asp:Panel
                ID="PanelAttivita"
                runat="server">
                <div class="rsAptSubject">
                    <%# Eval("Subject")%>
                </div>
                <div>
                    <%# Eval("Description")%>
                </div>
            </asp:Panel>
        </AppointmentTemplate>
    </telerik:RadScheduler>
    <br />
    <uc3:LegendaColoriAttivita ID="LegendaColoriAttivita1" runat="server" />
</asp:Content>


