﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SchedaCantiere.aspx.cs" Inherits="Cantieri_SchedaCantiere" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuCantieriStatistiche.ascx" tagname="MenuCantieriStatistiche" tagprefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc2:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc3:titolosottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Scheda Cantiere"
        titolo="Cantieri" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerSchedaCantiere" runat="server" ProcessingMode="Remote"
        width="100%">
    </rsweb:ReportViewer>
    </td></tr></table>
    <br />
</asp:Content>


