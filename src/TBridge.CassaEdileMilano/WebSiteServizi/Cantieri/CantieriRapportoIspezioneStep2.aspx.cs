using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;

public partial class CantieriRapportoIspezioneStep2 : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly TSBusiness tsbiz = new TSBusiness();
    private RapportoIspezione ispezione;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        List<FunzionalitaPredefinite> funzionalita
            = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        if (!Page.IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idAttivita"]) || !String.IsNullOrEmpty(Request.QueryString["idIspezione"]))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["idAttivita"]))
                {
                    int idAttivita = Int32.Parse(Request.QueryString["idAttivita"]);
                    ispezione = biz.GetIspezione(idAttivita);
                }
                if (!String.IsNullOrEmpty(Request.QueryString["idIspezione"]))
                {
                    int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
                    ispezione = biz.GetIspezioneByKey(idIspezione);
                }

                Int32 idCantiere = ispezione.Cantiere.IdCantiere.Value;
                SubappaltiCantiere1.CaricaCantiere(idCantiere);

                CantieriTestataIspezione1.ImpostaTestata(ispezione);
            }
        }

        ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);
        ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonPassoPrecedente);
    }


    

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        SubappaltoCollection subappalti = SubappaltiCantiere1.GetSubappalti();

        if (biz.InsertSubappalti(subappalti))
        {
            String redir = String.Format("~/Cantieri/CantieriRapportoIspezioneStep3.aspx?idAttivita={0}&idCantiere={1}&giorno={2}&idIspettore={3}&idIspezione={4}",
                               Request.QueryString["idAttivita"],
                               Request.QueryString["idCantiere"],
                               Request.QueryString["giorno"],
                               Request.QueryString["idIspettore"],
                               Request.QueryString["idIspezione"]);
            Response.Redirect(redir);
        }
        else
        {
            LabelRisultato.Text = "Errore durante l'inserimento dei subappalti";
        }
    }

    protected void ButtonPassoPrecedente_Click(object sender, EventArgs e)
    {
        SubappaltoCollection subappalti = SubappaltiCantiere1.GetSubappalti();

        if (biz.InsertSubappalti(subappalti))
        {
            String redir = String.Format("~/Cantieri/CantieriRapportoIspezioneStep1.aspx?idAttivita={0}&idCantiere={1}&giorno={2}&idIspettore={3}&idIspezione={4}",
                               Request.QueryString["idAttivita"],
                               Request.QueryString["idCantiere"],
                               Request.QueryString["giorno"],
                               Request.QueryString["idIspettore"],
                               Request.QueryString["idIspezione"]);
            Response.Redirect(redir);
        }
        else
        {
            LabelRisultato.Text = "Errore durante l'inserimento dei subappalti";
        }
    }

    protected void ButtonAnnullaIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cantieri/CantieriDefault.aspx");
    }
}
