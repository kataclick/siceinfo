using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;

public partial class CantieriInserimentoModificaCommittente : Page
{
    private readonly Common _commonBiz = new Common();
    private readonly CantieriBusiness _cantieriBiz = new CantieriBusiness();
    private bool aggiornamento;

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriGestione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonOperazione, null) + ";");
        sb.Append("return true;");
        ButtonOperazione.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonOperazione);

        #endregion

        if (!Page.IsPostBack)
        {
            CaricaProvince();

            //DropDownListProvincia.SelectedItem.Text = "MI";
            //DropDownListProvincia.DataBind();
            // Porcheria
            DropDownListProvincia.SelectedValue = "13";

            int idProvincia;
            if ((DropDownListProvincia.SelectedValue != null) &&
                (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                CaricaComuni(idProvincia);

            //DropDownListComuni.SelectedItem.Text = "MILANO";
            //DropDownListComuni.DataBind();
            // Porcheria
            DropDownListComuni.SelectedValue = "1772";

            Int64 idComune;
            if ((DropDownListComuni.SelectedValue != null) &&
                (Int64.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                CaricaCAP(idComune);

            string modalita = Request.QueryString["modalita"];
            if (modalita == null || modalita != "modifica")
            {
                ModalitaInserimento();
            }
            else
            {
                string idCommittenteS = Request.QueryString["idCommittente"];
                int idCommittente;

                if (Int32.TryParse(idCommittenteS, out idCommittente))
                {
                    ModalitaAggiornamento();
                    CaricaCommittente(idCommittente);
                }
                else
                    ModalitaInserimento();
            }
        }
    }

    private void ModalitaAggiornamento()
    {
        TitoloSottotitolo1.sottoTitolo = "Modifica committente";
        ButtonOperazione.Text = "Aggiorna";
        aggiornamento = true;
    }

    private void ModalitaInserimento()
    {
        TitoloSottotitolo1.sottoTitolo = "Inserimento nuovo committente";
        ButtonOperazione.Text = "Inserisci";
        aggiornamento = false;
    }

    private void CaricaCommittente(int idCommittente)
    {
        CommittenteCollection listaCommittenti =
            _cantieriBiz.GetCommittentiOrdinati(idCommittente, null, null, null, null, null, null, false);
        if (listaCommittenti.Count == 1)
        {
            Committente committente = listaCommittenti[0];

            TextBoxIdCommittente.Text = committente.IdCommittente.ToString();
            TextBoxRagioneSociale.Text = committente.RagioneSociale;
            TextBoxPartitaIva.Text = committente.PartitaIva;
            TextBoxCodiceFiscale.Text = committente.CodiceFiscale;
            TextBoxIndirizzo.Text = committente.Indirizzo;
            DropDownListProvincia.Text = committente.Provincia;
            DropDownListProvincia.Text = committente.Comune;
            DropDownListCAP.Text = committente.Cap;
        }
    }

    protected void ButtonOperazione_Click(object sender, EventArgs e)
    {
        if (ControlloCampiServer())
        {
            Committente committente = CreaCommittente();
            bool res = false;
            ;

            if (Request.QueryString["modalita"] == "modifica")
                aggiornamento = true;

            // Eseguo l'operazione
            if (aggiornamento)
            {
                int idCommittente;
                Int32.TryParse(TextBoxIdCommittente.Text, out idCommittente);
                committente.IdCommittente = idCommittente;

                // Aggiorno il record
                res = _cantieriBiz.UpdateCommittente(committente);
            }
            else
            {
                // Inserisco il record
                _cantieriBiz.InsertCommittente(committente);
                if (committente.IdCommittente > 0)
                {
                    res = true;
                    // Ritorno alla pagina di insertmodificacantieri
                }
            }

            // Gestisco il risultato dell'operazione
            if (res)
            {
                // Tutto ok
                if (aggiornamento)
                    LabelRisultato.Text = "Aggiornamento effettuato correttamente";
                else
                    LabelRisultato.Text = "Inserimento effettuato correttamente";
            }
            else
            {
                LabelRisultato.Text = "Si � verificato un errore durante l'operazione";
            }
        }
    }

    private Committente CreaCommittente()
    {
        // Creazione oggetto Committente
        string provincia;
        string comune;
        string cap;
        string indirizzo = null;
        string ragioneSociale;
        string partitaIva = null;
        string codiceFiscale = null;

        ragioneSociale = TextBoxRagioneSociale.Text;

        if (!string.IsNullOrEmpty(TextBoxIndirizzo.Text))
            indirizzo = TextBoxIndirizzo.Text;
        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text))
            partitaIva = TextBoxPartitaIva.Text;
        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
            codiceFiscale = TextBoxCodiceFiscale.Text;

        provincia = DropDownListProvincia.SelectedItem.Text;
        comune = DropDownListComuni.SelectedItem.Text;
        cap = DropDownListCAP.SelectedValue;

        Committente committente =
            new Committente(null, ragioneSociale, partitaIva, codiceFiscale, indirizzo, comune, provincia, cap);

        return committente;
    }

    /// <summary>
    ///   Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        // CAMPI CONTROLLATI
        // Ragione sociale

        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            res = false;
            errori.Append("Campo ragione sociale vuoto" + Environment.NewLine);
        }

        if (!res)
            LabelRisultato.Text = errori.ToString();
        return res;
    }

    #region Metodi per la gestione delle province, comuni, cap

    private void CaricaProvince()
    {
        DataTable dtProvince = _commonBiz.GetProvince();

        DropDownListProvincia.DataSource = dtProvince;
        DropDownListProvincia.DataTextField = "sigla";
        DropDownListProvincia.DataValueField = "idProvincia";

        DropDownListProvincia.DataBind();
    }

    private void CaricaComuni(int idProvincia)
    {
        DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

        DropDownListComuni.DataSource = dtComuni;
        DropDownListComuni.DataTextField = "denominazione";
        DropDownListComuni.DataValueField = "idComune";

        DropDownListComuni.DataBind();
    }

    private void CaricaCAP(Int64 idComune)
    {
        DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

        DropDownListCAP.DataSource = dtCAP;
        DropDownListCAP.DataTextField = "cap";
        DropDownListCAP.DataValueField = "cap";

        DropDownListCAP.DataBind();
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idProvincia;

        if ((DropDownListProvincia.SelectedValue != null) &&
            (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
            CaricaComuni(idProvincia);

        int idComune;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idComune;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    #endregion
}