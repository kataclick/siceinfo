<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriLocalizzazione.aspx.cs" Inherits="CantieriLocalizzazione" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.464044;
                var longitudine = 9.191567;
                var zoom = 17;

                function LoadMap(lat, lon) {
                    if (lat)
                        latitudine = lat;
                    if (lon)
                        longitudine = lon;

                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }
            </script>
        </telerik:RadCodeBlock>
        <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Localizzazione cantiere"
            titolo="Cantieri" />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Errore durante la localizzazione" Visible="False"></asp:Label><br />
        <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
        </div>
        <br />
    </telerik:RadAjaxPanel>
    <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="200px" />
    <br />
</asp:Content>
