using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using System.Collections.Generic;

public partial class Cantieri_StatisticheGenerali : Page
{
    private readonly BusinessEF bizEF = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriStatisticheGenerali);
        #endregion

        // Fix report viewer bug in IE7
        string userAgent = Request.ServerVariables.Get("HTTP_USER_AGENT");
        if (userAgent.Contains("MSIE 7.0"))
        {
            ReportViewerAttivita.Style.Add("margin-bottom", "70px");
            //ReportViewerAttivita.Attributes.Add("style", "overflow:auto;");
        }

        if (!Page.IsPostBack)
        {
            CaricaTipologieAppalto();
            CaricaTipologiaSegnalazioni();
        }
    }

    private void CaricaTipologiaSegnalazioni()
    {
        //Presenter.CaricaElementiInDropDownConElementoVuoto(
        //    DropDownListTipoSegnalazione,
        //    bizEF.GetSegnalazioneMotivazioni(),
        //    "Descrizione",
        //    "Id");
        
        List<CantiereSegnalazioneMotivazione> mot = bizEF.GetSegnalazioneMotivazioni();

        DropDownListTipoSegnalazione.Items.Clear();
        DropDownListTipoSegnalazione.Items.Add(new ListItem("Tutti", ""));
        DropDownListTipoSegnalazione.Items.Add(new ListItem("Spontanea", "-1"));

        DropDownListTipoSegnalazione.DataSource = mot;
        DropDownListTipoSegnalazione.DataValueField = "Id";
        DropDownListTipoSegnalazione.DataTextField = "Descrizione";
        DropDownListTipoSegnalazione.DataBind();
    }

    private void CaricaTipologieAppalto()
    {
        DropDownListTipoAppalto.Items.Clear();
        DropDownListTipoAppalto.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListTipoAppalto.Items.Add(new ListItem("Pubblico", "Pubblico"));
        DropDownListTipoAppalto.Items.Add(new ListItem("Privato", "Privato"));

        DropDownListTipoAppalto.DataBind();
    }

    private void ImpostaReport()
    {
        ReportViewerAttivita.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        ReportParameter[] listaParam;

        DateTime mese;
        DateTime? finePeriodo = null;
        TipologiaAppalto? tipoAppalto = null;
        String stato = null;
        Int32 tipoSegnalazione = 0;

        string sFinePeriodo = null;
        string sTipoAppalto = null;

        // Riepilogo ispezioni
        if (RadioButtonIspezioni.Checked)
        {
            ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/StatisticheRiepilogoIspezioniMensile";

            mese = DateTime.ParseExact(TextBoxMeseMensile.Text, "MM/yyyy", null);

            if (!String.IsNullOrEmpty(DropDownStatoIspezione.SelectedValue))
            {
                stato = DropDownStatoIspezione.SelectedValue;
            }

            if (!string.IsNullOrEmpty(DropDownListTipoAppalto.SelectedValue))
                tipoAppalto =
                    (TipologiaAppalto) Enum.Parse(typeof(TipologiaAppalto), DropDownListTipoAppalto.SelectedValue);

            if (!String.IsNullOrEmpty(DropDownListTipoSegnalazione.SelectedValue))
            {
                tipoSegnalazione = Int32.Parse(DropDownListTipoSegnalazione.SelectedValue);
            }

            listaParam = new ReportParameter[4];

            // Mese
            listaParam[0] = new ReportParameter("mese", mese.ToShortDateString());

            // Tipo appalto
            if (tipoAppalto.HasValue && tipoAppalto != TipologiaAppalto.NonDefinito)
            {
                int iTipoAppalto = (int) tipoAppalto.Value;
                sTipoAppalto = iTipoAppalto.ToString();
            }
            listaParam[1] = new ReportParameter("tipoAppalto", sTipoAppalto);

            listaParam[2] = new ReportParameter("statoIspezione", stato);

            listaParam[3] = new ReportParameter("idTipoSegnalazione", tipoSegnalazione.ToString());
        }
        // Statistiche generali
        else
        {
            if (RadioButtonStatistici.Checked)
            {
                ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/StatisticheGenerali";
            }
            else
            {
                if (RadioButtonStatisticiPerIspezione.Checked)
                {
                    ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/StatisticheRiepilogoMensilePerIspezione";
                }
                else
                {
                    if (RadioButtonStatisticiAggregatiMese.Checked)
                    {
                        ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/StatisticheRiepilogoAggregatoPerMese";
                    }
                    else
                    {
                        ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/StatisticheRiepilogoIspezioniNonDefinite";
                    }
                }
            }

            if (RadioButtonMensile.Checked)
            {
                mese = DateTime.ParseExact(TextBoxMeseMensile.Text, "MM/yyyy", null);
            }
            else
            {
                if (RadioButtonAnnuale.Checked)
                {
                    finePeriodo = DateTime.ParseExact(TextBoxMeseAnnuale.Text, "MM/yyyy", null);
                    finePeriodo = finePeriodo.Value.AddMonths(1).AddMilliseconds(-1);
                    mese = new DateTime(finePeriodo.Value.Year, 1, 1);
                }
                else
                {
                    finePeriodo = DateTime.ParseExact(TextBoxMesePeriodoAl.Text, "MM/yyyy", null);
                    finePeriodo = finePeriodo.Value.AddMonths(1).AddMilliseconds(-1);

                    mese = DateTime.ParseExact(TextBoxMesePeriodoDal.Text, "MM/yyyy", null);
                }
            }

            if (!string.IsNullOrEmpty(DropDownListTipoAppalto.SelectedValue))
                tipoAppalto =
                    (TipologiaAppalto) Enum.Parse(typeof(TipologiaAppalto), DropDownListTipoAppalto.SelectedValue);

            if (!String.IsNullOrEmpty(DropDownListTipoSegnalazione.SelectedValue))
            {
                tipoSegnalazione = Int32.Parse(DropDownListTipoSegnalazione.SelectedValue);
            }

            listaParam = new ReportParameter[4];

            // Mese
            listaParam[0] = new ReportParameter("mese", mese.ToShortDateString());

            // Tipo appalto
            if (tipoAppalto.HasValue && tipoAppalto != TipologiaAppalto.NonDefinito)
            {
                int iTipoAppalto = (int) tipoAppalto.Value;
                sTipoAppalto = iTipoAppalto.ToString();
            }
            listaParam[1] = new ReportParameter("tipoAppalto", sTipoAppalto);

            // Mese
            if (finePeriodo.HasValue)
                sFinePeriodo = finePeriodo.Value.ToShortDateString();
            listaParam[2] = new ReportParameter("finePeriodo", sFinePeriodo);

            listaParam[3] = new ReportParameter("idTipoSegnalazione", tipoSegnalazione.ToString());
        }

        ReportViewerAttivita.ServerReport.SetParameters(listaParam);
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ImpostaReport();

            // Generazione PDF
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            byte[] bytes = null;

            //PDF
            bytes = ReportViewerAttivita.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", "attachment;filename=Statistica.pdf");
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }

    protected void ButtonVisualizzaExcel_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ImpostaReport();

            // Generazione PDF
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            byte[] bytes = null;

            //PDF
            bytes = ReportViewerAttivita.ServerReport.Render(
                "Excel", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";

            Response.AppendHeader("Content-Disposition", "attachment;filename=Statistica.xls");
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }

    protected void RadioButtonStatistici_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaAnnuale();
    }

    protected void RadioButtonStatisticiPerIspezione_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaAnnuale();
    }

    protected void RadioButtonStatisticiAggregatiMese_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaAnnuale();
    }

    protected void RadioButtonStatisticiIspezioniNonDefinite_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaAnnuale();
    }

    protected void RadioButtonIspezioni_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaAnnuale();
    }

    private void AbilitaAnnuale()
    {
        if (RadioButtonStatistici.Checked || RadioButtonStatisticiPerIspezione.Checked || RadioButtonStatisticiAggregatiMese.Checked || RadioButtonStatisticiIspezioniNonDefinite.Checked)
        {
            RadioButtonAnnuale.Enabled = true;
            RadioButtonPeriodo.Enabled = true;
            DropDownStatoIspezione.SelectedIndex = 0;
            DropDownStatoIspezione.Enabled = false;
        }
        else
        {
            DropDownStatoIspezione.Enabled = true;
            RadioButtonAnnuale.Checked = false;
            RadioButtonAnnuale.Enabled = false;
            RadioButtonMensile.Checked = true;
            RadioButtonMensile.Enabled = true;
            RadioButtonPeriodo.Checked = false;
            RadioButtonPeriodo.Enabled = false;
        }
    }

    protected void RadioButtonMensile_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaMensile(RadioButtonMensile.Checked);
        AbilitaAnnuale(!RadioButtonMensile.Checked);
        AbilitaPeriodo(!RadioButtonMensile.Checked);
    }

    protected void RadioButtonAnnuale_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaMensile(!RadioButtonAnnuale.Checked);
        AbilitaAnnuale(RadioButtonAnnuale.Checked);
        AbilitaPeriodo(!RadioButtonAnnuale.Checked);
    }

    protected void RadioButtonPeriodo_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaMensile(!RadioButtonPeriodo.Checked);
        AbilitaAnnuale(!RadioButtonPeriodo.Checked);
        AbilitaPeriodo(RadioButtonPeriodo.Checked);
    }

    private void AbilitaMensile(bool abilita)
    {
        if (abilita)
        {
            PanelMensile.Enabled = true;
            RequiredFieldValidatorMeseMensile.Enabled = true;
        }
        else
        {
            PanelMensile.Enabled = false;
            RequiredFieldValidatorMeseMensile.Enabled = false;
            TextBoxMeseMensile.Text = null;
        }
    }

    private void AbilitaAnnuale(bool abilita)
    {
        if (abilita)
        {
            PanelAnnuale.Enabled = true;
            RequiredFieldValidatorMeseAnnuale.Enabled = true;
        }
        else
        {
            PanelAnnuale.Enabled = false;
            RequiredFieldValidatorMeseAnnuale.Enabled = false;
            TextBoxMeseAnnuale.Text = null;
        }
    }

    private void AbilitaPeriodo(bool abilita)
    {
        if (abilita)
        {
            PanelPeriodo.Enabled = true;
            RequiredFieldValidatorMesePeriodoDal.Enabled = true;
            RequiredFieldValidatorMesePeriodoAl.Enabled = true;
        }
        else
        {
            PanelPeriodo.Enabled = false;
            RequiredFieldValidatorMesePeriodoDal.Enabled = false;
            RequiredFieldValidatorMesePeriodoAl.Enabled = false;
            TextBoxMesePeriodoDal.Text = null;
            TextBoxMesePeriodoAl.Text = null;
        }
    }
}