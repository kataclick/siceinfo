<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriRapportoIspezioneStep3.aspx.cs" Inherits="CantieriRapportoIspezioneStep3" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc8" %>
<%@ Register Src="~/WebControls/CantieriLavoratore.ascx" TagName="CantieriLavoratore"
    TagPrefix="uc7" %>
<%@ Register Src="~/WebControls/CantieriImpresa.ascx" TagName="CantieriImpresa" TagPrefix="uc6" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte"
    TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/CantieriRicercaLavoratoreArchivioUnico.ascx" TagName="CantieriRicercaLavoratoreArchivioUnico"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriTestataIspezione.ascx" TagName="CantieriTestataIspezione"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content6" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Rapporto di ispezione - Lavoratori"
        titolo="Cantieri" />

    <script type="text/javascript"> 

        function onEndRequest(sender, e) 
        { 
            var err = e.get_error();
            e.set_errorHandled(true);
            if (err != null)
            {
                if ((err.name == 'Sys.WebForms.PageRequestManagerTimeoutException'
                     || err.name == 'Sys.WebForms.PageRequestManagerServerErrorException' )) 
                { 
                    window.alert('Attenzione, il server non risponde o la connessione � caduta');
                }
                else
                {
                    window.alert('Attenzione, errore generico');
                }
            }
        } 

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest); 

    </script>

    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco lavoratori"></asp:Label>
                        <br />
                        <asp:GridView ID="GridViewIspezioneLavoratori" runat="server" AutoGenerateColumns="False"
                            OnRowDataBound="GridViewIspezioneLavoratori_RowDataBound" Width="100%" DataKeyNames="IdIspezioneLavoratore"
                            OnRowDeleting="GridViewIspezioneLavoratori_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="Lavoratore/i">
                                    <ItemTemplate>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridViewLavSiceNew" runat="server" AutoGenerateColumns="False"
                                                        DataKeyNames="IdLavoratore" Font-Size="XX-Small" OnSelectedIndexChanging="GridViewLavSiceNew_SelectedIndexChanging"
                                                        ShowHeader="False">
                                                        <Columns>
                                                            <asp:BoundField DataField="IdLavoratore" HeaderText="Id lavoratore">
                                                                <ItemStyle Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Cognome" HeaderText="Cognome">
                                                                <ItemStyle Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Nome" HeaderText="Nome">
                                                                <ItemStyle Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Impresa" HeaderText="Impresa">
                                                                <ItemStyle Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Sostituisci"
                                                                ShowSelectButton="True">
                                                                <ControlStyle Font-Size="XX-Small" />
                                                            </asp:CommandField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DataNascitaLavoratore" HeaderText="Data di nascita" />
                                <asp:TemplateField HeaderText="Impresa">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelImpresaLavoratore" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText=""
                                    ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png" />
                            </Columns>
                            <EmptyDataTemplate>
                                Nessun lavoratore presente
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <br />
                        <asp:Button ID="ButtonaggiungiLavoratore" runat="server" Text="Aggiungi lavoratore singolo"
                            Width="225px" OnClick="ButtonaggiungiLavoratore_Click" CausesValidation="False" />
                        <asp:Button ID="ButtonaggiungiGruppo" runat="server" OnClick="ButtonaggiungiGruppo_Click"
                            Text="Aggiungi gruppo lavoratori" Width="225px" CausesValidation="False" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Panel ID="PanelAggiungiLavoratore" runat="server" Width="100%" Visible="False">
                            <table class="filledtable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelAggiungiLav" runat="server" Font-Bold="True" ForeColor="White"
                                            Text="Inserimento lavoratore"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table class="borderedTable">
                                <tr>
                                    <td>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="LabelModalitaLavInserimento" runat="server" Text="Lavoratore da inserire"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextBoxLavoratore" runat="server" Width="300px" Enabled="False"></asp:TextBox>
                                                    <asp:Button ID="ButtonVisualizzaRicercaLavoratori" runat="server" OnClick="ButtonVisualizzaRicercaLavoratori_Click"
                                                        Text="Seleziona" Width="101px" CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:UpdateProgress ID="UpdateProgressLavoratore" runat="server" AssociatedUpdatePanelID="UpdatePanelLavoratore"
                                                        DisplayAfter="100">
                                                        <ProgressTemplate>
                                                            <center>
                                                                <asp:Image ID="ImageCommittenteProgress" runat="server" ImageUrl="~/images/loading6b.gif" /><asp:Label
                                                                    ID="LabelCommittenteProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                                                                    Text="In elaborazione...."></asp:Label>
                                                            </center>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                    <asp:UpdatePanel ID="UpdatePanelLavoratore" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="PanelRicercaInserisciLavoratore" runat="server" Width="100%" Visible="False">
                                                                <uc3:CantieriRicercaLavoratoreArchivioUnico ID="CantieriRicercaLavoratore1" runat="server"
                                                                    Visible="false" />
                                                                <asp:Panel ID="PanelInserisciLavoratore" runat="server" Width="100%" Visible="False">
                                                                    <table class="filledtable">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuovo lavoratore"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="borderedTable">
                                                                        <tr>
                                                                            <td>
                                                                                <uc7:CantieriLavoratore ID="CantieriLavoratore1" runat="server" />
                                                                                <asp:Button ID="ButtonInserisciNuovoLavoratore" runat="server" Text="Inserisci Lavoratore"
                                                                                    Width="170px" OnClick="ButtonInserisciNuovoLavoratore_Click" /><asp:Button ID="ButtonAnnullaInserimentoLavoratore"
                                                                                        runat="server" Text="Annulla" Width="170px" OnClick="ButtonAnnullaInserimentoLavoratore_Click"
                                                                                        CausesValidation="False" /><br />
                                                                                <asp:Label ID="LabelInserimentoNuovoLavoratoreRes" runat="server" ForeColor="Red"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Impresa a cui appartiene il lavoratore
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TextBoxImpresa" runat="server" Height="41px" TextMode="MultiLine"
                                                        Width="300px" Enabled="False"></asp:TextBox>
                                                    <asp:Button ID="ButtonVisualizzaRicercaImprese" runat="server" Text="Seleziona" Width="101px"
                                                        OnClick="ButtonVisualizzaRicercaImprese_Click" CausesValidation="False" /><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Panel ID="PanelSelezionaImpresaDaDropDown" runat="server" Width="710px" Visible="False">
                                                        <table class="filledtable">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Labelric" runat="server" Font-Bold="True" ForeColor="White" Text="Seleziona impresa"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="borderedTable">
                                                            <tr>
                                                                <td>
                                                                    Seleziona l'impresa dalla lista delle imprese inserite nel passo precedente:<br />
                                                                    <asp:DropDownList ID="DropDownListImpresa" runat="server" Width="300px">
                                                                    </asp:DropDownList>
                                                                    <asp:Button ID="ButtonSelezionaImpresaDaDropDownList" runat="server" OnClick="ButtonSelezionaImpresaDaDropDownList_Click"
                                                                        Text="Seleziona" Width="125px" CausesValidation="False" /><br />
                                                                    Se l'impresa a cui il lavoratore ha dichiarato di appartenere non � nella lista,
                                                                    premere:&nbsp;
                                                                    <asp:Button ID="ButtonAltro" runat="server" Text="Altra impresa" OnClick="ButtonAltro_Click1"
                                                                        Width="102px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdateProgress ID="UpdateProgressImpresa" runat="server" AssociatedUpdatePanelID="UpdatePanelImpresa"
                                                                        DisplayAfter="100">
                                                                        <ProgressTemplate>
                                                                            <center>
                                                                                <asp:Image ID="ImageImpProgress" runat="server" ImageUrl="~/images/loading6b.gif" /><asp:Label
                                                                                    ID="LabelImpProgress" runat="server" Font-Bold="True" Font-Size="Medium" Text="In elaborazione...."></asp:Label>
                                                                            </center>
                                                                        </ProgressTemplate>
                                                                    </asp:UpdateProgress>
                                                                    <asp:UpdatePanel ID="UpdatePanelImpresa" runat="server">
                                                                        <ContentTemplate>
                                                                            <uc5:CantieriRicercaImpresaUnicaFonte ID="CantieriRicercaImpresa1" runat="server"
                                                                                Visible="false" />
                                                                            <br />
                                                                            <asp:Panel ID="PanelRicercaImpresaEstesa" runat="server" Width="100%" Visible="False">
                                                                                <asp:Panel ID="PanelInserisciNuovaImpresa" runat="server" Width="100%"
                                                                                    Visible="False">
                                                                                    <table class="filledtable">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Inserisci impresa"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table class="borderedTable">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <uc6:CantieriImpresa ID="CantieriImpresa1" runat="server" />
                                                                                                <asp:Button ID="ButtonInserisciImpresa" runat="server" Text="Inserisci impresa" OnClick="ButtonInserisciImpresa_Click"
                                                                                                    Width="170px" />
                                                                                                <asp:Button ID="ButtonAnnullaInserimentoImpresa" runat="server" Text="Annulla" OnClick="ButtonAnnullaInserimentoImpresa_Click"
                                                                                                    Width="170px" CausesValidation="False" />
                                                                                                <asp:Label ID="LabelInserimentoNuovaImpresaRes" runat="server" ForeColor="Red"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:Panel>
                                                                            </asp:Panel>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Label ID="LabelResInserimentoLavInLista" runat="server" ForeColor="Red"></asp:Label><br />
                                        <asp:Button ID="ButtonInserisciLavoratore" runat="server" Text="Inserisci lavoratore"
                                            OnClick="ButtonInserisciLavoratore_Click" ValidationGroup="InsLav" />
                                        <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla"
                                            Width="139px" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    Se sono stati completati tutti i campi premere "Salva e continua" per andare al
    passo successivo<br />
    <asp:Button ID="ButtonPassoPrecedente" runat="server" Text="Salva e torna al passo 2"
        OnClick="ButtonPassoPrecedente_Click" Width="170px" CausesValidation="False" />
    <asp:Button ID="ButtonSalva" runat="server" OnClick="ButtonSalva_Click" Text="Salva e vai al passo 4"
        ValidationGroup="Step3" Width="170px" />
    <asp:Button ID="ButtonAnnullaPasso3" runat="server" OnClick="ButtonAnnullaPasso3_Click"
        Text="Annulla" Width="170px" CausesValidation="False" /><br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label><br />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc4:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc8:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <uc2:CantieriTestataIspezione ID="CantieriTestataIspezione1" runat="server" />
</asp:Content>
