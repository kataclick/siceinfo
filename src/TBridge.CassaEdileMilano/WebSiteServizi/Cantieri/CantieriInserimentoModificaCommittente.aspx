<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriInserimentoModificaCommittente.aspx.cs" Inherits="CantieriInserimentoModificaCommittente" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresa.ascx" TagName="CantieriRicercaImpresa"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriRicercaCommittente.ascx" TagName="CantieriRicercaCommittente"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc4:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc5:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento o modifica committente"
        titolo="Cantieri" />
    <br />
    <asp:Panel ID="PanelDatiAggiornamento" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="62px"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIdCommittente" runat="server" Width="353px" Enabled="False"
                        ReadOnly="True"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelDatiComuni" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelRagioneSociale" runat="server" Text="Ragione sociale"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="353px" MaxLength="255"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxRagioneSociale"
                        ErrorMessage="Digitare una ragione sociale"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelPartitaIVA" runat="server" Text="Partita IVA"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxPartitaIva" runat="server" Width="353px" MaxLength="11"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxPartitaIva"
                        ErrorMessage="Partita IVA errata" ValidationExpression="^\d{11}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelCodiceFiscale" runat="server" Text="Codice fiscale"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="353px" MaxLength="16"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                        ErrorMessage="Codice fiscale errato" ValidationExpression="^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelIndirizzo" runat="server" Text="Indirizzo"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="353px" MaxLength="255" Height="41px"
                        TextMode="MultiLine"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndirizzo" runat="server" ErrorMessage="Digitare un indirizzo"
                        ControlToValidate="TextBoxIndirizzo"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListProvincia" runat="server" Width="353px" AutoPostBack="True"
                        OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Comune
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListComuni" runat="server" Width="353px" AutoPostBack="True"
                        OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Cap
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCAP" runat="server" Width="353px">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonOperazione" runat="server" Text="Inserisci / Aggiorna" OnClick="ButtonOperazione_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
