<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StatistichePerIspettore.aspx.cs" Inherits="Cantieri_StatistichePerIspettore" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Statistiche per ispettore"
        titolo="Cantieri" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Ispettore:
            </td>
            <td>
                <asp:DropDownList ID="DropDownListIspettore" runat="server" Width="300px" AppendDataBoundItems="True">
                </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorIspettore" runat="server" ErrorMessage="Selezionare un ispettore"
                    ControlToValidate="DropDownListIspettore" ValidationGroup="visualizza">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Tipologia report:
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonIspezioni" runat="server" GroupName="tipoReport"
                                Text="Riepilogo ispezioni" Checked="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonStatistici" runat="server" GroupName="tipoReport"
                                Text="Dati statistici" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Mese (mm/aaaa):
            </td>
            <td>
                <asp:TextBox ID="TextBoxMese" runat="server" MaxLength="7" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMese" runat="server" ControlToValidate="TextBoxMese"
                    ErrorMessage="Scegliere un mese" ValidationGroup="visualizza">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="CustomValidatorMese" runat="server" ControlToValidate="TextBoxMese"
                    ErrorMessage="Formato data errato" ValidationGroup="visualizza" 
                    onservervalidate="CustomValidatorMese_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonVisualizza" runat="server" Text="Visualizza" OnClick="ButtonVisualizza_Click"
                    ValidationGroup="visualizza" />
            </td>
            <td colspan="2">
                <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="visualizza" CssClass="messaggiErrore" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <rsweb:ReportViewer ID="ReportViewerAttivita" runat="server" ProcessingMode="Remote"
                    Width="100%" Height="490px">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
