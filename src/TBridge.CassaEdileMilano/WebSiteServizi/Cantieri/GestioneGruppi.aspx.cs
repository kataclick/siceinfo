﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;

public partial class Cantieri_GestioneGruppi : System.Web.UI.Page
{
    private readonly BusinessEF bizEF = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        //GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestioneGruppi);
        #endregion

        if (!Page.IsPostBack)
        {
            CaricaGruppiIspezione();
        }
    }

    private void CaricaGruppiIspezione()
    {
        List<CantieriGruppoIspezione> gruppi = bizEF.GetGruppiIspezione();

        Presenter.CaricaElementiInGridView(
            RadGridGruppiIspezione,
            gruppi);
    }

    protected void RadGridGruppiIspezione_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            CantieriGruppoIspezione gruppo = (CantieriGruppoIspezione)e.Item.DataItem;

            RadListView lvIspettori = (RadListView)e.Item.FindControl("RadListViewIspettori");

            Presenter.CaricaElementiInListView(
                lvIspettori,
                gruppo.Ispettori);
        }
    }

    protected void RadListViewIspettori_ItemDataBound(object sender, Telerik.Web.UI.RadListViewItemEventArgs e)
    {
        if (e.Item is RadListViewDataItem)
        {
            Ispettore ispettore = (Ispettore)((Telerik.Web.UI.RadListViewDataItem)(e.Item)).DataItem;
            Label lIspettore = (Label)e.Item.FindControl("LabelIspettore");

            lIspettore.Text = String.Format("{0} {1}", ispettore.Cognome, ispettore.Nome);
        }
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        PanelNuovoGruppoIspezione.Visible = true;
    }

    protected void ButtonInserisci_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CantieriGruppoIspezione gruppo = DatiGruppoIspezione1.CreaGruppoIspezione();
            bizEF.InsertGruppoIspezione(gruppo);
            Reset();
            CaricaGruppiIspezione();
        }
    }

    private void Reset()
    {
        DatiGruppoIspezione1.Reset();
        PanelNuovoGruppoIspezione.Visible = false;
    }

    protected void RadGridGruppiIspezione_ItemCommand(object source, GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "elimina":
                Int32 idGruppo = (Int32)RadGridGruppiIspezione.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantieriGruppoIspezione"];
                bizEF.DeleteGruppoIspezione(idGruppo);
                CaricaGruppiIspezione();
                break;
        }
    }
}