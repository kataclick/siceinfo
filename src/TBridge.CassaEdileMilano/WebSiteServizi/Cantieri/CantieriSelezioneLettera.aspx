<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriSelezioneLettera.aspx.cs" Inherits="Cantieri_CantieriSelezioneLettera" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ispezione"
        titolo="Lettere cantieri" />
    <br />
    <asp:Panel ID="PanelGeneraLettere" runat="server" Enabled="false">
        <table class="standardTable">
            <tr>
                <td>
                    Tipologia appalto
                </td>
                <td>
                    <asp:Label ID="LabelTipoAppalto" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero di protocollo
                </td>
                <td>
                    <asp:TextBox ID="textBoxProtocollo" runat="server" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorProtocollo" runat="server" ForeColor="Red"
                        ControlToValidate="textBoxProtocollo" ErrorMessage="Numero di protocollo mancante">
                        *
                    </asp:RequiredFieldValidator>
                    <asp:CustomValidator
                        ID="CustomValidatorProtocolloUtilizzato"                    
                        runat="server"
                        ForeColor="Red"
                        ControlToValidate="textBoxProtocollo"
                        ErrorMessage="Il numero di protocollo � gi� presente nel sistema. Premere nuovamente il pulsante di generazione per sovrascrivere la lettera." 
                        onservervalidate="CustomValidatorProtocolloUtilizzato_ServerValidate">
                        *
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Data appuntamento (gg/mm/aaaa) - se pertinente
                </td>
                <td>
                    <asp:TextBox ID="textBoxDataAppuntamento" runat="server" />
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataApp" runat="server"
                        ErrorMessage="Formato non valido" ControlToValidate="textBoxDataAppuntamento" ForeColor="Red"
                        ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataApp" runat="server" ForeColor="Red"
                        ControlToValidate="textBoxDataAppuntamento" ErrorMessage="Data appuntamento mancante" ValidationGroup="conApp">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:ValidationSummary
                        ID="ValidationSummaryGeneraLettera"
                        runat="server"
                        CssClass="messaggiErrore" />
                    <asp:ValidationSummary
                        ID="ValidationSummaryGeneraLetteraConApp"
                        runat="server"
                        CssClass="messaggiErrore"
                        ValidationGroup="conApp" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Verifica ispettiva in corso"></asp:Label>
                    (M 7.05.05)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraVerifica" Text="Genera" runat="server" OnClick="buttonLetteraVerifica_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelVerificaInCorso" runat="server" Visible="false">
                        Prot.
                        <asp:Label ID="LabelVerificaInCorsoProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelVerificaInCorsoData" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="Verifica ispettiva in corso al comune"></asp:Label>
                    (M 7.05.05-02)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraVerificaComune" Text="Genera" runat="server" OnClick="buttonLetteraVerificaComune_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelVerificaInCorsoComune" runat="server" Visible="false">
                        Prot.
                        <asp:Label ID="LabelVerificaInCorsoComuneProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelVerificaInCorsoComuneData" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Irregolarit� riscontrate "></asp:Label>
                    (M 7.05.06 /
                    <br />
                    M 7.05.06-01)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraIrregolarita" Text="Genera" runat="server" OnClick="buttonLetteraIrregolarita_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelIrregolaritaRiscontrate" runat="server" Visible="false">
                        &nbsp;<asp:Label ID="LabelIrregolaritaRiscontrate" runat="server"></asp:Label></asp:Panel>
                </td>
            </tr>
            <tr style="visibility:hidden">
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Irregolarit� riscontrate con appuntamento"></asp:Label>
                    (M 7.05.06-02 /
                    <br />
                    M 7.05.06-03)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraIrregolaritaAppuntamento" Text="Genera" runat="server"
                        OnClick="buttonLetteraIrregolaritaAppuntamento_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelIrregolaritaAppuntamento" runat="server" Visible="false">
                        <asp:Label ID="LabelIrregolaritaRiscontrateAppuntamento" runat="server"></asp:Label></asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Esito positivo"></asp:Label>
                    (M 7.05.09)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraPositivo" Text="Genera" runat="server" OnClick="buttonLetteraPositivo_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelEsitoPositivo" runat="server" Visible="false">
                        Prot.
                        <asp:Label ID="LabelEsitoPositivoProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelEsitoPositivoData" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Esito positivo con subappaltatrici"></asp:Label>
                    (M 7.05.09-01)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraPositivo01" Text="Genera" runat="server" OnClick="buttonLetteraPositivo01_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelEsitoPositivoSub" runat="server" Visible="false">
                        Prot.
                        <asp:Label ID="LabelEsitoPositivoSubProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelEsitoPositivoSubData" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Irregolarit� riscontrate segnalate al committente"></asp:Label>
                    (M 7.05.10 /
                    <br />
                    M 7.05.11)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraIrregolaritaCommittente" Text="Genera" runat="server"
                        OnClick="buttonLetteraIrregolaritaCommittente_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelIrregolaritaAlCommittente" runat="server" Visible="false">
                        <asp:Label ID="LabelIrregolaritaAlCommittente" runat="server"></asp:Label>
                        <asp:Label ID="LabelIrregolaritaAlCommittenteProtLab" runat="server" Text="Prot. "></asp:Label>
                        <asp:Label ID="LabelIrregolaritaAlCommittenteProt" runat="server" Font-Bold="True"></asp:Label>
                        <asp:Label ID="LabelIrregolaritaAlCommittenteDataLab" runat="server" Text=" del "></asp:Label>
                        <asp:Label ID="LabelIrregolaritaAlCommittenteData" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Irregolarit� riscontrate segnalate al comune"></asp:Label>
                    (M 7.05.11)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraIrregolaritaComune" Text="Genera" runat="server" OnClick="buttonLetteraIrregolaritaComune_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelIrregolaritaAlComune" runat="server" Visible="false">
                        <asp:Label ID="LabelIrregolaritaAlComune" runat="server"></asp:Label>
                        <%--Prot.
                        <asp:Label ID="LabelIrregolaritaAlComuneProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelIrregolaritaAlComuneData" runat="server" Font-Bold="True"></asp:Label>--%>
                    </asp:Panel>
                </td>
            </tr>
            <tr style="visibility:hidden">
                <td>
                    <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Irregolarit� riscontrate segnalate al comune"></asp:Label>
                    (M 7.05.11-01)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraIrregolaritaComune01" Text="Genera" runat="server" OnClick="buttonLetteraIrregolaritaComune01_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelIrregolaritaAlComune01" runat="server" Visible="false">
                        <asp:Label ID="LabelIrregolaritaAlComune01" runat="server"></asp:Label>
                        <%--Prot.
                        <asp:Label ID="LabelIrregolaritaAlComuneProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelIrregolaritaAlComuneData" runat="server" Font-Bold="True"></asp:Label>--%>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Lettera ai Sindacati"></asp:Label>
                    (M 7.05.01)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraSindacati" Text="Genera" runat="server" OnClick="buttonLetteraSindacati_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelSindacati" runat="server" Visible="false">
                        <asp:Label ID="LabelSindacati" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="Cantiere di Qualit�"></asp:Label>
                    (M 7.05.05-01)
                </td>
                <td>
                    <asp:Button ID="buttonLetteraBollinoBlu" Text="Genera" runat="server" OnClick="buttonLetteraBollinoBlu_Click" />
                </td>
                <td>
                    <asp:Panel ID="PanelBollinoBlu" runat="server" Visible="false">
                        Prot.
                        <asp:Label ID="LabelBollinoBluProt" runat="server" Font-Bold="True"></asp:Label>
                        del
                        <asp:Label ID="LabelBollinoBluData" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" CausesValidation="False" OnClick="ButtonIndietro_Click"
        Text="Indietro" Width="150px" /><br />
    <asp:Label ID="LabelMessage" runat="server"></asp:Label><br />
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelSceltaImpresa" runat="server" Visible="false">
        <asp:Label ID="LabelSceltaImpresa" Text="Selezione impresa" runat="server" Font-Bold="True"></asp:Label>
        <asp:GridView ID="GridViewSubappalti" runat="server" AutoGenerateColumns="False"
            OnRowEditing="GridViewSubappalti_RowEditing" OnRowDataBound="GridViewSubappalti_RowDataBound"
            Width="100%" DataKeyNames="TipoImpresa,IdImpresa">
            <Columns>
                <asp:TemplateField HeaderText="Codice">
                    <ItemTemplate>
                        <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="70px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ragione sociale">
                    <ItemTemplate>
                        <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Seleziona"
                    ShowEditButton="True">
                    <ItemStyle Width="70px" />
                </asp:CommandField>
            </Columns>
            <EmptyDataTemplate>
                Nessuna impresa trovata
            </EmptyDataTemplate>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
