<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriRapportoIspezioneStep2.aspx.cs" Inherits="CantieriRapportoIspezioneStep2" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc6" %>
<%@ Register Src="~/WebControls/CantieriImpresa.ascx" TagName="CantieriImpresa" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriTestataIspezione.ascx" TagName="CantieriTestataIspezione"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte"
    TagPrefix="uc4" %>
<%@ Register src="WebControls/SubappaltiCantiere.ascx" tagname="SubappaltiCantiere" tagprefix="uc7" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc6:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Rapporto di ispezione - Subappalti"
        titolo="Cantieri" />

    <script type="text/javascript"> 

        function onEndRequest(sender, e) 
        { 
            var err = e.get_error();
            e.set_errorHandled(true);
            if (err != null)
            {
                if ((err.name == 'Sys.WebForms.PageRequestManagerTimeoutException'
                     || err.name == 'Sys.WebForms.PageRequestManagerServerErrorException' )) 
                { 
                    window.alert('Attenzione, il server non risponde o la connessione � caduta');
                }
                else
                {
                    window.alert('Attenzione, errore generico');
                }
            }
        } 

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest); 

    </script>

    <br />
    <uc7:SubappaltiCantiere ID="SubappaltiCantiere1" runat="server" />
    <br />

    Se sono stati completati tutti i campi premere "Salva e continua" per andare al
    passo successivo<br />
    <asp:Button ID="ButtonPassoPrecedente" runat="server" OnClick="ButtonPassoPrecedente_Click"
        Text="Salva e torna al passo 1" Width="170px" CausesValidation="False" /><asp:Button
            ID="ButtonSalva" runat="server" OnClick="ButtonSalva_Click" Text="Salva e vai al passo 3"
            ValidationGroup="Step2" Width="170px" /><asp:Button ID="ButtonAnnullaIndietro" runat="server"
                OnClick="ButtonAnnullaIndietro_Click" Text="Annulla" Width="170px" CausesValidation="False" /><br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <uc2:CantieriTestataIspezione ID="CantieriTestataIspezione1" runat="server" />
</asp:Content>
