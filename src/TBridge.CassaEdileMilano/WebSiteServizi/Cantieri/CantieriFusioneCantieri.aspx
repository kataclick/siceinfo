<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriFusioneCantieri.aspx.cs" Inherits="CantieriFusioneCantieri" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/CantieriRicercaCantiere.ascx" TagName="CantieriRicercaCantiere"
    TagPrefix="uc3" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione cantieri"
        sottoTitolo="Accorpamento cantieri" />
    <br />
    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Cantiere Selezionato:"></asp:Label><br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Indirizzo: "></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelIndirizzoCantiere" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelCivico" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Comune:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelComune" runat="server"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Provincia:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="CAP:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelCAP" runat="server"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Importo (�):"></asp:Label><td>
                    <asp:Label ID="LabelImporto" runat="server"></asp:Label><td>
                    </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Impresa:"></asp:Label><td>
                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label><td>
                    </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Cantieri assimilabili selezionati:"></asp:Label><br />
    <asp:GridView ID="GridViewCantieriAssimilabili" runat="server" Width="100%" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="Provincia" HeaderText="Provincia" />
            <asp:BoundField DataField="Comune" HeaderText="Comune" />
            <asp:BoundField DataField="Indirizzo" HeaderText="Indirizzo" />
            <asp:BoundField DataField="Civico" HeaderText="Civico" />
            <asp:BoundField DataField="Cap" HeaderText="Cap" />
            <asp:BoundField DataField="NomeImpresa" HeaderText="Impresa" />
        </Columns>
        <EmptyDataTemplate>
            Nessun cantiere selezionato
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonCantieriMerge" runat="server" Text="Accorpa cantieri" Width="180px"
        OnClick="ButtonCantieriMerge_Click" />
    <asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla" Width="180px" OnClick="ButtonAnnulla_Click" />&nbsp;<br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label><br />
    <br />
    <asp:Label ID="LabelRicercaCantiere" runat="server" Font-Bold="True" Text="Ricerca Cantiere:"></asp:Label><uc3:CantieriRicercaCantiere
        ID="CantieriRicercaCantiere1" runat="server" />
    <br />
</asp:Content>
