<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriRicercaIspezioni.aspx.cs" Inherits="CantieriRicercaIspezioni" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/CantieriRicercaIspezioni.ascx" TagName="CantieriRicercaIspezioni"
    TagPrefix="uc2" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ricerca Ispezioni"
        titolo="Cantieri" />

    <br />
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DisplayAfter="100">
        <ProgressTemplate>
            <center>
                <asp:Image ID="ImageCommittenteProgress" runat="server" ImageUrl="~/images/loading6b.gif" /><asp:Label
                    ID="LabelCommittenteProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                    Text="In elaborazione...."></asp:Label>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc2:CantieriRicercaIspezioni ID="CantieriRicercaIspezioni1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
