﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Enums;
using Cemi.NotifichePreliminari.Business;
using Cemi.NotifichePreliminari.Types.Entities;

public partial class Cantieri_GeneraCantiere : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();
    private readonly CantieriBusiness bizCant = new CantieriBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGenerazioneDaNotifiche);
        #endregion

        if (!Page.IsPostBack)
        {
            ButtonSalva.Enabled = false;

            if (Request.QueryString["protocolloRegionale"] != null
                && Request.QueryString["segnalazione"] != null)
            {
                String protocolloRegionale = Request.QueryString["protocolloRegionale"];
                Boolean segnalazione = Boolean.Parse(Request.QueryString["segnalazione"]);

                NotificaPreliminare notifica = biz.GetNotficaByNumero(protocolloRegionale);
                if (notifica != null)
                {
                    ViewState["protocolloRegionale"] = protocolloRegionale;
                    ViewState["segnalazione"] = segnalazione;
                    SelezioneIndirizziNotificaRegionale1.CaricaNotifica(notifica);
                    SegnalazioneDati1.CaricaDatiIniziali();
                    ButtonSalva.Enabled = true;

                    if (!segnalazione)
                    {
                        divSegnalazione.Visible = false;
                    }
                }
            }
        }
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["protocolloRegionale"] != null)
            {
                Int32 idIndirizzo = (SelezioneIndirizziNotificaRegionale1.GetIndirizzoSelezionato()).Value;
                String protocolloRegionale = (String) ViewState["protocolloRegionale"];
                Boolean segnalazioneF = (Boolean)ViewState["segnalazione"];

                NotificaPreliminare notifica = biz.GetNotficaByNumero(protocolloRegionale);
                Indirizzo indirizzo = notifica.Indirizzi.GetById(idIndirizzo);
                Segnalazione segnalazione = null;

                if (segnalazioneF)
                {
                    segnalazione = SegnalazioneDati1.CreaSegnalazione();
                }

                CreaCantiere(notifica, indirizzo, segnalazione);
                SegnalazioneDati1.Abilita(false);
                SelezioneIndirizziNotificaRegionale1.Abilita(false);
                ButtonSalva.Enabled = false;
            }
        }
    }

    private void CreaCantiere(NotificaPreliminare notifica, Indirizzo indirizzo, Segnalazione segnalazione)
    {
        TBridge.Cemi.Cantieri.Type.Entities.Cantiere cantiere = new TBridge.Cemi.Cantieri.Type.Entities.Cantiere();

        cantiere.ProtocolloRegionaleNotifica = notifica.NumeroNotifica;
        cantiere.DescrizioneLavori = String.Format("{0} ({1})", notifica.Categoria, notifica.Tipologia);

        if (notifica.TipoOperaDescrizione.ToUpper().Trim() == "PUBBLICA")
        {
            cantiere.TipologiaAppalto = TipologiaAppalto.Pubblico;
        }
        else
        {
            cantiere.TipologiaAppalto = TipologiaAppalto.NonDefinito;
        }

        #region Indirizzo
        cantiere.Indirizzo = Presenter.NormalizzaCampoTesto(indirizzo.NomeVia);
        cantiere.Civico = Presenter.NormalizzaCampoTesto(indirizzo.Civico);
        cantiere.Comune = Presenter.NormalizzaCampoTesto(indirizzo.Comune);
        cantiere.Provincia = Presenter.NormalizzaCampoTesto(indirizzo.Provincia);
        cantiere.Cap = indirizzo.Cap;
        if (indirizzo.Latitudine.HasValue)
        {
            cantiere.Latitudine = Convert.ToDouble(indirizzo.Latitudine.Value);
        }
        if (indirizzo.Longitudine.HasValue)
        {
            cantiere.Longitudine = Convert.ToDouble(indirizzo.Longitudine.Value);
        }
        #endregion

        #region Date e numeri
        if (indirizzo.DataInizioLavori.HasValue)
        {
            cantiere.DataInizioLavori = indirizzo.DataInizioLavori;
        }
        
        if (cantiere.DataInizioLavori.HasValue)
        {
            Int32 durataGiorni = 0;
            if (!String.IsNullOrEmpty(indirizzo.DescrizioneDurata) && indirizzo.NumeroDurata.HasValue)
            {
                switch (indirizzo.DescrizioneDurata.ToUpper())
                {
                    case "GIORNI":
                        durataGiorni = indirizzo.NumeroDurata.Value;
                        break;
                    case "MESI":
                        durataGiorni = indirizzo.NumeroDurata.Value * 30;
                        break;
                }
            }
            
            if (durataGiorni > 0)
            {
                cantiere.DataFineLavori = cantiere.DataInizioLavori.Value.AddDays(durataGiorni);
            }
        }

        cantiere.Importo = Convert.ToDouble(notifica.AmmontareComplessivo);
        #endregion

        #region Direzione lavori
        //if (notifica.DirettoreLavori != null)
        //{
        //    cantiere.DirezioneLavori = String.Format("{0} {1}", notifica.DirettoreLavori.PersonaCognome, notifica.DirettoreLavori.PersonaNome);
        //}
        #endregion

        #region Committente
        if (notifica.Committenti != null && notifica.Committenti.Count > 0)
        {
            cantiere.Committente = new Committente();
            cantiere.Committente.FonteNotifica = false;
            cantiere.Committente.RagioneSociale = Presenter.NormalizzaCampoTesto(String.Format("{0} {1}", notifica.Committenti[0].Cognome, notifica.Committenti[0].Nome));
            cantiere.Committente.PersonaRiferimento = cantiere.Committente.RagioneSociale;
            cantiere.Committente.CodiceFiscale = Presenter.NormalizzaCampoTesto(notifica.Committenti[0].CodiceFiscale);
            if (notifica.Committenti[0].Indirizzo != null)
            {
                cantiere.Committente.Indirizzo = Presenter.NormalizzaCampoTesto(notifica.Committenti[0].Indirizzo.IndirizzoBase);
                cantiere.Committente.Comune = Presenter.NormalizzaCampoTesto(notifica.Committenti[0].Indirizzo.Comune);
                cantiere.Committente.Provincia = Presenter.NormalizzaCampoTesto(notifica.Committenti[0].Indirizzo.Provincia);
                cantiere.Committente.Cap = Presenter.NormalizzaCampoTesto(notifica.Committenti[0].Indirizzo.Cap);
            }

            bizCant.InsertCommittente(cantiere.Committente);
        }
        #endregion

        if (bizCant.InsertCantiere(cantiere))
        {
            TBridge.Cemi.Cantieri.Type.Collections.SubappaltoCollection subappalti = new TBridge.Cemi.Cantieri.Type.Collections.SubappaltoCollection();

            // Subappalti
            foreach (ImpresaNotifichePreliminari imp in notifica.Imprese)
            {
                TBridge.Cemi.Cantieri.Type.Entities.Subappalto subCant = new TBridge.Cemi.Cantieri.Type.Entities.Subappalto();
                subCant.IdCantiere = cantiere.IdCantiere.Value;
                subappalti.Add(subCant);

                #region Subappaltata
                //if (sub.ImpresaSelezionata != null)
                //{
                    // Provo a cercarla nell'anagrafica SiceNew
                    Int32? idImpresa = bizCant.GetIdImpresaDaCodiceFiscale(imp.CodiceFiscale);
                    if (idImpresa.HasValue)
                    {
                        subCant.SubAppaltata = new Impresa();
                        subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.SiceNew;
                        subCant.SubAppaltata.IdImpresa = idImpresa.Value;
                    }
                    else
                    {
                        // Guardo se l'impresa era già stata trovata o inserita
                        Impresa impresaGiaSel = subappalti.FindImpresa(imp.CodiceFiscale);

                        if (impresaGiaSel == null)
                        {
                            // Provo a cercarla nell'anagrafica cantieri
                            ImpresaCollection imprese =
                                bizCant.GetimpreseOrdinate(TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri,
                                    null, null, null, null,
                                    imp.CodiceFiscale,
                                    null, null, false);

                            Boolean impresaTrovata = false;
                            // Guardo se tra le imprese trovate ce n'è una inserita dagli ispettori,
                            // se la trovo la seleziono
                            foreach (Impresa impr in imprese)
                            {
                                if (!impr.FonteNotifica)
                                {
                                    impresaTrovata = true;
                                    subCant.SubAppaltata = impr;
                                    break;
                                }
                            }
                            // Se non ho trovato l'impresa tra quelle inserite dagli ispettori,
                            // e esiste dalle vecchie notifiche la seleziono
                            if (!impresaTrovata && imprese.Count > 0)
                            {
                                impresaTrovata = true;
                                subCant.SubAppaltata = imprese[0];
                            }

                            // A questo punto la creo
                            if (!impresaTrovata)
                            {
                                subCant.SubAppaltata = new Impresa();
                                subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri;
                                subCant.SubAppaltata.FonteNotifica = true;

                                //subCant.SubAppaltata.Cap = sub.ImpresaSelezionata.Cap;
                                subCant.SubAppaltata.CodiceFiscale = imp.CodiceFiscale;
                                //subCant.SubAppaltata.Fax = sub.ImpresaSelezionata.Fax;
                                subCant.SubAppaltata.LavoratoreAutonomo = false;
                                if (imp.CodiceFiscale.Length == 11)
                                {
                                    subCant.SubAppaltata.PartitaIva = imp.CodiceFiscale;
                                }
                                else
                                {
                                    subCant.SubAppaltata.PartitaIva = imp.PartitaIva;
                                }
                                subCant.SubAppaltata.RagioneSociale = imp.RagioneSociale;
                                //subCant.SubAppaltata.Telefono = sub.ImpresaSelezionata.Telefono;

                                if (imp.Indirizzo != null)
                                {
                                    subCant.SubAppaltata.Indirizzo = imp.Indirizzo.IndirizzoBase;
                                    subCant.SubAppaltata.Comune = imp.Indirizzo.Comune;
                                    subCant.SubAppaltata.Provincia = imp.Indirizzo.Provincia;
                                    subCant.SubAppaltata.Cap = imp.Indirizzo.Cap;
                                }

                                bizCant.InsertImpresa(subCant.SubAppaltata);
                            }
                        }
                        else
                        {
                            subCant.SubAppaltata = impresaGiaSel;
                        }
                    }
                //}
                #endregion
            }

            //foreach(TBridge.Cemi.Cpt.Type.Entities.SubappaltoNotificheTelematiche sub in notifica.ImpreseEsecutrici)
            //{
            //    TBridge.Cemi.Cantieri.Type.Entities.Subappalto subCant = new TBridge.Cemi.Cantieri.Type.Entities.Subappalto();
            //    subCant.IdCantiere = cantiere.IdCantiere.Value;
            //    subappalti.Add(subCant);

            //    #region Subappaltata
            //    if (sub.ImpresaSelezionata != null)
            //    {
            //        // Provo a cercarla nell'anagrafica SiceNew
            //        Int32? idImpresa = biz.GetIdImpresaDaCodiceFiscale(sub.ImpresaSelezionata.CodiceFiscale);
            //        if (idImpresa.HasValue)
            //        {
            //            subCant.SubAppaltata = new Impresa();
            //            subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.SiceNew;
            //            subCant.SubAppaltata.IdImpresa = idImpresa.Value;
            //        }
            //        else
            //        {
            //            // Guardo se l'impresa era già stata trovata o inserita
            //            Impresa impresaGiaSel = subappalti.FindImpresa(sub.ImpresaSelezionata.CodiceFiscale);

            //            if (impresaGiaSel == null)
            //            {
            //                // Provo a cercarla nell'anagrafica cantieri
            //                ImpresaCollection imprese =
            //                    bizCant.GetimpreseOrdinate(TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri,
            //                        null, null, null, null,
            //                        sub.ImpresaSelezionata.CodiceFiscale,
            //                        null, null, false);

            //                Boolean impresaTrovata = false;
            //                // Guardo se tra le imprese trovate ce n'è una inserita dagli ispettori,
            //                // se la trovo la seleziono
            //                foreach (Impresa imp in imprese)
            //                {
            //                    if (!imp.FonteNotifica)
            //                    {
            //                        impresaTrovata = true;
            //                        subCant.SubAppaltata = imp;
            //                        break;
            //                    }
            //                }
            //                // Se non ho trovato l'impresa tra quelle inserite dagli ispettori,
            //                // e esiste dalle vecchie notifiche la seleziono
            //                if (!impresaTrovata && imprese.Count > 0)
            //                {
            //                    impresaTrovata = true;
            //                    subCant.SubAppaltata = imprese[0];
            //                }

            //                // A questo punto la creo
            //                if (!impresaTrovata)
            //                {
            //                    subCant.SubAppaltata = new Impresa();
            //                    subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri;
            //                    subCant.SubAppaltata.FonteNotifica = true;

            //                    subCant.SubAppaltata.Cap = sub.ImpresaSelezionata.Cap;
            //                    subCant.SubAppaltata.CodiceFiscale = sub.ImpresaSelezionata.CodiceFiscale;
            //                    subCant.SubAppaltata.Comune = sub.ImpresaSelezionata.Comune;
            //                    subCant.SubAppaltata.Fax = sub.ImpresaSelezionata.Fax;
            //                    subCant.SubAppaltata.Indirizzo = sub.ImpresaSelezionata.Indirizzo;
            //                    subCant.SubAppaltata.LavoratoreAutonomo = false;
            //                    subCant.SubAppaltata.PartitaIva = sub.ImpresaSelezionata.PartitaIva;
            //                    subCant.SubAppaltata.Provincia = sub.ImpresaSelezionata.Provincia;
            //                    subCant.SubAppaltata.RagioneSociale = sub.ImpresaSelezionata.RagioneSociale;
            //                    subCant.SubAppaltata.Telefono = sub.ImpresaSelezionata.Telefono;

            //                    bizCant.InsertImpresa(subCant.SubAppaltata);
            //                }
            //            }
            //            else
            //            {
            //                subCant.SubAppaltata = impresaGiaSel;
            //            }
            //        }
            //    }
            //    #endregion

            //    #region Subappaltatrice
            //    if (sub.AppaltataDa != null)
            //    {
            //        // Provo a cercarla nell'anagrafica SiceNew
            //        Int32? idImpresa = biz.GetIdImpresaDaCodiceFiscale(sub.AppaltataDa.CodiceFiscale);
            //        if (idImpresa.HasValue)
            //        {
            //            subCant.SubAppaltatrice = new Impresa();
            //            subCant.SubAppaltatrice.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.SiceNew;
            //            subCant.SubAppaltatrice.IdImpresa = idImpresa.Value;
            //        }
            //        else
            //        {
            //            // Guardo se l'impresa era già stata trovata o inserita
            //            Impresa impresaGiaSel = subappalti.FindImpresa(sub.AppaltataDa.CodiceFiscale);

            //            if (impresaGiaSel == null)
            //            {
            //                // Provo a cercarla nell'anagrafica cantieri
            //                ImpresaCollection imprese =
            //                    bizCant.GetimpreseOrdinate(TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri,
            //                        null, null, null, null,
            //                        sub.AppaltataDa.CodiceFiscale,
            //                        String.Empty, String.Empty, false);

            //                Boolean impresaTrovata = false;
            //                // Guardo se tra le imprese trovate ce n'è una inserita dagli ispettori,
            //                // se la trovo la seleziono
            //                foreach (Impresa imp in imprese)
            //                {
            //                    if (!imp.FonteNotifica)
            //                    {
            //                        impresaTrovata = true;
            //                        subCant.SubAppaltatrice = imp;
            //                        break;
            //                    }
            //                }
            //                // Se non ho trovato l'impresa tra quelle inserite dagli ispettori,
            //                // e esiste dalle vecchie notifiche la seleziono
            //                if (!impresaTrovata && imprese.Count > 0)
            //                {
            //                    impresaTrovata = true;
            //                    subCant.SubAppaltatrice = imprese[0];
            //                }

            //                // A questo punto la creo
            //                if (!impresaTrovata)
            //                {
            //                    subCant.SubAppaltatrice = new Impresa();
            //                    subCant.SubAppaltatrice.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri;
            //                    subCant.SubAppaltatrice.FonteNotifica = true;

            //                    subCant.SubAppaltatrice.Cap = sub.ImpresaSelezionata.Cap;
            //                    subCant.SubAppaltatrice.CodiceFiscale = sub.ImpresaSelezionata.CodiceFiscale;
            //                    subCant.SubAppaltatrice.Comune = sub.ImpresaSelezionata.Comune;
            //                    subCant.SubAppaltatrice.Fax = sub.ImpresaSelezionata.Fax;
            //                    subCant.SubAppaltatrice.Indirizzo = sub.ImpresaSelezionata.Indirizzo;
            //                    subCant.SubAppaltatrice.LavoratoreAutonomo = false;
            //                    subCant.SubAppaltatrice.PartitaIva = sub.ImpresaSelezionata.PartitaIva;
            //                    subCant.SubAppaltatrice.Provincia = sub.ImpresaSelezionata.Provincia;
            //                    subCant.SubAppaltatrice.RagioneSociale = sub.ImpresaSelezionata.RagioneSociale;
            //                    subCant.SubAppaltatrice.Telefono = sub.ImpresaSelezionata.Telefono;

            //                    bizCant.InsertImpresa(subCant.SubAppaltatrice);
            //                }
            //            }
            //            else
            //            {
            //                subCant.SubAppaltatrice = impresaGiaSel;
            //            }
            //        }
            //    }
            //    #endregion
            //}

            bizCant.InsertSubappalti(subappalti);

            // Segnalazione
            if (segnalazione != null)
            {
                segnalazione.IdCantiere = cantiere.IdCantiere.Value;
                bizCant.InsertSegnalazione(segnalazione);
            }
        }
    }

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!SelezioneIndirizziNotificaRegionale1.GetIndirizzoSelezionato().HasValue)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorSegnalazione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Boolean segnalazione = (Boolean)ViewState["segnalazione"];
        args.IsValid = true;

        if (segnalazione)
        {
            Page.Validate("segnalaCantiere");

            if (!Page.IsValid)
            {
                args.IsValid = false;
            }
        }
    }
}