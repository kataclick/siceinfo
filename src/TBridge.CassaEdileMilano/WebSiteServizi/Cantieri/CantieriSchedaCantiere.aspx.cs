using System;
using System.Drawing;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cantieri.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriSchedaCantiere : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione, "CantieriSchedaCantiere.aspx");

        if (Request.QueryString["idCantiere"] != null)
        {
            int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);

            Cantiere cantiere = biz.GetCantiere(idCantiere);
            IspezioniFilter filtro = new IspezioniFilter();
            filtro.IdCantiere = cantiere.IdCantiere;
            RapportoIspezioneCollection ispezioni = biz.GetIspezioni(filtro);

            LabelIndirizzo.Text = cantiere.Indirizzo;
            LabelCivico.Text = cantiere.Cap;
            LabelComune.Text = cantiere.Comune;
            LabelProvincia.Text = cantiere.Provincia;
            LabelCap.Text = cantiere.Cap;
            LabelPermessoCostruire.Text = cantiere.PermessoCostruire;
            CheckBoxAttivo.Checked = cantiere.Attivo;
            LabelImporto.Text = String.Format("{0:n}", cantiere.Importo);
            if (cantiere.DataInizioLavori.HasValue)
                LabelDataInizioLavori.Text = cantiere.DataInizioLavori.Value.ToShortDateString();
            if (cantiere.DataFineLavori.HasValue)
                LabelDataFineLavori.Text = cantiere.DataFineLavori.Value.ToShortDateString();
            LabelTipologiaAppalto.Text = cantiere.TipologiaAppalto.ToString();
            LabelLatitudine.Text = cantiere.Latitudine.ToString();
            LabelLongitudine.Text = cantiere.Longitudine.ToString();
            LabelDirezioneLavori.Text = cantiere.DirezioneLavori;
            LabelResponsabileProcedimento.Text = cantiere.ResponsabileProcedimento;
            LabelResponsabileCantiere.Text = cantiere.ResponsabileCantiere;
            LabelDescrizioneLavori.Text = cantiere.DescrizioneLavori;

            // Committente
            if (cantiere.Committente != null)
            {
                LabelCommittente.Text = cantiere.Committente.RagioneSociale;
            }
            else
            {
                if (cantiere.CommittenteTrovato != null)
                {
                    LabelCommittente.Text = cantiere.CommittenteTrovato;
                    LabelCommittente.ForeColor = Color.Red;
                }
            }

            // Impresa appaltatrice
            if (cantiere.ImpresaAppaltatrice != null)
            {
                LabelImpresa.Text = cantiere.ImpresaAppaltatrice.RagioneSociale;

                if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                    LabelImpresa.ForeColor = Color.Gray;
            }
            else
            {
                if (cantiere.ImpresaAppaltatriceTrovata != null)
                {
                    LabelImpresa.Text = cantiere.ImpresaAppaltatriceTrovata;
                    LabelImpresa.ForeColor = Color.Red;
                }
            }

            // Ispezioni
            GridViewIspezioni.DataSource = ispezioni;
            GridViewIspezioni.DataBind();
        }
    }

    protected void LinkButtonLegenda_Click(object sender, EventArgs e)
    {
        if (PanelLegenda.Visible)
        {
            LinkButtonLegenda.Text = "Mostra legenda";
        }
        else
        {
            LinkButtonLegenda.Text = "Nascondi legenda";
        }

        PanelLegenda.Visible = !PanelLegenda.Visible;
    }
}