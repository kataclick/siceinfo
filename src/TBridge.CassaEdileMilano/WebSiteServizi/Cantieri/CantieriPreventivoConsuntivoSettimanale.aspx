<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriPreventivoConsuntivoSettimanale.aspx.cs" Inherits="CantieriConsuntivoSettimanale" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Preventivo / consuntivo settimanale"
        titolo="Cantieri" />
    &nbsp;&nbsp;
    <table class="standardTable">
        <tr>
            <td class="cantieriSelezionePeriodi">
                Ispettore<br />
                <asp:DropDownList ID="DropDownListIspettore" runat="server" Width="260px" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownListIspettore_SelectedIndexChanged">
                </asp:DropDownList>
                <br />
                <br />
                Legenda<br />
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/legendaPrevCons.jpg" BorderStyle="Solid"
                    BorderWidth="1px" />
            </td>
            <td>
                &nbsp;Settimana<asp:Calendar ID="CalendarSettimana" runat="server" OnSelectionChanged="CalendarSettimana_SelectionChanged"
                    Width="150px"></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td style="width: 299px">
                &nbsp; &nbsp; &nbsp;&nbsp;
            </td>
            <td>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
        Text="Visualizza settimana selezionata" Width="240px" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
    <br />
    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" Font-Size="Medium" Text="Consuntivo settimale"></asp:Label><br />
    <br />
    <asp:LinkButton ID="LinkButtonRecupero" runat="server" OnClick="LinkButtonRecupero_Click">Recupero contributi</asp:LinkButton>&nbsp;&nbsp;|&nbsp;
    &nbsp;<asp:LinkButton ID="LinkButtonCantieri" runat="server" OnClick="ButtonCantieri_Click">Cantieri </asp:LinkButton>&nbsp;
    | &nbsp;<asp:LinkButton ID="LinkButtonVerificaIndirizzi" runat="server" OnClick="ButtonVerificaIndirizzi_Click">Verifica indirizzi</asp:LinkButton>&nbsp;
    | &nbsp;<asp:LinkButton ID="LinkButtonImpreseConsulenti" runat="server" OnClick="ButtonImprese_Click">Imprese e consulenti</asp:LinkButton><br />
    <asp:LinkButton ID="LinkButtonComuniSedi" runat="server" OnClick="ButtonComuni_Click">Comuni e sedi INPS-INAIL-ASL-DPL</asp:LinkButton>&nbsp;
    |&nbsp;
    <asp:LinkButton ID="LinkButtonSediCassaEdile" runat="server" OnClick="ButtonSedi_Click">Sedi cassa edile</asp:LinkButton>&nbsp;
    | &nbsp;<asp:LinkButton ID="LinkButtonFallimentiTribunali" runat="server" OnClick="ButtonFallimenti_Click">Fallimenti e tribunali</asp:LinkButton>&nbsp;
    | &nbsp;<asp:LinkButton ID="LinkButtonVarie" runat="server" OnClick="ButtonVarie_Click">Varie</asp:LinkButton>
    &nbsp; &nbsp;&nbsp;<asp:MultiView ID="MultiViewAttivita" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewRecuperoContributi" runat="server">
            <asp:GridView ID="GridViewRecuperoContributi" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewRecuperoContributi_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Recupero contributi">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaRecuperoContributi" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;
                                <hr>
                            </center>
                            <asp:GridView ID="GridViewRecuperoContributi" runat="server" AutoGenerateColumns="False"
                                ShowHeader="False" DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound"
                                OnRowDeleting="GridViewAttivita_RowDeleting" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemStyle Width="15px" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemStyle Width="15px" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="340px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewCantieri" runat="server">
            <asp:GridView ID="GridViewCantieri" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewCantieri_RowDataBound" DataKeyNames="Giorno" BorderColor="Black"
                BorderStyle="None" BorderWidth="1px">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Cantieri">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaCantieri" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewCantieri" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                DataKeyNames="IdAttivita,Cantiere,Ispettore" OnRowDataBound="GridViewAttivita_RowDataBound"
                                OnRowDeleting="GridViewAttivita_RowDeleting" OnRowEditing="GridViewCantieri_RowEditing"
                                BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Width="100%" OnRowCommand="GridViewCantieri_RowCommand">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" CommandName="EliminaFinto"
                                        ImageUrl="~/images/PallinoXrosso.png" Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="407px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" EditText="Rapporto"
                                        ShowEditButton="True" EditImageUrl="~/images/edit.png">
                                        <ItemStyle Width="24px" />
                                    </asp:CommandField>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Rapporto non accessibile">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" CommandName="VisualizzaReport"
                                        ImageUrl="~/images/printer.png" Text="Report">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Report non disponibile">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" CommandName="EliminaRapporto"
                                        Text="Elimina rapporto" ImageUrl="~/images/editdelete.png">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Eliminazione rapporto non consentita">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Selezionare una settimana e premere visualizza
                </EmptyDataTemplate>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewVerificaIndirizzi" runat="server">
            <asp:GridView ID="GridViewVerificaIndirizzi" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewVerificaIndirizzi_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Verifica indirizzi - Certificati">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaVerificaIndirizzi" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewVerificaIndirizzi" runat="server" AutoGenerateColumns="False"
                                ShowHeader="False" DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound"
                                OnRowDeleting="GridViewAttivita_RowDeleting" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="440px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewImprese" runat="server">
            <asp:GridView ID="GridViewImprese" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewImprese_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Imprese e consulenti">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaImpreseConsulenti" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewImpreseConsulenti" runat="server" AutoGenerateColumns="False"
                                ShowHeader="False" DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound"
                                OnRowDeleting="GridViewAttivita_RowDeleting" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="24px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="440px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewComuni" runat="server">
            <asp:GridView ID="GridViewComuni" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewComuni_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Comuni e sedi INPS-INAIL-ASL-DPL">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaComuniSedi" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewComuniSedi" runat="server" AutoGenerateColumns="False"
                                ShowHeader="False" DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound"
                                OnRowDeleting="GridViewAttivita_RowDeleting" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="16px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="440px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewSedi" runat="server">
            <asp:GridView ID="GridViewSedi" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewSedi_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Sedi cassa edile">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaSediCassa" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewSediCassa" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound" OnRowDeleting="GridViewAttivita_RowDeleting"
                                BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="16px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="440px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewFallimenti" runat="server">
            <asp:GridView ID="GridViewFallimenti" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewFallimenti_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Fallimenti e tribunali">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaFallimenti" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewFallimenti" runat="server" AutoGenerateColumns="False"
                                ShowHeader="False" DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound"
                                OnRowDeleting="GridViewAttivita_RowDeleting" BorderColor="Black" BorderStyle="Solid"
                                BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="16px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Font-Size="8pt" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="440px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="ViewVarie" runat="server">
            <asp:GridView ID="GridViewVarie" runat="server" Width="100%" AutoGenerateColumns="False"
                OnRowDataBound="GridViewVarie_RowDataBound" DataKeyNames="Giorno">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="20px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Varie">
                        <ItemTemplate>
                            <center>
                                <asp:Button ID="ButtonAggiungiAttivitaVarie" runat="server" OnClick="ButtonAggiungiAttivita_Click"
                                    Text="Aggiungi attivit�" />&nbsp;<hr>
                            </center>
                            <asp:GridView ID="GridViewVarie" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                DataKeyNames="IdAttivita" OnRowDataBound="GridViewAttivita_RowDataBound" OnRowDeleting="GridViewAttivita_RowDeleting"
                                BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Width="100%">
                                <Columns>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="~/images/PallinoXrosso.png"
                                        Text="Cancellazione attivit&#224; non abilitata">
                                        <ItemStyle Width="16px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Cancella attivit&#224;"
                                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                        <ItemStyle Font-Size="8pt" Width="16px" />
                                    </asp:CommandField>
                                    <asp:CheckBoxField DataField="Programmato" HeaderText="Programmato" ReadOnly="True">
                                        <ItemStyle Width="15px" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField HeaderText="Preventivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxPreventivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consuntivato">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxConsuntivato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                                        <ItemStyle Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Nome cantiere">
                                        <ItemStyle Font-Size="8pt" Width="440px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Impresa">
                                        <ItemStyle Width="100px" />
                                        <ItemTemplate>
                                            <asp:Label ID="LabelImpresa" runat="server" Font-Size="8pt"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView><br />
    <asp:Button ID="ButtonSalvaSituazione" runat="server" Enabled="False" OnClick="ButtonSalvaSituazione_Click"
        Text="Salva modifiche" Width="140px" /><asp:Button ID="ButtonReport" runat="server"
            Enabled="False" OnClick="ButtonReport_Click" Text="Visualizza report" /><br />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
