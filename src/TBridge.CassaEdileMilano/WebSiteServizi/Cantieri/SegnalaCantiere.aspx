﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SegnalaCantiere.aspx.cs" Inherits="Cantieri_SegnalaCantiere" %>

<%@ Register src="WebControls/SegnalazioneDati.ascx" tagname="SegnalazioneDati" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <table class="standardTable">
            <tr>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelIndirizzo"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label
                        ID="LabelCap"
                        runat="server">
                    </asp:Label>
                    <b>
                        <asp:Label
                            ID="LabelComune"
                            runat="server">
                        </asp:Label>
                    </b>
                    <br />
                    <asp:Label
                        ID="LabelProvincia"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <%--<tr>
                <td class="style1">
                    Data:
                </td>
                <td colspan="2">
                    <b>
                        <asp:Label
                            ID="LabelData"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Utente:
                </td>
                <td colspan="2">
                    <b>
                        <asp:Label
                            ID="LabelUtente"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Ricorrente:
                </td>
                <td>
                    <b>
                        <asp:CheckBox
                            ID="CheckBoxRicorrente"
                            runat="server" />
                    </b>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Motivazione:
                </td>
                <td>
                    <telerik:RadComboBox
                        ID="RadComboBoxMotivazione"
                        runat="server"
                        Width="250px"
                        EmptyMessage="Seleziona la motivazione" AppendDataBoundItems="True">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidatorMotivazione"
                        runat="server"
                        ErrorMessage="Selezionare una motivazione"
                        ValidationGroup="segnalaCantiere"
                        ControlToValidate="RadComboBoxMotivazione"
                        CssClass="messaggiErrore">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Priorità:
                </td>
                <td>
                    <telerik:RadComboBox
                        ID="RadComboBoxPriorita"
                        runat="server"
                        Width="250px"
                        EmptyMessage="Seleziona la priorità" AppendDataBoundItems="True">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidatorPriorita"
                        runat="server"
                        ErrorMessage="Selezionare una priorità"
                        ValidationGroup="segnalaCantiere"
                        ControlToValidate="RadComboBoxPriorita"
                        CssClass="messaggiErrore">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Note:
                </td>
                <td>
                    <telerik:RadTextBox
                        ID="RadTextBoxNote"
                        runat="server"
                        Width="250px"
                        MaxLength="100">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <uc1:SegnalazioneDati ID="SegnalazioneDati1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button
                        ID="ButtonSegnala"
                        runat="server"
                        Text="Segnala il cantiere"
                        Width="150px"
                        ValidationGroup="segnalaCantiere" onclick="ButtonSegnala_Click" />
                    &nbsp;<asp:Button
                        ID="ButtonElimina"
                        runat="server"
                        Text="Cancella la segnalazione"
                        Width="150px"
                        CausesValidation="false" 
                        Visible="false"
                        onclick="ButtonElimina_Click" />
                    <br />
                    <asp:ValidationSummary
                        ID="ValidationSummarySegnalaCantiere"
                        runat="server"
                        ValidationGroup="segnalaCantiere"
                        CssClass="messaggiErrore" />
                    <br />
                    <asp:Label
                        ID="LabelMessaggio"
                        runat="server"
                        CssClass="messaggiErrore">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
