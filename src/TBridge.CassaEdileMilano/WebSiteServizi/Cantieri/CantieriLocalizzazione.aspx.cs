using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriLocalizzazione : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);

        ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

        if (Request.QueryString["idCantiere"] != null && Request.QueryString["indirizzoCantiere"] != null)
        {
            int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
            //string indirizzoCantiere = Request.QueryString["indirizzoCantiere"];

            Cantiere cantiere = biz.GetCantiere(idCantiere);

            if (cantiere.Latitudine.HasValue && cantiere.Longitudine.HasValue)
            {
                Label1.Visible = false;
            }
            else
            {
                Label1.Visible = true;
            }

            var descrizione = new StringBuilder();
            descrizione.Append(cantiere.Indirizzo);
            descrizione.Append(" ");
            descrizione.Append(cantiere.Civico);
            descrizione.Append("<br />");
            descrizione.Append(cantiere.Comune);
            descrizione.Append(" ");
            descrizione.Append(cantiere.Provincia);
            descrizione.Append("<br />");
            // Impresa appaltatrice
            if (cantiere.ImpresaAppaltatrice != null)
                descrizione.Append(cantiere.ImpresaAppaltatrice.RagioneSociale);
            else if (cantiere.ImpresaAppaltatriceTrovata != null)
                descrizione.Append(cantiere.ImpresaAppaltatriceTrovata);
            descrizione.Append("<br />");
            descrizione.AppendFormat("<a href=CantieriSchedaCantiere.aspx?idCantiere={0}>Scheda cantiere</a>",
                                     idCantiere);

            string immagineCantiere = "../images/cantiere24.gif";

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(String.Format("LoadMap('{0}','{1}');", cantiere.Latitudine.ToString().Replace(',', '.'),
                                               cantiere.Longitudine.ToString().Replace(',', '.')));

            stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", 0,
                                               cantiere.Latitudine.ToString().Replace(',', '.'),
                                               cantiere.Longitudine.ToString().Replace(',', '.'), descrizione,
                                               immagineCantiere));

            RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
        }
    }
}