<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriInserimentoModificaZona.aspx.cs" Inherits="CantieriInserimentoModificaZona" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Inserimento/Modifica zona" />
    <br />
    Una zona � identificata con una lista di CAP. Quando associata ad un ispettore facilita
    l'assegnazione delle attivit� degli ispettori limitando la ricerca dei cantieti
    a quelli presenti nella zona dell'ispettore.<br />
    <br />
    <asp:Panel ID="PanelDatiAggiornamento" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelIdZona" runat="server" Text="Codice zona:" Width="100px"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIdZona" runat="server" Width="100px" Enabled="False" ReadOnly="True"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelDatiComuni" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelNome" runat="server" Text="Nome:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="100" Width="267px" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server" ErrorMessage="Il campo nome non pu� essere vuoto"
                        ControlToValidate="TextBoxNome" ValidationGroup="generale" Width="175px"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelDescrizione" runat="server" Text="Descrizione:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDescrizione" runat="server" Height="69px" MaxLength="255"
                        TextMode="MultiLine" Width="269px" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table class="filledtable">
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Gestione CAP"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table class="borderedTable">
                        <tr>
                            <td>
                                Seleziona CAP:<br />
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Provincia
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListProvincia" runat="server" Width="156px" AutoPostBack="True"
                                                OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Comune
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListComuni" runat="server" Width="158px" AutoPostBack="True"
                                                OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cap
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListCAP" runat="server" Width="158px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="ButtonAggiungiCap" runat="server" OnClick="ButtonAggiungiCap_Click"
                                                Text="Aggiungi nella lista" ValidationGroup="cap" CausesValidation="False" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Manuale
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxCap" runat="server" MaxLength="5"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCap" runat="server"
                                                ErrorMessage="*" ControlToValidate="TextBoxCap" ValidationExpression="^\d{5}$"
                                                ValidationGroup="cap"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Button ID="ButtonAggiungiCapManuale" runat="server" OnClick="ButtonAggiungiCapManuale_Click"
                                                Text="Aggiungi nella lista" /><br />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Elenco CAP inseriti:"></asp:Label><br />
                                <asp:GridView ID="GridViewCap" runat="server" OnSelectedIndexChanging="GridViewCap_SelectedIndexChanging"
                                    OnRowDeleting="GridViewCap_RowDeleting" Width="250px" ShowHeader="False" BorderColor="Black"
                                    BorderStyle="Solid" BorderWidth="1px" AutoGenerateColumns="False" OnRowDataBound="GridViewCap_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="Cap" HeaderText="CAP" />
                                        <asp:TemplateField HeaderText="CAP">
                                            <ItemTemplate>
                                                <asp:GridView ID="GridViewComuni" runat="server" AutoGenerateColumns="False" ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="Comune" HeaderText="Comune">
                                                            <ItemStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Provincia" HeaderText="Provincia">
                                                            <ItemStyle Width="10px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rimuovi"
                                            ShowDeleteButton="True" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        Nessun CAP nella lista
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <asp:Button ID="ButtonOperazione" runat="server" Text="Inserisci / Aggiorna zona"
            OnClick="ButtonOperazione_Click" ValidationGroup="generale" Width="240px" /><asp:Button
                ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
                Width="240px" /></asp:Panel>
    <br />
</asp:Content>
