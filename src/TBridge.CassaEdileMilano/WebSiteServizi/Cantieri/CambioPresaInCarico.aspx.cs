﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Presenter;

public partial class Cantieri_CambioPresaInCarico : System.Web.UI.Page
{
    private readonly BusinessEF bizEF = new BusinessEF();
    private readonly CantieriBusiness biz = new CantieriBusiness();

    private Int32 IdPresaInCarico
    {
        get 
        {
            return (Int32)ViewState["IdPresaInCarico"];
        }
        set
        {
            ViewState["IdPresaInCarico"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriConsuPrevRUI);
        #endregion

        if (!Page.IsPostBack)
        {
            IdPresaInCarico = Int32.Parse(Request.QueryString["idPresaInCarico"]);
            CaricaPresaInCarico();
        }

        //RadScriptManager1.RegisterAsyncPostBackControl(RadGridIspettori);
    }

    private void CaricaPresaInCarico()
    {
        CantieriCalendarioAttivita attivita = bizEF.GetAttivita(IdPresaInCarico);

        LabelTitolare.Text = attivita.Ispettore.NomeCompleto;
        Presenter.CaricaElementiInGridView(
            RadGridIspettori,
            attivita.IspettoriCorrelati);
    }

    protected void RadGridIspettori_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "PROMUOVI":
                Int32 idIspettore = (Int32)RadGridIspettori.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdIspettore"];
                CambiaPresaInCarico(idIspettore);
                CaricaPresaInCarico();
                break;
        }
    }

    private void CambiaPresaInCarico(Int32 idIspettore)
    {
        biz.CambioPresaInCarico(this.IdPresaInCarico, idIspettore);
    }
}