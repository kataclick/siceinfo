using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriInserimentoModificaImpresa : Page
{
    private readonly Common _commonBiz = new Common();
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private bool aggiornamento;

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriGestione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CantieriPreventivoSettimanale.aspx");


        if (!Page.IsPostBack)
        {
            CaricaProvince();

            //DropDownListProvincia.SelectedItem.Text = "MI";
            //DropDownListProvincia.DataBind();
            // Porcheria
            DropDownListProvincia.SelectedValue = "13";

            int idProvincia;
            if ((DropDownListProvincia.SelectedValue != null) &&
                (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                CaricaComuni(idProvincia);

            //DropDownListComuni.SelectedItem.Text = "MILANO";
            //DropDownListComuni.DataBind();
            // Porcheria
            DropDownListComuni.SelectedValue = "1772";

            Int64 idComune;
            if ((DropDownListComuni.SelectedValue != null) &&
                (Int64.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                CaricaCAP(idComune);

            string modalita = Request.QueryString["modalita"];
            if (modalita == null || modalita != "modifica")
            {
                ModalitaInserimento();
            }
            else
            {
                string idImpresaS = Request.QueryString["idImpresa"];
                int idImpresa;

                if (Int32.TryParse(idImpresaS, out idImpresa))
                {
                    ModalitaAggiornamento();
                    CaricaImpresa(idImpresa);
                }
                else
                    ModalitaInserimento();
            }
        }
    }

    private void ModalitaAggiornamento()
    {
        TitoloSottotitolo1.sottoTitolo = "Modifica impresa";
        ButtonOperazione.Text = "Aggiorna";
        aggiornamento = true;
    }

    private void ModalitaInserimento()
    {
        TitoloSottotitolo1.sottoTitolo = "Inserimento nuova impresa";
        ButtonOperazione.Text = "Inserisci";
        aggiornamento = false;
    }

    private void CaricaImpresa(int idImpresa)
    {
        ImpresaCollection listaImprese =
            biz.GetimpreseOrdinate(TipologiaImpresa.Cantieri, idImpresa, null, null, null, null, null, null, false);
        if (listaImprese.Count == 1)
        {
            Impresa impresa = listaImprese[0];

            TextBoxIdImpresa.Text = impresa.IdImpresa.ToString();
            TextBoxRagioneSociale.Text = impresa.RagioneSociale;
            TextBoxPartitaIva.Text = impresa.PartitaIva;
            TextBoxCodiceFiscale.Text = impresa.CodiceFiscale;
            TextBoxIndirizzo.Text = impresa.Indirizzo;
            DropDownListProvincia.Text = impresa.Provincia;
            DropDownListProvincia.Text = impresa.Comune;
            DropDownListCAP.Text = impresa.Cap;
        }
    }

    protected void ButtonOperazione_Click(object sender, EventArgs e)
    {
        if (ControlloCampiServer())
        {
            Impresa impresa = CreaImpresa();
            bool res = false;
            ;

            if (Request.QueryString["modalita"] == "modifica")
                aggiornamento = true;

            // Eseguo l'operazione
            if (aggiornamento)
            {
                int idImpresa;
                Int32.TryParse(TextBoxIdImpresa.Text, out idImpresa);
                impresa.IdImpresa = idImpresa;

                // Aggiorno il record
                res = biz.UpdateImpresa(impresa);
            }
            else
            {
                // Inserisco il record
                biz.InsertImpresa(impresa);
                if (impresa.IdImpresa > 0)
                {
                    res = true;
                    // Ritorno alla pagina di insertmodificacantieri
                }
            }

            // Gestisco il risultato dell'operazione
            if (res)
            {
                // Tutto ok
                if (aggiornamento)
                    LabelRisultato.Text = "Aggiornamento effettuato correttamente";
                else
                    LabelRisultato.Text = "Inserimento effettuato correttamente";
            }
            else
            {
                LabelRisultato.Text = "Si � verificato un errore durante l'operazione";
            }
        }
    }

    private Impresa CreaImpresa()
    {
        // Creazione oggetto Committente
        string provincia;
        string comune;
        string cap;
        string indirizzo = null;
        string ragioneSociale;
        string partitaIva = null;
        string codiceFiscale = null;

        ragioneSociale = TextBoxRagioneSociale.Text;

        if (!string.IsNullOrEmpty(TextBoxIndirizzo.Text))
            indirizzo = TextBoxIndirizzo.Text;
        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text))
            partitaIva = TextBoxPartitaIva.Text;
        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
            codiceFiscale = TextBoxCodiceFiscale.Text;

        provincia = DropDownListProvincia.SelectedItem.Text;
        comune = DropDownListComuni.SelectedItem.Text;
        cap = DropDownListCAP.SelectedValue;

        Impresa impresa = new Impresa(TipologiaImpresa.Cantieri, null, ragioneSociale, indirizzo,
                                      provincia, comune, cap, partitaIva, codiceFiscale);

        return impresa;
    }

    /// <summary>
    ///   Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        // CAMPI CONTROLLATI
        // Ragione sociale

        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            res = false;
            errori.Append("Campo ragione sociale vuoto" + Environment.NewLine);
        }

        if (!res)
            LabelRisultato.Text = errori.ToString();
        return res;
    }

    #region Metodi per la gestione delle province, comuni, cap

    private void CaricaProvince()
    {
        DataTable dtProvince = _commonBiz.GetProvince();

        DropDownListProvincia.DataSource = dtProvince;
        DropDownListProvincia.DataTextField = "sigla";
        DropDownListProvincia.DataValueField = "idProvincia";

        DropDownListProvincia.DataBind();
    }

    private void CaricaComuni(int idProvincia)
    {
        DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

        DropDownListComuni.DataSource = dtComuni;
        DropDownListComuni.DataTextField = "denominazione";
        DropDownListComuni.DataValueField = "idComune";

        DropDownListComuni.DataBind();
    }

    private void CaricaCAP(Int64 idComune)
    {
        DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

        DropDownListCAP.DataSource = dtCAP;
        DropDownListCAP.DataTextField = "cap";
        DropDownListCAP.DataValueField = "cap";

        DropDownListCAP.DataBind();
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idProvincia;

        if ((DropDownListProvincia.SelectedValue != null) &&
            (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
            CaricaComuni(idProvincia);

        int idComune;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idComune;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    #endregion
}