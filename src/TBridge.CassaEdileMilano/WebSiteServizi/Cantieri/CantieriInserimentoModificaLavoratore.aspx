<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriInserimentoModificaLavoratore.aspx.cs" Inherits="CantieriInserimentoModificaLavoratore" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresa.ascx" TagName="CantieriRicercaImpresa"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriRicercaCommittente.ascx" TagName="CantieriRicercaCommittente"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc4:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc5:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento o modifica lavoratore"
        titolo="Cantieri" />
    <br />
    <asp:Panel ID="PanelDatiAggiornamento" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="94px"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIdLavoratore" runat="server" Width="353px" Enabled="False"
                        ReadOnly="True"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelDatiComuni" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelCognome" runat="server" Text="Cognome"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCognome" runat="server" Width="353px" MaxLength="255"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxCognome"
                        ErrorMessage="Digitare un cognome"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelNome" runat="server" Text="Nome"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNome" runat="server" Width="353px" MaxLength="11"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNome"
                        ErrorMessage="Digitare un nome"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelDataNascita" runat="server" Text="Data di nascita (gg/mm/aaaa)"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataNascita" runat="server" Width="353px" MaxLength="16"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxDataNascita"
                        ErrorMessage="Digitare una data di nascita"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                        ErrorMessage="Formato data errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonOperazione" runat="server" Text="Inserisci / Aggiorna" OnClick="ButtonOperazione_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
