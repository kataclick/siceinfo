﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Enums;

public partial class Cantieri_GeneraCantiere : System.Web.UI.Page
{
    private readonly CptBusiness biz = new CptBusiness();
    private readonly CantieriBusiness bizCant = new CantieriBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGenerazioneDaNotifiche);
        #endregion

        if (!Page.IsPostBack)
        {
            ButtonSalva.Enabled = false;

            if (Request.QueryString["notificaRiferimento"] != null
                && Request.QueryString["segnalazione"] != null)
            {
                Int32 notificaRiferimento = Int32.Parse(Request.QueryString["notificaRiferimento"]);
                Boolean segnalazione = Boolean.Parse(Request.QueryString["segnalazione"]);

                NotificaTelematica notifica = biz.GetNotificaTelematicaUltimaVersione(notificaRiferimento);
                if (notifica != null)
                {
                    ViewState["NotificaRiferimento"] = notificaRiferimento;
                    ViewState["segnalazione"] = segnalazione;
                    SelezioneIndirizziNotifica1.CaricaNotifica(notifica);
                    SegnalazioneDati1.CaricaDatiIniziali();
                    ButtonSalva.Enabled = true;

                    if (!segnalazione)
                    {
                        divSegnalazione.Visible = false;
                    }
                }
            }
        }
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["NotificaRiferimento"] != null)
            {
                Int32 idIndirizzo = (SelezioneIndirizziNotifica1.GetIndirizzoSelezionato()).Value;
                Int32 notificaRiferimento = (Int32)ViewState["NotificaRiferimento"];
                Boolean segnalazioneF = (Boolean)ViewState["segnalazione"];

                NotificaTelematica notifica = biz.GetNotificaTelematicaUltimaVersione(notificaRiferimento);
                Indirizzo indirizzo = notifica.Indirizzi.GetById(idIndirizzo);
                Segnalazione segnalazione = null;

                if (segnalazioneF)
                {
                    segnalazione = SegnalazioneDati1.CreaSegnalazione();
                }

                CreaCantiere(notifica, indirizzo, segnalazione);
                SegnalazioneDati1.Abilita(false);
                SelezioneIndirizziNotifica1.Abilita(false);
                ButtonSalva.Enabled = false;
            }
        }
    }

    private void CreaCantiere(NotificaTelematica notifica, Indirizzo indirizzo, Segnalazione segnalazione)
    {
        Cantiere cantiere = new Cantiere();

        cantiere.NotificaRiferimento = notifica.IdNotificaRiferimento;

        cantiere.DescrizioneLavori = notifica.NaturaOpera;

        if (notifica.OperaPubblica.HasValue)
        {
            cantiere.TipologiaAppalto = notifica.OperaPubblica.Value ? TipologiaAppalto.Pubblico : TipologiaAppalto.Privato;
        }
        else
        {
            cantiere.TipologiaAppalto = TipologiaAppalto.NonDefinito;
        }

        #region Indirizzo
        cantiere.Indirizzo = Presenter.NormalizzaCampoTesto(indirizzo.Indirizzo1);
        cantiere.Civico = Presenter.NormalizzaCampoTesto(indirizzo.Civico);
        cantiere.Comune = Presenter.NormalizzaCampoTesto(indirizzo.Comune);
        cantiere.Provincia = Presenter.NormalizzaCampoTesto(indirizzo.Provincia);
        cantiere.Cap = indirizzo.Cap;
        if (indirizzo.Latitudine.HasValue)
        {
            cantiere.Latitudine = Convert.ToDouble(indirizzo.Latitudine.Value);
        }
        if (indirizzo.Longitudine.HasValue)
        {
            cantiere.Longitudine = Convert.ToDouble(indirizzo.Longitudine.Value);
        }
        #endregion

        #region Date e numeri
        if (indirizzo.DataInizioLavori.HasValue)
        {
            cantiere.DataInizioLavori = indirizzo.DataInizioLavori;
        }
        else
        {
            cantiere.DataInizioLavori = notifica.DataInizioLavori;
        }

        if (cantiere.DataInizioLavori.HasValue)
        {
            Int32 durataGiorni = 0;
            if (!String.IsNullOrEmpty(indirizzo.DescrizioneDurata) && indirizzo.NumeroDurata.HasValue)
            {
                switch (indirizzo.DescrizioneDurata.ToUpper())
                {
                    case "GIORNI":
                        durataGiorni = indirizzo.NumeroDurata.Value;
                        break;
                    case "MESI":
                        durataGiorni = indirizzo.NumeroDurata.Value * 30;
                        break;
                }
            }
            else
            {
                if (notifica.Durata.HasValue)
                {
                    durataGiorni = notifica.Durata.Value;
                }
            }
            if (durataGiorni > 0)
            {
                cantiere.DataFineLavori = cantiere.DataInizioLavori.Value.AddDays(durataGiorni);
            }
        }

        cantiere.Importo = Convert.ToDouble(notifica.AmmontareComplessivo);
        #endregion

        #region Direzione lavori
        if (notifica.DirettoreLavori != null)
        {
            cantiere.DirezioneLavori = String.Format("{0} {1}", notifica.DirettoreLavori.PersonaCognome, notifica.DirettoreLavori.PersonaNome);
        }
        #endregion

        #region Committente
        cantiere.Committente = new Committente();
        cantiere.Committente.FonteNotifica = false;
        cantiere.Committente.RagioneSociale = Presenter.NormalizzaCampoTesto(String.Format("{0} {1}", notifica.Committente.PersonaCognome, notifica.Committente.PersonaNome));
        cantiere.Committente.PersonaRiferimento = cantiere.Committente.RagioneSociale;
        cantiere.Committente.CodiceFiscale = Presenter.NormalizzaCampoTesto(notifica.Committente.PersonaCodiceFiscale);
        cantiere.Committente.Indirizzo = Presenter.NormalizzaCampoTesto(notifica.Committente.PersonaIndirizzo);
        cantiere.Committente.Comune = Presenter.NormalizzaCampoTesto(notifica.Committente.PersonaComune);
        cantiere.Committente.Provincia = Presenter.NormalizzaCampoTesto(notifica.Committente.PersonaProvincia);
        cantiere.Committente.Cap = Presenter.NormalizzaCampoTesto(notifica.Committente.PersonaCap);

        bizCant.InsertCommittente(cantiere.Committente);
        #endregion

        if (bizCant.InsertCantiere(cantiere))
        {
            TBridge.Cemi.Cantieri.Type.Collections.SubappaltoCollection subappalti = new TBridge.Cemi.Cantieri.Type.Collections.SubappaltoCollection();

            // Subappalti
            foreach (TBridge.Cemi.Cpt.Type.Entities.SubappaltoNotificheTelematiche sub in notifica.ImpreseAffidatarie)
            {
                TBridge.Cemi.Cantieri.Type.Entities.Subappalto subCant = new TBridge.Cemi.Cantieri.Type.Entities.Subappalto();
                subCant.IdCantiere = cantiere.IdCantiere.Value;
                subappalti.Add(subCant);

                #region Subappaltata
                if (sub.ImpresaSelezionata != null)
                {
                    // Provo a cercarla nell'anagrafica SiceNew
                    Int32? idImpresa = biz.GetIdImpresaDaCodiceFiscale(sub.ImpresaSelezionata.CodiceFiscale);
                    if (idImpresa.HasValue)
                    {
                        subCant.SubAppaltata = new Impresa();
                        subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.SiceNew;
                        subCant.SubAppaltata.IdImpresa = idImpresa.Value;
                    }
                    else
                    {
                        // Guardo se l'impresa era già stata trovata o inserita
                        Impresa impresaGiaSel = subappalti.FindImpresa(sub.ImpresaSelezionata.CodiceFiscale);

                        if (impresaGiaSel == null)
                        {
                            // Provo a cercarla nell'anagrafica cantieri
                            ImpresaCollection imprese =
                                bizCant.GetimpreseOrdinate(TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri,
                                    null, null, null, null,
                                    sub.ImpresaSelezionata.CodiceFiscale,
                                    null, null, false);

                            Boolean impresaTrovata = false;
                            // Guardo se tra le imprese trovate ce n'è una inserita dagli ispettori,
                            // se la trovo la seleziono
                            foreach (Impresa imp in imprese)
                            {
                                if (!imp.FonteNotifica)
                                {
                                    impresaTrovata = true;
                                    subCant.SubAppaltata = imp;
                                    break;
                                }
                            }
                            // Se non ho trovato l'impresa tra quelle inserite dagli ispettori,
                            // e esiste dalle vecchie notifiche la seleziono
                            if (!impresaTrovata && imprese.Count > 0)
                            {
                                impresaTrovata = true;
                                subCant.SubAppaltata = imprese[0];
                            }

                            // A questo punto la creo
                            if (!impresaTrovata)
                            {
                                subCant.SubAppaltata = new Impresa();
                                subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri;
                                subCant.SubAppaltata.FonteNotifica = true;

                                subCant.SubAppaltata.Cap = sub.ImpresaSelezionata.Cap;
                                subCant.SubAppaltata.CodiceFiscale = sub.ImpresaSelezionata.CodiceFiscale;
                                subCant.SubAppaltata.Comune = sub.ImpresaSelezionata.Comune;
                                subCant.SubAppaltata.Fax = sub.ImpresaSelezionata.Fax;
                                subCant.SubAppaltata.Indirizzo = sub.ImpresaSelezionata.Indirizzo;
                                subCant.SubAppaltata.LavoratoreAutonomo = false;
                                subCant.SubAppaltata.PartitaIva = sub.ImpresaSelezionata.PartitaIva;
                                subCant.SubAppaltata.Provincia = sub.ImpresaSelezionata.Provincia;
                                subCant.SubAppaltata.RagioneSociale = sub.ImpresaSelezionata.RagioneSociale;
                                subCant.SubAppaltata.Telefono = sub.ImpresaSelezionata.Telefono;

                                bizCant.InsertImpresa(subCant.SubAppaltata);
                            }
                        }
                        else
                        {
                            subCant.SubAppaltata = impresaGiaSel;
                        }
                    }
                }
                #endregion
            }

            foreach(TBridge.Cemi.Cpt.Type.Entities.SubappaltoNotificheTelematiche sub in notifica.ImpreseEsecutrici)
            {
                TBridge.Cemi.Cantieri.Type.Entities.Subappalto subCant = new TBridge.Cemi.Cantieri.Type.Entities.Subappalto();
                subCant.IdCantiere = cantiere.IdCantiere.Value;
                subappalti.Add(subCant);

                #region Subappaltata
                if (sub.ImpresaSelezionata != null)
                {
                    // Provo a cercarla nell'anagrafica SiceNew
                    Int32? idImpresa = biz.GetIdImpresaDaCodiceFiscale(sub.ImpresaSelezionata.CodiceFiscale);
                    if (idImpresa.HasValue)
                    {
                        subCant.SubAppaltata = new Impresa();
                        subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.SiceNew;
                        subCant.SubAppaltata.IdImpresa = idImpresa.Value;
                    }
                    else
                    {
                        // Guardo se l'impresa era già stata trovata o inserita
                        Impresa impresaGiaSel = subappalti.FindImpresa(sub.ImpresaSelezionata.CodiceFiscale);

                        if (impresaGiaSel == null)
                        {
                            // Provo a cercarla nell'anagrafica cantieri
                            ImpresaCollection imprese =
                                bizCant.GetimpreseOrdinate(TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri,
                                    null, null, null, null,
                                    sub.ImpresaSelezionata.CodiceFiscale,
                                    null, null, false);

                            Boolean impresaTrovata = false;
                            // Guardo se tra le imprese trovate ce n'è una inserita dagli ispettori,
                            // se la trovo la seleziono
                            foreach (Impresa imp in imprese)
                            {
                                if (!imp.FonteNotifica)
                                {
                                    impresaTrovata = true;
                                    subCant.SubAppaltata = imp;
                                    break;
                                }
                            }
                            // Se non ho trovato l'impresa tra quelle inserite dagli ispettori,
                            // e esiste dalle vecchie notifiche la seleziono
                            if (!impresaTrovata && imprese.Count > 0)
                            {
                                impresaTrovata = true;
                                subCant.SubAppaltata = imprese[0];
                            }

                            // A questo punto la creo
                            if (!impresaTrovata)
                            {
                                subCant.SubAppaltata = new Impresa();
                                subCant.SubAppaltata.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri;
                                subCant.SubAppaltata.FonteNotifica = true;

                                subCant.SubAppaltata.Cap = sub.ImpresaSelezionata.Cap;
                                subCant.SubAppaltata.CodiceFiscale = sub.ImpresaSelezionata.CodiceFiscale;
                                subCant.SubAppaltata.Comune = sub.ImpresaSelezionata.Comune;
                                subCant.SubAppaltata.Fax = sub.ImpresaSelezionata.Fax;
                                subCant.SubAppaltata.Indirizzo = sub.ImpresaSelezionata.Indirizzo;
                                subCant.SubAppaltata.LavoratoreAutonomo = false;
                                subCant.SubAppaltata.PartitaIva = sub.ImpresaSelezionata.PartitaIva;
                                subCant.SubAppaltata.Provincia = sub.ImpresaSelezionata.Provincia;
                                subCant.SubAppaltata.RagioneSociale = sub.ImpresaSelezionata.RagioneSociale;
                                subCant.SubAppaltata.Telefono = sub.ImpresaSelezionata.Telefono;

                                bizCant.InsertImpresa(subCant.SubAppaltata);
                            }
                        }
                        else
                        {
                            subCant.SubAppaltata = impresaGiaSel;
                        }
                    }
                }
                #endregion

                #region Subappaltatrice
                if (sub.AppaltataDa != null)
                {
                    // Provo a cercarla nell'anagrafica SiceNew
                    Int32? idImpresa = biz.GetIdImpresaDaCodiceFiscale(sub.AppaltataDa.CodiceFiscale);
                    if (idImpresa.HasValue)
                    {
                        subCant.SubAppaltatrice = new Impresa();
                        subCant.SubAppaltatrice.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.SiceNew;
                        subCant.SubAppaltatrice.IdImpresa = idImpresa.Value;
                    }
                    else
                    {
                        // Guardo se l'impresa era già stata trovata o inserita
                        Impresa impresaGiaSel = subappalti.FindImpresa(sub.AppaltataDa.CodiceFiscale);

                        if (impresaGiaSel == null)
                        {
                            // Provo a cercarla nell'anagrafica cantieri
                            ImpresaCollection imprese =
                                bizCant.GetimpreseOrdinate(TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri,
                                    null, null, null, null,
                                    sub.AppaltataDa.CodiceFiscale,
                                    String.Empty, String.Empty, false);

                            Boolean impresaTrovata = false;
                            // Guardo se tra le imprese trovate ce n'è una inserita dagli ispettori,
                            // se la trovo la seleziono
                            foreach (Impresa imp in imprese)
                            {
                                if (!imp.FonteNotifica)
                                {
                                    impresaTrovata = true;
                                    subCant.SubAppaltatrice = imp;
                                    break;
                                }
                            }
                            // Se non ho trovato l'impresa tra quelle inserite dagli ispettori,
                            // e esiste dalle vecchie notifiche la seleziono
                            if (!impresaTrovata && imprese.Count > 0)
                            {
                                impresaTrovata = true;
                                subCant.SubAppaltatrice = imprese[0];
                            }

                            // A questo punto la creo
                            if (!impresaTrovata)
                            {
                                subCant.SubAppaltatrice = new Impresa();
                                subCant.SubAppaltatrice.TipoImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa.Cantieri;
                                subCant.SubAppaltatrice.FonteNotifica = true;

                                subCant.SubAppaltatrice.Cap = sub.ImpresaSelezionata.Cap;
                                subCant.SubAppaltatrice.CodiceFiscale = sub.ImpresaSelezionata.CodiceFiscale;
                                subCant.SubAppaltatrice.Comune = sub.ImpresaSelezionata.Comune;
                                subCant.SubAppaltatrice.Fax = sub.ImpresaSelezionata.Fax;
                                subCant.SubAppaltatrice.Indirizzo = sub.ImpresaSelezionata.Indirizzo;
                                subCant.SubAppaltatrice.LavoratoreAutonomo = false;
                                subCant.SubAppaltatrice.PartitaIva = sub.ImpresaSelezionata.PartitaIva;
                                subCant.SubAppaltatrice.Provincia = sub.ImpresaSelezionata.Provincia;
                                subCant.SubAppaltatrice.RagioneSociale = sub.ImpresaSelezionata.RagioneSociale;
                                subCant.SubAppaltatrice.Telefono = sub.ImpresaSelezionata.Telefono;

                                bizCant.InsertImpresa(subCant.SubAppaltatrice);
                            }
                        }
                        else
                        {
                            subCant.SubAppaltatrice = impresaGiaSel;
                        }
                    }
                }
                #endregion
            }

            bizCant.InsertSubappalti(subappalti);

            // Segnalazione
            if (segnalazione != null)
            {
                segnalazione.IdCantiere = cantiere.IdCantiere.Value;
                bizCant.InsertSegnalazione(segnalazione);
            }
        }
    }

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!SelezioneIndirizziNotifica1.GetIndirizzoSelezionato().HasValue)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorSegnalazione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Boolean segnalazione = (Boolean)ViewState["segnalazione"];
        args.IsValid = true;

        if (segnalazione)
        {
            Page.Validate("segnalaCantiere");

            if (!Page.IsValid)
            {
                args.IsValid = false;
            }
        }
    }
}