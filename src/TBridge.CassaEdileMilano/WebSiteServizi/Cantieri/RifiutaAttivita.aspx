﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RifiutaAttivita.aspx.cs" Inherits="Cantieri_RifiutaAttivita" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="standardDiv">
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <table class="standardTable">
            <tr>
                <td>
                    Motivazione
                </td>
                <td>
                    <telerik:RadComboBox
                        ID="RadComboBoxMotivazione"
                        runat="server"
                        Width="150px"
                        AppendDataBoundItems="true"
                        EmptyMessage="Selezionare una motivazione">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidatorMotivazione"
                        runat="server"
                        ControlToValidate="RadComboBoxMotivazione"
                        ErrorMessage="Selezionare una motivazione"
                        ValidationGroup="salva"
                        CssClass="messaggiErrore">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button
                        ID="ButtonSalva"
                        runat="server"
                        Text="Salva"
                        Width="100px"
                        ValidationGroup="salva" onclick="ButtonSalva_Click" />
                    <asp:ValidationSummary
                        ID="ValidationSummarySalva"
                        runat="server"
                        ValidationGroup="salva"
                        CssClass="messaggiErrore" />
                    <asp:Label
                        ID="LabelMessaggio"
                        runat="server"
                        CssClass="messaggiErrore">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
