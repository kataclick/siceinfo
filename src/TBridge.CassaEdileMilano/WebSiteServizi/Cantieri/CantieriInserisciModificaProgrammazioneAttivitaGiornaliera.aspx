<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriInserisciModificaProgrammazioneAttivitaGiornaliera.aspx.cs"
    Inherits="CantieriInserisciModificaProgrammazioneAttivitaGiornaliera" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriRicercaCantiere.ascx" TagName="CantieriRicercaCantiere"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento / modifica attivit�"
        titolo="Cantieri" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Giorno
            </td>
            <td>
                <asp:TextBox ID="TextBoxGiorno" runat="server" Enabled="False" ReadOnly="True" Width="315px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Ispettore
            </td>
            <td>
                <asp:TextBox ID="TextBoxIspettore" runat="server" Enabled="False" ReadOnly="True"
                    Width="315px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Zona
            </td>
            <td>
                <asp:TextBox ID="TextBoxZona" runat="server" Enabled="False" ReadOnly="True" Width="315px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Attivit�
            </td>
            <td>
                <asp:DropDownList ID="DropDownListAttivita" runat="server" Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Descrizione
            </td>
            <td>
                <asp:TextBox ID="TextBoxDecrizione" runat="server" Height="88px" MaxLength="100"
                    TextMode="MultiLine" Width="315px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Cantiere selezionato
            </td>
            <td>
                <asp:TextBox ID="TextBoxCantiere" runat="server" Enabled="False" Height="85px" ReadOnly="True"
                    TextMode="MultiLine" Width="315px"></asp:TextBox>&nbsp;
                <asp:Button ID="ButtonSelezionaCantiere" runat="server" OnClick="ButtonSelezionaCantiere_Click"
                    Text="Seleziona cantiere" /><br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelRicercaCantieri" runat="server" Font-Bold="True" Text="Ricerca cantieri"
                    Visible="False"></asp:Label><br />
                <uc2:CantieriRicercaCantiere ID="CantieriRicercaCantiere1" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="ButtonInserisciModifica" runat="server" OnClick="ButtonInserisciModifica_Click"
                    Text="Inserisci" Width="170px" />
                <asp:Button ID="ButtonInserisciContinua" runat="server" OnClick="ButtonInserisciContinua_Click"
                    Text="Inserisci e continua" Width="170px" />
                <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="170px" OnClick="ButtonIndietro_Click" /><br />
                <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
