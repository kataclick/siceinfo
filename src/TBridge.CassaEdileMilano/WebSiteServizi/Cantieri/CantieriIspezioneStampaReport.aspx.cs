using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriIspezioneStampaReport : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita
            = new List<FunzionalitaPredefinite>
                  {
                      FunzionalitaPredefinite.CantieriConsuPrev,
                      FunzionalitaPredefinite.CantieriConsuPrevRUI
                  };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        // Fix report viewer bug in IE7
        //string userAgent = Request.ServerVariables.Get("HTTP_USER_AGENT");
        //if (userAgent.Contains("MSIE 7.0"))
        //{
        //    ReportViewerRapportoIspezione.Style.Add("margin-bottom", "70px");
        //    //ReportViewerRapportoIspezione.Attributes.Add("style", "overflow:auto;");
        //}

        if (!Page.IsPostBack)
        {
            if (Request.QueryString["modalita"] != "ricercaIspezioni")
                ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

            if (Request.QueryString["idIspezione"] != null)
            {
                int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
                RapportoIspezione ispezione = biz.GetIspezioneByKey(idIspezione);

                if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere))
                {
                    ButtonLettere.Visible = true;
                }

                CantieriTestataIspezione1.ImpostaTestata(ispezione);
                VisualizzaReportCompleto();
            }
        }
    }

    protected void ButtonStampaFoglio1_Click(object sender, EventArgs e)
    {
        ReportViewerRapportoIspezione.Visible = true;

        //Response.Redirect("ReportRapportoIspezioneFoglio1.aspx?idIspezione=" + Request.QueryString["idIspezione"]);        
        int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
        RapportoIspezione ispezione = biz.GetIspezioneByKey(idIspezione);

        ReportViewerRapportoIspezione.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        ReportViewerRapportoIspezione.ServerReport.ReportPath = "/ReportCantieri/RapportoIspezioneFoglio1";
        ReportParameter[] listaParam = new ReportParameter[3];
        listaParam[0] = new ReportParameter("idIspezione", ispezione.IdIspezione.ToString());
        listaParam[1] = new ReportParameter("idCantiere", ispezione.Cantiere.IdCantiere.ToString());
        listaParam[2] = new ReportParameter("idIspettore", ispezione.Ispettore.IdIspettore.ToString());

        ReportViewerRapportoIspezione.ServerReport.SetParameters(listaParam);
    }

    protected void ButtonStampaFoglio2_Click(object sender, EventArgs e)
    {
        ReportViewerRapportoIspezione.Visible = true;

        //Response.Redirect("ReportRapportoIspezioneFoglio2.aspx?idIspezione=" + Request.QueryString["idIspezione"]);
        int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
        RapportoIspezione ispezione = biz.GetIspezioneByKey(idIspezione);

        ReportViewerRapportoIspezione.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        ReportViewerRapportoIspezione.ServerReport.ReportPath = "/ReportCantieri/RapportoIspezioneFoglio2";
        ReportParameter[] listaParam = new ReportParameter[2];
        listaParam[0] = new ReportParameter("idIspezione", ispezione.IdIspezione.ToString());
        listaParam[1] = new ReportParameter("idIspettore", ispezione.Ispettore.IdIspettore.ToString());

        ReportViewerRapportoIspezione.ServerReport.SetParameters(listaParam);
    }

    protected void ButtonStampaRapporto_Click(object sender, EventArgs e)
    {
        VisualizzaReportCompleto();
    }

    private void VisualizzaReportCompleto()
    {
        ReportViewerRapportoIspezione.Visible = true;

        int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
        RapportoIspezione ispezione = biz.GetIspezioneByKey(idIspezione);

        ReportViewerRapportoIspezione.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        ReportViewerRapportoIspezione.ServerReport.ReportPath = "/ReportCantieri/ReportIspezione";
        ReportParameter[] listaParam = new ReportParameter[3];
        listaParam[0] = new ReportParameter("idIspezione", ispezione.IdIspezione.ToString());
        listaParam[1] = new ReportParameter("idCantiere", ispezione.Cantiere.IdCantiere.ToString());
        listaParam[2] = new ReportParameter("idIspettore", ispezione.Ispettore.IdIspettore.ToString());

        ReportViewerRapportoIspezione.ServerReport.SetParameters(listaParam);
        //Response.Redirect("ReportRapportoIspezione.aspx?idIspezione=" + Request.QueryString["idIspezione"]);
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["modalita"] == "ricercaIspezioni")
        {
            string indirizzo = "~/Cantieri/CantieriRicercaIspezioni.aspx?modalita=indietro";

            if (Request.QueryString["dal"] != null)
                indirizzo = String.Format("{0}&dal={1}", indirizzo, Request.QueryString["dal"]);
            if (Request.QueryString["al"] != null)
                indirizzo = String.Format("{0}&al={1}", indirizzo, Request.QueryString["al"]);
            if (Request.QueryString["ispettore"] != null)
                indirizzo = String.Format("{0}&ispettore={1}", indirizzo, Request.QueryString["ispettore"]);
            if (Request.QueryString["stato"] != null)
                indirizzo = String.Format("{0}&stato={1}", indirizzo, Request.QueryString["stato"]);
            if (Request.QueryString["detAppalto"] != null)
                indirizzo = String.Format("{0}&detAppalto={1}", indirizzo, Request.QueryString["detAppalto"]);
            if (Request.QueryString["subApp"] != null)
                indirizzo = String.Format("{0}&subApp={1}", indirizzo, Request.QueryString["subApp"]);
            indirizzo = String.Format("{0}&pagina={1}", indirizzo, Request.QueryString["pagina"]);

            Response.Redirect(indirizzo);
        }
    }

    protected void ButtonLettere_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/Cantieri/CantieriSelezioneLettera.aspx?idIspezione={0}", Request.QueryString["idIspezione"]));
    }
}