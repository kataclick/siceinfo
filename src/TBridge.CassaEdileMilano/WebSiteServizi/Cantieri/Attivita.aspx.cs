﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Cantieri.Type.Filters;

public partial class Cantieri_Attivita : System.Web.UI.Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        RicercaCantieriPerIspettori1.OnCantiereSelected += new TBridge.Cemi.Cantieri.Type.Delegates.CantieriSelectedEventHandler(RicercaCantieriPerIspettori1_OnCantiereSelected);

        if (!Page.IsPostBack)
        {
            CaricaTipiAttivita();

            if (Request.QueryString["modalita"] == null)
            {
                // Inserimento

                if (Request.QueryString["data"] != null)
                {
                    // Selezionata data

                    Char[] separatore1 = new char[1];
                    separatore1[0] = ' ';
                    Char[] separatore2 = new char[1];
                    separatore2[0] = '/';
                    Char[] separatore3 = new char[1];
                    separatore3[0] = ':';

                    String[] primoSplit = Request.QueryString["data"].Split(separatore1);
                    String[] splitData = primoSplit[0].Split(separatore2);
                    String[] splitOra = primoSplit[1].Split(separatore3);

                    //DateTime data = new DateTime(Int32.Parse(splitData[2]), Int32.Parse(splitData[1]),
                    //                             Int32.Parse(splitData[0]),
                    //                             Int32.Parse(splitOra[0]), Int32.Parse(splitOra[1]), 0);
                    // Non so perchè ma il controllo Telerik da sempre la data indietro di un mese
                    DateTime data = new DateTime(Int32.Parse(splitData[2]), Int32.Parse(splitData[1]) + 1,
                                                 Int32.Parse(splitData[0]),
                                                 9, 0, 0);
                    
                    InizializzaNuovaAttivita(data);
                }
                else
                {
                    // Selezionato cantiere
                    InizializzaNuovaAttivita(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 9, 0, 0));
                    RadComboBoxTipoAttivita.SelectedValue = "1";
                    RadComboBoxTipoAttivita.Enabled = false;
                    ButtonRicercaCantieri.Visible = false;
                    //RadTextBoxDescrizione.Enabled = false;

                    Int32 idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
                    TBridge.Cemi.Cantieri.Type.Entities.Cantiere cantiere = biz.GetCantiere(idCantiere);
                    CaricaCantiere(cantiere);
                }
            }
            else
            {
                // Modifica
                Int32 idAttivita = Int32.Parse(Request.QueryString["idAttivita"]);
                CaricaAttivita(idAttivita);
            }
        }
    }

    void RicercaCantieriPerIspettori1_OnCantiereSelected(TBridge.Cemi.Cantieri.Type.Entities.Cantiere cantiere)
    {
        CaricaCantiere(cantiere);
    }

    private void CaricaCantiere(TBridge.Cemi.Cantieri.Type.Entities.Cantiere cantiere)
    {
        ViewState["IdCantiere"] = cantiere.IdCantiere.Value;
        InformazioniBaseCantiere1.CaricaCantiere(cantiere);
        trRicercaCantieri.Visible = false;
    }

    private void CaricaCantiere(TBridge.Cemi.Type.Domain.Cantiere cantiere)
    {
        ViewState["IdCantiere"] = cantiere.IdCantiere;
        InformazioniBaseCantiere1.CaricaCantiere(cantiere);
        trRicercaCantieri.Visible = false;
    }

    private void CaricaAttivita(Int32 idAttivita)
    {
        Boolean rapportoIspezionePresente = false;
        CantieriCalendarioAttivita attivita = bizEF.GetAttivita(idAttivita);

        ViewState["IdAttivita"] = idAttivita;
        RadDateTimePickerInizio.SelectedDate = attivita.Data.Date;
        //RadDateTimePickerFine.SelectedDate = attivita.DataFineAttivita;
        
        RadTimePickerDalle.SelectedDate = new DateTime(1900, 1, 1, attivita.Data.Hour, attivita.Data.Minute, 0);
        RadTimePickerAlle.SelectedDate = new DateTime(1900, 1, 1, attivita.DataFineAttivita.Hour, attivita.DataFineAttivita.Minute, 1);
        SelezionaRadioPeriodo(RadTimePickerDalle.SelectedDate.Value, RadTimePickerAlle.SelectedDate.Value);
        
        RadComboBoxTipoAttivita.SelectedValue = attivita.IdTipologiaAttivita.ToString();
        RadTextBoxDescrizione.Text = attivita.Descrizione;
        LabelIspettore.Text = attivita.Ispettore.NomeCompleto;

        TBridge.Cemi.Cantieri.Type.Entities.RapportoIspezione ispezione = null;
        if (attivita.CantieriCantieri != null && attivita.CantieriCantieri.Count == 1)
        {
            Cantiere cantiere = attivita.CantieriCantieri.Single();
            CaricaCantiere(cantiere);

            ButtonRicercaCantieri.Enabled = false;
            RadComboBoxTipoAttivita.Enabled = false;

            IspezioniFilter filtro = new IspezioniFilter();
            filtro.IdCantiere = cantiere.IdCantiere;
            TBridge.Cemi.Cantieri.Type.Collections.RapportoIspezioneCollection rappIsp = biz.GetIspezioni(filtro);
            if (rappIsp.Count > 0)
            {
                rapportoIspezionePresente = true;
            }
        }

        Int32? idIspettore = null;
        if (GestioneUtentiBiz.IsIspettore())
        {
            idIspettore =
                ((TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).
                    IdIspettore;
        }

        if (!idIspettore.HasValue || !attivita.InCarico(idIspettore.Value) || rapportoIspezionePresente || attivita.Rifiuto != null)
        {
            DisabilitaTutto();
        }
    }

    private void InizializzaNuovaAttivita(DateTime data)
    {
        RadDateTimePickerInizio.SelectedDate = data;
        //RadDateTimePickerFine.SelectedDate = data.AddHours(9);

        TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore ispettore = 
            ((TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente());
        LabelIspettore.Text = String.Format("{0} {1}", ispettore.Cognome, ispettore.Nome);
    }

    private void CaricaTipiAttivita()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxTipoAttivita,
            bizEF.GetTipologieAttivita(),
            "Descrizione",
            "Id");
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CantieriCalendarioAttivita attivita = CreaAttivita();

            if (ViewState["IdAttivita"] == null)
            {
                // Inserimento
                try
                {
                    bizEF.InsertAttivita(attivita);
                    ConfermaInserimento();
                }
                catch
                {
                    ErroreNellInserimento();
                }
            }
            else
            {
                // Aggiornamento
                attivita.Id = (Int32)ViewState["IdAttivita"];
                try
                {
                    bizEF.UpdateAttivita(attivita);
                    ConfermaInserimento();
                }
                catch
                {
                    ErroreNellInserimento();
                }
            }
        }
    }

    private CantieriCalendarioAttivita CreaAttivita()
    {
        CantieriCalendarioAttivita attivita = new CantieriCalendarioAttivita();

        attivita.Data = new DateTime(
            RadDateTimePickerInizio.SelectedDate.Value.Year,
            RadDateTimePickerInizio.SelectedDate.Value.Month,
            RadDateTimePickerInizio.SelectedDate.Value.Day,
            RadTimePickerDalle.SelectedDate.Value.Hour, RadTimePickerDalle.SelectedDate.Value.Minute, 0);
        //attivita.Durata = (Int32)(RadDateTimePickerFine.SelectedDate.Value - RadDateTimePickerInizio.SelectedDate.Value).TotalHours;

        DateTime oraInizio = new DateTime(1900, 1, 1, RadTimePickerDalle.SelectedDate.Value.Hour, RadTimePickerDalle.SelectedDate.Value.Minute, RadTimePickerDalle.SelectedDate.Value.Second);
        DateTime oraFine = new DateTime(1900, 1, 1, RadTimePickerAlle.SelectedDate.Value.Hour, RadTimePickerAlle.SelectedDate.Value.Minute, RadTimePickerAlle.SelectedDate.Value.Second);

        attivita.Durata = (Int32) (oraFine - oraInizio).TotalHours;
        //attivita.Durata = 9;
        attivita.IdTipologiaAttivita = Int32.Parse(RadComboBoxTipoAttivita.SelectedValue);
        attivita.DataPresaInCarico = DateTime.Now;
        attivita.IdIspettore = ((TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdIspettore;
        if (!String.IsNullOrEmpty(RadTextBoxDescrizione.Text))
        {
            attivita.Descrizione = Presenter.NormalizzaCampoTesto(RadTextBoxDescrizione.Text);
        }
        if (ViewState["IdCantiere"] != null)
        {
            attivita.IdCantiere = (Int32)ViewState["IdCantiere"];
        }

        return attivita;
    }

    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento/modifica dell'attività.";
    }

    private void ConfermaInserimento()
    {
        DisabilitaTutto();

        LabelMessaggio.Text = "Attività salvata correttamente.";
    }

    public void DisabilitaTutto()
    {
        RadDateTimePickerInizio.Enabled = false;
        //RadDateTimePickerFine.Enabled = false;
        RadTextBoxDescrizione.Enabled = false;
        RadComboBoxTipoAttivita.Enabled = false;
        trRicercaCantieri.Visible = false;
        ButtonRicercaCantieri.Enabled = false;
        ButtonSalva.Enabled = false;

        ButtonRicercaCantieri.Visible = false;
        ButtonSalva.Visible = false;
    }

    #region Custom Validators
    protected void CustomValidatorInizio_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadDateTimePickerInizio.SelectedDate.HasValue)
        {
            args.IsValid = true;
        }
    }

    //protected void CustomValidatorFine_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = false;

    //    if (RadDateTimePickerFine.SelectedDate.HasValue)
    //    {
    //        args.IsValid = true;
    //    }
    //}

    protected void CustomValidatorFineMaggioreInizio_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        //if (RadDateTimePickerInizio.SelectedDate.HasValue && RadDateTimePickerFine.SelectedDate.HasValue)
        //{
        //    if (RadDateTimePickerInizio.SelectedDate.Value > RadDateTimePickerFine.SelectedDate.Value)
        //    {
        //        args.IsValid = false;
        //    }
        //}
        if (RadTimePickerDalle.SelectedDate.HasValue && RadTimePickerAlle.SelectedDate.HasValue)
        {
            DateTime dataInizio = new DateTime(1900, 1, 1, RadTimePickerDalle.SelectedDate.Value.Hour, RadTimePickerDalle.SelectedDate.Value.Minute, RadTimePickerDalle.SelectedDate.Value.Second);
            DateTime dataFine = new DateTime(1900, 1, 1, RadTimePickerAlle.SelectedDate.Value.Hour, RadTimePickerAlle.SelectedDate.Value.Minute, RadTimePickerAlle.SelectedDate.Value.Second);

            if (dataInizio > dataFine)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorCantiere_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadComboBoxTipoAttivita.SelectedValue == "1"
            && ViewState["IdCantiere"] == null)
        {
            // Ispezione
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDescrizione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadComboBoxTipoAttivita.SelectedValue != "1"
            && String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(RadTextBoxDescrizione.Text)))
        {
            // Ispezione
            args.IsValid = false;
        }
    }

    protected void CustomValidatorOrario_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (!String.IsNullOrEmpty(RadioButtonListPeriodo.SelectedValue))
        {
            args.IsValid = true;
        }
    }

    //protected void CustomValidatorInizioFineStessoGiorno_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = true;

    //    if (RadDateTimePickerInizio.SelectedDate.HasValue && RadDateTimePickerFine.SelectedDate.HasValue)
    //    {
    //        if (RadDateTimePickerInizio.SelectedDate.Value.ToString("ddMMyyyy") != RadDateTimePickerFine.SelectedDate.Value.ToString("ddMMyyyy"))
    //        {
    //            args.IsValid = false;
    //        }
    //    }
    //}
    #endregion

    protected void ButtonRicercaCantieri_Click(object sender, EventArgs e)
    {
        trRicercaCantieri.Visible = true;
    }

    protected void RadComboBoxTipoAttivita_SelectedIndexChanged(object o, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        switch (e.Value)
        {
            // Ispezione
            case "1":
                Presenter.SvuotaCampo(RadTextBoxDescrizione);
                //RadTextBoxDescrizione.Enabled = false;
                ButtonRicercaCantieri.Enabled = true;
                break;
            default:
                InformazioniBaseCantiere1.Reset();
                ViewState["IdCantiere"] = null;
                //RadTextBoxDescrizione.Enabled = true;
                ButtonRicercaCantieri.Enabled = false;
                break;
        }
    }

    protected void RadioButtonListPeriodo_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadTimePickerDalle.Enabled = false;
        RadTimePickerAlle.Enabled = false;

        switch (RadioButtonListPeriodo.SelectedValue)
        {
            case "G":
                RadTimePickerDalle.SelectedDate = new DateTime(1900, 1, 1, 8, 30, 0);
                RadTimePickerAlle.SelectedDate = new DateTime(1900, 1, 1, 17, 30, 0);
                break;
            case "M":
                RadTimePickerDalle.SelectedDate = new DateTime(1900, 1, 1, 8, 30, 0);
                RadTimePickerAlle.SelectedDate = new DateTime(1900, 1, 1, 12, 30, 0);
                break;
            case "P":
                RadTimePickerDalle.SelectedDate = new DateTime(1900, 1, 1, 13, 30, 0);
                RadTimePickerAlle.SelectedDate = new DateTime(1900, 1, 1, 17, 30, 0);
                break;
            case "E":
                RadTimePickerDalle.Enabled = true;
                RadTimePickerAlle.Enabled = true;
                break;
        }
    }

    protected void RadTimePicker_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
    {
        if (RadTimePickerDalle.SelectedDate.HasValue && RadTimePickerAlle.SelectedDate.HasValue)
        {
            SelezionaRadioPeriodo(RadTimePickerDalle.SelectedDate.Value, RadTimePickerAlle.SelectedDate.Value);
        }
    }

    private void SelezionaRadioPeriodo(DateTime inizio, DateTime fine)
    {
        RadTimePickerDalle.Enabled = false;
        RadTimePickerAlle.Enabled = false;

        if (inizio.Hour == 8 && inizio.Minute == 30
            && fine.Hour == 17 && fine.Minute == 30)
        {
            RadioButtonListPeriodo.SelectedValue = "G";
            return;
        }
        if (inizio.Hour == 8 && inizio.Minute == 30
            && fine.Hour == 12 && fine.Minute == 30)
        {
            RadioButtonListPeriodo.SelectedValue = "M";
            return;
        }
        if (inizio.Hour == 13 && inizio.Minute == 30
            && fine.Hour == 17 && fine.Minute == 30)
        {
            RadioButtonListPeriodo.SelectedValue = "P";
            return;
        }

        RadTimePickerDalle.Enabled = true;
        RadTimePickerAlle.Enabled = true;
        RadioButtonListPeriodo.SelectedValue = "E";
    }
}