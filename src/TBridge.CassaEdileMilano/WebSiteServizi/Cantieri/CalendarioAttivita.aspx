﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CalendarioAttivita.aspx.cs" Inherits="Cantieri_CalendarioAttivita" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/RicercaCantieriPerIspettori.ascx" tagname="RicercaCantieriPerIspettori" tagprefix="uc3" %>
<%@ Register src="WebControls/LegendaColoriAttivita.ascx" tagname="LegendaColoriAttivita" tagprefix="uc4" %>
<%@ Register src="../WebControls/MenuCantieriStatistiche.ascx" tagname="MenuCantieriStatistiche" tagprefix="uc5" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc5:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function OnClientClose(oWnd, args) {
                <%# RecuperaPostBackCode() %>;
            }

            function openRadWindowMappa(latitudine, longitudine, indirizzo, cap, comune, provincia, note) 
            {
                var oWindow = radopen("../VisualizzazioneMappa.aspx?latitudine=" + latitudine + "&longitudine=" + longitudine + "&cap=" + cap + "&indirizzo=" + indirizzo + "&provincia=" + provincia + "&comune=" + comune + "&note=" + note, null);
                oWindow.set_title("Mappa");
                oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                oWindow.setSize(800, 560);
                oWindow.center();
            }

            function openRadWindowNuovaAttivita(sender, eventArgs) {
                var data = eventArgs.get_time();
                var oWindow = radopen("../Cantieri/Attivita.aspx?data=" + data.getDate() + "/" + data.getMonth() + "/" + data.getFullYear() + " " + data.getHours() + ":" + data.getMinutes() , null);
                oWindow.set_title("Nuova Attività");
                oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                oWindow.setSize(600, 450);
                oWindow.center();
                oWindow.add_close(OnClientClose);
            }

            function openRadWindowModificaAttivita(sender, eventArgs) {
                var apt = eventArgs.get_appointment();
                var oWindow = radopen("../Cantieri/Attivita.aspx?modalita=modifica&idAttivita=" + apt.get_id() , null);
                oWindow.set_title("Modifica Attività");
                oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                oWindow.setSize(600, 450);
                oWindow.center();
                oWindow.add_close(OnClientClose);
            }

            function openRadWindowControllo(idCantiere) {
                var oWindow = radopen("../Cantieri/Attivita.aspx?idCantiere=" + idCantiere, null);
                oWindow.set_title("Nuova Attività");
                oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                oWindow.setSize(600, 450);
                oWindow.center();
                oWindow.add_close(OnClientClose);
            }

            function appointmentContextMenuItemClicked(sender, eventArgs)
            {
                var selectedAppointment = eventArgs.get_appointment();
                var clickedItem = eventArgs.get_item();

                var evento = clickedItem.get_value();
                if (evento == "APRI")
                {
                    var oWindow = radopen("../Cantieri/Attivita.aspx?modalita=modifica&idAttivita=" + selectedAppointment.get_id() , null);
                    oWindow.set_title("Modifica Attività");
                    oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                    oWindow.setSize(600, 450);
                    oWindow.center();
                    oWindow.add_close(OnClientClose);

                    eventArgs.set_cancel(true);
                }
                else
                {
                    if (evento == "RIFIUTA")
                    {
                        var oWindow = radopen("../Cantieri/RifiutaAttivita.aspx?idAttivita=" + selectedAppointment.get_id() , null);
                        oWindow.set_title("Ispezione non effettuabile");
                        oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
                        oWindow.setSize(300, 250);
                        oWindow.center();
                        oWindow.add_close(OnClientClose);

                        eventArgs.set_cancel(true);
                    }
                }
            }
        </script>
    </telerik:RadScriptBlock>

    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
        VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />

    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Calendario Attività" />
    <br />
    <b>
        Attività assegnate e non prese in carico
    </b>
    <br />
    <uc3:RicercaCantieriPerIspettori ID="RicercaCantieriPerIspettori1" 
        runat="server" />
    <br />
</asp:Content>

<asp:Content ID="Content5" runat="server" contentplaceholderid="MainPage2">
    <b>
        Attività pianificate
    </b>
    <asp:Label 
        ID="LabelErrore"
        runat="server"
        Text="Si è verificato un errore durante l'operazione."
        ForeColor="Red"
        Visible="false">
    </asp:Label>

    <telerik:RadScheduler ID="RadSchedulerAttivita" runat="server" 
        AppointmentStyleMode="Default" Culture="it-IT" FirstDayOfWeek="Monday" 
        LastDayOfWeek="Friday" SelectedView="WeekView" Width="100%"
        DataSubjectField="TipologiaAttivita" DataDescriptionField="DescrizioneAttivita"
        DataStartField="Data" DataEndField="DataFineAttivita" DataKeyField="Id" Height="600px"
        MinutesPerRow="60" OverflowBehavior="Expand" 
        onappointmentdatabound="RadSchedulerAttivita_AppointmentDataBound" 
        onappointmentdelete="RadSchedulerAttivita_AppointmentDelete" 
        onappointmentupdate="RadSchedulerAttivita_AppointmentUpdate" 
        OnClientTimeSlotClick="openRadWindowNuovaAttivita" 
        
        Localization-ConfirmDeleteText="Sei sicuro di voler eliminare quest'attività?" 
        Localization-ConfirmDeleteTitle="Conferma cancellazione" 
        Localization-ConfirmCancel="Annulla" Localization-Cancel="Elimina" 
        DayEndTime="19:00:00" DayStartTime="07:00:00" ShowFooter="False" 
        WorkDayEndTime="19:00:00" WorkDayStartTime="07:00:00" 
        onclientappointmentdoubleclick="openRadWindowModificaAttivita"
        onclientappointmentclick="openRadWindowModificaAttivita" 
        onappointmentcontextmenuitemclicked="RadSchedulerAttivita_AppointmentContextMenuItemClicked"
        
        OnClientAppointmentContextMenuItemClicking="appointmentContextMenuItemClicked" 
        HoursPanelTimeFormat="HH:mmtt" 
        onnavigationcomplete="RadSchedulerAttivita_NavigationComplete" 
        AllowEdit="False">
        <AdvancedForm Modal="true" />

        <Localization Cancel="Elimina" ConfirmDeleteTitle="Conferma cancellazione" 
            ConfirmDeleteText="Sei sicuro di voler eliminare quest&#39;attivit&#224;?" 
            ConfirmCancel="Annulla" ShowMore="altro..."></Localization>

        <TimelineView UserSelectable="false" />
        <WeekView DayEndTime="19:00:00" DayStartTime="07:00:00" />
        <AppointmentContextMenus>
            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenuIspezioniModificaStampa" 
                runat="server">
                <Items>
                    <telerik:RadMenuItem runat="server" Text="Rapporto di Ispezione">
                        <Items>
                            <telerik:RadMenuItem runat="server" Text="Modifica" Value="MODIFICA">
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem runat="server" Text="Stampa" Value="STAMPA">
                            </telerik:RadMenuItem>
                        </Items>
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Apri" Value="APRI">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Elimina" Enabled="false" Value="CommandDelete">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadSchedulerContextMenu>
            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenuIspezioniModifica" 
                runat="server">
                <Items>
                    <telerik:RadMenuItem runat="server" Text="Rapporto di Ispezione">
                        <Items>
                            <telerik:RadMenuItem runat="server" Text="Compila" Value="MODIFICA">
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem runat="server" Text="Non effettuabile" Value="RIFIUTA">
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem runat="server" Enabled="False" Text="Stampa" Value="STAMPA">
                            </telerik:RadMenuItem>
                        </Items>
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Apri" Value="APRI">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Elimina" Value="CommandDelete">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadSchedulerContextMenu>
            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenuIspezioniStampa" 
                runat="server">
                <Items>
                    <telerik:RadMenuItem runat="server" Text="Rapporto di Ispezione">
                        <Items>
                            <telerik:RadMenuItem runat="server" Enabled="false" Text="Modifica" Value="MODIFICA">
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem runat="server" Text="Stampa" Value="STAMPA">
                            </telerik:RadMenuItem>
                        </Items>
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Apri" Value="APRI">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Elimina" Enabled="false" Value="CommandDelete">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadSchedulerContextMenu>
            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenuIspezioniApriElimina" 
                runat="server">
                <Items>
                    <telerik:RadMenuItem runat="server" Text="Apri" Value="APRI">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Elimina" Value="CommandDelete">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadSchedulerContextMenu>
            <telerik:RadSchedulerContextMenu ID="RadSchedulerContextMenuIspezioni" 
                runat="server">
                <Items>
                    <telerik:RadMenuItem runat="server" Text="Rapporto di Ispezione" Enabled="false">
                        <Items>
                            <telerik:RadMenuItem runat="server" Enabled="false" Text="Modifica" Value="MODIFICA">
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem runat="server" Text="Stampa" Value="STAMPA" Enabled="false">
                            </telerik:RadMenuItem>
                        </Items>
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Apri" Value="APRI" Enabled="false">
                    </telerik:RadMenuItem>
                    <telerik:RadMenuItem runat="server" Text="Elimina" Value="CommandDelete" Enabled="false">
                    </telerik:RadMenuItem>
                </Items>
            </telerik:RadSchedulerContextMenu>
        </AppointmentContextMenus>
        <AppointmentTemplate>
            <asp:Panel
                ID="PanelAttivita"
                runat="server">
                <div class="rsAptSubject">
                    <%# Eval("Subject")%>
                </div>
                <div>
                    <%# Eval("Description")%>
                </div>
            </asp:Panel>
        </AppointmentTemplate>
    </telerik:RadScheduler>
    <br />
    <uc4:LegendaColoriAttivita ID="LegendaColoriAttivita1" runat="server" />
</asp:Content>



