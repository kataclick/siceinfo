﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Subappalti.aspx.cs" Inherits="Cantieri_Subappalti" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc6" %>
<%@ Register Src="~/WebControls/CantieriImpresa.ascx" TagName="CantieriImpresa" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriTestataIspezione.ascx" TagName="CantieriTestataIspezione"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte"
    TagPrefix="uc4" %>


<%@ Register src="WebControls/SubappaltiCantiere.ascx" tagname="SubappaltiCantiere" tagprefix="uc7" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Subappalti" />
    <br />
    <uc7:SubappaltiCantiere ID="SubappaltiCantiere1" runat="server" />
    <br />
    <asp:Button
        ID="ButtonSalvaAppalti"
        runat="server"
        Text="Salva situazione appalti"
        Width="150px" onclick="ButtonSalvaAppalti_Click" />
</asp:Content>


