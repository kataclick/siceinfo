<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriLocalizzazioneMultipla.aspx.cs" Inherits="CantieriLocalizzazioneMultipla" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.464044;
                var longitudine = 9.191567;
                var zoom = 10;

                function LoadMap() {
                    var latitudine = 45.464044;
                    var longitudine = 9.191567;
                    var zoom = 10;
                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }
            </script>
        </telerik:RadCodeBlock>
        <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Localizzazione" />
        <br />
        <table class="standardTable">
            <tr>
                <td>
                    Impresa appaltatrice
                </td>
                <td>
                    Committente
                </td>
                <td>
                    Subappaltatrice
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="TextBoxImpresa" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCommittente" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxSubappaltatrice" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <%--<asp:DropDownList ID="DropDownListZone" runat="server" AppendDataBoundItems="True"
                        Width="100%">
                    </asp:DropDownList>--%>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia
                </td>
                <td>
                    Comune
                </td>
                <td>
                    Indirizzo
                </td>
                <td>
                    Importo (�)&gt;<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                        ControlToValidate="TextBoxImporto" ErrorMessage="*" ValidationExpression="^[.][0-9]+$|[0-9]*[.]*[0-9]+$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="TextBoxProvincia" runat="server" Width="100%" MaxLength="2"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxComune" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="100%" MaxLength="255"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxImporto" runat="server" Width="100%" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="ButtonVisualizza" runat="server" Text="Visualizza" OnClick="ButtonVisualizza_Click" />
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Attenzione "></asp:Label>
        Impostare sempre un filtro di ricerca, altrimenti la localizzazione potrebbe richiedere
        parecchio tempo e la navigazione risultare rallentata<br />
        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="I cantieri da visualizzare sono troppi, filtrare maggiormemte"
            Visible="False"></asp:Label><br />
        <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Cantieri trovati" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="LabelCantieriSuMappa" runat="server" Text="Cantieri con coordinate" Visible="False"></asp:Label>
        <br />
        <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
        </div>
        <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
            <script type="text/javascript">
                LoadMap();
            </script>
        </telerik:RadCodeBlock>
        <br />
        <table>
            <tr>
                <td colspan="2">
                    <b>
                        Legenda
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="ImageNonIspezionato" runat="server" 
                        ImageUrl="~/images/cantiere24.gif" />
                </td>
                <td>
                    Cantiere non ispezionato
                </td>
                <td>
                    <asp:Image ID="ImageInVerifica" runat="server" 
                        ImageUrl="~/images/cantiere24standby.gif" />
                </td>
                <td>
                    Ispezione "In verifica"
                </td>
                <td>
                    <asp:Image ID="ImageConclusa" runat="server" 
                        ImageUrl="~/images/cantiere24attivo.gif" />
                </td>
                <td>
                    Ispezione "Conclusa"
                </td>
            </tr>
        </table>
        <br />
        <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="200px" Visible="true" /><br />
    </telerik:RadAjaxPanel>
</asp:Content>
