using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cantieri.Type.Filters;

public partial class Cantieri_CantieriSelezioneLettera : Page
{
    private readonly CantieriBusiness bz = new CantieriBusiness();
    private readonly CantieriBusiness cantieriBiz = new CantieriBusiness();
    private int idIspezione = -1;
    private TipologiaAppalto tipoAppalto;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriLettere);
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere))
        {
            PanelGeneraLettere.Enabled = true;
        }

        if (!Int32.TryParse(Request["idIspezione"], out idIspezione))
            Response.Redirect("~/DefaultErrore.aspx");

        if (!Page.IsPostBack)
        {
            TitoloSottotitolo1.sottoTitolo = string.Format("Ispezione {0}", idIspezione);
            LabelMessage.Text = string.Empty;
            GridViewSubappalti.Visible = false;
        }

        CaricaLettereGenerate();
        CaricaTipologiaAppalto();
    }

    private void CaricaLettereGenerate()
    {
        List<LogLettera> lettere = bz.CaricaLogLetterePerIspezione(idIspezione);

        LabelIrregolaritaRiscontrate.Text = String.Empty;
        LabelIrregolaritaRiscontrateAppuntamento.Text = String.Empty;
        LabelIrregolaritaAlComune.Text = String.Empty;
        LabelIrregolaritaAlComune01.Text = String.Empty;

        foreach (LogLettera lettera in lettere)
        {
            switch (lettera.TipoLettera)
            {
                case GruppoLettera.VerificaInCorso:
                    LabelVerificaInCorsoData.Text = lettera.Giorno.ToShortDateString();
                    LabelVerificaInCorsoProt.Text = lettera.Protocollo;
                    PanelVerificaInCorso.Visible = true;
                    break;
                case GruppoLettera.VerificaInCorsoComune:
                    LabelVerificaInCorsoComuneData.Text = lettera.Giorno.ToShortDateString();
                    LabelVerificaInCorsoComuneProt.Text = lettera.Protocollo;
                    PanelVerificaInCorsoComune.Visible = true;
                    break;
                case GruppoLettera.IrregolaritaRiscontrate:
                    if (lettera.TipoImpresa == TipologiaImpresa.SiceNew)
                        LabelIrregolaritaRiscontrate.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2} {3}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.IdImpresa,
                                          lettera.RagioneSocialeImpresa);
                    else
                        LabelIrregolaritaRiscontrate.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.RagioneSocialeImpresa);
                    PanelIrregolaritaRiscontrate.Visible = true;
                    break;
                case GruppoLettera.IrregolaritaRiscontrateConAppuntamento:
                    if (lettera.TipoImpresa == TipologiaImpresa.SiceNew)
                        LabelIrregolaritaRiscontrateAppuntamento.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2} {3}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.IdImpresa,
                                          lettera.RagioneSocialeImpresa);
                    else
                        LabelIrregolaritaRiscontrateAppuntamento.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.RagioneSocialeImpresa);
                    PanelIrregolaritaAppuntamento.Visible = true;
                    break;
                case GruppoLettera.EsitoPositivo:
                    LabelEsitoPositivoData.Text = lettera.Giorno.ToShortDateString();
                    LabelEsitoPositivoProt.Text = lettera.Protocollo;
                    PanelEsitoPositivo.Visible = true;
                    break;
                case GruppoLettera.EsitoPositivoConSubappaltatrici:
                    LabelEsitoPositivoSubData.Text = lettera.Giorno.ToShortDateString();
                    LabelEsitoPositivoSubProt.Text = lettera.Protocollo;
                    PanelEsitoPositivoSub.Visible = true;
                    break;
                case GruppoLettera.IrregolaritaRiscontrateAlCommittente:
                    if (lettera.IdImpresa.HasValue)
                    {
                        if (lettera.TipoImpresa == TipologiaImpresa.SiceNew)
                            LabelIrregolaritaAlCommittente.Text +=
                                String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2} {3}<br />", lettera.Protocollo,
                                              lettera.Giorno.ToShortDateString(), lettera.IdImpresa,
                                              lettera.RagioneSocialeImpresa);
                        else
                            LabelIrregolaritaAlCommittente.Text +=
                                String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2}<br />", lettera.Protocollo,
                                              lettera.Giorno.ToShortDateString(), lettera.RagioneSocialeImpresa);

                        LabelIrregolaritaAlCommittenteDataLab.Visible = false;
                        LabelIrregolaritaAlCommittenteProtLab.Visible = false;
                    }
                    else
                    {
                        LabelIrregolaritaAlCommittenteData.Text = lettera.Giorno.ToShortDateString();
                        LabelIrregolaritaAlCommittenteProt.Text = lettera.Protocollo;
                    }

                    PanelIrregolaritaAlCommittente.Visible = true;
                    break;
                case GruppoLettera.Sindacati:

                    if (lettera.TipoImpresa == TipologiaImpresa.SiceNew)
                        LabelSindacati.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2} {3}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.IdImpresa,
                                          lettera.RagioneSocialeImpresa);
                    else
                        LabelSindacati.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.RagioneSocialeImpresa);
                    PanelSindacati.Visible = true;
                    break;
                case GruppoLettera.BollinoBlu:
                    LabelBollinoBluData.Text = lettera.Giorno.ToShortDateString();
                    LabelBollinoBluProt.Text = lettera.Protocollo;
                    PanelBollinoBlu.Visible = true;
                    break;
                case GruppoLettera.IrregolaritaRiscontrateAlComune:
                    //LabelIrregolaritaAlComuneData.Text = lettera.Giorno.ToShortDateString();
                    //LabelIrregolaritaAlComuneProt.Text = lettera.Protocollo;
                    if (lettera.TipoImpresa == TipologiaImpresa.SiceNew)
                        LabelIrregolaritaAlComune.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2} {3}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.IdImpresa,
                                          lettera.RagioneSocialeImpresa);
                    else
                        LabelIrregolaritaAlComune.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.RagioneSocialeImpresa);
                    PanelIrregolaritaAlComune.Visible = true;
                    break;
                case GruppoLettera.IrregolaritaRiscontrateAlComune01:
                    //LabelIrregolaritaAlComuneData.Text = lettera.Giorno.ToShortDateString();
                    //LabelIrregolaritaAlComuneProt.Text = lettera.Protocollo;
                    if (lettera.TipoImpresa == TipologiaImpresa.SiceNew)
                        LabelIrregolaritaAlComune01.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2} {3}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.IdImpresa,
                                          lettera.RagioneSocialeImpresa);
                    else
                        LabelIrregolaritaAlComune01.Text +=
                            String.Format("Prot. <b>{0}</b> del <b>{1}</b>, {2}<br />", lettera.Protocollo,
                                          lettera.Giorno.ToShortDateString(), lettera.RagioneSocialeImpresa);
                    PanelIrregolaritaAlComune01.Visible = true;
                    break;
            }
        }
    }

    private void CaricaTipologiaAppalto()
    {
        tipoAppalto = cantieriBiz.CaricaTipologiaAppalto(idIspezione);
        LabelTipoAppalto.Text = tipoAppalto.ToString();
        if (tipoAppalto == TipologiaAppalto.NonDefinito)
        {
            buttonLetteraIrregolarita.Enabled = false;
            buttonLetteraIrregolaritaAppuntamento.Enabled = false;
            buttonLetteraIrregolaritaCommittente.Enabled = false;
        }
    }

    private void ResponseWord(string fileNameForClient, string fileWord)
    {
        Response.ClearContent();
        Response.AppendHeader("content-disposition", string.Format("attachment; filename={0}.docx", fileNameForClient));
        Response.ContentType = "application/vnd.ms-word";
        Response.WriteFile(fileWord);
        Response.Flush();
        Response.End();
    }

    protected void buttonLetteraVerifica_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            PanelSceltaImpresa.Visible = false;

            TipologiaLettera tipoLettera = TipologiaLettera.M70505;
            LetteraParam param = new LetteraParam();
            param.IdIspezione = idIspezione;
            param.Protocollo = textBoxProtocollo.Text;
            param.GruppoLettera = GruppoLettera.VerificaInCorso;
            string lettera = bz.GeneraLettera(tipoLettera, param);

            ResponseWord(param.Protocollo, lettera);
        }
    }

    protected void buttonLetteraVerificaComune_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            PanelSceltaImpresa.Visible = false;

            TipologiaLettera tipoLettera = TipologiaLettera.M7050502Comune;
            LetteraParam param = new LetteraParam();
            param.IdIspezione = idIspezione;
            param.Protocollo = textBoxProtocollo.Text;
            param.GruppoLettera = GruppoLettera.VerificaInCorsoComune;
            string lettera = bz.GeneraLettera(tipoLettera, param);

            ResponseWord(param.Protocollo, lettera);
        }
    }

    protected void buttonLetteraIrregolarita_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["gruppoLettera"] = GruppoLettera.IrregolaritaRiscontrate;

            if (tipoAppalto == TipologiaAppalto.Privato)
            {
                ViewState["tipoLettera"] = TipologiaLettera.M70506;
                CaricaSubappalti();
            }
            else if (tipoAppalto == TipologiaAppalto.Pubblico)
            {
                ViewState["tipoLettera"] = TipologiaLettera.M7050601;
                CaricaSubappalti();
            }
            else
            {
                LabelMessage.Text = "Specificare tipo di appalto";
            }
        }
    }

    protected void buttonLetteraIrregolaritaAppuntamento_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Page.Validate("conApp");
            if (Page.IsValid)
            {
                ViewState["gruppoLettera"] = GruppoLettera.IrregolaritaRiscontrateConAppuntamento;

                if (tipoAppalto == TipologiaAppalto.Privato)
                {
                    ViewState["tipoLettera"] = TipologiaLettera.M7050602;
                    CaricaSubappalti();
                }
                else if (tipoAppalto == TipologiaAppalto.Pubblico)
                {
                    ViewState["tipoLettera"] = TipologiaLettera.M7050603;
                    CaricaSubappalti();
                }
                else
                {
                    LabelMessage.Text = "Specificare tipo di appalto";
                }
            }
        }
    }

    protected void buttonLetteraPositivo_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            PanelSceltaImpresa.Visible = false;

            TipologiaLettera tipoLettera = TipologiaLettera.M70509;
            LetteraParam param = new LetteraParam();
            param.IdIspezione = idIspezione;
            param.Protocollo = textBoxProtocollo.Text;
            param.GruppoLettera = GruppoLettera.EsitoPositivo;
            string lettera = bz.GeneraLettera(tipoLettera, param);

            ResponseWord(param.Protocollo, lettera);
        }
    }

    protected void buttonLetteraPositivo01_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            PanelSceltaImpresa.Visible = false;

            TipologiaLettera tipoLettera = TipologiaLettera.M7050901;
            LetteraParam param = new LetteraParam();
            param.IdIspezione = idIspezione;
            param.Protocollo = textBoxProtocollo.Text;
            param.GruppoLettera = GruppoLettera.EsitoPositivoConSubappaltatrici;
            string lettera = bz.GeneraLettera(tipoLettera, param);

            ResponseWord(param.Protocollo, lettera);
        }
    }

    protected void buttonLetteraIrregolaritaCommittente_Click(object sender, EventArgs e)
    {
        //if (Page.IsValid)
        //{
        //    PanelSceltaImpresa.Visible = false;

        //    if (tipoAppalto == TipologiaAppalto.Privato)
        //    {
        //        TipologiaLettera tipoLettera = TipologiaLettera.M70510;
        //        LetteraParam param = new LetteraParam();
        //        param.IdIspezione = idIspezione;
        //        param.Protocollo = textBoxProtocollo.Text;
        //        param.GruppoLettera = GruppoLettera.IrregolaritaRiscontrateAlCommittente;
        //        string lettera = bz.GeneraLettera(tipoLettera, param);

        //        ResponseWord(param.Protocollo, lettera);
        //    }
        //    else if (tipoAppalto == TipologiaAppalto.Pubblico)
        //    {
        //        TipologiaLettera tipoLettera = TipologiaLettera.M70511;
        //        LetteraParam param = new LetteraParam();
        //        param.IdIspezione = idIspezione;
        //        param.Protocollo = textBoxProtocollo.Text;
        //        param.GruppoLettera = GruppoLettera.IrregolaritaRiscontrateAlCommittente;
        //        string lettera = bz.GeneraLettera(tipoLettera, param);

        //        ResponseWord(param.Protocollo, lettera);
        //    }
        //    else
        //    {
        //        LabelMessage.Text = "Specificare tipo di appalto";
        //    }
        //}
        if (Page.IsValid)
        {
            ViewState["gruppoLettera"] = GruppoLettera.IrregolaritaRiscontrateAlCommittente;

            if (tipoAppalto == TipologiaAppalto.Privato)
            {
                ViewState["tipoLettera"] = TipologiaLettera.M70510;
                CaricaSubappalti();
            }
            else if (tipoAppalto == TipologiaAppalto.Pubblico)
            {
                ViewState["tipoLettera"] = TipologiaLettera.M70511;
                CaricaSubappalti();
            }
            else
            {
                LabelMessage.Text = "Specificare tipo di appalto";
            }
        }
    }

    protected void buttonLetteraIrregolaritaComune_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["gruppoLettera"] = GruppoLettera.IrregolaritaRiscontrateAlComune;
            ViewState["tipoLettera"] = TipologiaLettera.M70511Comune;

            CaricaSubappalti();
        }

        //if (Page.IsValid)
        //{
        //    PanelSceltaImpresa.Visible = false;

        //    TipologiaLettera tipoLettera = TipologiaLettera.M70511Comune;
        //    LetteraParam param = new LetteraParam();
        //    param.IdIspezione = idIspezione;
        //    param.Protocollo = textBoxProtocollo.Text;
        //    param.GruppoLettera = GruppoLettera.IrregolaritaRiscontrateAlComune;
        //    string lettera = bz.GeneraLettera(tipoLettera, param);

        //    ResponseWord(param.Protocollo, lettera);
        //}
    }

    protected void buttonLetteraIrregolaritaComune01_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["gruppoLettera"] = GruppoLettera.IrregolaritaRiscontrateAlComune01;
            ViewState["tipoLettera"] = TipologiaLettera.M7051101Comune;

            CaricaSubappalti();
        }
    }

    protected void buttonLetteraSindacati_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["gruppoLettera"] = GruppoLettera.Sindacati;

            ViewState["tipoLettera"] = TipologiaLettera.M70501;
            CaricaSubappalti();
        }

        //if (Page.IsValid)
        //{
        //    PanelSceltaImpresa.Visible = false;

        //    TipologiaLettera tipoLettera = TipologiaLettera.M70501;
        //    LetteraParam param = new LetteraParam();
        //    param.IdIspezione = idIspezione;
        //    param.Protocollo = textBoxProtocollo.Text;
        //    param.GruppoLettera = GruppoLettera.Sindacati;
        //    string lettera = bz.GeneraLettera(tipoLettera, param);

        //    ResponseWord(param.Protocollo, lettera);
        //}
    }

    protected void buttonLetteraBollinoBlu_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            PanelSceltaImpresa.Visible = false;

            TipologiaLettera tipoLettera = TipologiaLettera.M7050501;
            LetteraParam param = new LetteraParam();
            param.IdIspezione = idIspezione;
            param.Protocollo = textBoxProtocollo.Text;
            param.GruppoLettera = GruppoLettera.BollinoBlu;
            string lettera = bz.GeneraLettera(tipoLettera, param);

            ResponseWord(param.Protocollo, lettera);
        }
    }

    protected void GridViewSubappalti_RowEditing(object sender, GridViewEditEventArgs e)
    {
        LetteraParam param = new LetteraParam();
        param.IdIspezione = idIspezione;
        param.Protocollo = textBoxProtocollo.Text;

        TipologiaImpresa tipoImpresa =
            (TipologiaImpresa) GridViewSubappalti.DataKeys[e.NewEditIndex].Values["TipoImpresa"];

        if (tipoImpresa == TipologiaImpresa.Cantieri)
            param.IdCantieriImpresa = (int?) GridViewSubappalti.DataKeys[e.NewEditIndex].Values["IdImpresa"];
        else
            param.IdImpresa = (int?) GridViewSubappalti.DataKeys[e.NewEditIndex].Values["IdImpresa"];

        TipologiaLettera tipoLettera = (TipologiaLettera) ViewState["tipoLettera"];
        param.GruppoLettera = (GruppoLettera) ViewState["gruppoLettera"];

        if (tipoLettera == TipologiaLettera.M7050602 || tipoLettera == TipologiaLettera.M7050603)
            param.DataAppuntamento = DateTime.Parse(textBoxDataAppuntamento.Text);

        string lettera = bz.GeneraLettera(tipoLettera, param);

        ResponseWord(param.Protocollo, lettera);
    }

    private void CaricaSubappalti()
    {
        PanelSceltaImpresa.Visible = true;
        List<SubappaltoLettera> sub = bz.CaricaSubappaltiLettera(idIspezione);

        GridViewSubappalti.Visible = true;
        GridViewSubappalti.DataSource = sub;
        GridViewSubappalti.DataBind();
    }

    protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoLettera sub = (SubappaltoLettera) e.Row.DataItem;

            Label lCodice = (Label) e.Row.FindControl("LabelCodice");
            Label lRagioneSociale = (Label) e.Row.FindControl("LabelRagioneSociale");

            if (sub.IdImpresaSubappaltata.HasValue)
            {
                lCodice.Text = sub.IdImpresaSubappaltata.ToString();
                lRagioneSociale.Text = sub.RagioneSocialeImp;
            }
            else
            {
                lRagioneSociale.Text = sub.RagioneSocialeCantieriImp;
            }
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["modalita"] == "ricercaIspezioni")
        {
            string indirizzo = "~/Cantieri/CantieriRicercaIspezioni.aspx?modalita=indietro";

            if (Request.QueryString["dal"] != null)
                indirizzo = String.Format("{0}&dal={1}", indirizzo, Request.QueryString["dal"]);
            if (Request.QueryString["al"] != null)
                indirizzo = String.Format("{0}&al={1}", indirizzo, Request.QueryString["al"]);
            if (Request.QueryString["ispettore"] != null)
                indirizzo = String.Format("{0}&ispettore={1}", indirizzo, Request.QueryString["ispettore"]);
            if (Request.QueryString["stato"] != null)
                indirizzo = String.Format("{0}&stato={1}", indirizzo, Request.QueryString["stato"]);
            if (Request.QueryString["detAppalto"] != null)
                indirizzo = String.Format("{0}&detAppalto={1}", indirizzo, Request.QueryString["detAppalto"]);
            if (Request.QueryString["subApp"] != null)
                indirizzo = String.Format("{0}&subApp={1}", indirizzo, Request.QueryString["subApp"]);
            indirizzo = String.Format("{0}&pagina={1}", indirizzo, Request.QueryString["pagina"]);

            Response.Redirect(indirizzo);
        }
    }

    protected void CustomValidatorProtocolloUtilizzato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!String.IsNullOrEmpty(textBoxProtocollo.Text))
        {
            LettereFilter filtro = new LettereFilter();
            filtro.Protocollo = textBoxProtocollo.Text;

            List<LogLettera> lettere = cantieriBiz.CaricaLogLettere(filtro);
            if (lettere != null && lettere.Count > 0 && ViewState["Avvertito"] == null)
            {
                args.IsValid = false;
                ViewState["Avvertito"] = true;
            }
            else
            {
                ViewState["Avvertito"] = null;
            }
        }
    }
}