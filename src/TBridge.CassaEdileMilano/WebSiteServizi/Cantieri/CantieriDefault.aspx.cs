using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriGestione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneGestione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);
        funzionalita.Add(FunzionalitaPredefinite.CantieriGestioneZone);
        funzionalita.Add(FunzionalitaPredefinite.CantieriModifica);
        funzionalita.Add(FunzionalitaPredefinite.CantieriModificaIspettori);
        funzionalita.Add(FunzionalitaPredefinite.CantieriStatisticheGenerali);
        funzionalita.Add(FunzionalitaPredefinite.CantieriStatistichePerIspettore);
        funzionalita.Add(FunzionalitaPredefinite.CantieriLettere);
        funzionalita.Add(FunzionalitaPredefinite.CantieriGestioneGruppi);
        funzionalita.Add(FunzionalitaPredefinite.CantieriSegnalazione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriAssegnazione);
        funzionalita.Add(FunzionalitaPredefinite.CantieriEstrazioneLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}