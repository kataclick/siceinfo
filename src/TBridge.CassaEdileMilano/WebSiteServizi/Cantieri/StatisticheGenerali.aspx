<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StatisticheGenerali.aspx.cs" Inherits="Cantieri_StatisticheGenerali" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server"></uc3:MenuCantieriStatistiche>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Statistiche generali"
        titolo="Cantieri" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Tipologia report:
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonIspezioni" runat="server" GroupName="tipoReport"
                                Text="Riepilogo ispezioni" Checked="True" AutoPostBack="True" OnCheckedChanged="RadioButtonIspezioni_CheckedChanged" />
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownStatoIspezione" runat="server" Width="200px">
                                <asp:ListItem Value="2">Concluse</asp:ListItem>
                                <asp:ListItem Value="1">In verifica</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonStatistici" runat="server" GroupName="tipoReport"
                                Text="Dati statistici" AutoPostBack="True" OnCheckedChanged="RadioButtonStatistici_CheckedChanged" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonStatisticiPerIspezione" runat="server" GroupName="tipoReport"
                                Text="Dati statistici per ispezione" AutoPostBack="True" OnCheckedChanged="RadioButtonStatisticiPerIspezione_CheckedChanged" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonStatisticiAggregatiMese" runat="server" GroupName="tipoReport"
                                Text="Dati statistici aggregati per mese" AutoPostBack="True" OnCheckedChanged="RadioButtonStatisticiAggregatiMese_CheckedChanged" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonStatisticiIspezioniNonDefinite" runat="server" GroupName="tipoReport"
                                Text="Ispezioni non definite" AutoPostBack="True" OnCheckedChanged="RadioButtonStatisticiIspezioniNonDefinite_CheckedChanged" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                Tipo Segnalazione:
            </td>
            <td>
                <asp:DropDownList
                    ID="DropDownListTipoSegnalazione"
                    runat="server"
                    Width="300px"
                    AppendDataBoundItems="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                Periodo:
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonMensile" runat="server" GroupName="periodo" Text="Mensile"
                                Checked="true" AutoPostBack="True" OnCheckedChanged="RadioButtonMensile_CheckedChanged" />
                        </td>
                        <td>
                            <asp:Panel ID="PanelMensile" runat="server">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Mese (mm/aaaa):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxMeseMensile" runat="server" MaxLength="7" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMeseMensile" runat="server"
                                                ControlToValidate="TextBoxMeseMensile" ErrorMessage="Scegliere un mese" ValidationGroup="visualizza"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMeseMensile" runat="server"
                                                ErrorMessage="Formato data errato" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="visualizza"
                                                ControlToValidate="TextBoxMeseMensile"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonAnnuale" runat="server" GroupName="periodo" Text="Annuale"
                                Enabled="False" AutoPostBack="True" OnCheckedChanged="RadioButtonAnnuale_CheckedChanged" />
                        </td>
                        <td>
                            <asp:Panel ID="PanelAnnuale" runat="server" Enabled="false">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Fino a (mm/aaaa):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxMeseAnnuale" runat="server" MaxLength="7" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMeseAnnuale" runat="server"
                                                ControlToValidate="TextBoxMeseAnnuale" ErrorMessage="Scegliere un mese" ValidationGroup="visualizza"
                                                Enabled="False"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMeseAnnuale" runat="server"
                                                ErrorMessage="Formato data errato" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="visualizza"
                                                ControlToValidate="TextBoxMeseAnnuale"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonPeriodo" runat="server" GroupName="periodo" Text="Periodo"
                                Enabled="False" AutoPostBack="True" OnCheckedChanged="RadioButtonPeriodo_CheckedChanged" />
                        </td>
                        <td>
                            <asp:Panel ID="PanelPeriodo" runat="server" Enabled="false">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Dal (mm/aaaa):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxMesePeriodoDal" runat="server" MaxLength="7" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMesePeriodoDal" runat="server"
                                                ControlToValidate="TextBoxMesePeriodoDal" ErrorMessage="Scegliere un mese" ValidationGroup="visualizza"
                                                Enabled="False"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMesePeriodoDal" runat="server"
                                                ErrorMessage="Formato data errato" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="visualizza"
                                                ControlToValidate="TextBoxMesePeriodoDal"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Al (mm/aaaa):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxMesePeriodoAl" runat="server" MaxLength="7" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMesePeriodoAl" runat="server"
                                                ControlToValidate="TextBoxMesePeriodoAl" ErrorMessage="Scegliere un mese" ValidationGroup="visualizza"
                                                Enabled="False"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorMesePeriodoAl" runat="server"
                                                ErrorMessage="Formato data errato" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="visualizza"
                                                ControlToValidate="TextBoxMesePeriodoAl"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                Tipo Appalto:
            </td>
            <td>
                <asp:DropDownList ID="DropDownListTipoAppalto" runat="server" Width="300px" AppendDataBoundItems="True">
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
                <asp:Button ID="ButtonVisualizza" runat="server" Text="Genera PDF" OnClick="ButtonVisualizza_Click"
                    ValidationGroup="visualizza" Width="200px" />
                &nbsp;<asp:Button ID="ButtonVisualizzaExcel" runat="server" Text="Genera Excel" 
                    ValidationGroup="visualizza" Width="200px" 
                    onclick="ButtonVisualizzaExcel_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <rsweb:ReportViewer ID="ReportViewerAttivita" runat="server" ProcessingMode="Remote"
                    Width="100%" Height="100px" ShowParameterPrompts="False" Visible="false">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
