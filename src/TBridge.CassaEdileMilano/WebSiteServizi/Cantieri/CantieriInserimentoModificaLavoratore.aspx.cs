using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriInserimentoModificaLavoratore : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private bool aggiornamento;

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CantieriPreventivoSettimanale.aspx");


        if (!Page.IsPostBack)
        {
            string modalita = Request.QueryString["modalita"];
            if (modalita == null || modalita != "modifica")
            {
                ModalitaInserimento();
            }
            else
            {
                string idLavoratoreS = Request.QueryString["idLavoratore"];
                int idLavoratore;

                if (Int32.TryParse(idLavoratoreS, out idLavoratore))
                {
                    ModalitaAggiornamento();
                    CaricaLavoratore(idLavoratore);
                }
                else
                    ModalitaInserimento();
            }
        }
    }

    private void ModalitaAggiornamento()
    {
        TitoloSottotitolo1.sottoTitolo = "Modifica lavoratore";
        ButtonOperazione.Text = "Aggiorna";
        aggiornamento = true;
    }

    private void ModalitaInserimento()
    {
        TitoloSottotitolo1.sottoTitolo = "Inserimento nuovo lavoratore";
        ButtonOperazione.Text = "Inserisci";
        aggiornamento = false;
    }

    private void CaricaLavoratore(int idLavoratore)
    {
        LavoratoreCollection listaLavoratori =
            biz.GetLavoratoriOrdinati(TipologiaLavoratore.Cantieri, idLavoratore, null, null, null, null, null);
        if (listaLavoratori.Count == 1)
        {
            Lavoratore lavoratore = listaLavoratori[0];

            TextBoxCognome.Text = lavoratore.Cognome;
            TextBoxNome.Text = lavoratore.Nome;
            TextBoxDataNascita.Text = lavoratore.DataNascita.Value.ToShortDateString();
        }
    }

    protected void ButtonOperazione_Click(object sender, EventArgs e)
    {
        if (ControlloCampiServer())
        {
            Lavoratore lavoratore = CreaLavoratore();
            bool res = false;
            ;

            if (Request.QueryString["modalita"] == "modifica")
                aggiornamento = true;

            // Eseguo l'operazione
            if (aggiornamento)
            {
                int idLavoratore;
                Int32.TryParse(TextBoxIdLavoratore.Text, out idLavoratore);
                lavoratore.IdLavoratore = idLavoratore;

                // Aggiorno il record
                res = biz.UpdateLavoratore(lavoratore);
            }
            else
            {
                // Inserisco il record
                biz.InsertLavoratore(lavoratore);
                if (lavoratore.IdLavoratore > 0)
                {
                    res = true;
                    // Ritorno alla pagina di insertmodificacantieri
                }
            }

            // Gestisco il risultato dell'operazione
            if (res)
            {
                // Tutto ok
                if (aggiornamento)
                    LabelRisultato.Text = "Aggiornamento effettuato correttamente";
                else
                    LabelRisultato.Text = "Inserimento effettuato correttamente";
            }
            else
            {
                LabelRisultato.Text = "Si � verificato un errore durante l'operazione";
            }
        }
    }

    private Lavoratore CreaLavoratore()
    {
        // Creazione oggetto Lavoratore
        string cognome;
        string nome;
        DateTime dataNascita;

        cognome = TextBoxCognome.Text;
        nome = TextBoxNome.Text;
        dataNascita = DateTime.Parse(TextBoxDataNascita.Text);

        Lavoratore lavoratore = new Lavoratore(TipologiaLavoratore.Cantieri, null, cognome, nome, dataNascita);

        return lavoratore;
    }

    /// <summary>
    /// Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxCognome.Text))
        {
            res = false;
            errori.Append("Campo cognome vuoto" + Environment.NewLine);
        }

        if (string.IsNullOrEmpty(TextBoxNome.Text))
        {
            res = false;
            errori.Append("Campo nome vuoto" + Environment.NewLine);
        }

        if (string.IsNullOrEmpty(TextBoxDataNascita.Text))
        {
            res = false;
            errori.Append("Campo data di nascita vuoto" + Environment.NewLine);
        }

        DateTime dataNascita;
        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            !DateTime.TryParse(TextBoxDataNascita.Text, out dataNascita))
        {
            res = false;
            errori.Append("Formato data nascita errato" + Environment.NewLine);
        }

        if (!res)
            LabelRisultato.Text = errori.ToString();
        return res;
    }
}