using System;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using System.Collections.Generic;

public partial class CantieriConfermaEliminazioneCantiere : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione,
                                              "CantieriConfermaEliminazioneCantiere.aspx");

        if (Request.QueryString["idCantiere"] != null)
        {
            int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
            TBridge.Cemi.Cantieri.Type.Entities.Cantiere cantiere = biz.GetCantiere(idCantiere);

            AttivitaCollection attivita = biz.GetProgrammazionePerCantiere(idCantiere);

            Presenter.CaricaElementiInGridView(
                GridViewAttivita,
                attivita,
                0);

            if (cantiere.PresaInCarico != null)
            {
                List<CantieriCalendarioAttivita> nuoveAttivita = new List<CantieriCalendarioAttivita>();
                nuoveAttivita.Add(cantiere.PresaInCarico);

                Presenter.CaricaElementiInGridView(
                    GridViewAttivitaNuove,
                    nuoveAttivita,
                    0);

                if (nuoveAttivita != null && nuoveAttivita.Count > 0)
                {
                    ButtonCancella.Enabled = false;
                }
            }

            if (attivita != null && attivita.Count > 0)
            {
                ButtonCancella.Enabled = false;
            }
        }
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cantieri/CantieriGestioneCantieri.aspx");
    }

    protected void ButtonCancella_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["idCantiere"] != null)
        {
            int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);

            if (biz.DeleteCantiere(idCantiere))
                Response.Redirect("~/Cantieri/CantieriGestioneCantieri.aspx");
            else
                LabelErrore.Text = "Errore durante la cancellazione";
        }
    }
}