﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Presenter;
using Ispettore = TBridge.Cemi.Type.Domain.Ispettore;
using Telerik.Web.UI;
using System.Text;

public partial class Cantieri_AssegnaCantiere : System.Web.UI.Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();

    protected List<Ispettore> Ispettori
    {
        set
        {
            ViewState["Ispettori"] = value;
        }
        get
        {
            if (ViewState["Ispettori"] == null)
            {
                ViewState["Ispettori"] = new List<Ispettore>();
            }

            return (List<Ispettore>)ViewState["Ispettori"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriAssegnazione);
        #endregion

        if (!Page.IsPostBack)
        {
            CaricaMotivazioni();
            CaricaPriorita();
            CaricaGruppiIspezione();
            CaricaSingoliIspettori();
            CaricaIspettori();

            if (Request.QueryString["idCantiere"] != null)
            {
                String idCantiereString = Request.QueryString["idCantiere"];
                String[] cantieriString = idCantiereString.Split(',');
                List<Int32> cantieri = new List<int>();

                for (Int32 i = 0; i < cantieriString.Length; i++)
                {
                    cantieri.Add(Int32.Parse(cantieriString[i]));
                }
                Boolean modifica = Request.QueryString["modalita"] != null;

                CaricaDatiCantiere(cantieri, modifica);
            }
            else
            {
                ButtonAssegna.Enabled = false;
            }
        }
    }

    private void CaricaSingoliIspettori()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxIspettori,
            bizEF.GetIspettori(),
            "NomeCompleto",
            "IdIspettore");
    }

    private void CaricaIspettori()
    {
        Presenter.CaricaElementiInGridView(
            RadGridIspettori,
            this.Ispettori);
    }

    private void CaricaGruppiIspezione()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxGruppi,
            bizEF.GetGruppiIspezione(),
            "Descrizione",
            "IdCantieriGruppoIspezione");
    }

    private void CaricaPriorita()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxPriorita,
            bizEF.GetAssegnazionePriorita(),
            "Descrizione",
            "Id");
    }

    private void CaricaMotivazioni()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxMotivazione,
            bizEF.GetAssegnazioneMotivazioni(),
            "Descrizione",
            "Id");
    }

    private void CaricaDatiCantiere(List<Int32> idCantieri, Boolean modifica)
    {
        ViewState["IdCantiere"] = idCantieri;

        List<Cantiere> cantieri = new List<Cantiere>();

        foreach (Int32 cant in idCantieri)
        {
            Cantiere cantiere = biz.GetCantiere(cant);
            cantieri.Add(cantiere);
        }
        RepeaterCantieri.DataSource = cantieri;
        RepeaterCantieri.DataBind();

        //LabelIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
        //if (!String.IsNullOrEmpty(cantiere.Cap))
        //{
        //    LabelCap.Text = String.Format("{0} ", cantiere.Cap);
        //}
        //LabelComune.Text = cantiere.Comune;
        //LabelProvincia.Text = cantiere.Provincia;

        if (modifica)
        {
            CaricaAssegnazione(cantieri[0]);
        }
        {
            LabelData.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        // Se non esiste una segnalazione per il cantiere non permetto il rifiuto
        if (cantieri.Count > 1 || cantieri[0].Segnalazione == null)
        {
            RadioButtonRifiuta.Enabled = false;
            RadioButtonAssegna.Checked = true;
            Assegna();
            trRifiuta1.Visible = false;
            trRifiuta2.Visible = false;
            trRifiuta3.Visible = false;
        }
    }

    private void CaricaAssegnazione(Cantiere cantiere)
    {
        ButtonElimina.Visible = true;

        ViewState["IdAssegnazione"] = cantiere.Assegnazione.IdAssegnazione;

        LabelData.Text = cantiere.Assegnazione.Data.ToString("dd/MM/yyyy");
        if (cantiere.Assegnazione.MotivazioneRifiuto == null)
        {
            // Assegnato
            Assegna();
            RadioButtonAssegna.Checked = true;
            RadioButtonRifiuta.Checked = false;
            RadComboBoxPriorita.SelectedValue = cantiere.Assegnazione.Priorita.Id.ToString();

            TBridge.Cemi.Type.Domain.CantieriAssegnazione asse =
                bizEF.GetAssegnazione(cantiere.Assegnazione.IdAssegnazione);
            this.Ispettori.AddRange(asse.Ispettori);
            CaricaIspettori();
        }
        else
        {
            // Rifiutato
            Rifiuta();
            RadioButtonAssegna.Checked = false;
            RadioButtonRifiuta.Checked = true;
            RadComboBoxMotivazione.SelectedValue = cantiere.Assegnazione.MotivazioneRifiuto.Id.ToString();
        }
        
        ButtonAssegna.Text = "Modifica l'assegnazione";

        // Se il cantiere è stato preso in carico non permetto la modifica dell'assegnazione
        if (cantiere.PresaInCarico != null)
        {
            RadioButtonAssegna.Enabled = false;
            RadioButtonRifiuta.Enabled = false;
            RadComboBoxMotivazione.Enabled = false;
            RadComboBoxPriorita.Enabled = false;
            RadComboBoxGruppi.Enabled = false;
            RadComboBoxIspettori.Enabled = false;
            ButtonAggiungiALista.Enabled = false;
            ButtonAggiungiSingoloALista.Enabled = false;
            ButtonAssegna.Visible = false;
            ButtonElimina.Visible = false;

            foreach (GridDataItem item in RadGridIspettori.Items)
            {
                ImageButton ibElimina = (ImageButton)item.FindControl("ImageButtonElimina");
                ibElimina.ImageUrl = "~/images/pallinoXBN.png";
                ibElimina.Enabled = false;
            }
        }
    }

    private void Assegna()
    {
        PanelAssegna.Enabled = true;
        PanelRifiuta.Enabled = false;
    }

    private void Rifiuta()
    {
        PanelAssegna.Enabled = false;
        PanelRifiuta.Enabled = true;
    }

    protected void RadioButtonAssegna_CheckedChanged(object sender, EventArgs e)
    {
        Reset();

        if (RadioButtonAssegna.Checked)
        {
            Assegna();
        }
        else
        {
            Rifiuta();
        }
    }

    private void Reset()
    {
        RadComboBoxMotivazione.ClearSelection();
        RadComboBoxPriorita.ClearSelection();
        RadComboBoxGruppi.ClearSelection();
        this.Ispettori = new List<Ispettore>();
        CaricaIspettori();
    }

    protected void ButtonAssegna_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            StringBuilder errori = new StringBuilder();

            foreach(RepeaterItem ri in RepeaterCantieri.Items)
            {
                Label lIdCantiere = (Label) ri.FindControl("LabelIdCantiere");
                Int32 idCantiere = Int32.Parse(lIdCantiere.Text);

                Assegnazione assegnazione = CreaAssegnazione(idCantiere);

                if (assegnazione.IdAssegnazione <= 0)
                {
                    if (biz.InsertAssegnazione(assegnazione))
                    {
                        //ConfermaInserimento();
                    }
                    else
                    {
                        //ErroreNellInserimento();
                        errori.AppendLine(String.Format("Errore durante l'inserimento dell'assegnazione #{0}", ri.ItemIndex));
                    }
                }
                else
                {
                    try
                    {
                        bizEF.UpdateAssegnazione(assegnazione);
                        //ConfermaInserimento();
                    }
                    catch
                    {
                        //ErroreNellInserimento();
                        errori.AppendLine(String.Format("Errore durante l'aggiornamento dell'assegnazione #{0}", ri.ItemIndex));
                    }
                }
            }

            if (errori.Length > 0)
            {
                ErroreNellInserimento(errori.ToString());
            }
            else
            {
                ConfermaInserimento();
            }
        }
    }

    private void ErroreNellInserimento(String errori)
    {
        LabelMessaggio.Text = errori;
    }

    private void ConfermaInserimento()
    {
        RadioButtonAssegna.Enabled = false;
        RadioButtonRifiuta.Enabled = false;
        RadComboBoxGruppi.Enabled = false;
        ButtonAggiungiALista.Enabled = false;
        RadComboBoxMotivazione.Enabled = false;
        RadComboBoxPriorita.Enabled = false;
        ButtonAssegna.Enabled = false;
        RadComboBoxIspettori.Enabled = false;
        ButtonAggiungiSingoloALista.Enabled = false;

        foreach (GridDataItem item in RadGridIspettori.Items)
        {
            ImageButton ibElimina = (ImageButton)item.FindControl("ImageButtonElimina");
            ibElimina.ImageUrl = "~/images/pallinoXBN.png";
            ibElimina.Enabled = false;
        }

        LabelMessaggio.Text = "Assegnazione effettuata correttamente.";
    }

    private Assegnazione CreaAssegnazione(Int32 idCantiere)
    {
        Assegnazione assegnazione = new Assegnazione();

        if (ViewState["IdAssegnazione"] != null)
        {
            assegnazione.IdAssegnazione = (Int32)ViewState["IdAssegnazione"];
        }
        else
        {
            assegnazione.IdAssegnazione = -1;
        }
        //assegnazione.IdCantiere = (Int32)ViewState["IdCantiere"];
        assegnazione.IdCantiere = idCantiere;
        assegnazione.Data = DateTime.Now;
        if (!String.IsNullOrEmpty(RadComboBoxMotivazione.SelectedValue))
        {
            assegnazione.MotivazioneRifiuto = new TBridge.Cemi.Type.Domain.CantieriAssegnazioneMotivazione();
            assegnazione.MotivazioneRifiuto.Id = Int32.Parse(RadComboBoxMotivazione.SelectedItem.Value);
            assegnazione.MotivazioneRifiuto.Descrizione = RadComboBoxMotivazione.SelectedItem.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxPriorita.SelectedValue))
        {
            assegnazione.Priorita = new TBridge.Cemi.Type.Domain.CantieriAssegnazionePriorita();
            assegnazione.Priorita.Id = Int32.Parse(RadComboBoxPriorita.SelectedItem.Value);
            assegnazione.Priorita.Descrizione = RadComboBoxPriorita.SelectedItem.Text;
        }
        assegnazione.Ispettori = this.Ispettori;

        return assegnazione;
    }

    #region Custom Validators
    protected void CustomValidatorPriorita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadioButtonAssegna.Checked && String.IsNullOrEmpty(RadComboBoxPriorita.SelectedValue))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorMotivazione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadioButtonRifiuta.Checked && String.IsNullOrEmpty(RadComboBoxMotivazione.SelectedValue))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorIspettori_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadioButtonAssegna.Checked && this.Ispettori.Count == 0)
        {
            args.IsValid = false;
        }
    }
    #endregion

    protected void ButtonAggiungiALista_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(RadComboBoxGruppi.SelectedValue))
        {
            Int32 idGruppo = Int32.Parse(RadComboBoxGruppi.SelectedValue);
            List<Ispettore> ispettori = bizEF.GetIspettoriGruppo(idGruppo);

            AggiungiIspettoriALista(ispettori);
            RadComboBoxGruppi.ClearSelection();
            CaricaIspettori();
        }
    }

    private void AggiungiIspettoriALista(List<Ispettore> ispettori)
    {
        List<Ispettore> listaIspettori = this.Ispettori;

        foreach (Ispettore ispettore in ispettori)
        {
            Boolean trovato = false;

            foreach (Ispettore ispettoreLista in listaIspettori)
            {
                if (ispettore.IdIspettore == ispettoreLista.IdIspettore)
                {
                    trovato = true;
                    break;
                }
            }

            if (!trovato)
            {
                listaIspettori.Add(ispettore);
            }
        }

        this.Ispettori = listaIspettori;
    }

    protected void ButtonAggiungiSingoloALista_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(RadComboBoxIspettori.SelectedValue))
        {
            Int32 idIspettore = Int32.Parse(RadComboBoxIspettori.SelectedValue);
            List<Ispettore> ispettori = new List<Ispettore>();
            ispettori.Add(bizEF.GetIspettore(idIspettore));

            AggiungiIspettoriALista(ispettori);
            RadComboBoxIspettori.ClearSelection();
            CaricaIspettori();
        }
    }

    protected void ButtonElimina_Click(object sender, EventArgs e)
    {
        try
        {
            Int32 idAssegnazione = (Int32)ViewState["IdAssegnazione"];

            bizEF.DeleteAssegnazione(idAssegnazione);
            ConfermaInserimento();
        }
        catch
        {
            ErroreNellInserimento("Errore nella cancellazione dell'assegnazione");
        }
    }

    protected void RadGridIspettori_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "ELIMINA":
                this.Ispettori.RemoveAt(e.Item.ItemIndex);
                CaricaIspettori();
                break;
        }
    }

    protected void CustomValidatorAzione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!RadioButtonAssegna.Checked && !RadioButtonRifiuta.Checked)
        {
            args.IsValid = false;
        }
    }

    protected void RepeaterCantieri_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Cantiere cantiere = (Cantiere) e.Item.DataItem;

            Label lIdCantiere = (Label) e.Item.FindControl("LabelIdCantiere");
            Label lIndirizzo = (Label) e.Item.FindControl("LabelIndirizzo");
            Label lCap = (Label) e.Item.FindControl("LabelCap");
            Label lComune = (Label) e.Item.FindControl("LabelComune");
            Label lProvincia = (Label) e.Item.FindControl("LabelProvincia");

            lIdCantiere.Text = cantiere.IdCantiere.ToString();
            lIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
            lCap.Text = cantiere.Cap;
            lComune.Text = cantiere.Comune;
            lProvincia.Text = cantiere.Provincia;
        }
    }
}