﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NumeriAttivita.aspx.cs" Inherits="Cantieri_NumeriAttivita" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="../WebControls/MenuCantieriStatistiche.ascx" tagname="MenuCantieriStatistiche" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Numeri Attività" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Dal
            </td>
            <td>
                <telerik:RadDateInput runat="server" Width="100px" ID="RadDateInputDal">
                </telerik:RadDateInput>
            </td>
            <td>
                <asp:CustomValidator
                    ID="CustomValidatoreDal"
                    runat="server"
                    ValidationGroup="ricercaStatistiche" 
                    ErrorMessage="Indicare una data Dal"
                    onservervalidate="CustomValidatoreDal_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                Al
            </td>
            <td>
                <telerik:RadDateInput runat="server" Width="100px" ID="RadDateInputAl">
                </telerik:RadDateInput>
            </td>
            <td>
                <asp:CustomValidator
                    ID="CustomValidatoreAl"
                    runat="server"
                    ValidationGroup="ricercaStatistiche" 
                    ErrorMessage="Indicare una data Al"
                    onservervalidate="CustomValidatoreAl_ServerValidate">
                    *
                </asp:CustomValidator>
                <asp:CustomValidator
                    ID="CustomValidatoreDate"
                    runat="server"
                    ValidationGroup="ricercaStatistiche" 
                    ErrorMessage="La data Dal deve essere inferiore a quella Al"
                    onservervalidate="CustomValidatoreDate_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button
                    ID="ButtonRicerca"
                    runat="server"
                    Text="Visualizza"
                    Width="150px" 
                    ValidationGroup="ricercaStatistiche"
                    onclick="ButtonRicerca_Click" />
            </td>
            <td colspan="2">
                <asp:ValidationSummary
                    ID="ValidationSummaryRicercaStatistiche"
                    runat="server"
                    ValidationGroup="ricercaStatistiche"
                    CssClass="messaggiErrore" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel
                    ID="PanelDati"
                    runat="server"
                    Visible="false">
                    <table class="standardTable">
                        <tr>
                            <td>
                                Dati statistici dal&nbsp;
                                <b><asp:Label ID="LabelDal" runat="server"></asp:Label></b>
                                &nbsp;al&nbsp;
                                <b><asp:Label ID="LabelAl" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    Segnalazioni
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Totale: <b><asp:Label ID="LabelSegnalazioniTotale" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Di cui già assegnate/rifiutate: <b><asp:Label ID="LabelSegnalazioniAssegnateRifiutate" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    Assegnazioni
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Totale:&nbsp;<b><asp:Label ID="LabelAssegnazioniTotale" runat="server" Text=""></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Rifiutati:&nbsp;<b><asp:Label ID="LabelAssegnazioniSegnalatiRifiutati" runat="server" Text="0"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Assegnati:&nbsp;<b><asp:Label ID="LabelAssegnazioniSegnalatiAssegnati" runat="server" Text="0"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;Di cui già presi in carico:&nbsp;<b><asp:Label ID="LabelAssegnazioniPresiInCarico" runat="server" Text="0"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListView ID="ListViewAssegnazioniPerIspettore" runat="server">
                                    <LayoutTemplate>
                                        <table width="250px">
                                            <tr>
                                                <th align="left">
                                                    Ispettore
                                                </th>
                                                <th align="right">
                                                    N°Assegn.
                                                </th>
                                            </tr>
                                            <asp:Panel ID="itemPlaceholder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td align="left">
                                                <%# ((TBridge.Cemi.Cantieri.Type.Entities.AssegnazionePerIspettoreStatistica)Container.DataItem).CognomeNome %>
                                            </td>
                                            <td align="right">
                                                <%# ((TBridge.Cemi.Cantieri.Type.Entities.AssegnazionePerIspettoreStatistica)Container.DataItem).NumeroAssegnazioni.ToString() %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    Prese In Carico
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Totale: <b><asp:Label ID="LabelPreseInCaricoTotale" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;Di cui è stato creato un rapporto di ispezione: <b><asp:Label ID="LabelPreseInCaricoConIspezione" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;In Verifica: <b><asp:Label ID="LabelPreseInCaricoConIspezioneInVerifica" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;Concluse: <b><asp:Label ID="LabelPreseInCaricoConIspezioneConcluse" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListView ID="ListViewPreseInCaricoPerIspettore" runat="server">
                                    <LayoutTemplate>
                                        <table width="250px">
                                            <tr>
                                                <th align="left">
                                                    Ispettore
                                                </th>
                                                <th align="right">
                                                    N°prese in carico
                                                </th>
                                            </tr>
                                            <asp:Panel ID="itemPlaceholder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td align="left">
                                                <%# ((TBridge.Cemi.Cantieri.Type.Entities.PresaInCaricoPerIspettoreStatistica)Container.DataItem).CognomeNome %>
                                            </td>
                                            <td align="right">
                                                <%# ((TBridge.Cemi.Cantieri.Type.Entities.PresaInCaricoPerIspettoreStatistica)Container.DataItem).NumeroPreseInCarico.ToString()%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>


