﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;

public partial class Cantieri_ConfermaCancellazioneIspezione : System.Web.UI.Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdIspezione"] != null)
            {
                Int32 idIspezione = (Int32)Context.Items["IdIspezione"];
                CaricaIspezione(idIspezione);
            }
            else
            {
                Server.Transfer("~/Cantieri/CantieriRicercaIspezioni.aspx");
            }
        }
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Cantieri/CantieriRicercaIspezioni.aspx");
    }

    private void CaricaIspezione(Int32 idIspezione)
    {
        RapportoIspezione ispezione = biz.GetIspezioneByKey(idIspezione);

        ViewState["IdIspezione"] = ispezione.IdIspezione.Value;
        LabelGiorno.Text = ispezione.Giorno.ToString("dd/MM/yyyy");
        LabelCantiere.Text = ispezione.Cantiere.IndirizzoMappa;
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        Int32 idIspezione = (Int32)ViewState["IdIspezione"];

        if (biz.DeleteIspezioneFromKey(idIspezione))
        {
            Server.Transfer("~/Cantieri/CantieriRicercaIspezioni.aspx");
        }

        LabelErrore.Visible = true;
    }
}