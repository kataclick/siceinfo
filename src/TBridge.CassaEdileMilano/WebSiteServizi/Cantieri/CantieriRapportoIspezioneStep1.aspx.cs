using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;
using TBridge.Cemi.Geocode.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class CantieriRapportoIspezioneStep1 : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();
    private readonly TSBusiness tsbiz = new TSBusiness();
    private RapportoIspezione ispezione;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);
        
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        #region Registrazioni eventi controlli

        //Registriamo gli eventi di click sul controllo ricerca committente per sapere quando
        //viene selzionato un committente e quando viene richiesto l'inserimento di un nuovo committente
        CantieriRicercaCommittente1.OnCommittenteSelected +=
            CantieriRicercaCommittente1_OnCommittenteSelected;
        CantieriRicercaCommittente1.OnNuovoCommittenteSelected +=
            CantieriRicercaCommittente1_OnNuovoCommittenteSelected;

        //Registriamo gli eventi di click sul controllo ricerca impersa per sapere quando
        //viene selezionata una impresa e quando viene richiesto l'inserimento di una nuova impresa
        CantieriRicercaImpresa1.OnImpresaSelected +=
            CantieriRicercaImpresa1_OnImpresaSelected;
        CantieriRicercaImpresa1.OnNuovaImpresaSelected +=
            CantieriRicercaImpresa1_OnNuovaImpresaSelected;

        #endregion

        #region registrazione biz

        CantieriCommittente1.CantieriBiz = biz;

        CantieriImpresa1.CantieriBiz = biz;
        CantieriImpresa1.TsBiz = tsbiz;

        #endregion

        #region registrazione validationgroup

        //Settiamo il validation group cross-control
        string validationGroupImpresa = "validationGroupImpresa";
        ButtonInserisciImpresa.ValidationGroup = validationGroupImpresa;
        CantieriImpresa1.SetValidationGroup(validationGroupImpresa);

        //Settiamo il validation group cross-control
        string validationGroupCommittente = "validationGroupCommittente";
        ButtonInserisciCommittente.ValidationGroup = validationGroupCommittente;
        CantieriCommittente1.SetValidationGroup(validationGroupCommittente);

        #endregion

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();

        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupCommittente') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciCommittente, null) + ";");
        sb.Append("return true;");
        ButtonInserisciCommittente.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciCommittente);


        ////resettiamo
        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresa') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciImpresa, null) + ";");
        sb.Append("return true;");
        ButtonInserisciImpresa.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciImpresa);



        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('Step1') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalva, null) + ";");
        sb.Append("return true;");
        ButtonSalva.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);



        #endregion

        if (!Page.IsPostBack)
        {
            if ((Request.QueryString["giorno"] != null && Request.QueryString["idCantiere"] != null &&
                Request.QueryString["idIspettore"] != null) || Request.QueryString["idIspezione"] != null)
            {
                CaricaTipologieAppalto();
                CaricaSegnalazioniPervenute();
                CaricaStatiIspezione();
                CaricaTipiImpresaAppaltatrice();

                Int32? idAttivita = null;
                if (Request.QueryString["idIspezione"] != null)
                {
                    Int32? idIspezione = null;
                    if (Request.QueryString["idIspezione"] != null)
                    {
                        idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
                        ispezione = biz.GetIspezioneByKey(idIspezione.Value);
                    }
                }
                else
                {
                    if (Request.QueryString["idAttivita"] != null)
                    {
                        idAttivita = Int32.Parse(Request.QueryString["idAttivita"]);
                        ispezione = biz.GetIspezione(idAttivita.Value);
                    }
                }

                if (ispezione == null)
                {
                    DateTime giorno = DateTime.Parse(Request.QueryString["giorno"]);
                    int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
                    int idIspettore = Int32.Parse(Request.QueryString["idIspettore"]);

                    Cantiere cantiere = biz.GetCantiere(idCantiere);
                    Ispettore ispettore = biz.GetIspettori(idIspettore, null)[0];

                    ispezione = new RapportoIspezione(giorno, cantiere, ispettore, idAttivita);
                    ispezione.GruppoIspezione = new IspettoreCollection();

                    if (cantiere.PresaInCarico != null)
                    {
                        TBridge.Cemi.Type.Domain.CantieriCalendarioAttivita attivita = bizEF.GetAttivita(cantiere.PresaInCarico.Id);

                        foreach (TBridge.Cemi.Type.Domain.Ispettore isp in attivita.IspettoriCorrelati)
                        {
                            ispezione.GruppoIspezione.Add(new Ispettore(isp.IdIspettore, isp.Cognome, isp.Nome, null, true));
                        }
                    }
                    CantieriGruppoIspezione1.CaricaGruppoIspezione(ispezione.GruppoIspezione, 
                        ispezione.Ispettore.IdIspettore.Value);
                }
                else
                {
                    ispezione.GruppoIspezione = biz.GetGruppoIspezione(ispezione.IdIspezione);
                    CantieriGruppoIspezione1.CaricaGruppoIspezione(ispezione.GruppoIspezione,
                                                                   ispezione.Ispettore.IdIspettore.Value);
                }

                CantieriTestataIspezione1.ImpostaTestata(ispezione);

                if (ispezione != null)
                    CaricaDatiIspezione();
                else
                {
                    // Non ho ancora un rapporto di ispezione, devo comunque recuperare le info sul cantiere
                    CaricaDatiCantiere();
                }

                ispezione.Cantiere.PresaInCarico = null;
                ViewState["Ispezione"] = ispezione;
            }
        }

        ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);
        //((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonPassoPrecedente);
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["Ispezione"] != null)
            {
                Cantiere cantiere = CreaCantiere();

                RapportoIspezione ispezione = (RapportoIspezione) ViewState["Ispezione"];
                AggiornaDatiIspezione(ispezione);
                ispezione.GruppoIspezione = CantieriGruppoIspezione1.GruppoIspezione;

                // Eseguo l'operazione
                int idCantiere;
                Int32.TryParse(ViewState["IdCantiere"].ToString(), out idCantiere);
                cantiere.IdCantiere = idCantiere;

                bool aggComm = true;
                // Se il committente proviene dalle notifiche e non � stato mai toccato salvo la foto
                if (cantiere.Committente != null && cantiere.Committente.FonteNotifica &&
                    !cantiere.Committente.Modificato)
                    aggComm = biz.UpdateCommittenteIspettore(cantiere.Committente);

                bool aggImp = true;
                // Se l'impresa appaltatrice proviene dalle notifiche e non � stato mai toccato salvo la foto
                if (cantiere.ImpresaAppaltatrice != null && cantiere.ImpresaAppaltatrice.FonteNotifica &&
                    !cantiere.ImpresaAppaltatrice.Modificato)
                    aggComm = biz.UpdateImpresaIspettore(cantiere.ImpresaAppaltatrice);

                // Aggiorno il record
                if (aggComm && aggImp && biz.InsertUpdateIspezione(ispezione) && biz.UpdateCantiere(cantiere))
                {
                    string redir =
                        String.Format(
                            "~/Cantieri/CantieriRapportoIspezioneStep2.aspx?idAttivita={0}&idCantiere={1}&giorno={2}&idIspettore={3}&idIspezione={4}",
                            ispezione.IdAttivita,
                            Request.QueryString["idCantiere"],
                            Request.QueryString["giorno"],
                            Request.QueryString["idIspettore"],
                            ispezione.IdIspezione);
                    Response.Redirect(redir);
                }
                else
                    LabelRisultato.Text = "Si � verificato un errore durante l'operazione";
            }
        }
    }

    private void AggiornaDatiIspezione(RapportoIspezione ispezione)
    {
        if (ispezione != null)
        {
            if (!string.IsNullOrEmpty(DropDownListSegnalazionePervenuta.SelectedValue))
                ispezione.Segnalazione =
                    (SegnalazionePervenuta)
                    Enum.Parse(typeof (SegnalazionePervenuta), DropDownListSegnalazionePervenuta.SelectedValue);
            if (!string.IsNullOrEmpty(DropDownListStatoIspezione.SelectedValue))
                ispezione.Stato =
                    (StatoIspezione) Enum.Parse(typeof (StatoIspezione), DropDownListStatoIspezione.SelectedValue);
            if (!string.IsNullOrEmpty(TextBoxAvanzamentoLavori.Text))
                ispezione.Avanzamento = Int32.Parse(TextBoxAvanzamentoLavori.Text);
        }
    }

    private Cantiere CreaCantiere()
    {
        // Creazione oggetto Cantiere
        Cantiere cantiere = new Cantiere();
        
        Indirizzo indirizzo = IscrizioneLavoratoreIndirizzo1.GetIndirizzo();

        cantiere.Indirizzo = Presenter.NormalizzaCampoTesto(indirizzo.NomeVia);
        cantiere.Civico = Presenter.NormalizzaCampoTesto(indirizzo.Civico);
        cantiere.Provincia = Presenter.NormalizzaCampoTesto(indirizzo.Provincia);
        cantiere.Comune = Presenter.NormalizzaCampoTesto(indirizzo.Comune);
        cantiere.Cap = Presenter.NormalizzaCampoTesto(indirizzo.Cap);
        cantiere.Latitudine = indirizzo.Latitudine.HasValue ? (Double?)Decimal.ToDouble(indirizzo.Latitudine.Value) : null;
        cantiere.Longitudine = indirizzo.Longitudine.HasValue ? (Double?)Decimal.ToDouble(indirizzo.Longitudine.Value) : null;

        if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCantierePermessoCostruire.Text)))
            cantiere.PermessoCostruire = Presenter.NormalizzaCampoTesto(TextBoxCantierePermessoCostruire.Text);
        if (!string.IsNullOrEmpty(TextBoxCantiereImporto.Text))
            cantiere.Importo = double.Parse(TextBoxCantiereImporto.Text);
        if (!string.IsNullOrEmpty(TextBoxCantiereDataInizioLavori.Text))
            cantiere.DataInizioLavori = DateTime.Parse(TextBoxCantiereDataInizioLavori.Text);
        if (!string.IsNullOrEmpty(TextBoxCantiereDataFineLavori.Text))
            cantiere.DataFineLavori = DateTime.Parse(TextBoxCantiereDataFineLavori.Text);
        if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCantiereDirezioneLavori.Text)))
            cantiere.DirezioneLavori = Presenter.NormalizzaCampoTesto(TextBoxCantiereDirezioneLavori.Text);
        if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCantiereResponsabileProcedimento.Text)))
            cantiere.ResponsabileProcedimento = Presenter.NormalizzaCampoTesto(TextBoxCantiereResponsabileProcedimento.Text);
        if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCantiereResponsabileCantiere.Text)))
            cantiere.ResponsabileCantiere = Presenter.NormalizzaCampoTesto(TextBoxCantiereResponsabileCantiere.Text);
        if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxCantiereDescrizioneLavori.Text)))
            cantiere.DescrizioneLavori = Presenter.NormalizzaCampoTesto(TextBoxCantiereDescrizioneLavori.Text);

        cantiere.TipologiaAppalto =
            (TipologiaAppalto)
            Enum.Parse(typeof (TipologiaAppalto), DropDownListCantiereTipologiaAppalto.SelectedItem.ToString());
        cantiere.Attivo = CheckBoxCantiereAttivo.Checked;

        cantiere.Committente = ViewState["committente"] as Committente;
        cantiere.ImpresaAppaltatrice = ViewState["impresa"] as Impresa;

        if (cantiere.ImpresaAppaltatrice != null)
        {
            if (RadioButtonDefault.Checked && !string.IsNullOrEmpty(DropDownListTipoImpresa.SelectedValue))
            {
                cantiere.TipoImpresaAppaltatrice = DropDownListTipoImpresa.SelectedValue;
            }
            else if (RadioButtonAltro.Checked && !string.IsNullOrEmpty(TextBoxTipoImpresa.Text))
            {
                cantiere.TipoImpresaAppaltatrice = TextBoxTipoImpresa.Text;
            }
        }

        // Non cancellare il committente trovato e l'impresa trovata
        //Cantiere cantiere = new Cantiere(null, provincia, comune, cap, indirizzo, civico,
        //                                 impresaAppaltatrice, permessoCostruire, importo, dataInizioLavori,
        //                                 dataFineLavori,
        //                                 tipologiaAppalto, attivo, committente, null, impresa, null, null,
        //                                 direzioneLavori,
        //                                 responsabileProcedimento, responsabileCantiere, descrizioneLavori,
        //                                 tipoImpresaAppaltatrice, null);

        return cantiere;
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cantieri/CantieriDefault.aspx");
        //string paginaPrecedente = this.Page.Request.UrlReferrer.LocalPath.ToString();
        //Response.Redirect(paginaPrecedente);
    }

    protected void ButtonModificaCommittente_Click(object sender, EventArgs e)
    {
        if (ViewState["committente"] != null)
        {
            Committente committente = (Committente) ViewState["committente"];
            CantieriCommittente1.ImpostaCommittente(committente);

            //committente.Modificato = true;
            //ViewState["committente"] = committente;

            LabelInserimentoCommittenteRes.Text = string.Empty;
            CantieriRicercaCommittente1.Visible = false;
            PanelInserimentoCommittente.Visible = true;
            LabelTitoloInsComm.Text = "Modifica committente";
            ButtonInserisciCommittente.Text = "Modifica committente";
        }
    }

    protected void ButtonModificaImpresa_Click(object sender, EventArgs e)
    {
        if (ViewState["impresa"] != null)
        {
            Impresa impresa = (Impresa) ViewState["impresa"];
            CantieriImpresa1.ImpostaImpresa(impresa);

            //committente.Modificato = true;
            //ViewState["committente"] = committente;

            LabelInserimentoImpresaRes.Text = string.Empty;
            CantieriRicercaImpresa1.Visible = false;
            PanelInserimentoImpresa.Visible = true;
            LabelTitoloInserimentoImpresa.Text = "Modifica impresa";
            ButtonInserisciImpresa.Text = "Modifica impresa";
        }
    }

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (IscrizioneLavoratoreIndirizzo1.IndirizzoConfermato())
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    #region eventi inserimento nuovo committente

    protected void ButtonInserisciCommittente_Click(object sender, EventArgs e)
    {
        Page.Validate("ValidaInserimentoCommittente");

        if (Page.IsValid)
        {
            Committente committente = CantieriCommittente1.Committente;
            bool res = true;

            if (committente != null)
            {
                if (!committente.IdCommittente.HasValue)
                    // Inserisco il record
                    biz.InsertCommittente(committente);
                else
                {
                    if (committente.FonteNotifica && !committente.Modificato)
                    {
                        biz.UpdateCommittenteIspettore(committente);
                    }
                }

                if (committente.IdCommittente > 0 && res)
                {
                    CommittenteSelezionato(committente);
                    CantieriCommittente1.Reset();
                }
                else
                    LabelInserimentoCommittenteRes.Text = "Errore durante l'inserimento";
            }
            else
                LabelInserimentoCommittenteRes.Text = CantieriCommittente1.Errore;
        }
    }

    protected void ButtonAnnullaInserimentoCommittente_Click(object sender, EventArgs e)
    {
        PanelInserimentoCommittente.Visible = false;
        LabelInserimentoCommittenteRes.Text = string.Empty;
    }

    #endregion

    #region Gestione Eventi inserimento nuova impresa

    protected void ButtonInserisciImpresa_Click(object sender, EventArgs e)
    {
        Page.Validate("ValidaInserimentoImpresa");

        if (Page.IsValid)
        {
            Impresa impresa = CantieriImpresa1.impresa;
            bool res = true;

            if (impresa != null)
            {
                // Inserisco il record
                if (!impresa.IdImpresa.HasValue)
                    biz.InsertImpresa(impresa);
                else
                {
                    if (impresa.FonteNotifica && !impresa.Modificato)
                    {
                        res = biz.UpdateImpresaIspettore(impresa);
                    }
                }

                if (impresa.IdImpresa > 0 && res)
                {
                    ImpresaSelezionata(impresa);
                    CantieriImpresa1.Reset();
                }
                else
                    LabelInserimentoImpresaRes.Text = "Si � verificato un errore durante l'inserimento";
            }
            else
                LabelInserimentoImpresaRes.Text = CantieriImpresa1.Errore;
        }
    }

    protected void ButtonAnnullaInserimentoImpresa_Click(object sender, EventArgs e)
    {
        PanelInserimentoImpresa.Visible = false;
        LabelInserimentoImpresaRes.Text = string.Empty;
    }

    #endregion

    #region Eventi Committente selezionato/nuovo/ricerca

    protected void ButtonSelezionaCommittente_Click(object sender, EventArgs e)
    {
        PanelInserimentoCommittente.Visible = false;
        CantieriRicercaCommittente1.Visible = true;
    }

    private void CantieriRicercaCommittente1_OnCommittenteSelected(Committente committente)
    {
        CommittenteSelezionato(committente);
    }

    private void CommittenteSelezionato(Committente committente)
    {
        TextBoxCommittente.Text = committente.NomeCompleto;
        TextBoxCommittente.BackColor = Color.White;
        ViewState["committente"] = committente;

        CantieriRicercaCommittente1.Visible = false;
        PanelInserimentoCommittente.Visible = false;

        if (committente.FonteNotifica && !committente.Modificato)
            ButtonModificaCommittente.Enabled = true;
        else
            ButtonModificaCommittente.Enabled = false;
    }

    private void CantieriRicercaCommittente1_OnNuovoCommittenteSelected(object sender, EventArgs e)
    {
        LabelInserimentoCommittenteRes.Text = string.Empty;
        CantieriRicercaCommittente1.Visible = false;
        PanelInserimentoCommittente.Visible = true;
    }

    #endregion

    #region Eventi Impresa selezionato/nuovo/ricerca

    /// <summary>
    /// E' stata richiesta la ricerca, nascondiamo il pannello di inserimento e visualizziamo la ricerca
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonSelezionaAppaltatrice_Click(object sender, EventArgs e)
    {
        CantieriRicercaImpresa1.Visible = true;
        PanelInserimentoImpresa.Visible = false;
    }

    /// <summary>
    /// Da ricerca � stata selezionata un'impresa
    /// </summary>
    /// <param name="impresa"></param>
    private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        ImpresaSelezionata(impresa);
    }

    /// <summary>
    /// Memorizziamo l'impresa selezionata
    /// </summary>
    /// <param name="impresa"></param>
    private void ImpresaSelezionata(Impresa impresa)
    {
        RadioButtonImpresaCantieri.Checked = false;
        RadioButtonImpresaSiceInfo.Checked = false;

        TextBoxImpresaAppaltatrice.BackColor = Color.White;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
        {
            RadioButtonImpresaSiceInfo.Checked = true;
            TextBoxImpresaAppaltatrice.Text =
                String.Format("{0} - {1}", impresa.IdImpresa,
                              impresa.NomeCompleto);
        }
        else
        {
            RadioButtonImpresaCantieri.Checked = true;
            TextBoxImpresaAppaltatrice.Text = impresa.NomeCompleto;
        }
        ViewState["impresa"] = impresa;

        CantieriRicercaImpresa1.Visible = false;
        PanelInserimentoImpresa.Visible = false;

        if (impresa.FonteNotifica && !impresa.Modificato)
            ButtonModificaImpresa.Enabled = true;
        else
            ButtonModificaImpresa.Enabled = false;
    }

    /// <summary>
    /// Nascondiamo la ricerca e rendiamo visibile il pannello per l'inserimento di una nuova impresa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void CantieriRicercaImpresa1_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        LabelInserimentoImpresaRes.Text = string.Empty;
        CantieriRicercaImpresa1.Visible = false;
        PanelInserimentoImpresa.Visible = true;
    }

    #endregion

    #region Caricamenti DATI Cantiere

    private void CaricaTipiImpresaAppaltatrice()
    {
        DropDownListTipoImpresa.Items.Clear();
        DropDownListTipoImpresa.Items.Add(string.Empty);
        DropDownListTipoImpresa.Items.Add(new ListItem("Singola", "Singola"));
        DropDownListTipoImpresa.Items.Add(new ListItem("A.T.I.", "A.T.I."));
        DropDownListTipoImpresa.SelectedValue = string.Empty;
    }

    private void CaricaSegnalazioniPervenute()
    {
        DropDownListSegnalazionePervenuta.Items.Clear();
        DropDownListSegnalazionePervenuta.Items.Add(string.Empty);
        DropDownListSegnalazionePervenuta.DataSource = Enum.GetNames(typeof (SegnalazionePervenuta));
        DropDownListSegnalazionePervenuta.DataBind();
    }

    private void CaricaStatiIspezione()
    {
        DropDownListStatoIspezione.Items.Clear();
        DropDownListStatoIspezione.Items.Add(string.Empty);
        DropDownListStatoIspezione.DataSource = Enum.GetNames(typeof (StatoIspezione));
        DropDownListStatoIspezione.DataBind();
    }

    private void CaricaDatiIspezione()
    {
        CaricaDatiCantiere();

        if (ispezione.Segnalazione.HasValue)
            DropDownListSegnalazionePervenuta.SelectedValue = ispezione.Segnalazione.Value.ToString();
        else
            DropDownListSegnalazionePervenuta.SelectedValue = string.Empty;
        if (ispezione.Stato.HasValue)
            DropDownListStatoIspezione.SelectedValue = ispezione.Stato.Value.ToString();
        else
            DropDownListStatoIspezione.SelectedValue = string.Empty;
        if (ispezione.Avanzamento.HasValue)
            TextBoxAvanzamentoLavori.Text = ispezione.Avanzamento.ToString();
    }

    private void CaricaDatiCantiere()
    {
        ViewState["IdCantiere"] = ispezione.Cantiere.IdCantiere.Value;

        Indirizzo indirizzo = new Indirizzo
        {
            NomeVia = ispezione.Cantiere.Indirizzo,
            Civico = ispezione.Cantiere.Civico,
            Provincia = ispezione.Cantiere.Provincia,
            Comune = ispezione.Cantiere.Comune,
            Cap = ispezione.Cantiere.Cap,
            Latitudine = (Decimal?)ispezione.Cantiere.Latitudine,
            Longitudine = (Decimal?)ispezione.Cantiere.Longitudine
        };

        IscrizioneLavoratoreIndirizzo1.CaricaDatiIndirizzo(indirizzo);

        DropDownListCantiereTipologiaAppalto.Text = ispezione.Cantiere.TipologiaAppalto.ToString();
        TextBoxCantiereImporto.Text = ispezione.Cantiere.Importo.ToString();
        if (ispezione.Cantiere.DataInizioLavori.HasValue)
            TextBoxCantiereDataInizioLavori.Text = ispezione.Cantiere.DataInizioLavori.Value.ToShortDateString();
        if (ispezione.Cantiere.DataFineLavori.HasValue)
            TextBoxCantiereDataFineLavori.Text = ispezione.Cantiere.DataFineLavori.Value.ToShortDateString();
        TextBoxCantierePermessoCostruire.Text = ispezione.Cantiere.PermessoCostruire;
        CheckBoxCantiereAttivo.Checked = ispezione.Cantiere.Attivo;
        TextBoxCantiereDirezioneLavori.Text = ispezione.Cantiere.DirezioneLavori;
        TextBoxCantiereResponsabileProcedimento.Text = ispezione.Cantiere.ResponsabileProcedimento;
        TextBoxCantiereResponsabileCantiere.Text = ispezione.Cantiere.ResponsabileCantiere;
        TextBoxCantiereDescrizioneLavori.Text = ispezione.Cantiere.DescrizioneLavori;

        // Tipo impresa appaltatrice
        if (!string.IsNullOrEmpty(ispezione.Cantiere.TipoImpresaAppaltatrice))
        {
            if (DropDownListTipoImpresa.Items.FindByText(ispezione.Cantiere.TipoImpresaAppaltatrice) != null)
            {
                RadioButtonDefault.Checked = true;
                DropDownListTipoImpresa.SelectedValue = ispezione.Cantiere.TipoImpresaAppaltatrice;
            }
            else
            {
                RadioButtonAltro.Checked = true;
                TextBoxTipoImpresa.Text = ispezione.Cantiere.TipoImpresaAppaltatrice;
            }
        }

        // Committente
        if (ispezione.Cantiere.Committente != null)
        {
            TextBoxCommittente.Text = ispezione.Cantiere.Committente.NomeCompleto;
            ViewState["committente"] = ispezione.Cantiere.Committente;

            if (ispezione.Cantiere.Committente.FonteNotifica && !ispezione.Cantiere.Committente.Modificato)
                ButtonModificaCommittente.Enabled = true;
            else
                ButtonModificaCommittente.Enabled = false;
        }
        else if (ispezione.Cantiere.CommittenteTrovato != null)
        {
            TextBoxCommittente.BackColor = Color.LightCoral;
            TextBoxCommittente.Text = ispezione.Cantiere.CommittenteTrovato + Environment.NewLine +
                                      "[Committente non presente]";

            CantieriRicercaCommittente1.CommittenteTrovato = ispezione.Cantiere.CommittenteTrovato.Trim();
            CantieriRicercaCommittente1.CaricaCommittenti();
            CantieriRicercaCommittente1.Visible = true;

            CantieriCommittente1.ImpostaRagioneSociale(ispezione.Cantiere.CommittenteTrovato);
        }

        // Impresa appaltatrice
        if (ispezione.Cantiere.ImpresaAppaltatrice != null)
        {
            RadioButtonImpresaCantieri.Checked = false;
            RadioButtonImpresaSiceInfo.Checked = false;

            if (ispezione.Cantiere.ImpresaAppaltatrice.FonteNotifica &&
                !ispezione.Cantiere.ImpresaAppaltatrice.Modificato)
                ButtonModificaImpresa.Enabled = true;
            else
                ButtonModificaImpresa.Enabled = false;

            if (ispezione.Cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
            {
                RadioButtonImpresaCantieri.Checked = true;
                TextBoxImpresaAppaltatrice.Text = ispezione.Cantiere.ImpresaAppaltatrice.NomeCompleto;
            }
            else
            {
                RadioButtonImpresaSiceInfo.Checked = true;
                TextBoxImpresaAppaltatrice.Text =
                    String.Format("{0} - {1}", ispezione.Cantiere.ImpresaAppaltatrice.IdImpresa,
                                  ispezione.Cantiere.ImpresaAppaltatrice.NomeCompleto);
            }
            ViewState["impresa"] = ispezione.Cantiere.ImpresaAppaltatrice;
        }
        else if (ispezione.Cantiere.ImpresaAppaltatriceTrovata != null)
        {
            TextBoxImpresaAppaltatrice.BackColor = Color.LightCoral;
            TextBoxImpresaAppaltatrice.Text = ispezione.Cantiere.ImpresaAppaltatriceTrovata + Environment.NewLine +
                                              "[Impresa non presente]";

            CantieriRicercaImpresa1.ImpresaTrovata = ispezione.Cantiere.ImpresaAppaltatriceTrovata.Trim();
            CantieriRicercaImpresa1.CaricaImprese();
            CantieriRicercaImpresa1.Visible = true;

            CantieriImpresa1.ImpostaRagioneSociale(ispezione.Cantiere.ImpresaAppaltatriceTrovata);
        }
    }

    private void CaricaTipologieAppalto()
    {
        DropDownListCantiereTipologiaAppalto.DataSource = Enum.GetNames(typeof (TipologiaAppalto));
        DropDownListCantiereTipologiaAppalto.DataBind();
    }

    #endregion

    protected void CustomValidatorTipologiaAppalto_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListCantiereTipologiaAppalto.SelectedValue == "NonDefinito")
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
}
