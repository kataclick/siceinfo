using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class CantieriModificaIspettore : Page
{
    private readonly GestioneUtentiBiz gu =
        new GestioneUtentiBiz();

    private IspettoriCollection ispettori;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriModificaIspettori,
                                              "CantieriModificaIspettori.aspx");

        CaricaIspettori();
    }

    private void CaricaIspettori()
    {
        ispettori = gu.GetUtentiIspettori();

        GridViewIspettori.DataSource = ispettori;
        GridViewIspettori.DataBind();
    }

    protected void GridViewIspettori_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int idUtente = ((int) GridViewIspettori.DataKeys[e.RowIndex].Value);
        CaricaIspettore(idUtente);
    }

    private void CaricaIspettore(int idUtente)
    {
        Ispettore ispettore = (Ispettore)GestioneUtentiBiz.GetIdentitaUtente(idUtente);
        RegistrazioneIspettoreModifica1.Ispettore = ispettore;

        RegistrazioneIspettoreModifica1.Visible = true;
        LabelModificaIspettore.Visible = true;
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cantieri/CantieriDefault.aspx");
    }
}