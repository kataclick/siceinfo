﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GeneraCantiereRegionale.aspx.cs" Inherits="Cantieri_GeneraCantiere" %>

<%@ Register src="WebControls/SelezioneIndirizziNotificaRegionale.ascx" tagname="SelezioneIndirizziNotificaRegionale" tagprefix="uc1" %>

<%@ Register src="WebControls/SegnalazioneDati.ascx" tagname="SegnalazioneDati" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <div class="standardDiv">
            <uc1:SelezioneIndirizziNotificaRegionale ID="SelezioneIndirizziNotificaRegionale1" 
                runat="server" />
            <asp:CustomValidator
                ID="CustomValidatorIndirizzo"
                runat="server"
                ValidationGroup="creaCantiere"
                ErrorMessage="Selezionare l'indirizzo sul quale creare il cantiere" 
                onservervalidate="CustomValidatorIndirizzo_ServerValidate">
                &nbsp;
            </asp:CustomValidator>
        </div>
        <br />
        <div id="divSegnalazione" runat="server" class="standardDiv">
            <b>
                Segnalazione
            </b>
            <br />
            <uc2:SegnalazioneDati ID="SegnalazioneDati1" runat="server" />
            <asp:CustomValidator
                ID="CustomValidatorSegnalazione"
                runat="server"
                ValidationGroup="creaCantiere"
                ErrorMessage="Indicare la segnalazione" 
                onservervalidate="CustomValidatorSegnalazione_ServerValidate">
                &nbsp;
            </asp:CustomValidator>
        </div>
        <div class="standardDiv">
            <asp:Button 
                ID="ButtonSalva"
                runat="server"
                Text="Crea"
                Width="150px" 
                onclick="ButtonSalva_Click"
                ValidationGroup="creaCantiere" />
            <br />
            <asp:ValidationSummary
                ID="ValidationSummaryCreaCantiere"
                runat="server"
                CssClass="messaggiErrore"
                ValidationGroup="creaCantiere" />
        </div>
    </form>
</body>
</html>
