﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EstrazioneLodi.aspx.cs" Inherits="Cantieri_EstrazioneLodi" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuCantieriStatistiche.ascx" tagname="MenuCantieriStatistiche" tagprefix="uc2" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc2:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Estrazione Lodi" />
    <br />
    <asp:Button
        ID="ButtonEstrazione"
        runat="server"
        Width="150px"
        Text="Estrazione Lodi" onclick="ButtonEstrazione_Click" />
</asp:Content>


