using System;
using System.Configuration;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cantieri.Type.Filters;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Presenter;

public partial class CantieriLocalizzazioneMultipla : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly int maxPushpin = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPushpin"]);

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);

        ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

        if (!Page.IsPostBack)
        {
            //CaricaZone();
        }
    }

    private void CaricaCantieri(StringBuilder stringBuilder)
    {
        CantieriFilter filtro = new CantieriFilter();
        double importoD;
        
        filtro.Provincia = TextBoxProvincia.Text.Trim();
        filtro.Comune = TextBoxComune.Text.Trim();
        filtro.Indirizzo = TextBoxIndirizzo.Text.Trim();
        filtro.Impresa = TextBoxImpresa.Text.Trim();
        filtro.Committente = TextBoxCommittente.Text.Trim();
        //filtro.ImpresaSubappalto = TextBoxSubappaltatrice.Text.Trim();

        if (!String.IsNullOrEmpty(TextBoxImporto.Text) && Double.TryParse(TextBoxImporto.Text, out importoD))
        {
            filtro.Importo = importoD;
        }

        //string zonaT = DropDownListZone.SelectedValue;
        //if (!String.IsNullOrEmpty(DropDownListZone.SelectedValue) && DropDownListZone.SelectedValue != "0")
        //{
        //    filtro.IdZona = Int32.Parse(DropDownListZone.SelectedValue);
        //}
        
        CantiereCollection listaCantieri =
            biz.GetCantieri(filtro);


        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", listaCantieri.Count);

        if (listaCantieri.Count <= maxPushpin)
        {
            CaricaMappa(listaCantieri, stringBuilder);
            LabelErrore.Visible = false;
        }
        else
        {
            LabelErrore.Visible = true;
        }
    }

    private Int32 cantieriConCoordinate = 0;
    private void CaricaMappa(CantiereCollection listaCantieri, StringBuilder stringBuilder)
    {
        int i = 0;
        foreach (Cantiere cantiere in listaCantieri)
        {
            var descrizione = new StringBuilder();
            if (cantiere.Latitudine.HasValue && cantiere.Longitudine.HasValue && cantiere.IdCantiere.HasValue)
            {
                cantieriConCoordinate++;

                descrizione.Append(cantiere.Indirizzo);
                descrizione.Append(" ");
                descrizione.Append(cantiere.Civico);
                descrizione.Append("<br />");
                descrizione.Append(cantiere.Comune);
                descrizione.Append(" ");
                descrizione.Append(cantiere.Provincia);
                descrizione.Append("<br />");
                // Impresa appaltatrice
                if (cantiere.ImpresaAppaltatrice != null)
                    descrizione.Append(cantiere.ImpresaAppaltatrice.RagioneSociale);
                else if (cantiere.ImpresaAppaltatriceTrovata != null)
                    descrizione.Append(cantiere.ImpresaAppaltatriceTrovata);
                descrizione.Append("<br />");
                descrizione.Append("<a href=CantieriSchedaCantiere.aspx?idCantiere=" + cantiere.IdCantiere.Value +
                                   ">Scheda cantiere</a>");

                string immagineCantiere = String.Empty;
                if (!cantiere.StatoIspezione.HasValue)
                {
                    immagineCantiere = "../images/cantiere24.gif";
                }
                else
                {
                    switch (cantiere.StatoIspezione.Value)
                    {
                        case StatoIspezione.InVerifica:
                            immagineCantiere = "../images/cantiere24standby.gif";
                            break;
                        case StatoIspezione.Conclusa:
                            immagineCantiere = "../images/cantiere24attivo.gif";
                            break;
                        default:
                            immagineCantiere = "../images/cantiere24.gif";
                            break;
                    }
                }

                stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", i,
                                                   cantiere.Latitudine.ToString().Replace(',', '.'),
                                                   cantiere.Longitudine.ToString().Replace(',', '.'), descrizione.Replace("'", "").Replace('"', ' '),
                                                   immagineCantiere));
                i++;
            }
        }

        LabelCantieriSuMappa.Visible = true;
        LabelCantieriSuMappa.Text = String.Format("Cantieri con coordinate: {0}", cantieriConCoordinate);
    }

    //private void CaricaZone()
    //{
    //    ZonaCollection listaZone = biz.GetZone(null);

    //    DropDownListZone.Items.Clear();
    //    DropDownListZone.Items.Insert(0, new ListItem(" Tutti", "0"));
    //    DropDownListZone.DataSource = listaZone;
    //    DropDownListZone.DataTextField = "Nome";
    //    DropDownListZone.DataValueField = "IdZona";

    //    DropDownListZone.DataBind();
    //}


    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();

        sb.Append("LoadMap();");
        CaricaCantieri(sb);

        RadAjaxPanel1.ResponseScripts.Add(sb.ToString());
    }
}