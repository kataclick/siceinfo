using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriConsuntivoSettimanale : Page
{
    private const int idACCEDIRAPPORTO = 8;
    private const int idACCEDIRAPPORTOFINTO = 9;
    private const int idCANCELLAZIONEATTIVITA = 1;
    private const int idCANCELLAZIONEATTIVITAFINTO = 0;

    private const int idCANCELLAZIONERAPPORTO = 12;
    private const int idCANCELLAZIONERAPPORTOFINTO = 13;
    private const int idREPORTRAPPORTO = 10;
    private const int idREPORTRAPPORTOFINTO = 11;
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private int giorniScadenza;

    //private const int idCHECKPROGRAMMATO = 2;
    //private const int idCHECKPREVENTIVO = 3;
    //private const int idCHECKCONSUNTIVO = 4;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.CantieriConsuPrev,
                                                             FunzionalitaPredefinite.CantieriConsuPrevRUI
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        ((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("maindiv")).Attributes.Add("style", "width: 1040px;");
        ((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("outerdiv")).Attributes.Add("style", "width: 1300px;");

        //WebControls_MenuDx menudx = (WebControls_MenuDx)Master.FindControl("MenuDx1");
        //if (menudx != null)
        //    menudx.Visible = false;

        giorniScadenza = biz.GetScadenza();

        if (!Page.IsPostBack)
        {
            IspettoreCollection listaIspettori = biz.GetIspettori(null, true);
            DropDownListIspettore.DataSource = listaIspettori;
            DropDownListIspettore.DataTextField = "NomeCompleto";
            DropDownListIspettore.DataValueField = "IdIspettore";
            DropDownListIspettore.DataBind();

            //Se l'utente � ispettore e non � rui blocchiamo la dropdown
            if (GestioneUtentiBiz.IsIspettore() &&
                GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrev)
                &&
                !GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI))
            {
                //TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore ispettore =
                //    ((Ispettore) HttpContext.Current.User.Identity).
                //        Entity;
                TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore ispettore =
                    (TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                DropDownListIspettore.SelectedValue = ispettore.IdIspettore.ToString();
                DropDownListIspettore.Enabled = false;
            }

            // Torno dall'inserimento di una nuova attivit�
            if (Request.QueryString["idIspettore"] != null && Request.QueryString["dal"] != null)
            {
                DateTime dal = DateTime.Parse(Request.QueryString["dal"]);
                for (int i = 0; i < 7; i++)
                {
                    CalendarSettimana.SelectedDates.Add(dal);
                    dal = dal.AddDays(1);
                }
                CalendarSettimana.DataBind();

                DropDownListIspettore.SelectedValue = Request.QueryString["idIspettore"];

                VisualizzaPreventivo();
            }
        }

        LabelErrore.Text = string.Empty;

        if (Request.QueryString["modalita"] == "preventivo")
        {
            TitoloSottotitolo1.sottoTitolo = "Preventivo settimanale";
            LabelTitolo.Text = "Preventivo settimanale";
        }
        else
        {
            TitoloSottotitolo1.sottoTitolo = "Consuntivo settimanale";
            LabelTitolo.Text = "Consuntivo settimanale";
        }
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        VisualizzaPreventivo();
    }

    private void VisualizzaPreventivo()
    {
        if (DropDownListIspettore.SelectedValue != null)
            if (CalendarSettimana.SelectedDates.Count == 7) // controllo sulla settimana
            {
                DateTime dal = CalendarSettimana.SelectedDates[0];
                DateTime al = CalendarSettimana.SelectedDates[6];
                int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);

                ProgrammazioneGiornaliereIspettoreCollection programmazione;
                if (Request.QueryString["modalita"] != null)
                {
                    // Distinguo il caso preventivo dal consuntivo
                    if (Request.QueryString["modalita"] == "preventivo")
                        programmazione = biz.GetProgrammazioneSettimanale(dal, al, idIspettore, true, true, false);
                    else
                        programmazione = biz.GetProgrammazioneSettimanale(dal, al, idIspettore, false, true, true);
                }
                else
                {
                    // Per default carico il consuntivo
                    programmazione = biz.GetProgrammazioneSettimanale(dal, al, idIspettore, false, true, true);
                }

                if (programmazione != null)
                {
                    ButtonSalvaSituazione.Enabled = true;
                    VisualizzaTabbing(true);
                    ButtonReport.Enabled = true;
                }
                else
                {
                    ButtonSalvaSituazione.Enabled = false;
                    VisualizzaTabbing(false);
                    ButtonReport.Enabled = false;
                }

                //GridViewPreventivo.DataSource = programmazione;
                //GridViewPreventivo.DataBind();

                // Per MultiView
                GridViewRecuperoContributi.DataSource = programmazione;
                GridViewRecuperoContributi.DataBind();

                GridViewCantieri.DataSource = programmazione;
                GridViewCantieri.DataBind();

                GridViewVerificaIndirizzi.DataSource = programmazione;
                GridViewVerificaIndirizzi.DataBind();

                GridViewImprese.DataSource = programmazione;
                GridViewImprese.DataBind();

                GridViewComuni.DataSource = programmazione;
                GridViewComuni.DataBind();

                GridViewSedi.DataSource = programmazione;
                GridViewSedi.DataBind();

                GridViewFallimenti.DataSource = programmazione;
                GridViewFallimenti.DataBind();

                GridViewVarie.DataSource = programmazione;
                GridViewVarie.DataBind();
            }
            else
                LabelErrore.Text = "Selezionare una settimana";
        else
            LabelErrore.Text = "Selezionare un ispettore";
    }

    protected static void GridViewPreventivo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            // Tengo traccia del giorno
            //Button bAggiungi = (Button)e.Row.FindControl("ButtonAggiungiAttivitaCantieri");

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Recupero contributi":
                            GridView gvRecupero = (GridView) e.Row.FindControl("GridViewRecuperoContributi");

                            if (gvRecupero.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvRecupero.DataSource;

                            listaAttivita.Add(attivita);

                            gvRecupero.DataSource = listaAttivita;
                            gvRecupero.DataBind();
                            break;
                        case "Cantieri":
                            GridView gvCantieri = (GridView) e.Row.FindControl("GridViewCantieri");

                            if (gvCantieri.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvCantieri.DataSource;

                            listaAttivita.Add(attivita);

                            gvCantieri.DataSource = listaAttivita;
                            gvCantieri.DataBind();
                            break;
                        case "Verifica indirizzi - Certificati":
                            GridView gvVerificaIndirizzi = (GridView) e.Row.FindControl("GridViewVerificaIndirizzi");

                            if (gvVerificaIndirizzi.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvVerificaIndirizzi.DataSource;

                            listaAttivita.Add(attivita);

                            gvVerificaIndirizzi.DataSource = listaAttivita;
                            gvVerificaIndirizzi.DataBind();
                            break;
                        case "Imprese e consulenti":
                            GridView gvImpreseConsulenti = (GridView) e.Row.FindControl("GridViewImpreseConsulenti");

                            if (gvImpreseConsulenti.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvImpreseConsulenti.DataSource;

                            listaAttivita.Add(attivita);

                            gvImpreseConsulenti.DataSource = listaAttivita;
                            gvImpreseConsulenti.DataBind();
                            break;
                        case "Comuni e sedi INPS-INAIL-ASL-DPL":
                            GridView gvComuniSedi = (GridView) e.Row.FindControl("GridViewComuniSedi");

                            if (gvComuniSedi.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvComuniSedi.DataSource;

                            listaAttivita.Add(attivita);

                            gvComuniSedi.DataSource = listaAttivita;
                            gvComuniSedi.DataBind();
                            break;
                        case "Sedi cassa edile":
                            GridView gvSediCassa = (GridView) e.Row.FindControl("GridViewSediCassa");

                            if (gvSediCassa.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvSediCassa.DataSource;

                            listaAttivita.Add(attivita);

                            gvSediCassa.DataSource = listaAttivita;
                            gvSediCassa.DataBind();
                            break;
                        case "Fallimenti e tribunali":
                            GridView gvFallimenti = (GridView) e.Row.FindControl("GridViewFallimenti");

                            if (gvFallimenti.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvFallimenti.DataSource;

                            listaAttivita.Add(attivita);

                            gvFallimenti.DataSource = listaAttivita;
                            gvFallimenti.DataBind();
                            break;
                        case "Varie":
                            GridView gvVarie = (GridView) e.Row.FindControl("GridViewVarie");

                            if (gvVarie.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvVarie.DataSource;

                            listaAttivita.Add(attivita);

                            gvVarie.DataSource = listaAttivita;
                            gvVarie.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void DropDownListIspettore_SelectedIndexChanged(object sender, EventArgs e)
    {
        SvuotaAttivita();
    }

    private void SvuotaAttivita()
    {
        //GridViewPreventivo.DataSource = null;
        //GridViewPreventivo.DataBind();

        GridViewRecuperoContributi.DataSource = null;
        GridViewRecuperoContributi.DataBind();
        GridViewCantieri.DataSource = null;
        GridViewCantieri.DataBind();
        GridViewVerificaIndirizzi.DataSource = null;
        GridViewVerificaIndirizzi.DataBind();
        GridViewImprese.DataSource = null;
        GridViewImprese.DataBind();
        GridViewComuni.DataSource = null;
        GridViewComuni.DataBind();
        GridViewSedi.DataSource = null;
        GridViewSedi.DataBind();
        GridViewFallimenti.DataSource = null;
        GridViewFallimenti.DataBind();
        GridViewVarie.DataSource = null;
        GridViewVarie.DataBind();

        ButtonSalvaSituazione.Enabled = false;
        ButtonReport.Enabled = false;
    }

    protected void CalendarSettimana_SelectionChanged(object sender, EventArgs e)
    {
        DateTime giornoSel = CalendarSettimana.SelectedDate;
        CalendarSettimana.SelectedDates.Clear();
        for (int i = 0; i < 7; i++)
        {
            CalendarSettimana.SelectedDates.Add(giornoSel.AddDays(i));
        }
        CalendarSettimana.DataBind();

        SvuotaAttivita();
    }

    protected void ButtonAggiungiAttivita_Click(object sender, EventArgs e)
    {
        if (DropDownListIspettore.SelectedValue != null)
        {
            int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
            DateTime dal = CalendarSettimana.SelectedDates[0];
            DateTime giorno;
            string nomeAttivita = string.Empty;

            // Recupero la tipologia di attivit�
            Button bottone = (Button) sender;
            switch (bottone.ID)
            {
                case "ButtonAggiungiAttivitaRecuperoContributi":
                    nomeAttivita = "Recupero contributi";
                    break;
                case "ButtonAggiungiAttivitaCantieri":
                    nomeAttivita = "Cantieri";
                    break;
                case "ButtonAggiungiAttivitaVerificaIndirizzi":
                    nomeAttivita = "Verifica indirizzi - Certificati";
                    break;
                case "ButtonAggiungiAttivitaImpreseConsulenti":
                    nomeAttivita = "Imprese e consulenti";
                    break;
                case "ButtonAggiungiAttivitaComuniSedi":
                    nomeAttivita = "Comuni e sedi INPS-INAIL-ASL-DPL";
                    break;
                case "ButtonAggiungiAttivitaSediCassa":
                    nomeAttivita = "Sedi cassa edile";
                    break;
                case "ButtonAggiungiAttivitaFallimenti":
                    nomeAttivita = "Fallimenti e tribunali";
                    break;
                case "ButtonAggiungiAttivitaVarie":
                    nomeAttivita = "Varie";
                    break;
                    // Resto dei bottoni
            }

            // Recupero il giorno
            giorno = DateTime.Parse(((GridViewRow) bottone.Parent.Parent).Cells[0].Text);
            string modalita;

            if (Request.QueryString["modalita"] == "preventivo")
                modalita = "prev";
            else
                modalita = "cons";

            Response.Redirect(
                string.Format(
                    "~/Cantieri/CantieriInserisciModificaProgrammazioneAttivitaGiornaliera.aspx?idIspettore={0}&giorno={1}&dal={2}&nomeAttivita={3}&modalita={4}",
                    idIspettore, giorno.ToShortDateString(), dal.ToShortDateString(), nomeAttivita, modalita));
        }
    }

    protected void GridViewAttivita_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        bool isIspettoreStandard = GestioneUtentiBiz.IsIspettore() &&
                                   GestioneUtentiBiz.Autorizzato(
                                       FunzionalitaPredefinite.CantieriConsuPrev.ToString())
                                   &&
                                   !GestioneUtentiBiz.Autorizzato(
                                        FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString())
            ;

        //controlliamo che sia ispettore e che sia passato il limite temporale entro cui un ispettore pu� modificare il rapporto

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Attivita attivita = (Attivita) e.Row.DataItem;
            CheckBox cbPrev = (CheckBox) e.Row.FindControl("CheckBoxPreventivato");
            CheckBox cbCons = (CheckBox) e.Row.FindControl("CheckBoxConsuntivato");
            Label labelImpresa = (Label) e.Row.FindControl("LabelImpresa");

            if (e.Row.Parent.Parent.ID == "GridViewCantieri")
                GestioneCompilaIspezione(e.Row, true);

            //controlliamo che sia ispettore e che sia passato il limite temporale entro cui un ispettore pu� modificare il rapporto
            bool scaduto = isIspettoreStandard &&
                           (DateTime.Today.Subtract(attivita.Giorno).Days > giorniScadenza);

            cbPrev.Checked = attivita.Preventivato;
            cbCons.Checked = attivita.Consuntivato;

            // Impresa appaltatrice
            if (attivita.Cantiere != null)
            {
                if (attivita.Cantiere.ImpresaAppaltatrice != null)
                    labelImpresa.Text = attivita.Cantiere.ImpresaAppaltatrice.RagioneSociale;
                else if (!string.IsNullOrEmpty(attivita.Cantiere.ImpresaAppaltatriceTrovata))
                    labelImpresa.Text = attivita.Cantiere.ImpresaAppaltatriceTrovata;
            }

            // Gestione del combo box (preventivato e consuntivato)
            if (Request.QueryString["modalita"] == "preventivo")
            {
                cbPrev.Enabled = true;
                cbCons.Enabled = false;

                if (attivita.Consuntivato)
                    cbPrev.Enabled = false;

                if (!attivita.Programmato && !attivita.Consuntivato)
                {
                    GestioneCancellazioneAttivita(e.Row, true);
                    cbPrev.Enabled = false;
                }
                else
                {
                    GestioneCancellazioneAttivita(e.Row, false);
                }

                if (!attivita.Programmato)
                    e.Row.BackColor = Color.LightGray;
            }
            else if (Request.QueryString["modalita"] == "consuntivo")
            {
                cbPrev.Enabled = false;
                cbCons.Enabled = true;

                if (e.Row.Parent.Parent.ID == "GridViewCantieri" && attivita.Cantiere != null)
                {
                    if (biz.GetIspezione(attivita.IdAttivita.Value) != null)
                    {
                        cbCons.Enabled = false;
                    }
                }

                if (!attivita.Programmato && !attivita.Preventivato)
                {
                    GestioneCancellazioneAttivita(e.Row, true);
                    cbCons.Enabled = false;
                }
                else
                {
                    GestioneCancellazioneAttivita(e.Row, false);
                }

                if (!attivita.Programmato && !attivita.Preventivato)
                    e.Row.BackColor = Color.LightGray;
            }

            // Gestione icone rapporto ispezione (cancellazione e report)
            if (e.Row.Parent.Parent.ID == "GridViewCantieri")
            {
                if (biz.GetIspezione(attivita.IdAttivita.Value) != null)
                {
                    if (!scaduto)
                    {
                        GestioneCancellazioneStampaIspezione(e.Row, true);
                        GestioneCompilaIspezione(e.Row, true);
                    }
                    else
                    {
                        GestioneCancellazioneIspezione(e.Row, false);
                        GestioneStampaIspezione(e.Row, true);
                        GestioneCompilaIspezione(e.Row, false);
                    }
                }
                else
                {
                    GestioneCancellazioneStampaIspezione(e.Row, false);

                    if (scaduto)
                    {
                        GestioneCompilaIspezione(e.Row, false);
                        GestioneCancellazioneIspezione(e.Row, false);
                    }
                }
            }
            else
            {
                if (scaduto)
                    GestioneCancellazioneAttivita(e.Row, false);
            }

            if (scaduto)
            {
                cbCons.Enabled = false;
                cbPrev.Enabled = false;
            }
        }
    }

    private static void GestioneCompilaIspezione(GridViewRow row, bool abilita)
    {
        row.Cells[idACCEDIRAPPORTO].Visible = abilita;
        row.Cells[idACCEDIRAPPORTO].Enabled = abilita;

        row.Cells[idACCEDIRAPPORTOFINTO].Visible = !abilita;
        row.Cells[idACCEDIRAPPORTOFINTO].Enabled = false;
    }

    private void GestioneCancellazioneStampaIspezione(GridViewRow row, bool abilita)
    {
        GestioneCancellazioneIspezione(row, abilita);

        GestioneStampaIspezione(row, abilita);
    }

    private static void GestioneCancellazioneIspezione(GridViewRow row, bool abilita)
    {
        row.Cells[idCANCELLAZIONERAPPORTO].Visible = abilita;
        row.Cells[idCANCELLAZIONERAPPORTO].Enabled = abilita;

        row.Cells[idCANCELLAZIONERAPPORTOFINTO].Visible = !abilita;
        row.Cells[idCANCELLAZIONERAPPORTOFINTO].Enabled = false;
    }

    private static void GestioneStampaIspezione(GridViewRow row, bool abilita)
    {
        row.Cells[idREPORTRAPPORTO].Visible = abilita;
        row.Cells[idREPORTRAPPORTO].Enabled = abilita;

        row.Cells[idREPORTRAPPORTOFINTO].Visible = !abilita;
        row.Cells[idREPORTRAPPORTOFINTO].Enabled = false;
    }

    private static void GestioneCancellazioneAttivita(GridViewRow row, bool abilitaCancellazione)
    {
        row.Cells[idCANCELLAZIONEATTIVITA].Visible = abilitaCancellazione;
        row.Cells[idCANCELLAZIONEATTIVITA].Enabled = abilitaCancellazione;

        row.Cells[idCANCELLAZIONEATTIVITAFINTO].Visible = !abilitaCancellazione;
        row.Cells[idCANCELLAZIONEATTIVITAFINTO].Enabled = false;
    }

    protected void GridViewAttivita_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView attivita = (GridView) sender;
        int idAttivita = (int) attivita.DataKeys[e.RowIndex].Values["IdAttivita"];
        int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
        DateTime dal = CalendarSettimana.SelectedDates[0];

        //if (((GridView)sender).Rows[e.RowIndex].Parent.Parent.ID == "GridViewCantieri" && ((GridView)sender).Rows[e.RowIndex].Cells[6].Enabled)
        //{
        //    DateTime giorno;
        //    Cantiere cantiere = (Cantiere)attivita.DataKeys[e.RowIndex].Values["Cantiere"];
        //    Ispettore ispettore = (Ispettore)attivita.DataKeys[e.RowIndex].Values["Ispettore"];
        //    // Recupero il giorno
        //    // Fa schifo, ma non sono riuscito a trovare metodo migliore...
        //    giorno = DateTime.Parse(((System.Web.UI.WebControls.TableRow)(((GridViewRow)attivita.Parent.Parent))).Cells[0].Text);

        //    biz.DeleteIspezione(giorno, cantiere.IdCantiere.Value, ispettore.IdIspettore.Value);
        //}

        Response.Redirect(
            string.Format(
                "~/Cantieri/CantieriConfermaCancellazioneAttivita.aspx?idAttivita={0}&o=precon&idIspettore={1}&dal={2}&modalita={3}",
                idAttivita, idIspettore, dal.ToShortDateString(), Request.QueryString["modalita"]));
        //biz.DeleteAttivita(idAttivita);
        //VisualizzaPreventivo();
    }

    protected void ButtonSalvaSituazione_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridViewRecuperoContributi.Rows)
        {
            // Cantieri
            GridView gvRecupero = (GridView) row.FindControl("GridViewRecuperoContributi");
            AggiornaAttivita(gvRecupero);
        }

        foreach (GridViewRow row in GridViewCantieri.Rows)
        {
            // Cantieri
            GridView gvCantieri = (GridView) row.FindControl("GridViewCantieri");
            AggiornaAttivita(gvCantieri);
        }

        foreach (GridViewRow row in GridViewVerificaIndirizzi.Rows)
        {
            // Verifica indirizzi
            GridView gvVerificaIndirizzi = (GridView) row.FindControl("GridViewVerificaIndirizzi");
            AggiornaAttivita(gvVerificaIndirizzi);
        }

        foreach (GridViewRow row in GridViewImprese.Rows)
        {
            // Imprese consulenti
            GridView gvImpreseConsulenti = (GridView) row.FindControl("GridViewImpreseConsulenti");
            AggiornaAttivita(gvImpreseConsulenti);
        }

        foreach (GridViewRow row in GridViewComuni.Rows)
        {
            // Comuni sedi
            GridView gvComuniSedi = (GridView) row.FindControl("GridViewComuniSedi");
            AggiornaAttivita(gvComuniSedi);
        }

        foreach (GridViewRow row in GridViewSedi.Rows)
        {
            // Sedi cassa edile
            GridView gvSediCassa = (GridView) row.FindControl("GridViewSediCassa");
            AggiornaAttivita(gvSediCassa);
        }

        foreach (GridViewRow row in GridViewFallimenti.Rows)
        {
            // Fallimenti e tribunali
            GridView gvFallimenti = (GridView) row.FindControl("GridViewFallimenti");
            AggiornaAttivita(gvFallimenti);
        }

        foreach (GridViewRow row in GridViewVarie.Rows)
        {
            // Varie
            GridView gvVarie = (GridView) row.FindControl("GridViewVarie");
            AggiornaAttivita(gvVarie);
        }

        if (string.IsNullOrEmpty(LabelErrore.Text))
            LabelErrore.Text = "Situazione salvata";

        LabelErrore.Visible = true;
    }

    private void AggiornaAttivita(GridView gv)
    {
        foreach (GridViewRow rowInterna in gv.Rows)
        {
            int idAttivita = (int) gv.DataKeys[rowInterna.RowIndex].Values["IdAttivita"];

            CheckBox cbPrevCons;
            if (Request.QueryString["modalita"] == "preventivo")
            {
                cbPrevCons = (CheckBox) rowInterna.FindControl("CheckBoxPreventivato");
                if (!biz.UpdateAttivitaPreventivata(idAttivita, cbPrevCons.Checked))
                    LabelErrore.Text +=
                        string.Format("Errore salvataggio attivit� {0}{1}", idAttivita, Environment.NewLine);
            }
            if (Request.QueryString["modalita"] == "consuntivo")
            {
                cbPrevCons = (CheckBox) rowInterna.FindControl("CheckBoxConsuntivato");
                if (!biz.UpdateAttivitaConsuntivata(idAttivita, cbPrevCons.Checked))
                    LabelErrore.Text +=
                        string.Format("Errore salvataggio attivit� {0}{1}", idAttivita, Environment.NewLine);
            }
        }
    }

    protected void GridViewCantieri_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView gvCantieri = sender as GridView;

        if (gvCantieri != null)
        {
            DateTime giorno;

            Cantiere cantiere = (Cantiere) gvCantieri.DataKeys[e.NewEditIndex].Values["Cantiere"];
            Ispettore ispettore =
                (Ispettore) gvCantieri.DataKeys[e.NewEditIndex].Values["Ispettore"];
            // Recupero il giorno
            // Fa schifo, ma non sono riuscito a trovare metodo migliore...
            giorno = DateTime.Parse(((TableRow) gvCantieri.Parent.Parent).Cells[0].Text);
            int idAttivita = (int) gvCantieri.DataKeys[e.NewEditIndex].Value;

            Response.Redirect(
                string.Format(
                    "~/Cantieri/CantieriRapportoIspezioneStep1.aspx?idCantiere={0}&giorno={1}&idIspettore={2}&idAttivita={3}",
                    cantiere.IdCantiere, giorno.ToShortDateString(), ispettore.IdIspettore, idAttivita));
        }
    }

    private void VisualizzaTabbing(bool visualizza)
    {
        LinkButtonCantieri.Visible = visualizza;
        LinkButtonVerificaIndirizzi.Visible = visualizza;
        LinkButtonImpreseConsulenti.Visible = visualizza;
        LinkButtonComuniSedi.Visible = visualizza;
        LinkButtonSediCassaEdile.Visible = visualizza;
        LinkButtonFallimentiTribunali.Visible = visualizza;
        LinkButtonVarie.Visible = visualizza;
    }

    protected void GridViewCantieri_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView gvCantieri = (GridView) sender;
        //int index = Convert.ToInt32(e.CommandArgument);

        int idAttivita = (int) gvCantieri.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["IdAttivita"];

        if (e.CommandName == "EliminaRapporto")
        {
            int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
            DateTime dal = CalendarSettimana.SelectedDates[0];

            Response.Redirect(
                string.Format(
                    "~/Cantieri/CantieriConfermaCancellazioneIspezione.aspx?idAttivita={0}&o=precon&idIspettore={1}&dal={2}",
                    idAttivita, idIspettore, dal.ToShortDateString()));

            //biz.DeleteIspezione(idAttivita);

            //VisualizzaPreventivo();
        }
        else if (e.CommandName == "VisualizzaReport")
        {
            RapportoIspezione isp = biz.GetIspezione(idAttivita);

            int? idIspezione = isp.IdIspezione;

            Response.Redirect(
                string.Format("~/Cantieri/CantieriIspezioneStampaReport.aspx?idIspezione={0}", idIspezione));
        }
    }

    private bool GetScaduto(DateTime giorno)
    {
        bool isIspettoreStandard = GestioneUtentiBiz.IsIspettore() &&
                                   GestioneUtentiBiz.Autorizzato(
                                       FunzionalitaPredefinite.CantieriConsuPrev.ToString())
                                   &&
                                   !GestioneUtentiBiz.Autorizzato(
                                        FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString())
            ;
        //controlliamo che sia ispettore e che sia passato il limite temporale entro cui un ispettore pu� modificare il rapporto
        bool scaduto = isIspettoreStandard && (DateTime.Today.Subtract(giorno).Days > giorniScadenza);

        return scaduto;
    }

    protected void ButtonReport_Click(object sender, EventArgs e)
    {
        int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
        DateTime dal = CalendarSettimana.SelectedDates[0];

        if (Request.QueryString["modalita"] != null)
        {
            if (Request.QueryString["modalita"] == "preventivo")
                Response.Redirect(
                    string.Format("~/ReportCantieriAttivita.aspx?tipo=preventivo&idIspettore={0}&dal={1}", idIspettore,
                                  dal.ToShortDateString()));
            else
                Response.Redirect(
                    string.Format("~/ReportCantieriAttivita.aspx?tipo=consuntivo&idIspettore={0}&dal={1}", idIspettore,
                                  dal.ToShortDateString()));
        }
        else
            Response.Redirect(
                string.Format("~/ReportCantieriAttivita.aspx?tipo=consuntivo&idIspettore={0}&dal={1}", idIspettore,
                              dal.ToShortDateString()));
    }

    #region Click bottoni tabbing

    protected void LinkButtonRecupero_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 0;
    }

    protected void ButtonCantieri_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 1;
    }

    protected void ButtonVerificaIndirizzi_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 2;
    }

    protected void ButtonImprese_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 3;
    }

    protected void ButtonComuni_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 4;
    }

    protected void ButtonSedi_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 5;
    }

    protected void ButtonFallimenti_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 6;
    }

    protected void ButtonVarie_Click(object sender, EventArgs e)
    {
        MultiViewAttivita.ActiveViewIndex = 7;
    }

    #endregion

    #region RowDataBound delle viste

    protected void GridViewRecuperoContributi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaRecuperoContributi");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Recupero contributi":
                            GridView gvRecupero = (GridView) e.Row.FindControl("GridViewRecuperoContributi");

                            if (gvRecupero.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvRecupero.DataSource;

                            listaAttivita.Add(attivita);

                            gvRecupero.DataSource = listaAttivita;
                            gvRecupero.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewCantieri_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaCantieri");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Cantieri":
                            GridView gvCantieri = (GridView) e.Row.FindControl("GridViewCantieri");

                            if (gvCantieri.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvCantieri.DataSource;

                            listaAttivita.Add(attivita);

                            gvCantieri.DataSource = listaAttivita;
                            gvCantieri.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewVerificaIndirizzi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaVerificaIndirizzi");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Verifica indirizzi - Certificati":
                            GridView gvVerificaIndirizzi = (GridView) e.Row.FindControl("GridViewVerificaIndirizzi");

                            if (gvVerificaIndirizzi.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvVerificaIndirizzi.DataSource;

                            listaAttivita.Add(attivita);

                            gvVerificaIndirizzi.DataSource = listaAttivita;
                            gvVerificaIndirizzi.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewImprese_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaImpreseConsulenti");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Imprese e consulenti":
                            GridView gvImpreseConsulenti = (GridView) e.Row.FindControl("GridViewImpreseConsulenti");

                            if (gvImpreseConsulenti.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvImpreseConsulenti.DataSource;

                            listaAttivita.Add(attivita);

                            gvImpreseConsulenti.DataSource = listaAttivita;
                            gvImpreseConsulenti.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewComuni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaComuniSedi");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Comuni e sedi INPS-INAIL-ASL-DPL":
                            GridView gvComuniSedi = (GridView) e.Row.FindControl("GridViewComuniSedi");

                            if (gvComuniSedi.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvComuniSedi.DataSource;

                            listaAttivita.Add(attivita);

                            gvComuniSedi.DataSource = listaAttivita;
                            gvComuniSedi.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewSedi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaSediCassa");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Sedi cassa edile":
                            GridView gvSediCassa = (GridView) e.Row.FindControl("GridViewSediCassa");

                            if (gvSediCassa.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvSediCassa.DataSource;

                            listaAttivita.Add(attivita);

                            gvSediCassa.DataSource = listaAttivita;
                            gvSediCassa.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewFallimenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaFallimenti");
            bAggiungi.Enabled = !scaduto;

            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Fallimenti e tribunali":
                            GridView gvFallimenti = (GridView) e.Row.FindControl("GridViewFallimenti");

                            if (gvFallimenti.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvFallimenti.DataSource;

                            listaAttivita.Add(attivita);

                            gvFallimenti.DataSource = listaAttivita;
                            gvFallimenti.DataBind();
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewVarie_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneGiornalieraIspettore progGiorn = (ProgrammazioneGiornalieraIspettore) e.Row.DataItem;

            bool scaduto = GetScaduto(progGiorn.Giorno);
            Button bAggiungi = (Button) e.Row.FindControl("ButtonAggiungiAttivitaVarie");
            bAggiungi.Enabled = !scaduto;
            if (progGiorn.ListaAttivita != null)
            {
                foreach (Attivita attivita in progGiorn.ListaAttivita)
                {
                    AttivitaCollection listaAttivita;

                    switch (attivita.NomeAttivita)
                    {
                        case "Varie":
                            GridView gvVarie = (GridView) e.Row.FindControl("GridViewVarie");

                            if (gvVarie.DataSource == null)
                                listaAttivita = new AttivitaCollection();
                            else
                                listaAttivita = (AttivitaCollection) gvVarie.DataSource;

                            listaAttivita.Add(attivita);

                            gvVarie.DataSource = listaAttivita;
                            gvVarie.DataBind();
                            break;
                    }
                }
            }
        }
    }

    #endregion
}