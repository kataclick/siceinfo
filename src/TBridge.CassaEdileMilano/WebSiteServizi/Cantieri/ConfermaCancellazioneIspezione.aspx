﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaCancellazioneIspezione.aspx.cs" Inherits="Cantieri_ConfermaCancellazioneIspezione" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuCantieriStatistiche.ascx" tagname="MenuCantieriStatistiche" tagprefix="uc2" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc2:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Conferma cancellazione ispezione" />
    <br />
    <br />
    Si è scelto di eliminare l'ispezione del <b><asp:Label ID="LabelGiorno" runat="server"></asp:Label></b> al cantiere <b><asp:Label ID="LabelCantiere" runat="server"></asp:Label></b>. Premere il tasto "Conferma" per completare la cancellazione.
    <br />
    <asp:Button ID="ButtonConferma" runat="server" Text="Conferma" Width="100px" 
        onclick="ButtonConferma_Click" />
    &nbsp;<asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla" 
        Width="100px" onclick="ButtonAnnulla_Click" />
    <br />
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante la cancellazione" Visible="false"></asp:Label>
</asp:Content>


