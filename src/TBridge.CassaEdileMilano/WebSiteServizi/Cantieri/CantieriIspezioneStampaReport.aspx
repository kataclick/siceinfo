<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriIspezioneStampaReport.aspx.cs" Inherits="CantieriIspezioneStampaReport" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/CantieriTestataIspezione.ascx" TagName="CantieriTestataIspezione"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa Rapporto Ispezione"
        titolo="Cantieri" />
    <br />
    <uc3:CantieriTestataIspezione ID="CantieriTestataIspezione1" runat="server" />
    <br />
    <asp:Button ID="ButtonStampaFoglio1" runat="server" Text="Report parte 1" OnClick="ButtonStampaFoglio1_Click"
        Width="110px" Visible="false" />
    <asp:Button ID="ButtonStampaFoglio2" runat="server" Text="Report parte 2"
        OnClick="ButtonStampaFoglio2_Click" Width="110px" Visible="false" />
    <asp:Button ID="ButtonStampaRapporto"
        runat="server" Text="Report completo" OnClick="ButtonStampaRapporto_Click" Width="120px" Visible="false" />
    <asp:Button ID="ButtonLettere"
        runat="server" Text="Lettere" Width="120px" 
        onclick="ButtonLettere_Click" />
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
    <asp:Button ID="ButtonIndietro" runat="server"
        Text="Indietro" Width="170px" OnClick="ButtonIndietro_Click" /><br />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerRapportoIspezione" runat="server" ProcessingMode="Remote"
                    Visible="False" Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
