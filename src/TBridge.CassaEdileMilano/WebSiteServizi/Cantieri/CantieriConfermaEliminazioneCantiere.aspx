<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CantieriConfermaEliminazioneCantiere.aspx.cs" Inherits="CantieriConfermaEliminazioneCantiere" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Eliminazione cantiere"
        titolo="Cantieri" />
    <br />
    <b>
        Attivit� associate
    </b>
    <asp:GridView ID="GridViewAttivita" runat="server" AutoGenerateColumns="False" DataKeyNames="IdAttivita"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="Giorno" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Giorno"
                HtmlEncode="False" >
            <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="NomeIspettore" HeaderText="Ispettore" />
            <asp:BoundField DataField="NomeAttivita" HeaderText="Tipo attivit&#224;" >
            <ItemStyle Width="100px" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna attivit� associata al cantiere
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <b>
        Nuove Attivit� associate (Calendario Attivit�)
    </b>
    <asp:GridView ID="GridViewAttivitaNuove" runat="server" AutoGenerateColumns="False" DataKeyNames="Id"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Giorno"
                HtmlEncode="False" >
            <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="NomeCompletoIspettore" HeaderText="Ispettore" />
        </Columns>
        <EmptyDataTemplate>
            Nessuna attivit� associata al cantiere
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    Scegliendo di proseguire verranno eliminate anche le attivit� associate al cantiere,
    se presenti, e gli eventuali rapporti di ispezione.<br />
    <br />
    <asp:Button ID="ButtonCancella" runat="server" OnClick="ButtonCancella_Click" Text="Elimina" />
    &nbsp;
    <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Torna indietro" />
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>

