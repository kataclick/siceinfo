using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CantieriConfermaCancellazioneAttivita : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita
            = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CantieriConfermaCancellazioneAttivita.aspx");

        //controlliamo se � una risposta
        if (!string.IsNullOrEmpty(Request.QueryString["mod"]))
        {
            //
            switch (Request.QueryString["mod"])
            {
                case "cancellato":
                    LabelTesto.Text = "La cancellazione � andata a buon fine.";
                    ButtonAnnulla.Text = "Indietro";
                    ButtonConfermaEliminazioneAttivita.Visible = false;
                    break;

                case "errore":
                    LabelTesto.Text = "La cancellazione non � andata a buon fine.";
                    ButtonAnnulla.Text = "Indietro";
                    ButtonConfermaEliminazioneAttivita.Visible = false;
                    break;
            }
        }
    }

    protected void ButtonConfermaEliminazioneAttivita_Click(object sender, EventArgs e)
    {
        int idAttivita;

        if (int.TryParse(Request.QueryString["idAttivita"], out idAttivita))
        {
            biz.DeleteIspezione(idAttivita);
            biz.DeleteAttivita(idAttivita);

            Response.Redirect("~/Cantieri/CantieriConfermaCancellazioneAttivita.aspx?mod=cancellato&o=" +
                              Request.QueryString["o"] + "&idIspettore=" + Request.QueryString["idIspettore"] + "&dal=" +
                              Request.QueryString["dal"] + "&modalita=" + Request.QueryString["modalita"]);
        }

        Response.Redirect("~/Cantieri/CantieriConfermaCancellazioneAttivita.aspx?mod=errore&o=" +
                          Request.QueryString["o"] + "&idIspettore=" + Request.QueryString["idIspettore"] + "&dal=" +
                          Request.QueryString["dal"] + "&modalita=" + Request.QueryString["modalita"]);
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        //controlliamo se � una risposta
        if (!string.IsNullOrEmpty(Request.QueryString["o"]))
        {
            //
            switch (Request.QueryString["o"])
            {
                case "prog":
                    Response.Redirect("~/Cantieri/CantieriProgrammazioneSettimanale.aspx?idIspettore=" +
                                      Request.QueryString["idIspettore"] + "&dal=" + Request.QueryString["dal"] +
                                      "&modalita=" + Request.QueryString["modalita"]);
                    break;

                case "precon":
                    Response.Redirect("~/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?idIspettore=" +
                                      Request.QueryString["idIspettore"] + "&dal=" + Request.QueryString["dal"] +
                                      "&modalita=" + Request.QueryString["modalita"]);
                    break;
            }
        }
    }
}