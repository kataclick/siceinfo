<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    CodeFile="CantieriRapportoIspezioneStep4.aspx.cs" Inherits="CantieriRapportoIspezioneStep4" Theme="CETheme2009Wide" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc5" %>
<%@ Register Src="../WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriTestataIspezione.ascx" TagName="CantieriTestataIspezione"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc5:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Rapporto di ispezione - Dati generali"
        titolo="Cantieri" />

    <script type="text/javascript"> 

        function onEndRequest(sender, e) 
        { 
            var err = e.get_error();
            e.set_errorHandled(true);
            if (err != null)
            {
                if ((err.name == 'Sys.WebForms.PageRequestManagerTimeoutException'
                     || err.name == 'Sys.WebForms.PageRequestManagerServerErrorException' )) 
                { 
                    window.alert('Attenzione, il server non risponde o la connessione � caduta');
                }
                else
                {
                    window.alert('Attenzione, errore generico');
                }
            }
        } 

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest); 

    </script>

    <br />
    <table class="standardTable">
        <tr>
            <td colspan="2">
                <center>
                    <asp:Label ID="LabelTitoloNote" runat="server" Text="Note ispettore" Font-Bold="True"></asp:Label>
                </center>
            </td>
        </tr>
        <tr>
            <td>
                Note ispettore
            </td>
            <td>
                <asp:TextBox ID="TextBoxNoteIspettore1" runat="server" TextMode="MultiLine" Width="620px"
                    MaxLength="20000" Height="80px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                Audit effettuato dalle (hh:mm)
                <asp:TextBox ID="TextBoxNote1Dalle" runat="server" MaxLength="5" Width="73px"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator9" runat="server" ErrorMessage="*" ControlToValidate="TextBoxNote1Dalle"
                    ValidationExpression="^([0-1][0-9]|[2][0-3]).([0-5][0-9])$"></asp:RegularExpressionValidator>
                alle (hh:mm)
                <asp:TextBox ID="TextBoxNote1Alle" runat="server" MaxLength="5" Width="73px"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator10" runat="server" ErrorMessage="*" ControlToValidate="TextBoxNote1Alle"
                    ValidationExpression="^([0-1][0-9]|[2][0-3]).([0-5][0-9])$"></asp:RegularExpressionValidator>
                <br />
                in data (gg/mm/aaaa)
                <asp:TextBox ID="TextBoxNote1Data" runat="server" MaxLength="10" Width="111px"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator11" runat="server" ErrorMessage="*" ControlToValidate="TextBoxNote1Data"
                    ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                in cantiere
            </td>
        </tr>
        <tr>
            <td>
                Note ispettore
            </td>
            <td>
                <asp:TextBox ID="TextBoxNoteIspettore2" runat="server" TextMode="MultiLine" Width="620px"
                    MaxLength="20000" Height="80px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                Audit successivo effettuato dalle (hh:mm)&nbsp;
                <asp:TextBox ID="TextBoxNote2Dalle" runat="server" MaxLength="5" Width="73px"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator12" runat="server" ErrorMessage="*" ControlToValidate="TextBoxNote2Dalle"
                    ValidationExpression="^([0-1][0-9]|[2][0-3]).([0-5][0-9])$"></asp:RegularExpressionValidator>
                alle (hh:mm)
                <asp:TextBox ID="TextBoxNote2Alle" runat="server" MaxLength="5" Width="73px"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator13" runat="server" ErrorMessage="*" ControlToValidate="TextBoxNote2Alle"
                    ValidationExpression="^([0-1][0-9]|[2][0-3]).([0-5][0-9])$"></asp:RegularExpressionValidator>
                <br />
                in data (gg/mm/aaaa)
                <asp:TextBox ID="TextBoxNote2Data" runat="server" MaxLength="10" Width="111px"></asp:TextBox><asp:RegularExpressionValidator
                    ID="RegularExpressionValidator14" runat="server" ErrorMessage="*" ControlToValidate="TextBoxNote2Data"
                    ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                presso
                <asp:TextBox ID="TextBoxNote2Presso" runat="server" MaxLength="100" Width="231px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonAggiungiImpresa" runat="server" OnClick="ButtonAggiungiImpresa_Click"
        Text="Aggiungi impresa alla lista" /><br />
    <asp:Panel ID="PanelAggiungiImpresa" runat="server" Visible="False"
        Width="100%">
        <uc4:CantieriRicercaImpresaUnicaFonte ID="CantieriRicercaImpresaUnicaFonteAggiungiImpresa"
            runat="server" />
    </asp:Panel>
    <table class="standardTable">
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewRapportiImpresa" runat="server" AutoGenerateColumns="False"
                    OnRowDataBound="GridViewRapportiImpresa_RowDataBound" DataKeyNames="Impresa,IdRapportoIspezioneImpresa"
                    Width="100%" OnRowDeleting="GridViewRapportiImpresa_RowDeleting">
                    <Columns>
                        <asp:BoundField DataField="NomeImpresa" HeaderText="Impresa" >
                        <ItemStyle Font-Bold="True" Width="150px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Nuova iscr.">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxNuovaIscritta" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OSS/RAC">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                OSS
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="DropDownListOSS" runat="server" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="ButtonAggiungiOSS" runat="server" Text="Aggiungi" OnClick="ButtonAggiungiOSS_Click"
                                                            Height="20px" Width="100%" />
                                                    </td>
                                                    <td>
                                                        <asp:GridView ID="GridViewOSS" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                            DataKeyNames="Descrizione" OnRowDeleting="GridViewOSS_RowDeleting" BorderColor="Black"
                                                            BorderStyle="Solid" BorderWidth="1px">
                                                            <Columns>
                                                                <asp:BoundField HeaderText="OSS" DataField="IdOSS" />
                                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText=""
                                                                    ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png">
                                                                    <ItemStyle Width="16px" />
                                                                </asp:CommandField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                Nessuna OSS
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                RAC
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="DropDownListRAC" runat="server" Width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="ButtonAggiungiRAC" runat="server" Text="Aggiungi" OnClick="ButtonAggiungiRAC_Click"
                                                            Height="20px" Width="100%" /><br />
                                                    </td>
                                                    <td>
                                                        <asp:GridView ID="GridViewRAC" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                            DataKeyNames="Descrizione" OnRowDeleting="GridViewRAC_RowDeleting" BorderColor="Black"
                                                            BorderStyle="Solid" BorderWidth="1px">
                                                            <Columns>
                                                                <asp:BoundField HeaderText="RAC" DataField="IdRAC" />
                                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText=""
                                                                    ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png" />
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                Nessuna RAC
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date (gg/mm/aaaa)">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                Da att. entro
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="TextBoxDaAttuare" runat="server" Width="70px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator0" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxDaAttuare" ForeColor="Red" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                Chiusura
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="TextBoxDataChiusura" runat="server" Width="70px"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxDataChiusura" ForeColor="Red" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Operai">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Regolari:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiRegolari" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiRegolari" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="color:Gray">
                                                Integrati:
                                            </span>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiIntegrati" runat="server" Width="30px" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="*" Enabled="false"
                                                ControlToValidate="TextBoxOperaiIntegrati" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Trasformati da PT a FT:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiTrasformati" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiTrasformati" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            In trasf. iscritti in CEMI:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiTrasferta" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorOperaiTrasferta" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiTrasferta" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Iscritti CEMI, no altre CE:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiNoAltreCE" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorOperaiNoAltreCE" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiNoAltreCE" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            In distacco:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiDistacco" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorOperaiDistacco" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiDistacco" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Con altro CCNL:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiAltroCcnl" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorOperaiAltroCcnl" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiAltroCcnl" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Regolarizzati:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxOperaiRegolarizzati" runat="server" Width="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxOperaiRegolarizzati" ForeColor="Red" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="180px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ACE/CONTRATTI">
                            <ItemTemplate>
                                <table class="standardTable">
                                <tr>
                                    <td colspan="2">
                                        <b>
                                            Casse Edili di provenienza per gli operai in trasferta
                                        </b>
                                        <asp:CustomValidator 
                                            ID="CustomValidatorCasseEdili" 
                                            runat="server" 
                                            ErrorMessage="*"
                                            ForeColor="Red" 
                                            onservervalidate="CustomValidatorCasseEdili_ServerValidate">
                                        </asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 250px">
                                        <asp:DropDownList
                                            ID="DropDownListCassaEdile"
                                            runat="server"
                                            Width="170px">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Button
                                            ID="ButtonAggiungiCassaEdile"
                                            runat="server"
                                            Text="Aggiungi"
                                            Width="100%" Height="20px" onclick="ButtonAggiungiCassaEdile_Click" />
                                    </td>
                                    <td>
                                        <asp:GridView
                                            ID="GridViewCasseEdili"
                                            runat="server"
                                            AutoGenerateColumns="False"
                                            Width="200px"
                                            DataKeyNames="IdCassaEdile"
                                            ShowHeader="False"
                                            onrowdeleting="GridViewCasseEdili_RowDeleting">
                                            <EmptyDataTemplate>
                                                Nessuna cassa edile selezionata
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:BoundField HeaderText="Cassa edile" DataField="Descrizione" />
                                                <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/pallinoX.png" 
                                                    ShowDeleteButton="True">
                                                <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <b>
                                            Contratti di provenienza per gli operai non edili
                                        </b>
                                        <asp:CustomValidator 
                                            ID="CustomValidatorContratti" 
                                            runat="server" 
                                            ErrorMessage="*"
                                            ForeColor="Red" onservervalidate="CustomValidatorContratti_ServerValidate">
                                        </asp:CustomValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">
                                        <asp:DropDownList
                                            ID="DropDownListContratto"
                                            runat="server"
                                            Width="170px">
                                        </asp:DropDownList>
                                        <br />
                                        <asp:Button
                                            ID="ButtonAggiungiContratto"
                                            runat="server"
                                            Text="Aggiungi"
                                            Width="100%" Height="20px" onclick="ButtonAggiungiContratto_Click" />
                                    </td>
                                    <td>
                                        <asp:GridView
                                            ID="GridViewContratti"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            ShowHeader="false"
                                            Width="200px" onrowdeleting="GridViewContratti_RowDeleting">
                                            <EmptyDataTemplate>
                                                Nessun contratto selezionato
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:BoundField HeaderText="Contratto" DataField="Descrizione" />
                                                <asp:CommandField ButtonType="Image" DeleteImageUrl="~/images/pallinoX.png" 
                                                    ShowDeleteButton="True">
                                                <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vestiario">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Caschi
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxCaschi" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tute
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxTute" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Scarpe
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxScarpe" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Importi">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Contrib. rec.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxContributiRecuperati" runat="server" Width="80px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                ControlToValidate="TextBoxContributiRecuperati" ForeColor="Red" ErrorMessage="*" ValidationExpression="^\d+(\,\d\d)?$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Elenchi integr.
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxElenchiIntegrativi" runat="server" Width="80px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="*"
                                                ControlToValidate="TextBoxElenchiIntegrativi" ForeColor="Red" ValidationExpression="^\d+(\,\d\d)?$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stat.">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxStatistiche" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButtonCancella" runat="server" CommandName="Delete" ImageUrl="~/images/pallinoX.png" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna impresa associata al cantiere
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:Label ID="LabelErroreCancellazione" runat="server" ForeColor="Red" Text="Errore durante la cancellazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="LinkButtonMostraNascondiLegenda" runat="server" OnClick="LinkButtonMostraNascondiLegenda_Click">Mostra legenda</asp:LinkButton><br />
            <asp:Panel ID="PanelLegenda" runat="server" Width="700px" Visible="false">
                <asp:GridView ID="GridViewOSS" runat="server" AutoGenerateColumns="False" Height="145px"
                    Width="600px">
                    <Columns>
                        <asp:BoundField DataField="idOSS" HeaderText="N&#176;">
                            <ItemStyle Width="20px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                            <ItemStyle Width="530px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemStyle Width="50px" />
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text="OSS"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="GridViewRAC" runat="server" AutoGenerateColumns="False" Width="600px"
                    ShowHeader="False">
                    <Columns>
                        <asp:BoundField DataField="idRAC" HeaderText="N&#176;">
                            <ItemStyle Width="20px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descrizione" HeaderText="Descrizione">
                            <ItemStyle Width="530px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Tipo">
                            <ItemStyle Width="50px" />
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Height="16px" Text="RAC"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <table class="standardTable">
        <%--    <tr>
        </tr>--%>
        <tr>
            <td style="width: 733px">
                <asp:Label ID="LabelRapFin" runat="server" Font-Bold="True" Text="Rapporto finale"></asp:Label>
            </td>
        </tr>
        <tr visible="false" runat="server">
            <td style="width: 733px">
                Dalla verifica finale sono emersi n�
                <asp:TextBox ID="TextBoxNumeroRilievi" runat="server" MaxLength="3" Width="63px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="*"
                    ControlToValidate="TextBoxNumeroRilievi" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                rilievi classificati
            </td>
        </tr>
        <tr visible="false" runat="server">
            <td style="width: 733px">
                n�
                <asp:TextBox ID="TextBoxNumeroOsservazioni" runat="server" MaxLength="3" Width="63px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="*"
                    ControlToValidate="TextBoxNumeroOsservazioni" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                osservazioni
            </td>
        </tr>
        <tr visible="false" runat="server">
            <td style="width: 733px">
                n�
                <asp:TextBox ID="TextBoxNumeroRAC" runat="server" MaxLength="3" Width="63px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="*"
                    ControlToValidate="TextBoxNumeroRAC" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                richieste di azione correttiva
            </td>
        </tr>
        <tr>
            <td style="width: 733px">
                Il presente rapporto viene comunicato a:
                <asp:DropDownList ID="DropDownListComunicato" runat="server" Width="223px">
                </asp:DropDownList>
                il (gg/mm/aaaa)
                <asp:TextBox ID="TextBoxComunicatoIl" runat="server" Width="152px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBoxComunicatoIl"
                    ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 733px">
            </td>
        </tr>
        <tr visible="false" runat="server">
            <td style="width: 733px">
                Esito finale verifica:
                <asp:DropDownList ID="DropDownListEsito" runat="server" Width="223px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 733px" valign="top">
                Note:
                <asp:TextBox ID="TextBoxNote" runat="server" Height="55px" TextMode="MultiLine" Width="409px"
                    MaxLength="255"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    Se sono stati completati tutti i campi premere "Salva rapporto ispezione" per completare
    il rapporto di ispezione<br />
    <asp:Button ID="ButtonPassoPrecedente" runat="server" OnClick="ButtonPassoPrecedente_Click"
        Text="Salva e torna al passo 3" Width="170px" />
    <asp:Button ID="ButtonSalva" runat="server" Text="Salva rapporto ispezione" OnClick="ButtonSalva_Click"
        Width="190px" CausesValidation="true" />
    <asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla" Width="170px" OnClick="ButtonAnnulla_Click"
        CausesValidation="true" /><br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label><br />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <uc2:CantieriTestataIspezione ID="CantieriTestataIspezione1" runat="server" />
</asp:Content>
