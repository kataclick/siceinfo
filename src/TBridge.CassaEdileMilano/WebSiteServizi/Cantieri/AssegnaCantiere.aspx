﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssegnaCantiere.aspx.cs" Inherits="Cantieri_AssegnaCantiere" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="standardDiv">
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <table class="standardTable">
            <tr>
                <td colspan="3">
                    <b>
                        <asp:Label
                            ID="LabelIndirizzo"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label
                        ID="LabelCap"
                        runat="server">
                    </asp:Label>
                    <b>
                        <asp:Label
                            ID="LabelComune"
                            runat="server">
                        </asp:Label>
                    </b>
                    <br />
                    <asp:Label
                        ID="LabelProvincia"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>
                        Cantiere/i
                    </b>
                    <asp:Repeater
                        ID="RepeaterCantieri"
                        runat="server" 
                        onitemdatabound="RepeaterCantieri_ItemDataBound">
                        <HeaderTemplate>
                            <table class="borderedTable">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label
                                        ID="LabelIdCantiere"
                                        runat="server"
                                        Visible="false">
                                    </asp:Label>
                                    <b>
                                        <asp:Label
                                            ID="LabelIndirizzo"
                                            runat="server">
                                        </asp:Label>
                                    </b>
                                </td>
                                <td>
                                    <asp:Label
                                        ID="LabelCap"
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td>
                                    <b>
                                        <asp:Label
                                            ID="LabelComune"
                                            runat="server">
                                        </asp:Label>
                                    </b>
                                </td>
                                <td>
                                    <asp:Label
                                        ID="LabelProvincia"
                                        runat="server">
                                    </asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="cantieriTd">
                    Data:
                </td>
                <td colspan="2">
                    <b>
                        <asp:Label
                            ID="LabelData"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton
                        ID="RadioButtonAssegna"
                        runat="server"
                        Text="Assegna"
                        GroupName="assegna" AutoPostBack="True" 
                        oncheckedchanged="RadioButtonAssegna_CheckedChanged" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel
                        ID="PanelAssegna"
                        runat="server"
                        Enabled="false"
                        Width="100%"
                        BorderStyle="Solid"
                        BorderColor="Gray"
                        BorderWidth="1px">
                        <table class="standardTable">
                            <tr>
                                <td class="cantieriTd">
                                    Priorità:
                                </td>
                                <td>
                                    <telerik:RadComboBox
                                        ID="RadComboBoxPriorita"
                                        runat="server"
                                        Width="150px"
                                        EmptyMessage="Seleziona la priorità" 
                                        AppendDataBoundItems="True">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:CustomValidator
                                        ID="CustomValidatorPriorita"
                                        runat="server"
                                        ValidationGroup="assegnaCantiere"
                                        ErrorMessage="Selezionare una priorità" 
                                        onservervalidate="CustomValidatorPriorita_ServerValidate">
                                        *
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="cantieriTd">
                                    Ispettori:
                                </td>
                                <td>
                                    <telerik:RadGrid
                                        ID="RadGridIspettori"
                                        runat="server"
                                        AutoGenerateColumns="False"
                                        Width="100%" GridLines="None" ShowHeader="False" 
                                        onitemcommand="RadGridIspettori_ItemCommand">
                                        <MasterTableView>
                                            <NoRecordsTemplate>
                                                Nessun ispettore selezionato
                                            </NoRecordsTemplate>
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="NomeCompleto" UniqueName="column1">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn UniqueName="elimina" ItemStyle-Width="10px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButtonElimina" runat="server" CausesValidation="false" CommandName="ELIMINA" 
                                                            Text="" ImageUrl="~/images/pallinoX.png" Width="10px" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <br />
                                    <table>
                                        <tr>
                                            <td>
                                                Aggiungi gruppo
                                            </td>
                                            <td>
                                                Aggiungi singolo
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <telerik:RadComboBox
                                                    ID="RadComboBoxGruppi"
                                                    runat="server"
                                                    AppendDataBoundItems="true"
                                                    EmptyMessage="Seleziona un gruppo"
                                                    Width="150px">
                                                </telerik:RadComboBox>
                                                <asp:Button
                                                    ID="ButtonAggiungiALista"
                                                    runat="server"
                                                    Text="Aggiungi alla lista"
                                                    Width="150px" onclick="ButtonAggiungiALista_Click" />
                                            </td>
                                            <td>
                                                <telerik:RadComboBox
                                                    ID="RadComboBoxIspettori"
                                                    runat="server"
                                                    AppendDataBoundItems="true"
                                                    EmptyMessage="Seleziona un ispettori"
                                                    Width="150px">
                                                </telerik:RadComboBox>
                                                <asp:Button
                                                    ID="ButtonAggiungiSingoloALista"
                                                    runat="server"
                                                    Text="Aggiungi alla lista"
                                                    Width="150px" onclick="ButtonAggiungiSingoloALista_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </td>
                                <td>
                                    <asp:CustomValidator
                                        ID="CustomValidatorIspettori"
                                        runat="server"
                                        ValidationGroup="assegnaCantiere"
                                        ErrorMessage="Selezionare almeno un ispettore" 
                                        onservervalidate="CustomValidatorIspettori_ServerValidate">
                                        *
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr id="trRifiuta1" runat="server">
                <td colspan="2">
                    <asp:RadioButton
                        ID="RadioButtonRifiuta"
                        runat="server"
                        Text="Rifiuta"
                        GroupName="assegna" AutoPostBack="True" 
                        oncheckedchanged="RadioButtonAssegna_CheckedChanged" />
                </td>
            </tr>
            <tr id="trRifiuta2" runat="server">
                <td colspan="2">
                    <asp:Panel
                        ID="PanelRifiuta"
                        runat="server"
                        Enabled="false"
                        Width="100%"
                        BorderStyle="Solid"
                        BorderColor="Gray"
                        BorderWidth="1px">
                        <table class="standardTable">
                            <tr>
                                <td class="style1">
                                    Motivazione:
                                </td>
                                <td>
                                    <telerik:RadComboBox
                                        ID="RadComboBoxMotivazione"
                                        runat="server"
                                        Width="150px"
                                        EmptyMessage="Seleziona la motivazione" 
                                        AppendDataBoundItems="True">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:CustomValidator
                                        ID="CustomValidatorMotivazione"
                                        runat="server"
                                        ValidationGroup="assegnaCantiere"
                                        ErrorMessage="Selezionare una motivazione" 
                                        onservervalidate="CustomValidatorMotivazione_ServerValidate">
                                        *
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>            
            <tr id="trRifiuta3" runat="server">
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:CustomValidator
                        ID="CustomValidatorAzione"
                        runat="server"
                        ValidationGroup="assegnaCantiere"
                        ErrorMessage="Selezionare un'azione" 
                        onservervalidate="CustomValidatorAzione_ServerValidate">
                        &nbsp;
                    </asp:CustomValidator>
                    <asp:Button
                        ID="ButtonAssegna"
                        runat="server"
                        Text="Assegna il cantiere"
                        Width="150px"
                        ValidationGroup="assegnaCantiere" 
                        onclick="ButtonAssegna_Click" />
                    &nbsp;<asp:Button
                        ID="ButtonElimina"
                        runat="server"
                        Text="Cancella l'assegnazione"
                        Width="150px"
                        CausesValidation="false" 
                        Visible="false"
                        onclick="ButtonElimina_Click" />
                    <br />
                    <asp:ValidationSummary
                        ID="ValidationSummarySegnalaCantiere"
                        runat="server"
                        ValidationGroup="assegnaCantiere"
                        CssClass="messaggiErrore" />
                    <br />
                    <asp:Label
                        ID="LabelMessaggio"
                        runat="server"
                        CssClass="messaggiErrore">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
