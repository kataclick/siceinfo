<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CantieriInserimentoModificaCantiere.aspx.cs" Inherits="CantieriInserimentoModificaCantiere" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc7" %>
<%@ Register Src="../WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CantieriImpresa.ascx" TagName="CantieriImpresa" TagPrefix="uc6" %>
<%@ Register Src="~/WebControls/CantieriCommittente.ascx" TagName="CantieriCommittente"
    TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/CantieriRicercaCommittente.ascx" TagName="CantieriRicercaCommittente"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register src="../WebControls/SelezioneIndirizzoLibero.ascx" tagname="IscrizioneLavoratoreIndirizzo" tagprefix="uc8" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc4:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc7:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento o modifica cantiere"
        titolo="Cantieri" />

    <script type="text/javascript">

        function onEndRequest(sender, e) {
            var err = e.get_error();
            e.set_errorHandled(true);
            if (err != null) {
                if ((err.name == 'Sys.WebForms.PageRequestManagerTimeoutException'
                     || err.name == 'Sys.WebForms.PageRequestManagerServerErrorException')) {
                    window.alert('Attenzione, il server non risponde o la connessione � caduta');
                }
                else {
                    window.alert('Attenzione, errore generico');
                }
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest); 

    </script>

    <br />
    <b>Attenzione</b>: l'inserimento o la modifica del cantiere non verr� effettuata fin quando non verr� premuto il tasto "Inserisci" o "Modifica".
    <br />
    <br />
    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px" Visible="false"></asp:Label>
    <asp:TextBox ID="TextBoxIdCantiere" runat="server" Width="100px" Enabled="False"
        ReadOnly="True" Visible="false">
    </asp:TextBox>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="standardTable">
                <tr>
                    <td colspan="3">
                        <b>Indirizzo</b>*
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc8:IscrizioneLavoratoreIndirizzo ID="IscrizioneLavoratoreIndirizzo1" 
                            runat="server" />
                    </td>
                    <td>
                        <asp:CustomValidator
                            ID="CustomValidatorIndirizzo"
                            runat="server"
                            ErrorMessage="Inserire un indirizzo"
                            ValidationGroup="InsModCantiere" 
                            onservervalidate="CustomValidatorIndirizzo_ServerValidate"
                            CssClass="messaggiErrore">
                            *
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button
                            ID="ButtonModificaIndirizzo"
                            runat="server"
                            Text="Modifica indirizzo"
                            Enabled="false" onclick="ButtonModificaIndirizzo_Click" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date e numeri</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelDataInizioLavori" runat="server" Text="Data inizio lavori (gg/mm/aaaa)"
                            Width="202px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDataInizioLavori" runat="server" Width="353px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizioLavori" runat="server"
                            ErrorMessage="Data non corretta" ControlToValidate="TextBoxDataInizioLavori"
                            ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                            ValidationGroup="InsModCantiere" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelDataFineLavori" runat="server" Text="Data fine lavori (gg/mm/aaaa)"
                            Width="204px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDataFineLavori" runat="server" Width="353px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFineLavori" runat="server"
                            ErrorMessage="Data non corretta" ControlToValidate="TextBoxDataFineLavori" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                            ValidationGroup="InsModCantiere" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelImporto" runat="server" Text="Importo"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxImporto" runat="server" Width="353px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorImporto" runat="server"
                            ControlToValidate="TextBoxImporto" ErrorMessage="Formato non valido" ValidationExpression="^[.][0-9]+$|[0-9]*[,]*[0-9]{0,2}$"
                            ValidationGroup="InsModCantiere" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelPermessoCostruire" runat="server" Text="Permesso costruire / DIA"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxPermessoCostruire" runat="server" Width="353px" MaxLength="100"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Altro</b>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="LabelTipologiaAppalto" runat="server" Text="Tipologia appalto*"></asp:Label>
                    </td>
                    <td >
                        <asp:DropDownList ID="DropDownListTipologiaAppalto" runat="server" 
                            Width="353px">
                        </asp:DropDownList>
                    </td>
                    <td >
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelAttivo" runat="server" Text="Attivo"></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBoxAttivo" runat="server" Checked="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelDirezioneLavori" runat="server" Text="Direzione lavori"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDirezioneLavori" runat="server" Width="353px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelResponsabileProcedimento" runat="server" Text="Responsabile procedimento"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxResponsabileProcedimento" runat="server" Width="353px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelResponsabileCantiere" runat="server" Text="Responsabile cantiere"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxResponsabileCantiere" runat="server" Width="353px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelDescrizioneLavori" runat="server" Text="Descrizione lavori"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDescrizioneLavori" runat="server" Width="353px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelCommittente" runat="server" Text="Committente"></asp:Label>
                    </td>
                    <td>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:TextBox ID="TextBoxCommittente" runat="server" Text="" Width="353px" Height="90px"
                                        TextMode="MultiLine" Enabled="False" ReadOnly="True" CssClass="campoDisabilitato" />
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaCommittente" runat="server" Text="Modifica dati" OnClick="ButtonModificaCommittente_Click"
                                        Enabled="False" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="ButtonSelezionaCommittente" runat="server" Text="Cambia committente"
                                        OnClick="ButtonSelezionaCommittente_Click" Width="200px" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:UpdateProgress ID="UpdateProgressCommittente" runat="server" AssociatedUpdatePanelID="UpdatePanelCommittente"
                            DisplayAfter="100">
                            <ProgressTemplate>
                                <center>
                                    <asp:Image ID="ImageCommittenteProgress" runat="server" ImageUrl="~/images/loading6b.gif" />
                                    <asp:Label ID="LabelCommittenteProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                                        Text="In elaborazione...."></asp:Label>
                                </center>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel ID="UpdatePanelCommittente" runat="server">
                            <ContentTemplate>
                                <uc2:CantieriRicercaCommittente ID="CantieriRicercaCommittente1" runat="server" Visible="false" />
                                <br />
                                <asp:Panel ID="PanelInserimentoCommittente" runat="server" Width="100%"
                                    Visible="False">
                                    <table class="filledtable">
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelTitoloInserimentoCommittente" runat="server" Font-Bold="True"
                                                    ForeColor="White" Text="Inserisci Committente"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="borderedTable">
                                        <tr>
                                            <td>
                                                <uc5:CantieriCommittente ID="CantieriCommittente1" runat="server" />
                                                <br />
                                                <asp:Button ID="ButtonInserisciCommittente" runat="server" OnClick="ButtonInserisciCommittente_Click"
                                                    Text="Inserisci committente" Width="170px" ValidationGroup="ValidaInserimentoCommittente" />
                                                &nbsp;<asp:Button ID="ButtonAnnullaInserimentoCommittente" runat="server" OnClick="ButtonAnnullaInserimentoCommittente_Click"
                                                    Text="Indietro" Width="170px" CausesValidation="False" /><br />
                                                <asp:Label ID="LabelInserimentoCommittenteRes" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelImpresaAppaltatrice" runat="server" Text="Impresa appaltatrice"></asp:Label>
                    </td>
                    <td>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:RadioButton ID="RadioButtonImpresaSiceInfo" runat="server" Text="SiceNew" Enabled="False"
                                        GroupName="scelta" />
                                    <asp:RadioButton ID="RadioButtonImpresaCantieri" runat="server" Text="Cantieri" Enabled="False"
                                        GroupName="scelta" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="TextBoxImpresaAppaltatrice" runat="server" Text="" Width="353px"
                                        Height="90px" TextMode="MultiLine" Enabled="False" ReadOnly="True" CssClass="campoDisabilitato" />
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaImpresa" runat="server" Text="Modifica dati" Enabled="False"
                                        OnClick="ButtonModificaImpresa_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="ButtonSelezionaAppaltatrice" runat="server" Text="Cambia impresa appaltatrice"
                                        OnClick="ButtonSelezionaAppaltatrice_Click" Width="200px" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:UpdateProgress ID="UpdateProgressImpresa" runat="server" AssociatedUpdatePanelID="UpdatePanelImpresa"
                            DisplayAfter="100">
                            <ProgressTemplate>
                                <center>
                                    <asp:Image ID="ImageImpresaProgress" runat="server" ImageUrl="~/images/loading6b.gif" />
                                    <asp:Label ID="LabelProgressImpresa" runat="server" Font-Bold="True" Font-Size="Medium"
                                        Text="In elaborazione...."></asp:Label>
                                </center>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel ID="UpdatePanelImpresa" runat="server">
                            <ContentTemplate>
                                <uc3:CantieriRicercaImpresaUnicaFonte ID="CantieriRicercaImpresa1" runat="server"
                                    Visible="false" />
                                &nbsp;
                                <br />
                                <asp:Panel ID="PanelInserimentoImpresa" runat="server" Width="100%"
                                    Visible="False">
                                    <table class="filledtable">
                                        <tr>
                                            <td >
                                                <asp:Label ID="LabelInserimentoNuovaImpresa" runat="server" Font-Bold="True" ForeColor="White"
                                                    Text="Inserisci Impresa"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="borderedTable">
                                        <tr>
                                            <td >
                                                <uc6:CantieriImpresa ID="CantieriImpresa1" runat="server" />
                                                <br />
                                                <asp:Button ID="ButtonInserisciImpresa" runat="server" OnClick="ButtonInserisciImpresa_Click"
                                                    Text="Inserisci impresa" Width="170px" ValidationGroup="ValidaInserimentoImpresa" />
                                                <asp:Button ID="ButtonAnnullaInserimentoImpresa" runat="server" OnClick="ButtonAnnullaInserimentoImpresa_Click"
                                                    Text="Annulla" Width="170px" CausesValidation="False" /><br />
                                                <asp:Label ID="LabelInserimentoImpresaRes" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Tipo
                    </td>
                    <td colspan="2">
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:RadioButton ID="RadioButtonDefault" runat="server" GroupName="scelta" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListTipoImpresa" runat="server" Width="300px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="RadioButtonAltro" runat="server" GroupName="scelta" Text="Altro" />
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxTipoImpresa" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            * campi obbligatori<br />
            <br />
            <asp:Button ID="ButtonOperazione" runat="server" Text="Inserisci / Aggiorna cantiere"
                OnClick="ButtonOperazione_Click" Width="170px" ValidationGroup="InsModCantiere" />
            <asp:Button ID="ButtonAnnulla" runat="server" CausesValidation="False" OnClick="ButtonAnnulla_Click"
                Text="Annulla" Width="170px" /><br />
            <asp:ValidationSummary
                ID="ValidationSummeryCantiere"
                runat="server"
                ValidationGroup="InsModCantiere"
                CssClass="messaggiErrore" />
            <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
