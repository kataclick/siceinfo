﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Business;

public partial class Cantieri_RifiutaAttivita : System.Web.UI.Page
{
    private readonly BusinessEF bizEF = new BusinessEF();

    private Int32 idAttivita 
    {
        get 
        {
            return (Int32)ViewState["IdAttivita"];
        }

        set 
        {
            ViewState["IdAttivita"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaMotivazioniRifiuto();

            idAttivita = Int32.Parse(Request.QueryString["idAttivita"]);
        }
    }

    private void CaricaMotivazioniRifiuto()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxMotivazione,
            bizEF.GetRifiutiCalendarioAttivita(),
            "Descrizione",
            "Id");
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                bizEF.UpdateAttivita(idAttivita, Int32.Parse(RadComboBoxMotivazione.SelectedValue));
                ConfermaInserimento();
            }
            catch
            {
                ErroreNellInserimento();
            }
        }
    }

    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante il salvataggio.";
    }

    private void ConfermaInserimento()
    {
        RadComboBoxMotivazione.Enabled = false;
        ButtonSalva.Enabled = false;

        LabelMessaggio.Text = "Salvataggio effettuato.";
    }
}