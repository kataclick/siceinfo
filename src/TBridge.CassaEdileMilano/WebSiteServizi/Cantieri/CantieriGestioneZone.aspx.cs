using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class CantieriGestioneZone : Page
{
    private const int idCellaElimina = 4;

    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);

        CaricaZone();
    }

    private void CaricaZone()
    {
        ZonaCollection listaZone = biz.GetZone(null);

        GridViewZone.DataSource = listaZone;
        GridViewZone.DataBind();
    }

    protected void GridViewZone_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Zona zona = (Zona) e.Row.DataItem;

            // Non permetto la cancellazione se la zona � associata ad un ispettore..

            if (zona.IdZona.HasValue && biz.ZonaAssociata(zona.IdZona.Value))
                e.Row.Cells[idCellaElimina].Enabled = false;

            GridView gvCap = (GridView) e.Row.FindControl("GridViewCap");
            gvCap.DataSource = zona.ListaCap;
            gvCap.DataBind();
        }
    }

    protected void GridViewZone_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int idZona = (int) GridViewZone.DataKeys[e.NewEditIndex].Value;

        Response.Redirect(
            string.Format("~/Cantieri/CantieriInserimentoModificaZona.aspx?modalita=modifica&idZona={0}", idZona));
    }

    protected void ButtonNuova_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cantieri/CantieriInserimentoModificaZona.aspx");
    }

    protected void GridViewZone_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idZona = (int) GridViewZone.DataKeys[e.RowIndex].Value;

        if (!biz.ZonaAssociata(idZona))
        {
            if (!biz.DeleteZona(idZona))
                LabelRisultato.Text = "Errore nella cancellazione";
            else
                LabelRisultato.Text = string.Empty;
        }
        else
        {
            LabelRisultato.Text = "Non � possibile cancellare la zona perch� associata ad almeno un ispettore";
        }

        CaricaZone();
    }

    protected void GridViewZone_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewZone.PageIndex = e.NewPageIndex;
        GridViewZone.DataBind();
    }

    protected void GridViewCap_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CAP cap = (CAP) e.Row.DataItem;
            GridView gvComuni = (GridView) e.Row.FindControl("GridViewComuni");

            gvComuni.DataSource = cap.ListaComuni;
            gvComuni.DataBind();
        }
    }
}