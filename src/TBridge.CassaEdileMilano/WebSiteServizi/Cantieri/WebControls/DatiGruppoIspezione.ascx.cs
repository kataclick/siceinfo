﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Presenter;

public partial class Cantieri_WebControls_DatiGruppoIspezione : System.Web.UI.UserControl
{
    private readonly BusinessEF bizEF = new BusinessEF();

    protected List<Ispettore> Ispettori
    {
        set 
        {
            ViewState["Ispettori"] = value;
        }
        get 
        {
            if (ViewState["Ispettori"] == null)
            {
                ViewState["Ispettori"] = new List<Ispettore>();
            }

            return (List<Ispettore>)ViewState["Ispettori"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaIspettori();
            CaricaListaIspettori();
        }
    }

    private void CaricaListaIspettori()
    {
        Presenter.CaricaElementiInListView(
            RadListViewIspettori,
            Ispettori);
    }


    private void CaricaIspettori()
    {
        List<Ispettore> ispettori = bizEF.GetIspettori();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxIspettori,
            ispettori,
            "NomeCompleto",
            "IdIspettore");
    }

    protected void RadListViewIspettori_ItemDataBound(object sender, Telerik.Web.UI.RadListViewItemEventArgs e)
    {
        if (e.Item is RadListViewDataItem)
        {
            Ispettore ispettore = (Ispettore)((Telerik.Web.UI.RadListViewDataItem)(e.Item)).DataItem;
            Label lIspettore = (Label)e.Item.FindControl("LabelIspettore");

            lIspettore.Text = ispettore.NomeCompleto;
        }
    }

    public CantieriGruppoIspezione CreaGruppoIspezione()
    {
        CantieriGruppoIspezione gruppo = new CantieriGruppoIspezione();

        gruppo.Descrizione = RadTextBoxDescrizione.Text.Trim().ToUpper();
        gruppo.Ispettori = Ispettori;

        return gruppo;
    }

    protected void CustomValidatorIspettori_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (Ispettori.Count > 0)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void ButtonAggiungiIspettore_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(RadComboBoxIspettori.SelectedValue))
        {
            Int32 idIspettore = Int32.Parse(RadComboBoxIspettori.SelectedValue);
            Ispettore ispettore = bizEF.GetIspettore(idIspettore);

            AggiungiIspettoreALista(ispettore);
            CaricaListaIspettori();
        }
    }

    private void AggiungiIspettoreALista(Ispettore ispettore)
    {
        List<Ispettore> ispettori = this.Ispettori;

        Boolean trovato = false;
        foreach (Ispettore ispettoreLista in ispettori)
        {
            if (ispettoreLista.IdIspettore == ispettore.IdIspettore)
            {
                trovato = true;
                break;
            }
        }

        if (!trovato)
        {
            ispettori.Add(ispettore);
        }

        this.Ispettori = ispettori;
    }

    public void Reset()
    {
        Ispettori = new List<Ispettore>();
        CaricaListaIspettori();
        Presenter.SvuotaCampo(RadTextBoxDescrizione);
    }
}