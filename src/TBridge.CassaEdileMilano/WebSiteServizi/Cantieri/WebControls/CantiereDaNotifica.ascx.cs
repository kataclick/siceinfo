﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Type.Delegates;

public partial class Cantieri_WebControls_CantiereDaNotifica : System.Web.UI.UserControl
{
    public event CantiereGeneratoEventHandler OnCantiereGenerato;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Visualizzazione in base alla funzionalità
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGenerazioneDaNotifiche)
            && ViewState["nonVisualizzazione"] == null)
        {
            PanelGlobale.Visible = true;
        }
        else
        {
            PanelGlobale.Visible = false;
        }
        #endregion

        #region Codice per gestire il refresh da Javascript
        if (Page.IsPostBack)    
        {
            // Verifico se è un Refresh fatto con Javascript
            String eventArg = Request["__EVENTARGUMENT"];

            if (eventArg != null)
            {
                int offset = eventArg.IndexOf("@@@@@");

                if (offset > -1)
                {
                    // Refresh
                    if (OnCantiereGenerato != null)
                    {
                        OnCantiereGenerato();
                    }
                }
            }
        }
        #endregion
    }

    public void CaricaCantieri(IndirizzoCollection indirizzi, Int32 notificaRiferimento)
    {
        ViewState["NotificaRiferimento"] = notificaRiferimento;

        Presenter.CaricaElementiInGridView(
            GridViewCantieri,
            indirizzi,
            0);

        String comandoSenzaSegnalazione = String.Format(
                            "openRadWindowGeneraCantiere('{0}', false); return false;",
                            notificaRiferimento.ToString());

        String comandoConSegnalazione = String.Format(
                            "openRadWindowGeneraCantiere('{0}', true); return false;",
                            notificaRiferimento.ToString());

        ButtonGenera.Attributes.Add("OnClick", comandoSenzaSegnalazione);
        ButtonGeneraSegnala.Attributes.Add("OnClick", comandoConSegnalazione);
    }

    protected String RecuperaPostBackCode()
    {
        return this.Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
    }

    public void ForzaNonVisualizzazione()
    {
        ViewState["nonVisualizzazione"] = true;
        PanelGlobale.Visible = false;
    }
}