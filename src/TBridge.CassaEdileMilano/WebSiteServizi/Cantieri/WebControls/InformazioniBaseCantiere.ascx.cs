﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;

public partial class Cantieri_WebControls_InformazioniBaseCantiere : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaCantiere(TBridge.Cemi.Cantieri.Type.Entities.Cantiere cantiere)
    {
        MultiViewCantiere.ActiveViewIndex = 1;

        LabelIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
        if (!String.IsNullOrEmpty(cantiere.Cap))
        {
            LabelCap.Text = String.Format("{0} ", cantiere.Cap);
        }
        LabelComune.Text = cantiere.Comune;
        LabelProvincia.Text = cantiere.Provincia;
    }

    public void CaricaCantiere(TBridge.Cemi.Type.Domain.Cantiere cantiere)
    {
        MultiViewCantiere.ActiveViewIndex = 1;

        LabelIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Numero);
        if (!String.IsNullOrEmpty(cantiere.Cap))
        {
            LabelCap.Text = String.Format("{0} ", cantiere.Cap);
        }
        LabelComune.Text = cantiere.Comune;
        LabelProvincia.Text = cantiere.Provincia;
    }

    public void Reset()
    {
        MultiViewCantiere.ActiveViewIndex = 0;

        Presenter.SvuotaCampo(LabelIndirizzo);
        Presenter.SvuotaCampo(LabelCap);
        Presenter.SvuotaCampo(LabelComune);
        Presenter.SvuotaCampo(LabelProvincia);
    }
}