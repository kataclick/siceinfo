﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelezioneIndirizziNotificaRegionale.ascx.cs" Inherits="Cantieri_WebControls_SelezioneIndirizziNotificaRegionale" %>

<div class="standardDiv">
    <b>
        Indirizzo su cui creare il cantiere
    </b>
    <asp:RadioButtonList ID="RadioButtonListIndirizzi" runat="server">
    </asp:RadioButtonList>
</div>