﻿<%@ Control AutoEventWireup="true" CodeFile="RicercaCantieriPerIspettori.ascx.cs"
    Inherits="Cantieri_WebControls_RicercaCantieriPerIspettori" Language="C#" %>
<asp:Panel ID="PanelRicercaCantieri" runat="server" DefaultButton="ButtonVisualizza">
    <div id="divFiltriRicerca" runat="server" class="standardDiv">
        <table class="standardTable">
            <tr>
                <td>
                    Rag. soc. impresa
                </td>
                <td>
                    Committente
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <%--<telerik:RadComboBox ID="DropDownListZone" runat="server" AppendDataBoundItems="True"
                                Width="100%">
                            </telerik:RadComboBox>--%>
                    <telerik:RadTextBox ID="TextBoxImpresa" runat="server" MaxLength="255" Width="100%">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxCommittente" runat="server" MaxLength="255" Width="100%">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia
                </td>
                <td>
                    Comune
                </td>
                <td>
                    Indirizzo
                </td>
                <td>
                    Importo&gt;
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="TextBoxProvincia" runat="server" Width="100%" MaxLength="2">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxComune" runat="server" Width="100%" MaxLength="100">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxIndirizzo" runat="server" Width="100%" MaxLength="255">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxImporto" runat="server" Width="100%"
                        MaxLength="20" Type="Currency" MaxValue="100000000" MinValue="0">
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Stato assegnazione
                </td>
                <td>
                    Stato presa in carico
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadComboBox ID="DropDownListStatoAssegnazione" runat="server" Width="100%">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="Tutti" />
                            <telerik:RadComboBoxItem Text="Assegnati a me" Value="AssegnatiMe" Selected="True" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadComboBox ID="DropDownListStatoPresaCarico" runat="server" Width="100%">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="Tutti" />
                            <telerik:RadComboBoxItem Text="Presi in carico" Value="PresiCarico" />
                            <telerik:RadComboBoxItem Text="Non presi in carico" Value="NonPresiCarico" Selected="True" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click"
                        Width="100px" />
                </td>
            </tr>
        </table>
    </div>
    <div class="standardDiv">
        <asp:Label ID="LabelElencoCantieri" runat="server" Font-Bold="True" Text="Elenco cantieri"></asp:Label>
        <br />
        <telerik:RadGrid ID="RadGridCantieri" runat="server" Width="100%" GridLines="None"
            AllowPaging="True" OnItemDataBound="RadGridCantieri_ItemDataBound" OnPageIndexChanged="RadGridCantieri_PageIndexChanged"
            OnItemCommand="RadGridCantieri_ItemCommand" PageSize="5" OnDataBinding="RadGridCantieri_DataBinding">
            <MasterTableView DataKeyNames="IdCantiere, IndirizzoMappa">
                <Columns>
                    <telerik:GridTemplateColumn HeaderText="Indirizzo" UniqueName="indirizzo">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelIndirizzo" runat="server">
                                            </asp:Label>
                                            <br />
                                            <asp:Label ID="LabelCap" runat="server">
                                            </asp:Label>
                                            <asp:Label ID="LabelComune" runat="server">
                                            </asp:Label>
                                            <asp:Label ID="LabelProvincia" runat="server">
                                            </asp:Label>
                                        </b>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="ImageButtonMappa" runat="server" ImageUrl="~/images/maps.gif"
                                            CommandName="mappa" CommandArgument="<%# Container.RowIndex %>" ToolTip="Vedi su Mappa" />
                                        <br />
                                        <asp:ImageButton ID="ImageButtonSchedaCantiere" runat="server" ImageUrl="~/images/pdf24.png"
                                            CommandName="scheda" CommandArgument="<%# Container.RowIndex %>" ToolTip="Scheda Cantiere" />
                                    </td>
                                </tr>
                                <tr id="trNote" runat="server">
                                    <td colspan="2">
                                        <table class="borderedTable">
                                            <tr>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonSegnalazione" runat="server" ImageUrl="~/images/segnalazioneBN.png"
                                                        CommandName="segnala" CommandArgument="<%# Container.RowIndex %>" ToolTip="Segnalazione" />
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonAssegnazione" runat="server" ImageUrl="~/images/assegnatoBN.png"
                                                        CommandName="assegna" CommandArgument="<%# Container.RowIndex %>" ToolTip="Assegnazione" />
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonProgrammazione" runat="server" ImageUrl="~/images/programmatoBN.png"
                                                        CommandName="programma" CommandArgument="<%# Container.RowIndex %>" ToolTip="Programmazione"
                                                        Enabled="false" />
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonIspezione" runat="server" ImageUrl="~/images/ispezioneBN.png"
                                                        CommandName="ispeziona" CommandArgument="<%# Container.RowIndex %>" ToolTip="Ispezione"
                                                        Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataSegnalazione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataAssegnazione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataProgrammazione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataIspezione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="220px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="Importo(€)" DataField="Importo" SortExpression="Importo"
                        DataFormatString="{0:C}" HtmlEncode="False" UniqueName="importo">
                        <ItemStyle HorizontalAlign="Right" Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="impresa">
                        <ItemTemplate>
                            <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Committente" UniqueName="committente">
                        <ItemTemplate>
                            <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="TemplateColumn">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonModifica" ImageUrl="~/images/edit.png" runat="server"
                                CausesValidation="false" CommandName="modifica" Text="Modifica" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Panel>
