﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using System.Text;
using Telerik.Web.UI;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Presenter;

public partial class Cantieri_WebControls_SubappaltiCantiere : System.Web.UI.UserControl
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly TSBusiness tsbiz = new TSBusiness();
    private RapportoIspezione ispezione;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region impostiamo eventi e Biz dei controlli

        //Registriamo gli eventi del controllo
        CantieriRicercaImpresaAppaltataDa.OnImpresaSelected +=
            CantieriRicercaImpresa1_OnImpresaSelected;
        CantieriRicercaImpresaAppaltataDa.OnNuovaImpresaSelected +=
            CantieriRicercaImpresaAppaltataDa_OnNuovaImpresaSelected;
        CantieriRicercaImpresaSubappaltatrice.OnImpresaSelected +=
            CantieriRicercaImpresa2_OnImpresaSelected;
        CantieriRicercaImpresaSubappaltatrice.OnNuovaImpresaSelected +=
            CantieriRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected;

        //Forniamo al controllo le logiche di gestione
        CantieriImpresaAppaltataDa.CantieriBiz = biz;
        CantieriImpresaAppaltataDa.TsBiz = tsbiz;
        //Forniamo al controllo le logiche di gestione
        CantieriImpresaSubappaltatrice.CantieriBiz = biz;
        CantieriImpresaSubappaltatrice.TsBiz = tsbiz;

        CantieriImpresaModificaSubappaltatrice.CantieriBiz = biz;
        CantieriImpresaModificaSubappaltatrice.TsBiz = tsbiz;

        CantieriImpresaModificaAppaltata.CantieriBiz = biz;
        CantieriImpresaModificaAppaltata.TsBiz = tsbiz;

        #endregion

        #region Settiamo i validation group

        //Settiamo il validation group cross-control
        string validationGroupImpresaAppaltataDa = "validationGroupImpresaAppaltataDa";
        ButtonInserisciNuovaImpresaAppaltataDa.ValidationGroup = validationGroupImpresaAppaltataDa;
        CantieriImpresaAppaltataDa.SetValidationGroup(validationGroupImpresaAppaltataDa);

        string validationGroupImpresaSubappaltatrice = "validationGroupImpresaSubappaltatrice";
        ButtonInserisciNuovaImpresaSubappaltatrice.ValidationGroup = validationGroupImpresaSubappaltatrice;
        CantieriImpresaSubappaltatrice.SetValidationGroup(validationGroupImpresaSubappaltatrice);

        string validationGroupModificaSubappaltatrice = "validationGroupModificaSubappaltatrice";
        ButtonModificaImpresaSubappaltatrice.ValidationGroup = validationGroupModificaSubappaltatrice;
        CantieriImpresaModificaSubappaltatrice.SetValidationGroup(validationGroupModificaSubappaltatrice);

        //string validationGroupModificaSubappaltata = "validationGroupModificaSubappaltata";
        ButtonModificaImpresaSubappaltata.ValidationGroup = validationGroupModificaSubappaltatrice;
        CantieriImpresaModificaAppaltata.SetValidationGroup(validationGroupModificaSubappaltatrice);

        #endregion

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();

        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresaAppaltataDa') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovaImpresaAppaltataDa, null) + ";");
        sb.Append("return true;");
        ButtonInserisciNuovaImpresaAppaltataDa.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciNuovaImpresaAppaltataDa);

        ////resettiamo
        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresaSubappaltatrice') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovaImpresaSubappaltatrice, null) + ";");
        sb.Append("return true;");
        ButtonInserisciNuovaImpresaSubappaltatrice.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciNuovaImpresaSubappaltatrice);

        #endregion
    }

    public void CaricaCantiere(Int32 idCantiere)
    {
        Cantiere cantiere = biz.GetCantiere(idCantiere);
        SubappaltoCollection subappalti = biz.GetSubappalti(idCantiere);

        if (subappalti != null)
        {
            ViewState["subappalti"] = subappalti;
        }
        else
        {
            ViewState["subappalti"] = new SubappaltoCollection();
        }
        ViewState["impresaDetentriceAppalto"] = cantiere.ImpresaAppaltatrice;
        ViewState["IdCantiere"] = idCantiere;

        CaricaTipologieContratto(DropDownListTipoContratto);
        CaricaSubappalti();
    }

    public SubappaltoCollection GetSubappalti()
    {
        if (ViewState["subappalti"] != null)
        {
            AggiornaTipiContratto();

            return (SubappaltoCollection)ViewState["subappalti"];
        }
        else
        {
            return null;
        }
    }

    private void AggiornaTipiContratto()
    {
        SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];

        for (int i = 0; i < subappalti.Count; i++)
        {
            DropDownList dropTipoContratto =
                (DropDownList)RadGridSubappalti.MasterTableView.Items[i].FindControl("DropDownListTipoContratto");
            subappalti[i].TipoContratto =
                (TipologiaContratto)Enum.Parse(typeof(TipologiaContratto), dropTipoContratto.SelectedValue);
        }
    }

    private static void CaricaTipologieContratto(DropDownList dropTipoContratto)
    {
        dropTipoContratto.DataSource = Enum.GetNames(typeof(TipologiaContratto));
        dropTipoContratto.SelectedIndex = 0;
        dropTipoContratto.DataBind();
    }

    private void CaricaSubappalti()
    {
        if (ViewState["subappalti"] != null)
        {
            Presenter.CaricaElementiInGridView(
                RadGridSubappalti,
                (SubappaltoCollection)ViewState["subappalti"]);

            #region carica dropdown list imprese

            ImpresaCollection imprese = new ImpresaCollection();

            if (ViewState["impresaDetentriceAppalto"] != null)
            {
                Impresa impresaDetentriceAppalto = (Impresa)ViewState["impresaDetentriceAppalto"];
                imprese.Add(impresaDetentriceAppalto);
                //DropDownListImpresa.Items.Add(new ListItem(appaltatrice.RagioneSociale, ((int)appaltatrice.TipoImpresa).ToString() + "|" + appaltatrice.IdImpresa.ToString()));
            }

            if (ViewState["subappalti"] != null)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];

                foreach (Subappalto sub in subappalti)
                {
                    if (!imprese.Contains(sub.SubAppaltata))
                        imprese.Add(sub.SubAppaltata);
                }
            }

            DropDownListImpresa.DataSource = imprese;

            DropDownListImpresa.DataTextField = "RagioneSociale";
            DropDownListImpresa.DataValueField = "IdImpresaComposto";

            DropDownListImpresa.DataBind();

            #endregion
        }
    }

    protected void ButtonAltro_Click(object sender, EventArgs e)
    {
        //facciamo sparire la dropdown
        PanelImpresaDropDown.Visible = false;
        //rendiamo visibile il pannello 
        PanelRicercaImpresaNuovaImpresa.Visible = true;
        //rendiamo visibile i corretti elementi dentro il pannello
        CantieriRicercaImpresaAppaltataDa.Visible = true;
        PanelInserisciImpresaAppaltataDa.Visible = false;
    }

    protected void ButtonSelezionaImpresaDaDropDownList_Click(object sender, EventArgs e)
    {
        if (DropDownListImpresa.SelectedItem != null)
        {
            string ragioneSociale = DropDownListImpresa.SelectedItem.Text;
            int idImpresa;
            int tipoImpresa;

            Impresa.SplitIdImpresaComposto(DropDownListImpresa.SelectedItem.Value, out idImpresa, out tipoImpresa);
            Impresa imp =
                new Impresa((TipologiaImpresa)tipoImpresa, idImpresa, ragioneSociale, string.Empty, string.Empty,
                            string.Empty, string.Empty, string.Empty, string.Empty);

            ImpresaAppaltataDaSelezionata(imp);
        }
    }

    protected void ButtonModificaAppaltatrice_Click(object sender, EventArgs e)
    {
        PanelAppaltataDa.Visible = false;
        PanelAppaltataDaModifica.Visible = true;

        CantieriImpresaModificaSubappaltatrice.Reset();
        Impresa impresa = (Impresa)ViewState["impresaAppaltatrice"];
        CantieriImpresaModificaSubappaltatrice.ImpostaImpresa(impresa);
    }

    protected void ButtonModificaImpresaSubappaltatriceAnnulla_Click(object sender, EventArgs e)
    {
        PanelAppaltataDa.Visible = true;
        PanelAppaltataDaModifica.Visible = false;
    }

    protected void ButtonModificaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        Page.Validate("validationGroupModificaSubappaltatrice");

        if (Page.IsValid)
        {
            Impresa impresa = CantieriImpresaModificaSubappaltatrice.impresa;
            bool res = true;

            if (impresa != null)
            {
                if (impresa.FonteNotifica && !impresa.Modificato)
                {
                    res = biz.UpdateImpresaIspettore(impresa);
                }

                if (impresa.IdImpresa > 0 && res)
                {
                    ImpresaAppaltataDaSelezionata(impresa);
                    PanelAppaltataDa.Visible = true;
                    PanelAppaltataDaModifica.Visible = false;
                }
                else
                    LabelModificaImpresaSubappaltatriceErrore.Text = "Si è verificato un errore durante l'aggiornamento";
            }
            else
                LabelModificaImpresaSubappaltatriceErrore.Text = CantieriImpresaModificaSubappaltatrice.Errore;
        }
    }

    protected void ButtonModificaImpresaAppaltata_Click(object sender, EventArgs e)
    {
        PanelAppaltata.Visible = false;
        PanelAppaltataModifica.Visible = true;

        CantieriImpresaModificaAppaltata.Reset();
        Impresa impresa = (Impresa)ViewState["impresaAppaltata"];
        CantieriImpresaModificaAppaltata.ImpostaImpresa(impresa);
    }

    protected void ButtonModificaImpresaSubappaltataAnnulla_Click(object sender, EventArgs e)
    {
        PanelAppaltata.Visible = true;
        PanelAppaltataModifica.Visible = false;
    }

    protected void ButtonModificaImpresaSubappaltata_Click(object sender, EventArgs e)
    {
        Page.Validate("validationGroupModificaSubappaltata");

        if (Page.IsValid)
        {
            Impresa impresa = CantieriImpresaModificaAppaltata.impresa;
            bool res = true;

            if (impresa != null)
            {
                if (impresa.FonteNotifica && !impresa.Modificato)
                {
                    res = biz.UpdateImpresaIspettore(impresa);
                }

                if (impresa.IdImpresa > 0 && res)
                {
                    ImpresaSubappaltatriceSelezionata(impresa);
                    PanelAppaltata.Visible = true;
                    PanelAppaltataModifica.Visible = false;
                }
                else
                    LabelModificaImpresaSubappaltataErrore.Text = "Si è verificato un errore durante l'aggiornamento";
            }
            else
                LabelModificaImpresaSubappaltataErrore.Text = CantieriImpresaModificaAppaltata.Errore;
        }
    }

    #region gestione appalti

    protected void ButtonAggiungiAppalto_Click(object sender, EventArgs e)
    {
        Page.Validate("aggiungiAppalto");

        if (Page.IsValid)
        {
            if (ViewState["subappalti"] != null)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                Impresa appaltatrice = (Impresa) ViewState["impresaAppaltatrice"];
                Impresa appaltata = (Impresa) ViewState["impresaAppaltata"];
                Int32 idCantiere = (Int32)ViewState["IdCantiere"];
                Int32? indiceAggiornamento = (Int32?)ViewState["IndiceAggiornamento"];

                Subappalto subappalto =
                    new Subappalto(indiceAggiornamento.HasValue ? subappalti[indiceAggiornamento.Value].IdSubappalto : null, idCantiere, appaltatrice, appaltata,
                                   (TipologiaContratto) DropDownListTipoContratto.SelectedIndex);

                if (!indiceAggiornamento.HasValue)
                {
                    subappalti.Add(subappalto);
                }
                else
                {
                    subappalti[indiceAggiornamento.Value] = subappalto;
                }

                ViewState["impresaAppaltatrice"] = null;
                ViewState["impresaAppaltata"] = null;
                ViewState["IndiceAggiornamento"] = null;
                TextBoxImpresaAppaltata.Text = string.Empty;
                TextBoxImpresaAppaltatrice.Text = string.Empty;

                ViewState["subappalti"] = subappalti;

                CaricaSubappalti();

                PanelAppalti.Visible = false;
            }
        }
    }

    protected void ButtonVisualizzaAppalto_Click(object sender, EventArgs e)
    {
        PanelAppalti.Visible = true;
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        ViewState["IndiceAggiornamento"] = null;
        PanelAppalti.Visible = false;
    }

    #endregion

    #region eventi inserimento nuova impresa

    protected void ButtonInserisciNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        Impresa impresa = CantieriImpresaAppaltataDa.impresa;

        if (impresa != null)
        {
            // Inserisco il record
            biz.InsertImpresa(impresa);

            if (impresa.IdImpresa > 0)
            {
                ImpresaAppaltataDaSelezionata(impresa);
                CantieriImpresaAppaltataDa.Reset();
            }
            else
                LabelNuovaAppaltataDaRes.Text = "Si è verificato un errore durante l'inserimento";
        }
        else
            LabelNuovaAppaltataDaRes.Text = CantieriImpresaAppaltataDa.Errore; //"Non tutti i campi sono corretti";
    }

    protected void ButtonAnnullaNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        //
        CantieriRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaAppaltataDa.Visible = false;
    }

    protected void ButtonInserisciNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        Impresa impresa = CantieriImpresaSubappaltatrice.impresa;

        if (impresa != null)
        {
            // Inserisco il record
            biz.InsertImpresa(impresa);

            if (impresa.IdImpresa > 0)
            {
                ImpresaSubappaltatriceSelezionata(impresa);
                CantieriImpresaSubappaltatrice.Reset();
            }
            else
                LabelNuovaSubappaltatriceRes.Text = "Si è verificato un errore durante l'inserimento";
        }
        else
            LabelNuovaSubappaltatriceRes.Text = CantieriImpresaSubappaltatrice.Errore;
        //= "Non tutti i campi sono corretti";
    }

    protected void ButtonAnnullaNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        //facciamo sparire la dropdown
        PanelImpresaDropDown.Visible = false;
        //rendiamo visibile il pannello 
        PanelRicercaImpresaNuovaImpresa.Visible = false;
        //rendiamo visibile i corretti elementi dentro il pannello
        CantieriRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = false;
    }

    #endregion

    #region Eventi sui controlli di ricerca inserimento

    protected void ButtonSelezionaAppaltatrice_Click(object sender, EventArgs e)
    {
        PanelImpresaDropDown.Visible = true;
        PanelRicercaImpresaNuovaImpresa.Visible = false;
        //CantieriRicercaImpresaAppaltataDa.Visible = true;
        //PanelInserisciImpresaAppaltataDa.Visible = false;
    }

    protected void ButtonSelezionaAppaltata_Click(object sender, EventArgs e)
    {
        CantieriRicercaImpresaSubappaltatrice.Visible = true;
        PanelInserisciImpresaSubappaltata.Visible = false;
    }

    private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        ImpresaAppaltataDaSelezionata(impresa);
    }

    private void ImpresaAppaltataDaSelezionata(Impresa impresa)
    {
        TextBoxImpresaAppaltatrice.Text = impresa.NomeCompleto;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaAppaltatriceSiceInfo.Checked = true;
        else
            RadioButtonImpresaAppaltatriceCantieri.Checked = true;
        ViewState["impresaAppaltatrice"] = impresa;

        //nascondiamo gli elementi dentro il pannello PanelRicercaImpresaNuovaImpresa
        CantieriRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaAppaltataDa.Visible = false;
        //nascondiamo il pannello
        PanelRicercaImpresaNuovaImpresa.Visible = false;
        //nascondiamo il pannello per la scelta tramite dropdown
        PanelImpresaDropDown.Visible = false;

        if (impresa.FonteNotifica && !impresa.Modificato)
            ButtonModificaAppaltatrice.Enabled = true;
        else
            ButtonModificaAppaltatrice.Enabled = false;
    }

    private void CantieriRicercaImpresa2_OnImpresaSelected(Impresa impresa)
    {
        ImpresaSubappaltatriceSelezionata(impresa);
    }

    private void ImpresaSubappaltatriceSelezionata(Impresa impresa)
    {
        TextBoxImpresaAppaltata.Text = impresa.NomeCompleto;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaAppaltataSiceInfo.Checked = true;
        else
            RadioButtonImpresaAppaltataCantieri.Checked = true;
        ViewState["impresaAppaltata"] = impresa;

        CantieriRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = false;

        if (impresa.FonteNotifica && !impresa.Modificato)
            ButtonModificaImpresaAppaltata.Enabled = true;
        else
            ButtonModificaImpresaAppaltata.Enabled = false;
    }

    private void CantieriRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        CantieriRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = true;
    }

    private void CantieriRicercaImpresaAppaltataDa_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        CantieriRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaAppaltataDa.Visible = true;
    }

    #endregion

    protected void RadGridSubappalti_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Subappalto subappalto = (Subappalto)e.Item.DataItem;
            Label lSubappaltatriceRagioneSociale = (Label)e.Item.FindControl("LabelSubappaltatriceRagioneSociale");
            Label lSubappaltatriceIndirizzo = (Label)e.Item.FindControl("LabelSubappaltatriceIndirizzo");
            Label lSubappaltatriceIndirizzoAmmi = (Label)e.Item.FindControl("LabelSubappaltatriceIndirizzoAmmi");
            Label lSubappaltatriceLegale = (Label)e.Item.FindControl("LabelSubappaltatriceLegale");
            Label lSubappaltatriceAmministrativo = (Label)e.Item.FindControl("LabelSubappaltatriceAmministrativo");
            Label lSubappaltataRagioneSociale = (Label)e.Item.FindControl("LabelSubappaltataRagioneSociale");
            Label lSubappaltataIndirizzo = (Label)e.Item.FindControl("LabelSubappaltataIndirizzo");
            Label lSubappaltataIndirizzoAmmi = (Label)e.Item.FindControl("LabelSubappaltataIndirizzoAmmi");
            Label lSubappaltataLegale = (Label)e.Item.FindControl("LabelSubappaltataLegale");
            Label lSubappaltataAmministrativo = (Label)e.Item.FindControl("LabelSubappaltataAmministrativo");
            DropDownList dropTipoAppalto = (DropDownList)e.Item.FindControl("DropDownListTipoContratto");

            CaricaTipologieContratto(dropTipoAppalto);
            dropTipoAppalto.SelectedValue = subappalto.TipoContratto.ToString();

            if (subappalto.SubAppaltata != null)
            {
                lSubappaltatriceRagioneSociale.Text = subappalto.NomeAppaltata;
                lSubappaltatriceIndirizzo.Text = subappalto.SubAppaltata.IndirizzoCompleto;
                lSubappaltatriceIndirizzoAmmi.Text = subappalto.SubAppaltata.AmmiIndirizzoCompleto;
            }
            else
            {
                lSubappaltatriceLegale.Visible = false;
                lSubappaltatriceAmministrativo.Visible = false;
            }

            if (subappalto.SubAppaltatrice != null)
            {
                lSubappaltataRagioneSociale.Text = subappalto.NomeAppaltatrice;
                lSubappaltataIndirizzo.Text = subappalto.SubAppaltatrice.IndirizzoCompleto;
                lSubappaltataIndirizzoAmmi.Text = subappalto.SubAppaltatrice.AmmiIndirizzoCompleto;
            }
            else
            {
                lSubappaltataLegale.Visible = false;
                lSubappaltataAmministrativo.Visible = false;
            }
        }
    }

    protected void CustomValidatorImpresaSubappaltatriceSelezionata_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["impresaAppaltata"] == null)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void RadGridSubappalti_ItemCommand(object source, GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "elimina":
                SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];

                if (RadGridSubappalti.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdSubappalto"] != null)
                {
                    Int32 idSubappalto = (Int32)RadGridSubappalti.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdSubappalto"];
                    if (biz.DeleteSubappalto(idSubappalto))
                    {
                        subappalti.RemoveAt(e.Item.ItemIndex);
                    }
                }
                else
                {
                    subappalti.RemoveAt(e.Item.ItemIndex);
                }

                ViewState["subappalti"] = subappalti;

                CaricaSubappalti();
                break;
            case "modifica":
                SubappaltoCollection subappaltiM = (SubappaltoCollection)ViewState["subappalti"];
                Subappalto subappalto = null;

                if (RadGridSubappalti.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdSubappalto"] != null)
                {
                    Int32 idSubappalto = (Int32)RadGridSubappalti.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdSubappalto"];

                    var subTrov = from sub in subappaltiM
                                  where sub.IdSubappalto == idSubappalto
                                  select sub;
                    
                    subappalto = subTrov.First();
                }
                else
                {
                    subappalto = subappaltiM[e.Item.ItemIndex];
                }

                ModificaSubappalto(subappalto, e.Item.ItemIndex);
                //ViewState["subappalti"] = subappalti;

                //CaricaSubappalti();
                break;
        }
    }

    private void ModificaSubappalto(Subappalto subappalto, Int32 indiceSubappalto)
    {
        PanelAppalti.Visible = true;
        ViewState["IndiceAggiornamento"] = indiceSubappalto;

        ImpresaSubappaltatriceSelezionata(subappalto.SubAppaltata);
        if (subappalto.SubAppaltatrice != null)
        {
            ImpresaAppaltataDaSelezionata(subappalto.SubAppaltatrice);
        }
    }

    #region Vecchi metodi GridView
    //protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        Subappalto subappalto = (Subappalto)e.Row.DataItem;
    //        Label lSubappaltatriceRagioneSociale = (Label)e.Row.FindControl("LabelSubappaltatriceRagioneSociale");
    //        Label lSubappaltatriceIndirizzo = (Label)e.Row.FindControl("LabelSubappaltatriceIndirizzo");
    //        Label lSubappaltatriceIndirizzoAmmi = (Label)e.Row.FindControl("LabelSubappaltatriceIndirizzoAmmi");
    //        Label lSubappaltataRagioneSociale = (Label)e.Row.FindControl("LabelSubappaltataRagioneSociale");
    //        Label lSubappaltataIndirizzo = (Label)e.Row.FindControl("LabelSubappaltataIndirizzo");
    //        Label lSubappaltataIndirizzoAmmi = (Label)e.Row.FindControl("LabelSubappaltataIndirizzoAmmi");
    //        DropDownList dropTipoAppalto = (DropDownList)e.Row.FindControl("DropDownListTipoContratto");

    //        CaricaTipologieContratto(dropTipoAppalto);
    //        dropTipoAppalto.SelectedValue = subappalto.TipoContratto.ToString();

    //        if (subappalto.SubAppaltata != null)
    //        {
    //            lSubappaltatriceRagioneSociale.Text = subappalto.NomeAppaltata;
    //            lSubappaltatriceIndirizzo.Text = subappalto.SubAppaltata.IndirizzoCompleto;
    //            lSubappaltatriceIndirizzoAmmi.Text = subappalto.SubAppaltata.AmmiIndirizzoCompleto;
    //        }

    //        if (subappalto.SubAppaltatrice != null)
    //        {
    //            lSubappaltataRagioneSociale.Text = subappalto.NomeAppaltatrice;
    //            lSubappaltataIndirizzo.Text = subappalto.SubAppaltatrice.IndirizzoCompleto;
    //            lSubappaltataIndirizzoAmmi.Text = subappalto.SubAppaltatrice.AmmiIndirizzoCompleto;
    //        }
    //    }
    //}

    //protected void GridViewSubappalti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];

    //    if (GridViewSubappalti.DataKeys[e.RowIndex].Values["IdSubappalto"] != null)
    //    {
    //        int idSubappalto = (int)GridViewSubappalti.DataKeys[e.RowIndex].Values["IdSubappalto"];
    //        if (biz.DeleteSubappalto(idSubappalto))
    //            subappalti.RemoveAt(e.RowIndex);
    //    }
    //    else
    //    {
    //        subappalti.RemoveAt(e.RowIndex);
    //    }

    //    ViewState["subappalti"] = subappalti;

    //    CaricaSubappalti();
    //}
    #endregion
}