﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;

public partial class Cantieri_WebControls_LegendaColoriAttivita : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PanelIspezioniDaConsiderare.BackColor = ColoriAttivita.SfondoIspezione;
            PanelIspezioniConsiderate.BackColor = ColoriAttivita.SfondoIspezioneConsiderata;
            PanelIspezioniFantasma.BackColor = ColoriAttivita.SfondoIspezioneFantasma;
            PanelAppuntamenti.BackColor = ColoriAttivita.SfondoAppuntamento;
            PanelAttivita.BackColor = ColoriAttivita.SfondoBackOffice;
            PanelVarie.BackColor = ColoriAttivita.SfondoVarie;
        }
    }
}