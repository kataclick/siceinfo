﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Delegates;
using Telerik.Web.UI;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Cantieri.Type.Filters;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Type.Collections;
using System.Web.UI.HtmlControls;
using TBridge.Cemi.Cantieri.Type.Enums;
using System.Text;
using System.Drawing;
using Cantiere = TBridge.Cemi.Cantieri.Type.Entities.Cantiere;

public partial class Cantieri_WebControls_RicercaCantieriPerIspettori : System.Web.UI.UserControl
{
    private const int COLONNASELEZIONA = 4;

    private readonly CantieriBusiness biz = new CantieriBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();

    public event CantieriSelectedEventHandler OnCantiereSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{
        //    ((RadScriptManager)Page.FindControl("RadScriptManagerMain")).RegisterPostBackControl(RadGridCantieri);
        //}
        //catch
        //{
        //    ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(RadGridCantieri);
        //}

        if (ViewState["SoloVisualizzazioneAttivitaNonPreseInCarico"] != null)
        {
            //if (DropDownListZone.Items.Count == 0)
            //{
            //    CaricaZone();
            //}

            if (!Page.IsPostBack)
            {
                this.DataBind();
            }
            else
            {
                // Verifico se è un Refresh fatto con Javascript

                String eventArg = Request["__EVENTARGUMENT"];

                if (eventArg != null)
                {
                    int offset = eventArg.IndexOf("@@@@@");

                    if (offset > -1)
                    {
                        CaricaCantieri();
                    }
                }
            }
        }
    }

    public void SoloVisualizzazioneAttivitaNonPreseInCarico()
    {
        ViewState["SoloVisualizzazioneAttivitaNonPreseInCarico"] = true;
        //trFiltriRicerca.Visible = false;
        divFiltriRicerca.Visible = false;
        LabelElencoCantieri.Visible = false;

        CaricaCantieri();
    }

    private CantieriFilter CreaFiltro()
    {
        CantieriFilter filtro = new CantieriFilter();

        filtro.Provincia = TextBoxProvincia.Text.Trim();
        filtro.Comune = TextBoxComune.Text.Trim();
        filtro.Indirizzo = TextBoxIndirizzo.Text.Trim();
        filtro.Impresa = TextBoxImpresa.Text.Trim();
        filtro.Committente = TextBoxCommittente.Text.Trim();
        //filtro.ImpresaSubappalto = TextBoxSubappaltatrice.Text.Trim();
        filtro.Importo = RadNumericTextBoxImporto.Value;

        //if (!String.IsNullOrEmpty(DropDownListZone.SelectedValue) && DropDownListZone.SelectedValue != "0")
        //{
        //    filtro.IdZona = Int32.Parse(DropDownListZone.SelectedValue);
        //}

        switch(DropDownListStatoAssegnazione.SelectedValue)
        {
            case "AssegnatiMe":
                TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore ispettore = (TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                filtro.IdIspettoreAssegnato = ispettore.IdIspettore;
                break;
        }

        switch (DropDownListStatoPresaCarico.SelectedValue)
        {
            case "Tutti":
                filtro.PresaInCarico = StatoPresaInCaricoFiltro.Tutti;
                break;
            case "PresiCarico":
                filtro.PresaInCarico = StatoPresaInCaricoFiltro.PresiInCarico;
                break;
            case "NonPresiCarico":
                filtro.PresaInCarico = StatoPresaInCaricoFiltro.NonPresiInCarico;
                break;
        }

        filtro.NonCollegatiAttivita = true;

        return filtro;
    }

    private void CaricaCantieri()
    {
        CantieriFilter filtro = CreaFiltro();

        CantiereCollection listaCantieri =
            biz.GetCantieri(filtro);

        Presenter.CaricaElementiInGridView(
            RadGridCantieri,
            listaCantieri);
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        CaricaCantieri();
    }

    //private void CaricaZone()
    //{
    //    ZonaCollection listaZone = biz.GetZone(null);

    //    DropDownListZone.Items.Clear();
    //    DropDownListZone.Items.Insert(0, new RadComboBoxItem(" Tutti", "0"));
    //    DropDownListZone.DataSource = listaZone;
    //    DropDownListZone.DataTextField = "Nome";
    //    DropDownListZone.DataValueField = "IdZona";

    //    DropDownListZone.DataBind();
    //}

    protected void RadGridCantieri_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Cantiere cantiere = (Cantiere)e.Item.DataItem;
            
            ImageButton ibMappa = (ImageButton)e.Item.FindControl("ImageButtonMappa");

            if (cantiere.Latitudine.HasValue && cantiere.Longitudine.HasValue)
            {
                ibMappa.Enabled = true;
                ibMappa.ImageUrl = "~/images/maps.gif";

                string comando = String.Format(
                    "openRadWindowMappa('{0}','{1}','{2}','{3}','{4}','{5}','{6}'); return false;",
                    cantiere.Latitudine,
                    cantiere.Longitudine,
                    String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico),
                    cantiere.Cap,
                    cantiere.Comune,
                    cantiere.Provincia,
                    String.Empty);

                ibMappa.Attributes.Add("OnClick", comando);
            }
            else
            {
                ibMappa.Enabled = false;
                ibMappa.ImageUrl = "~/images/mapsBN.gif";
            }

            // Indirizzo
            Label lIndirizzo = (Label)e.Item.FindControl("LabelIndirizzo");
            Label lCap = (Label)e.Item.FindControl("LabelCap");
            Label lComune = (Label)e.Item.FindControl("LabelComune");
            Label lProvincia = (Label)e.Item.FindControl("LabelProvincia");
            lIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
            lCap.Text = cantiere.Cap;
            lComune.Text = cantiere.Comune;
            lProvincia.Text = cantiere.Provincia;

            HtmlTableRow trNote = (HtmlTableRow)e.Item.FindControl("trNote");
            if (ViewState["SoloSelezione"] != null)
            {
                trNote.Visible = false;
            }
            else
            {
                trNote.Visible = true;

                // Ispezioni
                IspezioniFilter filtro = new IspezioniFilter();
                filtro.IdCantiere = cantiere.IdCantiere;
                cantiere.Ispezioni = biz.GetIspezioni(filtro);
                ImageButton ibIspezione = (ImageButton)e.Item.FindControl("ImageButtonIspezione");
                if (cantiere.Ispezioni != null && cantiere.Ispezioni.Count > 0)
                {
                    ibIspezione.ImageUrl = "~/images/ispezione.png";
                    Label lDataIspezione = (Label)e.Item.FindControl("LabelDataIspezione");
                    StringBuilder stringaIspezione = new StringBuilder();

                    foreach (RapportoIspezione ispezione in cantiere.Ispezioni)
                    {
                        stringaIspezione.AppendLine(String.Format("{0} {1}", ispezione.Giorno.ToString("dd/MM/yyyy"),
                                                                  ispezione.Ispettore.NomeCompleto));
                        lDataIspezione.Text = ispezione.Giorno.ToString("dd/MM/yyyy");
                    }
                    ibIspezione.ToolTip = stringaIspezione.ToString();
                }

                // Assegnazioni
                ImageButton ibAssegnazione = (ImageButton)e.Item.FindControl("ImageButtonAssegnazione");
                ibAssegnazione.Enabled = false;
                if (cantiere.Assegnazione != null)
                {
                    Label lDataAssegnazione = (Label)e.Item.FindControl("LabelDataAssegnazione");
                    lDataAssegnazione.Text = cantiere.Assegnazione.Data.ToString("dd/MM/yyyy");

                    if (cantiere.Assegnazione.Priorita != null)
                    {
                        // Assegnato
                        ibAssegnazione.ImageUrl = "~/images/assegnato.png";
                        ibAssegnazione.ToolTip = String.Format("Cantiere assegnato in data {0}. Priorità: {1}",
                                                               lDataAssegnazione.Text, cantiere.Assegnazione.Priorita);

                        TBridge.Cemi.Type.Domain.CantieriAssegnazione assegnazione = bizEF.GetAssegnazione(cantiere.Assegnazione.IdAssegnazione);
                        foreach (TBridge.Cemi.Type.Domain.Ispettore isp in assegnazione.Ispettori)
                        {
                            ibAssegnazione.ToolTip = String.Format("{0}\n{1}", ibAssegnazione.ToolTip, isp.NomeCompleto);
                        }
                    }
                    else
                    {
                        // Rifiutato
                        ibAssegnazione.ImageUrl = "~/images/assegnatoNessuno.png";
                        ibAssegnazione.ToolTip = String.Format("Cantiere rifiutato in data {0}. Motivazione: {1}",
                                                               lDataAssegnazione.Text,
                                                               cantiere.Assegnazione.MotivazioneRifiuto);
                    }
                }
                else
                {
                    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriAssegnazione.ToString()))
                    {
                        ibAssegnazione.Enabled = true;

                        ibAssegnazione.ToolTip = "Assegna il cantiere";

                        string comando = String.Format(
                            "openRadWindowAssegnazione('{0}'); return false;",
                            cantiere.IdCantiere);

                        ibAssegnazione.Attributes.Add("OnClick", comando);
                    }
                }

                // Presa in carico
                ImageButton ibPresaInCarico = (ImageButton)e.Item.FindControl("ImageButtonProgrammazione");
                if (cantiere.PresaInCarico != null)
                {
                    ibPresaInCarico.ImageUrl = "~/images/programmato.png";
                    Label lDataProgrammazione = (Label)e.Item.FindControl("LabelDataProgrammazione");

                    lDataProgrammazione.Text = cantiere.PresaInCarico.DataPresaInCarico.ToString("dd/MM/yyyy");
                    ibPresaInCarico.ToolTip = String.Format("Ispezione programmata per il {0} da {1} {2}",
                        cantiere.PresaInCarico.Data.ToString("dd/MM/yyyy"),
                        cantiere.PresaInCarico.Ispettore.Cognome,
                        cantiere.PresaInCarico.Ispettore.Nome);
                }

                // Segnalazione
                ImageButton ibSegnalazione = (ImageButton)e.Item.FindControl("ImageButtonSegnalazione");
                if (cantiere.Segnalazione != null)
                {
                    ibSegnalazione.ImageUrl = "~/images/segnalazione.png";
                    Label lDataSegnalazione = (Label)e.Item.FindControl("LabelDataSegnalazione");

                    lDataSegnalazione.Text = cantiere.Segnalazione.Data.ToString("dd/MM/yyyy");
                    ibSegnalazione.ToolTip = String.Format("Segnalato per {0}. Note: {1}",
                        cantiere.Segnalazione.Motivazione.Descrizione,
                        cantiere.Segnalazione.Note);
                }

                // Impresa
                if (cantiere.ImpresaAppaltatrice != null)
                {
                    Label labelImpresa = (Label)e.Item.FindControl("LabelImpresa");
                    labelImpresa.Text = cantiere.ImpresaAppaltatrice.RagioneSociale;

                    if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                        labelImpresa.ForeColor = Color.Gray;
                }
                else
                {
                    if (cantiere.ImpresaAppaltatriceTrovata != null)
                    {
                        Label labelImpresa = (Label)e.Item.FindControl("LabelImpresa");
                        labelImpresa.Text = cantiere.ImpresaAppaltatriceTrovata;
                        labelImpresa.ForeColor = Color.Red;
                    }
                }
            }

            // Committente
            Label lCommittente = (Label)e.Item.FindControl("LabelCommittente");
            if (cantiere.Committente != null)
            {
                lCommittente.Text = cantiere.Committente.RagioneSociale;
            }
            else
            {
                lCommittente.Text = cantiere.CommittenteTrovato;
                lCommittente.ForeColor = Color.Red;
            }

            // Chiamata ad evento Javascript
            if (ViewState["SoloVisualizzazioneAttivitaNonPreseInCarico"] != null
                && cantiere.PresaInCarico == null)
            {
                ImageButton ibPresaInCarico = (ImageButton)e.Item.FindControl("ImageButtonProgrammazione");
                ibPresaInCarico.Enabled = true;
                
                string comando = String.Format(
                            "openRadWindowControllo('{0}'); return false;",
                            cantiere.IdCantiere);

                ibPresaInCarico.Attributes.Add("OnClick", comando);
            }
        }
    }

    protected void RadGridCantieri_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaCantieri();
    }

    protected void RadGridCantieri_ItemCommand(object source, GridCommandEventArgs e)
    {
        Int32 idCantiere = -1;

        switch (e.CommandName)
        {
            case "modifica":
                idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];
                Cantiere cantiere = biz.GetCantiere(idCantiere);

                if (OnCantiereSelected != null)
                    OnCantiereSelected(cantiere);
                break;
            case "scheda":
                idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];
                Context.Items["IdCantiere"] = idCantiere;
                Server.Transfer("~/Cantieri/SchedaCantiere.aspx");
                break;
        }
    }

    protected String RecuperaPostBackCode()
    {
        return this.Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
    }

    protected void RadGridCantieri_DataBinding(object sender, EventArgs e)
    {
        if (ViewState["SoloVisualizzazioneAttivitaNonPreseInCarico"] != null)
        {
            RadGridCantieri.MasterTableView.Columns[COLONNASELEZIONA].Visible = false;
        }
    }
}