﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Cantieri.Type.Filters;
using TBridge.Cemi.Cantieri.Type.Enums;

public partial class Cantieri_EstrazioneCantieri : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);
        #endregion

        if (Context.Items["Filtro"] != null)
        {
            CantieriFilter filtro = (CantieriFilter) Context.Items["Filtro"];

            ReportViewerSchedaCantiere.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerSchedaCantiere.ServerReport.ReportPath = "/ReportCantieri/EstrazioneCantieri";

            //ReportParameter[] listaParam = new ReportParameter[1];
            //listaParam[0] = new ReportParameter("idCantiere", idCantiere.ToString());
            List<ReportParameter> parametri = new List<ReportParameter>();

            if (filtro.IdImpresa.HasValue)
            {
                parametri.Add(new ReportParameter("idImpresa", filtro.IdImpresa.ToString()));
            }
            if (!String.IsNullOrEmpty(filtro.Impresa))
            {
                parametri.Add(new ReportParameter("impresa", filtro.Impresa));
            }
            if (!String.IsNullOrEmpty(filtro.CodiceFiscaleImpresa))
            {
                parametri.Add(new ReportParameter("codFiscImpresa", filtro.CodiceFiscaleImpresa));
            }
            if (!String.IsNullOrEmpty(filtro.Committente))
            {
                parametri.Add(new ReportParameter("committente", filtro.Committente));
            }
            if (!String.IsNullOrEmpty(filtro.Provincia))
            {
                parametri.Add(new ReportParameter("provincia", filtro.Provincia));
            }
            if (!String.IsNullOrEmpty(filtro.Comune))
            {
                parametri.Add(new ReportParameter("comune", filtro.Comune));
            }
            if (!String.IsNullOrEmpty(filtro.Indirizzo))
            {
                parametri.Add(new ReportParameter("indirizzo", filtro.Indirizzo));
            }
            if (!String.IsNullOrEmpty(filtro.Cap))
            {
                parametri.Add(new ReportParameter("cap", filtro.Cap));
            }
            if (filtro.Importo.HasValue)
            {
                parametri.Add(new ReportParameter("importo", filtro.Importo.Value.ToString()));
            }
            if (filtro.DataInserimentoDal.HasValue)
            {
                parametri.Add(new ReportParameter("dataInserimentoDal", filtro.DataInserimentoDal.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.DataInserimentoAl.HasValue)
            {
                parametri.Add(new ReportParameter("dataInserimentoAl", filtro.DataInserimentoAl.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.Segnalati.HasValue)
            {
                parametri.Add(new ReportParameter("segnalati", filtro.Segnalati.ToString()));
            }
            if (filtro.Assegnati.HasValue)
            {
                parametri.Add(new ReportParameter("assegnati", ((Int32)filtro.Assegnati).ToString()));
            }
            if (filtro.IdIspettoreAssegnato.HasValue)
            {
                parametri.Add(new ReportParameter("idIspettoreAssegnato", filtro.IdIspettoreAssegnato.ToString()));
            }
            if (filtro.PresaInCarico.HasValue)
            {
                parametri.Add(new ReportParameter("presoInCarico", ((Int32) filtro.PresaInCarico).ToString()));
            }
            if (filtro.IdIspettorePresoInCarico.HasValue)
            {
                parametri.Add(new ReportParameter("idIspettorePresoInCarico", filtro.IdIspettorePresoInCarico.ToString()));
            }
            parametri.Add(new ReportParameter("rapportoIspezione", ((Int32)filtro.RapportoIspezione).ToString()));
            if (filtro.DataSegnalazioneDal.HasValue)
            {
                parametri.Add(new ReportParameter("dataSegnalazioneDal", filtro.DataSegnalazioneDal.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.DataSegnalazioneAl.HasValue)
            {
                parametri.Add(new ReportParameter("dataSegnalazioneAl", filtro.DataSegnalazioneAl.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.DataAssegnazioneDal.HasValue)
            {
                parametri.Add(new ReportParameter("dataAssegnazioneDal", filtro.DataAssegnazioneDal.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.DataAssegnazioneAl.HasValue)
            {
                parametri.Add(new ReportParameter("dataAssegnazioneAl", filtro.DataAssegnazioneAl.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.DataPresaCaricoDal.HasValue)
            {
                parametri.Add(new ReportParameter("dataPresaCaricoDal", filtro.DataPresaCaricoDal.Value.ToString("dd/MM/yyyy")));
            }
            if (filtro.DataPresaCaricoAl.HasValue)
            {
                parametri.Add(new ReportParameter("dataPresaCaricoAl", filtro.DataPresaCaricoAl.Value.ToString("dd/MM/yyyy")));
            }
            
            if (parametri.Count > 0)
            {
                ReportViewerSchedaCantiere.ServerReport.SetParameters(parametri.ToArray());
            }

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            //PDF

            byte[] bytes = ReportViewerSchedaCantiere.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=EstrazioneCantieri {0}.pdf", DateTime.Now.ToString("dd.MM.yyyy")));
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }
}