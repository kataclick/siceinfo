<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DefaultErroreAutenticazione.aspx.cs" Inherits="DefaultErroreAutenticazione" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>


    <asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
        <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" Titolo="Gestione autenticazione"
        Sottotitolo="Accesso alla funzionalitą non consentito"
        runat="server" />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Il ruolo assegnato non possiede i diritti necessari per accedere a questa funzionalitą. 
        Contattare l'amministratore di sistema per avere ulteriori informazioni."></asp:Label></asp:Content>
