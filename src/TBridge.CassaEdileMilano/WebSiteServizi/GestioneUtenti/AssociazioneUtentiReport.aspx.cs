﻿using System;
using System.Data;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;

public partial class GestioneUtenti_AssociazioneUtentiReport : Page
{
    private readonly GestioneReport biz = new GestioneReport();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti);

        if (!Page.IsPostBack)
        {
            CaricaReport();
        }
    }

    private void SelectPage()
    {
        if (RadComboBoxReport.Items.Count > 0)
        {
            switch (DropDownListTipoUtente.SelectedValue)
            {
                case "Lavoratore":
                    RadMultiPage1.SelectedIndex = 0;
                    ListaUtentiLavoratori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;
                case "Impresa":
                    RadMultiPage1.SelectedIndex = 1;
                    ListaUtentiImprese1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;
                case "Dipendente":
                    RadMultiPage1.SelectedIndex = 2;
                    ListaUtentiDipendenti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;
                case "Ospite":
                    RadMultiPage1.SelectedIndex = 3;
                    ListaUtentiOspiti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "Fornitore":
                    RadMultiPage1.SelectedIndex = 4;
                    ListaUtentiFornitori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "Ispettore":
                    RadMultiPage1.SelectedIndex = 5;
                    ListaUtentiIspettori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "Esattore":
                    RadMultiPage1.SelectedIndex = 6;
                    ListaUtentiEsattori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "Consulente":
                    RadMultiPage1.SelectedIndex = 7;
                    ListaUtentiConsulenti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "Sindacalista":
                    RadMultiPage1.SelectedIndex = 8;
                    ListaUtentiSindacalisti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "CassaEdile":
                    RadMultiPage1.SelectedIndex = 9;
                    ListaUtentiCasseEdili1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;

                case "ASL":
                    RadMultiPage1.SelectedIndex = 10;
                    ListaUtentiAsl1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;
                case "Committente":
                    RadMultiPage1.SelectedIndex = 11;
                    ListaUtentiCommittenti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                    break;
            }
        }
        else
        {
            RadAjaxPanel1.Visible = false;
            PanelError.Visible = true;
        }
    }

    protected void DropDownListTipoUtente_SelectedIndexChanged(object sender, EventArgs e)
    {
        LabelTipoUtenteVisualizzato.Text = "Tipologia utente visualizzata: " + DropDownListTipoUtente.SelectedValue;
        SelectPage();
    }

    private void CaricaReport()
    {
        ReportCollection listaReport = biz.GetListaReport();
        RadComboBoxReport.DataSource = listaReport;
        RadComboBoxReport.DataBind();

        if (RadComboBoxReport.Items.Count > 0)
        {
            RadComboBoxReport.SelectedIndex = 0;
            ImpostaIndiceReport();
        }
        else
        {
            RadAjaxPanel1.Visible = false;
            PanelError.Visible = true;
        }
        //Presenter.CaricaElementiInDropDown(RadComboBoxReport, listaReport, "Nome", "Id");
        //if (RadComboBoxReport.Items.Count > 0)
        //{
        //    RadComboBoxReport.SelectedIndex = 0;
        //    LabelReportSelezionato.Text = "Report selezionato: " + RadComboBoxReport.SelectedItem.Text;
        //}
        
    }

    protected void RadComboBox_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
    {
        e.Item.Text = ((Report)e.Item.DataItem).Nome;
        e.Item.Value = ((Report)e.Item.DataItem).Id.ToString(); 
    }

    protected void RadComboBoxReport_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        LabelReportSelezionato.Text = "Report selezionato: " + RadComboBoxReport.SelectedItem.Text;

        ImpostaIndiceReport();
    }

    private void ImpostaIndiceReport()
    {
        switch (DropDownListTipoUtente.SelectedValue)
        {
            case "Lavoratore":

                ListaUtentiLavoratori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;
            case "Impresa":

                ListaUtentiImprese1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;
            case "Dipendente":

                ListaUtentiDipendenti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;
            case "Ospite":

                ListaUtentiOspiti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "Fornitore":

                ListaUtentiFornitori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "Ispettore":

                ListaUtentiIspettori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "Esattore":

                ListaUtentiEsattori1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "Consulente":

                ListaUtentiConsulenti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "Sindacalista":

                ListaUtentiSindacalisti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "CassaEdile":

                ListaUtentiCasseEdili1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;

            case "ASL":

                ListaUtentiAsl1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;
            case "Committente":

                ListaUtentiCommittenti1.ImpostaReport(Int32.Parse(RadComboBoxReport.SelectedValue));
                break;
        }
    }
}