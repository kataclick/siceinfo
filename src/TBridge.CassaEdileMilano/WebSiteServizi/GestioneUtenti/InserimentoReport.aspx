﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InserimentoReport.aspx.cs" Inherits="GestioneUtenti_InserimentoReport"
    Theme="CETheme2009Wide" %>

<%@ Register Src="~/WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Gestione utenti" sottoTitolo="Gestione Utenti - Inserimento report"
        runat="server" />
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <div class="borderedDiv">
            <table class="standardTable">
                <tr>
                    <td width="15%">
                        Report:
                    </td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBoxReport" MarkFirstMatch="true" EnableScreenBoundaryDetection="true"
                            AutoPostBack="true" runat="server" OnItemDataBound="RadComboBox_ItemDataBound"
                            Width="600px">
                            <HeaderTemplate>
                                <ul>
                                    <li class="col1">Nome</li>
                                    <li class="col2">Path</li>
                                </ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <ul>
                                    <li class="col1">
                                        <%# DataBinder.Eval(Container.DataItem, "Nome") %></li>
                                    <li class="col2">
                                        <%# DataBinder.Eval(Container.DataItem, "Path") %></li>
                                </ul>
                            </ItemTemplate>
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:CustomValidator ID="CustomValidatorTipologiaCategoria" runat="server" ValidationGroup="Report"
                            ErrorMessage="Selezionare un report" ForeColor="Red" OnServerValidate="CustomValidatorReport_ServerValidate">*</asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nome:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxNome" runat="server" MaxLength="255" Width="600px">
                        </telerik:RadTextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server" ErrorMessage="Fornire un nome"
                            ForeColor="Red" ValidationGroup="Report" ControlToValidate="RadTextBoxNome">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td width="18%">
                        Tema:
                    </td>
                    <td width="30%">
                        <telerik:RadComboBox ID="RadComboBoxTema" runat="server">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="Normale" Value="CETheme2009" Selected="true" />
                                <telerik:RadComboBoxItem runat="server" Text="Esteso" Value="CETheme2009Wide" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Button ID="ButtonInserisci" Text="Inserisci" runat="server" OnClick="ButtonInserisci_Click"
                ValidationGroup="Report" />
            <asp:ValidationSummary ID="ValidationSummaryReport" runat="server" ValidationGroup="Report"
                CssClass="messaggiErrore" />
            <asp:Panel ID="PanelErroreInserimento" ForeColor="Red" Visible="false" runat="server">
                <ul>
                    <li>
                        <asp:Label ID="Label1" Text="Errore durante l'inserimento. Verificare di non aver indicato un nome già presente" ForeColor="Red"
                            runat="server" /></li></ul>
            </asp:Panel>
        </div>
        <br />
        <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Report inseriti:" />
        <telerik:RadGrid Width="100%" ID="RadGridReport" runat="server" AllowFilteringByColumn="true"
            EnableLinqExpressions="false" OnNeedDataSource="RadGridReport_NeedDataSource"
            SortingSettings-SortToolTip="" AllowSorting="true" OnDeleteCommand="RadGridReport_DeleteCommand" OnItemCommand="RadGridReport_ItemCommand">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="true" AllowFilteringByColumn="true"
                DataKeyNames="Id" EditMode="InPlace">
                <Columns>
                    <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="Path" HeaderText="Path" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="Theme" HeaderText="Tema" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" />
                    <telerik:GridTemplateColumn>
                        <FilterTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="~/images/pallinoX.png"
                                AlternateText="Cancella filtro" ToolTip="Cancella filtro" OnClick="ButtonClearFilter_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/images/edit.png" CommandName="Aggiorna"
                        Text="Aggiorna report">
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/images/pallinoX.png"
                        CommandName="Delete" Text="Elimina report">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
        <br />
        <asp:Panel ID="PanelModifica" runat="server" Visible="false">
            <table class="standardTable">
                <tr>
                    <td>Id:</td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxId" runat="server" 
                            DataType="System.Int32" ReadOnly="True" NumberFormat-DecimalDigits="0" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nome:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxNomeModifica" runat="server" MaxLength="255" Width="600px">
                        </telerik:RadTextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ForeColor="Red" ValidationGroup="ReportMod" ControlToValidate="RadTextBoxNomeModifica"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td width="18%">
                        Tema:
                    </td>
                    <td width="30%">
                        <telerik:RadComboBox ID="RadComboBoxTemaModifica" runat="server">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="Normale" Value="CETheme2009"/>
                                <telerik:RadComboBoxItem runat="server" Text="Esteso" Value="CETheme2009Wide" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Button ID="ButtonModifica" Text="Aggiorna" runat="server" OnClick="ButtonModifica_Click"
                ValidationGroup="ReportMod" />
            <asp:ValidationSummary ID="ValidationSummaryMod" runat="server" ValidationGroup="ReportMod"
                CssClass="messaggiErrore" />
            <asp:Panel ID="PanelErroreAggiornamento" ForeColor="Red" Visible="false" runat="server">
                <ul>
                    <li>
                        <asp:Label ID="Label3" Text="Errore durante l'aggiornamento. Verificare di non aver indicato un nome già presente" ForeColor="Red" runat="server" />
                    </li>
                </ul>
            </asp:Panel>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>
