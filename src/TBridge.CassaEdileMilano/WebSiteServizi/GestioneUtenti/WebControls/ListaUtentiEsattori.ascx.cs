﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

public partial class GestioneUtenti_WebControls_ListaUtentiEsattori : UserControl
{
    private readonly GestioneReport biz = new GestioneReport();
    private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

    private Int32 IdReport
    {
        get { return ViewState["idReport"] == null ? 0 : (int)ViewState["idReport"]; }
        set { ViewState["idReport"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RadGridEsattoriAssociati.Rebind();
            RadGridEsattoriAssociabili.Rebind();
        }
    }

    public void ImpostaReport(int idReport)
    {
        IdReport = idReport;
        RadGridEsattoriAssociati.MasterTableView.Rebind();
    }

    protected void RadGridUtenti_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtenti();
    }

    protected void RadGridUtentiAssociati_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtentiAssociati();
    }

    private void BindUtenti()
    {
        RadGridEsattoriAssociabili.DataSource = gu.GetUtentiEsattori();
    }

    private void BindUtentiAssociati()
    {
        RadGridEsattoriAssociati.DataSource = biz.GetUtentiReportEsattori(IdReport);
    }

    protected void RadGridUtentiAssociati_DeleteCommand(object source, GridCommandEventArgs e)
    {
        int idUtente =
            Convert.ToInt32(
                (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUtente"].ToString());

        biz.DeleteUtenteReport(idUtente, IdReport);
    }

    protected void ButtonClearFiltersAssociabili_Click(object sender, ImageClickEventArgs e)
    {
        RadGridEsattoriAssociabili.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridEsattoriAssociabili.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridEsattoriAssociabili.MasterTableView.Rebind();
    }

    protected void ButtonClearFiltersAssociati_Click(object sender, ImageClickEventArgs e)
    {
        RadGridEsattoriAssociati.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridEsattoriAssociati.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridEsattoriAssociati.MasterTableView.Rebind();
    }

    protected void RadGridUtentiAssociabili_SelectedIndexChanging(object sender, EventArgs eventArgs)
    {
        int idUtente =
            (Int32)
            RadGridEsattoriAssociabili.MasterTableView.DataKeyValues[
                Int32.Parse(RadGridEsattoriAssociabili.SelectedIndexes[0])]["IdUtente"];
        biz.AssociaUtenteReport(idUtente, IdReport);
        RadGridEsattoriAssociati.Rebind();
    }
}