﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListaUtentiImprese.ascx.cs"
    Inherits="GestioneUtenti_WebControls_ListaUtentiImprese" %>
<div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <asp:Label ID="Label1" runat="server" Text="Utenti associati"></asp:Label> <br />
        <telerik:RadGrid Width="100%" ID="RadGridImpreseAssociati" runat="server" AllowFilteringByColumn="true"
            EnableLinqExpressions="false" OnNeedDataSource="RadGridUtentiAssociati_NeedDataSource"
            SortingSettings-SortToolTip="" AllowSorting="true" OnDeleteCommand="RadGridUtentiAssociati_DeleteCommand" >
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="true" AllowFilteringByColumn="true"
                DataKeyNames="IdUtente" EditMode="InPlace">
                <Columns>
                    <telerik:GridBoundColumn DataField="Username" HeaderText="Username" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idUtente" HeaderText="IdUtente" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="50px">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="idImpresa" HeaderText="Codice impresa" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="50px">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ragioneSociale" HeaderText="Ragione Sociale"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="partitaIVA" HeaderText="Partita IVA" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridTemplateColumn>
                        <FilterTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="~/images/pallinoX.png"
                                AlternateText="Cancella filtro" ToolTip="Cancella filtro" OnClick="ButtonClearFiltersAssociati_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/images/pallinoX.png"
                        CommandName="Delete" Text="Elimina associazione">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
        &nbsp;<br />
        &nbsp;<br />
        <asp:Label ID="Label2" runat="server" Text="Utenti associabili"></asp:Label> <br />
        <telerik:RadGrid Width="100%" ID="RadGridImpreseAssociabili" runat="server" AllowFilteringByColumn="true"
            EnableLinqExpressions="false" OnNeedDataSource="RadGridUtenti_NeedDataSource"
            OnSelectedIndexChanged="RadGridUtentiAssociabili_SelectedIndexChanging" SortingSettings-SortToolTip=""
            AllowSorting="true">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="true" AllowFilteringByColumn="true"
                DataKeyNames="IdUtente" EditMode="InPlace">
                <Columns>
                    <telerik:GridBoundColumn DataField="Username" HeaderText="Username" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idUtente" HeaderText="IdUtente" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="50px">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="idImpresa" HeaderText="Codice impresa" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="50px">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ragioneSociale" HeaderText="Ragione Sociale"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="partitaIVA" HeaderText="Partita IVA" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridTemplateColumn>
                        <FilterTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="~/images/pallinoX.png"
                                AlternateText="Cancella filtro" ToolTip="Cancella filtro" OnClick="ButtonClearFiltersAssociabili_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="PushButton" Text="Associa" CommandName="Select">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
    </telerik:RadAjaxPanel>
</div>
