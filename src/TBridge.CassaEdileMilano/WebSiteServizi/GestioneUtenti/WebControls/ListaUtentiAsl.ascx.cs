﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

public partial class GestioneUtenti_WebControls_ListaUtentiAsl : UserControl
{
    private readonly GestioneReport biz = new GestioneReport();
    private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

    private Int32 IdReport
    {
        get{ return ViewState["idReport"] == null ? 0 : (int)ViewState["idReport"]; }
        set { ViewState["idReport"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RadGridAslAssociati.Rebind();
            RadGridAslAssociabili.Rebind();
        }
    }

    public void ImpostaReport(int idReport)
    {
        IdReport = idReport;
        RadGridAslAssociati.MasterTableView.Rebind();
    }

    protected void RadGridUtentiAsl_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtentiAsl();
    }

    protected void RadGridUtentiAslAssociati_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtentiAslAssociati();
    }

    private void BindUtentiAsl()
    {
        RadGridAslAssociabili.DataSource = gu.GetUtentiASL();
    }

    private void BindUtentiAslAssociati()
    {
        RadGridAslAssociati.DataSource = biz.GetUtentiReportAsl(IdReport);
    }

    protected void RadGridUtentiAslAssociati_DeleteCommand(object source, GridCommandEventArgs e)
    {
        int idUtente =
            Convert.ToInt32(
                (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUtente"].ToString());

        biz.DeleteUtenteReport(idUtente, IdReport);
    }

    protected void ButtonClearFiltersAslAssociabili_Click(object sender, ImageClickEventArgs e)
    {
        RadGridAslAssociabili.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridAslAssociabili.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridAslAssociabili.MasterTableView.Rebind();
    }

    protected void ButtonClearFiltersAslAssociati_Click(object sender, ImageClickEventArgs e)
    {
        RadGridAslAssociati.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridAslAssociati.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridAslAssociati.MasterTableView.Rebind();
    }

    protected void RadGridUtentiAslAssociabili_SelectedIndexChanging(object sender, EventArgs eventArgs)
    {
        int idUtente =
            (Int32)
            RadGridAslAssociabili.MasterTableView.DataKeyValues[
                Int32.Parse(RadGridAslAssociabili.SelectedIndexes[0])]["IdUtente"];
        biz.AssociaUtenteReport(idUtente, IdReport);
        RadGridAslAssociati.Rebind();
    }
}