﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

public partial class GestioneUtenti_WebControls_ListaUtentiImprese : UserControl
{
    private readonly GestioneReport biz = new GestioneReport();
    private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

    private Int32 IdReport
    {
        get { return ViewState["idReport"] == null ? 0 : (int) ViewState["idReport"]; }
        set { ViewState["idReport"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RadGridImpreseAssociati.Rebind();
            RadGridImpreseAssociabili.Rebind();
        }
    }

    public void ImpostaReport(int idReport)
    {
        IdReport = idReport;
        RadGridImpreseAssociati.MasterTableView.Rebind();
    }

    protected void RadGridUtenti_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtenti();
    }

    protected void RadGridUtentiAssociati_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtentiAssociati();
    }

    private void BindUtenti()
    {
        RadGridImpreseAssociabili.DataSource = gu.GetUtentiImprese();
    }

    private void BindUtentiAssociati()
    {
        RadGridImpreseAssociati.DataSource = biz.GetUtentiReportImprese(IdReport);
    }

    protected void RadGridUtentiAssociati_DeleteCommand(object source, GridCommandEventArgs e)
    {
        int idUtente =
            Convert.ToInt32(
                (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUtente"].ToString());

        biz.DeleteUtenteReport(idUtente, IdReport);
    }

    protected void ButtonClearFiltersAssociabili_Click(object sender, ImageClickEventArgs e)
    {
        RadGridImpreseAssociabili.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridImpreseAssociabili.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridImpreseAssociabili.MasterTableView.Rebind();
    }

    protected void ButtonClearFiltersAssociati_Click(object sender, ImageClickEventArgs e)
    {
        RadGridImpreseAssociati.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridImpreseAssociati.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridImpreseAssociati.MasterTableView.Rebind();
    }

    protected void RadGridUtentiAssociabili_SelectedIndexChanging(object sender, EventArgs eventArgs)
    {
        int idUtente =
            (Int32)
            RadGridImpreseAssociabili.MasterTableView.DataKeyValues[
                Int32.Parse(RadGridImpreseAssociabili.SelectedIndexes[0])]["IdUtente"];
        biz.AssociaUtenteReport(idUtente, IdReport);
        RadGridImpreseAssociati.Rebind();
    }
}