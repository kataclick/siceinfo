using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneCE.Business;
using TBridge.Cemi.IscrizioneCE.Type.Entities;
using System.Collections.Specialized;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Presenter;
using Lavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;

public partial class WebControls_IscrizioneCELegaleRappresentante : System.Web.UI.UserControl
{
    private readonly Common bizCommon = new Common();
    private readonly IscrizioniManager biz = new IscrizioniManager();

    public String CodiceFiscale
    {
        get
        {
            return Presenter.NormalizzaCampoTesto(TextBoxRappLegaleCF.Text);
        }
    }

    public String Cognome
    {
        get
        {
            return Presenter.NormalizzaCampoTesto(TextBoxRappLegaleCognome.Text);
        }
    }

    public String Nome
    {
        get
        {
            return Presenter.NormalizzaCampoTesto(TextBoxRappLegaleNome.Text);
        }
    }

    public String DataNascita
    {
        get
        {
            return TextBoxRappLegaleDataNascita.Text;
        }
    }

    public String ComuneNascita
    {
        get
        {
            if (!String.IsNullOrEmpty(DropDownListRappLegaleComuneNascita.SelectedValue))
            {
                return DropDownListRappLegaleComuneNascita.SelectedValue;
            }
            else
            {
                return DropDownListRappLegaleComuneNascitaEstero.SelectedValue;
            }
        }
    }

    public String Sesso
    {
        get
        {
            if (RadioButtonRappLegaleSessoM.Checked)
            {
                return "M";
            }
            else
            {
                return "F";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (ViewState["MODIFICA"] == null)
            {
                CaricaCombo();
            }
        }
    }

    private void CaricaCombo()
    {
        // PreIndirizzi
        ListDictionary preIndirizzi = bizCommon.GetPreIndirizzi();

        DropDownListRappLegaleResidenzaPreIndirizzo.Items.Clear();
        DropDownListRappLegaleResidenzaPreIndirizzo.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListRappLegaleResidenzaPreIndirizzo.DataSource = preIndirizzi;
        DropDownListRappLegaleResidenzaPreIndirizzo.DataTextField = "Value";
        DropDownListRappLegaleResidenzaPreIndirizzo.DataValueField = "Key";
        DropDownListRappLegaleResidenzaPreIndirizzo.DataBind();

        // Localit� estere
        ComuneSiceNewCollection comuniEsteri = bizCommon.GetComuniSiceNew("EE");

        DropDownListRappLegaleComuneNascitaEstero.Items.Clear();
        DropDownListRappLegaleComuneNascitaEstero.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListRappLegaleComuneNascitaEstero.DataSource = comuniEsteri;
        DropDownListRappLegaleComuneNascitaEstero.DataTextField = "Comune";
        DropDownListRappLegaleComuneNascitaEstero.DataValueField = "CodiceCatastale";
        DropDownListRappLegaleComuneNascitaEstero.DataBind();

        // Province
        StringCollection province = bizCommon.GetProvinceSiceNew();

        DropDownListRappLegaleProvinciaNascita.Items.Clear();
        DropDownListRappLegaleProvinciaNascita.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListRappLegaleProvinciaNascita.DataSource = province;
        DropDownListRappLegaleProvinciaNascita.DataBind();

        DropDownListRappLegaleResidenzaProvincia.Items.Clear();
        DropDownListRappLegaleResidenzaProvincia.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListRappLegaleResidenzaProvincia.DataSource = province;
        DropDownListRappLegaleResidenzaProvincia.DataBind();

        // Paese
        ListDictionary nazioni = bizCommon.GetNazioni();

        DropDownListRappLegalePaeseNascita.Items.Clear();
        DropDownListRappLegalePaeseNascita.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListRappLegalePaeseNascita.DataSource = nazioni;
        DropDownListRappLegalePaeseNascita.DataTextField = "Value";
        DropDownListRappLegalePaeseNascita.DataValueField = "Key";
        DropDownListRappLegalePaeseNascita.DataBind();
    }

    public void CaricaDomanda(ModuloIscrizione domanda)
    {
        ViewState["MODIFICA"] = true;
        CaricaCombo();

        // LEGALE RAPPRESENTANTE
        ViewState["IdLegaleRappresentante"] = domanda.LegaleRappresentante.IdLavoratore;
        TextBoxRappLegaleCognome.Text = domanda.LegaleRappresentante.Cognome;
        TextBoxRappLegaleNome.Text = domanda.LegaleRappresentante.Nome;
        TextBoxRappLegaleCF.Text = domanda.LegaleRappresentante.CodiceFiscale;
        if (domanda.LegaleRappresentante.DataNascita != new DateTime())
            TextBoxRappLegaleDataNascita.Text = domanda.LegaleRappresentante.DataNascita.ToShortDateString();
        if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.PaeseNascita))
            DropDownListRappLegalePaeseNascita.SelectedValue = domanda.LegaleRappresentante.PaeseNascita;

        if (DropDownListRappLegalePaeseNascita.SelectedValue == "1")
        {
            // ITALIA
            if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.ProvinciaNascita) &&
                DropDownListRappLegaleProvinciaNascita.Items.FindByValue(domanda.LegaleRappresentante.ProvinciaNascita) !=
                null)
            {
                DropDownListRappLegaleProvinciaNascita.SelectedValue = domanda.LegaleRappresentante.ProvinciaNascita;
                biz.CaricaComuni(DropDownListRappLegaleComuneNascita, domanda.LegaleRappresentante.ProvinciaNascita, null);
            }
            if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.LuogoNascita))
                DropDownListRappLegaleComuneNascita.SelectedIndex =
                    IscrizioniManager.IndiceComuneInDropDown(DropDownListRappLegaleComuneNascita,
                                           domanda.LegaleRappresentante.LuogoNascita);

            PanelLuogoItalia.Enabled = true;
            PanelLuogoEstero.Enabled = false;
        }
        else
        {
            // PAESE ESTERO
            if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.LuogoNascita))
                DropDownListRappLegaleComuneNascitaEstero.SelectedValue = domanda.LegaleRappresentante.LuogoNascita;

            PanelLuogoItalia.Enabled = false;
            PanelLuogoEstero.Enabled = true;
        }


        if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.IndirizzoProvincia) &&
            DropDownListRappLegaleResidenzaProvincia.Items.FindByValue(domanda.LegaleRappresentante.IndirizzoProvincia) !=
            null)
        {
            DropDownListRappLegaleResidenzaProvincia.SelectedValue = domanda.LegaleRappresentante.IndirizzoProvincia;
            biz.CaricaComuni(DropDownListRappLegaleResidenzaComune, domanda.LegaleRappresentante.IndirizzoProvincia, null);

            if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.IndirizzoComune))
            {
                DropDownListRappLegaleResidenzaComune.SelectedIndex =
                    IscrizioniManager.IndiceComuneInDropDown(DropDownListRappLegaleResidenzaComune,
                                           domanda.LegaleRappresentante.IndirizzoComune);
                biz.CaricaFrazioni(DropDownListRappLegaleResidenzaFrazione, domanda.LegaleRappresentante.IndirizzoComune);

                if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.IndirizzoFrazione))
                    DropDownListRappLegaleResidenzaFrazione.SelectedIndex =
                        IscrizioniManager.IndiceFrazioneInDropDown(DropDownListRappLegaleResidenzaFrazione,
                                                 domanda.LegaleRappresentante.IndirizzoFrazione);
            }
        }

        if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.IndirizzoPreIndirizzo) &&
            DropDownListRappLegaleResidenzaPreIndirizzo.Items.FindByValue(
                domanda.LegaleRappresentante.IndirizzoPreIndirizzo) != null)
            DropDownListRappLegaleResidenzaPreIndirizzo.SelectedValue =
                domanda.LegaleRappresentante.IndirizzoPreIndirizzo;
        else
            DropDownListRappLegaleResidenzaPreIndirizzo.Text = domanda.LegaleRappresentante.IndirizzoPreIndirizzo;
        TextBoxRappLegaleResidenzaIndirizzo.Text = domanda.LegaleRappresentante.IndirizzoDenominazione;
        TextBoxRappLegaleResidenzaCap.Text = domanda.LegaleRappresentante.IndirizzoCAP;
        TextBoxRappLegaleTel.Text = domanda.LegaleRappresentante.Telefono;
        if (!string.IsNullOrEmpty(domanda.LegaleRappresentante.Sesso))
        {
            if (domanda.LegaleRappresentante.Sesso == "M")
            {
                RadioButtonRappLegaleSessoM.Checked = true;
                RadioButtonRappLegaleSessoF.Checked = false;
            }
            else
            {
                RadioButtonRappLegaleSessoM.Checked = false;
                RadioButtonRappLegaleSessoF.Checked = true;
            }
        }
    }

    public void CompletaDomandaConLegaleRappresentante(ModuloIscrizione modulo)
    {
        // Legale rappresentante
        GetLavoratore(modulo.LegaleRappresentante);
    }

    private Lavoratore GetLavoratore(Lavoratore legaleRappresentante)
    {
        if (legaleRappresentante == null)
        {
            legaleRappresentante = new Lavoratore();
        }

        legaleRappresentante.IdLavoratore = -1;
        if (ViewState["IdLegaleRappresentante"] != null)
        {
            legaleRappresentante.IdLavoratore = (Int32)ViewState["IdLegaleRappresentante"];
        }
        legaleRappresentante.Cognome = Presenter.NormalizzaCampoTesto(TextBoxRappLegaleCognome.Text);
        legaleRappresentante.Nome = Presenter.NormalizzaCampoTesto(TextBoxRappLegaleNome.Text);
        legaleRappresentante.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxRappLegaleCF.Text);
        if (!string.IsNullOrEmpty(TextBoxRappLegaleDataNascita.Text))
            legaleRappresentante.DataNascita = DateTime.Parse(TextBoxRappLegaleDataNascita.Text.Replace('.', '/'));
        if (DropDownListRappLegalePaeseNascita.SelectedValue == "1")
            legaleRappresentante.LuogoNascita =
                biz.GetCodiceCatastaleDaCombo(DropDownListRappLegaleComuneNascita.SelectedValue);
        else
            legaleRappresentante.LuogoNascita = DropDownListRappLegaleComuneNascitaEstero.SelectedValue;
        legaleRappresentante.PaeseNascita = DropDownListRappLegalePaeseNascita.SelectedValue;
        legaleRappresentante.IndirizzoPreIndirizzo = DropDownListRappLegaleResidenzaPreIndirizzo.SelectedValue;
        legaleRappresentante.IndirizzoProvincia = DropDownListRappLegaleResidenzaProvincia.SelectedValue;
        legaleRappresentante.IndirizzoComune =
            biz.GetCodiceCatastaleDaCombo(DropDownListRappLegaleResidenzaComune.SelectedValue);
        legaleRappresentante.IndirizzoFrazione =
            biz.GetFrazioneDaCombo(DropDownListRappLegaleResidenzaFrazione.SelectedValue);
        legaleRappresentante.IndirizzoDenominazione = Presenter.NormalizzaCampoTesto(TextBoxRappLegaleResidenzaIndirizzo.Text);
        legaleRappresentante.IndirizzoCAP = TextBoxRappLegaleResidenzaCap.Text.ToUpper();
        legaleRappresentante.IndirizzoProvincia = DropDownListRappLegaleResidenzaProvincia.SelectedValue;
        legaleRappresentante.Telefono = Presenter.NormalizzaCampoTesto(TextBoxRappLegaleTel.Text);
        if (RadioButtonRappLegaleSessoM.Checked)
            legaleRappresentante.Sesso = "M";
        else
            legaleRappresentante.Sesso = "F";

        return legaleRappresentante;
    }

    //public Lavoratore GetLavoratore()
    //{
    //    return GetLavoratore(null);
    //}

    public void CaricaDatiProva()
    {
        // LEGALE RAPPRESENTANTE
        TextBoxRappLegaleCognome.Text = "mura";
        TextBoxRappLegaleNome.Text = "alessio";
        TextBoxRappLegaleCF.Text = "MRULSS81L25D969A";
        TextBoxRappLegaleDataNascita.Text = "25/07/1981";
        DropDownListRappLegaleComuneNascita.SelectedIndex = 1;
        DropDownListRappLegalePaeseNascita.SelectedIndex = 1;
        DropDownListRappLegaleResidenzaProvincia.SelectedIndex = 1;
        DropDownListRappLegaleResidenzaComune.SelectedIndex = 1;
        DropDownListRappLegaleResidenzaPreIndirizzo.SelectedIndex = 1;
        TextBoxRappLegaleResidenzaIndirizzo.Text = "del rapp legale 11";
        TextBoxRappLegaleResidenzaCap.Text = "16100";
        TextBoxRappLegaleTel.Text = "68798789";
    }

    #region Eventi DropDown
    protected void DropDownListRappLegalePaeseNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Se Italia attivo ricerca province, comuni,...
        // altrimenti visualizzo solo i comuni e prendo quelli internazionali
        if (DropDownListRappLegalePaeseNascita.SelectedValue == "1")
        {
            PanelLuogoItalia.Enabled = true;
            PanelLuogoEstero.Enabled = false;

            DropDownListRappLegaleComuneNascitaEstero.SelectedIndex = 0;
        }
        else
        {
            PanelLuogoItalia.Enabled = false;
            PanelLuogoEstero.Enabled = true;

            ListItem item =
                DropDownListRappLegaleComuneNascitaEstero.Items.FindByText(
                    DropDownListRappLegalePaeseNascita.SelectedItem.Text);
            if (item != null)
                DropDownListRappLegaleComuneNascitaEstero.SelectedValue = item.Value;
        }
    }

    protected void DropDownListRappLegaleProvinciaNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        biz.CaricaComuni(DropDownListRappLegaleComuneNascita, DropDownListRappLegaleProvinciaNascita.SelectedValue, null);
    }

    protected void DropDownListRappLegaleResidenzaProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextBoxRappLegaleResidenzaCap.Text = string.Empty;
        biz.CaricaComuni(DropDownListRappLegaleResidenzaComune, DropDownListRappLegaleResidenzaProvincia.SelectedValue,
                     DropDownListRappLegaleResidenzaFrazione);
    }

    protected void DropDownListRappLegaleResidenzaComune_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextBoxRappLegaleResidenzaCap.Text = string.Empty;
        biz.CaricaCapOFrazioni(DropDownListRappLegaleResidenzaComune, DropDownListRappLegaleResidenzaFrazione,
                           TextBoxRappLegaleResidenzaCap);
    }

    protected void DropDownListRappLegaleResidenzaFrazione_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextBoxRappLegaleResidenzaCap.Text = string.Empty;
        biz.CaricaCap(DropDownListRappLegaleResidenzaFrazione.SelectedValue, TextBoxRappLegaleResidenzaCap);
    }
    #endregion

    #region Custom Validators
    protected void CustomValidatorRappLegaleDataNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (bizCommon.IsCampoDataSmallDateTimeMinoreDiOggi(TextBoxRappLegaleDataNascita.Text))
            args.IsValid = true;
        else
            args.IsValid = false;
    }

    protected void CustomValidatorLuogoDiNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (string.IsNullOrEmpty(DropDownListRappLegaleComuneNascita.SelectedValue) &&
            string.IsNullOrEmpty(DropDownListRappLegaleComuneNascitaEstero.SelectedValue))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorCAP_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CustomValidator customValidatorCAP = (CustomValidator)source;
        TextBox textBoxCAP = (TextBox)this.FindControl(customValidatorCAP.ControlToValidate);
        int iCAP;

        if (textBoxCAP.Text.Length == 5 && Int32.TryParse(textBoxCAP.Text, out iCAP))
        {
            string cap = textBoxCAP.Text;

            string codiceComboComune = null;
            switch (textBoxCAP.ID)
            {
                case "TextBoxRappLegaleResidenzaCap":
                    codiceComboComune = DropDownListRappLegaleResidenzaComune.SelectedValue;
                    break;
            }
            string[] codiceSplittato = codiceComboComune.Split('|');

            if (biz.IsGrandeCitta(codiceSplittato[0]) && cap[2] == '1' && cap[3] == '0' && cap[4] == '0')
                args.IsValid = false;
        }
    }

    protected void CustomValidatorCFRappLegale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // Controllo la correttezza del codice fiscale del legale rappresentante (primi 11 caratteri, bloccante)
        try
        {
            if (!string.IsNullOrEmpty(TextBoxRappLegaleDataNascita.Text))
            {
                string sesso;
                if (RadioButtonRappLegaleSessoM.Checked)
                    sesso = "M";
                else
                    sesso = "F";

                if (!bizCommon.VerificaPrimi11CaratteriCodiceFiscale(TextBoxRappLegaleNome.Text.Trim().ToUpper(),
                                                                     TextBoxRappLegaleCognome.Text.Trim().ToUpper(),
                                                                     sesso,
                                                                     DateTime.Parse(
                                                                         TextBoxRappLegaleDataNascita.Text.Replace('.', '/')),
                                                                     TextBoxRappLegaleCF.Text.Trim().ToUpper()))
                {
                    args.IsValid = false;
                }
            }
        }
        catch { }
    }
    #endregion
}
