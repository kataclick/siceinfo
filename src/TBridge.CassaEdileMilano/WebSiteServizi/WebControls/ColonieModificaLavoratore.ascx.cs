using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Colonie.Type.Delegates;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class WebControls_ColonieModificaLavoratore : UserControl
{
    private LavoratoreACE _lavoratore;

    public LavoratoreACE Lavoratore
    {
        get { return GetLavoratore(); }
        set
        {
            _lavoratore = value;
            if (_lavoratore == null)
                Reset();
            else
                Set(_lavoratore);
        }
    }

    public event LavoratoreACEModifiedEventHandler OnLavoratoreACEModified;
    public event CancelledEventHandler OnCancelled;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        if (OnCancelled != null)
            OnCancelled();
    }

    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        if (OnLavoratoreACEModified != null)
            OnLavoratoreACEModified(_lavoratore);
    }

    private void Set(LavoratoreACE lavoratore)
    {
        ViewState["IdLavoratore"] = lavoratore.IdLavoratore.Value;

        TextBoxCognome.Text = lavoratore.Cognome;
        TextBoxNome.Text = lavoratore.Nome;
        TextBoxDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
        TextBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;
        if (lavoratore.Sesso == 'M')
            RadioButtonLavoratoreM.Checked = true;
        else
            RadioButtonLavoratoreF.Checked = true;
        TextBoxTelefono.Text = lavoratore.Telefono;
        TextBoxCellulare.Text = lavoratore.Cellulare;
        TextBoxResidenzaIndirizzo.Text = lavoratore.Indirizzo;
        TextBoxResidenzaProvincia.Text = lavoratore.Provincia;
        TextBoxResidenzaComune.Text = lavoratore.Comune;
        TextBoxResidenzaCap.Text = lavoratore.Cap;
    }

    private void Reset()
    {
        TextBoxCognome.Text = null;
        TextBoxNome.Text = null;
        TextBoxDataNascita.Text = null;
        TextBoxCodiceFiscale.Text = null;
        RadioButtonLavoratoreM.Checked = false;
        TextBoxTelefono.Text = null;
        TextBoxCellulare.Text = null;
        TextBoxResidenzaIndirizzo.Text = null;
        TextBoxResidenzaProvincia.Text = null;
        TextBoxResidenzaComune.Text = null;
        TextBoxResidenzaCap.Text = null;
    }

    private LavoratoreACE GetLavoratore()
    {
        _lavoratore = new LavoratoreACE();

        if (ViewState["IdLavoratore"] != null)
            _lavoratore.IdLavoratore = (int) ViewState["IdLavoratore"];
        _lavoratore.Cognome = TextBoxCognome.Text.Trim().ToUpper();
        _lavoratore.Nome = TextBoxNome.Text.Trim().ToUpper();
        _lavoratore.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
        _lavoratore.CodiceFiscale = TextBoxCodiceFiscale.Text.Trim().ToUpper();
        if (RadioButtonLavoratoreM.Checked)
            _lavoratore.Sesso = 'M';
        else
            _lavoratore.Sesso = 'F';
        if (!string.IsNullOrEmpty(TextBoxTelefono.Text.Trim()))
            _lavoratore.Telefono = TextBoxTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxCellulare.Text.Trim()))
            _lavoratore.Cellulare = TextBoxCellulare.Text.Trim().ToUpper();
        _lavoratore.Indirizzo = TextBoxResidenzaIndirizzo.Text.Trim().ToUpper();
        _lavoratore.Provincia = TextBoxResidenzaProvincia.Text.Trim().ToUpper();
        _lavoratore.Comune = TextBoxResidenzaComune.Text.Trim().ToUpper();
        _lavoratore.Cap = TextBoxResidenzaCap.Text;
        if (ViewState["IdCassaEdile"] != null)
            _lavoratore.IdCassaEdile = (string) ViewState["IdCassaEdile"];

        return _lavoratore;
    }

    #region Custom Validators

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!String.IsNullOrEmpty(TextBoxDataNascita.Text))
            {
                DateTime dataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
                String sesso;

                sesso = RadioButtonLavoratoreM.Checked ? "M" : "F";

                //Controlliamo il codice Fiscale parzialmente per evitare i problemi di omocodia
                if (!CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(
                    Presenter.NormalizzaCampoTesto(TextBoxNome.Text),
                    Presenter.NormalizzaCampoTesto(TextBoxCognome.Text),
                    sesso,
                    dataNascita,
                    Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CodiceFiscaleCarattereControllo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(TextBoxCodiceFiscale.Text))
            {
                args.IsValid = false;
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    #endregion
}