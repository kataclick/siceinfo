using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cpt.Type.Collections;

public partial class WebControls_CptRiassuntoTelematiche : UserControl
{
    private readonly CptBusiness biz = new CptBusiness();

    private const string TESTOMOSTRA = "Mostra";
    private const string TESTONASCONDI = "Nascondi";

    protected void Page_Load(object sender, EventArgs e)
    {
        CantiereDaNotifica1.OnCantiereGenerato += new TBridge.Cemi.Cantieri.Type.Delegates.CantiereGeneratoEventHandler(CantiereDaNotifica1_OnCantiereGenerato);

        if (!Page.IsPostBack)
        {
            #region Inizializzazione visibilitÓ pannelli

            ViewState["MostraDatiGenerali"] = true;
            ViewState["MostraCommittente"] = true;
            ViewState["MostraResponsabileLavori"] = true;
            ViewState["MostraCoordinatoreSicurezzaProgettazione"] = true;
            ViewState["MostraCoordinatoreSicurezzaEsecuzione"] = true;
            ViewState["MostraDateNumeri"] = true;
            ViewState["MostraImpreseAffidatarie"] = true;
            ViewState["MostraImpreseEsecutrici"] = true;

            #endregion

            GestisciBottoni();
        }
    }

    public void CaricaNotifica(NotificaTelematica notifica)
    {
        if (notifica == null)
        {
            if (ViewState["IdNotifica"] != null)
            {
                Int32 idNotifica = (Int32)ViewState["IdNotifica"];
                notifica = biz.GetNotificaTelematica(idNotifica);
            }
        }
        else
        {
            ViewState["IdNotifica"] = notifica.IdNotifica.Value;
        }

        #region Cantieri generati
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGenerazioneDaNotifiche)
            && !String.IsNullOrEmpty(notifica.ProtocolloRegione))
        {
            IndirizzoCollection indirizzi = biz.GetCantieriGenerati(notifica.IdNotificaRiferimento);
            CantiereDaNotifica1.CaricaCantieri(indirizzi, notifica.IdNotificaRiferimento);
        }
        else
        {
            CantiereDaNotifica1.ForzaNonVisualizzazione();
        }
        #endregion

        // Dati generali
        trProtocollo.Visible = false;
        if (!String.IsNullOrEmpty(notifica.ProtocolloRegione))
        {
            trProtocollo.Visible = true;
            LabelProtocollo.Text = notifica.ProtocolloRegione;
        }
        else
        {
            if (notifica.IdNotifica.HasValue)
            {
                trProtocollo.Visible = true;
                LabelProtocollo.Text = notifica.IdNotifica.ToString();
            }
        }
        LabelDatiGeneraliData.Text = notifica.Data.ToShortDateString();
        LabelDatiGeneraliNaturaOpera.Text = notifica.NaturaOpera;
        if (notifica.OperaPubblica.HasValue)
        {
            LabelDatiGeneraliTipoOpera.Text = notifica.OperaPubblica.Value ? "Pubblica" : "Privata";
        }
        LabelDatiGeneraliNote.Text = notifica.Note;
        LabelDatiGeneraliNumeroAppalto.Text = notifica.NumeroAppalto;
        Presenter.CaricaElementiInListView(ListViewDatiGeneraliIndirizzi, notifica.Indirizzi);

        // Committente/i
        if (notifica.Committente != null)
        {
            // Persona
            LabelCommittentePersonaCognome.Text = notifica.Committente.PersonaCognome;
            LabelCommittentePersonaNome.Text = notifica.Committente.PersonaNome;
            LabelCommittentePersonaCodiceFiscale.Text = notifica.Committente.PersonaCodiceFiscale;
            LabelCommittentePersonaIndirizzo.Text = notifica.Committente.PersonaIndirizzo;
            LabelCommittentePersonaCitta.Text = notifica.Committente.PersonaComune;
            LabelCommittentePersonaProvincia.Text = notifica.Committente.PersonaProvincia;
            LabelCommittentePersonaCap.Text = notifica.Committente.PersonaCap;
            LabelCommittentePersonaTelefono.Text = notifica.Committente.PersonaTelefono;
            LabelCommittentePersonaFax.Text = notifica.Committente.PersonaFax;
            LabelCommittentePersonaCellulare.Text = notifica.Committente.PersonaCellulare;
            LabelCommittentePersonaEmail.Text = notifica.Committente.PersonaEmail;

            // Ente
            LabelCommittenteEnteRagioneSociale.Text = notifica.Committente.RagioneSociale;
            LabelCommittenteEntePartitaIVA.Text = notifica.Committente.PartitaIva;
            LabelCommittenteEnteCodiceFiscale.Text = notifica.Committente.CodiceFiscale;
            LabelCommittenteEnteIndirizzo.Text = notifica.Committente.Indirizzo;
            LabelCommittenteEnteCitta.Text = notifica.Committente.Comune;
            LabelCommittenteEnteProvincia.Text = notifica.Committente.Provincia;
            LabelCommittenteEnteCap.Text = notifica.Committente.Cap;
            LabelCommittenteEnteTelefono.Text = notifica.Committente.Telefono;
            LabelCommittenteEnteFax.Text = notifica.Committente.Fax;
        }

        // Responsabile dei lavori
        if (!notifica.ResponsabileCommittente && notifica.DirettoreLavori != null)
        {
            // Persona
            LabelResponsabileLavoriPersonaCognome.Text = notifica.DirettoreLavori.PersonaCognome;
            LabelResponsabileLavoriPersonaNome.Text = notifica.DirettoreLavori.PersonaNome;
            LabelResponsabileLavoriPersonaCodiceFiscale.Text = notifica.DirettoreLavori.PersonaCodiceFiscale;
            LabelResponsabileLavoriPersonaIndirizzo.Text = notifica.DirettoreLavori.Indirizzo;
            LabelResponsabileLavoriPersonaCitta.Text = notifica.DirettoreLavori.PersonaComune;
            LabelResponsabileLavoriPersonaProvincia.Text = notifica.DirettoreLavori.PersonaProvincia;
            LabelResponsabileLavoriPersonaCap.Text = notifica.DirettoreLavori.PersonaCap;
            LabelResponsabileLavoriPersonaTelefono.Text = notifica.DirettoreLavori.Telefono;
            LabelResponsabileLavoriPersonaFax.Text = notifica.DirettoreLavori.Fax;
            LabelResponsabileLavoriPersonaCellulare.Text = notifica.DirettoreLavori.PersonaCellulare;
            LabelResponsabileLavoriPersonaEmail.Text = notifica.DirettoreLavori.PersonaEmail;

            // Ente
            LabelResponsabileLavoriEnteRagioneSociale.Text = notifica.DirettoreLavori.RagioneSociale;
            LabelResponsabileLavoriEntePartitaIva.Text = notifica.DirettoreLavori.EntePartitaIva;
            LabelResponsabileLavoriEnteCodiceFiscale.Text = notifica.DirettoreLavori.EnteCodiceFiscale;
            LabelResponsabileLavoriEnteIndirizzo.Text = notifica.DirettoreLavori.EnteIndirizzo;
            LabelResponsabileLavoriEnteCitta.Text = notifica.DirettoreLavori.EnteComune;
            LabelResponsabileLavoriEnteProvincia.Text = notifica.DirettoreLavori.EnteProvincia;
            LabelResponsabileLavoriEnteCap.Text = notifica.DirettoreLavori.EnteCap;
            LabelResponsabileLavoriEnteTelefono.Text = notifica.DirettoreLavori.EnteTelefono;
            LabelResponsabileLavoriEnteFax.Text = notifica.DirettoreLavori.EnteFax;
        }
        else
        {
            if (notifica.ResponsabileCommittente)
            {
                CheckBoxNonNominato.Checked = true;
            }
        }

        // Coordinatore sicurezza progettazione
        if (notifica.CoordinatoreSicurezzaProgettazione != null)
        {
            // Persona
            LabelCoordinatoreProgettazionePersonaCognome.Text =
                notifica.CoordinatoreSicurezzaProgettazione.PersonaCognome;
            LabelCoordinatoreProgettazionePersonaNome.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaNome;
            LabelCoordinatoreProgettazionePersonaCodiceFiscale.Text =
                notifica.CoordinatoreSicurezzaProgettazione.PersonaCodiceFiscale;
            LabelCoordinatoreProgettazionePersonaIndirizzo.Text = notifica.CoordinatoreSicurezzaProgettazione.Indirizzo;
            LabelCoordinatoreProgettazionePersonaCitta.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaComune;
            LabelCoordinatoreProgettazionePersonaProvincia.Text =
                notifica.CoordinatoreSicurezzaProgettazione.PersonaProvincia;
            LabelCoordinatoreProgettazionePersonaCap.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaCap;
            LabelCoordinatoreProgettazionePersonaTelefono.Text = notifica.CoordinatoreSicurezzaProgettazione.Telefono;
            LabelCoordinatoreProgettazionePersonaFax.Text = notifica.CoordinatoreSicurezzaProgettazione.Fax;
            LabelCoordinatoreProgettazionePersonaCellulare.Text =
                notifica.CoordinatoreSicurezzaProgettazione.PersonaCellulare;
            LabelCoordinatoreProgettazionePersonaEmail.Text = notifica.CoordinatoreSicurezzaProgettazione.PersonaEmail;

            // Ente
            LabelCoordinatoreProgettazioneEnteRagioneSociale.Text =
                notifica.CoordinatoreSicurezzaProgettazione.RagioneSociale;
            LabelCoordinatoreProgettazioneEntePartitaIva.Text =
                notifica.CoordinatoreSicurezzaProgettazione.EntePartitaIva;
            LabelCoordinatoreProgettazioneEnteCodiceFiscale.Text =
                notifica.CoordinatoreSicurezzaProgettazione.EnteCodiceFiscale;
            LabelCoordinatoreProgettazioneEnteIndirizzo.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteIndirizzo;
            LabelCoordinatoreProgettazioneEnteCitta.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteComune;
            LabelCoordinatoreProgettazioneEnteProvincia.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteProvincia;
            LabelCoordinatoreProgettazioneEnteCap.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteCap;
            LabelCoordinatoreProgettazioneEnteTelefono.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteTelefono;
            LabelCoordinatoreProgettazioneEnteFax.Text = notifica.CoordinatoreSicurezzaProgettazione.EnteFax;
        }

        // Coordinatore sicurezza esecuzione
        if (notifica.CoordinatoreSicurezzaRealizzazione != null)
        {
            // Persona
            LabelCoordinatoreEsecuzionePersonaCognome.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaCognome;
            LabelCoordinatoreEsecuzionePersonaNome.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaNome;
            LabelCoordinatoreEsecuzionePersonaCodiceFiscale.Text =
                notifica.CoordinatoreSicurezzaRealizzazione.PersonaCodiceFiscale;
            LabelCoordinatoreEsecuzionePersonaIndirizzo.Text = notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo;
            LabelCoordinatoreEsecuzionePersonaCitta.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaComune;
            LabelCoordinatoreEsecuzionePersonaProvincia.Text =
                notifica.CoordinatoreSicurezzaRealizzazione.PersonaProvincia;
            LabelCoordinatoreEsecuzionePersonaCap.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaCap;
            LabelCoordinatoreEsecuzionePersonaTelefono.Text = notifica.CoordinatoreSicurezzaRealizzazione.Telefono;
            LabelCoordinatoreEsecuzionePersonaFax.Text = notifica.CoordinatoreSicurezzaRealizzazione.Fax;
            LabelCoordinatoreEsecuzionePersonaCellulare.Text =
                notifica.CoordinatoreSicurezzaRealizzazione.PersonaCellulare;
            LabelCoordinatoreEsecuzionePersonaEmail.Text = notifica.CoordinatoreSicurezzaRealizzazione.PersonaEmail;

            // Ente
            LabelCoordinatoreEsecuzioneEnteRagioneSociale.Text =
                notifica.CoordinatoreSicurezzaRealizzazione.RagioneSociale;
            LabelCoordinatoreEsecuzioneEntePartitaIva.Text = notifica.CoordinatoreSicurezzaRealizzazione.EntePartitaIva;
            LabelCoordinatoreEsecuzioneEnteCodiceFiscale.Text =
                notifica.CoordinatoreSicurezzaRealizzazione.EnteCodiceFiscale;
            LabelCoordinatoreEsecuzioneEnteIndirizzo.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteIndirizzo;
            LabelCoordinatoreEsecuzioneEnteCitta.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteComune;
            LabelCoordinatoreEsecuzioneEnteProvincia.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteProvincia;
            LabelCoordinatoreEsecuzioneEnteCap.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteCap;
            LabelCoordinatoreEsecuzioneEnteTelefono.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteTelefono;
            LabelCoordinatoreEsecuzioneEnteFax.Text = notifica.CoordinatoreSicurezzaRealizzazione.EnteFax;
        }

        // Date e numeri
        LabelDateNumeriAmmontareComplessivo.Text = Presenter.StampaValoreMonetario(notifica.AmmontareComplessivo);
        LabelDateNumeriDataInizioLavori.Text = Presenter.StampaDataFormattataClassica(notifica.DataInizioLavori);
        LabelDateNumeriDurataLavori.Text = notifica.Durata.ToString();
        LabelDateNumeriDataFineLavori.Text = Presenter.StampaDataFormattataClassica(notifica.DataFineLavori);
        LabelDateNumeriNumeroLavoratori.Text = notifica.NumeroMassimoLavoratori.ToString();
        LabelDateNumeriNumeroImprese.Text = notifica.NumeroImprese.ToString();
        LabelDateNumeriNumeroAutonomi.Text = notifica.NumeroLavoratoriAutonomi.ToString();
        LabelDateNumeriNumeroUominiGiorno.Text = notifica.NumeroGiorniUomo.ToString();

        // Imprese affidatarie
        Presenter.CaricaElementiInGridView(
            GridViewImpreseAffidatarie,
            notifica.ImpreseAffidatarie,
            0);

        // Imprese esecutrici
        Presenter.CaricaElementiInGridView(
            GridViewImpreseEsecutrici,
            notifica.ImpreseEsecutrici,
            0);
    }

    void CantiereDaNotifica1_OnCantiereGenerato()
    {
        CaricaNotifica(null);
    }

    protected void GridViewImpreseEsecutrici_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoNotificheTelematiche subappalto = (SubappaltoNotificheTelematiche) e.Row.DataItem;

            Label lImpresaRagioneSociale = (Label) e.Row.FindControl("LabelImpresaRagioneSociale");
            Label lImpresaPartitaIva = (Label) e.Row.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label) e.Row.FindControl("LabelImpresaCodiceFiscale");
            Label lSubappaltoDiRagioneSociale = (Label) e.Row.FindControl("LabelSubappaltoDiRagioneSociale");
            Label lSubappaltoDi = (Label) e.Row.FindControl("LabelSubappaltoDi");

            if (subappalto.ImpresaSelezionata != null)
            {
                lImpresaRagioneSociale.Text = subappalto.ImpresaSelezionata.RagioneSociale;
                lImpresaPartitaIva.Text = String.Format("P.Iva: {0}", subappalto.ImpresaSelezionata.PartitaIva);
                lImpresaCodiceFiscale.Text = String.Format("Cod.Fisc.: {0}", subappalto.ImpresaSelezionata.CodiceFiscale);
            }

            if (subappalto.AppaltataDa != null)
            {
                lSubappaltoDi.Visible = true;
                lSubappaltoDiRagioneSociale.Text = subappalto.AppaltataDa.RagioneSociale;
            }
        }
    }

    protected void GridViewImpreseAffidatarie_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubappaltoNotificheTelematiche subappalto = (SubappaltoNotificheTelematiche) e.Row.DataItem;
            Label lRagioneSociale = (Label) e.Row.FindControl("LabelRagioneSociale");
            Label lPartitaIva = (Label) e.Row.FindControl("LabelPartitaIva");
            Label lCodiceFiscale = (Label) e.Row.FindControl("LabelCodiceFiscale");

            lRagioneSociale.Text = subappalto.ImpresaSelezionataRagioneSociale;
            lPartitaIva.Text = String.Format("P.Iva: {0}", subappalto.ImpresaSelezionataPartitaIva);
            lCodiceFiscale.Text = String.Format("Cod.Fisc.: {0}", subappalto.ImpresaSelezionataCodiceFiscale);
        }
    }

    #region Eventi bottoni per mostrare o nascondere sezioni

    protected void ButtonMostraNascondiDatiGenerali_Click(object sender, EventArgs e)
    {
        bool mostraDatiGenerali = (bool) ViewState["MostraDatiGenerali"];
        ViewState["MostraDatiGenerali"] = !mostraDatiGenerali;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiCommittente_Click(object sender, EventArgs e)
    {
        bool mostraCommittente = (bool) ViewState["MostraCommittente"];
        ViewState["MostraCommittente"] = !mostraCommittente;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiResponsabileDeiLavori_Click(object sender, EventArgs e)
    {
        bool mostraResponsabileLavori = (bool) ViewState["MostraResponsabileLavori"];
        ViewState["MostraResponsabileLavori"] = !mostraResponsabileLavori;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiCoordinatoreSicurezzaProgettazione_Click(object sender, EventArgs e)
    {
        bool mostraCoordinatoreProgettazione = (bool) ViewState["MostraCoordinatoreSicurezzaProgettazione"];
        ViewState["MostraCoordinatoreSicurezzaProgettazione"] = !mostraCoordinatoreProgettazione;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiCoordinatoreSicurezzaEsecuzione_Click(object sender, EventArgs e)
    {
        bool mostraCoordinatoreEsecuzione = (bool) ViewState["MostraCoordinatoreSicurezzaEsecuzione"];
        ViewState["MostraCoordinatoreSicurezzaEsecuzione"] = !mostraCoordinatoreEsecuzione;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiDateNumeri_Click(object sender, EventArgs e)
    {
        bool mostraDateNumeri = (bool) ViewState["MostraDateNumeri"];
        ViewState["MostraDateNumeri"] = !mostraDateNumeri;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiImpreseAffidatarie_Click(object sender, EventArgs e)
    {
        bool mostraImpreseAffidatarie = (bool) ViewState["MostraImpreseAffidatarie"];
        ViewState["MostraImpreseAffidatarie"] = !mostraImpreseAffidatarie;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiImpreseEsecutrici_Click(object sender, EventArgs e)
    {
        bool mostraImpreseEsecutrici = (bool) ViewState["MostraImpreseEsecutrici"];
        ViewState["MostraImpreseEsecutrici"] = !mostraImpreseEsecutrici;

        GestisciBottoni();
    }

    #endregion

    #region Gestione dei bottoni Mostra/Nascondi

    private void GestisciBottoni()
    {
        bool mostraDatiGenerali = (bool) ViewState["MostraDatiGenerali"];
        bool mostraCommittente = (bool) ViewState["MostraCommittente"];
        bool mostraResponsabileLavori = (bool) ViewState["MostraResponsabileLavori"];
        bool mostraCoordinatoreSicurezzaProgettazione = (bool) ViewState["MostraCoordinatoreSicurezzaProgettazione"];
        bool mostraCoordinatoreSicurezzaEsecuzione = (bool) ViewState["MostraCoordinatoreSicurezzaEsecuzione"];
        bool mostraDateNumeri = (bool) ViewState["MostraDateNumeri"];
        bool mostraImpreseAffidatarie = (bool) ViewState["MostraImpreseAffidatarie"];
        bool mostraImpreseEsecutrici = (bool) ViewState["MostraImpreseEsecutrici"];

        if (mostraDatiGenerali)
        {
            ButtonMostraNascondiDatiGenerali.Text = TESTONASCONDI;
            divDatiGenerali.Visible = true;
        }
        else
        {
            ButtonMostraNascondiDatiGenerali.Text = TESTOMOSTRA;
            divDatiGenerali.Visible = false;
        }

        if (mostraCommittente)
        {
            ButtonMostraNascondiCommittente.Text = TESTONASCONDI;
            divCommittente.Visible = true;
        }
        else
        {
            ButtonMostraNascondiCommittente.Text = TESTOMOSTRA;
            divCommittente.Visible = false;
        }

        if (mostraResponsabileLavori)
        {
            ButtonMostraNascondiResponsabileDeiLavori.Text = TESTONASCONDI;
            divResponsabileLavori.Visible = true;
        }
        else
        {
            ButtonMostraNascondiResponsabileDeiLavori.Text = TESTOMOSTRA;
            divResponsabileLavori.Visible = false;
        }

        if (mostraCoordinatoreSicurezzaProgettazione)
        {
            ButtonMostraNascondiCoordinatoreSicurezzaProgettazione.Text = TESTONASCONDI;
            divCoordinatoreSicurezzaProgettazione.Visible = true;
        }
        else
        {
            ButtonMostraNascondiCoordinatoreSicurezzaProgettazione.Text = TESTOMOSTRA;
            divCoordinatoreSicurezzaProgettazione.Visible = false;
        }

        if (mostraCoordinatoreSicurezzaEsecuzione)
        {
            ButtonMostraNascondiCoordinatoreSicurezzaEsecuzione.Text = TESTONASCONDI;
            divCoordinatoreSicurezzaEsecuzione.Visible = true;
        }
        else
        {
            ButtonMostraNascondiCoordinatoreSicurezzaEsecuzione.Text = TESTOMOSTRA;
            divCoordinatoreSicurezzaEsecuzione.Visible = false;
        }

        if (mostraDateNumeri)
        {
            ButtonMostraNascondiDateNumeri.Text = TESTONASCONDI;
            divDateNumeri.Visible = true;
        }
        else
        {
            ButtonMostraNascondiDateNumeri.Text = TESTOMOSTRA;
            divDateNumeri.Visible = false;
        }

        if (mostraImpreseAffidatarie)
        {
            ButtonMostraNascondiImpreseAffidatarie.Text = TESTONASCONDI;
            divImpreseAffidatarie.Visible = true;
        }
        else
        {
            ButtonMostraNascondiImpreseAffidatarie.Text = TESTOMOSTRA;
            divImpreseAffidatarie.Visible = false;
        }

        if (mostraImpreseEsecutrici)
        {
            ButtonMostraNascondiImpreseEsecutrici.Text = TESTONASCONDI;
            divImpreseEsecutrici.Visible = true;
        }
        else
        {
            ButtonMostraNascondiImpreseEsecutrici.Text = TESTOMOSTRA;
            divImpreseEsecutrici.Visible = false;
        }
    }

    #endregion
}