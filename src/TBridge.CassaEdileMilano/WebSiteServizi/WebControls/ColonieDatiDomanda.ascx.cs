using System;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using CassaEdile = TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;

public partial class WebControls_ColonieDatiDomanda : UserControl
{
    private readonly ColonieBusiness _colonieBusiness = new ColonieBusiness();
    private readonly Common _commonBiz = new Common();
    private CassaEdile _cassaEdile;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Per prevenire click multipli

        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonAzione, null));
        sb.Append(";");
        sb.Append("return true;");
        ButtonAzione.Attributes.Add("onclick", sb.ToString());

        #endregion

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAzione);

        // Recupero la cassa edile loggata
        if (GestioneUtentiBiz.IsCassaEdile())
        {
            //cassaEdile =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.CassaEdile)HttpContext.Current.User.Identity).Entity;
            _cassaEdile = (CassaEdile) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        }

        // Registro l'evento sulla selezione del lavoratore
        ColonieRicercaLavoratoreACE1.OnLavoratoreACESelected += ColonieRicercaLavoratoreACE1_OnLavoratoreACESelected;
        if (ViewState["IdVacanza"] != null)
            ColonieRicercaLavoratoreACE1.ImpostaIdVacanza((int) ViewState["IdVacanza"]);

        if (!Page.IsPostBack)
        {
            //CaricaDatiProva();
            CaricaTaglie();
        }
    }

    private void CaricaDatiProva()
    {
        TextBoxLavoratoreCognome.Text = "Paperino";
        TextBoxLavoratoreNome.Text = "Paolino";
        TextBoxLavoratoreDataNascita.Text = "01/01/1956";
        TextBoxLavoratoreCodiceFiscale.Text = "pprpln23t56j789u";
        TextBoxLavoratoreTelefono.Text = "2321231";
        TextBoxLavoratoreCellulare.Text = "4564658";

        TextBoxLavoratoreResidenzaIndirizzo.Text = "via vai 3";
        TextBoxLavoratoreResidenzaComune.Text = "paperopoli";
        TextBoxLavoratoreResidenzaProvincia.Text = "pa";
        TextBoxLavoratoreResidenzaCap.Text = "20564";

        TextBoxBambinoCognome.Text = "Qui";
        TextBoxBambinoNome.Text = "Quo";
        TextBoxBambinoDataNascita.Text = "01/01/2000";
        TextBoxBambinoCodiceFiscale.Text = "quiquo78j67g654f";

        TextBoxTipoDisabilita.Text = "sdfsdfsdfsdfsd";
        TextBoxAccompagnatoreCognome.Text = "Paperina";
        TextBoxAccompagnatoreNome.Text = "Paolina";
        TextBoxAccompagnatoreDataNascita.Text = "01/01/1970";

        TextBoxBambinoIntolleranze.Text = "sdfsdfsdfsdfsdfsdfsdfd";
    }

    protected void ButtonAzione_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ControlloAccompagnatore())
            {
                LabelErroreAccompagnatore.Visible = false;

                DomandaACE domanda = CreaDomanda();
                bool giaPresente = false;

                if (!_colonieBusiness.InsertUpdateDomandaACE(domanda, out giaPresente))
                {
                    if (giaPresente)
                    {
                        LabelGiaPresente.Visible = true;
                        LabelErrore.Visible = false;
                    }
                    else
                    {
                        LabelErrore.Visible = true;
                        LabelGiaPresente.Visible = false;
                    }
                }
                else
                {
                    LabelErrore.Visible = false;
                    LabelGiaPresente.Visible = false;
                    Reset();
                }
            }
            else
                LabelErroreAccompagnatore.Visible = true;
        }
    }

    private bool ControlloAccompagnatore()
    {
        bool res = false;

        if (!string.IsNullOrEmpty(TextBoxAccompagnatoreCognome.Text) &&
            !string.IsNullOrEmpty(TextBoxAccompagnatoreNome.Text) &&
            !string.IsNullOrEmpty(TextBoxAccompagnatoreDataNascita.Text))
            res = true;

        if (string.IsNullOrEmpty(TextBoxAccompagnatoreCognome.Text) &&
            string.IsNullOrEmpty(TextBoxAccompagnatoreNome.Text) &&
            string.IsNullOrEmpty(TextBoxAccompagnatoreDataNascita.Text))
            res = true;

        return res;
    }

    public void CaricaDomanda(DomandaACE domanda, bool readOnly, string idCassaEdile)
    {
        if (readOnly)
        {
            PanelDati.Enabled = false;
            ButtonAzione.Visible = false;
            ButtonSelezionaLavoratore.Visible = false;
            ButtonNuovoLavoratore.Visible = false;
            ButtonNuovoBambino.Visible = false;
            ButtonNuovoAccompagnatore.Visible = false;
            ButtonModificaDatiBambino.Visible = false;
            ButtonModificaDatiLavoratore.Visible = false;
        }

        ViewState["IdDomanda"] = domanda.IdDomanda.Value;

        ViewState["IdLavoratore"] = domanda.Bambino.Parente.IdLavoratore.Value;
        TextBoxLavoratoreCognome.Text = domanda.Bambino.Parente.Cognome;
        TextBoxLavoratoreNome.Text = domanda.Bambino.Parente.Nome;
        TextBoxLavoratoreDataNascita.Text = domanda.Bambino.Parente.DataNascita.ToShortDateString();
        TextBoxLavoratoreCodiceFiscale.Text = domanda.Bambino.Parente.CodiceFiscale;
        if (domanda.Bambino.Parente.Sesso == 'M')
        {
            RadioButtonLavoratoreM.Checked = true;
            RadioButtonLavoratoreF.Checked = false;
        }
        else
        {
            RadioButtonLavoratoreM.Checked = false;
            RadioButtonLavoratoreF.Checked = true;
        }
        TextBoxLavoratoreTelefono.Text = domanda.Bambino.Parente.Telefono;
        TextBoxLavoratoreCellulare.Text = domanda.Bambino.Parente.Cellulare;
        TextBoxLavoratoreResidenzaIndirizzo.Text = domanda.Bambino.Parente.Indirizzo;
        TextBoxLavoratoreResidenzaProvincia.Text = domanda.Bambino.Parente.Provincia;
        TextBoxLavoratoreResidenzaComune.Text = domanda.Bambino.Parente.Comune;
        TextBoxLavoratoreResidenzaCap.Text = domanda.Bambino.Parente.Cap;
        if (domanda.Bambino.Parente.Modificabile)
            ButtonModificaDatiLavoratore.Visible = true;
        else
            ButtonModificaDatiLavoratore.Visible = false;

        ViewState["IdFamiliare"] = domanda.Bambino.IdFamiliare.Value;
        TextBoxBambinoCognome.Text = domanda.Bambino.Cognome;
        TextBoxBambinoNome.Text = domanda.Bambino.Nome;
        TextBoxBambinoDataNascita.Text = domanda.Bambino.DataNascita.ToShortDateString();
        TextBoxBambinoCodiceFiscale.Text = domanda.Bambino.CodiceFiscale;
        if (domanda.Bambino.Sesso == 'M')
        {
            RadioButtonBambinoM.Checked = true;
            RadioButtonBambinoF.Checked = false;
        }
        else
        {
            RadioButtonBambinoM.Checked = false;
            RadioButtonBambinoF.Checked = true;
        }
        CheckBoxBambinoHandicap.Checked = domanda.Bambino.PortatoreHandicap;
        if (domanda.Bambino.PortatoreHandicap)
        {
            PanelTipoDisabilita.Enabled = true;
            ButtonNuovoAccompagnatore.Enabled = true;

            if (domanda.Accompagnatore != null)
            {
                ViewState["IdAccompagnatore"] = domanda.Accompagnatore.IdAccompagnatore.Value;
                TextBoxAccompagnatoreCognome.Text = domanda.Accompagnatore.Cognome;
                TextBoxAccompagnatoreNome.Text = domanda.Accompagnatore.Nome;
                TextBoxAccompagnatoreDataNascita.Text = domanda.Accompagnatore.DataNascita.ToShortDateString();
                if (domanda.Accompagnatore.Sesso == 'M')
                {
                    RadioButtonAccompagnatoreM.Checked = true;
                    RadioButtonAccompagnatoreF.Checked = false;
                }
                else
                {
                    RadioButtonAccompagnatoreM.Checked = false;
                    RadioButtonAccompagnatoreF.Checked = true;
                }
            }
        }
        else
        {
            PanelTipoDisabilita.Enabled = false;
            PanelAccompagnatore.Enabled = false;
            ButtonNuovoAccompagnatore.Enabled = PanelAccompagnatore.Enabled;
        }
        TextBoxTipoDisabilita.Text = domanda.Bambino.NotaDisabilita;
        TextBoxBambinoIntolleranze.Text = domanda.Bambino.IntolleranzeAlimentari;
        if (domanda.Bambino.Modificabile)
            ButtonModificaDatiBambino.Visible = true;
        else
            ButtonModificaDatiBambino.Visible = false;

        Vacanza vacanza = _colonieBusiness.GetVacanzaByTurno(domanda.IdTurno);
        ImpostaVacanza(vacanza);
        ButtonAzione.Text = "Modifica";

        Turno turno = _colonieBusiness.GetTurni(domanda.IdTurno, null, null, null, null)[0];
        DropDownListDestinazione.SelectedValue = turno.Destinazione.IdCombo.ToString();
        CaricaTuttiTurni(vacanza.IdVacanza.Value, turno.Destinazione.IdDestinazione.Value);
        DropDownListTurno.SelectedValue = turno.IdCombo.ToString();

        if (domanda.IdTaglia.HasValue)
        {
            DropDownListTaglia.SelectedValue = domanda.IdTaglia.Value.ToString();
        }
        else
        {
            DropDownListTaglia.SelectedIndex = 0;
        }
    }

    private void Reset()
    {
        DropDownListDestinazione.SelectedValue = string.Empty;
        DropDownListTurno.Items.Clear();

        TextBoxLavoratoreCognome.Text = null;
        TextBoxLavoratoreNome.Text = null;
        TextBoxLavoratoreDataNascita.Text = null;
        TextBoxLavoratoreCodiceFiscale.Text = null;
        RadioButtonLavoratoreM.Checked = true;
        RadioButtonLavoratoreF.Checked = false;
        TextBoxLavoratoreTelefono.Text = null;
        TextBoxLavoratoreCellulare.Text = null;
        TextBoxLavoratoreResidenzaIndirizzo.Text = null;
        TextBoxLavoratoreResidenzaProvincia.Text = null;
        TextBoxLavoratoreResidenzaComune.Text = null;
        TextBoxLavoratoreResidenzaCap.Text = null;

        TextBoxBambinoCognome.Text = null;
        TextBoxBambinoNome.Text = null;
        TextBoxBambinoDataNascita.Text = null;
        TextBoxBambinoCodiceFiscale.Text = null;
        RadioButtonBambinoM.Checked = true;
        RadioButtonBambinoF.Checked = false;
        CheckBoxBambinoHandicap.Checked = false;
        TextBoxTipoDisabilita.Text = null;
        TextBoxBambinoIntolleranze.Text = null;

        PanelLavoratore.Enabled = false;
        PanelBambino.Enabled = false;
        PanelAccompagnatore.Enabled = false;
        TextBoxAccompagnatoreCognome.Text = null;
        TextBoxAccompagnatoreNome.Text = null;
        TextBoxAccompagnatoreDataNascita.Text = null;
        RadioButtonAccompagnatoreM.Checked = true;
        RadioButtonAccompagnatoreF.Checked = false;
        ButtonNuovoAccompagnatore.Enabled = false;
        ButtonModificaDatiBambino.Visible = false;
        ButtonModificaDatiLavoratore.Visible = false;

        DropDownListTaglia.SelectedIndex = 0;
    }

    public void ImpostaVacanza(Vacanza vacanza)
    {
        LabelVacanza.Text = vacanza.ToString();
        ViewState["IdVacanza"] = vacanza.IdVacanza.Value;
        CaricaDestinazioni(vacanza.TipoVacanza.IdTipoVacanza);

        ButtonAzione.Text = "Inserisci";
    }

    private void CaricaDestinazioni(int idTipoVacanza)
    {
        DropDownListDestinazione.Items.Clear();

        DropDownListDestinazione.Items.Add(new ListItem(string.Empty, string.Empty));
        DestinazioneCollection destinazioni = _colonieBusiness.GetDestinazioni(idTipoVacanza, null);
        DropDownListDestinazione.DataSource = destinazioni;
        DropDownListDestinazione.DataTextField = "DestinazioneLuogo";
        DropDownListDestinazione.DataValueField = "IdCombo";
        DropDownListDestinazione.DataBind();
    }

    /// <summary>
    ///   Carica solo i turni validi
    /// </summary>
    /// <param name = "idVacanza"></param>
    /// <param name = "idDestinazione"></param>
    private void CaricaTurni(int idVacanza, int idDestinazione)
    {
        DateTime adesso = DateTime.Now;
        DropDownListTurno.Items.Clear();

        DropDownListTurno.Items.Add(new ListItem(string.Empty, string.Empty));
        TurnoCollection turni = _colonieBusiness.GetTurni(null, idVacanza, idDestinazione, null, null);

        foreach (Turno turno in turni)
        {
            if (turno.Dal > adesso)
                DropDownListTurno.Items.Add(new ListItem(turno.DettaglioTurno, turno.IdCombo.ToString()));
        }

        //DropDownListTurno.DataSource = turni;
        //DropDownListTurno.DataValueField = "IdCombo";
        //DropDownListTurno.DataTextField = "DettaglioTurno";
        //DropDownListTurno.DataBind();
    }

    private void CaricaTuttiTurni(int idVacanza, int idDestinazione)
    {
        DropDownListTurno.Items.Clear();

        DropDownListTurno.Items.Add(new ListItem(string.Empty, string.Empty));
        TurnoCollection turni = _colonieBusiness.GetTurni(null, idVacanza, idDestinazione, null, null);
        DropDownListTurno.DataSource = turni;
        DropDownListTurno.DataValueField = "IdCombo";
        DropDownListTurno.DataTextField = "DettaglioTurno";
        DropDownListTurno.DataBind();
    }

    protected void DropDownListDestinazione_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idDestinazione = -1;
        if (!Int32.TryParse(DropDownListDestinazione.SelectedValue, out idDestinazione))
            idDestinazione = -1;

        int idVacaza = (int) ViewState["IdVacanza"];

        CaricaTurni(idVacaza, idDestinazione);
    }

    private DomandaACE CreaDomanda()
    {
        DomandaACE domanda = new DomandaACE();

        if (ViewState["IdDomanda"] != null)
        {
            // Imposto l'id della domanda
            domanda.IdDomanda = (int) ViewState["IdDomanda"];
        }

        domanda.IdTurno = Int32.Parse(DropDownListTurno.SelectedValue);

        if (ViewState["IdFamiliare"] != null)
            domanda.Bambino.IdFamiliare = (int) ViewState["IdFamiliare"];
        domanda.Bambino.Cognome = TextBoxBambinoCognome.Text.Trim().ToUpper();
        domanda.Bambino.Nome = TextBoxBambinoNome.Text.Trim().ToUpper();
        domanda.Bambino.DataNascita = DateTime.Parse(TextBoxBambinoDataNascita.Text.Replace('.', '/'));
        domanda.Bambino.CodiceFiscale = TextBoxBambinoCodiceFiscale.Text.Trim().ToUpper();
        if (RadioButtonBambinoM.Checked)
            domanda.Bambino.Sesso = 'M';
        else
            domanda.Bambino.Sesso = 'F';

        domanda.Bambino.PortatoreHandicap = CheckBoxBambinoHandicap.Checked;
        if (domanda.Bambino.PortatoreHandicap)
            domanda.Bambino.NotaDisabilita = TextBoxTipoDisabilita.Text.Trim().ToUpper();
        // Controllo se � presente un accompagnatore
        if (!string.IsNullOrEmpty(TextBoxAccompagnatoreCognome.Text))
        {
            domanda.Accompagnatore = new Accompagnatore();
            if (ViewState["IdAccompagnatore"] != null)
                domanda.Accompagnatore.IdAccompagnatore = (int) ViewState["IdAccompagnatore"];
            domanda.Accompagnatore.Cognome = TextBoxAccompagnatoreCognome.Text.Trim().ToUpper();
            domanda.Accompagnatore.Nome = TextBoxAccompagnatoreNome.Text.Trim().ToUpper();
            domanda.Accompagnatore.DataNascita = DateTime.Parse(TextBoxAccompagnatoreDataNascita.Text.Replace('.', '/'));
            if (RadioButtonAccompagnatoreM.Checked)
                domanda.Accompagnatore.Sesso = 'M';
            else
                domanda.Accompagnatore.Sesso = 'F';
        }
        if (!string.IsNullOrEmpty(TextBoxBambinoIntolleranze.Text.Trim()))
            domanda.Bambino.IntolleranzeAlimentari = TextBoxBambinoIntolleranze.Text.Trim().ToUpper();

        domanda.Bambino.Parente = new LavoratoreACE();
        if (ViewState["IdLavoratore"] != null)
            domanda.Bambino.Parente.IdLavoratore = (int) ViewState["IdLavoratore"];
        domanda.Bambino.Parente.Cognome = TextBoxLavoratoreCognome.Text.Trim().ToUpper();
        domanda.Bambino.Parente.Nome = TextBoxLavoratoreNome.Text.Trim().ToUpper();
        domanda.Bambino.Parente.DataNascita = DateTime.Parse(TextBoxLavoratoreDataNascita.Text.Replace('.', '/'));
        domanda.Bambino.Parente.CodiceFiscale = TextBoxLavoratoreCodiceFiscale.Text.Trim().ToUpper();
        if (RadioButtonLavoratoreM.Checked)
            domanda.Bambino.Parente.Sesso = 'M';
        else
            domanda.Bambino.Parente.Sesso = 'F';
        if (!string.IsNullOrEmpty(TextBoxLavoratoreTelefono.Text.Trim()))
            domanda.Bambino.Parente.Telefono = TextBoxLavoratoreTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxLavoratoreCellulare.Text.Trim()))
            domanda.Bambino.Parente.Cellulare = TextBoxLavoratoreCellulare.Text.Trim().ToUpper();
        domanda.Bambino.Parente.Indirizzo = TextBoxLavoratoreResidenzaIndirizzo.Text.Trim().ToUpper();
        domanda.Bambino.Parente.Provincia = TextBoxLavoratoreResidenzaProvincia.Text.Trim().ToUpper();
        domanda.Bambino.Parente.Comune = TextBoxLavoratoreResidenzaComune.Text.Trim().ToUpper();
        domanda.Bambino.Parente.Cap = TextBoxLavoratoreResidenzaCap.Text;
        domanda.Bambino.Parente.IdCassaEdile = _cassaEdile.IdCassaEdile;

        if (!string.IsNullOrEmpty(DropDownListTaglia.SelectedValue))
            domanda.IdTaglia = Int32.Parse(DropDownListTaglia.SelectedValue);

        return domanda;
    }

    protected void CheckBoxBambinoHandicap_CheckedChanged(object sender, EventArgs e)
    {
        PanelTipoDisabilita.Enabled = CheckBoxBambinoHandicap.Checked;
        PanelAccompagnatore.Enabled = false;
        ButtonNuovoAccompagnatore.Enabled = CheckBoxBambinoHandicap.Checked;
        ResetCampiAccompagnatore();
    }

    private void CaricaTaglie()
    {
        _colonieBusiness.CaricaTaglieInDropDown(DropDownListTaglia);
    }

    protected void TextBoxBambinoDataNascita_TextChanged1(object sender, EventArgs e)
    {
        DateTime dataNascita;

        if (DateTime.TryParseExact(TextBoxBambinoDataNascita.Text, "dd/MM/yyyy", null, DateTimeStyles.None,
                                   out dataNascita))
        {
            // Se il bambino ha un et� >= 11 anni non visualizzo la drop down per la taglia
            if ((DateTime.Now - dataNascita).Days/365 >= 11)
            {
                DropDownListTaglia.Enabled = false;
                RequiredFieldValidatorTaglia.Enabled = false;
                DropDownListTaglia.SelectedIndex = 0;
            }
            else
            {
                DropDownListTaglia.Enabled = true;
                RequiredFieldValidatorTaglia.Enabled = true;
            }
        }
        else
        {
            DropDownListTaglia.Enabled = true;
            RequiredFieldValidatorTaglia.Enabled = true;
        }
    }

    #region Gestione Lavoratore

    private void ResetCampiLavoratore()
    {
        ViewState["IdLavoratore"] = null;
        TextBoxLavoratoreCognome.Text = string.Empty;
        TextBoxLavoratoreNome.Text = string.Empty;
        TextBoxLavoratoreDataNascita.Text = string.Empty;
        TextBoxLavoratoreCodiceFiscale.Text = string.Empty;
        TextBoxLavoratoreTelefono.Text = string.Empty;
        TextBoxLavoratoreCellulare.Text = string.Empty;
        RadioButtonLavoratoreM.Checked = true;
        RadioButtonLavoratoreF.Checked = false;
        TextBoxLavoratoreResidenzaIndirizzo.Text = string.Empty;
        TextBoxLavoratoreResidenzaProvincia.Text = string.Empty;
        TextBoxLavoratoreResidenzaComune.Text = string.Empty;
        TextBoxLavoratoreResidenzaCap.Text = string.Empty;
        ButtonModificaDatiLavoratore.Visible = false;
    }

    private void CaricaDatiLavoratore(LavoratoreACE lavoratore)
    {
        ViewState["IdLavoratore"] = lavoratore.IdLavoratore.Value;
        TextBoxLavoratoreCognome.Text = lavoratore.Cognome;
        TextBoxLavoratoreNome.Text = lavoratore.Nome;
        TextBoxLavoratoreDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
        TextBoxLavoratoreCodiceFiscale.Text = lavoratore.CodiceFiscale;
        if (lavoratore.Sesso == 'M')
        {
            RadioButtonLavoratoreM.Checked = true;
            RadioButtonLavoratoreF.Checked = false;
        }
        else
        {
            RadioButtonLavoratoreM.Checked = false;
            RadioButtonLavoratoreF.Checked = true;
        }
        TextBoxLavoratoreTelefono.Text = lavoratore.Telefono;
        TextBoxLavoratoreCellulare.Text = lavoratore.Cellulare;
        TextBoxLavoratoreResidenzaIndirizzo.Text = lavoratore.Indirizzo;
        TextBoxLavoratoreResidenzaProvincia.Text = lavoratore.Provincia;
        TextBoxLavoratoreResidenzaComune.Text = lavoratore.Comune;
        TextBoxLavoratoreResidenzaCap.Text = lavoratore.Cap;

        if (lavoratore.Modificabile)
            ButtonModificaDatiLavoratore.Visible = true;
        else
            ButtonModificaDatiLavoratore.Visible = false;

        CaricaBambini(lavoratore.IdLavoratore.Value);
    }

    protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
    {
        ColonieRicercaLavoratoreACE1.Visible = true;
    }

    private void ColonieRicercaLavoratoreACE1_OnLavoratoreACESelected(LavoratoreACE lavoratore)
    {
        CaricaDatiLavoratore(lavoratore);
        ColonieRicercaLavoratoreACE1.Visible = false;
        PanelLavoratore.Enabled = false;

        ResetCampiBambino();
        PanelBambino.Enabled = false;
    }

    protected void ButtonNuovoLavoratore_Click(object sender, EventArgs e)
    {
        ResetCampiLavoratore();
        PanelLavoratore.Enabled = true;

        ResetCampiBambino();
    }

    protected void ButtonModificaDatiLavoratore_Click(object sender, EventArgs e)
    {
        PanelLavoratore.Enabled = true;
    }

    #endregion

    #region Gestione Bambino

    private void CaricaBambini(int idLavoratore)
    {
        FamiliareACEFilter filtro = new FamiliareACEFilter();
        filtro.IdLavoratore = idLavoratore;
        FamiliareACECollection bambini = _colonieBusiness.GetFamiliariACE(filtro);
        GridViewBambini.Visible = true;
        GridViewBambini.DataSource = bambini;
        GridViewBambini.PageIndex = 0;
        GridViewBambini.DataBind();
    }

    protected void ButtonNuovoBambino_Click(object sender, EventArgs e)
    {
        GridViewBambini.Visible = false;
        DropDownListTaglia.Enabled = true;
        ResetCampiBambino();
        PanelBambino.Enabled = true;
    }

    private void ResetCampiBambino()
    {
        ViewState["IdFamiliare"] = null;
        TextBoxBambinoCognome.Text = string.Empty;
        TextBoxBambinoNome.Text = string.Empty;
        TextBoxBambinoDataNascita.Text = string.Empty;
        TextBoxBambinoCodiceFiscale.Text = string.Empty;
        RadioButtonBambinoM.Checked = true;
        RadioButtonBambinoF.Checked = false;
        TextBoxBambinoIntolleranze.Text = string.Empty;
        CheckBoxBambinoHandicap.Checked = false;
        PanelTipoDisabilita.Enabled = false;
        PanelAccompagnatore.Enabled = false;
        ButtonNuovoAccompagnatore.Enabled = PanelAccompagnatore.Enabled;
        ResetCampiAccompagnatore();
        TextBoxTipoDisabilita.Text = string.Empty;
        ButtonModificaDatiBambino.Visible = false;
    }

    private void ResetCampiAccompagnatore()
    {
        TextBoxAccompagnatoreCognome.Text = string.Empty;
        TextBoxAccompagnatoreNome.Text = string.Empty;
        TextBoxAccompagnatoreDataNascita.Text = string.Empty;
        RadioButtonAccompagnatoreM.Checked = true;
        RadioButtonAccompagnatoreF.Checked = false;
    }

    protected void GridViewBambini_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idFamiliare = (int) GridViewBambini.DataKeys[e.NewSelectedIndex].Value;

        int? idVacanza = null;
        if (ViewState["IdVacanza"] != null)
            idVacanza = (int) ViewState["IdVacanza"];

        FamiliareACE familiare = _colonieBusiness.GetFamiliareACE(idFamiliare, idVacanza);
        GridViewBambini.Visible = false;

        ResetCampiAccompagnatore();

        CaricaDatiBambino(familiare);

        CaricaUltimaTaglia(idFamiliare);
    }

    protected void GridViewBambini_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewBambini.PageIndex = e.NewPageIndex;
        int idLavoratore = (int) ViewState["IdLavoratore"];

        CaricaBambini(idLavoratore);
    }

    private void CaricaDatiBambino(FamiliareACE familiare)
    {
        ViewState["IdFamiliare"] = familiare.IdFamiliare.Value;
        TextBoxBambinoCognome.Text = familiare.Cognome;
        TextBoxBambinoNome.Text = familiare.Nome;
        TextBoxBambinoDataNascita.Text = familiare.DataNascita.ToShortDateString();
        TextBoxBambinoCodiceFiscale.Text = familiare.CodiceFiscale;
        if (familiare.Sesso == 'M')
        {
            RadioButtonBambinoM.Checked = true;
            RadioButtonBambinoF.Checked = false;
        }
        else
        {
            RadioButtonBambinoM.Checked = false;
            RadioButtonBambinoF.Checked = true;
        }
        CheckBoxBambinoHandicap.Checked = familiare.PortatoreHandicap;
        if (familiare.PortatoreHandicap)
        {
            PanelTipoDisabilita.Enabled = true;
            ButtonNuovoAccompagnatore.Enabled = true;

            CaricaUltimoAccompagnatore(familiare.IdFamiliare.Value);

            PanelAccompagnatore.Enabled = false;
            ButtonNuovoAccompagnatore.Enabled = true;
        }
        else
        {
            PanelTipoDisabilita.Enabled = false;
            PanelAccompagnatore.Enabled = false;
            ButtonNuovoAccompagnatore.Enabled = PanelAccompagnatore.Enabled;
        }
        TextBoxTipoDisabilita.Text = familiare.NotaDisabilita;
        TextBoxBambinoIntolleranze.Text = familiare.IntolleranzeAlimentari;

        if (familiare.Modificabile)
            ButtonModificaDatiBambino.Visible = true;
        else
            ButtonModificaDatiBambino.Visible = false;

        // Se il bambino ha un et� >= 11 anni non visualizzo la drop down per la taglia
        if ((DateTime.Now - familiare.DataNascita).Days/365 >= 11)
        {
            DropDownListTaglia.Enabled = false;
            RequiredFieldValidatorTaglia.Enabled = false;
            DropDownListTaglia.SelectedIndex = 0;
        }
        else
        {
            DropDownListTaglia.Enabled = true;
            RequiredFieldValidatorTaglia.Enabled = true;
        }
    }

    private bool CaricaUltimaTaglia(int idFamiliare)
    {
        bool res = false;

        int? idTaglia = _colonieBusiness.GetUltimaTaglia(idFamiliare);

        if (idTaglia.HasValue)
        {
            DropDownListTaglia.SelectedValue = idTaglia.ToString();
        }

        return res;
    }

    private bool CaricaUltimoAccompagnatore(int idFamiliareACE)
    {
        bool res = false;
        Accompagnatore accompagnatore = _colonieBusiness.GetUltimoAccompagnatore(idFamiliareACE);

        if (accompagnatore != null)
        {
            TextBoxAccompagnatoreCognome.Text = accompagnatore.Cognome;
            TextBoxAccompagnatoreNome.Text = accompagnatore.Nome;
            TextBoxAccompagnatoreDataNascita.Text = accompagnatore.DataNascita.ToShortDateString();
            if (accompagnatore.Sesso == 'M')
            {
                RadioButtonAccompagnatoreM.Checked = true;
                RadioButtonAccompagnatoreF.Checked = false;
            }
            else
            {
                RadioButtonAccompagnatoreM.Checked = false;
                RadioButtonAccompagnatoreF.Checked = true;
            }

            res = true;
        }

        return res;
    }

    protected void ButtonNuovoAccompagnatore_Click(object sender, EventArgs e)
    {
        ResetCampiAccompagnatore();
        PanelAccompagnatore.Enabled = true;
        ButtonNuovoAccompagnatore.Enabled = PanelAccompagnatore.Enabled;
    }

    protected void ButtonModificaDatiBambino_Click(object sender, EventArgs e)
    {
        PanelBambino.Enabled = true;
    }

    protected void TextBoxBambinoDataNascita_TextChanged(object sender, EventArgs e)
    {
        Page.Validate("controlloDataNascita");
        if (Page.IsValid)
        {
            // Fare il controllo sulle date per la data di nascita del bimbo
            if (ViewState["IdVacanza"] != null)
            {
                int idVacanza = (int) ViewState["IdVacanza"];
                Vacanza vacanza = _colonieBusiness.GetVacanze(null, idVacanza)[0];
                DateTime dataNascita = DateTime.Parse(TextBoxBambinoDataNascita.Text.Replace('.', '/'));

                // Controllo
                if (vacanza.LimiteInferioreBimbi.Year > dataNascita.Year ||
                    vacanza.LimiteSuperioreBimbi.Year < dataNascita.Year)
                    LabelErroreDataNascita.Visible = true;
                else
                    LabelErroreDataNascita.Visible = false;
            }
        }
    }

    #endregion

    #region Custom Validators

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!String.IsNullOrEmpty(TextBoxBambinoDataNascita.Text))
            {
                DateTime dataNascita = DateTime.Parse(TextBoxBambinoDataNascita.Text.Replace('.', '/'));
                String sesso;

                sesso = RadioButtonBambinoM.Checked ? "M" : "F";

                //Controlliamo il codice Fiscale parzialmente per evitare i problemi di omocodia
                if (!_commonBiz.VerificaPrimi11CaratteriCodiceFiscale(
                    Presenter.NormalizzaCampoTesto(TextBoxBambinoNome.Text),
                    Presenter.NormalizzaCampoTesto(TextBoxBambinoCognome.Text),
                    sesso,
                    dataNascita,
                    Presenter.NormalizzaCampoTesto(TextBoxBambinoCodiceFiscale.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CodiceFiscaleCarattereControllo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(TextBoxBambinoCodiceFiscale.Text))
            {
                args.IsValid = false;
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorCodiceFiscale_ServerValidateLav(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!String.IsNullOrEmpty(TextBoxLavoratoreDataNascita.Text))
            {
                DateTime dataNascita = DateTime.Parse(TextBoxLavoratoreDataNascita.Text.Replace('.', '/'));
                String sesso;

                sesso = RadioButtonLavoratoreM.Checked ? "M" : "F";

                //Controlliamo il codice Fiscale parzialmente per evitare i problemi di omocodia
                if (!_commonBiz.VerificaPrimi11CaratteriCodiceFiscale(
                    Presenter.NormalizzaCampoTesto(TextBoxLavoratoreNome.Text),
                    Presenter.NormalizzaCampoTesto(TextBoxLavoratoreCognome.Text),
                    sesso,
                    dataNascita,
                    Presenter.NormalizzaCampoTesto(TextBoxLavoratoreCodiceFiscale.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CodiceFiscaleCarattereControllo_ServerValidateLav(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(TextBoxLavoratoreCodiceFiscale.Text))
            {
                args.IsValid = false;
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    #endregion
}