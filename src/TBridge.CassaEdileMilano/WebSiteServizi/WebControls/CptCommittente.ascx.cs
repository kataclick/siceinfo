using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;

public partial class WebControls_CptCommittente : UserControl
{
    private string errore;

    /// <summary>
    /// Propriet� che costruisce il committente. Se sono presenti errori ritorna null
    /// </summary>
    public Committente Committente
    {
        get
        {
            if (ControlloCampiServer())
            {
                return CreaCommittente();
            }
            else return null;
        }
    }

    public string Errore
    {
        get { return errore; }
        set { errore = value; }
    }

    #region committente properties

    public int IdCommittente { get; set; }

    public string RagioneSociale { get; set; }

    public string PartitaIVA { get; set; }

    public string CodiceFiscale { get; set; }

    public string ComIndirizzo { get; set; }

    public string Provincia { get; set; }

    public string Comune { get; set; }

    public string CAP { get; set; }

    #endregion

    #region Biz properties

    public TSBusiness TsBiz { get; set; }

    public CantieriBusiness CantieriBiz { get; set; }

    #endregion

    public void ImpostaRagioneSociale(string ragioneSociale)
    {
        TextBoxRagioneSociale.Text = ragioneSociale;
    }

    public void ImpostaCommittente(Committente committente)
    {
        ViewState["IdCommittente"] = committente.IdCommittente;
        TextBoxRagioneSociale.Text = committente.RagioneSociale;
        TextBoxPartitaIva.Text = committente.PartitaIva;
        TextBoxCodiceFiscale.Text = committente.CodiceFiscale;
        TextBoxTelefono.Text = committente.Telefono;
        TextBoxFax.Text = committente.Fax;
        TextBoxPersonaRiferimento.Text = committente.PersonaRiferimento;

        CptSelezioneIndirizzo1.ImpostaIndirizzo(committente);
    }

    public void Reset()
    {
        TextBoxIdCommittente.Text = string.Empty;
        CptSelezioneIndirizzo1.CancellaIndirizzo();
        TextBoxCodiceFiscale.Text = string.Empty;
        TextBoxPartitaIva.Text = string.Empty;
        TextBoxRagioneSociale.Text = string.Empty;
        TextBoxTelefono.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxPersonaRiferimento.Text = string.Empty;
    }

    /// <summary>
    /// Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        // CAMPI CONTROLLATI
        // Ragione sociale

        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            res = false;
            errori.Append("Campo ragione sociale vuoto" + Environment.NewLine);
        }
        if (!CptSelezioneIndirizzo1.IndirizzoSelezionato())
        {
            res = false;
            errori.Append("Indirizzo vuoto" + Environment.NewLine);
        }

        if (!res)
            errore = errori.ToString();

        return res;
    }

    //protected Committente GetCommittente()
    //{
    //    if (ControlloCampiServer())
    //    {
    //        Committente committente = CreaCommittente();
    //        bool res = false; ;

    //        if (Request.QueryString["modalita"] == "modifica")
    //            aggiornamento = true;

    //        // Eseguo l'operazione
    //        if (aggiornamento)
    //        {
    //            int idCommittente = -1;
    //            Int32.TryParse(TextBoxIdCommittente.Text, out idCommittente);
    //            committente.IdCommittente = idCommittente;

    //            // Aggiorno il record
    //            res = biz.UpdateCommittente(committente);
    //        }
    //        else
    //        {
    //            // Inserisco il record
    //            biz.InsertCommittente(committente);
    //            if (committente.IdCommittente > 0)
    //            {
    //                res = true;
    //                // Ritorno alla pagina di insertmodificacantieri
    //            }
    //        }

    //        // Gestisco il risultato dell'operazione
    //        if (res)
    //        {
    //            // Tutto ok
    //            if (aggiornamento)
    //                LabelRisultato.Text = "Aggiornamento effettuato correttamente";
    //            else
    //                LabelRisultato.Text = "Inserimento effettuato correttamente";
    //        }
    //        else
    //        {
    //            LabelRisultato.Text = "Si � verificato un errore durante l'operazione";
    //        }
    //    }
    //}

    private Committente CreaCommittente()
    {
        // Creazione oggetto Committente
        int? idCommittente = null;
        string ragioneSociale = null;
        string partitaIva = null;
        string codiceFiscale = null;
        string telefono = null;
        string fax = null;
        string personaRiferimento = null;
        Indirizzo indirizzo = null;

        if (ViewState["IdCommittente"] != null)
        {
            idCommittente = (int?) ViewState["IdCommittente"];
        }

        ragioneSociale = TextBoxRagioneSociale.Text.Trim().ToUpper();

        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()))
            partitaIva = TextBoxPartitaIva.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text.Trim()))
            codiceFiscale = TextBoxCodiceFiscale.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxTelefono.Text.Trim()))
            telefono = TextBoxTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxFax.Text.Trim()))
            fax = TextBoxFax.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxPersonaRiferimento.Text.Trim()))
            personaRiferimento = TextBoxPersonaRiferimento.Text.Trim().ToUpper();

        indirizzo = CptSelezioneIndirizzo1.Indirizzo();

        Committente committente = new Committente(idCommittente, ragioneSociale, partitaIva, codiceFiscale,
                                                  (indirizzo.Indirizzo1 + " " + indirizzo.Civico).Trim(),
                                                  indirizzo.Comune, indirizzo.Provincia, indirizzo.Cap, telefono, fax,
                                                  personaRiferimento);

        if (ViewState["IdCommittente"] != null)
        {
            committente.Modificato = true;
        }

        return committente;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void SetValidationGroup(string validationGroup)
    {
        RegularExpressionValidator1.ValidationGroup = validationGroup;
        RegularExpressionValidator2.ValidationGroup = validationGroup;
        RequiredFieldValidator1.ValidationGroup = validationGroup;
        //this.RequiredFieldValidatorIndirizzo.ValidationGroup = validationGroup;
    }
}