﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaLavoratore.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaLavoratore" %>
<%@ Register Src="AttestatoRegolaritaRicercaLavoratore.ascx" TagName="attestatoregolaritaricercalavoratore"
    TagPrefix="uc3" %>
<%@ Register Src="AttestatoRegolaritaRicercaImpresa.ascx" TagName="attestatoregolaritaricercaimpresa"
    TagPrefix="uc5" %>
<%@ Register Src="AttestatoRegolaritaImpreseSubappalti.ascx" TagName="attestatoregolaritaimpresesubappalti"
    TagPrefix="uc6" %>
<%@ Register Src="AttestatoRegolaritaLavoratoreInserimento.ascx" TagName="attestatoregolaritalavoratoreinserimento"
    TagPrefix="uc7" %>
<table class="standardTable">
    <tr>
        <td>
            <br />
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco lavoratori"></asp:Label>
            <br />
            <br />
            <asp:Label ID="LabelImprese" runat="server" Text="Imprese"></asp:Label>
            <asp:DropDownList ID="DropDownListImpresa" runat="server" Width="300px" AutoPostBack="True"
                OnSelectedIndexChanged="DropDownListImpresa_SelectedIndexChanged1">
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label ID="LabelImpresaSelezionata" runat="server" Text="Nessuna impresa selezionata"></asp:Label>
            <br />
            <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                OnRowDataBound="GridViewLavoratori_RowDataBound" Width="100%" OnRowDeleting="GridViewLavoratori_RowDeleting"
                DataKeyNames="IdDomandaLavoratore">
                <Columns>
                    <asp:TemplateField HeaderText="Lavoratore/i">
                        <ItemTemplate>
                            <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False" />
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText=""
                        ShowDeleteButton="True" DeleteImageUrl="~/images/pallinoX.png" HeaderText="Cancellazione">
                        <ItemStyle Width="10px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun lavoratore presente
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <br />
            <asp:Button ID="ButtonaggiungiLavoratore" runat="server" Text="Aggiungi lavoratore"
                Width="225px" OnClick="ButtonaggiungiLavoratore_Click" CausesValidation="False"
                Enabled="False" />
            <asp:Label ID="LabelGiaPresente" runat="server" ForeColor="Red" Text="Lavoratore già presente"
                Visible="False"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <br />
            <asp:Panel ID="PanelAggiungiLavoratore" runat="server" Width="100%" Visible="False">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelAggiungiLav" runat="server" Font-Bold="True" ForeColor="White"
                                Text="Inserimento lavoratore"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            <br />
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelRicercaInserisciLavoratore" runat="server" Visible="False" Width="100%">
                                            <uc3:attestatoregolaritaricercalavoratore ID="AttestatoRegolaritaRicercaLavoratore1"
                                                runat="server" Visible="false" />
                                            <asp:Panel ID="PanelInserisciLavoratore" runat="server" Visible="False" Width="100%">
                                                <table class="filledtable">
                                                    <tr>
                                                        <td>
                                                            Inserimento nuovo lavoratore
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table class="borderedTable">
                                                    <tr>
                                                        <td>
                                                            <uc7:attestatoregolaritalavoratoreinserimento ID="AttestatoRegolaritaLavoratore1"
                                                                runat="server" />
                                                            <asp:Button ID="ButtonInserisciNuovoLavoratore" runat="server" OnClick="ButtonInserisciNuovoLavoratore_Click"
                                                                Text="Inserisci Lavoratore" Width="170px" ValidationGroup="lavoratore" />
                                                            <asp:Button ID="ButtonAnnullaInserimentoLavoratore" runat="server" CausesValidation="False"
                                                                OnClick="ButtonAnnullaInserimentoLavoratore_Click" Text="Annulla" Width="170px" />
                                                            <br />
                                                            <asp:Label ID="LabelInserimentoNuovoLavoratoreRes" runat="server" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="LabelResInserimentoLavInLista" runat="server" ForeColor="Red"></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
