using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.TuteScarpe.Business;

public partial class WebControls_CantieriImpresa : UserControl
{
    private string errore;

    /// <summary>
    /// ProprietÓ che costruisce il committente. Se sono presenti errori ritorna null
    /// </summary>
    public Impresa impresa
    {
        get
        {
            if (ControlloCampiServer())
            {
                return CreaImpresa();
            }
            else return null;
        }
    }

    public string Errore
    {
        get { return errore; }
        set { errore = value; }
    }

    public void ImpostaRagioneSociale(string ragioneSociale)
    {
        TextBoxRagioneSociale.Text = ragioneSociale;
    }

    public void ImpostaImpresa(Impresa impresa)
    {
        ViewState["IdImpresa"] = impresa.IdImpresa;
        ViewState["FonteNotifica"] = impresa.FonteNotifica;
        TextBoxRagioneSociale.Text = impresa.RagioneSociale;
        TextBoxPartitaIva.Text = impresa.PartitaIva;
        TextBoxCodiceFiscale.Text = impresa.CodiceFiscale;
        TextBoxTelefono.Text = impresa.Telefono;
        TextBoxFax.Text = impresa.Fax;
        TextBoxPersonaRiferimento.Text = impresa.PersonaRiferimento;
        TextBoxIndirizzo.Text = impresa.Indirizzo;
        CheckBoxArtigiano.Checked = impresa.LavoratoreAutonomo;

        if (DropDownListTipoAttivita.Items.FindByText(impresa.TipoAttivita) != null)
        {
            DropDownListTipoAttivita.SelectedValue = impresa.TipoAttivita;

            TextBoxTipoAttivita.Text = string.Empty;
            TextBoxTipoAttivita.Visible = false;
        }
        else
        {
            if (!string.IsNullOrEmpty(impresa.TipoAttivita))
            {
                TextBoxTipoAttivita.Visible = true;
                TextBoxTipoAttivita.Text = impresa.TipoAttivita;
                DropDownListTipoAttivita.Text = "Sconosciuta";
            }
            else
            {
                TextBoxTipoAttivita.Text = string.Empty;
                TextBoxTipoAttivita.Visible = false;
            }
        }

        LabelProvincia.Text = string.Empty;
        LabelComune.Text = string.Empty;
        LabelCap.Text = string.Empty;

        if (impresa.FonteNotifica)
        {
            // Provincia
            if (impresa.Provincia != null)
            {
                ListItem prov = DropDownListProvincia.Items.FindByText(impresa.Provincia.ToUpper());
                if (prov != null)
                {
                    DropDownListProvincia.SelectedValue = prov.Value;
                    CaricaComuni(Int32.Parse(prov.Value));
                }
                else
                    LabelProvincia.Text = impresa.Provincia;
            }

            // Comune
            if (impresa.Comune != null)
            {
                ListItem com = DropDownListComuni.Items.FindByText(impresa.Comune.ToUpper());
                if (com != null)
                {
                    DropDownListComuni.SelectedValue = com.Value;
                    CaricaCAP(Int64.Parse(com.Value));
                }
                else
                    LabelComune.Text = impresa.Comune;
            }

            // Cap
            ListItem cap = DropDownListCAP.Items.FindByText(impresa.Cap);
            if (cap != null)
            {
                DropDownListCAP.SelectedValue = cap.Value;
            }
            else
                LabelCap.Text = impresa.Cap;
        }
        else
        {
            // Provincia            
            ListItem prov = DropDownListProvincia.Items.FindByText(impresa.Provincia);
            if (prov != null)
            {
                DropDownListProvincia.SelectedValue = prov.Value;
                CaricaComuni(Int32.Parse(prov.Value));
            }

            // Comune
            ListItem com = DropDownListComuni.Items.FindByText(impresa.Comune);
            if (com != null)
            {
                DropDownListComuni.SelectedValue = com.Value;
                CaricaCAP(Int64.Parse(com.Value));
            }

            // Cap
            ListItem cap = DropDownListCAP.Items.FindByText(impresa.Cap);
            if (cap != null)
            {
                DropDownListCAP.SelectedValue = cap.Value;
            }
        }
    }

    public void Reset()
    {
        TextBoxIdImpresa.Text = string.Empty;
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxCodiceFiscale.Text = string.Empty;
        TextBoxPartitaIva.Text = string.Empty;
        TextBoxRagioneSociale.Text = string.Empty;
        TextBoxTelefono.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxPersonaRiferimento.Text = string.Empty;
        CheckBoxArtigiano.Checked = false;

        CaricaProvince();
        CaricaComuni(-1);
        CaricaCAP(-1);
    }

    /// <summary>
    /// Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        // CAMPI CONTROLLATI
        // Ragione sociale

        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            res = false;
            errori.Append("Campo ragione sociale vuoto" + Environment.NewLine);
        }

        if (string.IsNullOrEmpty(TextBoxIndirizzo.Text))
        {
            res = false;
            errori.Append("Campo indirizzo vuoto" + Environment.NewLine);
        }

        if (DropDownListCAP.SelectedValue == null || DropDownListCAP.SelectedValue == string.Empty)
        {
            res = false;
            errori.Append("CAP non selezionato" + Environment.NewLine);
        }

        if (!res)
            errore = errori.ToString();

        return res;
    }

    private Impresa CreaImpresa()
    {
        // Creazione oggetto impresa
        string provincia = null;
        string comune = null;
        string cap = null;
        string indirizzo = null;
        string ragioneSociale = null;
        string partitaIva = null;
        string codiceFiscale = null;
        string telefono = null;
        string fax = null;
        string personaRiferimento = null;
        string tipoAttivita = null;
        int? idImpresa = null;
        bool fonteNotifica = false;

        if (ViewState["IdImpresa"] != null)
        {
            idImpresa = (int?) ViewState["IdImpresa"];
        }

        if (ViewState["FonteNotifica"] != null)
        {
            fonteNotifica = (bool) ViewState["FonteNotifica"];
        }

        ragioneSociale = TextBoxRagioneSociale.Text.Trim().ToUpper();

        if (!string.IsNullOrEmpty(TextBoxIndirizzo.Text.Trim()))
            indirizzo = TextBoxIndirizzo.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()))
            partitaIva = TextBoxPartitaIva.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text.Trim()))
            codiceFiscale = TextBoxCodiceFiscale.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxTelefono.Text.Trim()))
            telefono = TextBoxTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxFax.Text.Trim()))
            fax = TextBoxFax.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxPersonaRiferimento.Text.Trim()))
            personaRiferimento = TextBoxPersonaRiferimento.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(DropDownListTipoAttivita.SelectedValue))
            tipoAttivita = DropDownListTipoAttivita.SelectedValue;

        provincia = DropDownListProvincia.SelectedItem.Text;
        comune = DropDownListComuni.SelectedItem.Text;
        cap = DropDownListCAP.SelectedValue;

        Impresa impresa = new Impresa(TipologiaImpresa.Cantieri, idImpresa, ragioneSociale, indirizzo,
                                      provincia, comune, cap, partitaIva, codiceFiscale, tipoAttivita, telefono, fax,
                                      personaRiferimento, false);

        impresa.FonteNotifica = fonteNotifica;
        impresa.LavoratoreAutonomo = CheckBoxArtigiano.Checked;

        return impresa;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaProvince();
            CaricaTipologieAttivita();
        }
    }

    public void SetValidationGroup(string validationGroup)
    {
        RequiredFieldValidator1.ValidationGroup = validationGroup;
        RequiredFieldValidatorIndirizzo.ValidationGroup = validationGroup;
    }

    private void CaricaTipologieAttivita()
    {
        CptBusiness cptBiz = new CptBusiness();

        DropDownListTipoAttivita.DataSource = cptBiz.GetTipologieAttivita();
        DropDownListTipoAttivita.Text = "Sconosciuta";
        DropDownListTipoAttivita.DataBind();
    }

    #region impresa properties

    public int IdImpresa { get; set; }


    public string RagioneSociale { get; set; }

    public string PartitaIVA { get; set; }

    public string CodiceFiscale { get; set; }

    public string Indirizzo { get; set; }

    public string Provincia { get; set; }

    public string Comune { get; set; }

    public string CAP { get; set; }

    #endregion

    #region Biz properties

    private CantieriBusiness cantieriBiz;
    public TSBusiness TsBiz { get; set; }

    public CantieriBusiness CantieriBiz
    {
        get { return cantieriBiz; }
        set { cantieriBiz = value; }
    }

    #endregion

    #region Metodi per la gestione delle province, comuni, cap

    private void CaricaProvince()
    {
        cantieriBiz.CaricaProvinceInDropDown(DropDownListProvincia);
    }

    private void CaricaComuni(int idProvincia)
    {
        cantieriBiz.CaricaComuniInDropDown(DropDownListComuni, idProvincia);
    }

    private void CaricaCAP(Int64 idComune)
    {
        cantieriBiz.CaricaCapInDropDown(DropDownListCAP, idComune);
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        cantieriBiz.CambioSelezioneDropDownProvincia(DropDownListProvincia, DropDownListComuni, DropDownListCAP);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        cantieriBiz.CambioSelezioneDropDownComune(DropDownListComuni, DropDownListCAP);
    }

    #endregion
}