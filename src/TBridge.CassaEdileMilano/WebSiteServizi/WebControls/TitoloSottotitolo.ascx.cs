using System;
using System.Web.UI;

public partial class WebControls_TitoloSottotitolo : UserControl
{
    private string _sottoTitolo = string.Empty;
    private string _titolo = string.Empty;

    public string titolo
    {
        get { return _titolo; }
        set { _titolo = value; }
    }

    public string sottoTitolo
    {
        get { return _sottoTitolo; }
        set { _sottoTitolo = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}