<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificheImpresaSelezionata.ascx.cs" Inherits="WebControls_NotificheImpresaSelezionata" %>
<asp:Label ID="LabelImpresa" runat="server" Text="Impresa:"></asp:Label>&nbsp; &nbsp;<asp:DropDownList
    ID="DropDownListImprese" runat="server" AppendDataBoundItems="True"
    Width="284px">
</asp:DropDownList>
<asp:Button ID="ButtonSeleziona" runat="server" OnClick="ButtonSeleziona_Click" Text="Seleziona" /><br />
<br />
Impresa selezionata:&nbsp;
<asp:Label ID="LabelImpresaSelezionata" runat="server" Font-Bold="True"></asp:Label>
