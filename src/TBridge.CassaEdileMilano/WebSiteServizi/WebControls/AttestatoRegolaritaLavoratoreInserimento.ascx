﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaLavoratoreInserimento.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaLavoratoreInserimento" %>
<asp:Panel ID="PanelDatiAggiornamento" runat="server" Width="220px"
    Visible="False">
    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
    <asp:TextBox ID="TextBoxIdLavoratore" runat="server" Enabled="False" ReadOnly="True"
        Width="100px"></asp:TextBox></asp:Panel>
<table class="standardTable">
    <tr>
        <td>
            Cognome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxCognome"
                ErrorMessage="Digitare un cognome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="11" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNome"
                ErrorMessage="Digitare un nome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita (gg/mm/aaaa)*
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Digitare una data di nascita" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Formato data errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>
            <asp:CompareValidator ID="CompareValidatorDataNascitaDataAssunzione" runat="server"
                ControlToCompare="TextBoxDataAssunzione" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="La data di nascita non può essere successiva a quella di assunzione"
                Operator="LessThan" Type="Date" ValidationGroup="lavoratore">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Digitare un codice fiscale" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Formato codice fiscale errato" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$"
                ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non coerente con la data di nascita" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate"
                ValidationGroup="lavoratore">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Tipo assunzione
        </td>
        <td>
            Part Time:<asp:RadioButton ID="RadioButtonPartTimeSI" runat="server" AutoPostBack="True"
                GroupName="tipoAssunzione" Text="SI" />
            <asp:RadioButton ID="RadioButtonPartTimeNO" runat="server" AutoPostBack="True" Checked="True"
                GroupName="tipoAssunzione" Text="NO" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Data assunzione (gg/mm/aaaa)
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataAssunzione" runat="server" MaxLength="10" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidatorDataAssunzione" runat="server" Operator="DataTypeCheck"
                Type="Date" ControlToValidate="TextBoxDataAssunzione" ErrorMessage="Formato data assunzione non valido"
                ValidationGroup="lavoratore">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidatorDataAssunzioneCessazione" runat="server"
                ControlToCompare="TextBoxDataCessazione" ControlToValidate="TextBoxDataAssunzione"
                ErrorMessage="La data di assunzione non può essere successiva a quella di cessazione"
                Operator="LessThan" Type="Date" ValidationGroup="lavoratore">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data cessazione (gg/mm/aaaa)
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataCessazione" runat="server" MaxLength="10" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidatorDataCessazione" runat="server" Operator="DataTypeCheck"
                Type="Date" ControlToValidate="TextBoxDataCessazione" ErrorMessage="Formato data cessazione non valido"
                ValidationGroup="lavoratore">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                ValidationGroup="lavoratore" />
        </td>
    </tr>
</table>
* campi obbligatori 