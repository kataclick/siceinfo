using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Colonie.Type.Delegates;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class WebControls_ColonieGestioneLavoratore : UserControl
{
    
    public LavoratoreACE Lavoratore
    {
        get { return ColonieVisualizzaLavoratoreCE1.Lavoratore; }
    }

    public event LavoratoreACEConfirmedEventHandler OnLavoratoreACEConfirmed;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            Inizializzazione();

        ColonieRicercaLavoratoreACE1.OnLavoratoreACESelected +=
            new LavoratoreACESelectedEventHandler(ColonieRicercaLavoratoreACE1_OnLavoratoreACESelected);
        ColonieModificaLavoratore1.OnCancelled += new CancelledEventHandler(ColonieModificaLavoratore1_OnCancelled);
        ColonieModificaLavoratore1.OnLavoratoreACEModified +=
            new LavoratoreACEModifiedEventHandler(ColonieModificaLavoratore1_OnLavoratoreACEModified);
    }

    private void ColonieRicercaLavoratoreACE1_OnLavoratoreACESelected(LavoratoreACE lavoratore)
    {
        Inizializzazione();

        ViewState["IdLavoratore"] = lavoratore.IdLavoratore.Value;

        ColonieVisualizzaLavoratoreCE1.Lavoratore = lavoratore;
        ColonieVisualizzaLavoratoreCE1.Visible = true;
        ButtonConferma.Visible = true;
        ButtonModifica.Visible = true;

        //Se il lavoratore � legato ad altre domande non permettiamo la modifica dei dati
        if (lavoratore.Modificabile)
            ButtonModifica.Enabled = true;
        else
            ButtonModifica.Enabled = false;
    }

    private void Inizializzazione()
    {
        ViewState["IdLavoratore"] = null;

        ColonieRicercaLavoratoreACE1.Visible = true;

        ColonieModificaLavoratore1.Visible = false;
        ColonieVisualizzaLavoratoreCE1.Visible = false;

        ColonieModificaLavoratore1.Lavoratore = null;
        ColonieVisualizzaLavoratoreCE1.Lavoratore = null;

        ColonieVisualizzaLavoratoreCE1.AttivaVisualizzazioneParziale = false;

        ButtonConferma.Visible = false;
        ButtonModifica.Visible = false;
    }

    private void ConfermaSelezioneLavoratore()
    {
        bool azione = true;
        GestioneControlliLavoratore(azione);
    }

    /// <summary>
    /// se true conferma, se false reset
    /// </summary>
    /// <param name="azione"></param>
    private void GestioneControlliLavoratore(bool azione)
    {
        ColonieRicercaLavoratoreACE1.Visible = !azione;
        ColonieVisualizzaLavoratoreCE1.Visible = azione;

        ButtonModifica.Visible = false; // azione;
        ButtonConferma.Visible = false; // azione;

        ColonieVisualizzaLavoratoreCE1.Visible = azione;
        ColonieVisualizzaLavoratoreCE1.AttivaVisualizzazioneParziale = azione;

        ColonieModificaLavoratore1.Visible = false;
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        ConfermaSelezioneLavoratore();

        if (OnLavoratoreACEConfirmed != null)
            OnLavoratoreACEConfirmed(ColonieVisualizzaLavoratoreCE1.Lavoratore);
    }

    #region Eventi componente COLONIEMODIFICALAVORATORE

    private void ColonieModificaLavoratore1_OnCancelled()
    {
        //nascondiamo la gestione la modifica
        GestioneModificaLavoratore(false);
        ColonieModificaLavoratore1.Lavoratore = null;
    }

    private void ColonieModificaLavoratore1_OnLavoratoreACEModified(LavoratoreACE lavoratore)
    {
        //otteniamo il lavoratore modificato e lo assegniamo al componente di visualizzazione
        ColonieVisualizzaLavoratoreCE1.Lavoratore = ColonieModificaLavoratore1.Lavoratore;
        //nascondiamo la gstione della modifica
        GestioneModificaLavoratore(false);
    }


    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        //abilitiamo la modifica
        GestioneModificaLavoratore(true);
        //passiamo al controllo di modifica i dati
        ColonieModificaLavoratore1.Lavoratore = ColonieVisualizzaLavoratoreCE1.Lavoratore;
    }

    /// <summary>
    /// Se true attiva la modifica e abilita pannelli e controlli di conseguenza.
    /// Se false esattametne il contrario
    /// </summary>
    /// <param name="attiva"></param>
    private void GestioneModificaLavoratore(bool attiva)
    {
        ButtonModifica.Visible = !attiva;
        ButtonConferma.Visible = !attiva;
        ColonieVisualizzaLavoratoreCE1.Visible = !attiva;

        ColonieModificaLavoratore1.Visible = attiva;
    }

    #endregion


    
}