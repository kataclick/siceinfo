using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;

public partial class WebControls_AccessoCantieriImprese : UserControl
{
    private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();

    private Int32 idUtente;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region impostiamo eventi e Biz dei controlli

        //Registriamo gli eventi del controllo
        AccessoCantieriRicercaImpresaAppaltataDa.OnImpresaSelected +=
            AccessoCantieriRicercaImpresa1_OnImpresaSelected;
        AccessoCantieriRicercaImpresaAppaltataDa.OnNuovaImpresaSelected +=
            AccessoCantieriRicercaImpresaAppaltataDa_OnNuovaImpresaSelected;
        AccessoCantieriRicercaImpresaSubappaltatrice.OnImpresaSelected +=
            AccessoCantieriRicercaImpresa2_OnImpresaSelected;
        AccessoCantieriRicercaImpresaSubappaltatrice.OnNuovaImpresaSelected +=
            AccessoCantieriRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected;

        //Forniamo al controllo le logiche di gestione 
        AccessoCantieriImpreseSubappaltiAppaltataDa.AccessoCantieriBiz = biz;

        //Forniamo al controllo le logiche di gestione
        AccessoCantieriImpreseSubappaltiSubappaltatrice.AccessoCantieriBiz = biz;

        #endregion

        #region Settiamo i validation group

        //Settiamo il validation group cross-control
        const string validationGroupImpresaAppaltataDa = "validationGroupImpresaAppaltataDa";
        ButtonInserisciNuovaImpresaSubappaltata.ValidationGroup = validationGroupImpresaAppaltataDa;
        AccessoCantieriImpreseSubappaltiAppaltataDa.SetValidationGroup(validationGroupImpresaAppaltataDa);

        const string validationGroupImpresaSubappaltatrice = "validationGroupImpresaSubappaltatrice";
        ButtonInserisciNuovaImpresaAppaltante.ValidationGroup = validationGroupImpresaSubappaltatrice;
        AccessoCantieriImpreseSubappaltiSubappaltatrice.SetValidationGroup(validationGroupImpresaSubappaltatrice);

        #endregion

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        
        //validationGroupImpresaAppaltataDa

        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresaAppaltataDa') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovaImpresaSubappaltata, null));
        sb.Append(";");
        sb.Append("return true;");
        ButtonInserisciNuovaImpresaSubappaltata.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            ButtonInserisciNuovaImpresaSubappaltata);

        ////resettiamo
        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresaSubappaltatrice') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovaImpresaAppaltante, null));
        sb.Append(";");
        sb.Append("return true;");
        ButtonInserisciNuovaImpresaAppaltante.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            ButtonInserisciNuovaImpresaAppaltante);

        #endregion

        idUtente = GestioneUtentiBiz.GetIdUtente();

        if (!Page.IsPostBack)
        {
            if (ViewState["subappalti"] == null)
            {
                ViewState["subappalti"] = new SubappaltoCollection();
            }

            CaricaSubappalti();
        }
    }

    private void CaricaSubappalti()
    {
        if (ViewState["subappalti"] != null)
        {
            GridViewSubappalti.DataSource = ViewState["subappalti"];
            GridViewSubappalti.DataBind();

            #region Carica dropdown list imprese

            ImpresaCollection imprese = new ImpresaCollection();

            if (ViewState["impresaDetentriceAppalto"] != null)
            {
                Impresa impresaDetentriceAppalto = (Impresa) ViewState["impresaDetentriceAppalto"];
                imprese.Add(impresaDetentriceAppalto);
            }

            if (ViewState["subappalti"] != null)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                foreach (Subappalto sub in subappalti)
                {
                    if (!imprese.Contains(sub.Appaltata))
                        imprese.Add(sub.Appaltata);
                }
            }

            ImpresaCollection impreseTot = new ImpresaCollection();

            if (ViewState["impresaDetentriceAppalto"] != null)
            {
                Impresa impresaDetentriceAppalto = (Impresa) ViewState["impresaDetentriceAppalto"];
                impreseTot.Add(impresaDetentriceAppalto);
            }

            if (ViewState["subappalti"] != null)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                foreach (Subappalto sub in subappalti)
                {
                    if (!impreseTot.Contains(sub.Appaltata))
                        impreseTot.Add(sub.Appaltata);

                    if (sub.Appaltante != null)
                    {
                        if (!impreseTot.Contains(sub.Appaltante))
                            impreseTot.Add(sub.Appaltante);
                    }
                }
            }

            ViewState["impresaCollection"] = impreseTot;

            //

            DropDownListImpresaSubappaltata.DataSource = impreseTot; //imprese;

            DropDownListImpresaSubappaltata.DataTextField = "RagioneSociale";
            DropDownListImpresaSubappaltata.DataValueField = "IdImpresaComposto";

            DropDownListImpresaSubappaltata.DataBind();

            //

            DropDownListImpresaAppaltante.DataSource = impreseTot; //imprese;

            DropDownListImpresaAppaltante.DataTextField = "RagioneSociale";
            DropDownListImpresaAppaltante.DataValueField = "IdImpresaComposto";

            DropDownListImpresaAppaltante.DataBind();

            if (impreseTot.Count == 0)
            {
                PanelImpresaAppaltanteDropDown.Visible = false;

                PanelRicercaNuovaImpresaAppaltante.Visible = true;
                AccessoCantieriRicercaImpresaSubappaltatrice.Visible = true;
                PanelInserisciImpresaAppaltante.Visible = false;

                //ButtonAnnulla.Enabled = false;
                ButtonAggiungiAppalto.Enabled = false;

                PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
                PanelInserisciImpresaSubappaltata.Visible = false;
            }
            else
            {
                PanelImpresaAppaltanteDropDown.Visible = true;

                PanelRicercaNuovaImpresaAppaltante.Visible = false;
                AccessoCantieriRicercaImpresaSubappaltatrice.Visible = false;
                PanelInserisciImpresaAppaltante.Visible = false;

                //ButtonAnnulla.Enabled = true;
                ButtonAggiungiAppalto.Enabled = true;

                PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
                PanelInserisciImpresaSubappaltata.Visible = false;
            }

            #endregion
        }
    }

    public void CaricaImprese(SubappaltoCollection subappalti)
    {
        ViewState["subappalti"] = subappalti;
    }

    public void CaricaDomanda(Int32? idDomanda)
    {
        ViewState["idDomanda"] = idDomanda;
    }

    public ImpresaCollection GetImprese()
    {
        return (ImpresaCollection) ViewState["impresaCollection"];
    }

    public SubappaltoCollection GetSubappalti()
    {
        return (SubappaltoCollection) ViewState["subappalti"];
    }

    protected void ButtonAltro_Click(object sender, EventArgs e)
    {
        AccessoCantieriRicercaImpresaAppaltataDa.Reset();
        
        PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
        AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
        PanelInserisciImpresaSubappaltata.Visible = false;
        
        //ButtonAnnulla.Enabled = false;
        ButtonAggiungiAppalto.Enabled = false;
    }

    protected void ButtonAltroImpresaAppaltata_Click(object sender, EventArgs e)
    {
        AccessoCantieriRicercaImpresaSubappaltatrice.Reset();

        PanelRicercaNuovaImpresaAppaltante.Visible = true;
        AccessoCantieriRicercaImpresaSubappaltatrice.Visible = true;
        PanelInserisciImpresaAppaltante.Visible = false;

        ////ButtonAnnulla.Enabled = false;
        ButtonAggiungiAppalto.Enabled = false;
    }

    protected void ButtonSelezionaImpresaAppaltataDaDropDownList_Click(object sender, EventArgs e)
    {
        if (DropDownListImpresaAppaltante.SelectedItem != null)
        {
            Impresa impresa = new Impresa();

            string[] impresaIds = DropDownListImpresaAppaltante.SelectedItem.Value.Split('|');
            impresa.TipoImpresa = (TipologiaImpresa) Int32.Parse(impresaIds[0]);
            if (!String.IsNullOrEmpty(impresaIds[2]))
            {
                impresa.IdTemporaneo = new Guid(impresaIds[2]);
            }
            if (!String.IsNullOrEmpty(impresaIds[1]))
            {
                impresa.IdImpresa = Int32.Parse(impresaIds[1]);
            }

            Impresa impresaSel;

            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                ImpresaCollection imprese = (ImpresaCollection) ViewState["impresaCollection"];
                impresaSel = imprese.Select(impresa);
            }
            else
            {
                ImpresaCollection imprese = (ImpresaCollection) ViewState["impresaCollection"];
                impresaSel = imprese.Select(impresa);
            }

            ImpresaSubappaltatriceSelezionata(impresaSel);
        }
    }

    protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Subappalto subappalto = (Subappalto) e.Row.DataItem;
            Label lSubappaltatriceRagioneSociale = (Label) e.Row.FindControl("LabelSubappaltatriceRagioneSociale");
            Label lSubappaltatricePIva = (Label) e.Row.FindControl("LabelSubappaltatricePIva");
            Label lSubappaltatriceIndirizzo = (Label) e.Row.FindControl("LabelSubappaltatriceIndirizzo");
            Label lSubappaltatriceArtigiano = (Label) e.Row.FindControl("LabelSubappaltatriceArtigiano");

            Label lSubappaltataRagioneSociale = (Label) e.Row.FindControl("LabelSubappaltataRagioneSociale");
            Label lSubappaltataPIva = (Label) e.Row.FindControl("LabelSubappaltataPIva");
            Label lSubappaltataIndirizzo = (Label) e.Row.FindControl("LabelSubappaltataIndirizzo");
            Label lSubappaltataArtigiano = (Label) e.Row.FindControl("LabelSubappaltataArtigiano");

            if (subappalto.Appaltata != null)
            {
                lSubappaltatriceRagioneSociale.Text = subappalto.NomeAppaltata;
                lSubappaltatricePIva.Text = String.Format("P.IVA: {0}", subappalto.Appaltata.PartitaIva);
                lSubappaltatriceIndirizzo.Text = subappalto.Appaltata.IndirizzoCompleto;
                try
                {
                    if (subappalto.Appaltata.LavoratoreAutonomo)
                        lSubappaltatriceArtigiano.Text = "Artigiano autonomo senza dipendenti";
                    else
                        lSubappaltatriceArtigiano.Text = string.Empty;
                }
                catch
                {
                    lSubappaltatriceArtigiano.Text = string.Empty;
                }
            }

            if (subappalto.Appaltante != null)
            {
                lSubappaltataRagioneSociale.Text = subappalto.NomeAppaltatrice;
                lSubappaltataPIva.Text = String.Format("P.IVA: {0}", subappalto.Appaltante.PartitaIva);
                lSubappaltataIndirizzo.Text = subappalto.Appaltante.IndirizzoCompleto;
                try
                {
                    if (subappalto.Appaltante.LavoratoreAutonomo)
                        lSubappaltataArtigiano.Text = "Artigiano autonomo senza dipendenti";
                    else
                        lSubappaltataArtigiano.Text = string.Empty;
                }
                catch
                {
                    lSubappaltataArtigiano.Text = string.Empty;
                }
            }

            ImageButton ibElimina = (ImageButton) e.Row.FindControl("ImageButtonDelete");

            Int32? idDomanda;
            try
            {
                idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
            }
            catch
            {
                idDomanda = null;
            }

            if (idDomanda.HasValue)
            {
                Int32 timbAppaltante = 0;
                if (subappalto.Appaltata != null)
                {
                    Int32 timbAppaltata = biz.GetCountTimbrature(subappalto.Appaltata.PartitaIva, null, idDomanda.Value);

                    if (subappalto.Appaltante != null)
                    {
                        if (subappalto.Appaltante.IdImpresa.HasValue)
                        {
                            timbAppaltante = biz.GetCountTimbrature(subappalto.Appaltante.PartitaIva, null,
                                                                    idDomanda.Value);
                        }
                    }

                    ibElimina.Visible = false;

                    if (subappalto.Appaltante != null)
                    {
                        if (subappalto.Appaltante.IdImpresa.HasValue)
                        {
                            ibElimina.Visible = (timbAppaltata == 0 && timbAppaltante == 0 &&
                                                 !subappalto.Appaltante.LavoratoreAutonomo);
                        }

                        if (subappalto.Appaltante.LavoratoreAutonomo)
                        {
                            TimbraturaFilter filter = new TimbraturaFilter
                                                          {
                                                              CodiceFiscale = subappalto.Appaltante.CodiceFiscale,
                                                              IdAccessoCantieriWhiteList = idDomanda.Value
                                                          };

                            TimbraturaCollection timbAutonomo = biz.GetTimbratureByFilter(filter);

                            if (timbAutonomo.Count == 0 && timbAppaltata == 0)
                            {
                                ibElimina.Visible = true;
                            }
                            else
                            {
                                ibElimina.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        ibElimina.Visible = timbAppaltata == 0;
                    }
                }
            }
            else
            {
                ibElimina.Visible = true;
            }
        }
    }

    protected void CustomValidatorSubappalti_ServerValidate(object source, ServerValidateEventArgs args)
    {
        SubappaltoCollection sub = (SubappaltoCollection) ViewState["subappalti"];
        Impresa appaltatrice = (Impresa) ViewState["impresaAppaltatrice"];
        Impresa appaltata = (Impresa) ViewState["impresaAppaltata"];

        if (appaltatrice != null && appaltata != null)
        {
            if (((appaltatrice.IdImpresa != null) && (appaltata.IdImpresa != null) &&
                 (appaltatrice.IdImpresa == appaltata.IdImpresa)) ||
                ((appaltatrice.IdImpresa == null) && (appaltata.IdImpresa == null) &&
                 (appaltata.PartitaIva == appaltatrice.PartitaIva)) || (sub.Contains(appaltatrice, appaltata)) ||
                sub.Contains(appaltata, appaltatrice))
            {
                AccessoCantieriRicercaImpresaAppaltataDa.Reset();
                AccessoCantieriRicercaImpresaSubappaltatrice.Reset();

                args.IsValid = false;
            }

            else
                args.IsValid = true;
        }

        if (appaltatrice == null && appaltata != null)
        {
            if (sub.ContainsAppaltata(appaltata))
            {
                AccessoCantieriRicercaImpresaAppaltataDa.Reset();
                AccessoCantieriRicercaImpresaSubappaltatrice.Reset();
                args.IsValid = false;
            }

            else
            {
                if (appaltata.LavoratoreAutonomo)
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }
        }
    }

    protected void CustomValidatorAlmenoUnImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        ImpresaCollection imprese = GetImprese();

        if (imprese.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorImpresaRichiedentePresente_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        ImpresaCollection imprese = GetImprese();

        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa imp =
                (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            Impresa impAtt = new Impresa {IdImpresa = imp.IdImpresa, TipoImpresa = TipologiaImpresa.SiceNew};

            if (!imprese.Contains(impAtt))
            {
                args.IsValid = false;
            }
        }
    }

    public void CaricaDataInizio(DateTime? dataInizio)
    {
        ViewState["dataInizio"] = dataInizio;
    }

    public void CaricaDataFine(DateTime? dataFine)
    {
        ViewState["dataFine"] = dataFine;
    }

    #region gestione appalti

    protected void GridViewSubappalti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

        subappalti.RemoveAt(e.RowIndex);

        ViewState["subappalti"] = subappalti;

        CaricaSubappalti();
    }

    private bool ControlloCampiServer()
    {
        //Valla: modificare e farlo con un validator custom
        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (ViewState["impresaAppaltata"] == null)
        {
            res = false;
            errori.Append("Impresa appaltante non selezionata" + Environment.NewLine);
        }

        if (!res)
        {
            LabelRisultatoAppalto.Text = errori.ToString();
        }
        else
        {
            Presenter.SvuotaCampo(LabelRisultatoAppalto);
        }

        return res;
    }

    protected void ButtonAggiungiAppalto_Click(object sender, EventArgs e)
    {
        Page.Validate("appalto");

        if (Page.IsValid)
        {
            if (ViewState["subappalti"] != null && ControlloCampiServer())
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                Impresa appaltatrice = (Impresa) ViewState["impresaAppaltatrice"];
                Impresa appaltata = (Impresa) ViewState["impresaAppaltata"];

                //Valla: passare tipologia contratto 0=SiceNEW
                Subappalto subappalto =
                    new Subappalto(null, 0, appaltatrice, appaltata,
                                   0);

                subappalti.Add(subappalto);
                biz.LogSelezioneImpresaSubappalto(idUtente, subappalto);

                ViewState["impresaAppaltatrice"] = null;
                ViewState["impresaAppaltata"] = null;
                TextBoxImpresaAppaltante.Text = string.Empty;
                TextBoxImpresaSubappaltata.Text = string.Empty;
                RadioButtonImpresaAppaltanteNuova.Checked = false;
                RadioButtonImpresaSubappaltataNuova.Checked = false;
                RadioButtonImpresaAppaltanteSiceInfo.Checked = false;
                RadioButtonImpresaSubappaltataSiceInfo.Checked = false;

                CaricaSubappalti();

                PanelAppalti.Visible = false;
                ButtonVisualizzaAppalto.Enabled = true;

                Reset();
            }
        }
    }

    protected void ButtonVisualizzaAppalto_Click(object sender, EventArgs e)
    {
        PanelAppalti.Visible = true;
        ButtonVisualizzaAppalto.Enabled = false;
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        LabelRisultatoAppalto.Text = string.Empty;

        ButtonVisualizzaAppalto.Enabled = true;

        Reset();

        ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

        if (impreseTot.Count == 0)
        {
            PanelImpresaAppaltanteDropDown.Visible = false;

            PanelRicercaNuovaImpresaAppaltante.Visible = true;
            AccessoCantieriRicercaImpresaSubappaltatrice.Visible = true;
            PanelInserisciImpresaAppaltante.Visible = false;

            //ButtonAnnulla.Enabled = false;
            ButtonAggiungiAppalto.Enabled = false;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;
        }
        else
        {
            PanelImpresaAppaltanteDropDown.Visible = true;

            PanelRicercaNuovaImpresaAppaltante.Visible = false;
            AccessoCantieriRicercaImpresaSubappaltatrice.Visible = false;
            PanelInserisciImpresaAppaltante.Visible = false;

            //ButtonAnnulla.Enabled = true;
            ButtonAggiungiAppalto.Enabled = true;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;
        }

        PanelAppalti.Visible = false;
    }

    #endregion

    #region eventi inserimento nuova impresa

    protected void ButtonInserisciNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        Page.Validate("validationGroupImpresaSubappaltatrice");

        if (Page.IsValid)
        {
            Impresa impresa = AccessoCantieriImpreseSubappaltiSubappaltatrice.Impresa;
            ImpresaCollection impColl;

            if (ViewState["impreseSelColl"] == null)
            {
                impColl = new ImpresaCollection();
            }
            else
            {
                impColl = (ImpresaCollection) ViewState["impreseSelColl"];
            }

            if (impresa != null)
            {
                bool[] res = biz.EsisteIvaFiscImpresa(impresa.PartitaIva, impresa.CodiceFiscale);
                ControllaEsistenzaImpresa(LabelImpresaSelezionataEsistente, res);

                if (string.IsNullOrEmpty(LabelImpresaSelezionataEsistente.Text))
                {
                    impColl.Add(impresa);
                    ViewState["impreseSelColl"] = impColl;
                    ImpresaSubappaltatriceSelezionata(impresa);
                    AccessoCantieriImpreseSubappaltiSubappaltatrice.Reset();
                }
                else
                {
                    string pIva;
                    string codfisc;

                    if (res[0])
                    {
                        pIva = impresa.PartitaIva;
                        codfisc = string.Empty;
                        AccessoCantieriRicercaImpresaSubappaltatrice.CaricaImpreseRicerca(codfisc, pIva);
                    }
                    else
                    {
                        if (res[2])
                        {
                            pIva = string.Empty;
                            codfisc = impresa.CodiceFiscale;
                            AccessoCantieriRicercaImpresaSubappaltatrice.CaricaImpreseRicerca(codfisc, pIva);
                        }
                    }

                    ViewState["impresaAppaltatrice"] = null;

                    TextBoxImpresaAppaltante.Text = string.Empty;

                    RadioButtonImpresaAppaltanteNuova.Checked = false;
                    RadioButtonImpresaAppaltanteSiceInfo.Checked = false;

                    ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

                    if (impreseTot.Count == 0)
                    {
                        PanelImpresaAppaltanteDropDown.Visible = false;

                        PanelRicercaNuovaImpresaAppaltante.Visible = true;
                        AccessoCantieriRicercaImpresaSubappaltatrice.Visible = true;
                        PanelInserisciImpresaAppaltante.Visible = false;

                        //ButtonAnnulla.Enabled = false;
                        ButtonAggiungiAppalto.Enabled = false;

                        AccessoCantieriImpreseSubappaltiSubappaltatrice.Reset();
                    }
                    else
                    {
                        PanelImpresaAppaltanteDropDown.Visible = true;

                        PanelRicercaNuovaImpresaAppaltante.Visible = false;
                        AccessoCantieriRicercaImpresaSubappaltatrice.Visible = false;
                        PanelInserisciImpresaAppaltante.Visible = false;

                        //ButtonAnnulla.Enabled = true;
                        ButtonAggiungiAppalto.Enabled = true;

                        AccessoCantieriImpreseSubappaltiSubappaltatrice.Reset();
                    }
                }
            }
            else
                LabelNuovaAppaltanteRes.Text = AccessoCantieriImpreseSubappaltiSubappaltatrice.Errore;
        }
    }

    protected void ButtonInserisciNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        Page.Validate("validationGroupImpresaAppaltataDa");

        if (Page.IsValid)
        {
            Impresa impresa = AccessoCantieriImpreseSubappaltiAppaltataDa.Impresa;
            ImpresaCollection impColl;

            if (ViewState["impreseSelColl"] == null)
            {
                impColl = new ImpresaCollection();
            }
            else
            {
                impColl = (ImpresaCollection) ViewState["impreseSelColl"];
            }

            if (impresa != null)
            {
                bool[] res = biz.EsisteIvaFiscImpresa(impresa.PartitaIva, impresa.CodiceFiscale);
                ControllaEsistenzaImpresa(LabelImpresaAppaltataDaEsistente, res);

                if (string.IsNullOrEmpty(LabelImpresaAppaltataDaEsistente.Text))
                {
                    impColl.Add(impresa);
                    ViewState["impreseSelColl"] = impColl;
                    ImpresaAppaltataDaSelezionata(impresa);
                    AccessoCantieriImpreseSubappaltiAppaltataDa.Reset();
                }
                else
                {
                    string pIva;
                    string codfisc;

                    if (res[0])
                    {
                        pIva = impresa.PartitaIva;
                        codfisc = string.Empty;
                        AccessoCantieriRicercaImpresaAppaltataDa.CaricaImpreseRicerca(codfisc, pIva);
                    }
                    else
                    {
                        if (res[2])
                        {
                            pIva = string.Empty;
                            codfisc = impresa.CodiceFiscale;
                            AccessoCantieriRicercaImpresaAppaltataDa.CaricaImpreseRicerca(codfisc, pIva);
                        }
                    }

                    ViewState["impresaAppaltata"] = null;

                    TextBoxImpresaSubappaltata.Text = string.Empty;

                    RadioButtonImpresaSubappaltataNuova.Checked = false;
                    RadioButtonImpresaSubappaltataSiceInfo.Checked = false;

                    ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

                    if (impreseTot.Count == 0)
                    {
                        PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                        AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
                        PanelInserisciImpresaSubappaltata.Visible = false;

                        //ButtonAnnulla.Enabled = false;
                        ButtonAggiungiAppalto.Enabled = false;

                        AccessoCantieriImpreseSubappaltiAppaltataDa.Reset();
                    }
                    else
                    {
                        PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                        AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
                        PanelInserisciImpresaSubappaltata.Visible = false;

                        //ButtonAnnulla.Enabled = false;
                        ButtonAggiungiAppalto.Enabled = false;

                        AccessoCantieriImpreseSubappaltiAppaltataDa.Reset();
                    }
                }
            }
            else
                LabelNuovaSubappaltataDaRes.Text = AccessoCantieriImpreseSubappaltiAppaltataDa.Errore;
        }
    }

    //protected void CustomValidatorDateCantiereAppaltante_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    DateTime? dataInizio = null;

    //    if (RadDateDataInizioAttivitaImpresaAppaltante.SelectedDate.HasValue)
    //        dataInizio = RadDateDataInizioAttivitaImpresaAppaltante.SelectedDate.Value;

    //    DateTime? dataFine = null;

    //    if (RadDateDataFineAttivitaImpresaAppaltante.SelectedDate.HasValue)
    //        dataFine = RadDateDataFineAttivitaImpresaAppaltante.SelectedDate.Value;

    //    DateTime dataInizioCantiere = DateTime.Parse(ViewState["dataInizio"].ToString());
    //    DateTime dataFineCantiere = DateTime.Parse(ViewState["dataFine"].ToString());

    //    if (dataInizio.HasValue && dataFine.HasValue)
    //    {
    //        if ((dataInizio < dataInizioCantiere) || (dataFine > dataFineCantiere))
    //        {
    //            args.IsValid = false;
    //        }
    //        else
    //        {
    //            args.IsValid = true;
    //        }
    //    }
    //}

    //protected void CustomValidatorDateCantiereSubappaltata_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    DateTime? dataInizio = null;

    //    if (RadDateDataInizioAttivitaImpresaSubappaltata.SelectedDate.HasValue)
    //        dataInizio = RadDateDataInizioAttivitaImpresaSubappaltata.SelectedDate.Value;

    //    DateTime? dataFine = null;

    //    if (RadDateDataFineAttivitaImpresaSubappaltata.SelectedDate.HasValue)
    //        dataFine = RadDateDataFineAttivitaImpresaSubappaltata.SelectedDate.Value;

    //    DateTime dataInizioCantiere = DateTime.Parse(ViewState["dataInizio"].ToString());
    //    DateTime dataFineCantiere = DateTime.Parse(ViewState["dataFine"].ToString());

    //    if (dataInizio.HasValue && dataFine.HasValue)
    //    {
    //        if ((dataInizio < dataInizioCantiere) || (dataFine > dataFineCantiere))
    //        {
    //            args.IsValid = false;
    //        }
    //        else
    //        {
    //            args.IsValid = true;
    //        }
    //    }
    //}

    protected void ButtonAnnullaNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        LabelRisultatoAppalto.Text = string.Empty;

        ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

        if (impreseTot.Count == 0)
        {
            PanelImpresaAppaltanteDropDown.Visible = false;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;

            //ButtonAnnulla.Enabled = false;
            ButtonAggiungiAppalto.Enabled = false;
        }
        else
        {
            PanelImpresaAppaltanteDropDown.Visible = true;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AccessoCantieriRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;

            //ButtonAnnulla.Enabled = false;
            ButtonAggiungiAppalto.Enabled = false;
        }
    }

    private static void ControllaEsistenzaImpresa(Label label, bool[] res)
    {
        StringBuilder mess = new StringBuilder();

        if (res[0])
            mess.Append("Nell'anagrafica SiceNew esiste gi� un'impresa con la partita IVA inserita\n");
        if (res[2])
            mess.Append("Nell'anagrafica SiceNew esiste gi� un'impresa con il codice fiscale inserito\n");
        label.Text = mess.ToString();
    }


    protected void ButtonAnnullaNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        LabelRisultatoAppalto.Text = string.Empty;

        ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

        if (impreseTot.Count == 0)
        {
            PanelImpresaAppaltanteDropDown.Visible = false;

            PanelRicercaNuovaImpresaAppaltante.Visible = true;
            AccessoCantieriRicercaImpresaSubappaltatrice.Visible = true;
            PanelInserisciImpresaAppaltante.Visible = false;

            //ButtonAnnulla.Enabled = false;
            ButtonAggiungiAppalto.Enabled = false;
        }
        else
        {
            PanelImpresaAppaltanteDropDown.Visible = true;

            PanelRicercaNuovaImpresaAppaltante.Visible = false;
            AccessoCantieriRicercaImpresaSubappaltatrice.Visible = false;
            PanelInserisciImpresaAppaltante.Visible = false;

            //ButtonAnnulla.Enabled = true;
            ButtonAggiungiAppalto.Enabled = true;
        }
    }

    #endregion

    #region Eventi sui controlli di ricerca inserimento

    private void AccessoCantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        ImpresaAppaltataDaSelezionata(impresa);
    }

    private void ImpresaAppaltataDaSelezionata(Impresa impresa)
    {
        TextBoxImpresaSubappaltata.Text = impresa.NomeCompleto;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaSubappaltataSiceInfo.Checked = true;
        else
            RadioButtonImpresaSubappaltataNuova.Checked = true;
        ViewState["impresaAppaltatrice"] = impresa;

        AccessoCantieriRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = false;

        //ButtonAnnulla.Enabled = true;
        ButtonAggiungiAppalto.Enabled = true;

        PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = false;
    }

    private void AccessoCantieriRicercaImpresa2_OnImpresaSelected(Impresa impresa)
    {
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        ImpresaSubappaltatriceSelezionata(impresa);
    }

    private void ImpresaSubappaltatriceSelezionata(Impresa impresa)
    {
        TextBoxImpresaAppaltante.Text = impresa.NomeCompleto;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaAppaltanteSiceInfo.Checked = true;
        else
            RadioButtonImpresaAppaltanteNuova.Checked = true;
        ViewState["impresaAppaltata"] = impresa;

        AccessoCantieriRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaAppaltante.Visible = false;

        //ButtonAnnulla.Enabled = true;
        ButtonAggiungiAppalto.Enabled = true;

        PanelRicercaNuovaImpresaAppaltante.Visible = false;

        AccessoCantieriImpreseSubappaltiAppaltataDa.ImpostaCommittenteArtigiano(impresa.RagioneSociale);
    }

    private void AccessoCantieriRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        AccessoCantieriRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaAppaltante.Visible = true;

        //ButtonAnnulla.Enabled = false;
        ButtonAggiungiAppalto.Enabled = false;
    }

    private void AccessoCantieriRicercaImpresaAppaltataDa_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        AccessoCantieriRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = true;

        //ButtonAnnulla.Enabled = false;
        ButtonAggiungiAppalto.Enabled = false;
    }

    private void Reset()
    {
        AccessoCantieriImpreseSubappaltiAppaltataDa.Reset();
        AccessoCantieriImpreseSubappaltiSubappaltatrice.Reset();
        AccessoCantieriRicercaImpresaAppaltataDa.Reset();
        AccessoCantieriRicercaImpresaSubappaltatrice.Reset();

        ViewState["impresaAppaltata"] = null;
        ViewState["impresaAppaltatrice"] = null;

        TextBoxImpresaAppaltante.Text = string.Empty;
        TextBoxImpresaSubappaltata.Text = string.Empty;

        RadioButtonImpresaAppaltanteNuova.Checked = false;
        RadioButtonImpresaSubappaltataNuova.Checked = false;
        RadioButtonImpresaAppaltanteSiceInfo.Checked = false;
        RadioButtonImpresaSubappaltataSiceInfo.Checked = false;
    }

    #endregion
}