﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiDatiLavoratore.ascx.cs" Inherits="WebControls_CorsiDatiLavoratore" %>
<style type="text/css">
    .style1
    {
        width: 316px;
    }
</style>
<table  class="borderedTable">
    <tr>
        <td>
            Cognome*
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxCognome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognome" runat="server"
                ControlToValidate="TextBoxCognome" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome*
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxNome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server"
                ControlToValidate="TextBoxNome" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data nascita (gg/mm/aaaa)*
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxDataNascita" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidatorDataNascita" runat="server" ControlToValidate="TextBoxDataNascita"
                Type="Date" Operator="DataTypeCheck" ErrorMessage="Data di nascita non valida" ValidationGroup="datiLavoratore">*</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataNascita" runat="server"
                ControlToValidate="TextBoxDataNascita" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Sesso*
        </td>
        <td colspan="2">
            <asp:RadioButtonList ID="RadioButtonListSesso" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True">M</asp:ListItem>
                <asp:ListItem>F</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale*
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px" MaxLength="16"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                ControlToValidate="TextBoxCodiceFiscale" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                ValidationGroup="datiLavoratore">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr id="trPrimaEsperienza" runat="server">
        <td>
            Dichiara di non aver mai lavorato nell'edilizia*
        </td>
        <td class="style1">
            <asp:CheckBox ID="CheckBoxPrimaEsperienza" runat="server" Width="300px" MaxLength="16"></asp:CheckBox>
        </td>
        <td>
        </td>
    </tr>
    <tr id="trDataAssunzione" runat="server">
        <td>
            Data di assunzione (gg/mm/aaaa)*
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxDataAssunzione" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataAssunzione" runat="server"
                ControlToValidate="TextBoxDataAssunzione" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
            <asp:CompareValidator
                ID="CompareValidatorDataAssunzione"
                runat="server"
                ControlToValidate="TextBoxDataAssunzione"
                Operator="DataTypeCheck"
                Type="Date"
                ValidationGroup="datiLavoratore">
                *
            </asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Cittadinanza
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListCittadinanza" runat="server" Width="300px"
                AppendDataBoundItems="True"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Provincia di residenza
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListProvinciaResidenza" runat="server" Width="300px"
                AppendDataBoundItems="True"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Paese di nascita*
        </td>
        <td class="style1">
            <asp:DropDownList ID="DropDownListPaeseNascita" runat="server" Width="300px"
                AppendDataBoundItems="True" AutoPostBack="True" 
                onselectedindexchanged="DropDownListPaeseNascita_SelectedIndexChanged"></asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPaeseNascita" runat="server"
                ControlToValidate="DropDownListPaeseNascita" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Provincia di nascita
        </td>
        <td>
            <asp:DropDownList ID="DropDownListProvinciaNascita" runat="server" Width="300px"
                AppendDataBoundItems="True" AutoPostBack="True" 
                CssClass="campoDisabilitato" Enabled="False" 
                onselectedindexchanged="DropDownListProvinciaNascita_SelectedIndexChanged"></asp:DropDownList>
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorProvinciaNascita"
                runat="server"
                ValidationGroup="datiLavoratore" 
                onservervalidate="CustomValidatorProvinciaNascita_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Comune di nascita
        </td>
        <td>
            <asp:DropDownList ID="DropDownListComuneNascita" runat="server" Width="300px"
                AppendDataBoundItems="True" CssClass="campoDisabilitato" Enabled="False"></asp:DropDownList>
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorComuneNascita"
                runat="server"
                ValidationGroup="datiLavoratore" 
                onservervalidate="CustomValidatorComuneNascita_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Titolo di studio
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListTitoloStudio" runat="server" Width="300px" 
                AppendDataBoundItems="True"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            * campi obbligatori
        </td>
    </tr>
</table>