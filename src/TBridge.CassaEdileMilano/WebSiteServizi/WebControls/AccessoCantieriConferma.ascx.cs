﻿using System;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

public partial class WebControls_AccessoCantieriConferma : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaImprese(ImpresaCollection imprese)
    {
        AccessoCantieriDati1.CaricaImprese(imprese);
    }

    public void CaricaDomanda(WhiteList domanda)
    {
        AccessoCantieriDati1.CaricaDomanda(domanda);
    }

    public void CaricaAltrePersone(AltraPersonaCollection altrePersone)
    {
        AccessoCantieriDati1.CaricaAltrePersone(altrePersone);
    }

    public void CaricaReferenti(ReferenteCollection referenti)
    {
        AccessoCantieriDati1.CaricaReferenti(referenti);
    }

    public void CaricaRilevatori(RilevatoreCantiereCollection rilevatori)
    {
        AccessoCantieriDati1.CaricaRilevatori(rilevatori);
    }
}