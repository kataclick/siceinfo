﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuEdilconnect.ascx.cs" Inherits="WebControls_MenuEdilconnect" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%
    //if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
    //{
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Edilconnect
        </td>
    </tr>
    <tr>
        <td>
            <a id="A1" href="http://ww2.cassaedilemilano.it/ImpreseeConsulenti/InformazioniOperative/Edilconnect/tabid/211/language/it-IT/Default.aspx" runat="server">Info su Edilconnect</a>
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
        {
    %>
    <tr>
        <td>
            <a id="RedirectEdilconnectLink" runat="server" style="cursor:pointer">Accedi a Edilconnect</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    //}
%>