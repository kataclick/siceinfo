using System;
using System.Web.UI;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_CptDettagliNotifica : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaNotifica(Notifica notifica)
    {
        if (notifica != null)
        {
            TextBoxIdNotifica.Text = notifica.IdNotifica.ToString();
            TextBoxData.Text = notifica.Data.ToShortDateString();
            TextBoxDataInserimento.Text = notifica.DataInserimento.ToShortDateString();
            TextBoxUtente.Text = notifica.Utente;
            TextBoxNaturaOpera.Text = notifica.NaturaOpera;
            TextBoxCommittente.Text = notifica.CommittenteRagioneSociale;

            foreach (Indirizzo indirizzo in notifica.Indirizzi)
            {
                TextBoxIndirizzi.Text = String.Format("{0}\n", indirizzo.IndirizzoCompleto);
            }
        }
    }
}