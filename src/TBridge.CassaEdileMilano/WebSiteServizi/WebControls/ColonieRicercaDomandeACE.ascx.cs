using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Delegates;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Enums;
using TBridge.Cemi.Colonie.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using CassaEdile=TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;

public partial class WebControls_ColonieRicercaDomandeACE : UserControl
{
    private const int INDICEELIMINA = 6;
    private const int INDICEMODIFICA = 5;

    private ColonieBusiness biz = new ColonieBusiness();

    private string idCassaEdile = null;

    private int? idVacanza;

    private bool readOnly;

    public int? IdVacanza
    {
        get { return idVacanza; }
        set { idVacanza = value; }
    }

    public bool ReadOnly
    {
        get { return readOnly; }
        set { readOnly = value; }
    }

    public event DomandaACESelectedEventHandler OnDomandaSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsCassaEdile())
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile cassaEdile =
            //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.CassaEdile)HttpContext.Current.User.Identity).
            //            Entity;
            CassaEdile cassaEdile =
                (CassaEdile) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idCassaEdile = cassaEdile.IdCassaEdile;
        }

        if (!Page.IsPostBack)
        {
            CaricaStati();
        }
    }

    public void ModalitaReadOnly()
    {
        ReadOnly = true;
    }

    private void CaricaStati()
    {
        DropDownListStato.Items.Clear();

        DropDownListStato.Items.Add(new ListItem("Tutti", string.Empty));
        DropDownListStato.DataSource = Enum.GetNames(typeof (StatoDomandaACE));
        DropDownListStato.DataBind();
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate();

        if (Page.IsValid)
        {
            GridViewDomande.PageIndex = e.NewPageIndex;
            CaricaDomande();
        }
    }

    protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.NewSelectedIndex].Value;

        DomandaACEFilter filtro = new DomandaACEFilter();
        filtro.IdDomanda = idDomanda;
        DomandaACE domanda = biz.GetDomandeACE(filtro)[0];

        if (OnDomandaSelected != null)
            OnDomandaSelected(domanda);
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewDomande.PageIndex = 0;
            CaricaDomande();
        }
    }

    private DomandaACECollection CaricaDomande()
    {
        DomandaACEFilter filtro = new DomandaACEFilter();

        if (!string.IsNullOrEmpty(DropDownListTurno.SelectedValue))
            filtro.IdTurno = Int32.Parse(DropDownListTurno.SelectedValue);

        if (!string.IsNullOrEmpty(TextBoxCognomeLavoratore.Text.Trim()))
            filtro.CognomeLavoratore = TextBoxCognomeLavoratore.Text.Trim();

        if (!string.IsNullOrEmpty(TextBoxCognomeBambino.Text.Trim()))
            filtro.CognomeBambino = TextBoxCognomeBambino.Text.Trim();

        if (!string.IsNullOrEmpty(DropDownListStato.SelectedValue))
            filtro.Stato = (StatoDomandaACE) Enum.Parse(typeof (StatoDomandaACE), DropDownListStato.SelectedValue);

        filtro.IdVacanza = idVacanza;
        filtro.IdCassaEdile = idCassaEdile;

        DomandaACECollection domande = biz.GetDomandeACE(filtro);
        GridViewDomande.DataSource = domande;
        GridViewDomande.DataBind();

        return domande;
    }

    protected void GridViewDomande_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.NewEditIndex].Value;

        Context.Items["IdDomanda"] = idDomanda;
        Server.Transfer("~/Colonie/IscrizioneAltreCasseEdili.aspx", true);
    }

    public void CaricaTurni(TurnoCollection turni)
    {
        DropDownListTurno.Items.Clear();

        DropDownListTurno.Items.Add(new ListItem("Tutti", string.Empty));
        DropDownListTurno.DataSource = turni;
        DropDownListTurno.DataTextField = "DescrizioneTurno";
        DropDownListTurno.DataValueField = "IdCombo";
        DropDownListTurno.DataBind();
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DomandaACE domanda = (DomandaACE) e.Row.DataItem;

            if (domanda.StatoDomanda != StatoDomandaACE.DaValutare || ReadOnly)
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = false;
                e.Row.Cells[INDICEELIMINA].Enabled = false;
            }
        }
    }

    protected void ButtonEsporta_Click(object sender, EventArgs e)
    {
        DomandaACECollection domande = CaricaDomande();
        StringWriter sw = PreparaStampa(domande);

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Domande.xls");
        Response.ContentType = "application/vnd.ms-excel";
        Response.Write(sw.ToString());
        Response.End();
    }

    private StringWriter PreparaStampa(DomandaACECollection domande)
    {
        GridView gv = new GridView();
        gv.ID = "gvDomande";
        gv.AutoGenerateColumns = false;

        BoundField bc1 = new BoundField();
        bc1.HeaderText = "Lavoratore";
        bc1.DataField = "StringaLavoratore";
        BoundField bc2 = new BoundField();
        bc2.HeaderText = "Codice fiscale lavoratore";
        bc2.DataField = "CodiceFiscaleLavoratore";
        BoundField bc3 = new BoundField();
        bc3.HeaderText = "Partecipante";
        bc3.DataField = "StringaPartecipante";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Codice fiscale partecipante";
        bc4.DataField = "CodiceFiscalePartecipante";
        BoundField bc5 = new BoundField();
        bc5.HeaderText = "Indirizzo";
        bc5.DataField = "IndirizzoLavoratore";
        BoundField bc6 = new BoundField();
        bc6.HeaderText = "Stato";
        bc6.DataField = "StatoDomanda";
        BoundField bc7 = new BoundField();
        bc7.HeaderText = "N� corredo";
        bc7.DataField = "NumeroCorredo";

        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc6);
        gv.Columns.Add(bc7);

        gv.DataSource = domande;
        gv.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw;
    }

    protected void GridViewDomande_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.RowIndex].Value;

        if (biz.DeleteDomandaACE(idDomanda))
        {
            CaricaDomande();
            LabelErrore.Visible = false;
        }
        else
            LabelErrore.Visible = true;
    }
}