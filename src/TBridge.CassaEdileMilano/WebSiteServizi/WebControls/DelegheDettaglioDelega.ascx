<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DelegheDettaglioDelega.ascx.cs"
    Inherits="WebControls_DelegheDettaglioDelega" %>
<table class="standardTable">
    <tr>
        <td>
            Comprensorio:
        </td>
        <td>
            <asp:Label ID="LabelComprensorio" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Delega da:
        </td>
        <td>
            <asp:Label ID="LabelDelegaRecuperataDa" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            Cognome:
        </td>
        <td>
            <asp:Label ID="LabelCognome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Nome:
        </td>
        <td>
            <asp:Label ID="LabelNome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita:
        </td>
        <td>
            <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Codice impresa:
        </td>
        <td>
            <asp:Label ID="LabelImpresaCodice" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Impresa:
        </td>
        <td>
            <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            Residenza:
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Indirizzo:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaIndirizzo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaProvincia" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaComune" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cap:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaCap" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            Cantiere:
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Indirizzo:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereIndirizzo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereProvincia" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereComune" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cap:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereCap" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<asp:Panel ID="PanelGestioneModifica" runat="server" Visible="True">
    <asp:Label ID="LabelModifica" runat="server" Text="Se si reputa che la delega non sia stata associata causa errati dati anagrafici si pu� forzare una modifica degli stessi. Con questa operazione si autorizza Cassa Edile a gestire la delega in questione con i nuovi dati inseriti"
        Visible="False"></asp:Label>
    <br />
    <asp:Button ID="ButtonModifica" runat="server" OnClick="ButtonModifica_Click" Text="Forza modifica"
        Visible="False" />
</asp:Panel>
