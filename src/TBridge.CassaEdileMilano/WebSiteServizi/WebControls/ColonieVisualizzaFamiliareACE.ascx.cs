using System;
using System.Web.UI;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class WebControls_ColonieVisualizzaFamiliareACE : UserControl
{
    private Accompagnatore accompagnatore;
    private FamiliareACE familiare;

    public FamiliareACE Familiare
    {
        get { return GetFamiliare(); }
        set
        {
            familiare = value;
            if (familiare == null)
                Reset();
            else
                Set(familiare);
        }
    }

    public Accompagnatore Accompagnatore
    {
        get { return GetAccompagnatore(); }
        set
        {
            accompagnatore = value;
            if (accompagnatore == null)
            {
                PanelAccompagnatore.Visible = false;
                Reset();
            }
            else
            {
                PanelAccompagnatore.Visible = true;
                Set(accompagnatore);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void Set(FamiliareACE familiare)
    {
        ViewState["IdFamiliare"] = familiare.IdFamiliare.Value;
        LabelCognome.Text = familiare.Cognome;
        LabelNome.Text = familiare.Nome;
        LabelDataNascita.Text = familiare.DataNascita.ToShortDateString();
        LabelCodiceFiscale.Text = familiare.CodiceFiscale;
        LabelSesso.Text = familiare.Sesso.ToString();
        if (familiare.PortatoreHandicap)
        {
            CheckBoxPortatoreHandicap.Checked = true;
            PanelAccompagnatore.Visible = true;
            LabelHandicap.Text = familiare.NotaDisabilita;
        }
        else
        {
            CheckBoxPortatoreHandicap.Checked = false;
            PanelAccompagnatore.Visible = false;
            LabelHandicap.Text = null;
        }

        LabelIntolleranze.Text = familiare.IntolleranzeAlimentari;
    }

    private void Reset()
    {
        LabelCognome.Text = null;
        LabelNome.Text = null;
        LabelDataNascita.Text = null;
        LabelCodiceFiscale.Text = null;
        LabelSesso.Text = null;
        LabelIntolleranze.Text = null;
        LabelHandicap.Text = null;
    }

    private FamiliareACE GetFamiliare()
    {
        familiare = new FamiliareACE();

        if (ViewState["IdFamiliare"] != null)
            familiare.IdFamiliare = (int) ViewState["IdFamiliare"];

        familiare.Cognome = LabelCognome.Text.Trim().ToUpper();
        familiare.Nome = LabelNome.Text.Trim().ToUpper();
        familiare.DataNascita = DateTime.Parse(LabelDataNascita.Text.Replace('.', '/'));
        familiare.CodiceFiscale = LabelCodiceFiscale.Text.Trim().ToUpper();
        if (LabelSesso.Text == "M")
            familiare.Sesso = 'M';
        else
            familiare.Sesso = 'F';

        familiare.PortatoreHandicap = CheckBoxPortatoreHandicap.Checked;

        if (familiare.PortatoreHandicap)
            familiare.NotaDisabilita = LabelHandicap.Text.Trim().ToUpper();

        if (!string.IsNullOrEmpty(LabelIntolleranze.Text.Trim()))
            familiare.IntolleranzeAlimentari = LabelIntolleranze.Text;

        return familiare;
    }

    private void Set(Accompagnatore accom)
    {
        //
        LabelAccompagnatoreCognome.Text = accom.Cognome;
        LabelAccompagnatoreNome.Text = accom.Nome;
        LabelAccompagnatoreDataNascita.Text = accom.DataNascita.ToString();
        LabelAccompagnatoreSesso.Text = accom.Sesso.ToString();
    }

    private Accompagnatore GetAccompagnatore()
    {
        if (CheckBoxPortatoreHandicap.Checked)
        {
            accompagnatore = new Accompagnatore();

            accompagnatore.Cognome = LabelAccompagnatoreCognome.Text;
            accompagnatore.Nome = LabelAccompagnatoreNome.Text;
            accompagnatore.DataNascita = DateTime.Parse(LabelAccompagnatoreDataNascita.Text.Replace('.', '/'));

            if (LabelAccompagnatoreSesso.Text == "M")
                accompagnatore.Sesso = 'M';
            else
                accompagnatore.Sesso = 'F';

            return accompagnatore;
        }
        else
            return null;
    }
}