﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
//using System.Collections.Specialized;
//using TBridge.Cemi.AccessoCantieri.Business;
//using TBridge.Cemi.AccessoCantieri.Type.Entities;
//using TBridge.Cemi.AccessoCantieri.Type.Enums;

//using TBridge.Cemi.Type.Collections;

public partial class WebControls_AccessoCantieriLavoratoreInserimento : UserControl
{
    //private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
    //private readonly Common bizCommon = new Common();

    //private Lavoratore CreaLavoratore()
    //{
    //    Lavoratore lavoratore = new Lavoratore();

    //    lavoratore.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
    //    lavoratore.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
    //    lavoratore.DataNascita = RadDateDataNascita.SelectedDate.Value;
    //    lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
    //    lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;

    //    return lavoratore;
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void Reset()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        RadDateDataNascita.Clear();
        Presenter.SvuotaCampo(TextBoxIdLavoratore);
    }

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if ((RadDateDataNascita.SelectedDate.HasValue) && (TextBoxCodiceFiscale.Text.Length == 16))
        {
            DateTime dataNascita = RadDateDataNascita.SelectedDate.Value;

            if (!string.IsNullOrEmpty(TextBoxNome.Text) && !string.IsNullOrEmpty(TextBoxCognome.Text))
            {
                string maschio = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                           TextBoxCognome.Text, "M",
                                                                                           dataNascita);
                string femmina = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                           TextBoxCognome.Text, "F",
                                                                                           dataNascita);

                if (maschio.Substring(0, 6) != TextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper() &&
                    femmina.Substring(0, 6) != TextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper())
                {
                    args.IsValid = false;
                }
            }

            String codiceMaschile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "M", dataNascita, "A000");
            String codiceFemminile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "F", dataNascita, "A000");

            if (codiceMaschile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                &&
                codiceFemminile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                )
            {
                args.IsValid = false;
            }
        }
    }
}