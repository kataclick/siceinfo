using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Delegates;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Filters;

public partial class WebControls_ColonieGestioneFamiliare : UserControl
{
    private ColonieBusiness biz = new ColonieBusiness();

    public FamiliareACE Familiare
    {
        get { return ColonieVisualizzaFamiliareACE1.Familiare; }
    }

    public Accompagnatore Accompagnatore
    {
        get { return ColonieVisualizzaFamiliareACE1.Accompagnatore; }
    }

    public event FamiliareACEConfirmedEventHandler OnFamiliareACEConfirmed;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            Inizializzazione();

        ColonieModificaFamiliareACE1.OnCancelled += new CancelledEventHandler(ColonieModificaFamiliareACE1_OnCancelled);
        ColonieModificaFamiliareACE1.OnFamiliareACEModified +=
            new FamiliareACEModifiedEventHandler(ColonieModificaFamiliareACE1_OnFamiliareACEModified);
    }

    private void Inizializzazione()
    {
        //
        GridViewBambini.Visible = true;
        ColonieVisualizzaFamiliareACE1.Visible = false;
        ColonieModificaFamiliareACE1.Visible = false;

        ColonieModificaFamiliareACE1.Familiare = null;
        ColonieVisualizzaFamiliareACE1.Familiare = null;

        ButtonNuovoFamiliare.Visible = false;
    }


    protected void ButtonNuovoFamiliare_Click(object sender, EventArgs e)
    {
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
    }

    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        ButtonConferma.Visible = false;
        ButtonModifica.Visible = false;

        ColonieVisualizzaFamiliareACE1.Visible = false;
        ColonieModificaFamiliareACE1.Visible = true;

        ColonieModificaFamiliareACE1.Familiare = ColonieVisualizzaFamiliareACE1.Familiare;
        ColonieModificaFamiliareACE1.Accompagnatore = ColonieVisualizzaFamiliareACE1.Accompagnatore;
    }

    /// <summary>
    /// Carica i familiari del lavoratore
    /// </summary>
    /// <param name="idLavoratore"></param>
    public void CaricaFamiliariLavoratore(int idLavoratore)
    {
        FamiliareACEFilter filtro = new FamiliareACEFilter();
        filtro.IdLavoratore = idLavoratore;

        FamiliareACECollection bambini = biz.GetFamiliariACE(filtro);
        GridViewBambini.DataSource = bambini;
        GridViewBambini.PageIndex = 0;
        GridViewBambini.DataBind();

        ButtonNuovoFamiliare.Visible = true;
    }

    protected void GridViewBambini_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idFamiliare = (int) GridViewBambini.DataKeys[e.NewSelectedIndex].Value;

        ViewState["IdFamiliare"] = idFamiliare;

        int? idVacanza = null;
        if (ViewState["IdVacanza"] != null)
            idVacanza = (int) ViewState["IdVacanza"];

        FamiliareACE familiare = biz.GetFamiliareACE(idFamiliare, idVacanza);

        ButtonModifica.Visible = true;
        ButtonConferma.Visible = true;

        ColonieVisualizzaFamiliareACE1.Visible = true;

        ColonieVisualizzaFamiliareACE1.Familiare = familiare;

        if (familiare.Modificabile)
            ButtonModifica.Enabled = true;
        else
            ButtonModifica.Enabled = false;

        CaricaUltimoAccompagnatore(familiare.IdFamiliare.Value);
    }

    private bool CaricaUltimoAccompagnatore(int idFamiliareACE)
    {
        bool res = false;
        Accompagnatore accompagnatore = biz.GetUltimoAccompagnatore(idFamiliareACE);

        if (accompagnatore != null)
        {
            ColonieVisualizzaFamiliareACE1.Accompagnatore = accompagnatore;
            res = true;
        }

        return res;
    }

    protected void ButtonConfermaFamiliare_Click(object sender, EventArgs e)
    {
        GridViewBambini.Visible = false;
        ButtonConferma.Visible = false;
        ButtonModifica.Visible = false;

        if (OnFamiliareACEConfirmed != null)
            OnFamiliareACEConfirmed(ColonieVisualizzaFamiliareACE1.Familiare);
    }

    protected void ButtonModificaFamiliare_Click(object sender, EventArgs e)
    {
        ButtonModifica.Visible = false;
        ButtonConferma.Visible = false;
        ColonieVisualizzaFamiliareACE1.Visible = false;
        ColonieModificaFamiliareACE1.Visible = true;
        ColonieModificaFamiliareACE1.Familiare = ColonieVisualizzaFamiliareACE1.Familiare;
    }

    private void ColonieModificaFamiliareACE1_OnCancelled()
    {
        ButtonModifica.Visible = true;
        ButtonConferma.Visible = true;
        ColonieVisualizzaFamiliareACE1.Visible = true;
        ColonieModificaFamiliareACE1.Visible = false;
        ColonieModificaFamiliareACE1.Familiare = null;
    }

    private void ColonieModificaFamiliareACE1_OnFamiliareACEModified(FamiliareACE familiare)
    {
        ButtonModifica.Visible = true;
        ButtonConferma.Visible = true;
        ColonieVisualizzaFamiliareACE1.Visible = true;
        ColonieModificaFamiliareACE1.Visible = false;
        ColonieModificaFamiliareACE1.Familiare = null;
        ColonieVisualizzaFamiliareACE1.Familiare = ColonieModificaFamiliareACE1.Familiare;
    }
}