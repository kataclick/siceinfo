﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.Presenter;

#endregion

public partial class WebControls_AttestatoRegolaritaDatiDomanda : UserControl
{
    private const string TESTOMOSTRA = "Mostra";
    private const string TESTONASCONDI = "Nascondi";
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["MostraDatiGenerali"] = true;
            ViewState["MostraImprese"] = false;
            ViewState["MostraLavoratori"] = false;

            GestisciBottoni();
        }
    }

    private void GestisciBottoni()
    {
        bool mostraDatiGenerali = (bool) ViewState["MostraDatiGenerali"];
        bool mostraImprese = (bool) ViewState["MostraImprese"];
        bool mostraLavoratori = (bool) ViewState["MostraLavoratori"];

        if (mostraDatiGenerali)
        {
            ButtonMostraNascondiDatiGenerali.Text = TESTONASCONDI;
            divDatiGenerali.Visible = true;
        }
        else
        {
            ButtonMostraNascondiDatiGenerali.Text = TESTOMOSTRA;
            divDatiGenerali.Visible = false;
        }

        if (mostraImprese)
        {
            ButtonMostraNascondiAppalti.Text = TESTONASCONDI;
            divAppalti.Visible = true;
        }
        else
        {
            ButtonMostraNascondiAppalti.Text = TESTOMOSTRA;
            divAppalti.Visible = false;
        }

        if (mostraLavoratori)
        {
            ButtonMostraNascondiLavoratori.Text = TESTONASCONDI;
            divLavoratori.Visible = true;
        }
        else
        {
            ButtonMostraNascondiLavoratori.Text = TESTOMOSTRA;
            divLavoratori.Visible = false;
        }
    }

    public void CaricaDomanda(Domanda domanda)
    {
        SvuotaCampi();

        ViewState["Subappalti"] = domanda.Subappalti;
        if (domanda.IdDomanda.HasValue)
        {
            ViewState["imprese"] = biz.GetImpreseSelezionateInSubappalto(domanda.IdDomanda.Value);
        }

        LabelIndirizzo.Text = domanda.Indirizzo;
        LabelCivico.Text = domanda.Civico;
        LabelComune.Text = domanda.Comune;
        LabelProvincia.Text = domanda.Provincia;
        LabelCap.Text = domanda.Cap;
        LabelMese.Text = domanda.Mese.ToString("MM/yyyy");

        if (domanda.Importo.HasValue)
            LabelImporto.Text = domanda.Importo.Value.ToString("C");
        if (!string.IsNullOrEmpty(domanda.Descrizione))
            LabelDescrizione.Text = domanda.Descrizione;
        if (domanda.PercentualeManodopera.HasValue)
            LabelPercentualeManodopera.Text = domanda.PercentualeManodopera.ToString();
        if (domanda.NumeroLavoratori.HasValue)
            LabelNumeroLavoratori.Text = domanda.NumeroLavoratori.ToString();
        if (domanda.NumeroGiorni.HasValue)
            LabelNumeroGiorni.Text = domanda.NumeroGiorni.ToString();

        LabelDataInizio.Text = domanda.DataInizio.ToString("dd/MM/yyyy");
        LabelDataFine.Text = domanda.DataFine.ToString("dd/MM/yyyy");

        LabelAutorizzazione.Text = domanda.Autorizzazione;
        LabelConcessione.Text = domanda.Concessione;
        LabelDIA.Text = domanda.Dia;

        if (domanda.RichiestaVisitaIspettiva)
        {
            LabelRichiestaVisita.Text = "SI";
            if (domanda.DataVisitaIspettiva.HasValue)
                LabelDataVisita.Text = domanda.DataVisitaIspettiva.Value.Date.ToShortDateString();
            LabelOrarioVisita.Text = domanda.OrarioVisitaIspettiva;
            LabelContattoTelefonicoVisita.Text = domanda.ContattoTelefonicoVisitaIspettiva;
            LabelMotivoVisita.Text = domanda.MotivoVisitaIspettiva;
        }
        else
        {
            LabelRichiestaVisita.Text = "NO";
            LabelDataVisita.Text = string.Empty;
            LabelOrarioVisita.Text = string.Empty;
            LabelContattoTelefonicoVisita.Text = string.Empty;
            LabelMotivoVisita.Text = string.Empty;
        }

        LabelCommittenteCAP.Text = domanda.Committente.Cap;
        LabelCommittenteCivico.Text = domanda.Committente.Civico;
        LabelCommittenteComune.Text = domanda.Committente.ComuneDescrizione;
        LabelCommittenteIndirizzo.Text = domanda.Committente.Indirizzo;
        LabelCommittenteProvincia.Text = domanda.Committente.ProvinciaDescrizione;
        LabelCommittenteRagioneSociale.Text = domanda.Committente.RagioneSociale;
        LabelCommittenteTipologia.Text = domanda.Committente.Tipologia.ToString();
        LabelCommittenteCodFisc.Text = domanda.Committente.CodiceFiscale;
        LabelCommittentePIVA.Text = domanda.Committente.PartitaIVA;

        CaricaSubappalti(domanda.Subappalti, 0);

        AttestatoRegolaritaLavoratoriImpresa1.Inizializza(domanda, (ImpresaCollection) ViewState["imprese"]);

        if (domanda.IdDomanda.HasValue)
        {
            if (domanda.Lavoratori != null)
            {
                AttestatoRegolaritaLavoratoriTutteImprese1.CaricaImpreseLavoratori(domanda.Lavoratori);
            }
            else
            {
                AttestatoRegolaritaLavoratoriTutteImprese1.CaricaImpreseLavoratori(domanda.IdDomanda.Value);
            }
        }
        else
        {
            AttestatoRegolaritaLavoratoriTutteImprese1.CaricaImpreseLavoratori(domanda.Lavoratori);
        }
    }

    public void CaricaImprese(ImpresaCollection imprese)
    {
        ViewState["imprese"] = imprese;
    }

    private void CaricaSubappalti(SubappaltoCollection subappalti, Int32 pagina)
    {
        Presenter.CaricaElementiInListView(
            ListViewSubappalti,
            subappalti);

        Presenter.CaricaElementiInGridView(
            GridViewSubappalti,
            subappalti,
            pagina);
    }

    private void SvuotaCampi()
    {
        Presenter.SvuotaCampo(LabelIndirizzo);
        Presenter.SvuotaCampo(LabelCivico);
        Presenter.SvuotaCampo(LabelComune);
        Presenter.SvuotaCampo(LabelProvincia);
        Presenter.SvuotaCampo(LabelCap);
        Presenter.SvuotaCampo(LabelMese);

        Presenter.CaricaElementiInListView(
            ListViewSubappalti,
            null);

        Presenter.SvuotaCampo(LabelImporto);
        Presenter.SvuotaCampo(LabelDescrizione);
        Presenter.SvuotaCampo(LabelPercentualeManodopera);
        Presenter.SvuotaCampo(LabelNumeroLavoratori);
        Presenter.SvuotaCampo(LabelNumeroGiorni);

        Presenter.SvuotaCampo(LabelDataInizio);
        Presenter.SvuotaCampo(LabelDataFine);

        Presenter.SvuotaCampo(LabelAutorizzazione);
        Presenter.SvuotaCampo(LabelConcessione);
        Presenter.SvuotaCampo(LabelDIA);

        Presenter.SvuotaCampo(LabelRichiestaVisita);
        Presenter.SvuotaCampo(LabelDataVisita);
        Presenter.SvuotaCampo(LabelOrarioVisita);
        Presenter.SvuotaCampo(LabelMotivoVisita);
        Presenter.SvuotaCampo(LabelContattoTelefonicoVisita);

        Presenter.SvuotaCampo(LabelCommittenteCAP);
        Presenter.SvuotaCampo(LabelCommittenteCivico);
        Presenter.SvuotaCampo(LabelCommittenteComune);
        Presenter.SvuotaCampo(LabelCommittenteIndirizzo);
        Presenter.SvuotaCampo(LabelCommittenteProvincia);
        Presenter.SvuotaCampo(LabelCommittenteRagioneSociale);
        Presenter.SvuotaCampo(LabelCommittenteTipologia);
        Presenter.SvuotaCampo(LabelCommittentePIVA);
        Presenter.SvuotaCampo(LabelCommittenteCodFisc);
    }

    protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Subappalto subappalto = (Subappalto) e.Row.DataItem;
            Label lAtaRagioneSociale = (Label) e.Row.FindControl("LabelAtaRagioneSociale");
            Label lAtaPartitaIva = (Label) e.Row.FindControl("LabelAtaPartitaIva");
            Label lAtaCodiceFiscale = (Label) e.Row.FindControl("LabelAtaCodiceFiscale");
            Label lAtaIndirizzo = (Label) e.Row.FindControl("LabelAtaIndirizzo");
            Label lAtaArtigiano = (Label) e.Row.FindControl("LabelAtaArtigiano");

            Label lAnteRagioneSociale = (Label) e.Row.FindControl("LabelAnteRagioneSociale");
            Label lAntePartitaIva = (Label) e.Row.FindControl("LabelAntePartitaIva");
            Label lAnteCodiceFiscale = (Label) e.Row.FindControl("LabelAnteCodiceFiscale");
            Label lAnteIndirizzo = (Label) e.Row.FindControl("LabelAnteIndirizzo");
            Label lAnteArtigiano = (Label) e.Row.FindControl("LabelAnteArtigiano");

            // Appaltata
            lAtaRagioneSociale.Text = subappalto.NomeAppaltata;
            if (!String.IsNullOrEmpty(subappalto.PartitaIvaAppaltata))
                lAtaPartitaIva.Text = String.Format(" P.Iva: {0}", subappalto.PartitaIvaAppaltata);
            if (!String.IsNullOrEmpty(subappalto.CodiceFiscaleAppaltata))
                lAtaCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", subappalto.CodiceFiscaleAppaltata);
            lAtaIndirizzo.Text = subappalto.IndirizzoAppaltata;
            //Valla: sistemare campo, lavoratore autonomo può essere null...
            try
            {
                if (subappalto.Appaltata.LavoratoreAutonomo)
                    lAtaArtigiano.Text = "Artigiano autonomo senza dipendenti";
                else
                    lAtaArtigiano.Text = string.Empty;
            }
            catch
            {
                lAtaArtigiano.Text = string.Empty;
            }

            // Appaltante
            lAnteRagioneSociale.Text = subappalto.NomeAppaltatrice;
            if (!String.IsNullOrEmpty(subappalto.PartitaIvaAppaltatrice))
                lAntePartitaIva.Text = String.Format(" P.Iva: {0}", subappalto.PartitaIvaAppaltatrice);
            if (!String.IsNullOrEmpty(subappalto.CodiceFiscaleAppaltatrice))
                lAnteCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", subappalto.CodiceFiscaleAppaltatrice);
            lAnteIndirizzo.Text = subappalto.IndirizzoAppaltatrice;
            //Valla: sistemare campo, lavoratore autonomo può essere null...
            try
            {
                if (subappalto.Appaltante.LavoratoreAutonomo)
                    lAnteArtigiano.Text = "Artigiano autonomo senza dipendenti";
                else
                    lAnteArtigiano.Text = string.Empty;
            }
            catch
            {
                lAnteArtigiano.Text = string.Empty;
            }
        }
    }

    protected void GridViewSubappalti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["Subappalti"];
        CaricaSubappalti(subappalti, e.NewPageIndex);
    }

    protected void ButtonMostraNascondiDatiGenerali_Click(object sender, EventArgs e)
    {
        bool mostraDatiGenerali = (bool) ViewState["MostraDatiGenerali"];
        ViewState["MostraDatiGenerali"] = !mostraDatiGenerali;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiAppalti_Click(object sender, EventArgs e)
    {
        bool mostraImprese = (bool) ViewState["MostraImprese"];
        ViewState["MostraImprese"] = !mostraImprese;

        GestisciBottoni();
    }

    protected void ButtonMostraNascondiLavoratori_Click(object sender, EventArgs e)
    {
        bool mostraLavoratori = (bool) ViewState["MostraLavoratori"];
        ViewState["MostraLavoratori"] = !mostraLavoratori;

        GestisciBottoni();
    }
}