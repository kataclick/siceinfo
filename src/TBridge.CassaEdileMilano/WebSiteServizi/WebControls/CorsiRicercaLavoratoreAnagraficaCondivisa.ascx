﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiRicercaLavoratoreAnagraficaCondivisa.ascx.cs"
    Inherits="WebControls_CorsiRicercaLavoratoreAnagraficaCondivisa" %>
<asp:Panel ID="PanelRicercaLavoratori" runat="server" DefaultButton="ButtonVisualizza"
    Width="100%">
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxCognome"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Nome
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxNome"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Data di nascita (gg/mm/aaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                runat="server" ControlToValidate="TextBoxDataNascita" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaLavoratori" />
                            <asp:Button ID="ButtonNuovo" runat="server" OnClick="ButtonNuovo_Click" Text="Nuovo"
                                CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                    Width="100%" PageSize="5" AllowPaging="True" DataKeyNames="TipoLavoratore, IdAnagraficaCondivisa" 
                    OnPageIndexChanging="GridViewLavoratori_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewLavoratori_SelectedIndexChanging" 
                    onrowdatabound="GridViewLavoratori_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="IdLavoratore" HeaderText="Codice">
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <b>
                                    <%--<asp:Label ID="LabelIdLavoratore" runat="server" Text='<%# Bind("IdLavoratore") %>'></asp:Label>
                                    &nbsp;--%><asp:Label ID="LabelCognome" runat="server" Text='<%# Bind("Cognome") %>'></asp:Label>
                                    &nbsp;<asp:Label ID="LabelNome" runat="server" Text='<%# Bind("Nome") %>'></asp:Label>
                                </b>
                                <br />
                                <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
                                <br />
                                <table class="standardTable" runat="server" id="TableDati16Ore">
                                    <tr>
                                        <td style="width:150px;">
                                            Dichiara di non aver mai lavorato nell'edilizia*
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxPrimaEsperienza" runat="server" Width="100px"></asp:CheckBox>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:150px;">
                                            Data di assunzione (gg/mm/aaaa)*
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDataAssunzione" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator 
                                                ID="RequiredFieldValidatorDataAssunzione" 
                                                runat="server"
                                                ControlToValidate="TextBoxDataAssunzione" 
                                                ValidationGroup="datiLavoratoreSelezione"
                                                ForeColor="Red">*</asp:RequiredFieldValidator>
                                            <asp:CompareValidator
                                                ID="CompareValidatorDataAssunzione"
                                                runat="server"
                                                ControlToValidate="TextBoxDataAssunzione"
                                                Operator="DataTypeCheck"
                                                Type="Date"
                                                ValidationGroup="datiLavoratoreSelezione"
                                                ForeColor="Red">
                                                *
                                            </asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data di nascita">
                            <ItemTemplate>
                                <asp:Label ID="LabelDataNascita" runat="server" 
                                    Text='<%# Bind("DataNascita", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                <br />
                                <asp:Label ID="LabelLuogoNascita" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Cod. fisc." >
                        <ItemStyle Font-Bold="True" />
                        </asp:BoundField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="ButtonSeleziona" runat="server" CausesValidation="True" 
                                    CommandName="Select" Text="Seleziona" />
                            </ItemTemplate>
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun lavoratore trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
