﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriReferentiInserimento.ascx.cs"
    Inherits="WebControls_AccessoCantieriReferentiInserimento" %>
<table class="standardTable">
    <tr>
        <td>
            Cognome<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxCognome"
                ErrorMessage="Digitare un cognome" ValidationGroup="referente">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNome"
                ErrorMessage="Digitare un nome" ValidationGroup="referente">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita (gg/mm/aaaa)<b>*</b>:
        </td>
        <td>
            <%--<asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="250px"></asp:TextBox>--%>
            <telerik:RadDatePicker ID="RadDateDataNascita" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Digitare una data di nascita" ValidationGroup="referente">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Formato data errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                ValidationGroup="referente">*</asp:RegularExpressionValidator>--%>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RadDateDataNascita"
                ErrorMessage="Digitare una data di nascita" ValidationGroup="referente">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Digitare un codice fiscale" ValidationGroup="referente">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Formato codice fiscale errato" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$"
                ValidationGroup="referente">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non coerente con la data di nascita" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate"
                ValidationGroup="referente">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ruolo:
        </td>
        <td>
            <asp:DropDownList ID="DropDownList1" runat="server" Width="250px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Affiliazione:
        </td>
        <td>
            <asp:DropDownList ID="DropDownList2" runat="server" Width="250px">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <asp:TextBox ID="TextBoxTelefono" runat="server" MaxLength="11" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxTelefono"
                ErrorMessage="Il telefono inserito non è valido" ValidationExpression="\d+" ValidationGroup="referente">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            e-Mail:
        </td>
        <td>
            <asp:TextBox ID="TextBoxEmail" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxEmail"
                ErrorMessage="Indirizzo e-Mail non valido" ValidationExpression="^[\w\-\.]*[\w\.]\@[\w\.]*[\w\-\.]+[\w\-]+[\w]\.+[\w]+[\w $]"
                ValidationGroup="referente">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Modalità di contatto:
        </td>
        <td>
            <asp:RadioButton ID="RadioButtonEmail" runat="server" AutoPostBack="True" GroupName="modalitaContatto"
                Text="email" Checked="True" />
            <asp:RadioButton ID="RadioButtonTelefono" runat="server" AutoPostBack="True" GroupName="modalitaContatto"
                Text="telefono" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                ValidationGroup="altraPersona
                " />
        </td>
    </tr>
</table>
<br />
<b>*</b> campi obbligatori