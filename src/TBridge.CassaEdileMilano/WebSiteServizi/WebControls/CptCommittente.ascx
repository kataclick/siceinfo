<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptCommittente.ascx.cs" Inherits="WebControls_CptCommittente" %>
<%@ Register Src="CptSelezioneIndirizzo.ascx" TagName="CptSelezioneIndirizzo" TagPrefix="uc2" %>
<%@ Register Src="CptGestioneIndirizzi.ascx" TagName="CptGestioneIndirizzi" TagPrefix="uc1" %>
<asp:Panel ID="PanelCodiceCommittente" runat="server" Visible="False"
    Width="220px">
    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxIdCommittente" runat="server" Enabled="False" ReadOnly="True"
    Width="100px"></asp:TextBox></asp:Panel>
    
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="LabelRagioneSociale" runat="server" Text="Ragione sociale*"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="353px"></asp:TextBox></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxRagioneSociale"
                    ErrorMessage="Digitare una ragione sociale" ValidationGroup="ValidaInserimentoCommittente"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPartitaIVA" runat="server" Text="Partita IVA"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxPartitaIva" runat="server" MaxLength="11" Width="353px"></asp:TextBox></td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxPartitaIva"
                    ErrorMessage="Partita IVA errata" ValidationExpression="^\d{11}$" ValidationGroup="ValidaInserimentoCommittente"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCodiceFiscale" runat="server" Text="Codice fiscale"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="353px"></asp:TextBox></td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                    ErrorMessage="Codice fiscale errato" ValidationExpression="^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]" ValidationGroup="ValidaInserimentoCommittente"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Telefono"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxTelefono" runat="server" MaxLength="50" Width="353px"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Fax"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxFax" runat="server" MaxLength="50" Width="353px"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Persona di riferimento"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxPersonaRiferimento" runat="server" MaxLength="100" Width="353px"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Indirizzo*
            </td>
            <td colspan="2">
                <uc2:CptSelezioneIndirizzo ID="CptSelezioneIndirizzo1" runat="server" />
                
            </td>
        </tr>
    </table>
    
* campi obbligatori
