using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Colonie.Type.Delegates;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class WebControls_ColonieModificaFamiliareACE : UserControl
{
    private readonly Common _commonBiz = new Common();
    private Accompagnatore _accompagnatore;
    private FamiliareACE _familiare;

    public FamiliareACE Familiare
    {
        get { return GetFamiliare(); }
        set
        {
            _familiare = value;
            if (_familiare == null)
                Reset();
            else
                Set(_familiare);
        }
    }

    public Accompagnatore Accompagnatore
    {
        get { return GetAccompagnatore(); }
        set
        {
            _accompagnatore = value;
            if (_accompagnatore == null)
                Reset();
            else
                Set(_accompagnatore);
        }
    }

    public event FamiliareACEModifiedEventHandler OnFamiliareACEModified;
    public event CancelledEventHandler OnCancelled;


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void Set(FamiliareACE familiare)
    {
        ViewState["IdFamiliare"] = familiare.IdFamiliare.Value;
        TextBoxBambinoCognome.Text = familiare.Cognome;
        TextBoxBambinoNome.Text = familiare.Nome;
        TextBoxBambinoDataNascita.Text = familiare.DataNascita.ToShortDateString();
        TextBoxBambinoCodiceFiscale.Text = familiare.CodiceFiscale;
        if (familiare.Sesso == 'M')
            RadioButtonBambinoM.Checked = true;
        else
            RadioButtonBambinoF.Checked = true;
        CheckBoxBambinoHandicap.Checked = familiare.PortatoreHandicap;

        if (familiare.PortatoreHandicap)
        {
            PanelTipoDisabilita.Visible = true;
        }
        else
        {
            PanelTipoDisabilita.Visible = false;
        }
        TextBoxTipoDisabilita.Text = familiare.NotaDisabilita;
        TextBoxBambinoIntolleranze.Text = familiare.IntolleranzeAlimentari;
    }

    private void Reset()
    {
        TextBoxBambinoCognome.Text = null;
        TextBoxBambinoNome.Text = null;
        TextBoxBambinoDataNascita.Text = null;
        TextBoxBambinoCodiceFiscale.Text = null;
        RadioButtonBambinoM.Checked = true;
        CheckBoxBambinoHandicap.Checked = false;
        TextBoxBambinoIntolleranze.Text = null;
        PanelTipoDisabilita.Enabled = false;
        TextBoxTipoDisabilita.Text = null;

        TextBoxAccompagnatoreCognome.Text = null;
        TextBoxAccompagnatoreNome.Text = null;
        TextBoxAccompagnatoreDataNascita.Text = null;
        RadioButtonAccompagnatoreM.Checked = true;
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        if (OnCancelled != null)
            OnCancelled();
    }

    protected void ButtonModificaFamiliare_Click(object sender, EventArgs e)
    {
        if (OnFamiliareACEModified != null)
            OnFamiliareACEModified(_familiare);
    }

    private FamiliareACE GetFamiliare()
    {
        _familiare = new FamiliareACE();

        if (ViewState["IdFamiliare"] != null)
            _familiare.IdFamiliare = (int) ViewState["IdFamiliare"];

        _familiare.Cognome = TextBoxBambinoCognome.Text.Trim().ToUpper();
        _familiare.Nome = TextBoxBambinoNome.Text.Trim().ToUpper();
        _familiare.DataNascita = DateTime.Parse(TextBoxBambinoDataNascita.Text.Replace('.', '/'));
        _familiare.CodiceFiscale = TextBoxBambinoCodiceFiscale.Text.Trim().ToUpper();
        if (RadioButtonBambinoM.Checked)
            _familiare.Sesso = 'M';
        else
            _familiare.Sesso = 'F';

        _familiare.PortatoreHandicap = CheckBoxBambinoHandicap.Checked;
        if (_familiare.PortatoreHandicap)
            _familiare.NotaDisabilita = TextBoxTipoDisabilita.Text.Trim().ToUpper();

        if (!string.IsNullOrEmpty(TextBoxBambinoIntolleranze.Text.Trim()))
            _familiare.IntolleranzeAlimentari = TextBoxBambinoIntolleranze.Text;

        return _familiare;
    }

    protected void CheckBoxBambinoHandicap_CheckedChanged(object sender, EventArgs e)
    {
        PanelTipoDisabilita.Visible = CheckBoxBambinoHandicap.Checked;
    }

    private void Set(Accompagnatore accom)
    {
        //
        TextBoxAccompagnatoreCognome.Text = accom.Cognome;
        TextBoxAccompagnatoreNome.Text = accom.Nome;
        TextBoxAccompagnatoreDataNascita.Text = accom.DataNascita.ToString();
        if (accom.Sesso == 'M')
            RadioButtonAccompagnatoreM.Checked = true;
        else
            RadioButtonAccompagnatoreF.Checked = true;
    }

    private Accompagnatore GetAccompagnatore()
    {
        if (CheckBoxBambinoHandicap.Checked)
        {
            _accompagnatore = new Accompagnatore();

            _accompagnatore.Cognome = TextBoxAccompagnatoreCognome.Text;
            _accompagnatore.Nome = TextBoxAccompagnatoreNome.Text;
            _accompagnatore.DataNascita = DateTime.Parse(TextBoxAccompagnatoreDataNascita.Text.Replace('.', '/'));

            if (RadioButtonAccompagnatoreM.Checked)
                _accompagnatore.Sesso = 'M';
            else
                _accompagnatore.Sesso = 'F';

            return _accompagnatore;
        }
        return null;
    }

    #region Custom Validators

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!String.IsNullOrEmpty(TextBoxBambinoDataNascita.Text))
            {
                DateTime dataNascita = DateTime.Parse(TextBoxBambinoDataNascita.Text.Replace('.', '/'));
                String sesso;

                sesso = RadioButtonBambinoM.Checked ? "M" : "F";

                //Controlliamo il codice Fiscale parzialmente per evitare i problemi di omocodia
                if (!_commonBiz.VerificaPrimi11CaratteriCodiceFiscale(
                    Presenter.NormalizzaCampoTesto(TextBoxBambinoNome.Text),
                    Presenter.NormalizzaCampoTesto(TextBoxBambinoCognome.Text),
                    sesso,
                    dataNascita,
                    Presenter.NormalizzaCampoTesto(TextBoxBambinoCodiceFiscale.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CodiceFiscaleCarattereControllo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(TextBoxBambinoCodiceFiscale.Text))
            {
                args.IsValid = false;
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    #endregion
}