using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;
using TBridge.Cemi.Type.Entities;

public partial class WebControls_RegistrazioneSindacalista : UserControl
{
    private readonly Common bizCommon = new Common();
    private readonly GestioneUtentiBiz bizGU = new GestioneUtentiBiz();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti,
                                              "GestioneUtentiRegistrazioneSindacalista.aspx");

        if (!Page.IsPostBack)
        {
            CaricaDropDownList();
        }
    }

    private void CaricaDropDownList()
    {
        CaricaSindacati();
        CaricaComprensori();
    }

    private void CaricaComprensori()
    {
        DropDownListComprensorio.Items.Clear();
        DropDownListComprensorio.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListComprensorio.DataSource = bizCommon.GetComprensori(true);
        DropDownListComprensorio.DataTextField = "Descrizione";
        DropDownListComprensorio.DataValueField = "Id";
        DropDownListComprensorio.DataBind();
    }

    private void CaricaSindacati()
    {
        DropDownListSindacato.Items.Clear();
        DropDownListSindacato.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListSindacato.DataSource = bizCommon.GetSindacati();
        DropDownListSindacato.DataTextField = "Descrizione";
        DropDownListSindacato.DataValueField = "Id";
        DropDownListSindacato.DataBind();
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("GestioneUtentiDefault.aspx");
    }

    protected void ButtonRegistraSindacalista_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            //if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text))
            //{
            //Sindacalista sindacalista = new Sindacalista();
            Sindacalista sindacalista = new Sindacalista(
                0,
                TextBoxLogin.Text
                );

            sindacalista.Nome = TextBoxNome.Text.Trim();
            sindacalista.Cognome = TextBoxCognome.Text.Trim();

            sindacalista.Sindacato = new Sindacato(DropDownListSindacato.SelectedItem.Value,
                                                   DropDownListSindacato.SelectedItem.Text);
            if (!string.IsNullOrEmpty(DropDownListComprensorio.SelectedValue))
                sindacalista.ComprensorioSindacale =
                    new ComprensorioSindacale(DropDownListComprensorio.SelectedItem.Value,
                                              DropDownListComprensorio.SelectedItem.Text);

            sindacalista.Email = TextBoxEmail.Text;
            //sindacalista.UserName = TextBoxLogin.Text;
            sindacalista.Password = TextBoxPassword.Text;

            //Inseriamo il dipendente nel sistema
            switch (bizGU.InserisciSindacalista(sindacalista))
            {
                case ErroriRegistrazione.RegistrazioneEffettuata:
                    TextBoxNome.Text = string.Empty;
                    TextBoxCognome.Text = string.Empty;
                    DropDownListSindacato.SelectedIndex = 0;
                    DropDownListComprensorio.SelectedIndex = 0;
                    TextBoxLogin.Text = string.Empty;
                    TextBoxPassword.Text = string.Empty;
                    TextBoxPasswordRidigitata.Text = string.Empty;

                    LabelResult.Text = "Registrazione completata";

                    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                    break;
                case ErroriRegistrazione.ChallengeNonPassato:
                    LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                    break;
                case ErroriRegistrazione.LoginPresente:
                    LabelResult.Text =
                        "La username scelta � gi� presente nel sistema, sceglierne un'altra e riprovare.";
                    break;
                case ErroriRegistrazione.RegistrazioneGiaPresente:
                    LabelResult.Text = "La registrazione risulta come gi� effettuata.";
                    break;
                case ErroriRegistrazione.Errore:
                    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                    break;
                case ErroriRegistrazione.LoginNonCorretta:
                    LabelResult.Text =
                        "La username fornita contiene caratteri non validi o la sua lunghezza non � compresa fra 5 e 15 caratteri.";
                    break;
                case ErroriRegistrazione.PasswordNonCorretta:
                    LabelResult.Text =
                        "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                    break;
                default:
                    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                    break;
            }
            #region old
            //if (bizGU.InserisciSindacalista(sindacalista))
            //{
            //    TextBoxNome.Text = string.Empty;
            //    TextBoxCognome.Text = string.Empty;
            //    DropDownListSindacato.SelectedIndex = 0;
            //    DropDownListComprensorio.SelectedIndex = 0;
            //    TextBoxLogin.Text = string.Empty;
            //    TextBoxPassword.Text = string.Empty;
            //    TextBoxPasswordRidigitata.Text = string.Empty;

            //    LabelResult.Text = "Registrazione completata";

            //    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
            //}
            //else
            //{
            //    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare.";
            //}
            #endregion

            //}
            //else
            //{
            //    LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
            //}
        }
    }
}