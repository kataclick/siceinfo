<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DelegheDatiDelega.ascx.cs"
    Inherits="WebControls_DelegheDatiDelega" %>
<table class="standardTable">
    <tr>
        <td style="width: 25%;">
            Sindacato:
        </td>
        <td>
            <asp:Label ID="LabelSindacato" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Comprensorio:
        </td>
        <td>
            <asp:Label ID="LabelComprensorio" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Mese conferma:
        </td>
        <td>
            <asp:Label ID="LabelMeseConferma" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Stato:
        </td>
        <td>
            <asp:Label ID="LabelStato" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:Label ID="LabelId" runat="server" Font-Bold="True" Visible="False"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Cognome:
        </td>
        <td>
            <asp:Label ID="LabelCognome" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Nome:
        </td>
        <td>
            <asp:Label ID="LabelNome" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita:
        </td>
        <td>
            <asp:Label ID="LabelDataNascita" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Impresa:
        </td>
        <td>
            <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            Residenza
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td style="width: 25%;">
                        Indirizzo:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaIndirizzo" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaProvincia" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaComune" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cap:
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaCap" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            Cantiere
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td style="width: 25%;">
                        Indirizzo:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereIndirizzo" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereProvincia" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereComune" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cap:
                    </td>
                    <td>
                        <asp:Label ID="LabelCantiereCap" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
