using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Deleghe.Business;
using TBridge.Cemi.Deleghe.Type.Collections;
using TBridge.Cemi.Deleghe.Type.Delegates;
using TBridge.Cemi.Deleghe.Type.Entities;
using TBridge.Cemi.Deleghe.Type.Filters;

public partial class WebControls_DelegheRicercaLavoratore : UserControl
{
    private readonly DelegheBusiness biz = new DelegheBusiness();
    public event LavoratoriSelectedEventHandler OnLavoratoreSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaLavoratori()
    {
        LabelErrore.Text = string.Empty;
        string cognome;
        string nome;
        string codiceFiscale;
        DateTime? dataNascita = null;
        DateTime dataNascitaD;

        cognome = TextBoxCognome.Text.Trim();
        nome = TextBoxNome.Text.Trim();

        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
            dataNascita = dataNascitaD;

        codiceFiscale = TextBoxCodiceFiscale.Text;

        LavoratoreFilter filtro = new LavoratoreFilter();
        filtro.Cognome = cognome;
        filtro.Nome = nome;
        filtro.DataNascita = dataNascita;
        filtro.CodiceFiscale = codiceFiscale;

        LavoratoreCollection listaLavoratori = biz.GetLavoratori(filtro);
        if (listaLavoratori != null && listaLavoratori.Count > 0)
            LabelSeleziona.Visible = true;

        GridViewLavoratori.DataSource = listaLavoratori;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idLavoratore = (int) GridViewLavoratori.DataKeys[e.NewSelectedIndex]["IdLavoratore"];
        LavoratoreFilter filtro = new LavoratoreFilter();
        filtro.IdLavoratore = idLavoratore;
        Lavoratore lavoratore = biz.GetLavoratori(filtro)[0];

        if (OnLavoratoreSelected != null)
            OnLavoratoreSelected(lavoratore);
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!string.IsNullOrEmpty(TextBoxCognome.Text.Trim())
                || !string.IsNullOrEmpty(TextBoxNome.Text.Trim())
                || !string.IsNullOrEmpty(TextBoxDataNascita.Text.Trim())
                || !string.IsNullOrEmpty(TextBoxCodiceFiscale.Text.Trim()))
            {
                LabelEsitoRicerca.Visible = true;
                LabelErrore.Visible = false;
                CaricaLavoratori();
            }
            else
            {
                LabelErrore.Visible = true;
                LabelErrore.Text = "Immettere un filtro";
            }
        }
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaLavoratori();

        GridViewLavoratori.PageIndex = e.NewPageIndex;
        GridViewLavoratori.DataBind();
    }

    protected void ButtonChiudi_Click(object sender, EventArgs e)
    {
        Visible = false;
    }
}