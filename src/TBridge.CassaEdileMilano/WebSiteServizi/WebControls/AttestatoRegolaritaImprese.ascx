﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaImprese.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaImprese" %>
<%@ Register Src="AttestatoRegolaritaRicercaImpresa.ascx" TagName="AttestatoRegolaritaRicercaImpresa"
    TagPrefix="uc4" %>
<%@ Register Src="AttestatoRegolaritaImpreseSubappalti.ascx" TagName="AttestatoRegolaritaImpreseSubappalti"
    TagPrefix="uc5" %>

<table class="standardTable">
    <tr>
        <td>
            <asp:GridView ID="GridViewSubappalti" runat="server" AutoGenerateColumns="False"
                DataKeyNames="IdSubappalto" OnRowDeleting="GridViewSubappalti_RowDeleting" Width="100%"
                OnRowDataBound="GridViewSubappalti_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Subappaltata">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltataRagioneSociale" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltataIndirizzo" runat="server" Font-Size="XX-Small" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltataArtigiano" runat="server" Font-Size="XX-Small" ForeColor="Gray"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="45%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appaltatore/impresa">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltatriceRagioneSociale" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltatriceIndirizzo" runat="server" Font-Size="XX-Small"
                                            ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltatriceArtigiano" runat="server" Font-Size="XX-Small"
                                            ForeColor="Gray"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="45%" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="NomeAppaltatrice" HeaderText="Subappaltata" Visible="False">
                        <ItemStyle Width="45%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="NomeAppaltata" HeaderText="Appaltatore/impresa" Visible="False">
                        <ItemStyle Width="45%" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Cancellazione">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonDelete" runat="server" CommandName="Delete" ImageUrl="~/images/pallinoX.png"
                                ToolTip="Cancella l'appalto" />
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun appalto presente
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Button ID="ButtonVisualizzaAppalto" runat="server" OnClick="ButtonVisualizzaAppalto_Click"
                Text="Nuovo appalto..." Width="130px" CausesValidation="False" />
            <asp:CustomValidator ID="CustomValidatorAlmenoUnImpresa" runat="server" ErrorMessage="Inserire almeno un appalto"
                OnServerValidate="CustomValidatorAlmenoUnImpresa_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidatorImpresaRichiedentePresente" runat="server"
                ErrorMessage="Gli appalti devono comprendere l'impresa che effettua la richiesta "
                OnServerValidate="CustomValidatorImpresaRichiedentePresente_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAppalti" runat="server" Width="100%" Visible="False">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Crea appalto"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td valign="top">
                            Appaltatore/
                            <br />
                            Impresa
                        </td>
                        <td>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="RadioButtonImpresaAppaltanteSiceInfo" runat="server" Text="Anagrafica"
                                            Enabled="False" GroupName="scelta" />
                                        <asp:RadioButton ID="RadioButtonImpresaAppaltanteNuova" runat="server" Text="Nuova impresa"
                                            Enabled="False" GroupName="scelta" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelAppaltante" runat="server" Visible="true" Width="100%">
                                                        <table class="standardTable">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxImpresaAppaltante" runat="server" Text="" Width="400px" Height="57px"
                                                                        TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:Label ID="LabelImpresaSelezionataEsistente" runat="server" ForeColor="Red"></asp:Label>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxImpresaAppaltante"
                                                                        ErrorMessage="*" ValidationGroup="aggiungiAppalto"></asp:RequiredFieldValidator>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelImpresaAppaltanteDropDown" runat="server" Width="100%">
                                            <asp:DropDownList ID="DropDownListImpresaAppaltante" runat="server" Width="300px">
                                            </asp:DropDownList>
                                            <asp:Button ID="ButtonSelezionaImpresaAppaltanteDaDropDownList" runat="server" CausesValidation="False"
                                                OnClick="ButtonSelezionaImpresaAppaltataDaDropDownList_Click" Text="Seleziona" />
                                            &nbsp;<asp:Button ID="ButtonAltroImpresaAppaltante" runat="server" Text="Altra impresa"
                                                Width="100px" OnClick="ButtonAltroImpresaAppaltata_Click" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelRicercaNuovaImpresaAppaltante" runat="server" Width="100%" Visible="False">
                                            <uc4:AttestatoRegolaritaRicercaImpresa ID="AttestatoRegolaritaRicercaImpresaSubappaltatrice"
                                                runat="server" Visible="False" />
                                        </asp:Panel>
                                        <asp:Panel ID="PanelInserisciImpresaAppaltante" runat="server"  Width="100%"
                                            Visible="false">
                                            <table class="filledtable">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="borderedTable">
                                                <tr>
                                                    <td>
                                                        <uc5:AttestatoRegolaritaImpreseSubappalti ID="AttestatoRegolaritaImpreseSubappaltiSubappaltatrice"
                                                            runat="server" />
                                                        <br />
                                                        <asp:Button ID="ButtonInserisciNuovaImpresaAppaltante" runat="server" Text="Inserisci nuova impresa"
                                                            Width="170" OnClick="ButtonInserisciNuovaImpresaSubappaltatrice_Click" />
                                                        <asp:Button ID="ButtonAnnullaNuovaImpresaAppaltante" runat="server" Text="Annulla"
                                                            Width="170" OnClick="ButtonAnnullaNuovaImpresaSubappaltatrice_Click" />
                                                        <br />
                                                        <asp:Label ID="LabelNuovaAppaltanteRes" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Subappaltata
                        </td>
                        <td>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="RadioButtonImpresaSubappaltataSiceInfo" runat="server" Text="Anagrafica"
                                            Enabled="False" GroupName="scelta" />
                                        <asp:RadioButton ID="RadioButtonImpresaSubappaltataNuova" runat="server" Text="Nuova impresa"
                                            Enabled="False" GroupName="scelta" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelSubappaltata" runat="server" Visible="true" Width="100%">
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxImpresaSubappaltata" runat="server" Text="" Width="400px"
                                                            Height="57px" TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:Label ID="LabelImpresaAppaltataDaEsistente" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelImpresaSubappaltataDropDown" runat="server" Width="100%" Visible="False">
                                            <asp:DropDownList ID="DropDownListImpresaSubappaltata" runat="server" Width="300px">
                                            </asp:DropDownList>
                                            <asp:Button ID="ButtonSelezionaImpresaSubappaltataDaDropDownList" runat="server"
                                                CausesValidation="False" OnClick="ButtonSelezionaImpresaDaDropDownList_Click"
                                                Text="Seleziona" />
                                            &nbsp;<asp:Button ID="ButtonAltroSubappaltata" runat="server" Text="Altra impresa"
                                                Width="100px" OnClick="ButtonAltro_Click" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelRicercaImpresaSubappaltataNuovaImpresa" runat="server" Width="100%"
                                            Visible="false">
                                            <uc4:AttestatoRegolaritaRicercaImpresa ID="AttestatoRegolaritaRicercaImpresaAppaltataDa"
                                                runat="server" Visible="false" />
                                        </asp:Panel>
                                        <asp:Panel ID="PanelInserisciImpresaSubappaltata" runat="server" Visible="false"
                                            Width="500px">
                                            <table class="filledtable">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="borderedTable">
                                                <tr>
                                                    <td>
                                                        <uc5:AttestatoRegolaritaImpreseSubappalti ID="AttestatoRegolaritaImpreseSubappaltiAppaltataDa"
                                                            runat="server" />
                                                        <ul>
                                                            <li>
                                                                <asp:Button ID="ButtonInserisciNuovaImpresaSubappaltata" runat="server" OnClick="ButtonInserisciNuovaImpresaAppaltataDa_Click"
                                                                    Text="Inserisci nuova impresa" Width="170" />
                                                                <asp:Button ID="ButtonAnnullaNuovaImpresaSubappaltata" runat="server" CausesValidation="False"
                                                                    OnClick="ButtonAnnullaNuovaImpresaAppaltataDa_Click" Text="Annulla" Width="170" />
                                                                <br />
                                                                <asp:Label ID="LabelNuovaSubappaltataDaRes" runat="server" ForeColor="Red"></asp:Label>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="ButtonAggiungiAppalto" runat="server" OnClick="ButtonAggiungiAppalto_Click"
                                Text="Salva appalto" />
                            <asp:Button ID="ButtonAnnulla" runat="server" CausesValidation="False" OnClick="ButtonAnnulla_Click"
                                Text="Annulla" Width="150px" />
                            <asp:Label ID="LabelRisultatoAppalto" runat="server" ForeColor="Red"></asp:Label>
                            <asp:CustomValidator ID="CustomValidatorAppalto" runat="server" ErrorMessage="Appalto non valido.">&nbsp;</asp:CustomValidator>
                            <br />
                            <asp:CustomValidator ID="CustomValidatorSubappalti" runat="server" ErrorMessage="Appalto non valido"
                                OnServerValidate="CustomValidatorSubappalti_ServerValidate" ValidationGroup="appalto"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
