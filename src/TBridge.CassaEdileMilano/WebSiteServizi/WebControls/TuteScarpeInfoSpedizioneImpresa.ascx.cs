using System;
using System.Web.UI;
using TBridge.Cemi.TuteScarpe.Data.Entities;

public partial class WebControls_TuteScarpeInfoSpedizioneImpresa : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaDatiImpresa(string ragioneSociale, Indirizzo sedeLegale, Indirizzo sedeAmministrativa)
    {
        LabelRagioneSociale.Text = "Contatti per l'impresa: " + ragioneSociale;

        if (sedeLegale != null)
        {
            LabelSedeLegaleIndirizzo.Text = sedeLegale.IndirizzoCompleto;
            LabelSedeLegaleTelefono.Text = sedeLegale.Telefono;
            LabelSedeLegaleFax.Text = sedeLegale.Fax;
            LabelSedeLegaleEmail.Text = sedeLegale.Email;
        }

        if (sedeAmministrativa != null)
        {
            LabelSedeAmministrativaIndirizzo.Text = sedeAmministrativa.IndirizzoCompleto;
            LabelSedeAmministrativaPresso.Text = sedeAmministrativa.Presso;
            LabelSedeAmministrativaTelefono.Text = sedeAmministrativa.Telefono;
            LabelSedeAmministrativaFax.Text = sedeAmministrativa.Fax;
            LabelSedeAmministrativaEmail.Text = sedeAmministrativa.Email;
        }
    }

    public void Reset()
    {
        LabelRagioneSociale.Text = null;
        LabelSedeLegaleIndirizzo.Text = null;
        LabelSedeLegaleTelefono.Text = null;
        LabelSedeLegaleFax.Text = null;
        LabelSedeLegaleEmail.Text = null;
        LabelSedeAmministrativaIndirizzo.Text = null;
        LabelSedeAmministrativaPresso.Text = null;
        LabelSedeAmministrativaTelefono.Text = null;
        LabelSedeAmministrativaFax.Text = null;
        LabelSedeAmministrativaEmail.Text = null;
    }
}