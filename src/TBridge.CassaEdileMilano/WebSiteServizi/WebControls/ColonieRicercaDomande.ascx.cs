using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class WebControls_ColonieRicercaDomande : UserControl
{
    private readonly ColonieBusiness biz = new ColonieBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonAggiorna.Attributes.Add("onclick", "opener.PostBackOnMainPage()");

        if (!Page.IsPostBack)
        {
            int idVacanza = Int32.Parse(Request.QueryString["idVacanza"]);

            Vacanza vacanza = biz.GetVacanze(null, idVacanza)[0];
            ViewState["Vacanza"] = vacanza;

            TurnoCollection turni = biz.GetTurni(null, idVacanza, null, null, null);
            ViewState["Turni"] = turni;

            CaricaDestinazioni(vacanza.TipoVacanza.IdTipoVacanza);
            CaricaTurni(vacanza.IdVacanza.Value);

            if (Request.QueryString["cognome"] != null)
            {
                TextBoxCognome.Text = Request.QueryString["cognome"];
                ButtonAggiorna.Visible = false;
            }

            CaricaDomande();
        }
    }

    private void CaricaTurni(int idVacanza)
    {
        TurnoCollection turni = biz.GetTurni(null, idVacanza, null, null, null);

        DropDownListTurno.Items.Add(new ListItem("Tutti", "TUTTI"));

        DropDownListTurno.DataSource = turni;
        DropDownListTurno.DataTextField = "DescrizioneTurno";
        DropDownListTurno.DataValueField = "IdCombo";
        DropDownListTurno.DataBind();
    }

    private void CaricaDestinazioni(int idTipoVacanza)
    {
        DestinazioneCollection destinazioni = biz.GetDestinazioni(idTipoVacanza, null);

        DropDownListDestinazione.Items.Add(new ListItem("Tutti", "TUTTI"));

        DropDownListDestinazione.DataSource = destinazioni;
        DropDownListDestinazione.DataTextField = "Luogo";
        DropDownListDestinazione.DataValueField = "IdCombo";
        DropDownListDestinazione.DataBind();
    }

    public void ImpostaFiltri(Boolean nonAssegnate, Int32 idDestinazione, Int32 idTurno)
    {
        if (nonAssegnate)
        {
            CheckBoxNonAssegnate.Checked = true;
            DropDownListDestinazione.Enabled = false;
            DropDownListTurno.Enabled = false;
        }
        else
        {
            DropDownListDestinazione.Enabled = true;
            DropDownListTurno.Enabled = true;
            CheckBoxNonAssegnate.Checked = false;
            DropDownListDestinazione.SelectedValue = idDestinazione.ToString();
            DropDownListTurno.SelectedValue = idTurno.ToString();
        }
        //CaricaDomande();
    }

    public void CaricaDomande()
    {
        Vacanza vacanza = (Vacanza) ViewState["Vacanza"];

        Int32? idDestinazione = null;
        Int32? idTurno = null;
        String cognome = null;
        Boolean nonAssegnate = false;

        if (DropDownListDestinazione.SelectedValue != null && DropDownListDestinazione.SelectedValue != "TUTTI")
            idDestinazione = Int32.Parse(DropDownListDestinazione.SelectedValue);
        if (DropDownListTurno.SelectedValue != null && DropDownListTurno.SelectedValue != "TUTTI")
            idTurno = Int32.Parse(DropDownListTurno.SelectedValue);
        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            cognome = TextBoxCognome.Text;
        nonAssegnate = CheckBoxNonAssegnate.Checked;

        DomandaCollection domande =
            biz.GetDomandeLiquidate(vacanza.IdVacanza.Value, idDestinazione, cognome, idTurno, false, null, nonAssegnate);
        GridViewDomande.DataSource = domande;
        GridViewDomande.DataBind();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        LabelMessaggio.Text = string.Empty;
        CaricaDomande();
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView gvTurni = (GridView) e.Row.FindControl("GridViewTurni");
            Domanda domanda = (Domanda) e.Row.DataItem;

            TurnoPresenzaCollection presenze = biz.GetPresenzeTurno(domanda.IdDomanda);
            gvTurni.DataSource = presenze;
            gvTurni.DataBind();
        }
    }

    protected void GridViewTurni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TurnoPresenza turnoPres = (TurnoPresenza) e.Row.DataItem;
            CheckBox cbScelta = (CheckBox) e.Row.FindControl("CheckBoxScelta");

            if (turnoPres.Presenza)
                cbScelta.Checked = true;
        }
    }

    protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.NewSelectedIndex].Value;
        GridView gvTurni = (GridView) GridViewDomande.Rows[e.NewSelectedIndex].FindControl("GridViewTurni");
        List<int> turni = new List<int>();

        foreach (GridViewRow row in gvTurni.Rows)
        {
            int idTurno = (int) gvTurni.DataKeys[row.RowIndex].Value;
            CheckBox cbScelta = (CheckBox) row.FindControl("CheckBoxScelta");

            if (cbScelta.Checked)
                turni.Add(idTurno);
        }

        if (biz.SalvaPresenze(idDomanda, turni))
        {
            LabelMessaggio.Text = "Situazione salvata";
            //Button bSalva = (Button)GridViewDomande.Rows[e.NewSelectedIndex].FindControl("ButtonSalva");

            StringCollection sovrapposizioni = biz.GetSovrapposizioniTurni(idDomanda);
            if (sovrapposizioni.Count > 0)
            {
                LabelSovrapposizioni.Text = "Rilevate le sovrapposizioni: ";
                foreach (string sov in sovrapposizioni)
                {
                    LabelSovrapposizioni.Text += sov + "   ";
                }
            }
            else
                LabelSovrapposizioni.Text = string.Empty;
        }
        else
            LabelMessaggio.Text = "Errore nel salvataggio";
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaDomande();

        GridViewDomande.PageIndex = e.NewPageIndex;
        GridViewDomande.DataBind();
    }

    protected void CheckBoxNonAssegnate_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxNonAssegnate.Checked)
        {
            DropDownListDestinazione.Enabled = false;
            DropDownListTurno.Enabled = false;

            DropDownListDestinazione.SelectedValue = "TUTTI";
            DropDownListTurno.SelectedValue = "TUTTI";
        }
        else
        {
            DropDownListDestinazione.Enabled = true;
            DropDownListTurno.Enabled = true;
        }
    }
}