<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniDatiFatturaOperatore.ascx.cs" Inherits="WebControls_PrestazioniDatiFatturaOperatore" %>
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
    <Columns>
        <asp:BoundField DataField="IdPrestazioniFattura" HeaderText="IdPrestazioniFattura"
            Visible="False" />
        <asp:BoundField DataField="idTipoPrestazioneImportoFattura" HeaderText="idTipoPrestazioneImportoFattura"
            Visible="False" />
        <asp:BoundField DataField="descrizione" HeaderText="Descrizione" />
        <asp:BoundField DataField="valore" HeaderText="Importo" />
        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" HeaderText="Azioni" SelectText="Conferma" ShowSelectButton="True" />
    </Columns>
</asp:GridView>
<br />
<br />
Annulla
