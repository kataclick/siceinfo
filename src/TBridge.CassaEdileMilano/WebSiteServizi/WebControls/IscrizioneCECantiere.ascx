﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneCECantiere.ascx.cs"
    Inherits="WebControls_IscrizioneCECantiere" %>
<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelCantiereNormale" runat="server" Text="Cantiere" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Provincia:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCantiereProvincia" runat="server" Width="300px"
                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListCantiereProvincia_SelectedIndexChanged" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCantiereProvincia" runat="server"
                ErrorMessage="Provincia mancante" ControlToValidate="DropDownListCantiereProvincia"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Comune:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCantiereComune" runat="server" Width="300px" AppendDataBoundItems="True"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownListCantiereComune_SelectedIndexChanged" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCantiereComune" runat="server"
                ErrorMessage="Comune mancante" ControlToValidate="DropDownListCantiereComune"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Frazione:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCantiereFrazione" runat="server" Width="300px"
                AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListCantiereFrazione_SelectedIndexChanged"
                AutoPostBack="True" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCantierePreIndirizzo" runat="server" Width="100px"
                AppendDataBoundItems="True" />
            <asp:TextBox ID="TextBoxCantiereIndirizzo" runat="server" MaxLength="255" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCantiereIndirizzo" runat="server"
                ErrorMessage="Indirizzo mancante" ControlToValidate="TextBoxCantiereIndirizzo"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPreIndirizzoCantiere" runat="server"
                ControlToValidate="DropDownListCantierePreIndirizzo" ErrorMessage="Pre indirizzo mancante"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            CAP:
        </td>
        <td>
            <asp:TextBox ID="TextBoxCantiereCap" runat="server" MaxLength="5" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCantiereCap" runat="server"
                ErrorMessage="Cap mancante" ControlToValidate="TextBoxCantiereCap" ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCantiereCap" runat="server"
                ControlToValidate="TextBoxCantiereCap" ErrorMessage="Cap non corretto" ValidationExpression="^\d{5}$"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCantiereCAP" runat="server" ControlToValidate="TextBoxCantiereCap"
                ErrorMessage="Non pu&#242; essere utilizzato un CAP generico per il cantiere"
                OnServerValidate="CustomValidatorCAP_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Lavoratori occupati:
        </td>
        <td>
            <asp:TextBox ID="TextBoxLavoratoriOccupati" runat="server" Width="300px" MaxLength="10" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoriOccupati" runat="server"
                ErrorMessage="Numero lavoratori occupati mancante" ControlToValidate="TextBoxLavoratoriOccupati"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorLavoratoriOccupati"
                runat="server" ControlToValidate="TextBoxLavoratoriOccupati" ErrorMessage="Numero lavoratori occupati non corretto"
                ValidationExpression="^\d{0,4}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCantiereLavoratoriOccupati" runat="server"
                ControlToValidate="TextBoxLavoratoriOccupati" ErrorMessage="Il numero dei lavoratori occupati deve essere positivo"
                OnServerValidate="CustomValidatorCantiereLavoratoriOccupati_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Committente:
        </td>
        <td>
            <asp:TextBox ID="TextBoxCantiereCommittente" runat="server" MaxLength="60" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCommittente" runat="server"
                ErrorMessage="Committente mancante" ControlToValidate="TextBoxCantiereCommittente"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
        </td>
    </tr>
</table>
