using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;

public partial class WebControls_PrestazioniSelezionaDocumentiRicevuta : UserControl
{
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        //if(!Page.IsPostBack)
        //    ViewState["IdDomanda"] = Context.Items["IdDomanda"];
    }

    /// <summary>
    /// In funzione dei check, estra la lista dei documenti di una domanda con iddomanda
    /// </summary>
    /// <returns></returns>
    public DocumentoCollection GetDocumenti(int idDomanda)
    {
        DocumentoCollection documenti = new DocumentoCollection();

        Domanda d = biz.GetDomanda(idDomanda);

        String idTipoPrestazione = d.IdTipoPrestazione; //DropDownListTipoPrestazioneTipoPrestazione.SelectedValue;
        String beneficiarioPrestazione = d.Beneficiario; //DropDownListTipoPrestazioneBeneficiario.SelectedValue;

        return GetDocumenti(idTipoPrestazione, beneficiarioPrestazione);
    }

    /// <summary>
    /// In funzione dei check, estra la lista dei documenti di una domanda non esistente
    /// </summary>
    /// <param name="idTipoPrestazione"></param>
    /// <param name="beneficiarioPrestazione"></param>
    /// <returns></returns>
    public DocumentoCollection GetDocumenti(String idTipoPrestazione, String beneficiarioPrestazione)
    {
        DocumentoCollection documenti = new DocumentoCollection();

        foreach (GridViewRow row in GridViewDocumentiRichiesti.Rows)
        {
            TipoDocumento tipoDocumento =
                (TipoDocumento) GridViewDocumentiRichiesti.DataKeys[row.RowIndex].Values["TipoDocumento"];
            CheckBox cbConsegnato = (CheckBox) row.FindControl("CheckBoxConsegnato");

            Documento documento = new Documento();
            documenti.Add(documento);

            documento.IdTipoPrestazione = idTipoPrestazione;
            documento.BeneficiarioPrestazione = beneficiarioPrestazione;
            documento.TipoDocumento = tipoDocumento;
            documento.Consegnato = cbConsegnato.Checked;
        }

        return documenti;
    }

    protected void GridViewDocumentiRichiesti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Documento documento = (Documento) e.Row.DataItem;

            Label lBeneficiario = (Label) e.Row.FindControl("LabelBeneficiario");
            Label lStato = (Label) e.Row.FindControl("LabelStato");
            CheckBox cbConsegnato = (CheckBox) e.Row.FindControl("CheckBoxConsegnato");
            Button bVisualizza = (Button) e.Row.FindControl("ButtonVisualizza");

            if (documento.Consegnato.HasValue)
                cbConsegnato.Checked = documento.Consegnato.Value;

            lBeneficiario.Text = biz.TrasformaGradoParentelaInDescrizione(documento.RiferitoA);

            if (!String.IsNullOrEmpty(documento.IdArchidoc)) //prima, quando idarch era int, il controllo era .HasValue
            {
                if (documento.DataScansione.HasValue)
                {
                    lStato.Text = String.Format("Presente in Cassa Edile. Ricevuto il {0}",
                                                documento.DataScansione.Value.ToShortDateString());
                }
                else
                {
                    lStato.Text = String.Format("Presente in Cassa Edile");
                }

                //bVisualizza.Enabled = true;
                bVisualizza.Visible = true;
            }
            else
            {
                lStato.Text = String.Format("Da allegare alla domanda");

                //bVisualizza.Enabled = false;
                bVisualizza.Visible = false;
            }
        }
    }

    /// <summary>
    /// carica la lista di documenti da consegnare in funzione della prestazione selezionata
    /// </summary>
    /// <param name="idTipoPrestazione"></param>
    /// <param name="beneficiario"></param>
    /// <param name="idLavoratore"></param>
    /// <param name="idFamiliare"></param>
    /// <param name="operatore"></param>
    public void CaricaDocumenti(String idTipoPrestazione, String beneficiario,
                                Int32 idLavoratore, Int32? idFamiliare, Boolean operatore, Int32? idPrestazioniDomanda)
    {
        Presenter.CaricaElementiInGridView(
            GridViewDocumentiRichiesti,
            biz.GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, operatore,
                                      idPrestazioniDomanda),
            0);
    }

    /// <summary>
    /// Carica una lista vuota di documenti
    /// </summary>
    public void CaricaDocumentiListaVuota()
    {
        Presenter.CaricaElementiInGridView(
            GridViewDocumentiRichiesti,
            null,
            0);
    }

    protected void GridViewDocumentiRichiesti_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int indice = Int32.Parse(e.CommandArgument.ToString());
        LabelErroreVisualizzazioneDocumento.Visible = false;

        if (e.CommandName == "visualizzadocumento")
        {
            string idArchidoc = (string) GridViewDocumentiRichiesti.DataKeys[indice].Values["IdArchidoc"];

            // Caricare il documento tramite il Web Service Archidoc
            try
            {
                IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                if (file != null)
                {
                    Presenter.RestituisciFileArchidoc(idArchidoc, file, Page);
                }
            }
            catch
            {
                LabelErroreVisualizzazioneDocumento.Visible = true;
            }
        }
    }
}