using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;

public partial class WebControls_CantieriCommittente : UserControl
{
    private string errore;

    /// <summary>
    /// Propriet� che costruisce il committente. Se sono presenti errori ritorna null
    /// </summary>
    public Committente Committente
    {
        get
        {
            if (ControlloCampiServer())
            {
                return CreaCommittente();
            }
            else return null;
        }
    }

    public string Errore
    {
        get { return errore; }
        set { errore = value; }
    }

    #region committente properties

    public int IdCommittente { get; set; }

    public string RagioneSociale { get; set; }

    public string PartitaIVA { get; set; }

    public string CodiceFiscale { get; set; }

    public string Indirizzo { get; set; }

    public string Provincia { get; set; }

    public string Comune { get; set; }

    public string CAP { get; set; }

    #endregion

    #region Biz properties

    private CantieriBusiness cantieriBiz;

    public CantieriBusiness CantieriBiz
    {
        get { return cantieriBiz; }
        set { cantieriBiz = value; }
    }

    #endregion

    #region Metodi per la gestione delle province, comuni, cap

    private void CaricaProvince()
    {
        cantieriBiz.CaricaProvinceInDropDown(DropDownListProvincia);
    }

    private void CaricaComuni(int idProvincia)
    {
        cantieriBiz.CaricaComuniInDropDown(DropDownListComuni, idProvincia);
    }

    private void CaricaCAP(Int64 idComune)
    {
        cantieriBiz.CaricaCapInDropDown(DropDownListCAP, idComune);
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        cantieriBiz.CambioSelezioneDropDownProvincia(DropDownListProvincia, DropDownListComuni, DropDownListCAP);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        cantieriBiz.CambioSelezioneDropDownComune(DropDownListComuni, DropDownListCAP);
    }

    #endregion

    public void ImpostaRagioneSociale(string ragioneSociale)
    {
        TextBoxRagioneSociale.Text = ragioneSociale;
    }

    public void ImpostaCommittente(Committente committente)
    {
        ViewState["IdCommittente"] = committente.IdCommittente;
        ViewState["FonteNotifica"] = committente.FonteNotifica;
        TextBoxRagioneSociale.Text = committente.RagioneSociale;
        TextBoxPartitaIva.Text = committente.PartitaIva;
        TextBoxCodiceFiscale.Text = committente.CodiceFiscale;
        TextBoxTelefono.Text = committente.Telefono;
        TextBoxFax.Text = committente.Fax;
        TextBoxPersonaRiferimento.Text = committente.PersonaRiferimento;
        TextBoxIndirizzo.Text = committente.Indirizzo;

        LabelProvincia.Text = string.Empty;
        LabelComune.Text = string.Empty;
        LabelCap.Text = string.Empty;

        if (committente.FonteNotifica)
        {
            // Provincia
            if (committente.Provincia != null)
            {
                ListItem prov = DropDownListProvincia.Items.FindByText(committente.Provincia.ToUpper());
                if (prov != null)
                {
                    DropDownListProvincia.SelectedValue = prov.Value;
                    CaricaComuni(Int32.Parse(prov.Value));
                }
                else
                    LabelProvincia.Text = committente.Provincia;
            }

            // Comune
            if (committente.Comune != null)
            {
                ListItem com = DropDownListComuni.Items.FindByText(committente.Comune.ToUpper());
                if (com != null)
                {
                    DropDownListComuni.SelectedValue = com.Value;
                    CaricaCAP(Int64.Parse(com.Value));
                }
                else
                    LabelComune.Text = committente.Comune;
            }

            // Cap
            ListItem cap = DropDownListCAP.Items.FindByText(committente.Cap);
            if (cap != null)
            {
                DropDownListCAP.SelectedValue = cap.Value;
            }
            else
                LabelCap.Text = committente.Cap;
        }
        else
        {
            // Provincia            
            ListItem prov = DropDownListProvincia.Items.FindByText(committente.Provincia);
            if (prov != null)
            {
                DropDownListProvincia.SelectedValue = prov.Value;
                CaricaComuni(Int32.Parse(prov.Value));
            }

            // Comune
            ListItem com = DropDownListComuni.Items.FindByText(committente.Comune);
            if (com != null)
            {
                DropDownListComuni.SelectedValue = com.Value;
                CaricaCAP(Int64.Parse(com.Value));
            }

            // Cap
            ListItem cap = DropDownListCAP.Items.FindByText(committente.Cap);
            if (cap != null)
            {
                DropDownListCAP.SelectedValue = cap.Value;
            }
        }
    }

    public void Reset()
    {
        TextBoxIdCommittente.Text = string.Empty;
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxCodiceFiscale.Text = string.Empty;
        TextBoxPartitaIva.Text = string.Empty;
        TextBoxRagioneSociale.Text = string.Empty;
        TextBoxTelefono.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxPersonaRiferimento.Text = string.Empty;

        CaricaProvince();
        CaricaComuni(-1);
        CaricaCAP(-1);
    }

    /// <summary>
    /// Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        // CAMPI CONTROLLATI
        // Ragione sociale

        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            res = false;
            errori.Append("Campo ragione sociale vuoto" + Environment.NewLine);
        }
        if (string.IsNullOrEmpty(TextBoxIndirizzo.Text))
        {
            res = false;
            errori.Append("Campo indirizzo vuoto" + Environment.NewLine);
        }

        if (DropDownListCAP.SelectedValue == null || DropDownListCAP.SelectedValue == string.Empty)
        {
            res = false;
            errori.Append("CAP non selezionato" + Environment.NewLine);
        }

        if (!res)
            errore = errori.ToString();

        return res;
    }

    //protected Committente GetCommittente()
    //{
    //    if (ControlloCampiServer())
    //    {
    //        Committente committente = CreaCommittente();
    //        bool res = false; ;

    //        if (Request.QueryString["modalita"] == "modifica")
    //            aggiornamento = true;

    //        // Eseguo l'operazione
    //        if (aggiornamento)
    //        {
    //            int idCommittente = -1;
    //            Int32.TryParse(TextBoxIdCommittente.Text, out idCommittente);
    //            committente.IdCommittente = idCommittente;

    //            // Aggiorno il record
    //            res = biz.UpdateCommittente(committente);
    //        }
    //        else
    //        {
    //            // Inserisco il record
    //            biz.InsertCommittente(committente);
    //            if (committente.IdCommittente > 0)
    //            {
    //                res = true;
    //                // Ritorno alla pagina di insertmodificacantieri
    //            }
    //        }

    //        // Gestisco il risultato dell'operazione
    //        if (res)
    //        {
    //            // Tutto ok
    //            if (aggiornamento)
    //                LabelRisultato.Text = "Aggiornamento effettuato correttamente";
    //            else
    //                LabelRisultato.Text = "Inserimento effettuato correttamente";
    //        }
    //        else
    //        {
    //            LabelRisultato.Text = "Si � verificato un errore durante l'operazione";
    //        }
    //    }
    //}

    private Committente CreaCommittente()
    {
        // Creazione oggetto Committente
        int? idCommittente = null;
        string provincia = null;
        string comune = null;
        string cap = null;
        string indirizzo = null;
        string ragioneSociale = null;
        string partitaIva = null;
        string codiceFiscale = null;
        string telefono = null;
        string fax = null;
        string personaRiferimento = null;
        bool fonteNotifica = false;

        if (ViewState["IdCommittente"] != null)
        {
            idCommittente = (int?) ViewState["IdCommittente"];
        }

        if (ViewState["FonteNotifica"] != null)
        {
            fonteNotifica = (bool) ViewState["FonteNotifica"];
        }

        ragioneSociale = TextBoxRagioneSociale.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxIndirizzo.Text.Trim()))
            indirizzo = TextBoxIndirizzo.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()))
            partitaIva = TextBoxPartitaIva.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text.Trim()))
            codiceFiscale = TextBoxCodiceFiscale.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxTelefono.Text.Trim()))
            telefono = TextBoxTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxFax.Text.Trim()))
            fax = TextBoxFax.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxPersonaRiferimento.Text.Trim()))
            personaRiferimento = TextBoxPersonaRiferimento.Text.Trim().ToUpper();

        provincia = DropDownListProvincia.SelectedItem.Text;
        comune = DropDownListComuni.SelectedItem.Text;
        cap = DropDownListCAP.SelectedValue;

        Committente committente = new Committente(idCommittente, ragioneSociale, partitaIva, codiceFiscale,
                                                  indirizzo, comune, provincia, cap, telefono, fax, personaRiferimento);

        committente.FonteNotifica = fonteNotifica;

        return committente;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaProvince();
        }
    }

    public void SetValidationGroup(string validationGroup)
    {
        RegularExpressionValidator1.ValidationGroup = validationGroup;
        RegularExpressionValidator2.ValidationGroup = validationGroup;
        RequiredFieldValidator1.ValidationGroup = validationGroup;
        RequiredFieldValidatorIndirizzo.ValidationGroup = validationGroup;
    }
}