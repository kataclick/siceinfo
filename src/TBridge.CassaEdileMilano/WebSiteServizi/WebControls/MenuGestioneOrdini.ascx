<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuGestioneOrdini.ascx.cs" Inherits="WebControls_MenuGestioneOrdini" %>

 <%
     if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneOrdini.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati.ToString()))
    {
    %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Gestione Ordini</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneOrdini.ToString()))
    {
    %>
    <tr>
        <td>
            <a id="A1" href="TuteScarpeCreazioneOrdine.aspx">Crea nuovo ordine</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="TuteScarpeGestioneOrdini.aspx">Gestione ordini</a>
        </td>
    </tr>

    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati.ToString()))
    {
    %>
    <tr>
        <td>
            <a id="A3" href="TuteScarpeGestioneOrdiniConfermati.aspx">Gestione ordini confermati</a>
        </td>
    </tr>
    <%
    } 
    %>
</table>

    <%  
    } 
    %>