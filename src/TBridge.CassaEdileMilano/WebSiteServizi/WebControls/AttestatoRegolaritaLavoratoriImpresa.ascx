﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaLavoratoriImpresa.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaLavoratoriImpresa" %>
<table class="standardTable">
    <tr>
        <td>
            Impresa:
            <asp:DropDownList ID="DropDownListImprese" runat="server" Width="300px" AppendDataBoundItems="True"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownListImprese_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                Width="100%">
                <Columns>
                    <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                    <asp:BoundField DataField="Nome" HeaderText="Nome" />
                    <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CodiceFiscale" HeaderText="Cod. fisc.">
                        <ItemStyle Width="200px" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun lavoratore inserito per l&#39;impresa selezionata
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
