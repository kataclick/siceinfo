﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaRicerca.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaRicerca" %>
<asp:Panel ID="PanelRicercaImprese" runat="server" DefaultButton="ButtonVisualizza">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Indirizzo cantiere
                        </td>
                        <td>
                            Comune cantiere
                        </td>
                        <td>
                            Mese (mm/aaaa)
                            <asp:CustomValidator ID="CustomValidatorMese" runat="server" ControlToValidate="TextBoxMese"
                                ErrorMessage="*" OnServerValidate="CustomValidatorMese_ServerValidate" ValidationGroup="ricercaAttestati"></asp:CustomValidator>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxMese" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelPadding" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaAttestati"
                                OnClick="ButtonVisualizza_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewAttestati" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="IdDomanda" OnPageIndexChanging="GridViewAttestati_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewAttestati_SelectedIndexChanging" OnRowEditing="GridViewAttestati_RowEditing"
                    OnRowCommand="GridViewAttestati_RowCommand" OnRowDataBound="GridViewAttestati_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Cantiere">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Ind:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Com:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelComune" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Prov:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Rich.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRichiedente" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Mese" HeaderText="Mese" DataFormatString="{0:MM/yyyy}">
                            <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DataControllo" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data controllo"
                            HtmlEncode="False">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Modifica"
                            ShowEditButton="True">
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonVisualizza" runat="server" Text="Visualizza" CommandName="Visualizza"
                                    Width="100px" CommandArgument="<%# Container.DataItemIndex %>" />
                                <asp:Button ID="ButtonCopia" runat="server" Text="Copia" CommandName="Copia" Width="100px"
                                    CommandArgument="<%# Container.DataItemIndex %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="StampaAttestato"
                            Text="Attestato">
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
