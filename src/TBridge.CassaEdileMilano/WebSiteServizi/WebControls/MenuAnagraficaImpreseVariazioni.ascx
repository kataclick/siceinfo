﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuAnagraficaImpreseVariazioni.ascx.cs"
    Inherits="WebControls_MenuAnagraficaImpreseVariazioni" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Variazioni Impresa
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRichiestaVariazioneRecapiti))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/AnagraficaImprese/GestioneRecapiti.aspx" runat="server">Variazione
                Recapiti</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseGestioneStato))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/AnagraficaImprese/GestioneStato.aspx" runat="server">Variazione Stato</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseGestioneRichiesteCambioStato))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/AnagraficaImprese/GestioneRichiesteCambioStato.aspx" runat="server">
                Gestione Variazione Stato</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
