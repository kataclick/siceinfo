using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Enums;
using TBridge.Cemi.Prestazioni.Type.Filters;
using Telerik.Web.UI;
//using TBridge.Cemi.GestioneUtenti.Business.Identities;

public partial class WebControls_PrestazioniRicercaDomande : UserControl
{
    private const int INDICEINCARICO = 9;
    private const int INDICEVISUALIZZA = 8;
    private const Char GRUPPOSPLIT = '|';
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    private bool gestioneAmministrativa;

    protected void Page_Load(object sender, EventArgs e)
    {
        gestioneAmministrativa =
            GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin.ToString());

        if (!Page.IsPostBack)
        {
            CaricaStati();
            CaricaTipiPrestazione();

            if (Request.QueryString["menu"] == null)
            {
                Int32 pagina;
                DomandaFilter filtro = biz.GetFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), out pagina);

                if (filtro != null)
                {
                    ImpostaFiltro(filtro);

                    try
                    {
                        CaricaDomande(pagina);
                    }
                    catch
                    {
                        biz.DeleteFiltroRicerca(GestioneUtentiBiz.GetIdUtente());
                        throw;
                    }
                }
            }
        }
    }

    private void ImpostaFiltro(DomandaFilter filtro)
    {
        if (filtro.DataDomandaDal.HasValue)
        {
            TextBoxDataDomandaDal.Text = filtro.DataDomandaDal.Value.ToString("dd/MM/yyyy");
        }

        if (filtro.DataDomandaAl.HasValue)
        {
            TextBoxDataDomandaAl.Text = filtro.DataDomandaAl.Value.ToString("dd/MM/yyyy");
        }

        if (filtro.IdGruppo.HasValue)
        {
            String valSel = null;

            if (!String.IsNullOrEmpty(filtro.IdTipoPrestazione))
            {
                valSel = String.Format("{0}{1}{2}", filtro.IdGruppo, GRUPPOSPLIT, filtro.IdTipoPrestazione);
            }
            else
            {
                valSel = filtro.IdGruppo.ToString();
            }

            DropDownListTipoPrestazione.SelectedValue = valSel;
        }
        //if (!String.IsNullOrEmpty(filtro.IdTipoPrestazione))
        //{
        //    DropDownListTipoPrestazione.SelectedValue = filtro.IdTipoPrestazione;
        //}

        if (filtro.IdLavoratore.HasValue)
        {
            TextBoxIdLavoratore.Text = filtro.IdLavoratore.ToString();
        }

        if (!String.IsNullOrEmpty(filtro.CognomeLavoratore))
        {
            TextBoxCognomeLavoratore.Text = filtro.CognomeLavoratore;
        }

        if (!String.IsNullOrEmpty(filtro.NomeLavoratore))
        {
            TextBoxNomeLavoratore.Text = filtro.NomeLavoratore;
        }

        if (filtro.DataNascitaLavoratore.HasValue)
        {
            TextBoxDataNascitaLavoratore.Text = filtro.DataNascitaLavoratore.Value.ToString("dd/MM/yyyy");
        }

        if (filtro.Stato.HasValue)
        {
            DropDownListStato.SelectedValue = filtro.Stato.ToString();
        }

        if (filtro.IdUtente.HasValue)
        {
            if (filtro.IdUtente.Value > 0)
            {
                DropDownListInCarico.SelectedValue = "InCarico";
            }
            else
            {
                DropDownListInCarico.SelectedValue = "NonInCarico";
            }
        }

        if (filtro.TipologiaInserimento.HasValue)
        {
            DropDownListTipoInserimento.SelectedValue = filtro.TipologiaInserimento.ToString();
        }

        if (filtro.Anno.HasValue)
        {
            TextBoxAnno.Text = filtro.Anno.ToString();
        }

        if (filtro.Protocollo.HasValue)
        {
            TextBoxProtocollo.Text = filtro.Protocollo.ToString();
        }
    }

    private void CaricaTipiPrestazione()
    {
        //Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipoPrestazione,
        //                                                   biz.GetTipiPrestazione(), "Descrizione", "IdTipoPrestazione");

        DropDownListTipoPrestazione.Items.Clear();
        //DropDownListTipoPrestazione.Items.Add(new ListItem("", ""));
        DropDownListTipoPrestazione.Items.Add(new RadComboBoxItem("", ""));

        TipoPrestazioneCollection tipiPrestazione = biz.GetTipiPrestazione();

        Gruppo ultimoGruppo = null;
        foreach (TipoPrestazione tp in tipiPrestazione)
        {
            // Gestione gruppo
            if (ultimoGruppo == null || ultimoGruppo.Codice != tp.Gruppo.Codice)
            {
                //DropDownListTipoPrestazione.Items.Add(new ListItem(String.Format("-- {0}", tp.Gruppo.Descrizione.ToUpper()), tp.Gruppo.Codice.ToString()));
                RadComboBoxItem rcbiGruppo = new RadComboBoxItem(String.Format("-- {0}", tp.Gruppo.Descrizione), tp.Gruppo.Codice.ToString());
                rcbiGruppo.Font.Bold = true;
                rcbiGruppo.Font.Size = new FontUnit(12, UnitType.Pixel);
                DropDownListTipoPrestazione.Items.Add(rcbiGruppo);
                ultimoGruppo = tp.Gruppo;
            }

            // Tipo prestazione
            //DropDownListTipoPrestazione.Items.Add(new ListItem(tp.Descrizione.ToLower(), String.Format("{0}{1}{2}", tp.Gruppo.Codice.ToString(), GRUPPOSPLIT, tp.IdTipoPrestazione)));
            RadComboBoxItem rcbiElem = new RadComboBoxItem(tp.Descrizione, String.Format("{0}{1}{2}", tp.Gruppo.Codice.ToString(), GRUPPOSPLIT, tp.IdTipoPrestazione));
            rcbiElem.Font.Size = new FontUnit(12, UnitType.Pixel);
            DropDownListTipoPrestazione.Items.Add(rcbiElem);
        }
    }

    private void CaricaStati()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListStato,
                                                           biz.GetStatiDomanda(), "Descrizione", "IdStato");
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaDomande(0);
        }
    }

    private DomandaFilter CreaFiltro()
    {
        DomandaFilter filtro = new DomandaFilter();

        if (!string.IsNullOrEmpty(TextBoxIdLavoratore.Text))
            filtro.IdLavoratore = Int32.Parse(TextBoxIdLavoratore.Text);
        if (!string.IsNullOrEmpty(DropDownListStato.SelectedValue))
            filtro.Stato = DropDownListStato.SelectedValue[0];

        if (!string.IsNullOrEmpty(DropDownListTipoPrestazione.SelectedValue))
        {
            String selez = DropDownListTipoPrestazione.SelectedValue;
            String[] selezSplitted = selez.Split(GRUPPOSPLIT);

            if (selezSplitted.Length == 1)
            {
                filtro.IdGruppo = Int32.Parse(selezSplitted[0]);
            }
            else
            {
                filtro.IdGruppo = Int32.Parse(selezSplitted[0]);
                filtro.IdTipoPrestazione = selezSplitted[1];
            }
        }
        //if (!string.IsNullOrEmpty(DropDownListTipoPrestazione.SelectedValue))
        //{
        //    filtro.IdTipoPrestazione = DropDownListTipoPrestazione.SelectedValue;
        //}

        if (DropDownListInCarico.SelectedItem.Value == "InCarico")
        {
            //UtenteAbilitato utente = ((UtenteAbilitato) HttpContext.Current.User.Identity);
            //filtro.IdUtente = utente.IdUtente;
            filtro.IdUtente = GestioneUtentiBiz.GetIdUtente();
        }
        else if (DropDownListInCarico.SelectedItem.Value == "NonInCarico")
        {
            filtro.IdUtente = -1; //indica alla dal/SP di selezionare le domande non in carico
        }
        if (!string.IsNullOrEmpty(TextBoxAnno.Text))
            filtro.Anno = Int32.Parse(TextBoxAnno.Text);
        if (!string.IsNullOrEmpty(TextBoxProtocollo.Text))
            filtro.Protocollo = Int32.Parse(TextBoxProtocollo.Text);
        if (DropDownListTipoInserimento.SelectedValue == "Normale")
        {
            filtro.TipologiaInserimento = TipoInserimento.Normale;
        }
        else if (DropDownListTipoInserimento.SelectedValue == "Fast")
        {
            filtro.TipologiaInserimento = TipoInserimento.Fast;
        }

        filtro.CognomeLavoratore = TextBoxCognomeLavoratore.Text;
        filtro.NomeLavoratore = TextBoxNomeLavoratore.Text;
        if (!String.IsNullOrEmpty(TextBoxDataNascitaLavoratore.Text))
        {
            filtro.DataNascitaLavoratore = DateTime.Parse(TextBoxDataNascitaLavoratore.Text.Replace('.', '/'));
        }

        if (!String.IsNullOrEmpty(TextBoxDataDomandaDal.Text))
        {
            filtro.DataDomandaDal = DateTime.Parse(TextBoxDataDomandaDal.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxDataDomandaAl.Text))
        {
            filtro.DataDomandaAl = DateTime.Parse(TextBoxDataDomandaAl.Text);
        }

        return filtro;
    }

    private void CaricaDomande(Int32 pagina)
    {
        DomandaFilter filtro = CreaFiltro();
        biz.InsertFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), filtro, pagina);

        DomandaCollection domande = biz.GetDomande(filtro);
        Presenter.CaricaElementiInGridView(
            GridViewDomande, 
            domande,
            pagina);
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Domanda domanda = (Domanda) e.Row.DataItem;

            Label lTipoPrestazione = (Label) e.Row.FindControl("LabelTipoPrestazione");
            Label lProtocollo = (Label) e.Row.FindControl("LabelProtocollo");
            Label lIdLavoratore = (Label) e.Row.FindControl("LabelIdLavoratore");
            Label lCognome = (Label) e.Row.FindControl("LabelCognome");
            Label lNome = (Label) e.Row.FindControl("LabelNome");
            Label lStato = (Label) e.Row.FindControl("LabelStato");
            Button bAzione = (Button) e.Row.FindControl("ButtonAzione");

            lTipoPrestazione.Text = domanda.IdTipoPrestazione;
            lProtocollo.Text = domanda.ProtocolloPrestazioneCompleto;
            lIdLavoratore.Text = domanda.Lavoratore.IdLavoratore.ToString();
            lCognome.Text = domanda.Lavoratore.Cognome;
            lNome.Text = domanda.Lavoratore.Nome;
            lStato.Text = domanda.Stato.Descrizione;

            //UtenteAbilitato utente = ((UtenteAbilitato) HttpContext.Current.User.Identity);
            Utente utente = (Utente) Membership.GetUser();

            //se la domanda � modificabile...
            if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
            {
                if (!gestioneAmministrativa)
                {
                    //modifichiamo la label del tasto
                    if (!string.IsNullOrEmpty(domanda.LoginInCarico))
                    {
                        if (!domanda.IdUtenteInCarico.HasValue ||
                            (domanda.IdUtenteInCarico.HasValue && domanda.IdUtenteInCarico.Value == utente.IdUtente))
                            bAzione.Text = "Rilascia";
                    }

                    ///gestiamo la visualizzazione
                    //if (!domanda.IdUtenteInCarico.HasValue || (domanda.IdUtenteInCarico.HasValue && domanda.IdUtenteInCarico.Value != utente.IdUtente))
                    //{
                    //    e.Row.Cells[INDICEVISUALIZZA].Enabled = false;

                    //    if (domanda.IdUtenteInCarico.HasValue && domanda.IdUtenteInCarico.Value != utente.IdUtente)
                    //        e.Row.Cells[INDICEINCARICO].Enabled = false;
                    //}

                    //Cambiata la gestione. Tutti possono visualizzre. Ogni pagina ha dentro un controllo per gestire le azioni
                    e.Row.Cells[INDICEVISUALIZZA].Enabled = true;
                }
                else
                {
                    if (domanda.IdUtenteInCarico.HasValue)
                    {
                        bAzione.Text = "Rilascia";
                        e.Row.Cells[INDICEINCARICO].Enabled = true;
                    }
                    else
                    {
                        //dovrebbe mantenere lo stato true, perche l'admin deve comuque prendere in carico di modo che altri non la manipolino.
                        e.Row.Cells[INDICEINCARICO].Enabled = true;
                    }

                    e.Row.Cells[INDICEVISUALIZZA].Enabled = true;
                }
            }
            else
            {
                //se la domanda � R, D, L tutti possono vederla, nessuno la pu� prendere in carico tanto non pu� essere gestita
                e.Row.Cells[INDICEVISUALIZZA].Enabled = true;
                e.Row.Cells[INDICEINCARICO].Enabled = false;
            }
        }
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaDomande(e.NewPageIndex);
    }

    protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];

        Context.Items["IdDomanda"] = idDomanda;
        Server.Transfer("~/Prestazioni/ControlloDomanda.aspx");
    }

    protected void GridViewDomande_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.RowIndex].Values["IdDomanda"];
        int? idUtenteInCarico = (int?) GridViewDomande.DataKeys[e.RowIndex].Values["IdUtenteInCarico"];
        //UtenteAbilitato utente = ((UtenteAbilitato) HttpContext.Current.User.Identity);

        if (biz.UpdateDomandaSetPresaInCarico(idDomanda, GestioneUtentiBiz.GetIdUtente(), !idUtenteInCarico.HasValue))
        {
            CaricaDomande(GridViewDomande.PageIndex);
        }
    }
}