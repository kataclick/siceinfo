using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.Presenter;
using Indirizzo = TBridge.Cemi.Geocode.Type.Entities.Indirizzo;

public partial class WebControls_AccessoCantieriCantiere : UserControl
{
    //private const Int16 INDICEINDIRIZZOINSERITO = 0;
    //private const Int16 INDICEINDIRIZZOPROPOSTO = 1;
    //private readonly AccessoCantieriBusiness _accessoCantieriBiz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        SelezioneIndirizzoLiberoCantiere.OnIndirizzoSelected += SelezioneIndirizzoLiberoCantiere_OnIndirizzoSelected;

        if (!Page.IsPostBack)
        {
            if (ViewState["Indirizzi"] == null)
            {
                ViewState["Indirizzi"] = new IndirizzoCollection();
            }
        }
    }

    private void SelezioneIndirizzoLiberoCantiere_OnIndirizzoSelected(Indirizzo indirizzo)
    {
        // Converto nell'indirizzo di Accesso Cantieri
        TBridge.Cemi.AccessoCantieri.Type.Entities.Indirizzo indirizzoConv = new TBridge.Cemi.AccessoCantieri.Type.
            Entities.Indirizzo
                                                                                 {
                                                                                     Indirizzo1 = indirizzo.NomeVia,
                                                                                     Civico = indirizzo.Civico,
                                                                                     Cap = indirizzo.Cap,
                                                                                     Comune = indirizzo.Comune,
                                                                                     Provincia = indirizzo.Provincia,
                                                                                     Latitudine = indirizzo.Latitudine,
                                                                                     Longitudine = indirizzo.Longitudine
                                                                                 };

        AggiungiIndirizzoAllaLista(indirizzoConv);
    }

    public void CaricaDati(WhiteList domanda, Boolean copia)
    {
        if (domanda.DataInizio.HasValue)
            RadDateDataInizioLavori.SelectedDate = domanda.DataInizio.Value;
        if (domanda.DataFine.HasValue)
            RadDateDataFineLavori.SelectedDate = domanda.DataFine.Value;

        TextBoxDescrizione.Text = domanda.Descrizione;

        TextBoxAutorizzazioneAlSubappalto.Text = domanda.AutorizzazioneAlSubappalto;

        ViewState["Indirizzi"] = new IndirizzoCollection();
        TBridge.Cemi.AccessoCantieri.Type.Entities.Indirizzo indirizzo = new TBridge.Cemi.AccessoCantieri.Type.Entities.
            Indirizzo
                                                                             {
                                                                                 Indirizzo1 = domanda.Indirizzo,
                                                                                 Civico = domanda.Civico,
                                                                                 Provincia = domanda.Provincia,
                                                                                 InfoAggiuntiva = domanda.InfoAggiuntiva,
                                                                                 Comune = domanda.Comune,
                                                                                 Cap = domanda.Cap,
                                                                                 Latitudine = domanda.Latitudine,
                                                                                 Longitudine = domanda.Longitudine
                                                                             };

        // Converto nel formato del controllo
        Indirizzo indirizzoConv = new Indirizzo();
        indirizzoConv.NomeVia = indirizzo.Indirizzo1;
        indirizzoConv.Civico = indirizzo.Civico;
        indirizzoConv.Cap = indirizzo.Cap;
        indirizzoConv.Comune = indirizzo.Comune;
        indirizzoConv.Provincia = indirizzo.Provincia;
        indirizzoConv.Latitudine = indirizzo.Latitudine;
        indirizzoConv.Longitudine = indirizzo.Longitudine;
        SelezioneIndirizzoLiberoCantiere.CaricaDatiIndirizzo(indirizzoConv);

        AggiungiIndirizzoAllaLista(indirizzo);
    }

    private void AggiungiIndirizzoAllaLista(TBridge.Cemi.AccessoCantieri.Type.Entities.Indirizzo indirizzo)
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];

        indirizzi.Clear();

        indirizzi.Add(indirizzo);
        ViewState["Indirizzi"] = indirizzi;
    }

    public IndirizzoCollection GetIndirizzi()
    {
        IndirizzoCollection indirizzi = null;

        Page.Validate("ListaIndirizzi");

        if (Page.IsValid)
        {
            if (ViewState["Indirizzi"] != null)
            {
                indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];
            }
        }

        return indirizzi;
    }

    public string GetDescrizione()
    {
        return Presenter.NormalizzaCampoTesto(TextBoxDescrizione.Text);
    }

    public string GetAutorizzazioneAlSubappalto()
    {
        return Presenter.NormalizzaCampoTesto(TextBoxAutorizzazioneAlSubappalto.Text);
    }

    public DateTime? GetDataInizio()
    {
        return RadDateDataInizioLavori.SelectedDate;
    }

    public DateTime? GetDataFine()
    {
        return RadDateDataFineLavori.SelectedDate;
    }

    protected void CustomValidatorDataLavori_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDateDataInizioLavori.SelectedDate.HasValue &&
            RadDateDataFineLavori.SelectedDate.HasValue)
        {
            if (RadDateDataInizioLavori.SelectedDate >= RadDateDataFineLavori.SelectedDate)
            {
                args.IsValid = false;
            }
        }
    }
}