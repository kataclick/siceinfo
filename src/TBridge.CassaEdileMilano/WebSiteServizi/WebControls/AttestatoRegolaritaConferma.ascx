﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaConferma.ascx.cs" Inherits="WebControls_AttestatoRegolaritaConferma" %>
<%@ Register src="AttestatoRegolaritaDatiDomanda.ascx" tagname="AttestatoRegolaritaDatiDomanda" tagprefix="uc1" %>
<div>
    
    <asp:CheckBox ID="CheckBoxRichediVisitaIspettiva" runat="server" 
        Text="Visita di Verifica" AutoPostBack="True" 
        oncheckedchanged="CheckBoxRichediVisitaIspettiva_CheckedChanged" />
    <asp:Panel ID="PanelVisitaIspettiva" runat="server" Visible="false">
        <table class="borderedTable">
            <tr>
                <td colspan="3">
                    <b>
                        Visita di Verifica
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    Data (gg/mm/aaaa):
                </td>
                <td>
                    <asp:TextBox ID="TextBoxVisitaData" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator ID="RangeValidatorDataVisita" runat="server" 
                        ControlToValidate="TextBoxVisitaData" ErrorMessage="Formato data visita errato" 
                        MaximumValue="06/06/2079" MinimumValue="01/01/1900" ValidationGroup="stopConferma" 
                        Type="Date">*</asp:RangeValidator>
                    <asp:CustomValidator ID="CustomValidatorData" runat="server" 
                        ErrorMessage="Digitare una data per la visita" 
                        onservervalidate="CustomValidatorData_ServerValidate" 
                        ValidationGroup="stopConferma">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Orario preferenziale (hh:mm):
                </td>
                <td>
                    <asp:TextBox ID="TextBoxVisitaOrario" runat="server" Width="300px" MaxLength="5"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ErrorMessage="Formato orario errato" 
                        ValidationExpression="^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$" 
                        ValidationGroup="stopConferma" ControlToValidate="TextBoxVisitaOrario">*</asp:RegularExpressionValidator>
                    <asp:CustomValidator ID="CustomValidatorOrario" runat="server" 
                        ErrorMessage="Digitare un orario per la visita di verifica" 
                        onservervalidate="CustomValidatorOrario_ServerValidate" 
                        ValidationGroup="stopConferma">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Contatto telefonico:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxVisitaTelefono" runat="server" Width="300px" MaxLength="20"></asp:TextBox>
                </td>
                <td>
                    <asp:CustomValidator ID="CustomValidatorContatto" runat="server" 
                        ErrorMessage="Digitare un contatto telefonico" 
                        onservervalidate="CustomValidatorContatto_ServerValidate" 
                        ValidationGroup="stopConferma">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Motivo della richiesta:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxVisitaMotivo" runat="server" Width="300px" TextMode="MultiLine" Height="70px" MaxLength="255"></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" 
                    CssClass="messaggiErrore" ValidationGroup="stopConferma" />
                </td>
            <td></td>
            <td></td>
            </tr>
        </table>
    </asp:Panel>
</div>
<br />
<div>
    <uc1:AttestatoRegolaritaDatiDomanda ID="AttestatoRegolaritaDatiDomanda1" 
        runat="server" />
</div>