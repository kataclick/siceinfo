<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieRicercaCassaEdile.ascx.cs"
    Inherits="WebControls_ColonieRicercaCassaEdile" %>
<asp:Panel ID="PanelRicercaLavoratori" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca casse edili"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Codice
                        </td>
                        <td>
                            Descrizione
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="4" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDescrizione" runat="server" MaxLength="90" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewCasseEdili" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" Width="100%" DataKeyNames="IdCassaEdile" OnPageIndexChanging="GridViewCasseEdili_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewCasseEdili_SelectedIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="IdCassaEdile" HeaderText="Codice">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True">
                            <ItemStyle Width="50px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna cassa edile trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
