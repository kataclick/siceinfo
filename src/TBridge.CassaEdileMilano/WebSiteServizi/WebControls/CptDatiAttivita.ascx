<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptDatiAttivita.ascx.cs" Inherits="WebControls_CptDatiAttivita" %>
<table class="standardTable">
    <tr>
        <td>
            Ente:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListEnte" runat="server" Width="400px" AppendDataBoundItems="true"></asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDropDownListEnte" runat="server" ControlToValidate="DropDownListEnte" ErrorMessage="Selezionare un ente" ValidationGroup="visita">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data (gg/mm/aaaa):
        </td>
        <td>
            <asp:TextBox ID="TextBoxData" runat="server" Width="400px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTextBoxData" runat="server"
                ControlToValidate="TextBoxData" ErrorMessage="Immettere una data" ValidationGroup="visita">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorTextBoxData" runat="server" ControlToValidate="TextBoxData"
                ErrorMessage="Formato data non valido" Operator="DataTypeCheck" Type="Date" ValidationGroup="visita">*</asp:CompareValidator></td>
    </tr>
    <tr>
        <td>
            Tipologia visita:</td>
        <td>
            <asp:DropDownList ID="DropDownListTipologia" runat="server" Width="400px" AppendDataBoundItems="True"></asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDropDownListTipologia" runat="server" ControlToValidate="DropDownListTipologia" ErrorMessage="Selezionare una tipologia" ValidationGroup="visita">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Esito:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListEsito" runat="server" Width="400px" 
                AppendDataBoundItems="True" AutoPostBack="True" 
                onselectedindexchanged="DropDownListEsito_SelectedIndexChanged"></asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDropDownListEsito" runat="server" ControlToValidate="DropDownListEsito" ErrorMessage="Selezionare un esito" ValidationGroup="visita">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Grado irregolaritą
        </td>
        <td>
            <asp:DropDownList ID="DropDownListGradoIrregolarita" runat="server" 
                Width="400px" AppendDataBoundItems="True" Enabled="False"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                ValidationGroup="visita" />
            
        </td>
    </tr>
</table>
