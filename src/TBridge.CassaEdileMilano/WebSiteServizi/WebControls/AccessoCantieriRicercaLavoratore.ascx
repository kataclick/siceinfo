﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriRicercaLavoratore.ascx.cs"
    Inherits="WebControls_AccessoCantieriRicercaLavoratore" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openLavoratoreNuovo(idWhiteList, dataInizio, dataFine) {
            var oWindow = radopen("../AccessoCantieri/DatiAggiuntiviLavoratore.aspx?idWhiteList=" + idWhiteList + "&dataInizio=" + dataInizio + "&dataFine=" + dataFine, null);
            oWindow.set_title("Info Lavoratore");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 600);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openLavoratore(idWhiteList, idLavoratore, idImpresa, dataInizio, dataFine) {
            var oWindow = radopen("../AccessoCantieri/DatiAggiuntiviLavoratore.aspx?idWhiteList=" + idWhiteList + "&idLavoratore=" + idLavoratore + "&idImpresa=" + idImpresa + "&dataInizio=" + dataInizio + "&dataFine=" + dataFine, null);
            oWindow.set_title("Info Lavoratore");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 600);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />

<asp:Panel ID="PanelRicercaLavoratori" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca lavoratori"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Data di nascita (gg/mm/aaaa)
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                runat="server" ControlToValidate="TextBoxDataNascita" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                                ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="200"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="100" Width="200"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="255" Width="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice fiscale
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="255" Width="200"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaLavoratori" />
                            <asp:Button ID="ButtonNuovo" runat="server" OnClick="ButtonNuovo_Click" Text="Nuovo"
                                CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewLavoratori" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyNames="IdLavoratore" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewLavoratori_SelectedIndexChanging" OnSorting="GridViewLavoratori_Sorting"
                    Width="100%" PageSize="5" onrowdatabound="GridViewLavoratori_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                            HtmlEncode="False">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="ButtonSeleziona" runat="server" CausesValidation="False"
                                    CommandName="Select" Text="Seleziona" />
                            </ItemTemplate>
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun lavoratore trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
