using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Delegates;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Filters;

public partial class WebControls_PrestazioniRicercaLavoratore : UserControl
{
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    public event LavoratoreSelectedEventHandler OnLavoratoreSelected;
    public event LavoratoreNuovoEventHandler OnLavoratoreNuovo;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CercaLavoratori(0);
        }
    }

    private void CercaLavoratori(int pagina)
    {
        LavoratoreFilter filtro = CreaFiltro();

        Presenter.CaricaElementiInGridView(
            GridViewLavoratori,
            biz.GetLavoratoriSiceNew(filtro),
            pagina);
    }

    private LavoratoreFilter CreaFiltro()
    {
        LavoratoreFilter filtro = new LavoratoreFilter();

        if (!String.IsNullOrEmpty(TextBoxCodice.Text))
        {
            filtro.IdLavoratore = Int32.Parse(TextBoxCodice.Text);
        }
        filtro.Cognome = TextBoxCognome.Text;
        filtro.Nome = TextBoxNome.Text;
        if (!String.IsNullOrEmpty(TextBoxDataNascita.Text))
        {
            filtro.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
        }

        return filtro;
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        if (OnLavoratoreNuovo != null)
        {
            OnLavoratoreNuovo();
        }
    }

    //public void ForzaRicerca(String codiceFiscale)
    //{
    //    ResetRicerca();
    //    TextBoxCodiceFiscale.Text = codiceFiscale;
    //    GridViewLavoratori.Visible = true;
    //    CercaLavoratori(0);
    //}

    public void ResetRicerca()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxDataNascita);

        GridViewLavoratori.Visible = false;
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricercaLavoratori");

        if (Page.IsValid)
        {
            CercaLavoratori(e.NewPageIndex);
        }
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idLavoratore = (Int32) GridViewLavoratori.DataKeys[e.NewSelectedIndex].Values["IdLavoratore"];

        if (OnLavoratoreSelected != null)
        {
            Lavoratore lavoratore = biz.GetLavoratoreSiceNewByKey(idLavoratore);
            OnLavoratoreSelected(lavoratore);
        }
    }
}