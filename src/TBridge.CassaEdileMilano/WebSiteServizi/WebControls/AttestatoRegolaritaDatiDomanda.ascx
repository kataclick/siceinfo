﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaDatiDomanda.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaDatiDomanda" %>
<%@ Register Src="AttestatoRegolaritaLavoratoriImpresa.ascx" TagName="AttestatoRegolaritaLavoratoriImpresa"
    TagPrefix="uc1" %>
<%@ Register Src="AttestatoRegolaritaLavoratoriTutteImprese.ascx" TagName="AttestatoRegolaritaLavoratoriTutteImprese"
    TagPrefix="uc2" %>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Dati generali </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiDatiGenerali" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiDatiGenerali_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divDatiGenerali" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Periodo di osservazione </b>
            </td>
        </tr>
        <tr>
            <td>
                Mese/anno domanda:
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelMese" runat="server"></asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Cantiere </b>
            </td>
        </tr>
        <tr>
            <td>
                Data inizio:
            </td>
            <td>
                <asp:Label ID="LabelDataInizio" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Data fine:
            </td>
            <td>
                <asp:Label ID="LabelDataFine" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Importo dei lavori:
            </td>
            <td>
                <asp:Label ID="LabelImporto" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Descrizione lavori:
            </td>
            <td>
                <asp:Label ID="LabelDescrizione" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Percentuale incidenza mano d'opera:
            </td>
            <td>
                <asp:Label ID="LabelPercentualeManodopera" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Numero massimo lavoratori previsti:
            </td>
            <td>
                <asp:Label ID="LabelNumeroLavoratori" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Numero totale uomini giorno:
            </td>
            <td>
                <asp:Label ID="LabelNumeroGiorni" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Numero D.I.A.:
            </td>
            <td>
                <asp:Label ID="LabelDIA" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Numero concessione:
            </td>
            <td>
                <asp:Label ID="LabelConcessione" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Numero autorizzazione:
            </td>
            <td>
                <asp:Label ID="LabelAutorizzazione" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Appalto del cantiere </b>
            </td>
        </tr>
        <tr>
            <td>
                Indirizzo:
            </td>
            <td>
                <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Civico:
            </td>
            <td>
                <asp:Label ID="LabelCivico" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Comune:
            </td>
            <td>
                <asp:Label ID="LabelComune" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Provincia:
            </td>
            <td>
                <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Cap:
            </td>
            <td>
                <asp:Label ID="LabelCap" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Committente </b>
            </td>
        </tr>
        <tr>
            <td>
                Ragione sociale/denominazione:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteRagioneSociale" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Codice fiscale:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteCodFisc" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Partita IVA:
            </td>
            <td>
                <asp:Label ID="LabelCommittentePIVA" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Tipologia:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteTipologia" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Indirizzo:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteIndirizzo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Civico:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteCivico" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Provincia:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteProvincia" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Comune:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteComune" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                CAP:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteCAP" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Visita di Verifica </b>
            </td>
        </tr>
        <tr>
            <td>
                Richiesta:
            </td>
            <td>
                <asp:Label ID="LabelRichiestaVisita" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Data:
            </td>
            <td>
                <asp:Label ID="LabelDataVisita" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Orario:
            </td>
            <td>
                <asp:Label ID="LabelOrarioVisita" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Contatto telefonico:
            </td>
            <td>
                <asp:Label ID="LabelContattoTelefonicoVisita" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Motivo:
            </td>
            <td>
                <asp:Label ID="LabelMotivoVisita" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Imprese </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiAppalti" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiAppalti_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divAppalti" runat="server">
    <table class="borderedTable">
        <tr>
            <td>
                <b>Catena appalti </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewSubappalti" runat="server" AutoGenerateColumns="False"
                    Width="100%" AllowPaging="True" OnRowDataBound="GridViewSubappalti_RowDataBound"
                    PageSize="5" OnPageIndexChanging="GridViewSubappalti_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Subappaltata">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelAnteRagioneSociale" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAntePartitaIva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAnteCodiceFiscale" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAnteIndirizzo" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAnteArtigiano" runat="server" CssClass="campoPiccolo" ForeColor="Gray"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Appaltatore/impresa">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelAtaRagioneSociale" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaPartitaIva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaCodiceFiscale" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaIndirizzo" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaArtigiano" runat="server" CssClass="campoPiccolo" ForeColor="Gray"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:Panel ID="PanelNascosto" runat="server" Visible="false">
                    <asp:ListView ID="ListViewSubappalti" runat="server">
                        <LayoutTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <b>Impresa </b>
                                    </td>
                                    <td>
                                        <b>Appaltata da </b>
                                    </td>
                                </tr>
                                <asp:Panel ID="itemPlaceholder" runat="server" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <b>
                                        <%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).NomeAppaltata %>
                                    </b>
                                </td>
                                <td>
                                    <b>
                                        <%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).NomeAppaltatrice %>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td class="campoPiccolo">
                                    &nbsp;Cod fisc:
                                    <%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).CodiceFiscaleAppaltata %>
                                    <br />
                                    &nbsp;P. Iva:
                                    <%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).PartitaIvaAppaltata %>
                                </td>
                                <td class="campoPiccolo">
                                    &nbsp;Cod fisc:
                                    <%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).CodiceFiscaleAppaltatrice%>
                                    <br />
                                    &nbsp;P. Iva:
                                    <%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).PartitaIvaAppaltatrice%>
                                </td>
                            </tr>
                            <tr>
                                <td class="campoPiccolo">
                                    &nbsp;<%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).IndirizzoAppaltata %>
                                </td>
                                <td class="campoPiccolo">
                                    &nbsp;<%# ((TBridge.Cemi.AttestatoRegolarita.Type.Entities.Subappalto)Container.DataItem).IndirizzoAppaltatrice %>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    <asp:DataPager ID="DataPagerSubappalti" runat="server" PagedControlID="ListViewSubappalti"
                        PageSize="1">
                        <Fields>
                            <asp:NumericPagerField />
                        </Fields>
                    </asp:DataPager>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Lavoratori </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiLavoratori" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiLavoratori_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divLavoratori" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Lavoratori </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:AttestatoRegolaritaLavoratoriImpresa ID="AttestatoRegolaritaLavoratoriImpresa1"
                    runat="server" Visible="False" />
                <uc2:AttestatoRegolaritaLavoratoriTutteImprese ID="AttestatoRegolaritaLavoratoriTutteImprese1"
                    runat="server" />
                <br />
            </td>
        </tr>
    </table>
</div>
