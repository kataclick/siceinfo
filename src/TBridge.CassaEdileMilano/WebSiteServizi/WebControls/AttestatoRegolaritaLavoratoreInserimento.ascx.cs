﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;

public partial class WebControls_AttestatoRegolaritaLavoratoreInserimento : UserControl
{
    public Lavoratore Lavoratore
    {
        get
        {
            Page.Validate("lavoratore");
            Lavoratore lavoratore = null;

            if (Page.IsValid)
            {
                lavoratore = CreaLavoratore();
            }

            return lavoratore;
        }
    }

    public string Errore { get; set; }

    private Lavoratore CreaLavoratore()
    {
        Lavoratore lavoratore = new Lavoratore();

        lavoratore.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        lavoratore.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        lavoratore.DataNascita = DateTime.Parse(TextBoxDataNascita.Text);
        lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
        if (!String.IsNullOrEmpty(TextBoxDataAssunzione.Text))
            lavoratore.DataAssunzione = DateTime.Parse(TextBoxDataAssunzione.Text);
        if (!String.IsNullOrEmpty(TextBoxDataCessazione.Text))
            lavoratore.DataCessazione = DateTime.Parse(TextBoxDataCessazione.Text);
        //if (!String.IsNullOrEmpty(DropDownListCassaEdile.SelectedValue))
        //    lavoratore.IdCassaEdileProvenienza = DropDownListCassaEdile.SelectedValue;
        if (RadioButtonPartTimeNO.Checked)
            lavoratore.TipoAssunzione = 0;
        else lavoratore.TipoAssunzione = 1;

        return lavoratore;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //CaricaTipiAssunzione();
            //CaricaCasseEdili();
        }
    }

    //private void CaricaCasseEdili()
    //{
    //    Presenter.CaricaElementiInDropDownConElementoVuoto(
    //        DropDownListCassaEdile,
    //        commonBiz.GetCasseEdili(),
    //        "Descrizione",
    //        "IdCassaEdile");
    //}

    //private void CaricaTipiAssunzione()
    //{
    //    Presenter.CaricaElementiInDropDownConElementoVuoto(
    //        DropDownListTipoAssunzione,
    //        null,
    //        "",
    //        "");
    //}

    public void SetValidationGroup(string validationGroup)
    {
        RegularExpressionValidator1.ValidationGroup = validationGroup;
        RequiredFieldValidator1.ValidationGroup = validationGroup;
        RequiredFieldValidator2.ValidationGroup = validationGroup;
        RequiredFieldValidator3.ValidationGroup = validationGroup;
    }

    public void Reset()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxDataAssunzione);
        Presenter.SvuotaCampo(TextBoxDataCessazione);
        Presenter.SvuotaCampo(TextBoxDataNascita);
        Presenter.SvuotaCampo(TextBoxIdLavoratore);

        //CaricaTipiAssunzione();
        //CaricaCasseEdili();
    }

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (!String.IsNullOrEmpty(TextBoxDataNascita.Text) && (TextBoxCodiceFiscale.Text.Length == 16))
        {
            DateTime dataNascita;

            if (DateTime.TryParse(TextBoxDataNascita.Text, out dataNascita))
            {
                String codiceMaschile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "M", dataNascita, "A000");
                String codiceFemminile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "F", dataNascita,
                                                                                   "A000");

                if (codiceMaschile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                    &&
                    codiceFemminile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                    )
                {
                    args.IsValid = false;
                }
            }
        }
    }
}