﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class WebControls_AttestatoRegolaritaLavoratoriTutteImprese : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaImpreseLavoratori(Int32 idDomanda)
    {
        ViewState["IdDomanda"] = idDomanda;
        DomandaImpresaCollection impreseLavoratori = biz.GetLavoratoriInDomanda(idDomanda);
        CaricaImpreseLavoratori(impreseLavoratori);
    }

    public void CaricaImpreseLavoratori(DomandaImpresaCollection impreseLavoratori)
    {
        Presenter.CaricaElementiInGridView(
            GridViewImpreseLavoratori,
            impreseLavoratori,
            0);
    }

    protected void GridViewImpreseLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DomandaImpresa domandaImpresa = (DomandaImpresa) e.Row.DataItem;
            GridView gvLavoratori = (GridView) e.Row.FindControl("GridViewLavoratori");

            Presenter.CaricaElementiInGridView(
                gvLavoratori,
                domandaImpresa.Lavoratori,
                0);
        }
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }

    protected void GridViewImpreseLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Int32 idDomanda = (Int32) ViewState["IdDomanda"];

        DomandaImpresaCollection impreseLavoratori = biz.GetLavoratoriInDomanda(idDomanda);

        Presenter.CaricaElementiInGridView(
            GridViewImpreseLavoratori,
            impreseLavoratori,
            e.NewPageIndex);
    }
}