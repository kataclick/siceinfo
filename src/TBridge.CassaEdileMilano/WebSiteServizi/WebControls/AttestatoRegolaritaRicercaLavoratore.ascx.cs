﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Delegates;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;

#endregion

public partial class WebControls_AttestatoRegolaritaRicercaLavoratore : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();
    public event LavoratoriSelectedEventHandler OnLavoratoreSelected;
    public event EventHandler OnNuovoLavoratoreSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaLavoratori()
    {
        if (string.IsNullOrEmpty(TextBoxCognome.Text) && string.IsNullOrEmpty(TextBoxNome.Text) &&
            string.IsNullOrEmpty(TextBoxDataNascita.Text) && string.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
        {
            LabelErrore.Text = "Digitare un filtro";
        }
        else
        {
            LabelErrore.Text = string.Empty;
            string cognome = null;
            string nome = null;
            string codFisc = null;
            DateTime? dataNascita = null;
            DateTime dataNascitaD;
            //TipologiaLavoratore tipoLavoratore = TipologiaLavoratore.TutteLeFonti;

            Impresa impresaSelezionata = (Impresa) ViewState["impresaSelezionata"];

            int? idImpresa = impresaSelezionata.IdImpresa;

            string cogRic = TextBoxCognome.Text.Trim();
            if (!string.IsNullOrEmpty(cogRic))
                cognome = cogRic;

            string nomRic = TextBoxNome.Text.Trim();
            if (!string.IsNullOrEmpty(nomRic))
                nome = nomRic;

            if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
                DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
                dataNascita = dataNascitaD;

            string codFiscRic = TextBoxCodiceFiscale.Text.Trim();
            if (!string.IsNullOrEmpty(codFiscRic))
                codFisc = codFiscRic;

            //Valla: farlo con i filter
            LavoratoreCollection listaLavoratori =
                biz.GetLavoratoriOrdinati(null, cognome, nome, dataNascita, codFisc, null, null, null, null, idImpresa);

            if (listaLavoratori.Count > 0)
            {
                GridViewLavoratori.DataSource = listaLavoratori;
                GridViewLavoratori.DataBind();
                GridViewLavoratori.Visible = true;
            }
            else
            {
                GridViewLavoratori.Visible = false;
                LabelErrore.Text = "Nessun lavoratore trovato";
            }
        }
    }

    private void CaricaLavoratori(string sortExpression)
    {
        string cognome = null;
        string nome = null;
        string codFisc = null;
        DateTime? dataNascita = null;
        DateTime dataNascitaD;

        Impresa impresaSelezionata = (Impresa) ViewState["impresaSelezionata"];
        int? idImpresa = impresaSelezionata.IdImpresa;

        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            cognome = TextBoxCognome.Text;

        if (!string.IsNullOrEmpty(TextBoxNome.Text))
            nome = TextBoxNome.Text;

        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
            dataNascita = dataNascitaD;

        string codFiscRic = TextBoxCodiceFiscale.Text.Trim();
        if (!string.IsNullOrEmpty(codFiscRic))
            codFisc = codFiscRic;

        string direct = "ASC";
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');
            if (ord[0] == sortExpression && ord[1] == "ASC")
                direct = "DESC";
            else
                direct = "ASC";
        }
        ViewState["ordina"] = sortExpression + "|" + direct;

        LavoratoreCollection listaLavoratori =
            biz.GetLavoratoriOrdinati(null, cognome, nome, dataNascita, codFisc, null, null, sortExpression, direct,
                                      idImpresa);

        if (listaLavoratori.Count > 0)
        {
            GridViewLavoratori.DataSource = listaLavoratori;
            GridViewLavoratori.PageIndex = 0;
            GridViewLavoratori.DataBind();
            GridViewLavoratori.Visible = true;
        }
        else
        {
            GridViewLavoratori.Visible = false;
            LabelErrore.Text = "Nessun lavoratore trovato";
        }
    }

    private void CaricaLavoratoriPreservaOrdine(string sortExpression)
    {
        string cognome = null;
        string nome = null;
        string codFisc = null;
        DateTime? dataNascita = null;
        DateTime dataNascitaD;

        Impresa impresaSelezionata = (Impresa) ViewState["impresaSelezionata"];
        int? idImpresa = impresaSelezionata.IdImpresa;

        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            cognome = TextBoxCognome.Text;

        if (!string.IsNullOrEmpty(TextBoxNome.Text))
            nome = TextBoxNome.Text;

        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
            dataNascita = dataNascitaD;

        string codFiscRic = TextBoxCodiceFiscale.Text.Trim();
        if (!string.IsNullOrEmpty(codFiscRic))
            codFisc = codFiscRic;

        string direct = "ASC";
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');
            if (ord[0] == sortExpression && ord[1] == "ASC")
                direct = "ASC";
            else
                direct = "DESC";
        }
        ViewState["ordina"] = sortExpression + "|" + direct;

        LavoratoreCollection listaLavoratori =
            biz.GetLavoratoriOrdinati(null, cognome, nome, dataNascita, codFisc, null, null, sortExpression, direct,
                                      idImpresa);

        if (listaLavoratori.Count > 0)
        {
            GridViewLavoratori.DataSource = listaLavoratori;
            GridViewLavoratori.PageIndex = 0;
            GridViewLavoratori.DataBind();
            GridViewLavoratori.Visible = true;
        }
        else
        {
            GridViewLavoratori.Visible = false;
            LabelErrore.Text = "Nessun lavoratore trovato";
        }
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idLavoratore = (int) GridViewLavoratori.DataKeys[e.NewSelectedIndex]["IdLavoratore"];

        Impresa impresaSelezionata = (Impresa) ViewState["impresaSelezionata"];
        int? idImpresa = impresaSelezionata.IdImpresa;

        Lavoratore lavoratore =
            biz.GetLavoratoriOrdinati(idLavoratore, null, null, null, null, null, null, null, null, idImpresa)[0];

        if (OnLavoratoreSelected != null)
        {
            OnLavoratoreSelected(lavoratore);
            Reset();
        }
    }

    protected void Reset()
    {
        TextBoxCognome.Text = string.Empty;
        TextBoxNome.Text = string.Empty;
        TextBoxDataNascita.Text = string.Empty;
        TextBoxCodiceFiscale.Text = string.Empty;

        GridViewLavoratori.DataSource = null;
        GridViewLavoratori.DataBind();
        GridViewLavoratori.Visible = false;
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaLavoratori();
        }
    }

    public void SetImpresaSelezionata(Impresa impresa)
    {
        ViewState["impresaSelezionata"] = impresa;
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');

            if (ord[1] == "DESC")
                CaricaLavoratoriPreservaOrdine(ord[0]);
            else
                CaricaLavoratoriPreservaOrdine(ord[0]);
        }
        else
            CaricaLavoratori();

        GridViewLavoratori.PageIndex = e.NewPageIndex;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_Sorting(object sender, GridViewSortEventArgs e)
    {
        //SortDirection dir = SortDirection.Descending;
        //if (e.SortDirection == SortDirection.Descending)
        //    dir = SortDirection.Ascending;

        CaricaLavoratori(e.SortExpression);
    }

    protected void ButtonChiudi_Click(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        if (OnNuovoLavoratoreSelected != null)
            OnNuovoLavoratoreSelected(this, null);
        //Response.Redirect("CantieriInserimentoModificaLavoratore.aspx");
    }
}