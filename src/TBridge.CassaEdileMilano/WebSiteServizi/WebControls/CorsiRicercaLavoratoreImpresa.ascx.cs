using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.Presenter;

public partial class WebControls_CorsiRicercaLavoratoreImpresa : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CercaIscrizioni(0);
        }
    }

    private void CercaIscrizioni(int pagina)
    {
        PartecipazioneLavoratoreImpresaFilter filtro = CreaFiltro();

        Presenter.CaricaElementiInGridView(
            GridViewPartecipazioni,
            biz.GetPartecipazioniByLavoratoreImpresa(filtro),
            pagina);
    }

    private PartecipazioneLavoratoreImpresaFilter CreaFiltro()
    {
        PartecipazioneLavoratoreImpresaFilter filtro = new PartecipazioneLavoratoreImpresaFilter();

        filtro.LavoratoreCognome = TextBoxCognome.Text;
        filtro.LavoratoreNome = TextBoxNome.Text;
        filtro.LavoratoreCodiceFiscale = TextBoxCodiceFiscale.Text;

        if (!String.IsNullOrEmpty(TextBoxCodice.Text))
        {
            filtro.IdImpresa = Int32.Parse(TextBoxCodice.Text);
        }
        filtro.ImpresaRagioneSociale = TextBoxRagioneSociale.Text;
        filtro.ImpresaIvaFisc = TextBoxIvaFisc.Text;

        return filtro;
    }

    protected void GridViewPartecipazioni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PartecipazioneModulo part = (PartecipazioneModulo) e.Row.DataItem;

            Label lLavoratore = (Label) e.Row.FindControl("LabelLavoratore");
            Label lImpresa = (Label) e.Row.FindControl("LabelImpresa");
            Label lCorso = (Label) e.Row.FindControl("LabelCorso");
            Label lModulo = (Label) e.Row.FindControl("LabelModulo");
            Label lSchedulazione = (Label) e.Row.FindControl("LabelSchedulazione");
            Label lLocazione = (Label) e.Row.FindControl("LabelLocazione");
            CheckBox cbPresente = (CheckBox) e.Row.FindControl("CheckBoxPresente");
            CheckBox cbAttestato = (CheckBox) e.Row.FindControl("CheckBoxAttestato");

            lLavoratore.Text = part.Lavoratore.CognomeNome;
            lImpresa.Text = part.Impresa.CodiceERagioneSociale;

            lCorso.Text = part.Programmazione.Corso.Descrizione;
            lModulo.Text = part.Programmazione.Modulo.Descrizione;
            lSchedulazione.Text = part.Programmazione.Schedulazione.Value.ToShortDateString();
            lLocazione.Text = part.Programmazione.Locazione.IndirizzoCompleto;

            cbPresente.Checked = part.Lavoratore.Presente;
            cbAttestato.Checked = part.Lavoratore.Attestato;
        }
    }

    protected void GridViewPartecipazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CercaIscrizioni(e.NewPageIndex);
    }

    protected void GridViewPartecipazioni_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        DateTime? dataPianificazione =
            (DateTime?) GridViewPartecipazioni.DataKeys[e.NewSelectedIndex].Values["DataPianificazione"];
        Int32? idCorso = (Int32?) GridViewPartecipazioni.DataKeys[e.NewSelectedIndex].Values["IdCorso"];
        Int32? idLocazione = (Int32?) GridViewPartecipazioni.DataKeys[e.NewSelectedIndex].Values["IdLocazione"];

        Response.Redirect(String.Format("~/Corsi/GestionePartecipantiCorsi.aspx?dataPianificazione={0}&idCorso={1}&idLocazione={2}",
                          dataPianificazione,
                          idCorso,
                          idLocazione));
    }
}