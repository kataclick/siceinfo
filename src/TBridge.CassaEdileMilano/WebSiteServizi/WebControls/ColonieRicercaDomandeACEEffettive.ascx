﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieRicercaDomandeACEEffettive.ascx.cs"
    Inherits="WebControls_ColonieRicercaDomandeACEEffettive" %>
<table class="standardTable">
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Cognome
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="100" Width="180"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                            Text="Ricerca" Width="175px" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        &nbsp; &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" DataKeyNames="IdDomanda"
                Width="100%" OnRowDataBound="GridViewDomande_RowDataBound" AllowPaging="True"
                OnPageIndexChanging="GridViewDomande_PageIndexChanging" 
                ondatabinding="GridViewDomande_DataBinding">
                <Columns>
                    <asp:BoundField HeaderText="Cognome" DataField="Cognome" />
                    <asp:BoundField HeaderText="Nome" DataField="Nome" />
                    <asp:BoundField HeaderText="Data di nascita" DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Turni">
                        <ItemTemplate>
                            <asp:GridView ID="GridViewTurni" runat="server" DataKeyNames="IdTurno" Width="150px"
                                AutoGenerateColumns="False" OnRowDataBound="GridViewTurni_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Turno">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelTurno" runat="server" Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Presente">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPresente" runat="server" Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Data annull.">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRichiestaAnnullamento" runat="server" Width="70"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rientro antic.">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelRientroAnticipato" runat="server" Width="70"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Partenza postic.">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelPartenzaPosticipata" runat="server" Width="70"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    Nessun dato presente
                                </EmptyDataTemplate>
                                <EditRowStyle Height="10px" />
                                <RowStyle Height="10px" />
                                <AlternatingRowStyle Height="10px" />
                            </asp:GridView>
                        </ItemTemplate>
                        <ItemStyle Width="150px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna partecipante trovato
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
