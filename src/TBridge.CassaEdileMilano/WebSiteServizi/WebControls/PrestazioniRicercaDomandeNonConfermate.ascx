<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniRicercaDomandeNonConfermate.ascx.cs"
    Inherits="WebControls_PrestazioniRicercaDomandeNonConfermate" %>
<asp:Panel ID="PanelRicercaDomande" runat="server">
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco domande"></asp:Label><br />
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" DataKeyNames="IdDomanda" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                    OnRowDataBound="GridViewDomande_RowDataBound" OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging"
                    OnRowDeleting="GridViewDomande_RowDeleting">
                    <EmptyDataTemplate>
                        Nessuna domanda trovata
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="DataConferma" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data compilazione">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DescrizioneTipoPrestazione" HeaderText="Tipo prest.">
                            <ItemStyle Width="200px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Beneficiario">
                            <ItemTemplate>
                                <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" ShowSelectButton="True"
                            SelectText="Seleziona">
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina"
                            ShowDeleteButton="True">
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
