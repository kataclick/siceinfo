﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriDati.ascx.cs"
    Inherits="WebControls_AccessoCantieriDati" %>
<%@ Import Namespace="TBridge.Cemi.AccessoCantieri.Type.Entities" %>
<%@ Register Src="AccessoCantieriLavoratoriImpresa.ascx" TagName="AccessoCantieriLavoratoriImpresa"
    TagPrefix="uc1" %>
<%@ Register Src="AccessoCantieriLavoratoriTutteImprese.ascx" TagName="AccessoCantieriLavoratoriTutteImprese"
    TagPrefix="uc2" %>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Dati generali </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiDatiGenerali" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiDatiGenerali_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divDatiGenerali" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Cantiere </b>
            </td>
        </tr>
        <tr>
            <td>
                Data inizio:
            </td>
            <td>
                <asp:Label ID="LabelDataInizio" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Data fine:
            </td>
            <td>
                <asp:Label ID="LabelDataFine" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Descrizione lavori:
            </td>
            <td>
                <asp:Label ID="LabelDescrizione" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Appalto del cantiere </b>
            </td>
        </tr>
        <tr>
            <td>
                Indirizzo:
            </td>
            <td>
                <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Civico:
            </td>
            <td>
                <asp:Label ID="LabelCivico" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Comune:
            </td>
            <td>
                <asp:Label ID="LabelComune" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Provincia:
            </td>
            <td>
                <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Cap:
            </td>
            <td>
                <asp:Label ID="LabelCap" runat="server"></asp:Label>
            </td>
        </tr>
       <%-- <tr>
            <td colspan="2">
                <b>Committente </b>
            </td>
        </tr>
        <tr>
            <td>
                Ragione sociale/denominazione:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteRagioneSociale" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Codice fiscale:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteCodFisc" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Partita IVA:
            </td>
            <td>
                <asp:Label ID="LabelCommittentePIVA" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Tipologia:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteTipologia" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Indirizzo:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteIndirizzo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Civico:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteCivico" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Provincia:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteProvincia" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Comune:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteComune" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                CAP:
            </td>
            <td>
                <asp:Label ID="LabelCommittenteCAP" runat="server"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Referenti </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiReferenti" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiReferenti_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divReferenti" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Referenti </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewReferenti" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewReferenti_RowDataBound"
                    Width="100%" DataKeyNames="IdReferente">
                    <Columns>
                        <asp:TemplateField HeaderText="Referente">
                            <ItemTemplate>
                                <asp:Label ID="LabelReferente" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="codiceFiscale" HeaderText="Codice fiscale" />
                        <asp:TemplateField HeaderText="Contatto">
                            <ItemTemplate>
                                <asp:Label ID="LabelContatto" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ruolo">
                            <ItemTemplate>
                                <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun referente presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Imprese </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiAppalti" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiAppalti_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divAppalti" runat="server">
    <table class="borderedTable">
        <tr>
            <td>
                <b>Catena appalti </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewSubappalti" runat="server" AutoGenerateColumns="False"
                    Width="100%" AllowPaging="True" OnRowDataBound="GridViewSubappalti_RowDataBound"
                    PageSize="5" OnPageIndexChanging="GridViewSubappalti_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Subappaltata">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelAnteRagioneSociale" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAntePartitaIva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAnteCodiceFiscale" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAnteIndirizzo" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAnteArtigiano" runat="server" CssClass="campoPiccolo" ForeColor="Gray"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Appaltatore/impresa">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelAtaRagioneSociale" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaPartitaIva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaCodiceFiscale" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaIndirizzo" runat="server" CssClass="campoPiccolo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="campoPiccolo">
                                            <asp:Label ID="LabelAtaArtigiano" runat="server" CssClass="campoPiccolo" ForeColor="Gray"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="50%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:Panel ID="PanelNascosto" runat="server" Visible="false">
                    <asp:ListView ID="ListViewSubappalti" runat="server">
                        <LayoutTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <b>Impresa </b>
                                    </td>
                                    <td>
                                        <b>Appaltata da </b>
                                    </td>
                                </tr>
                                <asp:Panel ID="itemPlaceholder" runat="server" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <b>
                                        <%#((Subappalto) Container.DataItem).NomeAppaltata%>
                                    </b>
                                </td>
                                <td>
                                    <b>
                                        <%#((Subappalto) Container.DataItem).NomeAppaltatrice%>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td class="campoPiccolo">
                                    &nbsp;Cod fisc:
                                    <%#((Subappalto) Container.DataItem).CodiceFiscaleAppaltata%>
                                    <br />
                                    &nbsp;P. Iva:
                                    <%#((Subappalto) Container.DataItem).PartitaIvaAppaltata%>
                                </td>
                                <td class="campoPiccolo">
                                    &nbsp;Cod fisc:
                                    <%#((Subappalto) Container.DataItem).CodiceFiscaleAppaltatrice%>
                                    <br />
                                    &nbsp;P. Iva:
                                    <%#((Subappalto) Container.DataItem).PartitaIvaAppaltatrice%>
                                </td>
                            </tr>
                            <tr>
                                <td class="campoPiccolo">
                                    &nbsp;<%#((Subappalto) Container.DataItem).IndirizzoAppaltata%>
                                </td>
                                <td class="campoPiccolo">
                                    &nbsp;<%#((Subappalto) Container.DataItem).IndirizzoAppaltatrice%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    <asp:DataPager ID="DataPagerSubappalti" runat="server" PagedControlID="ListViewSubappalti"
                        PageSize="1">
                        <Fields>
                            <asp:NumericPagerField />
                        </Fields>
                    </asp:DataPager>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr />
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Imprese Periodi </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiImpresePeriodi" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiImpresePeriodi_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divImpresePeriodi" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Imprese periodi </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewImpresePeriodi" runat="server" AutoGenerateColumns="False"
                    OnRowDataBound="GridViewImpresePeriodi_RowDataBound" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Impresa">
                            <ItemTemplate>
                                <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Periodo">
                            <ItemTemplate>
                                <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna impresa presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Lavoratori </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiLavoratori" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiLavoratori_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divLavoratori" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Lavoratori </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:AccessoCantieriLavoratoriImpresa ID="AccessoCantieriLavoratoriImpresa1" runat="server"
                    Visible="False" />
                <uc2:AccessoCantieriLavoratoriTutteImprese ID="AccessoCantieriLavoratoriTutteImprese1"
                    runat="server" />
                <br />
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Altre Persone abilitate all'accesso </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiAltrePersone" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiAltrePersone_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divAltrePersone" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Altre persone abilitate all'accesso </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewAltrePersone" runat="server" AutoGenerateColumns="False"
                    OnRowDataBound="GridViewAltrePersone_RowDataBound" Width="100%" DataKeyNames="IdAltraPersona">
                    <Columns>
                        <asp:TemplateField HeaderText="AltraPersona">
                            <ItemTemplate>
                                <asp:Label ID="LabelAltraPersona" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="codiceFiscale" HeaderText="Codice fiscale" />
                        <asp:TemplateField HeaderText="Ruolo">
                            <ItemTemplate>
                                <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna altra persona presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
            </td>
        </tr>
    </table>
</div>
<table class="standardTable">
    <tr>
        <td class="colonnaTabellaIntestazioneTesto" width="30%">
            <b>Rilevatori di presenza </b>
        </td>
        <td class="colonnaTabellaIntestazioneBottone">
            <asp:Button ID="ButtonMostraNascondiRilevatori" runat="server" Text="" Width="150px"
                OnClick="ButtonMostraNascondiRilevatori_Click" />
        </td>
    </tr>
</table>
<br />
<div id="divRilevatori" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Rilevatori</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewRilevatori" runat="server" AutoGenerateColumns="False"
                    OnRowDataBound="GridViewRilevatori_RowDataBound" Width="100%" DataKeyNames="IdRilevatore">
                    <Columns>
                        <asp:TemplateField HeaderText="Rilevatore">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelCodice0" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelSocieta0" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelCantiere0" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelReferente0" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelPeriodo0" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna rilevatore presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
            </td>
        </tr>
    </table>
</div>
