using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;

public partial class WebControls_CptDatiAttivita : UserControl
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieVisita();
            CaricaEsitiVisita();
            CaricaEnti();
            CaricaGradiIrregolarita();

            PreSelezionaEnte();
        }
    }

    private void PreSelezionaEnte()
    {
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaASLInserimento.ToString()))
        {
            DropDownListEnte.SelectedValue = "ASL";
            DropDownListEnte.Enabled = false;
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaCPTInserimento.ToString()))
        {
            DropDownListEnte.SelectedValue = "CPT";
            DropDownListEnte.Enabled = false;
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaDPLInserimento.ToString()))
        {
            DropDownListEnte.SelectedValue = "DPL";
            DropDownListEnte.Enabled = false;
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaASLERSLTInserimento.ToString()))
        {
            DropDownListEnte.SelectedValue = "ASLERSLT";
            DropDownListEnte.Enabled = false;
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaCassaEdileInserimento.ToString()))
        {
            DropDownListEnte.SelectedValue = "CassaEdile";
            DropDownListEnte.Enabled = false;
        }
    }

    private void CaricaEnti()
    {
        DropDownListEnte.Items.Clear();
        DropDownListEnte.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListEnte.DataSource = Enum.GetNames(typeof (EnteVisita));
        DropDownListEnte.DataBind();
    }

    private void CaricaEsitiVisita()
    {
        DropDownListEsito.Items.Clear();
        DropDownListEsito.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListEsito.DataSource = biz.GetEsitiVisita();
        DropDownListEsito.DataTextField = "Descrizione";
        DropDownListEsito.DataValueField = "IdEsitoVisita";
        DropDownListEsito.DataBind();
    }

    private void CaricaTipologieVisita()
    {
        DropDownListTipologia.Items.Clear();
        DropDownListTipologia.Items.Add(new ListItem(string.Empty, string.Empty));

        DropDownListTipologia.DataSource = biz.GetTipologieVisita();
        DropDownListTipologia.DataTextField = "Descrizione";
        DropDownListTipologia.DataValueField = "IdTipologiaVisita";
        DropDownListTipologia.DataBind();
    }

    private void CaricaGradiIrregolarita()
    {
        GradoIrregolaritaCollection gradi = biz.GetGradiIrregolarita();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListGradoIrregolarita,
            gradi,
            "Descrizione",
            "IdGradoIrregolarita");
    }

    public void BloccaCampi()
    {
        TextBoxData.Enabled = false;
        DropDownListTipologia.Enabled = false;
        DropDownListEsito.Enabled = false;
        DropDownListEnte.Enabled = false;
    }

    public Visita CreaVisita()
    {
        //int idUtente = ((TBridge.Cemi.GestioneUtenti.Business.Identities.UtenteAbilitato)
        //    HttpContext.Current.User.Identity).IdUtente;
        int idUtente = GestioneUtentiBiz.GetIdUtente();

        Visita visita = new Visita();

        visita.Ente = (EnteVisita) Enum.Parse(typeof (EnteVisita), DropDownListEnte.SelectedValue);
        visita.Data = DateTime.Parse(TextBoxData.Text.Replace('.', '/'));
        visita.Tipologia = new TipologiaVisita();
        visita.Tipologia.IdTipologiaVisita = Int32.Parse(DropDownListTipologia.SelectedItem.Value);
        visita.Esito = new EsitoVisita();
        visita.Esito.IdEsitoVisita = Int32.Parse(DropDownListEsito.SelectedItem.Value);
        visita.IdUtente = idUtente;
        visita.IdNotifica = (int) ViewState["IdNotifica"];

        if (!string.IsNullOrEmpty(DropDownListGradoIrregolarita.SelectedItem.Value))
        {
            visita.GradoIrregolarita = new GradoIrregolarita();
            visita.GradoIrregolarita.IdGradoIrregolarita = Int16.Parse(DropDownListGradoIrregolarita.SelectedItem.Value);
        }

        return visita;
    }

    public void ImpostaIdNotifica(int idNotifica)
    {
        ViewState["IdNotifica"] = idNotifica;
    }

    public void CaricaVisita(Visita visita)
    {
        DropDownListEnte.SelectedValue = visita.Ente.ToString();
        TextBoxData.Text = visita.Data.ToShortDateString();
        DropDownListTipologia.SelectedValue = visita.Tipologia.IdTipologiaVisita.ToString();
        DropDownListEsito.SelectedValue = visita.Esito.IdEsitoVisita.ToString();

        if (visita.Esito.Descrizione == "Irregolare")
        {
            DropDownListGradoIrregolarita.Enabled = true;
        }
        else
        {
            DropDownListGradoIrregolarita.Enabled = false;
        }

        if (visita.GradoIrregolarita != null)
        {
            DropDownListGradoIrregolarita.SelectedValue = visita.GradoIrregolarita.IdGradoIrregolarita.ToString();
        }

        ImpostaIdNotifica(visita.IdNotifica);
    }

    protected void DropDownListEsito_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownListGradoIrregolarita.SelectedIndex = 0;

        if (DropDownListEsito.SelectedItem != null)
        {
            if (DropDownListEsito.SelectedItem.Text == "Irregolare")
            {
                DropDownListGradoIrregolarita.Enabled = true;
            }
            else
            {
                DropDownListGradoIrregolarita.Enabled = false;
            }
        }
    }
}