using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Delegates;
using TBridge.Cemi.Cantieri.Type.Entities;

public partial class WebControls_CantieriRicercaCommittente : UserControl
{
    private const int INDICECODICE = 1;
    private const int INDICEFONTE = 0;

    private readonly CantieriBusiness biz = new CantieriBusiness();

    private string committenteTrovato;
    private bool modalitaNotifiche;

    public string CommittenteTrovato
    {
        get { return committenteTrovato; }
        set
        {
            committenteTrovato = value;
            TextBoxRagioneSociale.Text = "%" + committenteTrovato;
        }
    }

    public string RagioneSociale
    {
        get { return TextBoxRagioneSociale.Text; }
    }

    public event EventHandler OnNuovoCommittenteSelected;
    public event CommittentiSelectedEventHandler OnCommittenteSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        //ButtonNuovoCommittente.Attributes.Add("onclick", "window.open('CantieriInserimentoModificaCommittente.aspx', '', 'menubar=no,height=400,width=650'); return false;");
    }

    public void ModalitaNotifiche()
    {
        ButtonChiudi.Visible = false;
        modalitaNotifiche = true;
        ButtonNuovoCommittente.Visible = true;
        GridViewCommittenti.Columns[INDICECODICE].Visible = true;
        GridViewCommittenti.Columns[INDICEFONTE].Visible = false;
    }

    public void Reset()
    {
        GridViewCommittenti.DataSource = null;
        GridViewCommittenti.DataBind();

        TextBoxRagioneSociale.Text = string.Empty;
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxComune.Text = string.Empty;
        TextBoxIvaFiscale.Text = string.Empty;
    }

    public void CaricaCommittenti()
    {
        string ragioneSociale = null;
        string comune = null;
        string indirizzo = null;
        string ivaCodFisc = null;

        string ragSocRic = TextBoxRagioneSociale.Text.Trim();
        if (!string.IsNullOrEmpty(ragSocRic))
            ragioneSociale = ragSocRic;

        string comRic = TextBoxComune.Text.Trim();
        if (!string.IsNullOrEmpty(comRic))
            comune = comRic;

        string indRic = TextBoxIndirizzo.Text.Trim();
        if (!string.IsNullOrEmpty(indRic))
            indirizzo = indRic;

        string ivaRic = TextBoxIvaFiscale.Text.Trim();
        if (!string.IsNullOrEmpty(ivaRic))
            ivaCodFisc = ivaRic;

        CommittenteCollection listaCommittenti =
            biz.GetCommittentiOrdinati(null, ragioneSociale, comune, indirizzo, ivaCodFisc, null, null,
                                       modalitaNotifiche);
        GridViewCommittenti.DataSource = listaCommittenti;
        GridViewCommittenti.DataBind();
    }

    private void CaricaCommittenti(string sortExpression)
    {
        string ragioneSociale = null;
        string comune = null;
        string indirizzo = null;
        string ivaCodFisc = null;

        string ragSocRic = TextBoxRagioneSociale.Text.Trim();
        if (!string.IsNullOrEmpty(ragSocRic))
            ragioneSociale = ragSocRic;

        string comRic = TextBoxComune.Text.Trim();
        if (!string.IsNullOrEmpty(comRic))
            comune = comRic;

        string indRic = TextBoxIndirizzo.Text.Trim();
        if (!string.IsNullOrEmpty(indRic))
            indirizzo = indRic;

        string ivaRic = TextBoxIvaFiscale.Text.Trim();
        if (!string.IsNullOrEmpty(ivaRic))
            ivaCodFisc = ivaRic;

        string direct = "ASC";
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');
            if (ord[0] == sortExpression && ord[1] == "ASC")
                direct = "DESC";
            else
                direct = "ASC";
        }
        ViewState["ordina"] = sortExpression + "|" + direct;

        CommittenteCollection listaCommittenti =
            biz.GetCommittentiOrdinati(null, ragioneSociale, comune, indirizzo, ivaCodFisc, sortExpression, direct,
                                       modalitaNotifiche);
        GridViewCommittenti.DataSource = listaCommittenti;
        GridViewCommittenti.PageIndex = 0;
        GridViewCommittenti.DataBind();
    }

    private void CaricaCommittentiPreservaOrdine(string sortExpression)
    {
        string ragioneSociale = null;
        string comune = null;
        string indirizzo = null;
        string ivaCodFisc = null;

        string ragSocRic = TextBoxRagioneSociale.Text.Trim();
        if (!string.IsNullOrEmpty(ragSocRic))
            ragioneSociale = ragSocRic;

        string comRic = TextBoxComune.Text.Trim();
        if (!string.IsNullOrEmpty(comRic))
            comune = comRic;

        string indRic = TextBoxIndirizzo.Text.Trim();
        if (!string.IsNullOrEmpty(indRic))
            indirizzo = indRic;

        string ivaRic = TextBoxIvaFiscale.Text.Trim();
        if (!string.IsNullOrEmpty(ivaRic))
            ivaCodFisc = ivaRic;

        string direct = "ASC";
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');
            if (ord[0] == sortExpression && ord[1] == "ASC")
                direct = "ASC";
            else
                direct = "DESC";
        }
        ViewState["ordina"] = sortExpression + "|" + direct;

        CommittenteCollection listaCommittenti =
            biz.GetCommittentiOrdinati(null, ragioneSociale, comune, indirizzo, ivaCodFisc, sortExpression, direct,
                                       modalitaNotifiche);
        GridViewCommittenti.DataSource = listaCommittenti;
        GridViewCommittenti.PageIndex = 0;
        GridViewCommittenti.DataBind();
    }

    protected void GridViewCommittenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idCommittente = (int) GridViewCommittenti.DataKeys[e.NewSelectedIndex].Value;
        Committente committente =
            biz.GetCommittentiOrdinati(idCommittente, null, null, null, null, null, null, modalitaNotifiche)[0];

        if (OnCommittenteSelected != null)
            OnCommittenteSelected(committente);
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        ButtonNuovoCommittente.Visible = true;

        CaricaCommittenti();
    }

    protected void GridViewCommittenti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');

            if (ord[1] == "DESC")
                CaricaCommittentiPreservaOrdine(ord[0]);
            else
                CaricaCommittentiPreservaOrdine(ord[0]);
        }
        else
            CaricaCommittenti();

        GridViewCommittenti.PageIndex = e.NewPageIndex;
        GridViewCommittenti.DataBind();
    }

    protected void GridViewCommittenti_Sorting(object sender, GridViewSortEventArgs e)
    {
        //SortDirection dir = SortDirection.Descending;
        //if (e.SortDirection == SortDirection.Descending)
        //    dir = SortDirection.Ascending;

        CaricaCommittenti(e.SortExpression);
    }

    protected void ButtonChiudi_Click(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void ButtonNuovoCommittente_Click(object sender, EventArgs e)
    {
        if (OnNuovoCommittenteSelected != null)
            OnNuovoCommittenteSelected(this, null);
    }

    protected void GridViewCommittenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Committente committente = (Committente) e.Row.DataItem;

            Label lFonte = (Label) e.Row.FindControl("LabelFonte");
            Label lModificato = (Label) e.Row.FindControl("LabelModificato");

            if (committente.FonteNotifica)
                lFonte.Text = "Not.";
            else
                lFonte.Text = "Isp.";

            lModificato.Visible = committente.Modificato;

            if (!modalitaNotifiche && committente.FonteNotifica && !committente.Modificato)
                e.Row.ForeColor = Color.Gray;
        }
    }
}