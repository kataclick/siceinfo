<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuGestioneUtenti.ascx.cs"
    Inherits="WebControls_MenuGestioneUtenti" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<%
    if (
        (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti))
        )
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" runat="server" class="menuTable">
        <td>
            Gestione utenti
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti))
        {
    %>
    <tr>
        <td>
            <a runat="server" id="A1" href="~/GestioneUtentiRegistrazioneDipendente.aspx">Inserisci
                dipendenti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A2" href="~/GestioneUtentiRegistrazioneFornitore.aspx">Inserisci
                fornitore</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A3" href="~/GestioneUtentiRegistrazioneIspettori.aspx">Inserisci
                ispettore</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A10" href="~/GestioneUtentiRegistrazioneSindacalista.aspx">Inserisci
                sindacalista</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A11" href="~/GestioneUtentiRegistrazioneCommittente.aspx">Inserisci
                committente</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A12" href="~/GestioneUtentiRegistrazioneOspiti.aspx">Inserisci
                ospite</a>
        </td>
    </tr>
    <tr>
        <td>
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A4" href="~/GestioneUtentiUtenti_Ruoli.aspx">Gestisci utenti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A5" href="~/GestioneUtentiCreaRuolo.aspx">Crea ruolo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A6" href="~/GestioneUtentiRuoli_Funzionalita.aspx">Gestisci ruoli</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A7" href="~/GestioneUtenti/ResetPassword.aspx">Reset password</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A14" href="~/GestioneUtenti/InserimentoReport.aspx">Inserisci
                Report</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A13" href="~/GestioneUtenti/AssociazioneUtentiReport.aspx">Gestisci
                Report</a>
        </td>
    </tr>
    <%
        }

    %>
</table>
<%
    }
%>
