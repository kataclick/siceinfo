using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_CptGestioneIndirizzi : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["Indirizzi"] == null)
            ViewState["Indirizzi"] = new IndirizzoCollection();
    }

    protected void ButtonAggiungiIndirizzo_Click(object sender, EventArgs e)
    {
        if (TextBoxIndirizzo.Text != string.Empty)
        {
            Indirizzo indirizzo = new Indirizzo();
            indirizzo.Indirizzo1 = TextBoxIndirizzo.Text.Trim().ToUpper();
            indirizzo.Civico = TextBoxCivico.Text.Trim().ToUpper();
            indirizzo.InfoAggiuntiva = TextBoxInfoAggiuntiva.Text.Trim().ToUpper();
            indirizzo.Cap = TextBoxCap.Text.Trim().ToUpper();
            indirizzo.Comune = TextBoxComune.Text.Trim().ToUpper();
            indirizzo.Provincia = TextBoxProvincia.Text.Trim().ToUpper();

            decimal temp;
            if (decimal.TryParse(TextBoxLatitudine.Text, out temp))
                indirizzo.Latitudine = temp;
            else
                indirizzo.Latitudine = null;
            if (decimal.TryParse(TextBoxLongitudine.Text, out temp))
                indirizzo.Longitudine = temp;
            else
                indirizzo.Longitudine = null;

            IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];
            indirizzi.Add(indirizzo);
            ViewState["Indirizzi"] = indirizzi;
            CaricaIndirizzi();

            SvuotaCampi();
            LabelNessunIndirizzo.Visible = false;
            ButtonEliminaSelezione.Enabled = true;
        }
    }

    private void CaricaIndirizzi()
    {
        CheckBoxListIndirizzi.DataSource = ViewState["Indirizzi"];
        CheckBoxListIndirizzi.DataTextField = "IndirizzoCompleto";
        CheckBoxListIndirizzi.DataBind();
    }

    public void CaricaIndirizzi(IndirizzoCollection indirizzi)
    {
        ViewState["Indirizzi"] = indirizzi;
        CheckBoxListIndirizzi.DataSource = ViewState["Indirizzi"];
        CheckBoxListIndirizzi.DataTextField = "IndirizzoCompleto";
        CheckBoxListIndirizzi.DataBind();
    }

    private void SvuotaCampi()
    {
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxCivico.Text = string.Empty;
        TextBoxInfoAggiuntiva.Text = string.Empty;
        TextBoxCap.Text = string.Empty;
        TextBoxComune.Text = string.Empty;
        TextBoxProvincia.Text = string.Empty;
        TextBoxLatitudine.Text = string.Empty;
        TextBoxLongitudine.Text = string.Empty;
        GridViewIndirizzi.DataSource = null;
    }

    protected void ButtonGeocoder_Click(object sender, EventArgs e)
    {
        if (TextBoxIndirizzo.Text != string.Empty)
        {
            PanelIndirizzoGeo.Visible = true;

            Indirizzo indirizzo = new Indirizzo();
            indirizzo.Indirizzo1 = TextBoxIndirizzo.Text;
            indirizzo.Civico = TextBoxCivico.Text;
            indirizzo.Cap = TextBoxCap.Text;
            indirizzo.Comune = TextBoxComune.Text;
            indirizzo.Provincia = TextBoxProvincia.Text;

            IndirizzoCollection indirizzi = CantieriGeocoding.GeoCodeGoogleMultiplo(indirizzo.IndirizzoPerGeocoder);
            ViewState["IndirizziGeo"] = indirizzi;
            GridViewIndirizzi.DataSource = indirizzi;
            GridViewIndirizzi.DataBind();
        }
    }

    protected void ButtonEliminaSelezione_Click(object sender, EventArgs e)
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];

        while (CheckBoxListIndirizzi.SelectedIndex != -1)
        {
            int indice = CheckBoxListIndirizzi.SelectedIndex;

            CheckBoxListIndirizzi.Items.RemoveAt(indice);
            indirizzi.RemoveAt(indice);
        }

        if (indirizzi.Count == 0)
        {
            LabelNessunIndirizzo.Visible = true;
            ButtonEliminaSelezione.Enabled = false;
        }
    }

    protected void ButtonResettaCampi_Click(object sender, EventArgs e)
    {
        SvuotaCampi();
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (ViewState["IndirizziGeo"] != null)
        {
            IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["IndirizziGeo"];
            indirizzi[e.NewSelectedIndex].Civico = TextBoxCivico.Text;

            TextBoxIndirizzo.Text = indirizzi[e.NewSelectedIndex].Indirizzo1;
            TextBoxCivico.Text = indirizzi[e.NewSelectedIndex].Civico;
            TextBoxCap.Text = indirizzi[e.NewSelectedIndex].Cap;
            TextBoxComune.Text = indirizzi[e.NewSelectedIndex].Comune;
            TextBoxProvincia.Text = indirizzi[e.NewSelectedIndex].Provincia;
            TextBoxLatitudine.Text = indirizzi[e.NewSelectedIndex].Latitudine.ToString();
            TextBoxLongitudine.Text = indirizzi[e.NewSelectedIndex].Longitudine.ToString();

            PanelIndirizzoGeo.Visible = false;
        }
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        SvuotaCampi();
        //nascondiamo il pannello??? 
        PanelNuovoIndirizzo.Visible = false;
    }

    protected void ButtonAnnulla_Click1(object sender, EventArgs e)
    {
        SvuotaCampi();
        PanelNuovoIndirizzo.Visible = false;
    }

    private Control FindControl(string controlID, ControlCollection controls)
    {
        foreach (Control c in controls)
        {
            if (c.ID == controlID)
                return c;

            if (c.HasControls())
            {
                Control cTmp = FindControl(controlID, c.Controls);

                if (cTmp != null)
                    return cTmp;
            }
        }
        return null;
    }

    protected void GridViewIndirizzi_DataBound(object sender, EventArgs e)
    {
        Label labelErrore = (Label) FindControl("LabelErrore", GridViewIndirizzi.Controls);

        if (labelErrore != null)
        {
            if (GridViewIndirizzi.DataSource == null)
            {
                labelErrore.Text = "Il server non � disponibile o si � verificato un errore durante la richiesta";
            }
            else
            {
                labelErrore.Text = "Indirizzo non riconosciuto";
            }
        }
    }

    #region Esposizione degli indirizzi

    public IndirizzoCollection GetIndirizzi()
    {
        return (IndirizzoCollection) ViewState["Indirizzi"];
    }

    #endregion

    #region Funzioni temporanee da cancellare

    private GeocodingResult GeoOk()
    {
        GeocodingResult res = new GeocodingResult();
        res.GeoStatusCode = 200;
        res.Result = true;
        res.Indirizzo = "Via San Luca";
        res.Civico = "15";
        res.Provincia = "MI";
        res.Comune = "Milano";
        res.Cap = "20200";
        res.Latitude = 23.444444;
        res.Longitude = 44.66545654;

        return res;
    }

    private GeocodingResult GeoKo()
    {
        GeocodingResult res = new GeocodingResult();
        res.Result = false;

        return res;
    }

    #endregion
}