﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaRiepilogoAttestati.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaRiepilogoAttestati" %>
<table class="standardTable">
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Mese (mm/aaaa)
                        <asp:CustomValidator ID="CustomValidatorMese" runat="server" ControlToValidate="TextBoxMese"
                            ErrorMessage="*" OnServerValidate="CustomValidatorMese_ServerValidate" ValidationGroup="ricercaAttestati"></asp:CustomValidator>
                    </td>
                    <td>
                        Rag. Soc./Denominazione
                    </td>
                    <td>
                        P.IVA/Cod. Fisc.
                    </td>
                    <td>
                        Comune cantiere
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxMese" runat="server" MaxLength="7" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxPartitaIVA" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaAttestati"
                            OnClick="ButtonVisualizza_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewRiepilogoAttestati" runat="server" Width="100%" AutoGenerateColumns="False"
                AllowPaging="True" OnPageIndexChanging="GridViewRiepilogoAttestati_PageIndexChanging"
                OnRowDataBound="GridViewRiepilogoAttestati_RowDataBound" PageSize="5">
                <Columns>
                    <asp:BoundField DataField="mese" HeaderText="Periodo di osservazione" DataFormatString="{0:MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="150px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Appalto del cantiere">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelIndirizzoCivico" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="campoPiccolo">
                                        <asp:Label ID="LabelCapComuneProvincia" runat="server" CssClass="campoPiccolo"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="campoPiccolo">
                                        <asp:Label ID="LabelInfoAggiuntiva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Richiedente">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="campoPiccolo">
                                        <asp:Label ID="LabelPartitaIva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="campoPiccolo">
                                        <asp:Label ID="LabelCodiceFiscale" runat="server" CssClass="campoPiccolo"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Imprese">
                        <ItemTemplate>
                            <asp:GridView ID="GridViewImprese" runat="server" AutoGenerateColumns="False" Width="100%"
                                OnPageIndexChanging="GridViewImprese_PageIndexChanging" PageSize="5" OnRowDataBound="GridViewImprese_RowDataBound"
                                ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <b>
                                                            <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                                                        </b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="campoPiccolo">
                                                        <asp:Label ID="LabelPartitaIva" runat="server" CssClass="campoPiccolo"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="campoPiccolo">
                                                        <asp:Label ID="LabelCodiceFiscale" runat="server" CssClass="campoPiccolo"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="50%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    Nessuna impresa presente
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun dato estratto
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
