<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniDatiDomanda.ascx.cs" Inherits="WebControls_PrestazioniDatiDomanda" %>
<table class="borderedTable">
    <tr>
        <td style="width: 200px">
            Tipo prestazione:
        </td>
        <td>
            <asp:Label ID="LabelTipoPrestazione" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Codice prestazione:
        </td>
        <td>
            <asp:Label ID="LabelCodicePrestazione" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Lavoratore:
        </td>
        <td>
            <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Beneficiario:
        </td>
        <td>
            <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
        </td>
    </tr>
</table>
