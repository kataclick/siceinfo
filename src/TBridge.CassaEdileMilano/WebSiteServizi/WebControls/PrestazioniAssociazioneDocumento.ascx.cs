using System;
using System.Web.UI;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Delegates;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Business;

public partial class WebControls_PrestazioniAssociazioneDocumento : UserControl
{
    private PrestazioniBusiness biz = new PrestazioniBusiness();

    public event AssociazioneEffettuataEventHandler OnAssociazioneEffettuata;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ImpostaAssociazione(Documento documento, Domanda domanda)
    {
        LabelTipoDocumento.Text = documento.TipoDocumento.ToString();
        LabelIdTipoDocumento.Text = documento.TipoDocumento.IdTipoDocumento.ToString();
        LabelIdDocumento.Text = documento.IdDocumento.ToString();
        LabelIdArchidoc.Text = documento.IdArchidoc;

        CheckBoxPerDomanda.Checked = documento.PerPrestazione;

        if (documento.RiferitoA == "L")
        {
            RadioButtonFamiliare.Visible = false;
            RadioButtonFamiliare.Checked = false;
            RadioButtonLavoratore.Checked = true;
        }
        else
        {
            RadioButtonFamiliare.Checked = true;
            RadioButtonLavoratore.Checked = false;
        }

        ViewState["IdDomanda"] = domanda.IdDomanda.Value;
    }

    protected void ButtonAssocia_Click(object sender, EventArgs e)
    {
        short tipoDocumento = short.Parse(LabelIdTipoDocumento.Text);
        int idDocumento = Int32.Parse(LabelIdDocumento.Text);
        int? idDomandaP = null;

        int idDomanda = (int) ViewState["IdDomanda"];
        Domanda domanda = biz.GetDomanda(idDomanda);

        //if (CheckBoxPerDomanda.Checked)
        //    idDomandaP = domanda.IdDomanda;

        if (biz.AssociaPersonaDocumento(tipoDocumento, idDocumento, domanda.Lavoratore, domanda.Familiare,
                                        domanda.IdDomanda))
        {
            ResetCampiAssociazione();

            if (OnAssociazioneEffettuata != null)
                OnAssociazioneEffettuata();

            //Notifichiamo al CRM un cambio nella documentazione, le eccezioni del WS non vengono gestire a questo livello
            if (!Common.Sviluppo)
            {
                biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
            }
        }
    }

    public void ResetCampiAssociazione()
    {
        LabelTipoDocumento.Text = null;
        LabelIdTipoDocumento.Text = null;
        CheckBoxPerDomanda.Checked = false;
        LabelIdDocumento.Text = null;
        LabelIdArchidoc.Text = null;
        RadioButtonLavoratore.Checked = true;
        RadioButtonFamiliare.Checked = false;
    }
}