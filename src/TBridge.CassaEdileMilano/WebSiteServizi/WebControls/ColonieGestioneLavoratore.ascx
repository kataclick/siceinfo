<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieGestioneLavoratore.ascx.cs" Inherits="WebControls_ColonieGestioneLavoratore" %>
<%@ Register Src="ColonieRicercaLavoratoreACE.ascx" TagName="ColonieRicercaLavoratoreACE"
    TagPrefix="uc1" %>
<%@ Register Src="ColonieVisualizzaLavoratoreCE.ascx" TagName="ColonieVisualizzaLavoratoreCE"
    TagPrefix="uc2" %>
<%@ Register Src="ColonieModificaLavoratore.ascx" TagName="ColonieModificaLavoratore"
    TagPrefix="uc3" %>
<uc1:ColonieRicercaLavoratoreACE ID="ColonieRicercaLavoratoreACE1" runat="server" />
<uc2:ColonieVisualizzaLavoratoreCE ID="ColonieVisualizzaLavoratoreCE1" runat="server" />
<uc3:ColonieModificaLavoratore ID="ColonieModificaLavoratore1" runat="server" />
<asp:Button ID="ButtonConferma" runat="server" OnClick="ButtonConferma_Click" Text="Conferma" /><asp:Button
    ID="ButtonModifica" runat="server" OnClick="ButtonModifica_Click" Text="Modifica" />
