using System;
using System.Web.UI;

public partial class WebControls_GestioneRegistrazione : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void DropDownListTipoUtente_SelectedIndexChanged(object sender, EventArgs e)
    {
        GestionePaginaRegistrazione();
    }

    /// <summary>
    /// Bottone per chiamare la registrazione voluto. E' stato aggiunto perch� nel caso in cui i JS fossero disabilitati l'evento 
    /// alla cambio della combo non verrebbe lanciato
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonRegistra_Click(object sender, EventArgs e)
    {
        GestionePaginaRegistrazione();
    }

    /// <summary>
    /// Carica la pagina corretta in funzione del ripo di registrazione scelto
    /// </summary>
    private void GestionePaginaRegistrazione()
    {
        //Il load control a runtime da problemi perch� bisogna legare anche l'eventhandler altrimenti non funziona. 
        //Quindi si � perferita una soluzione pi� classica con una pagina per ogni registrazione

        if (DropDownListTipoUtente.SelectedValue == "Lavoratore")
        {
            Response.Redirect("~/GestioneUtentiRegistrazioneLavoratori.aspx");
        }
        else if (DropDownListTipoUtente.SelectedValue == "Impresa")
        {
            Response.Redirect("~/GestioneUtentiRegistrazioneImpresa.aspx");
        }
        else if (DropDownListTipoUtente.SelectedValue == "Consulente")
        {
            Response.Redirect("~/GestioneUtentiRegistrazioneConsulente.aspx");
        }
        //else if (DropDownListTipoUtente.SelectedValue == "Ospite")
        //{
        //    Response.Redirect("~/GestioneUtentiRegistrazioneOspiti.aspx");
        //}
    }
}