﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriImprese.ascx.cs"
    Inherits="WebControls_AccessoCantieriImprese" %>
<%@ Register Src="AccessoCantieriRicercaImpresa.ascx" TagName="AccessoCantieriRicercaImpresa"
    TagPrefix="uc4" %>
<%@ Register Src="AccessoCantieriImpreseSubappalti.ascx" TagName="AccessoCantieriImpreseSubappalti"
    TagPrefix="uc5" %>
<table class="standardTable">
    <tr>
        <td>
            <asp:GridView ID="GridViewSubappalti" runat="server" AutoGenerateColumns="False"
                DataKeyNames="IdSubappalto" OnRowDeleting="GridViewSubappalti_RowDeleting" Width="100%"
                OnRowDataBound="GridViewSubappalti_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Subappaltata">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltataRagioneSociale" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelSubappaltataPIva" runat="server" ForeColor="Gray"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelSubappaltataIndirizzo" runat="server" ForeColor="Gray"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelSubappaltataArtigiano" runat="server" ForeColor="Gray"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="49%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appaltatore/impresa">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSubappaltatriceRagioneSociale" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelSubappaltatricePIva" runat="server" ForeColor="Gray"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelSubappaltatriceIndirizzo" runat="server" 
                                                ForeColor="Gray"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <small>
                                            <asp:Label ID="LabelSubappaltatriceArtigiano" runat="server" 
                                                ForeColor="Gray"></asp:Label>
                                        </small>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="49%" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="NomeAppaltatrice" HeaderText="Subappaltata" Visible="False">
                        <ItemStyle Width="45%" />
                    </asp:BoundField>
                    <asp:BoundField DataField="NomeAppaltata" HeaderText="Appaltatore/impresa" Visible="False">
                        <ItemStyle Width="45%" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonDelete" runat="server" CommandName="Delete" ImageUrl="~/images/pallinoX.png"
                                ToolTip="Cancella l'appalto" />
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun appalto presente
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <asp:Button ID="ButtonVisualizzaAppalto" runat="server" OnClick="ButtonVisualizzaAppalto_Click"
                Text="Nuovo appalto..." Width="130px" CausesValidation="False" />
            <asp:CustomValidator ID="CustomValidatorAlmenoUnImpresa" runat="server" ErrorMessage="Inserire almeno un appalto"
                OnServerValidate="CustomValidatorAlmenoUnImpresa_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidatorImpresaRichiedentePresente" runat="server"
                ErrorMessage="Gli appalti devono comprendere l'impresa che effettua la richiesta "
                OnServerValidate="CustomValidatorImpresaRichiedentePresente_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAppalti" runat="server" Visible="False">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Crea appalto"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            <table class="borderedTable">
                                <tr>
                                    <td>
                                        <tr>
                                            <td>
                                                Appaltatore/
                                                <br />
                                                Impresa
                                            </td>
                                            <td>
                                                <table class="standardTable">
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="RadioButtonImpresaAppaltanteSiceInfo" runat="server" Text="Anagrafica"
                                                                Enabled="False" GroupName="scelta" />
                                                            <asp:RadioButton ID="RadioButtonImpresaAppaltanteNuova" runat="server" Text="Nuova impresa"
                                                                Enabled="False" GroupName="scelta" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table class="standardTable">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Panel ID="PanelAppaltante" runat="server" Visible="true">
                                                                            <table class="standardTable">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="TextBoxImpresaAppaltante" runat="server" Text="" Width="400px" Height="57px"
                                                                                            TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="LabelImpresaSelezionataEsistente" runat="server" ForeColor="Red"></asp:Label>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxImpresaAppaltante"
                                                                                            ErrorMessage="*" ValidationGroup="aggiungiAppalto"></asp:RequiredFieldValidator>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%--<tr>
                                                        <td>
                                                            <table class="borderedTable" visible="false">
                                                                <tr>
                                                                    <td>
                                                                        Data inizio attività
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadDatePicker ID="RadDateDataInizioAttivitaImpresaAppaltante" runat="server"
                                                                            Width="200px">
                                                                        </telerik:RadDatePicker>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CustomValidator ID="CustomValidatorDateCantiereAppaltante" runat="server" ControlToValidate="RadDateDataInizioAttivitaImpresaAppaltante"
                                                                            ErrorMessage="Periodo non corente con le date del cantiere." OnServerValidate="CustomValidatorDateCantiereAppaltante_ServerValidate"
                                                                            ValidationGroup="stop">*</asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Data fine attività
                                                                    </td>
                                                                    <td>
                                                                        <telerik:RadDatePicker ID="RadDateDataFineAttivitaImpresaAppaltante" runat="server"
                                                                            Width="200px">
                                                                        </telerik:RadDatePicker>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToCompare="RadDateDataFineAttivitaImpresaAppaltante"
                                                                            ControlToValidate="RadDateDataInizioAttivitaImpresaAppaltante" ErrorMessage="La data di inizio dell'attività non può essere superiore a quella di fine."
                                                                            Operator="LessThan" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="PanelImpresaAppaltanteDropDown" runat="server">
                                                                <asp:DropDownList ID="DropDownListImpresaAppaltante" runat="server" Width="300px">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="ButtonSelezionaImpresaAppaltanteDaDropDownList" runat="server" CausesValidation="False"
                                                                    OnClick="ButtonSelezionaImpresaAppaltataDaDropDownList_Click" Text="Seleziona" />
                                                                &nbsp;<asp:Button ID="ButtonAltroImpresaAppaltante" runat="server" Text="Altra impresa"
                                                                    Width="100px" OnClick="ButtonAltroImpresaAppaltata_Click" />
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="PanelRicercaNuovaImpresaAppaltante" runat="server" Visible="False">
                                                                <uc4:AccessoCantieriRicercaImpresa ID="AccessoCantieriRicercaImpresaSubappaltatrice"
                                                                    runat="server" Visible="False" />
                                                            </asp:Panel>
                                                            <asp:Panel ID="PanelInserisciImpresaAppaltante" runat="server" Visible="false">
                                                                <table class="filledtable">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table class="borderedTable">
                                                                    <tr>
                                                                        <td>
                                                                            <uc5:AccessoCantieriImpreseSubappalti ID="AccessoCantieriImpreseSubappaltiSubappaltatrice"
                                                                                runat="server" />
                                                                            <ul>
                                                                                <li>
                                                                                    <asp:Button ID="ButtonInserisciNuovaImpresaAppaltante" runat="server" Text="Inserisci nuova impresa"
                                                                                        Width="180" OnClick="ButtonInserisciNuovaImpresaSubappaltatrice_Click" />
                                                                                    <asp:Button ID="ButtonAnnullaNuovaImpresaAppaltante" runat="server" Text="Annulla inserimento impresa"
                                                                                        Width="180" OnClick="ButtonAnnullaNuovaImpresaSubappaltatrice_Click" />
                                                                                    <br />
                                                                                    <asp:Label ID="LabelNuovaAppaltanteRes" runat="server" ForeColor="Red"></asp:Label>
                                                                                </li>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="borderedTable">
                                <tr>
                                    <td>
                                        Subappaltata
                                    </td>
                                    <td>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButtonImpresaSubappaltataSiceInfo" runat="server" Text="Anagrafica"
                                                        Enabled="False" GroupName="scelta" />
                                                    <asp:RadioButton ID="RadioButtonImpresaSubappaltataNuova" runat="server" Text="Nuova impresa"
                                                        Enabled="False" GroupName="scelta" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelSubappaltata" runat="server" Visible="true">
                                                        <table class="standardTable">
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="TextBoxImpresaSubappaltata" runat="server" Text="" Width="400px"
                                                                        Height="57px" TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <asp:Label ID="LabelImpresaAppaltataDaEsistente" runat="server" ForeColor="Red"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <table class="borderedTable" visible="false">
                                                        <tr>
                                                            <td>
                                                                Data inizio attività
                                                            </td>
                                                            <td>
                                                                <telerik:RadDatePicker ID="RadDateDataInizioAttivitaImpresaSubappaltata" runat="server"
                                                                    Width="200px">
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="CustomValidatorDateCantiereSubappaltata" runat="server" ControlToValidate="RadDateDataInizioAttivitaImpresaSubappaltata"
                                                                    ErrorMessage="Periodo non corente con le date del cantiere." OnServerValidate="CustomValidatorDateCantiereSubappaltata_ServerValidate"
                                                                    ValidationGroup="stop">*</asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Data fine attività
                                                            </td>
                                                            <td>
                                                                <telerik:RadDatePicker ID="RadDateDataFineAttivitaImpresaSubappaltata" runat="server"
                                                                    Width="200px">
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                            <td>
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="RadDateDataFineAttivitaImpresaSubappaltata"
                                                                    ControlToValidate="RadDateDataInizioAttivitaImpresaSubappaltata" ErrorMessage="La data di inizio dell'attività non può essere superiore a quella di fine."
                                                                    Operator="LessThan" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelImpresaSubappaltataDropDown" runat="server" Visible="False">
                                                        <asp:DropDownList ID="DropDownListImpresaSubappaltata" runat="server" Width="300px">
                                                        </asp:DropDownList>
                                                        <asp:Button ID="ButtonSelezionaImpresaSubappaltataDaDropDownList" runat="server"
                                                            CausesValidation="False" Text="Seleziona" />
                                                        &nbsp;<asp:Button ID="ButtonAltroSubappaltata" runat="server" Text="Altra impresa"
                                                            Width="100px" OnClick="ButtonAltro_Click" />
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelRicercaImpresaSubappaltataNuovaImpresa" runat="server" Visible="false">
                                                        <uc4:AccessoCantieriRicercaImpresa ID="AccessoCantieriRicercaImpresaAppaltataDa"
                                                            runat="server" Visible="false" />
                                                    </asp:Panel>
                                                    <asp:Panel ID="PanelInserisciImpresaSubappaltata" runat="server" Visible="false">
                                                        <table class="filledtable">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="borderedTable">
                                                            <tr>
                                                                <td>
                                                                    <uc5:AccessoCantieriImpreseSubappalti ID="AccessoCantieriImpreseSubappaltiAppaltataDa"
                                                                        runat="server" />
                                                                    <ul>
                                                                        <li>
                                                                            <asp:Button ID="ButtonInserisciNuovaImpresaSubappaltata" runat="server" OnClick="ButtonInserisciNuovaImpresaAppaltataDa_Click"
                                                                                Text="Inserisci nuova impresa" Width="180" />
                                                                            <asp:Button ID="ButtonAnnullaNuovaImpresaSubappaltata" runat="server" CausesValidation="False"
                                                                                OnClick="ButtonAnnullaNuovaImpresaAppaltataDa_Click" Text="Annulla inserimento impresa"
                                                                                Width="180" />
                                                                            <br />
                                                                            <asp:Label ID="LabelNuovaSubappaltataDaRes" runat="server" ForeColor="Red"></asp:Label>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonAggiungiAppalto" runat="server" Width="180px" OnClick="ButtonAggiungiAppalto_Click"
                                Text="Salva intero appalto" />
                            <asp:Button ID="ButtonAnnulla" runat="server" CausesValidation="False" OnClick="ButtonAnnulla_Click"
                                Text="Annulla intero appalto" Width="180px" />
                            <asp:Label ID="LabelRisultatoAppalto" runat="server" ForeColor="Red"></asp:Label>
                            <asp:CustomValidator ID="CustomValidatorAppalto" runat="server" ErrorMessage="Appalto non valido."
                                ForeColor="Red">&nbsp;</asp:CustomValidator>
                            <br />
                            <asp:CustomValidator ID="CustomValidatorSubappalti" runat="server" ErrorMessage="Appalto già presente o non valido."
                                ForeColor="Red" OnServerValidate="CustomValidatorSubappalti_ServerValidate" ValidationGroup="appalto"></asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
