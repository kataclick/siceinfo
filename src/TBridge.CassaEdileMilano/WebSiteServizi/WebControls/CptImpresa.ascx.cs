using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;

public partial class WebControls_CptImpresa : UserControl
{
    private readonly CptBusiness biz = new CptBusiness();

    private string errore;

    /// <summary>
    /// ProprietÓ che costruisce il committente. Se sono presenti errori ritorna null
    /// </summary>
    public Impresa impresa
    {
        get
        {
            if (ControlloCampiServer())
            {
                return CreaImpresa();
            }
            else return null;
        }
    }

    public string Errore
    {
        get { return errore; }
        set { errore = value; }
    }

    public void ImpostaRagioneSociale(string ragioneSociale)
    {
        TextBoxRagioneSociale.Text = ragioneSociale;
    }

    public void ImpostaImpresa(Impresa impresa)
    {
        ViewState["IdImpresa"] = impresa.IdImpresa;
        ViewState["IdImpresaTemporaneo"] = impresa.IdImpresaTemporaneo;

        TextBoxRagioneSociale.Text = impresa.RagioneSociale;
        TextBoxPartitaIva.Text = impresa.PartitaIva;
        TextBoxCodiceFiscale.Text = impresa.CodiceFiscale;
        TextBoxTelefono.Text = impresa.Telefono;
        TextBoxFax.Text = impresa.Fax;
        TextBoxPersonaRiferimento.Text = impresa.PersonaRiferimento;
        CheckBoxLavoratoreAutonomo.Checked = impresa.LavoratoreAutonomo;
        if (DropDownListTipoAttivita.Items.FindByText(impresa.TipoAttivita) != null)
        {
            DropDownListTipoAttivita.SelectedValue = impresa.TipoAttivita;

            TextBoxTipoAttivita.Text = string.Empty;
            TextBoxTipoAttivita.Visible = false;
        }
        else
        {
            if (!string.IsNullOrEmpty(impresa.TipoAttivita))
            {
                TextBoxTipoAttivita.Visible = true;
                TextBoxTipoAttivita.Text = impresa.TipoAttivita;
                DropDownListTipoAttivita.Text = "Sconosciuta";
            }
            else
            {
                TextBoxTipoAttivita.Text = string.Empty;
                TextBoxTipoAttivita.Visible = false;
            }
        }

        CptSelezioneIndirizzo1.ImpostaIndirizzo(impresa);
    }

    public void Reset()
    {
        TextBoxIdImpresa.Text = string.Empty;
        CptSelezioneIndirizzo1.CancellaIndirizzo();
        TextBoxCodiceFiscale.Text = string.Empty;
        TextBoxPartitaIva.Text = string.Empty;
        TextBoxRagioneSociale.Text = string.Empty;
        TextBoxTelefono.Text = string.Empty;
        TextBoxFax.Text = string.Empty;
        TextBoxPersonaRiferimento.Text = string.Empty;
    }

    /// <summary>
    /// Effettua il controllo dei campi lato server
    /// </summary>
    /// <returns></returns>
    private bool ControlloCampiServer()
    {
        // CAMPI CONTROLLATI
        // Ragione sociale

        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
        {
            res = false;
            errori.Append("Campo ragione sociale vuoto" + Environment.NewLine);
        }

        if (!CptSelezioneIndirizzo1.IndirizzoSelezionato())
        {
            res = false;
            errori.Append("Campo indirizzo vuoto" + Environment.NewLine);
        }

        if (!res)
            errore = errori.ToString();

        return res;
    }

    private Impresa CreaImpresa()
    {
        // Creazione oggetto impresa
        int? idImpresa = null;
        int? idImpresaTemporaneo = null;
        string ragioneSociale;
        string partitaIva = null;
        string codiceFiscale = null;
        string telefono = null;
        string fax = null;
        string personaRiferimento = null;
        string tipoAttivita = null;
        bool lavoratoreAutonomo;
        bool modificata = false;

        if (ViewState["IdImpresa"] != null)
        {
            idImpresa = (int?) ViewState["IdImpresa"];
            modificata = true;
        }
        if (ViewState["IdImpresaTemporaneo"] != null)
        {
            idImpresaTemporaneo = (int?) ViewState["IdImpresaTemporaneo"];
            modificata = true;
        }

        ragioneSociale = TextBoxRagioneSociale.Text.Trim().ToUpper();

        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()))
            partitaIva = TextBoxPartitaIva.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text.Trim()))
            codiceFiscale = TextBoxCodiceFiscale.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxTelefono.Text.Trim()))
            telefono = TextBoxTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxFax.Text.Trim()))
            fax = TextBoxFax.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxPersonaRiferimento.Text.Trim()))
            personaRiferimento = TextBoxPersonaRiferimento.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(DropDownListTipoAttivita.Text.Trim()))
        {
            if (DropDownListTipoAttivita.Text == "Sconosciuta" && !string.IsNullOrEmpty(TextBoxTipoAttivita.Text))
                tipoAttivita = TextBoxTipoAttivita.Text.Trim();
            else
                tipoAttivita = DropDownListTipoAttivita.Text.Trim();
        }

        lavoratoreAutonomo = CheckBoxLavoratoreAutonomo.Checked;

        Indirizzo indirizzo = CptSelezioneIndirizzo1.Indirizzo();

        Impresa impresa =
            new Impresa(TipologiaImpresa.Cantieri, idImpresa, ragioneSociale,
                        indirizzo.Indirizzo1 + " " + indirizzo.Civico,
                        indirizzo.Provincia, indirizzo.Comune, indirizzo.Cap, partitaIva, codiceFiscale, tipoAttivita,
                        telefono, fax, personaRiferimento,
                        lavoratoreAutonomo);

        impresa.Modificato = modificata;
        impresa.IdImpresaTemporaneo = idImpresaTemporaneo;

        return impresa;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieAttivita();
        }
    }

    private void CaricaTipologieAttivita()
    {
        DropDownListTipoAttivita.DataSource = biz.GetTipologieAttivita();
        DropDownListTipoAttivita.Text = "Sconosciuta";
        DropDownListTipoAttivita.DataBind();
    }

    public void SetValidationGroup(string validationGroup)
    {
        RequiredFieldValidator1.ValidationGroup = validationGroup;
        //this.RequiredFieldValidatorIndirizzo.ValidationGroup = validationGroup;
    }

    #region impresa properties

    public int IdImpresa { get; set; }


    public string RagioneSociale { get; set; }

    public string PartitaIVA { get; set; }

    public string CodiceFiscale { get; set; }

    public string ImpIndirizzo { get; set; }

    public string Provincia { get; set; }

    public string Comune { get; set; }

    public string CAP { get; set; }

    #endregion

    #region Biz properties

    public TSBusiness TsBiz { get; set; }

    public CantieriBusiness CantieriBiz { get; set; }

    #endregion
}