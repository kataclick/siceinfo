using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class WebControls_SmsInfoMenu : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSScadenza.ToString()))
        {
            Controls.Add(LoadControl("~/WebControls/SmsAScadenza.ascx"));
        }

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSImmediati.ToString()))
        {
            Controls.Add(LoadControl("~/WebControls/SmsImmediati.ascx"));
        }

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSInviati.ToString()))
        {
            Controls.Add(LoadControl("~/WebControls/SmsInviati.ascx"));
        }

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciErrori.ToString()))
        {
            Controls.Add(LoadControl("~/WebControls/SmsRicevuti.ascx"));
        }
    }
}