<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuGestioneFatturazione.ascx.cs" Inherits="WebControls_MenuGestioneFatturazione" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeImmissioneFatture.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFatture.ToString()))
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Gestione Fatture</td>
    </tr>

    <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeImmissioneFatture.ToString()))
    {
    %>
    <tr>
        <td>
            <a id="A1" href="TuteScarpeImmissioneFatture.aspx">Immissione nuova fattura</a>
        </td>
    </tr>
    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFatture.ToString()))
    {
    %>
    <tr>
        <td>
            <a id="A2" href="TuteScarpeGestioneFatture.aspx">Gestione fatture</a>
        </td>
    </tr>
    <%
    }     
%>
</table>
<%
    }     
%>