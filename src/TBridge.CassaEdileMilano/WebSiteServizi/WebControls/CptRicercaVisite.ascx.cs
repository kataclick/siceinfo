using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class WebControls_CptRicercaVisite : UserControl
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieVisita();
            CaricaEnti();
            CaricaEsiti();
            CaricaAree();

            SelezionaArea();
        }
    }

    private void SelezionaArea()
    {
        // Se l'utente � autorizzato all'inserimento di notifiche per una sola area
        // blocco la selezione
        if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicerca.ToString())
            ^
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicercaLodi.ToString())
            )
        {
            // Seleziono l'area Milano
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptRicerca.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "1";
            }

            // Seleziono l'area Lodi
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptRicercaLodi.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "2";
            }

            DropDownListArea.Enabled = false;
        }
    }

    private void CaricaAree()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListArea,
            biz.GetAree(),
            "Descrizione",
            "IdArea");
    }

    private void CaricaEsiti()
    {
        EsitoVisitaCollection esiti = biz.GetEsitiVisita();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListEsito,
            esiti,
            "Descrizione",
            "IdEsitoVisita");
    }

    private void CaricaEnti()
    {
        DropDownListEnte.Items.Clear();
        DropDownListEnte.Items.Add(new ListItem(string.Empty, string.Empty));

        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaASLVisualizzazione.ToString())
            )
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.ASL.ToString(), ((int) EnteVisita.ASL).ToString()));
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione.ToString())
            )
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.CPT.ToString(), ((int) EnteVisita.CPT).ToString()));
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione.ToString())
            )
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.DPL.ToString(), ((int) EnteVisita.DPL).ToString()));
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione.ToString())
            )
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.ASLERSLT.ToString(),
                                                    ((int) EnteVisita.ASLERSLT).ToString()));
        }
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaCassaEdileVisualizzazione.ToString())
            )
        {
            DropDownListEnte.Items.Add(new ListItem(EnteVisita.CassaEdile.ToString(),
                                                    ((int) EnteVisita.CassaEdile).ToString()));
        }

        if (DropDownListEnte.Items.Count == 2)
        {
            DropDownListEnte.SelectedIndex = 1;
            DropDownListEnte.Enabled = false;
        }
    }

    private void CaricaTipologieVisita()
    {
        TipologiaVisitaCollection tipologieVisita = biz.GetTipologieVisita();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipologia,
            tipologieVisita,
            "Descrizione",
            "IdTipologiaVisita");
    }

    protected void GridViewVisite_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricerca");

        if (Page.IsValid)
        {
            GridViewVisite.PageIndex = e.NewPageIndex;
            CaricaVisite();
        }
    }

    protected void GridViewVisite_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Visita visita = (Visita) e.Row.DataItem;
            WebControls_CptAllegatiAttivita allAtt =
                (WebControls_CptAllegatiAttivita) e.Row.FindControl("CptAllegatiAttivita1");
            GridView gvIndirizzi = (GridView) e.Row.FindControl("GridViewIndirizzi");
            Label lEnte = (Label) e.Row.FindControl("LabelEnte");
            Label lTipologia = (Label) e.Row.FindControl("LabelTipologia");
            Label lEsito = (Label) e.Row.FindControl("LabelEsito");
            Label lGradoIrregolarita = (Label) e.Row.FindControl("LabelGradoIrregolarita");

            lEnte.Text = visita.Ente.ToString();
            lTipologia.Text = visita.Tipologia.ToString();
            lEsito.Text = visita.Esito.ToString();
            if (visita.GradoIrregolarita != null)
                lGradoIrregolarita.Text = visita.GradoIrregolarita.ToString();

            Presenter.CaricaElementiInGridView(gvIndirizzi, visita.Indirizzi, 0);
            allAtt.ImpostaAllegati(visita.Allegati);
        }
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewVisite.PageIndex = 0;
            CaricaVisite();
        }
    }

    private void CaricaVisite()
    {
        VisitaFilter filtro = new VisitaFilter();

        if (!string.IsNullOrEmpty(TextBoxData.Text))
            filtro.Data = DateTime.Parse(TextBoxData.Text.Replace('.', '/'));

        if (!string.IsNullOrEmpty(DropDownListTipologia.SelectedValue))
        {
            filtro.Tipologia = new TipologiaVisita();
            filtro.Tipologia.IdTipologiaVisita = Int32.Parse(DropDownListTipologia.SelectedValue);
        }

        if (!string.IsNullOrEmpty(DropDownListEnte.SelectedValue))
            filtro.Ente = (EnteVisita) Enum.Parse(typeof (EnteVisita), DropDownListEnte.SelectedValue);

        if (!string.IsNullOrEmpty(DropDownListEsito.SelectedValue))
        {
            filtro.Esito = new EsitoVisita();
            filtro.Esito.IdEsitoVisita = Int32.Parse(DropDownListEsito.SelectedValue);
        }

        if (!string.IsNullOrEmpty(TextBoxIndirizzo.Text))
            filtro.Indirizzo = TextBoxIndirizzo.Text;

        if (!string.IsNullOrEmpty(TextBoxComune.Text))
            filtro.Comune = TextBoxComune.Text;

        if (GestioneUtentiBiz.IsASL())
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.ASL asl =
            //    ((ASL)HttpContext.Current.User.Identity).Entity;
            ASL asl =
                (ASL) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            filtro.IdASL = asl.IdASL;
        }

        filtro.IdArea = Int16.Parse(DropDownListArea.SelectedValue);

        VisitaCollection visite = biz.GetVisite(filtro);
        GridViewVisite.DataSource = visite;
        GridViewVisite.DataBind();
    }

    protected void GridViewVisite_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idNotifica = (int) GridViewVisite.DataKeys[e.NewSelectedIndex].Values["IdNotifica"];
        Notifica notificaAggiornata = biz.GetNotificaUltimaVersione(idNotifica);

        Response.Redirect(String.Format("~/Cpt/CptNotificaDettagli.aspx?idNotifica={0}", notificaAggiornata.IdNotifica));
    }
}