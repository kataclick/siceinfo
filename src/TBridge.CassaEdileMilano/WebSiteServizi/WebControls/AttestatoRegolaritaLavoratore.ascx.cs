﻿#region

using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;

#endregion

public partial class WebControls_AttestatoRegolaritaLavoratore : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        //List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        //funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        //funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        //GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CantieriRapportoIspezioneStep3.aspx");

        #region biz ed eventi

        AttestatoRegolaritaRicercaLavoratore1.OnLavoratoreSelected +=
            AttestatoRegolaritaRicercaLavoratore1_OnLavoratoreSelected;
        AttestatoRegolaritaRicercaLavoratore1.OnNuovoLavoratoreSelected +=
            AttestatoRegolaritaRicercaLavoratore1_OnNuovoLavoratoreSelected;

        #endregion

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();

        ////resettiamo
        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupLavoratore') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovoLavoratore, null) + ";");
        sb.Append("return true;");
        ButtonInserisciNuovoLavoratore.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciNuovoLavoratore);
        
        #endregion

        if (!Page.IsPostBack)
        {
            if (ViewState["domandeImprese"] == null)
            {
                ViewState["domandeImprese"] = new DomandaImpresaCollection();
            }

            //CaricaDati();
        }
    }

    private void CaricaDati()
    {
        ViewState["domandeImprese"] = new DomandaImpresaCollection();
    }

    public void CaricaImprese(ImpresaCollection Imprese)
    {
        DropDownListImpresa.Items.Clear();
        DropDownListImpresa.Items.Add(new ListItem(string.Empty, string.Empty));
        foreach (Impresa Imp in Imprese)
        {
            if (!Imp.LavoratoreAutonomo)
                DropDownListImpresa.Items.Add(
                    new ListItem(Imp.CodiceERagioneSociale,
                                 string.Format("{0}|{1}|{2}", ((int) Imp.TipoImpresa), Imp.IdImpresa, Imp.IdTemporaneo)));
        }

        if (Imprese.Count == 0)
        {
            //ButtonVisualizzaRicercaImprese.Visible = false;
            LabelImprese.Text = "Non ci sono imprese da selezionare";
            DropDownListImpresa.Visible = false;
            LabelImpresaSelezionata.Visible = false;
            ButtonaggiungiLavoratore.Enabled = false;
            PanelAggiungiLavoratore.Visible = false;
            AttestatoRegolaritaRicercaLavoratore1.Visible = false;
            PanelInserisciLavoratore.Visible = false;
        }
        else
        {
            //ButtonVisualizzaRicercaImprese.Visible = true;
            LabelImprese.Text = "Imprese ";
            DropDownListImpresa.Visible = true;
            LabelImpresaSelezionata.Visible = true;
        }
    }

    public void CaricaLavoratori(DomandaImpresaCollection domImp)
    {
        ViewState["domandeImprese"] = domImp;
        //CaricaDatiLavoratori();
    }

    private void CaricaDatiLavoratori()
    {
        GridViewLavoratori.Visible = true;
        DomandaImpresaCollection domandeImprese = (DomandaImpresaCollection) ViewState["domandeImprese"];

        int? idImpresa = null;
        Guid? idTemporaneo = null;
        string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
        TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

        if (tipoImpresa == TipologiaImpresa.SiceNew)
        {
            idImpresa = Int32.Parse(tipoId[1]);
        }
        else
        {
            if (String.IsNullOrEmpty(tipoId[1]))
            {
                Guid gid = new Guid(tipoId[2]);
                idTemporaneo = gid;
            }
            else
            {
                idImpresa = Int32.Parse(tipoId[1]);
            }
        }


        DomandaImpresa domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, idImpresa, idTemporaneo);

        if (domandaImpresa != null)
        {
            Presenter.CaricaElementiInGridView(
                GridViewLavoratori,
                domandaImpresa.Lavoratori,
                0);
        }
        else
        {
            Presenter.CaricaElementiInGridView(
                GridViewLavoratori,
                null,
                0);
        }
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore Lav = (Lavoratore) e.Row.DataItem;
            Label lLavoratore = (Label) e.Row.FindControl("LabelLavoratore");

            if (Lav != null)
            {
                if (Lav.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    lLavoratore.Text = string.Format("{0} - {1} {2}", Lav.IdLavoratore, Lav.Cognome, Lav.Nome);
                }
                else
                {
                    lLavoratore.Text = string.Format("{0} {1}", Lav.Cognome, Lav.Nome);
                }
            }
        }
    }

    protected void GridViewLavoratori_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DomandaImpresa dImp = (DomandaImpresa) ViewState["domandaImpresa"];

        dImp.Lavoratori.RemoveAt(e.RowIndex);

        ViewState["domandaImpresa"] = dImp;

        Salva();

        CaricaDatiLavoratori();
    }

    private void ResetCampi()
    {
        ViewState["Lavoratore"] = null;
        LabelResInserimentoLavInLista.Text = string.Empty;
    }

    public void DeselezionaImpresa()
    {
        ViewState["Impresa"] = null;
        LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";
        DropDownListImpresa.SelectedIndex = 0;

        GridViewLavoratori.Visible = false;
        ButtonaggiungiLavoratore.Enabled = false;
        PanelAggiungiLavoratore.Visible = false;
        AttestatoRegolaritaRicercaLavoratore1.Visible = false;
        PanelInserisciLavoratore.Visible = false;
    }

    protected void ButtonaggiungiLavoratore_Click(object sender, EventArgs e)
    {
        LabelGiaPresente.Visible = false;
        AttestatoRegolaritaLavoratore1.Reset();

        PanelAggiungiLavoratore.Visible = true;
        AttestatoRegolaritaRicercaLavoratore1.Visible = true;

        PanelRicercaInserisciLavoratore.Visible = true;
        AttestatoRegolaritaRicercaLavoratore1.Visible = true;
        PanelInserisciLavoratore.Visible = false;
    }

    //protected void GridViewLavSiceNew_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    GridView gvLavoratoriSiceNew = (GridView) sender;

    //    int? idLavoratore = (int?) gvLavoratoriSiceNew.DataKeys[e.NewSelectedIndex].Value;

    //    if (idLavoratore.HasValue)
    //    {
    //        //int indiceLav = ((GridViewRow)(((Control)sender).Parent.Parent)).RowIndex;
    //        //int idIspezioneLav = (int)GridViewIspezioneLavoratori.DataKeys[indiceLav].Value;

    //        ////if (biz.UpdateIspezioneLavoratore(idIspezioneLav, idLavoratore.Value))
    //        //    CaricaDati();
    //    }
    //}

    protected void DropDownListImpresa_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
            SelezionaImpresaDaDropDowList();
    }

    private void SelezionaImpresaDaDropDowList()
    {
        //try
        //{
        string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
        TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

        Impresa impresa = new Impresa();
        impresa.TipoImpresa = tipoImpresa;

        if (tipoImpresa == TipologiaImpresa.SiceNew)
        {
            impresa.IdImpresa = Int32.Parse(tipoId[1]);
        }
        else
        {
            if (String.IsNullOrEmpty(tipoId[1]))
            {
                Guid gid = new Guid(tipoId[2]);
                impresa.IdTemporaneo = gid;
            }
            else
            {
                impresa.IdImpresa = Int32.Parse(tipoId[1]);
            }
        }

        // Porcata per gestire il fatto che nella DropDown c'è anche l'ID    
        String ragioneSociale = DropDownListImpresa.SelectedItem.Text;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
        {
            String[] splitted = ragioneSociale.Split('-');
            ragioneSociale = String.Empty;

            for (int i = 1; i < splitted.Length; i++)
            {
                ragioneSociale += splitted[i];

                if (i > 1)
                {
                    ragioneSociale += "-";
                }
            }
        }

        impresa.RagioneSociale = ragioneSociale;

        ImpresaSelezionata(impresa);

        DomandaImpresaCollection domandeImprese = (DomandaImpresaCollection) ViewState["domandeImprese"];
        DomandaImpresa domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, impresa.IdImpresa,
                                                                               impresa.IdTemporaneo);

        ViewState["domandaImpresa"] = domandaImpresa;

        Salva();

        CaricaDatiLavoratori();
        //}
        //catch
        //{
        //}
    }

    private void Salva()
    {
        DomandaImpresaCollection domandeImprese = (DomandaImpresaCollection) ViewState["domandeImprese"];
        DomandaImpresa domImp = (DomandaImpresa) ViewState["domandaImpresa"];

        if (domandeImprese != null)
        {
            if (domImp != null)
            {
                if (!domandeImprese.Contains(domImp))
                {
                    domandeImprese.Add(domImp);
                }
                else
                {
                    domandeImprese.Cancella(domImp);
                    domandeImprese.Add(domImp);
                }

                ViewState["domandeImprese"] = domandeImprese;
            }
        }
    }

    private void ImpresaSelezionata(Impresa impresa)
    {
        ViewState["Impresa"] = impresa;

        LabelImpresaSelezionata.Text = string.Format("{0} : {1}", "Impresa selezionata", impresa.RagioneSociale);

        AttestatoRegolaritaRicercaLavoratore1.SetImpresaSelezionata(impresa);

        ButtonaggiungiLavoratore.Enabled = true;
    }

    protected void ButtonVisualizzaRicercaImprese_Click(object sender, EventArgs e)
    {
        //SelezionaImpresaDaDropDowList();
    }

    protected void DropDownListImpresa_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
            SelezionaImpresaDaDropDowList();
        else
        {
            ViewState["Impresa"] = null;

            LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";

            GridViewLavoratori.Visible = false;
            ButtonaggiungiLavoratore.Enabled = false;
            PanelAggiungiLavoratore.Visible = false;
            AttestatoRegolaritaRicercaLavoratore1.Visible = false;
            PanelInserisciLavoratore.Visible = false;
        }
    }

    #region Eventi gestione inserimento ricerca lavoratore

    /// <summary>
    /// Visualizziamo la ricerca dei lavoratori
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ButtonVisualizzaRicercaLavoratori_Click(object sender, EventArgs e)
    {
        PanelRicercaInserisciLavoratore.Visible = true;
        AttestatoRegolaritaRicercaLavoratore1.Visible = true;
        PanelInserisciLavoratore.Visible = false;
    }

    /// <summary>
    /// Evento per l'inserimento di un nuovo lavoratore
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void AttestatoRegolaritaRicercaLavoratore1_OnNuovoLavoratoreSelected(object sender, EventArgs e)
    {
        PanelRicercaInserisciLavoratore.Visible = true;
        AttestatoRegolaritaRicercaLavoratore1.Visible = false;
        PanelInserisciLavoratore.Visible = true;
    }

    private void AttestatoRegolaritaRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        LavoratoreSelezionato(lavoratore);
    }

    /// <summary>
    /// Memorizziamo la selezione di un nuovo lavoratore
    /// </summary>
    /// <param name="lavoratore"></param>
    private void LavoratoreSelezionato(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        if (ViewState["Lavoratore"] != null && ViewState["Impresa"] != null)
        {
            Impresa impresa = (Impresa) ViewState["Impresa"];

            DomandaImpresa dImp;

            if (ViewState["domandaImpresa"] == null)
            {
                dImp = new DomandaImpresa();
                dImp.Lavoratori = new LavoratoreCollection();
                ViewState["domandaImpresa"] = dImp;
            }

            dImp = (DomandaImpresa) ViewState["domandaImpresa"];

            if (lavoratore.TipoLavoratore == TipologiaLavoratore.Nuovo)
            {
                if (!dImp.Lavoratori.ContainsCodFisc(lavoratore))
                {
                    dImp.Lavoratori.Add(lavoratore);
                }
            }
            else
            {
                if (!dImp.Lavoratori.Contains(lavoratore))
                {
                    dImp.Lavoratori.Add(lavoratore);
                    LabelGiaPresente.Visible = false;
                }
                else
                {
                    LabelGiaPresente.Visible = true;
                }
            }


            dImp.Impresa = impresa;
            ViewState["domandaImpresa"] = dImp;

            Salva();

            CaricaDatiLavoratori();
            PanelAggiungiLavoratore.Visible = false;

            ResetCampi();
        }
        else
        {
            LabelResInserimentoLavInLista.Text =
                "E' necessario selezionare sia il lavoratore che l'impresa a cui ha dichiarato di appartenere";
        }
    }

    protected void ButtonInserisciNuovoLavoratore_Click(object sender, EventArgs e)
    {
        Lavoratore lavoratore = AttestatoRegolaritaLavoratore1.Lavoratore;

        if (lavoratore != null)
        {
            LavoratoreSelezionato(lavoratore);
        }
        else
            LabelInserimentoNuovoLavoratoreRes.Text = "Non tutti i campi sono corretti";
    }

    public void ButtonAnnullaInserimentoLavoratore_Click(object sender, EventArgs e)
    {
        PanelAggiungiLavoratore.Visible = false;
        PanelRicercaInserisciLavoratore.Visible = false;
        AttestatoRegolaritaRicercaLavoratore1.Visible = false;
        PanelInserisciLavoratore.Visible = false;
    }

    public DomandaImpresaCollection GetDomandeImprese()
    {
        return (DomandaImpresaCollection) ViewState["domandeImprese"];
    }

    #endregion
}