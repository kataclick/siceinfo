using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_CptSelezioneIndirizzo : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void SvuotaCampiIndirizzo()
    {
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxCivico.Text = string.Empty;
        TextBoxCap.Text = string.Empty;
        TextBoxComune.Text = string.Empty;
        TextBoxProvincia.Text = string.Empty;
        //TextBoxLatitudine.Text = string.Empty;
        //TextBoxLongitudine.Text = string.Empty;
        GridViewIndirizzi.DataSource = null;
    }

    public void CancellaIndirizzo()
    {
        TextBoxIndirizzoSel.Text = string.Empty;
        ViewState["Indirizzo"] = null;
    }

    public bool IndirizzoSelezionato()
    {
        if (!string.IsNullOrEmpty(TextBoxIndirizzoSel.Text))
            return true;
        else
            return false;
    }

    public Indirizzo Indirizzo()
    {
        if (ViewState["Indirizzo"] != null)
            return (Indirizzo) ViewState["Indirizzo"];
        else
            return null;
    }

    public void ImpostaIndirizzo(Committente committente)
    {
        TextBoxIndirizzo.Text = committente.Indirizzo;
        TextBoxCap.Text = committente.Cap;
        TextBoxComune.Text = committente.Comune;
        TextBoxProvincia.Text = committente.Provincia;

        Indirizzo indirizzo = CreaIndirizzo();

        ViewState["Indirizzo"] = indirizzo;
        ImpostaIndirizzoSelezionato(indirizzo);
    }

    public void ImpostaIndirizzo(Impresa impresa)
    {
        TextBoxIndirizzo.Text = impresa.Indirizzo;
        TextBoxCap.Text = impresa.Cap;
        TextBoxComune.Text = impresa.Comune;
        TextBoxProvincia.Text = impresa.Provincia;

        Indirizzo indirizzo = CreaIndirizzo();

        ViewState["Indirizzo"] = indirizzo;
        ImpostaIndirizzoSelezionato(indirizzo);
    }

    protected void ButtonAggiungiIndirizzo_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Indirizzo indirizzo = CreaIndirizzo();

            //decimal temp;
            //if (decimal.TryParse(TextBoxLatitudine.Text, out temp))
            //    indirizzo.Latitudine = temp;
            //else
            //    indirizzo.Latitudine = null;
            //if (decimal.TryParse(TextBoxLongitudine.Text, out temp))
            //    indirizzo.Longitudine = temp;
            //else
            //    indirizzo.Longitudine = null;

            ViewState["Indirizzo"] = indirizzo;
            ImpostaIndirizzoSelezionato(indirizzo);

            SvuotaCampi();
        }
    }

    protected Indirizzo CreaIndirizzo()
    {
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.Indirizzo1 = TextBoxIndirizzo.Text.Trim().ToUpper();
        indirizzo.Civico = TextBoxCivico.Text.Trim().ToUpper();
        indirizzo.Cap = TextBoxCap.Text.Trim().ToUpper();
        indirizzo.Comune = TextBoxComune.Text.Trim().ToUpper();
        indirizzo.Provincia = TextBoxProvincia.Text.Trim().ToUpper();

        return indirizzo;
    }

    protected void ImpostaIndirizzoSelezionato(Indirizzo indirizzo)
    {
        TextBoxIndirizzoSel.Text =
            string.Format("{0} {1}\n{2} {3}", indirizzo.Indirizzo1, indirizzo.Civico, indirizzo.Comune,
                          indirizzo.Provincia);
    }

    protected void ButtonGeocoder_Click(object sender, EventArgs e)
    {
        if (TextBoxIndirizzo.Text != string.Empty)
        {
            PanelIndirizzoGeo.Visible = true;

            Indirizzo indirizzo = new Indirizzo();
            indirizzo.Indirizzo1 = TextBoxIndirizzo.Text;
            indirizzo.Civico = TextBoxCivico.Text;
            indirizzo.Cap = TextBoxCap.Text;
            indirizzo.Comune = TextBoxComune.Text;
            indirizzo.Provincia = TextBoxProvincia.Text;

            IndirizzoCollection indirizzi =
                CantieriGeocoding.GeoCodeGoogleMultiplo(indirizzo.IndirizzoPerGeocoderGenerico);
            ViewState["IndirizziGeo"] = indirizzi;
            GridViewIndirizzi.DataSource = indirizzi;
            GridViewIndirizzi.DataBind();
        }
    }

    private void SvuotaCampi()
    {
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxCivico.Text = string.Empty;
        TextBoxCap.Text = string.Empty;
        TextBoxComune.Text = string.Empty;
        TextBoxProvincia.Text = string.Empty;
        //TextBoxLatitudine.Text = string.Empty;
        //TextBoxLongitudine.Text = string.Empty;
        GridViewIndirizzi.DataSource = null;
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (ViewState["IndirizziGeo"] != null)
        {
            IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["IndirizziGeo"];
            indirizzi[e.NewSelectedIndex].Civico = TextBoxCivico.Text;

            TextBoxIndirizzo.Text = indirizzi[e.NewSelectedIndex].Indirizzo1;
            TextBoxCivico.Text = indirizzi[e.NewSelectedIndex].Civico;
            TextBoxCap.Text = indirizzi[e.NewSelectedIndex].Cap;
            TextBoxComune.Text = indirizzi[e.NewSelectedIndex].Comune;
            TextBoxProvincia.Text = indirizzi[e.NewSelectedIndex].Provincia;
            TextBoxLatitudine.Text = indirizzi[e.NewSelectedIndex].Latitudine.ToString();
            TextBoxLongitudine.Text = indirizzi[e.NewSelectedIndex].Longitudine.ToString();

            PanelIndirizzoGeo.Visible = false;
        }
    }

    protected void ButtonResettaCampi_Click(object sender, EventArgs e)
    {
        SvuotaCampiIndirizzo();
    }
}