﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneCEImpiegati.ascx.cs"
    Inherits="WebControls_IscrizioneCEImpiegati" %>
<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelCantierePrevedi" runat="server" Text="Impiegati" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Numero impiegati:
        </td>
        <td>
            <asp:TextBox ID="TextBoxLavoratoriOccupatiPrevedi" runat="server" Width="300px" MaxLength="10" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoriOccupatiPrevedi"
                runat="server" ErrorMessage="Numero impiegati mancante" ControlToValidate="TextBoxLavoratoriOccupatiPrevedi"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorLavoratoriOccupatiPrevedi"
                runat="server" ControlToValidate="TextBoxLavoratoriOccupatiPrevedi" ErrorMessage="Numero impiegati non corretto"
                ValidationExpression="^\d{0,4}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCantiereNumeroImpiegati" runat="server" ControlToValidate="TextBoxLavoratoriOccupatiPrevedi"
                ErrorMessage="Il numero degli impiegati deve essere positivo" OnServerValidate="CustomValidatorCantiereNumeroImpiegati_ServerValidate"
                ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
</table>
