using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Business;

public partial class WebControls_PrestazioniDatiFamiliare : UserControl
{
    private Common commonBiz = new Common();
    private PrestazioniBusiness biz = new PrestazioniBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        bool forzatura = false;

        if (ViewState["Forzatura"] != null)
            forzatura = true;

        if (!Page.IsPostBack && !forzatura)
        {
            StatoCampi(false);
        }
    }

    public void StatoCampi(bool stato)
    {
        TextBoxDatiFamiliareCognome.Enabled = stato;
        TextBoxDatiFamiliareNome.Enabled = stato;
        TextBoxDatiFamiliareDataNascita.Enabled = stato;
        TextBoxDatiFamiliareCodiceFiscale.Enabled = stato;
        RadioButtonSessoM.Enabled = stato;
        RadioButtonSessoF.Enabled = stato;
    }

    public void ResetCampi()
    {
        StatoCampi(false);

        TextBoxDatiFamiliareCognome.Text = null;
        TextBoxDatiFamiliareNome.Text = null;
        TextBoxDatiFamiliareDataNascita.Text = null;
        TextBoxDatiFamiliareCodiceFiscale.Text = null;
        RadioButtonSessoM.Checked = true;
        RadioButtonSessoF.Checked = false;
    }

    public void CaricaTipoPrestazione(string idTipoPrestazione)
    {
        ViewState["idTipoPrestazione"] = idTipoPrestazione;
    }

    public void CaricaIdLavoratore(Lavoratore Lavoratore)
    {
        ViewState["idLavoratore"] = Lavoratore.IdLavoratore;
    }

    public void CaricaDatiFamiliare(Familiare familiare)
    {
        ResetCampi();

        TextBoxDatiFamiliareCognome.Text = familiare.Cognome;
        TextBoxDatiFamiliareNome.Text = familiare.Nome;
        if (familiare.DataNascita.HasValue)
        {
            TextBoxDatiFamiliareDataNascita.Text = familiare.DataNascita.Value.ToString("dd/MM/yyyy");
            TextBoxDatiFamiliareDataNascita.Enabled = false;
        }
        else
        {
            TextBoxDatiFamiliareDataNascita.Enabled = true;
        }

        RadioButtonSessoM.Enabled = true;
        RadioButtonSessoF.Enabled = true;

        TextBoxDatiFamiliareCodiceFiscale.Enabled = true;
        if (!string.IsNullOrEmpty(familiare.CodiceFiscale))
        {
            TextBoxDatiFamiliareCodiceFiscale.Text = familiare.CodiceFiscale;
        }

        if (familiare.Sesso == 'M')
        {
            RadioButtonSessoM.Checked = true;
            RadioButtonSessoF.Checked = false;
        }
        else
        {
            RadioButtonSessoM.Checked = false;
            RadioButtonSessoF.Checked = true;
        }
    }

    public Familiare CreaFamiliare()
    {
        Familiare familiare = new Familiare();

        familiare.Cognome = TextBoxDatiFamiliareCognome.Text.Trim().ToUpper();
        familiare.Nome = TextBoxDatiFamiliareNome.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(TextBoxDatiFamiliareDataNascita.Text))
            familiare.DataNascita = DateTime.Parse(TextBoxDatiFamiliareDataNascita.Text.Replace('.', '/'));
        familiare.CodiceFiscale = TextBoxDatiFamiliareCodiceFiscale.Text.Trim().ToUpper();

        return familiare;
    }

    protected void CustomValidatorDatiFamiliareCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string sesso;

        if (RadioButtonSessoM.Checked)
            sesso = "M";
        else
            sesso = "F";
        try
        {
            if (!commonBiz.VerificaPrimi11CaratteriCodiceFiscale(TextBoxDatiFamiliareNome.Text.Trim().ToUpper(),
                                                                 TextBoxDatiFamiliareCognome.Text.Trim().ToUpper(),
                                                                 sesso,
                                                                 DateTime.Parse(TextBoxDatiFamiliareDataNascita.Text.Replace('.', '/')),
                                                                 TextBoxDatiFamiliareCodiceFiscale.Text.Trim().ToUpper()))
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        catch (Exception)
        {
            args.IsValid = false;
        }
    }

    public void ForzaStatoCampi()
    {
        ViewState["Forzatura"] = true;
    }

    public void DisabilitaControlloCodiceFiscale()
    {
        GestioneControlloCodiceFiscale(false);
    }

    /// <summary>
    /// True abilita il controllo de CF, false disabilita
    /// </summary>
    /// <param name="controllo"></param>
    public void GestioneControlloCodiceFiscale(bool controllo)
    {
        RequiredFieldValidatorDatiFamiliareCodiceFiscale.Enabled = controllo;
        RegularExpressionValidatorDatiFamiliareCodiceFiscale.Enabled = controllo;
        CustomValidatorDatiFamiliareCodiceFiscale.Enabled = controllo;
    }
    protected void CustomValidatorDataNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime? dataNascita = null;
        DateTime dn;
        if( DateTime.TryParseExact(TextBoxDatiFamiliareDataNascita.Text,"dd/MM/yyyy",null, System.Globalization.DateTimeStyles.None, out dn))
            dataNascita = dn;
        
        int idLav = 0;
        if (ViewState["idLavoratore"] != null)
            idLav = (int)ViewState["idLavoratore"];

        if (ViewState["idTipoPrestazione"] as string == "PRENAT" && dataNascita.HasValue && idLav > 0)
        {
            Configurazione configurazione = biz.GetConfigurazionePrestazione("PRENAT", "F", idLav);
            Domanda domandaTemporanea = new Domanda();
            DateTime dataDomanda = DateTime.Now;
            //DateTime dd;
            //if (ViewState["IdDomandaTemporanea"] != null)
            //{
            //    int idDomandaTemporanea = (int)ViewState["IdDomandaTemporanea"];
            //    domandaTemporanea = biz.GetDomandaTemporanea(idDomandaTemporanea);
            //    if (DateTime.TryParseExact(domandaTemporanea.DataDomanda.ToString(), "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dd))
            //        dataDomanda = dd;
            //}            
            
            if (dataNascita < dataDomanda.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda))
            { 
                args.IsValid = false; 
            }
        }        
    }
}