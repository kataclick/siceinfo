<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SmsRicevuti.ascx.cs" Inherits="WebControls_SmsRicevuti" %>
<table class="menuTable">
<tr class="menuTable">
        <td>SMS ricevuti</td>
    </tr>
     <tr>
        <td><a href="SmsRicevuti.aspx?tipoOp=All">Verifica SMS ricevuti</a></td>
     </tr>
     <tr>
        <td> <a href="SmsRicevuti.aspx?tipoOp=NonGestiti">Verifica SMS non gestiti</a></td>
     </tr>
     <tr>
        <td> <a href="SmsRicevuti.aspx?tipoOp=NonCorretti">Verifica SMS non corretti</a></td>
    </tr>
    <tr>
        <td> <a href="SmsRicevuti.aspx?tipoOp=Nascosti">Verifica SMS nascosti</a></td>
    </tr>
</table>
