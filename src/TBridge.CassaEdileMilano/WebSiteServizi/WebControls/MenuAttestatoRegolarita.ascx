<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuAttestatoRegolarita.ascx.cs"    Inherits="WebControls_MenuAttestatoRegolarita" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaRichiesta.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaRiepilogoAttestati.ToString())
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td >
            Verifica RegolaritÓ per Cantiere
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaRichiesta.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE.ToString())
            )
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/AttestatoRegolarita/RichiestaAttestato.aspx" runat="server">Richiesta</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="~/AttestatoRegolarita/GestioneAttestati.aspx" runat="server">Gestione
                richieste</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/AttestatoRegolarita/GestioneCE.aspx" runat="server">Gestione CE</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.AttestatoRegolaritaRiepilogoAttestati.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/AttestatoRegolarita/RiepilogoAttestati.aspx" runat="server">Riepilogo attestati</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
