﻿using System;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.Presenter;

public partial class WebControls_AccessoCantieriRilevatori : UserControl
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
    private Boolean _res;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaFornitori(_biz.GetFornitori());
            LabelRes.Text = string.Empty;
        }
        else
        {
            if (_res)
            {
                LabelRes.Visible = true;
                LabelRes.Text = "Inserito correttamente.";
            }

            else
            {
                LabelRes.Visible = true;
                LabelRes.Text = "Problemi nell'inserimento, controllare i dati.";
            }
        }
    }

    private void CaricaFornitori(FornitoreCollection fornitori)
    {
        Presenter.CaricaElementiInDropDown(
            DropDownList1,
            fornitori,
            "RagioneSociale",
            "idFornitore");
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        //_res = _biz.InsertRilevatore(TextBox1.Text, Int32.Parse(DropDownList1.SelectedValue));

        if (_res)
        {
            LabelRes.Visible = true;
            LabelRes.Text = "Inserito correttamente.";
        }

        else
        {
            LabelRes.Visible = true;
            LabelRes.Text = "Problemi nell'inserimento, controllare i dati.";
        }
    }
}