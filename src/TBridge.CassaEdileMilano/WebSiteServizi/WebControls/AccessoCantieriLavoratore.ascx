﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriLavoratore.ascx.cs"
    Inherits="WebControls_AccessoCantieriLavoratore" %>
<%@ Register Src="AccessoCantieriRicercaLavoratore.ascx" TagName="AccessoCantieriricercalavoratore"
    TagPrefix="uc3" %>
<%@ Register Src="AccessoCantieriRicercaImpresa.ascx" TagName="AccessoCantieriricercaimpresa"
    TagPrefix="uc5" %>
<%@ Register Src="AccessoCantieriImpreseSubappalti.ascx" TagName="AccessoCantieriimpresesubappalti"
    TagPrefix="uc6" %>
<%@ Register Src="AccessoCantieriLavoratoreInserimento.ascx" TagName="AccessoCantierilavoratoreinserimento"
    TagPrefix="uc7" %>

<style type="text/css">
    .style1
    {
        width: 133px;
    }
</style>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function refreshGridNew(arg) {
            <%= PostBackStringNew %>
        }

        function refreshGridEdit(arg) {
            <%= PostBackStringEdit %>
        }

        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openLavoratore(idWhiteList, dataInizio, dataFine, index) {
            var oWindow = radopen("../AccessoCantieri/DatiAggiuntiviLavoratore.aspx?idWhiteList=" + idWhiteList + "&dataInizio=" + dataInizio + "&dataFine=" + dataFine + "&index=" + index, null);
            oWindow.set_title("Info Lavoratore");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 600);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openImport(idWhiteList) {
            var oWindow = radopen("../AccessoCantieri/ImportLavoratori.aspx?idWhiteList=" + idWhiteList, null);
            oWindow.set_title("Import Lavoratori");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(600, 400);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadCodeBlock>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />

<table class="standardTable">
    <tr>
        <td>
            <br />
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco lavoratori"></asp:Label>
            <br />
            <br />
            <asp:Label ID="LabelImprese" runat="server" Text="Impresa"></asp:Label>&nbsp;
            <asp:DropDownList ID="DropDownListImpresa" runat="server" Width="300px" AutoPostBack="True"
                OnSelectedIndexChanged="DropDownListImpresa_SelectedIndexChanged1">
            </asp:DropDownList>&nbsp;
            <asp:Button ID="ButtonEsportaLavoratori" runat="server" Text="Esporta lavoratori"
                Width="225px" OnClick="ButtonEsportaLavoratori_Click" CausesValidation="False"
                Enabled="False" />
            <br />
            <br />
            <asp:Label ID="LabelImpresaSelezionata" runat="server" Text="Nessuna impresa selezionata"></asp:Label>
            <asp:Panel
                ID="PanelLavoratoriInForza"
                runat="server"
                Width="100%"
                Visible="false">
                <b>
                    Lavoratori in forza
                    (<asp:Label
                        ID="LabelLavoratoriForza"
                        runat="server">
                    </asp:Label>
                    )
                </b>
                (NON vengono caricati automaticamente in white list) 
                <asp:Button
                    ID="ButtonMostraNascondi"
                    runat="server"
                    Text="Mostra" 
                    Width="100px"
                    onclick="ButtonMostraNascondi_Click" />
                <asp:Panel
                    ID="PanelForza"
                    runat="server"
                    Width="100%"
                    Visible="false">
                    <asp:GridView
                        ID="GridViewLavoratoriInForza"
                        runat="server"
                        Width="100%"
                        AutoGenerateColumns="False"
                        AllowPaging="True" 
                        onrowdatabound="GridViewLavoratoriInForza_RowDataBound"
                        PageSize="5"
                        DataKeyNames="IdLavoratore,Cognome,Nome,CodiceFiscale,DataNascita"
                        onpageindexchanging="GridViewLavoratoriInForza_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBoxSeleziona" runat="server" />
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="IdLavoratore" HeaderText="Codice">
                            <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Cognome" HeaderText="Cognome">
                            <ItemStyle Width="200px" Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nome" HeaderText="Nome">
                            <ItemStyle Width="200px" Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" 
                                HeaderText="Data nascita">
                            <ItemStyle Width="80px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale">
                            <ItemStyle Width="150px" />
                            </asp:BoundField>
                        </Columns>
                        <EmptyDataTemplate>
                            Non risulta al momento nessun lavoratore in forza.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Button
                        ID="ButtonAggiungiForza"
                        runat="server"
                        Text="Aggiungi i lavoratori selezionati in white list"
                        Width="280px" 
                        onclick="ButtonAggiungiForza_Click" />
                </asp:Panel>
            </asp:Panel>
            <br />
            <asp:Panel
                ID="PanelWhiteList"
                runat="server"
                Visible="false">
            <b>
                Lavoratori in white list
            </b>
            <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                OnRowDataBound="GridViewLavoratori_RowDataBound" Width="100%" OnRowDeleting="GridViewLavoratori_RowDeleting"
                DataKeyNames="IdDomandaLavoratore">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image
                                ID="ImageBadge"
                                runat="server"
                                Visible="false"
                                ToolTip="Sono presenti tutti i dati per la stampa del badge" 
                                ImageUrl="~/images/badge16.jpg" />
                        </ItemTemplate>
                        <ItemStyle Width="20px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lavoratore/i">
                        <ItemTemplate>
                            <b>
                                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                            </b>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" HtmlEncode="False">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="dataInizioAttivita" DataFormatString="{0:dd/MM/yyyy}"
                        HeaderText="Inizio attività">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="dataFineAttivita" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fine attività">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="EffettuaControlli" HeaderText="Effettua Controlli"
                        ReadOnly="True">
                        <ItemStyle Width="50px" />
                    </asp:CheckBoxField>
                    <asp:TemplateField AccessibleHeaderText="Elimina">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonElimina" runat="server" CommandName="Delete" ImageUrl="~/images/editdelete.png" />
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonModifica" runat="server" CausesValidation="false" 
                                CommandName="Modifica" ImageUrl="~/images/edit.png" Text="Modifica" />
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun lavoratore presente
                </EmptyDataTemplate>
            </asp:GridView>
            </asp:Panel>
            <br />
            <br />
            <%--<asp:Panel ID="Panel1" runat="server" Width="100%" Visible="False">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Modifica lavoratore"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            Data Inizio attività
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDateDataInizioAttivitaModifica" runat="server" Width="200px">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="RadDateDataInizioAttivitaModifica"
                                ErrorMessage="Periodo non corente con le date del cantiere." OnServerValidate="CustomValidator1_ServerValidate"
                                ValidationGroup="stop">*</asp:CustomValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data fine attività
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDateDataFineAttivitaModifica" runat="server" Width="200px">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToCompare="RadDateDataFineAttivitaModifica"
                                ControlToValidate="RadDateDataInizioAttivitaModifica" ErrorMessage="La data di inizio dell'attività non può essere superiore a quella di fine."
                                Operator="LessThan" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                        </td>
                        <td>
                            <asp:Label ID="LabelModificaDateNonCorenti" runat="server" ForeColor="Red" Text="Lavoratore già presente"
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data assunzione
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDateDataAssunzioneModifica" runat="server" Width="200px">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Paese di nascita:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListPaeseNascita" runat="server" AppendDataBoundItems="True"
                                OnSelectedIndexChanged="DropDownListPaeseNascita_SelectedIndexChanged" AutoPostBack="True"
                                Width="200px" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Luogo di nascita:
                        </td>
                        <td>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelLuogoItalia" runat="server" Enabled="False">
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        Italiano
                                                    </td>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Provincia
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DropDownListProvinciaNascita" runat="server" AppendDataBoundItems="True"
                                                            Width="200" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvinciaNascita_SelectedIndexChanged" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Comune
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DropDownListComuneNascita" runat="server" AppendDataBoundItems="True"
                                                            Width="200" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelLuogoEstero" runat="server" Enabled="False">
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        Estero
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="DropDownListComuneNascitaEstero" runat="server" AppendDataBoundItems="true"
                                                            Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fotografia (formato JPG, max 100KB)</td>
                        <td>
                            <telerik:RadUpload ID="RadUploadFotoModifica" AllowedFileExtensions=".jpg"
                                OnClientFileSelected="OnClientFileSelectedHandlerModifica" runat="server" MaxFileInputsCount="1"
                                MaxFileSize="102400" ControlObjectsVisibility="AddButton" InitialFileInputsCount="0"
                                Width="200px">
                                <Localization Add="Aggiungi foto" Select="Seleziona" />
                            </telerik:RadUpload>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorFotoModifica" runat="server" ErrorMessage="Possono essere importati solo file con estensione .jpg e con dimensione inferiore a 100kB"
                                OnServerValidate="CustomValidatorFotoModifica_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <telerik:RadBinaryImage runat="server" ID="RadBinaryImageFotoModifica" Height="60px"
                                AutoAdjustImageControlSize="False" ResizeMode="Fit" Width="55px" ImageUrl="~/images/noImg.JPG" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonRemoveFoto" runat="server" CausesValidation="False" Text="Elimina foto"
                                OnClick="ButtonRemoveFoto_Click" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <script type="text/javascript">

                        function OnClientFileSelectedHandlerModifica(sender, eventArgs) {
                            var input = eventArgs.get_fileInputField();
                            if (sender.isExtensionValid(input.value)) {


                                var radImg = document.getElementById('<%= RadBinaryImageFotoModifica.ClientID %>');
                                if (radImg) {
                                    radImg.src = input.value;
                                }

                            }


                        }

                    </script>
                    <tr>
                        <td>
                            Includi nei controlli di regolarità
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxControlliAggiornamento" runat="server" Checked="true" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErroriModifica" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="stop" />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonModificaLavoratore" runat="server" CausesValidation="False"
                                Text="Salva" Width="225px" OnClick="ButtonModificaLavoratore_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>--%>
            <br />
            <table>
                <tr>
                    <td>
                        <asp:Button ID="ButtonaggiungiLavoratore" runat="server" Text="Aggiungi singolo lavoratore"
                            Width="225px" OnClick="ButtonAggiungiLavoratore_Click" CausesValidation="False"
                            Enabled="False" />
                        <asp:Label ID="LabelGiaPresente" runat="server" ForeColor="Red" Text="Lavoratore già presente"
                            Visible="False"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="ButtonImportaListaLavoratori" runat="server" Text="Aggiungi lista lavoratori"
                            Width="225px" CausesValidation="False"
                            Enabled="False" onclick="ButtonImportaListaLavoratori_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <br />
            <asp:Panel
                ID="PanelLavoratoriDaFile"
                runat="server"
                Width="100%"
                Visible="false">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelAggiungiListaLav" runat="server" Font-Bold="True" ForeColor="White"
                                Text="Inserimento lavoratori"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td valign="top">
                            <table class="standardTable">
                                <tr>
                                    <td class="style1">
                                        Data inizio attività:
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker
                                            ID="RadDatePickerDataInizioAttivita"
                                            runat="server">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Data fine attività:
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker
                                            ID="RadDatePickerDataFineAttivita"
                                            runat="server">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <small>
                                            Se l'impresa di origine del lavoratore e quella di destinazione hanno periodi di attività sovrapposti indicare le date per i lavoratori
                                        </small>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:FileUpload ID="FileUploadCsv" runat="server" Width="225px" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Label ID="LabelStatoImportExport" runat="server" ForeColor="Red" Text="" Visible="True" />
                        </td>
                        <td>
                            <asp:Button ID="ButtonVerificaLavoratori" runat="server" Text="Verifica lista lavoratori"
                                Width="225px" CausesValidation="False"
                                onclick="ButtonVerificaLavoratori_Click" />
                                <br />
                            <asp:Button ID="ButtonImportaLavoratori" runat="server" Text="Aggiungi lista lavoratori"
                                Width="225px" OnClick="ButtonImportaLavoratori_Click" CausesValidation="False" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="ButtonAnnullaInserimentoLista" runat="server" CausesValidation="False"
                            Text="Annulla" Width="170px" onclick="ButtonAnnullaInserimentoLista_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ListView
                                ID="ListViewErrori"
                                runat="server">
                                <ItemTemplate>
                                    <%# Container.DataItem.ToString() %>
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="PanelAggiungiLavoratore" runat="server" Width="100%" Visible="False">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelAggiungiLav" runat="server" Font-Bold="True" ForeColor="White"
                                Text="Inserimento lavoratore"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            <%--<table class="standardTable">
                                <tr>
                                    <td>
                                        Data Inizio attività
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker ID="RadDateInizioAttivita" runat="server" Width="200px">
                                        </telerik:RadDatePicker>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Data fine attività
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker ID="RadDateFineAttivita" runat="server" Width="200px">
                                        </telerik:RadDatePicker>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="RadDateFineAttivita"
                                            ControlToValidate="RadDateInizioAttivita" ErrorMessage="La data di inizio dell'attività non può essere superiore a quella di fine,"
                                            Operator="LessThan" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Data assunzione
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker ID="RadDateDataAssunzione" runat="server" Width="200px">
                                        </telerik:RadDatePicker>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Paese di nascita:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListPaeseNascitaNuovo" runat="server" AppendDataBoundItems="True"
                                            OnSelectedIndexChanged="DropDownListPaeseNascitaNuovo_SelectedIndexChanged" AutoPostBack="True"
                                            Width="200px" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Luogo di nascita:
                                    </td>
                                    <td>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelLuogoItaliaNuovo" runat="server" Enabled="False">
                                                        <table class="standardTable">
                                                            <tr>
                                                                <td>
                                                                    Italiano
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Provincia
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DropDownListProvinciaNascitaNuovo" runat="server" AppendDataBoundItems="True"
                                                                        Width="200" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvinciaNascitaNuovo_SelectedIndexChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Comune
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="DropDownListComuneNascitaNuovo" runat="server" AppendDataBoundItems="True"
                                                                        Width="200" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelLuogoEsteroNuovo" runat="server" Enabled="False">
                                                        <table class="standardTable">
                                                            <tr>
                                                                <td>
                                                                    Estero
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="DropDownListComuneNascitaEsteroNuovo" runat="server" AppendDataBoundItems="true"
                                                                        Width="200px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Fotografia
                                    </td>
                                    <td>
                                        <telerik:RadUpload ID="RadUploadFoto" runat="server" AllowedFileExtensions=".jpg"
                                            ControlObjectsVisibility="AddButton" InitialFileInputsCount="0"
                                            MaxFileInputsCount="1" MaxFileSize="100000" OnClientFileSelected="OnClientFileSelectedHandler"
                                            Width="200px">
                                            <Localization Add="Aggiungi foto" Select="Seleziona" />
                                        </telerik:RadUpload>
                                    </td>
                                    <td>
                                        <asp:CustomValidator ID="CustomValidatorFoto" runat="server" ErrorMessage="Possono essere importati solo file con estensione .jpg e con dimensione inferiore a 100kB"
                                            OnServerValidate="CustomValidatorFoto_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <telerik:RadBinaryImage ID="RadBinaryImageFoto" runat="server" AutoAdjustImageControlSize="False"
                                            Height="60px" ImageUrl="~/images/noImg.JPG" ResizeMode="Fit" Width="55px" />
                                    </td>
                                    <td>
                                        <asp:Button ID="ButtonRemoveFotoNuovo" runat="server" CausesValidation="False" Text="Elimina foto"
                                            OnClick="ButtonRemoveFoto_Click" />
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <script type="text/javascript">


                                    function OnClientFileSelectedHandler(sender, eventArgs) {
                                        var input = eventArgs.get_fileInputField();
                                        if (sender.isExtensionValid(input.value)) {
                                            var radImg = document.getElementById('<%= RadBinaryImageFoto.ClientID %>');
                                            if (radImg) {
                                                radImg.src = input.value;
                                            }
                                        }
                                    }
                                   

                                </script>
                                <tr>
                                    <td>
                                        Includi nei controlli di regolarità
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxControlliInserimento" runat="server" Checked="true" />
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <br />--%>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelRicercaInserisciLavoratore" runat="server" Visible="False" Width="100%">
                                            <uc3:AccessoCantieriricercalavoratore ID="AccessoCantieriRicercaLavoratore1" runat="server"
                                                Visible="false" />
                                            <asp:Panel ID="PanelInserisciLavoratore" runat="server" Visible="False" Width="100%">
                                                <table class="filledtable">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuovo lavoratore"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table class="borderedTable">
                                                    <tr>
                                                        <td>
                                                            <uc7:AccessoCantierilavoratoreinserimento ID="AccessoCantieriLavoratore1" runat="server" />
                                                            <asp:Button ID="ButtonInserisciNuovoLavoratore" runat="server"
                                                                Text="Inserisci Lavoratore" Width="170px" ValidationGroup="lavoratore" 
                                                                onclick="ButtonInserisciNuovoLavoratore_Click" />
                                                            <asp:Button ID="ButtonAnnullaInserimentoLavoratore" runat="server" CausesValidation="False"
                                                                OnClick="ButtonAnnullaInserimentoLavoratore_Click" Text="Annulla" Width="170px" />
                                                            <br />
                                                            <br />
                                                            <asp:Label ID="LabelInserimentoNuovoLavoratoreRes" runat="server" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Label ID="LabelResInserimentoLavInLista" runat="server" ForeColor="Red"></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
