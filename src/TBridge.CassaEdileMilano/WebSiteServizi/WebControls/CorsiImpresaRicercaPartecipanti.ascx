﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiImpresaRicercaPartecipanti.ascx.cs"
    Inherits="WebControls_CorsiImpresaRicercaPartecipanti" %>
<asp:Panel ID="PanelRicercaLavoratoreImpresa" runat="server" DefaultButton="ButtonRicerca">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelPadding2" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="cercaIscrizioni"
                                Width="100px" OnClick="ButtonRicerca_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Iscrizioni trovate"></asp:Label><br />
                <asp:GridView ID="GridViewPartecipazioni" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="GridViewPartecipazioni_PageIndexChanging"
                    OnRowDataBound="GridViewPartecipazioni_RowDataBound" PageSize="5" DataKeyNames="DataPianificazione,IdCorso,IdLocazione">
                    <Columns>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Iscrizione">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Corso:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCorso" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Modulo:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelModulo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Schedulazione:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelSchedulazione" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Locazione:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelLocazione" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Panel ID="PanelStato" runat="server" Visible="false">
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="CheckBoxPresente" runat="server" Enabled="false" Text="Presente" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="CheckBoxAttestato" runat="server" Enabled="false" Text="Attestato" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ItemTemplate>
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna iscrizione trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
