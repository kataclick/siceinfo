using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Delegates;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Filters;

public partial class WebControls_PrestazioniRicercaDocumenti : UserControl
{
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    public event DocumentoSelectedEventHandler OnDocumentoSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipiDocumento();
        }
    }

    public void PreImpostaTipoDocumento(TipoDocumento tipoDocumento, bool perPrestazione, string relativoA)
    {
        DropDownListTipoDocumento.SelectedValue = tipoDocumento.IdTipoDocumento.ToString();
        DropDownListTipoDocumento.Enabled = false;
        ViewState["PerPrestazione"] = perPrestazione;
        ViewState["RelativoA"] = relativoA;

        GridViewDocumenti.Visible = false;
    }

    private void CaricaTipiDocumento()
    {
        DropDownListTipoDocumento.Items.Clear();

        DropDownListTipoDocumento.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListTipoDocumento.DataSource = biz.GetTipiDocumento();
        DropDownListTipoDocumento.DataTextField = "Descrizione";
        DropDownListTipoDocumento.DataValueField = "IdTipoDocumento";
        DropDownListTipoDocumento.DataBind();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        LabelErroreVisualizzazioneDocumento.Visible = false;

        if (Page.IsValid)
        {
            GridViewDocumenti.Visible = true;
            GridViewDocumenti.PageIndex = 0;
            CaricaDocumenti();
        }
    }

    private void CaricaDocumenti()
    {
        DocumentoFilter filtro = new DocumentoFilter();

        if (!string.IsNullOrEmpty(TextBoxIdLavoratore.Text))
            filtro.IdLavoratore = Int32.Parse(TextBoxIdLavoratore.Text);
        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            filtro.Cognome = TextBoxCognome.Text;
        if (!string.IsNullOrEmpty(DropDownListTipoDocumento.SelectedValue))
            filtro.IdTipoDocumento = short.Parse(DropDownListTipoDocumento.SelectedValue);

        if (!string.IsNullOrEmpty(TextBoxDataScansioneDa.Text))
            filtro.DataScansioneDa = DateTime.Parse(TextBoxDataScansioneDa.Text.Replace('.', '/'));

        if (!string.IsNullOrEmpty(TextBoxDataScansioneA.Text))
            filtro.DataScansioneA = DateTime.Parse(TextBoxDataScansioneA.Text.Replace('.', '/'));

        if (!string.IsNullOrEmpty(TextBoxIdTipoPrestazione.Text))
            filtro.IdTipoPrestazione = (TextBoxIdTipoPrestazione.Text);

        if (!string.IsNullOrEmpty(TextBoxProtocolloPrestazione.Text))
            filtro.ProtocolloPrestazione = int.Parse(TextBoxProtocolloPrestazione.Text);

        if (!string.IsNullOrEmpty(TextBoxNumeroProcolloPrestazione.Text))
            filtro.NumeroProtocolloPrestazione = int.Parse(TextBoxNumeroProcolloPrestazione.Text);

        GridViewDocumenti.DataSource = biz.GetDocumenti(filtro);
        GridViewDocumenti.DataBind();
    }

    protected void GridViewDocumenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Documento documento = (Documento) e.Row.DataItem;

            Label lLavoratore = (Label) e.Row.FindControl("LabelLavoratore");
            Label lFamiliare = (Label) e.Row.FindControl("LabelFamiliare");

            lLavoratore.Text = String.Format("{0} {1}", documento.LavoratoreCognome, documento.LavoratoreNome);
            lFamiliare.Text = String.Format("{0} {1}", documento.FamiliareCognome, documento.FamiliareNome);
        }
    }

    protected void GridViewDocumenti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LabelErroreVisualizzazioneDocumento.Visible = false;

        Page.Validate("ricercaDocumenti");

        if (Page.IsValid)
        {
            GridViewDocumenti.PageIndex = e.NewPageIndex;
            CaricaDocumenti();
        }
    }

    protected void GridViewDocumenti_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int indice = Int32.Parse(e.CommandArgument.ToString());
        LabelErroreVisualizzazioneDocumento.Visible = false;

        switch (e.CommandName)
        {
            case "visualizza":
                string idArchidoc = (string) GridViewDocumenti.DataKeys[indice].Values["IdArchidoc"];

                // Caricare il documento tramite il Web Service Archidoc
                try
                {
                    IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                    byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                    if (file != null)
                    {
                        Presenter.RestituisciFileArchidoc(idArchidoc, file, Page);
                    }
                }
                catch
                {
                    LabelErroreVisualizzazioneDocumento.Visible = true;
                }
                break;
            case "associa":
                TipoDocumento tipoDocumento = (TipoDocumento) GridViewDocumenti.DataKeys[indice].Values["TipoDocumento"];
                int idDocumento = (int) GridViewDocumenti.DataKeys[indice].Values["IdDocumento"];

                Documento documento = biz.GetDocumento(tipoDocumento.IdTipoDocumento, idDocumento);
                documento.PerPrestazione = (bool) ViewState["PerPrestazione"];
                documento.RiferitoA = (string) ViewState["RelativoA"];

                if (OnDocumentoSelected != null)
                    OnDocumentoSelected(documento);
                break;
        }
    }
}