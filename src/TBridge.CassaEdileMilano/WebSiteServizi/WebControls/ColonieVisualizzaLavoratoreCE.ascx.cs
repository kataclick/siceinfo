using System;
using System.Web.UI;
using TBridge.Cemi.Colonie.Type.Entities;

public partial class WebControls_ColonieVisualizzaLavoratoreCE : UserControl
{
    private LavoratoreACE lavoratore;

    public LavoratoreACE Lavoratore
    {
        get { return GetLavoratore(); }
        set
        {
            lavoratore = value;
            if (lavoratore == null)
                Reset();
            else
                Set(lavoratore);
        }
    }

    public bool AttivaVisualizzazioneParziale { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void Set(LavoratoreACE lavoratore)
    {
        ViewState["IdLavoratore"] = lavoratore.IdLavoratore.Value;

        LabelCognome.Text = lavoratore.Cognome;
        LabelNome.Text = lavoratore.Nome;
        LabelDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
        LabelCodiceFiscale.Text = lavoratore.CodiceFiscale;
        LabelSesso.Text = lavoratore.Sesso.ToString();
        LabelTelefono.Text = lavoratore.Telefono;
        LabelCellulare.Text = lavoratore.Cellulare;
        LabelResidenzaIndirizzo.Text = lavoratore.Indirizzo;
        LabelResidenzaProvincia.Text = lavoratore.Provincia;
        LabelResidenzaComune.Text = lavoratore.Comune;
        LabelResidenzaCap.Text = lavoratore.Cap;
    }

    private void Reset()
    {
        LabelCognome.Text = null;
        LabelNome.Text = null;
        LabelDataNascita.Text = null;
        LabelCodiceFiscale.Text = null;
        LabelSesso.Text = null;
        LabelTelefono.Text = null;
        LabelCellulare.Text = null;
        LabelResidenzaIndirizzo.Text = null;
        LabelResidenzaProvincia.Text = null;
        LabelResidenzaComune.Text = null;
        LabelResidenzaCap.Text = null;
    }

    private LavoratoreACE GetLavoratore()
    {
        lavoratore = new LavoratoreACE();

        if (ViewState["IdLavoratore"] != null)
            lavoratore.IdLavoratore = (int) ViewState["IdLavoratore"];
        lavoratore.Cognome = LabelCognome.Text.Trim().ToUpper();
        lavoratore.Nome = LabelNome.Text.Trim().ToUpper();
        lavoratore.DataNascita = DateTime.Parse(LabelDataNascita.Text.Replace('.', '/'));
        lavoratore.CodiceFiscale = LabelCodiceFiscale.Text.Trim().ToUpper();
        if (LabelSesso.Text == "M")
            lavoratore.Sesso = 'M';
        else
            lavoratore.Sesso = 'F';
        if (!string.IsNullOrEmpty(LabelTelefono.Text.Trim()))
            lavoratore.Telefono = LabelTelefono.Text.Trim().ToUpper();
        if (!string.IsNullOrEmpty(LabelCellulare.Text.Trim()))
            lavoratore.Cellulare = LabelCellulare.Text.Trim().ToUpper();
        lavoratore.Indirizzo = LabelResidenzaIndirizzo.Text.Trim().ToUpper();
        lavoratore.Provincia = LabelResidenzaProvincia.Text.Trim().ToUpper();
        lavoratore.Comune = LabelResidenzaComune.Text.Trim().ToUpper();
        lavoratore.Cap = LabelResidenzaCap.Text;
        if (ViewState["IdCassaEdile"] != null)
            lavoratore.IdCassaEdile = (string) ViewState["IdCassaEdile"];

        return lavoratore;
    }
}