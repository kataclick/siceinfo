<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuVOD.ascx.cs" Inherits="WebControls_MenuVOD" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.VodElencoImpreseConsulenti)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.VodElencoImpreseMedie))
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            VCOD
        </td>
    </tr>
    <%

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.VodElencoImpreseMedie))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="ReportVOD.aspx?tipo=ImpreseMedie">Elenco Imprese con fasce</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
