#region

using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.AttestatoRegolarita.Type.Filters;
using TBridge.Cemi.Presenter;

#endregion

public partial class WebControls_AttestatoRegolaritaRiepilogoAttestati : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonVisualizza.Focus();
    }

    private DomandaFilter CreaFiltro()
    {
        DomandaFilter filtro = new DomandaFilter();

        filtro.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        if (!String.IsNullOrEmpty(TextBoxMese.Text))
            filtro.Mese = DateTime.ParseExact(TextBoxMese.Text, "MM/yyyy", null);
        filtro.IvaCodFisc = Presenter.NormalizzaCampoTesto(TextBoxPartitaIVA.Text);
        filtro.RagSocDenominazione = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);

        return filtro;
    }

    private void CaricaAttestati(Int32 pagina)
    {
        DomandaFilter filtro = CreaFiltro();

        DomandaCollection domColl = biz.CaricaAttestati(filtro);

        Presenter.CaricaElementiInGridView(
            GridViewRiepilogoAttestati,
            domColl,
            pagina);
    }

    protected void GridViewRiepilogoAttestati_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricercaAttestati");

        if (Page.IsValid)
        {
            CaricaAttestati(e.NewPageIndex);
        }
    }

    protected void GridViewRiepilogoAttestati_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Domanda domanda = (Domanda) e.Row.DataItem;

            Label lIndirizzoCivico = (Label) e.Row.FindControl("LabelIndirizzoCivico");
            Label lCapComuneProvinca = (Label) e.Row.FindControl("LabelCapComuneProvincia");
            Label lInfoAggiuntiva = (Label) e.Row.FindControl("LabelInfoAggiuntiva");

            lIndirizzoCivico.Text = String.Format("{0} {1}", domanda.Indirizzo, domanda.Civico);
            lCapComuneProvinca.Text = String.Format("{0} {1} {2}", domanda.Cap, domanda.Comune, domanda.Provincia);
            lInfoAggiuntiva.Text = domanda.InfoAggiuntiva;

            Label lRagioneSociale = (Label) e.Row.FindControl("LabelRagioneSociale");
            Label lCodiceFiscale = (Label) e.Row.FindControl("LabelCodiceFiscale");
            Label lPartitaIva = (Label) e.Row.FindControl("LabelPartitaIva");

            if (domanda.Committente.IdCommittente.HasValue)
            {
                if (!string.IsNullOrEmpty(domanda.Committente.RagioneSociale))
                    lRagioneSociale.Text = String.Format("{0}", domanda.Committente.RagioneSociale);
                else
                {
                    lRagioneSociale.Text = String.Format("{0} {1}", domanda.Committente.Nome,
                                                         domanda.Committente.Cognome);
                }
                lCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", domanda.Committente.CodiceFiscale);
                lPartitaIva.Text = String.Format(" P.Iva: {0}", domanda.Committente.PartitaIVA);
            }
            else
            {
                if (!string.IsNullOrEmpty(domanda.Committente.RagioneSociale))
                {
                    lRagioneSociale.Text = String.Format("{0}", domanda.Committente.RagioneSociale);
                    lCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", domanda.Committente.CodiceFiscale);
                    lPartitaIva.Text = String.Format(" P.Iva: {0}", domanda.Committente.PartitaIVA);
                }

                if (domanda.ImpresaRichiedente.IdImpresa.HasValue)
                {
                    lRagioneSociale.Text = String.Format("{0}", domanda.ImpresaRichiedente.RagioneSociale);
                    lCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", domanda.ImpresaRichiedente.CodiceFiscale);
                    lPartitaIva.Text = String.Format(" P.Iva: {0}", domanda.ImpresaRichiedente.PartitaIva);
                }
            }

            GridView gvImprese = (GridView) e.Row.FindControl("GridViewImprese");

            Presenter.CaricaElementiInGridView(
                gvImprese,
                domanda.ListaImprese,
                0);
        }
    }

    protected void GridViewImprese_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
    }

    protected void GridViewImprese_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Impresa impresa = (Impresa) e.Row.DataItem;

            Label lRagioneSociale = (Label) e.Row.FindControl("LabelRagioneSociale");
            Label lPartitaIva = (Label) e.Row.FindControl("LabelPartitaIva");
            Label lCodiceFiscale = (Label) e.Row.FindControl("LabelCodiceFiscale");

            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                lRagioneSociale.Text = String.Format("{0}-{1}", impresa.IdImpresa, impresa.RagioneSociale);
            }
            else
            {
                if (impresa.IdImpresaTrovata.HasValue)
                {
                    lRagioneSociale.Text = String.Format("{0}-{1}", impresa.IdImpresaTrovata, impresa.RagioneSociale);
                }
                else
                {
                    lRagioneSociale.Text = impresa.RagioneSociale;
                }
            }

            if (!String.IsNullOrEmpty(impresa.PartitaIva))
                lPartitaIva.Text = String.Format(" P.Iva: {0}", impresa.PartitaIva);
            if (!String.IsNullOrEmpty(impresa.CodiceFiscale))
                lCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", impresa.CodiceFiscale);
        }
    }

    protected void CustomValidatorMese_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime mese;

        if (DateTime.TryParseExact(TextBoxMese.Text, "MM/yyyy", null, DateTimeStyles.None, out mese))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaAttestati(0);
        }
    }
}