using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneCE.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class WebControls_IscrizioneCEContoCorrente : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaDomanda(ModuloIscrizione domanda)
    {
        TextBoxBancaIBAN.Text = domanda.IBAN;
        TextBoxBancaIntestatario.Text = domanda.IntestatarioConto;
    }

    public void CompletaDomandaConContoCorrente(ModuloIscrizione modulo)
    {
        // Conto corrente
        modulo.IBAN = Presenter.NormalizzaCampoTesto(TextBoxBancaIBAN.Text);
        modulo.IntestatarioConto = Presenter.NormalizzaCampoTesto(TextBoxBancaIntestatario.Text);
    }

    public void CaricaDatiProva()
    {
        TextBoxBancaIBAN.Text = "IT59F0529673970CC0000033409";
        TextBoxBancaIntestatario.Text = "pippo stock";
    }

    #region Custom Validators

    protected void CustomValidatorBancaIBAN_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string IBAN = Presenter.NormalizzaCampoTesto(TextBoxBancaIBAN.Text);

        bool ibanCorretto = false;

        try
        {
            ibanCorretto = IbanManager.VerificaCodiceIban(IBAN);
        }
        catch
        {
        }

        // Controllo che il codice IBAN sia corretto
        if (!string.IsNullOrEmpty(IBAN) && (!ibanCorretto || IBAN.ToUpper() == "IT73J0615513000000000012345"))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    #endregion
}