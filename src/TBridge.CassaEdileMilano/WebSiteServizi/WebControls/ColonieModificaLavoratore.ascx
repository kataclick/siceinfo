<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieModificaLavoratore.ascx.cs"
    Inherits="WebControls_ColonieModificaLavoratore" %>
<table class="standardTable">
    <tr>
        <td>
            Cognome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreCognome" runat="server"
                ErrorMessage="Cognome del lavoratore mancante" ControlToValidate="TextBoxCognome"
                ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreNome" runat="server"
                ErrorMessage="Nome del lavoratore mancante" ControlToValidate="TextBoxNome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita (gg/mm/aaaa)*
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataNascita" runat="server" Width="400px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreDataNascita" runat="server"
                ErrorMessage="Data di nascita del lavoratore mancante" ControlToValidate="TextBoxDataNascita"
                ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Formato della data di nascita del lavoratore non valido" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="400px" MaxLength="16"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreCodiceFiscale" runat="server"
                ErrorMessage="Codice fiscale del lavoratore mancante" ControlToValidate="TextBoxCodiceFiscale"
                ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
           <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ValidationGroup="lavoratore" ErrorMessage="Codice fiscale non valido" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate">
                *
            </asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidatorCodiceFiscaleCarattereControllo" runat="server"
                ControlToValidate="TextBoxCodiceFiscale" ValidationGroup="lavoratore" ErrorMessage="Codice di controllo del Codice fiscale non valido"
                OnServerValidate="CodiceFiscaleCarattereControllo_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Sesso*
        </td>
        <td>
            <asp:RadioButton ID="RadioButtonLavoratoreM" runat="server" GroupName="lavoratoreSesso"
                Checked="true" Text="M" />
            <asp:RadioButton ID="RadioButtonLavoratoreF" runat="server" GroupName="lavoratoreSesso"
                Text="F" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Telefono
        </td>
        <td>
            <asp:TextBox ID="TextBoxTelefono" runat="server" Width="400px" MaxLength="20"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Cellulare
        </td>
        <td>
            <asp:TextBox ID="TextBoxCellulare" runat="server" Width="400px" MaxLength="20"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Residenza
        </td>
        <td colspan="2">
            <table class="standardTable">
                <tr>
                    <td>
                        Indirizzo*
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxResidenzaIndirizzo" runat="server" Width="400px" MaxLength="60"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaIndirizz"
                            runat="server" ErrorMessage="Indirizzo di residenza del lavoratore mancante"
                            ControlToValidate="TextBoxResidenzaIndirizzo" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune*
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxResidenzaComune" runat="server" Width="400px" MaxLength="60"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaComune"
                            runat="server" ErrorMessage="Comune di residenza del lavoratore mancante" ControlToValidate="TextBoxResidenzaComune"
                            ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia*
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxResidenzaProvincia" runat="server" Width="400px" MaxLength="2"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaProvincia"
                            runat="server" ErrorMessage="Provincia di residenza del lavoratore mancante"
                            ControlToValidate="TextBoxResidenzaProvincia" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cap*
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxResidenzaCap" runat="server" Width="400px" MaxLength="5"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaCap" runat="server"
                            ErrorMessage="Cap di residenza del lavoratore mancante" ControlToValidate="TextBoxResidenzaCap"
                            ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorLavoratoreResidenzaCap"
                            runat="server" ControlToValidate="TextBoxResidenzaCap" ErrorMessage="Formato del CAP non valido"
                            ValidationExpression="^\d{5}$" ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<asp:Button ID="ButtonModifica" runat="server" OnClick="ButtonModifica_Click" Text="Modifica"
    ValidationGroup="lavoratore" /><asp:Button ID="ButtonAnnulla" runat="server" CausesValidation="False"
        OnClick="ButtonAnnulla_Click" Text="Annulla" />