﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

public partial class WebControls_AccessoCantieriAssegnazioneRilevatori : UserControl
{
    private const Int32 INDEX = 2;
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (ViewState["rilevatori"] == null)
            {
                ViewState["rilevatori"] = new RilevatoreCantiereCollection();
            }

            CaricaGridRilevatori();
        }

        CaricaTuttiRilevatori();
    }

    private void CaricaGridRilevatori()
    {
        if (ViewState["rilevatori"] != null)
        {
            GridViewRilevatoriCantiere.DataSource = ViewState["rilevatori"];
            GridViewRilevatoriCantiere.DataBind();
        }
    }

    private void CaricaTuttiRilevatori()
    {
        GridViewRilevatori.DataSource = _biz.GetRilevatoriLiberiCantieri();
        GridViewRilevatori.DataBind();
    }

    public void CaricaRilevatori(RilevatoreCantiereCollection rilevatori)
    {
        ViewState["rilevatori"] = rilevatori;
    }

    public RilevatoreCantiereCollection GetRilevatori()
    {
        return (RilevatoreCantiereCollection) ViewState["rilevatori"];
    }

    protected void GridViewRilevatori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RilevatoreCantiere rilevatoreCantiere = (RilevatoreCantiere) e.Row.DataItem;

            Label lCodice = (Label) e.Row.FindControl("LabelCodice");
            Label lSocieta = (Label) e.Row.FindControl("LabelSocieta");

            Label lCantiere = (Label) e.Row.FindControl("LabelCantiere");
            Label lPeriodo = (Label) e.Row.FindControl("LabelPeriodo");
            Label lReferente = (Label) e.Row.FindControl("LabelReferente");


            lCodice.Text = string.Format("Codice: {0}", rilevatoreCantiere.CodiceRilevatore);
            lSocieta.Text = string.Format("Codice: {0}", rilevatoreCantiere.RagioneSociale);

            {
                lCantiere.Text = "Non assegnato";
                lPeriodo.Text = lReferente.Text = "";
            }
        }
    }

    protected void GridViewRilevatori_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Select":
                if (GridViewRilevatori.DataKeys != null)
                {
                    Int32 idRilevatore = (Int32)
                                         GridViewRilevatori.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values[
                                             "IdRilevatore"];

                    String codice = (String)
                                    GridViewRilevatori.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values[
                                        "CodiceRilevatore"];

                    String ragioneSociale = (String)
                                            GridViewRilevatori.DataKeys[Int32.Parse(e.CommandArgument.ToString())].
                                                Values["RagioneSociale"];

                    DateTime dataInizio = DateTime.Now;

                    DateTime? dataFine = null;

                    Assegna(idRilevatore, codice, ragioneSociale, dataInizio, dataFine);
                }

                break;
        }
    }

    private void Assegna(Int32 idRilevatore, string codice, string ragioneSociale, DateTime dataInizio,
                         DateTime? dataFine)
    {
        RilevatoreCantiereCollection rilevatori = (RilevatoreCantiereCollection) ViewState["rilevatori"];

        RilevatoreCantiere rilevatore = new RilevatoreCantiere
                                            {
                                                IdRilevatore = idRilevatore,
                                                RagioneSociale = ragioneSociale,
                                                CodiceRilevatore = codice,
                                                DataInizio = dataInizio
                                            };

        if (dataFine.HasValue)
            rilevatore.DataFine = dataFine;

        rilevatori.Add(rilevatore);

        ViewState["rilevatori"] = rilevatori;

        CaricaGridRilevatori();
    }

    private void Termina(Int32 idRilevatore, string codice, string ragioneSociale, DateTime dataInizio,
                         DateTime? dataFine)
    {
        RilevatoreCantiereCollection rilevatori = (RilevatoreCantiereCollection) ViewState["rilevatori"];

        rilevatori.Termina(idRilevatore);

        ViewState["rilevatori"] = rilevatori;

        CaricaGridRilevatori();
    }

    protected void GridViewRilevatoriCantiere_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Select":
                if (GridViewRilevatoriCantiere.DataKeys != null)
                {
                    Int32 idRilevatore = (Int32)
                                         GridViewRilevatoriCantiere.DataKeys[Int32.Parse(e.CommandArgument.ToString())].
                                             Values["IdRilevatore"
                                             ];

                    String codice = (String)
                                    GridViewRilevatoriCantiere.DataKeys[Int32.Parse(e.CommandArgument.ToString())].
                                        Values[
                                            "CodiceRilevatore"];

                    String ragioneSociale = (String)
                                            GridViewRilevatoriCantiere.DataKeys[
                                                Int32.Parse(e.CommandArgument.ToString())].Values[
                                                    "RagioneSociale"];

                    DateTime dataInizio = (DateTime)
                                          GridViewRilevatoriCantiere.DataKeys[Int32.Parse(e.CommandArgument.ToString())]
                                              .Values["DataInizio"];

                    DateTime? dataFine = DateTime.Now;

                    Termina(idRilevatore, codice, ragioneSociale, dataInizio, dataFine);
                }

                break;
        }
    }

    protected void GridViewRilevatoriCantiere_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RilevatoreCantiere rilevatoreCantiere = (RilevatoreCantiere) e.Row.DataItem;

            Label lCodice = (Label) e.Row.FindControl("LabelCodice0");
            Label lSocieta = (Label) e.Row.FindControl("LabelSocieta0");

            Label lPeriodo = (Label) e.Row.FindControl("LabelPeriodo0");

            lCodice.Text = string.Format("Codice: {0}", rilevatoreCantiere.CodiceRilevatore);
            lSocieta.Text = string.Format("Codice: {0}", rilevatoreCantiere.RagioneSociale);

            if (rilevatoreCantiere.IdCantiere.HasValue)
            {
                if (rilevatoreCantiere.DataInizio != null)
                    lPeriodo.Text = string.Format("Inizio periodo: {0}",
                                                  rilevatoreCantiere.DataInizio.Value.ToShortDateString());

                if (rilevatoreCantiere.DataFine.HasValue)
                {
                    lPeriodo.Text = lPeriodo.Text + " " + string.Format("Fine periodo: {0}",
                                                                        rilevatoreCantiere.DataFine.Value.
                                                                            ToShortDateString());

                    e.Row.Cells[INDEX].Enabled = false;
                }
            }

            else
            {
                lPeriodo.Text = string.Format("Inizio periodo: {0}",
                                              DateTime.Now.ToShortDateString());

                if (rilevatoreCantiere.DataFine.HasValue)
                {
                    lPeriodo.Text = lPeriodo.Text + " " + string.Format("Fine periodo: {0}",
                                                                        rilevatoreCantiere.DataFine.Value.
                                                                            ToShortDateString());

                    e.Row.Cells[INDEX].Enabled = false;
                }
            }
        }
    }

    protected void GridViewRilevatoriCantiere_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
}