<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CantieriRicercaLavoratore.ascx.cs"
    Inherits="WebControls_CantieriRicercaLavoratore" %>
<asp:Panel ID="PanelRicercaLavoratori" runat="server" DefaultButton="ButtonVisualizza">
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td style="height: 20px">
                            <asp:RadioButton ID="RadioButtonSiceNew" runat="server" Text="Anagrafica SiceNew"
                                Checked="True" GroupName="scelta" />
                        </td>
                        <td style="height: 20px">
                            <asp:RadioButton ID="RadioButtonCantieri" runat="server" Text="Anagrafica Cantieri"
                                GroupName="scelta" />
                        </td>
                        <td style="height: 20px">
                            <asp:RadioButton ID="RadioButtonNotifiche" runat="server" Text="Anagrafica Notifiche"
                                GroupName="scelta" />
                        </td>
                        <td style="height: 20px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cognome
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxCognome"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Nome
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxNome"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Data di nascita (gg/mm/aaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                runat="server" ControlToValidate="TextBoxDataNascita" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Button ID="ButtonNuovo" runat="server" OnClick="ButtonNuovo_Click" Text="Nuovo"
                                CausesValidation="False" /><asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                    Text="Ricerca" ValidationGroup="ricercaLavoratori" /><asp:Button ID="ButtonChiudi"
                                        runat="server" OnClick="ButtonChiudi_Click" Text="X" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewLavoratori" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyNames="IdLavoratore,TipoLavoratore" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewLavoratori_SelectedIndexChanging" OnSorting="GridViewLavoratori_Sorting"
                    Width="100%" PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                            HtmlEncode="False">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun lavoratore trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
