﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuSOLDO.ascx.cs" Inherits="WebControls_MenuSOLDO" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%
    if (//GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente() || 
        TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOImpresa)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOConsulente)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOAmministratoreApplicativo)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOAmministratoreTecnico)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOOperatore))
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            SOLDO
        </td>
    </tr>
    <%
        if (//GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente() || 
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOImpresa)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOConsulente)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOAmministratoreApplicativo)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOAmministratoreTecnico)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SOLDOOperatore))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/SOLDO/SOLDO.aspx?direzione=SOLDO" runat="server">Accedi a SOLDO</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>