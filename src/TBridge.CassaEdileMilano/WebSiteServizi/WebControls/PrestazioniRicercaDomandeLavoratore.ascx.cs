using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Filters;
using System.Web.UI.HtmlControls;

public partial class WebControls_PrestazioniRicercaDomandeLavoratore : UserControl
{
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaStati();
            CaricaTipiPrestazione();
        }
    }

    private void CaricaTipiPrestazione()
    {
        DropDownListTipoPrestazione.Items.Clear();

        DropDownListTipoPrestazione.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListTipoPrestazione.DataSource = biz.GetTipiPrestazione();
        DropDownListTipoPrestazione.DataTextField = "Descrizione";
        DropDownListTipoPrestazione.DataValueField = "IdTipoPrestazione";
        DropDownListTipoPrestazione.DataBind();
    }

    private void CaricaStati()
    {
        DropDownListStato.Items.Clear();

        DropDownListStato.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListStato.DataSource = biz.GetStatiDomanda();
        DropDownListStato.DataTextField = "Descrizione";
        DropDownListStato.DataValueField = "IdStato";
        DropDownListStato.DataBind();
    }

    public void ImpostaIdLavoratore(int idLavoratore)
    {
        ViewState["IdLavoratore"] = idLavoratore;
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricerca");

        if (Page.IsValid)
        {
            GridViewDomande.PageIndex = e.NewPageIndex;
            CaricaDomande();
        }
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewDomande.PageIndex = 0;
            CaricaDomande();
        }
    }

    private void CaricaDomande()
    {
        DomandaFilter filtro = new DomandaFilter();

        filtro.IdLavoratore = (int) ViewState["IdLavoratore"];
        if (!string.IsNullOrEmpty(DropDownListStato.SelectedValue))
            filtro.Stato = DropDownListStato.SelectedValue[0];
        if (!string.IsNullOrEmpty(DropDownListTipoPrestazione.SelectedValue))
            filtro.IdTipoPrestazione = DropDownListTipoPrestazione.SelectedValue;
        if (!string.IsNullOrEmpty(TextBoxAnno.Text))
            filtro.Anno = Int32.Parse(TextBoxAnno.Text);

        DomandaCollection domande = biz.GetDomande(filtro);
        GridViewDomande.DataSource = domande;
        GridViewDomande.DataBind();
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Domanda domanda = (Domanda) e.Row.DataItem;

            Label lBeneficiario = (Label) e.Row.FindControl("LabelBeneficiario");
            Label lStato = (Label) e.Row.FindControl("LabelStato");
            Label lStatoCure = (Label) e.Row.FindControl("LabelStatoCure");
            Label lStatoProtesi = (Label) e.Row.FindControl("LabelStatoProtesi");

            HtmlTableRow trCure = (HtmlTableRow) e.Row.FindControl("trStatoCure");
            HtmlTableRow trProtesi = (HtmlTableRow) e.Row.FindControl("trStatoProtesi");

            // Se � una domanda protesi e cure recupero gli stati
            if (domanda.IdTipoPrestazione == "C004CP")
            {
                trCure.Visible = true;
                trProtesi.Visible = true;
                StatiCureProtesi stati = biz.GetStatiCureProtesi(domanda.IdDomanda.Value);

                if (stati != null)
                {
                    if (!String.IsNullOrEmpty(stati.StatoCL))
                    {
                        lStatoCure.Text = stati.StatoCL;
                    }
                    if (!String.IsNullOrEmpty(stati.StatoPL))
                    {
                        lStatoProtesi.Text = stati.StatoPL;
                    }
                }
            }

            lStato.Text = domanda.Stato.Descrizione;

            Button bModulo = (Button) e.Row.FindControl("ButtonModulo");
            if (domanda != null && domanda.IdDomanda.HasValue)
                bModulo.CommandArgument = e.Row.RowIndex.ToString();

            //Disabilitiamo il modulo
            if (domanda.Stato.IdStato == "A")
                bModulo.Enabled = false;

            if (domanda.Beneficiario != "L")
            {
                if (domanda.Familiare != null)
                {
                    lBeneficiario.Text = domanda.Familiare.NomeCompleto;
                }
                else
                {
                    if (domanda.FamiliareFornito != null)
                    {
                        lBeneficiario.Text = domanda.FamiliareFornito.NomeCompleto;
                    }
                }
            }
            else
            {
                lBeneficiario.Text = "LAVORATORE";
            }
        }
    }


    protected void ButtonModulo_Click(object sender, EventArgs e)
    {
        Button bConferma = (Button) sender;
        Int32 indiceRiga = ((GridViewRow) bConferma.Parent.Parent).RowIndex;

        Int32 idDomanda = (Int32) GridViewDomande.DataKeys[indiceRiga].Values["IdDomanda"];

        Context.Items["IdDomanda"] = idDomanda;
        Domanda d = biz.GetDomanda(idDomanda);

        Context.Items["TipoModulo"] = d.TipoModulo.Modulo;

        Configurazione conf = biz.GetConfigurazionePrestazione(
            d.TipoPrestazione.IdTipoPrestazione,
            d.Beneficiario,
            d.Lavoratore.IdLavoratore.Value);

        //switch (d.TipoModulo.IdTipoModulo)
        switch (conf.TipoMacroPrestazione.IdTipoMacroPrestazione)
        {
                // Prestazioni Sanitarie
            case 1:
                Server.Transfer("~/Prestazioni/ReportPrestazioniSanitarie.aspx");
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                Server.Transfer("~/Prestazioni/ReportPrestazioniScolastiche.aspx");
                break;
        }
    }
}