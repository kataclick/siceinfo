using System;
using System.Web.UI;
using TBridge.Cemi.Deleghe.Type.Entities;

public partial class WebControls_DelegheDatiDelega : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaDelega(Delega delega)
    {
        LabelSindacato.Text = delega.Sindacato.ToString();
        LabelComprensorio.Text = delega.ComprensorioSindacale.ToString();
        if (delega.DataConferma.HasValue)
            LabelMeseConferma.Text = delega.DataConferma.Value.ToString("MM/yyyy");
        LabelStato.Text = delega.Stato.ToString();

        LabelId.Text = delega.IdDelega.ToString();
        LabelCognome.Text = delega.Lavoratore.Cognome;
        LabelNome.Text = delega.Lavoratore.Nome;
        LabelDataNascita.Text = delega.Lavoratore.DataNascita.Value.ToShortDateString();
        LabelImpresa.Text = delega.Lavoratore.Impresa;

        LabelResidenzaIndirizzo.Text = delega.Lavoratore.Residenza.IndirizzoDenominazione;
        LabelResidenzaProvincia.Text = delega.Lavoratore.Residenza.Provincia;
        LabelResidenzaComune.Text = delega.Lavoratore.Residenza.Comune;
        LabelResidenzaCap.Text = delega.Lavoratore.Residenza.Cap;

        LabelCantiereIndirizzo.Text = delega.Cantiere.IndirizzoDenominazione;
        LabelCantiereProvincia.Text = delega.Cantiere.Provincia;
        LabelCantiereComune.Text = delega.Cantiere.Comune;
        LabelCantiereCap.Text = delega.Cantiere.Cap;
    }

    public void Reset()
    {
        LabelSindacato.Text = null;
        LabelComprensorio.Text = null;
        LabelMeseConferma.Text = null;
        LabelStato.Text = null;

        LabelId.Text = null;
        LabelCognome.Text = null;
        LabelNome.Text = null;
        LabelDataNascita.Text = null;
        LabelImpresa.Text = null;

        LabelResidenzaIndirizzo.Text = null;
        LabelResidenzaProvincia.Text = null;
        LabelResidenzaComune.Text = null;
        LabelResidenzaCap.Text = null;

        LabelCantiereIndirizzo.Text = null;
        LabelCantiereProvincia.Text = null;
        LabelCantiereComune.Text = null;
        LabelCantiereCap.Text = null;
    }

    public void ImpostaStato(string stato)
    {
        LabelStato.Text = stato;
    }
}