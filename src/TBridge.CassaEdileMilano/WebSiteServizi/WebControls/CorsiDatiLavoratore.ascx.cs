using System;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Enums;

public partial class WebControls_CorsiDatiLavoratore : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTitoliStudio();
            CaricaProvince();
            CaricaNazioni();
        }
    }

    private void CaricaNazioni()
    {
        ListDictionary nazioni = commonBiz.GetNazioni();

        // Cittadinanza
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCittadinanza,
            nazioni,
            "Value",
            "Key");

        // Paese di nascita
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListPaeseNascita,
            nazioni,
            "Value",
            "Key");
    }
    
    private void CaricaProvince()
    {
        StringCollection province = commonBiz.GetProvinceSiceNew();

        // Provincia residenza
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListProvinciaResidenza,
            province,
            null,
            null);

        // Provincia nascita
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListProvinciaNascita,
            province,
            null,
            null);
    }

    private void CaricaTitoliStudio()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTitoloStudio,
            biz.GetTitoliDiStudioAll(),
            "Descrizione",
            "IdTitoloStudio");
    }

    public Lavoratore GetLavoratore()
    {
        Lavoratore lavoratore = null;

        Page.Validate("datiLavoratore");
        if (Page.IsValid)
        {
            lavoratore = new Lavoratore();
            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
            lavoratore.Fonte = FonteAnagraficheComuni.Corsi;

            if (ViewState["IdLavoratore"] != null)
            {
                lavoratore.IdLavoratore = (Int32) ViewState["IdLavoratore"];
            }

            lavoratore.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
            lavoratore.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
            lavoratore.Sesso = RadioButtonListSesso.SelectedValue[0];
            lavoratore.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.','/'));
            lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

            if (!String.IsNullOrEmpty(DropDownListCittadinanza.SelectedValue))
            {
                lavoratore.Cittadinanza = DropDownListCittadinanza.SelectedValue;
            }

            if (!String.IsNullOrEmpty(DropDownListProvinciaResidenza.SelectedValue))
            {
                lavoratore.ProvinciaResidenza = DropDownListProvinciaResidenza.SelectedValue;
            }

            if (!String.IsNullOrEmpty(DropDownListPaeseNascita.SelectedValue))
            {
                lavoratore.PaeseNascita = DropDownListPaeseNascita.SelectedValue;
            }

            if (!String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue))
            {
                lavoratore.ProvinciaNascita = DropDownListProvinciaNascita.SelectedValue;
            }

            if (!String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue))
            {
                lavoratore.ComuneNascita = DropDownListComuneNascita.SelectedValue;
            }

            if (!String.IsNullOrEmpty(DropDownListTitoloStudio.SelectedValue))
            {
                lavoratore.TitoloStudio = new TitoloDiStudio();
                lavoratore.TitoloStudio.IdTitoloStudio = Int32.Parse(DropDownListTitoloStudio.SelectedItem.Value);
                lavoratore.TitoloStudio.Descrizione = DropDownListTitoloStudio.SelectedItem.Text;
            }

            lavoratore.PrimaEsperienza = CheckBoxPrimaEsperienza.Checked;
            if (!String.IsNullOrWhiteSpace(TextBoxDataAssunzione.Text))
            {
                DateTime dtAssunzione;

                if (DateTime.TryParseExact(TextBoxDataAssunzione.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dtAssunzione))
                {
                    lavoratore.DataAssunzione = dtAssunzione;
                }
            }
        }

        return lavoratore;
    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        RadioButtonListSesso.SelectedValue = "M";
        Presenter.SvuotaCampo(TextBoxDataNascita);

        Presenter.SvuotaCampo(TextBoxDataAssunzione);
        CheckBoxPrimaEsperienza.Checked = false;
    }

    public void DatiAssunzione(Boolean visibili)
    {
        trPrimaEsperienza.Visible = visibili;
        trDataAssunzione.Visible = visibili;
    }

    protected void DropDownListPaeseNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        GestisciPaeseNascita(null);
    }

    private void GestisciPaeseNascita(String paeseNascita)
    {
        if (DropDownListPaeseNascita.SelectedValue.ToUpper() == "1" || (!String.IsNullOrEmpty(paeseNascita) && paeseNascita == "1")) // ITALIA
        {
            DropDownListProvinciaNascita.Enabled = true;
            DropDownListComuneNascita.Enabled = true;
            DropDownListProvinciaNascita.CssClass = "";
            DropDownListComuneNascita.CssClass = "";
        }
        else
        {
            DropDownListProvinciaNascita.SelectedValue = String.Empty;
            DropDownListProvinciaNascita.Text = String.Empty;
            CaricaComuni(DropDownListComuneNascita, null);
            DropDownListProvinciaNascita.Enabled = false;
            DropDownListComuneNascita.Enabled = false;
            DropDownListProvinciaNascita.CssClass = "campoDisabilitato";
            DropDownListComuneNascita.CssClass = "campoDisabilitato";
        }
    }

    private void CaricaComuni(DropDownList dropDownListComuni, string provincia)
    {
        if (!string.IsNullOrEmpty(provincia))
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListComuneNascita,
                commonBiz.GetComuniSiceNew(provincia),
                "Comune",
                "CodiceCatastale");
        }
        else
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListComuneNascita,
                null,
                "",
                "");
        }
    }

    protected void DropDownListProvinciaNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaComuni(DropDownListComuneNascita, DropDownListProvinciaNascita.SelectedValue);
    }

    public void CaricaDatiLavoratore(Lavoratore lavoratore)
    {
        ResetCampi();

        TextBoxCognome.Text = lavoratore.Cognome;
        TextBoxNome.Text = lavoratore.Nome;
        if (lavoratore.DataNascita > new DateTime())
        {
            TextBoxDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
        }
        if (!String.IsNullOrEmpty(lavoratore.Sesso.ToString()))
        {
            RadioButtonListSesso.SelectedValue = lavoratore.Sesso.ToString();
        }
        TextBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;

        if (!String.IsNullOrEmpty(lavoratore.PaeseNascita))
        {
            DropDownListPaeseNascita.SelectedValue = lavoratore.PaeseNascita;

            GestisciPaeseNascita(lavoratore.PaeseNascita);
            if (!String.IsNullOrEmpty(lavoratore.ProvinciaNascita))
            {
                DropDownListProvinciaNascita.SelectedValue = lavoratore.ProvinciaNascita;
                CaricaComuni(DropDownListComuneNascita, lavoratore.ProvinciaNascita);
            }
            if (!String.IsNullOrEmpty(lavoratore.CodiceCatastaleComuneNascita))
            {
                DropDownListComuneNascita.SelectedValue = lavoratore.CodiceCatastaleComuneNascita;
            }
        }

        if (!String.IsNullOrEmpty(lavoratore.Cittadinanza))
        {
            DropDownListCittadinanza.SelectedValue = lavoratore.Cittadinanza;
        }
        if (!String.IsNullOrEmpty(lavoratore.ProvinciaResidenza))
        {
            DropDownListProvinciaResidenza.SelectedValue = lavoratore.ProvinciaResidenza;
        }

        if (lavoratore.PrimaEsperienza.HasValue)
        {
            CheckBoxPrimaEsperienza.Checked = lavoratore.PrimaEsperienza.Value;
        }
        if (lavoratore.DataAssunzione.HasValue)
        {
            TextBoxDataAssunzione.Text = lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy");
        }
    }
    
    public void CaricaDatiLavoratore(Int32 idLavoratore, Int32 idPartecipazione)
    {
        ResetCampi();

        Lavoratore lavoratore = biz.GetCorsiLavoratore(idLavoratore);
        //PartecipazioneCollection partecipazione = biz.GetPartecipazioni(idPartecipazione);

        ViewState["IdLavoratore"] = lavoratore.IdLavoratore.Value;
        TextBoxCognome.Text = lavoratore.Cognome;
        TextBoxNome.Text = lavoratore.Nome;
        TextBoxDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
        RadioButtonListSesso.SelectedValue = lavoratore.Sesso.ToString();
        TextBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;
        DropDownListPaeseNascita.SelectedValue = lavoratore.PaeseNascita;

        if (!String.IsNullOrEmpty(lavoratore.Cittadinanza))
        {
            DropDownListCittadinanza.SelectedValue = lavoratore.Cittadinanza;
        }
        if (!String.IsNullOrEmpty(lavoratore.ProvinciaResidenza))
        {
            DropDownListProvinciaResidenza.SelectedValue = lavoratore.ProvinciaResidenza;
        }

        GestisciPaeseNascita(lavoratore.PaeseNascita);
        if (!String.IsNullOrEmpty(lavoratore.ProvinciaNascita))
        {
            DropDownListProvinciaNascita.SelectedValue = lavoratore.ProvinciaNascita;
            CaricaComuni(DropDownListComuneNascita, lavoratore.ProvinciaNascita);
        }
        if (!String.IsNullOrEmpty(lavoratore.ComuneNascita))
        {
            DropDownListComuneNascita.SelectedValue = lavoratore.ComuneNascita;
        }
        if (lavoratore.TitoloStudio != null)
        {
            DropDownListTitoloStudio.SelectedValue = lavoratore.TitoloStudio.IdTitoloStudio.ToString();
        }

        Boolean? primaEsperienza;
        DateTime? dataAssunzione;

        biz.GetDatiAggiuntiviPartecipazione(idPartecipazione, out primaEsperienza, out dataAssunzione);

        if (primaEsperienza.HasValue)
        {
            CheckBoxPrimaEsperienza.Checked = primaEsperienza.Value;
        }
        if (dataAssunzione.HasValue)
        {
            TextBoxDataAssunzione.Text = dataAssunzione.Value.ToString("dd/MM/yyyy");
        }
    }

    protected void CustomValidatorComuneNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListPaeseNascita.SelectedValue == "1" && String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorProvinciaNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListPaeseNascita.SelectedValue == "1" && String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue))
        {
            args.IsValid = false;
        }
    }
}