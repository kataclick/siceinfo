<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptDettagliNotifica.ascx.cs"
    Inherits="WebControls_CptDettagliNotifica" %>
<table class="standardTable">
    <tr>
        <td colspan="2">
            <asp:Label ID="Label1" runat="server" Text="Notifica" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Protocollo
        </td>
        <td>
            <asp:TextBox ID="TextBoxIdNotifica" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Data notifica
        </td>
        <td>
            <asp:TextBox ID="TextBoxData" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Data inserimento
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataInserimento" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Utente inserimento
        </td>
        <td>
            <asp:TextBox ID="TextBoxUtente" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Natura opera
        </td>
        <td>
            <asp:TextBox ID="TextBoxNaturaOpera" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Committente
        </td>
        <td>
            <asp:TextBox ID="TextBoxCommittente" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzi
        </td>
        <td>
            <asp:TextBox ID="TextBoxIndirizzi" runat="server" Enabled="False" Width="300px" Height="152px"
                TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
</table>
