<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TuteScarpeMenu.ascx.cs" Inherits="WebControls_MenuTuteScarpeCompleto" %>
<%@ Register Src="TuteScarpeMenuStatistiche.ascx" TagName="TuteScarpeMenuStatistiche"
    TagPrefix="uc4" %>
<%@ Register Src="MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc2" %>
<%@ Register Src="MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc3" %>
<uc1:MenuTuteScarpe ID="MenuTuteScarpe1" runat="server" />
<uc2:MenuGestioneOrdini ID="MenuGestioneOrdini1" runat="server" />
<uc3:MenuGestioneFatturazione ID="MenuGestioneFatturazione1" runat="server" />
<uc4:TuteScarpeMenuStatistiche ID="TuteScarpeMenuStatistiche1" runat="server" />