<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCpt.ascx.cs" Inherits="WebControls_MenuCpt" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptRicerca.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptRicercaLodi.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptAnnullamentoNotifica.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptStatistiche.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptEstrazioneExcelCantieri.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptEstrazioneBrescia.ToString())
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Notifiche</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi.ToString())
            )
    {                  
    %>
    <tr>
        <td>
        <a id="A1" href="~/Cpt/CptInserimentoNotifica.aspx" runat="server">Inserimento notifica</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A2" href="~/Cpt/CptRicercaPerAggiornamento.aspx" runat="server">Aggiornamento notifica</a>
        </td>
    </tr>
    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptAnnullamentoNotifica.ToString()))
    {                  
    %>
    <tr>
        <td>
        <a id="A3" href="~/Cpt/CptAnnullaNotifica.aspx" runat="server">Annullamento notifica</a>
        </td>
    </tr>
    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptRicerca.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptRicercaLodi.ToString())
        )
    {                  
    %>
     <tr>
        <td>
        <a id="A4" href="~/Cpt/CptRicercaNotifiche.aspx" runat="server">Ricerca notifiche</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A5" href="~/Cpt/CptRicercaCantiere.aspx" runat="server">Ricerca cantieri</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A6" href="~/Cpt/CptLocalizzazione.aspx" runat="server">Localizzazione cantieri</a>
        </td>
    </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptAttivitaASLVisualizzazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione.ToString())
    )
    {                  
    %>
     <tr>
        <td>
        <a id="A7" href="~/Cpt/RicercaVisite.aspx" runat="server">Ricerca visite</a>
        </td>
    </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptStatistiche.ToString()))
    {                  
    %>
        <tr>
            <td>
                <a id="A8" href="~/Cpt/Statistiche.aspx" runat="server">Statistiche</a>
            </td>
        </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptEstrazioneBrescia.ToString()))
    {                  
    %>
        <tr>
            <td>
                <a id="A9" href="~/Cpt/EstrazioneBrescia.aspx" runat="server">Estrazione Brescia</a>
            </td>
        </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CptEstrazioneAssimpredil.ToString()))
    {                  
    %>
        <tr>
            <td>
                <a id="A10" href="~/Cpt/EstrazioneAssimpredil.aspx" runat="server">Estrazione Assimpredil</a>
            </td>
        </tr>
    <%
    }
     %>
</table>
<% 
    }
    %>
