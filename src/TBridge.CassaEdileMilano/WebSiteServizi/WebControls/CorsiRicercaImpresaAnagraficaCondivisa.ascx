﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiRicercaImpresaAnagraficaCondivisa.ascx.cs"
    Inherits="WebControls_CorsiRicercaImpresaAnagraficaCondivisa" %>
<asp:Panel ID="PanelRicercaLavoratori" runat="server" DefaultButton="ButtonVisualizza"
    Width="100%">
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Codice
                            <asp:CompareValidator ID="CompareValidatorCodice" runat="server" Type="Integer" Operator="DataTypeCheck"
                                ValidationGroup="ricercaImprese" ControlToValidate="TextBoxCodice">*</asp:CompareValidator>
                        </td>
                        <td>
                            Ragione sociale
                        </td>
                        <td>
                            Partita IVA/Cod. fisc.
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIvaFisc" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaImprese" />
                            <asp:Button ID="ButtonNuovo" runat="server" OnClick="ButtonNuovo_Click" Text="Nuova"
                                CausesValidation="False" style="height: 21px" Visible="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                    Width="100%" PageSize="5" AllowPaging="True" DataKeyNames="TipoImpresa,IdAnagraficaCondivisa" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewLavoratori_SelectedIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="Codice" DataField="IdImpresa">
                        <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" ItemStyle-Font-Bold="true" />
                        <asp:BoundField DataField="PartitaIva" HeaderText="P. Iva">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Cod. fisc.">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna impresa trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
