<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCorsi.ascx.cs" Inherits="WebControls_MenuCorsi" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiGestione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneImpresa.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiEstrazioneFormedil.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneConsulente.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiStatisticheAttestati.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiGestioneDomandePrestazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiPrestazioniDTA.ToString())
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Iscrizione Corsi
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiGestione.ToString()))
        {
    %>
        <tr>
            <td>
                <a id="A1" href="~/Corsi/AnagraficaLocazioni.aspx" runat="server">Anagrafica locazioni</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A2" href="~/Corsi/AnagraficaCorsi.aspx" runat="server">Anagrafica corsi</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A3" href="~/Corsi/PianificazioneCorsi.aspx" runat="server">Pianificazione</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A4" href="~/Corsi/IscrizioneCorsi.aspx" runat="server">Iscrizione</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A5" href="~/Corsi/GestionePartecipantiCorsi.aspx" runat="server">Gestione partecipanti</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A8" href="~/Corsi/RicercaLavoratoreImpresa.aspx" runat="server">Ricerca lavoratore/impresa</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore())
            )
        {
    %>
        <tr>
            <td>
                <a id="A6" href="~/Corsi/LavoratoreCorsi.aspx" runat="server">Corsi svolti/programmati</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneImpresa.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
            )
        {
    %>
        <tr>
            <td>
                <a id="A9" href="~/Corsi/IscrizioneCorsiImpresa.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneConsulente.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            )
        {
    %>
        <tr>
            <td>
                <a id="A11" href="~/Corsi/IscrizioneCorsiConsulente.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
            )
        {
    %>
        <tr>
            <td>
                <a id="A7" href="~/Corsi/ImpresaCorsi.aspx" runat="server">Corsi dei lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            )
        {
    %>
        <tr>
            <td>
                <a id="A12" href="~/Corsi/ConsulenteCorsi.aspx" runat="server">Corsi dei lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiEstrazioneFormedil.ToString())
            )
        {
    %>
        <tr>
            <td>
                <a id="A10" href="~/Corsi/EstrazioneFormedil.aspx" runat="server">Estrazione per Formedil</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiStatisticheAttestati.ToString())
            )
        {
    %>
        <tr>
            <td>
                <a id="A13" href="~/Corsi/StatisticheAttestati.aspx" runat="server">Statistiche attestati</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiGestioneDomandePrestazione.ToString())
            )
        {
    %>
        <tr>
            <td>
                <a id="A14" href="~/Corsi/GestioneCorsiPrestazioniDomande.aspx" runat="server">Gestione prestazioni</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiPrestazioniDTA.ToString())
            )
        {
    %>
        <tr>
            <td>
                <a id="A15" href="~/Corsi/PrestazioniDTA.aspx" runat="server">Prestazioni DTA</a>
            </td>
        </tr>
    <%
        }
    %>
    
</table>
<%
    }
%>
