#region

using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
//using TBridge.Cemi.GestioneUtenti.Business.Identities;

#endregion

public partial class WebControls_AttestatoRegolaritaRicerca : UserControl
{
    private const Int32 INDICEATTESTATO = 6;
    private const Int32 INDICEMODIFICA = 4;
    private const Int32 INDICEVISUALIZZA = 5;
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();
    private Int32 mesiAvvioControlli = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["mesiAvvioControlli"] = biz.GetMesiAvvioControlli();
        }

        mesiAvvioControlli = (Int32) ViewState["mesiAvvioControlli"];
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaAttestati(0);
        }
    }

    private void CaricaAttestati(Int32 pagina)
    {
        DomandaFilter filtro = CreaFiltro();

        DomandaCollection domande = biz.GetDomandeByFilter(filtro);
        Presenter.CaricaElementiInGridView(
            GridViewAttestati,
            domande,
            pagina);
    }

    private DomandaFilter CreaFiltro()
    {
        DomandaFilter filtro = new DomandaFilter();

        filtro.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        filtro.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        if (!String.IsNullOrEmpty(TextBoxMese.Text))
            filtro.Mese = DateTime.ParseExact(TextBoxMese.Text, "MM/yyyy", null);

        if (!GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AttestatoRegolaritaGestioneCE.ToString()))
        {
            filtro.IdUtente = GestioneUtentiBiz.GetIdUtente();
        }

        return filtro;
    }

    protected void GridViewAttestati_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricercaAttestati");

        if (Page.IsValid)
        {
            CaricaAttestati(e.NewPageIndex);
        }
    }

    protected void GridViewAttestati_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idDomanda = (Int32) GridViewAttestati.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];
        Context.Items["IdDomanda"] = idDomanda;

        Server.Transfer("~/AttestatoRegolarita/VisualizzaDomanda.aspx");
    }

    protected void GridViewAttestati_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Int32 idDomanda = (Int32) GridViewAttestati.DataKeys[e.NewEditIndex].Values["IdDomanda"];
        Context.Items["IdDomanda"] = idDomanda;

        Server.Transfer("~/AttestatoRegolarita/richiestaAttestato.aspx");
    }

    protected void GridViewAttestati_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 idDomanda;

        switch (e.CommandName)
        {
            case "StampaAttestato":
                idDomanda =
                    (Int32) GridViewAttestati.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["IdDomanda"];
                Context.Items["IdDomanda"] = idDomanda;
                Server.Transfer("~/AttestatoRegolarita/ReportAttestatoRegolarita.aspx");
                break;
            case "Visualizza":
                idDomanda =
                    (Int32)
                    GridViewAttestati.DataKeys[Int32.Parse(e.CommandArgument.ToString())%GridViewAttestati.PageSize].
                        Values["IdDomanda"];
                Context.Items["IdDomanda"] = idDomanda;
                Server.Transfer("~/AttestatoRegolarita/VisualizzaDomanda.aspx");
                break;
            case "Copia":
                idDomanda =
                    (Int32)
                    GridViewAttestati.DataKeys[Int32.Parse(e.CommandArgument.ToString())%GridViewAttestati.PageSize].
                        Values["IdDomanda"];
                Context.Items["IdDomanda"] = idDomanda;

                Server.Transfer("~/AttestatoRegolarita/richiestaAttestato.aspx?modalita=copia");
                break;
        }
    }

    protected void GridViewAttestati_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Domanda domanda = (Domanda) e.Row.DataItem;
            Label lStato = (Label) e.Row.FindControl("LabelStato");
            Label lIndirizzo = (Label) e.Row.FindControl("LabelIndirizzo");
            Label lComune = (Label) e.Row.FindControl("LabelComune");
            Label lProvincia = (Label) e.Row.FindControl("LabelProvincia");
            Label lRichiedente = (Label) e.Row.FindControl("LabelRichiedente");
            Button bCopia = (Button) e.Row.FindControl("ButtonCopia");

            lIndirizzo.Text = domanda.Indirizzo;
            lComune.Text = domanda.Comune;
            lProvincia.Text = domanda.Provincia;

            if (domanda.ImpresaRichiedente != null)
            {
                lRichiedente.Text = String.Format("Impresa - {0} {1}",
                                                  domanda.ImpresaRichiedente.IdImpresa,
                                                  domanda.ImpresaRichiedente.RagioneSociale);
            }
            else
            {
                if (domanda.CommittenteRichiedente != null)
                {
                    lRichiedente.Text = String.Format("Committente - {0}",
                                                      domanda.CommittenteRichiedente.RagioneSociale);
                }
                else
                {
                    lRichiedente.Text = domanda.LoginUtente;
                }
            }

            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            if (domanda.DataControllo.HasValue
                || !biz.SiPuoModificare(domanda.Mese, mesiAvvioControlli)
                // Se � l'utente in visualizzazione � diverso dall'utente dell'inserimento non permetto la modifica
                || idUtente != domanda.IdUtente
                )
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = false;

                if (idUtente != domanda.IdUtente)
                {
                    bCopia.Enabled = false;
                }
            }

            if (!domanda.DataControllo.HasValue)
            {
                e.Row.Cells[INDICEATTESTATO].Enabled = false;
            }

            if (!domanda.DataControllo.HasValue)
            {
                if (!biz.SiPuoModificare(domanda.Mese, mesiAvvioControlli))
                {
                    lStato.Text = "Da Valutare";
                }
                else
                {
                    lStato.Text = String.Empty;
                }
                (e.Row.Cells[INDICEATTESTATO].Controls[0]).Visible = false;
            }
            else
            {
                if (domanda.AttestatoRilasciabile.HasValue)
                {
                    if (domanda.AttestatoRilasciabile.Value)
                    {
                        lStato.Text = "Attestato rilasciato";
                    }
                    else
                    {
                        lStato.Text = "Anomalie";
                        ((Button) e.Row.Cells[INDICEATTESTATO].Controls[0]).Text = "Anomalie";
                    }
                }
                else
                {
                    lStato.Text = "Da valutare";
                }
            }
        }
    }

    protected void CustomValidatorMese_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime mese;

        if (DateTime.TryParseExact(TextBoxMese.Text, "MM/yyyy", null, DateTimeStyles.None, out mese))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
}