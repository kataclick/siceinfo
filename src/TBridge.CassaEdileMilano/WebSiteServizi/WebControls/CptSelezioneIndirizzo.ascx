<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptSelezioneIndirizzo.ascx.cs" Inherits="WebControls_CptSelezioneIndirizzo" %>
<asp:UpdatePanel ID="UpdatePanelIndirizzo" runat="server">
        <ContentTemplate>
        <table class="standardTable">
        <tr>
            <td>
                <asp:TextBox ID="TextBoxIndirizzoSel" runat="server" Height="50px" MaxLength="255" TextMode="MultiLine"
                    Width="395px" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelNuovoIndirizzo" runat="server" Visible="true">
                <table class="filledtable">
                    <tr>
                        <td style="height: 16px">
                            <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="Compilazione indirizzo"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            Indirizzo* 
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="250px"></asp:TextBox>
                            
                        </td>
                        <td style="width: 69px">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Campo obbligatorio" ControlToValidate="TextBoxIndirizzo" ValidationGroup="inserimentoIndirizzoControllo"></asp:RequiredFieldValidator>
                        </td>
                        
                        
                    </tr>
                    <tr>
                        <td>
                            Civico
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxCivico" runat="server" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" Width="250px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Campo obbligatorio" ControlToValidate="TextBoxComune" ValidationGroup="inserimentoIndirizzoControllo"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Provincia
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxProvincia" runat="server" Width="250px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Cap
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxCap" runat="server" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <!--<tr>
                        <td>
                            Latitudine
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxLatitudine" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Longitudine
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxLongitudine" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>-->
                    <tr>
                        <td>
                                    &nbsp;
                        </td>
                        <td colspan="2">
                            <asp:UpdatePanel ID="UpdatePanelGeocodifica" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="ButtonGeocoder" runat="server" Text="Geocodifica" Width="300px" ValidationGroup="inserimentoIndirizzoControllo" OnClick="ButtonGeocoder_Click" />
                                <asp:Panel ID="PanelIndirizzoGeo" runat="server" Width="125px" Visible="false">
                            
                                    <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnSelectedIndexChanging="GridViewIndirizzi_SelectedIndexChanging" Width="300px">
                                        <Columns>
                                            <asp:BoundField DataField="Provincia">
                                                <ItemStyle Font-Size="XX-Small" Width="20px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Comune">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IndirizzoDenominazione">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemStyle Width="20px" />
                                                <ItemTemplate>
                                                    <asp:Button ID="ButtonSeleziona" runat="server" CommandName="Select" Font-Size="XX-Small"
                                                        Height="18px" Text="Seleziona" Width="56px" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="Non � possibile geocodificare o il server non � disponibile"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>                                   
                    </tr>
                    <tr>
                        <td>
                            *campi obbligatori</td>
                        <td colspan="2" align="right">
                            <asp:Button ID="ButtonResettaCampi" runat="server" Text="Azzera campi" Width="150px" OnClick="ButtonResettaCampi_Click" CausesValidation="False" /><asp:Button ID="ButtonAggiungiIndirizzo" runat="server" OnClick="ButtonAggiungiIndirizzo_Click" Text="Salva indirizzo" Width="150px" ValidationGroup="inserimentoIndirizzoControllo" />
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </td>
        </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel>
