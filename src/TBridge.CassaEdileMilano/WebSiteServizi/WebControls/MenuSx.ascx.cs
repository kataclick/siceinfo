using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class WebControls_MenuSx : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciErrori.ToString())
            || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSImmediati.ToString())
            || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSInviati.ToString())
            || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSScadenza.ToString()))
        {
            Controls.Add(LoadControl("~/WebControls/SmsInfoMenu.ascx"));
        }
    }
}