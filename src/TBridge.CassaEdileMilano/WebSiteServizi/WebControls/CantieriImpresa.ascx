<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CantieriImpresa.ascx.cs"
    Inherits="WebControls_CantieriImpresa" %>
<asp:Panel ID="PanelCodiceImpresa" runat="server" Visible="False" Width="220px">
    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
    <asp:TextBox ID="TextBoxIdImpresa" runat="server" Enabled="False" ReadOnly="True"
        Width="100px"></asp:TextBox></asp:Panel>
<table class="standardTable">
    <tr>
        <td>
            <asp:Label ID="LabelRagioneSociale" runat="server" Text="Ragione sociale*"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="353px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxRagioneSociale"
                ErrorMessage="Digitare una ragione sociale" ValidationGroup="ValidaInserimentoImpresa"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelPartitaIVA" runat="server" Text="Partita IVA"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPartitaIva" runat="server" MaxLength="11" Width="353px"></asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxPartitaIva"
                ErrorMessage="Partita IVA errata" ValidationExpression="^\d{11}$" ValidationGroup="ValidaInserimentoImpresa"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelCodiceFiscale" runat="server" Text="Codice fiscale"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="353px"></asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale errato" ValidationExpression="^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z][A-Z0-9]{4}"
                ValidationGroup="ValidaInserimentoImpresa"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Artigiano
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxArtigiano" runat="server" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Telefono"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxTelefono" runat="server" MaxLength="50" Width="353px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="Fax"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxFax" runat="server" MaxLength="50" Width="353px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="Persona di riferimento"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPersonaRiferimento" runat="server" MaxLength="100" Width="353px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="Tipologia attivit�"></asp:Label>
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxTipoAttivita" runat="server" Enabled="false" Width="353px"
                            Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="DropDownListTipoAttivita" runat="server" Width="353px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelIndirizzo" runat="server" Text="Indirizzo*"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxIndirizzo" runat="server" Height="41px" MaxLength="255" TextMode="MultiLine"
                Width="353px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndirizzo" runat="server" ControlToValidate="TextBoxIndirizzo"
                ErrorMessage="Digitare un indirizzo" ValidationGroup="ValidaInserimentoImpresa"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Provincia*
        </td>
        <td>
            <asp:DropDownList ID="DropDownListProvincia" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged"
                Width="353px" AppendDataBoundItems="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListProvincia"
                ErrorMessage="Scegliere una provincia" ValidationGroup="ValidaInserimentoImpresa"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Comune*
        </td>
        <td>
            <asp:DropDownList ID="DropDownListComuni" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                Width="353px" AppendDataBoundItems="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Label ID="LabelComune" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListComuni"
                ErrorMessage="Scegliere un comune" ValidationGroup="ValidaInserimentoImpresa"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Cap*
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCAP" runat="server" Width="353px" AppendDataBoundItems="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Label ID="LabelCap" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownListCAP"
                ErrorMessage="Scegliere un cap" ValidationGroup="ValidaInserimentoImpresa"></asp:RequiredFieldValidator>
        </td>
    </tr>
</table>
* campi obbligatori