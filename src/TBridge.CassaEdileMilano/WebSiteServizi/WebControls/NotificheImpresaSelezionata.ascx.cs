using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Notifiche.Business;
using TBridge.Cemi.Notifiche.Data.Delegates;
//using TBridge.Cemi.GestioneUtenti.Business.Identities;

public partial class WebControls_NotificheImpresaSelezionata : UserControl
{
    private NotificaBusiness notificaBiz;
    public event ImpresaSelectedEventHandler OnImpresaSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                notificaBiz = new NotificaBusiness();

                //TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente consulente =
                //     ((Consulente)HttpContext.Current.User.Identity).Entity;
                Consulente consulente =
                    (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idConsulente = consulente.IdConsulente;

                CaricaImpreseAssociateAlConsulente(idConsulente);
                CaricaImpresaSelezionata();
            }
        }
    }

    private void CaricaImpresaSelezionata()
    {
        int? idImpresa = null;
        string ragioneSociale = null;

        if (Session["idImpresa"] != null)
            idImpresa = (int) Session["idImpresa"];

        if (Session["ragioneSociale"] != null)
            ragioneSociale = (string) Session["ragioneSociale"];

        if ((idImpresa != null) && (ragioneSociale != null))
            LabelImpresaSelezionata.Text = ragioneSociale;
        else
            LabelImpresaSelezionata.Text = string.Empty;
    }

    protected void ButtonSeleziona_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(DropDownListImprese.SelectedValue))
        {
            Session["idImpresa"] = Int32.Parse(DropDownListImprese.SelectedItem.Value);
            Session["ragioneSociale"] = DropDownListImprese.SelectedItem.Text;

            DropDownListImprese.SelectedIndex = 0;
        }
        else
        {
            Session["idImpresa"] = null;
            Session["ragioneSociale"] = null;
        }

        CaricaImpresaSelezionata();

        if (OnImpresaSelected != null)
            OnImpresaSelected();
    }

    private void CaricaImpreseAssociateAlConsulente(int idConsulente)
    {
        DropDownListImprese.Items.Clear();

        DropDownListImprese.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListImprese.DataSource = notificaBiz.ImpreseConsulente(idConsulente);
        DropDownListImprese.DataTextField = "NomeComposto";
        DropDownListImprese.DataValueField = "IdImpresa";
        DropDownListImprese.DataBind();
    }
}