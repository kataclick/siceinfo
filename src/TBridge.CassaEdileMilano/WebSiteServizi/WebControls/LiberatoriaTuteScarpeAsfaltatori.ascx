<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LiberatoriaTuteScarpeAsfaltatori.ascx.cs"
    Inherits="WebControls_LiberatoriaTuteScarpeAsfaltatori" %>
<table class="standardTable">
    <tr>
        <td colspan="2">
            <div>
                Per richiedere l�abbigliamento asfaltista, la sottoscritta impresa
                <asp:Label ID="LabelRagioneSocialeImpresa" runat="server" Font-Bold="True"></asp:Label>
                conferma di essere qualificata SOA nella categoria OG3, ovvero codice ATECO 42.11.00 (costruzione strade),
                42.12.00 (costruzione linee ferroviarie e metropolitane), 42.13.00 (costruzione ponti e gallerie).
                <br />
            </div>
        </td>
    </tr>
</table>
