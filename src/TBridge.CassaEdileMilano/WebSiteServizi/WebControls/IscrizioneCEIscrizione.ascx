﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneCEIscrizione.ascx.cs"
    Inherits="WebControls_IscrizioneCEIscrizione" %>
<%@ Register Src="IscrizioneCERicercaConsulenti.ascx" TagName="IscrizioneCERicercaConsulenti"
    TagPrefix="uc4" %>
<%@ Register Src="IscrizioneCEDatiGenerali.ascx" TagName="IscrizioneCEDatiGenerali"
    TagPrefix="uc1" %>
<%@ Register Src="IscrizioneCESedi.ascx" TagName="IscrizioneCESedi" TagPrefix="uc2" %>
<%@ Register Src="IscrizioneCEContoCorrente.ascx" TagName="IscrizioneCEContoCorrente"
    TagPrefix="uc3" %>
<%@ Register Src="IscrizioneCELegaleRappresentante.ascx" TagName="IscrizioneCELegaleRappresentante"
    TagPrefix="uc5" %>
<%@ Register Src="IscrizioneCELavoratori.ascx" TagName="IscrizioneCELavoratori" TagPrefix="uc6" %>
<%@ Register Src="IscrizioneCEImpiegati.ascx" TagName="IscrizioneCEImpiegati" TagPrefix="uc7" %>
<%@ Register Src="IscrizioneCECantiere.ascx" TagName="IscrizioneCECantiere" TagPrefix="uc8" %>
<asp:Wizard ID="WizardInserimento" runat="server" ActiveStepIndex="0" Height="200px"
    Width="100%" StartNextButtonText="Avanti" StepNextButtonText="Avanti" StepPreviousButtonText="Indietro"
    FinishCompleteButtonText="Inserisci" FinishPreviousButtonText="Indietro" OnSideBarButtonClick="WizardInserimento_SideBarButtonClick"
    OnNextButtonClick="WizardInserimento_NextButtonClick" OnPreviousButtonClick="WizardInserimento_PreviousButtonClick"
    OnFinishButtonClick="WizardInserimento_FinishButtonClick">
    <StepStyle VerticalAlign="Top" />
    <StartNavigationTemplate>
        <asp:Button ID="btnSuccessivoStart" runat="server" Text="Avanti" CommandName="MoveNext"
            Width="100px" CausesValidation="true" TabIndex="1" ValidationGroup="stop" />
    </StartNavigationTemplate>
    <StepNavigationTemplate>
        <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
            Width="100px" CausesValidation="true" TabIndex="2" ValidationGroup="stop" />
        <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
            CausesValidation="true" Width="100px" TabIndex="1" ValidationGroup="stop" />
    </StepNavigationTemplate>
    <FinishNavigationTemplate>
        <asp:Button ID="btnPrecedenteFinish" runat="server" Text="Indietro" CommandName="MovePrevious"
            Width="100px" CausesValidation="true" TabIndex="2" ValidationGroup="stop" />
        <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Conferma domanda"
            CausesValidation="true" Width="150px" TabIndex="1" ValidationGroup="stop" />
    </FinishNavigationTemplate>
    <WizardSteps>
        <asp:WizardStep ID="WizardStepDatiGenerali" runat="server" Title="- Dati generali"
            StepType="Start">
            <!-- DATI GENERALI -->
            <table class="standardTable">
                <tr>
                    <td>
                        <uc1:IscrizioneCEDatiGenerali ID="IscrizioneCEDatiGenerali1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSummaryDatiGenerali" runat="server" Width="500px">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryDatiGenerali" runat="server" ValidationGroup="datiGenerali"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryDatiGeneraliStop" runat="server" ValidationGroup="stop"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStepConsulente" runat="server" StepType="Step" Title="- Consulente">
            <!-- CONSULENTE -->
            <asp:Panel ID="PanelConsulenteSelezione" runat="server">
                <table class="standardTable">
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="Label1" runat="server" Text="Consulente" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Consulente:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxConsulenteSelezionato" runat="server" Height="77px" TextMode="MultiLine"
                                Width="350px" Enabled="False"></asp:TextBox>
                            <asp:Button ID="ButtonConsulenteSeleziona" runat="server" Text="Seleziona" OnClick="ButtonConsulenteSeleziona_Click" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="PanelSelezionaConsulente" runat="server" Visible="false">
                                <br />
                                &nbsp;<uc4:IscrizioneCERicercaConsulenti ID="TuteScarpeRicercaConsulenti1" runat="server" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStepSedi" runat="server" Title="- Sedi" StepType="Step">
            <!-- SEDI -->
            <table class="standardTable">
                <tr>
                    <td>
                        <uc2:IscrizioneCESedi ID="IscrizioneCESedi1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSummarySedi" runat="server" Width="500px">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummarySedi" runat="server" ValidationGroup="sedi"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummarySediStop" runat="server" ValidationGroup="stop"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStepBanca" runat="server" Title="- Conto corrente" StepType="Step">
            <!-- BANCA -->
            <table class="standardTable">
                <tr>
                    <td>
                        <uc3:IscrizioneCEContoCorrente ID="IscrizioneCEContoCorrente1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSummaryBanca" runat="server" Width="500px">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryBanca" runat="server" ValidationGroup="banca"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryBancaStop" runat="server" ValidationGroup="stop"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStepLegaleRappresentante" runat="server" Title="- Legale rappresentante"
            StepType="Step">
            <!-- LEGALE RAPPRESENTANTE -->
            <table class="standardTable">
                <tr>
                    <td>
                        <uc5:IscrizioneCELegaleRappresentante ID="IscrizioneCELegaleRappresentante1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSummaryLegaleRappresentante" runat="server" Width="500px">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryLegaleRappresentante" runat="server"
                                            ValidationGroup="legaleRappresentante" CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryLegaleRappresentanteStop" runat="server"
                                            ValidationGroup="stop" CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStepCantiere" runat="server" Title="- Lavoratori occupati" StepType="Step">
            <!-- CANTIERE -->
            <asp:Panel ID="PanelLavoratoriUnico" runat="server" Visible="true">
                <uc6:IscrizioneCELavoratori ID="IscrizioneCELavoratori1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="PanelCantiereNormale" runat="server" Visible="false">
                <uc8:IscrizioneCECantiere ID="IscrizioneCECantiere1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="PanelCantierePrevedi" runat="server" Visible="false">
                <uc7:IscrizioneCEImpiegati ID="IscrizioneCEImpiegati1" runat="server" />
            </asp:Panel>
            <table class="standardTable">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelSummaryCantiere" runat="server" Width="500px">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryCantiere" runat="server" ValidationGroup="cantiere"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ValidationSummary ID="ValidationSummaryCantiereStop" runat="server" ValidationGroup="stop"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStepConferma" runat="server" Title="- Conferma" StepType="Finish">
            <table class="standardTable">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="LabelRiassunto" runat="server" Font-Bold="True" Text="Riassunto"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="LabelErroreBloccante" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Problemi bloccanti"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelDatiGeneraliValidation" runat="server" Font-Bold="False" Text="Dati generali"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="BulletedListDatiGenerali" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoDatiGenerali" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr id="rigaRiassuntoConsulente" runat="server">
                    <td>
                        <asp:Label ID="LabelConsulenteValidation" runat="server" Font-Bold="False" Text="Consulente"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="BulletedListConsulente" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoConsulente" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelSediValidation" runat="server" Font-Bold="False" Text="Sedi"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="BulletedListSedi" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoSedi" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelBancaValidation" runat="server" Font-Bold="False" Text="Banca"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="BulletedListBanca" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoBanca" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelLegaleRappresentanteValidation" runat="server" Font-Bold="False"
                            Text="Legale rappresentante"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="BulletedListLegaleRappresentante" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoLegaleRappresentante" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelCantiereValidation" runat="server" Font-Bold="False" Text="Lavoratori occupati"></asp:Label>
                    </td>
                    <td>
                        <asp:BulletedList ID="BulletedListCantiere" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoCantiere" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Avvertimenti"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:BulletedList ID="BulletedListAvvertimenti" runat="server">
                        </asp:BulletedList>
                        <asp:Label ID="LabelRiassuntoAvvertimenti" runat="server" Text="OK" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="LabelErroreNonBloccante" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Una volta confermata la domanda, si ricorda che la stessa verrà considerata completata
                            al ricevimento dell’originale (la domanda dovrà essere stampata e firmata dal legale
                            rappresentante dell’azienda richiedente) corredato dai seguenti allegati:
                            <ul>
                                <li>schema di consenso al trattamento dei dati sottoscritto dal legale rappresentante
                                    dell’azienda; </li>
                                <li>visura camerale non antecedente 6 mesi dalla data della richiesta; </li>
                                <li>autocertificazione della comunicazione antimafia. </li>
                            </ul>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
    </WizardSteps>
    <SideBarTemplate>
        <asp:DataList ID="SideBarList" runat="server">
            <ItemTemplate>
                <asp:LinkButton ID="SideBarButton" runat="server" CausesValidation="false"></asp:LinkButton>
            </ItemTemplate>
        </asp:DataList>
    </SideBarTemplate>
    <SideBarStyle Width="180px" Wrap="True" />
    <StepNextButtonStyle Width="100px" />
    <StartNextButtonStyle Width="100px" />
    <FinishCompleteButtonStyle Width="150px" />
    <FinishPreviousButtonStyle Width="100px" />
    <StepPreviousButtonStyle Width="100px" />
</asp:Wizard>
<br />
<asp:Button ID="ButtonSalva" runat="server" Text="Salva temporaneamente la domanda"
    Visible="false" OnClick="ButtonSalva_Click" Width="264px" ValidationGroup="stop" />
<asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante il salvataggio della domanda"
    Visible="False"></asp:Label>
<asp:Label ID="LabelDomandaGiaInserita" runat="server" ForeColor="Red" Text="La domanda è già stata inserita nel sistema."
    Visible="False"></asp:Label>
<asp:Label ID="LabelCodiceFiscalePartitaIvaPresente" runat="server" ForeColor="Red" Text="E' già presente una domanda di iscrizione con lo stesso codice fiscale/partita iva o l'impresa è già iscritta."
    Visible="False"></asp:Label>