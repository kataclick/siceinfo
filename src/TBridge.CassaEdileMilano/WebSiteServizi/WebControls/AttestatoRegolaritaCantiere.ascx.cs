#region

using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Geocoding = TBridge.Cemi.AttestatoRegolarita.Business.AttestatoRegolaritaGeocoding;

#endregion

public partial class WebControls_AttestatoRegolaritaCantiere : UserControl
{
    private const Int16 INDICEINDIRIZZOINSERITO = 0;
    private const Int16 INDICEINDIRIZZOPROPOSTO = 1;
    private readonly AttestatoRegolaritaBusiness attestatoRegolaritaBiz = new AttestatoRegolaritaBusiness();
    private Boolean modifica;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (ViewState["Indirizzi"] == null)
            {
                ViewState["Indirizzi"] = new IndirizzoCollection();
            }

            if (!modifica)
            {
                PanelNuovoIndirizzo.Visible = true;
                ButtonNuovoIndirizzo.Visible = false;

                CaricaProvince();
                CaricaTipologieCommittenti();

                if (GestioneUtentiBiz.IsCommittente())
                {
                    //TBridge.Cemi.GestioneUtenti.Type.Entities.Committente committente =
                    //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Committente) HttpContext.Current.User.Identity)
                    //        .Entity;
                    TBridge.Cemi.GestioneUtenti.Type.Entities.Committente committente =
                        (TBridge.Cemi.GestioneUtenti.Type.Entities.Committente)
                        GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    TextBoxCommittenteRagioneSociale.Text = committente.RagioneSociale;
                    TextBoxCommittentePIVA.Text = committente.PartitaIva;
                    TextBoxCommittenteCodFisc.Text = committente.CodiceFiscale;
                    TextBoxCommittenteIndirizzo.Text = committente.Indirizzo;
                    DropDownListCommittenteTipologia.SelectedValue = committente.Tipologia.ToString();
                    if (committente.Provincia.HasValue)
                    {
                        DropDownListCommittenteProvincia.SelectedValue = committente.Provincia.ToString();
                        CaricaComuni(committente.Provincia.Value);

                        if (committente.Comune.HasValue)
                        {
                            DropDownListCommittenteComuni.SelectedValue = committente.Comune.ToString();
                            CaricaCAP(committente.Comune.Value);

                            if (!String.IsNullOrEmpty(committente.Cap))
                            {
                                DropDownListCommittenteCAP.SelectedValue = committente.Cap;
                            }
                        }
                    }
                }
            }

            // Per fare delle prove
            //CaricaDatiProva();
        }
    }

    private void CaricaDatiProva()
    {
        TextBoxMeseAnno.Text = "01/2009";

        TextBoxDataInizioLavori.Text = "01/01/2008";
        TextBoxDataFineLavori.Text = "01/06/2009";
        TextBoxImporto.Text = "1000,50";
        TextBoxDescrizione.Text = "Ristrutturazione palazzina";
        TextBoxManodopera.Text = "80";
        TextBoxNumeroLavoratori.Text = "55";
        TextBoxNumeroGiorni.Text = "2000";
        TextBoxDIA.Text = "23423";

        TextBoxCommittenteRagioneSociale.Text = "Roger Costruzioni S.r.l.";
        DropDownListCommittenteTipologia.SelectedIndex = 1;
        TextBoxCommittentePIVA.Text = "00000000000";
        TextBoxCommittenteIndirizzo.Text = "Piazza della Vittoria";
        TextBoxCommittenteCivico.Text = "11A/8";
        //DropDownListCommittenteProvincia.SelectedItem.Text = "GE";
        //attestatoRegolaritaBiz.CambioSelezioneDropDownProvincia(DropDownListCommittenteProvincia, DropDownListCommittenteComuni,
        //                                                        DropDownListCommittenteCAP);
        //DropDownListCommittenteComuni.SelectedItem.Text = "GENOVA";
        //attestatoRegolaritaBiz.CambioSelezioneDropDownComune(DropDownListCommittenteComuni, DropDownListCommittenteCAP);
        //DropDownListCommittenteCAP.SelectedIndex = 1;
    }

    public void CaricaDati(Domanda domanda, Boolean copia)
    {
        modifica = true;

        if (!copia)
        {
            TextBoxMeseAnno.Text = domanda.Mese.ToString("MM/yyyy");
        }
        else
        {
            LabelMeseAnnoOriginale.Text = domanda.Mese.ToString("MM/yyyy");
        }

        TextBoxDataInizioLavori.Text = domanda.DataInizio.ToString("dd/MM/yyyy");
        TextBoxDataFineLavori.Text = domanda.DataFine.ToString("dd/MM/yyyy");
        TextBoxImporto.Text = domanda.Importo.Value.ToString();
        TextBoxDescrizione.Text = domanda.Descrizione;
        if (domanda.PercentualeManodopera.HasValue)
            TextBoxManodopera.Text = domanda.PercentualeManodopera.Value.ToString("0");
        TextBoxNumeroLavoratori.Text = domanda.NumeroLavoratori.ToString();
        TextBoxNumeroGiorni.Text = domanda.NumeroGiorni.ToString();
        TextBoxDIA.Text = domanda.Dia;
        TextBoxConcessione.Text = domanda.Concessione;
        TextBoxAutorizzazione.Text = domanda.Autorizzazione;

        ViewState["Indirizzi"] = new IndirizzoCollection();
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.Indirizzo1 = domanda.Indirizzo;
        indirizzo.Civico = domanda.Civico;
        indirizzo.Provincia = domanda.Provincia;
        indirizzo.InfoAggiuntiva = domanda.InfoAggiuntiva;
        indirizzo.Comune = domanda.Comune;
        indirizzo.Cap = domanda.Cap;
        indirizzo.Latitudine = domanda.Latitudine;
        indirizzo.Longitudine = domanda.Longitudine;

        AggiungiIndirizzoAllaLista(indirizzo);
        CaricaIndirizzi();
        //SvuotaCampi();
        MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOINSERITO;
        PanelNuovoIndirizzo.Visible = false;
        ButtonNuovoIndirizzo.Visible = true;

        ViewState["IdCommittente"] = domanda.Committente.IdCommittente.Value;
        TextBoxCommittenteRagioneSociale.Text = domanda.Committente.RagioneSociale;
        TextBoxCommittentePIVA.Text = domanda.Committente.PartitaIVA;
        TextBoxCommittenteCodFisc.Text = domanda.Committente.CodiceFiscale;
        CaricaTipologieCommittenti();
        DropDownListCommittenteTipologia.SelectedValue = domanda.Committente.Tipologia.ToString();
        TextBoxCommittenteIndirizzo.Text = domanda.Committente.Indirizzo;
        TextBoxCommittenteCivico.Text = domanda.Committente.Civico;

        CaricaProvince();
        DropDownListCommittenteProvincia.SelectedValue = domanda.Committente.Provincia.ToString();
        CaricaComuni(domanda.Committente.Provincia);
        DropDownListCommittenteComuni.SelectedValue = domanda.Committente.Comune.ToString();
        CaricaCAP(domanda.Committente.Comune);
        DropDownListCommittenteCAP.SelectedValue = domanda.Committente.Cap;
    }

    private void CaricaTipologieCommittenti()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCommittenteTipologia,
            Enum.GetNames(typeof (TipologiaCommittente)),
            "",
            "");
    }

    protected void ButtonGeocodifica_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Indirizzo indirizzo = CreaIndirizzo();

            LabelIndirizzoFornito.Text = indirizzo.IndirizzoCompleto;
            IndirizzoCollection indirizzi =
                AttestatoRegolaritaGeocoding.GeoCodeGoogleMultiplo(indirizzo.IndirizzoPerGeocoder);

            ViewState["IndirizziGeocodificati"] = indirizzi;
            Presenter.CaricaElementiInGridView(GridViewIndirizzi, indirizzi, 0);

            LabelSceltaIndirizzo.Visible = false;
            if (indirizzi != null)
            {
                if (indirizzi.Count > 0)
                {
                    LabelSceltaIndirizzo.Visible = true;
                }
            }


            MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOPROPOSTO;
        }
    }

    private Indirizzo CreaIndirizzo()
    {
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.Indirizzo1 = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        indirizzo.InfoAggiuntiva = Presenter.NormalizzaCampoTesto(TextBoxInfoAggiuntiva.Text);
        indirizzo.Civico = Presenter.NormalizzaCampoTesto(TextBoxCivico.Text);
        indirizzo.Cap = Presenter.NormalizzaCampoTesto(TextBoxCap.Text);
        indirizzo.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        indirizzo.Provincia = Presenter.NormalizzaCampoTesto(TextBoxProvincia.Text);

        return indirizzo;
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (ViewState["IndirizziGeocodificati"] != null)
        {
            IndirizzoCollection indirizziGeo = (IndirizzoCollection) ViewState["IndirizziGeocodificati"];
            indirizziGeo[e.NewSelectedIndex].Civico = Presenter.NormalizzaCampoTesto(TextBoxCivico.Text);

            AggiungiIndirizzoAllaLista(indirizziGeo[e.NewSelectedIndex]);
            CaricaIndirizzi();

            SvuotaCampi();
            MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOINSERITO;
            PanelNuovoIndirizzo.Visible = false;
            ButtonNuovoIndirizzo.Visible = true;
        }
    }

    private void CaricaIndirizzi()
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];

        if (indirizzi.Count > 0)
        {
            LabelIndirizzo.Text = indirizzi[0].IndirizzoDenominazione;
            LabelCAPComuneProvincia.Text = string.Format("{0} {1} ({2})", indirizzi[0].Cap, indirizzi[0].Comune,
                                                         indirizzi[0].Provincia);
            LabelIndirizzo.Visible = true;
            LabelCAPComuneProvincia.Visible = true;

            ButtonEliminaSelezione.Enabled = true;
            LabelNessunIndirizzo.Visible = false;

            ButtonNuovoIndirizzo.Text = "Modifica indirizzo";
        }
        else
        {
            LabelIndirizzo.Text = string.Empty;
            LabelCAPComuneProvincia.Text = string.Empty;
            LabelIndirizzo.Visible = false;
            LabelCAPComuneProvincia.Visible = false;

            ButtonEliminaSelezione.Enabled = false;
            LabelNessunIndirizzo.Visible = true;

            ButtonNuovoIndirizzo.Text = "Aggiungi indirizzo";
        }
    }

    private void SvuotaCampi()
    {
        Presenter.SvuotaCampo(TextBoxIndirizzo);
        Presenter.SvuotaCampo(TextBoxCivico);
        Presenter.SvuotaCampo(TextBoxInfoAggiuntiva);
        Presenter.SvuotaCampo(TextBoxCap);
        Presenter.SvuotaCampo(TextBoxComune);
        Presenter.SvuotaCampo(TextBoxProvincia);

        Presenter.SvuotaCampo(LabelIndirizzoFornito);
    }

    protected void ButtonAggiungiIndirizzo_Click(object sender, EventArgs e)
    {
        Indirizzo indirizzo = CreaIndirizzo();
        AggiungiIndirizzoAllaLista(indirizzo);
        CaricaIndirizzi();

        SvuotaCampi();
        MultiViewIndirizzo.ActiveViewIndex = INDICEINDIRIZZOINSERITO;
        PanelNuovoIndirizzo.Visible = false;
        ButtonNuovoIndirizzo.Visible = true;
    }

    private void AggiungiIndirizzoAllaLista(Indirizzo indirizzo)
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];

        indirizzi.Clear();

        indirizzi.Add(indirizzo);
        ViewState["Indirizzi"] = indirizzi;
    }

    protected void ButtonEliminaSelezione_Click(object sender, EventArgs e)
    {
        IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];
        indirizzi.Clear();

        CaricaIndirizzi();
    }

    protected void ButtonNuovoIndirizzo_Click(object sender, EventArgs e)
    {
        IndirizzoCollection indirizzi = GetIndirizzi();

        if (indirizzi.Count > 0)
        {
            TextBoxIndirizzo.Text = indirizzi[0].Indirizzo1;
            TextBoxCap.Text = indirizzi[0].Cap;
            TextBoxCivico.Text = indirizzi[0].Civico;
            TextBoxProvincia.Text = indirizzi[0].Provincia;
            TextBoxInfoAggiuntiva.Text = indirizzi[0].InfoAggiuntiva;
            TextBoxComune.Text = indirizzi[0].Comune;
            TextBoxLatitudine.Text = indirizzi[0].Latitudine.ToString();
            TextBoxLongitudine.Text = indirizzi[0].Longitudine.ToString();
        }

        PanelNuovoIndirizzo.Visible = true;
        ButtonNuovoIndirizzo.Visible = false;
    }

    public IndirizzoCollection GetIndirizzi()
    {
        IndirizzoCollection indirizzi = null;

        Page.Validate("ListaIndirizzi");

        if (Page.IsValid)
        {
            if (ViewState["Indirizzi"] != null)
            {
                indirizzi = (IndirizzoCollection) ViewState["Indirizzi"];
            }
        }

        return indirizzi;
    }

    public DateTime GetDataRichiesta()
    {
        return DateTime.ParseExact(TextBoxMeseAnno.Text, "MM/yyyy", null);
    }

    public string GetDIA()
    {
        return Presenter.NormalizzaCampoTesto(TextBoxDIA.Text);
    }

    public string GetConcessione()
    {
        return Presenter.NormalizzaCampoTesto(TextBoxConcessione.Text);
    }

    public string GetAutorizzazione()
    {
        return Presenter.NormalizzaCampoTesto(TextBoxAutorizzazione.Text);
    }

    public Decimal? GetImporto()
    {
        try
        {
            return Decimal.Parse(TextBoxImporto.Text);
        }
        catch
        {
            return null;
        }
    }

    public string GetDescrizione()
    {
        return Presenter.NormalizzaCampoTesto(TextBoxDescrizione.Text);
    }

    public Int32? GetNumeroLavoratori()
    {
        try
        {
            return Int32.Parse(TextBoxNumeroLavoratori.Text);
        }
        catch
        {
            return null;
        }
    }

    public Int32? GetNumeroGiorni()
    {
        try
        {
            return Int32.Parse(TextBoxNumeroGiorni.Text);
        }
        catch
        {
            return null;
        }
    }

    public Decimal? GetPercentualeManodopera()
    {
        try
        {
            return Decimal.Parse(TextBoxManodopera.Text);
        }
        catch
        {
            return null;
        }
    }

    public DateTime GetDataInizio()
    {
        return DateTime.ParseExact(TextBoxDataInizioLavori.Text, "dd/MM/yyyy", null);
    }

    public DateTime GetDataFine()
    {
        return DateTime.ParseExact(TextBoxDataFineLavori.Text, "dd/MM/yyyy", null);
    }

    public Committente GetCommittente()
    {
        Committente committente = new Committente();

        if (ViewState["IdCommittente"] != null)
            committente.IdCommittente = (Int32) ViewState["IdCommittente"];

        committente.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxCommittenteRagioneSociale.Text);
        committente.Tipologia =
            (TipologiaCommittente)
            Enum.Parse(typeof (TipologiaCommittente), DropDownListCommittenteTipologia.SelectedValue);
        committente.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxCommittenteIndirizzo.Text);
        committente.Civico = Presenter.NormalizzaCampoTesto(TextBoxCommittenteCivico.Text);
        committente.Provincia = Int32.Parse(DropDownListCommittenteProvincia.SelectedValue);
        committente.Comune = Int64.Parse(DropDownListCommittenteComuni.SelectedValue);
        committente.Cap = DropDownListCommittenteCAP.SelectedValue;
        committente.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCommittenteCodFisc.Text);
        committente.PartitaIVA = Presenter.NormalizzaCampoTesto(TextBoxCommittentePIVA.Text);

        return committente;
    }

    private void CaricaProvince()
    {
        attestatoRegolaritaBiz.CaricaProvinceInDropDown(DropDownListCommittenteProvincia);
    }

    private void CaricaComuni(int idProvincia)
    {
        attestatoRegolaritaBiz.CaricaComuniInDropDown(DropDownListCommittenteComuni, idProvincia);
    }

    private void CaricaCAP(Int64 idComune)
    {
        attestatoRegolaritaBiz.CaricaCapInDropDown(DropDownListCommittenteCAP, idComune);
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        attestatoRegolaritaBiz.CambioSelezioneDropDownProvincia(DropDownListCommittenteProvincia,
                                                                DropDownListCommittenteComuni,
                                                                DropDownListCommittenteCAP);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        attestatoRegolaritaBiz.CambioSelezioneDropDownComune(DropDownListCommittenteComuni, DropDownListCommittenteCAP);
    }

    protected void ButtonAzzeraCampi_Click(object sender, EventArgs e)
    {
        TextBoxIndirizzo.Text = string.Empty;
        TextBoxCivico.Text = string.Empty;
        TextBoxInfoAggiuntiva.Text = string.Empty;
        TextBoxCap.Text = string.Empty;
        TextBoxComune.Text = string.Empty;
        TextBoxProvincia.Text = string.Empty;
        TextBoxLatitudine.Text = string.Empty;
        TextBoxLongitudine.Text = string.Empty;
        GridViewIndirizzi.DataSource = null;
    }

    protected void CustomValidatorCodFiscPIVACommittente_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (string.IsNullOrEmpty(TextBoxCommittentePIVA.Text.Trim()) &&
            string.IsNullOrEmpty(TextBoxCommittenteCodFisc.Text.Trim()))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorDIAAutoConc_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListCommittenteTipologia.SelectedValue == TipologiaCommittente.Privato.ToString())
        {
            if (string.IsNullOrEmpty(TextBoxDIA.Text.Trim()) && string.IsNullOrEmpty(TextBoxConcessione.Text.Trim()) &&
                string.IsNullOrEmpty(TextBoxAutorizzazione.Text.Trim()))
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorPartitaIva_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!String.IsNullOrEmpty(TextBoxCommittentePIVA.Text) && TextBoxCommittentePIVA.Text.Length == 11)
        {
            if (PartitaIvaManager.VerificaPartitaIva(TextBoxCommittentePIVA.Text))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorMeseTraDataInizioEFineLavori_ServerValidate(object source,
                                                                              ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (!String.IsNullOrEmpty(TextBoxMeseAnno.Text)
            && !String.IsNullOrEmpty(TextBoxDataInizioLavori.Text)
            && !String.IsNullOrEmpty(TextBoxDataFineLavori.Text))
        {
            DateTime mese;
            DateTime dataInizio;
            DateTime dataFine;

            if (DateTime.TryParseExact(TextBoxMeseAnno.Text, "MM/yyyy", null, DateTimeStyles.None, out mese)
                && DateTime.TryParse(TextBoxDataInizioLavori.Text, out dataInizio)
                && DateTime.TryParse(TextBoxDataFineLavori.Text, out dataFine))
            {
                // Data inizio
                if (dataInizio >= mese.AddMonths(1)
                    || dataFine < mese)
                {
                    args.IsValid = false;
                }
            }
        }
    }

    protected void CustomValidatorMeseCopia_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (!String.IsNullOrEmpty(LabelMeseAnnoOriginale.Text))
        {
            if (LabelMeseAnnoOriginale.Text == TextBoxMeseAnno.Text)
            {
                args.IsValid = false;
            }
        }
    }
}