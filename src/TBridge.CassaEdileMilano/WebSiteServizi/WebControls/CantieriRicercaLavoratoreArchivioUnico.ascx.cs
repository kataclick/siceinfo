using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Delegates;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;

public partial class WebControls_CantieriRicercaLavoratoreArchivioUnico : UserControl
{
    private readonly CantieriBusiness biz = new CantieriBusiness();
    public event LavoratoriSelectedEventHandler OnLavoratoreSelected;
    public event EventHandler OnNuovoLavoratoreSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaLavoratori()
    {
        if (string.IsNullOrEmpty(TextBoxCognome.Text) && string.IsNullOrEmpty(TextBoxNome.Text) &&
            string.IsNullOrEmpty(TextBoxDataNascita.Text))
        {
            LabelErrore.Text = "Digitare un filtro";
        }
        else
        {
            LabelErrore.Text = string.Empty;
            string cognome = null;
            string nome = null;
            DateTime? dataNascita = null;
            DateTime dataNascitaD;
            TipologiaLavoratore tipoLavoratore = TipologiaLavoratore.TutteLeFonti;

            string cogRic = TextBoxCognome.Text.Trim();
            if (!string.IsNullOrEmpty(cogRic))
                cognome = cogRic;

            string nomRic = TextBoxNome.Text.Trim();
            if (!string.IsNullOrEmpty(nomRic))
                nome = nomRic;

            if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
                DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
                dataNascita = dataNascitaD;

            LavoratoreCollection listaLavoratori =
                biz.GetLavoratoriOrdinati(tipoLavoratore, null, cognome, nome, dataNascita, null, null);
            GridViewLavoratori.DataSource = listaLavoratori;
            GridViewLavoratori.DataBind();
        }
    }

    private void CaricaLavoratori(string sortExpression)
    {
        string cognome = null;
        string nome = null;
        DateTime? dataNascita = null;
        DateTime dataNascitaD;
        TipologiaLavoratore tipoLavoratore = TipologiaLavoratore.TutteLeFonti;

        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            cognome = TextBoxCognome.Text;

        if (!string.IsNullOrEmpty(TextBoxNome.Text))
            nome = TextBoxNome.Text;

        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
            dataNascita = dataNascitaD;

        string direct = "ASC";
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');
            if (ord[0] == sortExpression && ord[1] == "ASC")
                direct = "DESC";
            else
                direct = "ASC";
        }
        ViewState["ordina"] = sortExpression + "|" + direct;

        LavoratoreCollection listaLavoratori =
            biz.GetLavoratoriOrdinati(tipoLavoratore, null, cognome, nome, dataNascita, sortExpression, direct);
        GridViewLavoratori.DataSource = listaLavoratori;
        GridViewLavoratori.PageIndex = 0;
        GridViewLavoratori.DataBind();
    }

    private void CaricaLavoratoriPreservaOrdine(string sortExpression)
    {
        string cognome = null;
        string nome = null;
        DateTime? dataNascita = null;
        DateTime dataNascitaD;
        TipologiaLavoratore tipoLavoratore = TipologiaLavoratore.TutteLeFonti;

        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            cognome = TextBoxCognome.Text;

        if (!string.IsNullOrEmpty(TextBoxNome.Text))
            nome = TextBoxNome.Text;

        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
            dataNascita = dataNascitaD;

        string direct = "ASC";
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');
            if (ord[0] == sortExpression && ord[1] == "ASC")
                direct = "ASC";
            else
                direct = "DESC";
        }
        ViewState["ordina"] = sortExpression + "|" + direct;

        LavoratoreCollection listaLavoratori =
            biz.GetLavoratoriOrdinati(tipoLavoratore, null, cognome, nome, dataNascita, sortExpression, direct);
        GridViewLavoratori.DataSource = listaLavoratori;
        GridViewLavoratori.PageIndex = 0;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idLavoratore = (int) GridViewLavoratori.DataKeys[e.NewSelectedIndex]["IdLavoratore"];
        TipologiaLavoratore tipoLavoratore =
            (TipologiaLavoratore) GridViewLavoratori.DataKeys[e.NewSelectedIndex]["TipoLavoratore"];
        Lavoratore lavoratore = biz.GetLavoratoriOrdinati(tipoLavoratore, idLavoratore, null, null, null, null, null)[0];

        if (OnLavoratoreSelected != null)
            OnLavoratoreSelected(lavoratore);
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaLavoratori();
        }
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["ordina"] != null)
        {
            string[] ord = ((string) ViewState["ordina"]).Split('|');

            if (ord[1] == "DESC")
                CaricaLavoratoriPreservaOrdine(ord[0]);
            else
                CaricaLavoratoriPreservaOrdine(ord[0]);
        }
        else
            CaricaLavoratori();

        GridViewLavoratori.PageIndex = e.NewPageIndex;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_Sorting(object sender, GridViewSortEventArgs e)
    {
        //SortDirection dir = SortDirection.Descending;
        //if (e.SortDirection == SortDirection.Descending)
        //    dir = SortDirection.Ascending;

        CaricaLavoratori(e.SortExpression);
    }

    protected void ButtonChiudi_Click(object sender, EventArgs e)
    {
        Visible = false;
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        if (OnNuovoLavoratoreSelected != null)
            OnNuovoLavoratoreSelected(this, null);
        //Response.Redirect("CantieriInserimentoModificaLavoratore.aspx");
    }
}