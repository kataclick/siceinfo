using System;
using System.Web.UI;
using TBridge.Cemi.Cantieri.Type.Entities;

public partial class WebControls_CantieriTestataIspezione : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ImpostaTestata(RapportoIspezione ispezione)
    {
        TextBoxGiorno.Text = ispezione.Giorno.ToShortDateString();
        TextBoxCantiere.Text = ispezione.Cantiere.IndirizzoMappa;
        TextBoxIspettore.Text = ispezione.Ispettore.Cognome + " " + ispezione.Ispettore.Nome;
    }
}