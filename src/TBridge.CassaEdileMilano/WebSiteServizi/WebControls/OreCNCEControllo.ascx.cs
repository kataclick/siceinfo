﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.UI.HtmlControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Collections;

public partial class WebControls_OreCNCEControllo : System.Web.UI.UserControl
{

    private const int INDICERICHIEDIORE = 2;
    // private readonly Business biz = new Business();
    private readonly Common commonBiz = new Common();


    protected void Page_Load(object sender, EventArgs e)
    {
        //#region Autenticazione

        //GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande,
        //                                      "~/Prestazioni/ControlloDomandaOreCNCE.aspx");

        //#endregion

        //if (!Page.IsPostBack)
        //{
        //    //Nel caso in cui il contesto non fosse presente, torniamo alla pagina di ricerca
        //    if (Context.Items["IdDomanda"] == null)
        //    {
        //        Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
        //    }

        //    //int idDomanda = (int)Context.Items["IdDomanda"];
        //    //Domanda domanda = biz.GetDomanda(idDomanda);

            
            
        //    //CaricaDati();
        //    //CaricaCasseEdili();
        //}
    }

    //private void CaricaCasseEdili()
    //{
    //    //Presenter.CaricaElementiInDropDownConElementoVuoto(
    //    //    DropDownListCassaEdile,
    //    //    commonBiz.GetCasseEdili(),
    //    //    "Descrizione",
    //    //    "IdCassaEdile");
    //}

    
     
    public void CaricaDati(CassaEdileCollection casseEdili, Int32 idLavoratore, String nomeLavoratore, String cognomeLavoratore, String codiceFiscale, DateTime dataRiferimento, Boolean abilita)
    {
        ViewState["CasseEdili"] = casseEdili;
        ViewState["IdLavoratore"] = idLavoratore;
        ViewState["NomeLavoratore"] = nomeLavoratore;
        ViewState["CognomeLavoratore"] = cognomeLavoratore;
        ViewState["CodiceFiscale"] = codiceFiscale;
        ViewState["DataRiferimento"] = dataRiferimento;
        ViewState["Abilita"] = abilita;

        GridViewCasseEdili.DataSource = casseEdili;
        GridViewCasseEdili.DataBind();

        
        CaricaOre(idLavoratore);
        
    }

    //private void CaricaDomanda()
    //{
    //    ViewState["IdDomanda"] = domanda.IdDomanda.Value;

    //    if (domanda.ControlloOreCnce.HasValue && domanda.ControlloOreCnce.Value)
    //        ButtonForzaTermineRecuperoOre.Enabled = false;

    //    GridViewCasseEdili.DataSource = domanda.CasseEdili;
    //    GridViewCasseEdili.DataBind();

    //    ViewState["IdLavoratore"] = domanda.Lavoratore.IdLavoratore.Value;
    //    CaricaOre(domanda.Lavoratore.IdLavoratore.Value);

    //    PrestazioniDatiDomanda1.CaricaDomanda(domanda);
    //}

    private void CaricaOre(int idLavoratore)
    {
        // Carico le ore già presenti nel nostro DB
        OreMensiliCNCECollection ore = OreCNCEManager.GetOreMensiliCNCE(idLavoratore);
        GridViewOreCaricate.DataSource = ore;
        GridViewOreCaricate.DataBind();

        if (ore.Count > 0)
        {
            LabelSegnalazione.Visible = true;
        }
        else
        {
            LabelSegnalazione.Visible = false;
        }
    }

   

    protected void GridViewCasseEdili_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CassaEdile cassaEdile = (CassaEdile)e.Row.DataItem;

            if (cassaEdile.Cnce)
                ButtonRichiediOreCnce.Enabled = true;

            if (!(Boolean)ViewState["Abilita"])
            {
                ButtonRichiediOreCnce.Enabled = false;
            }

            Button bCaricaManuale = (Button)e.Row.FindControl("ButtonCaricaManuale");
            bCaricaManuale.CommandArgument = e.Row.RowIndex.ToString();
            bCaricaManuale.Enabled = (Boolean) ViewState["Abilita"];

           
        }
    }

    protected void GridViewCasseEdili_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int indice;
        switch (e.CommandName)
        {
            case "carica":
                indice = Int32.Parse(e.CommandArgument.ToString());

                string idCassaEdile = (string)GridViewCasseEdili.DataKeys[indice].Values["IdCassaEdile"];

                PrestazioniCaricamentoManualeOre1.ImpostaCassaEdileELavoratore(idCassaEdile, (int)ViewState["IdLavoratore"]);
                PanelCaricamentoManuale.Visible = true;

                break;
        }
    }

    protected void GridViewCasseEdili_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string idCassaEdile = (string)GridViewCasseEdili.DataKeys[e.RowIndex].Values["IdCassaEdile"];
        
        PrestazioniCaricamentoManualeOre1.ImpostaCassaEdileELavoratore(idCassaEdile, (int)ViewState["IdLavoratore"]);
        PanelCaricamentoManuale.Visible = true;
    }

    protected void ButtonCaricaOre_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            bool oreDuplicate;
            OreMensiliCNCE ore = PrestazioniCaricamentoManualeOre1.CreaOre();

            if (OreCNCEManager.InsertOreMensili(ore, out oreDuplicate))
            {
                int idLavoratore = (int)ViewState["IdLavoratore"];
                CaricaOre(idLavoratore);
                PrestazioniCaricamentoManualeOre1.ResetCampi();
                PanelCaricamentoManuale.Visible = false;
            }
            else
            {
                if (oreDuplicate)
                {
                }
            }
        }
    }

    protected void GridViewOreCaricate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewOreCaricate.PageIndex = e.NewPageIndex;

        int idLavoratore = (int)ViewState["IdLavoratore"];
        CaricaOre(idLavoratore);
    }

    protected void GridViewCasseEdili_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string idCassaEdile = (string)GridViewCasseEdili.DataKeys[e.NewSelectedIndex].Values["IdCassaEdile"];
        int idLavoratore = (int)ViewState["IdLavoratore"];

        // Va effettuata la chiamata al Web Service e bisogna capire come gestire eventuali aggiornamenti..
    }

    //protected void ButtonForzaTermineRecuperoOre_Click(object sender, EventArgs e)
    //{
    //    int idDomanda = (int)ViewState["IdDomanda"];

    //    if (biz.ForzaControlloOreCNCE(idDomanda, true))
    //    {
    //        ButtonForzaTermineRecuperoOre.Enabled = false;
    //    }
    //}

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
      //  Context.Items["IdDomanda"] = (int)ViewState["IdDomanda"];
        //Server.Transfer("~/Prestazioni/ControlloDomanda.aspx");
    }

    protected void ButtonRichiediOreCnce_Click(object sender, EventArgs e)
    {
        //int idDomanda = (int)ViewState["IdDomanda"];
        //Domanda domanda = biz.GetDomanda(idDomanda);
        CassaEdileCollection casseEdili = (CassaEdileCollection)ViewState["CasseEdili"];
        Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
        String codiceFiscale = ViewState["CodiceFiscale"].ToString();
        String nomeLavoratore = ViewState["NomeLavoratore"].ToString();
        String cognomeLavoratore = ViewState["CognomeLavoratore"].ToString();
        DateTime dataRiferimento = (DateTime)ViewState["DataRiferimento"];

        string loginCNCE = ConfigurationManager.AppSettings["CNCEUserName"];
        string passwordCNCE = ConfigurationManager.AppSettings["CNCEPassword"];
        string urlCNCE = ConfigurationManager.AppSettings["CNCEUrl"];

        OreCNCEManager.GestisciOreCNCE(casseEdili,
            loginCNCE,
            passwordCNCE,
            urlCNCE,
            idLavoratore,
            codiceFiscale,
            cognomeLavoratore,
            nomeLavoratore,
            dataRiferimento);
        //biz.GestisciOreCNCE(domanda, loginCNCE, passwordCNCE, urlCNCE);
        CaricaOre(idLavoratore);
    }

    protected void GridViewOreCaricate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            OreMensiliCNCE ore = (OreMensiliCNCE)e.Row.DataItem;

            Button bModifica = (Button)e.Row.FindControl("ButtonModifica");
            bModifica.CommandArgument = e.Row.RowIndex.ToString();
            bModifica.Enabled = (Boolean)ViewState["Abilita"];
            Button bConfermaEliminazioneSi = (Button)e.Row.FindControl("ButtonConfermaEliminazioneSi");
            bConfermaEliminazioneSi.CommandArgument = e.Row.RowIndex.ToString();
            //bConfermaEliminazioneSi.Enabled = (Boolean)ViewState["Abilita"];
            Button bConfermaEliminazioneNo = (Button)e.Row.FindControl("ButtonConfermaEliminazioneNo");
            bConfermaEliminazioneNo.CommandArgument = e.Row.RowIndex.ToString();
            //bConfermaEliminazioneNo.Enabled = (Boolean)ViewState["Abilita"];
            Button bElimina = (Button)e.Row.FindControl("ButtonElimina");
            bElimina.Enabled = (Boolean)ViewState["Abilita"];

            if ((ore.OreLavorate + ore.OreFerie + ore.OreInfortunio + ore.OreMalattia + ore.OreCassaIntegrazione + ore.OrePermessoRetribuito) >= 209)
                e.Row.ForeColor = Color.Red;
        }
    }

    protected void GridViewOreCaricate_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int indice;

        switch (e.CommandName)
        {
            case "confermaDeleteSi":
                indice = Int32.Parse(e.CommandArgument.ToString());

                int idLavoratore = (int)GridViewOreCaricate.DataKeys[indice].Values["IdLavoratore"];
                string idCassaEdile = (string)GridViewOreCaricate.DataKeys[indice].Values["IdCassaEdile"];
                int anno = (int)GridViewOreCaricate.DataKeys[indice].Values["Anno"];
                int mese = (int)GridViewOreCaricate.DataKeys[indice].Values["Mese"];

                if (OreCNCEManager.DeleteOreCNCE(idLavoratore, idCassaEdile, anno, mese))
                {
                    //Domanda domanda = biz.GetDomanda((int)ViewState["IdDomanda"]);
                    //CaricaDomanda(domanda);
                    CaricaOre(idLavoratore);
                }
                break;
            case "confermaDeleteNo":
                indice = Int32.Parse(e.CommandArgument.ToString());
                HtmlTableRow rigaConferma =
                    (HtmlTableRow)GridViewOreCaricate.Rows[indice].FindControl("trConfermaEliminazione");
                rigaConferma.Visible = false;
                break;
            case "Modifica":
                indice = Convert.ToInt32(e.CommandArgument);
                int idLav = (int)GridViewOreCaricate.DataKeys[indice].Values["IdLavoratore"];
                string idCe = (string)GridViewOreCaricate.DataKeys[indice].Values["IdCassaEdile"];
                int year = (int)GridViewOreCaricate.DataKeys[indice].Values["Anno"];
                int month = (int)GridViewOreCaricate.DataKeys[indice].Values["Mese"];

                OreMensiliCNCE ore = OreCNCEManager.GetOreMensiliCNCE(idLav, idCe, year, month);
                PrestazioniCaricamentoManualeOre1.CaricaOre(ore);
                PanelCaricamentoManuale.Visible = true;
                break;
        }
    }

    protected void GridViewOreCaricate_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HtmlTableRow rigaConferma =
            (HtmlTableRow)GridViewOreCaricate.Rows[e.RowIndex].FindControl("trConfermaEliminazione");
        rigaConferma.Visible = true;
    }

    //protected void ButtonAggiungiCassaEdile_Click(object sender, EventArgs e)
    //{
    //    PanelNuovaCassaEdile.Visible = true;
    //}

    //protected void ButtonNuovaCassaEdile_Click(object sender, EventArgs e)
    //{
    //    if (Page.IsValid)
    //    {
    //        String idCassaEdile = DropDownListCassaEdile.SelectedValue;

    //        Int32 idDomanda = (Int32)ViewState["IdDomanda"];
    //        Domanda domanda = biz.GetDomanda(idDomanda);

    //        if (!domanda.CasseEdili.PresenteNellaLista(idCassaEdile))
    //        {
    //            if (biz.InsertCassaEdileInDomanda(domanda.IdDomanda.Value, idCassaEdile))
    //            {
    //                DropDownListCassaEdile.SelectedIndex = 0;
    //                PanelNuovaCassaEdile.Visible = false;

    //                domanda = biz.GetDomanda(idDomanda);
    //                CaricaDomanda(domanda);
    //            }
    //        }
    //    }
    //}


}