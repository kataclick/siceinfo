<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuNotifiche.ascx.cs" Inherits="WebControls_MenuNotifiche" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>
<%
    if ((GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NNotificheGestione.ToString())
         && (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente()))
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NNotificheAnomalie.ToString())
        )
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>Notifiche</td>
    </tr>
     <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NNotificheGestione.ToString())
            && (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente()))
            //per ora controlliamo che sia un'impresa, in futuro se anche un utente qualsiasi potr� identificare un 'impresa e gestire le notifiche
            //servir� un controllo sulla presenza si una session[idImpresa] o un sistema simile
        {%>
    <tr>
        <td>
        <a id="A1" href="~/NotificheAssunzione.aspx" runat="server">Gestione nuovi rapporti</a>
        </td>
    </tr>
    <tr>
        <td><a id="A2" href="~/NotificheCessazione.aspx" runat="server">Gestione cessazione rapporti</a>
        </td>
    </tr>
    <tr>
        <td><a id="A3" href="~/NotificheRicerca.aspx" runat="server">Ricerca dipendenti</a>
        </td>
    </tr>
     <%
        }

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NNotificheAnomalie.ToString()))
        {
%>
    <tr>
        <td><a id="A4" href="~/NotificheAnomalie.aspx" runat="server">Anomalie</a>
        </td>
    </tr>
    <%
        }
%>
</table>
<%
    }
%>