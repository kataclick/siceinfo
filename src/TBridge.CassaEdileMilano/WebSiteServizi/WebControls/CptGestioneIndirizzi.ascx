<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptGestioneIndirizzi.ascx.cs" Inherits="WebControls_CptGestioneIndirizzi" %>
<asp:UpdatePanel ID="UpdatePanelIndirizzi" runat="server">
<ContentTemplate>
<table class="standardTable">
    <tr>
        <td>
            <table class="filledtable">
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Elenco Indirizzi"></asp:Label>
                </td>
            </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td style="width: 459px">
                        <asp:CheckBoxList ID="CheckBoxListIndirizzi" runat="server" Width="410px" BorderStyle="Solid">
                        </asp:CheckBoxList>
                        <asp:Label ID="LabelNessunIndirizzo" runat="server">Nessun indirizzo inserito</asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 459px" >
                        <asp:Button ID="ButtonEliminaSelezione" runat="server" Text="Elimina indirizzi selezionati" OnClick="ButtonEliminaSelezione_Click" Height="25px" Enabled="False" Width="100%" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdatePanelIndirizzo" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelNuovoIndirizzo" runat="server" Visible="true">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Nuovo indirizzo"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            Indirizzo* 
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="250px"></asp:TextBox>
                            
                        </td>
                        <td style="width: 69px">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Campo obbligatorio" ControlToValidate="TextBoxIndirizzo" ValidationGroup="inserimentoIndirizzo"></asp:RequiredFieldValidator>
                        </td>
                        
                        
                    </tr>
                    <tr>
                        <td>
                            Civico
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxCivico" runat="server" Width="50px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Info agg.
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxInfoAggiuntiva" runat="server" Width="250px" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxComune" runat="server" Width="250px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Provincia
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxProvincia" runat="server" Width="250px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            Cap
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxCap" runat="server" Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Latitudine
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxLatitudine" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Longitudine
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="TextBoxLongitudine" runat="server" Width="150px" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                    &nbsp;
                        </td>
                        <td colspan="2">
                            <asp:UpdatePanel ID="UpdatePanelGeocodifica" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="ButtonGeocoder" runat="server" Text="Geocodifica" Width="300px" ValidationGroup="inserimentoIndirizzo" OnClick="ButtonGeocoder_Click" />
                                <asp:Panel ID="PanelIndirizzoGeo" runat="server" Width="125px" Visible="false">
                            
                                    <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" ShowHeader="False" OnSelectedIndexChanging="GridViewIndirizzi_SelectedIndexChanging" Width="300px" OnDataBound="GridViewIndirizzi_DataBound">
                                        <Columns>
                                            <asp:BoundField DataField="Provincia">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Comune">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IndirizzoDenominazione">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="ButtonSeleziona" runat="server" CommandName="Select" Font-Size="XX-Small"
                                                        Height="18px" Text="Seleziona" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Non � possibile geocodificare o il server non � disponibile"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>                                   
                    </tr>
                    <tr>
                        <td>
                            *campi obbligatori</td>
                        <td colspan="2" align="right">
                            <asp:Button ID="ButtonResettaCampi" runat="server" Text="Azzera campi" Width="150px" OnClick="ButtonResettaCampi_Click" CausesValidation="False" /><asp:Button ID="ButtonAggiungiIndirizzo" runat="server" OnClick="ButtonAggiungiIndirizzo_Click" Text="Aggiungi indirizzo" Width="150px" ValidationGroup="inserimentoIndirizzo" />
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>