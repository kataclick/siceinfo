using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Colonie.Business;
using TBridge.Cemi.Colonie.Type.Collections;
using TBridge.Cemi.Colonie.Type.Delegates;
using TBridge.Cemi.Colonie.Type.Entities;
using TBridge.Cemi.Colonie.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using CassaEdile=TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;

public partial class WebControls_ColonieRicercaLavoratoreACE : UserControl
{
    private ColonieBusiness biz = new ColonieBusiness();
    private CassaEdile cassaEdile = null;
    private int? idVacanza = null;

    #region Eventi

    // Evento selezione lavoratore
    public event LavoratoreACESelectedEventHandler OnLavoratoreACESelected;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Recupero la cassa edile loggata
        if (GestioneUtentiBiz.IsCassaEdile())
        {
            //cassaEdile = ((TBridge.Cemi.GestioneUtenti.Business.Identities.CassaEdile)HttpContext.Current.User.Identity).Entity;
            cassaEdile = (CassaEdile) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        }
    }

    public void ImpostaIdVacanza(int idVacanza)
    {
        this.idVacanza = idVacanza;
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewLavoratori.PageIndex = 0;
            CaricaLavoratori();
        }
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewLavoratori.PageIndex = e.NewPageIndex;
        CaricaLavoratori();
    }

    private void CaricaLavoratori()
    {
        LavoratoreACEFilter filtro = new LavoratoreACEFilter();

        filtro.Cognome = TextBoxCognome.Text.Trim();
        filtro.Nome = TextBoxNome.Text.Trim();
        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text))
            filtro.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
        if (cassaEdile != null)
            filtro.IdCassaEdile = cassaEdile.IdCassaEdile;

        LavoratoreACECollection lavoratori = biz.GetLavoratoriACE(filtro);
        GridViewLavoratori.DataSource = lavoratori;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idLavoratoreACE = (int) GridViewLavoratori.DataKeys[e.NewSelectedIndex].Value;
        LavoratoreACE lavoratore = biz.GetLavoratoreACE(idLavoratoreACE, idVacanza);

        if (OnLavoratoreACESelected != null)
            OnLavoratoreACESelected(lavoratore);
    }
}