<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IISLogMenu.ascx.cs" Inherits="WebControls_IISLogMenu" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>
<table class="menuTable">
    <tr class="menuTable">
        <td >
            Statistiche IIS</td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IISLogStatistiche.ToString()))
        {%>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=browser">Statistiche browser</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=country">Statistiche geografiche</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=giornoMese">Statistiche giorno-mese</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=giornoSettimana">Statistiche giorno-settimana</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=globali">Statistiche globali</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=ore">Statistiche ore-giorno</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=durata">Statistiche durata</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=OS">Statistiche sistema operativo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=pagine">Statistiche pagine</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=riepilogo">Statistiche di riepilogo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=temporali">Statistiche temporali</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=visitatori">Statistiche visitatori</a>
        </td>
    </tr>
    <%
        }
%>
</table>
