<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptRicercaNotificaPerAggiornamento.ascx.cs"
    Inherits="WebControls_CptRicercaNotificaPerAggiornamento" %>
<table class="standardTable">
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Data (ggmmaaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                            ControlToValidate="TextBoxData" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                            ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        Committente
                    </td>
                    <td>
                        Indirizzo
                    </td>
                    <td>
                        Data ins. (ggmmaaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            runat="server" ControlToValidate="TextBoxDataInserimento" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                            ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxData" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCommittente" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDataInserimento" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero appalto
                    </td>
                    <td>
                        Protocollo
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxProtocollo"
                            ErrorMessage="*" ValidationExpression="^\d{0,20}$" ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
                    </td>
                    <td>
                        Area
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" ControlToValidate="DropDownListArea"
                            ValidationGroup="ricercaNotifiche" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxNumeroAppalto" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxProtocollo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListArea" runat="server" Width="100%" AppendDataBoundItems="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                    </td>
                    <td align="right" colspan="2">
                        <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                            Text="Ricerca" ValidationGroup="ricercaNotifiche" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewNotifiche" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" DataKeyNames="IdNotifica,IdNotificaPadre" OnSelectedIndexChanging="GridViewNotifiche_SelectedIndexChanging"
                Width="100%" OnRowDataBound="GridViewNotifiche_RowDataBound" OnRowDeleting="GridViewNotifiche_RowDeleting"
                OnPageIndexChanging="GridViewNotifiche_PageIndexChanging" OnRowCommand="GridViewNotifiche_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="Tipo notifica">
                        <ItemTemplate>
                            <asp:Label ID="LabelTipoNotifica" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="IdNotifica" HeaderText="Codice" />
                    <asp:BoundField DataField="NumeroAppalto" HeaderText="Num. app." />
                    <asp:BoundField DataField="IdNotificaPadre" HeaderText="Notifica rif." />
                    <asp:BoundField DataField="Data" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False" />
                    <asp:BoundField DataField="CommittenteRagioneSociale" HeaderText="Committente" />
                    <asp:TemplateField HeaderText="Indirizzi">
                        <ItemTemplate>
                            <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" ShowHeader="False">
                                <Columns>
                                    <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
                                </Columns>
                            </asp:GridView>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Utente" HeaderText="Utente" />
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Aggiorna"
                        ShowSelectButton="True">
                        <ItemStyle Width="50px" />
                    </asp:CommandField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Copert."
                        ShowDeleteButton="True" />
                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="dettagli"
                        Text="Dettagli" />
                </Columns>
                <EmptyDataTemplate>
                    Nessuna notifica trovata
                </EmptyDataTemplate>
            </asp:GridView>
            &nbsp; &nbsp;
        </td>
    </tr>
</table>
