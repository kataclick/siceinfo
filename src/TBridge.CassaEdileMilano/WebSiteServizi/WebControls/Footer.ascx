﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="WebControls_Footer" %>

<div id="footer">
    <strong>Cassa edile di mutualità e assistenza di Milano, Lodi, Monza e Brianza © 2009</strong>
    <br />
    <strong>C.F.:</strong> 80038030153
    <br />
    <strong>Indirizzo:</strong> Via San Luca 6 20122 - MILANO - MI
    <br />
    <strong>Telefono:</strong> 02/584961
    <br />
    <strong>PEC:</strong> <%--<a hrer="mailto:direzione.mi00@infopec.cassaedile.it">--%>direzione.mi00@infopec.cassaedile.it<%--</a>--%>
</div>
