<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuPrestazioni.ascx.cs" Inherits="WebControls_MenuPrestazioni" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniCompilazioneDomanda.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniCompilazioneDomandaFast.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniGestioneDomande.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore.ToString())
    )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Prestazioni</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniCompilazioneDomanda.ToString()))
    {                  
    %>
    <tr>
        <td>
        <a id="A1" href="~/Prestazioni/CompilazioneDomanda.aspx" runat="server">Compilazione domanda</a>
        </td>
    </tr>
    <%
    }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniCompilazioneDomandaFast.ToString()))
    {                  
    %>
    <tr>
        <td>
        <a id="A5" href="~/Prestazioni/CompilazioneDomandaFast.aspx" runat="server">Compilazione domanda FAST</a>
        </td>
    </tr>
    <%
    }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniGestioneDomande.ToString()))
    {                  
    %>
    <tr>
        <td>
        <a id="A2" href="~/Prestazioni/GestioneDomande.aspx?menu=si" runat="server">Gestione domande</a>
        </td>
    </tr>
    <%
    }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore()
            && TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore.ToString()))
    {                  
    %>
    <tr>
        <td>
            <a id="A4" href="~/Prestazioni/GestioneDomandeNonConfermate.aspx" runat="server">Domande non confermate</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/Prestazioni/GestioneDomandePersonali.aspx" runat="server">Storico domande</a>
        </td>
    </tr>
    <%
    }
    %>
</table>
<% 
    }
    %>