﻿#region

using System;
using System.Web.UI;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.Presenter;

#endregion

public partial class WebControls_AttestatoRegolaritaLavoratoriImpresa : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    //public void CaricaImprese(ImpresaCollection Imprese)
    //{
    //    DropDownListImprese.Items.Clear();
    //    DropDownListImprese.Items.Add(new ListItem(string.Empty, string.Empty));
    //    foreach (Impresa Imp in Imprese)
    //    {
    //        DropDownListImprese.Items.Add(
    //            new ListItem(Imp.RagioneSociale,
    //                         string.Format("{0}|{1}|{2}", ((int)Imp.TipoImpresa), Imp.IdImpresa, Imp.IdTemporaneo)));
    //    }

    //}

    public void Inizializza(Domanda domanda, ImpresaCollection imprese)
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListImprese,
            imprese,
            "CodiceERagioneSociale",
            "IdImpresaComposto");

        //CaricaImprese(imprese);

        ViewState["LavoratoriImpresa"] = domanda.Lavoratori;
        ViewState["IdDomanda"] = domanda.IdDomanda;
    }

    protected void DropDownListImprese_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(DropDownListImprese.SelectedValue))
        {
            //String idImpresaComposto = DropDownListImprese.SelectedValue;

            //Int32 idImpresa;
            //TipologiaImpresa tipoImpresa;

            //Impresa.SplitIdImpresaComposto(idImpresaComposto, out idImpresa, out tipoImpresa);

            //DomandaImpresaCollection lavoratori = (DomandaImpresaCollection) ViewState["LavoratoriImpresa"];
            //DomandaImpresa lavoratoriImpresaSel = lavoratori.TrovaLavoratoriDellImpresa(tipoImpresa, idImpresa);

            //if (lavoratoriImpresaSel != null)
            //{
            //    Presenter.CaricaElementiInGridView(
            //        GridViewLavoratori,
            //        lavoratoriImpresaSel.Lavoratori,
            //        0);
            //}

            int? idImpresa = null;
            Guid? idTemporaneo = null;
            string[] tipoId = DropDownListImprese.SelectedValue.Split('|');
            TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

            GridViewLavoratori.Visible = true;

            if (!String.IsNullOrEmpty(tipoId[1]))
            {
                idImpresa = Int32.Parse(tipoId[1]);
            }

            if (!String.IsNullOrEmpty(tipoId[2]))
            {
                Guid gid = new Guid(tipoId[2]);
                idTemporaneo = gid;
            }

            DomandaImpresaCollection domandeImprese = (DomandaImpresaCollection) ViewState["LavoratoriImpresa"];
            DomandaImpresa domandaImpresa = null;

            if (domandeImprese != null)
            {
                domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, idImpresa, idTemporaneo);
            }
            else
            {
                Int32? idDomanda = (Int32?) ViewState["IdDomanda"];

                if (tipoImpresa == TipologiaImpresa.SiceNew)
                    domandaImpresa = biz.GetLavoratoriInDomandaPerImpresa(idDomanda.Value, idImpresa, null);
                else if (tipoImpresa == TipologiaImpresa.Nuova)
                    domandaImpresa = biz.GetLavoratoriInDomandaPerImpresa(idDomanda.Value, null, idImpresa);
            }

            if (domandaImpresa != null)
            {
                Presenter.CaricaElementiInGridView(
                    GridViewLavoratori,
                    domandaImpresa.Lavoratori,
                    0);
            }
            else
            {
                Presenter.CaricaElementiInGridView(
                    GridViewLavoratori,
                    null,
                    0);
            }
        }
        else
        {
            Presenter.CaricaElementiInGridView(
                GridViewLavoratori,
                null,
                0);

            GridViewLavoratori.Visible = false;
        }
    }
}