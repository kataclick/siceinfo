using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Business;
//using TBridge.Cemi.GestioneUtenti.Business;

public partial class WebControls_AccessoCantieriRilevatori : UserControl
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        SelezioneCantiere1.OnCantiereSelected += SelezioneCantiere1_OnCantiereSelected;

        if (!Page.IsPostBack)
        {
            CaricaRuoli();
        }
    }

    private void SelezioneCantiere1_OnCantiereSelected(WhiteList whiteListIdDomanda)
    {
        if (whiteListIdDomanda.IdWhiteList != null) ViewState["IdDomanda"] = whiteListIdDomanda.IdWhiteList.Value;

        PanelFiltroTimb.Visible = true;
        PanelAggiungiRilevatore.Visible = true;

        CaricaGridTimbrature();

        ButtonStampa.Visible = true;
        ButtonStampaSelezione.Visible = true;
        //ButtonStampaTutto.Visible = true;
        PanelRilevatori.Visible = true;


        ImpresaCollection imprese = new ImpresaCollection();
        foreach (Subappalto sub in whiteListIdDomanda.Subappalti)
        {
            if (sub.Appaltante != null)
                if (!imprese.Contains(sub.Appaltante))
                    imprese.Add(sub.Appaltante);

            if (sub.Appaltata != null)
                if (!imprese.Contains(sub.Appaltata))
                    imprese.Add(sub.Appaltata);
        }

        Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListImpresa, imprese,
                                                           "RagioneSociale", "idImpresa");
    }


    private void CaricaGridTimbrature()
    {
        WhiteList whiteListIdDomanda = _biz.GetDomandaByKey(Int32.Parse(ViewState["IdDomanda"].ToString()));

        WhiteListImpresaCollection wliColl = _biz.GetLavoratoriInDomanda(Int32.Parse(ViewState["IdDomanda"].ToString()));

        whiteListIdDomanda.Lavoratori = wliColl;

        AltraPersonaCollection altrePers = _biz.GetAltrePersone(Int32.Parse(ViewState["IdDomanda"].ToString()));

        whiteListIdDomanda.ListaAltrePersone = altrePers;

        ReferenteCollection referenti = _biz.GetReferenti(Int32.Parse(ViewState["IdDomanda"].ToString()));

        whiteListIdDomanda.ListaReferenti = referenti;

        TimbraturaCollection timbratureIdDomanda = _biz.GetTimbrature2(Int32.Parse(ViewState["IdDomanda"].ToString()));

        GridViewTimbrature.DataSource = timbratureIdDomanda;

        GridViewTimbrature.DataBind();
    }

    private void CaricaRuoli()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListRuolo,
            Enum.GetNames(typeof (TipologiaRuoloTimbratura)),
            "",
            "");
    }

    protected void ButtonVisualizzaTimb_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewTimbrature.PageIndex = 0;

            TimbraturaFilter filtro = new TimbraturaFilter
                                          {
                                              IdAccessoCantieriWhiteList = Int32.Parse(ViewState["IdDomanda"].ToString())
                                          };

            try
            {
                filtro.DataInizio = DateTime.Parse(TextBoxDataInizio.Text);
            }
            catch
            {
                filtro.DataInizio = null;
            }

            try
            {
                filtro.DataFine = DateTime.Parse(TextBoxDataFine.Text).AddDays(1);
            }
            catch
            {
                filtro.DataFine = null;
            }

            Int32? idImpresa = null;
            if (DropDownListImpresa.SelectedItem.ToString() != string.Empty)
                idImpresa = Int32.Parse(DropDownListImpresa.SelectedValue);

            Int32? ruolo = null;
            if (!String.IsNullOrEmpty(DropDownListRuolo.SelectedValue))
            {
                ruolo = (Int32) Enum.Parse(typeof(TipologiaRuoloTimbratura), DropDownListRuolo.SelectedValue);
            }

            filtro.TipoUtente = (TipologiaRuoloTimbratura?) ruolo;

            filtro.IdImpresa = idImpresa;

            filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

            CaricaGridTimbratureFilter(filtro, 0);
        }
    }

    private void CaricaGridTimbratureFilter(TimbraturaFilter filtro, Int32 pagina)
    {
        WhiteList whiteListIdDomanda = _biz.GetDomandaByKey(Int32.Parse(ViewState["IdDomanda"].ToString()));

        WhiteListImpresaCollection wliColl = _biz.GetLavoratoriInDomanda(Int32.Parse(ViewState["IdDomanda"].ToString()));

        whiteListIdDomanda.Lavoratori = wliColl;

        AltraPersonaCollection altrePers = _biz.GetAltrePersone(Int32.Parse(ViewState["IdDomanda"].ToString()));

        whiteListIdDomanda.ListaAltrePersone = altrePers;

        ReferenteCollection referenti = _biz.GetReferenti(Int32.Parse(ViewState["IdDomanda"].ToString()));

        whiteListIdDomanda.ListaReferenti = referenti;

        TimbraturaCollection timbratureIdDomanda = _biz.GetTimbratureByFilter(filtro);

        GridViewTimbrature.DataSource = timbratureIdDomanda;

        GridViewTimbrature.PageIndex = pagina;

        GridViewTimbrature.DataBind();
    }

    protected void GridViewTimbrature_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Timbratura timbratura = (Timbratura) e.Row.DataItem;

            Label lCodiceRilevatore = (Label) e.Row.FindControl("LabelCodiceRilevatore");
            Label lFornitore = (Label) e.Row.FindControl("LabelFornitore");

            Label lLatitudine = (Label) e.Row.FindControl("LabelLatitudine");
            Label lLongitudine = (Label) e.Row.FindControl("LabelLongitudine");

            Label lNomeCognome = (Label) e.Row.FindControl("LabelNomeCognome");
            Label lCodiceFiscale = (Label) e.Row.FindControl("LabelCodiceFiscale");

            Label lRuolo = (Label) e.Row.FindControl("LabelRuolo");
            Label lCodiceCe = (Label) e.Row.FindControl("LabelCodiceCE");

            Label lIngressoUscita = (Label) e.Row.FindControl("LabelIngressoUscita");

            Label lAnomalia = (Label) e.Row.FindControl("LabelAnomalia");
            Label lAnomaliaRiga2 = (Label) e.Row.FindControl("LabelAnomaliaRiga2");
            Label lAnomaliaRiga3 = (Label) e.Row.FindControl("LabelAnomaliaRiga3");

            lCodiceRilevatore.Text = timbratura.CodiceRilevatore;
            lFornitore.Text = timbratura.RagioneSociale;
            lLatitudine.Text = timbratura.Latitudine.ToString();
            lLongitudine.Text = timbratura.Longitudine.ToString();
            if (timbratura.IngressoUscita)
                lIngressoUscita.Text = "Ingresso";
            else
                lIngressoUscita.Text = "Uscita";

            WhiteList whiteListIdDomanda = _biz.GetDomandaByKey(Int32.Parse(ViewState["IdDomanda"].ToString()));

            WhiteListImpresaCollection wliColl =
                _biz.GetLavoratoriInDomanda(Int32.Parse(ViewState["IdDomanda"].ToString()));

            whiteListIdDomanda.Lavoratori = wliColl;

            AltraPersonaCollection altrePers = _biz.GetAltrePersone(Int32.Parse(ViewState["IdDomanda"].ToString()));
            whiteListIdDomanda.ListaAltrePersone = altrePers;

            ReferenteCollection referenti = _biz.GetReferenti(Int32.Parse(ViewState["IdDomanda"].ToString()));
            whiteListIdDomanda.ListaReferenti = referenti;

            ImpresaCollection imprese =
                _biz.GetImpreseAutonomeSelezionateInSubappalto(Int32.Parse(ViewState["IdDomanda"].ToString()));
            whiteListIdDomanda.ListaImprese = imprese;

            //TimbraturaCollection timbratureIdDomanda =
            //    _biz.GetTimbrature2(Int32.Parse(ViewState["IdDomanda"].ToString()));

            string codFisc = timbratura.CodiceFiscale;

            lNomeCognome.Text = "Non disponibile";
            lCodiceFiscale.Text = codFisc;
            lRuolo.Text = "Non disponibile";
            lCodiceCe.Text = "Non disponibile";

            int? idCantiere = Int32.Parse(ViewState["IdDomanda"].ToString());

            int codiceCe = _biz.GetCodiceCe(codFisc, null, idCantiere);
            if (codiceCe != -1)
                lCodiceCe.Text = codiceCe.ToString();
            else
                lCodiceCe.Text = "Non disponibile";

            Lavoratore lav = null;
            AltraPersona altr = null;
            Referente refe = null;
            Impresa impr = null;

            if (whiteListIdDomanda.Lavoratori != null)
                lav = whiteListIdDomanda.Lavoratori.GetLavoratoreCodFisc(codFisc);
            if (whiteListIdDomanda.ListaAltrePersone != null)
                altr = whiteListIdDomanda.ListaAltrePersone.GetAltraPersonaCodFisc(codFisc);
            if (whiteListIdDomanda.ListaReferenti != null)
                refe = whiteListIdDomanda.ListaReferenti.GetReferenteCodFisc(codFisc);
            if (whiteListIdDomanda.ListaImprese != null)
                impr = whiteListIdDomanda.ListaImprese.GetImpresaAutonomaCodFisc(codFisc);

            if (lav != null)
            {
                lNomeCognome.Text = string.Format("{0} {1}", lav.Nome, lav.Cognome);
                lCodiceFiscale.Text = lav.CodiceFiscale;
                lRuolo.Text = "Lavoratore";
            }
            else if (altr != null)
            {
                lNomeCognome.Text = string.Format("{0} {1}", altr.Nome, altr.Cognome);
                lCodiceFiscale.Text = altr.CodiceFiscale;
                if (altr.TipoRuolo == TipologiaRuolo.ResponsabileSicurezza)
                    lRuolo.Text = "Persona abilitata (resp. sicurezza)";
                if (altr.TipoRuolo == TipologiaRuolo.ResponsabileLavoratori)
                    lRuolo.Text = "Persona abilitata (resp. lavoratori)";
                if (altr.TipoRuolo == TipologiaRuolo.ResponsabileLegale)
                    lRuolo.Text = "Persona abilitata (resp. leg. impresa)";
                if (altr.TipoRuolo == TipologiaRuolo.Titolare)
                    lRuolo.Text = "Persona abilitata (titolare impresa)";
                if (altr.TipoRuolo == TipologiaRuolo.Socio)
                    lRuolo.Text = "Persona abilitata (socio impresa)";
                if (altr.TipoRuolo == TipologiaRuolo.Altro)
                    lRuolo.Text = "Persona abilitata";
            }
            else if (refe != null)
            {
                lNomeCognome.Text = string.Format("{0} {1}", refe.Nome, refe.Cognome);
                lCodiceFiscale.Text = refe.CodiceFiscale;

                if (refe.TipoRuolo == TipologiaRuoloReferente.Cantiere)
                    lRuolo.Text = "Referente cantiere";
                if (refe.TipoRuolo == TipologiaRuoloReferente.Committente)
                    lRuolo.Text = "Referente committente";
                if (refe.TipoRuolo == TipologiaRuoloReferente.Impresa)
                    lRuolo.Text = "Referente impresa";
                if (refe.TipoRuolo == TipologiaRuoloReferente.Altro)
                    lRuolo.Text = "Referente altro";
            }

            else if (impr != null)
            {
                lNomeCognome.Text = string.Format("{0}-{1} {2}", impr.RagioneSociale, impr.Nome, impr.Cognome);
                lCodiceFiscale.Text = impr.CodiceFiscale;
                lRuolo.Text = "Ditta artigiana";
            }

            if (codiceCe != -1)
            {
                Nominativo nominativo = _biz.GetNominativo(codiceCe);
                lNomeCognome.Text = string.Format("{0} {1}", nominativo.Nome, nominativo.Cognome);
            }

            switch (timbratura.Anomalia)
            {
                case (TipologiaAnomalia) 1:
                    lAnomalia.Text = "Non in lista";
                    lNomeCognome.Text = "Non disponibile";
                    lRuolo.Text = "Non disponibile";
                    lCodiceCe.Text = "Non disponibile";
                    break;
                case (TipologiaAnomalia) 2:
                    lAnomalia.Text = "Periodo non valido";
                    break;
                default:
                    lAnomalia.Text = "";
                    break;
            }

            lAnomaliaRiga2.Text = "";
            lAnomaliaRiga3.Text = "";

            if (lCodiceCe.Text == "Non disponibile")
            {
                if (!string.IsNullOrEmpty(codFisc))
                {
                    if (codFisc.Length == 16)
                    {
                        if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codFisc))
                        {
                            lNomeCognome.ForeColor = Color.Red;
                            lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                        }
                        else
                        {
                            lNomeCognome.ForeColor = Color.Black;
                        }
                    }
                    else
                    {
                        lNomeCognome.ForeColor = Color.Red;
                        lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                    }
                }
                else
                {
                    lNomeCognome.ForeColor = Color.Red;
                    lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                }
            }
        }
    }

    protected void ButtonStampa_Click(object sender, EventArgs e)
    {
        Context.Items["IdDomanda"] = ViewState["IdDomanda"];
        Context.Items["soloAnomalie"] = 1;
        Server.Transfer("~/AccessoCantieri/ReportAccessoCantieriAnomalie.aspx");
    }

    /* VECCHIO BOTTONE "STAMPA LISTA"
    protected void ButtonStampaTutto_Click(object sender, EventArgs e)
    {
        Context.Items["IdDomanda"] = ViewState["IdDomanda"];
        Context.Items["soloAnomalie"] = 0;
        Server.Transfer("~/AccessoCantieri/ReportAccessoCantieriAnomalie.aspx");
    }
    */

    protected void ButtonStampaSelezione_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DateTime? dataInizio;
            DateTime? dataFine;

            try
            {
                dataInizio = DateTime.Parse(TextBoxDataInizio.Text);
            }
            catch
            {
                dataInizio = null;
            }

            try
            {
                dataFine = DateTime.Parse(TextBoxDataFine.Text).AddDays(1);
            }
            catch
            {
                dataFine = null;
            }

            Int32? idImpresa = null;
            if (DropDownListImpresa.SelectedItem.ToString() != string.Empty)
                idImpresa = Int32.Parse(DropDownListImpresa.SelectedValue);

            Int32? ruolo = null;
            if (DropDownListRuolo.SelectedItem.ToString() != string.Empty)
                ruolo = (DropDownListRuolo.SelectedIndex - 1);

            TipologiaRuoloTimbratura? tipologiaUtente = (TipologiaRuoloTimbratura?)ruolo;
            int? tipoUtente = null;

            if (tipologiaUtente.HasValue)
            {
                switch (tipologiaUtente.Value)
                {
                    case TipologiaRuoloTimbratura.Lavoratore:
                        tipoUtente = 0;
                        break;
                    case TipologiaRuoloTimbratura.DittaArtigiana:
                        tipoUtente = 1;
                        break;
                    case TipologiaRuoloTimbratura.PersonaAbilitata:
                        tipoUtente = 2;
                        break;
                    case TipologiaRuoloTimbratura.Referente:
                        tipoUtente = 3;
                        break;
                    default:
                        tipoUtente = 4;
                        break;
                }
            }


            string codiceFiscale = TextBoxCodiceFiscale.Text;


            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Context.Items["soloAnomalie"] = 3;
            if (dataInizio.HasValue)
                Context.Items["dataInizio"] = dataInizio;
            else
                Context.Items["dataInizio"] = null;
            if (dataFine.HasValue)
                Context.Items["dataFine"] = dataFine.Value.AddDays(-1);
            else
                Context.Items["dataFine"] = null;
            Context.Items["idImpresa"] = idImpresa;
            Context.Items["ragioneSocialeImpresa"] = DropDownListImpresa.SelectedItem.ToString();
            Context.Items["tipoUtente"] = tipoUtente;
            Context.Items["codiceFiscale"] = codiceFiscale;


            Server.Transfer("~/AccessoCantieri/ReportAccessoCantieriAnomalie.aspx");
        }
    }

    protected void GridViewTimbrature_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        TimbraturaFilter filtro = new TimbraturaFilter
                                      {
                                          IdAccessoCantieriWhiteList = Int32.Parse(ViewState["IdDomanda"].ToString())
                                      };

        try
        {
            filtro.DataInizio = DateTime.Parse(TextBoxDataInizio.Text);
        }
        catch
        {
            filtro.DataInizio = null;
        }

        try
        {
            filtro.DataFine = DateTime.Parse(TextBoxDataFine.Text).AddDays(1);
        }
        catch
        {
            filtro.DataFine = null;
        }

        Int32? idImpresa = null;
        if (DropDownListImpresa.SelectedItem.ToString() != string.Empty)
            idImpresa = Int32.Parse(DropDownListImpresa.SelectedValue);

        Int32? ruolo = null;
        if (DropDownListRuolo.SelectedItem.ToString() != string.Empty)
            ruolo = (DropDownListRuolo.SelectedIndex - 1);

        filtro.TipoUtente = (TipologiaRuoloTimbratura?) ruolo;

        filtro.IdImpresa = idImpresa;

        filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

        CaricaGridTimbratureFilter(filtro, e.NewPageIndex);
    }

    protected void ValidateDuration(object sender, ServerValidateEventArgs e)
    {
        DateTime start = DateTime.Parse(TextBoxDataInizio.Text);
        DateTime end = DateTime.Parse(TextBoxDataFine.Text);

        TimeSpan ts = end - start;
        
        e.IsValid = ts.Days < 32;
    }
}