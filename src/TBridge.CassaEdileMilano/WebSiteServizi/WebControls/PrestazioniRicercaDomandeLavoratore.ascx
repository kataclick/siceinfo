<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniRicercaDomandeLavoratore.ascx.cs"
    Inherits="WebControls_PrestazioniRicercaDomandeLavoratore" %>
<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonVisualizza">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Stato
                        </td>
                        <td>
                            Tipo prestazione
                        </td>
                        <td>
                            Anno (aaaa)&nbsp;
                            <asp:RangeValidator ID="RangeValidatorAnno" runat="server" ControlToValidate="TextBoxAnno"
                                ErrorMessage="*" MaximumValue="2100" MinimumValue="1950" Type="Integer" ValidationGroup="ricerca"></asp:RangeValidator>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownListStato" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipoPrestazione" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAnno" runat="server" Width="100%" MaxLength="4"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            &nbsp;
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click"
                                ValidationGroup="ricerca" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si � verificato un errore durante l'operazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco domande"></asp:Label><br />
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" DataKeyNames="IdDomanda" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                    OnRowDataBound="GridViewDomande_RowDataBound">
                    <EmptyDataTemplate>
                        Nessuna domanda trovata
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="DataConferma" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data compilazione">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DescrizioneTipoPrestazione" HeaderText="Tipo prest.">
                            <ItemStyle Width="200px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Beneficiario">
                            <ItemTemplate>
                                <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr id="trStatoProtesi" runat="server" visible="false">
                                        <td style="width:30px">
                                            Prot:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelStatoProtesi" runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr id="trStatoCure" runat="server" visible="false">
                                        <td style="width:30px">
                                            Cure:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelStatoCure" runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Modulo">
                            <ItemTemplate>
                                <asp:Button ID="ButtonModulo" runat="server" OnClick="ButtonModulo_Click" Text="Modulo" />
                            </ItemTemplate>
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
