<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieRicercaLavoratoreACE.ascx.cs"
    Inherits="WebControls_ColonieRicercaLavoratoreACE" %>
<asp:Panel ID="PanelRicercaLavoratoreACE" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca lavoratori"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Data di nascita (gg/mm/aaaa)
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                                ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            &nbsp;<asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click"
                                ValidationGroup="ricercaLavoratori" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco domande"></asp:Label><br />
                <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                    Width="100%" AllowPaging="True" DataKeyNames="IdLavoratore" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewLavoratori_SelectedIndexChanging" PageSize="5">
                    <EmptyDataTemplate>
                        Nessun lavoratore trovato
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data di nascita"
                            HtmlEncode="False">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True">
                            <ItemStyle Width="50px" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
                &nbsp; &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
