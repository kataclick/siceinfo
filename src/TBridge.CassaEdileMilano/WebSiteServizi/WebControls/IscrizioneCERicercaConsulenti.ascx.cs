using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Delegates;

public partial class WebControls_IscrizioneCERicercaConsulenti : UserControl
{
    private readonly TSBusiness biz = new TSBusiness();
    public event ConsulenteSelectedEventHandler OnConsulenteSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaConsulenti()
    {
        string ragioneSociale = null;
        string comune = null;
        string indirizzo = null;
        string codFisc = null;
        int? codice = null;
        int codiceD;

        string ragSocRic = TextBoxRagioneSociale.Text.Trim();
        if (!string.IsNullOrEmpty(ragSocRic))
            ragioneSociale = ragSocRic;

        string comRic = TextBoxComune.Text.Trim();
        if (!string.IsNullOrEmpty(comRic))
            comune = comRic;

        string indRic = TextBoxIndirizzo.Text.Trim();
        if (!string.IsNullOrEmpty(indRic))
            indirizzo = indRic;

        string fiscRic = TextBoxFiscale.Text.Trim();
        if (!string.IsNullOrEmpty(fiscRic))
            codFisc = fiscRic;

        string codRic = TextBoxCodice.Text.Trim();
        if (!string.IsNullOrEmpty(codRic) && Int32.TryParse(codRic, out codiceD))
            codice = codiceD;

        ConsulentiCollection listaConsulenti = biz.GetConsulenti(codice, ragioneSociale, comune, indirizzo, codFisc);
        GridViewConsulenti.DataSource = listaConsulenti;
        GridViewConsulenti.DataBind();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaConsulenti();
        }
    }

    protected void GridViewConsulenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idConsulente = (int) GridViewConsulenti.DataKeys[e.NewSelectedIndex].Value;
        Consulente consulente = biz.GetConsulenti(idConsulente, null, null, null, null)[0];

        if (OnConsulenteSelected != null)
            OnConsulenteSelected(consulente);
    }

    protected void GridViewConsulenti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaConsulenti();

        GridViewConsulenti.PageIndex = e.NewPageIndex;
        GridViewConsulenti.DataBind();
    }
}