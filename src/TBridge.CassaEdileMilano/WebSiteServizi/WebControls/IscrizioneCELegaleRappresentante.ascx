﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneCELegaleRappresentante.ascx.cs"
    Inherits="WebControls_IscrizioneCELegaleRappresentante" %>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

    <script type="text/javascript">
        function CFUpperCaseOnly(sender) {
            sender.value = sender.value.toUpperCase();
    }
    </script>

</telerik:RadScriptBlock>

<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelLegaleRappresentante" runat="server" Text="Legale rappresentante"
                Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Cognome:
        </td>
        <td>
            <asp:TextBox ID="TextBoxRappLegaleCognome" runat="server" MaxLength="30" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognomeRapLegale" runat="server"
                ErrorMessage="Cognome mancante" ControlToValidate="TextBoxRappLegaleCognome"
                ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome:
        </td>
        <td>
            <asp:TextBox ID="TextBoxRappLegaleNome" runat="server" MaxLength="30" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNomeRapLegale" runat="server"
                ErrorMessage="Nome mancante" ControlToValidate="TextBoxRappLegaleNome" ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Sesso:
        </td>
        <td>
            <asp:RadioButton ID="RadioButtonRappLegaleSessoM" runat="server" Checked="true" GroupName="rappLegaleSesso"
                Text="M" />
            <asp:RadioButton ID="RadioButtonRappLegaleSessoF" runat="server" Checked="false"
                GroupName="rappLegaleSesso" Text="F" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale:
        </td>
        <td>
            <asp:TextBox ID="TextBoxRappLegaleCF" runat="server" MaxLength="16" Width="300px" onchange="CFUpperCaseOnly(this)" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCFRappLegale" runat="server"
                ErrorMessage="Codice fiscale mancante" ControlToValidate="TextBoxRappLegaleCF"
                ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionLegaleCF" runat="server" ControlToValidate="TextBoxRappLegaleCF"
                ErrorMessage="Codice fiscale non corretto" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}$"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCFRappLegale" runat="server" ErrorMessage="Codice fiscale non coerente con i dati forniti"
                ValidationGroup="legaleRappresentante" OnServerValidate="CustomValidatorCFRappLegale_ServerValidate">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita:
        </td>
        <td>
            <asp:TextBox ID="TextBoxRappLegaleDataNascita" runat="server" MaxLength="10" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRappLegaleDataNascita" runat="server"
                ErrorMessage="Data di nascita mancante" ControlToValidate="TextBoxRappLegaleDataNascita"
                ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataNascita" runat="server"
                ControlToValidate="TextBoxRappLegaleDataNascita" ErrorMessage="Data di nascita non corretta"
                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorRappLegaleDataNascita" runat="server" ControlToValidate="TextBoxRappLegaleDataNascita"
                ErrorMessage="Data di nascita non valida" OnServerValidate="CustomValidatorRappLegaleDataNascita_ServerValidate"
                ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Paese di nascita:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListRappLegalePaeseNascita" runat="server" Width="300px"
                AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListRappLegalePaeseNascita_SelectedIndexChanged"
                AutoPostBack="True" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPaeseNascita" runat="server"
                ErrorMessage="Paese di nascita mancante" ControlToValidate="DropDownListRappLegalePaeseNascita"
                ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidatorLuogoDiNascita" runat="server" ErrorMessage="Luogo di nascita mancante"
                OnServerValidate="CustomValidatorLuogoDiNascita_ServerValidate" ValidationGroup="legaleRappresentante">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Luogo di nascita:
        </td>
        <td colspan="2">
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Panel ID="PanelLuogoItalia" runat="server" Enabled="False">
                            <table class="standardTable">
                                <tr>
                                    <td colspan="3">
                                        Italiano
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Provincia
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListRappLegaleProvinciaNascita" runat="server" Width="250px"
                                            AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListRappLegaleProvinciaNascita_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comune
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListRappLegaleComuneNascita" runat="server" Width="250px"
                                            AppendDataBoundItems="True" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelLuogoEstero" runat="server" Enabled="False">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Estero
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DropDownListRappLegaleComuneNascitaEstero" runat="server" AppendDataBoundItems="true"
                                            Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            Residenza:
        </td>
        <td colspan="2">
            <table class="standardTable">
                <tr>
                    <td>
                        Provincia
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListRappLegaleResidenzaProvincia" runat="server" Width="250px"
                            AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListRappLegaleResidenzaProvincia_SelectedIndexChanged" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRappLegaleResidenzaProvincia"
                            runat="server" ErrorMessage="Provincia di residenza mancante" ControlToValidate="DropDownListRappLegaleResidenzaProvincia"
                            ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListRappLegaleResidenzaComune" runat="server" Width="250px"
                            AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListRappLegaleResidenzaComune_SelectedIndexChanged" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRappLegaleResidenzaComune"
                            runat="server" ErrorMessage="Comune di residenza mancante" ControlToValidate="DropDownListRappLegaleResidenzaComune"
                            ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Frazione:
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListRappLegaleResidenzaFrazione" runat="server" Width="250px"
                            AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListRappLegaleResidenzaFrazione_SelectedIndexChanged"
                            AutoPostBack="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Indirizzo
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListRappLegaleResidenzaPreIndirizzo" runat="server"
                            Width="100px" AppendDataBoundItems="True" />
                        <asp:TextBox ID="TextBoxRappLegaleResidenzaIndirizzo" runat="server" MaxLength="66"
                            Width="250px" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRappLegaleResidenzaIndirizzo"
                            runat="server" ErrorMessage="Indirizzo di residenza mancante" ControlToValidate="TextBoxRappLegaleResidenzaIndirizzo"
                            ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPreIndirizzoLegaleRappresentante"
                            runat="server" ControlToValidate="DropDownListRappLegaleResidenzaPreIndirizzo"
                            ErrorMessage="Pre indirizzo di residenza mancante" ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        CAP
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxRappLegaleResidenzaCap" runat="server" MaxLength="5" Width="250px" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorRappLegaleResidenzaCap" runat="server"
                            ErrorMessage="CAP di residenza mancante" ControlToValidate="TextBoxRappLegaleResidenzaCap"
                            ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorRappLegaleResidenzaCap"
                            runat="server" ControlToValidate="TextBoxRappLegaleResidenzaCap" ErrorMessage="Cap di residenza non corretto"
                            ValidationExpression="^\d{5}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="CustomValidatorRappLegaleCAP" runat="server" ControlToValidate="TextBoxRappLegaleResidenzaCap"
                            ErrorMessage="Non pu&#242; essere utilizzato un CAP generico per la residenza del legale rappresentante"
                            OnServerValidate="CustomValidatorCAP_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <asp:TextBox ID="TextBoxRappLegaleTel" runat="server" MaxLength="20" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRappLegaleTel" runat="server"
                ErrorMessage="Telefono mancante" ControlToValidate="TextBoxRappLegaleTel" ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorRappLegaleTelefono"
                runat="server" ControlToValidate="TextBoxRappLegaleTel" ErrorMessage="Telefono legale rappresentante non corretto"
                ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
</table>
