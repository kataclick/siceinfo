using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.Presenter;

public partial class WebControls_CorsiRicercaPartecipanti : UserControl
{
    private const Int32 INDICEPRESENZA = 5;

    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected String PostBackStringRefresh;
    
    protected void Page_Init(object sender, EventArgs e)
    {
        PostBackStringRefresh = Page.ClientScript.GetPostBackEventReference(this, "Refresh");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["__EVENTARGUMENT"] == "Refresh")
        {
            CaricaPartecipantiInterna(GridViewPartecipanti.PageIndex);
        }
    }

    //protected String RecuperaPostBackCode()
    //{
    //    ClientScriptManager scriptManager = ;
    //    return scriptManager.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
    //}

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        CaricaPartecipantiInterna(0);
    }

    public void CaricaPartecipanti(Int32 idProgrammazioneModulo)
    {
        ViewState["IdProgrammazioneModulo"] = idProgrammazioneModulo;
        CaricaPartecipantiInterna(0);
    }

    private void CaricaPartecipantiInterna(Int32 pagina)
    {
        PartecipanteFilter filtro = CreaFiltro();
        LavoratoreCollection lavoratori = biz.GetPartecipanti(filtro);

        Presenter.CaricaElementiInGridView(
            GridViewPartecipanti,
            lavoratori,
            pagina);

        if (lavoratori != null)
        {
            LabelNumeroPartecipanti.Text = lavoratori.Count.ToString();
        }
        else
        {
            LabelNumeroPartecipanti.Text = "0";
        }
    }

    private PartecipanteFilter CreaFiltro()
    {
        PartecipanteFilter filtro = new PartecipanteFilter();

        filtro.IdProgrammazioneModulo = (Int32) ViewState["IdProgrammazioneModulo"];
        filtro.Cognome = TextBoxCognome.Text;
        filtro.Nome = TextBoxNome.Text;
        filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;
        filtro.AttestatoRilasciato = CheckBoxAttestatoRilasciato.Checked;

        return filtro;
    }

    protected void GridViewPartecipanti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Int32 idProgrammazioneModulo = (Int32) ViewState["IdProgrammazioneModulo"];
        CaricaPartecipantiInterna(e.NewPageIndex);
    }

    protected void GridViewPartecipanti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore lavoratore = (Lavoratore) e.Row.DataItem;
            CheckBox cbPresente = (CheckBox) e.Row.FindControl("CheckBoxPresente");
            CheckBox cbAttestato = (CheckBox) e.Row.FindControl("CheckBoxAttestato");
            CheckBox cbPrenImpresa = (CheckBox) e.Row.FindControl("CheckBoxPrenotazioneImpresa");
            CheckBox cbPrenConsulente = (CheckBox) e.Row.FindControl("CheckBoxPrenotazioneConsulente");
            Label lCodice = (Label) e.Row.FindControl("LabelCodice");
            Label lCognome = (Label) e.Row.FindControl("LabelCognome");
            Label lNome = (Label) e.Row.FindControl("LabelNome");
            Label lImpresa = (Label) e.Row.FindControl("LabelImpresa");
            Label lDataNascita = (Label)e.Row.FindControl("LabelDataNascita");
            Label lLuogoNascita = (Label)e.Row.FindControl("LabelLuogoNascita");
            Button bElimina = (Button) e.Row.FindControl("ButtonElimina");
            Button bAggiorna = (Button) e.Row.FindControl("ButtonSalva");
            Button bModifica = (Button) e.Row.FindControl("ButtonModifica");
            Button bAttestato = (Button) e.Row.FindControl("ButtonAttestato");
            Label lPrimaEsperienza = (Label) e.Row.FindControl("LabelPrimaEsperienza");
            Label lDataAssunzione = (Label) e.Row.FindControl("LabelDataAssunzione");
            Label lDiritto = (Label) e.Row.FindControl("LabelDiritto");

            bModifica.CommandArgument = e.Row.RowIndex.ToString();
            String comando = String.Format(
                            "openRadWindowModifica('{0}', '{1}', '{2}'); return false;",
                            lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew ? lavoratore.IdLavoratore.Value.ToString() : null,
                            lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica ? lavoratore.IdLavoratore.Value.ToString() : null,
                            lavoratore.IdPartecipazione);
            //String comando = "openRadWindowModifica(); return false;";
            //String comando = "window.alert('ciao'); return false;";

            bModifica.Attributes.Add("OnClick", comando);

            bAttestato.CommandArgument = e.Row.RowIndex.ToString();

            cbPresente.Checked = lavoratore.Presente;
            cbAttestato.Checked = lavoratore.Attestato;
            bAttestato.Enabled = lavoratore.Attestato;
            cbPrenImpresa.Checked = lavoratore.PrenotazioneImpresa;
            cbPrenConsulente.Checked = lavoratore.PrenotazioneConsulente;

            if (!lavoratore.Presente)
            {
                e.Row.ForeColor = Color.Gray;
            }

            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                lCodice.Text = lavoratore.IdLavoratore.ToString();
                //bModifica.Enabled = false;
            }

            lCognome.Text = lavoratore.Cognome;
            lNome.Text = lavoratore.Nome;
            lImpresa.Text = lavoratore.Impresa.CodiceERagioneSociale;
            if (lavoratore.DataNascita != new DateTime())
            {
                lDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
            }
            lLuogoNascita.Text = lavoratore.ComuneNascita;

            // Se � stato confermato il registro presenze blocco la cancellazione e modifica presenza
            if (lavoratore.RegistroConfermato)
            {
                bElimina.Enabled = false;
                bAggiorna.Enabled = false;

                cbPresente.Enabled = false;
            }

            lPrimaEsperienza.Text = String.Format("Prima esperienza: {0}", lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No");
            lDataAssunzione.Text = lavoratore.DataAssunzione.HasValue ? String.Format("Data assunzione: {0}", lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy")) : String.Empty;

            lDiritto.Text = !lavoratore.Diritto.HasValue ? "n/d" : (lavoratore.Diritto.Value ? "S�" : "No");
        }
    }

    protected void GridViewPartecipanti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idPartecipazioneModulo =
            (Int32) GridViewPartecipanti.DataKeys[e.NewSelectedIndex].Values["IdPartecipazioneModulo"];
        Boolean presente =
            ((CheckBox) GridViewPartecipanti.Rows[e.NewSelectedIndex].FindControl("CheckBoxPresente")).Checked;

        if (biz.UpdatePartecipazioneModuloPresenza(idPartecipazioneModulo, presente))
        {
            CaricaPartecipantiInterna(GridViewPartecipanti.PageIndex);

            Label lMessaggio = (Label) GridViewPartecipanti.Rows[e.NewSelectedIndex].FindControl("LabelSalvaMessaggio");
            lMessaggio.Text = "Ok";
        }
        else
        {
            Label lMessaggio = (Label) GridViewPartecipanti.Rows[e.NewSelectedIndex].FindControl("LabelSalvaMessaggio");
            lMessaggio.Text = "Ko";
        }
    }

    protected void ButtonElimina_Click(object sender, EventArgs e)
    {
        Button bElimina = (Button) sender;
        Panel pConfermaEliminazione = (Panel) bElimina.Parent.FindControl("PanelConfermaEliminazione");

        pConfermaEliminazione.Visible = true;
        bElimina.Enabled = false;
    }

    protected void ButtonConfermaNo_Click(object sender, EventArgs e)
    {
        Button bConfermaNo = (Button) sender;
        Panel pConfermaEliminazione = (Panel) bConfermaNo.Parent.FindControl("PanelConfermaEliminazione");
        Button bElimina = (Button) bConfermaNo.Parent.FindControl("ButtonElimina");

        pConfermaEliminazione.Visible = false;
        bElimina.Enabled = true;
    }

    protected void ButtonConfermaSi_Click(object sender, EventArgs e)
    {
        Button bConfermaSi = (Button) sender;
        Int32 indiceRiga = ((GridViewRow) bConfermaSi.Parent.Parent.Parent).RowIndex;

        Int32 idPartecipazioneModulo =
            (Int32) GridViewPartecipanti.DataKeys[indiceRiga].Values["IdPartecipazioneModulo"];

        if (biz.DeletePartecipazione(idPartecipazioneModulo))
        {
            Int32 idProgrammazioneModulo = (Int32) ViewState["IdProgrammazioneModulo"];
            CaricaPartecipanti(idProgrammazioneModulo);
        }
    }

    protected void GridViewPartecipanti_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Int32 idPartecipazione;
        Int32 indice;

        switch (e.CommandName)
        {
            case "modifica":
                indice = Int32.Parse(e.CommandArgument.ToString());
                Int32 idLavoratore = (Int32) GridViewPartecipanti.DataKeys[indice].Values["IdLavoratore"];
                idPartecipazione = (Int32) GridViewPartecipanti.DataKeys[indice].Values["IdPartecipazione"];
                ModificaLavoratore(idLavoratore, idPartecipazione);
                break;
            case "attestato":
                indice = Int32.Parse(e.CommandArgument.ToString());
                idPartecipazione = (Int32) GridViewPartecipanti.DataKeys[indice].Values["IdPartecipazione"];
                Int32 idCorso = (Int32) GridViewPartecipanti.DataKeys[indice].Values["IdCorso"];
                Context.Items["IdPartecipazione"] = idPartecipazione;
                Context.Items["IdCorso"] = idCorso;
                Server.Transfer("~/Corsi/VisualizzaCertificato.aspx");
                break;
        }
    }

    private void ModificaLavoratore(Int32 idLavoratore, Int32 idPartecipazione)
    {
        PanelModifica.Visible = true;
        ViewState["IdPartecipazione"] = idPartecipazione;
        CorsiDatiLavoratore1.CaricaDatiLavoratore(idLavoratore, idPartecipazione);
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        PanelModifica.Visible = false;
    }

    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        Lavoratore lavoratore = CorsiDatiLavoratore1.GetLavoratore();
        Int32 idPartecipazione = (Int32) ViewState["IdPartecipazione"];

        if (biz.UpdateCorsiLavoratore(lavoratore, idPartecipazione))
        {
            PanelModifica.Visible = false;
            CaricaPartecipantiInterna(GridViewPartecipanti.PageIndex);
        }
    }
}