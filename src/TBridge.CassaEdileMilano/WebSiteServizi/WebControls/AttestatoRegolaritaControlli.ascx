<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaControlli.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaControlli" %>
<asp:Panel ID="PanelControlli" runat="server" Visible="true">
    <table class="borderedTable">
        <table class="filledtable">
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Anomalie riscontrate"></asp:Label>
                </td>
            </tr>
        </table>
        <tr>
            <br />
            <td>
                <asp:Panel ID="PanelControlliCantiere" runat="server" Visible="true" Width="100%">
                    <table class="standardTable">
                        <tr>
                            <td>
                                CANTIERE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelMessaggioCantiere" runat="server" Text="L'indirizzo fornito non � presente tra le notifiche"
                                    CssClass="messaggiErrore"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:Panel ID="PanelControlliImpreseEdili" runat="server" Visible="true" Width="100%">
                    <table class="standardTable">
                        <caption>
                            <br />
                            <tr>
                                <td>
                                    IMPRESE EDILI
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridViewImpreseEdili" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="CodiceERagioneSociale" HeaderText="Ragione sociale" />
                                            <asp:CheckBoxField DataField="ImpresaAssociataNegativo" HeaderText="A">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloCessataAttivitaNegativo" HeaderText="B">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloIscrittaPrecedentementeNegativo" HeaderText="C">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloSospesaNegativo" HeaderText="D">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloBNINegativo" HeaderText="E">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Nessuna anomalia riscontrata
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PanelLegendaImpreseEdili" runat="server" Visible="true" Width="100%">
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    LEGENDA
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    A - Impresa non presente nell&#39;anagrafica.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    B - Impresa che ha cessato l&#39;attivit� precedentemente alla richiesta dell&#39;attestato
                                                    di regolarit�.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    C - Impresa non ancora iscritta.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    D - Impresa sospesa.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    E - Impresa irregolare per BNI.
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Checked="True" Enabled="False" Text="Controllo superato." />
                                                    <asp:CheckBox ID="CheckBox2" runat="server" Enabled="False" Text="Controllo non superato." />
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </caption>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:Panel ID="PanelControlliImpreseNonEdili" runat="server" Visible="true" Width="100%">
                    <table class="standardTable">
                        <caption>
                            <br />
                            <tr>
                                <td>
                                    IMPRESE NON EDILI
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GridViewImpreseNonEdili" runat="server" AutoGenerateColumns="False"
                                        Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="CodiceERagioneSociale" HeaderText="Ragione sociale" />
                                            <asp:CheckBoxField DataField="ControlloNonEdileNoAnagraficaNegativo" HeaderText="A">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Nessuna anomalia riscontrata
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PanelLegendaImpreseNonEdili" runat="server" Visible="true" Width="100%">
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    LEGENDA
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    A - Impresa non edile presente in anagrafica.
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <asp:CheckBox ID="CheckBox3" runat="server" Checked="True" Enabled="False" 
                                                        Text="Controllo superato." />
                                                    <asp:CheckBox ID="CheckBox4" runat="server" Enabled="False" 
                                                        Text="Controllo non superato." />
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </caption>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:Panel ID="PanelControlliLavoratori" runat="server" Visible="true" Width="100%">
                    <table class="standardTable">
                        <caption>
                            <br />
                            <tr>
                                <td class="style1">
                                    LAVORATORI (DELLE IMPRESE EDILI)
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">
                                    <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                                        OnRowDataBound="GridViewLavoratori_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                                            <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                            <asp:BoundField DataField="CodiceERagioneSocialeImpresa" HeaderText="Impresa" />
                                            <asp:CheckBoxField DataField="LavoratoreAssociatoNegativo" HeaderText="A">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloPresenzaRapportoNegativo" HeaderText="B">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloOreLavorateNegativo" HeaderText="C">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="ControlloPartTimeNegativo" HeaderText="D">
                                                <ItemStyle Width="20px" />
                                            </asp:CheckBoxField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Nessuna anomalia riscontrata
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PanelLegendaLavoratori" runat="server" Visible="true" Width="100%">
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    LEGENDA
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    A - Lavoratore non presente in anagrafica.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    B - Il lavoratore non ha un rapporto di lavoro con l&#39;impresa dichiarata nel
                                                    periodo di richiesta.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    C - Non esistono ore lavorate denunciate per il lavoratore.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    D - Incoerenze con il tipo di assunzione dichiarato.
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <asp:CheckBox ID="CheckBox5" runat="server" Checked="True" Enabled="False" 
                                                        Text="Controllo superato." />
                                                    <asp:CheckBox ID="CheckBox6" runat="server" Enabled="False" 
                                                        Text="Controllo non superato." />
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </caption>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
