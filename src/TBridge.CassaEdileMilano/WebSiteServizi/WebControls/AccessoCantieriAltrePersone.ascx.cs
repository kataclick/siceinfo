﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Delegates;

public partial class WebControls_AccessoCantieriAltrePersone : UserControl
{
    public event LavoratoriDeletedEventHandler OnLavoratoreDeleted;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (ViewState["altrePersone"] == null)
            {
                ViewState["altrePersone"] = new AltraPersonaCollection();
            }

            CaricaGridAltrePersone();
        }
    }

    private void CaricaGridAltrePersone()
    {
        if (ViewState["altrePersone"] != null)
        {
            GridViewAltrePersone.DataSource = ViewState["altrePersone"];
            GridViewAltrePersone.DataBind();
        }
    }

    public void CaricaAltrePersone(AltraPersonaCollection altrePersone)
    {
        ViewState["altrePersone"] = altrePersone;
    }

    public AltraPersonaCollection GetAltrePersone()
    {
        return (AltraPersonaCollection) ViewState["altrePersone"];
    }

    protected void GridViewAltrePersone_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            AltraPersona Alt = (AltraPersona) e.Row.DataItem;
            Label lAltraPersona = (Label) e.Row.FindControl("LabelAltraPersona");
            Label lRuolo = (Label) e.Row.FindControl("LabelRuolo");

            if (Alt != null)
            {
                lAltraPersona.Text = string.Format("{0} {1}", Alt.Cognome, Alt.Nome);

                switch (Alt.TipoRuolo)
                {
                    case TipologiaRuolo.ResponsabileLavoratori:
                        lRuolo.Text = "Responsabile lavoratori";
                        break;
                    case TipologiaRuolo.ResponsabileSicurezza:
                        lRuolo.Text = "Responsabile sicurezza";
                        break;
                    case TipologiaRuolo.ResponsabileLegale:
                        lRuolo.Text = "Responsabile legale impresa";
                        break;
                    case TipologiaRuolo.Titolare:
                        lRuolo.Text = "Titolare impresa";
                        break;
                    case TipologiaRuolo.Socio:
                        lRuolo.Text = "Socio impresa";
                        break;
                    default:
                        lRuolo.Text = "Altro";
                        break;
                }
            }
        }
    }

    protected void GridViewAltrePersone_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        AltraPersonaCollection altrePersone = (AltraPersonaCollection) ViewState["altrePersone"];

        if (OnLavoratoreDeleted != null)
        {
            OnLavoratoreDeleted(altrePersone[e.RowIndex].CodiceFiscale);
        }

        altrePersone.RemoveAt(e.RowIndex);

        ViewState["altrePersone"] = altrePersone;

        CaricaGridAltrePersone();
    }

    protected void ButtonaggiungiAltraPersona_Click(object sender, EventArgs e)
    {
        LabelGiaPresente.Visible = false;


        AltraPersona altraPersona = AccessoCantieriAltrePersoneInserimento1.AltraPersona;

        if (altraPersona != null)
        {
            AltraPersonaCollection altrePersone = (AltraPersonaCollection) ViewState["altrePersone"];

            altrePersone.Add(altraPersona);

            ViewState["altrePersone"] = altrePersone;

            CaricaGridAltrePersone();

            AccessoCantieriAltrePersoneInserimento1.Reset();

            //controllare che non sia già stato inserito...

            LabelInserimentoAltraPersona.Text = string.Empty;
        }

        else
            LabelInserimentoAltraPersona.Text = "Non tutti i campi sono corretti";
    }
}