#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Collections;

#endregion

public partial class WebControls_AttestatoRegolaritaImpreseSubappalti : UserControl
{
    private readonly Common commonBiz = new Common();

    /// <summary>
    ///   ProprietÓ che costruisce il committente. Se sono presenti errori ritorna null
    /// </summary>
    public Impresa impresa
    {
        get
        {
            Page.Validate("ValidaInserimentoImpresa");

            if (Page.IsValid)
            {
                return CreaImpresa();
            }
            else
                return null;
        }
    }

    public string Errore { get; set; }

    public void ImpostaRagioneSociale(string ragioneSociale)
    {
        TextBoxRagioneSociale.Text = ragioneSociale;
    }

    private void CaricaCasseEdili()
    {
        CassaEdileCollection CEprovenienza = commonBiz.GetCasseEdili();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCEProvenienza,
            CEprovenienza,
            "Descrizione",
            "IdCassaEdile");
    }

    public void ImpostaImpresa(Impresa impresa)
    {
        ViewState["IdImpresa"] = impresa.IdImpresa;
        TextBoxPartitaIva.Text = impresa.PartitaIva;
        TextBoxIndirizzo.Text = impresa.Indirizzo;

        CheckBoxArtigiano.Checked = impresa.LavoratoreAutonomo;
        TextBoxCognome.Text = impresa.Cognome;
        TextBoxNome.Text = impresa.Nome;
        if (impresa.DataNascita.HasValue)
            TextBoxDataNascita.Text = impresa.DataNascita.ToString();
        TextBoxNumeroCameraCommercio.Text = impresa.NumeroCameraCommercio;


        // Provincia            
        ListItem prov = DropDownListProvincia.Items.FindByText(impresa.Provincia);
        if (prov != null)
        {
            DropDownListProvincia.SelectedValue = prov.Value;
            CaricaComuni(Int32.Parse(prov.Value));
        }

        // Comune
        ListItem com = DropDownListComuni.Items.FindByText(impresa.Comune);
        if (com != null)
        {
            DropDownListComuni.SelectedValue = com.Value;
            CaricaCAP(Int64.Parse(com.Value));
        }

        // Cap
        ListItem cap = DropDownListCAP.Items.FindByText(impresa.Cap);
        if (cap != null)
        {
            DropDownListCAP.SelectedValue = cap.Value;
        }

        // Cassa edile provenienza
        ListItem CEprovenienza = DropDownListCAP.Items.FindByValue(impresa.IdCassaEdileProvenienza);
        if (CEprovenienza != null)
        {
            DropDownListCEProvenienza.SelectedValue = CEprovenienza.Value;
        }
    }

    public void Reset()
    {
        Presenter.SvuotaCampo(TextBoxIdImpresa);
        Presenter.SvuotaCampo(TextBoxIndirizzo);
        Presenter.SvuotaCampo(TextBoxPartitaIva);
        Presenter.SvuotaCampo(TextBoxRagioneSociale);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);

        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxDataNascita);
        Presenter.SvuotaCampo(TextBoxNumeroCameraCommercio);

        CheckBoxArtigiano.Checked = false;

        TextBoxCognome.CssClass =
            TextBoxNome.CssClass =
            TextBoxDataNascita.CssClass = TextBoxNumeroCameraCommercio.CssClass = "campoDisabilitato";

        TextBoxCognome.Enabled =
            TextBoxNome.Enabled =
            TextBoxDataNascita.Enabled = TextBoxNumeroCameraCommercio.Enabled = CheckBoxArtigiano.Checked;

        CaricaTipologieContratto();
        CaricaCasseEdili();
        CaricaProvince();
        CaricaComuni(-1);
        CaricaCAP(-1);
    }

    private Impresa CreaImpresa()
    {
        Impresa impresa = new Impresa();

        if (ViewState["IdImpresa"] != null)
        {
            impresa.IdImpresa = (int?) ViewState["IdImpresa"];
        }

        impresa.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
        impresa.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);

        impresa.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()))
            impresa.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxPartitaIva.Text);

        impresa.Provincia = DropDownListProvincia.SelectedItem.Text;
        impresa.Comune = DropDownListComuni.SelectedItem.Text;
        impresa.Cap = DropDownListCAP.SelectedValue;

        impresa.TipologiaContratto =
            (TipologiaContratto) Enum.Parse(typeof (TipologiaContratto), DropDownListTipologiaContratto.SelectedValue);
        impresa.TipoImpresa = TipologiaImpresa.Nuova;
        impresa.IdTemporaneo = Guid.NewGuid();

        if (!String.IsNullOrEmpty(DropDownListCEProvenienza.SelectedValue))
            impresa.IdCassaEdileProvenienza = DropDownListCEProvenienza.SelectedValue;

        impresa.LavoratoreAutonomo = CheckBoxArtigiano.Checked;

        if (CheckBoxArtigiano.Checked)
        {
            impresa.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
            impresa.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
            impresa.DataNascita = DateTime.Parse(TextBoxDataNascita.Text);
            impresa.NumeroCameraCommercio = Presenter.NormalizzaCampoTesto(TextBoxNumeroCameraCommercio.Text);
        }

        return impresa;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaProvince();
            CaricaTipologieContratto();
            CaricaCasseEdili();
        }
    }

    private void CaricaTipologieContratto()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipologiaContratto,
            Enum.GetNames(typeof (TipologiaContratto)),
            "",
            "");
    }

    public void SetValidationGroup(string validationGroup)
    {
        RequiredFieldValidatorRagSoc.ValidationGroup = validationGroup;
        RequiredFieldValidatorIndirizzo.ValidationGroup = validationGroup;
        RequiredFieldValidatorCAP.ValidationGroup = validationGroup;
        RequiredFieldValidatorProvincia.ValidationGroup = validationGroup;
        RequiredFieldValidatorComune.ValidationGroup = validationGroup;
        RegularExpressionValidatorPIVA.ValidationGroup = validationGroup;
    }

    protected void CheckBoxArtigiano_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxArtigiano.Checked)
        {
            TextBoxCognome.CssClass =
                TextBoxNome.CssClass =
                TextBoxDataNascita.CssClass = TextBoxNumeroCameraCommercio.CssClass = string.Empty;
        }
        else
        {
            TextBoxCognome.CssClass =
                TextBoxNome.CssClass =
                TextBoxDataNascita.CssClass = TextBoxNumeroCameraCommercio.CssClass = "campoDisabilitato";
        }

        TextBoxCognome.Text =
            TextBoxNome.Text =
            TextBoxDataNascita.Text = TextBoxNumeroCameraCommercio.Text = string.Empty;

        TextBoxCognome.Enabled =
            TextBoxNome.Enabled =
            TextBoxDataNascita.Enabled = TextBoxNumeroCameraCommercio.Enabled = CheckBoxArtigiano.Checked;
    }

    protected void CustomValidatorCognome_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxCognome.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorNome_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxNome.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorDataNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxDataNascita.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorNumeroCameraCommercio_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxNumeroCameraCommercio.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorPartitaIva_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!String.IsNullOrEmpty(TextBoxPartitaIva.Text) && TextBoxPartitaIva.Text.Length == 11)
        {
            if (PartitaIvaManager.VerificaPartitaIva(TextBoxPartitaIva.Text))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
        else
        {
            args.IsValid = true;
        }
    }

    #region Biz properties

    private AttestatoRegolaritaBusiness attestatoRegolaritaBiz;

    public AttestatoRegolaritaBusiness AttestatoRegolaritaBiz
    {
        get { return attestatoRegolaritaBiz; }
        set { attestatoRegolaritaBiz = value; }
    }

    #endregion

    #region Metodi per la gestione delle province, comuni, cap

    private void CaricaProvince()
    {
        attestatoRegolaritaBiz.CaricaProvinceInDropDown(DropDownListProvincia);
    }

    private void CaricaComuni(int idProvincia)
    {
        attestatoRegolaritaBiz.CaricaComuniInDropDown(DropDownListComuni, idProvincia);
    }

    private void CaricaCAP(Int64 idComune)
    {
        attestatoRegolaritaBiz.CaricaCapInDropDown(DropDownListCAP, idComune);
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        attestatoRegolaritaBiz.CambioSelezioneDropDownProvincia(DropDownListProvincia, DropDownListComuni,
                                                                DropDownListCAP);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        attestatoRegolaritaBiz.CambioSelezioneDropDownComune(DropDownListComuni, DropDownListCAP);
    }

    #endregion
}