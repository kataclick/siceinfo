using System;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Enums;

public partial class WebControls_CorsiDatiImpresa : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipiIscrizione();
            CaricaTipiContratto();
            CaricaProvince();
        }
    }

    private void CaricaProvince()
    {
        StringCollection province = commonBiz.GetProvinceSiceNew();

        // Provincia
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListProvincia,
            province,
            null,
            null);
    }

    private void CaricaTipiContratto()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipoContratto,
            biz.GetTipiContrattoAll(),
            "Descrizione",
            "IdTipoContratto");
    }

    private void CaricaTipiIscrizione()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipoIscrizione,
            biz.GetTipiIscrizioneAll(),
            "Descrizione",
            "IdTipoIscrizione");
    }

    public Impresa GetImpresa()
    {
        Impresa impresa = null;

        Page.Validate("datiImpresa");
        if (Page.IsValid)
        {
            impresa = new Impresa();
            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
            impresa.Fonte = FonteAnagraficheComuni.Corsi;

            impresa.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
            impresa.PartitaIva = TextBoxPartitaIva.Text;
            impresa.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

            if (!String.IsNullOrEmpty(DropDownListTipoContratto.SelectedValue))
            {
                impresa.TipoContratto = new TipoContratto();
                impresa.TipoContratto.IdTipoContratto = Int32.Parse(DropDownListTipoContratto.SelectedItem.Value);
                impresa.TipoContratto.Descrizione = DropDownListTipoContratto.SelectedItem.Text;
            }

            //if (!String.IsNullOrEmpty(DropDownListTipoIscrizione.SelectedValue))
            //{
            //    impresa.TipoIscrizione = new TipoIscrizione();
            //    impresa.TipoIscrizione.IdTipoIscrizione = Int32.Parse(DropDownListTipoIscrizione.SelectedItem.Value);
            //    impresa.TipoIscrizione.Descrizione = DropDownListTipoIscrizione.SelectedItem.Text;
            //}

            // Il tipo iscrizione � stato stabilito come Cassa Edile (sempre)
            impresa.TipoIscrizione = new TipoIscrizione();
            impresa.TipoIscrizione.IdTipoIscrizione = 1;

            if (!String.IsNullOrEmpty(TextBoxNumeroDipendenti.Text))
            {
                impresa.NumeroDipendenti = Int32.Parse(TextBoxNumeroDipendenti.Text);
            }

            if (!String.IsNullOrEmpty(DropDownListProvincia.SelectedValue))
            {
                impresa.ProvinciaSedeLegale = DropDownListProvincia.SelectedValue;
            }

            if (!String.IsNullOrEmpty(DropDownListComune.SelectedValue))
            {
                impresa.ComuneSedeLegale = DropDownListComune.SelectedValue;
            }
        }

        return impresa;
    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxRagioneSociale);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxPartitaIva);
    }

    private void CaricaComuni(DropDownList dropDownListComuni, string provincia)
    {
        if (!string.IsNullOrEmpty(provincia))
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListComune,
                commonBiz.GetComuniSiceNew(provincia),
                "Comune",
                "CodicePerCombo");
        }
        else
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListComune,
                null,
                "",
                "");
        }
    }

    protected void DropDownListProvinciaNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaComuni(DropDownListComune, DropDownListProvincia.SelectedValue);
    }

    protected void CustomValidatorPartitaIva_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()) &&
            !PartitaIvaManager.VerificaPartitaIva(TextBoxPartitaIva.Text))
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    public void CaricaDatiImpresa(Impresa impresa)
    {
        ResetCampi();

        TextBoxRagioneSociale.Text = impresa.RagioneSociale;
        TextBoxPartitaIva.Text = impresa.PartitaIva;
        TextBoxCodiceFiscale.Text = impresa.CodiceFiscale;

    }
}