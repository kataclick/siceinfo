using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;

public partial class WebControls_RegistrazioneDipendente : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonRegistraDipendente_Click(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text)
            && TextBoxPassword.Text == TextBoxPasswordRidigitata.Text)
        {
            //
            //Dipendente dipendete = new Dipendente();
            Dipendente dipendete = new Dipendente(
                0,
                TextBoxLogin.Text
                );

            dipendete.Nome = TextBoxNome.Text;
            dipendete.Cognome = TextBoxCognome.Text;
            dipendete.EMail = TextBoxEmail.Text;

            //dipendete.Username = TextBoxLogin.Text;
            dipendete.Password = TextBoxPassword.Text;

            GestioneUtentiBiz gu = new GestioneUtentiBiz();

            #region new
            switch (gu.InserisciDipendente(dipendete))
            {
                case ErroriRegistrazione.RegistrazioneEffettuata:
                    TextBoxNome.Text = string.Empty;
                    TextBoxCognome.Text = string.Empty;
                    TextBoxEmail.Text = string.Empty;
                    TextBoxLogin.Text = string.Empty;
                    TextBoxPassword.Text = string.Empty;
                    TextBoxPasswordRidigitata.Text = string.Empty;

                    LabelResult.Text = "Registrazione completata";

                    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                    break;
                case ErroriRegistrazione.ChallengeNonPassato:
                    LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                    break;
                case ErroriRegistrazione.LoginPresente:
                    LabelResult.Text =
                        "La username scelta � gi� presente nel sistema, sceglierne un'altra e riprovare.";
                    break;
                case ErroriRegistrazione.RegistrazioneGiaPresente:
                    LabelResult.Text = "La registrazione risulta come gi� effettuata.";
                    break;
                case ErroriRegistrazione.Errore:
                    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                    break;
                case ErroriRegistrazione.LoginNonCorretta:
                    LabelResult.Text =
                        "La username fornita contiene caratteri non validi o la sua lunghezza non � compresa fra 5 e 15 caratteri.";
                    break;
                case ErroriRegistrazione.PasswordNonCorretta:
                    LabelResult.Text =
                        "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                    break;
                default:
                    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                    break;
            }
            #endregion

            #region old
            //Inseriamo il dipendente nel sistema
            //if (gu.InserisciDipendente(dipendete))
            //{
            //    TextBoxNome.Text = string.Empty;
            //    TextBoxCognome.Text = string.Empty;
            //    TextBoxEmail.Text = string.Empty;
            //    TextBoxLogin.Text = string.Empty;
            //    TextBoxPassword.Text = string.Empty;
            //    TextBoxPasswordRidigitata.Text = string.Empty;

            //    LabelResult.Text = "Registrazione completata";

            //    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
            //}
            //else
            //{
            //    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare.";
            //}
            #endregion
        }
        else
        {
            LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("GestioneUtentiDefault.aspx");
    }
}