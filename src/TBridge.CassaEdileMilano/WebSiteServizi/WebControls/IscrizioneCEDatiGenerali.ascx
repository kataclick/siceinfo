﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneCEDatiGenerali.ascx.cs"
    Inherits="WebControls_IscrizioneCEDatiGenerali" %>

<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

    <script type="text/javascript">
        function CFUpperCaseOnly(sender) {
            sender.value = sender.value.toUpperCase();
    }
    </script>

</telerik:RadScriptBlock>

<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelDatiGenerali" runat="server" Text="Dati generali" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Denominazione o ragione sociale dell'impresa:
        </td>
        <td>
            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="60" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRagioneSociale" runat="server"
                ErrorMessage="Ragione sociale mancante" ControlToValidate="TextBoxRagioneSociale"
                ValidationGroup="stop">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Codice Fiscale:
        </td>
        <td>
            <asp:TextBox ID="TextBoxCF" runat="server" MaxLength="16" Width="300px" onchange="CFUpperCaseOnly(this)" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCFPIva1" runat="server" ErrorMessage="Codice fiscale mancante"
                ControlToValidate="TextBoxCF" ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionCF" runat="server" ControlToValidate="TextBoxCF"
                ErrorMessage="Codice fiscale non corretto" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Partita IVA:
        </td>
        <td>
            <asp:TextBox ID="TextBoxPIVA" runat="server" MaxLength="11" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCFPIva2" runat="server" ErrorMessage="Partita IVA mancante"
                ControlToValidate="TextBoxPIVA" ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPIVA" runat="server"
                ControlToValidate="TextBoxPIVA" ErrorMessage="Partita IVA non corretta" ValidationExpression="^\d{11}$"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorPartitaIVA" runat="server" ControlToValidate="TextBoxPIVA"
                ErrorMessage="Partita IVA non valida" OnServerValidate="CustomValidatorPartitaIVA_ServerValidate"
                ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Matricola INPS:
        </td>
        <td>
            <asp:TextBox ID="TextBoxMatricolaInps" runat="server" MaxLength="12" Width="300px" />
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Codice ditta INAIL:
        </td>
        <td>
            <asp:TextBox ID="TextBoxMatricolaInail" runat="server" MaxLength="9" Width="200px" />
            &nbsp;/
            <asp:TextBox ID="TextBoxControCodiceInail" runat="server" MaxLength="2" Width="80px" />
        </td>
    </tr>
    <tr>
        <td style="width: 200px">
            Data richiesta d'iscrizione alla Cassa Edile (gg/mm/aaaa):
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataRichiestaIscrizione" runat="server" MaxLength="10" Width="300px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataRichiestaIscrizione" runat="server"
                ErrorMessage="Data richiesta iscrizione mancante" ControlToValidate="TextBoxDataRichiestaIscrizione"
                ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataIscrizione" runat="server"
                ControlToValidate="TextBoxDataRichiestaIscrizione" ErrorMessage="Data richiesta iscrizione non corretta"
                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorDataIscrizione" runat="server" ControlToValidate="TextBoxDataRichiestaIscrizione"
                ErrorMessage="Data richiesta d'iscrizione non valida" OnServerValidate="CustomValidatorSmallDateTime_ServerValidate"
                ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr id="trAssociazioneImprenditoriale" runat="server">
        <td style="width: 200px">
            Organizzazione imprenditoriale:
        </td>
        <td colspan="2">
            <asp:UpdatePanel ID="UpdatePanelOrganizzazioneImprenditoriale" runat="server">
                <ContentTemplate>
                    <table class="standardTable">
                        <tr runat="server">
                            <td colspan="2">
                                <asp:DropDownList ID="DropDownListAssociazioneImprenditoriale" runat="server" Width="300px"
                                    AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListAssociazioneImprenditoriale_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Posiz. n°
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxPosAssociazioneImprenditoriale" runat="server" Width="200px"
                                    MaxLength="20"></asp:TextBox>
                            </td>
                            <td>
                                <asp:CustomValidator ID="CustomValidatorPosizioneOrganizzazioneImprenditoriale" runat="server"
                                    ErrorMessage="Posizione organizzazione imprenditoriale mancante" OnServerValidate="CustomValidatorPosizioneOrganizzazioneImprenditoriale_ServerValidate"
                                    ValidationGroup="datiGenerali">*</asp:CustomValidator>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="UpdatePanelTipiImpresaFormaGiuridica" runat="server">
    <ContentTemplate>
        <table class="standardTable">
            <tr>
                <td style="width: 200px">
                    Forma giuridica:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListFormaGiuridica" runat="server" Width="300px" AppendDataBoundItems="True"
                        AutoPostBack="True" OnSelectedIndexChanged="DropDownListFormaGiuridica_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorFormaGiuridica" runat="server"
                        ErrorMessage="Forma giuridica non selezionata" ControlToValidate="DropDownListFormaGiuridica"
                        ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    CCNL Applicato/Tipo impresa:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTipoImpresa" runat="server" Width="300px" AppendDataBoundItems="True"
                        AutoPostBack="True" OnSelectedIndexChanged="DropDownListTipoImpresa_SelectedIndexChanged"
                        Enabled="False">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoImpresa" runat="server"
                        ErrorMessage="CCNL Applicato/Tipo impresa non selezionato" ControlToValidate="DropDownListTipoImpresa"
                        ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidatorTipoImpresa" runat="server" ErrorMessage="Deve essere specificato il N° di iscrizione all'albo delle imprese artigiane"
                        ValidationGroup="datiGenerali" OnServerValidate="CustomValidatorTipoImpresa_ServerValidate">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    N° iscrizione al Registro delle Imprese:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNumCameraCommercio" runat="server" MaxLength="10" Width="300px" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNumeroCameraCommercio" runat="server"
                        ErrorMessage="Numero iscrizione Camera di commercio mancante" ControlToValidate="TextBoxNumCameraCommercio"
                        ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorNumeroCameraCommercio"
                        runat="server" ControlToValidate="TextBoxNumCameraCommercio" ErrorMessage="N° iscrizione al Registro delle Imprese non corretto"
                        ValidationExpression="^\d{0,50}$" ValidationGroup="datiGenerali">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    Iscritta dal (gg/mm/aaaa):
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIscrittaDal" runat="server" MaxLength="10" Width="300px" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorIscrittaDal" runat="server"
                        ErrorMessage="Data iscrizione mancante" ControlToValidate="TextBoxIscrittaDal"
                        ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorIscrittaDal" runat="server"
                        ControlToValidate="TextBoxIscrittaDal" ErrorMessage="Data iscrizione non corretta"
                        ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="stop">*</asp:RegularExpressionValidator>
                    <asp:CustomValidator ID="CustomValidatorIscrittaDal" runat="server" ControlToValidate="TextBoxIscrittaDal"
                        ErrorMessage="Data di iscrizione alla camera di commercio non valida" ValidationGroup="stop"
                        OnServerValidate="CustomValidatorIscrittaDal_ServerValidate">*</asp:CustomValidator>
                </td>
            </tr>
        </table>
        <asp:Panel ID="PanelNumImpreseArtigiane" runat="server" Enabled="false">
            <table class="standardTable">
                <tr>
                    <td style="width: 200px">
                        N° iscrizione al Registro delle Imprese - sezione speciale (solo per imprese 
                        artigiane):
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxNumImpreseArtigiane" runat="server" MaxLength="10" Width="300px"
                            CssClass="campoDisabilitato" />
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorNumeroImpreseArtigiane"
                            runat="server" ControlToValidate="TextBoxNumImpreseArtigiane" ErrorMessage="N° iscrizione al Registro delle Imprese - sezione speciale non corretto"
                            ValidationExpression="^\d{0,50}$" ValidationGroup="datiGenerali">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<table class="standardTable">
    <tr>
        <td style="width: 200px">
            Codice attività:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCodiceAttivita" runat="server" Width="300px" AppendDataBoundItems="True"
                OnDataBound="DropDownListCodiceAttivita_DataBound">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceAttivita" runat="server"
                ErrorMessage="Codice attivit&#224; non selezionato" ControlToValidate="DropDownListCodiceAttivita"
                ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
        </td>
    </tr>
</table>
<asp:UpdatePanel ID="UpdatePanelTipoInvioDenuncia" runat="server">
    <ContentTemplate>
        <table class="standardTable">
            <tr>
                <td style="width: 200px">
                    Invio denuncia:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTipoInvioDenuncia" runat="server" Width="300px"
                        AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListTipoInvioDenuncia_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoInvioDenuncia" runat="server"
                        ErrorMessage="Tipo invio denuncia non selezionato" ControlToValidate="DropDownListTipoInvioDenuncia"
                        ValidationGroup="datiGenerali">*</asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidatorInvioDenuncia" runat="server" ErrorMessage="Alcuni dati del consulente sono mancanti"
                        OnServerValidate="CustomValidatorInvioDenuncia_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                </td>
            </tr>
        </table>
        <asp:Panel ID="PanelImmissioneDatiConsulente" runat="server" Visible="true">
            <table class="standardTable">
                <tr>
                    <td style="width: 200px">
                        Consulente
                    </td>
                    <td>
                        <asp:Panel ID="PanelDatiConsulenteImpresa" runat="server" Enabled="false">
                            <table class="standardTable">
                                <tr runat="server" visible="false">
                                    <td>
                                        Codice
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxCodiceConsulente" runat="server" MaxLength="10" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodiceConsulente" runat="server"
                                            ControlToValidate="TextBoxCodiceConsulente" ErrorMessage="Codice consulente non corretto"
                                            ValidationExpression="^\d{0,10}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Ragione sociale
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxRagioneSocialeConsulente" runat="server" MaxLength="60" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr runat="server" visible="false">
                                    <td>
                                        Comune
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxComuneConsulente" runat="server" MaxLength="30" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Codice fiscale
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxCodiceFiscaleConsulente" runat="server" MaxLength="16" Width="200px"
                                            CssClass="campoDisabilitato" onchange="CFUpperCaseOnly(this); return false;"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodiceFiscaleConsulente"
                                            runat="server" ControlToValidate="TextBoxCodiceFiscaleConsulente" ErrorMessage="Codice fiscale del consulente non corretto"
                                            ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                                            ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Provincia
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListProvinciaConsulente" runat="server" Width="200px"
                                            AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvinciaConsulente_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comune
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListComuneConsulente" runat="server" Width="200px"
                                            AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListComuneConsulente_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CAP
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxCAPConsulente" runat="server" MaxLength="5" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorCAPConsulente" ControlToValidate="TextBoxCAPConsulente"
                                            ValidationGroup="stop" runat="server" ErrorMessage="Cap consulente non corretto"
                                            ValidationExpression="^\d{5}$">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Indirizzo
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxIndirizzoConsulente" runat="server" MaxLength="50" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Telefono
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxTelefonoConsulente" runat="server" MaxLength="20" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorTelefonoConsulente"
                                            runat="server" ControlToValidate="TextBoxTelefonoConsulente" ErrorMessage="Telefono consulente non corretto"
                                            ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Fax
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxFaxConsulente" runat="server" MaxLength="20" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorFaxConsulente" runat="server"
                                            ControlToValidate="TextBoxFaxConsulente" ErrorMessage="Fax consulente non corretto"
                                            ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxEmailConsulente" runat="server" MaxLength="100" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmailConsulente" runat="server"
                                            ControlToValidate="TextBoxEmailConsulente" ErrorMessage="Email consulente non corretta"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        PEC
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxPECConsulente" runat="server" MaxLength="255" Width="200px"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorPecConsulente" runat="server"
                                            ControlToValidate="TextBoxPECConsulente" ErrorMessage="Pec consulente non corretta"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table class="standardTable">
            <tr>
                <td style="width: 200px">
                    Corrispondenza presso:
                </td>
                <td>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:RadioButton ID="RadioButtonSedeLegale" runat="server" GroupName="corrispondenza"
                                    Text="Sede legale" Checked="True" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="RadioButtonSedeAmministrativa" runat="server" GroupName="corrispondenza"
                                    Text="Sede amministrativa" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton ID="RadioButtonConsulente" runat="server" GroupName="corrispondenza"
                                    Text="Consulente" Enabled="False" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
