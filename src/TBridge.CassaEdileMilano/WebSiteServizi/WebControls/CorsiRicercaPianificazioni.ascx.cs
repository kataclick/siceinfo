using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Delegates;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.Presenter;

public partial class WebControls_CorsiRicercaPianificazioni : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    public event ProgrammazioneSelectedEventHandler OnProgrammazioneSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaCorsi();
            CaricaLocazioni();
        }
    }

    private void CaricaLocazioni()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListLocazione,
            biz.GetLocazioni(),
            "IndirizzoCompleto",
            "IdLocazione");
    }

    private void CaricaCorsi()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCorso,
            biz.GetCorsiAll(),
            "NomeCompleto",
            "IdCorso");
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CercaPianificazioni(0);
        }
    }

    private void CercaPianificazioni(Int32 pagina)
    {
        ProgrammazioneModuloFilter filtro = CreaFiltro();

        Presenter.CaricaElementiInGridView(
            GridViewFatturePianificazioni,
            biz.GetProgrammazioni(filtro),
            pagina);
    }

    private ProgrammazioneModuloFilter CreaFiltro()
    {
        ProgrammazioneModuloFilter filtro = new ProgrammazioneModuloFilter();

        if (!String.IsNullOrEmpty(TextBoxDal.Text))
        {
            filtro.Dal = DateTime.Parse(TextBoxDal.Text.Replace('.', '/'));
        }

        if (!String.IsNullOrEmpty(TextBoxAl.Text))
        {
            filtro.Al = DateTime.Parse(TextBoxAl.Text.Replace('.', '/'));
        }

        if (!String.IsNullOrEmpty(DropDownListCorso.SelectedValue))
        {
            filtro.IdCorso = Int32.Parse(DropDownListCorso.SelectedValue);
        }

        if (!String.IsNullOrEmpty(DropDownListLocazione.SelectedValue))
        {
            filtro.IdLocazione = Int32.Parse(DropDownListLocazione.SelectedValue);
        }

        return filtro;
    }

    protected void GridViewFatturePianificazioni_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idProgrammazione =
            (Int32) GridViewFatturePianificazioni.DataKeys[e.NewSelectedIndex].Values["IdProgrammazione"];
        Int32 idProgrammazioneModulo =
            (Int32) GridViewFatturePianificazioni.DataKeys[e.NewSelectedIndex].Values["IdProgrammazioneModulo"];

        if (OnProgrammazioneSelected != null)
        {
            OnProgrammazioneSelected(idProgrammazione, idProgrammazioneModulo);
        }
    }

    protected void GridViewFatturePianificazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("cercaPianificazione");

        if (Page.IsValid)
        {
            CercaPianificazioni(e.NewPageIndex);
        }
    }

    public void ImpostaNumeroElementiPagina(Int32 numeroElementi)
    {
        GridViewFatturePianificazioni.PageSize = numeroElementi;
    }

    protected void GridViewFatturePianificazioni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ProgrammazioneModulo prog = (ProgrammazioneModulo) e.Row.DataItem;
            Image iSemaforo = (Image) e.Row.FindControl("ImageConfermaPresenze");
            Label lSchedulazione = (Label) e.Row.FindControl("LabelSchedulazione");
            Label lLocazione = (Label) e.Row.FindControl("LabelLocazione");

            lSchedulazione.Text = String.Format("{0} - {1}", prog.Schedulazione.Value.ToString("dd/MM/yyyy hh:mm"),
                                                prog.SchedulazioneFine.Value.ToString("HH:mm"));
            lLocazione.Text = prog.Locazione.IndirizzoCompleto;
            iSemaforo.ImageUrl = biz.ConvertiBoolInSemaforo(prog.PresenzeConfermate);
        }
    }

    public void ForzaRicerca()
    {
        CercaPianificazioni(GridViewFatturePianificazioni.PageIndex);
    }

    public void ImpostaDataPianificazione(DateTime dataPianificazione)
    {
        TextBoxDal.Text = dataPianificazione.ToShortDateString();
        TextBoxAl.Text = dataPianificazione.ToShortDateString();
    }

    public void ImpostaCorso(Int32 idCorso)
    {
        DropDownListCorso.SelectedValue = idCorso.ToString();
    }

    public void ImpostaLocazione(Int32 idLocazione)
    {
        DropDownListLocazione.SelectedValue = idLocazione.ToString();
    }
}