<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniSelezioneCasseEdili.ascx.cs"
    Inherits="WebControls_PrestazioniSelezioneCasseEdili" %>
<table class="standardTable">
    <tr>
        <td>
            Cassa Edile
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCasseEdili" runat="server" Width="300px" AppendDataBoundItems="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:Button ID="ButtonAggiungiCassaEdile" runat="server" Text="Aggiungi alla lista"
                OnClick="ButtonAggiungiCassaEdile_Click" ValidationGroup="inserisciCassaEdile" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCasseEdili" runat="server"
                ErrorMessage="Selezionare una Cassa Edile" ValidationGroup="inserisciCassaEdile"
                ControlToValidate="DropDownListCasseEdili"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:GridView ID="GridViewCasseEdili" runat="server" AutoGenerateColumns="False"
                DataKeyNames="IdCassaEdile" ShowHeader="False" Width="530px" OnRowDeleting="GridViewCasseEdili_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="Descrizione" HeaderText="Cassa Edile" />
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rimuovi dalla lista"
                        ShowDeleteButton="True">
                        <ItemStyle Width="10px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna Cassa Edile selezionata
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
