using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;

public partial class WebControls_CptRicercaNotificaPerAggiornamento : UserControl
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAree();

            SelezionaArea();
        }
    }

    private void SelezionaArea()
    {
        // Se l'utente � autorizzato all'inserimento di notifiche per una sola area
        // blocco la selezione
        if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica.ToString())
            ^
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi.ToString())
            )
        {
            // Seleziono l'area Milano
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica.ToString()))
            {
                DropDownListArea.SelectedValue = "1";
            }

            // Seleziono l'area Lodi
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi.ToString()))
            {
                DropDownListArea.SelectedValue = "2";
            }

            DropDownListArea.Enabled = false;
        }
    }

    private void CaricaAree()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListArea,
            biz.GetAree(),
            "Descrizione",
            "IdArea");
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            LabelErrore.Text = string.Empty;
            CaricaNotifiche();
        }
    }

    public void CaricaNotifiche()
    {
        if (string.IsNullOrEmpty(TextBoxData.Text) && string.IsNullOrEmpty(TextBoxIndirizzo.Text) &&
            string.IsNullOrEmpty(TextBoxCommittente.Text) && string.IsNullOrEmpty(TextBoxDataInserimento.Text)
            && string.IsNullOrEmpty(TextBoxNumeroAppalto.Text) && string.IsNullOrEmpty(TextBoxProtocollo.Text))
        {
            LabelErrore.Text = "Digitare un filtro";
        }
        else
        {
            LabelErrore.Text = string.Empty;
            DateTime? data = null;
            string committente = null;
            string indirizzo = null;
            DateTime? dataInserimento = null;
            string numeroAppalto = null;
            int? protocollo = null;
            Int16 idArea;

            string datRic = TextBoxData.Text.Trim();
            if (!string.IsNullOrEmpty(datRic))
            {
                string dataS = datRic.Substring(0, 2) + "/" + datRic.Substring(2, 2) + "/" + datRic.Substring(4, 4);
                data = DateTime.Parse(dataS);
            }

            string comRic = TextBoxCommittente.Text.Trim();
            if (!string.IsNullOrEmpty(comRic))
                committente = comRic;

            string indRic = TextBoxIndirizzo.Text.Trim();
            if (!string.IsNullOrEmpty(indRic))
                indirizzo = indRic;

            string datInsRic = TextBoxDataInserimento.Text.Trim();
            if (!string.IsNullOrEmpty(datInsRic))
            {
                string dataInsS = datInsRic.Substring(0, 2) + "/" + datInsRic.Substring(2, 2) + "/" +
                                  datInsRic.Substring(4, 4);
                dataInserimento = DateTime.Parse(dataInsS);
            }

            if (!string.IsNullOrEmpty(TextBoxProtocollo.Text))
                protocollo = Int32.Parse(TextBoxProtocollo.Text);

            string numAppRic = TextBoxNumeroAppalto.Text.Trim();
            if (!string.IsNullOrEmpty(numAppRic))
                numeroAppalto = numAppRic;

            idArea = Int16.Parse(DropDownListArea.SelectedValue);

            NotificaCollection notifiche =
                biz.RicercaNotifichePerAggiornamento(data, committente, indirizzo, dataInserimento, numAppRic,
                                                     protocollo, idArea, null);
            GridViewNotifiche.DataSource = notifiche;
            GridViewNotifiche.DataBind();
        }
    }

    protected void GridViewNotifiche_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idNotifica = (int) GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotifica"];
        int? idNotificaPadre = null;
        if (GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotificaPadre"] != null)
            idNotificaPadre = (int) GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotificaPadre"];

        if (idNotificaPadre.HasValue)
            Response.Redirect("~/Cpt/CptInserimentoNotifica.aspx?idNotificaPadre=" + idNotificaPadre);
        else
            Response.Redirect("~/Cpt/CptInserimentoNotifica.aspx?idNotificaPadre=" + idNotifica);
    }

    protected void GridViewNotifiche_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notifica = (Notifica) e.Row.DataItem;
            GridView gvIndirizzi = (GridView) e.Row.FindControl("GridViewIndirizzi");
            Label lTipoNotifica = (Label) e.Row.FindControl("LabelTipoNotifica");

            if (notifica.IdNotificaPadre.HasValue)
            {
                lTipoNotifica.Text = "Aggiornamento";
                e.Row.ForeColor = Color.Gray;
            }
            else
                lTipoNotifica.Text = "Notifica preliminare";

            gvIndirizzi.DataSource = notifica.Indirizzi;
            gvIndirizzi.DataBind();
        }
    }

    protected void GridViewNotifiche_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idNotifica = (int) GridViewNotifiche.DataKeys[e.RowIndex].Values["IdNotifica"];

        Response.Redirect("~/Cpt/CPTStampaProtocolloNotifica.aspx?idNotifica=" + idNotifica);
    }

    protected void GridViewNotifiche_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaNotifiche();

        GridViewNotifiche.PageIndex = e.NewPageIndex;
        GridViewNotifiche.DataBind();
    }

    protected void GridViewNotifiche_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "dettagli")
        {
            int indice = Int32.Parse(e.CommandArgument.ToString());
            int idNotifica = (int) GridViewNotifiche.DataKeys[indice].Values["IdNotifica"];

            Response.Redirect("~/Cpt/CptNotificaDettagli.aspx?idNotifica=" + idNotifica);
        }
    }
}