using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class WebControls_PrestazioniDatiFattura : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ImpostaValidatorPerDataFattura(int limiteFattureMesi)
    {
        CustomValidatorFattureNuovaDataLimite.ErrorMessage =
            String.Format(
                "La data della fattura non pu� essere antecedente il {0}� mese rispetto alla data della domanda",
                limiteFattureMesi);
        TextBoxLimiteFatturaAntecedente.Text = DateTime.Now.AddMonths(-limiteFattureMesi).ToShortDateString();
        CompareValidatorFattureNuovaDataLimiteOggi.ValueToCompare = DateTime.Today.ToShortDateString();
    }

    public FatturaDichiarata CreaFattura()
    {
        FatturaDichiarata fattura = new FatturaDichiarata();

        fattura.Data = DateTime.Parse(TextBoxFattureNuovaData.Text.Replace('.', '/'));
        fattura.Numero = TextBoxFattureNuovaNumero.Text.Trim().ToUpper();
        fattura.ImportoTotale = decimal.Parse(TextBoxFattureNuovaImporto.Text);
        //fattura.Saldo = RadioButtonAnticipoNo.Checked;
        fattura.Saldo = true;

        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
        if (idUtente > 0)
        {
            fattura.IdUtenteInserimento = idUtente;
        }

        return fattura;
    }

    public void ResetCampi()
    {
        TextBoxFattureNuovaData.Text = null;
        TextBoxFattureNuovaNumero.Text = null;
        TextBoxFattureNuovaImporto.Text = null;
    }

    protected void CustomValidatorFattureNuovaDataLimite_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Faccio uil controllo Data fattura - Data domanda solo sulla fattura a saldo
        if (RadioButtonAnticipoNo.Checked)
        {
            DateTime dataFattura = DateTime.Parse(TextBoxFattureNuovaData.Text.Replace('.', '/'));
            DateTime dataLimite = DateTime.Parse(TextBoxLimiteFatturaAntecedente.Text.Replace('.', '/'));

            if (dataFattura < dataLimite)
                args.IsValid = false;
        }
    }
}