﻿using System;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Presenter;

public partial class WebControls_AccessoCantieriLavoratoriImpresa : UserControl
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void Inizializza(WhiteList domanda, ImpresaCollection imprese)
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListImprese,
            imprese,
            "CodiceERagioneSociale",
            "IdImpresaComposto");

        ViewState["LavoratoriImpresa"] = domanda.Lavoratori;
        ViewState["IdDomanda"] = domanda.IdWhiteList;
    }

    protected void DropDownListImprese_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(DropDownListImprese.SelectedValue))
        {
            int? idImpresa = null;
            Guid? idTemporaneo = null;
            string[] tipoId = DropDownListImprese.SelectedValue.Split('|');
            TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

            GridViewLavoratori.Visible = true;

            if (!String.IsNullOrEmpty(tipoId[1]))
            {
                idImpresa = Int32.Parse(tipoId[1]);
            }

            if (!String.IsNullOrEmpty(tipoId[2]))
            {
                Guid gid = new Guid(tipoId[2]);
                idTemporaneo = gid;
            }

            WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection) ViewState["LavoratoriImpresa"];
            WhiteListImpresa domandaImpresa = null;

            if (domandeImprese != null)
            {
                domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, idImpresa, idTemporaneo, null);
            }
            else
            {
                Int32? idDomanda = (Int32?) ViewState["IdDomanda"];

                if (tipoImpresa == TipologiaImpresa.SiceNew)
                    domandaImpresa = _biz.GetLavoratoriInDomandaPerImpresa(idDomanda.Value, idImpresa, null);
                else if (tipoImpresa == TipologiaImpresa.Nuova)
                    domandaImpresa = _biz.GetLavoratoriInDomandaPerImpresa(idDomanda.Value, null, idImpresa);
            }

            if (domandaImpresa != null)
            {
                Presenter.CaricaElementiInGridView(
                    GridViewLavoratori,
                    domandaImpresa.Lavoratori,
                    0);
            }
            else
            {
                Presenter.CaricaElementiInGridView(
                    GridViewLavoratori,
                    null,
                    0);
            }
        }
        else
        {
            Presenter.CaricaElementiInGridView(
                GridViewLavoratori,
                null,
                0);

            GridViewLavoratori.Visible = false;
        }
    }
}