using System;
using System.Web.UI;

public partial class WebControls_LiberatoriaSubappalti : UserControl
{
    //private string ragioneSocialeImpresa = string.Empty;

    /// <summary>
    /// Set per impostare il nome dell'impresa da visualizzare all'interno del disclaimer/liberatoria
    /// </summary>
    public string RagioneSocialeImpresa
    {
        set
        {
            //ragioneSocialeImpresa = value; 
            LabelRagioneSocialeImpresa.Text = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}