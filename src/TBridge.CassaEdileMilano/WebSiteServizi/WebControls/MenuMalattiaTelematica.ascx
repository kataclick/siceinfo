<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuMalattiaTelematica.ascx.cs"
    Inherits="WebControls_MenuMalattiaTelematica" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeImpresa.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeBackOffice.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaEstrattoConto.ToString())
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Malattia / Infortunio
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeImpresa))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/MalattiaTelematica/RicercaAssenze.aspx" runat="server">Gestione assenze</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeBackOffice))
        {
    %>
    <tr>
        <td>
            <a id="A6" href="~/MalattiaTelematica/RicercaAssenzeBackOffice.aspx" runat="server">
                Gestione assenze</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaUploadCertificati))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/MalattiaTelematica/UploadCertificati.aspx" runat="server">Caricamento
                file XML</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaEstrattoConto))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/MalattiaTelematica/EstrattoConto.aspx" runat="server">Estratto conto</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaMessaggi))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/MalattiaTelematica/RicercaMessaggi.aspx" runat="server">Ricerca messaggi</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaErrori))
        {
    %>
    <tr>
        <td>
            <a id="A8" href="~/MalattiaTelematica/RicercaErrori.aspx" runat="server">Ricerca errori</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeInAttesa))
        {
    %>
    <tr>
        <td>
            <a id="A9" href="~/MalattiaTelematica/RicercaAssenzaInAttesa.aspx" runat="server">Ricerca assenze in attesa</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaStatisticaLiquidazioni))
        {
    %>
    <tr>
        <td>
            <a id="A5" href="~/MalattiaTelematica/StatisticaLiquidazioni.aspx" runat="server">Statistica
                liquidazioni</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.MalattiaTelematicaStatisticaEvase))
        {
    %>
    <tr>
        <td>
            <a id="A7" href="~/MalattiaTelematica/StatisticaEvase.aspx" runat="server">Statistica
                evase</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
