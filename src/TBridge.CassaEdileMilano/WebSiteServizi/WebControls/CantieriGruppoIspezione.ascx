<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CantieriGruppoIspezione.ascx.cs" Inherits="WebControls_CantieriGruppoIspezione" %>
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelTitolo" runat="server" Font-Bold="true">Eventuali altri ispettori</asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:GridView ID="GridViewGruppoIspezione" runat="server" AutoGenerateColumns="False" Width="400px" OnRowDeleting="GridViewGruppoIspezione_RowDeleting">
                <Columns>
                    <asp:BoundField HeaderText="Ispettore" DataField="NomeCompleto" />
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rimuovi" ShowDeleteButton="True">
                        <ItemStyle Width="10px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessun ispettore aggiuntivo ha partecipato all'ispezione
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList ID="DropDownListIspettori" runat="server" Width="250px"></asp:DropDownList>    
        </td>
        <td>
            <asp:Button ID="ButtonAggiungi" runat="server" Width="100px" Text="Aggiungi" OnClick="ButtonAggiungi_Click" />
        </td>
    </tr>
</table>
