<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaImpreseSubappalti.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaImpreseSubappalti" %>
<asp:Panel ID="PanelInserimentoImpresa" runat="server" Visible="true" Width="450px">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelCodiceImpresa" runat="server" Visible="False" Width="300px">
                <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxIdImpresa" runat="server" Enabled="False" ReadOnly="True"
                    Width="100px"></asp:TextBox></asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Width="420px">
                <table class="borderedTable">
                    <tr>
                        <td>
                            <asp:CheckBox ID="CheckBoxArtigiano" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxArtigiano_CheckedChanged"
                                Text="Artigiano autonomo senza dipendenti" />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cognome*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="30" Width="200px" CssClass="campoDisabilitato"
                                Enabled="False"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorCognome" runat="server" ErrorMessage="Digitare un cognome per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorCognome_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nome*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="30" Width="200px" CssClass="campoDisabilitato"
                                Enabled="False"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorNome" runat="server" ErrorMessage="Digitare un nome per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorNome_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data di nascita (gg\mm\aaaa)*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="200px"
                                CssClass="campoDisabilitato" Enabled="False"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorDataNascita" runat="server" ErrorMessage="Digitare una data di nascita per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorDataNascita_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                            <asp:RangeValidator ID="RangeValidatorDataNascita" runat="server" ControlToValidate="TextBoxDataNascita"
                                ErrorMessage="data di nascita artigiano autonomo errata" MaximumValue="06/06/2079"
                                MinimumValue="01/01/1900" Type="Date" ValidationGroup="ValidaInserimentoImpresa">*</asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            N� posiz. Camera di Commercio*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNumeroCameraCommercio" runat="server" MaxLength="10" Width="200px"
                                CssClass="campoDisabilitato" Enabled="False"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorNumeroCameraCommercio" runat="server" ErrorMessage="Digitare il numero posizione Camera di Commercio per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorNumeroCameraCommercio_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <table class="standardTable">
                    <tr>
                        <td>
                            Ragione sociale*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRagSoc" runat="server" ControlToValidate="TextBoxRagioneSociale"
                                ErrorMessage="Digitare una ragione sociale" ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice fiscale*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodFisc" runat="server"
                                ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Codice fiscale errato"
                                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$|^\d{11}$"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                                ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Digitare un codice fiscale"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Partita IVA*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxPartitaIva" runat="server" MaxLength="11" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPIVA" runat="server"
                                ControlToValidate="TextBoxPartitaIva" ErrorMessage="Partita IVA errata" ValidationExpression="^\d{11}$"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPartitaIVA" runat="server"
                                ControlToValidate="TextBoxPartitaIva" ErrorMessage="Digitare una partita IVA"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorPartitaIva" runat="server" ErrorMessage="Partita IVA non valida"
                                ValidationGroup="ValidaInserimentoImpresa" OnServerValidate="CustomValidatorPartitaIva_ServerValidate">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tipologia contratto*
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipologiaContratto" runat="server" Width="200px"
                                AppendDataBoundItems="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipContratto" runat="server"
                                ControlToValidate="DropDownListTipologiaContratto" ErrorMessage="Selezionare la tipologia di contratto"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Indirizzo*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" Height="41px" MaxLength="255" TextMode="MultiLine"
                                Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndirizzo" runat="server" ControlToValidate="TextBoxIndirizzo"
                                ErrorMessage="Digitare un indirizzo" ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Provincia*
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListProvincia" runat="server" AppendDataBoundItems="True"
                                AutoPostBack="True" Height="17px" OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorProvincia" runat="server" ControlToValidate="DropDownListProvincia"
                                ErrorMessage="Scegliere una provincia" ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune*
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListComuni" runat="server" AppendDataBoundItems="True"
                                AutoPostBack="True" Height="16px" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorComune" runat="server" ControlToValidate="DropDownListComuni"
                                ErrorMessage="Scegliere un comune" ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cap*
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListCAP" runat="server" AppendDataBoundItems="True"
                                Height="17px" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCAP" runat="server" ControlToValidate="DropDownListCAP"
                                EnableViewState="False" ErrorMessage="Scegliere un CAP" ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Altra Cassa Edile di provenienza
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListCEProvenienza" runat="server" AppendDataBoundItems="True"
                                Height="17px" Width="200px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="ValidaInserimentoImpresa"
                                CssClass="messaggiErrore" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            * campi obbligatori
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
