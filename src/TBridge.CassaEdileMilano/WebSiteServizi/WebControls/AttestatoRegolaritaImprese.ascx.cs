#region

using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;

#endregion

public partial class WebControls_AttestatoRegolaritaImprese : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    private Int32 idUtente;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region impostiamo eventi e Biz dei controlli

        //Registriamo gli eventi del controllo
        AttestatoRegolaritaRicercaImpresaAppaltataDa.OnImpresaSelected +=
            AttestatoRegolaritaRicercaImpresa1_OnImpresaSelected;
        AttestatoRegolaritaRicercaImpresaAppaltataDa.OnNuovaImpresaSelected +=
            AttestatoRegolaritaRicercaImpresaAppaltataDa_OnNuovaImpresaSelected;
        AttestatoRegolaritaRicercaImpresaSubappaltatrice.OnImpresaSelected +=
            AttestatoRegolaritaRicercaImpresa2_OnImpresaSelected;
        AttestatoRegolaritaRicercaImpresaSubappaltatrice.OnNuovaImpresaSelected +=
            AttestatoRegolaritaRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected;

        //Forniamo al controllo le logiche di gestione 
        AttestatoRegolaritaImpreseSubappaltiAppaltataDa.AttestatoRegolaritaBiz = biz;

        //Forniamo al controllo le logiche di gestione
        AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.AttestatoRegolaritaBiz = biz;

        #endregion

        #region Settiamo i validation group

        //Settiamo il validation group cross-control
        string validationGroupImpresaAppaltataDa = "validationGroupImpresaAppaltataDa";
        ButtonInserisciNuovaImpresaSubappaltata.ValidationGroup = validationGroupImpresaAppaltataDa;
        AttestatoRegolaritaImpreseSubappaltiAppaltataDa.SetValidationGroup(validationGroupImpresaAppaltataDa);

        string validationGroupImpresaSubappaltatrice = "validationGroupImpresaSubappaltatrice";
        ButtonInserisciNuovaImpresaAppaltante.ValidationGroup = validationGroupImpresaSubappaltatrice;
        AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.SetValidationGroup(validationGroupImpresaSubappaltatrice);

        #endregion

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();

        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresaAppaltataDa') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovaImpresaSubappaltata, null) + ";");
        sb.Append("return true;");
        ButtonInserisciNuovaImpresaSubappaltata.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciNuovaImpresaSubappaltata);
        
        ////resettiamo
        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('validationGroupImpresaSubappaltatrice') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovaImpresaAppaltante, null) + ";");
        sb.Append("return true;");
        ButtonInserisciNuovaImpresaAppaltante.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciNuovaImpresaAppaltante);
        
        #endregion

        //idUtente = ((TBridge.Cemi.GestioneUtenti.Business.Identities.IUtente)
        //        HttpContext.Current.User.Identity).IdUtente;
        idUtente = GestioneUtentiBiz.GetIdUtente();

        if (!Page.IsPostBack)
        {
            if (ViewState["subappalti"] == null)
            {
                ViewState["subappalti"] = new SubappaltoCollection();
            }

            CaricaSubappalti();
        }
    }

    private void CaricaSubappalti()
    {
        if (ViewState["subappalti"] != null)
        {
            GridViewSubappalti.DataSource = ViewState["subappalti"];
            GridViewSubappalti.DataBind();

            #region Carica dropdown list imprese

            ImpresaCollection imprese = new ImpresaCollection();

            if (ViewState["impresaDetentriceAppalto"] != null)
            {
                Impresa impresaDetentriceAppalto = (Impresa) ViewState["impresaDetentriceAppalto"];
                imprese.Add(impresaDetentriceAppalto);
            }

            if (ViewState["subappalti"] != null)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                foreach (Subappalto sub in subappalti)
                {
                    if (!imprese.Contains(sub.Appaltata))
                        imprese.Add(sub.Appaltata);
                }
            }

            ImpresaCollection impreseTot = new ImpresaCollection();

            if (ViewState["impresaDetentriceAppalto"] != null)
            {
                Impresa impresaDetentriceAppalto = (Impresa) ViewState["impresaDetentriceAppalto"];
                impreseTot.Add(impresaDetentriceAppalto);
            }

            if (ViewState["subappalti"] != null)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                foreach (Subappalto sub in subappalti)
                {
                    if (!impreseTot.Contains(sub.Appaltata))
                        impreseTot.Add(sub.Appaltata);

                    if (sub.Appaltante != null)
                    {
                        if (!impreseTot.Contains(sub.Appaltante))
                            impreseTot.Add(sub.Appaltante);
                    }
                }
            }

            ViewState["impresaCollection"] = impreseTot;

            //

            DropDownListImpresaSubappaltata.DataSource = impreseTot; //imprese;

            DropDownListImpresaSubappaltata.DataTextField = "RagioneSociale";
            DropDownListImpresaSubappaltata.DataValueField = "IdImpresaComposto";

            DropDownListImpresaSubappaltata.DataBind();

            //

            DropDownListImpresaAppaltante.DataSource = impreseTot; //imprese;

            DropDownListImpresaAppaltante.DataTextField = "RagioneSociale";
            DropDownListImpresaAppaltante.DataValueField = "IdImpresaComposto";

            DropDownListImpresaAppaltante.DataBind();

            if (impreseTot.Count == 0)
            {
                PanelImpresaAppaltanteDropDown.Visible = false;

                PanelRicercaNuovaImpresaAppaltante.Visible = true;
                AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = true;
                PanelInserisciImpresaAppaltante.Visible = false;

                PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
                PanelInserisciImpresaSubappaltata.Visible = false;
            }
            else
            {
                PanelImpresaAppaltanteDropDown.Visible = true;

                PanelRicercaNuovaImpresaAppaltante.Visible = false;
                AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = false;
                PanelInserisciImpresaAppaltante.Visible = false;

                PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
                PanelInserisciImpresaSubappaltata.Visible = false;
            }

            #endregion
        }
    }

    public void CaricaImprese(SubappaltoCollection subappalti)
    {
        ViewState["subappalti"] = subappalti;
    }

    public ImpresaCollection GetImprese()
    {
        return (ImpresaCollection) ViewState["impresaCollection"];
    }

    public SubappaltoCollection GetSubappalti()
    {
        return (SubappaltoCollection) ViewState["subappalti"];
    }

    protected void ButtonAltro_Click(object sender, EventArgs e)
    {
        PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
        AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
        PanelInserisciImpresaSubappaltata.Visible = false;
    }

    protected void ButtonSelezionaImpresaDaDropDownList_Click(object sender, EventArgs e)
    {
        //if (DropDownListImpresaSubappaltata.SelectedItem != null)
        //{
        //    string ragioneSociale = DropDownListImpresaSubappaltata.SelectedItem.Text;
        //    int idImpresa;
        //    TipologiaImpresa tipoImpresa;

        //    string[] impresaIds = DropDownListImpresaSubappaltata.SelectedItem.Value.Split('|');
        //    tipoImpresa = (TipologiaImpresa) Int32.Parse(impresaIds[0]);

        //    Impresa impresaSel;

        //    if (tipoImpresa == TipologiaImpresa.SiceNew)
        //    {
        //        idImpresa = int.Parse(impresaIds[1]);
        //        ImpresaCollection imprese = (ImpresaCollection) ViewState["impresaCollection"];
        //        impresaSel = imprese.Select(idImpresa);
        //    }
        //    else
        //    {
        //        ImpresaCollection imprese = (ImpresaCollection) ViewState["impreseSelColl"];
        //        impresaSel = imprese.Select(ragioneSociale);
        //    }

        //    ImpresaAppaltataDaSelezionata(impresaSel);
        //}
    }

    protected void ButtonAltroImpresaAppaltata_Click(object sender, EventArgs e)
    {
        PanelRicercaNuovaImpresaAppaltante.Visible = true;
        AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = true;
        PanelInserisciImpresaAppaltante.Visible = false;
    }

    protected void ButtonSelezionaImpresaAppaltataDaDropDownList_Click(object sender, EventArgs e)
    {
        if (DropDownListImpresaAppaltante.SelectedItem != null)
        {
            //string ragioneSociale = DropDownListImpresaAppaltante.SelectedItem.Text;
            //int idImpresa;
            //TipologiaImpresa tipoImpresa;

            Impresa impresa = new Impresa();

            string[] impresaIds = DropDownListImpresaAppaltante.SelectedItem.Value.Split('|');
            impresa.TipoImpresa = (TipologiaImpresa) Int32.Parse(impresaIds[0]);
            if (!String.IsNullOrEmpty(impresaIds[2]))
            {
                impresa.IdTemporaneo = new Guid(impresaIds[2]);
            }
            if (!String.IsNullOrEmpty(impresaIds[1]))
            {
                impresa.IdImpresa = Int32.Parse(impresaIds[1]);
            }

            Impresa impresaSel;

            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                ImpresaCollection imprese = (ImpresaCollection) ViewState["impresaCollection"];
                impresaSel = imprese.Select(impresa);
            }
            else
            {
                ImpresaCollection imprese = (ImpresaCollection) ViewState["impreseSelColl"];
                impresaSel = imprese.Select(impresa);
            }

            ImpresaSubappaltatriceSelezionata(impresaSel);
        }
    }

    protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Subappalto subappalto = (Subappalto) e.Row.DataItem;
            Label lSubappaltatriceRagioneSociale = (Label) e.Row.FindControl("LabelSubappaltatriceRagioneSociale");
            Label lSubappaltatriceIndirizzo = (Label) e.Row.FindControl("LabelSubappaltatriceIndirizzo");
            Label lSubappaltatriceArtigiano = (Label) e.Row.FindControl("LabelSubappaltatriceArtigiano");

            Label lSubappaltataRagioneSociale = (Label) e.Row.FindControl("LabelSubappaltataRagioneSociale");
            Label lSubappaltataIndirizzo = (Label) e.Row.FindControl("LabelSubappaltataIndirizzo");
            Label lSubappaltataArtigiano = (Label) e.Row.FindControl("LabelSubappaltataArtigiano");

            if (subappalto.Appaltata != null)
            {
                lSubappaltatriceRagioneSociale.Text = subappalto.NomeAppaltata;
                lSubappaltatriceIndirizzo.Text = subappalto.Appaltata.IndirizzoCompleto;
                //Valla: sistemare campo, lavoratore autonomo pu� essere null...
                try
                {
                    if (subappalto.Appaltata.LavoratoreAutonomo)
                        lSubappaltatriceArtigiano.Text = "Artigiano autonomo senza dipendenti";
                    else
                        lSubappaltatriceArtigiano.Text = string.Empty;
                }
                catch
                {
                    lSubappaltatriceArtigiano.Text = string.Empty;
                }
            }

            if (subappalto.Appaltante != null)
            {
                lSubappaltataRagioneSociale.Text = subappalto.NomeAppaltatrice;
                lSubappaltataIndirizzo.Text = subappalto.Appaltante.IndirizzoCompleto;
                //Valla: sistemare campo, lavoratore autonomo pu� essere null...
                try
                {
                    if (subappalto.Appaltante.LavoratoreAutonomo)
                        lSubappaltataArtigiano.Text = "Artigiano autonomo senza dipendenti";
                    else
                        lSubappaltataArtigiano.Text = string.Empty;
                }
                catch
                {
                    lSubappaltataArtigiano.Text = string.Empty;
                }
            }
        }
    }

    protected void CustomValidatorSubappalti_ServerValidate(object source, ServerValidateEventArgs args)
    {
        SubappaltoCollection sub = (SubappaltoCollection) ViewState["subappalti"];
        Impresa appaltatrice = (Impresa) ViewState["impresaAppaltatrice"];
        Impresa appaltata = (Impresa) ViewState["impresaAppaltata"];

        if (appaltatrice != null && appaltata != null)
        {
            if (((appaltatrice.IdImpresa != null) && (appaltata.IdImpresa != null) &&
                 (appaltatrice.IdImpresa == appaltata.IdImpresa)) ||
                ((appaltatrice.IdImpresa == null) && (appaltata.IdImpresa == null) &&
                 (appaltata.PartitaIva == appaltatrice.PartitaIva)) || (sub.Contains(appaltatrice, appaltata)) ||
                sub.Contains(appaltata, appaltatrice))
            {
                AttestatoRegolaritaRicercaImpresaAppaltataDa.Reset();
                AttestatoRegolaritaRicercaImpresaSubappaltatrice.Reset();
                args.IsValid = false;
            }

            else
                args.IsValid = true;
        }

        if (appaltatrice == null && appaltata != null)
        {
            if (sub.ContainsAppaltata(appaltata))
            {
                AttestatoRegolaritaRicercaImpresaAppaltataDa.Reset();
                AttestatoRegolaritaRicercaImpresaSubappaltatrice.Reset();
                args.IsValid = false;
            }

            else
                args.IsValid = true;
        }
    }

    protected void CustomValidatorAlmenoUnImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        ImpresaCollection imprese = GetImprese();

        if (imprese.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorImpresaRichiedentePresente_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        ImpresaCollection imprese = GetImprese();

        if (GestioneUtentiBiz.IsImpresa())
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa imp = ((TBridge.Cemi.GestioneUtenti.Business.Identities.Impresa)HttpContext.Current.User.Identity).Entity;
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa imp =
                (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            Impresa impAtt = new Impresa();

            impAtt.IdImpresa = imp.IdImpresa;
            impAtt.TipoImpresa = TipologiaImpresa.SiceNew;

            if (!imprese.Contains(impAtt))
            {
                args.IsValid = false;
            }
        }
    }

    #region gestione appalti

    protected void GridViewSubappalti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

        subappalti.RemoveAt(e.RowIndex);

        ViewState["subappalti"] = subappalti;

        CaricaSubappalti();
    }

    private bool ControlloCampiServer()
    {
        //Valla: modificare e farlo con un validator custom
        bool res = true;
        StringBuilder errori = new StringBuilder();

        if (ViewState["impresaAppaltata"] == null)
        {
            res = false;
            errori.Append("Impresa appaltante non selezionata" + Environment.NewLine);
        }

        if (!res)
        {
            LabelRisultatoAppalto.Text = errori.ToString();
        }
        else
        {
            Presenter.SvuotaCampo(LabelRisultatoAppalto);
        }

        return res;
    }

    protected void ButtonAggiungiAppalto_Click(object sender, EventArgs e)
    {
        Page.Validate("appalto");

        if (Page.IsValid)
        {
            if (ViewState["subappalti"] != null && ControlloCampiServer())
            {
                SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["subappalti"];

                Impresa appaltatrice = (Impresa) ViewState["impresaAppaltatrice"];
                Impresa appaltata = (Impresa) ViewState["impresaAppaltata"];

                //Valla: passare tipologia contratto 0=SiceNEW
                Subappalto subappalto =
                    new Subappalto(null, 0, appaltatrice, appaltata,
                                   0 /*(TipologiaContratto)DropDownListTipoContratto.SelectedIndex*/);
                subappalti.Add(subappalto);
                biz.LogSelezioneImpresaSubappalto(idUtente, subappalto);

                ViewState["impresaAppaltatrice"] = null;
                ViewState["impresaAppaltata"] = null;
                TextBoxImpresaAppaltante.Text = string.Empty;
                TextBoxImpresaSubappaltata.Text = string.Empty;
                RadioButtonImpresaAppaltanteNuova.Checked = false;
                RadioButtonImpresaSubappaltataNuova.Checked = false;
                RadioButtonImpresaAppaltanteSiceInfo.Checked = false;
                RadioButtonImpresaSubappaltataSiceInfo.Checked = false;

                CaricaSubappalti();

                PanelAppalti.Visible = false;
                ButtonVisualizzaAppalto.Enabled = true;

                Reset();
            }
        }
    }

    protected void ButtonVisualizzaAppalto_Click(object sender, EventArgs e)
    {
        PanelAppalti.Visible = true;
        ButtonVisualizzaAppalto.Enabled = false;
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        LabelRisultatoAppalto.Text = string.Empty;

        ButtonVisualizzaAppalto.Enabled = true;

        Reset();

        ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

        if (impreseTot.Count == 0)
        {
            PanelImpresaAppaltanteDropDown.Visible = false;

            PanelRicercaNuovaImpresaAppaltante.Visible = true;
            AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = true;
            PanelInserisciImpresaAppaltante.Visible = false;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;
        }
        else
        {
            PanelImpresaAppaltanteDropDown.Visible = true;

            PanelRicercaNuovaImpresaAppaltante.Visible = false;
            AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = false;
            PanelInserisciImpresaAppaltante.Visible = false;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;
        }

        PanelAppalti.Visible = false;
    }

    #endregion

    #region eventi inserimento nuova impresa

    protected void ButtonInserisciNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        Impresa impresa = AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.impresa;
        ImpresaCollection impColl;

        if (ViewState["impreseSelColl"] == null)
        {
            impColl = new ImpresaCollection();
        }
        else
        {
            impColl = (ImpresaCollection) ViewState["impreseSelColl"];
        }

        if (impresa != null)
        {
            bool[] res = biz.EsisteIvaFiscImpresa(impresa.PartitaIva, impresa.CodiceFiscale);
            ControllaEsistenzaImpresa(LabelImpresaSelezionataEsistente, res);

            if (string.IsNullOrEmpty(LabelImpresaSelezionataEsistente.Text))
            {
                impColl.Add(impresa);
                ViewState["impreseSelColl"] = impColl;
                ImpresaSubappaltatriceSelezionata(impresa);
                AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.Reset();
            }
            else
            {
                string pIVA = string.Empty;
                string codfisc = string.Empty;

                if (res[0])
                {
                    pIVA = impresa.PartitaIva;
                    codfisc = string.Empty;
                    AttestatoRegolaritaRicercaImpresaSubappaltatrice.CaricaImpreseRicerca(codfisc, pIVA);
                }
                else
                {
                    if (res[2])
                    {
                        pIVA = string.Empty;
                        codfisc = impresa.CodiceFiscale;
                        AttestatoRegolaritaRicercaImpresaSubappaltatrice.CaricaImpreseRicerca(codfisc, pIVA);
                    }
                }

                ViewState["impresaAppaltatrice"] = null;

                TextBoxImpresaAppaltante.Text = string.Empty;

                RadioButtonImpresaAppaltanteNuova.Checked = false;
                RadioButtonImpresaAppaltanteSiceInfo.Checked = false;

                ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

                if (impreseTot.Count == 0)
                {
                    PanelImpresaAppaltanteDropDown.Visible = false;

                    PanelRicercaNuovaImpresaAppaltante.Visible = true;
                    AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = true;
                    PanelInserisciImpresaAppaltante.Visible = false;

                    AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.Reset();
                }
                else
                {
                    PanelImpresaAppaltanteDropDown.Visible = true;

                    PanelRicercaNuovaImpresaAppaltante.Visible = false;
                    AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = false;
                    PanelInserisciImpresaAppaltante.Visible = false;

                    AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.Reset();
                }
            }
        }
        else
            LabelNuovaAppaltanteRes.Text = AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.Errore;
    }

    protected void ButtonInserisciNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        Impresa impresa = AttestatoRegolaritaImpreseSubappaltiAppaltataDa.impresa;
        ImpresaCollection impColl;

        if (ViewState["impreseSelColl"] == null)
        {
            impColl = new ImpresaCollection();
        }
        else
        {
            impColl = (ImpresaCollection) ViewState["impreseSelColl"];
        }

        if (impresa != null)
        {
            bool[] res = biz.EsisteIvaFiscImpresa(impresa.PartitaIva, impresa.CodiceFiscale);
            ControllaEsistenzaImpresa(LabelImpresaAppaltataDaEsistente, res);

            if (string.IsNullOrEmpty(LabelImpresaAppaltataDaEsistente.Text))
            {
                impColl.Add(impresa);
                ViewState["impreseSelColl"] = impColl;
                ImpresaAppaltataDaSelezionata(impresa);
                AttestatoRegolaritaImpreseSubappaltiAppaltataDa.Reset();
            }
            else
            {
                string pIVA = string.Empty;
                string codfisc = string.Empty;

                if (res[0])
                {
                    pIVA = impresa.PartitaIva;
                    codfisc = string.Empty;
                    AttestatoRegolaritaRicercaImpresaAppaltataDa.CaricaImpreseRicerca(codfisc, pIVA);
                }
                else
                {
                    if (res[2])
                    {
                        pIVA = string.Empty;
                        codfisc = impresa.CodiceFiscale;
                        AttestatoRegolaritaRicercaImpresaAppaltataDa.CaricaImpreseRicerca(codfisc, pIVA);
                    }
                }

                ViewState["impresaAppaltata"] = null;

                TextBoxImpresaSubappaltata.Text = string.Empty;

                RadioButtonImpresaSubappaltataNuova.Checked = false;
                RadioButtonImpresaSubappaltataSiceInfo.Checked = false;

                ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

                if (impreseTot.Count == 0)
                {
                    PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                    AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
                    PanelInserisciImpresaSubappaltata.Visible = false;

                    AttestatoRegolaritaImpreseSubappaltiAppaltataDa.Reset();
                }
                else
                {
                    PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
                    AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
                    PanelInserisciImpresaSubappaltata.Visible = false;

                    AttestatoRegolaritaImpreseSubappaltiAppaltataDa.Reset();
                }
            }
        }
        else
            LabelNuovaSubappaltataDaRes.Text = AttestatoRegolaritaImpreseSubappaltiAppaltataDa.Errore;
        //"Non tutti i campi sono corretti";
    }

    protected void ButtonAnnullaNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        LabelRisultatoAppalto.Text = string.Empty;

        ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

        if (impreseTot.Count == 0)
        {
            PanelImpresaAppaltanteDropDown.Visible = false;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;
        }
        else
        {
            PanelImpresaAppaltanteDropDown.Visible = true;

            PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = true;
            AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = true;
            PanelInserisciImpresaSubappaltata.Visible = false;
        }
    }

    private void ControllaEsistenzaImpresa(Label label, bool[] res)
    {
        StringBuilder mess = new StringBuilder();

        if (res[0])
            mess.Append("Nell'anagrafica SiceNew esiste gi� un'impresa con la partita IVA inserita\n");
        if (res[2])
            mess.Append("Nell'anagrafica SiceNew esiste gi� un'impresa con il codice fiscale inserito\n");
        label.Text = mess.ToString();
    }


    protected void ButtonAnnullaNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        LabelRisultatoAppalto.Text = string.Empty;

        ImpresaCollection impreseTot = (ImpresaCollection) ViewState["impresaCollection"];

        if (impreseTot.Count == 0)
        {
            PanelImpresaAppaltanteDropDown.Visible = false;

            PanelRicercaNuovaImpresaAppaltante.Visible = true;
            AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = true;
            PanelInserisciImpresaAppaltante.Visible = false;
        }
        else
        {
            PanelImpresaAppaltanteDropDown.Visible = true;

            PanelRicercaNuovaImpresaAppaltante.Visible = false;
            AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = false;
            PanelInserisciImpresaAppaltante.Visible = false;
        }
    }

    #endregion

    #region Eventi sui controlli di ricerca inserimento

    private void AttestatoRegolaritaRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        ImpresaAppaltataDaSelezionata(impresa);
    }

    private void ImpresaAppaltataDaSelezionata(Impresa impresa)
    {
        TextBoxImpresaSubappaltata.Text = impresa.NomeCompleto;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaSubappaltataSiceInfo.Checked = true;
        else
            RadioButtonImpresaSubappaltataNuova.Checked = true;
        ViewState["impresaAppaltatrice"] = impresa;

        AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = false;

        PanelRicercaImpresaSubappaltataNuovaImpresa.Visible = false;
    }

    private void AttestatoRegolaritaRicercaImpresa2_OnImpresaSelected(Impresa impresa)
    {
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        ImpresaSubappaltatriceSelezionata(impresa);
    }

    private void ImpresaSubappaltatriceSelezionata(Impresa impresa)
    {
        TextBoxImpresaAppaltante.Text = impresa.NomeCompleto;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaAppaltanteSiceInfo.Checked = true;
        else
            RadioButtonImpresaAppaltanteNuova.Checked = true;
        ViewState["impresaAppaltata"] = impresa;

        AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaAppaltante.Visible = false;

        PanelRicercaNuovaImpresaAppaltante.Visible = false;
    }

    private void AttestatoRegolaritaRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        AttestatoRegolaritaRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaAppaltante.Visible = true;
    }

    private void AttestatoRegolaritaRicercaImpresaAppaltataDa_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        AttestatoRegolaritaRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = true;
    }

    private void Reset()
    {
        AttestatoRegolaritaImpreseSubappaltiAppaltataDa.Reset();
        AttestatoRegolaritaImpreseSubappaltiSubappaltatrice.Reset();
        AttestatoRegolaritaRicercaImpresaAppaltataDa.Reset();
        AttestatoRegolaritaRicercaImpresaSubappaltatrice.Reset();

        ViewState["impresaAppaltata"] = null;
        ViewState["impresaAppaltatrice"] = null;

        TextBoxImpresaAppaltante.Text = string.Empty;
        TextBoxImpresaSubappaltata.Text = string.Empty;

        RadioButtonImpresaAppaltanteNuova.Checked = false;
        RadioButtonImpresaSubappaltataNuova.Checked = false;
        RadioButtonImpresaAppaltanteSiceInfo.Checked = false;
        RadioButtonImpresaSubappaltataSiceInfo.Checked = false;
    }

    #endregion
}