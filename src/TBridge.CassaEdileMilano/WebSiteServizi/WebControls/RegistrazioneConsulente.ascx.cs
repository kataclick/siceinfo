using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;

public partial class WebControls_RegistrazioneConsulente : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("GestioneUtentiRegistrazione.aspx");
    }

    protected void ButtonRegistraImpresa_Click(object sender, EventArgs e)
    {
        if (((RadioButtonList) LiberatoriaPrivacy1.FindControl("RadioButtonListPrivacy")).SelectedValue == "Consento")
        {
            if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text)
                && TextBoxPassword.Text == TextBoxPasswordRidigitata.Text)
            {
                //
                //Consulente consulente = new Consulente();
                Consulente consulente = new Consulente(
                    0,
                    TextBoxLogin.Text
                    );

                int codice;
                if (Int32.TryParse(TextBoxCodice.Text, out codice))
                    consulente.IdConsulente = codice;
                else
                    consulente.IdConsulente = -1;

                consulente.PIN = TextBoxPIN.Text.ToUpper();

                //consulente.Username = TextBoxLogin.Text;
                consulente.Password = TextBoxPassword.Text;

                GestioneUtentiBiz gu = new GestioneUtentiBiz();

                //Tentiamo la registrazione di una impresa
                switch (gu.RegistraConsulente(consulente))
                {
                    case ErroriRegistrazione.RegistrazioneEffettuata:
                        //Rigenero il PIN
                        //gu.GeneraPinConsulente(consulente.IdConsulente);
                        (new UtentiManager()).ImpostaPinConsulenteByIdUtente(consulente.IdUtente);
                        int numeroMesi = Convert.ToInt32(ConfigurationManager.AppSettings["MesiValiditaPassword"]);
                        (new UtentiManager()).AggiornaScadenzaPassword(consulente.IdUtente, numeroMesi);
                        LabelResult.Text = "Registrazione completata";
                        //Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                        Context.Items["idUtente"] = consulente.IdUtente;
                        Server.Transfer("GestioneUtentiRegistrazioneAvvenuta.aspx");
                        break;
                    case ErroriRegistrazione.ChallengeNonPassato:
                        LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                        break;
                    case ErroriRegistrazione.LoginPresente:
                        LabelResult.Text =
                            "La username scelta � gi� presente nel sistema, sceglierne un'altra e riprovare.";
                        break;
                    case ErroriRegistrazione.RegistrazioneGiaPresente:
                        LabelResult.Text = "La registrazione risulta come gi� effettuata.";
                        break;
                    case ErroriRegistrazione.Errore:
                        LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                        break;
                    case ErroriRegistrazione.LoginNonCorretta:
                        LabelResult.Text =
                            "La username fornita contiene caratteri non validi o la sua lunghezza non � compresa fra 5 e 15 caratteri.";
                        break;
                    case ErroriRegistrazione.PasswordNonCorretta:
                        LabelResult.Text =
                            "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                        break;
                    default:
                        LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                        break;
                }
                //if (gu.RegistraImpresa(impresa) == ErroriRegistrazione.RegistrazioneEffettuata)
                //{
                //    this.LabelResult.Text = "Registrazione completata";
                //    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                //}
                //else
                //{
                //    this.LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare.";
                //}
            }
            else
            {
                LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
            }
        }
        else
        {
            LabelResult.Text = "Deve essere rilasciato il consenso al trattamento dei dati";
        }
    }
}