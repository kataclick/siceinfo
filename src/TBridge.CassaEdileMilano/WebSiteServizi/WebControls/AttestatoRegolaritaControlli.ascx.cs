#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Business;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;

#endregion

public partial class WebControls_AttestatoRegolaritaControlli : UserControl
{
    private readonly AttestatoRegolaritaBusiness biz = new AttestatoRegolaritaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }

    public void CaricaControlli(Int32 idDomanda)
    {
        ImpresaCollection impreseEdiliControlli = biz.CaricaImpreseEdiliControlli(idDomanda);
        ImpresaCollection impreseNonEdiliControlli = biz.CaricaImpreseNonEdiliControlli(idDomanda);
        LavoratoreCollection lavoratoriControlli = biz.CaricaLavoratoriControlli(idDomanda);
        Domanda domandaControlli = biz.CaricaDomandaControlli(idDomanda);

        if (domandaControlli.SegnalazioneCantiereInNotifiche.HasValue)
        {
            if (domandaControlli.SegnalazioneCantiereInNotifiche.Value)
                LabelMessaggioCantiere.Text = "Nessuna anomalia riscontrata";
            else
                LabelMessaggioCantiere.Text = "L'indirizzo fornito non � presente tra le notifiche";
        }

        GridViewImpreseEdili.DataSource = impreseEdiliControlli;
        GridViewImpreseEdili.DataBind();
        PanelLegendaImpreseEdili.Visible = (impreseEdiliControlli.Count != 0);

        GridViewImpreseNonEdili.DataSource = impreseNonEdiliControlli;
        GridViewImpreseNonEdili.DataBind();
        PanelLegendaImpreseNonEdili.Visible = (impreseNonEdiliControlli.Count != 0);

        GridViewLavoratori.DataSource = lavoratoriControlli;
        GridViewLavoratori.DataBind();
        PanelLegendaLavoratori.Visible = (lavoratoriControlli.Count != 0);
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore lav = (Lavoratore) e.Row.DataItem;

            if (lav.TipoContrattoImpresa != TipologiaContratto.ContrattoEdile)
            {
                e.Row.Visible = false;
            }
        }
    }
}