using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Delegates;
using System.Web.UI.HtmlControls;

public partial class WebControls_CorsiRicercaLavoratoreAnagraficaCondivisa : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    public event LavoratoreSelectedEventHandler OnLavoratoreSelected;
    public event LavoratoreNuovoEventHandler OnLavoratoreNuovo;
    public event LavoratoreRicercaEventHandler OnLavoratoreRicerca;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void InForza(Boolean inForza)
    {
        ViewState["InForza"] = inForza;
    }

    public void IdImpresa(Int32 idImpresa)
    {
        ViewState["IdImpresa"] = idImpresa;
    }

    public void LavoratoreNuovoErrore()
    {
        LabelErrore.Text = "Non � possibile inserire un nuovo lavoratore per il corso selezionato. Scegliere tra i lavoratori in forza tramite il bottone \"Ricerca\"";
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        Presenter.SvuotaCampo(LabelErrore);

        if (OnLavoratoreRicerca != null)
            OnLavoratoreRicerca();

        if (Page.IsValid)
        {
            CercaLavoratori(0);
            GridViewLavoratori.Visible = true;
        }
    }

    private void CercaLavoratori(int pagina)
    {
        LavoratoreFilter filtro = CreaFiltro();

        Presenter.CaricaElementiInGridView(
            GridViewLavoratori,
            biz.GetLavoratoriAnagraficaCondivisa(filtro),
            pagina);
    }

    private LavoratoreFilter CreaFiltro()
    {
        LavoratoreFilter filtro = new LavoratoreFilter();

        filtro.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        filtro.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        if (!String.IsNullOrEmpty(TextBoxDataNascita.Text))
        {
            filtro.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
        }
        filtro.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        filtro.InForza = ViewState["InForza"] != null && (Boolean)ViewState["InForza"];
        filtro.IdImpresa = ViewState["IdImpresa"] != null ? (Int32?)ViewState["IdImpresa"] : null;

        return filtro;
    }

    public void SetFiltro(String nome, String cognome, String codiceFiscale)
    {
        TextBoxCognome.Text = cognome;
        TextBoxNome.Text = nome;
        TextBoxCodiceFiscale.Text = codiceFiscale;
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        if (OnLavoratoreNuovo != null)
        {
            OnLavoratoreNuovo();
        }
    }

    public void ForzaRicerca(String codiceFiscale)
    {
        ResetRicerca();
        TextBoxCodiceFiscale.Text = codiceFiscale;
        GridViewLavoratori.Visible = true;
        CercaLavoratori(0);
    }

    public void ResetRicerca()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxDataNascita);

        GridViewLavoratori.Visible = false;
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricercaLavoratori");

        if (Page.IsValid)
        {
            CercaLavoratori(e.NewPageIndex);
        }
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Page.Validate("datiLavoratoreSelezione");

        if (Page.IsValid)
        {
            TipologiaLavoratore tipoLavoratore = (TipologiaLavoratore) GridViewLavoratori.DataKeys[e.NewSelectedIndex].Values["TipoLavoratore"];
            Int32 idLavoratore = (Int32) GridViewLavoratori.DataKeys[e.NewSelectedIndex].Values["IdAnagraficaCondivisa"];

            if (OnLavoratoreSelected != null)
            {
                Lavoratore lavoratore = biz.GetLavoratoreAnagraficaCondivisaByKey(idLavoratore);

                CheckBox CheckBoxPrimaEsperienza = (CheckBox) GridViewLavoratori.Rows[e.NewSelectedIndex].FindControl("CheckBoxPrimaEsperienza");
                TextBox TextBoxDataAssunzione = (TextBox) GridViewLavoratori.Rows[e.NewSelectedIndex].FindControl("TextBoxDataAssunzione");

                lavoratore.PrimaEsperienza = CheckBoxPrimaEsperienza.Checked;
                if (!String.IsNullOrWhiteSpace(TextBoxDataAssunzione.Text))
                {
                    DateTime dtAssunzione;

                    if (DateTime.TryParseExact(TextBoxDataAssunzione.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dtAssunzione))
                    {
                        lavoratore.DataAssunzione = dtAssunzione;
                    }
                }

                OnLavoratoreSelected(lavoratore);
            }
        }
    }

    public void GestioneVisualizzazioneButtonNuovoLavoratore(bool visualizza)
    {
        ButtonNuovo.Visible = visualizza;
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore lavoratore = (Lavoratore) e.Row.DataItem;
            Label lIndirizzo = (Label) e.Row.FindControl("LabelIndirizzo");
            Label lLuogoNascita = (Label) e.Row.FindControl("LabelLuogoNascita");
            CheckBox cbPrimaEsperienza = (CheckBox) e.Row.FindControl("CheckBoxPrimaEsperienza");

            RequiredFieldValidator rfvDataAssunzione = (RequiredFieldValidator) e.Row.FindControl("RequiredFieldValidatorDataAssunzione");
            CompareValidator cvDataAssunzione = (CompareValidator) e.Row.FindControl("CompareValidatorDataAssunzione");
            Button bSeleziona = (Button) e.Row.FindControl("ButtonSeleziona");
            HtmlTable tDati16Ore = (HtmlTable)e.Row.FindControl("TableDati16Ore");

            rfvDataAssunzione.ValidationGroup = String.Format("datiLavoratoreSelezione{0}", e.Row.RowIndex);
            cvDataAssunzione.ValidationGroup = String.Format("datiLavoratoreSelezione{0}", e.Row.RowIndex);
            bSeleziona.ValidationGroup = String.Format("datiLavoratoreSelezione{0}", e.Row.RowIndex);

            if (ViewState["InForza"] != null && (Boolean)ViewState["InForza"])
                tDati16Ore.Visible = false;

            cbPrimaEsperienza.Enabled = true;
            if (lavoratore.OreDenunciate > 10)
            {
                cbPrimaEsperienza.Checked = false;
                cbPrimaEsperienza.Enabled = false;
            }

            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew &&
                !GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
            {
                lLuogoNascita.Text = String.Format("{0} {1}", lavoratore.ComuneNascita, lavoratore.ProvinciaNascita);
                lIndirizzo.Text = lavoratore.IndirizzoCompleto;
            }
        }
    }
}