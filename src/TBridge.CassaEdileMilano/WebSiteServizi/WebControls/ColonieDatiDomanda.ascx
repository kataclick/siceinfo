<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieDatiDomanda.ascx.cs"
    Inherits="WebControls_ColonieDatiDomanda" %>
<%@ Register Src="ColonieRicercaLavoratoreACE.ascx" TagName="ColonieRicercaLavoratoreACE"
    TagPrefix="uc1" %>
<asp:Panel ID="PanelDati" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="standardTable">
                <tr>
                    <td colspan="3">
                        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si � vericato un errore durante l'operazione"
                            Visible="False"></asp:Label>
                        <asp:Label ID="LabelGiaPresente" runat="server" ForeColor="Red" Text="E' gi� presente una domanda associata al bambino selezionato"
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True">Vacanza</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Selezionare destinazione e turno.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        Vacanza
                    </td>
                    <td>
                        <asp:Label ID="LabelVacanza" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Destinazione
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListDestinazione" runat="server" Width="400px" AppendDataBoundItems="true"
                            AutoPostBack="True" OnSelectedIndexChanged="DropDownListDestinazione_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDestinazione" runat="server"
                            ErrorMessage="Destinazione non selezionata" ControlToValidate="DropDownListDestinazione"
                            ValidationGroup="azione">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Turno
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListTurno" runat="server" Width="400px" AppendDataBoundItems="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorTurno" runat="server" ErrorMessage="Turno non selezionato"
                            ControlToValidate="DropDownListTurno" ValidationGroup="azione">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                        <hr />
                    </td>
                </tr>
            </table>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Label ID="LabelTitoloDatiLavoratore" runat="server" Font-Bold="True">Dati lavoratore</asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="ButtonSelezionaLavoratore" runat="server" Text="Seleziona da anagrafica"
                            OnClick="ButtonSelezionaLavoratore_Click" />&nbsp;
                        <asp:Button ID="ButtonNuovoLavoratore" runat="server" Text="Nuovo" OnClick="ButtonNuovoLavoratore_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Inserire i dati del lavoratore cliccando su "Nuovo" oppure selezionandoli da anagrafica
                        precedentemente digitata ("Selezione da anagrafica").
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc1:ColonieRicercaLavoratoreACE ID="ColonieRicercaLavoratoreACE1" runat="server"
                            Visible="false"></uc1:ColonieRicercaLavoratoreACE>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="ButtonModificaDatiLavoratore" runat="server" Text="Modifica dati"
                            Visible="false" OnClick="ButtonModificaDatiLavoratore_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="PanelLavoratore" runat="server" Enabled="false">
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLavoratoreCognome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreCognome" runat="server"
                                ErrorMessage="Cognome del lavoratore mancante" ControlToValidate="TextBoxLavoratoreCognome"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nome*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLavoratoreNome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreNome" runat="server"
                                ErrorMessage="Nome del lavoratore mancante" ControlToValidate="TextBoxLavoratoreNome"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data di nascita (gg/mm/aaaa)*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLavoratoreDataNascita" runat="server" Width="400px" MaxLength="10"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreDataNascita" runat="server"
                                ErrorMessage="Data di nascita del lavoratore mancante" ControlToValidate="TextBoxLavoratoreDataNascita"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxLavoratoreDataNascita"
                                ErrorMessage="Formato della data di nascita del lavoratore non valido" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                                ValidationGroup="azione">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice fiscale*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLavoratoreCodiceFiscale" runat="server" Width="400px" MaxLength="16"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreCodiceFiscale" runat="server"
                                ErrorMessage="Codice fiscale del lavoratore mancante" ControlToValidate="TextBoxLavoratoreCodiceFiscale"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="TextBoxLavoratoreCodiceFiscale"
                                ValidationGroup="azione" ErrorMessage="Codice fiscale non valido" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidateLav">
                            *
                            </asp:CustomValidator>
                            <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="TextBoxLavoratoreCodiceFiscale"
                                ValidationGroup="azione" ErrorMessage="Codice di controllo del Codice fiscale non valido"
                                OnServerValidate="CodiceFiscaleCarattereControllo_ServerValidateLav">
                            *
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sesso*
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonLavoratoreM" runat="server" GroupName="lavoratoreSesso"
                                Checked="true" Text="M" />
                            <asp:RadioButton ID="RadioButtonLavoratoreF" runat="server" GroupName="lavoratoreSesso"
                                Text="F" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telefono*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLavoratoreTelefono" runat="server" Width="400px" MaxLength="20"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreTelefono" runat="server"
                                ControlToValidate="TextBoxLavoratoreTelefono" ErrorMessage="Telefono del lavoratore mancante"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cellulare
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLavoratoreCellulare" runat="server" Width="400px" MaxLength="20"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreCellulare" runat="server"
                                ControlToValidate="TextBoxLavoratoreCellulare" ErrorMessage="Cellulare del lavoratore mancante"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Residenza
                        </td>
                        <td colspan="2">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Indirizzo*
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxLavoratoreResidenzaIndirizzo" runat="server" Width="400px"
                                            MaxLength="60"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaIndirizz"
                                            runat="server" ErrorMessage="Indirizzo di residenza del lavoratore mancante"
                                            ControlToValidate="TextBoxLavoratoreResidenzaIndirizzo" ValidationGroup="azione">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comune*
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxLavoratoreResidenzaComune" runat="server" Width="400px" MaxLength="60"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaComune"
                                            runat="server" ErrorMessage="Comune di residenza del lavoratore mancante" ControlToValidate="TextBoxLavoratoreResidenzaComune"
                                            ValidationGroup="azione">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Provincia*
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxLavoratoreResidenzaProvincia" runat="server" Width="400px"
                                            MaxLength="2"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaProvincia"
                                            runat="server" ErrorMessage="Provincia di residenza del lavoratore mancante"
                                            ControlToValidate="TextBoxLavoratoreResidenzaProvincia" ValidationGroup="azione">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cap*
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxLavoratoreResidenzaCap" runat="server" Width="400px" MaxLength="5"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoreResidenzaCap" runat="server"
                                            ErrorMessage="Cap di residenza del lavoratore mancante" ControlToValidate="TextBoxLavoratoreResidenzaCap"
                                            ValidationGroup="azione">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorLavoratoreResidenzaCap"
                                            runat="server" ControlToValidate="TextBoxLavoratoreResidenzaCap" ErrorMessage="Formato del CAP non valido"
                                            ValidationExpression="^\d{5}$" ValidationGroup="azione">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <hr />
            &nbsp;<br />
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Font-Bold="True">Dati bambino</asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="ButtonNuovoBambino" runat="server" Text="Nuovo" OnClick="ButtonNuovoBambino_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="GridViewBambini" runat="server" AutoGenerateColumns="False" Visible="False"
                            Width="100%" DataKeyNames="IdFamiliare" OnSelectedIndexChanging="GridViewBambini_SelectedIndexChanging"
                            OnPageIndexChanging="GridViewBambini_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                                <asp:BoundField DataField="Nome" HeaderText="Nome" />
                                <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}"
                                    HtmlEncode="False">
                                    <ItemStyle Width="100px" />
                                </asp:BoundField>
                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                                    ShowSelectButton="True">
                                    <ItemStyle Width="50px" />
                                </asp:CommandField>
                            </Columns>
                            <EmptyDataTemplate>
                                Non � stato trovato nessun bambino associato al lavoratore selezionato
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="ButtonModificaDatiBambino" runat="server" Text="Modifica dati" Visible="false"
                            OnClick="ButtonModificaDatiBambino_Click" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="PanelBambino" runat="server" Enabled="false">
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxBambinoCognome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBambinoCognome" runat="server"
                                ErrorMessage="Cognome del bambino mancante" ControlToValidate="TextBoxBambinoCognome"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nome*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxBambinoNome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBambinoNome" runat="server"
                                ErrorMessage="Nome del bambino mancante" ControlToValidate="TextBoxBambinoNome"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data di nascita (gg/mm/aaaa)*
                        </td>
                        <td>
                            &nbsp;<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="TextBoxBambinoDataNascita" runat="server" Width="400px" MaxLength="10"
                                        AutoPostBack="True" OnTextChanged="TextBoxBambinoDataNascita_TextChanged1"></asp:TextBox><br />
                                    <asp:Label ID="LabelErroreDataNascita" runat="server" ForeColor="Red" Text="Et� del bambino non valida"
                                        Visible="False"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBambinoDataNascita" runat="server"
                                ErrorMessage="Data di nascita del lavoratore mancante" ControlToValidate="TextBoxBambinoDataNascita"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorBambinoDataNascita"
                                runat="server" ControlToValidate="TextBoxBambinoDataNascita" ErrorMessage="Formato della data di nascita del bambino non valido"
                                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                                ValidationGroup="azione">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice fiscale*
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxBambinoCodiceFiscale" runat="server" Width="400px" MaxLength="16"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBambinoCodiceFiscale" runat="server"
                                ErrorMessage="Codice fiscale del lavoratore mancante" ControlToValidate="TextBoxBambinoCodiceFiscale"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="TextBoxBambinoCodiceFiscale"
                                ValidationGroup="azione" ErrorMessage="Codice fiscale non valido" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate">
                            *
                            </asp:CustomValidator>
                            <asp:CustomValidator ID="CustomValidatorCodiceFiscaleCarattereControllo" runat="server"
                                ControlToValidate="TextBoxBambinoCodiceFiscale" ValidationGroup="azione" ErrorMessage="Codice di controllo del Codice fiscale non valido"
                                OnServerValidate="CodiceFiscaleCarattereControllo_ServerValidate">
                            *
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sesso*
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonBambinoM" runat="server" GroupName="bambinoSesso"
                                Checked="true" Text="M" />
                            <asp:RadioButton ID="RadioButtonBambinoF" runat="server" GroupName="bambinoSesso"
                                Text="F" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Portatore handicap
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="CheckBoxBambinoHandicap" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxBambinoHandicap_CheckedChanged" /><br />
                            Per l'indicazione di eventuali disabilit�, la sottoscritta Cassa Edile autorizza Cassa Edile di Milano a gestire i dati dell'utente.
                            <asp:Panel ID="PanelTipoDisabilita" runat="server" Enabled="false">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Tipo disabilit�:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxTipoDisabilita" runat="server" Height="80px" MaxLength="500"
                                                TextMode="MultiLine" Width="400px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Intolleranze alimentari* (se non presenti scrivere "NO")
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxBambinoIntolleranze" runat="server" Width="400px" MaxLength="255"
                                Height="50px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBambinoIntolleranzeAlimentari"
                                runat="server" ControlToValidate="TextBoxBambinoIntolleranze" ErrorMessage="Intolleranze alimentari mancante"
                                ValidationGroup="azione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table class="standardTable">
                <tr id="trTaglia" runat="server">
                    <td>
                        Taglia*
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListTaglia" runat="server" Width="400px" AppendDataBoundItems="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorTaglia" runat="server" ControlToValidate="DropDownListTaglia"
                            ErrorMessage="Selezionare una taglia" ValidationGroup="azione"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
            </table>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Font-Bold="True">Accompagnatore</asp:Label>
                        &nbsp;solo per portatori handicap
                    </td>
                    <td colspan="2" align="right">
                        <asp:Button ID="ButtonNuovoAccompagnatore" runat="server" Text="Nuovo" OnClick="ButtonNuovoAccompagnatore_Click"
                            Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="LabelErroreAccompagnatore" runat="server" ForeColor="Red" Text="Mancano alcuni dati dell'accompagnatore"
                            Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="PanelAccompagnatore" runat="server" Enabled="false">
                <table class="standardTable">
                    <tr>
                        <td style="height: 27px">
                            Cognome
                        </td>
                        <td style="height: 27px">
                            <asp:TextBox ID="TextBoxAccompagnatoreCognome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nome
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAccompagnatoreNome" runat="server" Width="400px" MaxLength="30"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data di nascita (gg/mm/aaaa)&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAccompagnatoreDataNascita" runat="server" Width="400px" MaxLength="10"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxAccompagnatoreDataNascita"
                                ErrorMessage="Formato della data di nascita dell'accompagnatore non valido" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                                ValidationGroup="azione">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sesso
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonAccompagnatoreM" runat="server" GroupName="accompagnatoreSesso"
                                Checked="true" Text="M" />
                            <asp:RadioButton ID="RadioButtonAccompagnatoreF" runat="server" GroupName="accompagnatoreSesso"
                                Text="F" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table class="standardTable">
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonAzione" runat="server" Text="Azione" OnClick="ButtonAzione_Click"
                    ValidationGroup="azione" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="azione" />
            </td>
        </tr>
    </table>
</asp:Panel>
