﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiRicercaPrestazioniDomande.ascx.cs"
    Inherits="WebControls_CorsiRicercaPrestazioniDomande" %>
<table class="standardTable">
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Cognome
                    </td>
                    <td>
                        Nome
                    </td>
                    <td>
                        Codice fiscale
                    </td>
                    <td>
                        Stato prestazione
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListTipoStatoPrestazione" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr valign="middle">
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Presenza denuncia:"></asp:Label>
                        <asp:DropDownList ID="DropDownListDenuncia" runat="server">
                            <asp:ListItem Selected="True" Value="">Tutti</asp:ListItem>
                            <asp:ListItem Value="True">Sì</asp:ListItem>
                            <asp:ListItem Value="False">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Univocità CF:"></asp:Label>
                        <asp:DropDownList ID="DropDownListCF" runat="server">
                            <asp:ListItem Selected="True" Value="">Tutti</asp:ListItem>
                            <asp:ListItem Value="True">Sì</asp:ListItem>
                            <asp:ListItem Value="False">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Presenza CP:"></asp:Label>
                        <asp:DropDownList ID="DropDownListCP" runat="server">
                            <asp:ListItem Selected="True" Value="">Tutti</asp:ListItem>
                            <asp:ListItem Value="True">Sì</asp:ListItem>
                            <asp:ListItem Value="False">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td valign="middle">
                        <asp:Label ID="Label4" runat="server" Text="Iscritto e pos.valida:" Width="60px"></asp:Label>
                        <asp:DropDownList ID="DropDownListIscritto" runat="server">
                            <asp:ListItem Selected="True" Value="">Tutti</asp:ListItem>
                            <asp:ListItem Value="True">Sì</asp:ListItem>
                            <asp:ListItem Value="False">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="" Width="100px"
                            OnClick="ButtonRicerca_Click" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="4">
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Label ID="Label5" runat="server" Text="Risultati ricerca" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                AllowPaging="True" OnRowDataBound="GridViewDomande_RowDataBound" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                DataKeyNames="IdCorsiPrestazioneDomanda">
                <Columns>
                    <asp:TemplateField HeaderText="Cod.">
                        <ItemTemplate>
                            <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="30px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Lavoratore">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelCognome" runat="server"></asp:Label>
                                            &nbsp;
                                            <asp:Label ID="LabelNome" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonRicercaLavoratore" runat="server" Text="Ricerca lavoratore"
                                            OnClick="ButtonRicercaLavoratore_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxCodiceFiscaleUnico" runat="server" Text="CF unico" Enabled="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxDenunciapresente" runat="server" Text="Denuncia presente"
                                            Enabled="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxCartaPrepagataPresente" runat="server" Text="CP presente"
                                            Enabled="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxPosizioneValida" runat="server" Text="Posizione valida"
                                            Enabled="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="LabelSalvaMessaggio" runat="server" ForeColor="Red"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Corso">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelTestoInizio" runat="server" Text="Inizio:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelDataInizio" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelTestoFine" runat="server" Text="Fine:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelDataFine" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Prestazione">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelTestoNumeroProtocolloPrestazione" runat="server" Text="Nr.Prot.:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelNumeroProtocolloPrestazione" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelTestoProtocolloPrestazione" runat="server" Text="Prot.:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelProtocolloPrestazione" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato">
                        <ItemTemplate>
                            <asp:Label ID="LabelStato" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="ButtonConferma" runat="server" OnClick="ButtonConferma_Click" Text="Conferma"
                                Width="75px" />
                            <br />
                            <asp:Button ID="ButtonRespingi" runat="server" OnClick="ButtonRespingi_Click" Text="Respingi"
                                Width="75px" />
                            <br />
                            <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla"
                                Width="75px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna domanda di prestazione trovata
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
