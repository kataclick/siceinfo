﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriRilevatori.ascx.cs"
    Inherits="WebControls_AccessoCantieriRilevatori" %>
<%@ Register Src="AccessoCantieriRilevatoriInserimento.ascx" TagName="AccessoCantieriRilevatoriInserimento"
    TagPrefix="uc7" %>
<table class="standardTable">
    <tr>
        <td>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAggiungiRilevatore" runat="server" Visible="True">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Rilevatori di presenze"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Codice Rilevatore
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Codice Fornitore
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownList1" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Button ID="ButtonSalva" runat="server" Text="Inserisci" OnClick="ButtonSalva_Click"
                            Visible="True" />
                        <asp:Label ID="LabelRes" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
