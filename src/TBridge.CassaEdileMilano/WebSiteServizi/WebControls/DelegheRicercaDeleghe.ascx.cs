using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using TBridge.Cemi.Archidoc.Type.Collections;
using TBridge.Cemi.Archidoc.Type.Entities;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Deleghe.Business;
using TBridge.Cemi.Deleghe.Type.Collections;
using TBridge.Cemi.Deleghe.Type.Delegates;
using TBridge.Cemi.Deleghe.Type.Enums;
using TBridge.Cemi.Deleghe.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using Delega = TBridge.Cemi.Deleghe.Type.Entities.Delega;

public partial class WebControls_DelegheRicercaDeleghe : UserControl
{
    private const int INDICEARCHIDOC = 10;
    private const int INDICELETTERAARCHIDOC = 11;
    private const int INDICEDATACONFERMA = 5;
    private const int INDICEELIMINA = 9;
    private const int INDICEID = 1;
    //private const int INDICEMODIFICA = 8;
    private const int INDICESINDACATO = 3;
    private const int INDICESTAMPAID = 6;
    private const int INDICESTATO = 7;
    private readonly DelegheBusiness biz = new DelegheBusiness();
    public int? IdDelega;
    private bool storico;
    public event DelegaSelectedEventHandler OnDelegaSelected;
    public event DelegaSearchedEventHandler OnDelegaSearched;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestioneCE.ToString()) ||
        //    GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheVisioneCE.ToString()))
        //{
        //    LabelMeseModStato.Visible = true;
        //    TextBoxMeseModStato.Visible = true;
        //}

        if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
            storico = true;

        if (!Page.IsPostBack)
        {
            CaricaComprensori();
            CaricaSindacati();
            CaricaStati();

            if (!storico)
            {
                Riga1Agg.Visible = false;
                Riga2Agg.Visible = false;
                GridViewDeleghe.Columns[INDICESTATO].Visible = false;
                GridViewDeleghe.Columns[INDICEDATACONFERMA].Visible = false;
                GridViewDeleghe.Columns[INDICEID].Visible = false;
            }
            else
            {
                //if (TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.IsSindacalista())
                //{
                //    GridViewDeleghe.Columns[INDICEMODIFICA].Visible = false;
                //}

                GridViewDeleghe.Columns[INDICEELIMINA].Visible = false;
            }

            if (GestioneUtentiBiz.IsSindacalista())
            {
                //Sindacalista sindacalista =
                //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista) HttpContext.Current.User.Identity).
                //        Entity;
                Sindacalista sindacalista =
                    (Sindacalista) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                GridViewDeleghe.Columns[INDICESINDACATO].Visible = false;

                DropDownListSindacato.SelectedValue = sindacalista.Sindacato.Id;
                DropDownListSindacato.Enabled = false;

                if (sindacalista.ComprensorioSindacale != null)
                {
                    DropDownListComprensorio.SelectedValue = sindacalista.ComprensorioSindacale.Id;
                    DropDownListComprensorio.Enabled = false;
                }
            }
        }
        else
        {
            CaricaDeleghe();
        }
    }

    public void ForzaRicerca()
    {
        CaricaDeleghe();
    }

    private void CaricaStati()
    {
        DropDownListStato.Items.Clear();

        DropDownListStato.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListStato.DataSource = Enum.GetNames(typeof (StatoDelega));
        DropDownListStato.DataBind();
    }

    private void CaricaComprensori()
    {
        DropDownListComprensorio.Items.Clear();

        DropDownListComprensorio.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListComprensorio.DataSource = biz.GetComprensori();
        DropDownListComprensorio.DataTextField = "Descrizione";
        DropDownListComprensorio.DataValueField = "Id";
        DropDownListComprensorio.DataBind();
    }

    private void CaricaSindacati()
    {
        DropDownListSindacato.Items.Clear();

        DropDownListSindacato.Items.Add(new ListItem(string.Empty, string.Empty));
        DropDownListSindacato.DataSource = biz.GetSindacati();
        DropDownListSindacato.DataTextField = "Descrizione";
        DropDownListSindacato.DataValueField = "Id";
        DropDownListSindacato.DataBind();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewDeleghe.PageIndex = 0;
            CaricaDeleghe();

            if (OnDelegaSearched != null)
                OnDelegaSearched();
        }
    }

    private DelegaCollection CaricaDeleghe()
    {
        LabelErrore.Text = string.Empty;
        string comprensorio;
        string cognome;
        string nome;
        DateTime? dataNascita = null;
        DateTime dataNascitaD;
        DateTime? dalMese = null;
        DateTime? alMese = null;
        DateTime dalMeseD;
        DateTime alMeseD;
        StatoDelega? stato = null;
        string sindacato;
        string operatoreTerritorio = null;
        DateTime? meseModStato = null;

        cognome = TextBoxCognome.Text.Trim();
        nome = TextBoxNome.Text.Trim();

        if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
            DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
            dataNascita = dataNascitaD;

        if (!string.IsNullOrEmpty(TextBoxMeseDal.Text) &&
            DateTime.TryParseExact(TextBoxMeseDal.Text, "MM/yyyy", null, DateTimeStyles.None, out dalMeseD))
            dalMese = dalMeseD;

        if (!string.IsNullOrEmpty(TextBoxMeseAl.Text) &&
            DateTime.TryParseExact(TextBoxMeseAl.Text, "MM/yyyy", null, DateTimeStyles.None, out alMeseD))
            alMese = alMeseD;

        comprensorio = DropDownListComprensorio.SelectedValue;
        sindacato = DropDownListSindacato.SelectedValue;

        if (!string.IsNullOrEmpty(DropDownListStato.SelectedValue))
            stato = (StatoDelega) Enum.Parse(typeof (StatoDelega), DropDownListStato.SelectedValue);

        if (!string.IsNullOrEmpty(TextBoxDelegaRecuperataDa.Text))
            operatoreTerritorio = TextBoxDelegaRecuperataDa.Text;

        DateTime meseModStatoD;
        if (!string.IsNullOrEmpty(TextBoxMeseModStato.Text) &&
            DateTime.TryParseExact(TextBoxMeseModStato.Text, "MM/yyyy", null, DateTimeStyles.None, out meseModStatoD))
            meseModStato = meseModStatoD;

        DelegheFilter filtro = new DelegheFilter
                                   {
                                       Cognome = cognome,
                                       Nome = nome,
                                       DataNascita = dataNascita,
                                       ComprensorioSindacale = comprensorio,
                                       Confermate = storico,
                                       DalMeseConferma = dalMese,
                                       AlMeseConferma = alMese,
                                       Stato = stato,
                                       Sindacato = sindacato,
                                       OperatoreTerritorio = operatoreTerritorio,
                                       MeseModificaStato = meseModStato
                                   };

        DelegaCollection listaDeleghe = biz.GetDeleghe(filtro);
        GridViewDeleghe.DataSource = listaDeleghe;
        GridViewDeleghe.DataBind();

        return listaDeleghe;
    }

    protected void GridViewDeleghe_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        var idDelega = (int) GridViewDeleghe.DataKeys[e.NewSelectedIndex].Values["IdDelega"];
        Delega delega = biz.GetDelega(idDelega);

        if (OnDelegaSelected != null)
            OnDelegaSelected(delega);
    }

    protected void GridViewDeleghe_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //var idDelega = (int) GridViewDeleghe.DataKeys[e.RowIndex].Values["IdDelega"];

        //if (biz.DeleteDelega(idDelega))
        //{
        //    CaricaDeleghe();
        //    LabelErrore.Visible = false;
        //}
        //else
        //{
        //    LabelErrore.Text = "Errore durante la cancellazione";
        //    LabelErrore.Visible = true;
        //}
    }

    protected void GridViewDeleghe_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewDeleghe.PageIndex = e.NewPageIndex;
        CaricaDeleghe();
    }

    protected void ButtonEsporta_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewDeleghe.PageIndex = 0;
            DelegaCollection deleghe = CaricaDeleghe();

            if (OnDelegaSearched != null)
                OnDelegaSearched();

            /*
            StringWriter sw = PreparaStampa(deleghe);

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Deleghe.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(sw.ToString());
            Response.End();
            */

            var gv = new GridView
            {
                ID = "gvDeleghe",
                AutoGenerateColumns = false
            };
            gv.RowDataBound += gv_RowDataBound;

            var bc0 = new BoundField
            {
                HeaderText = "",
                DataField = "IdDelega"
            };
            if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
                bc0.DataFormatString = "CONFERMATA";
            else
                bc0.DataFormatString = "DRAFT";
            var bc0bis = new BoundField
            {
                HeaderText = "Stato",
                DataField = "Stato"
            };
            var bc1 = new BoundField
            {
                HeaderText = "Sindacato",
                DataField = "Sindacato"
            };
            var bc2 = new BoundField
            {
                HeaderText = "Comprensorio",
                DataField = "ComprensorioSindacale"
            };
            var bc3 = new BoundField
            {
                HeaderText = "Cognome",
                DataField = "LavoratoreCognome"
            };
            var bc3bis = new BoundField
            {
                HeaderText = "Nome",
                DataField = "LavoratoreNome"
            };
            var bc3tris = new BoundField
            {
                HeaderText = "Cod.",
                DataField = "LavoratoreId"
            };
            var bc4 = new BoundField
            {
                HeaderText = "Data nascita",
                DataField = "DataNascitaLavoratore",
                DataFormatString = "{0:dd/MM/yyyy}",
                HtmlEncode = false
            };
            var bc4bis = new BoundField
            {
                HeaderText = "Cellulare",
                DataField = "LavoratoreCellulare"
            };
            var bc5 = new BoundField
            {
                HeaderText = "Codice impresa",
                DataField = "IdImpresaLavoratore"
            };
            var bc6 = new BoundField
            {
                HeaderText = "Impresa",
                DataField = "ImpresaLavoratore"
            };
            var bc7 = new BoundField
            {
                HeaderText = "Residenza",
                DataField = "ResidenzaLavoratore"
            };
            var bc8 = new BoundField
            {
                HeaderText = "Cantiere",
                DataField = "IndirizzoCantiere"
            };
            var bc9 = new BoundField
            {
                HeaderText = "Delega da",
                DataField = "OperatoreTerritorio"
            };
            var bc7_1 = new BoundField { HeaderText = "Indirizzo", DataField = "IndirizzoDenominazioneLavoratore" };
            var bc7_2 = new BoundField { HeaderText = "Comune", DataField = "ComuneLavoratore" };
            var bc7_3 = new BoundField { HeaderText = "CAP", DataField = "CapLavoratore" };
            var bc7_4 = new BoundField { HeaderText = "Provincia", DataField = "ProvinciaLavoratore" };

            gv.Columns.Add(bc0);
            gv.Columns.Add(bc0bis);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc3bis);
            gv.Columns.Add(bc3tris);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc4bis);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            gv.Columns.Add(bc7_1);
            gv.Columns.Add(bc7_2);
            gv.Columns.Add(bc7_3);
            gv.Columns.Add(bc7_4);
            gv.Columns.Add(bc8);
            gv.Columns.Add(bc9);

            if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
            {
                var bc10 = new BoundField
                {
                    HeaderText = "Mese conferma",
                    DataField = "dataConferma",
                    HtmlEncode = false,
                    DataFormatString = "{0:MM/yyyy}"
                };

                gv.Columns.Add(bc10);
            }

            gv.RowDataBound += gv_RowDataBound;

            gv.DataSource = deleghe;
            gv.DataBind();


            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Deleghe");
            var totalCols = gv.Rows[0].Cells.Count;
            var totalRows = gv.Rows.Count;
            var headerRow = gv.HeaderRow;
            for (var i = 1; i <= totalCols; i++)
            {
                workSheet.Cells[1, i].Value = Server.HtmlDecode(headerRow.Cells[i - 1].Text);
            }
            for (var j = 1; j <= totalRows; j++)
            {
                for (var i = 1; i <= totalCols; i++)
                {
                    GridViewRow row = gv.Rows[j - 1];
                    workSheet.Cells[j + 1, i].Value = Server.HtmlDecode(row.Cells[i - 1].Text);
                }
            }

            using (var memoryStream = new MemoryStream())
            {
                Response.ClearContent();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=deleghe.xlsx");
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
    
    private StringWriter PreparaStampa(DelegaCollection deleghe)
    {
        var gv = new GridView();
        gv.ID = "gvDeleghe";
        gv.AutoGenerateColumns = false;
        gv.RowDataBound += gv_RowDataBound;

        var bc0 = new BoundField();
        bc0.HeaderText = "";
        bc0.DataField = "IdDelega";
        if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
            bc0.DataFormatString = "CONFERMATA";
        else
            bc0.DataFormatString = "DRAFT";
        var bc0bis = new BoundField();
        bc0bis.HeaderText = "Stato";
        bc0bis.DataField = "Stato";
        var bc1 = new BoundField();
        bc1.HeaderText = "Sindacato";
        bc1.DataField = "Sindacato";
        var bc2 = new BoundField();
        bc2.HeaderText = "Comprensorio";
        bc2.DataField = "ComprensorioSindacale";
        var bc3 = new BoundField();
        bc3.HeaderText = "Cognome";
        bc3.DataField = "LavoratoreCognome";
        var bc3bis = new BoundField();
        bc3bis.HeaderText = "Nome";
        bc3bis.DataField = "LavoratoreNome";
        var bc3tris = new BoundField();
        bc3tris.HeaderText = "Cod.";
        bc3tris.DataField = "LavoratoreId";
        var bc4 = new BoundField();
        bc4.HeaderText = "Data nascita";
        bc4.DataField = "DataNascitaLavoratore";
        bc4.DataFormatString = "{0:dd/MM/yyyy}";
        bc4.HtmlEncode = false;
        var bc4bis = new BoundField();
        bc4bis.HeaderText = "Cellulare";
        bc4bis.DataField = "LavoratoreCellulare";
        var bc5 = new BoundField();
        bc5.HeaderText = "Codice impresa";
        bc5.DataField = "IdImpresaLavoratore";
        var bc6 = new BoundField();
        bc6.HeaderText = "Impresa";
        bc6.DataField = "ImpresaLavoratore";
        var bc7 = new BoundField();
        bc7.HeaderText = "Residenza";
        bc7.DataField = "ResidenzaLavoratore";
        var bc8 = new BoundField();
        bc8.HeaderText = "Cantiere";
        bc8.DataField = "IndirizzoCantiere";
        var bc9 = new BoundField();
        bc9.HeaderText = "Delega da";
        bc9.DataField = "OperatoreTerritorio";
        var bc7_1 = new BoundField {HeaderText = "Indirizzo", DataField = "IndirizzoDenominazioneLavoratore"};
        var bc7_2 = new BoundField {HeaderText = "Comune", DataField = "ComuneLavoratore"};
        var bc7_3 = new BoundField {HeaderText = "CAP", DataField = "CapLavoratore"};
        var bc7_4 = new BoundField {HeaderText = "Provincia", DataField = "ProvinciaLavoratore"};

        gv.Columns.Add(bc0);
        gv.Columns.Add(bc0bis);
        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc3bis);
        gv.Columns.Add(bc3tris);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc4bis);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc6);
        //if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
        //{
        gv.Columns.Add(bc7_1);
        gv.Columns.Add(bc7_2);
        gv.Columns.Add(bc7_3);
        gv.Columns.Add(bc7_4);
        //}
        //else
        //{
        //    gv.Columns.Add(bc7);
        //}
        gv.Columns.Add(bc8);
        gv.Columns.Add(bc9);

        if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
        {
            var bc10 = new BoundField();
            bc10.HeaderText = "Mese conferma";
            bc10.DataField = "dataConferma";
            bc10.HtmlEncode = false;
            bc10.DataFormatString = "{0:MM/yyyy}";

            gv.Columns.Add(bc10);
        }

        gv.RowDataBound +=new GridViewRowEventHandler(gv_RowDataBound);

        gv.DataSource = deleghe;
        gv.DataBind();

        var sw = new StringWriter();
        var htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw;
    }

    private void gv_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Delega delega = (Delega) e.Row.DataItem;

            if (delega.Stato != StatoDelega.Confermata)
            {
                e.Row.Cells[INDICESTAMPAID].Text = null;
            }
            else
            {
                if (Request.QueryString["modalita"] == "storico")
                {
                    // Se visualizzazione storico per le deleghe
                    // confermate nell'indirizzo ci metto quello 
                    // anagrafico
                    e.Row.Cells[11].Text = delega.AnagraficaResidenzaIndirizzo;
                    e.Row.Cells[12].Text = delega.AnagraficaResidenzaComune;
                    e.Row.Cells[13].Text = delega.AnagraficaResidenzaCap;
                    e.Row.Cells[14].Text = delega.AnagraficaResidenzaProvincia;
                }
            }
        }
    }

    protected void GridViewDeleghe_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Delega delega = (Delega) e.Row.DataItem;

            if (delega.Stato != StatoDelega.Confermata)
                e.Row.Cells[INDICEID].Text = null;

            if (string.IsNullOrEmpty(delega.IdArchidoc))
                e.Row.Cells[INDICEARCHIDOC].Text = null;

            if (string.IsNullOrEmpty(delega.NomeAllegatoLettera))
                e.Row.Cells[INDICELETTERAARCHIDOC].Text = null;

            if (string.IsNullOrEmpty(delega.NomeAllegatoBusta))
                e.Row.Cells[INDICELETTERAARCHIDOC+1].Text = null;
        }
    }

    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // Se uno solo dei campi data di ricerca � valorizzato mostro l'errore
        if (!string.IsNullOrEmpty(TextBoxMeseDal.Text) ^ !string.IsNullOrEmpty(TextBoxMeseAl.Text))
            args.IsValid = false;
    }

    protected void CustomValidator2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        // Controllo che la data di inizio ricerca sia inferiore di quella di fine
        if (!string.IsNullOrEmpty(TextBoxMeseDal.Text) && (!string.IsNullOrEmpty(TextBoxMeseAl.Text)))
        {
            DateTime dal;
            DateTime al;

            if (DateTime.TryParse(TextBoxMeseDal.Text, out dal) && DateTime.TryParse(TextBoxMeseAl.Text, out al))
                if (dal > al)
                    args.IsValid = false;
        }
    }

    protected void GridViewDeleghe_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //try
        //{
        //    String idArchidoc = (String) GridViewDeleghe.DataKeys[e.NewEditIndex].Values["IdArchidoc"];

        //     //Caricare il documento tramite il Web Service Archidoc
        //    PdfManager pdfManager = new PdfManager();
        //    IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
        //    byte[] file = servizioArchidoc.GetDocument(idArchidoc);
        //    if (file != null && file.Length > 0)
        //    {
        //        Stream archidocDocumentStream = pdfManager.GetByteStream(file);
        //        Stream archidocFirstPage = pdfManager.GetPdfPage(archidocDocumentStream, 0);
        //        byte[] bytePdf = new byte[archidocFirstPage.Length];
        //        archidocFirstPage.Read(bytePdf, 0, (int)archidocFirstPage.Length);
        //        if (file != null)
        //        {
        //            Presenter.RestituisciFileArchidoc(idArchidoc, bytePdf, Page);
        //            //RestituisciFile(idArchidoc, file);
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    using (EventLog appLog = new EventLog())
        //    {
        //        appLog.Source = "SiceInfo";
        //        appLog.WriteEntry(String.Format("Eccezione Archidoc: {0}", ex.Message));
        //    }
        //}
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        var idDelega =
            (int)
            GridViewDeleghe.DataKeys[((GridViewRow) ((ImageButton) sender).Parent.Parent).RowIndex].Values["IdDelega"];

        if (biz.DeleteDelega(idDelega))
        {
            CaricaDeleghe();
            LabelErrore.Visible = false;
        }
        else
        {
            LabelErrore.Text = "Errore durante la cancellazione";
            LabelErrore.Visible = true;
        }
    }

    protected void GridViewDeleghe_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Lettera")
        {
            //try
            //{
            //    String idArchidoc = (String)GridViewDeleghe.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["IdArchidoc"];

            //    // Caricare il documento tramite il Web Service Archidoc
            //    IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            //    AllegatoCollection allegati = servizioArchidoc.GetAllegati(idArchidoc, true);

            //    Allegato lettera = (from allegato in allegati
            //                        where allegato.NomeFile.StartsWith("Lettera")
            //                        select allegato).SingleOrDefault();

            //    if (lettera != null) Presenter.RestituisciFileArchidoc(idArchidoc, lettera.FileByteArray, Page);
            //}
            //catch (Exception ex)
            //{
            //    using (EventLog appLog = new EventLog())
            //    {
            //        appLog.Source = "SiceInfo";
            //        appLog.WriteEntry(String.Format("Eccezione Archidoc: {0}", ex.Message));
            //    }
            //}
        }
    }

    protected void GridViewDeleghe_RowCreated(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
            e.Row.ID = e.Row.RowIndex.ToString();
    }


    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String idArchidoc = (String)GridViewDeleghe.DataKeys[((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex].Values["IdArchidoc"];

            //Caricare il documento tramite il Web Service Archidoc
            PdfManager pdfManager = new PdfManager();
            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            byte[] file = servizioArchidoc.GetDocument(idArchidoc);
            if (file != null && file.Length > 0)
            {
                Stream archidocDocumentStream = pdfManager.GetByteStream(file);
                Stream archidocFirstPage = pdfManager.GetPdfPage(archidocDocumentStream, 0);
                byte[] bytePdf = new byte[archidocFirstPage.Length];
                archidocFirstPage.Read(bytePdf, 0, (int)archidocFirstPage.Length);
                if (file != null)
                {
                    Presenter.RestituisciFileArchidoc(idArchidoc, bytePdf, Page);
                    //RestituisciFile(idArchidoc, file);
                }
            }
        }
        catch (Exception ex)
        {
            using (EventLog appLog = new EventLog())
            {
                appLog.Source = "SiceInfo";
                appLog.WriteEntry(String.Format("Eccezione Archidoc: {0}", ex.Message));
            }
        }
    }

    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
      
            try
            {
                String idArchidoc = (String)GridViewDeleghe.DataKeys[((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex].Values["IdArchidoc"];

                // Caricare il documento tramite il Web Service Archidoc
                IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                AllegatoCollection allegati = servizioArchidoc.GetAllegati(idArchidoc, true);

                Allegato lettera = (from allegato in allegati
                                    where allegato.NomeFile.StartsWith("Lettera")
                                    select allegato).SingleOrDefault();

                if (lettera != null) Presenter.RestituisciFileArchidoc(Path.GetFileNameWithoutExtension(lettera.NomeFile), lettera.FileByteArray, Page);
            }
            catch (Exception ex)
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione Archidoc: {0}", ex.Message));
                }
            }
        
    }

    protected void ImageButtonBusta_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            int rowIndex = ((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex;
            String idArchidoc = (String)GridViewDeleghe.DataKeys[((GridViewRow)((ImageButton)sender).Parent.Parent).RowIndex].Values["IdArchidoc"];
            String nomefileBusta = (String)GridViewDeleghe.DataKeys[rowIndex].Values["NomeAllegatoBusta"];

            // Caricare il documento tramite il Web Service Archidoc
            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            
            //AllegatoCollection allegati = servizioArchidoc.GetAllegati(idArchidoc, true);
            //Allegato lettera = (from allegato in allegati
            //                    where allegato.Note.Equals("busta", StringComparison.InvariantCulture)
            //                    select allegato).SingleOrDefault();

            Allegato lettera = servizioArchidoc.GetAllegatoEsternoByNomeFile(idArchidoc, nomefileBusta, true);

            if (lettera != null) Presenter.RestituisciFileArchidoc(Path.GetFileNameWithoutExtension(lettera.NomeFile), lettera.FileByteArray, Page);
        }
        catch (Exception ex)
        {
            using (EventLog appLog = new EventLog())
            {
                appLog.Source = "SiceInfo";
                appLog.WriteEntry(String.Format("Eccezione Archidoc: {0}", ex.Message));
            }
        }
    }
}