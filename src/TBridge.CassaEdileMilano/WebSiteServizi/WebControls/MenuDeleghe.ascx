<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuDeleghe.ascx.cs" Inherits="WebControls_MenuDeleghe" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheGestione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheGestioneCE.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheConferma.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheSblocco.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheVisioneCE.ToString()))
    {                  
%>
<table class="menuTable" >
    <tr id="RigaTitolo" class="menuTable" runat="server" >
        <td>Deleghe</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheGestione.ToString()))
        {                  
    %>
    <tr>
        <td>
            <a id="A1" href="~/Deleghe/InserimentoDelega.aspx" runat="server">Inserimento delega</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="~/Deleghe/GestioneDelegheSindacato.aspx" runat="server">Gestione deleghe</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/Deleghe/SbloccoDeleghe.aspx" runat="server">Sblocco deleghe</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheConferma.ToString()))
        {
     %>
    <tr>
        <td>
            <a id="A4" href="~/Deleghe/ConfermaConfermaDeleghe.aspx" runat="server">Conferma mensile</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/Deleghe/EsportazioneDeleghe.aspx" runat="server">Esportazione</a>
        </td>
    </tr>    
     <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheGestione.ToString()))
        {
     %>
    <tr>
        <td>
            <a id="A6" href="~/Deleghe/GestioneDelegheSindacato.aspx?modalita=storico" runat="server">Storico</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheGestioneCE.ToString()))
        {
     %>  
    <tr>
        <td>
            <a id="A7" href="~/Deleghe/GestioneDelegheCE.aspx?modalita=storico" runat="server">Gestione deleghe</a>
        </td>
    </tr>  
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheVisioneCE.ToString()))
        {
     %>  
    <tr>
        <td>
            <a id="A11" href="~/Deleghe/VisualizzaDelegheCE.aspx?modalita=storico" runat="server">Visualizza deleghe</a>
        </td>
    </tr>  
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheSblocco.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A8" href="~/Deleghe/DelegheBloccate.aspx" runat="server">Deleghe bloccate</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheGestioneCE.ToString()))
        {
     %>
    <tr>
        <td>
            <a id="A9" href="~/Deleghe/GenerazioneLettera.aspx" runat="server">Lettere</a>
        </td>
    </tr> 
     <%
        }
            if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.DelegheEstrazioneMetadati.ToString()))
        {
     %>
    <tr>
        <td>
            <a id="A10" href="~/Deleghe/EstrazioneArchiDoc.aspx" runat="server">Estrazione per ArchiDoc</a>
        </td>
    </tr> 
     <%
        }
    %>
</table>
<%
    }
 %>
