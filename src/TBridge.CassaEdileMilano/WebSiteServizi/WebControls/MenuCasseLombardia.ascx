﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCasseLombardia.ascx.cs" Inherits="WebControls_MenuCasseLombardia" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%
    if (GestioneUtentiBiz.IsCassaEdile())
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Casse Lombardia
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsCassaEdile())
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CasseLombardia/CasseLombardia.aspx" runat="server">Accesso per le Casse Lombardia</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>