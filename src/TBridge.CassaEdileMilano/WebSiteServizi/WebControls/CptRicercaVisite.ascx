<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptRicercaVisite.ascx.cs"
    Inherits="WebControls_CptRicercaVisite" %>
<%@ Register Src="CptAllegatiAttivita.ascx" TagName="CptAllegatiAttivita" TagPrefix="uc1" %>
<style type="text/css">
    .style1
    {
        width: 73px;
    }
</style>
<asp:Panel ID="PanelRicercaVisite" runat="server" DefaultButton="ButtonVisualizza">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Data (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorData" runat="server" ControlToValidate="TextBoxData"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date" ValidationGroup="ricerca"></asp:CompareValidator>
                        </td>
                        <td>
                            Ente
                        </td>
                        <td>
                            Tipologia
                        </td>
                        <td>
                            Esito
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxData" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListEnte" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipologia" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListEsito" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Indirizzo
                        </td>
                        <td>
                            Comune
                        </td>
                        <td>
                            Area
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" ControlToValidate="DropDownListArea"
                                ValidationGroup="ricerca" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListArea" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click"
                                ValidationGroup="ricerca" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si � verificato un errore durante l'operazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Elenco visite"></asp:Label>
                <br />
                <asp:GridView ID="GridViewVisite" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" DataKeyNames="IdVisita,IdNotifica" OnPageIndexChanging="GridViewVisite_PageIndexChanging"
                    OnRowDataBound="GridViewVisite_RowDataBound" OnSelectedIndexChanging="GridViewVisite_SelectedIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data"
                            HtmlEncode="False">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Info visita">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="style1">
                                            Ente:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelEnte" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style1">
                                            Tipologia:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelTipologia" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style1">
                                            Esito:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelEsito" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style1">
                                            Grado irr.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelGradoIrregolarita" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Indirizzi">
                            <ItemTemplate>
                                <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    Width="300px">
                                    <Columns>
                                        <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzi" ItemStyle-CssClass="scrittaPiccola" />
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Allegati" Visible="false">
                            <ItemTemplate>
                                <uc1:CptAllegatiAttivita ID="CptAllegatiAttivita1" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Notifica"
                            ShowSelectButton="True">
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna visita trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
