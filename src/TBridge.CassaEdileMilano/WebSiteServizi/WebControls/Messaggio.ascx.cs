using System;
using System.Web.UI;

public partial class WebControls_Messaggio : UserControl
{
    private string _label = "";
    private string _titolo = "Messaggio";


    public string TestoMessaggio
    {
        get { return _label; }
        set
        {
            _label = value;
            Label1.Text = _label;
        }
    }

    public string Titolo
    {
        get { return _titolo; }
        set { _titolo = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}