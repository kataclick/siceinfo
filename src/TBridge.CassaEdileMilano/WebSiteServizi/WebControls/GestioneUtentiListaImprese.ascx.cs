﻿using System;
using System.Web.UI;
using ASP;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

public partial class WebControls_GestioneUtentiListaImprese : UserControl
{
    private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void RadGridUtenti_SelectedIndexChanging(object sender, EventArgs eventArgs)
    {
        int idUtente =
            (Int32)
            RadGridUtentiImprese.MasterTableView.DataKeyValues[
                Int32.Parse(RadGridUtentiImprese.SelectedIndexes[0])]["IdUtente"];
        GestioneUtentiListaRuoli1.CaricaRuoliUtenteSelezionato(idUtente);
    }


    protected void ButtonVai_Click(object sender, EventArgs e)
    {
        CaricaListaUtenti();
    }

    protected void RadGridUtenti_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        BindUtenti();
    }

    private void BindUtenti()
    {
        RadGridUtentiImprese.DataSource = gu.GetUtentiImprese();
    }

    private void CaricaListaUtenti()
    {
        GestioneUtentiListaRuoli1 = new webcontrols_gestioneutentilistaruoli_ascx();
        BindUtenti();
    }

    protected void ButtonClearFilters_Click(object sender, ImageClickEventArgs e)
    {
        RadGridUtentiImprese.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridUtentiImprese.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridUtentiImprese.MasterTableView.Rebind();
    }

    protected void RadGrid_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "ResetPwd")
        {
            string username = e.Item.Cells[2].Text;
            Response.Redirect(String.Format("~/GestioneUtenti/ResetPassword.aspx?username={0}", Server.UrlEncode(username)));
        }
    }
}