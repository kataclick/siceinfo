<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubappaltiMenu.ascx.cs" Inherits="SubappaltiMenu" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>

<%

    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SubappaltiRicerca.ToString()))
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>Funzioni</td>
    </tr>
    <tr>
        <td>    
            <a href="SubappaltiRicercaImprese.aspx">Ricerca impresa</a><br />
        </td>
    </tr>
</table>
<%
    }

    if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.SubappaltiStorico.ToString()) ||
        GestioneUtentiBiz.IsImpresa())
    {
%>

<table class="menuTable">
    <tr class="menuTable" style="BACKGROUND-IMAGE: url(images/sfondo_sotto_voci_menu_sinistra.gif); HEIGHT: 28px">
        <td>Storico</td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa())
        {
%>
    <tr>
        <td>    
            <a href="SubappaltiVisualizzaStoricoPropriaImpresa.aspx" >Chi mi ha cercato</a>
        </td>
    </tr>
    <%
        }
%>
        
    <%
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.SubappaltiStorico.ToString()))
        {
%>
    <tr>
        <td>    
            <a href="SubappaltiVisualizzaStorico.aspx" >Tutti gli storici</a>
        </td>
    </tr>
    <%
        }
%>
</table>

        <%
    }
%>