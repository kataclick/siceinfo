<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCantieriStatistiche.ascx.cs" Inherits="WebControls_MenuCantieriStatistiche" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriStatistichePerIspettore.ToString())
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriStatisticheGenerali.ToString())
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriStatistichePerIspettoreRUI.ToString())
    )
   {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td >Statistiche</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriStatistichePerIspettore.ToString())
            ||
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriStatistichePerIspettoreRUI.ToString())
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A1" href="~/Cantieri/StatistichePerIspettore.aspx" runat="server">Statistiche per ispettore</a>
        </td>
    </tr>
       <%
       }
       if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriStatisticheGenerali.ToString())
           )
        {                  
         %>
    <tr>       
        <td>
            <a id="A2" href="~/Cantieri/StatisticheGenerali.aspx" runat="server">Statistiche generali</a>
        </td>
    </tr>
     <%
       }
     %>
</table>
<%
    }
%>