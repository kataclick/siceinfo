using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Delegates;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using UtenteConsulente = TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente;

public partial class WebControls_CorsiRicercaImpresa : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    private Int32? idConsulente;

    public event ImpresaSelectedEventHandler OnImpresaSelected;
    public event ImpresaNuovaEventHandler OnImpresaNuova;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsConsulente())
        {
            //UtenteConsulente consulente =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Consulente)HttpContext.Current.User.Identity).
            //        Entity;
            UtenteConsulente consulente =
                (UtenteConsulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idConsulente = consulente.IdConsulente;
        }
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricercaImprese");

        if (Page.IsValid)
        {
            CercaImprese(e.NewPageIndex);
        }
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        TipologiaImpresa tipoImpresa = (TipologiaImpresa)GridViewLavoratori.DataKeys[e.NewSelectedIndex].Values["TipoImpresa"];
        Int32 idimpresa = (Int32) GridViewLavoratori.DataKeys[e.NewSelectedIndex].Values["IdImpresa"];

        if (OnImpresaSelected != null)
        {
            Impresa impresa = null;

            if (tipoImpresa == TipologiaImpresa.SiceNew)
            {
                impresa = biz.GetImpresaSiceNewByKey(idimpresa);
            }
            else
            {
                impresa = biz.GetImpresaCorsiByKey(idimpresa);
            }
            OnImpresaSelected(impresa);
        }
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewLavoratori.Visible = true;
            CercaImprese(0);
        }
    }

    private void CercaImprese(int pagina)
    {
        ImpresaFilter filtro = CreaFiltro();

        Presenter.CaricaElementiInGridView(
            GridViewLavoratori,
            biz.GetImpreseSiceNewEAnagrafica(filtro),
            pagina);
    }

    private ImpresaFilter CreaFiltro()
    {
        ImpresaFilter filtro = new ImpresaFilter();

        if (!String.IsNullOrEmpty(TextBoxCodice.Text))
        {
            filtro.IdImpresa = Int32.Parse(TextBoxCodice.Text);
        }
        filtro.RagioneSociale = TextBoxRagioneSociale.Text;
        filtro.IvaFisc = TextBoxIvaFisc.Text;
        filtro.IdConsulente = idConsulente;

        return filtro;
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        if (OnImpresaNuova != null)
        {
            OnImpresaNuova();
        }
    }

    public void ForzaRicerca(Int32 idImpresa)
    {
        ResetRicerca();
        TextBoxCodice.Text = idImpresa.ToString();
        GridViewLavoratori.Visible = true;
        CercaImprese(0);
    }

    public void ForzaRicerca(String partitaIva)
    {
        ResetRicerca();
        TextBoxIvaFisc.Text = partitaIva;
        GridViewLavoratori.Visible = true;
        CercaImprese(0);
    }

    public void ResetRicerca()
    {
        Presenter.SvuotaCampo(TextBoxCodice);
        Presenter.SvuotaCampo(TextBoxRagioneSociale);
        Presenter.SvuotaCampo(TextBoxIvaFisc);

        GridViewLavoratori.Visible = false;
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Impresa impresa = (Impresa) e.Row.DataItem;
            Label lFonte = (Label) e.Row.FindControl("LabelFonte");

            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                lFonte.Text = "CE";
            }
            else
            {
                lFonte.Text = "Anag.";
                e.Row.ForeColor = Color.Gray;
            }
        }
    }
}