﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiRicercaPartecipanti.ascx.cs"
    Inherits="WebControls_CorsiRicercaPartecipanti" %>
<%@ Register Src="CorsiDatiLavoratore.ascx" TagName="CorsiDatiLavoratore" TagPrefix="uc1" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function openRadWindowModifica(idLavoratore, idCorsiLavoratore, idPartecipazione) {
            var oWindow = radopen("../Corsi/ModificaLavoratore.aspx?idLavoratore=" + idLavoratore + "&idCorsiLavoratore=" + idCorsiLavoratore + "&idPartecipazione=" + idPartecipazione, null);
            oWindow.set_title("Modifica lavoratore");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(700, 460);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function GetRadWindow()
        {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.radWindow;
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function CloseRadWindow()
        {
            var oWindow = GetRadWindow();
            oWindow.argument = null;
            oWindow.close();
            return false;
        }

        function refreshGrid(arg) {
            <%= PostBackStringRefresh %>
        }

        function OnClientClose(oWnd, args) {
            //alert('prova');
            var btnID = '<%=ButtonModifica.ClientID %>';
            __doPostback(btnID, args);
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />

<asp:Panel ID="PanelRicercaPartecipanti" runat="server" DefaultButton="ButtonRicerca">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                        <td>
                            Solo attestati rilasciati
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxAttestatoRilasciato" runat="server" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            N° partecipanti trovati:
                            <asp:Label ID="LabelNumeroPartecipanti" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="cercaPartecipanti"
                                Width="100px" OnClick="ButtonRicerca_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Partecipanti trovati"></asp:Label><br />
                <asp:GridView ID="GridViewPartecipanti" runat="server" AutoGenerateColumns="False"
                    Width="100%" AllowPaging="True" OnPageIndexChanging="GridViewPartecipanti_PageIndexChanging"
                    OnRowDataBound="GridViewPartecipanti_RowDataBound" DataKeyNames="IdPartecipazione,IdPartecipazioneModulo,IdLavoratore,IdCorso"
                    OnSelectedIndexChanging="GridViewPartecipanti_SelectedIndexChanging" OnRowCommand="GridViewPartecipanti_RowCommand"
                    PageSize="5">
                    <Columns>
                        <asp:TemplateField HeaderText="Cod.">
                            <ItemTemplate>
                                <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="30px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCognome" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelNome" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="LabelPrimaEsperienza" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="LabelDataAssunzione" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Diritto ticket:
                                             <b>
                                                <asp:Label ID="LabelDiritto" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="ButtonElimina" runat="server" Width="150px" Text="Rimuovi iscrizione"
                                                OnClick="ButtonElimina_Click" />
                                            <asp:Panel ID="PanelConfermaEliminazione" runat="server" Visible="false">
                                                Verranno eliminate anche le iscrizioni agli altri moduli che compongono il corso.
                                                Confermi?
                                                <asp:Button ID="ButtonConfermaSi" runat="server" Text="Sì" Width="30px" OnClick="ButtonConfermaSi_Click" />
                                                &nbsp;<asp:Button ID="ButtonConfermaNo" runat="server" Text="No" Width="30px" OnClick="ButtonConfermaNo_Click" />
                                            </asp:Panel>
                                            <asp:Button ID="ButtonModifica" runat="server" Width="150px" Text="Modifica" CommandName="modifica" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data nascita">
                            <ItemTemplate>
                                <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
                                <br />
                                <asp:Label ID="LabelLuogoNascita" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Cod. fisc." DataField="CodiceFiscale">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxPresente" runat="server" Text="Presente" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxPrenotazioneImpresa" runat="server" Text="Pren. impresa"
                                                Enabled="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxPrenotazioneConsulente" runat="server" Text="Pren. consulente"
                                                Enabled="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxAttestato" runat="server" Text="Attestato" Enabled="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="ButtonSalva" runat="server" Text="Salva" CommandName="Select" Width="97%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="ButtonAttestato" runat="server" Text="Attestato" CommandName="attestato"
                                                Width="97%" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label ID="LabelSalvaMessaggio" runat="server" ForeColor="Red"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun partecipante trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="borderedTable">
                <asp:Panel ID="PanelModifica" runat="server" CssClass="borderedTable" Visible="false">
                    <table class="filledtable">
                        <tr>
                            <td style="height: 16px">
                                <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Modifica dati lavoratore"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <uc1:CorsiDatiLavoratore ID="CorsiDatiLavoratore1" runat="server" />
                    <asp:Button ID="ButtonModifica" runat="server" Width="150px" Text="Salva" OnClick="ButtonModifica_Click" />
                    <asp:Button ID="ButtonAnnulla" runat="server" Width="150px" Text="Annulla" OnClick="ButtonAnnulla_Click" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
