<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuNotifichePreliminari.ascx.cs" Inherits="WebControls_MenuNotifichePreliminari" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariRicercaMappa)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariRicercaMappa)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Notifiche preliminari</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariRicercaNotifiche)
            )
    {                  
    %>
    <tr>
        <td>
        <a id="A1" href="~/NotifichePreliminari/RicercaNotifiche.aspx" runat="server">Ricerca notifiche</a>
        </td>
    </tr>
    <%
    }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri))
    {                  
    %>
    <tr>
        <td>
        <a id="A2" href="~/NotifichePreliminari/RicercaCantieri.aspx" runat="server">Ricerca cantieri</a>
        </td>
    </tr>
    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariRicercaMappa)
        )
    {                  
    %>
     <tr>
        <td>
        <a id="A3" href="~/NotifichePreliminari/RicercaCantieriGeolocalizzazione.aspx" runat="server">Localizzazione cantieri</a>
        </td>
    </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariEstrazioneBrescia))
    {                  
    %>
        <tr>
            <td>
                <a id="A9" href="~/NotifichePreliminari/EstrazioneBrescia.aspx" runat="server">Estrazione Brescia</a>
            </td>
        </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.NotifichePreliminariEstrazioneAssimpredil))
    {                  
    %>
        <tr>
            <td>
                <a id="A10" href="~/NotifichePreliminari/EstrazioneAssimpredil.aspx" runat="server">Estrazione Assimpredil</a>
            </td>
        </tr>
    <%
    }
     %>
</table>
<% 
    }
    %>
