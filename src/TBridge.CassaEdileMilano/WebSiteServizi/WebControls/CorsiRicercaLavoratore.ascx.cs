using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Delegates;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Corsi.Type.Enums;

public partial class WebControls_CorsiRicercaLavoratore : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    public event LavoratoreSelectedEventHandler OnLavoratoreSelected;
    public event LavoratoreNuovoEventHandler OnLavoratoreNuovo;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CercaLavoratori(0);
            GridViewLavoratori.Visible = true;
        }
    }

    private void CercaLavoratori(int pagina)
    {
        LavoratoreFilter filtro = CreaFiltro();

        Presenter.CaricaElementiInGridView(
            GridViewLavoratori,
            biz.GetLavoratoriSiceNew(filtro),
            pagina);
    }

    private LavoratoreFilter CreaFiltro()
    {
        LavoratoreFilter filtro = new LavoratoreFilter();

        filtro.Cognome = TextBoxCognome.Text;
        filtro.Nome = TextBoxNome.Text;
        if (!String.IsNullOrEmpty(TextBoxDataNascita.Text))
        {
            filtro.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
        }
        filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

        return filtro;
    }

    public void SetFiltro(string nome, string cognome, string codiceFiscale)
    {
        TextBoxCognome.Text = cognome;
        TextBoxNome.Text = nome;
        TextBoxCodiceFiscale.Text = codiceFiscale;
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        if (OnLavoratoreNuovo != null)
        {
            OnLavoratoreNuovo();
        }
    }

    public void ForzaRicerca(String codiceFiscale)
    {
        ResetRicerca();
        TextBoxCodiceFiscale.Text = codiceFiscale;
        GridViewLavoratori.Visible = true;
        CercaLavoratori(0);
    }

    public void ResetRicerca()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxDataNascita);

        GridViewLavoratori.Visible = false;
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricercaLavoratori");

        if (Page.IsValid)
        {
            CercaLavoratori(e.NewPageIndex);
        }
    }

    protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Page.Validate("datiLavoratoreSelezione");

        if (Page.IsValid)
        {
            Int32 idLavoratore = (Int32) GridViewLavoratori.DataKeys[e.NewSelectedIndex].Values["IdLavoratore"];

            if (OnLavoratoreSelected != null)
            {
                Lavoratore lavoratore = biz.GetLavoratoreSiceNewByKey(idLavoratore);

                CheckBox CheckBoxPrimaEsperienza = (CheckBox) GridViewLavoratori.Rows[e.NewSelectedIndex].FindControl("CheckBoxPrimaEsperienza");
                TextBox TextBoxDataAssunzione = (TextBox) GridViewLavoratori.Rows[e.NewSelectedIndex].FindControl("TextBoxDataAssunzione");

                lavoratore.PrimaEsperienza = CheckBoxPrimaEsperienza.Checked;
                if (!String.IsNullOrWhiteSpace(TextBoxDataAssunzione.Text))
                {
                    DateTime dtAssunzione;

                    if (DateTime.TryParseExact(TextBoxDataAssunzione.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dtAssunzione))
                    {
                        lavoratore.DataAssunzione = dtAssunzione;
                    }
                }

                OnLavoratoreSelected(lavoratore);
            }
        }
    }

    public void GestioneVisualizzazioneButtonNuovoLavoratore(bool visualizza)
    {
        ButtonNuovo.Visible = visualizza;
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore lavoratore = (Lavoratore) e.Row.DataItem;
            Label lIndirizzo = (Label) e.Row.FindControl("LabelIndirizzo");
            Label lLuogoNascita = (Label) e.Row.FindControl("LabelLuogoNascita");
            CheckBox cbPrimaEsperienza = (CheckBox) e.Row.FindControl("CheckBoxPrimaEsperienza");

            RequiredFieldValidator rfvDataAssunzione = (RequiredFieldValidator) e.Row.FindControl("RequiredFieldValidatorDataAssunzione");
            CompareValidator cvDataAssunzione = (CompareValidator) e.Row.FindControl("CompareValidatorDataAssunzione");
            Button bSeleziona = (Button) e.Row.FindControl("ButtonSeleziona");

            rfvDataAssunzione.ValidationGroup = String.Format("datiLavoratoreSelezione{0}", e.Row.RowIndex);
            cvDataAssunzione.ValidationGroup = String.Format("datiLavoratoreSelezione{0}", e.Row.RowIndex);
            bSeleziona.ValidationGroup = String.Format("datiLavoratoreSelezione{0}", e.Row.RowIndex);

            cbPrimaEsperienza.Enabled = true;
            if (lavoratore.OreDenunciate > 10)
            {
                cbPrimaEsperienza.Checked = false;
                cbPrimaEsperienza.Enabled = false;
            }

            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew &&
                !GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
            {
                lLuogoNascita.Text = String.Format("{0} {1}", lavoratore.ComuneNascita, lavoratore.ProvinciaNascita);
                lIndirizzo.Text = lavoratore.IndirizzoCompleto;
            }
        }
    }
}