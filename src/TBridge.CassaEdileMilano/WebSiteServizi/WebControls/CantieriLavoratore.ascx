<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CantieriLavoratore.ascx.cs" Inherits="WebControls_CantieriLavoratore" %>
<asp:Panel ID="PanelDatiAggiornamento" runat="server" Width="220px" Visible="False">
<asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
<asp:TextBox ID="TextBoxIdLavoratore" runat="server" Enabled="False" ReadOnly="True"
    Width="100px"></asp:TextBox></asp:Panel>
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="LabelCognome" runat="server" Text="Cognome*"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="353px"></asp:TextBox></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxCognome"
                    ErrorMessage="Digitare un cognome"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelNome" runat="server" Text="Nome*"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="255" Width="353px"></asp:TextBox></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNome"
                    ErrorMessage="Digitare un nome"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelDataNascita" runat="server" Text="Data di nascita (gg/mm/aaaa)*"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="353px"></asp:TextBox></td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxDataNascita"
                    ErrorMessage="Digitare una data di nascita"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                    ErrorMessage="Formato data errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator></td>
        </tr>
    </table>
* campi obbligatori
