<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CantieriRicercaCommittente.ascx.cs"
    Inherits="WebControls_CantieriRicercaCommittente" %>
<asp:Panel ID="PanelRicercaCommittenti" runat="server" DefaultButton="ButtonVisualizza" Width="100%">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Committente">
                </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Ragione sociale
                        </td>
                        <td>
                            Indirizzo
                        </td>
                        <td>
                            Comune
                        </td>
                        <td>
                            Partita Iva / Codice fiscale
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIvaFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonNuovoCommittente" runat="server" Text="Nuovo " OnClick="ButtonNuovoCommittente_Click"
                                CausesValidation="False" Visible="False" />
                            <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" />
                            <asp:Button ID="ButtonChiudi" runat="server" OnClick="ButtonChiudi_Click" Text="X"
                                CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewCommittenti" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyNames="IdCommittente" OnPageIndexChanging="GridViewCommittenti_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewCommittenti_SelectedIndexChanging" OnSorting="GridViewCommittenti_Sorting"
                    Width="100%" OnRowDataBound="GridViewCommittenti_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Fonte">
                            <ItemTemplate>
                                <asp:Label ID="LabelFonte" runat="server"></asp:Label><br />
                                <asp:Label ID="LabelModificato" runat="server" Font-Size="XX-Small" ForeColor="Red"
                                    Text="MOD" Visible="False"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IdCommittente" HeaderText="Cod." Visible="False" />
                        <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" SortExpression="RagioneSociale" />
                        <asp:BoundField DataField="Indirizzo" HeaderText="Indirizzo" SortExpression="Indirizzo" />
                        <asp:BoundField DataField="Provincia" HeaderText="Provincia" SortExpression="Provincia" />
                        <asp:BoundField DataField="Comune" HeaderText="Comune" SortExpression="Comune" />
                        <asp:BoundField DataField="Cap" HeaderText="CAP" />
                        <asp:BoundField DataField="PartitaIva" HeaderText="Partita Iva" SortExpression="PartitaIva" />
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" SortExpression="CodiceFiscale" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun committente trovato
                    </EmptyDataTemplate>
                </asp:GridView>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
