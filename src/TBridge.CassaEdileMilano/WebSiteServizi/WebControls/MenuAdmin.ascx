<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuAdmin.ascx.cs" Inherits="WebControls_MenuAdmin" %>
<%
    if (
        TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestisciUtenti.ToString())
        ||
        (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestionePIN.ToString()))
        ||
        (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori.ToString()))
        ||
        TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IISLogStatistiche.ToString())
        ||
        TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SMSInfoGestisciSMSScadenza.ToString())
        ||
        TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti.ToString())
        )
    {%>

<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td >
            Sezione Admin</td>
    </tr>
    <%
    if (
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestisciUtenti.ToString())
            ||
            (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestionePIN.ToString()))
            ||
            (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori.ToString()))
            ||
            (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti.ToString()))
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A1" href="~/GestioneUtentiDefault.aspx" runat="server">Gestione utenti</a>
            <!--<asp:Button ID="Prova" runat="server" />-->
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IISLogStatistiche.ToString()))
        { %>
    <tr>
        <td>
            <!--<a href="ActivityTracking.aspx">Activity Tracking</a>-->
            <a id="A2" href="~/IISLogDefault.aspx" runat="server">Statistiche IIS</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.SMSInfoGestisciSMSScadenza.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/SmsDefault.aspx" runat="server">Sms Info</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
