<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegistrazioneFornitore.ascx.cs"
    Inherits="WebControls_RegistrazioneFornitore" %>
Tutti i campi sono obbligatori<br />
<table width="600pt">
    <tbody>
        <tr>
            <td align="right">
                <asp:Label ID="LabelRagioneSociale" Font-Bold="True" Text="Regione sociale:" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxRagioneSociale"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelCodice" Font-Bold="True" Text="Codice:" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxCodice" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxCodice"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodice"
                    ErrorMessage="Massimo 6 caratteri" ValidationExpression=".{1,6}"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelEmail" Font-Bold="True" Text="Email:" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxEmail"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxEmail"
                    ErrorMessage="Formato email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelUsername" Font-Bold="True" Text="Username:" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxLogin" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxLogin"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelPassword" Font-Bold="True" Text="Password:" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxPassword"
                    ErrorMessage="*">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="LabelPasswordRidigitata" Font-Bold="True" Text="Ridigita password:"
                    runat="server">
                </asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxPasswordRidigitata" runat="server" TextMode="Password">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxPasswordRidigitata"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxPasswordRidigitata"
                    ErrorMessage="Password diversa" ControlToCompare="TextBoxPassword"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
                dal username e deve contenere almeno una lettera e un numero.<br />
                Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
                pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
                ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
                dal sistema.
            </td>
        </tr>
        <tr>
            <td colspan="3" height="5">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Button ID="ButtonRegistraFornitore" OnClick="ButtonRegistraFornitore_Click"
                    Text="Registra fornitore" runat="server"></asp:Button>
                <asp:Button ID="ButtonIndietro" OnClick="ButtonIndietro_Click" Text="Indietro" runat="server"
                    CausesValidation="False"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Label ID="LabelResult" runat="server" ForeColor="Red">
                </asp:Label>
            </td>
        </tr>
    </tbody>
</table>
