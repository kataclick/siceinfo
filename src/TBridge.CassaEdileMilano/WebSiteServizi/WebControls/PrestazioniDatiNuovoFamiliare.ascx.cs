using System;
using System.Web.UI;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Type.Entities;

public partial class WebControls_PrestazioniDatiNuovoFamiliare : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public Familiare GetFamiliare()
    {
        Familiare familiare = null;

        Page.Validate("datiFamiliare");
        if (Page.IsValid)
        {
            familiare = new Familiare();

            familiare.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
            familiare.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
            familiare.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
        }

        return familiare;
    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxDataNascita);
    }
}