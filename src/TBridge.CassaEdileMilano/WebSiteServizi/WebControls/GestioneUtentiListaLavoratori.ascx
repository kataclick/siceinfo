﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GestioneUtentiListaLavoratori.ascx.cs"
    Inherits="WebControls_GestioneUtentiListaLavoratori" %>
<%@ Register Src="GestioneUtentiListaRuoli.ascx" TagName="GestioneUtentiListaRuoli"
    TagPrefix="uc1" %>
<div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadGrid Width="100%" ID="RadGridLavoratori" runat="server" AllowFilteringByColumn="true"
            OnSelectedIndexChanged="RadGridUtenti_SelectedIndexChanging" EnableLinqExpressions="false"
            OnNeedDataSource="RadGridUtenti_NeedDataSource" SortingSettings-SortToolTip=""
            AllowSorting="true" OnItemCommand="RadGrid_ItemCommand">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="true" AllowFilteringByColumn="true"
                DataKeyNames="IdUtente">
                <Columns>
                    <telerik:GridBoundColumn DataField="Username" HeaderText="Username" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idUtente" HeaderText="IdUtente" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" FilterControlWidth="40px">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="idLavoratore" HeaderText="Codice lavoratore"
                        ShowFilterIcon="false" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true"
                        FilterControlWidth="50px">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="cognome" HeaderText="Cognome" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="nome" HeaderText="Nome" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="codiceFiscale" HeaderText="Codice Fiscale" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridTemplateColumn>
                        <FilterTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="~/images/pallinoX.png"
                                AlternateText="Cancella filtro" ToolTip="Cancella filtro" OnClick="ButtonClearFilters_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="PushButton" Text="Visualizza" CommandName="Select">
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn ButtonType="PushButton" Text="Reset PWD" CommandName="ResetPwd" />
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
        &nbsp;<br />
        &nbsp;<br />
        <uc1:GestioneUtentiListaRuoli ID="GestioneUtentiListaRuoli1" runat="server" />
    </telerik:RadAjaxPanel>
</div>
