<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCorsiEsem.ascx.cs" Inherits="WebControls_MenuCorsiEsem" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneImpresa.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneLavoratore.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneConsulente.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiGestioneDomandePrestazione.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiPrestazioniDTA.ToString())
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Iscrizione Corsi
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore())
            )
        {
    %>
        <tr>
            <td>
                <a id="A6" href="~/Corsi/CorsiEsem.aspx" runat="server">Iscrizioni effettuate</a>
            </td>
        </tr>
    <%
        }
    %>
        <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneLavoratore.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore())
            )
        {
    %>
        <tr>
            <td>
                <a id="A1" href="~/Corsi/IscrizioneCorsiLavoratoreEsem.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneImpresa.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
            )
        {
    %>
        <tr>
            <td>
                <a id="A9" href="~/Corsi/IscrizioneCorsiImpresaEsem.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiIscrizioneConsulente.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            )
        {
    %>
        <tr>
            <td>
                <a id="A11" href="~/Corsi/IscrizioneCorsiConsulenteEsem.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
            )
        {
    %>
        <tr>
            <td>
                <a id="A7" href="~/Corsi/CorsiEsem.aspx" runat="server">Corsi dei lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiVisualizzazione.ToString())
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            )
        {
    %>
        <tr>
            <td>
                <a id="A12" href="~/Corsi/ConsulenteCorsiEsem.aspx" runat="server">Corsi dei lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiGestioneDomandePrestazione.ToString())
            )
        {
    %>
        <tr>
            <td>
                <a id="A14" href="~/Corsi/GestioneCorsiPrestazioniDomande.aspx" runat="server">Gestione prestazioni</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CorsiPrestazioniDTA.ToString())
            )
        {
    %>
        <tr>
            <td>
                <a id="A15" href="~/Corsi/PrestazioniDTA.aspx" runat="server">Prestazioni DTA</a>
            </td>
        </tr>
    <%
        }
    %>
    
</table>
<%
    }
%>
