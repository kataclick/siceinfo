using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Delegates;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
//using TBridge.Cemi.GestioneUtenti.Business.Identities;

public partial class WebControls_CorsiRicercaPrestazioniDomande : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    public event RicercaLavoratoreSelectedEventHandler OnRicercaLavoratoreSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaStati();
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        CorsiBusiness biz = new CorsiBusiness();

        CaricaPrestazioneDomandeCorsi(0);
    }

    private void CaricaStati()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipoStatoPrestazione,
                                                           biz.GetStatiDomanda(), "Descrizione", "IdStato");
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PrestazioneDomandaCorso domanda = (PrestazioneDomandaCorso) e.Row.DataItem;

            CheckBox cbCFUnico = (CheckBox) e.Row.FindControl("CheckBoxCodiceFiscaleUnico");
            CheckBox cbDenunciaPresente = (CheckBox) e.Row.FindControl("CheckBoxDenunciapresente");
            CheckBox cbCPPresente = (CheckBox) e.Row.FindControl("CheckBoxCartaPrepagataPresente");
            CheckBox cbPosizioneValida = (CheckBox)e.Row.FindControl("CheckBoxPosizioneValida");

            Label lCodice = (Label) e.Row.FindControl("LabelCodice");
            Label lCognome = (Label) e.Row.FindControl("LabelCognome");
            Label lNome = (Label) e.Row.FindControl("LabelNome");
            Label lCF = (Label) e.Row.FindControl("LabelCodiceFiscale");
            Label lDataNascita = (Label) e.Row.FindControl("LabelDataNascita");
            Label lInizioCorso = (Label) e.Row.FindControl("LabelDataInizio");
            Label lFineCorso = (Label) e.Row.FindControl("LabelDataFine");
            Label lStato = (Label) e.Row.FindControl("LabelStato");
            Label lNumero = (Label) e.Row.FindControl("LabelNumeroProtocolloPrestazione");
            Label lProtocollo = (Label) e.Row.FindControl("LabelProtocolloPrestazione");

            Button bConferma = (Button) e.Row.FindControl("ButtonConferma");
            bConferma.CommandArgument = e.Row.RowIndex.ToString();
            Button bRespingi = (Button) e.Row.FindControl("ButtonRespingi");
            bRespingi.CommandArgument = e.Row.RowIndex.ToString();
            Button bAnnulla = (Button) e.Row.FindControl("ButtonAnnulla");
            bAnnulla.CommandArgument = e.Row.RowIndex.ToString();
            Button bRicerca = (Button) e.Row.FindControl("ButtonRicercaLavoratore");
            bRicerca.CommandArgument = e.Row.RowIndex.ToString();

            //if (domanda.IdLavoratore.HasValue)
            //    bRicerca.Visible = false;

            #region Gestione stato abilizazione dei pulsanti azione

            //Se il lavoratore NON � iscritto in CE la prestazione pu� essere annullata
            if (domanda.IdLavoratore.HasValue)
            {
                bAnnulla.Enabled = false;

                //Se la domanda � in stato definitivo
                if (domanda.Stato.IdStato == "C" || domanda.Stato.IdStato == "D" ||
                    domanda.Stato.IdStato == "L" || domanda.Stato.IdStato == "R" ||
                    domanda.Stato.IdStato == "A")
                {
                    bRespingi.Enabled = false;
                    bConferma.Enabled = false;
                    bRicerca.Visible = false;
                }
            }
            else
            {
                if (domanda.Stato.IdStato == "A")
                {
                    bRespingi.Enabled = false;
                    bConferma.Enabled = false;
                    bAnnulla.Enabled = false;
                    bRicerca.Visible = false;
                }
                else
                {
                    bRespingi.Enabled = false;
                    bConferma.Enabled = false;
                    bAnnulla.Enabled = true;
                    bRicerca.Visible = true;
                }
            }

            #endregion

            cbCFUnico.Checked = domanda.CodiceFiscaleUnico;
            cbDenunciaPresente.Checked = domanda.DenunciaPresente;
            cbCPPresente.Checked = domanda.CartaPrepagataPresente;
            cbPosizioneValida.Checked = domanda.PosizioneValida;

            lCognome.Text = domanda.Cognome;
            lNome.Text = domanda.Nome;
            lCF.Text = domanda.CodiceFiscale;
            lCodice.Text = domanda.IdLavoratore.ToString();
            lDataNascita.Text = domanda.DataNascita.Value.ToString("dd/MM/yyyy");

            lInizioCorso.Text = domanda.InizioCorso.Value.ToString("dd/MM/yyyy");
            lFineCorso.Text = domanda.FineCorso.Value.ToString("dd/MM/yyyy");
            lStato.Text = domanda.Stato.Descrizione;

            if (domanda.NumeroProtocolloPrestazione.HasValue)
                lNumero.Text = domanda.NumeroProtocolloPrestazione.ToString();
            else
                lNumero.Text = "-";

            if (domanda.ProtocolloPrestazione.HasValue)
                lProtocollo.Text = domanda.ProtocolloPrestazione.ToString();
            else
                lProtocollo.Text = "-";
        }
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaPrestazioneDomandeCorsi(e.NewPageIndex);
    }

    private void CaricaPrestazioneDomandeCorsi(Int32 pageIndex)
    {
        ViewState["pageIndex"] = pageIndex;

        PrestazioneDomandaCorsoFilter filtro = GetFiltro();
        PrestazioniDomandeCorsiCollection domande = biz.GetPrestazioniDomandeCorsi(filtro);

        Presenter.CaricaElementiInGridView(
            GridViewDomande,
            domande,
            pageIndex);
    }

    public void Refresh()
    {
        int pageIndex = (Int32) ViewState["pageIndex"];
        CaricaPrestazioneDomandeCorsi(pageIndex);
    }

    public PrestazioneDomandaCorsoFilter GetFiltro()
    {
        PrestazioneDomandaCorsoFilter filtro = new PrestazioneDomandaCorsoFilter();

        if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
            filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;
        if (!string.IsNullOrEmpty(TextBoxNome.Text))
            filtro.Nome = TextBoxNome.Text;
        if (!string.IsNullOrEmpty(TextBoxCognome.Text))
            filtro.Cognome = TextBoxCognome.Text;
        if (!string.IsNullOrEmpty(DropDownListTipoStatoPrestazione.SelectedValue))
            filtro.IdTipoStatoPrestazione = DropDownListTipoStatoPrestazione.SelectedValue;

        if (DropDownListCF.SelectedValue != "")
        {
            filtro.CodiceFiscalePresente = Boolean.Parse(DropDownListCF.SelectedValue);
        }
        if (DropDownListCP.SelectedValue != "")
        {
            filtro.CarpaPrepagataPresente = Boolean.Parse(DropDownListCP.SelectedValue);
        }
        if (DropDownListDenuncia.SelectedValue != "")
        {
            filtro.DenunciaPresente = Boolean.Parse(DropDownListDenuncia.SelectedValue);
        }
        if (DropDownListIscritto.SelectedValue != "")
        {
            filtro.Iscritto = Boolean.Parse(DropDownListIscritto.SelectedValue);
        }

        return filtro;
    }

    #region Eventi bottoni azioni grid view

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        //UtenteAbilitato utente = ((UtenteAbilitato)HttpContext.Current.User.Identity);
        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

        Button bConferma = (Button) sender;
        Int32 indiceRiga = ((GridViewRow) bConferma.Parent.Parent).RowIndex;

        Int32 idCorsiPrestazioneDomanda =
            (Int32) GridViewDomande.DataKeys[indiceRiga].Values["IdCorsiPrestazioneDomanda"];

        biz.AccogliPrestazioneDomandaCorso(idCorsiPrestazioneDomanda, idUtente);
        CaricaPrestazioneDomandeCorsi(0);
    }

    protected void ButtonRespingi_Click(object sender, EventArgs e)
    {
        //UtenteAbilitato utente = ((UtenteAbilitato)HttpContext.Current.User.Identity);
        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

        Button bConferma = (Button) sender;
        Int32 indiceRiga = ((GridViewRow) bConferma.Parent.Parent).RowIndex;

        Int32 idCorsiPrestazioneDomanda =
            (Int32) GridViewDomande.DataKeys[indiceRiga].Values["IdCorsiPrestazioneDomanda"];

        biz.RespingiPrestazioneDomandaCorso(idCorsiPrestazioneDomanda, idUtente);
        CaricaPrestazioneDomandeCorsi(0);
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        //UtenteAbilitato utente = ((UtenteAbilitato)HttpContext.Current.User.Identity);
        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

        Button bConferma = (Button) sender;
        Int32 indiceRiga = ((GridViewRow) bConferma.Parent.Parent).RowIndex;

        Int32 idCorsiPrestazioneDomanda =
            (Int32) GridViewDomande.DataKeys[indiceRiga].Values["IdCorsiPrestazioneDomanda"];

        biz.AnnullaPrestazioneDomandaCorso(idCorsiPrestazioneDomanda, idUtente);
        CaricaPrestazioneDomandeCorsi(0);
    }

    protected void ButtonRicercaLavoratore_Click(object sender, EventArgs e)
    {
        Button bConferma = (Button) sender;
        Int32 indiceRiga = ((GridViewRow) bConferma.Parent.Parent).RowIndex;

        Int32 idCorsiPrestazioneDomanda =
            (Int32) GridViewDomande.DataKeys[indiceRiga].Values["IdCorsiPrestazioneDomanda"];

        if (OnRicercaLavoratoreSelected != null)
            OnRicercaLavoratoreSelected(idCorsiPrestazioneDomanda);
    }

    #endregion
}