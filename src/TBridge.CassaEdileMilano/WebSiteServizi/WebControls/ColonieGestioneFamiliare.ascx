<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieGestioneFamiliare.ascx.cs" Inherits="WebControls_ColonieGestioneFamiliare" %>
<%@ Register Src="ColonieVisualizzaFamiliareACE.ascx" TagName="ColonieVisualizzaFamiliareACE"
    TagPrefix="uc1" %>
<%@ Register Src="ColonieModificaFamiliareACE.ascx" TagName="ColonieModificaFamiliareACE"
    TagPrefix="uc2" %>
<asp:GridView ID="GridViewBambini" runat="server" AutoGenerateColumns="False" DataKeyNames="IdFamiliare"
    OnSelectedIndexChanging="GridViewBambini_SelectedIndexChanging" Visible="False"
    width="100%">
    <Columns>
        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
        <asp:BoundField DataField="Nome" HeaderText="Nome" />
        <asp:BoundField DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data di nascita">
            <ItemStyle Width="100px" />
        </asp:BoundField>
        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona" ShowSelectButton="True">
            <ItemStyle Width="50px" />
        </asp:CommandField>
    </Columns>
    <EmptyDataTemplate>
        Non � stato trovato nessun bambino associato al lavoratore selezionato
    </EmptyDataTemplate>
</asp:GridView>
<asp:Button ID="ButtonNuovoFamiliare" runat="server" OnClick="ButtonNuovoFamiliare_Click"
    Text="Nuovo familiare" Width="180px" /><br />
<br />
<uc1:ColonieVisualizzaFamiliareACE ID="ColonieVisualizzaFamiliareACE1" runat="server"
    Visible="false" />
<br />
<asp:Button ID="ButtonConferma" runat="server" OnClick="ButtonConferma_Click" Text="Button" /><asp:Button
    ID="ButtonModifica" runat="server" OnClick="ButtonModifica_Click" Text="Button" /><br />
<uc2:ColonieModificaFamiliareACE ID="ColonieModificaFamiliareACE1" runat="server"
    Visible="false" />
