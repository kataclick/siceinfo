﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiDatiPianificazione.ascx.cs"
    Inherits="WebControls_CorsiDatiPianificazione" %>
<asp:GridView ID="GridViewPianificazione" runat="server" AutoGenerateColumns="False"
    Width="100%" DataKeyNames="IdModulo,IdProgrammazioneModulo,IdProgrammazione"
    OnDataBinding="GridViewPianificazione_DataBinding" OnRowDataBound="GridViewPianificazione_RowDataBound">
    <Columns>
        <asp:TemplateField HeaderText="Progr.">
            <ItemTemplate>
                <asp:Label ID="LabelProgressivo" runat="server" Text="Label"></asp:Label>
            </ItemTemplate>
            <ItemStyle Width="50px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Modulo">
            <ItemTemplate>
                <asp:Label ID="LabelModulo" runat="server" Text="Label"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Prenotabile">
            <ItemTemplate>
                <asp:CheckBox ID="CheckBoxPrenotabile" runat="server" AutoPostBack="true" OnCheckedChanged="CheckBoxPrenotabile_CheckedChanged" />
            </ItemTemplate>
            <ItemStyle Width="50px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Giorno (gg/mm/aaaa)">
            <ItemTemplate>
                <asp:TextBox ID="TextBoxGiorno" runat="server" Width="100%" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorGiorno" runat="server" ErrorMessage="Indicare un giorno"
                    ValidationGroup="salvaPianificazione" ControlToValidate="TextBoxGiorno">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidatorGiorno" runat="server" ControlToValidate="TextBoxGiorno"
                    ErrorMessage="Formato data non valido" ValidationGroup="salvaPianificazione"
                    Type="Date" Operator="DataTypeCheck">*</asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidatorGiornoFuturo" runat="server" ControlToValidate="TextBoxGiorno"
                    ErrorMessage="Il giorno di pianificazione deve essere nel futuro" Operator="GreaterThan"
                    ValidationGroup="salvaPianificazione" Type="Date">*</asp:CompareValidator>
            </ItemTemplate>
            <ItemStyle Width="100px" />
        </asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <table class="standardTable">
                    <tr>
                        <td>
                            Inizio (hh:mm):
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxOra" runat="server" Width="50px" MaxLength="5"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorOra" runat="server" ErrorMessage="Indicare un'ora di inizio"
                                ValidationGroup="salvaPianificazione" ControlToValidate="TextBoxOra">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorOra" runat="server" ErrorMessage="Formato ora inizio non valido"
                                OnServerValidate="CustomValidatorOra_ServerValidate" ValidationGroup="salvaPianificazione"
                                ControlToValidate="TextBoxOra">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fine (hh:mm):
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxOraFine" runat="server" Width="50px" MaxLength="5"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorOraFine" runat="server" ErrorMessage="Indicare un'ora di fine"
                                ValidationGroup="salvaPianificazione" ControlToValidate="TextBoxOraFine">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorOraFine" runat="server" ErrorMessage="Formato ora fine non valido"
                                OnServerValidate="CustomValidatorOraFine_ServerValidate" ValidationGroup="salvaPianificazione"
                                ControlToValidate="TextBoxOraFine">*</asp:CustomValidator>
                            <asp:CustomValidator ID="CustomValidatorOraInizioMinoreOraFine" runat="server" ErrorMessage="L'ora di fine deve essere maggiore dell'ora di inizio"
                                OnServerValidate="CustomValidatorOraInizioMinoreOraFine_ServerValidate" ValidationGroup="salvaPianificazione"
                                ControlToValidate="TextBoxOraFine">*</asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <ItemStyle Width="180px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Locazione">
            <ItemTemplate>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownListLocazione" runat="server" AppendDataBoundItems="true"
                                Width="95%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLocazione" runat="server" ErrorMessage="Selezionare una locazione"
                                ValidationGroup="salvaPianificazione" ControlToValidate="DropDownListLocazione">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Max. partecipanti:
                            <asp:TextBox ID="TextBoxMaxPartecipanti" runat="server" MaxLength="3" Width="100px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMaxPartecipanti" runat="server"
                                ErrorMessage="Indicare il numero massimo di partecipanti" ValidationGroup="salvaPianificazione"
                                ControlToValidate="TextBoxMaxPartecipanti">*</asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidatorMaxPartecipanti" runat="server" Type="Integer"
                                ControlToValidate="TextBoxMaxPartecipanti" ErrorMessage="Formato del numero massimo partecipanti non valido"
                                MinimumValue="0" MaximumValue="999" ValidationGroup="salvaPianificazione">*</asp:RangeValidator>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <ItemStyle Width="300px" />
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="salvaPianificazione"
    CssClass="messaggiErrore" />
