<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LiberatoriaPrivacy.ascx.cs"
    Inherits="WebControls_LiberatoriaPrivacy" %>
<table class="standardTable">
    <tr>
        <td colspan="2">
            CONSENSO ALL'ATTO DI REGISTRAZIONE DELL'UTENTE
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <div align="justify">
                Dopo aver letto l�informativa di cui alla sezione imprese/lavoratori del presente
                sito internet e la descrizione dei programmi in esso contenuti, prendo atto che
                il trattamento dei dati personali ed aziendali, di cui al D. Lgs. 196/2003, avverr�
                in piena conformit� all�informativa fornita ed alla normativa in vigore in materia
                di tutela dei dati personali. Acconsento, quindi, al trattamento dei dati per le
                finalit� di cui al punto 1) della gi� citata informativa, ivi incluso il tracciamento
                dei dati.</div>
            <br />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <center>
                <asp:RadioButtonList ID="RadioButtonListPrivacy" runat="server" RepeatDirection="Horizontal"
                    Width="300px">
                    <asp:ListItem>Consento</asp:ListItem>
                    <asp:ListItem Selected="True">Non Consento</asp:ListItem>
                </asp:RadioButtonList>
                &nbsp;</center>
        </td>
    </tr>
</table>
