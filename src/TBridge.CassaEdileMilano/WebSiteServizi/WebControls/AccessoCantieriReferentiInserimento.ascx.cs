using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;

public partial class WebControls_AccessoCantieriReferentiInserimento : UserControl
{
    public Referente Referente
    {
        get
        {
            Page.Validate("referente");
            Referente referente = null;

            if (Page.IsValid)
            {
                referente = CreaReferente();
            }

            return referente;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaRuoli();
            CaricaAffiliazioni();
        }
    }

    private void CaricaRuoli()
    {
        Presenter.CaricaElementiInDropDown(
            DropDownList1,
            Enum.GetNames(typeof (TipologiaRuoloReferente)),
            "",
            "");
    }

    private void CaricaAffiliazioni()
    {
        Presenter.CaricaElementiInDropDown(
            DropDownList2,
            Enum.GetNames(typeof (TipologiaAffiliazione)),
            "",
            "");
    }

    private Referente CreaReferente()
    {
        if (RadDateDataNascita.SelectedDate != null)
        {
            Referente referente = new Referente
                                      {
                                          Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text),
                                          Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text),
                                          DataNascita = RadDateDataNascita.SelectedDate.Value,
                                          CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text),
                                          Telefono = Presenter.NormalizzaCampoTesto(TextBoxTelefono.Text),
                                          Email = Presenter.NormalizzaCampoTesto(TextBoxEmail.Text),
                                          TipoRuolo =
                                              (TipologiaRuoloReferente)
                                              Enum.Parse(typeof (TipologiaRuoloReferente), DropDownList1.SelectedValue),
                                          TipoAffiliazione =
                                              (TipologiaAffiliazione)
                                              Enum.Parse(typeof (TipologiaAffiliazione), DropDownList2.SelectedValue)
                                      };

            if (RadioButtonTelefono.Checked)
                referente.ModalitaContatto = true;
            else referente.ModalitaContatto = false;

            return referente;
        }
        return null;
    }

    public void Reset()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxTelefono);
        Presenter.SvuotaCampo(TextBoxEmail);
        RadDateDataNascita.Clear();
        CaricaRuoli();
        CaricaAffiliazioni();
    }

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if ((RadDateDataNascita.SelectedDate.HasValue) && (TextBoxCodiceFiscale.Text.Length == 16))
        {
            DateTime dataNascita = RadDateDataNascita.SelectedDate.Value;
            if (!string.IsNullOrEmpty(TextBoxNome.Text) && !string.IsNullOrEmpty(TextBoxCognome.Text))
            {
                string maschio = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                           TextBoxCognome.Text, "M",
                                                                                           dataNascita);
                string femmina = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                           TextBoxCognome.Text, "F",
                                                                                           dataNascita);

                if (maschio.Substring(0, 6) != TextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper() &&
                    femmina.Substring(0, 6) != TextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper())
                {
                    args.IsValid = false;
                }
            }


            String codiceMaschile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "M", dataNascita, "A000");
            String codiceFemminile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "F", dataNascita, "A000");

            if (codiceMaschile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                &&
                codiceFemminile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                )
            {
                args.IsValid = false;
            }
        }
    }
}