﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuGestionePin.ascx.cs"
    Inherits="WebControls_MenuGestionePin" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<table class="menuTable">
    <tr id="RigaTitolo" runat="server" class="menuTable">
        <td>
            Gestione PIN
        </td>
    </tr>
    <%

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePIN.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A7" href="GestioneUtentiGestionePIN.aspx">Gestisci PIN aziende</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A8" href="GestioneUtentiGestionePINLavoratori.aspx">Gestisci PIN lavoratori</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti.ToString()))
        {
    %>
    <tr>
        <td>
            <a id="A9" href="GestioneUtentiGestionePINConsulenti.aspx">Gestisci PIN consulenti</a>
        </td>
    </tr>
    <%
        }
    %>
</table>