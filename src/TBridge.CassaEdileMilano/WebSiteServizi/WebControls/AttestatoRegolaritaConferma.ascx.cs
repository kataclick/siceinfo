﻿#region

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.Presenter;

#endregion

public partial class WebControls_AttestatoRegolaritaConferma : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaImprese(ImpresaCollection imprese)
    {
        AttestatoRegolaritaDatiDomanda1.CaricaImprese(imprese);
    }

    public void CaricaDomanda(Domanda domanda)
    {
        AttestatoRegolaritaDatiDomanda1.CaricaDomanda(domanda);
    }

    protected void CheckBoxRichediVisitaIspettiva_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxRichediVisitaIspettiva.Checked)
        {
            PanelVisitaIspettiva.Visible = true;
        }
        else
        {
            PanelVisitaIspettiva.Visible = false;
            ResetCampiVisitaIspettiva();
        }
    }

    private void ResetCampiVisitaIspettiva()
    {
        Presenter.SvuotaCampo(TextBoxVisitaData);
        Presenter.SvuotaCampo(TextBoxVisitaOrario);
        Presenter.SvuotaCampo(TextBoxVisitaTelefono);
        Presenter.SvuotaCampo(TextBoxVisitaMotivo);
    }

    protected void CustomValidatorOrario_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxRichediVisitaIspettiva.Checked) && (string.IsNullOrEmpty(TextBoxVisitaOrario.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorData_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxRichediVisitaIspettiva.Checked) && (string.IsNullOrEmpty(TextBoxVisitaData.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    protected void CustomValidatorContatto_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if ((CheckBoxRichediVisitaIspettiva.Checked) && (string.IsNullOrEmpty(TextBoxVisitaTelefono.Text.Trim())))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    public string GetOrarioVisitaIspettiva()
    {
        return TextBoxVisitaOrario.Text;
    }

    public string GetMotivoVisitaIspettiva()
    {
        return TextBoxVisitaMotivo.Text;
    }

    public string GetContattoTelefonicoVisitaIspettiva()
    {
        return TextBoxVisitaTelefono.Text;
    }

    public DateTime? GetDataVisitaIspettiva()
    {
        try
        {
            return DateTime.Parse(TextBoxVisitaData.Text);
        }
        catch
        {
            return null;
        }
    }

    public bool GetRichiestaVisitaIspettiva()
    {
        return CheckBoxRichediVisitaIspettiva.Checked;
    }

    public void CaricaDatiVisita(Domanda domanda)
    {
        CheckBoxRichediVisitaIspettiva.Checked = domanda.RichiestaVisitaIspettiva;
        if (CheckBoxRichediVisitaIspettiva.Checked)
        {
            PanelVisitaIspettiva.Visible = true;
            if (domanda.DataVisitaIspettiva.HasValue)
                TextBoxVisitaData.Text = domanda.DataVisitaIspettiva.Value.ToShortDateString();
            TextBoxVisitaMotivo.Text = domanda.MotivoVisitaIspettiva;
            TextBoxVisitaTelefono.Text = domanda.ContattoTelefonicoVisitaIspettiva;
            TextBoxVisitaOrario.Text = domanda.OrarioVisitaIspettiva;
        }
    }
}