using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;

public partial class WebControls_AccessoCantieriRicerca : UserControl
{
    private const Int32 INDICEATTESTATO = 6;
    private const Int32 INDICEMODIFICA = 4;
    private const Int32 INDICEVISUALIZZA = 5;
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    private Boolean isCommittente;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.isCommittente = GestioneUtentiBiz.IsCommittente();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            RadGridCantieri.Visible = true;
            CaricaCantieri();
        }
    }

    private void CaricaCantieri()
    {
        WhiteListFilter filtro = CreaFiltro();

        WhiteListCollection domande = _biz.GetDomandeByFilter(filtro);
        Presenter.CaricaElementiInGridView(
            RadGridCantieri,
            domande);
    }

    private WhiteListFilter CreaFiltro()
    {
        WhiteListFilter filtro = new WhiteListFilter
                                     {
                                         Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text),
                                         Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text),
                                         IdUtente = GestioneUtentiBiz.GetIdUtente()
                                     };

        if (this.isCommittente)
        {
            filtro.IdCommittente = ((TBridge.Cemi.GestioneUtenti.Type.Entities.Committente) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdCommittente;
        }

        return filtro;
    }

    protected void RadGridCantieri_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            WhiteList domanda = (WhiteList) e.Item.DataItem;

            Label lIndirizzo = (Label) e.Item.FindControl("LabelIndirizzo");
            Label lComune = (Label) e.Item.FindControl("LabelComune");
            Label lProvincia = (Label) e.Item.FindControl("LabelProvincia");

            HtmlTable tableModifica = (HtmlTable) e.Item.FindControl("tableModifica");

            lIndirizzo.Text = String.Format("{0} {1}", domanda.Indirizzo, domanda.Civico);
            lComune.Text = domanda.Comune;
            lProvincia.Text = domanda.Provincia;

            if (GestioneUtentiBiz.IsCommittente())
            {
                tableModifica.Visible = false;
            }
        }
    }

    protected void RadGridCantieri_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaCantieri();
    }

    protected void RadGridCantieri_ItemCommand(object sender, GridCommandEventArgs e)
    {
        Int32 idDomanda;

        switch (e.CommandName)
        {
            case "MODIFICA":
                idDomanda = (Int32) e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["idWhiteList"];
                Context.Items["IdDomanda"] = idDomanda;
                Server.Transfer("~/AccessoCantieri/GestioneCantiere.aspx?op=modifica");
                break;
            case "VISUALIZZA":
                idDomanda = (Int32) e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["idWhiteList"];
                Context.Items["IdDomanda"] = idDomanda;
                Server.Transfer("~/AccessoCantieri/VisualizzaWhiteList.aspx");
                break;
            case "STAMPA":
                idDomanda = (Int32) e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["idWhiteList"];
                Context.Items["IdDomanda"] = idDomanda;
                Server.Transfer("~/AccessoCantieri/ReportAccessoCantieri.aspx");
                break;
        }
    }
}