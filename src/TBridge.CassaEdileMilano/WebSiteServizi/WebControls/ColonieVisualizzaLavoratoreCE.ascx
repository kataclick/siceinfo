<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieVisualizzaLavoratoreCE.ascx.cs"
    Inherits="WebControls_ColonieVisualizzaLavoratoreCE" %>
<table class="standardTable">
    <tr>
        <td style="height: 18px" colspan="2">
            <asp:Label ID="Label2" runat="server" Font-Bold="True">Dettagli lavoratore</asp:Label>
        </td>
    </tr>
    <tr>
        <td style="height: 18px">
            Cognome
        </td>
        <td style="height: 18px">
            <asp:Label ID="LabelCognome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Nome
        </td>
        <td>
            <asp:Label ID="LabelNome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita
        </td>
        <td>
            <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale
        </td>
        <td>
            <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
        </td>
    </tr>
    <% if (!AttivaVisualizzazioneParziale)
       { %>
    <tr>
        <td>
            Sesso
        </td>
        <td>
            <asp:Label ID="LabelSesso" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Telefono
        </td>
        <td>
            <asp:Label ID="LabelTelefono" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Cellulare
        </td>
        <td>
            <asp:Label ID="LabelCellulare" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Residenza
        </td>
        <td colspan="2">
            <table class="standardTable">
                <tr>
                    <td>
                        Indirizzo
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaIndirizzo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaComune" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaProvincia" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        CAP
                    </td>
                    <td>
                        <asp:Label ID="LabelResidenzaCap" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <%} %>
</table>
