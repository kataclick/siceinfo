<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuTuteScarpe.ascx.cs" Inherits="WebControls_MenuTuteScarpe" %>
<%
    if (
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFabbisogniConfermati.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneConsulenti.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneSms.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeSelezioneTaglie.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneScadenze.ToString())
        )
        {                  
         %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Tute e Scarpe</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeSelezioneTaglie.ToString())
           && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore()))
        {                  
         %>
    <tr>
        <td>
            <a id="A9" href="~/TuteScarpe/SelezioneTaglie.aspx" runat="server">Selezione taglie</a>
        </td>
    </tr>
    
    <%
        } 
        %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno.ToString())
           && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa()))
        {                  
         %>
    <tr>
        <td>
            <a id="A10" href="~/TuteScarpe/GestioneFabbisogno.aspx" runat="server">Gestione ultimo fabbisogno</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A12" href="~/TuteScarpe/VisualizzaStorico.aspx" runat="server">Visualizza Storico fabbisogni</a>
        </td>
    </tr>
    
    <%
        } 
        %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno.ToString())
       && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsFornitore()))
        {                  
     %>
    <tr>
        <td>
            <a id="A3" href="~/TuteScarpe/TuteScarpeGestioneFabbisognoFornitore.aspx" runat="server">Gestione fabbisogni</a>
        </td>
    </tr>
    <%
    }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno.ToString())
       && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente()))
    {                  
     %>
    <tr>
        <td>
            <a id="A4" href="~/TuteScarpe/TuteScarpeGestioneFabbisognoConsulenti.aspx" runat="server">Gestione fabbisogni</a>
        </td>
    </tr>
    <%
        } 
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneFabbisogniConfermati.ToString()))
        {                  
         %>    
    <tr>
        <td>
        <A id="A5" HREF="~/TuteScarpe/TuteScarpeGestioneFabbisogniConfermati.aspx" runat="server">Gestione fabbisogni confermati</A>
        </td>
    </tr>
    <%
        } 
         if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneScadenze.ToString()))
        {                  
         %>
    <tr>
        <td>
        <A id="A6" HREF="~/TuteScarpe/TuteScarpeScadenzaFabbisogni.aspx" runat="server">Gestione scadenze</A>
        </td>
    </tr>
    <%
        } 
         if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneConsulenti.ToString()))
        {                  
         %> 
    <tr>
        <td>
            <A id="A7" HREF="~/TuteScarpe/TuteScarpeConsulenteImprese.aspx" runat="server">Gestione consulenti</A>
        </td>
    </tr>
    <%
        } 
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeGestioneSms.ToString()))
        {                  
         %> 
    <tr>
        <td>
            <A id="A8" HREF="~/TuteScarpe/TuteScarpeVisualizzaRichiesteSms.aspx" runat="server">Visualizza richieste SMS</A>
        </td>
    </tr>
    <%
        } %>
</table>
<%
   } 
    
    %>