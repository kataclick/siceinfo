﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuIscrizioneLavoratori.ascx.cs"
    Inherits="WebControls_MenuIscrizioneLavoratori" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Notifiche lavoratori
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/IscrizioneLavoratori/IscrizioneLavoratore.aspx" runat="server">Iscrizione</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/IscrizioneLavoratori/IscrizioneLavoratoreSintesiInterfaccia.aspx" runat="server">Iscrizione Sintesi</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia)
            )
        {
    %>
    <tr>
        <td>
            <a id="A5" href="~/IscrizioneLavoratori/Ricerca.aspx" runat="server">Ricerca</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/IscrizioneLavoratori/GestioneCE.aspx" runat="server">Gestione</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>