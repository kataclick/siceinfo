<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniRicercaDocumenti.ascx.cs"
    Inherits="WebControls_PrestazioniRicercaDocumenti" %>
<style type="text/css">
    .style1
    {
        height: 16px;
        width: 149px;
    }
    .style2
    {
        width: 149px;
    }
</style>
<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonVisualizza">
    <table class="borderedTable">
        <tr>
            <td>
                <asp:Label ID="LabelErroreVisualizzazioneDocumento" runat="server" Text="Documento non trovato o errore durante la lettura da Archidoc"
                    Visible="False" ForeColor="Red">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td style="height: 16px">
                            Codice lav.
                            <asp:CompareValidator ID="CompareValidatorIdLavoratore" runat="server" ControlToValidate="TextBoxIdLavoratore"
                                ErrorMessage="*" Operator="GreaterThan" Type="Integer" ValidationGroup="ricercaDocumenti"
                                ValueToCompare="0"></asp:CompareValidator>
                        </td>
                        <td class="style1">
                            Cognome
                        </td>
                        <td style="height: 16px">
                            Tipo Documento
                        </td>
                        <td style="height: 16px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIdLavoratore" runat="server" MaxLength="30" Width="170"></asp:TextBox>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="30" Width="170"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipoDocumento" runat="server" AppendDataBoundItems="true"
                                Width="170px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="LabelPadding" runat="server" Width="170px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 16px">
                            Tipo prestazione
                        </td>
                        <td class="style1">
                            Protocollo prestazione(anno)
                            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="TextBoxProtocolloPrestazione"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                        </td>
                        <td style="height: 16px">
                            Numero protocollo prestazione
                            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="TextBoxNumeroProcolloPrestazione"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                        </td>
                        <td style="height: 16px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIdTipoPrestazione" runat="server" MaxLength="10" Width="170"></asp:TextBox>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="TextBoxProtocolloPrestazione" runat="server" MaxLength="10" Width="170"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNumeroProcolloPrestazione" runat="server" MaxLength="10"
                                Width="170"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 16px">
                            Data scansione da
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxDataScansioneDa"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                        </td>
                        <td class="style1">
                            Data scansione a
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBoxDataScansioneA"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                        </td>
                        <td style="height: 16px">
                            &nbsp;
                        </td>
                        <td style="height: 16px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxDataScansioneDa" runat="server" MaxLength="10" Width="170"></asp:TextBox>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="TextBoxDataScansioneA" runat="server" MaxLength="10" Width="170"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td class="style2">
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            &nbsp;
                            <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaDocumenti" Style="margin-left: 0px" Width="120px" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si � verificato un errore durante l'operazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco documenti"></asp:Label><br />
                <asp:GridView ID="GridViewDocumenti" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    DataKeyNames="TipoDocumento,IdDocumento,PerPrestazione,IdArchidoc" OnPageIndexChanging="GridViewDocumenti_PageIndexChanging"
                    OnRowDataBound="GridViewDocumenti_RowDataBound" Width="100%" OnRowCommand="GridViewDocumenti_RowCommand"
                    PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo doc.">
                            <ItemStyle Width="70px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdLavoratore" HeaderText="Codice lav.">
                            <ItemStyle Width="20px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Codice fam." DataField="IdFamiliare">
                            <ItemStyle Width="20px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Familiare">
                            <ItemTemplate>
                                <asp:Label ID="LabelFamiliare" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="IdTipoPrestazione" HeaderText="Tipo" />
                        <asp:BoundField DataField="ProtocolloPrestazione" HeaderText="Prot." />
                        <asp:BoundField DataField="NumeroProtocolloPrestazione" HeaderText="Num.Prot." />
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="visualizza"
                            Text="Vis.">
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="associa"
                            Text="Ass.">
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun documento trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
