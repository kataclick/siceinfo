using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;

public partial class WebControls_RegistrazioneLavoratore : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonRegistraLavoratore_Click(object sender, EventArgs e)
    {
        if (((RadioButtonList) LiberatoriaPrivacy1.FindControl("RadioButtonListPrivacy")).SelectedValue == "Consento")
        {
            if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text)
                //&& GestioneUtentiBiz.ControllaFormatoEmail(this.TextBoxEmail.Text)
                && TextBoxPassword.Text == TextBoxPasswordRidigitata.Text)
            {
                //
                //Lavoratore lavoratore = new Lavoratore();
                Lavoratore lavoratore = new Lavoratore(
                    0,
                    TextBoxLogin.Text
                    );

                lavoratore.Nome = TextBoxNome.Text;
                lavoratore.Cognome = TextBoxCognome.Text;
                lavoratore.CodiceFiscale = TextBoxCodiceFiscale.Text;

                //lavoratore.Username = TextBoxLogin.Text;
                lavoratore.Password = TextBoxPassword.Text;

                lavoratore.Pin = TextBoxPIN.Text;

                GestioneUtentiBiz gu = new GestioneUtentiBiz();

                //
                switch (gu.RegistraLavoratore(lavoratore))
                {
                    case ErroriRegistrazione.RegistrazioneEffettuata:
                        //Rigenero il PIN
                        //gu.GeneraPinLavoratore(lavoratore.IdLavoratore);
                        (new UtentiManager()).ImpostaPinLavoratoreByIdUtente(lavoratore.IdUtente);
                        int numeroMesi = Convert.ToInt32(ConfigurationManager.AppSettings["MesiValiditaPassword"]);
                        (new UtentiManager()).AggiornaScadenzaPassword(lavoratore.IdUtente, numeroMesi);
                        LabelResult.Text = "Registrazione completata.";
                        //Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx);
                        Context.Items["idUtente"] = lavoratore.IdUtente;
                        Server.Transfer("GestioneUtentiRegistrazioneAvvenuta.aspx");
                        break;
                    case ErroriRegistrazione.ChallengeNonPassato:
                        LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                        break;
                    case ErroriRegistrazione.LoginPresente:
                        LabelResult.Text =
                            "La username scelta � gi� presente nel sistema, sceglierne un'altra e riprovare.";
                        break;
                    case ErroriRegistrazione.RegistrazioneGiaPresente:
                        LabelResult.Text = "La registrazione risulta come gi� effettuata.";
                        break;
                    case ErroriRegistrazione.Errore:
                        LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                        break;
                    case ErroriRegistrazione.LoginNonCorretta:
                        LabelResult.Text =
                            "La username fornita contiene caratteri non validi o la sua lunghezza non � compresa fra 5 e 15 caratteri.";
                        break;
                    case ErroriRegistrazione.PasswordNonCorretta:
                        LabelResult.Text =
                            "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                        break;
                    default:
                        LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                        break;
                }
                //
                //if (gu.RegistraLavoratore(lavoratore))
                //{
                //    this.LabelResult.Text = "Registrazione completata";
                //    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                //}
                //else
                //{
                //    this.LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare.";
                //}
            }
            else
            {
                LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
            }
        }
        else
        {
            LabelResult.Text = "Deve essere rilasciato il consenso al trattamento dei dati";
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("GestioneUtentiRegistrazione.aspx");
    }
}