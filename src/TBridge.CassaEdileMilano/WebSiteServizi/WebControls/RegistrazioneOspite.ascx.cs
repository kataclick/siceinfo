using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;

public partial class WebControls_RegistrazioneOspite : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonRegistraLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            // Page.Validate("UsernameValidation");

            if (Page.IsValid)
            {
                //if (((RadioButtonList)LiberatoriaPrivacy1.FindControl("RadioButtonListPrivacy")).SelectedValue ==
                //    "Consento")
                //{
                    //if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text)
                    //    && GestioneUtentiBiz.ControllaFormatoEmail(TextBoxEmail.Text)
                    //    && TextBoxPassword.Text == TextBoxPasswordRidigitata.Text)
                    //{
                    //Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
                    //Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

                    //if (patternUsername.IsMatch(TextBoxLogin.Text))
                    //{
                    //    if (patternPassword.IsMatch(TextBoxPassword.Text))
                    //    {
                    //
                    //Ospite ospite = new Ospite();
                    Ospite ospite = new Ospite(
                        0,
                        TextBoxLogin.Text
                        );

                    ospite.Nome = TextBoxNome.Text;
                    ospite.Cognome = TextBoxCognome.Text;
                    ospite.Ente = TextBoxEnte.Text;
                    ospite.Email = TextBoxEmail.Text;

                    //ospite.Username = TextBoxLogin.Text;
                    ospite.Password = TextBoxPassword.Text;

                    GestioneUtentiBiz gu = new GestioneUtentiBiz();

                    //
                    switch (gu.InserisciOspite(ospite))
                    {
                        case ErroriRegistrazione.RegistrazioneEffettuata:
                            TextBoxNome.Text = string.Empty;
                            TextBoxCognome.Text = string.Empty;
                            TextBoxEmail.Text = string.Empty;
                            TextBoxLogin.Text = string.Empty;
                            TextBoxPassword.Text = string.Empty;
                            TextBoxPasswordRidigitata.Text = string.Empty;

                            LabelResult.Text = "Registrazione completata";

                            Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                            break;
                        case ErroriRegistrazione.ChallengeNonPassato:
                            LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                            break;
                        case ErroriRegistrazione.LoginPresente:
                            LabelResult.Text =
                                "La username scelta � gi� presente nel sistema, sceglierne un'altra e riprovare.";
                            break;
                        case ErroriRegistrazione.RegistrazioneGiaPresente:
                            LabelResult.Text = "La registrazione risulta come gi� effettuata.";
                            break;
                        case ErroriRegistrazione.Errore:
                            LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                            break;
                        case ErroriRegistrazione.LoginNonCorretta:
                            LabelResult.Text =
                                "La username fornita contiene caratteri non validi o la sua lunghezza non � compresa fra 5 e 15 caratteri.";
                            break;
                        case ErroriRegistrazione.PasswordNonCorretta:
                            LabelResult.Text =
                                "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                            break;
                        default:
                            LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare pi� tardi.";
                            break;
                    }

                    #region old

                    //if (gu.InserisciOspite(ospite))
                    //{
                    //    LabelResult.Text = "Registrazione completata";
                    //    //Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                    //    Context.Items["idUtente"] = ospite.IdUtente;
                    //    Server.Transfer("GestioneUtentiRegistrazioneAvvenuta.aspx");
                    //}
                    //else
                    //{
                    //    LabelResult.Text = "La registrazione non � andata a buon fine. Riprovare.";
                    //}

                    #endregion
                }
                //    else
                //    {
                //        LabelResult.Text =
                //            "La password deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                //    }
                //}
                //else
                //{
                //    LabelResult.Text =
                //        "La username fornita contiene caratteri non validi o la sua lunghezza non � compresa fra 5 e 15 caratteri.";
                //}
                //}
                //else
                //{
                //    LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
                //}
            }
            else
            {
                LabelResult.Text = "Deve essere rilasciato il consenso al trattamento dei dati";
            }
        //}
    }


    //protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
    //    args.IsValid = patternUsername.IsMatch(TextBoxLogin.Text);
    //}

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect
        ("GestioneUtentiRegistrazione.aspx");
    }

}