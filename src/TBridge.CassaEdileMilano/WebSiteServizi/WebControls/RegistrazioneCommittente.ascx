﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegistrazioneCommittente.ascx.cs"
    Inherits="WebControls_RegistrazioneCommittente" %>
<br />
<table width="600">
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Nome:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Cognome:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Ragione sociale:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRagioneSociale" runat="server"
                ControlToValidate="TextBoxRagioneSociale" ErrorMessage="*" ValidationGroup="registrazioneCommittente"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Partita IVA:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPartitaIva" runat="server" MaxLength="11" Width="250px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Codice fiscale:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="250px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="Tipologia:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCommittenteTipologia" runat="server" Width="250px"
                AppendDataBoundItems="True" Height="17px">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipologia" runat="server" ControlToValidate="DropDownListCommittenteTipologia"
                ErrorMessage="*" ValidationGroup="registrazioneCommittente"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Indirizzo:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Provincia:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListProvincia" runat="server" AppendDataBoundItems="True"
                AutoPostBack="True" Height="17px" OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged"
                Width="250px">
            </asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Comune:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListComuni" runat="server" AppendDataBoundItems="True"
                AutoPostBack="True" Height="16px" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                Width="250px">
            </asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Cap:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListCAP" runat="server" AppendDataBoundItems="True"
                Width="250px">
            </asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="Telefono:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxTelefono" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="Fax:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxFax" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelEmail" runat="server" Font-Bold="True" Text="Email:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxEmail" runat="server" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato email non valido"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="TextBoxEmail"
                ValidationGroup="registrazioneCommittente"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelUsername" runat="server" Font-Bold="True" Text="Username:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxLogin" runat="server" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxLogin"
                ErrorMessage="*" ValidationGroup="registrazioneCommittente"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPassword" runat="server" Font-Bold="True" Text="Password:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxPassword"
                ErrorMessage="*" ValidationGroup="registrazioneCommittente"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPasswordRidigitata" runat="server" Font-Bold="True" Text="Ridigita password:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPasswordRidigitata" runat="server" TextMode="Password" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxPasswordRidigitata"
                ErrorMessage="*" ValidationGroup="registrazioneCommittente"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPassword"
                ControlToValidate="TextBoxPasswordRidigitata" ErrorMessage="Password diversa"
                ValidationGroup="registrazioneCommittente"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
            dal username e deve contenere almeno una lettera e un numero.<br />
            Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
            pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
            ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
            dal sistema.
        </td>
    </tr>
    <tr>
        <td colspan="3" height="5">
        </td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Button ID="ButtonRegistraCommittente" runat="server" Text="Registra committente"
                OnClick="ButtonRegistraCommittente_Click" ValidationGroup="registrazioneCommittente" />
            <asp:Button ID="ButtonIndietro" runat="server" CausesValidation="False" OnClick="ButtonIndietro_Click"
                Text="Indietro" Width="96px" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Label ID="LabelResult" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>
<br />
