using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.AccessoCantieri.Type.Delegates;
using TBridge.Cemi.AccessoCantieri.Type.Exceptions;
//using System.Collections.Specialized;
//using TBridge.Cemi.Type.Collections;

public partial class WebControls_AccessoCantieriLavoratore : UserControl
{
    private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
    //private readonly Common _bizCommon = new Common();
    protected String PostBackStringEdit;
    protected String PostBackStringNew;

    public event LavoratoriDeletedEventHandler OnLavoratoreDeleted;

    protected void Page_Init(object sender, EventArgs e)
    {
        PostBackStringNew = Page.ClientScript.GetPostBackEventReference(this, "RefreshNew");
        PostBackStringEdit = Page.ClientScript.GetPostBackEventReference(this, "RefreshEdit");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region biz ed eventi

        AccessoCantieriRicercaLavoratore1.OnNuovoLavoratoreSelected +=
            AttestatoRegolaritaRicercaLavoratore1_OnNuovoLavoratoreSelected;

        #endregion

        #region Impostiamo gli eventi JS per la gestione dei click multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();

        ////resettiamo
        sb.Remove(0, sb.Length);
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append(
            "if (Page_ClientValidate('validationGroupLavoratore') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovoLavoratore, null) + ";");
        sb.Append("return true;");
        ButtonInserisciNuovoLavoratore.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            ButtonInserisciNuovoLavoratore);

        #endregion

        if (!Page.IsPostBack)
        {
            if (ViewState["domandeImprese"] == null)
            {
                ViewState["domandeImprese"] = new WhiteListImpresaCollection();
            }
        }
        else
        {
            String eventArgs = Request["__EVENTARGUMENT"];
            if (eventArgs == "RefreshNew")
            {
                AggiornaLavoratoreDaPopupNew();
            }
            if (eventArgs == "RefreshEdit")
            {
                AggiornaLavoratoreDaPopupEdit();
            }
        }
    }

    protected String RecuperaPostBackCode()
    {
        return Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
    }

    private void AggiornaLavoratoreDaPopupNew()
    {
        if (Session["AccessoCantieriLavoratore"] != null)
        {
            Lavoratore lavoratore = (Lavoratore) Session["AccessoCantieriLavoratore"];
            LavoratoreSelezionato(lavoratore, false);

            Session["AccessoCantieriLavoratore"] = null;
        }
    }

    private void AggiornaLavoratoreDaPopupEdit()
    {
        if (Session["AccessoCantieriLavoratore"] != null && Session["index"] != null)
        {
            Lavoratore lavoratore = (Lavoratore) Session["AccessoCantieriLavoratore"];
            Int32 index = (Int32) Session["index"];
            ViewState["editIndex"] = index;
            WhiteListImpresa imprese = (WhiteListImpresa) Session["LavoratoriImpresa"];
            //imprese.Lavoratori[index] = lavoratore;
            ViewState["Lavoratore"] = lavoratore;
            ViewState["domandaImpresa"] = imprese;

            LavoratoreSelezionato(lavoratore, true);

            Session["AccessoCantieriLavoratore"] = null;
            Session["index"] = null;
        }
    }

    public void CaricaDataInizio(DateTime? dataInizio)
    {
        ViewState["dataInizio"] = dataInizio;
        AccessoCantieriRicercaLavoratore1.CaricaDataInizio(dataInizio);
    }

    public void CaricaDataFine(DateTime? dataFine)
    {
        ViewState["dataFine"] = dataFine;
        AccessoCantieriRicercaLavoratore1.CaricaDataFine(dataFine);
    }

    public void CaricaDomanda(Int32? idDomanda)
    {
        ViewState["idDomanda"] = idDomanda;
        //ButtonImportaLavoratori.Attributes.Add("onClick", String.Format("openImport({0}); return false;", idDomanda));
    }

    public void CaricaImprese(ImpresaCollection imprese, SubappaltoCollection subappalti)
    {
        ViewState["subappalti"] = subappalti;

        DropDownListImpresa.Items.Clear();
        DropDownListImpresa.Items.Add(new ListItem(string.Empty, string.Empty));
        foreach (Impresa imp in imprese)
        {
            if (!imp.LavoratoreAutonomo)
                DropDownListImpresa.Items.Add(
                    new ListItem(imp.CodiceERagioneSociale,
                                 string.Format("{0}|{1}|{2}", ((int) imp.TipoImpresa), imp.IdImpresa, imp.IdTemporaneo)));
        }

        if (imprese.Count == 0)
        {
            LabelImprese.Text = "Non ci sono imprese da selezionare";
            DropDownListImpresa.Visible = false;
            LabelImpresaSelezionata.Visible = false;
            ButtonaggiungiLavoratore.Enabled = false;
            ButtonEsportaLavoratori.Enabled = false;
            //ButtonImportaLavoratori.Enabled = false;
            //FileUploadCsv.Enabled = false;
            ButtonImportaListaLavoratori.Enabled = false;
            PanelAggiungiLavoratore.Visible = false;
            AccessoCantieriRicercaLavoratore1.Visible = false;
            PanelInserisciLavoratore.Visible = false;
        }
        else
        {
            LabelImprese.Text = "Imprese ";
            DropDownListImpresa.Visible = true;
            LabelImpresaSelezionata.Visible = true;
        }
    }

    public void CaricaLavoratori(WhiteListImpresaCollection domImp)
    {
        ViewState["domandeImprese"] = domImp;
    }

    private void CaricaDatiLavoratori()
    {
        GridViewLavoratori.Visible = true;
        WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection) ViewState["domandeImprese"];

        int? idImpresa = null;
        Guid? idTemporaneo = null;
        string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
        TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

        if (tipoImpresa == TipologiaImpresa.SiceNew)
        {
            idImpresa = Int32.Parse(tipoId[1]);
        }
        else
        {
            if (String.IsNullOrEmpty(tipoId[1]))
            {
                Guid gid = new Guid(tipoId[2]);
                idTemporaneo = gid;
            }
            else
            {
                idImpresa = Int32.Parse(tipoId[1]);
            }
        }

        WhiteListImpresa domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, idImpresa, idTemporaneo,
                                                                                 null);

        if (ViewState["idDomanda"] != null)
        {
            WhiteList domanda = biz.GetDomandaByKey((Int32) ViewState["idDomanda"]);
            ViewState["AutSub"] = domanda.AutorizzazioneAlSubappalto;
        }

        if (domandaImpresa != null)
        {
            Presenter.CaricaElementiInGridView(
                GridViewLavoratori,
                domandaImpresa.Lavoratori,
                0);
        }
        else
        {
            Presenter.CaricaElementiInGridView(
                GridViewLavoratori,
                null,
                0);
        }
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore lav = (Lavoratore) e.Row.DataItem;
            Label lLavoratore = (Label) e.Row.FindControl("LabelLavoratore");
            ImageButton ibModifica = (ImageButton) e.Row.FindControl("ImageButtonModifica");
            Image iBadge = (Image) e.Row.FindControl("ImageBadge");

            if (lav != null)
            {
                //if (AccessoCantieriBusiness.BadgeReady(lav, ViewState["AutSub"] as String))
                //{
                //    iBadge.Visible = true;
                //}

                Impresa impresa = (Impresa)ViewState["Impresa"];
                //Int32 idCantiere = Int32.Parse(ViewState["idDomanda"].ToString());
                Int32 idCantiere = -1;
                if (ViewState["idDomanda"] != null)
                {
                    idCantiere = Int32.Parse(ViewState["idDomanda"].ToString());
                }
                string AutSub = string.Empty;
                if (ViewState["AutSub"] != null)
                {
                    AutSub = ViewState["AutSub"].ToString();
                }

                if (AutSub != null && impresa != null && idCantiere != -1 && lav != null)
                {
                    if (biz.BadgeReadyFoto(lav, AutSub, impresa, idCantiere))
                    {
                        iBadge.Visible = true;
                    }
                }

                if (lav.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    lLavoratore.Text = string.Format("{0} - {1} {2}", lav.IdLavoratore, lav.Cognome, lav.Nome);
                }
                else
                {
                    lLavoratore.Text = string.Format("{0} {1}", lav.Cognome, lav.Nome);
                }

                ImageButton ibElimina = (ImageButton) e.Row.FindControl("ImageButtonElimina");

                Int32? idDomanda;
                try
                {
                    idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
                }
                catch
                {
                    idDomanda = null;
                }

                if (idDomanda.HasValue)
                {
                    Int32 timbCount = biz.GetCountTimbrature(null, lav.CodiceFiscale, idDomanda.Value);

                    ibElimina.Visible = timbCount == 0;
                }
                else
                {
                    ibElimina.Visible = true;
                }

                String dataInizio = String.Empty;
                if (ViewState["dataInizio"] != null)
                {
                    dataInizio = ((DateTime) ViewState["dataInizio"]).ToString("dd/MM/yyyy");
                }
                String dataFine = String.Empty;
                if (ViewState["dataFine"] != null)
                {
                    dataFine = ((DateTime) ViewState["dataFine"]).ToString("dd/MM/yyyy");
                }
                ibModifica.Attributes.Add("onclick",
                                          String.Format("openLavoratore(1, '{0}', '{1}', {2}); return false;",
                                                        dataInizio, dataFine, e.Row.RowIndex));
            }
        }
    }

    protected void GridViewLavoratori_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        WhiteListImpresa dImp = (WhiteListImpresa) ViewState["domandaImpresa"];

        if (OnLavoratoreDeleted != null)
        {
            OnLavoratoreDeleted(dImp.Lavoratori[e.RowIndex].CodiceFiscale);
        }

        dImp.Lavoratori.RemoveAt(e.RowIndex);

        ViewState["domandaImpresa"] = dImp;
        Int32? idDomanda;
        try
        {
            idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
        }
        catch
        {
            idDomanda = null;
        }
        if (idDomanda.HasValue)
            dImp.IdDomanda = idDomanda.Value;
        Session["LavoratoriImpresa"] = dImp;

        Salva();

        CaricaDatiLavoratori();

        Impresa impresa = ViewState["Impresa"] as Impresa;
        if (impresa != null && impresa.TipoImpresa == TipologiaImpresa.SiceNew)
        {
            CaricaLavoratoriInForza(impresa.IdImpresa.Value, GridViewLavoratoriInForza.PageIndex);
        }
    }

    private void ResetCampi()
    {
        ViewState["Lavoratore"] = null;
        LabelResInserimentoLavInLista.Text = string.Empty;
    }

    public void DeselezionaImpresa()
    {
        ViewState["Impresa"] = null;
        LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";
        DropDownListImpresa.SelectedIndex = 0;

        GridViewLavoratori.Visible = false;
        ButtonaggiungiLavoratore.Enabled = false;
        //ButtonImportaLavoratori.Enabled = false;
        //FileUploadCsv.Enabled = false;
        ButtonImportaListaLavoratori.Enabled = false;
        ButtonEsportaLavoratori.Enabled = false;
        PanelAggiungiLavoratore.Visible = false;
        AccessoCantieriRicercaLavoratore1.Visible = false;
        PanelInserisciLavoratore.Visible = false;
        LabelStatoImportExport.Text = "";
    }

    protected void ButtonAggiungiLavoratore_Click(object sender, EventArgs e)
    {
        PanelLavoratoriDaFile.Visible = false;
        AccessoCantieriRicercaLavoratore1.Reset();

        LabelGiaPresente.Visible = false;
        AccessoCantieriLavoratore1.Reset();

        PanelAggiungiLavoratore.Visible = true;
        AccessoCantieriRicercaLavoratore1.Visible = true;

        PanelRicercaInserisciLavoratore.Visible = true;
        AccessoCantieriRicercaLavoratore1.Visible = true;
        PanelInserisciLavoratore.Visible = false;
    }

    //protected void DropDownListImpresa_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
    //        SelezionaImpresaDaDropDowList();
    //}

    private void SelezionaImpresaDaDropDowList()
    {
        PanelWhiteList.Visible = true;
        string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
        TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

        Impresa impresa = new Impresa {TipoImpresa = tipoImpresa};
        PanelLavoratoriInForza.Visible = false;

        if (tipoImpresa == TipologiaImpresa.SiceNew)
        {
            impresa.IdImpresa = Int32.Parse(tipoId[1]);
        }
        else
        {
            if (String.IsNullOrEmpty(tipoId[1]))
            {
                Guid gid = new Guid(tipoId[2]);
                impresa.IdTemporaneo = gid;
            }
            else
            {
                impresa.IdImpresa = Int32.Parse(tipoId[1]);
            }
        }

        String ragioneSociale = DropDownListImpresa.SelectedItem.Text;
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
        {
            String[] splitted = ragioneSociale.Split('-');
            ragioneSociale = String.Empty;

            for (int i = 1; i < splitted.Length; i++)
            {
                ragioneSociale += splitted[i];

                if (i > 1)
                {
                    ragioneSociale += "-";
                }
            }
        }

        Int32? idDomanda;
        try
        {
            idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
        }
        catch
        {
            idDomanda = null;
        }

        impresa.RagioneSociale = ragioneSociale;

        WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection) ViewState["domandeImprese"];
        WhiteListImpresa domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, impresa.IdImpresa,
                                                                                 impresa.IdTemporaneo, null);
        ViewState["domandaImpresa"] = domandaImpresa;
        //if (idDomanda.HasValue )
        //    domandaImpresa.IdDomanda = idDomanda.Value;

        if (idDomanda.HasValue )
            Session["IdCantiere"] = idDomanda.Value;

        Session["LavoratoriImpresa"] = domandaImpresa;


        ImpresaSelezionata(impresa);

        Salva();

        CaricaDatiLavoratori();
    }

    private void Salva()
    {
        WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection) ViewState["domandeImprese"];
        WhiteListImpresa domImp = (WhiteListImpresa) ViewState["domandaImpresa"];

        if (domandeImprese != null)
        {
            if (domImp != null)
            {
                if (!domandeImprese.Contains(domImp) && domImp.Impresa.IdImpresa != null)
                {
                    domandeImprese.Add(domImp);
                }
                else
                {
                    if (domImp.Impresa.IdImpresa != null)
                    {
                        domandeImprese.Cancella(domImp);
                        domandeImprese.Add(domImp);
                    }
                }

                if (domImp.Impresa.IdImpresa == null)
                {
                    domandeImprese.CancellaTutto(domImp, domImp.Impresa.RagioneSociale);
                    domandeImprese.Add(domImp);
                }

                ViewState["domandeImprese"] = domandeImprese;
            }
        }
    }

    private void ImpresaSelezionata(Impresa impresa)
    {
        ViewState["Impresa"] = impresa;

        LabelImpresaSelezionata.Text = string.Format("{0} : {1}", "Impresa selezionata", impresa.RagioneSociale);

        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
        {
            CaricaLavoratoriInForza(impresa.IdImpresa.Value, 0);
        }

        AccessoCantieriRicercaLavoratore1.SetImpresaSelezionata(impresa);

        ButtonaggiungiLavoratore.Enabled = true;
        ButtonEsportaLavoratori.Enabled = true;
        //ButtonImportaLavoratori.Enabled = true;
        //FileUploadCsv.Enabled = true;
        ButtonImportaListaLavoratori.Enabled = true;
        LabelStatoImportExport.Text = "";

        WhiteListImpresa dImp = (WhiteListImpresa) ViewState["domandaImpresa"];
        Int32? idDomanda;
        try
        {
            idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
        }
        catch
        {
            idDomanda = null;
        }

        // Questo serviva a bloccare il caricamento da lista se erano presenti delle timbrature
        //if (idDomanda.HasValue)
        //{
        //    if (dImp != null)
        //    {
        //        if (dImp.Lavoratori != null)
        //        {
        //            foreach (Lavoratore lav in dImp.Lavoratori)
        //            {
        //                Int32 timbCount = biz.GetCountTimbrature(null, lav.CodiceFiscale, idDomanda.Value);
        //                if (timbCount > 0)
        //                {
        //                    ButtonImportaLavoratori.Enabled = false;
        //                    FileUploadCsv.Enabled = false;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //}
    }

    protected void DropDownListImpresa_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ResetCaricaLista();

        LabelGiaPresente.Text = null;
        AccessoCantieriRicercaLavoratore1.Reset();

        if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
            SelezionaImpresaDaDropDowList();
        else
        {
            ViewState["Impresa"] = null;

            LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";

            GridViewLavoratori.Visible = false;
            ButtonaggiungiLavoratore.Enabled = false;
            //ButtonImportaLavoratori.Enabled = false;
            ButtonImportaListaLavoratori.Enabled = false;
            //FileUploadCsv.Enabled = false;
            ButtonEsportaLavoratori.Enabled = false;
            PanelAggiungiLavoratore.Visible = false;
            AccessoCantieriRicercaLavoratore1.Visible = false;
            PanelInserisciLavoratore.Visible = false;
            LabelStatoImportExport.Text = "";
        }
    }

    private void ResetCaricaLista()
    {
        RadDatePickerDataInizioAttivita.SelectedDate = null;
        RadDatePickerDataFineAttivita.SelectedDate = null;
        Presenter.CaricaElementiInListView(
            ListViewErrori,
            null);
        PanelLavoratoriDaFile.Visible = false;
        PanelWhiteList.Visible = false;
        PanelLavoratoriInForza.Visible = false;
    }

    //protected void GridViewLavoratori_RowCommand(object sender, GridViewCommandEventArgs e)
    //{

    //}

    //public static int IndiceComuneInDropDown(DropDownList dropDownListComuni, string codiceCatastale)
    //{
    //    int res = 0;

    //    for (int i = 0; i < dropDownListComuni.Items.Count; i++)
    //    {
    //        string valoreCombo = dropDownListComuni.Items[i].Value;
    //        string[] splitted = valoreCombo.Split('|');
    //        if (splitted.Length > 1)
    //        {
    //            if (splitted[0] == codiceCatastale)
    //            {
    //                res = i;
    //                break;
    //            }
    //        }
    //    }

    //    return res;
    //}

    private static string ToCsv(Lavoratore lav, string codiceFiscaleImpresa)
    {
        if (lav.TipoLavoratore == TipologiaLavoratore.SiceNew)
        {
            return
                codiceFiscaleImpresa + "|" +
                lav.Cognome + "|" +
                lav.Nome + "|" +
                lav.DataNascita + "|" +
                lav.CodiceFiscale + "|" +
                null + "|" +
                null + "|" +
                null + "|" +
                (lav.DataAssunzione.HasValue ? lav.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
        }
        return
            codiceFiscaleImpresa + "|" +
            lav.Cognome + "|" +
            lav.Nome + "|" +
            lav.DataNascita + "|" +
            lav.CodiceFiscale + "|" +
            lav.PaeseNascita + "|" +
            lav.ProvinciaNascita + "|" +
            lav.LuogoNascita + "|" +
            (lav.DataAssunzione.HasValue ? lav.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
    }

    protected void ButtonEsportaLavoratori_Click(object sender, EventArgs e)
    {
        WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection) ViewState["domandeImprese"];

        int? idImpresa = null;
        Guid? idTemporaneo = null;
        string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
        TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

        if (tipoImpresa == TipologiaImpresa.SiceNew)
        {
            idImpresa = Int32.Parse(tipoId[1]);
        }
        else
        {
            if (String.IsNullOrEmpty(tipoId[1]))
            {
                Guid gid = new Guid(tipoId[2]);
                idTemporaneo = gid;
            }
            else
            {
                idImpresa = Int32.Parse(tipoId[1]);
            }
        }

        WhiteListImpresa domandaImpresa = null;
        if (domandeImprese != null)
        {
            WhiteListImpresa dAux = new WhiteListImpresa();
            if (domandeImprese.Count != 0)
            {
                foreach (WhiteListImpresa d in domandeImprese)
                {

                    if (tipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        if (d.Impresa.IdImpresa == idImpresa)
                        {
                            dAux = d;
                            break;
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(tipoId[1]))
                        {
                            if (d.Impresa.IdTemporaneo == idTemporaneo)
                            {
                                dAux = d;
                                break;
                            }
                        }
                        else
                        {
                            if (d.Impresa.IdImpresa == idImpresa)
                            {
                                dAux = d;
                                break;
                            }
                        }
                    }
                }

                if (dAux != null && dAux.Impresa != null)
                {
                    domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, idImpresa, idTemporaneo,
                                                                            /*domandeImprese[0].*/dAux.Impresa.CodiceFiscale);
                }
            }
        }

        //WhiteList wl = biz.GetDomandaByKey(Int32.Parse(ViewState["idDomanda"].ToString()));

        if (domandaImpresa != null)
        {
            if (domandaImpresa.Lavoratori.Count > 0)
            {
                string fileName = string.Format("{0}.{1}.csv",
                                                domandaImpresa.Impresa.CodiceFiscale, DateTime.Now.ToString("ddMMyyyy"));

                using (MemoryStream outputFile = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(outputFile))
                    {
                        foreach (Lavoratore lav in domandaImpresa.Lavoratori)
                        {
                            sw.WriteLine(ToCsv(lav, domandaImpresa.Impresa.CodiceFiscale));
                        }
                    }

                    RestituisciFile(outputFile.ToArray(), fileName);
                }
            }
            else
            {
                LabelStatoImportExport.Text = "Impossibile esportare il file: nessun lavoratore in lista";
            }
        }
        else
        {
            LabelStatoImportExport.Text = "Impossibile esportare il file: nessun lavoratore in lista";
        }
    }

    private void RestituisciFile(byte[] file, String path)
    {
        Response.AddHeader("content-disposition",
                           String.Format("attachment; filename={0}", path));
        Response.ContentType = "text/plain";
        Response.BinaryWrite(file);
        //TransmitFile invece di WriteFile perch� file grosso e transmit non carica in memoria
        Response.Flush();
        Response.End();
    }

    protected void ButtonVerificaLavoratori_Click(object sender, EventArgs e)
    {
        List<String> errori = CaricaDaLista(false);

        Presenter.CaricaElementiInListView(
            ListViewErrori,
            errori);
    }

    protected void ButtonImportaLavoratori_Click(object sender, EventArgs e)
    {
        List<String> errori = CaricaDaLista(true);

        Presenter.CaricaElementiInListView(
            ListViewErrori,
            errori);
    }

    private List<String> CaricaDaLista(Boolean importa)
    {
        List<String> erroriRes = new List<String>();

        AccessoCantieriRicercaLavoratore1.Reset();

        LabelStatoImportExport.Text = "";

        if (FileUploadCsv.HasFile)
        {
            Impresa impresa = (Impresa) ViewState["Impresa"];

            WhiteListImpresa dImp;

            if (ViewState["domandaImpresa"] == null)
            {
                dImp = new WhiteListImpresa { Lavoratori = new LavoratoreCollection(), Impresa = impresa };
            }
            else
                dImp = (WhiteListImpresa) ViewState["domandaImpresa"];

            Stream s = new MemoryStream(FileUploadCsv.FileBytes);
            //dImp.Lavoratori = ReadCSVFile(s);
            LavoratoreCollection lavoratori = null;
            try
            {
                lavoratori = ReadCsvFile(s);
            }
            catch (LetturaCSVException exc)
            {
                LabelStatoImportExport.Text = String.Format("Si � verificato un errore durante la lettura del file alla linea <b>{0}</b>: {1}", exc.Line, exc.InnerException.Message);
            }
            catch (Exception exc)
            {
                LabelStatoImportExport.Text = String.Format("Si � verificato un errore imprevisto: {0}", exc.Message);
            }

            if (lavoratori != null)
            {
                //// Recupero le date di inizio e fine attivit� dell'impresa destinazione
                //DateTime? dataInizioImpresa = null;
                //DateTime? dataFineImpresa = null;

                //SubappaltoCollection subappalti = ViewState["subappalti"] as SubappaltoCollection;
                //foreach (Subappalto sub in subappalti)
                //{
                //    if (sub.Appaltante != null)
                //    {
                //        if (sub.Appaltante.PartitaIva == dImp.Impresa.PartitaIva)
                //        {
                //            dataInizioImpresa = sub.DataInizioAttivitaAppaltatrice;
                //            dataFineImpresa = sub.DataFineAttivitaAppaltatrice;
                //            break;
                //        }
                //    }

                //    if (sub.Appaltata != null)
                //    {
                //        if (sub.Appaltata.PartitaIva == dImp.Impresa.PartitaIva)
                //        {
                //            dataInizioImpresa = sub.DataInizioAttivitaAppaltata;
                //            dataFineImpresa = sub.DataFineAttivitaAppaltata;
                //            break;
                //        }
                //    }
                //}

                //DateTime? dataInizioImpresa = GetDataInizioImpresa(dImp);
                //DateTime? dataFineImpresa = GetDataFineImpresa(dImp);

                ArrayList errori = new ArrayList();
                foreach (Lavoratore lav in lavoratori)
                {
                    errori.Clear();

                    try
                    {
                        if (lav.CodiceFiscale.Length != 16)
                        {
                            //LabelStatoImportExport.Text +=
                            errori.Add(
                                "Formato codice fiscale non corretto");
                        }

                        if (!string.IsNullOrEmpty(lav.Nome) && !string.IsNullOrEmpty(lav.Cognome) &&
                            lav.DataNascita.HasValue)
                        {
                            string maschio = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(lav.Nome,
                                                                                                       lav.Cognome, "M",
                                                                                                       lav.DataNascita.Value);
                            string femmina = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(lav.Nome,
                                                                                                       lav.Cognome, "F",
                                                                                                       lav.DataNascita.Value);

                            if (maschio.Substring(0, 6) != lav.CodiceFiscale.Substring(0, 6).ToUpper() &&
                                femmina.Substring(0, 6) != lav.CodiceFiscale.Substring(0, 6).ToUpper())
                            {
                                errori.Add("Codice fiscale non coerente");
                            }
                            else
                            {
                                String codiceMaschile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "M",
                                                                                          lav.DataNascita.Value, "A000");
                                String codiceFemminile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "F",
                                                                                                   lav.DataNascita.Value, "A000");

                                if (codiceMaschile.Substring(6, 5).ToUpper() != lav.CodiceFiscale.Substring(6, 5).ToUpper()
                                    &&
                                    codiceFemminile.Substring(6, 5).ToUpper() != lav.CodiceFiscale.Substring(6, 5).ToUpper()
                                    )
                                {
                                    errori.Add("Codice fiscale non coerente");
                                }
                            }
                        }

                        if (((lav.DataInizioAttivita.HasValue && lav.DataFineAttivita.HasValue &&
                              (lav.DataFineAttivita.Value <= lav.DataInizioAttivita.Value))))
                        {
                            errori.Add("Periodo di lavoro non corretto");
                        }

                        LavoratoreCollection lavColl;
                        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                        {

                            if (lav.TipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                lavColl = biz.GetLavoratoriOrdinati(lav.IdLavoratore, lav.Cognome, lav.Nome,
                                                                    lav.DataNascita, lav.CodiceFiscale,
                                                                    null, null, null, null,
                                                                    impresa.IdImpresa);

                                if (lavColl.Count == 0)
                                {
                                    errori.Add("Lavoratore senza un rapporto di lavoro attivo");
                                }
                            }
                        }
                        else
                        {
                            if (lav.TipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                errori.Add("Lavoratore non valido per l'impresa");
                            }
                        }

                        if (dImp.Lavoratori.ContainsCodFisc(lav))
                        {
                            errori.Add("Il lavoratore � gi� presente in lista");
                        }

                        WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection) ViewState["domandeImprese"];

                        if (!ControlloPresenzaWhitelistLista(lav, domandeImprese,dImp))
                        {
                             errori.Add("Lavoratore gi� presente per un'altra impresa in lista. Valorizzare in maniera coerente le date di inizio/fine periodo");
                        }
                    }
                    catch
                    {
                        errori.Add("Formato data non corretto");
                    }

                    if (errori.Count > 0)
                    {
                        string errorString = string.Empty;
                        foreach (string str in errori)
                        {
                            if (string.IsNullOrEmpty(errorString))
                                errorString = String.Format("  - <span style=\"color:Red\">{0}</span>", str);
                            else
                                errorString = String.Format("{0}<br /> - <span style=\"color:Red\">{1}</span>", errorString, str);
                        }

                        String testo = null;
                        if (lav.TipoLavoratore == TipologiaLavoratore.Nuovo)
                        {
                            testo = string.Format("Lavoratore: <b>{0} {1}</b><br />{2}<br />", lav.Nome,
                                                     lav.Cognome, errorString);
                        }
                        else
                        {
                            testo = string.Format("Lavoratore: {0} <b>{1} {2}</b><br />{3}<br />", lav.IdLavoratore, lav.Nome,
                                                     lav.Cognome, errorString);
                        }
                        //LabelStatoImportExport.Text += testo;

                        erroriRes.Add(testo);
                    }
                    else
                    {
                        if (importa)
                        {
                            //if (dImp.Lavoratori.ContainsCodFisc(lav))
                            //{
                            //    Int32? idDomanda;
                            //    try
                            //    {
                            //        idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
                            //    }
                            //    catch
                            //    {
                            //        idDomanda = null;
                            //    }

                            //    Int32 timbCount = 0;
                            //    if (idDomanda.HasValue)
                            //    {
                            //        timbCount = biz.GetCountTimbrature(null, lav.CodiceFiscale, idDomanda.Value);
                            //    }

                            //    if (timbCount == 0)
                            //    {
                            //        dImp.Lavoratori.RemoveCodFisc(lav);
                            //        dImp.Lavoratori.Add(lav);
                            //    }
                            //}
                            //else
                            //{
                            dImp.Lavoratori.Add(lav);
                            //}
                        }
                        else
                        {
                            erroriRes.Add(string.Format("Lavoratore: <b>{0} {1}</b><br /> - <span style=\"color:Green\">OK</span><br />", lav.Nome,
                                                     lav.Cognome));
                        }
                    }
                }

                if (importa)
                {
                    ViewState["domandaImpresa"] = dImp;
                    Int32? idDom;
                    try
                    {
                        idDom = Int32.Parse(ViewState["idDomanda"].ToString());
                    }
                    catch
                    {
                        idDom = null;
                    }
                    if (idDom.HasValue)
                        dImp.IdDomanda = idDom.Value;
                    Session["LavoratoriImpresa"] = dImp;

                    Salva();

                    CaricaDatiLavoratori();
                }
            }
        }
        else
        {
            LabelStatoImportExport.Text = "Selezionare un file cliccando sul pulsante Sfoglia";
        }

        return erroriRes;
    }

    private Lavoratore FromCsv(string csv)
    {
        if (String.IsNullOrEmpty(csv))
        {
            return null;
        }

        string[] parts = csv.Split(new[] {'|'});

        Lavoratore lav = new Lavoratore
                             {
                                 IdLavoratore = biz.GetIdLavoratore(parts[4]),
                                 TipoLavoratore = TipologiaLavoratore.Nuovo
                             };

        if (lav.IdLavoratore.HasValue)
            lav.TipoLavoratore = TipologiaLavoratore.SiceNew;

        lav.Nome = parts[2];
        lav.Cognome = parts[1];
        lav.DataNascita = DateTime.Parse(parts[3]);
        lav.CodiceFiscale = parts[4];
        
        lav.EffettuaControlli = true;

        if (lav.TipoLavoratore == TipologiaLavoratore.Nuovo)
        {
            lav.PaeseNascita = parts[5];
            lav.ProvinciaNascita = parts[6];
            lav.LuogoNascita = parts[7];
        }

        if (!string.IsNullOrEmpty(parts[8].Trim()))
            lav.DataAssunzione = DateTime.ParseExact(parts[8], "dd/MM/yyyy", null);

        if (RadDatePickerDataInizioAttivita.SelectedDate.HasValue)
        {
            lav.DataInizioAttivita = RadDatePickerDataInizioAttivita.SelectedDate.Value;
        }
        if (RadDatePickerDataFineAttivita.SelectedDate.HasValue)
        {
            lav.DataFineAttivita = RadDatePickerDataFineAttivita.SelectedDate.Value;
        }

        return lav;
    }

    private LavoratoreCollection ReadCsvFile(Stream stream)
    {
        using (StreamReader sr = new StreamReader(stream))
        {
            List<String> lines = new List<String>();

            string curLine = sr.ReadLine();

            while (curLine != null)
            {
                lines.Add(curLine);

                curLine = sr.ReadLine();
            }

            LavoratoreCollection res = new LavoratoreCollection();

            Int32 lineNumber = 1;
            try
            {
                foreach (string line in lines)
                {
                    Lavoratore lav = FromCsv(line);
                    res.Add(lav);

                    lineNumber++;
                }
            }
            catch (Exception exc)
            {
                throw new LetturaCSVException(lineNumber, exc);
            }

            return res;
        }
    }

    #region Eventi gestione inserimento ricerca lavoratore

    //protected void ButtonVisualizzaRicercaLavoratori_Click(object sender, EventArgs e)
    //{
    //    PanelRicercaInserisciLavoratore.Visible = true;
    //    AccessoCantieriRicercaLavoratore1.Visible = true;
    //    PanelInserisciLavoratore.Visible = false;
    //}

    private void AttestatoRegolaritaRicercaLavoratore1_OnNuovoLavoratoreSelected(object sender, EventArgs e)
    {
        PanelRicercaInserisciLavoratore.Visible = true;
        AccessoCantieriRicercaLavoratore1.Visible = false;
        PanelInserisciLavoratore.Visible = true;
    }

    private void LavoratoreSelezionato(Lavoratore lavoratore, Boolean edit)
    {
        ViewState["Lavoratore"] = lavoratore;

        if (ViewState["Lavoratore"] != null && ViewState["Impresa"] != null)
        {
            Impresa impresa = (Impresa) ViewState["Impresa"];

            WhiteListImpresa dImp;

            if (ViewState["domandaImpresa"] == null)
            {
                dImp = new WhiteListImpresa {Lavoratori = new LavoratoreCollection(), Impresa = impresa};
                ViewState["domandaImpresa"] = dImp;
                Int32? idDomanda;
                try
                {
                    idDomanda = Int32.Parse(ViewState["idDomanda"].ToString());
                }
                catch
                {
                    idDomanda = null;
                }
                if (idDomanda.HasValue)
                    dImp.IdDomanda = idDomanda.Value;
                Session["LavoratoriImpresa"] = dImp;
            }

            dImp = (WhiteListImpresa) ViewState["domandaImpresa"];

            WhiteListImpresaCollection domandeImprese = (WhiteListImpresaCollection)ViewState["domandeImprese"];
            WhiteListImpresa dom = ControlloPresenzaWhitelist(lavoratore, domandeImprese, dImp);

            bool doppione = false;

            if (dom != null)
            {
               
                doppione = dom.Impresa.RagioneSociale != dImp.Impresa.RagioneSociale;

                if (dom.Impresa.RagioneSociale != dImp.Impresa.RagioneSociale)
                {
                    LabelGiaPresente.Text = "Lavoratore gi� presente per un'altra impresa in lista (" +
                                                              dom.Impresa.CodiceERagioneSociale +
                                                              "). Le date di inizio e fine attivit� devono essere sempre valorizzate e devono essere coerenti.Modificare le date di inizio e fine attivit� del lavoratore precededentemente inserito in lista per l'impresa " +
                                                              dom.Impresa.CodiceERagioneSociale +
                                                              " e successivamente procedere con il nuovo inserimento per l'impresa " +
                                                              dImp.Impresa.CodiceERagioneSociale + ".";
                    LabelGiaPresente.Visible = true;
                }
                else
                {
                    if (!edit)
                    {
                        dImp.Lavoratori.Add(lavoratore);
                    }
                    else
                    {
                        Int32 editIndex = (Int32)ViewState["editIndex"];
                        dImp.Lavoratori[editIndex] = lavoratore;
                    }
                    LabelGiaPresente.Text = "";
                    LabelGiaPresente.Visible = false;
                }
            }

            if (!doppione)
            {
                LabelGiaPresente.Text = "Lavoratore gi� presente per questa impresa.";
                if (lavoratore.TipoLavoratore == TipologiaLavoratore.Nuovo)
                {
                    if (!dImp.Lavoratori.ContainsCodFisc(lavoratore))
                    {
                        LabelGiaPresente.Text = "";
                        LabelGiaPresente.Visible = false;

                        dImp.Lavoratori.Add(lavoratore);
                    }
                    else
                    {
                        if (edit)
                        {
                            Int32 editIndex = (Int32) ViewState["editIndex"];
                            dImp.Lavoratori[editIndex] = lavoratore;
                        }
                        else
                        {
                            LabelGiaPresente.Visible = true;
                        }
                    }
                }
                else
                {
                    if (!dImp.Lavoratori.Contains(lavoratore))
                    {
                        dImp.Lavoratori.Add(lavoratore);
                        LabelGiaPresente.Text = "";
                        LabelGiaPresente.Visible = false;
                    }
                    else
                    {
                        if (!edit)
                        {
                            LabelGiaPresente.Text = "Lavoratore gi� presente";
                            LabelGiaPresente.Visible = true;
                        }
                        else
                        {
                            LabelGiaPresente.Text = null;

                            Int32 editIndex = (Int32) ViewState["editIndex"];
                            dImp.Lavoratori[editIndex] = lavoratore;
                        }
                    }
                }
            }

            dImp.Impresa = impresa;
            ViewState["domandaImpresa"] = dImp;
            Int32? idDom;
            try
            {
                idDom = Int32.Parse(ViewState["idDomanda"].ToString());
            }
            catch
            {
                idDom = null;
            }
            if (idDom.HasValue)
                dImp.IdDomanda = idDom.Value;
            Session["LavoratoriImpresa"] = dImp;

            Salva();

            CaricaDatiLavoratori();
            PanelAggiungiLavoratore.Visible = false;

            ResetCampi();
        }
        else
        {
            LabelResInserimentoLavInLista.Text =
                "E' necessario selezionare sia il lavoratore che l'impresa a cui ha dichiarato di appartenere";
        }
    }

    private DateTime? GetDataInizioImpresa(WhiteListImpresa domanda)
    {
        DateTime? dataInizioImpresa = null;
        DateTime? dataFineImpresa = null;

        SubappaltoCollection subappalti = ViewState["subappalti"] as SubappaltoCollection;
        foreach (Subappalto sub in subappalti)
        {
            if (sub.Appaltante != null)
            {
                if (sub.Appaltante.PartitaIva == domanda.Impresa.PartitaIva)
                {
                    dataInizioImpresa = sub.DataInizioAttivitaAppaltatrice;
                    dataFineImpresa = sub.DataFineAttivitaAppaltatrice;
                    break;
                }
            }

            if (sub.Appaltata != null)
            {
                if (sub.Appaltata.PartitaIva == domanda.Impresa.PartitaIva)
                {
                    dataInizioImpresa = sub.DataInizioAttivitaAppaltata;
                    dataFineImpresa = sub.DataFineAttivitaAppaltata;
                    break;
                }
            }
        }
        return dataInizioImpresa;
    }

    private DateTime? GetDataFineImpresa(WhiteListImpresa domanda)
    {
        DateTime? dataInizioImpresa = null;
        DateTime? dataFineImpresa = null;

        SubappaltoCollection subappalti = ViewState["subappalti"] as SubappaltoCollection;
        foreach (Subappalto sub in subappalti)
        {
            if (sub.Appaltante != null)
            {
                if (sub.Appaltante.PartitaIva == domanda.Impresa.PartitaIva)
                {
                    dataInizioImpresa = sub.DataInizioAttivitaAppaltatrice;
                    dataFineImpresa = sub.DataFineAttivitaAppaltatrice;
                    break;
                }
            }

            if (sub.Appaltata != null)
            {
                if (sub.Appaltata.PartitaIva == domanda.Impresa.PartitaIva)
                {
                    dataInizioImpresa = sub.DataInizioAttivitaAppaltata;
                    dataFineImpresa = sub.DataFineAttivitaAppaltata;
                    break;
                }
            }
        }
        return dataFineImpresa;
    }

    private WhiteListImpresa ControlloPresenzaWhitelist(Lavoratore lavoratore, WhiteListImpresaCollection domandeImprese, WhiteListImpresa domanda)
    {
        foreach (WhiteListImpresa dom in domandeImprese)
        {
            if (dom.Impresa.RagioneSociale != domanda.Impresa.RagioneSociale)
            {
                if (dom.Lavoratori.ContainsCodFisc(lavoratore))
                {
                    LavoratoreCollection lavColl = dom.Lavoratori.GetByCodFisc(lavoratore);

                    foreach (Lavoratore labor in lavColl)
                    {
               
                        if (!labor.DataInizioAttivita.HasValue || !labor.DataFineAttivita.HasValue ||
                            !lavoratore.DataInizioAttivita.HasValue || !lavoratore.DataFineAttivita.HasValue)
                        {
                            return dom;
                        }

                        if ((labor.DataInizioAttivita <= lavoratore.DataFineAttivita &&
                             labor.DataFineAttivita >= lavoratore.DataInizioAttivita) ||
                            (lavoratore.DataInizioAttivita <= labor.DataFineAttivita &&
                             lavoratore.DataFineAttivita >= labor.DataInizioAttivita))
                        {
                            return dom;
                        }
                    }
                }
            }
            else
            {
                return null;
            }
        }

        return domanda;
    }

    private Boolean ControlloPresenzaWhitelistLista(Lavoratore lavoratore, WhiteListImpresaCollection domandeImprese, WhiteListImpresa domanda)
    {
        DateTime? dataInizioImpresa = GetDataInizioImpresa(domanda);
        DateTime? dataFineImpresa = GetDataFineImpresa(domanda);

        DateTime? dataInizioAttivita = lavoratore.DataInizioAttivita.HasValue ? lavoratore.DataInizioAttivita : dataInizioImpresa;
        DateTime? dataFineAttivita = lavoratore.DataFineAttivita.HasValue ? lavoratore.DataFineAttivita : dataFineImpresa;

        foreach (WhiteListImpresa dom in domandeImprese)
        {
            if (dom.Impresa.RagioneSociale != domanda.Impresa.RagioneSociale)
            {
                if (dom.Lavoratori.ContainsCodFisc(lavoratore))
                {
                    LavoratoreCollection lavColl = dom.Lavoratori.GetByCodFisc(lavoratore);

                    DateTime? dataInizioImpresaSrc = GetDataInizioImpresa(dom);
                    DateTime? dataFineImpresaSrc = GetDataFineImpresa(dom);

                    foreach (Lavoratore labor in lavColl)
                    {

                        DateTime? dataInizioAttivitaSrc = labor.DataInizioAttivita.HasValue ? labor.DataInizioAttivita : dataInizioImpresaSrc;
                        DateTime? dataFineAttivitaSrc = labor.DataFineAttivita.HasValue ? labor.DataFineAttivita : dataFineImpresaSrc;

                        if (!dataInizioAttivitaSrc.HasValue || !dataFineAttivitaSrc.HasValue ||
                            !dataInizioAttivita.HasValue || !dataFineAttivita.HasValue)
                        {
                            return false;
                           
                        }

                        if (dataInizioAttivitaSrc.HasValue && dataFineAttivitaSrc.HasValue &&
                            dataInizioAttivita.HasValue && dataFineAttivita.HasValue)
                        {
                            if ((dataInizioAttivitaSrc <= dataFineAttivita &&
                                 dataFineAttivitaSrc >= dataInizioAttivita) ||
                                (dataInizioAttivita <= dataFineAttivitaSrc &&
                                 dataFineAttivita >= dataInizioAttivitaSrc))
                            {
                                return false;
                            
                            }
                        }
                    }
                }
            }
        }

        return true;
    }



    //protected void ButtonInserisciNuovoLavoratore_Click(object sender, EventArgs e)
    //{

    //}

    protected void ButtonAnnullaInserimentoLavoratore_Click(object sender, EventArgs e)
    {
        PanelAggiungiLavoratore.Visible = false;
        PanelRicercaInserisciLavoratore.Visible = false;
        AccessoCantieriRicercaLavoratore1.Visible = false;
        PanelInserisciLavoratore.Visible = false;

        AccessoCantieriRicercaLavoratore1.Reset();
    }

    public WhiteListImpresaCollection GetDomandeImprese()
    {
        return (WhiteListImpresaCollection) ViewState["domandeImprese"];
    }

    #endregion

    //public LavoratoreCollection ReadCsvFile(string fileName)
    //{
    //    using (StreamReader sr = new StreamReader(fileName))
    //    {
    //        List<String> lines = new List<String>();

    //        string curLine = sr.ReadLine();

    //        while (curLine != null)
    //        {
    //            lines.Add(curLine);

    //            curLine = sr.ReadLine();
    //        }

    //        LavoratoreCollection res = new LavoratoreCollection();

    //        foreach (string line in lines)
    //        {
    //            Lavoratore lav = FromCsv(line);

    //            res.Add(lav);
    //        }

    //        return res;
    //    }
    //}

    //public bool ThumbnailCallback()
    //{
    //    return false;
    //}

    protected void ButtonInserisciNuovoLavoratore_Click(object sender, EventArgs e)
    {
        AccessoCantieriRicercaLavoratore1.Reset();
    }

    protected void ButtonImportaListaLavoratori_Click(object sender, EventArgs e)
    {
        //AccessoCantieriRicercaLavoratore1.Reset();
        PanelAggiungiLavoratore.Visible = false;
        PanelLavoratoriDaFile.Visible = true;
    }

    protected void ButtonAnnullaInserimentoLista_Click(object sender, EventArgs e)
    {
        ResetCaricaLista();
    }

    protected void CaricaLavoratoriInForza(Int32 idImpresa, Int32 pagina)
    {
        // Carico i lavoratori in forza
        LavoratoreCollection lavInForza = biz.GetLavoratoriInForza(idImpresa);

        LabelLavoratoriForza.Text = lavInForza.Count.ToString();

        Presenter.CaricaElementiInGridView(
            GridViewLavoratoriInForza,
            lavInForza,
            pagina);
        PanelLavoratoriInForza.Visible = true;
    }

    protected void GridViewLavoratoriInForza_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Lavoratore lavoratore = (Lavoratore) e.Row.DataItem;

            CheckBox cbSelezione = (CheckBox) e.Row.FindControl("CheckBoxSeleziona");

            WhiteListImpresa impresa = (WhiteListImpresa) ViewState["domandaImpresa"];
            if (impresa != null)
            {
                if (impresa.Lavoratori.Contains(new Lavoratore() { IdLavoratore = lavoratore.IdLavoratore, TipoLavoratore = TipologiaLavoratore.SiceNew }))
                {
                    cbSelezione.Enabled = false;
                    cbSelezione.Visible = false;
                }
            }
        }
    }

    protected void GridViewLavoratoriInForza_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Impresa impresa = (Impresa) ViewState["Impresa"];
        CaricaLavoratoriInForza(impresa.IdImpresa.Value, e.NewPageIndex);
    }

    protected void ButtonAggiungiForza_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in GridViewLavoratoriInForza.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox cbSel = (CheckBox) row.FindControl("CheckBoxSeleziona");

                if (cbSel.Checked)
                {
                    Lavoratore lav = new Lavoratore()
                    {
                        TipoLavoratore = TipologiaLavoratore.SiceNew,
                        IdLavoratore = (Int32) GridViewLavoratoriInForza.DataKeys[row.RowIndex].Values["IdLavoratore"],
                        Cognome = GridViewLavoratoriInForza.DataKeys[row.RowIndex].Values["Cognome"] as String,
                        Nome = GridViewLavoratoriInForza.DataKeys[row.RowIndex].Values["Nome"] as String,
                        CodiceFiscale = GridViewLavoratoriInForza.DataKeys[row.RowIndex].Values["CodiceFiscale"] as String,
                        DataNascita = GridViewLavoratoriInForza.DataKeys[row.RowIndex].Values["DataNascita"] as DateTime?,
                        EffettuaControlli = true
                    };
                    LavoratoreSelezionato(lav, false);
                }
            }
        }

        Impresa impresa = (Impresa) ViewState["Impresa"];
        CaricaLavoratoriInForza(impresa.IdImpresa.Value, GridViewLavoratoriInForza.PageIndex);
    }

    protected void ButtonMostraNascondi_Click(object sender, EventArgs e)
    {
        PanelForza.Visible = !PanelForza.Visible;
        ButtonMostraNascondi.Text = PanelForza.Visible ? "Nascondi" : "Mostra";
    }
}