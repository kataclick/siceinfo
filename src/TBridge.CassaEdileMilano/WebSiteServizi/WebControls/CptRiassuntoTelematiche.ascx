﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CptRiassuntoTelematiche.ascx.cs"
    Inherits="WebControls_CptRiassuntoTelematiche" %>
<%@ Register src="../Cantieri/WebControls/CantiereDaNotifica.ascx" tagname="CantiereDaNotifica" tagprefix="uc1" %>
<style type="text/css">
    .styleIntestazione
    {
        width: 250px;
    }
    .colonnaTabellaIntestazioneBottone
    {
        text-align: right;
    }
</style>
<div class="tabellaRiassuntiva">
    <div class="borderedDiv">
        <uc1:CantiereDaNotifica ID="CantiereDaNotifica1" runat="server" />
    </div>
    <br />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Dati generali </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiDatiGenerali" runat="server" Text="" Width="150px"
                    OnClick="ButtonMostraNascondiDatiGenerali_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divDatiGenerali" runat="server">
        <table class="standardTable">
            <tr id="trProtocollo" runat="server">
                <td class="styleIntestazione">
                    Protocollo:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelProtocollo" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Data:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDatiGeneraliData" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Natura dell'opera:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDatiGeneraliNaturaOpera" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Tipo opera:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDatiGeneraliTipoOpera" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Note:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDatiGeneraliNote" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    N° appalto:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDatiGeneraliNumeroAppalto" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzi:
                </td>
                <td>
                    <asp:ListView ID="ListViewDatiGeneraliIndirizzi" runat="server">
                        <LayoutTemplate>
                            <table class="standardTable">
                                <asp:Panel ID="itemPlaceholder" runat="server" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <b>
                                        <%# ((TBridge.Cemi.Cpt.Type.Entities.Indirizzo)Container.DataItem).IndirizzoCompleto %>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <%# ((TBridge.Cemi.Cpt.Type.Entities.Indirizzo)Container.DataItem).IndirizzoDatiAggiuntivi%>
                                    </small>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Committente </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiCommittente" runat="server" Text="" Width="150px"
                    OnClick="ButtonMostraNascondiCommittente_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divCommittente" runat="server">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <b>Persona</b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cognome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCommittentePersonaCognome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Nome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCommittentePersonaNome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cellulare:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaCellulare" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Email:
                </td>
                <td>
                    <asp:Label ID="LabelCommittentePersonaEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Ente</b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Ragione Sociale:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCommittenteEnteRagioneSociale" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Partita IVA:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEntePartitaIVA" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice Fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelCommittenteEnteFax" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <asp:Panel
        ID="PanelPersone"
        runat="server"
        Visible="false">
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Responsabile dei lavori </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiResponsabileDeiLavori" runat="server" Text=""
                    Width="150px" OnClick="ButtonMostraNascondiResponsabileDeiLavori_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divResponsabileLavori" runat="server">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxNonNominato" runat="server" Enabled="false" Text="Non nominato" />
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cognome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelResponsabileLavoriPersonaCognome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Nome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelResponsabileLavoriPersonaNome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cellulare:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaCellulare" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Email:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriPersonaEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Ente</b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Ragione Sociale:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelResponsabileLavoriEnteRagioneSociale" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Partita IVA:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEntePartitaIva" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice Fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileLavoriEnteFax" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Coordinatore alla sicurezza durante la progettazione </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiCoordinatoreSicurezzaProgettazione" runat="server"
                    Text="" Width="150px" OnClick="ButtonMostraNascondiCoordinatoreSicurezzaProgettazione_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divCoordinatoreSicurezzaProgettazione" runat="server">
        <table class="standardTable">
            <tr>
                <td class="styleIntestazione">
                    Cognome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCoordinatoreProgettazionePersonaCognome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Nome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCoordinatoreProgettazionePersonaNome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cellulare:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaCellulare" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Email:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazionePersonaEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Ente</b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Ragione Sociale:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCoordinatoreProgettazioneEnteRagioneSociale" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Partita IVA:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEntePartitaIva" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice Fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreProgettazioneEnteFax" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Coordinatore alla sicurezza durante l'esecuzione </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiCoordinatoreSicurezzaEsecuzione" runat="server"
                    Text="" Width="150px" OnClick="ButtonMostraNascondiCoordinatoreSicurezzaEsecuzione_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divCoordinatoreSicurezzaEsecuzione" runat="server">
        <table class="standardTable">
            <tr>
                <td class="styleIntestazione">
                    Cognome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCoordinatoreEsecuzionePersonaCognome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Nome:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCoordinatoreEsecuzionePersonaNome" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cellulare:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaCellulare" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Email:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzionePersonaEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Ente</b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Ragione Sociale:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCoordinatoreEsecuzioneEnteRagioneSociale" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Partita IVA:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEntePartitaIva" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Codice Fiscale:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Città:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteCitta" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Telefono:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Fax:
                </td>
                <td>
                    <asp:Label ID="LabelCoordinatoreEsecuzioneEnteFax" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </asp:Panel>
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Date e numeri </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiDateNumeri" runat="server" Text="" Width="150px"
                    OnClick="ButtonMostraNascondiDateNumeri_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divDateNumeri" runat="server">
        <table class="standardTable">
            <tr>
                <td class="styleIntestazione">
                    Ammontare complessivo presunto dei lavori:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriAmmontareComplessivo" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Data presunta inizio lavori:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriDataInizioLavori" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Durata presunta dei lavori (giorni):
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriDurataLavori" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    Data presunta fine lavori:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriDataFineLavori" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    N° massimo presunto di lavoratori sul cantiere:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriNumeroLavoratori" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    N° previsto di imprese sul cantiere:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriNumeroImprese" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    N° previsto di lavoratori autonomi sul cantiere:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriNumeroAutonomi" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td class="styleIntestazione">
                    N° totale uomini / giorno:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelDateNumeriNumeroUominiGiorno" runat="server"></asp:Label></b>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Imprese affidatarie </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiImpreseAffidatarie" runat="server" Text="" Width="150px"
                    OnClick="ButtonMostraNascondiImpreseAffidatarie_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divImpreseAffidatarie" runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:GridView ID="GridViewImpreseAffidatarie" runat="server" Width="100%" AutoGenerateColumns="False"
                        OnRowDataBound="GridViewImpreseAffidatarie_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td style="width: 100%;">
                                                <b>
                                                    <asp:Label ID="LabelRagioneSociale" runat="server">
                                                    </asp:Label>
                                                </b>
                                                <br />
                                                <asp:Label ID="LabelPartitaIva" runat="server">
                                                </asp:Label>
                                                <br />
                                                <asp:Label ID="LabelCodiceFiscale" runat="server">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna impresa nella lista.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <hr />
    <table class="standardTable">
        <tr>
            <td class="colonnaTabellaIntestazioneTesto">
                <b>Imprese esecutrici </b>
            </td>
            <td class="colonnaTabellaIntestazioneBottone">
                <asp:Button ID="ButtonMostraNascondiImpreseEsecutrici" runat="server" Text="" Width="150px"
                    OnClick="ButtonMostraNascondiImpreseEsecutrici_Click" />
            </td>
        </tr>
    </table>
    <br />
    <div id="divImpreseEsecutrici" runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:GridView ID="GridViewImpreseEsecutrici" runat="server" Width="100%" AutoGenerateColumns="False"
                        OnRowDataBound="GridViewImpreseEsecutrici_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td style="width: 100%;">
                                                <b>
                                                    <asp:Label ID="LabelImpresaRagioneSociale" runat="server"></asp:Label>
                                                </b>
                                                <br />
                                                <asp:Label ID="LabelImpresaPartitaIva" runat="server"></asp:Label>
                                                <br />
                                                <asp:Label ID="LabelImpresaCodiceFiscale" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <b>
                                                    <asp:Label ID="LabelSubappaltoDi" runat="server" Text="Subappalto di:" Visible="false"></asp:Label></b>
                                                <asp:Label ID="LabelSubappaltoDiRagioneSociale" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna impresa nella lista.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    <br />
</div>
