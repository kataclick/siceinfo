﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriRilevatoriInserimento.ascx.cs"
    Inherits="WebControls_AccessoCantieriRilevatoriInserimento" %>
<table class="standardTable">
    <tr>
        <td>
            ID terminale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxIDTerminale" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxIDTerminale"
                ErrorMessage="Digitare un ID per il terminale" ValidationGroup="rilevatore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Latitudine*
        </td>
        <td>
            <asp:TextBox ID="TextBoxLatitudine" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxLatitudine"
                ErrorMessage="Digitare un valore per la latitudine" ValidationGroup="rilevatore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Longitudine*
        </td>
        <td>
            <asp:TextBox ID="TextBoxLongitudine" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxLongitudine"
                ErrorMessage="Digitare una valore per la longitudine" ValidationGroup="rilevatore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                ValidationGroup="rilevatore" />
        </td>
    </tr>
</table>
* campi obbligatori