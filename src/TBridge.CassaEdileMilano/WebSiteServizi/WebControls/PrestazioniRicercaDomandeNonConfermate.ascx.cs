using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Delegates;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Filters;

public partial class WebControls_PrestazioniRicercaDomandeNonConfermate : UserControl
{
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    public event DomandaTemporaneaSelectedEventHandler OnDomandaTemporaneaSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaDomande();
        }
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewDomande.PageIndex = 0;
            CaricaDomande();
        }
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricerca");

        if (Page.IsValid)
        {
            GridViewDomande.PageIndex = e.NewPageIndex;
            CaricaDomande();
        }
    }

    public void ImpostaIdLavoratore(int idLavoratore)
    {
        ViewState["IdLavoratore"] = idLavoratore;
    }

    private void CaricaDomande()
    {
        DomandaFilter filtro = new DomandaFilter();

        filtro.IdLavoratore = (int) ViewState["IdLavoratore"];

        DomandaCollection domande = biz.GetDomandeTemporanee(filtro);
        GridViewDomande.DataSource = domande;
        GridViewDomande.DataBind();
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Domanda domanda = (Domanda) e.Row.DataItem;

            Label lBeneficiario = (Label) e.Row.FindControl("LabelBeneficiario");

            if (domanda.Beneficiario != "L")
            {
                if (domanda.Familiare != null)
                {
                    lBeneficiario.Text = domanda.Familiare.NomeCompleto;
                }
                else
                {
                    if (domanda.FamiliareFornito != null)
                    {
                        lBeneficiario.Text = domanda.FamiliareFornito.NomeCompleto;
                    }
                }
            }
            else
            {
                lBeneficiario.Text = "LAVORATORE";
            }
        }
    }

    protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];

        if (OnDomandaTemporaneaSelected != null)
            OnDomandaTemporaneaSelected(idDomanda);
    }

    protected void GridViewDomande_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idDomanda = (int) GridViewDomande.DataKeys[e.RowIndex].Values["IdDomanda"];

        if (biz.DeleteDomandaTemporanea(idDomanda))
        {
            CaricaDomande();
        }
    }
}