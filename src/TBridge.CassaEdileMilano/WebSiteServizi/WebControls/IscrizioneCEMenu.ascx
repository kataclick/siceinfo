<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneCEMenu.ascx.cs" Inherits="WebControls_IscrizioneCEMenu" %>
<%
if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneCEIscrizione.ToString())
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneCEGestioneDomande.ToString()))
   {                  
%>
<table class="menuTable">
    <tr class="menuTable" id="RigaTitolo" runat="server">
        <td >Iscrizione CE</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneCEIscrizione.ToString()))
        {                  
         %>
    <tr>
        <td>
            <a id="A1" href="~/IscrizioneCE/Requisiti.aspx" runat="server">Iscrizione</a>
        </td>
    </tr>
    <%
        }
     %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente() 
            && TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneCEIscrizione.ToString())
            )
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/IscrizioneCE/GestioneDomandeConsulente.aspx?modalita=daConfermare" runat="server">Domande da confermare</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/IscrizioneCE/GestioneDomandeConsulente.aspx?modalita=confermate" runat="server">Domande confermate</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.IscrizioneCEGestioneDomande.ToString()))
        {                  
    %>
    <tr>
        <td>
            <a id="A4" href="~/IscrizioneCE/GestioneDomandeCassaEdile.aspx" runat="server">Gestione domande</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/ReportImpreseIscritte.aspx" runat="server">Imprese iscritte</a>
        </td>
    </tr>
    <%
        }
     %>
</table>
<%
    }
 %>