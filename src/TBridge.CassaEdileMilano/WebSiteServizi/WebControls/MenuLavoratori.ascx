<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuLavoratori.ascx.cs" Inherits="WebControls_MenuLavoratori" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>

<table class="menuTable">
    <tr class="menuTable">
        <td>Estratto conto</td>
    </tr>
     <%
         if (GestioneUtentiBiz.IsLavoratore())
         {%>
    <tr>
        <td>
        <a href="HomeLavoratore.aspx">Anagrafica</a>
        </td>
    </tr>
    <tr>
        <td><a id="A1" href="ReportLavoratori.aspx?tipo=rapporto">Rapporti di lavoro</a>
        </td>
    </tr>
    <tr>
        <td><a id="A2" href="ReportLavoratori.aspx?tipo=ore">Ore accantonate</a>
        </td>
    </tr>
    <tr>
        <td><a id="A3" href="ReportLavoratori.aspx?tipo=prestazioni">Prestazioni erogate</a>
        </td>
    </tr>
     <%
         }
%>
</table>