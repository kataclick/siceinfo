<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CantieriRicercaImpresa.ascx.cs"
    Inherits="WebControls_CantieriRicercaImpresa" %>
<asp:Panel ID="PanelRicercaImprese" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Imprese"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonSiceNew" runat="server" Text="Anagrafica SiceNew"
                                Checked="True" GroupName="scelta" />
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonCantieri" runat="server" Text="Anagrafica Cantieri"
                                GroupName="scelta" />
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ragione sociale
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxRagioneSociale"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Indirizzo
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxIndirizzo"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Comune
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxComune"
                                ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Partita Iva / Codice fiscale
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIvaFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cod.
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxCodice"
                                ErrorMessage="numerico" ValidationExpression="^\d{1,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Width="100%"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonNuovo" runat="server" Font-Size="X-Small" OnClick="ButtonNuovo_Click"
                                Text="Nuovo" CausesValidation="False" />
                            <asp:Button ID="ButtonVisualizza" runat="server" Font-Size="X-Small" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaImprese" />
                            <asp:Button ID="ButtonChiudi" runat="server" Font-Size="X-Small" OnClick="ButtonChiudi_Click"
                                Text="X" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewImprese" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyNames="IdImpresa,TipoImpresa" OnPageIndexChanging="GridViewImprese_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewImprese_SelectedIndexChanging" OnSorting="GridViewImprese_Sorting"
                    Width="100%" OnRowDataBound="GridViewImprese_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="IdImpresa" HeaderText="Cod." Visible="False" />
                        <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" SortExpression="RagioneSociale" />
                        <asp:TemplateField HeaderText="Sedi">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelLegale" runat="server">Leg.:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelSedeLegale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelAmministrativa" runat="server">Amm.:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelSedeAmministrativa" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Indirizzo" HeaderText="Indirizzo" SortExpression="Indirizzo"
                            Visible="False" />
                        <asp:BoundField DataField="Provincia" HeaderText="Provincia" SortExpression="Provincia"
                            Visible="False" />
                        <asp:BoundField DataField="Comune" HeaderText="Comune" SortExpression="Comune" Visible="False" />
                        <asp:BoundField DataField="Cap" HeaderText="CAP" Visible="False" />
                        <asp:BoundField DataField="PartitaIva" HeaderText="Partita Iva" SortExpression="PartitaIva" />
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" SortExpression="CodiceFiscale" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna impresa trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
