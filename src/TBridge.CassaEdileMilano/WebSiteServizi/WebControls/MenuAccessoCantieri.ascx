<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuAccessoCantieri.ascx.cs"
    Inherits="WebControls_MenuAccessoCantieri" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerImpresa)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriControlli)
        || (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerCommittente) && GestioneUtentiBiz.IsCommittente())
        )
    {%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Accesso ai Cantieri
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/AccessoCantieri/GestioneCantiere.aspx" runat="server">Inserimento
                cantiere</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere)
            || (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerCommittente) && GestioneUtentiBiz.IsCommittente()))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/AccessoCantieri/RiepilogoCantieri.aspx" runat="server">Riepilogo
                cantieri</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A11" href="~/AccessoCantieri/RicercaLavoratore.aspx" runat="server">Ricerca lavoratore</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/AccessoCantieri/GestioneTimbrature.aspx" runat="server">Gestione
                timbrature </a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A23" href="~/AccessoCantieri/Presenze.aspx" runat="server">Presenze</a>
        </td>
    </tr>
    <%-- <tr>
        <td>
            <a id="A5" href="~/AccessoCantieri/InserimentoTimbrature.aspx" runat="server">Inserimento timbrature</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A6" href="~/AccessoCantieri/ProvaImportazioneTimbrature.aspx" runat="server">Prova Importazione timbrature</a>
        </td>
    </tr>--%>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriControlli))
        {
    %>
    <tr>
        <td>
            <a id="A6" href="~/AccessoCantieri/ControlliCexChange.aspx" runat="server">Controlli
            </a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerImpresa))
        {
    %>
    <tr>
        <td>
            <a id="A7" href="~/AccessoCantieri/AccessoCantieriReportTimbratureImpresa.aspx" runat="server">
                Situazione timbrature</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A10" href="~/AccessoCantieri/ControlliCEMIImprese.aspx" runat="server">
                Situazione controlli</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/AccessoCantieri/GestioneLettori.aspx" runat="server">Gestione rilevatori
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A9" href="~/AccessoCantieri/GestioneCommittenti.aspx" runat="server">Gestione
                committenti </a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/AccessoCantieri/Statistiche.aspx" runat="server">Statistiche</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A8" href="~/AccessoCantieri/StampaBadge.aspx" runat="server">Stampa Badge</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
