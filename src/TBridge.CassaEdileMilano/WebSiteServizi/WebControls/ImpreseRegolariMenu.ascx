<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImpreseRegolariMenu.ascx.cs" Inherits="WebControls_ImpreseRegolariMenu" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>

<%
    if (true
        /*TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariModificaLista.ToString())
        || TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariGestisciAnomalie.ToString())
        || TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariSegnalaAnomalie.ToString())
        || TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariImpostaParametri.ToString())
         */
        )
    {%>
<table class="menuTable" >
    <tr id="RigaTitolo" class="menuTable" runat="server" >
        <td >Imprese Adempienti</td>
    </tr>
    <tr>
        <td>
            <a href="ImpreseAdempientiReport.aspx">Visualizza lista imprese adempienti</a>
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariModificaLista.ToString()))
        {%>
     <tr>
        <td>
            <a href="ImpreseRegolariForzaInLista.aspx">Modifica lista</a>
        </td>
    </tr>
    <%
        }
%>

    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariImpostaParametri.ToString()))
        {%>
    <tr>
        <td>
            <a href="configura.aspx">Configura i parametri</a>
        </td>
    </tr>
    <%
        }
%>
     
     <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariGestisciAnomalie.ToString()))
        {%>
    <tr>
        <td><a href="Anomalie.aspx">Visualizza anomalie</a>
        </td>
    </tr>
    <%
        }
%>
     <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariSegnalaAnomalie.ToString()))
        {%>
     <tr>
        <td>
            <a href="ImpreseRegolariSegnalaAnomalie.aspx">Segnala anomalia</a>
        </td>
    </tr>
    <%
        }
%>
 
</table>
<%
    }
%>