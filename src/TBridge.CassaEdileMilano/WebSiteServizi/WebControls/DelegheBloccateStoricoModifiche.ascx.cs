﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Deleghe.Business;
using TBridge.Cemi.Deleghe.Type.Entities;

public partial class WebControls_DelegheBloccateStoricoModifiche : UserControl
{
    private readonly DelegheBusiness biz = new DelegheBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        //
    }

    public void CaricaStoricoDelega(int idDelega)
    {
        List<StoricoDelegaBloccata> delegheBloccate = biz.GetStoricoDelegaBloccata(idDelega);
        GridViewStoricoDelegaBloccata.DataSource = delegheBloccate;
        GridViewStoricoDelegaBloccata.DataBind();
    }
}