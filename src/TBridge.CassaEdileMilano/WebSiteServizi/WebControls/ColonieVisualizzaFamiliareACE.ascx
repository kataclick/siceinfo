<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColonieVisualizzaFamiliareACE.ascx.cs"
    Inherits="WebControls_ColonieVisualizzaFamiliareACE" %>
<asp:Label ID="Label2" runat="server" Font-Bold="True">Dettagli familiare</asp:Label><br />
<table class="standardTable">
    <tr>
        <td>
            Cognome
        </td>
        <td>
            <asp:Label ID="LabelCognome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Nome
        </td>
        <td>
            <asp:Label ID="LabelNome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita
        </td>
        <td>
            <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale
        </td>
        <td>
            <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Sesso
        </td>
        <td>
            <asp:Label ID="LabelSesso" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Intolleranze alimentari
        </td>
        <td>
            <asp:Label ID="LabelIntolleranze" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Portatore handicap
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxPortatoreHandicap" runat="server" Enabled="False" /><br />
            <asp:Panel ID="PanelAccompagnatore" runat="server" Width="400px">
                <asp:Label ID="LabelHandicap" runat="server"></asp:Label><br />
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreCognome" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nome
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreNome" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data di nascita
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreDataNascita" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sesso
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreSesso" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
