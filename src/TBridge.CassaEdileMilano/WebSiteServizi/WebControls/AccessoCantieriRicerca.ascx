﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriRicerca.ascx.cs"
    Inherits="WebControls_AccessoCantieriRicerca" %>
<asp:Panel ID="PanelRicercaImprese" runat="server" DefaultButton="ButtonVisualizza">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Indirizzo cantiere
                        </td>
                        <td>
                            Comune cantiere
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" 
                                OnClick="ButtonVisualizza_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="RadGridCantieri" runat="server" AllowPaging="True" PageSize="5"
                    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" OnItemCommand="RadGridCantieri_ItemCommand"
                    OnItemDataBound="RadGridCantieri_ItemDataBound" 
                    OnPageIndexChanged="RadGridCantieri_PageIndexChanged" Visible="False">
                    <MasterTableView DataKeyNames="idWhiteList,indirizzo,comune,provincia">
                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter Cantiere column" UniqueName="Cantiere">
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                Indirizzo:
                                            </td>
                                            <td>
                                                <b>
                                                    <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Comune:
                                            </td>
                                            <td>
                                                <b>
                                                    <asp:Label ID="LabelComune" runat="server"></asp:Label>
                                                </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Provincia:
                                            </td>
                                            <td>
                                                <b>
                                                    <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
                                                </b>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <table class="standardTable" id="tableModifica" runat="server">
                                        <tr>
                                            <td align="center">
                                                <asp:ImageButton
                                                    ID="ImageButtonModifica"
                                                    runat="server"
                                                    CommandName="MODIFICA"
                                                    ImageUrl="~/images/edit.png"
                                                    ToolTip="Modifica" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <small>
                                                    Modifica
                                                </small>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td align="center">
                                                <asp:ImageButton
                                                    ID="ImageButtonVisualizza"
                                                    runat="server"
                                                    CommandName="VISUALIZZA"
                                                    ImageUrl="~/images/lente.png"
                                                    ToolTip="Visualizza" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <small>
                                                    Visualizza
                                                </small>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td align="center">
                                                <asp:ImageButton
                                                    ID="ImageButtonStampa"
                                                    runat="server"
                                                    CommandName="STAMPA"
                                                    ImageUrl="~/images/printer.png"
                                                    ToolTip="Stampa" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <small>
                                                    Stampa
                                                </small>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle Width="10px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                    </HeaderContextMenu>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Panel>
