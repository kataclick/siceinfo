<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TuteScarpeMenuStatistiche.ascx.cs" Inherits="WebControls_TuteScarpeMenuStatistiche" %>
 <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeStatistiche.ToString())
           )
        {                  
         %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Statistiche</td>
    </tr>  
    <tr>
        <td>
            <a id="a1" href="~/TuteScarpe/TuteScarpeStatisticheRiassunto.aspx" runat="server">Riassunto</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a2" href="~/TuteScarpe/TuteScarpeStatisticheFabbisogniProposti.aspx" runat="server">Fabbisogni proposti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a3" href="~/TuteScarpe/TuteScarpeStatisticheFabbisogniConfermati.aspx" runat="server">Fabbisogni confermati</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a4" href="~/TuteScarpe/TuteScarpeStatisticheOrdiniEffettuati.aspx" runat="server">Ordini predisposti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a5" href="~/TuteScarpe/TuteScarpeStatisticheOrdiniEvasi.aspx" runat="server">Ordini evasi</a>
        </td>
    </tr>
     <tr>
        <td>
            <a id="a6" href="~/TuteScarpe/TuteScarpeStatisticheOrdiniInevasi.aspx" runat="server">Ordini inevasi</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a7" href="~/TuteScarpe/TuteScarpeStatisticheOrdiniConsegnati.aspx" runat="server">Ordini consegnati</a>
        </td>
    </tr>
     <tr>
        <td>
            <a id="a8" href="~/TuteScarpe/TuteScarpeStatisticheOrdiniNonConsegnati.aspx" runat="server">Ordini non consegnati</a>
        </td>
    </tr>
</table>
<%
   } 
    
    %>