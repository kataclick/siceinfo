using System;
using System.Web.UI;
using TBridge.Cemi.Prestazioni.Type.Entities;

public partial class WebControls_PrestazioniDatiLavoratore : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaLavoratore(Lavoratore lavoratore)
    {
        LabelCodice.Text = lavoratore.IdLavoratore.ToString();
        LabelCognome.Text = lavoratore.Cognome;
        LabelNome.Text = lavoratore.Nome;
    }

    private void Reset()
    {
        LabelCodice.Text = null;
        LabelCognome.Text = null;
        LabelNome.Text = null;
    }
}