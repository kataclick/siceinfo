﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaCantiere.ascx.cs"
    Inherits="WebControls_AttestatoRegolaritaCantiere" %>
<table class="standardTable">
    <tr>
        <td>
            <table class="filledtable">
                <tr>
                    <td>
                        Periodo di osservazione
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td class="accessoCantieriTd">
                        Mese/anno domanda (mm/aaaa):
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxMeseAnno" runat="server" MaxLength="7" Width="200px"></asp:TextBox>
                        <asp:Label ID="LabelMeseAnnoOriginale" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxMeseAnno"
                            ErrorMessage="Mese del controllo mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxMeseAnno"
                            ErrorMessage="Formato del mese/anno della domanda errato" ValidationExpression="(0[1-9]|1[012])[- /.](19|20)[0-9]{2}"
                            ValidationGroup="stop">*</asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="CustomValidatorMeseTraDataInizioEFineLavori" runat="server"
                            ValidationGroup="stop" ErrorMessage="Il mese della domanda deve essere compreso tra la data di inizio lavori e la data di fine lavori"
                            OnServerValidate="CustomValidatorMeseTraDataInizioEFineLavori_ServerValidate">*</asp:CustomValidator>
                        <asp:CustomValidator ID="CustomValidatorMeseCopia" runat="server" ValidationGroup="stop"
                            ErrorMessage="Non può essere indicato lo stesso mese della domanda che si vuole copiare"
                            OnServerValidate="CustomValidatorMeseCopia_ServerValidate">*</asp:CustomValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <table class="filledtable">
                <tr>
                    <td>
                        Cantiere
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td class="accessoCantieriTd">
                        Data inizio lavori (gg/mm/aaaa):
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDataInizioLavori" runat="server" MaxLength="10" Width="200px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxDataInizioLavori"
                            ErrorMessage="Data inizio lavori mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidatorDataInizio" runat="server" ControlToValidate="TextBoxDataInizioLavori"
                            ErrorMessage="Formato data inizio lavori errato" MaximumValue="06/06/2079" MinimumValue="01/01/1900"
                            ValidationGroup="stop" Type="Date">*</asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Data fine lavori (gg/mm/aaaa):
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDataFineLavori" runat="server" MaxLength="10" Width="200px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBoxDataFineLavori"
                            ErrorMessage="Data fine lavori mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="TextBoxDataFineLavori"
                            ErrorMessage="Formato data fine lavori errato" Operator="DataTypeCheck" Type="Date"
                            ValidationGroup="stop">*</asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="TextBoxDataFineLavori"
                            ControlToValidate="TextBoxDataInizioLavori" ErrorMessage="La data di inizio lavori non può essere superiore a quella di fine lavori"
                            Operator="LessThan" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Importo dei lavori (ammontare presunto dell&#39;appalto):
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxImporto" runat="server" Width="200px" MaxLength="19"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxImporto"
                            ErrorMessage="Importo dei lavori mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Immettere un importo lavori valido"
                            ControlToValidate="TextBoxImporto" Type="Currency" ValidationGroup="stop" Operator="DataTypeCheck">*</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Descrizione lavori (opere appaltate) nel cantiere:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDescrizione" runat="server" Height="60px" Width="200px" TextMode="MultiLine"
                            MaxLength="255"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxDescrizione"
                            ErrorMessage="Descrizione lavori mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Percentuale incidenza manodopera (%):
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxManodopera" runat="server" Width="200px" MaxLength="3"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Immettere una valore percentuale valido"
                            MaximumValue="100" MinimumValue="0" ValidationGroup="stop" ControlToValidate="TextBoxManodopera"
                            Type="Integer">*</asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero massimo lavoratori previsti:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxNumeroLavoratori" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="TextBoxNumeroLavoratori"
                            ErrorMessage="Numero presunto lavoratori operanti nel cantiere mancanti" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Immettere un numero presunto lavoratori operanti nel cantiere valido"
                            MaximumValue="10000" MinimumValue="1" ValidationGroup="stop" ControlToValidate="TextBoxNumeroLavoratori"
                            Type="Integer">*</asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero totale uomini giorno:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxNumeroGiorni" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="TextBoxNumeroGiorni"
                            ErrorMessage="Numero giorni presunti dei lavoratori mancanti" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="Immettere un numero giorni presunti dei lavoratori valido"
                            MaximumValue="1000000" MinimumValue="1" ValidationGroup="stop" ControlToValidate="TextBoxNumeroGiorni"
                            Type="Integer">*</asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero D.I.A.:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDIA" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CustomValidator ID="CustomValidatorDIAAutoConc" runat="server" ErrorMessage="Inserire numero  DIA o numero concessione o numero Autorizzazione"
                            OnServerValidate="CustomValidatorDIAAutoConc_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero concessione:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxConcessione" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero autorizzazione:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxAutorizzazione" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <table class="filledtable">
                <tr>
                    <td>
                        Committente
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table class="borderedTable">
                        <tr>
                            <td class="accessoCantieriTd">
                                Ragione sociale / denominazione:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCommittenteRagioneSociale" runat="server" Width="200px" MaxLength="255"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="TextBoxCommittenteRagioneSociale"
                                    ErrorMessage="Ragione sociale del committente mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Partita IVA:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCommittentePIVA" runat="server" Width="200px" MaxLength="11"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPIVA" runat="server"
                                    ControlToValidate="TextBoxCommittentePIVA" ErrorMessage="Formato della partita IVA del committente errato"
                                    ValidationExpression="^\d{11}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="CustomValidatorCodFiscPIVACommittente" runat="server" ErrorMessage="Inserire partita IVA o codice fiscale del committente"
                                    OnServerValidate="CustomValidatorCodFiscPIVACommittente_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                                <asp:CustomValidator ID="CustomValidatorPartitaIva" runat="server" ErrorMessage="Partita IVA del committente non valida"
                                    ValidationGroup="stop" OnServerValidate="CustomValidatorPartitaIva_ServerValidate">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Codice fiscale:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCommittenteCodFisc" runat="server" Width="200px" MaxLength="16"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCommittenteCodFisc"
                                    ErrorMessage="Formato codice fiscale committente errato" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                                    ValidationGroup="stop">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tipologia:
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListCommittenteTipologia" runat="server" Width="200px"
                                    AppendDataBoundItems="True">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="DropDownListCommittenteTipologia"
                                    ErrorMessage="Selezionare la tipologia di committente" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Indirizzo:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCommittenteIndirizzo" runat="server" Width="200px" Height="50px"
                                    MaxLength="245" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="TextBoxCommittenteIndirizzo"
                                    ErrorMessage="Indirizzo del committente mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Civico:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCommittenteCivico" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Provincia:
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListCommittenteProvincia" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True" Height="17px" OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged"
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="DropDownListCommittenteProvincia"
                                    ErrorMessage="Selezionare la provincia del committente" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Comune
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListCommittenteComuni" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True" Height="16px" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="DropDownListCommittenteComuni"
                                    ErrorMessage="Selezionare il comune del committente" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cap
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListCommittenteCAP" runat="server" AppendDataBoundItems="True"
                                    Height="17px" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="DropDownListCommittenteCAP"
                                    ErrorMessage="Selezionare il cap del committente" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Indirizzo:
                    </td>
                    <td colspan="3">
                        <asp:Label ID="LabelNessunIndirizzo" runat="server" ForeColor="Red">Per inserire 
                            un indirizzo immettere i dati nei campi e cliccare sul pulsante &quot;Ricerca 
                            geocodifiche&quot;</asp:Label>
                        <asp:Label ID="LabelIndirizzo" runat="server" Font-Bold="True" Font-Names="Verdana"
                            Text="LabelIndirizzo" Visible="False"></asp:Label>
                        <br />
                        <asp:Label ID="LabelCAPComuneProvincia" runat="server" Font-Bold="True" Font-Names="Verdana"
                            Text="LabelCAPComuneProvincia" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonEliminaSelezione" runat="server" Text="Cancella indirizzo"
                Height="25px" Enabled="False" Width="200px" OnClick="ButtonEliminaSelezione_Click"
                Visible="False" />
            <br />
            &nbsp;<asp:Button ID="ButtonNuovoIndirizzo" runat="server" Text="Aggiungi indirizzo"
                Height="25px" Width="200px" OnClick="ButtonNuovoIndirizzo_Click" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelNuovoIndirizzo" runat="server" Visible="false">
                <asp:MultiView ID="MultiViewIndirizzo" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewIndirizzoInserito" runat="server">
                        <asp:Panel ID="PanelIndirizzoInserito" DefaultButton="ButtonGeocodifica" runat="server">
                            <table class="filledtable">
                                <tr>
                                    <td>
                                        Appalto del cantiere
                                    </td>
                                </tr>
                            </table>
                            <table class="borderedTable">
                                <tr>
                                    <td class="accessoCantieriTd">
                                        Indirizzo
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="200px" Height="50px" MaxLength="245"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere un indirizzo"
                                            ControlToValidate="TextBoxIndirizzo" ValidationGroup="inserimentoIndirizzo">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Civico
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBoxCivico" runat="server" Width="200px" MaxLength="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Info agg.
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBoxInfoAggiuntiva" runat="server" Width="200px" MaxLength="100"
                                            Height="50px" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comune
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBoxComune" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Provincia
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBoxProvincia" runat="server" Width="200px" MaxLength="25"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cap
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxCap" runat="server" Width="200px" MaxLength="5"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorCap" runat="server"
                                            ValidationGroup="inserimentoIndirizzo" ErrorMessage="Formato del Cap errato"
                                            ValidationExpression="^\d{5}$" ControlToValidate="TextBoxCap">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Latitudine
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBoxLatitudine" runat="server" Width="200px" Enabled="false"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Longitudine
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBoxLongitudine" runat="server" Width="200px" Enabled="false"
                                            CssClass="campoDisabilitato"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="ButtonGeocodifica" runat="server" Text="Ricerca geocodifiche" OnClick="ButtonGeocodifica_Click"
                                            ValidationGroup="inserimentoIndirizzo" Width="200px" />
                                        <asp:Button ID="ButtonAzzeraCampi" runat="server" OnClick="ButtonAzzeraCampi_Click"
                                            Text="Azzera campi" Width="200px" Visible="False" />
                                        <asp:ValidationSummary ID="ValidationSummaryIndirizzo" runat="server" ValidationGroup="inserimentoIndirizzo"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="ViewIndirizzoProposto" runat="server">
                        <table class="standardTable">
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="LabelSceltaIndirizzo" runat="server" Text="Se l'indirizzo inserito è presente nella tabella sottostante cliccare su &quot;Seleziona&quot;. Se nessuno degli indirizzi proposti corrisponde cliccare su &quot;Conferma indirizzo dichiarato&quot;"></asp:Label>
                                    .
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Indirizzo dichiarato:
                                </td>
                                <td>
                                    <asp:Label ID="LabelIndirizzoFornito" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        OnSelectedIndexChanging="GridViewIndirizzi_SelectedIndexChanging" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="Provincia">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Comune">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IndirizzoDenominazione">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cap">
                                                <ItemStyle Font-Size="XX-Small" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Button ID="ButtonSeleziona" runat="server" CommandName="Select" Font-Size="XX-Small"
                                                        Height="18px" Text="Seleziona"  />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Non è stato trovato un indirizzo corrispondente nell&#39;anagrafica degli indirizzi.
                                            Se l&#39;indirizzo digitato è corretto cliccare su &quot;Conferma indirizzo dichiarato&quot;.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="ButtonAggiungiIndirizzo" runat="server" OnClick="ButtonAggiungiIndirizzo_Click"
                                        Text="Conferma indirizzo dichiarato" Width="200px" ValidationGroup="inserimentoIndirizzo" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </td>
    </tr>
</table>
