using System;
using System.Web.UI;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Type.Entities;

public partial class WebControls_PrestazioniDatiNuovoLavoratore : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public Lavoratore GetLavoratore()
    {
        Lavoratore lavoratore = null;

        Page.Validate("datiLavoratore");
        if (Page.IsValid)
        {
            lavoratore = new Lavoratore();

            if (ViewState["IdLavoratore"] != null)
            {
                lavoratore.IdLavoratore = (Int32) ViewState["IdLavoratore"];
            }

            lavoratore.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
            lavoratore.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
            lavoratore.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
            lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
        }

        return lavoratore;
    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxDataNascita);
    }
}