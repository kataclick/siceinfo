<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCantieri.ascx.cs"
    Inherits="WebControls_MenuCantieri" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriProgrammazioneGestione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrev)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrevRUI)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriLettere)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestioneGruppi)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriSegnalazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriAssegnazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriEstrazioneLodi)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Cantieri</td>
    </tr>
    <%
        //if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestioneZone.ToString())
        //   )
        //{                  
    %>
    <%--<tr>
        <td>
            <a id="A1" href="~/Cantieri/CantieriGestioneZone.aspx" runat="server">Gestione zone</a>
        </td>
    </tr>--%>
    <%
        //}
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestioneGruppi)
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A10" href="~/Cantieri/GestioneGruppi.aspx" runat="server">Gestione gruppi</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestione)
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A2" href="~/Cantieri/CantieriGestioneCantieri.aspx" runat="server">Gestione cantieri</a>
        </td>
    </tr>
    <% } %>
    <%--<% 
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione.ToString())
           || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriProgrammazioneGestione.ToString())
          )
      {                  
    %>
    <tr>
        <td>
            <a id="A3" href="~/Cantieri/CantieriProgrammazioneSettimanale.aspx" runat="server">Programmazione
                settimanale</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrev.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString())
           )
      {                  
    %>
    <tr>
        <td>
            <a id="A4" href="~/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=preventivo"
                runat="server">Preventivo settimanale</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=consuntivo"
                runat="server">Consuntivo settimanale</a>
        </td>
    </tr>
    <% } %>--%>
    <%
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestione)
           )
      {                  
    %>
    <tr>
        <td>
            <a id="A6" href="~/Cantieri/CantieriLocalizzazioneMultipla.aspx" runat="server">Localizzazione</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrevRUI)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrev)
          || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriLettere)
       )
      {                  
    %>
    <tr>
        <td>
            <a id="A7" href="~/Cantieri/CantieriRicercaIspezioni.aspx" runat="server">Ricerca ispezioni</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriModificaIspettori)
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A8" href="~/Cantieri/CantieriModificaIspettore.aspx" runat="server">Modifica ispettori</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriLettere)
            )
        {    %>
    <tr>
        <td>
            <a id="A9" href="~/Cantieri/CantieriLogLettere.aspx" runat="server">Log Lettere</a>
        </td>
    </tr>
    <%
        } 
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsIspettore())
        {    %>
    <tr>
        <td>
            <a id="A11" href="~/Cantieri/CalendarioAttivita.aspx" runat="server">Calendario attivit�</a>
        </td>
    </tr>
    <%
        } if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestioneAttivita))
        {    %>
    <tr>
        <td>
            <a id="A12" href="~/Cantieri/ElencoAttivita.aspx" runat="server">Elenco attivit�</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A13" href="~/Cantieri/NumeriAttivita.aspx" runat="server">Stato attivit�</a>
        </td>
    </tr>
    <%
        } 
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriEstrazioneLodi))
        {    %>
    <tr>
        <td>
            <a id="A14" href="~/Cantieri/EstrazioneLodi.aspx" runat="server">Estrazione Lodi</a>
        </td>
    </tr>
    <%
        } 
    %>
</table>
<%
    }
%>
