<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuCigoTelematica.ascx.cs"
    Inherits="WebControls_MenuCigoTelematica" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CigoTelematicaGestioneImpresa.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CigoTelematicaGestioneBackOffice.ToString())
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CigoTelematicaEstrattoConto.ToString())
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            CIGO
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CigoTelematicaGestioneImpresa))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CigoTelematica/DomandeRicerca.aspx" runat="server">Gestione Domande CIGO</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CigoTelematicaGestioneBackOffice))
        {
    %>
    <tr>
        <td>
            <a id="A6" href="~/CigoTelematica/DomandeRicercaBackOffice.aspx" runat="server">
                Gestione Domande CIGO</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CigoTelematicaEstrattoConto))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/CigoTelematica/EstrattoConto.aspx" runat="server">Estratto conto</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
