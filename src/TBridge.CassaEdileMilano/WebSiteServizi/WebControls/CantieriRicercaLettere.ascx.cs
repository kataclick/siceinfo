using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cantieri.Type.Filters;

public partial class WebControls_CantieriRicercaLettere : UserControl
{
    private readonly CantieriBusiness cantieriBiz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipiLettere();
        }
    }

    private void CaricaTipiLettere()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipoLettera,
            Enum.GetNames(typeof(GruppoLettera)),
            String.Empty,
            String.Empty);
    }

    protected void GridViewLettere_RowEditing(object sender, GridViewEditEventArgs e)
    {
        string protocollo = (string) GridViewLettere.DataKeys[e.NewEditIndex]["Protocollo"];

        byte[] lettera = cantieriBiz.CaricaLettera(protocollo);

        Response.ClearContent();
        Response.AppendHeader("content-disposition", string.Format("attachment; filename={0}.docx", protocollo));
        Response.ContentType = "application/vnd.ms-word";
        Response.BinaryWrite(lettera);
        Response.Flush();
        Response.End();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaLettere();
        }
    }

    private void CaricaLettere()
    {
        LettereFilter filtro = CreaFiltro();

        List<LogLettera> lettere = cantieriBiz.CaricaLogLettere(filtro);

        Presenter.CaricaElementiInGridView(
            GridViewLettere,
            lettere,
            0);
    }

    private LettereFilter CreaFiltro()
    {
        LettereFilter filtro = new LettereFilter();

        if (!String.IsNullOrEmpty(TextBoxDal.Text))
        {
            filtro.Dal = DateTime.Parse(TextBoxDal.Text);
        }
        if (!String.IsNullOrEmpty(TextBoxAl.Text))
        {
            filtro.Al = DateTime.Parse(TextBoxAl.Text);
        }
        if (!String.IsNullOrEmpty(DropDownListTipoLettera.Text))
        {
            filtro.TipoLettera = (GruppoLettera)Enum.Parse(typeof(GruppoLettera), DropDownListTipoLettera.Text);
        }
        filtro.IndirizzoCantiere = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        filtro.ComuneCantiere = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        filtro.Protocollo = Presenter.NormalizzaCampoTesto(TextBoxProtocollo.Text);
        if (!String.IsNullOrEmpty(TextBoxIdImpresa.Text))
        {
            filtro.IdImpresa = Int32.Parse(TextBoxIdImpresa.Text);
        }
        filtro.RagioneSocialeImpresa = Presenter.NormalizzaCampoTesto(TextBoxRagioneSocialeImpresa.Text);
        filtro.IvaFiscImpresa = Presenter.NormalizzaCampoTesto(TextBoxFiscIvaImpresa.Text);

        return filtro;
    }

    protected void GridViewLettere_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaLettere();

        GridViewLettere.PageIndex = e.NewPageIndex;
        GridViewLettere.DataBind();
    }

    protected void GridViewLettere_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LogLettera lettera = (LogLettera) e.Row.DataItem;
            Label lCantiere = (Label) e.Row.FindControl("LabelCantiere");

            if (!string.IsNullOrEmpty(lettera.CantiereProvincia))
                lCantiere.Text =
                    String.Format("{0} {1} ({2})", lettera.CantiereIndirizzo, lettera.CantiereComune,
                                  lettera.CantiereProvincia);
            else
                lCantiere.Text = String.Format("{0} {1}", lettera.CantiereIndirizzo, lettera.CantiereComune);
        }
    }

    protected void GridViewLettere_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Context.Items["protocollo"] = GridViewLettere.DataKeys[e.RowIndex]["Protocollo"];
        Server.Transfer("~/Cantieri/ConfermaCancellazioneLettera.aspx");
    }
}