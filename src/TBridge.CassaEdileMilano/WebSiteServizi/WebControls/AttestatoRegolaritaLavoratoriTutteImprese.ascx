﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttestatoRegolaritaLavoratoriTutteImprese.ascx.cs" Inherits="WebControls_AttestatoRegolaritaLavoratoriTutteImprese" %>
<asp:GridView ID="GridViewImpreseLavoratori" runat="server" Width="100%" 
    AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="Impresa" 
    onpageindexchanging="GridViewImpreseLavoratori_PageIndexChanging" 
    onrowdatabound="GridViewImpreseLavoratori_RowDataBound" PageSize="5">
    <Columns>
        <asp:BoundField DataField="Impresa" HeaderText="Impresa">
        <ItemStyle Width="150px" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="Lavoratori">
            <ItemTemplate>
                <asp:GridView ID="GridViewLavoratori" runat="server" 
                    AutoGenerateColumns="False" Width="100%" 
                    onpageindexchanging="GridViewLavoratori_PageIndexChanging" PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" 
                            HeaderText="Data di nascita" HtmlEncode="False">
                        <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Cod. fisc.">
                        <ItemStyle Width="200px" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun lavoratore presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        Nessun dato estratto
    </EmptyDataTemplate>
</asp:GridView>