<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegistrazioneSindacalista.ascx.cs"
    Inherits="WebControls_RegistrazioneSindacalista" %>
Tutti i campi sono obbligatori<br />
<br />
<table width="600">
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Nome:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxNome"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Cognome:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxCognome"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelSindacato" runat="server" Font-Bold="True" Text="Sindacato:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSindacato" runat="server" AppendDataBoundItems="True">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListSindacato"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelComprensorio" runat="server" Font-Bold="True" Text="Comprensorio:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListComprensorio" runat="server" AppendDataBoundItems="True">
            </asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelEmail" runat="server" Font-Bold="True" Text="Email:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxEmail" runat="server">
            </asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato email non valido"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="TextBoxEmail"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelUsername" runat="server" Font-Bold="True" Text="Username:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxLogin" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxLogin"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPassword" runat="server" Font-Bold="True" Text="Password:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxPassword"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPasswordRidigitata" runat="server" Font-Bold="True" Text="Ridigita password:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPasswordRidigitata" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxPasswordRidigitata"
                ErrorMessage="*"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPassword"
                ControlToValidate="TextBoxPasswordRidigitata" ErrorMessage="Password diversa"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
            dal username e deve contenere almeno una lettera e un numero.<br />
            Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
            pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
            ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
            dal sistema.
        </td>
    </tr>
    <tr>
        <td colspan="3" height="5">
        </td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Button ID="ButtonRegistraSindacalista" runat="server" Text="Registra sindacalista"
                OnClick="ButtonRegistraSindacalista_Click" />
            <asp:Button ID="ButtonIndietro" runat="server" CausesValidation="False" OnClick="ButtonIndietro_Click"
                Text="Indietro" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Label ID="LabelResult" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>
<br />
