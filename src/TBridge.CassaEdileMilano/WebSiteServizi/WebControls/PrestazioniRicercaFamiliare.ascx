<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniRicercaFamiliare.ascx.cs" Inherits="WebControls_PrestazioniRicercaFamiliare" %>
<asp:GridView ID="GridViewFamiliari" runat="server" AutoGenerateColumns="False" DataKeyNames="IdLavoratore,IdFamiliare"
    OnSelectedIndexChanging="GridViewFamiliari_SelectedIndexChanging"
    Width="100%" onrowdatabound="GridViewFamiliari_RowDataBound">
    <Columns>
        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
        <asp:BoundField DataField="Nome" HeaderText="Nome" />
        <asp:BoundField DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" 
            HeaderText="Data di nascita" >
        <ItemStyle Width="80px" />
        </asp:BoundField>
        <asp:BoundField DataField="CodiceFiscale" HeaderText="Cod.fisc." >
        <ItemStyle Width="100px" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="A Carico">
            <ItemTemplate>
                <asp:Label ID="LabelACarico" runat="server"></asp:Label>
            </ItemTemplate>
            <ItemStyle Width="50px" />
        </asp:TemplateField>
        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona" ShowSelectButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

            <ItemStyle Width="10px" />
        </asp:CommandField>
    </Columns>
    <EmptyDataTemplate>
        Nessun familiare presente nell'anagrafica
    </EmptyDataTemplate>
</asp:GridView>

