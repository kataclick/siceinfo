﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccessoCantieriTimbratureImpresa.ascx.cs"
    Inherits="WebControls_AccessoCantieriRilevatori" %>
<%@ Register Src="AccessoCantieriRilevatoriInserimento.ascx" TagName="AccessoCantieriRilevatoriInserimento"
    TagPrefix="uc7" %>
<table class="standardTable">
    <tr>
        <td>
            <asp:Panel ID="PanelRilevatori" runat="server">
                <asp:Label ID="LabelCantiere0" runat="server" Font-Bold="True" Text="Elenco timbrature per il cantiere"
                    Visible="True"></asp:Label>
                <br />
                <br />
                <asp:Panel ID="PanelFiltroTimb" runat="server" Visible="True">
                    <table class="standardTable">
                        <tr>
                            <td>
                                Cantiere<b>*</b>
                            </td>
                            <td>
                                Impresa<b>*</b>
                            </td>
                            <td>
                                Data inizio (gg/mm/aaaa)
                            </td>
                            <td>
                                Data fine (gg/mm/aaaa)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownListCantiere" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True" Width="150px" 
                                    onselectedindexchanged="DropDownListCantiere_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidatorCantiere"
                                    runat="server"
                                    ControlToValidate="DropDownListCantiere"
                                    ValidationGroup="ricercaTimbrature"
                                    ForeColor="Red"
                                    ErrorMessage="Selezionare un cantiere">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListImprese" runat="server" AppendDataBoundItems="True"
                                    Width="150px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidatorImpresa"
                                    runat="server"
                                    ControlToValidate="DropDownListImprese"
                                    ValidationGroup="ricercaTimbrature"
                                    ForeColor="Red"
                                    ErrorMessage="Selezionare un'impresa">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxDataInizio" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxDataFine" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Codice fiscale
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:Button ID="ButtonVisualizzaTimb" runat="server" OnClick="ButtonVisualizzaTimb_Click"
                                    Text="Ricerca" ValidationGroup="ricercaTimbrature" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:ValidationSummary
                                    ID="ValidationSummaryRicerca"
                                    runat="server"
                                    CssClass="messaggiErrore" 
                                    ValidationGroup="ricercaTimbrature" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                </asp:Panel>
                <asp:GridView ID="GridViewTimbrature" runat="server" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="IdTimbratura" OnRowDataBound="GridViewTimbrature_RowDataBound"
                    AllowSorting="True"
                    AllowPaging="True" OnPageIndexChanging="GridViewTimbrature_PageIndexChanging"
                    PageSize="15">
                    <Columns>
                        <asp:TemplateField HeaderText="Rilevatore" Visible="True">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Codice:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCodiceRilevatore" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fornitore:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelFornitore" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Posizione GPS" Visible="False">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Lat.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelLatitudine" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Long.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelLongitudine" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="codiceFiscale" HeaderText="Codice fiscale" Visible="True" />
                        <asp:BoundField DataField="dataOra" HeaderText="Data e ora" />
                        <asp:TemplateField HeaderText="Ingresso/Uscita">
                            <ItemTemplate>
                                <asp:Label ID="LabelIngressoUscita" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Cantiere">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Indirizzo:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelIndirizzoCantiere" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Comune:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelComuneCantiere" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelProvinciaCantiere" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Persona">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Nome:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelNomeCognome" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cod. Fisc.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ruolo:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Codice CE:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCodiceCE" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Anomalie">
                            <ItemTemplate>
                                <asp:Label ID="LabelAnomalia" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna timbratura presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAggiungiRilevatore" runat="server" Visible="True">
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Button ID="ButtonStampaTutto" runat="server" Text="Stampa lista" Visible="False"
                             Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonStampa" runat="server" Text="Stampa anomalie" Visible="False"
                            Width="150px" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonStampaSelezione" runat="server" Text="Stampa selezione" OnClick="ButtonStampaSelezione_Click"
                            Visible="False" Width="150px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
