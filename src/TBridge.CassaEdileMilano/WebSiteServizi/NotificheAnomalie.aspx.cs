using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Notifiche.Business;
using TBridge.Cemi.Notifiche.Type.Collections;

public partial class NotificheAnomalie : Page
{
    private NotificaBusiness biz;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.NNotificheAnomalie, "NotificheAnomalie.aspx");

        biz = new NotificaBusiness();

        if (Page.IsPostBack)
        {
            Ricerca();
        }
    }

    protected void ButtonCerca_Click(object sender, EventArgs e)
    {
        Ricerca();

        LabelTitoloElenco.Visible = true;
    }

    private void Ricerca()
    {
        NotificaCollection listaNotifiche = null;

        if (RadioButtonNonDenunciati.Checked)
            listaNotifiche = biz.NotificheNonDenunciate();
        if (RadioButtonCessatiDenunciati.Checked)
            listaNotifiche = biz.NotificheRapportiCessatiDenunciati();

        GridViewLavoratori.DataSource = listaNotifiche;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewLavoratori.PageIndex = e.NewPageIndex;
        GridViewLavoratori.DataBind();
    }
}