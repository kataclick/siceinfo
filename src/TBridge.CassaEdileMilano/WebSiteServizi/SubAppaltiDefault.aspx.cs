using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class SubAppalti : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.SubappaltiRicerca);
        funzionalita.Add(FunzionalitaPredefinite.SubappaltiStorico);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "Subappalti.aspx");
    }
}