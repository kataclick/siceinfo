﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestionePartecipantiCorsi.aspx.cs" Inherits="Corsi_GestionePartecipantiCorsi"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CorsiRicercaPianificazioni.ascx" TagName="CorsiRicercaPianificazioni"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/CorsiRicercaPartecipanti.ascx" TagName="CorsiRicercaPartecipanti"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/CorsiDatiLavoratore.ascx" TagName="CorsiDatiLavoratore"
    TagPrefix="uc5" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Gestione partecipanti" />
    <br />
    <uc3:CorsiRicercaPianificazioni ID="CorsiRicercaPianificazioni1" runat="server" />
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelPartecipanti" runat="server" Visible="false" CssClass="borderedTable">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <b>Partecipanti al corso </b>
                </td>
            </tr>
            <tr>
                <td>
                    Corso:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelCorso" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    Modulo:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelModulo" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    Schedulazione:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelSchedulazione" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    Locazione:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelLocazione" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    Posti occupati:
                </td>
                <td>
                    <b>
                        <asp:Label ID="LabelPostiOccupati" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    Presenze confermate:
                </td>
                <td>
                    <asp:Image ID="ImagePresenzeConfermate" runat="server" />
                    &nbsp;<asp:Button ID="ButtonConfermaPresenze" runat="server" Text="Conferma" OnClick="ButtonConfermaPresenze_Click"
                        Width="100px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc4:CorsiRicercaPartecipanti ID="CorsiRicercaPartecipanti1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="ButtonEstraiPartecipanti" runat="server" Text="Genera registro presenze"
                        Width="200px" OnClick="ButtonEstraiPartecipanti_Click" />
                    &nbsp;
                    <asp:Button ID="ButtonTicketPartecipanti" runat="server" Text="Genera registro consegna ticket"
                        Width="200px" OnClick="ButtonTicketPartecipanti_Click" />
                    &nbsp;
                    <asp:Button ID="ButtonVisualizzaCertificati" runat="server" Text="Visualizza Attestati"
                        Width="200px" OnClick="ButtonVisualizzaCertificati_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
