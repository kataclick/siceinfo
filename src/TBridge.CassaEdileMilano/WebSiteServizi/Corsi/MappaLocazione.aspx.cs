using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Corsi_MappaLocazione : Page
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CorsiGestione);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneImpresa);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneConsulente);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        if (!Page.IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idLocazione"]))
            {
                Int32 idLocazione = Int32.Parse(Request.QueryString["idLocazione"]);
                StringBuilder stringBuilder = new StringBuilder();
                CaricaLocazione(idLocazione, stringBuilder);
                RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
            }
        }
    }

    private void CaricaLocazione(Int32 idLocazione, StringBuilder stringBuilder)
    {
        Locazione locazione = biz.GetLocazioneByKey(idLocazione);

        if (locazione.Latitudine.HasValue && locazione.Longitudine.HasValue)
        {
            var descrizione = new StringBuilder();

            // Indirizzo del cantiere
            descrizione.Append(locazione.Indirizzo);
            descrizione.Append("<br />");
            descrizione.Append(locazione.Comune);
            descrizione.Append(" ");
            if (!string.IsNullOrEmpty(locazione.Provincia))
            {
                descrizione.Append(" (");
                descrizione.Append(locazione.Provincia);
                descrizione.Append(")");
            }

            // Informazioni aggiuntive
            descrizione.Append("<br />");
            descrizione.Append("<br />");
            descrizione.Append(locazione.Note);

            stringBuilder.Append(String.Format("LoadMap('{0}','{1}');", locazione.Latitudine.ToString().Replace(',', '.'),
                                               locazione.Longitudine.ToString().Replace(',', '.')));

            stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", 0,
                                               locazione.Latitudine.ToString().Replace(',', '.'),
                                               locazione.Longitudine.ToString().Replace(',', '.'), descrizione,
                                               null));
        }
    }
}