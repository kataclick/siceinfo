﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;
using LavoratoreIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;
using TBridge.Cemi.Business;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Lavoratore = TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using Impresa = TBridge.Cemi.Corsi.Type.Entities.Impresa;

public partial class Corsi_CorsiEsemAvvisoPagamento : Page
{

    private readonly CorsiEsemBusiness corsiEsemBiz = new CorsiEsemBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["costo"] != null)
            {
                Decimal costo = Decimal.Parse(Request.QueryString["costo"]);
                LabelCorsoPagamento.Text = "<b>ATTENZIONE!</b><br />Il corso per il quale si sta effettuando l'iscrizione comporterà un costo di €" + costo.ToString() + ".<br />Procedere con l'iscrizione?";
            }
            else
            {
                LabelCorsoPagamento.Text = "<b>ATTENZIONE!</b><br />Selezionare un corso prima di procedere.";
                BtnCorsoPagamentoNo.Visible = false;
                BtnCorsoPagamentoYes.Visible = false;
                BtnCorsoPagamentoOk.Visible = true;
            }
        }
    }

    protected void BtnCorsoPagamentoYes_Click(object sender, EventArgs e)
    {
        Iscrizione iscrizione = (Iscrizione)ViewState["iscrizione"];
        Utente utente = (Utente)ViewState["utente"];
        String messaggio;

        if (corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione))
        {
            ViewState["messaggio"] = null;
            //LabelMessaggio.Visible = true;
        }
        else
        {
            ViewState["messaggio"] = messaggio;
            //LabelMessaggio.Visible = true;
        }
        
        body1.Attributes.Add("onload", "CloseAndRebind();");
    }
}