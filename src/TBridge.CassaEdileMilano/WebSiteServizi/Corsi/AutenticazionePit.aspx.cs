﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.Business.Pit;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Filters;

public partial class Corsi_AutenticazionePit : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //
    }

    protected void ButtonLogin_Click(object sender, EventArgs e)
    {
        string username = TextBoxLogin.Text;
        string password = TextBoxPassword.Text;

        bool autorizzatoCemi = Membership.ValidateUser(username, password);
        if (autorizzatoCemi)
        {
            FormsAuthentication.RedirectFromLoginPage(username, false);
        }
        else
        {
            PIT16orews client = new PIT16orews();
            Output output = client.login(username, password);

            if (output.OutputRiconosciuto)
            {
                GestioneUtentiBiz biz = new GestioneUtentiBiz();

                //cerco se censito da noi
                if (output.OutputTipo == 1)
                {
                    //impresa
                    //simulo cf: 09996850153
                    output.OutputCf = "09996850153";
                    List<ImpresaConCredenziali> imprese =
                        biz.GetImprese(new ImpresaFilter {CodiceFiscale = output.OutputCf});
                    if (imprese.Count == 1)
                    {
                        //OK - autentico
                        string usernameCemi = GestioneUtentiBiz.GetIdentitaUtente(imprese[0].IdUtente).UserName;
                        FormsAuthentication.SetAuthCookie(usernameCemi, false);
                        Response.Redirect("~/Corsi/CorsiDefault.aspx");
                    }
                    else
                    {
                        //CF non riconosciuto
                        LabelResponse.Text = "Impresa non censita presso Cassa Edile";
                    }
                }
                else if (output.OutputTipo == 2)
                {
                    //consulente
                    //simulo cf: LZZRSL58S67F205P
                    output.OutputCf = "LZZRSL58S67F205P";
                    List<ConsulenteConCredenziali> consulenti =
                        biz.GetConsulentiConCredenziali(new ConsulenteFilter {CodiceFiscale = output.OutputCf});
                    if (consulenti.Count == 1)
                    {
                        //OK - autentico
                        string usernameCemi = GestioneUtentiBiz.GetIdentitaUtente(consulenti[0].IdUtente).UserName;
                        FormsAuthentication.SetAuthCookie(usernameCemi, false);
                        Response.Redirect("~/Corsi/CorsiDefault.aspx");
                    }
                    else
                    {
                        //CF non riconosciuto
                        LabelResponse.Text = "Consulente non censito presso Cassa Edile";
                    }
                }
            }
            else
            {
                LabelResponse.Text = "Credenziali fornite non valide";
            }
        }
    }
}