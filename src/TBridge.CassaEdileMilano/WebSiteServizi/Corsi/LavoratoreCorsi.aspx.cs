using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;

public partial class Corsi_LavoratoreCorsi : Page
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiVisualizzazione, "LavoratoreCorsi.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            CaricaCorsiLavoratore(0);
        }
    }

    private void CaricaCorsiLavoratore(Int32 pagina)
    {
        //UtenteLavoratore lavoratore =
        //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore)HttpContext.Current.User.Identity).
        //            Entity;
        UtenteLavoratore lavoratore =
            (UtenteLavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        PartecipazioneFilter filtro = new PartecipazioneFilter();
        filtro.IdLavoratore = lavoratore.IdLavoratore;

        Presenter.CaricaElementiInGridView(
            GridViewCorsi,
            biz.GetPartecipazioni(filtro),
            pagina);
    }

    protected void GridViewCorsi_PageIndexChanging1(object sender, GridViewPageEventArgs e)
    {
        CaricaCorsiLavoratore(e.NewPageIndex);
    }
}