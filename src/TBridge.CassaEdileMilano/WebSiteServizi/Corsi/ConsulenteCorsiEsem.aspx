﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ConsulenteCorsiEsem.aspx.cs" Inherits="Corsi_ConsulenteCorsiEsem" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsiEsem.ascx" TagName="MenuCorsiEsem" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CorsiRicercaLavoratoreImpresaConsulente.ascx" TagName="CorsiRicercaLavoratoreImpresaConsulente"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc4" %>
<%@ Register src="WebControls/CorsiImpresaRicercaPartecipantiEsem.ascx" tagname="CorsiImpresaRicercaPartecipantiEsem" tagprefix="uc5" %>
<%@ Register src="../WebControls/ConsulenteSelezioneImpresa.ascx" tagname="ConsulenteSelezioneImpresa" tagprefix="uc6" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCorsiEsem ID="MenuCorsiEsem1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Corsi dei lavoratori" />
    <br />
    <uc6:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
    <br />
    <br />
    <uc5:CorsiImpresaRicercaPartecipantiEsem ID="CorsiImpresaRicercaPartecipantiEsem1" runat="server" />
    <br />
    
</asp:Content>
