using System;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Telerik.WinControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;
using LavoratoreIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;
using TBridge.Cemi.Business;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Lavoratore = TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using Impresa = TBridge.Cemi.Corsi.Type.Entities.Impresa;

public partial class Corsi_IscrizioneCorsiLavoratoriEsem : Page
{

    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly AnagraficaCondivisaManager anagCondManager = new AnagraficaCondivisaManager();
    private readonly CorsiEsemBusiness corsiEsemBiz = new CorsiEsemBusiness();
    private readonly Common commonBiz = new Common();


    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiIscrizioneLavoratore);

        #endregion

        #region Eventi dei controlli custom

        CorsiRicercaImpresaAnagraficaCondivisa1.OnImpresaSelected += new TBridge.Cemi.Corsi.Type.Delegates.ImpresaSelectedEventHandler(CorsiRicercaImpresaAnagraficaCondivisa1_OnImpresaSelected);

        #endregion

        if (!Page.IsPostBack)
        {
            UtenteLavoratore lavoratore =
                (UtenteLavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            SelezionaLavoratore(lavoratore);



        }



        LabelMessaggio.Visible = false;
    }




    void CorsiRicercaImpresaAnagraficaCondivisa1_OnImpresaSelected(Impresa impresa)
    {
        SelezionaImpresaAnagraficaCondivisa(impresa);
    }

    private void SelezionaImpresaAnagraficaCondivisa(Impresa impresa)
    {
        ViewState["Impresa"] = impresa;

        CorsiRicercaImpresaAnagraficaCondivisa1.Visible = false;
        ButtonSelezionaImpresa.Enabled = true;

        if (impresa != null)
        {
            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                TextBoxImpresa.Text = String.Format("{0} {1}\n{2}\n{3}",
                                                    impresa.IdImpresa,
                                                    impresa.RagioneSociale,
                                                    impresa.PartitaIva,
                                                    impresa.CodiceFiscale);
            }
/*            else
            {
                //TextBoxImpresa.Text = String.Format("{0}\n{1}\n{2}",
                //                                    impresa.RagioneSociale,
                //                                    impresa.PartitaIva,
                //                                    impresa.CodiceFiscale);
                MultiViewImpresa.Visible = true;
                MultiViewImpresa.ActiveViewIndex = INDICENUOVAIMPRESA;
                //CorsiDatiImpresa1.CaricaDatiImpresa(impresa);
            }
 */
        }
        else
        {
            TextBoxImpresa.Text = String.Empty;
        }
    }

    protected void ButtonIscriviLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String messaggio;
            Iscrizione iscrizione = CreaIscrizione();
            LabelMessaggio.Visible = false;

            Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            //if (iscrizione.Impresa != null)
            //{
            //    if (commonBiz.IsImpresaIrregolareBNI((int)iscrizione.Impresa.IdImpresa))
            //}
            if (!iscrizione.Gratuito) 
            {
                if (iscrizione.Corso.Costo > 0)
                {
                    Page.DataBind();
                    Page.SetFocus(UserListDialog.ClientID);
                    ViewState["iscrizione"] = iscrizione;
                    ViewState["utente"] = utente;
                    Decimal costo = iscrizione.Corso.Costo;
                    lblCorsoPagamento.Text = "<b>ATTENZIONE!</b><br />Il corso per il quale si sta effettuando l'iscrizione comporter� un costo di �" + costo.ToString() + ".<br />Procedere con l'iscrizione?";
                    String script = "function openWindowNoteAllT(){openRadWindowCosto(); Sys.Application.remove_load(openWindowNoteAllT);}Sys.Application.add_load(openWindowNoteAllT);";
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "openRadWindowCosto", script, true);

                    //String script = "function openWindowNoteAll("+iscrizione.Corso.Costo.ToString()+"){$find(\"" + UserListDialog.ClientID +
                    //                   "\").show(); Sys.Application.remove_load(openWindowNoteAll);}Sys.Application.add_load(openWindowNoteAll);";

                    /*
                    String script = "function openRadWindowCosto(){$find(\"" + UserListDialog.ClientID +
                                   "\").show(); Sys.Application.remove_load(openRadWindowCosto);}Sys.Application.add_load(openRadWindowCosto);";
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "openRadWindowCosto", script, true);
                    */
                }
                else
                {
                    iscrizione.Gratuito = true;

                    ViewState["iscrizione"] = iscrizione;
                    ViewState["utente"] = utente;

                    if (corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione))
                    {
                        LabelMessaggio.Text = messaggio;
                        LabelMessaggio.Visible = true;
                    }
                    else
                    {
                        LabelMessaggio.Text = messaggio;
                        LabelMessaggio.Visible = true;
                    }
                }

            }
            else 
            {
                ViewState["iscrizione"] = iscrizione;
                ViewState["utente"] = utente;

                if (corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione))
                {
                    LabelMessaggio.Text = messaggio;
                    LabelMessaggio.Visible = true;
                }
                else
                {
                    LabelMessaggio.Text = messaggio;
                    LabelMessaggio.Visible = true;
                }

                /*
                Page.DataBind();
                Page.SetFocus(PrivacyDialog.ClientID);
                ViewState["iscrizione"] = iscrizione;
                ViewState["utente"] = utente;
                //Server.Transfer("../IscrizioneCorsiPrivacyEsem.aspx");
                String script = "function openWindowNoteAllT(){openRadWindowPrivacy(); Sys.Application.remove_load(openWindowNoteAllT);}Sys.Application.add_load(openWindowNoteAllT);";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "openRadWindowPrivacy", script, true);
                */

                //Page.Master.Attributes.Add("onload", "CloseAndRebind();");
                //CorsiSelezionePregrammazioneEsem1.CaricaProgrammazioniInternaDaPopUp();

            }

        }
    }


    protected void RadButtonNoPagamento_OnClick(object sender, EventArgs e)
    {
        Page.Master.Attributes.Add("onload", "CloseAndRebind();");
        CorsiSelezionePregrammazioneEsem1.CaricaProgrammazioniInternaDaPopUp();
    }



    protected void RadButtonOkPagamento_OnClick(object sender, EventArgs e)
    {
        Iscrizione iscrizione = (Iscrizione)ViewState["iscrizione"];
        Utente utente = (Utente)ViewState["utente"];
        String messaggio;

        if (corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione))
        {
            LabelMessaggio.Text = messaggio;
            LabelMessaggio.Visible = true;
        }
        else
        {
            LabelMessaggio.Text = messaggio;
            LabelMessaggio.Visible = true;
        }

        Page.Master.Attributes.Add("onload", "CloseAndRebind();");
        CorsiSelezionePregrammazioneEsem1.CaricaProgrammazioniInternaDaPopUp();

        /*
        Page.Master.Attributes.Add("onload", "CloseAndRebind();");
        Page.DataBind();
        Page.SetFocus(PrivacyDialog.ClientID);
        String script = "function openWindowNoteAllT(){openRadWindowPrivacy(); Sys.Application.remove_load(openWindowNoteAllT);}Sys.Application.add_load(openWindowNoteAllT);";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "openRadWindowPrivacy", script, true);
        */



    }

    /*
    protected void RadButtonOkPrivacy_OnClick(object sender, EventArgs e)
    {
        Iscrizione iscrizione = (Iscrizione)ViewState["iscrizione"];
        Utente utente = (Utente)ViewState["utente"];
        String messaggio;

        if (corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione))
        {
            LabelMessaggio.Visible = true;
        }
        else
        {
            LabelMessaggio.Text = messaggio;
            LabelMessaggio.Visible = true;
        }

        Page.Master.Attributes.Add("onload", "CloseAndRebind();");
        CorsiSelezionePregrammazioneEsem1.CaricaProgrammazioniInternaDaPopUp();
    }
    */




    private void ResetCampi()
    {
        ViewState["Lavoratore"] = null;
        Presenter.SvuotaCampo(TextBoxLavoratore);
    }


    protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Lavoratore"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Impresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();

        if (corso != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    private Iscrizione CreaIscrizione()
    {
        Iscrizione iscrizione = new Iscrizione();

        iscrizione.Corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();
        iscrizione.Lavoratore = (Lavoratore)ViewState["Lavoratore"];
        iscrizione.Impresa = (Impresa)ViewState["Impresa"];
        // DA MODIFICARE
        iscrizione.Contatto = "3402788089";
        // END
        if (iscrizione.Lavoratore.IdLavoratore.HasValue)
        {
            iscrizione.Gratuito = corsiEsemBiz.CorsiGratuita(iscrizione.Lavoratore.IdLavoratore.Value,DateTime.Now.Date);
        }
        // Da fare solo se corso 16ore mics
        //iscrizione.Ticket = corsiEsemBiz.CorsiTicket(iscrizione.Corso.DataInizio.Value, iscrizione.Lavoratore.DataAssunzione.Value, iscrizione.Lavoratore.PrimaEsperienza.Value);

        return iscrizione;
    }

    private void SelezionaLavoratore(UtenteLavoratore lavoratore)
    {
        Lavoratore lav = new Lavoratore();
        lav.TipoLavoratore = TipologiaLavoratore.SiceNew;
        lav.IdLavoratore = lavoratore.IdLavoratore;
        lav.CodiceFiscale = lavoratore.CodiceFiscale;

        List<TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Lavoratore> lavCond = anagCondManager.GetLavoratoriByFilter(new TBridge.Cemi.Type.Filters.AnagraficaCondivisa.LavoratoreFilter() { CodiceCassaEdile = lavoratore.IdLavoratore});
        if (lavCond.Count == 1)
        {
            lav.IdAnagraficaCondivisa = lavCond[0].IdLavoratore;
        }
        else
        {
            throw new Exception("Lavoratore non trovato in anagrafica condivisa");
        }
        
        ViewState["Lavoratore"] = lav;

        
        if (lavoratore != null)
        {
            
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       String.Empty,
                                                       String.Empty
                                                       //lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       //lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty
                                                       );
        }
    }
    protected void ButtonSelezionaImpresa_Click(object sender, EventArgs e)
    {
        CorsiRicercaImpresaAnagraficaCondivisa1.Visible = true;
        ButtonSelezionaImpresa.Enabled = false;
    }

    protected void BtnCorsoNoPrerequisitiOk_Click(object sender, EventArgs e)
    {
        return;
    }

    protected void CustomValidatorCertificatoMedico_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = CheckBoxCertificatoMedico.Checked;
    }

    protected void CustomValidatorCondizioniGenerali_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = CheckBoxCondizioniGenerali.Checked;
    }

    protected void CustomValidatorRadioPrivacy_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = RadButtonAutorizza.Checked;
    }
}
