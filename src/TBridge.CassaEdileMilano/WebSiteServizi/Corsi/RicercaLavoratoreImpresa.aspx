﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RicercaLavoratoreImpresa.aspx.cs" Inherits="Corsi_RicercaLavoratoreImpresa" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CorsiRicercaLavoratoreImpresa.ascx" TagName="CorsiRicercaLavoratoreImpresa"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc4" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Ricerca lavoratore/impresa" />
    <br />
    <uc4:PitHeader ID="PitHeader1" runat="server" />
    <br />
    <uc3:CorsiRicercaLavoratoreImpresa ID="CorsiRicercaLavoratoreImpresa1" runat="server" />
    <br />
</asp:Content>
