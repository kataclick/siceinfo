using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using UtenteImpresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using LavoratoreIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;
using TBridge.Cemi.Business;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Lavoratore = TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using Impresa = TBridge.Cemi.Corsi.Type.Entities.Impresa;

public partial class Corsi_IscrizioneCorsiImpresaEsem : Page
{
    private const Int32 INDICENUOVOLAVORATORE = 1;
    private const Int32 INDICESELEZIONELAVORATORE = 0;

    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly AnagraficaCondivisaManager anagCondManager = new AnagraficaCondivisaManager();
    private readonly CorsiEsemBusiness corsiEsemBiz = new CorsiEsemBusiness();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiIscrizioneImpresa);

        #endregion

        #region Eventi dei controlli custom

        CorsiRicercaLavoratore1.OnLavoratoreSelected += CorsiRicercaLavoratore1_OnLavoratoreSelected;
        CorsiRicercaLavoratore1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;

        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreSelected += new TBridge.Cemi.Corsi.Type.Delegates.LavoratoreSelectedEventHandler(CorsiRicercaLavoratoreAnagraficaCondivisa1_OnLavoratoreSelected);
        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;
        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreRicerca += CorsiRicercaLavoratore1_OnLavoratoreRicerca;

        #endregion

        #region Per prevenire click multipli

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate('iscrizione') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonIscriviLavoratore, null));
        sb.Append(";");
        sb.Append("return true;");
        ButtonIscriviLavoratore.Attributes.Add("onclick", sb.ToString());

        #endregion

        if (!Page.IsPostBack)
        {
            //UtenteImpresa impresa =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Impresa)HttpContext.Current.User.Identity).
            //        Entity;
            UtenteImpresa impresa =
                (UtenteImpresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            if (commonBiz.IsImpresaIrregolareBNI(impresa.IdImpresa))
                Server.Transfer("IscrizioneCorsiImpresaIrregolareBNI.aspx");

            SelezionaImpresa(impresa);

            if (Context.Items["lavoratoreIscrizioneLavoratori"] != null)
            {
                LavoratoreIscrizioneLavoratori lavIscri =
                    (LavoratoreIscrizioneLavoratori)Context.Items["lavoratoreIscrizioneLavoratori"];

                Lavoratore lav = new Lavoratore();
                lav.Nome = lavIscri.Nome;
                lav.Cognome = lavIscri.Cognome;
                lav.CodiceFiscale = lavIscri.CodiceFiscale;
                lav.DataNascita = lavIscri.DataNascita;
                lav.IdLavoratore = lavIscri.IdLavoratore;
                lav.TipoLavoratore = (TipologiaLavoratore)lavIscri.TipoLavoratore;

                SelezionaLavoratore(lav);
            }
        }

        LabelMessaggio.Visible = false;

        //if (Context.Items["impresaIscrizioneLavoratori"] != null)
        //    SelezionaImpresa((Impresa)Context.Items["impresaIscrizioneLavoratori"]);

    }

    protected void ButtonIscriviLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String messaggio;
            Iscrizione iscrizione = CreaIscrizione();
            LabelMessaggio.Visible = false;

            if (iscrizione.Corso.Costo == 0)
            {
                iscrizione.Gratuito = true;
            }

            Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione);

            LabelMessaggio.Text = messaggio;
            LabelMessaggio.Visible = true;
        }
    }

 
    private void ResetCampi()
    {
        ViewState["Lavoratore"] = null;
        Presenter.SvuotaCampo(TextBoxLavoratore);
    }

    protected void ButtonNuovoLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Lavoratore lavoratore = CorsiDatiLavoratoreEsem1.GetLavoratore();

            if (!biz.EsisteLavoratoreConStessoCodiceFiscale(lavoratore.CodiceFiscale))
            {
                SelezionaLavoratore(lavoratore);
                CorsiDatiLavoratoreEsem1.ResetCampi();
            }
            else
            {
                MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
                CorsiRicercaLavoratoreAnagraficaCondivisa1.ForzaRicerca(lavoratore.CodiceFiscale);
            }
        }
    }

    protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
    {
        MultiViewLavoratore.Visible = true;
        MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
        ButtonSelezionaLavoratore.Enabled = false;
    }

    private void SelezionaImpresa(UtenteImpresa utenteImpresa)
    {
        
        Impresa impresa = new Impresa();
        impresa.TipoImpresa = TipologiaImpresa.SiceNew;
        impresa.IdImpresa = utenteImpresa.IdImpresa;
        impresa.RagioneSociale = utenteImpresa.RagioneSociale;
        impresa.PartitaIva = utenteImpresa.PartitaIVA;
        impresa.CodiceFiscale = utenteImpresa.CodiceFiscale;
        List<TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Impresa> impCond = anagCondManager.GetImpreseByFilter(new TBridge.Cemi.Type.Filters.AnagraficaCondivisa.ImpresaFilter() { CodiceCassaEdile = impresa.IdImpresa });
        if (impCond.Count == 1)
        {
            impresa.IdAnagraficaCondivisa = impCond[0].IdImpresa;

            var querySedi = from sede in impCond[0].Sedi
                            where (sede.IdEnteGestore == 1 || sede.IdEnteGestore == 2)  && !String.IsNullOrEmpty(sede.Email)
                            orderby sede.IdEnteGestore descending, sede.IdTipoSede ascending
                            select sede;
            List<TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Sede> sediConEmail = querySedi.ToList();

            if (sediConEmail.Count > 0)
            {
                TextBoxContatto.Text = sediConEmail[0].Email;
            }
        }
        else
        {
            throw new Exception("Impresa non trovata in anagrafica condivisa");
        }

        ViewState["Impresa"] = impresa;

        CorsiRicercaLavoratoreAnagraficaCondivisa1.IdImpresa(impresa.IdImpresa.Value);

        if (impresa != null)
        {
            TextBoxImpresa.Text = String.Format("{0} {1}\n{2}\n{3}",
                                                impresa.IdImpresa,
                                                impresa.RagioneSociale,
                                                impresa.PartitaIva,
                                                impresa.CodiceFiscale);
        }
        else
        {
            TextBoxImpresa.Text = String.Empty;
        }
    }

    protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Lavoratore"] != null)
        {
            TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();
            if (corso == null || corso.Codice != "16OREMICS")
            {
                args.IsValid = true;
            }
            else
            {
                Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
                if (lavoratore.PrimaEsperienza.HasValue && lavoratore.DataAssunzione.HasValue)
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }

        }
        else
        {
            args.IsValid = false;
        }


    }

    protected void CustomValidatorImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Impresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();

        if (corso != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    private Iscrizione CreaIscrizione()
    {
        Iscrizione iscrizione = new Iscrizione();

        iscrizione.Corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();
        iscrizione.Lavoratore = (Lavoratore)ViewState["Lavoratore"];
        iscrizione.Impresa = (Impresa)ViewState["Impresa"];
        iscrizione.Contatto = TextBoxContatto.Text;

        if (iscrizione.Lavoratore.IdLavoratore.HasValue)
        {
            iscrizione.Gratuito = corsiEsemBiz.CorsiGratuita(iscrizione.Lavoratore.IdLavoratore.Value,DateTime.Now.Date);
        }
        // Da fare solo se corso 16ore mics
        //iscrizione.Ticket = corsiEsemBiz.CorsiTicket(iscrizione.Corso.DataInizio.Value, iscrizione.Lavoratore.DataAssunzione.Value, iscrizione.Lavoratore.PrimaEsperienza.Value);

        return iscrizione;
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreNuovo()
    {
        //TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();
        // DA CONTROLLARE CODICE CORSO!!
        //if (corso != null && corso.Codice == "16OREMICS")
        //{
            MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
            ButtonSelezionaLavoratore.Enabled = true;
        //}
        //else
        //{
        //    CorsiRicercaLavoratoreAnagraficaCondivisa1.LavoratoreNuovoErrore();
        //}
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreRicerca()
    {
        TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezionePregrammazioneEsem1.GetCorsoSelezionato();
        // DA CONTROLLARE CODICE CORSO!!
        if (corso != null && corso.Codice == "16OREMICS")
        {
            CorsiRicercaLavoratoreAnagraficaCondivisa1.InForza(false);
        }
        else
        {
            CorsiRicercaLavoratoreAnagraficaCondivisa1.InForza(true);
        }
        
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        SelezionaLavoratore(lavoratore);
    }

    private void SelezionaLavoratore(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        
        MultiViewLavoratore.Visible = false;
        ButtonSelezionaLavoratore.Enabled = true;

        if (lavoratore != null)
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
            else
            {
                TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
        }
    }

    void CorsiRicercaLavoratoreAnagraficaCondivisa1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        SelezionaLavoratoreAnagraficaCondivisa(lavoratore);
    }

    private void SelezionaLavoratoreAnagraficaCondivisa(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        MultiViewLavoratore.Visible = false;
        ButtonSelezionaLavoratore.Enabled = true;

        if (lavoratore != null)
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
            else
            {
                //TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                //                                       lavoratore.Cognome,
                //                                       lavoratore.Nome,
                //                                       lavoratore.DataNascita.ToShortDateString(),
                //                                       lavoratore.CodiceFiscale,
                //                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                //                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
                MultiViewLavoratore.Visible = true;
                MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
                CorsiDatiLavoratoreEsem1.CaricaDatiLavoratore(lavoratore);
            }

            //if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            //{
            //    LanciaRicercaUltimaImpresaDisponibile(lavoratore.IdLavoratore.Value);
            //}
        }
        else
        {
            TextBoxLavoratore.Text = String.Empty;
        }
    }
}