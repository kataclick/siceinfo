﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Enums;

public partial class Corsi_ModificaLavoratore : System.Web.UI.Page
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione);

        #endregion

        if (!Page.IsPostBack)
        {
            if ((Request.QueryString["IdLavoratore"] != null
                || Request.QueryString["IdCorsiLavoratore"] != null)
                && Request.QueryString["IdPartecipazione"] != null)
            {
                Int32 idLavoratore;
                Int32 idPartecipazione;
                TipologiaLavoratore tipoLavoratore;

                idPartecipazione = Int32.Parse(Request.QueryString["IdPartecipazione"]);
                ViewState["IdPartecipazione"] = idPartecipazione;

                if (!String.IsNullOrEmpty(Request.QueryString["IdLavoratore"]))
                {
                    idLavoratore = Int32.Parse(Request.QueryString["IdLavoratore"]);
                    tipoLavoratore = TipologiaLavoratore.SiceNew;
                }
                else
                {
                    idLavoratore = Int32.Parse(Request.QueryString["IdCorsiLavoratore"]);
                    tipoLavoratore = TipologiaLavoratore.Anagrafica;
                }

                ViewState["TipoLavoratore"] = tipoLavoratore;

                if (tipoLavoratore == TipologiaLavoratore.Anagrafica)
                {
                    CorsiDatiLavoratore1.Visible = true;
                    CorsiDatiLavoratore1.CaricaDatiLavoratore(idLavoratore, idPartecipazione);
                }
                else
                {
                    DatiLavoratoreSiceNew1.Visible = true;
                    DatiLavoratoreSiceNew1.CaricaDatiLavoratore(idLavoratore, idPartecipazione);
                }
            }
        }
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        // Chiudere la finestra
    }

    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        Page.Validate("datiLavoratore");

        if (Page.IsValid)
        {
            Int32 idPartecipazione = (Int32) ViewState["IdPartecipazione"];
            TipologiaLavoratore tipoLavoratore = (TipologiaLavoratore) ViewState["TipoLavoratore"];

            if (tipoLavoratore == TipologiaLavoratore.Anagrafica)
            {
                Lavoratore lavoratore = CorsiDatiLavoratore1.GetLavoratore();

                if (biz.UpdateCorsiLavoratore(lavoratore, idPartecipazione))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "CloseRadWindowAndRefresh",
                        "<script type='text/javascript'>CloseRadWindowAndRebind()</script>");

                    //CaricaPartecipantiInterna(GridViewPartecipanti.PageIndex);
                }
            }
            else
            {
                Boolean? primaEsperienza;
                DateTime? dataAssunzione;

                DatiLavoratoreSiceNew1.GetDatiPartecipazione(out primaEsperienza, out dataAssunzione);

                if (biz.UpdatePartecipazione(idPartecipazione, primaEsperienza, dataAssunzione))
                {
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "CloseRadWindowAndRefresh",
                        "<script type='text/javascript'>CloseRadWindowAndRebind()</script>");
                }
            }
        }
    }
}