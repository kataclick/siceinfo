﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IscrizioneCorsiLavoratoreEsem.aspx.cs" Inherits="Corsi_IscrizioneCorsiLavoratoriEsem"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CorsiRicercaLavoratore.ascx" TagName="CorsiRicercaLavoratore"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/CorsiSelezioneProgrammazioneEsem.ascx" TagName="CorsiSelezioneProgrammazioneEsem"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/CorsiDatiLavoratore.ascx" TagName="CorsiDatiLavoratore"
    TagPrefix="uc5" %>
<%@ Register Src="../WebControls/CorsiRicercaImpresa.ascx" TagName="CorsiRicercaImpresa"
    TagPrefix="uc6" %>
<%@ Register Src="../WebControls/CorsiDatiImpresa.ascx" TagName="CorsiDatiImpresa"
    TagPrefix="uc7" %>
<%@ Register Src="../WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc4" %>
<%@ Register src="../WebControls/CorsiRicercaLavoratoreAnagraficaCondivisa.ascx" tagname="CorsiRicercaLavoratoreAnagraficaCondivisa" 
    TagPrefix="uc9" %>
<%@ Register src="../WebControls/MenuCorsiEsem.ascx" tagname="MenuCorsiEsem" tagprefix="uc8" %>
<%@ Register src="../WebControls/CorsiRicercaImpresaAnagraficaCondivisa.ascx" tagname="CorsiRicercaImpresaAnagraficaCondivisa" tagprefix="uc10" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc8:MenuCorsiEsem ID="MenuCorsiEsem1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">


<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">


        function openRadWindowCosto()
        {
            var radwindow = $find('<%=UserListDialog.ClientID %>');
            radwindow.show();
            radwindow.set_title("Corso a pagamento");
            radwindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            radwindow.setSize(310, 320);
            radwindow.center();
        }
        
        function Cancel()
        {
            openRadWindowCosto().close();
        }

//        function openWindowNoteAll(costo) {
            /*window.radopen("../Corsi/CorsiEsemAvvisoPagamento.aspx?costo="+costo, "UserListDialog"); 
            return false; 
            */
  /*          var oWindow = radopen("../Corsi/CorsiEsemAvvisoPagamento.aspx?costo="+costo.toString(), "UserListDialog");
            oWindow.set_title("Corso a pagamento");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(310, 320);
            oWindow.center();
            oWindow.add_close(OnClientClose);
            
        }
        */
    </script>
</telerik:RadCodeBlock>
<telerik:RadWindow ID="UserListDialog" runat="server" Title="Corso a Pagamento" Height="200px" Width="310px" ShowContentDuringLoad="false" EnableViewState="true">
        <ContentTemplate>
            <asp:Label ID="lblCorsoPagamento" runat="server"></asp:Label>
            <br />
            <telerik:RadButton runat="server" ID="RadButtonOkPagamento" Text="Ok" OnClick="RadButtonOkPagamento_OnClick" />
            &nbsp;
            <telerik:RadButton ID="RadButtonNoPagamento" runat="server" Text="No" onClick="RadButtonNoPagamento_OnClick"/>
        </ContentTemplate>
</telerik:RadWindow>
<%     
/*    
<telerik:RadWindow ID="PrivacyDialog" runat="server" Title="Privacy" Width="730px" ShowContentDuringLoad="false"  EnableViewState="true">
    <ContentTemplate>
        <p style="text-align:justify">
                <b>AUTOCERTIFICAZIONE MEDICA</b><br />
                L’impresa autocertifica ai sensi della L.46 D.P.R. 28 dicembre 2000, n. 445 che il partecipante è in possesso di idoneità psicofisica alla
                mansione professionale oggetto dell’attività formativa (accertata a cura del medico del lavoro).<br />
                <asp:CheckBox ID="CheckBoxCertificatoMedico" runat="server"  /> Ho letto e accetto <asp:RequiredFieldValidator ID="ValidatorCertificatoMedico" runat="server" ValidationGroup="GroupPrivacy" ControlToValidate="CheckBoxCertificatoMedico" Text="*" ForeColor="Red" />
                <br /><br />
                <b>N.B. Se sei un lavoratore che accede in maniera autonoma ai corsi Esem sarà necessario produrre il certificato medico di idoneità alla mansione coerente con le attività previste nel corso rilasciato dal medico del lavoro</b>
            <hr />
            <p style="text-align:justify">
                <b>CONDIZIONI GENERALI</b><br />
                Tutti i corsi sono a numero chiuso e le iscrizioni vengono accettate fino ad esaurimento posti. L'iscrizione e la riserva del diritto di frequenza saranno validi
                solo al momento del saldo della quota d’iscrizione, la ricevuta dell'avvenuto pagamento dovrà essere inviata contestualmente all'invio della scheda d’iscrizione.
                In caso di rinuncia verranno addebitati € 100,00 per costi di segreteria, mentre la rinuncia a 3 giorni dall’inizio del corso comporterà l'addebito dell'intero importo. Esem si riserva la possibilità
                di modificare o annullare la programmazione dei propri corsi, nel caso non venisse raggiunto il numero minimo di iscritti o in caso di avverse condizioni meteo per i corsi svolti all'aperto
                <br />
                <asp:CheckBox ID="CheckBoxCondizioniGenerali" runat="server"  /> Ho letto e accetto <asp:RequiredFieldValidator ID="ValidatorCondizioniGenerali" runat="server" ValidationGroup="GroupPrivacy" ControlToValidate="CheckBoxCondizioniGenerali" Text="*" ForeColor="Red" />
            <hr />
            <p style="text-align:justify">
                <b>GARANZIA DI RISERVATEZZA</b><br />
                Ai sensi della legge 196/03 sulla tutela dei dati personali, Esem - fornita anche attraverso l’affissione nell’ingresso della sede Esem l’informativa sull’utilizzo delle
                informazioni - garantisce la riservatezza dei dati raccolti e la possibilità di richiederne la rettifica o la cancellazione gratuita scrivendo a: Esem, Via Newton, 3 - 20148 Milano. 
                Tali dati saranno custoditi nell’archivio elettronico Esem e verranno utilizzati al solo scopo di inviare, con le modalità ritenute più idonee, tutte le informazioni inerenti le iniziative organizzate 
                dall’Ente e di soddisfare le richieste dei soggetti finanziatori delle attività, delle parti sociali e degli altri enti bilaterali. 
                Tali dati potranno essere altresì inseriti nella specifica banca dati dei partecipanti ai corsi Esem con accesso remoto, attraverso password, al corsista per la visione e stampa del proprio percorso formativo;
                alle imprese per la formazione fruita dai propri dipendenti; agli operatori e amministratori Esem per una visione del quadro generale delle attività. 
                Esem è altresì autorizzata ad utilizzare le immagini fotografiche e video riprese durante il corso e, ai sensi dell’art. 96 L633/91, ad ogni ulteriore utilizzo delle suddette immagini.
                <br />
                <asp:Table runat="server" Width="100%">
                    <asp:TableRow HorizontalAlign="Center">
                        <asp:TableCell Width="50%" HorizontalAlign="Center"><asp:RadioButton ID="CheckBoxAcconsente" runat="server" /><b>AUTORIZZA</b> Esem al trattamento dei dati <asp:RequiredFieldValidator ID="ValidatorConsenso" runat="server" ValidationGroup="GroupPrivacy" ControlToValidate="CheckBoxAcconsente" Text="*" ForeColor="Red" /></asp:TableCell><asp:TableCell HorizontalAlign="Center"><asp:RadioButton ID="CheckBoxRifiuta" runat="server" /><b>NON AUTORIZZA</b> Esem al trattamento dei dati</asp:TableCell></asp:TableRow></asp:Table><i>La mancata autorizzazione al trattamento dei dati comporta per Esem l’impossibilità di erogare il servizio oggetto della presente richiesta. Le ricordiamo, inoltre che potrà far valere i suoi diritti come previsto dall’art. 7 D.Lgs.196/03, 
                rivolgendosi al titolare del trattamento.</i> </p><br /><asp:Table runat="server" Width="100%" ID="TablePrivacy">
                <asp:TableRow>
                    <asp:TableCell Width="50%" HorizontalAlign="Center"><telerik:RadButton ID="BtnPrivacyNo" runat="server" Text="Annulla" onClick="RadButtonNoPrivacy_OnClick"/></asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center"><telerik:RadButton runat="server" ID="BtnPrivacyOk" Text="Procedi con l'iscrizione" OnClick="RadButtonOkPrivacy_OnClick" /></asp:TableCell>                    
                </asp:TableRow>
            </asp:Table>
                <asp:ValidationSummary
                    ID="ValidationSummaryRicercaTimbrature"
                    runat="server"
                    ValidationGroup="ricercaTimbrature"
                    ForeColor="Red" />
            </ContentTemplate>
</telerik:RadWindow>
*/
%>
<Telerik:radwindow ID="RadWindowNoPrerequisiti" CenterIfModal="True" Modal="True" MinWidth="400" MinHeight="200" AutoSize="False" Behaviors="None" IconUrl="~/Images/favicon.ico" Title="Corso a pagamento" runat="server" Skin="Default">
    <ContentTemplate>
        <p>&nbsp;</p>
        <p style="text-align:center">
            <b>ATTENZIONE!</b><br />
            Il corso per il quale si vuole effettuando l'iscrizione prevede la partecipazione a precedenti sessioni.<br /> 
            Non si dispongono dei requisiti necessari per completare l'iscrizione
        </p>
        <p style="text-align:center">
            Contattare ESEM per ulteriori informazioni.
        </p>
        <p style="text-align:center">
            <asp:Button ID="BtnCorsoNoPrerequisitiOk" Text="Ok" runat="server" onclick="BtnCorsoNoPrerequisitiOk_Click" Width="80" />
        </p>
    </ContentTemplate>
</Telerik:radwindow>

    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Iscrizione lavoratori" />
    <br />
    <p>
    TBD </p><br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Corso"></asp:Label></td></tr></table><table class="borderedTable">
            <tr>
                <td>
                    <uc3:CorsiSelezioneProgrammazioneEsem ID="CorsiSelezionePregrammazioneEsem1" runat="server" /></td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Lavoratore"></asp:Label></td></tr></table><table class="borderedTable">
            <tr>
                <td style="width: 100px">
                    Lavoratore </td><td>
                    <asp:TextBox ID="TextBoxLavoratore" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox></td></tr><tr>
                <td colspan="2">
                    &nbsp;</td></tr></table></div><br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Impresa"></asp:Label></td></tr></table><table class="borderedTable">
            <tr>
                <td style="width: 100px">
                    Impresa </td><td>
                    <asp:TextBox ID="TextBoxImpresa" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox><asp:Button ID="ButtonSelezionaImpresa" runat="server" Text="Seleziona impresa" Width="200px"
                        Enabled="True" OnClick="ButtonSelezionaImpresa_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">                    
                    <uc10:CorsiRicercaImpresaAnagraficaCondivisa ID="CorsiRicercaImpresaAnagraficaCondivisa1" 
                                runat="server" Visible= "false" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <p style="text-align:justify; font-size:9px">
        <b>AUTOCERTIFICAZIONE MEDICA</b><br />
        L’impresa autocertifica ai sensi della L.46 D.P.R. 28 dicembre 2000, n. 445 che il partecipante è in possesso di idoneità psicofisica alla
        mansione professionale oggetto dell’attività formativa (accertata a cura del medico del lavoro).<br />
        <asp:CheckBox ID="CheckBoxCertificatoMedico" runat="server"  /> Ho letto e accetto <asp:CustomValidator ID="CustomValidatorCertificatoMedico" runat="server" ValidationGroup="iscrizione" OnServerValidate="CustomValidatorCertificatoMedico_ServerValidate" Text="*" ForeColor="Red" />
        <br /><br />
        <b>N.B. Se sei un lavoratore che accede in maniera autonoma ai corsi Esem sarà necessario produrre il certificato medico di idoneità alla mansione coerente con le attività previste nel corso rilasciato dal medico del lavoro</b> </p><hr />
    <p style="text-align:justify; font-size:9px">
        <b>CONDIZIONI GENERALI</b><br />
        Tutti i corsi sono a numero chiuso e le iscrizioni vengono accettate fino ad esaurimento posti. L'iscrizione e la riserva del diritto di frequenza saranno validi
        solo al momento del saldo della quota d’iscrizione, la ricevuta dell'avvenuto pagamento dovrà essere inviata contestualmente all'invio della scheda d’iscrizione.
        In caso di rinuncia verranno addebitati € 100,00 per costi di segreteria, mentre la rinuncia a 3 giorni dall’inizio del corso comporterà l'addebito dell'intero importo. Esem si riserva la possibilità
        di modificare o annullare la programmazione dei propri corsi, nel caso non venisse raggiunto il numero minimo di iscritti o in caso di avverse condizioni meteo per i corsi svolti all'aperto <br />
        <asp:CheckBox ID="CheckBoxCondizioniGenerali" runat="server"  /> Ho letto e accetto <asp:CustomValidator ID="CustomValidatorCondizioniGenerali" runat="server" ValidationGroup="iscrizione" OnServerValidate="CustomValidatorCondizioniGenerali_ServerValidate" Text="*" ForeColor="Red" />
    </p>
    <hr />
    <p style="text-align:justify; font-size:9px">
        <b>GARANZIA DI RISERVATEZZA</b><br />
        Ai sensi della legge 196/03 sulla tutela dei dati personali, Esem - fornita anche attraverso l’affissione nell’ingresso della sede Esem l’informativa sull’utilizzo delle
        informazioni - garantisce la riservatezza dei dati raccolti e la possibilità di richiederne la rettifica o la cancellazione gratuita scrivendo a: Esem, Via Newton, 3 - 20148 Milano. 
        Tali dati saranno custoditi nell’archivio elettronico Esem e verranno utilizzati al solo scopo di inviare, con le modalità ritenute più idonee, tutte le informazioni inerenti le iniziative organizzate 
        dall’Ente e di soddisfare le richieste dei soggetti finanziatori delle attività, delle parti sociali e degli altri enti bilaterali. 
        Tali dati potranno essere altresì inseriti nella specifica banca dati dei partecipanti ai corsi Esem con accesso remoto, attraverso password, al corsista per la visione e stampa del proprio percorso formativo;
        alle imprese per la formazione fruita dai propri dipendenti; agli operatori e amministratori Esem per una visione del quadro generale delle attività. 
        Esem è altresì autorizzata ad utilizzare le immagini fotografiche e video riprese durante il corso e, ai sensi dell’art. 96 L633/91, ad ogni ulteriore utilizzo delle suddette immagini. <br />
        <asp:Table runat="server" Width="100%">
            <asp:TableRow HorizontalAlign="Center">
                <asp:TableCell Width="50%" HorizontalAlign="Center">
                    <asp:RadioButton ID="RadButtonAutorizza" runat="server" /><b>AUTORIZZA</b> Esem al trattamento dei dati <asp:CustomValidator id="CustomValidatorRadioPrivacy" runat="server" Display="Dynamic" ValidationGroup="iscrizione" OnServerValidate="CustomValidatorRadioPrivacy_ServerValidate" Text="*" ForeColor="Red" />
                </asp:TableCell><asp:TableCell HorizontalAlign="Center">
                    <asp:RadioButton ID="RadButtonNega" runat="server" /><b>NON AUTORIZZA</b> Esem al trattamento dei dati
                </asp:TableCell></asp:TableRow></asp:Table><i>La mancata autorizzazione al trattamento dei dati comporta per Esem l’impossibilità di erogare il servizio oggetto della presente richiesta. Le ricordiamo, inoltre che potrà far valere i suoi diritti come previsto dall’art. 7 D.Lgs.196/03, 
        rivolgendosi al titolare del trattamento.</i> </p><br />
    <asp:CustomValidator ID="CustomValidatorProgrammazioni" runat="server" ValidationGroup="iscrizione" ErrorMessage="Corso non selezionato" OnServerValidate="CustomValidatorProgrammazioni_ServerValidate">&nbsp;</asp:CustomValidator><asp:CustomValidator ID="CustomValidatorLavoratore" runat="server" ValidationGroup="iscrizione" ErrorMessage="Lavoratore non selezionato/inserito" OnServerValidate="CustomValidatorLavoratore_ServerValidate">&nbsp;</asp:CustomValidator><br />
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red" Text="Iscrizione effettuata correttamente" Visible="false"></asp:Label><asp:ValidationSummary ID="ValidationSummaryIscrizione" runat="server" ValidationGroup="iscrizione" CssClass="messaggiErrore" />
    <br />
    <asp:Button ID="ButtonIscriviLavoratore" runat="server" Text="Iscrivi lavoratore" ValidationGroup="iscrizione" OnClick="ButtonIscriviLavoratore_Click" />
    <br />
</asp:Content>
