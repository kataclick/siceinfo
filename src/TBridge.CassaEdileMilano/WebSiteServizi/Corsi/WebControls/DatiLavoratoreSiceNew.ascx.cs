﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities;

public partial class Corsi_WebControls_DatiLavoratoreSiceNew : System.Web.UI.UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ResetCampi()
    {
        Presenter.SvuotaCampo(TextBoxDataAssunzione);
        CheckBoxPrimaEsperienza.Checked = false;
    }

    public void CaricaDatiLavoratore(Int32 idLavoratore, Int32 idPartecipazione)
    {
        ResetCampi();

        Boolean? primaEsperienza;
        DateTime? dataAssunzione;
        
        biz.GetDatiAggiuntiviPartecipazione(idPartecipazione, out primaEsperienza, out dataAssunzione);
        //PartecipazioneCollection partecipazione = biz.GetPartecipazioni(idPartecipazione);

        ViewState["IdLavoratore"] = idLavoratore;
        if (primaEsperienza.HasValue)
        {
            CheckBoxPrimaEsperienza.Checked = primaEsperienza.Value;
        }
        if (dataAssunzione.HasValue)
        {
            TextBoxDataAssunzione.Text = dataAssunzione.Value.ToString("dd/MM/yyyy");
        }
    }

    public void GetDatiPartecipazione(out Boolean? primaEsperienza, out DateTime? dataAssunzione)
    {
        primaEsperienza = CheckBoxPrimaEsperienza.Checked;

        DateTime dtAssunzione;
        if (DateTime.TryParseExact(TextBoxDataAssunzione.Text, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dtAssunzione))
        {
            dataAssunzione = dtAssunzione;
        }
        else
        {
            dataAssunzione = null;
        }
    }
}