﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiSelezioneProgrammazioneEsem.ascx.cs"
    Inherits="WebControls_CorsiSelezioneProgrammazioneEsem" %>
<table class="standardTable">
    <tr>
        <td>
            Tipo programmazione:
        </td>
        <td colspan="2">
        <asp:RadioButton ID="RadioButtonSchedulato" runat="server" GroupName="CorsiProgrammazione" Checked="true" Text="A calendario" AutoPostBack="true" EnableViewState="true" oncheckedchanged="RadioButtonSchedulato_CheckedChanged" /> - <asp:RadioButton ID="RadioButtonNonSchedulato" runat="server" GroupName="CorsiProgrammazione" Text="Non schedulato" AutoPostBack="true" EnableViewState="true" oncheckedchanged="RadioButtonNonSchedulato_CheckedChanged" />
        </td>
    </tr>
    <tr>
        <td>
            Schedulazioni dal:
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBoxDataInizio" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataInizio" runat="server"
                ControlToValidate="TextBoxDataInizio" ValidationGroup="caricaSchedulazioni">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorDataInizio" runat="server" ControlToValidate="TextBoxDataInizio"
                ValidationGroup="caricaSchedulazioni" Type="Date" Operator="DataTypeCheck">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Schedulazioni al:
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBoxDataFine" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataFine" runat="server" ControlToValidate="TextBoxDataFine"
                ValidationGroup="caricaSchedulazioni">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorDataFine" runat="server" ControlToValidate="TextBoxDataFine"
                ValidationGroup="caricaSchedulazioni" Type="Date" Operator="DataTypeCheck">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidatorDataInizioDataFine" runat="server" ControlToValidate="TextBoxDataInizio"
                ControlToCompare="TextBoxDataFine" ValidationGroup="caricaSchedulazioni" Type="Date"
                Operator="LessThanEqual">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Tipo Corso:
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListTipoCorso" runat="server" Width="400px" AppendDataBoundItems="true">
            </asp:DropDownList>

        </td>
    </tr>
    <tr>
        <td>
            Sede Corso:
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListSedeCorso" runat="server" Width="400px" AppendDataBoundItems="true">
            </asp:DropDownList>

        </td>
    </tr>
    <tr><td colspan="3">
    <asp:Button ID="ButtonAggiornaProgrammazioni" runat="server" ValidationGroup="caricaSchedulazioni"
                Width="150px" OnClick="ButtonAggiornaProgrammazioni_Click" Text="Aggiorna" />
    </td></tr>
    <tr>
        <td colspan="3">
            <asp:GridView ID="GridViewProgrammazioniDisponibili" runat="server" AutoGenerateColumns="False"
                Width="100%" 
                OnRowDataBound="GridViewProgrammazioniDisponibili_RowDataBound" 
                AllowPaging="True" 
                PageSize="5"
                onpageindexchanging="GridViewProgrammazioniDisponibili_PageIndexChanging" onrowcreated="GridViewProgrammazioniDisponibili_RowCreated">
                <Columns>
                    <asp:TemplateField>
                    <ItemStyle Width="10px" />
                        <ItemTemplate>
                            <%--<asp:RadioButton ID="RadioButtonSelezione" runat="server" GroupName="Selezione">
                            </asp:RadioButton>--%>
                            <asp:Literal
                                ID="LiteralRadioButtonSelezioneMarkup"
                                runat="server">
                            </asp:Literal>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DataInizio" HeaderText="Data inizio" DataFormatString="{0:dd/MM/yyyy}" >
                        <ItemStyle Width="80px"  />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Corso">
                        <ItemTemplate>
                            <b><asp:Label ID="LabelCorsoDescrizione" runat="server" Text='<%# Bind("Descrizione") %>'></asp:Label></b>
                            (<asp:Label ID="LabelCorsoCodice" runat="server" Text='<%# Bind("Codice") %>'></asp:Label>)
                            <br /><br />
                            <asp:Label ID="LabelSede" runat="server"></asp:Label>
                            <br />
                            <asp:GridView ID="GridViewElencoDateCorso" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyNames="GiornoCorso, OraInizio, OraFine" ShowHeader="false">
                                <Columns>
                                    <asp:BoundField DataField="GiornoCorso" DataFormatString="{0:dd/MM/yyyy}" ShowHeader="false">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OraInizio" DataFormatString="{0:H:mm}" ShowHeader="false">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OraFine" DataFormatString="{0:H:mm}" ShowHeader="false">
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Costo" HeaderText="Costo">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Durata" HeaderText="Durata (ore)">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PostiDisponibili" HeaderText="Posti Disponibili">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ObbligoVisitaMedica" HeaderText="Visita Medica">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna corso trovato
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
