﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiLavoratoreSiceNew.ascx.cs" Inherits="Corsi_WebControls_DatiLavoratoreSiceNew" %>
<table  class="borderedTable">
    <tr>
        <td>
            Dichiara di non aver mai lavorato nell'edilizia*
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxPrimaEsperienza" runat="server" Width="300px" MaxLength="16"></asp:CheckBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Data di assunzione (gg/mm/aaaa)*
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataAssunzione" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataAssunzione" runat="server"
                ControlToValidate="TextBoxDataAssunzione" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
            <asp:CompareValidator
                ID="CompareValidatorDataAssunzione"
                runat="server"
                ControlToValidate="TextBoxDataAssunzione"
                Operator="DataTypeCheck"
                Type="Date"
                ValidationGroup="datiLavoratore">
                *
            </asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            * campi obbligatori
        </td>
    </tr>
</table>