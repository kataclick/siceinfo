﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CorsiImpresaRicercaPartecipantiEsem.ascx.cs"
    Inherits="WebControls_CorsiImpresaRicercaPartecipantiEsem" %>
<asp:Panel ID="PanelRicercaLavoratoreImpresa" runat="server" DefaultButton="ButtonRicerca">
    <table class="standardTable">
        <tr>
            <td>
            <asp:Panel ID="FiltersFileds" runat="server">
                <table class="standardTable">
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelPadding2" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="cercaIscrizioni"
                                Width="100px" OnClick="ButtonRicerca_Click" />
                        </td>
                    </tr>
                </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Iscrizioni trovate"></asp:Label><br />
                <asp:GridView ID="GridViewPartecipazioni" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="GridViewPartecipazioni_PageIndexChanging"
                    OnRowDataBound="GridViewPartecipazioni_RowDataBound" PageSize="5" DataKeyNames="Lavoratore">
                    <Columns>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Iscrizione">
                            <ItemTemplate>
                                <asp:GridView ID="GridViewCorsi" runat="server" Width="100%" AutoGenerateColumns="False" onrowdeleting="GridViewCorsi_RowDeleting" DataKeyNames="Codice,DataInizio,CodiceSede,Progressivo" onrowdatabound="GridViewCorsi_RowDataBound">
                                <Columns>
                                     <asp:BoundField DataField="DataInizio" HeaderText="Data inizio" DataFormatString="{0:dd/MM/yyyy}" >
                                        <ItemStyle Width="80px"  />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Corso">
                                        <ItemTemplate>
                                            <b><asp:Label ID="LabelCorsoDescrizione" runat="server" Text='<%# Bind("Descrizione") %>'></asp:Label></b>
                                            (<asp:Label ID="LabelCorsoCodice" runat="server" Text='<%# Bind("Codice") %>'></asp:Label>)
                                            <br /><br />
                                            <asp:Label ID="LabelSede" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stato iscrizione">
                                         <ItemTemplate>
                                             <asp:Label ID="LabelTipoIscrizione" runat="server"></asp:Label>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Cauzione">
                                         <ItemTemplate>
                                             <asp:Label ID="LabelCauzioneCosto" runat="server"></asp:Label>
                                             <br />
                                             <asp:Label ID="LabelCauzioneVersata" runat="server"></asp:Label>
                                         </ItemTemplate>
                                         <ItemStyle Width="80px" />
                                     </asp:TemplateField>
                                     <asp:TemplateField ShowHeader="False">
                                         <ItemTemplate>
                                             <asp:Button ID="ButtonDelete" runat="server" CausesValidation="False" CommandName="Delete" Text="Cancella" />
                                         </ItemTemplate>
                                         <ItemStyle Width="10px" />
                                     </asp:TemplateField>
                                </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna iscrizione trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Label
        ID="LabelMessaggio"
        runat="server"
        ForeColor="Red">
    </asp:Label>
</asp:Panel>
