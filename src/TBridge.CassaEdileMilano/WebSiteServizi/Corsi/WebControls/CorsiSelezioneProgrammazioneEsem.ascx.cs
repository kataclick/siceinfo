using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters.Esem;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using Telerik.Web.UI;

public partial class WebControls_CorsiSelezioneProgrammazioneEsem : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly CorsiEsemBusiness esembiz = new CorsiEsemBusiness();
    private Boolean utenteConsulente;
    private Boolean utenteImpresa;
    private Boolean nuovaRicerca = false;

    private int IndiceCorsoSelezionato
    {
        get
        {
            if (string.IsNullOrEmpty(Request.Form["GruppoCorsi"]))
            {
                if (ViewState["indiceSelezione"] != null)
                {
                    return (Int32)ViewState["indiceSelezione"];
                }
            }
            else
            {
                if (!nuovaRicerca)
                {
                    return Convert.ToInt32(Request.Form["GruppoCorsi"]);
                }
            }

            return -1;
        }
    }

    //private List<Int32> CorsiDisabilitati
    //{
    //    get
    //    {
    //        if (ViewState["CorsiDisabilitati"] != null)
    //        {
    //            return (List<Int32>)ViewState["CorsiDisabilitati"];
    //        }
    //        else
    //        {
    //            return new List<Int32>();
    //        }
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsImpresa())
        {
            utenteImpresa = true;
        }

        if (GestioneUtentiBiz.IsConsulente())
        {
            utenteConsulente = true;
        }

        /*RadioButton rb;
        if (ViewState["TipoProgrammazione"] != null)
        {
            rb = (RadioButton)Page.FindControl(ViewState["TipoProgrammazione"].ToString());
            rb.Checked = true;
        }
        else
        {
            RadioButtonSchedulato.Checked = true;
        }
        */
        if (!Page.IsPostBack)
        {
            ViewState["pagina"] = 0;
            CaricaDropDown();
            InizializzaRicercaDate();
            CaricaProgrammazioniInterna(0);
            
        }


    }

    public int QualePagina()
    { 
        int pagina = 0;
        if (ViewState["pagina"] != null)
            pagina = (int)ViewState["pagina"];
        return pagina;
    }

    private void InizializzaRicercaDate()
    {
        TextBoxDataInizio.Text = DateTime.Now.ToString("dd/MM/yyyy");
        TextBoxDataFine.Text = DateTime.Now.AddDays(10).ToString("dd/MM/yyyy");
    }

    private void CaricaDropDown()
    {
        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipoCorso,
            esembiz.GetTipiCorso(utente),
            "Descrizione",
            "Codice");

       
        List<Sede> sedi = esembiz.GetSedi(utente);
        ViewState["sedi"] = TrasformaSediInDictionary(sedi);

        String calendario = RadioButtonSchedulato.Checked?"S":"N";
        List<Sede> sediDropDown = new List<Sede>();

        for (Int32 i = 0; i < sedi.Count; i++)
        {
            if (sedi[i].Calendarizzato == calendario)
            {
                Sede sedeDropDown = new Sede();
                sediDropDown.Add(sedeDropDown);

                sedeDropDown.Codice = sedi[i].Codice;
                sedeDropDown.Descrizione = sedi[i].Descrizione;
                sedeDropDown.Indirizzo = sedi[i].Indirizzo;
                sedeDropDown.Calendarizzato = sedi[i].Calendarizzato;
            }
        }


        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListSedeCorso,
            sediDropDown,
            "Descrizione",
            "Codice");
    }

    private Dictionary<String, String> TrasformaSediInDictionary(List<Sede> sedi)
    {
        Dictionary<String, String> dic = new Dictionary<String, String>();

        foreach (Sede s in sedi)
        {
            if (!dic.ContainsKey(s.Codice))
            {
                dic.Add(s.Codice, s.Descrizione + " - " + s.Indirizzo);
            }
        }
        dic.Add("", "Da definire");

        return dic;
    }

    /*private void CaricaProgrammazioni(ModuloCollection moduli)
    {
        Presenter.CaricaElementiInGridView(
            GridViewProgrammazioniDisponibili,
            moduli,
            0);
    }
    */
    protected void GridViewProgrammazioniDisponibili_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Corso corso = (Corso)e.Row.DataItem;
            Label lSede = (Label)e.Row.FindControl("LabelSede");
            //RadioButton rbSel = (RadioButton)e.Row.FindControl("RadioButtonSelezione");

            if (ViewState["sedi"] != null)
            {
                Dictionary<String, String> sedi = (Dictionary<String, String>)ViewState["sedi"];
                //if (corso.CodiceSede < 0)
                    
                lSede.Text = sedi[corso.CodiceSede];
            }

            GridView gvElencoDate = (GridView)e.Row.FindControl("GridViewElencoDateCorso");
            Presenter.CaricaElementiInGridView(gvElencoDate, corso.DateCorso, 0);

            //if (ViewState["indiceSelezione"] != null)
            //{
            //    Int32 indiceSelezione = (Int32)ViewState["indiceSelezione"];
            //    //rbSel.Checked = (indiceSelezione == e.Row.DataItemIndex);
            //}
        }
    }

    public void CaricaProgrammazioniInterna(Int32 pagina)
    {
        //Int32 idCorso = 0;//Int32.Parse(DropDownListCorso.SelectedValue);
        //DateTime dataInizio = DateTime.Parse(TextBoxDataInizio.Text.Replace('.', '/'));
        //DateTime dataFine = DateTime.Parse(TextBoxDataFine.Text.Replace('.', '/'));

        //ModuloCollection moduli = biz.GetModuliConProgrammazioniFuture(idCorso, dataInizio, dataFine,
        //                                                               utenteImpresa || utenteConsulente,
        //                                                               !utenteImpresa && !utenteConsulente);
        //CaricaProgrammazioni(moduli);
        ViewState["pagina"] = pagina;
        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        CorsoFilter filter = CreaFiltro();

        List<Corso> corsi = esembiz.GetCorsi(utente, filter);
        ViewState["corsi"] = corsi;
        
        Presenter.CaricaElementiInGridView(
            GridViewProgrammazioniDisponibili,
            corsi,
            pagina);
    }


    public void CaricaProgrammazioniInternaDaPopUp()
    {

        Int32 pagina = QualePagina();
        ViewState["pagina"] = pagina;
        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        CorsoFilter filter = CreaFiltro();

        List<Corso> corsi = esembiz.GetCorsi(utente, filter);
        ViewState["corsi"] = corsi;

        Presenter.CaricaElementiInGridView(
            GridViewProgrammazioniDisponibili,
            corsi,
            pagina);
    }


    private CorsoFilter CreaFiltro()
    {
        CorsoFilter filter = new CorsoFilter();

        filter.Dal = DateTime.ParseExact(TextBoxDataInizio.Text, "dd/MM/yyyy", null);
        filter.Al = DateTime.ParseExact(TextBoxDataFine.Text, "dd/MM/yyyy", null);
        filter.CodiceTipoCorso = DropDownListTipoCorso.SelectedValue;
        filter.CodiceSede = DropDownListSedeCorso.SelectedValue;

        return filter;
    }

    public Corso GetCorsoSelezionato()
    {
        Corso corso = null;

        List<Corso> corsi = ViewState["corsi"] as List<Corso>;
        if (corsi != null
            && IndiceCorsoSelezionato > -1)
        {
            corso = corsi[IndiceCorsoSelezionato];
        }

        return corso;
    }

    protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        RadioButtonList rbProgrammazioni =
            (RadioButtonList) ((CustomValidator) source).FindControl("RadioButtonListProgrammazioni");

        if (rbProgrammazioni.SelectedIndex >= 0)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void ButtonAggiornaProgrammazioni_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["indiceSelezione"] = null;
            nuovaRicerca = true;
            CaricaProgrammazioniInterna(0);
        }
    }

    private void SelezionaLaProgrammazione(int idProgrammazione)
    {
        //foreach (GridViewRow row in GridViewProgrammazioniDisponibili.Rows)
        //{
        //    RadioButtonList rblProgrammazioni = (RadioButtonList) row.FindControl("RadioButtonListProgrammazioni");
        //    SelezionaLaProgrammazione(idProgrammazione, rblProgrammazioni);
        //}
    }

    private void SelezionaLaProgrammazione(int idProgrammazione, RadioButtonList rblProgrammazioni)
    {
        //rblProgrammazioni.SelectedIndex = -1;

        //foreach (ListItem programmazione in rblProgrammazioni.Items)
        //{
        //    if (ProgrammazioneModulo.IdProgrammazioneDaIdComposto(programmazione.Value) == idProgrammazione)
        //    {
        //        programmazione.Selected = true;
        //        return;
        //    }
        //}
    }

    protected void GridViewProgrammazioniDisponibili_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ViewState["indiceSelezione"] = IndiceCorsoSelezionato;

        CaricaProgrammazioniInterna(e.NewPageIndex);
    }

    protected void GridViewProgrammazioniDisponibili_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Literal output = (Literal)e.Row.FindControl("LiteralRadioButtonSelezioneMarkup");
            output.Text = string.Format(
                @"<input type=""radio"" name=""GruppoCorsi"" " +
                @"id=""RowSelector{0}"" value=""{0}"" ", e.Row.DataItemIndex);

            
            if (IndiceCorsoSelezionato == e.Row.DataItemIndex)
            {
                output.Text += @" checked=""checked""";
            }

            Corso corso = (Corso)e.Row.DataItem;
            if (corso != null && (((string)ViewState["TipoProgrammazione"] == "RadioButtonSchedulato" || ViewState["TipoProgrammazione"] == null) && (corso.PostiDisponibili == null || corso.PostiDisponibili < 1)))
            {
                output.Text += @" disabled";
            }
            else
            {
                List<Corso> corsi = (List<Corso>)ViewState["corsi"];

                if (corsi != null && (((string)ViewState["TipoProgrammazione"] == "RadioButtonSchedulato" || ViewState["TipoProgrammazione"] == null) && (corsi[e.Row.DataItemIndex].PostiDisponibili == null || corsi[e.Row.DataItemIndex].PostiDisponibili < 1)))
                {
                    output.Text += @" disabled";
                }
            }

            output.Text += " />";
        }
    }

    protected void RadioButtonSchedulato_CheckedChanged(object sender, EventArgs e)
    {
        TextBoxDataInizio.Enabled = true;
        TextBoxDataFine.Enabled = true;
        InizializzaRicercaDate();
        ViewState["TipoProgrammazione"] = "RadioButtonSchedulato";
        //CaricaDropDown();
        CaricaProgrammazioniInterna(0);
    }
    protected void RadioButtonNonSchedulato_CheckedChanged(object sender, EventArgs e)
    {        
        TextBoxDataInizio.Text = new DateTime(1900, 01, 01).ToString("dd/MM/yyyy");
        TextBoxDataFine.Text = new DateTime(1900, 01, 01).ToString("dd/MM/yyyy");
        TextBoxDataInizio.Enabled = false;
        TextBoxDataFine.Enabled = false;
        ViewState["TipoProgrammazione"] = "RadioButtonNonSchedulato";
        //CaricaDropDown();
        CaricaProgrammazioniInterna(0);
    }
}