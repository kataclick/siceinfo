﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Business;
using Utente = TBridge.Cemi.GestioneUtenti.Type.Entities.Utente;
using UtenteImpresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;

public partial class Corsi_WebControls_ScadenzeLavoratori : System.Web.UI.UserControl
{
    private readonly CorsiEsemBusiness esemBiz = new CorsiEsemBusiness();
    //private readonly CorsiBusiness corsiBiz = new CorsiBusiness();   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaCorsiScadenza(0);
        }
    }

    private void CaricaCorsiScadenza(Int32 pagina)
    {
        Utente utente = null;
        List<LavoratoreCorsi> corsiScad = new List<LavoratoreCorsi>();

        if (GestioneUtentiBiz.IsImpresa())
        {
            utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            corsiScad = esemBiz.GetCorsiScadenzeImpresa(utente, ((UtenteImpresa)utente).CodiceFiscale, ((UtenteImpresa)utente).IdImpresa);
        }

        if (GestioneUtentiBiz.IsLavoratore())
        {
            utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            LavoratoreCorsi corsoScad = esemBiz.GetCorsiScadenzeLavoratore(utente, ((UtenteLavoratore)utente).CodiceFiscale, ((UtenteLavoratore)utente).IdLavoratore);

            if (corsoScad != null)
            {
                corsiScad.Add(corsoScad);
            }
        }

        Presenter.CaricaElementiInGridView(
            GridViewPartecipazioni,
            corsiScad,
            pagina);
    }


    protected void GridViewPartecipazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("cercaIscrizioni");

        if (Page.IsValid)
        {
            CaricaCorsiScadenza(e.NewPageIndex);
        }
    }

    protected void GridViewPartecipazioni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LavoratoreCorsi lc = (LavoratoreCorsi)e.Row.DataItem;

            Label lLavoratore = (Label)e.Row.FindControl("LabelLavoratore");
            GridView gvCorsi = (GridView)e.Row.FindControl("GridViewCorsi");

            lLavoratore.Text = lc.Lavoratore.CognomeNome;

            Presenter.CaricaElementiInGridView(gvCorsi, lc.Corsi, 0);

        }
    }
}