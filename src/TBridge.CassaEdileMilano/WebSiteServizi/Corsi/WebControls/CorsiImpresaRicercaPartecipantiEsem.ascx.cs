using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Utente = TBridge.Cemi.GestioneUtenti.Type.Entities.Utente;
using UtenteImpresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using UtenteConsulente = TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.Corsi.Type.Filters;
using Lavoratore = TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using TBridge.Cemi.Business;
using System.Collections.Generic;

public partial class WebControls_CorsiImpresaRicercaPartecipantiEsem : UserControl
{
    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly CorsiEsemBusiness esemBiz = new CorsiEsemBusiness();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsLavoratore())
        {
            FiltersFileds.Visible = false;
        }

        CaricaDizionarioSedi();

            if (!Page.IsPostBack)
            {
                CaricaSedi();
                CaricaCorsiImpresa(0);
            }

    }

    private void CaricaSedi()
    {
        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        List<Sede> sedi = esemBiz.GetSedi(utente);
        ViewState["sedi"] = TrasformaSediInDictionary(sedi);
    }
    private void CaricaDizionarioSedi()
    {
        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        //String calendario = if 
        
        List<Sede> sedi = esemBiz.GetSedi(utente);
        ViewState["sedi"] = TrasformaSediInDictionary(sedi);

    }

    private Dictionary<String, String> TrasformaSediInDictionary(List<Sede> sedi)
    {
        Dictionary<String, String> dic = new Dictionary<String, String>();

        foreach (Sede s in sedi)
        {
            if (!dic.ContainsKey(s.Codice))
            {
                dic.Add(s.Codice, s.Descrizione + " - " + s.Indirizzo);
            }
        }
        dic.Add("", "Da definire");

        return dic;
    }

    private void CaricaCorsiImpresa(Int32 pagina)
    {
        //UtenteImpresa impresa =
        //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Impresa)HttpContext.Current.User.Identity).
        //            Entity;

        Utente utente = null;
        Int32 idImpresa = -1;
        String ragSocImpresa = null;
        String codFiscImpresa = null;

        if (GestioneUtentiBiz.IsImpresa())
        {
            utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idImpresa = ((UtenteImpresa)utente).IdImpresa;
            codFiscImpresa = ((UtenteImpresa)utente).CodiceFiscale;
        }
        if (GestioneUtentiBiz.IsConsulente())
        {
            utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            commonBiz.ConsulenteImpresaSelezionata(((UtenteConsulente)utente).IdConsulente, out idImpresa, out ragSocImpresa, out codFiscImpresa);
        }

        if (idImpresa > 0)
        {
            PartecipazioneLavoratoreImpresaFilter filtro = CreaFiltro();
            filtro.ImpresaIvaFisc = codFiscImpresa;
            filtro.IdImpresa = idImpresa;

            Presenter.CaricaElementiInGridView(
                GridViewPartecipazioni,
                esemBiz.GetIscrizioniImpresa(utente, filtro),
                pagina);
        }



        if (GestioneUtentiBiz.IsLavoratore())
        {
            utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            LavoratoreCorsi lc = esemBiz.GetIscrizioniLavoratore(utente, ((UtenteLavoratore)utente).IdLavoratore, ((UtenteLavoratore)utente).CodiceFiscale);
            List<LavoratoreCorsi> lavoratoriCorsi = new List<LavoratoreCorsi>();
            if (lc != null)
            {
                lavoratoriCorsi.Add(lc);
            }

            Presenter.CaricaElementiInGridView(
                GridViewPartecipazioni,
                lavoratoriCorsi,
                pagina);
        }


    }

    private PartecipazioneLavoratoreImpresaFilter CreaFiltro()
    {
        PartecipazioneLavoratoreImpresaFilter filtro = new PartecipazioneLavoratoreImpresaFilter();

        filtro.LavoratoreCognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        filtro.LavoratoreNome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        filtro.LavoratoreCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

        return filtro;
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaCorsiImpresa(0);
        }
    }

    protected void GridViewPartecipazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("cercaIscrizioni");

        if (Page.IsValid)
        {
            CaricaCorsiImpresa(e.NewPageIndex);
        }
    }

    protected void GridViewPartecipazioni_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Corso corso = (Corso)e.Row.DataItem;
            //Label lSede = (Label)e.Row.FindControl("LabelSede");
            LavoratoreCorsi lc = (LavoratoreCorsi)e.Row.DataItem;

            Label lLavoratore = (Label) e.Row.FindControl("LabelLavoratore");
            GridView gvCorsi = (GridView)e.Row.FindControl("GridViewCorsi");

            lLavoratore.Text = lc.Lavoratore.CognomeNome;

            Presenter.CaricaElementiInGridView(gvCorsi, lc.Corsi, 0);

        }
    }
    protected void GridViewCorsi_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gvCorsi = (GridView)sender;
        Corso corso = new Corso();
        String messaggio;

        corso.Codice = (String)gvCorsi.DataKeys[e.RowIndex].Values["Codice"];
        corso.DataInizio = (DateTime)gvCorsi.DataKeys[e.RowIndex].Values["DataInizio"];
        corso.CodiceSede = (String)gvCorsi.DataKeys[e.RowIndex].Values["CodiceSede"];
        corso.Progressivo = (Int32)gvCorsi.DataKeys[e.RowIndex].Values["Progressivo"];

        Lavoratore lavoratore = (Lavoratore)GridViewPartecipazioni.DataKeys[((GridViewRow)gvCorsi.NamingContainer).RowIndex].Values["Lavoratore"];
        Iscrizione iscrizione = new Iscrizione();
        iscrizione.Lavoratore = lavoratore;
        iscrizione.Corso = corso;

        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        UtenteImpresa ui = null;
        
        if (GestioneUtentiBiz.IsConsulente())
        {
            Int32 idImpresa = -1;
            String ragSocImpresa;
            String codFiscImpresa;
            

            commonBiz.ConsulenteImpresaSelezionata(((UtenteConsulente)utente).IdConsulente, out idImpresa, out ragSocImpresa, out codFiscImpresa);

            ui = new UtenteImpresa();
            ui.IdImpresa = idImpresa;
            ui.CodiceFiscale = codFiscImpresa;
            ui.RagioneSociale = ragSocImpresa;
        }
        

        if (!esemBiz.CancellaIscrizione(out messaggio, ui != null ? ui : utente, iscrizione))
        {
            //CaricaCorsiImpresa(GridViewPartecipazioni.PageIndex);
            LabelMessaggio.Text = messaggio;
        }
        else
        {
            CaricaCorsiImpresa(GridViewPartecipazioni.PageIndex);
        }
    }

    protected void GridViewCorsi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Corso corso = (Corso)e.Row.DataItem;
            Label lSede = (Label)e.Row.FindControl("LabelSede");
            Label lTipo = (Label)e.Row.FindControl("LabelTipoIscrizione");
            Label lCauz = (Label)e.Row.FindControl("LabelCauzioneCosto");
            Label lPagCauz = (Label)e.Row.FindControl("LabelCauzioneVersata");
            Button btn = (Button)e.Row.FindControl("ButtonDelete");

            if (!String.IsNullOrWhiteSpace(corso.DescrizioneSede) && !String.IsNullOrWhiteSpace(corso.IndirizzoSede))
            {
                lSede.Text = corso.DescrizioneSede.ToString() + " - " + corso.IndirizzoSede.ToString();
            }
            else
            {
                if (ViewState["sedi"] != null)
                {
                    Dictionary<String, String> sedi = (Dictionary<String, String>)ViewState["sedi"];
                    //if (corso.CodiceSede < 0)

                    lSede.Text = sedi[corso.CodiceSede];
                }
                else
                {
                    lSede.Text = "Altra sede";
                }
                
            }
            /*
            if (ViewState["sedi"] != null)
            {
                Dictionary<String, String> sedi = (Dictionary<String, String>)ViewState["sedi"];
                //if (corso.CodiceSede < 0)
                if (sedi.ContainsKey(corso.CodiceSede))
                {
                    lSede.Text = sedi[corso.CodiceSede];
                }
                else
                {
                    lSede.Text = "Altra sede";
                }
            }
            */
            if (corso.Progressivo == 0)
            {
                lTipo.Text = "Prenotato";
            }
            else
            {
                lTipo.Text = "Calendarizzato";
            }

            if (corso.DataInizio.HasValue && corso.DataInizio.Value.Date.AddDays(-3) <= DateTime.Now.Date && corso.DataInizio.Value.Date != DateTime.ParseExact("1900-01-01","yyyy-MM-dd",null))
            {
                btn.Enabled = false;
            }

            if (corso.CostoCauzione > (Decimal)0)
            {
                lCauz.Text = String.Format("Costo: {0}", corso.CostoCauzione.ToString());

                if (corso.DataPagamentoCauzione.HasValue)
                {
                    lPagCauz.Text = String.Format("Versata il: {0:dd/MM/yyyy}", corso.DataPagamentoCauzione.Value);
                }
                else
                {
                    lPagCauz.Text = String.Format("Da versare entro il: {0:dd/MM/yyyy}", corso.DataInizio.Value.Date.AddDays(-5));
                }
            }
            else
            {
                lCauz.Text = String.Format("Nessuna cauzione prevista");
            }
        }
    }

}