﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScadenzeLavoratori.ascx.cs" Inherits="Corsi_WebControls_ScadenzeLavoratori" %>
                <asp:GridView ID="GridViewPartecipazioni" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="GridViewPartecipazioni_PageIndexChanging"
                    OnRowDataBound="GridViewPartecipazioni_RowDataBound" PageSize="5" DataKeyNames="Lavoratore">
                    <Columns>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Iscrizione">
                            <ItemTemplate>
                                <asp:GridView ID="GridViewCorsi" runat="server" Width="100%" AutoGenerateColumns="False">
                                <Columns>
                                     <asp:BoundField DataField="DataInizio" HeaderText="Data inizio" DataFormatString="{0:dd/MM/yyyy}" >
                                        <ItemStyle Width="80px"  />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Corso">
                                        <ItemTemplate>
                                            <b><asp:Label ID="LabelCorsoDescrizione" runat="server" Text='<%# Bind("Descrizione") %>'></asp:Label></b>
                                            (<asp:Label ID="LabelCorsoCodice" runat="server" Text='<%# Bind("Codice") %>'></asp:Label>)
                                            <br /><br />
                                            <asp:Label ID="LabelSede" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="DataScadenza" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data scadenza">
                                     <ItemStyle Font-Bold="True" Width="80px" />
                                     </asp:BoundField>
                                </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun corso in scadenza trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            
