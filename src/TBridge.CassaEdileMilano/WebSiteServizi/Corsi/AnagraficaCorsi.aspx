<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AnagraficaCorsi.aspx.cs" Inherits="Corsi_AnagraficaCorsi" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Anagrafica corsi" />
    <br />
    <div style="width: 100%">
        <asp:GridView ID="GridViewCorsi" runat="server" AutoGenerateColumns="False" Width="100%"
            OnRowDataBound="GridViewCorsi_RowDataBound" AllowPaging="True" OnPageIndexChanging="GridViewCorsi_PageIndexChanging"
            PageSize="3" DataKeyNames="IdCorso" OnRowDeleting="GridViewCorsi_RowDeleting">
            <Columns>
                <asp:BoundField DataField="Codice" HeaderText="Codice">
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Descrizione">
                    <ItemTemplate>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelDescrizione" runat="server"></asp:Label>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelDurata" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelVersione" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="CheckBoxIscrizioneLibera" runat="server" Enabled="false" Text="Iscrizione libera" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="200px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Moduli">
                    <ItemTemplate>
                        <asp:GridView ID="GridViewModuli" runat="server" Width="100%" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="Progressivo" HeaderText="Progr.">
                                    <ItemStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                                <asp:BoundField DataField="OreDurata" HeaderText="Durata (ore)">
                                    <ItemStyle Width="100px" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina"
                    ShowDeleteButton="True">
                    <ItemStyle Width="10px" />
                </asp:CommandField>
            </Columns>
            <EmptyDataTemplate>
                Nessun corso presente
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:Button ID="ButtonNuovoCorso" runat="server" Text="Nuovo corso..." OnClick="ButtonNuovoCorso_Click"
            Width="150px" />
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <div style="width: 100%">
        <asp:Panel ID="PanelNuovoCorso" runat="server" Visible="false" Width="100%" DefaultButton="ButtonInserimentoCorso">
            <table class="standardTable">
                <tr>
                    <td>
                        Codice
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="10" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodice" runat="server" ControlToValidate="TextBoxCodice"
                            ValidationGroup="nuovoCorso" ErrorMessage="Codice del corso mancante">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Descrizione
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDescrizione" runat="server" MaxLength="255" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescrizione" runat="server"
                            ControlToValidate="TextBoxDescrizione" ValidationGroup="nuovoCorso" ErrorMessage="Descrizione del corso mancante">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Versione
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxVersione" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Iscrizione libera
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBoxIscrizioneLibera" runat="server" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Durata complessiva (ore)
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxDurataComplessiva" runat="server" MaxLength="50" Width="300px"
                            Enabled="False" CssClass="campoDisabilitato"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDurataComplessiva" runat="server"
                            ControlToValidate="TextBoxDurataComplessiva" ValidationGroup="nuovoCorso" ErrorMessage="Durata del corso mancante">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidatorDurataComplessiva" runat="server" Type="Integer"
                            ControlToValidate="TextBoxDurataComplessiva" MinimumValue="1" MaximumValue="10000"
                            ValidationGroup="nuovoCorso" ErrorMessage="Durata del corso non valida">*</asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Moduli
                    </td>
                    <td>
                        <asp:GridView ID="GridViewModuli" runat="server" AutoGenerateColumns="False" Width="100%">
                            <Columns>
                                <asp:BoundField HeaderText="Descrizione" DataField="Descrizione" />
                                <asp:BoundField HeaderText="Durata (ore)" DataField="OreDurata">
                                    <ItemStyle Width="80px" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                Nessun modulo inserito
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:Button ID="ButtonNuovoModulo" runat="server" Text="Nuovo modulo..." Width="150px"
                            OnClick="ButtonNuovoModulo_Click" />
                    </td>
                    <td>
                        <asp:CustomValidator ID="CustomValidatorModuli" runat="server" ValidationGroup="nuovoCorso"
                            ErrorMessage="Inserire almeno un modulo" OnServerValidate="CustomValidatorModuli_ServerValidate1">*</asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="2">
                        <asp:Panel ID="PanelNuovoModulo" runat="server" Visible="false" Width="100%" DefaultButton="ButtonInserimentoNuovoModulo">
                            <table class="filledtable">
                                <tr>
                                    <td style="height: 16px">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Nuovo modulo"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table class="borderedTable">
                                <tr>
                                    <td>
                                        Descrizione
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxModuloDescrizione" runat="server" Width="300px" MaxLength="255"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorModuloDescrizione" runat="server"
                                            ControlToValidate="TextBoxModuloDescrizione" ValidationGroup="nuovoModulo" ErrorMessage="Descrizione del modulo mancante">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Durata (ore)
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxModuloDurata" runat="server" Width="300px" MaxLength="5"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RangeValidator ID="RangeValidator" runat="server" ControlToValidate="TextBoxModuloDurata"
                                            Type="Integer" MinimumValue="1" MaximumValue="99999" ErrorMessage="Durata del modulo non valida"
                                            ValidationGroup="nuovoModulo">*</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="ButtonInserimentoNuovoModulo" runat="server" Text="Inserisci modulo"
                                            Width="150px" OnClick="ButtonInserimentoNuovoModulo_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:ValidationSummary ID="ValidationSummaryNuovoModulo" runat="server" ValidationGroup="nuovoModulo"
                                            CssClass="messaggiErrore" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button ID="ButtonInserimentoCorso" runat="server" Width="150px" Text="Inserisci corso"
                            ValidationGroup="nuovoCorso" OnClick="ButtonInserimentoCorso_Click1" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:ValidationSummary ID="ValidationSummaryCorso" runat="server" ValidationGroup="nuovoCorso"
                            CssClass="messaggiErrore" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
