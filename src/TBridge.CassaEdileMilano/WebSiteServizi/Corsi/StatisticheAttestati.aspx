<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StatisticheAttestati.aspx.cs" Inherits="Corsi_StatisticheAttestati" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Statistiche attestati" />
    <br />
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td class="centermain">
                    Dal:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDa" Width="100%" runat="server" />
                </td>
                <td class="centermain">
                    Al:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataA" Width="100%" runat="server" />
                </td>
            </tr>
        </table>
        <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
            Text="Visualizza report" ValidationGroup="VisualizzazioneReport" />
        <br />
        <asp:CompareValidator ID="CompareValidatorData" ControlToValidate="RadDatePickerDataA"
            ControlToCompare="RadDatePickerDataDa" Operator="GreaterThanEqual" ErrorMessage="La data di fine non pu� essere precedente a quella di inizio"
            ValidationGroup="VisualizzazioneReport" runat="server" Type="Date" />
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataDa" ControlToValidate="RadDatePickerDataDa"
            ErrorMessage="Data inizio ricerca assente." ValidationGroup="VisualizzazioneReport"
            runat="server" />
        <br />
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorA" ControlToValidate="RadDatePickerDataA"
            ErrorMessage="Data fine ricerca assente." ValidationGroup="VisualizzazioneReport"
            runat="server" />
    </div>
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerCorsi" runat="server" ProcessingMode="Remote"
                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" ShowPageNavigationControls="False"
                    Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
</asp:Content>
