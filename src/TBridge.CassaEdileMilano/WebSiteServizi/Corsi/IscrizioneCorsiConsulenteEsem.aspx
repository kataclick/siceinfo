<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IscrizioneCorsiConsulenteEsem.aspx.cs" Inherits="Corsi_IscrizioneCorsiConsulenteEsem"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CorsiRicercaLavoratore.ascx" TagName="CorsiRicercaLavoratore"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CorsiSelezioneProgrammazione.ascx" TagName="CorsiSelezioneProgrammazione"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc4" %>
<%@ Register Src="WebControls/CorsiDatiLavoratoreEsem.ascx" TagName="CorsiDatiLavoratoreEsem"
    TagPrefix="uc5" %>
<%@ Register Src="../WebControls/CorsiRicercaImpresa.ascx" TagName="CorsiRicercaImpresa"
    TagPrefix="uc6" %>
<%@ Register Src="../WebControls/CorsiDatiImpresa.ascx" TagName="CorsiDatiImpresa"
    TagPrefix="uc7" %>
<%@ Register Src="../WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc8" %>
<%@ Register src="../WebControls/CorsiRicercaLavoratoreAnagraficaCondivisa.ascx" tagname="CorsiRicercaLavoratoreAnagraficaCondivisa" 
    TagPrefix="uc9" %>
<%@ Register src="../WebControls/CorsiRicercaImpresaAnagraficaCondivisa.ascx" tagname="CorsiRicercaImpresaAnagraficaCondivisa" tagprefix="uc10" %>
<%@ Register src="../WebControls/ConsulenteSelezioneImpresa.ascx" tagname="ConsulenteSelezioneImpresa" tagprefix="uc11" %>
<%@ Register src="WebControls/CorsiSelezioneProgrammazioneEsem.ascx" tagname="CorsiSelezioneProgrammazioneEsem" tagprefix="uc12" %>
<%@ Register src="../WebControls/MenuCorsiEsem.ascx" tagname="MenuCorsiEsem" tagprefix="uc13" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc13:MenuCorsiEsem ID="MenuCorsiEsem1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Iscrizione lavoratori" />
    <br />
    Bla bla bla
    <br />
    Contatto*: <asp:TextBox ID="TextBoxContatto" runat="server" MaxLength="255" Width="200px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionContatto" runat="server" ControlToValidate="TextBoxContatto" ValidationGroup="iscrizione" ErrorMessage="Email di contatto non valida" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$">*</asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidatorContatto" runat="server" ControlToValidate="TextBoxContatto" ValidationGroup="iscrizione" ErrorMessage="Email di contatto mancante">*</asp:RequiredFieldValidator>
    <br />
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Impresa"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    <uc11:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" 
                        runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <asp:MultiView ID="MultiviewIscrizione" runat="server">
    <asp:View ID="ViewIscrizione" runat="server">
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Corso"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    <uc12:CorsiSelezioneProgrammazioneEsem ID="CorsiSelezioneProgrammazioneEsem1" 
                        runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Lavoratore"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td style="width: 100px">
                    Lavoratore
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLavoratore" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox>
                    <asp:Button ID="ButtonSelezionaLavoratore" runat="server" Text="Seleziona lavoratore"
                        Width="200px" Enabled="False" OnClick="ButtonSelezionaLavoratore_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="MultiViewLavoratore" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewLavoratoreSelezione" runat="server">
                            <uc2:CorsiRicercaLavoratore ID="CorsiRicercaLavoratore1" runat="server" Visible="false" />
                            <uc9:CorsiRicercaLavoratoreAnagraficaCondivisa ID="CorsiRicercaLavoratoreAnagraficaCondivisa1" 
                                runat="server" />
                        </asp:View>
                        <asp:View ID="ViewLavoratoreNuovo" runat="server">
                            <uc5:CorsiDatiLavoratoreEsem ID="CorsiDatiLavoratoreEsem1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummaryDatiLavoratore" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="datiLavoratore" />
                            <asp:Button ID="ButtonNuovoLavoratore" runat="server" Text="Salva lavoratore" Width="200px"
                                ValidationGroup="datiLavoratore" OnClick="ButtonNuovoLavoratore_Click" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </div>
    <br />
   <asp:CustomValidator ID="CustomValidatorProgrammazioni" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Corso non selezionato" OnServerValidate="CustomValidatorProgrammazioni_ServerValidate">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidatorLavoratore" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Lavoratore non selezionato/inserito o dati mancanti (effettuare nuovamente la selezione del lavoratore)" OnServerValidate="CustomValidatorLavoratore_ServerValidate">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Impresa non selezionata/inserita" OnServerValidate="CustomValidatorImpresa_ServerValidate">&nbsp;</asp:CustomValidator>
    <br />
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red" Text="Iscrizione effettuata correttamente"
        Visible="False"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummaryIscrizione" runat="server" ValidationGroup="iscrizione"
        CssClass="messaggiErrore" />
    <br />
    <asp:Button ID="ButtonIscriviLavoratore" runat="server" Text="Iscrivi lavoratore"
        ValidationGroup="iscrizione" OnClick="ButtonIscriviLavoratore_Click" />
    <br />
    </asp:View>
    <asp:View ID="ViewNoIscrizione" runat="server">
        <p><b>ATTENZIONE!!!</b></p>
        <p>L'impresa risulta irregolare per BNI.<br />
        Non � possibile proseguire con l'iscrizione.<br /></p>
    </asp:View>
    </asp:MultiView>
</asp:Content>
