using System;
using System.Web.UI;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;

public partial class Corsi_PianificazioneCorsi : Page
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione, "PianificazioneCorsi.aspx");

        #endregion

        CorsiRicercaPianificazioni1.OnProgrammazioneSelected += CorsiRicercaPianificazioni1_OnProgrammazioneSelected;

        if (!Page.IsPostBack)
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCorso,
                biz.GetCorsiPianificabili(),
                "NomeCompleto",
                "IdCorso");
        }
    }

    protected void ButtonNuovaPianificazione_Click(object sender, EventArgs e)
    {
        PaneNuovaPianificazione.Visible = true;
        PaneNuovaPianificazione.Enabled = true;
        PanelAggiornamento.Visible = false;
        DropDownListCorso.Visible = true;
        LabelTitolo.Text = "Nuova pianificazione";

        ResetSelezioneCorsoEDati();
    }

    private void ResetSelezioneCorsoEDati()
    {
        DropDownListCorso.SelectedIndex = 0;
        Presenter.SvuotaCampo(LabelCorso);
        CorsiDatiPianificazione1.SvuotaCampi();
        PanelDatiNuovaPianificazione.Visible = false;
    }

    protected void DropDownListCorso_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(DropDownListCorso.SelectedValue))
        {
            Int32 idCorso = Int32.Parse(DropDownListCorso.SelectedValue);
            CorsiDatiPianificazione1.CaricaCorso(idCorso);

            PanelDatiNuovaPianificazione.Visible = true;
        }
    }

    protected void ButtonSalvaPianificazione_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Programmazione programmazione = CorsiDatiPianificazione1.GetPianificazioni();

            if (biz.InsertUpdateProgrammazione(programmazione))
            {
                CorsiDatiPianificazione1.SvuotaCampi();
                PanelDatiNuovaPianificazione.Visible = false;
                PaneNuovaPianificazione.Visible = false;

                CorsiRicercaPianificazioni1.ForzaRicerca();
            }
        }
    }

    private void CorsiRicercaPianificazioni1_OnProgrammazioneSelected(Int32 idProgrammazione,
                                                                      Int32 idProgrammazioneModulo)
    {
        DropDownListCorso.Visible = false;
        LabelMessaggioModificaVietata.Visible = false;

        // Modifica della programmazione
        PanelAggiornamento.Visible = true;
        ProgrammazioneModuloCollection prog = biz.GetProgrammazione(idProgrammazione);
        CorsiDatiPianificazione1.CaricaProgrammazioni(prog);
        ViewState["IdProgrammazione"] = idProgrammazione;

        if (prog.Count > 0)
        {
            LabelCorso.Text = prog[0].Corso.Descrizione;

            if (prog.EsisteRegistroConfermato() || prog.EsistonoIscritti())
            {
                PaneNuovaPianificazione.Enabled = false;
                LabelMessaggioModificaVietata.Visible = true;
            }
            else
            {
                PaneNuovaPianificazione.Enabled = true;
            }
        }

        PaneNuovaPianificazione.Visible = true;
        PanelDatiNuovaPianificazione.Visible = true;
        LabelTitolo.Text = "Aggiorna pianificazione";
    }

    protected void ButtonEliminaPianificazione_Click(object sender, EventArgs e)
    {
        Int32 idProgrammazione = (Int32) ViewState["IdProgrammazione"];

        if (biz.DeleteProgrammazione(idProgrammazione))
        {
            CorsiDatiPianificazione1.SvuotaCampi();
            PanelDatiNuovaPianificazione.Visible = false;
            PaneNuovaPianificazione.Visible = false;

            CorsiRicercaPianificazioni1.ForzaRicerca();
        }
    }
}