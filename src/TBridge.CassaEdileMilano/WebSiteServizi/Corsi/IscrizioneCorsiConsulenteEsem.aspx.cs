using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using Impresa=TBridge.Cemi.Corsi.Type.Entities.Impresa;
using ImpresaIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using Lavoratore=TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using LavoratoreIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;
using UtenteConsulente = TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.Business;
using System.Collections.Generic;

public partial class Corsi_IscrizioneCorsiConsulenteEsem : Page
{
    private const Int32 INDICENUOVAIMPRESA = 1;
    private const Int32 INDICENUOVOLAVORATORE = 1;
    private const Int32 INDICESELEZIONEIMPRESA = 0;
    private const Int32 INDICESELEZIONELAVORATORE = 0;
    private readonly CorsiBusiness biz = new CorsiBusiness();
    private readonly AnagraficaCondivisaManager anagCondManager = new AnagraficaCondivisaManager();
    private readonly CorsiEsemBusiness corsiEsemBiz = new CorsiEsemBusiness();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiIscrizioneConsulente,
                                              "IscrizioneCorsiConsulente.aspx");

        #endregion

        #region Eventi dei controlli custom

        CorsiRicercaLavoratore1.OnLavoratoreSelected += CorsiRicercaLavoratore1_OnLavoratoreSelected;
        CorsiRicercaLavoratore1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;

        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreSelected += new TBridge.Cemi.Corsi.Type.Delegates.LavoratoreSelectedEventHandler(CorsiRicercaLavoratoreAnagraficaCondivisa1_OnLavoratoreSelected);
        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;
        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreRicerca += CorsiRicercaLavoratore1_OnLavoratoreRicerca;

        ConsulenteSelezioneImpresa1.OnImpresaSelected += new TBridge.Cemi.Type.Delegates.ImpresaSelectedEventHandler(ConsulenteSelezioneImpresa1_OnImpresaSelected);

        #endregion

        if (!Page.IsPostBack)
        {
            UtenteConsulente consulente = (UtenteConsulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            TextBoxContatto.Text= consulente.EMail;

            if (Context.Items["lavoratoreIscrizioneLavoratori"] != null)
            {
                LavoratoreIscrizioneLavoratori lavIscri =
                    (LavoratoreIscrizioneLavoratori) Context.Items["lavoratoreIscrizioneLavoratori"];

                Lavoratore lav = new Lavoratore();
                lav.TipoLavoratore = lavIscri.TipoLavoratore == TBridge.Cemi.IscrizioneLavoratori.Type.Enums.TipologiaLavoratore.SiceNew ? TipologiaLavoratore.SiceNew : TipologiaLavoratore.Anagrafica;
                lav.IdLavoratore = lavIscri.IdLavoratore;
                lav.Nome = lavIscri.Nome;
                lav.Cognome = lavIscri.Cognome;
                lav.CodiceFiscale = lavIscri.CodiceFiscale;
                lav.DataNascita = lavIscri.DataNascita;
                lav.TipoLavoratore = (TipologiaLavoratore) lavIscri.TipoLavoratore;

                SelezionaLavoratore(lav);
            }

            Int32 idImpresaSel = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
            if (idImpresaSel > 0)
            {
                if (commonBiz.IsImpresaIrregolareBNI(idImpresaSel))
                {
                    MultiviewIscrizione.SetActiveView(ViewNoIscrizione);
                }
                else
                {
                    MultiviewIscrizione.SetActiveView(ViewIscrizione);
                }

                List<TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Impresa> imprese = anagCondManager.GetImpreseByFilter(new TBridge.Cemi.Type.Filters.AnagraficaCondivisa.ImpresaFilter() { CodiceCassaEdile = idImpresaSel });

                if (imprese != null && imprese.Count == 1)
                {
                    ViewState["Impresa"] = imprese[0];
                    CorsiRicercaLavoratoreAnagraficaCondivisa1.IdImpresa((int)imprese[0].CodiceCassaEdile);
                    ViewState["IdImpresa"] = imprese[0].CodiceCassaEdile;


                    
                }
            }
        }

    }

 

    void ConsulenteSelezioneImpresa1_OnImpresaSelected(int idImpresa, string codiceRagioneSociale)
    {

        if (commonBiz.IsImpresaIrregolareBNI(idImpresa))
        {
            MultiviewIscrizione.SetActiveView(ViewNoIscrizione);
        }
        else
        {
            MultiviewIscrizione.SetActiveView(ViewIscrizione);
        }

        List<TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Impresa> imprese = anagCondManager.GetImpreseByFilter(new TBridge.Cemi.Type.Filters.AnagraficaCondivisa.ImpresaFilter() { CodiceCassaEdile = idImpresa });

        if (imprese != null && imprese.Count == 1)
        {
            ViewState["Impresa"] = imprese[0];
            CorsiRicercaLavoratoreAnagraficaCondivisa1.IdImpresa((int)imprese[0].CodiceCassaEdile);
            
        }

        
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreNuovo()
    {
        TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezioneProgrammazioneEsem1.GetCorsoSelezionato();
        // DA CONTROLLARE CODICE CORSO!!
        if (corso != null && corso.Codice == "16OREMICS")
        {
            MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
            ButtonSelezionaLavoratore.Enabled = true;
        }
        else
        {
            CorsiRicercaLavoratoreAnagraficaCondivisa1.LavoratoreNuovoErrore();
        }
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreRicerca()
    {
        TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezioneProgrammazioneEsem1.GetCorsoSelezionato();
        // DA CONTROLLARE CODICE CORSO!!
        if (corso != null && corso.Codice == "16OREMICS")
        {
            CorsiRicercaLavoratoreAnagraficaCondivisa1.InForza(false);
        }
        else
        {
            CorsiRicercaLavoratoreAnagraficaCondivisa1.InForza(true);
        }

    }

    private void CorsiRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        SelezionaLavoratore(lavoratore);
    }

    protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
    {
        MultiViewLavoratore.Visible = true;
        MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
        ButtonSelezionaLavoratore.Enabled = false;
    }

    protected void ButtonNuovoLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Lavoratore lavoratore = CorsiDatiLavoratoreEsem1.GetLavoratore();

            if (!biz.EsisteLavoratoreConStessoCodiceFiscale(lavoratore.CodiceFiscale))
            {
                SelezionaLavoratore(lavoratore);
                CorsiDatiLavoratoreEsem1.ResetCampi();
            }
            else
            {
                MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
                CorsiRicercaLavoratore1.ForzaRicerca(lavoratore.CodiceFiscale);
            }
        }
    }

    protected void ButtonIscriviLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String messaggio;
            Iscrizione iscrizione = CreaIscrizione();
            LabelMessaggio.Visible = false;

            Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            if (corsiEsemBiz.IscriviLavoratore(out messaggio, utente, iscrizione))
            {
                LabelMessaggio.Text = messaggio;
                LabelMessaggio.Visible = true;
            }
            else
            {
                LabelMessaggio.Text = messaggio;
                LabelMessaggio.Visible = true;
            }


        }
    }

    private void SelezionaLavoratore(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        MultiViewLavoratore.Visible = false;
        ButtonSelezionaLavoratore.Enabled = true;
        

        if (lavoratore != null)
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
            else
            {
                TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }

            //if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            //{
            //    LanciaRicercaUltimaImpresaDisponibile(lavoratore.IdLavoratore.Value);
            //}
        }
        else
        {
            TextBoxLavoratore.Text = String.Empty;
        }
    }

    private void ResetCampi()
    {
        ViewState["Lavoratore"] = null;
        Presenter.SvuotaCampo(TextBoxLavoratore);
        CorsiDatiLavoratoreEsem1.ResetCampi();

        ViewState["Impresa"] = null;


    }

    private Iscrizione CreaIscrizione()
    {
        Iscrizione iscrizione = new Iscrizione();

        iscrizione.Corso = CorsiSelezioneProgrammazioneEsem1.GetCorsoSelezionato();
        iscrizione.Lavoratore = (Lavoratore) ViewState["Lavoratore"];

        TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Impresa impresa = (TBridge.Cemi.Type.Domain.AnagraficaCondivisa.Impresa)ViewState["Impresa"];
        iscrizione.Impresa = new Impresa();
        iscrizione.Impresa.IdImpresa = impresa.CodiceCassaEdile;
        iscrizione.Impresa.IdAnagraficaCondivisa = impresa.IdImpresa;
        iscrizione.Impresa.PartitaIva = impresa.PartitaIVA;
        iscrizione.Impresa.CodiceFiscale = impresa.CodiceFiscale;

        if (!String.IsNullOrWhiteSpace(TextBoxContatto.Text))
        {
            iscrizione.Contatto = TextBoxContatto.Text;
        }

        return iscrizione;
    }

    void CorsiRicercaLavoratoreAnagraficaCondivisa1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        SelezionaLavoratoreAnagraficaCondivisa(lavoratore);
    }

    private void SelezionaLavoratoreAnagraficaCondivisa(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        MultiViewLavoratore.Visible = false;
        ButtonSelezionaLavoratore.Enabled = true;

        if (lavoratore != null)
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
            else
            {
                //TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                //                                       lavoratore.Cognome,
                //                                       lavoratore.Nome,
                //                                       lavoratore.DataNascita.ToShortDateString(),
                //                                       lavoratore.CodiceFiscale,
                //                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                //                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
                MultiViewLavoratore.Visible = true;
                MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
                CorsiDatiLavoratoreEsem1.CaricaDatiLavoratore(lavoratore);
            }

            //if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            //{
            //    LanciaRicercaUltimaImpresaDisponibile(lavoratore.IdLavoratore.Value);
            //}
        }
        else
        {
            TextBoxLavoratore.Text = String.Empty;
        }
    }

    #region Custom Validators

    protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Lavoratore"] != null)
        {
            TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezioneProgrammazioneEsem1.GetCorsoSelezionato();
            //VERIFICARE CODICE CORSO!!!
            if (corso == null || corso.Codice != "16OREMICS")
            {
                args.IsValid = true;
            }
            else
            {
                Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
                if (lavoratore.PrimaEsperienza.HasValue && lavoratore.DataAssunzione.HasValue)
                {
                    args.IsValid = true;
                }
                else
                {
                    args.IsValid = false;
                }
            }

        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Impresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        TBridge.Cemi.Corsi.Type.Entities.Esem.Corso corso = CorsiSelezioneProgrammazioneEsem1.GetCorsoSelezionato();

        if (corso != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    #endregion
}