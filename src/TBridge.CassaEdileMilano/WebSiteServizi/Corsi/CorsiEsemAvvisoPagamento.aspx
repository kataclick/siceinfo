﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CorsiEsemAvvisoPagamento.aspx.cs" Inherits="Corsi_CorsiEsemAvvisoPagamento" EnableViewState="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body id="body1" runat="server">
    <form id="form1" runat="server">

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refresh(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function Cancel() {
            GetRadWindow().close();
        }
    </script>

    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server"></telerik:RadScriptManager>
        <asp:Label runat="server" ID="LabelCorsoPagamento"></asp:Label>
        <!-- <p style="text-align:center"><b>ATTENZIONE!</b><br />Il corso per il quale si sta effettuando l'iscrizione comporterà un costo.<br />Procedere con l'iscrizione?</p> -->
        <p style="text-align:center"><asp:Button ID="BtnCorsoPagamentoYes" Text="Sì" runat="server" onclick="BtnCorsoPagamentoYes_Click" Width="80" /> &nbsp; <asp:Button ID="BtnCorsoPagamentoNo" Text="No" runat="server" Width="80" OnClientClick="Cancel()" /></p>
        <p style="text-align:center"><asp:Button ID="BtnCorsoPagamentoOk" Text="Ok" runat="server" Width="80" OnClientClick="Cancel()" Visible="false" /></p>
    </div>
    </form>
</body>
</html>
