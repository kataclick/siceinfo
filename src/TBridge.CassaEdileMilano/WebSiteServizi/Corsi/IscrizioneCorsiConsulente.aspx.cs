using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using Impresa=TBridge.Cemi.Corsi.Type.Entities.Impresa;
using ImpresaIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using Lavoratore=TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using LavoratoreIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;

public partial class Corsi_IscrizioneCorsiConsulente : Page
{
    private const Int32 INDICENUOVAIMPRESA = 1;
    private const Int32 INDICENUOVOLAVORATORE = 1;
    private const Int32 INDICESELEZIONEIMPRESA = 0;
    private const Int32 INDICESELEZIONELAVORATORE = 0;
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiIscrizioneConsulente,
                                              "IscrizioneCorsiConsulente.aspx");

        #endregion

        #region Eventi dei controlli custom

        CorsiRicercaLavoratore1.OnLavoratoreSelected += CorsiRicercaLavoratore1_OnLavoratoreSelected;
        CorsiRicercaLavoratore1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;
        CorsiRicercaImpresa1.OnImpresaSelected += CorsiRicercaImpresa1_OnImpresaSelected;
        CorsiRicercaImpresa1.OnImpresaNuova += CorsiRicercaImpresa1_OnImpresaNuova;

        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreSelected += new TBridge.Cemi.Corsi.Type.Delegates.LavoratoreSelectedEventHandler(CorsiRicercaLavoratoreAnagraficaCondivisa1_OnLavoratoreSelected);
        CorsiRicercaLavoratoreAnagraficaCondivisa1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;

        CorsiRicercaImpresaAnagraficaCondivisa1.OnImpresaSelected += new TBridge.Cemi.Corsi.Type.Delegates.ImpresaSelectedEventHandler(CorsiRicercaImpresaAnagraficaCondivisa1_OnImpresaSelected);
        CorsiRicercaImpresaAnagraficaCondivisa1.OnImpresaNuova += CorsiRicercaImpresa1_OnImpresaNuova;

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["impresaIscrizioneLavoratori"] != null)
            {
                ImpresaIscrizioneLavoratori impIscri =
                    (ImpresaIscrizioneLavoratori) Context.Items["impresaIscrizioneLavoratori"];

                Impresa imp = new Impresa();
                imp.IdImpresa = impIscri.IdImpresa;
                imp.RagioneSociale = impIscri.RagioneSociale;
                imp.PartitaIva = impIscri.PartitaIva;
                imp.CodiceFiscale = impIscri.CodiceFiscale;
                imp.TipoImpresa = TipologiaImpresa.SiceNew;

                SelezionaImpresa(imp);
            }

            if (Context.Items["lavoratoreIscrizioneLavoratori"] != null)
            {
                LavoratoreIscrizioneLavoratori lavIscri =
                    (LavoratoreIscrizioneLavoratori) Context.Items["lavoratoreIscrizioneLavoratori"];

                Lavoratore lav = new Lavoratore();
                lav.TipoLavoratore = lavIscri.TipoLavoratore == TBridge.Cemi.IscrizioneLavoratori.Type.Enums.TipologiaLavoratore.SiceNew ? TipologiaLavoratore.SiceNew : TipologiaLavoratore.Anagrafica;
                lav.IdLavoratore = lavIscri.IdLavoratore;
                lav.Nome = lavIscri.Nome;
                lav.Cognome = lavIscri.Cognome;
                lav.CodiceFiscale = lavIscri.CodiceFiscale;
                lav.DataNascita = lavIscri.DataNascita;
                lav.TipoLavoratore = (TipologiaLavoratore) lavIscri.TipoLavoratore;

                SelezionaLavoratore(lav);
            }
        }

    }

    private void CorsiRicercaImpresa1_OnImpresaNuova()
    {
        MultiViewImpresa.ActiveViewIndex = INDICENUOVAIMPRESA;
        ButtonSelezionaImpresa.Enabled = true;
        //CorsiDatiImpresa1.ResetCampi();
    }

    private void CorsiRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        SelezionaImpresa(impresa);
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreNuovo()
    {
        MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
        ButtonSelezionaLavoratore.Enabled = true;
    }

    private void CorsiRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        SelezionaLavoratore(lavoratore);
    }

    protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
    {
        MultiViewLavoratore.Visible = true;
        MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
        ButtonSelezionaLavoratore.Enabled = false;
    }

    protected void ButtonSelezionaImpresa_Click(object sender, EventArgs e)
    {
        MultiViewImpresa.Visible = true;
        MultiViewImpresa.ActiveViewIndex = INDICESELEZIONEIMPRESA;
        ButtonSelezionaImpresa.Enabled = false;
    }

    protected void ButtonNuovoLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Lavoratore lavoratore = CorsiDatiLavoratore1.GetLavoratore();

            if (!biz.EsisteLavoratoreConStessoCodiceFiscale(lavoratore.CodiceFiscale))
            {
                SelezionaLavoratore(lavoratore);
                CorsiDatiLavoratore1.ResetCampi();
            }
            else
            {
                MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
                CorsiRicercaLavoratore1.ForzaRicerca(lavoratore.CodiceFiscale);
            }
        }
    }

    protected void ButtonNuovaImpresa_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Impresa impresa = CorsiDatiImpresa1.GetImpresa();

            if (!biz.EsisteImpresaConStessaIvaFisc(impresa.PartitaIva, impresa.CodiceFiscale))
            {
                SelezionaImpresa(impresa);
                CorsiDatiImpresa1.ResetCampi();
            }
            else
            {
                MultiViewImpresa.ActiveViewIndex = INDICESELEZIONEIMPRESA;
                CorsiRicercaImpresa1.ForzaRicerca(impresa.PartitaIva);
            }
        }
    }

    private void SelezionaImpresa(Impresa impresa)
    {
        ViewState["Impresa"] = impresa;

        MultiViewImpresa.Visible = false;
        ButtonSelezionaImpresa.Enabled = true;

        if (impresa != null)
        {
            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                TextBoxImpresa.Text = String.Format("{0} {1}\n{2}\n{3}",
                                                    impresa.IdImpresa,
                                                    impresa.RagioneSociale,
                                                    impresa.PartitaIva,
                                                    impresa.CodiceFiscale);
            }
            else
            {
                TextBoxImpresa.Text = String.Format("{0}\n{1}\n{2}",
                                                    impresa.RagioneSociale,
                                                    impresa.PartitaIva,
                                                    impresa.CodiceFiscale);
            }
        }
        else
        {
            TextBoxImpresa.Text = String.Empty;
        }
    }

    protected void ButtonIscriviLavoratore_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            LabelMessaggio.Visible = false;
            Partecipazione partecipazione = CreaPartecipazione();

            String codiceFiscaleLavoratore = partecipazione.Lavoratore.CodiceFiscale;
            String codiceCorso = partecipazione.Corso.Codice;

            if (codiceCorso == "8 ORE"
                && !Label16OreNonPresente.Visible
                && !biz.LavoratoreSeguito16Ore(codiceFiscaleLavoratore))
            {
                Label16OreNonPresente.Visible = true;
            }
            else
            {
                Label16OreNonPresente.Visible = false;

                try
                {
                    if (biz.InsertPartecipazione(partecipazione, true))
                    {
                        ResetCampi();
                        LabelMessaggio.Visible = true;

                        Context.Items["IdPartecipazione"] = partecipazione.IdPartecipazione;
                        Context.Items["Cognome"] = partecipazione.Lavoratore.Cognome;
                        Context.Items["Nome"] = partecipazione.Lavoratore.Nome;
                        if (partecipazione.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                        {
                            Context.Items["CodiceImpresa"] = partecipazione.Impresa.IdImpresa;
                        }
                        Context.Items["RagioneSociale"] = partecipazione.Impresa.RagioneSociale;
                        Server.Transfer("~/Corsi/RicevutaIscrizione.aspx");
                    }
                }
                catch (LavoratoreGiaIscrittoException exc1)
                {
                    LabelLavoratoreGiaIscritto.Visible = true;
                }
                catch (DisponibilitaEsauritaException exc2)
                {
                    LabelDisponibilitaEsaurita.Visible = true;
                }
            }
        }
    }

    private void SelezionaLavoratore(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        MultiViewLavoratore.Visible = false;
        ButtonSelezionaLavoratore.Enabled = true;
        Label16OreNonPresente.Visible = false;

        if (lavoratore != null)
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
            else
            {
                TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }

            //if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            //{
            //    LanciaRicercaUltimaImpresaDisponibile(lavoratore.IdLavoratore.Value);
            //}
        }
        else
        {
            TextBoxLavoratore.Text = String.Empty;
        }
    }

    private void ResetCampi()
    {
        ViewState["Lavoratore"] = null;
        Presenter.SvuotaCampo(TextBoxLavoratore);
        CorsiDatiLavoratore1.ResetCampi();

        ViewState["Impresa"] = null;
        Presenter.SvuotaCampo(TextBoxImpresa);
        CorsiDatiImpresa1.ResetCampi();

        LabelLavoratoreGiaIscritto.Visible = false;
        LabelDisponibilitaEsaurita.Visible = false;
    }

    private Partecipazione CreaPartecipazione()
    {
        Partecipazione partecipazione = new Partecipazione();
        partecipazione.PartecipazioneModuli = new PartecipazioneModuloCollection();
        ProgrammazioneModuloCollection programmazioni = CorsiSelezioneProgrammazione1.GetProgrammazioniSelezionate();

        partecipazione.Lavoratore = (Lavoratore) ViewState["Lavoratore"];
        partecipazione.Impresa = (Impresa) ViewState["Impresa"];
        partecipazione.PrenotazioneImpresa = false;
        partecipazione.PrenotazioneConsulente = true;
        partecipazione.IdConsulente =
            ((Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdConsulente;

        partecipazione.Corso = CorsiSelezioneProgrammazione1.GetCorsoSelezionato();

        foreach (ProgrammazioneModulo prog in programmazioni)
        {
            PartecipazioneModulo partecipazioneModulo = new PartecipazioneModulo();
            partecipazione.PartecipazioneModuli.Add(partecipazioneModulo);

            partecipazioneModulo.Programmazione = prog;
        }

        return partecipazione;
    }

    void CorsiRicercaImpresaAnagraficaCondivisa1_OnImpresaSelected(Impresa impresa)
    {
        SelezionaImpresaAnagraficaCondivisa(impresa);
    }

    void CorsiRicercaLavoratoreAnagraficaCondivisa1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        SelezionaLavoratoreAnagraficaCondivisa(lavoratore);
    }

    private void SelezionaImpresaAnagraficaCondivisa(Impresa impresa)
    {
        ViewState["Impresa"] = impresa;

        MultiViewImpresa.Visible = false;
        ButtonSelezionaImpresa.Enabled = true;

        if (impresa != null)
        {
            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                TextBoxImpresa.Text = String.Format("{0} {1}\n{2}\n{3}",
                                                    impresa.IdImpresa,
                                                    impresa.RagioneSociale,
                                                    impresa.PartitaIva,
                                                    impresa.CodiceFiscale);
            }
            else
            {
                //TextBoxImpresa.Text = String.Format("{0}\n{1}\n{2}",
                //                                    impresa.RagioneSociale,
                //                                    impresa.PartitaIva,
                //                                    impresa.CodiceFiscale);
                MultiViewImpresa.Visible = true;
                MultiViewImpresa.ActiveViewIndex = INDICENUOVAIMPRESA;
                CorsiDatiImpresa1.CaricaDatiImpresa(impresa);
            }
        }
        else
        {
            TextBoxImpresa.Text = String.Empty;
        }
    }

    private void SelezionaLavoratoreAnagraficaCondivisa(Lavoratore lavoratore)
    {
        ViewState["Lavoratore"] = lavoratore;

        MultiViewLavoratore.Visible = false;
        ButtonSelezionaLavoratore.Enabled = true;

        if (lavoratore != null)
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                       lavoratore.IdLavoratore,
                                                       lavoratore.Cognome,
                                                       lavoratore.Nome,
                                                       lavoratore.DataNascita.ToShortDateString(),
                                                       lavoratore.CodiceFiscale,
                                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
            }
            else
            {
                //TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                //                                       lavoratore.Cognome,
                //                                       lavoratore.Nome,
                //                                       lavoratore.DataNascita.ToShortDateString(),
                //                                       lavoratore.CodiceFiscale,
                //                                       lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "S�" : "No",
                //                                       lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
                MultiViewLavoratore.Visible = true;
                MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
                CorsiDatiLavoratore1.CaricaDatiLavoratore(lavoratore);
            }

            //if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            //{
            //    LanciaRicercaUltimaImpresaDisponibile(lavoratore.IdLavoratore.Value);
            //}
        }
        else
        {
            TextBoxLavoratore.Text = String.Empty;
        }
    }

    #region Custom Validators

    protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Lavoratore"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Impresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Partecipazione partecipazione = CreaPartecipazione();

        if (partecipazione != null && partecipazione.PartecipazioneModuli.Count != 0)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    #endregion
}