using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Corsi_RegistroPresenze : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione,
                                              "RegistroPresenze.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            ViewState["IdProgrammazioneModulo"] = Context.Items["IdProgrammazioneModulo"];
        }

        Int32 idProgrammazioneModulo = (Int32) ViewState["IdProgrammazioneModulo"];

        ReportViewerCorsi.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        ReportViewerCorsi.ServerReport.ReportPath = "/ReportCorsi/ReportRegistroPresenze";

        ReportParameter[] listaParam = new ReportParameter[1];
        listaParam[0] = new ReportParameter("idModuloProgrammazione", idProgrammazioneModulo.ToString());
        ReportViewerCorsi.ServerReport.SetParameters(listaParam);

        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string extension;

        //PDF

        byte[] bytes = ReportViewerCorsi.ServerReport.Render(
            "PDF", null, out mimeType, out encoding, out extension,
            out streamids, out warnings);

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/pdf";

        Response.AppendHeader("Content-Disposition", "attachment;filename=RegistroPresenze.pdf");
        Response.BinaryWrite(bytes);

        Response.Flush();
        Response.End();

    }
}