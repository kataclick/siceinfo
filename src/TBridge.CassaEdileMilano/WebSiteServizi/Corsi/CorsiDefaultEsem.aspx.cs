using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Corsi_CorsiDefaultEsem : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        
        funzionalita.Add(FunzionalitaPredefinite.CorsiVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneImpresa);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneLavoratore);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneConsulente);
        funzionalita.Add(FunzionalitaPredefinite.CorsiPrestazioniDTA);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}