﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AutenticazionePit.aspx.cs"
    Inherits="Corsi_AutenticazionePit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <img id="Img1" alt="header" src="~/images/banner1024.jpg" runat="server" />
        <table style="background-color: #242424; font-family: Arial; font-size: x-large;
            color: White;" width="97%">
            <tr>
                <td align="left" style="vertical-align: middle;">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/logo16oreSmall.jpg" />
                </td>
                <td align="center" valign="middle" style="font-size: 18pt; vertical-align: middle;">
                    PIT 16 Ore
                </td>
                <td align="right" style="vertical-align: middle;">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/logoformedil.jpg" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <br />
    <table width="100%" style="text-align: center">
        <tr>
            <td>
                <asp:Label ID="LabelLogin" runat="server" Text="Username"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxLogin" Width="190px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelPassword" runat="server" Text="Password"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxPassword" Width="190px" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="ButtonLogin" runat="server" OnClick="ButtonLogin_Click" Text="Username"
                    CausesValidation="False" Width="90px" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelResponse" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
