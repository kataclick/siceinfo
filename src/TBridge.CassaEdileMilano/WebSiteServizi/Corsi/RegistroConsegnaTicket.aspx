﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RegistroConsegnaTicket.aspx.cs" Inherits="Corsi_RegistroConsegnaTicket" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Registro Consegna Ticket" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerCorsi" runat="server" ProcessingMode="Remote"
                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" Height="550pt"
                    Width="550pt" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
