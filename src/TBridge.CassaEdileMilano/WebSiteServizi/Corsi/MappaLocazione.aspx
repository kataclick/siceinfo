<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MappaLocazione.aspx.cs" Inherits="Corsi_MappaLocazione" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.464044;
                var longitudine = 9.191567;
                var zoom = 17;

                function LoadMap(lat, lon) {
                    if (lat)
                        latitudine = lat;
                    if (lon)
                        longitudine = lon;

                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }
            </script>
        </telerik:RadCodeBlock>
        <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Mappa locazione" />
        <br />
        <uc3:PitHeader ID="PitHeader1" runat="server" />
        <br />
        <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
        </div>
        <br />
    </telerik:RadAjaxPanel>
</asp:Content>
