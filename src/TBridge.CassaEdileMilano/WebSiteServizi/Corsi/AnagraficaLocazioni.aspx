<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AnagraficaLocazioni.aspx.cs" Inherits="Corsi_AnagraficaLocazioni" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Anagrafica locazioni" />
    <br />
    <div style="width: 100%">
        <asp:GridView ID="GridViewLocazioni" runat="server" AutoGenerateColumns="False" Width="100%"
            DataKeyNames="IdLocazione" OnRowDataBound="GridViewLocazioni_RowDataBound" OnRowDeleting="GridViewLocazioni_RowDeleting">
            <Columns>
                <asp:BoundField DataField="Indirizzo" HeaderText="Indirizzo">
                    <ItemStyle Width="250px" />
                </asp:BoundField>
                <asp:BoundField DataField="Comune" HeaderText="Comune">
                    <ItemStyle Width="120px" />
                </asp:BoundField>
                <asp:BoundField DataField="Provincia" HeaderText="Provincia">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Note" HeaderText="Note" />
                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina"
                    ShowDeleteButton="True">
                    <ItemStyle Width="10px" />
                </asp:CommandField>
            </Columns>
            <EmptyDataTemplate>
                Nessuna locazione presente
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:Button ID="ButtonNuovaLocazione" runat="server" Text="Nuova locazione..." OnClick="ButtonNuovaLocazione_Click"
            Width="150px" />
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <div style="width: 100%">
        <asp:Panel ID="PanelNuovaLocazione" runat="server" Visible="false" Width="100%">
            <table class="standardTable">
                <tr>
                    <td colspan="3">
                        <b>Nuova locazione </b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Indirizzo*:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="300px" MaxLength="255"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndirizzo" runat="server" ControlToValidate="TextBoxIndirizzo"
                            ErrorMessage="Indirizzo mancante">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="TextBoxComune" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="TextBoxProvincia" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <!--<tr>
                    <td>
                        Latitudine:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxLatitudine" runat="server" Width="300px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Longitudine:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxLongitudine" runat="server" Width="300px" Enabled="false"></asp:TextBox>
                    </td>
                </tr>-->
                <tr>
                    <td>
                        Note:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="TextBoxNote" runat="server" Width="300px" TextMode="MultiLine" Height="50px"
                            MaxLength="500"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button ID="ButtonGeocodifica" runat="server" ValidationGroup="geocodificaLocazione"
                            Text="Cerca geocodifiche" OnClick="ButtonGeocodifica_Click" Width="150px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                            ValidationGroup="geocodificaLocazione" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="PanelSceltaIndirizzo" runat="server" Visible="false" Width="100%">
                            <table class="standardTable">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="LabelSceltaIndirizzo" runat="server" Text="Se l'indirizzo inserito � presente nella tabella sottostante cliccare su &quot;Seleziona&quot;. Se nessuno degli indirizzi proposti corrisponde cliccare su &quot;Conferma indirizzo dichiarato&quot;"></asp:Label>
                                        .
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Indirizzo dichiarato:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelIndirizzoFornito" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            OnSelectedIndexChanging="GridViewIndirizzi_SelectedIndexChanging" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="Provincia">
                                                    <ItemStyle Font-Size="XX-Small" Width="20px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Comune">
                                                    <ItemStyle Font-Size="XX-Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IndirizzoDenominazione">
                                                    <ItemStyle Font-Size="XX-Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Cap">
                                                    <ItemStyle Font-Size="XX-Small" Width="20px" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="20px" />
                                                    <ItemTemplate>
                                                        <asp:Button ID="ButtonSeleziona" runat="server" CommandName="Select" Font-Size="XX-Small"
                                                            Height="18px" Text="Seleziona" Width="56px" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                Non � stato trovato un indirizzo corrispondente nell&#39;anagrafica degli indirizzi.
                                                Se l&#39;indirizzo digitato � corretto cliccare su &quot;Conferma indirizzo dichiarato&quot;.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="ButtonAggiungiIndirizzo" runat="server" OnClick="ButtonAggiungiIndirizzo_Click"
                                            Text="Conferma indirizzo dichiarato" Width="200px" ValidationGroup="inserimentoIndirizzo" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
