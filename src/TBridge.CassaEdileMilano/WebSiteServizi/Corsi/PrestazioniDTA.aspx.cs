﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.Corsi.Type.Filters;
using System.IO;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using System.Text;

public partial class Corsi_PrestazioniDTA : System.Web.UI.Page
{
    private readonly CorsiBusiness corsiBiz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiPrestazioniDTA);
        #endregion

        if (!Page.IsPostBack)
        {
            TextBoxAnno.Text = DateTime.Now.Year.ToString();
            CaricaAnni();
        }
    }

    private void CaricaAnni()
    {
        Dictionary<Int32, String> anni = corsiBiz.GetPrestazioniDTAAnni();
        Presenter.CaricaElementiInDropDown(
            DropDownListAnno,
            anni,
            "Value",
            "Key");
    }

    protected void ButtonCarica_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ViewState["filtro"] = null;
            Presenter.SvuotaCampo(TextBoxCognome);

            CaricaPannelloPrestazioniDTA();
        }
    }

    private void CaricaPannelloPrestazioniDTA()
    {
        if (!String.IsNullOrEmpty(DropDownListAnno.SelectedValue))
        {
            //Int32 anno = Int32.Parse(TextBoxAnno.Text);
            Int32 anno = Int32.Parse(DropDownListAnno.SelectedValue);
            ViewState["anno"] = anno;

            // Verifico se le prestazioni per l'anno selezionato sono già state erogate
            DateTime? dataErogazione = corsiBiz.DataErogazionePrestazioniDTA(anno);
            ViewState["dataErogazione"] = dataErogazione;

            if (dataErogazione.HasValue)
            {
                LabelErogazione.Text = String.Format("Le prestazioni per l'anno formativo <b>{0}/{1}</b> sono state erogate il <b>{2}</b>",
                    anno,
                    anno + 1,
                    dataErogazione.Value.ToString("dd/MM/yyyy"));
                LabelErogazione.Visible = true;
                ButtonConferma.Visible = false;
                //ButtonConferma.Enabled = false;
            }
            else
            {
                LabelErogazione.Text = String.Empty;
                LabelErogazione.Visible = false;
                ButtonConferma.Visible = true;
                //ButtonConferma.Enabled = true;
            }

            CaricaPrestazioniDTA();
            PanelPrestazioniDTA.Visible = true;
        }
    }

    private void CaricaPrestazioniDTA()
    {
        if (ViewState["anno"] != null)
        {
            Int32 anno = (Int32) ViewState["anno"];
            DTAFilter filtro = ViewState["filtro"] as DTAFilter;

            // Carico le prestazioni erogabili/erogate
            List<PrestazioneDTA> prestazioni = corsiBiz.GetPrestazioniDTALavoratori(anno, filtro);
            Presenter.CaricaElementiInGridView(
                RadGridPrestazioniDTA,
                prestazioni);

            // Le recupero comunque tutte per effettuare i conteggi
            Int32 selezionate = 0;
            Int32 iscritti = 0;
            Int32 conDiritto = 0;
            List<PrestazioneDTA> prestazioniTutte = corsiBiz.GetPrestazioniDTALavoratori(anno, null);
            foreach (PrestazioneDTA pr in prestazioniTutte)
            {
                if (pr.Selezionato)
                    selezionate++;

                if (pr.IdLavoratore.HasValue)
                    iscritti++;

                if (pr.Diritto)
                    conDiritto++;
            }

            if (prestazioniTutte.Count > 0)
            {
                LabelLavoratoriTotale.Text = prestazioniTutte.Count.ToString();
                LabelLavoratoriIscritti.Text = String.Format("{0} ({1:0.00}%)", iscritti, ((Decimal) iscritti / (Decimal) prestazioniTutte.Count) * 100);
                LabelLavoratoriConDiritto.Text = String.Format("{0} ({1:0.00}%)", conDiritto, ((Decimal) conDiritto / (Decimal) prestazioniTutte.Count) * 100);
                LabelDomandeSelezionate.Text = String.Format("{0} ({1:0.00}%)", selezionate, ((Decimal) selezionate / (Decimal) prestazioniTutte.Count) * 100);
            }
            else
            {
                LabelLavoratoriTotale.Text = "-";
                LabelLavoratoriIscritti.Text = "-";
                LabelLavoratoriConDiritto.Text = "-";
                LabelDomandeSelezionate.Text = "-";
            }
        }
    }

    private void EstraiPrestazioniDTA()
    {
        if (ViewState["anno"] != null)
        {
            Int32 anno = (Int32) ViewState["anno"];
            DTAFilter filtro = ViewState["filtro"] as DTAFilter;

            GridView GridViewEstrazioneDTA = new GridView();
            GridViewEstrazioneDTA.ID = "gvLavoratoriDTA";
            GridViewEstrazioneDTA.AutoGenerateColumns = false;
            GridViewEstrazioneDTA.RowDataBound += new GridViewRowEventHandler(GridViewEstrazioneDTA_RowDataBound);

            var bc0 = new BoundField();
            bc0.HeaderText = "Selezionato";
            bc0.DataField = "Selezionato";
            var bc1 = new BoundField();
            bc1.HeaderText = "Diritto";
            bc1.DataField = "Diritto";
            var bc2 = new BoundField();
            bc2.HeaderText = "Codice lavoratore";
            bc2.DataField = "IdLavoratore";
            var bc3 = new BoundField();
            bc3.HeaderText = "Cognome";
            bc3.DataField = "Cognome";
            var bc4 = new BoundField();
            bc4.HeaderText = "Nome";
            bc4.DataField = "Nome";
            var bc5 = new BoundField();
            bc5.HeaderText = "Codice fiscale";
            bc5.DataField = "CodiceFiscale";
            var bc6 = new BoundField();
            bc6.HeaderText = "Data di nascita";
            bc6.DataField = "DataNascita";
            bc6.DataFormatString = "{0:dd/MM/yyyy}";
            var bc7 = new BoundField();
            bc7.HeaderText = "Corso";
            bc7.DataField = "CodiceCorso";
            var bc8 = new BoundField();
            bc8.HeaderText = "Data corso";
            bc8.DataField = "DataFineCorso";
            var bc9 = new BoundField();
            bc9.HeaderText = "Denuncia nei due anni";
            bc9.DataField = "DenunciaPresente";
            var bc91 = new BoundField();
            bc91.HeaderText = "IBAN in anagrafica";
            bc91.DataField = "IbanPresente";
            var bc10 = new BoundField();
            bc10.HeaderText = "Protocollo prestazione";
            bc10.DataField = "ProtocolloPrestazioneCompleto";
            var bc11 = new BoundField();
            bc11.HeaderText = "Stato prestazione";
            bc11.DataField = "DescrizioneStato";

            GridViewEstrazioneDTA.Columns.Add(bc0);
            GridViewEstrazioneDTA.Columns.Add(bc1);
            GridViewEstrazioneDTA.Columns.Add(bc2);
            GridViewEstrazioneDTA.Columns.Add(bc3);
            GridViewEstrazioneDTA.Columns.Add(bc4);
            GridViewEstrazioneDTA.Columns.Add(bc5);
            GridViewEstrazioneDTA.Columns.Add(bc6);
            GridViewEstrazioneDTA.Columns.Add(bc7);
            GridViewEstrazioneDTA.Columns.Add(bc8);
            GridViewEstrazioneDTA.Columns.Add(bc9);
            GridViewEstrazioneDTA.Columns.Add(bc91);
            GridViewEstrazioneDTA.Columns.Add(bc10);
            GridViewEstrazioneDTA.Columns.Add(bc11);

            // Carico le prestazioni erogabili/erogate
            List<PrestazioneDTA> prestazioni = corsiBiz.GetPrestazioniDTALavoratori(anno, filtro);
            Presenter.CaricaElementiInGridView(
                GridViewEstrazioneDTA,
                prestazioni,
                0);

            //gv.RowDataBound += new GridViewRowEventHandler(gv_RowDataBound);

            //gv.DataSource = deleghe;
            //gv.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GridViewEstrazioneDTA.RenderControl(htw);

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=CorsistiDTA.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(sw.ToString());
            Response.End();
        }
    }

    void GridViewEstrazioneDTA_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PrestazioneDTA prest = (PrestazioneDTA) e.Row.DataItem;

            e.Row.Cells[0].Text = prest.Selezionato ? "SI" : "NO";
            e.Row.Cells[1].Text = prest.Diritto ? "SI" : "NO";
            e.Row.Cells[9].Text = prest.DenunciaPresente ? "SI" : "NO";
            e.Row.Cells[10].Text = prest.IbanPresente ? "SI" : "NO";
        }
    }

    protected void RadGridPrestazioniDTA_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            PrestazioneDTA prestazione = (PrestazioneDTA) e.Item.DataItem;
            DateTime? dataErogazione = ViewState["dataErogazione"] as DateTime?;

            Label lIdLavoratore = (Label) e.Item.FindControl("LabelIdLavoratore");
            Label lCognomeNome = (Label) e.Item.FindControl("LabelCognomeNome");
            Label lCodiceFiscale = (Label) e.Item.FindControl("LabelCodiceFiscale");
            Label lDataNascita = (Label) e.Item.FindControl("LabelDataNascita");
            Image iDiritto = (Image) e.Item.FindControl("ImageDiritto");
            CheckBox cbSelezione = (CheckBox) e.Item.FindControl("CheckBoxSelection");

            MultiView mvDomanda = (MultiView) e.Item.FindControl("MultiViewDomanda");
            View vDomanda = (View) e.Item.FindControl("ViewDomanda");
            View vNoDomanda = (View) e.Item.FindControl("ViewNoDomanda");
            Label lProtocollo = (Label) e.Item.FindControl("LabelProtocollo");
            Label lStato = (Label) e.Item.FindControl("LabelStato");

            lIdLavoratore.Text = prestazione.IdLavoratore.ToString();
            lCognomeNome.Text = String.Format("{0} {1}", prestazione.Cognome, prestazione.Nome);
            lCodiceFiscale.Text = prestazione.CodiceFiscale;
            if (prestazione.DataNascita.HasValue)
            {
                lDataNascita.Text = prestazione.DataNascita.Value.ToString("dd/MM/yyyy");
            }

            if (!prestazione.IdLavoratore.HasValue)
            {
                iDiritto.ImageUrl = "~/images/semaforoRosso.png";
                iDiritto.ToolTip = "Il lavoratore non risulta iscritto alla Cassa Edile";
                cbSelezione.Visible = false;
            }
            else
            {
                if (!prestazione.DenunciaPresente)
                {
                    StringBuilder msgErr = new StringBuilder();

                    iDiritto.ImageUrl = "~/images/semaforoRosso.png";
                    msgErr.AppendLine("Il lavoratore non è stato denunciato nei due anni precedenti al corso");

                    if (!prestazione.IbanPresente)
                    {
                        msgErr.AppendLine("Il lavoratore non ha un IBAN nell'anagrafica Cassa Edile");
                    }

                    iDiritto.ToolTip = msgErr.ToString();
                }
                else
                {
                    if (!prestazione.IbanPresente)
                    {
                        iDiritto.ImageUrl = "~/images/semaforoRosso.png";
                        iDiritto.ToolTip = "Il lavoratore non ha un IBAN nell'anagrafica Cassa Edile";
                    }
                    else
                    {
                        iDiritto.ImageUrl = "~/images/semaforoVerde.png";
                        iDiritto.ToolTip = "Ok";
                    }
                }
            }

            cbSelezione.Checked = prestazione.Selezionato;
            if (prestazione.Selezionato)
            {
                ButtonConferma.Enabled = true;
            }

            if (dataErogazione.HasValue)
            {
                cbSelezione.Enabled = false;
            }

            if (prestazione.ProtocolloPrestazione.HasValue && prestazione.NumeroProtocolloPrestazione.HasValue)
            {
                mvDomanda.SetActiveView(vDomanda);
                lProtocollo.Text = String.Format("{0}/{1}", 
                    prestazione.ProtocolloPrestazione, 
                    prestazione.NumeroProtocolloPrestazione);
                lStato.Text = prestazione.DescrizioneStato;
            }
            else
            {
                mvDomanda.SetActiveView(vNoDomanda);
            }
        }
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (ViewState["anno"] != null)
        {
            Int32 anno = (Int32) ViewState["anno"];
            List<PrestazioneDTALavoratore> prestazioniTemp = new List<PrestazioneDTALavoratore>();

            foreach (GridDataItem item in RadGridPrestazioniDTA.Items)
            {
                object idLav = item.GetDataKeyValue("IdLavoratore");
                Int32? idLavoratore = null;
                if (idLav != null)
                {
                    idLavoratore = idLav as Int32?;
                }

                CheckBox cbSelezione = (CheckBox) item.FindControl("CheckBoxSelection");

                if (idLavoratore.HasValue)
                {
                    PrestazioneDTALavoratore prest = new PrestazioneDTALavoratore();
                    prest.Anno = anno;
                    prest.IdLavoratore = idLavoratore.Value;
                    prest.Selezionato = cbSelezione.Checked;

                    prestazioniTemp.Add(prest);
                }
            }

            corsiBiz.SalvaPrestazioniDTALavoratori(prestazioniTemp);
            CaricaPrestazioniDTA();
        }
    }

    protected void CheckBoxSelection_CheckedChanged(object sender, EventArgs e)
    {
        if (ViewState["anno"] != null)
        {
            CheckBox cbSelezionato = (CheckBox) sender;

            Int32 idLavoratore = (Int32) ((GridDataItem) cbSelezionato.NamingContainer).GetDataKeyValue("IdLavoratore");
            Int32 anno = (Int32) ViewState["anno"];

            PrestazioneDTALavoratore prestazione = new PrestazioneDTALavoratore();
            prestazione.Anno = anno;
            prestazione.IdLavoratore = idLavoratore;
            prestazione.Selezionato = cbSelezionato.Checked;

            corsiBiz.SalvaPrestazioneDTALavoratore(prestazione);
            CaricaPrestazioniDTA();
        }
    }

    protected void RadGridPrestazioniDTA_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        RadGridPrestazioniDTA.CurrentPageIndex = e.NewPageIndex;
        CaricaPrestazioniDTA();
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        if (ViewState["anno"] != null)
        {
            Int32 anno = (Int32) ViewState["anno"];

            ButtonSalva_Click(sender, e);

            corsiBiz.GeneraDomandePrestazioniDTALavoratori(anno);
            CaricaPannelloPrestazioniDTA();
        }
    }

    protected void GeneraFiltroRicerca()
    {
        DTAFilter filtro = new DTAFilter();

        filtro.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        if (DropDownListCorso.SelectedValue != "Tutti")
        {
            filtro.Corso = DropDownListCorso.SelectedValue;
        }
        if (DropDownListDiritto.SelectedValue != "Tutti")
        {
            filtro.Diritto = Boolean.Parse(DropDownListDiritto.SelectedValue);
        }

        ViewState["filtro"] = filtro;
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        GeneraFiltroRicerca();
        CaricaPrestazioniDTA();
    }

    protected void ButtonExcel_Click(object sender, EventArgs e)
    {
        GeneraFiltroRicerca();
        EstraiPrestazioniDTA();
    }
}