using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Business;

public partial class Corsi_VisualizzaCertificato : Page
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione);

        #endregion

        if (!Page.IsPostBack)
        {
            ViewState["IdPartecipazione"] = Context.Items["IdPartecipazione"];
            ViewState["IdCorso"] = Context.Items["IdCorso"];
        }

        Int32 idPartecipazione = (Int32) ViewState["IdPartecipazione"];
        Int32 idCorso = (Int32) ViewState["IdCorso"];

        ReportViewerCorsi.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        Corso corso = biz.GetCorsoByKey(idCorso);

        //if (ViewState["CodiceCorso"].ToString() == "16 ORE")
        //{
        //    ReportViewerCorsi.ServerReport.ReportPath = "/ReportCorsi/ReportAttestato16Ore";
        //}
        //else
        //{
        //    ReportViewerCorsi.ServerReport.ReportPath = "/ReportCorsi/ReportAttestato8Ore";
        //}
        ReportViewerCorsi.ServerReport.ReportPath = String.Format("/ReportCorsi/{0}", corso.ReportAttestato);

        ReportParameter[] listaParam = new ReportParameter[1];
        listaParam[0] = new ReportParameter("idPartecipazione", idPartecipazione.ToString());
        ReportViewerCorsi.ServerReport.SetParameters(listaParam);

        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string extension;

        //PDF

        byte[] bytes = ReportViewerCorsi.ServerReport.Render(
            "PDF", null, out mimeType, out encoding, out extension,
            out streamids, out warnings);

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/pdf";

        Response.AppendHeader("Content-Disposition", "attachment;filename=Attestato.pdf");
        Response.BinaryWrite(bytes);

        Response.Flush();
        Response.End();

    }
}