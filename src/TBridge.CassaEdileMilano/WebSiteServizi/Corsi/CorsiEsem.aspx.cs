using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Corsi_CorsiEsem : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiVisualizzazione);

        #endregion
    }
}