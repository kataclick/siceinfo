﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IscrizioneCorsiImpresaIrregolareBNI.aspx.cs" Inherits="Corsi_IscrizioneCorsiImpresaIrregolareBNI" %>

<%@ Register src="../WebControls/MenuCorsiEsem.ascx" tagname="MenuCorsiEsem" tagprefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCorsiEsem ID="MenuCorsiEsem1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
<uc2:titolosottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Iscrizione lavoratori" />
        <p><b>ATTENZIONE!!!</b></p>
        <p>L'impresa risulta irregolare per BNI.<br />
        Non è possibile proseguire con l'iscrizione.</p>
        <p>Per ulteriori informazioni contattare direttamente <a href="http://www.esem.it/Pages/Default.aspx" target="_blank">ESEM</a></p>
</asp:Content>


