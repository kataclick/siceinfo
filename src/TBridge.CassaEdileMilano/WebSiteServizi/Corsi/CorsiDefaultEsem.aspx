<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CorsiDefaultEsem.aspx.cs" Inherits="Corsi_CorsiDefaultEsem" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register src="../WebControls/MenuCorsiEsem.ascx" tagname="MenuCorsiEsem" tagprefix="uc4" %>
<%@ Register src="WebControls/ScadenzeLavoratori.ascx" tagname="ScadenzeLavoratori" tagprefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc4:MenuCorsiEsem ID="MenuCorsiEsem1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Benvenuto nella sezione Corsi" />
    <br />
    Iscrizione telematica ai corsi ESEM
    <br />
    <br />
    Scadenze Corsi<br />
    <uc2:ScadenzeLavoratori ID="ScadenzeLavoratori1" runat="server" />
</asp:Content>
