using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Corsi_CorsiDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CorsiGestione);
        funzionalita.Add(FunzionalitaPredefinite.CorsiVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneImpresa);
        funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneConsulente);
        funzionalita.Add(FunzionalitaPredefinite.CorsiEstrazioneFormedil);
        funzionalita.Add(FunzionalitaPredefinite.CorsiStatisticheAttestati);
        funzionalita.Add(FunzionalitaPredefinite.CorsiPrestazioniDTA);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}