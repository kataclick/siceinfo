<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LavoratoreCorsi.aspx.cs" Inherits="Corsi_LavoratoreCorsi" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Corsi svolti/programmati" />
    <br />
    <uc3:PitHeader ID="PitHeader1" runat="server" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:GridView ID="GridViewCorsi" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataKeyNames="IdProgrammazione,IdProgrammazioneModulo" AllowPaging="True" OnPageIndexChanging="GridViewCorsi_PageIndexChanging1">
                    <Columns>
                        <asp:BoundField DataField="Schedulazione" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                            HtmlEncode="False">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Schedulazione" DataFormatString="{0:hh:mm}" HeaderText="Ora">
                            <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Corso" HeaderText="Corso" />
                        <asp:BoundField DataField="Modulo" HeaderText="Modulo" />
                        <asp:CheckBoxField HeaderText="Pres.">
                            <ItemStyle Width="10px" />
                        </asp:CheckBoxField>
                        <asp:CheckBoxField HeaderText="Cert.">
                            <ItemStyle Width="10px" />
                        </asp:CheckBoxField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna pianificazione trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
