using System;
using System.Web.UI;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Corsi_GestionePartecipantiCorsi : Page
{
    private readonly CorsiBusiness biz = new CorsiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione);

        #endregion

        CorsiRicercaPianificazioni1.OnProgrammazioneSelected += CorsiRicercaPianificazioni1_OnProgrammazioneSelected;
        CorsiRicercaPianificazioni1.ImpostaNumeroElementiPagina(5);

        if (!Page.IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["dataPianificazione"]))
            {
                CorsiRicercaPianificazioni1.ImpostaDataPianificazione(
                    DateTime.Parse(Request.QueryString["dataPianificazione"]));
            }

            if (!String.IsNullOrEmpty(Request.QueryString["idCorso"]))
            {
                CorsiRicercaPianificazioni1.ImpostaCorso(Int32.Parse(Request.QueryString["idCorso"]));
            }

            if (!String.IsNullOrEmpty(Request.QueryString["idLocazione"]))
            {
                CorsiRicercaPianificazioni1.ImpostaLocazione(Int32.Parse(Request.QueryString["idLocazione"]));
            }

            if (!String.IsNullOrEmpty(Request.QueryString["dataPianificazione"])
                || !String.IsNullOrEmpty(Request.QueryString["idCorso"])
                || !String.IsNullOrEmpty(Request.QueryString["idLocazione"]))
            {
                CorsiRicercaPianificazioni1.ForzaRicerca();
            }
        }
    }

    private void CorsiRicercaPianificazioni1_OnProgrammazioneSelected(Int32 idProgrammazione,
                                                                      Int32 idProgrammazioneModulo)
    {
        PanelPartecipanti.Visible = true;
        ViewState["IdProgrammazioneModulo"] = idProgrammazioneModulo;
        ViewState["IdProgrammazione"] = idProgrammazione;
        CorsiRicercaPartecipanti1.CaricaPartecipanti(idProgrammazioneModulo);

        ProgrammazioneModuloCollection programmazioni = biz.GetProgrammazione(idProgrammazione);
        if (programmazioni.Count > 0)
        {
            CaricaInfoCorso(programmazioni, idProgrammazioneModulo);
        }
    }

    private void CaricaInfoCorso(ProgrammazioneModuloCollection programmazioni, Int32 idProgrammazioneModulo)
    {
        ProgrammazioneModulo prog = programmazioni.GetProgrammazioneModulo(idProgrammazioneModulo);

        ViewState["IdCorso"] = prog.Corso.IdCorso;

        LabelCorso.Text = prog.Corso.Descrizione;
        LabelModulo.Text = prog.Modulo.Descrizione;
        LabelSchedulazione.Text = String.Format("{0} - {1}", prog.Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                                prog.SchedulazioneFine.Value.ToString("HH:mm"));
        LabelLocazione.Text = prog.Locazione.IndirizzoCompleto;
        LabelPostiOccupati.Text = String.Format("{0} / {1}", prog.Partecipanti, prog.MaxPartecipanti);
        ImagePresenzeConfermate.ImageUrl = biz.ConvertiBoolInSemaforo(prog.PresenzeConfermate);
        ButtonConfermaPresenze.Enabled = !prog.PresenzeConfermate;
    }

    protected void ButtonEstraiPartecipanti_Click(object sender, EventArgs e)
    {
        Context.Items["IdProgrammazioneModulo"] = ViewState["IdProgrammazioneModulo"];
        Server.Transfer("~/Corsi/RegistroPresenze.aspx");
    }

    protected void ButtonVisualizzaCertificati_Click(object sender, EventArgs e)
    {
        Context.Items["IdProgrammazioneModulo"] = ViewState["IdProgrammazioneModulo"];
        Context.Items["IdCorso"] = ViewState["IdCorso"];
        Server.Transfer("~/Corsi/VisualizzaCertificati.aspx");
    }

    protected void ButtonConfermaPresenze_Click(object sender, EventArgs e)
    {
        Int32 idProgrammazioneModulo = (Int32) ViewState["IdProgrammazioneModulo"];

        if (biz.UpdateProgrammazioneModuloConfermaPresenze(idProgrammazioneModulo))
        {
            Int32 idProgrammazione = (Int32) ViewState["IdProgrammazione"];

            CorsiRicercaPianificazioni1.ForzaRicerca();
            CorsiRicercaPartecipanti1.CaricaPartecipanti(idProgrammazioneModulo);
            ProgrammazioneModuloCollection programmazioni = biz.GetProgrammazione(idProgrammazione);
            if (programmazioni.Count > 0)
            {
                CaricaInfoCorso(programmazioni, idProgrammazioneModulo);
            }
        }
    }

    protected void ButtonTicketPartecipanti_Click(object sender, EventArgs e)
    {
        Context.Items["IdProgrammazioneModulo"] = ViewState["IdProgrammazioneModulo"];
        Server.Transfer("~/Corsi/RegistroConsegnaTicket.aspx");
    }
}