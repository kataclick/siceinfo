using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

/// <summary>
/// Summary description for AnomalieManager
/// </summary>
public class AnomalieManager
{
    private readonly Database databaseCemi;

    public AnomalieManager()
    {
        databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
    }

    public bool RegistraAnomalia(string descrizione, int idUtente, string loginUtente)
    {
        bool ret = false;

        try
        {
            DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_AnomalieImpreseSegnalateInsert");
            databaseCemi.AddInParameter(dbCommand, "@descrizione", DbType.String, descrizione);
            databaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
            databaseCemi.AddInParameter(dbCommand, "@loginUtente", DbType.String, loginUtente);

            int id = (int) databaseCemi.ExecuteScalar(dbCommand);
            if (id > 0)
                ret = true;
        }
        catch
        {
        }

        return ret;
    }

    public DataSet CaricaAnomalie(DateTime periodoDa, DateTime periodoA)
    {
        DataSet ret = null;

        try
        {
            DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_AnomalieImpreseSegnalateSelectByDate");
            databaseCemi.AddInParameter(dbCommand, "@dataDa", DbType.DateTime, periodoDa);
            databaseCemi.AddInParameter(dbCommand, "@dataA", DbType.DateTime, periodoA);

            ret = databaseCemi.ExecuteDataSet(dbCommand);
        }
        catch
        {
        }

        return ret;
    }
}