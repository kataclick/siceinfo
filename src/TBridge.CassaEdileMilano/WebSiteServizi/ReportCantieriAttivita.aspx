<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportCantieriAttivita.aspx.cs" Inherits="ReportCantieriAttivita" %>

<%@ Register Src="WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<%@ Register Src="WebControls/MenuImprese.ascx" TagName="MenuImprese" TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Attivita" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerAttivita" runat="server"
        ProcessingMode="Remote" width="100%" Height="490px">
    </rsweb:ReportViewer>
    </td></tr></table>
    <br />
</asp:Content>
