<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SchedaImpresa.ascx.cs" Inherits="SchedaImpresa" %>
<asp:Panel ID="pnlControlli" runat="server" Width="100%">
    <table class="standardTable">
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblCodice" runat="server" Text="Codice Impresa:"></asp:Label>
                </td>
            <td colspan="2">
                <asp:Label ID="lblTxtCodice" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblRagioneSociale" runat="server" Text="Ragione sociale:"></asp:Label>&nbsp;
            </td>
            <td colspan="2">
                <asp:Label ID="lblTxtRagioneSociale" runat="server" Font-Bold="True"></asp:Label>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 19px;">
                <asp:Label ID="lblCodiceFiscale" runat="server" Text="Codice fiscale:"></asp:Label></td>
            <td colspan="2" style="height: 19px">
                <asp:Label ID="lblTxtCodiceFiscale" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblPartitaIVA" runat="server" Text="Partita IVA:"></asp:Label></td>
            <td colspan="2">
                <asp:Label ID="lblTxtPartitaIva" runat="server" Font-Bold="True"></asp:Label></td>

        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="width: 642px"></td>
        </tr>
        <tr>
            <td style="width: 185px;">
                <asp:Label ID="lblTipoImpresa" runat="server" Text="Tipo impresa:"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lblTxtTipoImpresa" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblCodiceINAIL" runat="server" Text="Codice INAIL:"></asp:Label></td>
            <td colspan="2">
                <asp:Label ID="lblTxtCodiceInail" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblCodiceINPS" runat="server" Text="Codice INPS:"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lblTxtCodiceInps" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblNumeroIscrizioneCCIAA" runat="server" Text="N� iscrizione CCIAA:"></asp:Label>&nbsp;
            </td>
            <td colspan="2">
                <asp:Label ID="lblTxtNrIscrizCCIAA" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblAttivitaISTAT" runat="server" Text="Attivit� ISTAT:"></asp:Label></td>
            <td colspan="2">
                <asp:Label ID="lblTxtAttivitaIstat" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td style="width: 642px"></td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblSedeLegale" runat="server" Text="Sede legale:"></asp:Label></td>
            <td colspan="2">
    <asp:Label ID="lblTxtIndirizzoSedeLegale" runat="server" Font-Bold="True"></asp:Label>
    <asp:Label ID="lblTxtCapSedeLegale" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td colspan="2">
    <asp:Label ID="lblTxtLocalitaSedeLegale" runat="server" Font-Bold="True"></asp:Label>
    <asp:Label ID="lblTxtProvinciaSedeLegale" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 21px;">
            </td>
            <td>
            </td>
            <td style="width: 642px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
                <asp:Label ID="lblSedeAmministrazione" runat="server" Text="Sede amministrazione:"></asp:Label></td>
            <td colspan = "2">
    <asp:Label ID="lblTxtIndirizzoSedeAmministrazione" runat="server" Font-Bold="True"></asp:Label>
    <asp:Label ID="lblTxtCapSedeAmministrazione" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td colspan = "2">
    <asp:Label ID="lblTxtLocalitaSedeAmministrazione" runat="server" Font-Bold="True"></asp:Label>
    <asp:Label ID="lblTxtProvinciaSedeAmministrazione" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
            </td>
            <td>
    <asp:Label ID="lblPressoSedeAmministrazione" runat="server" Text="Presso:"></asp:Label></td>
            <td style="width: 642px">
                &nbsp;<asp:Label ID="lblTxtPressoSedeAmministrazione" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 21px;">
            </td>
            <td>
            </td>
            <td style="width: 642px">
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
    <asp:Label ID="lblTelefono" runat="server" Text="Telefono:"></asp:Label>
                </td>
            <td colspan="2">
                <asp:Label ID="lblTxtTelefono" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td>
    <asp:Label ID="lblFax" runat="server" Text="Fax :"></asp:Label></td>
            <td colspan="2">
                <asp:Label ID="lblTxtFax" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
    <asp:Label ID="lblEmail" runat="server" Text="E-mail :"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Label ID="lblTxtEmail" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 185px">
    <asp:Label ID="lblCellulare" runat="server" Text="Telefono cellulare :"></asp:Label>
                </td>
            <td colspan="2">
                <asp:Label ID="lblTxtCellulare" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 185px">
    <asp:Label ID="lblSitoWeb" runat="server" Text="Indirizzo web :"></asp:Label></td>
            <td colspan="2">
                <asp:Label ID="lblTxtSito" runat="server" Font-Bold="True"></asp:Label></td>
        </tr>
    </table>
</asp:Panel>
