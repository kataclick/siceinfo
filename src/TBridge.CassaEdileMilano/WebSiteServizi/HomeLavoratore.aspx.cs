using System;
using System.Collections.Specialized;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Data.Entities;

public partial class HomeLavoratore : Page
{
    private readonly Common _commonBiz = new Common();
    private readonly GestioneUtentiBiz _gestioneUtentiBiz = new GestioneUtentiBiz();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoLavoratore);

        if (GestioneUtentiBiz.IsLavoratore())
        {
            TitoloSottotitolo1.sottoTitolo = Membership.GetUser().UserName;

            if (Request.QueryString["op"] != null && Request.QueryString["op"] == "modifica")
            {
                Panel1.Visible = true;
                if (!Page.IsPostBack)
                {
                    CaricaDatiLavoratore();

                    CaricaPreIndirizzi();
                    CaricaProvince();

                    int idProvincia = -1;
                    if ((DropDownListProvincia.SelectedValue != null) &&
                        (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                        CaricaComuni(idProvincia);

                    Int64 idComune = -1;
                    if ((DropDownListComuni.SelectedValue != null) &&
                        (Int64.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                        CaricaCAP(idComune);
                }
            }
            else
            {
                Panel1.Visible = false;
                if (!Page.IsPostBack)
                {
                    CaricaDatiLavoratore();
                }

                if (Request.QueryString["op"] != null && Request.QueryString["op"] == "ok")
                    LabelRisultato.Text = "Segnalazione effettuata";
                else if (Request.QueryString["op"] != null && Request.QueryString["op"] == "ko")
                    LabelRisultato.Text = "Errore";
            }
        }
        else
        {
            Response.Redirect("DefaultAutenticato.aspx");
        }
        //}
    }

    private void CaricaDatiLavoratore()
    {
        Lavoratore lavoratore =
            (Lavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        TextBoxCognome.Text = lavoratore.Cognome;
        TextBoxNome.Text = lavoratore.Nome;
        TextBoxCF.Text = lavoratore.CodiceFiscale;
        TextBoxDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
        TextBoxLuogoNascita.Text = lavoratore.LuogoNascita;
        TextBoxSesso.Text = lavoratore.Sesso;
        TextBoxIndirizzo.Text = lavoratore.IndirizzoCompleto;
    }

    private void CaricaProvince()
    {
        DataTable dtProvince = _commonBiz.GetProvince();

        DropDownListProvincia.DataSource = dtProvince;
        DropDownListProvincia.DataTextField = "sigla";
        DropDownListProvincia.DataValueField = "idProvincia";

        DropDownListProvincia.DataBind();
    }

    private void CaricaPreIndirizzi()
    {
        ListDictionary dtPreIndirizzi = _commonBiz.GetPreIndirizzi();

        DropDownListPreIndirizzo.DataSource = dtPreIndirizzi;
        DropDownListPreIndirizzo.DataTextField = "preIndirizzo";
        DropDownListPreIndirizzo.DataValueField = "preIndirizzo";

        DropDownListPreIndirizzo.DataBind();
    }

    private void CaricaComuni(int idProvincia)
    {
        DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

        DropDownListComuni.DataSource = dtComuni;
        DropDownListComuni.DataTextField = "denominazione";
        DropDownListComuni.DataValueField = "idComune";

        DropDownListComuni.DataBind();
    }

    private void CaricaCAP(Int64 idComune)
    {
        DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

        DropDownListCAP.DataSource = dtCAP;
        DropDownListCAP.DataTextField = "cap";
        DropDownListCAP.DataValueField = "cap";

        DropDownListCAP.DataBind();
    }

    protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idProvincia = -1;

        if ((DropDownListProvincia.SelectedValue != null) &&
            (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
            CaricaComuni(idProvincia);

        int idComune = -1;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idComune = -1;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    protected void ButtonAggiorna_Click(object sender, EventArgs e)
    {
        //Lavoratore lavoratore =
        //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore) (HttpContext.Current.User).Identity).Entity;
        Lavoratore lavoratore =
            (Lavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        Indirizzo indirizzo = CostruzioneIndirizzo();
        if (
            _gestioneUtentiBiz.RichiediAggiornamentoIndirizzoLavoratore(lavoratore.IdLavoratore, indirizzo.IndirizzoVia,
                                                           indirizzo.Cap, indirizzo.Provincia, indirizzo.Comune))
        {
            //((TBridge.Cemi.GestioneUtentiBiz.Business.Identities.Lavoratore)(HttpContext.Current.User).Identity).Entity.IndirizzoCAP = indirizzo.Cap;
            //((TBridge.Cemi.GestioneUtentiBiz.Business.Identities.Lavoratore)(HttpContext.Current.User).Identity).Entity.IndirizzoProvincia = indirizzo.Provincia;
            //((TBridge.Cemi.GestioneUtentiBiz.Business.Identities.Lavoratore)(HttpContext.Current.User).Identity).Entity.IndirizzoDenominazione = indirizzo.IndirizzoVia;
            // LabelRisultato.Text = "Segnalazione avvenuta con successo";
            Response.Redirect("~/HomeLavoratore.aspx?op=ok");
        }
        else
            //LabelRisultato.Text = "Errore";
            Response.Redirect("~/HomeLavoratore.aspx?op=ko");

        //Response.Redirect("~/HomeLavoratore.aspx?op=ko");
    }

    private Indirizzo CostruzioneIndirizzo()
    {
        Indirizzo indirizzo = null;

        string ind = string.Empty;
        string provincia = string.Empty;
        string comune = string.Empty;
        string cap = string.Empty;

        ind = DropDownListPreIndirizzo.Text + " " + TextBoxIndirizzoModifica.Text;
        provincia = DropDownListProvincia.SelectedItem.Text;
        comune = DropDownListComuni.SelectedItem.Text;
        cap = DropDownListCAP.SelectedItem.Text;

        if ((ind != string.Empty) && (provincia != string.Empty) && (comune != string.Empty) && (cap != string.Empty))
            indirizzo = new Indirizzo(ind, provincia, comune, cap);

        return indirizzo;
    }

    protected void ButtonModificaIndirizzo_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/HomeLavoratore.aspx?op=modifica");
    }
}