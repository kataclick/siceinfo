﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using System.Text;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Filters;

public partial class AnagraficaImprese_QuestionarioImpiegatiConsulente : System.Web.UI.Page
{
    private readonly ImpresaQuestionarioManager questionarioManager = new ImpresaQuestionarioManager();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();
        funzionalita.Add(FunzionalitaPredefinite.ImpreseQuestionario);
        funzionalita.Add(FunzionalitaPredefinite.ImpreseQuestionarioConsulenti);
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        if (!Page.IsPostBack)
        {
            Int32 pagina = 0;

            ConsulenteImpreseFilter filtro = null;

            try
            {
                filtro = questionarioManager.GetFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), out pagina);
            }
            catch
            {
                questionarioManager.DeleteFiltroRicerca(GestioneUtentiBiz.GetIdUtente());
            }

            if (filtro != null)
            {
                CaricaFiltro(filtro, pagina);

                ViewState["Filtro"] = filtro;

                CaricaEstrattoConto();
            }
        }

    }

    private void CaricaFiltro(ConsulenteImpreseFilter filtro, Int32 pagina)
    {
        RadGridImprese.CurrentPageIndex = pagina;

        if (filtro.IdImpresa.HasValue)
        {
            RadNumericTextBoxIdImpresa.Value = (double)filtro.IdImpresa;
        }
        RadTextBoxRagioneSociale.Text = filtro.RagioneSocialeImpresa;
        RadTextBoxCF.Text = filtro.CodiceFiscaleImpresa;

        if (filtro.Questionario.HasValue)
        {
            if (filtro.Questionario.Value)
            {
                RadComboBoxQuestionario.SelectedValue = "SI";
            }
            else
            {
                RadComboBoxQuestionario.SelectedValue = "NO";
            }
        }
    }


    protected void RadGridImprese_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "gestione")
        {
            Int32 idImpresa = (Int32)RadGridImprese.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];


            Context.Items["IdImpresa"] = idImpresa;
            //Context.Items["Tutti"] = true;

            Server.Transfer("~/AnagraficaImprese/QuestionarioImpiegati.aspx");
        }
    }

    protected void RadGridImprese_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            //PrestazioneDomandaAssenza ass = (PrestazioneDomandaAssenza)e.Item.DataItem;
            //Label lPrestazione = (Label)e.Item.FindControl("LabelPrestazione");


            //// Prestazione

            //lPrestazione.Text = String.Format("{0} - {1} / {2}",
            //                                  ass.TipoProtocollo, ass.NumeroProtocollo, ass.AnnoProtocollo);

        }
    }

    protected void RadGridImprese_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        ConsulenteImpreseFilter filtro = CreaFiltro();
        questionarioManager.InsertFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), filtro, e.NewPageIndex);
        
        CaricaEstrattoConto();
    }

    protected void RadButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            ConsulenteImpreseFilter filtro = CreaFiltro();
            questionarioManager.InsertFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), filtro, 0);

            ViewState["Filtro"] = filtro;

            CaricaEstrattoConto();
        }
    }



    private ConsulenteImpreseFilter CreaFiltro()
    {
        ConsulenteImpreseFilter filtro = new ConsulenteImpreseFilter();

        if (RadNumericTextBoxIdImpresa.Value.HasValue)
        {
            filtro.IdImpresa = (Int32)RadNumericTextBoxIdImpresa.Value.Value;
        }

        filtro.RagioneSocialeImpresa = RadTextBoxRagioneSociale.Text;
        filtro.CodiceFiscaleImpresa = RadTextBoxCF.Text;

        switch (RadComboBoxQuestionario.SelectedValue)
        {
            case "SI":
                filtro.Questionario = true;
                break;
            case "NO":
                filtro.Questionario = false;
                break;
        }

        return filtro;
    }

    private void CaricaEstrattoConto()
    {
        if (ViewState["Filtro"] != null)
        {
            RadGridImprese.Visible = true;
            ConsulenteImpreseFilter filtro = (ConsulenteImpreseFilter)ViewState["Filtro"];

            if (GestioneUtentiBiz.IsConsulente())
            {
                TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente consulente =
                    (TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                Presenter.CaricaElementiInGridView(RadGridImprese, questionarioManager.GetImpreseConsulente(consulente.IdConsulente, filtro));
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }
        }
    }
}