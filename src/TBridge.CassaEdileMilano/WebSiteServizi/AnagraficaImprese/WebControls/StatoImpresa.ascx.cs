﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.Type.Filters;
using Telerik.Web.UI;
using TipoStatoGestionePratica = TBridge.Cemi.Type.Enums.TipoStatoGestionePratica;
using TipoStatoImpresa = TBridge.Cemi.Type.Enums.TipoStatoImpresa;
using ImpresaRichiestaVariazioneStato = Cemi.SiceInfo.Type.Domain.ImpresaRichiestaVariazioneStato;
using System.IO;
using Cemi.SiceInfo.Business.Imp.Storage;
using Cemi.SiceInfo.Type.Domain;
using Cemi.SiceInfo.Type.Domain.Storage;
using Cemi.SiceInfo.Type.Enum;

public partial class AnagraficaImprese_WebControls_StatoImpresa : UserControl
{
    private readonly ImpresaRecapitiManager _impresaManager = new ImpresaRecapitiManager();
    private readonly ImpresaVariazioneStatoManager _impresaVariazioneStatoManager = new ImpresaVariazioneStatoManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Per prevenire click multipli

        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(RadButtonOk, null));
        sb.Append(";");
        sb.Append("return true;");
        RadButtonOk.Attributes.Add("onclick", sb.ToString());

        #endregion

        if (!Page.IsPostBack)
        {
            CaricaTipoRichiesta();
            //LabelRichiesteSospese.Visible = false;

            RadDatePickerDataInizio.MinDate = DateTime.Today.AddYears(-2);
            RadDatePickerDataInizio.MaxDate = DateTime.Today;
        }
    }

    private void CaricaTipoRichiesta()
    {
        List<TBridge.Cemi.Type.Domain.TipoStatoImpresa> tipiStatoImpresa = _impresaVariazioneStatoManager.GetTipiStatoImpresa();
        foreach (TBridge.Cemi.Type.Domain.TipoStatoImpresa tipoStatoImpresa in tipiStatoImpresa)
        {
            if (tipoStatoImpresa.Descrizione == "Cessazione")
                tipoStatoImpresa.Descrizione = "Cessazione in CCIAA";
        }

        tipiStatoImpresa = (from tipo in tipiStatoImpresa
                            where tipo.Descrizione != "Cessazione in CCIAA"
                            select tipo).ToList();

        Presenter.CaricaElementiInDropDownConElementoVuoto(
            RadComboBoxTipoRichiesta,
            tipiStatoImpresa,
            "Descrizione",
            "Id");
    }

    public void CaricaDatiImpresa(int idImpresa)
    {
        ImpresaRecapiti impresaRecapiti = _impresaManager.GetImpresaRecapitiById(idImpresa);

        RadTextBoxIdImpresa.Text = impresaRecapiti.IdImpresa.ToString();
        RadTextBoxCf.Text = impresaRecapiti.CodiceFiscale;
        RadTextBoxRagionesociale.Text = impresaRecapiti.RagioneSociale;
        RadTextBoxSedeLegaleIndirizzo.Text = impresaRecapiti.IndirizzoSedeLegale.IndirizzoCompleto;
        ViewState["StatoOriginale"] = impresaRecapiti.StatoImpresa;
        RadTextBoxStatoAttuale.Text = (impresaRecapiti.StatoImpresa == "SOSP.UFF.")
                                          ? "ATTIVA"
                                          : impresaRecapiti.StatoImpresa;

        if (impresaRecapiti.StatoImpresa == StatoImpresa.CESSATA.ToString())
        {
            RadButtonOk.Enabled = false;
        }
        else
        {
            VerificaRichiestePendenti(idImpresa);
        }
    }

    private void VerificaRichiestePendenti(int idImpresa)
    {
        List<ImpresaRichiestaVariazioneStato> listaRichieste = _impresaVariazioneStatoManager.GetImpreseRichiesteVariazioneStato(new ImpresaRichiestaVariazioneStatoFilter
                                                                                                                      {
                                                                                                                          IdImpresa = idImpresa,
                                                                                                                          IdTipoStatoGestionePratica = (int?) TipoStatoGestionePratica.DaValutare
                                                                                                                      });

        listaRichieste.AddRange(
            _impresaVariazioneStatoManager.GetImpreseRichiesteVariazioneStato(new ImpresaRichiestaVariazioneStatoFilter
                                                                                  {
                                                                                      IdImpresa = idImpresa,
                                                                                      IdTipoStatoGestionePratica =
                                                                                          (int?)
                                                                                          TipoStatoGestionePratica.
                                                                                              InAttesaDiDocumentazione
                                                                                  }));

        if (listaRichieste.Count > 0)
        {
            RadButtonOk.Enabled = false;
            LabelRichiesteSospese.Visible = true;
        }
        else
        {
            RadButtonOk.Enabled = true;
            LabelRichiesteSospese.Visible = false;
        }
    }

    public void Reset()
    {
        RadTextBoxIdImpresa.Text = string.Empty;
        RadTextBoxCf.Text = string.Empty;
        RadTextBoxRagionesociale.Text = string.Empty;
        RadTextBoxSedeLegaleIndirizzo.Text = string.Empty;
        RadTextBoxStatoAttuale.Text = string.Empty;

        RadTextBoxNote.Text = string.Empty;
        RadComboBoxTipoRichiesta.SelectedIndex = 0;
        RadDatePickerDataInizio.Clear();

        RadTextBoxNote.Enabled = true;
        RadComboBoxTipoRichiesta.Enabled = true;
        RadDatePickerDataInizio.Enabled = true;

        RadButtonOk.Enabled = true;
        LabelEsito.Text = string.Empty;
    }

    protected void RadButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            RadButtonOk.Enabled = false;

            ImpresaRichiestaVariazioneStato richiestaVariazioneStato = new ImpresaRichiestaVariazioneStato();
            richiestaVariazioneStato.DataGestioneSiceNew = null;
            richiestaVariazioneStato.DataInizioCambioStato = RadDatePickerDataInizio.SelectedDate.Value;
            richiestaVariazioneStato.DataRichiesta = DateTime.Today;
            richiestaVariazioneStato.IdImpresa = Convert.ToInt32(RadTextBoxIdImpresa.Text);
            richiestaVariazioneStato.IdTipoStatoImpresa =
                (TipiStatoImpresa) Convert.ToInt32(RadComboBoxTipoRichiesta.SelectedValue);
            richiestaVariazioneStato.IdTipoStatoPratica = 0;
            richiestaVariazioneStato.IdUtente = GestioneUtentiBiz.GetIdUtente();
            richiestaVariazioneStato.Note = RadTextBoxNote.Text;
            richiestaVariazioneStato.DataGestioneOperatore = null;
            richiestaVariazioneStato.GestitoSiceNew = false;
            richiestaVariazioneStato.IdUtenteOperatore = null;

            Boolean sospensione = (RadComboBoxTipoRichiesta.SelectedValue ==
                                   ((Int32) TipoStatoImpresa.Sospensione).ToString());

            if (sospensione)
            {


                for (int i = 0; i < AsyncUploadAllegatoLibroUnico.UploadedFiles.Count; i++)
                {
                    // Recupero l'allegato libro unico (obbligatorio)
                    Documento docLibroUnico = new Documento();
                    docLibroUnico.Filename = AsyncUploadAllegatoLibroUnico.UploadedFiles[i].FileName;
                    docLibroUnico.FileExtension = AsyncUploadAllegatoLibroUnico.UploadedFiles[i].GetExtension()
                        .Remove(0, 1);
                    docLibroUnico.TipoDocumento = TipiDocumentiStored.LibroUnico.ToString();
                    docLibroUnico.TipoDocumentoId = (int) TipiDocumentiStored.LibroUnico;

                    //richiestaVariazioneStato.AllegatoLibroUnicoNome = AsyncUploadAllegatoLibroUnico.UploadedFiles[0].FileName;
                    using (Stream str = AsyncUploadAllegatoLibroUnico.UploadedFiles[i].InputStream)
                    {
                        byte[] file = new byte[AsyncUploadAllegatoLibroUnico.UploadedFiles[i].ContentLength];
                        str.Read(file, 0, AsyncUploadAllegatoLibroUnico.UploadedFiles[i].ContentLength);
                        //richiestaVariazioneStato.AllegatoLibroUnico = file;

                        StorageManager storageManager = new StorageManager();
                        int storedDocId = storageManager.Add(file);
                        /*
                        if (storedDocId == -1)
                        {
                            throw new Exception("Allegato non caricato su storage");
                        }
                        */
                        docLibroUnico.StoredDocumentId = storedDocId;
                    }

                    richiestaVariazioneStato.Documenti.Add(docLibroUnico);
                }

                for (int i = 0; i < AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles.Count; i++)
                {
                    // Recupero l'allegato denunce (se presente)
                    //if (AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles.Count > 0)
                    //{
                    Documento docDenunce = new Documento();
                    docDenunce.Filename = AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[i].FileName;
                    docDenunce.FileExtension = AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[i].GetExtension()
                        .Remove(0, 1);
                    docDenunce.TipoDocumento = TipiDocumentiStored.Denunce.ToString();
                    docDenunce.TipoDocumentoId = (int) TipiDocumentiStored.Denunce;

                    //richiestaVariazioneStato.AllegatoDenunceNome = AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[0].FileName;
                    using (Stream str = AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[i].InputStream)
                    {
                        byte[] file = new byte[AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[i].ContentLength];
                        str.Read(file, 0, AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[i].ContentLength);
                        //richiestaVariazioneStato.AllegatoDenunce = file;

                        StorageManager storageManager = new StorageManager();
                        int storedDocId = storageManager.Add(file);

                        docDenunce.StoredDocumentId = storedDocId;
                    }

                    richiestaVariazioneStato.Documenti.Add(docDenunce);
                    // }
                }

                for (int i = 0; i < AsyncUploadAllegatoUniemens.UploadedFiles.Count; i++)
                {
                    //// Recupero l'allegato denunce (se presente)
                    //if (AsyncUploadAllegatoUniemens.UploadedFiles.Count > 0)
                    //{
                    Documento docUniemens = new Documento();
                    docUniemens.Filename = AsyncUploadAllegatoUniemens.UploadedFiles[i].FileName;
                    docUniemens.FileExtension = AsyncUploadAllegatoUniemens.UploadedFiles[i].GetExtension()
                        .Remove(0, 1);
                    docUniemens.TipoDocumento = TipiDocumentiStored.Uniemens.ToString();
                    docUniemens.TipoDocumentoId = (int) TipiDocumentiStored.Uniemens;

                    //richiestaVariazioneStato.AllegatoDenunceNome = AsyncUploadAllegatoDenunceAltreCasse.UploadedFiles[0].FileName;
                    using (Stream str = AsyncUploadAllegatoUniemens.UploadedFiles[i].InputStream)
                    {
                        byte[] file = new byte[AsyncUploadAllegatoUniemens.UploadedFiles[i].ContentLength];
                        str.Read(file, 0, AsyncUploadAllegatoUniemens.UploadedFiles[i].ContentLength);
                        //richiestaVariazioneStato.AllegatoDenunce = file;

                        StorageManager storageManager = new StorageManager();
                        int storedDocId = storageManager.Add(file);

                        docUniemens.StoredDocumentId = storedDocId;
                    }

                    richiestaVariazioneStato.Documenti.Add(docUniemens);
                    //}
                }
            }

            bool ret = _impresaVariazioneStatoManager.SaveRichiestaVariazioneStato(richiestaVariazioneStato);

            LabelEsito.Text = ret ? "Inserimento richiesta effettuato correttamente." : "";
        }
    }

    protected void RadComboBoxTipoRichiesta_SelectedIndexChanged(object sender,
                                                                 RadComboBoxSelectedIndexChangedEventArgs e)
    {
        Boolean sospensione = (RadComboBoxTipoRichiesta.SelectedValue ==
                                              ((Int32) TipoStatoImpresa.Sospensione).ToString());

        //RequiredFieldValidatorNote.Enabled = 
        AsyncUploadAllegatoLibroUnico.Enabled = sospensione;
        AsyncUploadAllegatoDenunceAltreCasse.Enabled = sospensione;
        AsyncUploadAllegatoUniemens.Enabled = sospensione;
    }

    protected void CustomValidatorTipoRichiesta_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (ViewState["StatoOriginale"].ToString() == "ATTIVA" ||
            ViewState["StatoOriginale"].ToString() == "SOSP.UFF.")
        {
            if (RadComboBoxTipoRichiesta.SelectedIndex == Convert.ToInt32(TipoStatoImpresa.Riattivazione))
            {
                args.IsValid = false;
            }
        }

        if (ViewState["StatoOriginale"].ToString() == "SOSPESA")
        {
            if (RadComboBoxTipoRichiesta.SelectedIndex == Convert.ToInt32(TipoStatoImpresa.Sospensione))
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorAllegatoLibroUnico_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Boolean sospensione = (RadComboBoxTipoRichiesta.SelectedValue ==
                                              ((Int32) TipoStatoImpresa.Sospensione).ToString());

        if (sospensione && AsyncUploadAllegatoLibroUnico.UploadedFiles.Count == 0)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorAllegatoUniemens_ServerValidate(object source, ServerValidateEventArgs args)
    {
        Boolean sospensione = (RadComboBoxTipoRichiesta.SelectedValue ==
                                              ((Int32)TipoStatoImpresa.Sospensione).ToString());

        if (sospensione && AsyncUploadAllegatoUniemens.UploadedFiles.Count == 0)
        {
            args.IsValid = false;
        }
    }
}