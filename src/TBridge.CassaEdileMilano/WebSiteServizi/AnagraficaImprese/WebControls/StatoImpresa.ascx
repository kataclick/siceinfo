﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StatoImpresa.ascx.cs"
    Inherits="AnagraficaImprese_WebControls_StatoImpresa" %>
<style type="text/css">
    .style1
    {
        width: 260px;
    }
</style>
<table class="borderedTable">
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Codice Impresa</b>
        </td>
        <td class="style1">
            <telerik:RadTextBox ID="RadTextBoxIdImpresa" runat="server" EmptyMessage="Seleziona un'impresa"
                Width="250px" ReadOnly="True" Enabled="false" />
        </td>
        <td>
            <asp:Label runat="server" ID="LabelRichiesteSospese" ForeColor="Red" Visible="false"><i>(Richiesta pendente)</i></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Ragione Sociale</b>
        </td>
        <td class="style1">
            <telerik:RadTextBox ID="RadTextBoxRagionesociale" runat="server" EmptyMessage="Seleziona un'impresa"
                Width="250px" ReadOnly="True" Enabled="false" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Codice Fiscale/P. IVA</b>
        </td>
        <td class="style1">
            <telerik:RadTextBox ID="RadTextBoxCf" runat="server" EmptyMessage="Seleziona un'impresa"
                Width="250px" ReadOnly="True" Enabled="false" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Indirizzo Sede Legale</b>
        </td>
        <td class="style1">
            <telerik:RadTextBox ID="RadTextBoxSedeLegaleIndirizzo" runat="server" Width="250px"
                ReadOnly="True" TextMode="MultiLine" Style="overflow: hidden" Enabled="false" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Stato Impresa Attuale</b>
        </td>
        <td class="style1">
            <telerik:RadTextBox ID="RadTextBoxStatoAttuale" runat="server" Width="250px" ReadOnly="True"
                Style="overflow: hidden" Enabled="false" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Tipo Richiesta*</b>
        </td>
        <td class="style1">
            <telerik:RadComboBox ID="RadComboBoxTipoRichiesta" runat="server" Width="249px" AppendDataBoundItems="True"
                OnSelectedIndexChanged="RadComboBoxTipoRichiesta_SelectedIndexChanged" AutoPostBack="True" />
        </td>
        <td>
        
            <asp:CustomValidator ID="CustomValidatorTipoRichiesta" 
                ValidationGroup="cambioStatoImpresa" runat="server" 
                onservervalidate="CustomValidatorTipoRichiesta_ServerValidate"
                ErrorMessage="Tipologia di richiesta non selezionabile">*</asp:CustomValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoRichiesta" runat="server"
                ControlToValidate="RadComboBoxTipoRichiesta" ValidationGroup="cambioStatoImpresa"
                ErrorMessage="Selezionare la tipologia di richiesta">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Data Inizio Cambio Stato*</b>
        </td>
        <td class="style1">
            <telerik:RadDatePicker ID="RadDatePickerDataInizio" runat="server" Width="250px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataInizio" runat="server"
                ControlToValidate="RadDatePickerDataInizio" ValidationGroup="cambioStatoImpresa"
                ErrorMessage="Selezionare una data valida">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            In caso di sospensione della posizione l’impresa si impegna, ai sensi del D.P.R. n. 445/2000, a:
            <ul>
                <li>
                    riattivare la posizione tramite invio di dichiarazione mensile di manodopera occupata non appena iniziata un’attività con dipendenti operai
                </li>
            </ul>
            Affinché la richiesta possa essere accolta l’impresa deve obbligatoriamente inviare la seguente documentazione:
            <ul>
                <li>
                    Libro Unico del Lavoro con riepilogo e Uniemens relativi all’ultimo mese da cui si ha evidenza della cessazione del rapporto di lavoro dei dipendenti tramite caricamento del file in formato .tif, .pdf, ecc.
                </li>
                <li>
                    Frontespizio dell’ultima denuncia mensile disponibile relativa al periodo intercorrente tra la data di richiesta di sospensione ed il suo effettivo inizio (<b>solo in caso di dichiarazione della manodopera occupata presso altra Cassa Edile</b>)
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Note</b>
        </td>
        <td class="style1">
            <telerik:RadTextBox ID="RadTextBoxNote" runat="server" Width="250px" TextMode="MultiLine" />
        </td>
        <td>
            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorNote" runat="server" ControlToValidate="RadTextBoxNote"
                ValidationGroup="cambioStatoImpresa" ErrorMessage="Immettere le note">
                *
            </asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    
    <tr><td colspan="4"><hr /></td></tr>
    <tr style="border:solid; border-width:1px;">
        <td class="anagraficaImpreseTd">
            <b>Libro unico*</b>
        </td>
        <td class="style1">
            <telerik:RadAsyncUpload runat="server" ID="AsyncUploadAllegatoLibroUnico"
                HideFileInput="true"
                MultipleFileSelection="Disabled"
                AllowedFileExtensions=".tif,.pdf"
                MaxFileSize="10000000" Enabled="false" >
                <Localization Cancel="Annulla" DropZone="Trascina i file qui" Remove="Rimuovi" 
                    Select="Seleziona" />
            </telerik:RadAsyncUpload>
            <span class="allowed-attachments">Scegli i file da allegare (<%= String.Join(",", AsyncUploadAllegatoLibroUnico.AllowedFileExtensions)%>)
            </span>
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorAllegatoLibroUnico"
                runat="server"
                ValidationGroup="cambioStatoImpresa"
                ErrorMessage="Selezionare un allegato per il libro unico" 
                onservervalidate="CustomValidatorAllegatoLibroUnico_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr><td colspan="4"><hr /></td></tr>
    <tr style="border:solid; border-width:1px;">
        <td class="anagraficaImpreseTd">
            <b>Uniemens*</b>
        </td>
        <td class="style1">
            <telerik:RadAsyncUpload runat="server" ID="AsyncUploadAllegatoUniemens"
                HideFileInput="true"
                MultipleFileSelection="Disabled"
                AllowedFileExtensions=".tif,.pdf,.UniEM,.xml"
                MaxFileSize="10000000" Enabled="false" >
                <Localization Cancel="Annulla" DropZone="Trascina i file qui" Remove="Rimuovi" 
                    Select="Seleziona" />
            </telerik:RadAsyncUpload>
            <span class="allowed-attachments">Scegli i file da allegare (<%= String.Join(",", AsyncUploadAllegatoUniemens.AllowedFileExtensions)%>)
            </span>
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorAllegatoUniemens"
                runat="server"
                ValidationGroup="cambioStatoImpresa"
                ErrorMessage="Selezionare un allegato per Uniemens" 
                onservervalidate="CustomValidatorAllegatoUniemens_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr><td colspan="4"><hr /></td></tr>
    <tr>
        <td class="anagraficaImpreseTd">
            <b>Denunce altre Casse Edili</b>
        </td>
        <td class="style1">
            <telerik:RadAsyncUpload runat="server" ID="AsyncUploadAllegatoDenunceAltreCasse"
                HideFileInput="true"
                MultipleFileSelection="Disabled"
                AllowedFileExtensions=".tif,.pdf"
                MaxFileSize="10000000" Enabled="false" >
                <Localization Cancel="Annulla" DropZone="Trascina i file qui" Remove="Rimuovi" 
                    Select="Seleziona" />
            </telerik:RadAsyncUpload>
            <span class="allowed-attachments">Scegli i file da allegare (<%= String.Join(",", AsyncUploadAllegatoDenunceAltreCasse.AllowedFileExtensions)%>)
            </span>
        </td>
        <td>
        </td>
    </tr>
    <tr><td colspan="4"><hr /></td></tr>
    <tr>
        <td style="font-size:smaller"><b>LEGENDA stati dell'allegato</b></td>
        <td colspan="3">
        <table>
            
            <tr style="text-align:left">
                <td>
                    <img runat="server" src="../../images/dot_green.gif" /></td>
                <td style="font-size:smaller">File valido per l'invio della richiesta</td>
            </tr>
            <tr style="text-align:left">
                <td>
                    <img runat="server" src="../../images/dot_orange.gif" /></td>
                <td style="font-size:smaller">File in caricamento / in fase validazione</td>
            </tr>
            <tr style="text-align:left">
                <td>
                    <img runat="server" src="../../images/dot_red.gif" /></td>
                <td style="font-size:smaller">File <b>NON VALIDO</b>! Il documento <b>NON</b> verrà utilizzato per la richiesta di cambio stato</td>
            </tr>
        </table>
        </td></tr>
    <tr><td colspan="4"><hr /></td></tr>
    <tr>
        <td colspan="4">
            Per informazioni contattare l’ufficio Servizi alle Imprese (tel. 02.584961 – tasto 1)
        </td>
    </tr>
    <tr>
        <td>
            <asp:ValidationSummary ID="ValidationSummaryCambioStato" runat="server" CssClass="messaggiErrore"
                ValidationGroup="cambioStatoImpresa" />
        </td>
        <td class="style1">
            <asp:Button ID="RadButtonOk" runat="server" Text="Invia Richiesta" OnClick="RadButtonOk_Click"
                ValidationGroup="cambioStatoImpresa">
            </asp:Button>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelEsito" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
</table>
