﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AnagraficaImpreseDefault.aspx.cs" Inherits="AnagraficaImprese_AnagraficaImpreseDefault" %>

<%@ Register Src="../WebControls/MenuAnagraficaImpreseVariazioni.ascx" TagName="MenuAnagraficaImpreseVariazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello web"
        sottoTitolo="Variazione recapiti e stato" />
    <p>
        Per comunicare in modo rapido ed efficace l’aggiornamento dei recapiti della sede legale, 
        amministrativa e di corrispondenza e dello stato (sospensione, riattivazione, cessazione) di un’impresa iscritta.
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuAnagraficaImpreseVariazioni ID="MenuAnagraficaImpreseVariazioni1" runat="server" />
</asp:Content>
