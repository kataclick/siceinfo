﻿using System;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Collections.Generic;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;
using Telerik.Web.UI;
using Cemi.SiceInfo.Type.Delegates;
using Documento = Cemi.SiceInfo.Type.Domain.Documento;
using System.Web;

public partial class AnagraficaImprese_GestioneRichiesteCambioStato : Page
{
    private readonly ImpresaVariazioneStatoManager _impresaVariazioneStatoManager = new ImpresaVariazioneStatoManager();
    public event RichiestaDocumentoSelectedEventHandler OnDocumentSelected;    

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseGestioneRichiesteCambioStato);

        if (!Page.IsPostBack)
        {
            CaricaCausali();
            RadButtonRifiutaRichiesta.Enabled = false;
            DropDownListCausaliRespinta.EmptyMessage = "Selezionare una causale di rifiuto";
        }        

        RichiesteCambioStatoImpresa1.OnRichiestaSelected += RichiesteCambioStatoImpresa1_OnRichiestaSelected;

        RichiesteCambioStatoImpresa1.OnRicercaRichiestaSelected +=
            RichiesteCambioStatoImpresa1_OnRicercaRichiestaSelected;

        OnDocumentSelected += RichiestaDocumento_OnDocumentSelected;

        RadScriptManager rsm = (RadScriptManager) Master.FindControl("RadScriptManagerMain");
        //rsm.RegisterPostBackControl(ButtonDownloadLibroUnico);
        //rsm.RegisterPostBackControl(ButtonDownloadDenunce);
        rsm.RegisterPostBackControl(RadGridLibroUnico);
        rsm.RegisterPostBackControl(RadGridUniemens);
        rsm.RegisterPostBackControl(RadGridDenunce);
        //rsm.RegisterPostBackControl(RadButtonRifiutaRichiesta);
        //rsm.RegisterPostBackControl(RadButtonAccettaRichiesta);
        rsm.RegisterPostBackControl(DropDownListCausaliRespinta);

        #region Per prevenire click multipli

        StringBuilder sbAccetta = new StringBuilder();
        sbAccetta.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbAccetta.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbAccetta.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbAccetta.Append("this.value = 'Attendere...';");
        sbAccetta.Append("this.disabled = true;");
        sbAccetta.Append(Page.ClientScript.GetPostBackEventReference(RadButtonAccettaRichiesta, null));
        sbAccetta.Append(";");
        sbAccetta.Append("return true;");
        RadButtonAccettaRichiesta.Attributes.Add("onclick", sbAccetta.ToString());

        StringBuilder sbRifiuta = new StringBuilder();
        sbRifiuta.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbRifiuta.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbRifiuta.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbRifiuta.Append("this.value = 'Attendere...';");
        sbRifiuta.Append("this.disabled = true;");
        sbRifiuta.Append(Page.ClientScript.GetPostBackEventReference(RadButtonRifiutaRichiesta, null));
        sbRifiuta.Append(";");
        sbRifiuta.Append("return true;");
        RadButtonRifiutaRichiesta.Attributes.Add("onclick", sbRifiuta.ToString());

        StringBuilder sbSospendi = new StringBuilder();
        sbSospendi.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbSospendi.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbSospendi.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbSospendi.Append("this.value = 'Attendere...';");
        sbSospendi.Append("this.disabled = true;");
        sbSospendi.Append(Page.ClientScript.GetPostBackEventReference(RadButtonSospendiRichiesta, null));
        sbSospendi.Append(";");
        sbSospendi.Append("return true;");
        RadButtonSospendiRichiesta.Attributes.Add("onclick", sbSospendi.ToString());

        #endregion
    }

    private void RichiestaDocumento_OnDocumentSelected(int idDocumento, string documentaleId, string fileName)
    {

        if (String.IsNullOrEmpty(documentaleId))        
        { 
            byte[] allegato =
                _impresaVariazioneStatoManager.GetGenericAllegato(idDocumento);
            RestituisciFile(fileName, allegato);
        }
        {
            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            byte[] file = servizioArchidoc.GetDocument(documentaleId);

            Presenter.RestituisciFileArchidoc(documentaleId, file, Path.GetExtension(fileName), Page);
        }


    }

    private void RetrieveDocument(int idDocumento, string documentaleId, string fileName)
    {
        // TO DO - Capire se archidoc o no
        if (String.IsNullOrEmpty(documentaleId))
        {
            byte[] allegato =
                _impresaVariazioneStatoManager.GetGenericAllegato(idDocumento);
            RestituisciFile(fileName, allegato);
        }
        {
            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            byte[] file = servizioArchidoc.GetDocument(documentaleId);

            Presenter.RestituisciFileArchidoc(documentaleId, file, Path.GetExtension(fileName), Page);
        }
    }

    private void RichiesteCambioStatoImpresa1_OnRichiestaSelected(int idRichiesta)
    {
        ResetCampi();
        PanelDettaglioRchiesta.Visible = true;

        ImpresaVariazioneStato impresaVariazioneStato =
            _impresaVariazioneStatoManager.GetImpresaVariazioneStatoByIdRichiesta(idRichiesta);

        RadTextBoxUtente.Text = impresaVariazioneStato.Login;
        RadTextBoxIdRichiesta.Text = impresaVariazioneStato.IdRichiesta.ToString();
        RadTextBoxDataRichiesta.Text = impresaVariazioneStato.DataRichiesta.ToShortDateString();
        RadTextBoxImpresa.Text = impresaVariazioneStato.NomeComposto;
        RadTextBoxStatoImpresaAttuale.Text = impresaVariazioneStato.StatoImpresaAttuale;
        RadTextBoxStatoImpresaRichiesto.Text = impresaVariazioneStato.StatoImpresa;
        RadTextBoxDataInizioCambioStato.Text = impresaVariazioneStato.DataInizioCambioStato.ToShortDateString();
        //RadTextBoxStatoPratica.Text = impresaVariazioneStato.StatoPratica;
        RadTextBoxStatoPratica.Text = impresaVariazioneStato.DataGestioneOperatore.HasValue
            ? $"{impresaVariazioneStato.StatoPratica} ({impresaVariazioneStato.DataGestioneOperatore.Value})"
            : impresaVariazioneStato.StatoPratica;
        RadTextBoxNote.Text = impresaVariazioneStato.Note;

        if (!string.IsNullOrWhiteSpace(impresaVariazioneStato.AllegatoLibroUnicoNome) || !string.IsNullOrWhiteSpace(impresaVariazioneStato.AllegatoLibroUnicoIdArchidoc))
        {

            // POPOLAZIONE GRIDVIEW LIBRO UNICO
            List<Documento> docLibroUnico = new List<Documento>();
            List<Documento> docUniemens = new List<Documento>();
            List<Documento> docDenunce = new List<Documento>();

            foreach (Documento d in impresaVariazioneStato.AltriAllegati)
            {
                if (d.TipoDocumento == Cemi.SiceInfo.Type.Enum.TipiDocumentiStored.LibroUnico.ToString())
                {
                    docLibroUnico.Add(d);
                }
                if (d.TipoDocumento == Cemi.SiceInfo.Type.Enum.TipiDocumentiStored.Uniemens.ToString())
                {
                    docUniemens.Add(d);
                }
                if (d.TipoDocumento == Cemi.SiceInfo.Type.Enum.TipiDocumentiStored.Denunce.ToString())
                {
                    docDenunce.Add(d);
                }
            }

            if (docLibroUnico.Count > 0)
            {
                RadGridLibroUnico.DataSource = docLibroUnico;
                RadGridLibroUnico.DataBind();
                RadGridLibroUnico.Visible = true;
                LabelLibroUnicoNonDisponibile.Visible = false;
            }
            else
            {
                RadGridLibroUnico.Visible = false;
                LabelLibroUnicoNonDisponibile.Visible = true;
            }

            if (docUniemens.Count > 0)
            {
                RadGridUniemens.DataSource = docUniemens;
                RadGridUniemens.DataBind();
                RadGridUniemens.Visible = true;
                LabelUniemensNonDisponibile.Visible = false;
            }
            else
            {
                RadGridUniemens.Visible = false;
                LabelUniemensNonDisponibile.Visible = true;
            }

            if (docDenunce.Count > 0)
            {
                RadGridDenunce.DataSource = docDenunce;
                RadGridDenunce.DataBind();
                RadGridDenunce.Visible = true;
                LabelDenunceNonDisponibile.Visible = false;
            }
            else
            {
                RadGridDenunce.Visible = false;
                LabelDenunceNonDisponibile.Visible = true;
            }
            //

            //ButtonDownloadLibroUnico.Visible = false;
            //LabelLibroUnicoNonDisponibile.Visible = false;
            //ButtonDownloadDenunce.Visible = true;
            //LabelDenunceNonDisponibile.Visible = false;
            //ViewState["AllegatoLibroUnicoNome"] = impresaVariazioneStato.AllegatoLibroUnicoNome; //TODO
            //ViewState["AllegatoLibroUnicoIdArchidoc"] = impresaVariazioneStato.AllegatoLibroUnicoIdArchidoc;

            if (!string.IsNullOrWhiteSpace(impresaVariazioneStato.AllegatoLibroUnicoIdArchidoc))
            {
                //LabelLibroUnicoNonDisponibile.Visible = true;
                //LabelLibroUnicoNonDisponibile.Text = "Archidoc";
                //LabelDenunceNonDisponibile.Visible = true;
                //LabelDenunceNonDisponibile.Text = "Archidoc";
            }
        }
        else
        {
            //ButtonDownloadLibroUnico.Visible = false;
            //LabelLibroUnicoNonDisponibile.Visible = true;
            //ButtonDownloadDenunce.Visible = false;
            //LabelDenunceNonDisponibile.Visible = true;
        }

        //if (!string.IsNullOrWhiteSpace(impresaVariazioneStato.AllegatoDenunceNome) || !string.IsNullOrWhiteSpace(impresaVariazioneStato.AllegatoDenunceIdArchidoc))
        //{
        //    ButtonDownloadDenunce.Visible = true;
        //    LabelDenunceNonDisponibile.Visible = false;
        //    ViewState["AllegatoDenunceNome"] = impresaVariazioneStato.AllegatoDenunceNome; 
        //    ViewState["AllegatoDenunceIdArchidoc"] = impresaVariazioneStato.AllegatoDenunceIdArchidoc;

        //    if (!string.IsNullOrWhiteSpace(impresaVariazioneStato.AllegatoDenunceIdArchidoc))
        //    {
        //        LabelDenunceNonDisponibile.Visible = true;
        //        LabelDenunceNonDisponibile.Text = "Archidoc";
        //    }
        //}
        //else
        //{
        //    ButtonDownloadDenunce.Visible = false;
        //    LabelDenunceNonDisponibile.Visible = true;
        //}

        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.DataUltimaDenuncia.HasValue)
            RadTextBoxDataUltimaDenuncia.Text =
                impresaVariazioneStato.InfoAggiuntiveAnalisi.DataUltimaDenuncia.Value.ToString("MM/yyyy");
        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.OreUltimaDenuncia.HasValue)
            RadTextBoxOreUltimaDenuncia.Text =
                impresaVariazioneStato.InfoAggiuntiveAnalisi.OreUltimaDenuncia.Value.ToString();
        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.OreLavorateUltimaDenuncia.HasValue)
            RadTextBoxOreLavorateUltimaDenuncia.Text =
                impresaVariazioneStato.InfoAggiuntiveAnalisi.OreLavorateUltimaDenuncia.Value.ToString();
        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.DataIscrizione.HasValue)
            RadTextBoxDataIscrizione.Text =
                impresaVariazioneStato.InfoAggiuntiveAnalisi.DataIscrizione.Value.ToShortDateString();
        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.DataRiattivazione.HasValue)
            RadTextBoxDataRipresa.Text =
                impresaVariazioneStato.InfoAggiuntiveAnalisi.DataRiattivazione.Value.ToShortDateString();
        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.DataSospensione.HasValue)
            RadTextBoxDataSospensione.Text =
                impresaVariazioneStato.InfoAggiuntiveAnalisi.DataSospensione.Value.ToShortDateString();
        if (impresaVariazioneStato.InfoAggiuntiveAnalisi.Debito.HasValue)
            RadTextBoxDebito.Text = impresaVariazioneStato.InfoAggiuntiveAnalisi.Debito.Value.ToString();
        if (impresaVariazioneStato.IdTipoCausaleRespinta.HasValue)
        {
            DropDownListCausaliRespinta.SelectedValue = impresaVariazioneStato.IdTipoCausaleRespinta.ToString();
            
            List<TBridge.Cemi.Type.Domain.TipoCausaleRespintaVariazioneStatoImpresa> tipiCausaliRespinta = _impresaVariazioneStatoManager.GetTipiCausaliRespinta();
            foreach (TBridge.Cemi.Type.Domain.TipoCausaleRespintaVariazioneStatoImpresa tc in tipiCausaliRespinta)
            {
                if (tc.IdTipoCausaleRespintaVariazioneStatoImpresa.ToString() == DropDownListCausaliRespinta.SelectedValue)
                {
                    RadTextBoxCausaleRespinta.Text = tc.Descrizione;
                }
            }
        }
            
        

            if (impresaVariazioneStato.IdTipoStatoPratica == (int) TipoStatoGestionePratica.Approvata ||
            impresaVariazioneStato.IdTipoStatoPratica == (int) TipoStatoGestionePratica.Rifiutata)
        {
            RadButtonSospendiRichiesta.Enabled = false;
            RadButtonRifiutaRichiesta.Enabled = false;
            RadButtonAccettaRichiesta.Enabled = false;
            DropDownListCausaliRespinta.Enabled = false;
            DropDownListCausaliRespinta.Visible = false;
            //RadTextBoxCausaleRespinta.Visible = true;
            RowCausaleRespinta.Visible = true;
        }
        else
        {
            RadButtonSospendiRichiesta.Enabled = true;
            DropDownListCausaliRespinta.Enabled = true;
            DropDownListCausaliRespinta.Visible = true;
            if (DropDownListCausaliRespinta.SelectedIndex > 0)
            {
                RadButtonRifiutaRichiesta.Enabled = true;
            }
            //RadTextBoxCausaleRespinta.Visible = false;
            RowCausaleRespinta.Visible = false;
            RadButtonAccettaRichiesta.Enabled = true;
        }
    }

    private void ResetCampi()
    {
        Presenter.SvuotaCampo(RadTextBoxUtente);
        Presenter.SvuotaCampo(RadTextBoxIdRichiesta);
        Presenter.SvuotaCampo(RadTextBoxDataRichiesta);
        Presenter.SvuotaCampo(RadTextBoxImpresa);
        Presenter.SvuotaCampo(RadTextBoxStatoImpresaAttuale);
        Presenter.SvuotaCampo(RadTextBoxStatoImpresaRichiesto);
        Presenter.SvuotaCampo(RadTextBoxDataInizioCambioStato);
        Presenter.SvuotaCampo(RadTextBoxStatoPratica);
        Presenter.SvuotaCampo(RadTextBoxNote);
        Presenter.SvuotaCampo(RadTextBoxDataUltimaDenuncia);
        Presenter.SvuotaCampo(RadTextBoxOreUltimaDenuncia);
        Presenter.SvuotaCampo(RadTextBoxOreLavorateUltimaDenuncia);
        Presenter.SvuotaCampo(RadTextBoxDataIscrizione);
        Presenter.SvuotaCampo(RadTextBoxDataRipresa);
        Presenter.SvuotaCampo(RadTextBoxDataSospensione);
        Presenter.SvuotaCampo(RadTextBoxDebito);
        Presenter.SvuotaCampo(RadTextBoxCausaleRespinta);
        DropDownListCausaliRespinta.SelectedIndex = 0;
    }

    private void RichiesteCambioStatoImpresa1_OnRicercaRichiestaSelected()
    {
        PanelDettaglioRchiesta.Visible = false;

        RichiesteCambioStatoImpresa1.CaricaRicerca();
    }

    private void CaricaCausali()
    {
        List<TBridge.Cemi.Type.Domain.TipoCausaleRespintaVariazioneStatoImpresa> tipiCausaliRespinta = _impresaVariazioneStatoManager.GetTipiCausaliRespinta();

            Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCausaliRespinta,
            tipiCausaliRespinta,
            "Descrizione",
            "IdTipoCausaleRespintaVariazioneStatoImpresa");

    }
    protected void RadButtonAccettaRichiesta_Click(object sender, EventArgs e)
    {
        _impresaVariazioneStatoManager.UpdateStatoPratica(Convert.ToInt32(RadTextBoxIdRichiesta.Text),
            (int) TipoStatoGestionePratica.Approvata,
            GestioneUtentiBiz.GetIdUtente());

        RichiesteCambioStatoImpresa1_OnRicercaRichiestaSelected();
    }

    protected void RadButtonRifiutaRichiesta_Click(object sender, EventArgs e)
    {
        _impresaVariazioneStatoManager.UpdateStatoPratica(Convert.ToInt32(RadTextBoxIdRichiesta.Text),
            (int)TipoStatoGestionePratica.Rifiutata,
            GestioneUtentiBiz.GetIdUtente(),
            Convert.ToInt32(DropDownListCausaliRespinta.SelectedValue));

        RichiesteCambioStatoImpresa1_OnRicercaRichiestaSelected();
    }

    protected void RadButtonSospendiRichiesta_Click(object sender, EventArgs e)
    {
        _impresaVariazioneStatoManager.UpdateStatoPratica(Convert.ToInt32(RadTextBoxIdRichiesta.Text),
            (int) TipoStatoGestionePratica.InAttesaDiDocumentazione,
            GestioneUtentiBiz.GetIdUtente());

        RichiesteCambioStatoImpresa1_OnRicercaRichiestaSelected();
    }

    //protected void ButtonDownloadLibroUnico_Click(object sender, EventArgs e)
    //{
    //    string idArchidoc = ViewState["AllegatoLibroUnicoIdArchidoc"] as string;
    //    string allegatoNome = ViewState["AllegatoLibroUnicoNome"] as string;

    //    if (string.IsNullOrEmpty(idArchidoc))
    //    {
    //        byte[] allegato =
    //            _impresaVariazioneStatoManager.GetImpresaVariazioneStatoAllegatoLibroUnico(
    //                Convert.ToInt32(RadTextBoxIdRichiesta.Text));
    //        RestituisciFile(allegatoNome, allegato);
    //    }
    //    else
    //    {
    //        IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
    //        byte[] file = servizioArchidoc.GetDocument(idArchidoc);

    //        Presenter.RestituisciFileArchidoc(idArchidoc, file, Path.GetExtension(allegatoNome), Page);
    //    }
    //}

    //protected void ButtonDownloadDenunce_Click(object sender, EventArgs e)
    //{
    //    //DENUNCE non obbligatorie - possibile allegato NULL!!!

    //    string idArchidoc = ViewState["AllegatoDenunceIdArchidoc"] as string;
    //    string allegatoNome = ViewState["AllegatoDenunceNome"] as string;

    //    if (string.IsNullOrEmpty(idArchidoc))
    //    {
    //        byte[] allegato =
    //            _impresaVariazioneStatoManager.GetImpresaVariazioneStatoAllegatoDenunce(
    //                Convert.ToInt32(RadTextBoxIdRichiesta.Text));
    //        RestituisciFile(allegatoNome, allegato);
    //    }
    //    else
    //    {
    //        IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
    //        byte[] file = servizioArchidoc.GetDocument(idArchidoc);

    //        Presenter.RestituisciFileArchidoc(idArchidoc, file, Path.GetExtension(allegatoNome), Page);
    //    }
    //}

    protected void RestituisciFile(string nomeFile, byte[] file)
    {
        switch (Path.GetExtension(nomeFile).ToUpper())
        {
            case ".PDF":
                Response.ContentType = "application/pdf";
                break;
            case ".TIF":
                Response.ContentType = "image/tiff";
                break;
            case ".XML":
                Response.ContentType = "text/xml";
                break;
            default:
                Response.ContentType = "application/pdf";
                break;
        }

        Response.AddHeader("Content-Disposition", $"attachment; filename={nomeFile}");
        Response.BinaryWrite(file);
        Response.Flush();
        Response.End();
    }

    protected void DropDownListCausaliRespinta_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListCausaliRespinta.SelectedIndex <= 0)
        {
            RadButtonRifiutaRichiesta.Enabled = false;
        }
        else
        {
            RadButtonRifiutaRichiesta.Enabled = true;
        }
    }

    protected void DropDownListCausaliRespinta_ItemCreated(object sender, RadComboBoxItemEventArgs e)
    {
        RadComboBoxItem item = e.Item;

        if (item.Index % 2 == 0)
        {
            // Pari
            item.BackColor = System.Drawing.Color.White;
        }
        else
        {
            // Dispari
            item.BackColor = System.Drawing.Color.LightGray;
        }
        
    }

    protected void RadGridLibroUnico_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridLibroUnico.SelectedIndexes[0]);
        Int32 idDocumento =
            (Int32)RadGridLibroUnico.MasterTableView.DataKeyValues[indiceSelezionato]["Id"];
        string documentaleId =
            (string)RadGridLibroUnico.MasterTableView.DataKeyValues[indiceSelezionato]["DocumentaleId"];
        string fileName =
            (string)RadGridLibroUnico.MasterTableView.DataKeyValues[indiceSelezionato]["Filename"];


        RetrieveDocument(idDocumento, documentaleId, fileName);
        
    }

    protected void RadGridUniemens_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridUniemens.SelectedIndexes[0]);
        Int32 idDocumento =
            (Int32)RadGridUniemens.MasterTableView.DataKeyValues[indiceSelezionato]["Id"];
        string documentaleId =
            (string)RadGridUniemens.MasterTableView.DataKeyValues[indiceSelezionato]["DocumentaleId"];
        string fileName =
            (string)RadGridUniemens.MasterTableView.DataKeyValues[indiceSelezionato]["Filename"];

        RetrieveDocument(idDocumento, documentaleId, fileName);

    }

    protected void RadGridDenunce_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridDenunce.SelectedIndexes[0]);
        Int32 idDocumento =
            (Int32)RadGridDenunce.MasterTableView.DataKeyValues[indiceSelezionato]["Id"];
        string documentaleId =
            (string)RadGridDenunce.MasterTableView.DataKeyValues[indiceSelezionato]["DocumentaleId"];
        string fileName =
            (string)RadGridDenunce.MasterTableView.DataKeyValues[indiceSelezionato]["Filename"];

        RetrieveDocument(idDocumento, documentaleId, fileName);
    }

}