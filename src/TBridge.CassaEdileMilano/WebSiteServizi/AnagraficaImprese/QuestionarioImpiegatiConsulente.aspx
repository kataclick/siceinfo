﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="QuestionarioImpiegatiConsulente.aspx.cs" Inherits="AnagraficaImprese_QuestionarioImpiegatiConsulente" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello Web"
        sottoTitolo="Questionario Impiegati Imprese" />
    <br />
    <div class="standardDiv">
        <table class="standardTable">
            <tr runat="server" id="trFiltriImpresa">
                <td>
                    Codice Impresa
                </td>
                <td>
                    Ragione sociale Impresa
                </td>
                <td>
                    Codice fiscale Impresa
                </td>
                <td>
                    Questionario compilato
                </td>
            </tr>
            <tr runat="server" id="trFiltriImpresa1">
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresa" runat="server" Type="Number"
                        DataType="System.Int32" MinValue="1">
                        <NumberFormat GroupSeparator="" DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" />
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxCF" runat="server" />
                </td>
                <td>
                    <telerik:RadComboBox
                        ID="RadComboBoxQuestionario"
                        runat="server"
                        Width="100px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Tutti" Value="TUTTI" Selected="true" />
                            <telerik:RadComboBoxItem runat="server" Text="Sì" Value="SI" />
                            <telerik:RadComboBoxItem runat="server" Text="No" Value="NO" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                        ValidationGroup="Ricerca" />
                </td>
                <td align="right">
                    <br />
                    <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                        ValidationGroup="Ricerca" Width="100px" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="standardDiv">
        <telerik:RadGrid ID="RadGridImprese" runat="server" GridLines="None" OnItemDataBound="RadGridImprese_ItemDataBound"
            Width="100%" OnPageIndexChanged="RadGridImprese_PageIndexChanged" CellSpacing="0"
            OnItemCommand="RadGridImprese_ItemCommand" AllowPaging="True" Visible = "False">
            <ClientSettings>
                <Selecting CellSelectionMode="None"></Selecting>
            </ClientSettings>
            <MasterTableView DataKeyNames="IdImpresa">
                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Codice Impresa" UniqueName="idImpresa" DataField="IdImpresa">
                        <ItemStyle Width="60px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Ragione Sociale" UniqueName="ragioneSociale"
                        DataField="RagioneSociale">
                        <ItemStyle Font-Bold="True" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Partita IVA" UniqueName="partitaIVA" DataField="PartitaIva">
                        <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Codice fiscale" UniqueName="codiceFiscale" DataField="CodiceFiscale">
                        <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridCheckBoxColumn HeaderText="Questionario presente" DataField="QuestionarioPresente"
                        FilterControlAltText="Filter questionario column" UniqueName="questionario" ItemStyle-HorizontalAlign="Center"
                        FooterStyle-BorderStyle="NotSet" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle Width="30px" />
                    </telerik:GridCheckBoxColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="gestione" FilterControlAltText="Filter column column"
                        ImageUrl="~/images/edit.png" UniqueName="column">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
    <br />
</asp:Content>
