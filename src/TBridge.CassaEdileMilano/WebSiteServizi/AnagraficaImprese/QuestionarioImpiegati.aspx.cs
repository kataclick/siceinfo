﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Entities;

public partial class AnagraficaImprese_QuestionarioImpiegati : System.Web.UI.Page
{
    private Int32 counterParziali = 1;
    private Int32 counterPieni = 1;

    private ImpresaQuestionarioManager questionarioManager = new ImpresaQuestionarioManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();
        funzionalita.Add(FunzionalitaPredefinite.ImpreseQuestionario);
        funzionalita.Add(FunzionalitaPredefinite.ImpreseQuestionarioConsulenti);
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        if (!Page.IsPostBack)
        {
            Presenter.CaricaElementiInListView(
                RadListViewScadenzaParziali,
                new Durata[0]);
            Presenter.CaricaElementiInListView(
                RadListViewScadenzaPieni,
                new Durata[0]);


            Int32 idImpresa = 0;
            if (Context.Items["IdImpresa"] != null)
            {
                ViewState["IdImpresa"] = (int)Context.Items["IdImpresa"];
                idImpresa = (Int32)ViewState["IdImpresa"];
            }

            if (idImpresa != 0)
            {
                CaricaQuestionario(idImpresa);
            }
            else
            {
                if (GestioneUtentiBiz.IsImpresa())
                {
                    TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresa =
                        (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    CaricaQuestionario(impresa.IdImpresa);
                }
                else
                {
                    Server.Transfer("~/Default.aspx");
                }
            }
        }
    }

    private void CaricaQuestionario(Int32 idImpresa)
    {
        ViewState["IdImpresa"] = idImpresa;

        ImpresaQuestionarioImpiegati questionario = questionarioManager.GetQuestionario(idImpresa);
        if (questionario != null)
        {
            RadNumericTextBoxForzaAmministrativi.Value = questionario.ImpiegatiForzaAmministrativi;
            RadNumericTextBoxForzaTecnici.Value = questionario.ImpiegatiForzaTecnici;
            RadNumericTextBoxIndeterminatiParziali.Value = questionario.ImpiegatiIndeterminatiParziali;
            RadNumericTextBoxIndeterminatiPieni.Value = questionario.ImpiegatiIndeterminatiPieni;
            RadNumericTextBoxDeterminatiParziali.Value = questionario.ImpiegatiDeterminatiParziali;
            RadNumericTextBoxDeterminatiPieni.Value = questionario.ImpiegatiDeterminatiPieni;

            if (questionario.ScadenzeParziali != null &&
                questionario.ScadenzeParziali.Count > 0)
            {
                Presenter.CaricaElementiInListView(
                    RadListViewScadenzaParziali,
                    questionario.ScadenzeParziali.ToArray());
            }
            else
            {
                Presenter.CaricaElementiInListView(
                    RadListViewScadenzaParziali,
                    new Durata[questionario.ImpiegatiDeterminatiParziali]);
            }

            if (questionario.ScadenzePiene != null &&
                questionario.ScadenzePiene.Count > 0)
            {
                Presenter.CaricaElementiInListView(
                    RadListViewScadenzaPieni,
                    questionario.ScadenzePiene.ToArray());
            }
            else
            {
                Presenter.CaricaElementiInListView(
                    RadListViewScadenzaPieni,
                    new Durata[questionario.ImpiegatiDeterminatiPieni]);
            }
            
            RadNumericTextBoxAssenzeMalattie.Value = questionario.OreAssenzaMalattia;
            RadNumericTextBoxAssenzeCassa.Value = questionario.OreAssenzaCassa;
        }
    }

    protected void ButtonDeterminatiParzialiAggiungiScadenza_Click(object sender, EventArgs e)
    {
        CreaScadenzeParziali(1);
    }

    protected void ButtonDeterminatiParzialiRimuoviScadenza_Click(object sender, EventArgs e)
    {
        CreaScadenzeParziali(-1);
    }

    protected void ButtonDeterminatiPienoAggiungiScadenza_Click(object sender, EventArgs e)
    {
        CreaScadenzePiene(1);
    }

    protected void ButtonDeterminatiPienoRimuoviScadenza_Click(object sender, EventArgs e)
    {
        CreaScadenzePiene(-1);
    }

    protected void RadButtonSalva_Click(object sender, EventArgs e)
    {
        Presenter.SvuotaCampo(LabelMessaggio);

        if (Page.IsValid)
        {
            ImpresaQuestionarioImpiegati questionario = CreaQuestionario();
            questionarioManager.SaveQuestionario(questionario);

            LabelMessaggio.Text = "I dati sono stati correttamente salvati.";
            
            if (GestioneUtentiBiz.IsConsulente())
            {
                //Context.Items["func"] = 1;
                Server.Transfer("~/AnagraficaImprese/QuestionarioImpiegatiConsulente.aspx");
            }

        }
    }

    private ImpresaQuestionarioImpiegati CreaQuestionario()
    {
        ImpresaQuestionarioImpiegati questionario = new ImpresaQuestionarioImpiegati();

        questionario.IdImpresa = (Int32) ViewState["IdImpresa"];
        questionario.ImpiegatiForzaAmministrativi = Convert.ToInt16(RadNumericTextBoxForzaAmministrativi.Value);
        questionario.ImpiegatiForzaTecnici = Convert.ToInt16(RadNumericTextBoxForzaTecnici.Value);
        questionario.ImpiegatiIndeterminatiParziali = Convert.ToInt16(RadNumericTextBoxIndeterminatiParziali.Value);
        questionario.ImpiegatiIndeterminatiPieni = Convert.ToInt16(RadNumericTextBoxIndeterminatiPieni.Value);
        questionario.ImpiegatiDeterminatiParziali = Convert.ToInt16(RadNumericTextBoxDeterminatiParziali.Value);
        questionario.ImpiegatiDeterminatiPieni = Convert.ToInt16(RadNumericTextBoxDeterminatiPieni.Value);
        questionario.Scadenze = new List<ImpresaQuestionarioImpiegatiDeterminatiScadenza>();

        Durata[] scadenzeParziali = GetScadenzeParziali(0);
        if (questionario.ImpiegatiDeterminatiParziali > 0)
        {
            for (Int32 i = 0; i < scadenzeParziali.Length; i++)
            {
                ImpresaQuestionarioImpiegatiDeterminatiScadenza scad = new ImpresaQuestionarioImpiegatiDeterminatiScadenza();
                scad.TipoOrario = Convert.ToInt16((Int32) ImpresaQuestionarioTipoScadenza.Parziali);
                scad.IdImpresa = questionario.IdImpresa;
                scad.DataInizio = scadenzeParziali[i].Inizio;
                scad.DataScadenza = scadenzeParziali[i].Fine;

                questionario.Scadenze.Add(scad);
            }
        }

        Durata[] scadenzePieni = GetScadenzePieni(0);
        if (questionario.ImpiegatiDeterminatiPieni > 0)
        {
            for (Int32 i = 0; i < scadenzePieni.Length; i++)
            {
                ImpresaQuestionarioImpiegatiDeterminatiScadenza scad = new ImpresaQuestionarioImpiegatiDeterminatiScadenza();
                scad.TipoOrario = Convert.ToInt16((Int32) ImpresaQuestionarioTipoScadenza.Piene);
                scad.IdImpresa = questionario.IdImpresa;
                scad.DataInizio = scadenzePieni[i].Inizio;
                scad.DataScadenza = scadenzePieni[i].Fine;

                questionario.Scadenze.Add(scad);
            }
        }

        questionario.OreAssenzaMalattia = Convert.ToInt16(RadNumericTextBoxAssenzeMalattie.Value);
        questionario.OreAssenzaCassa = Convert.ToInt16(RadNumericTextBoxAssenzeCassa.Value);

        return questionario;
    }

    protected void RadListViewScadenzaPieni_ItemDataBound(object sender, Telerik.Web.UI.RadListViewItemEventArgs e)
    {
        if (e.Item.ItemType == Telerik.Web.UI.RadListViewItemType.DataItem
            || e.Item.ItemType == Telerik.Web.UI.RadListViewItemType.AlternatingItem)
        {
            Label lNumero = (Label) e.Item.FindControl("LabelNumero");
            RadDatePicker rdpInizio = (RadDatePicker) e.Item.FindControl("RadDatePickerInizioPieno");
            RadDatePicker rdpScadenza = (RadDatePicker) e.Item.FindControl("RadDatePickerScadenzaPieno");
            RequiredFieldValidator rfvScadenza = (RequiredFieldValidator) e.Item.FindControl("RequiredFieldValidatorScadenzaPieno");
            CompareValidator cvScadenza = (CompareValidator) e.Item.FindControl("CompareValidatorDeterminatiPieno");
            CompareValidator cvScadenzaMinore = (CompareValidator) e.Item.FindControl("CompareValidatorDeterminatiPienoMinore");
            Button bAggiungiScadenza = (Button) e.Item.FindControl("ButtonDeterminatiPienoAggiungiScadenza");
            Button bRimuoviScadenza = (Button) e.Item.FindControl("ButtonDeterminatiPienoRimuoviScadenza");

            lNumero.Text = String.Format("#{0}", counterPieni);
            //rdpScadenza.MinDate = DateTime.Now;
            rfvScadenza.ErrorMessage = String.Format("Indicare la data di scadenza per il contratto a tempo determinato a tempo pieno #{0}", counterPieni);
            cvScadenza.ErrorMessage = String.Format("La data di scadenza per il contratto a tempo determinato a tempo pieno #{0} non può essere precedente all' {1}", counterParziali, cvScadenza.ValueToCompare);
            //cvScadenza.ValueToCompare = DateTime.Now.Date.ToString("dd/MM/yyyy");
            cvScadenzaMinore.ErrorMessage = String.Format("La data di inizio per il contratto a tempo determinato a tempo pieno #{0} non può essere successiva alla scadenza", counterPieni);
            counterPieni++;

            Durata dItem = (Durata) (((RadListViewDataItem) e.Item).DataItem);
            if (dItem != null)
            {
                rdpInizio.SelectedDate = dItem.Inizio;
                rdpScadenza.SelectedDate = dItem.Fine;
            }
            else
            {
                //rdpScadenza.MinDate = DateTime.Now;
            }

            if (((Telerik.Web.UI.RadListViewDataItem) (e.Item)).DataItemIndex != (((Durata[]) ((RadListView) e.Item.Parent.Parent).DataSource).Length - 1))
            {
                bAggiungiScadenza.Visible = false;
                bRimuoviScadenza.Visible = false;
            }

            if (((Durata[]) ((RadListView) e.Item.Parent.Parent).DataSource).Length == 1)
            {
                bRimuoviScadenza.Visible = false;
            }
        }
    }

    protected void RadListViewScadenzaParziali_ItemDataBound(object sender, Telerik.Web.UI.RadListViewItemEventArgs e)
    {
        if (e.Item.ItemType == Telerik.Web.UI.RadListViewItemType.DataItem
            || e.Item.ItemType == Telerik.Web.UI.RadListViewItemType.AlternatingItem)
        {
            Label lNumero = (Label) e.Item.FindControl("LabelNumero");
            RadDatePicker rdpInizio = (RadDatePicker) e.Item.FindControl("RadDatePickerInizioParziale");
            RadDatePicker rdpScadenza = (RadDatePicker) e.Item.FindControl("RadDatePickerScadenzaParziale");
            RequiredFieldValidator rfvScadenza = (RequiredFieldValidator) e.Item.FindControl("RequiredFieldValidatorScadenzaParziale");
            CompareValidator cvScadenza = (CompareValidator) e.Item.FindControl("CompareValidatorDeterminatiParziali");
            CompareValidator cvScadenzaMinore = (CompareValidator) e.Item.FindControl("CompareValidatorDeterminatiParzialiMinore");
            Button bAggiungiScadenza = (Button) e.Item.FindControl("ButtonDeterminatiParzialiAggiungiScadenza");
            Button bRimuoviScadenza = (Button) e.Item.FindControl("ButtonDeterminatiParzialiRimuoviScadenza");
            
            lNumero.Text = String.Format("#{0}", counterParziali);
            //rdpScadenza.MinDate = DateTime.Now;
            rfvScadenza.ErrorMessage = String.Format("Indicare la data di scadenza per il contratto a tempo determinato a tempo parziale #{0}", counterParziali);
            cvScadenza.ErrorMessage = String.Format("La data di scadenza per il contratto a tempo determinato a tempo parziale #{0}  non può essere precedente all' {1}", counterParziali, cvScadenza.ValueToCompare);
            //cvScadenza.ValueToCompare = DateTime.Now.Date.ToString("dd/MM/yyyy");
            cvScadenzaMinore.ErrorMessage = String.Format("La data di inizio per il contratto a tempo determinato a tempo parziale #{0} non può essere successiva alla scadenza", counterParziali);
            counterParziali++;

            Durata dItem = (Durata) (((RadListViewDataItem) e.Item).DataItem);
            if (dItem != null)
            {
                rdpInizio.SelectedDate = dItem.Inizio;
                rdpScadenza.SelectedDate = dItem.Fine;
            }
            else
            {
                //rdpScadenza.MinDate = DateTime.Now;
            }

            if (((Telerik.Web.UI.RadListViewDataItem) (e.Item)).DataItemIndex != (((Durata[]) ((RadListView) e.Item.Parent.Parent).DataSource).Length - 1))
            {
                bAggiungiScadenza.Visible = false;
                bRimuoviScadenza.Visible = false;
            }

            if (((Durata[]) ((RadListView) e.Item.Parent.Parent).DataSource).Length == 1)
            {
                bRimuoviScadenza.Visible = false;
            }
        }
    }

    protected void RadNumericTextBoxDeterminatiParziali_TextChanged(object sender, EventArgs e)
    {
        Presenter.SvuotaCampo(LabelMessaggio);
        CreaScadenzeParziali(0);
        CreaScadenzePiene(0);
    }

    protected void CreaScadenzeParziali(Int32 operazione)
    {
        Int32 numeroDeterminatiParziali = Convert.ToInt32(RadNumericTextBoxDeterminatiParziali.Value);
        //Presenter.CaricaElementiInListView(
        //    RadListViewScadenzaParziali,
        //    GetScadenzeParziali(numeroDeterminatiParziali));
        if (numeroDeterminatiParziali > 0)
        {
            Presenter.CaricaElementiInListView(
                RadListViewScadenzaParziali,
                GetScadenzeParziali(operazione));
        }
        else
        {
            Presenter.CaricaElementiInListView(
                RadListViewScadenzaParziali,
                null);
        }
    }

    protected void RadNumericTextBoxDeterminatiPieni_TextChanged(object sender, EventArgs e)
    {
        Presenter.SvuotaCampo(LabelMessaggio);
        CreaScadenzeParziali(0);
        CreaScadenzePiene(0);
    }

    protected void CreaScadenzePiene(Int32 operazione)
    {
        Int32 numeroDeterminatiPieni = Convert.ToInt32(RadNumericTextBoxDeterminatiPieni.Value);
        //Presenter.CaricaElementiInListView(
        //    RadListViewScadenzaPieni,
        //    GetScadenzePieni(numeroDeterminatiPieni));
        if (numeroDeterminatiPieni > 0)
        {
            Presenter.CaricaElementiInListView(
                RadListViewScadenzaPieni,
                GetScadenzePieni(operazione));
        }
        else
        {
            Presenter.CaricaElementiInListView(
                RadListViewScadenzaPieni,
                null);
        }
    }

    protected Durata[] GetScadenzeParziali(Int32 operazione)
    {
        Durata[] scadenze = null;
        Int32 numeroElementi = RadListViewScadenzaParziali.Items.Count;

        if (numeroElementi == 0)
        {
            scadenze = new Durata[1];
        }
        else
        {
            scadenze = new Durata[numeroElementi + operazione];
        }

        Int32 counter = 1;
        foreach (RadListViewDataItem item in RadListViewScadenzaParziali.Items)
        {
            if (counter <= scadenze.Length)
            {
                RadDatePicker rdpScadenza = (RadDatePicker) item.FindControl("RadDatePickerScadenzaParziale");

                if (rdpScadenza.SelectedDate.HasValue)
                {
                    scadenze[counter - 1] = new Durata();

                    RadDatePicker rdpInizio = (RadDatePicker) item.FindControl("RadDatePickerInizioParziale");
                    if (rdpInizio.SelectedDate.HasValue)
                    {
                        scadenze[counter - 1].Inizio = rdpInizio.SelectedDate.Value;
                    }

                    scadenze[counter - 1].Fine = rdpScadenza.SelectedDate.Value;
                }
            }

            counter++;
        }

        return scadenze;
    }

    protected Durata[] GetScadenzePieni(Int32 operazione)
    {
        Durata[] scadenze = null;
        Int32 numeroElementi = RadListViewScadenzaPieni.Items.Count;

        if (numeroElementi == 0)
        {
            scadenze = new Durata[1];
        }
        else
        {
            scadenze = new Durata[numeroElementi + operazione];
        }

        Int32 counter = 1;
        foreach (RadListViewDataItem item in RadListViewScadenzaPieni.Items)
        {
            if (counter <= scadenze.Length)
            {
                RadDatePicker rdpScadenza = (RadDatePicker) item.FindControl("RadDatePickerScadenzaPieno");

                if (rdpScadenza.SelectedDate.HasValue)
                {
                    scadenze[counter - 1] = new Durata();

                    RadDatePicker rdpInizio = (RadDatePicker) item.FindControl("RadDatePickerInizioPieno");
                    if (rdpInizio.SelectedDate.HasValue)
                    {
                        scadenze[counter - 1].Inizio = rdpInizio.SelectedDate.Value;
                    }

                    scadenze[counter - 1].Fine = rdpScadenza.SelectedDate.Value;
                }
            }

            counter++;
        }

        return scadenze;
    }

    #region Custom Validators
    protected void CustomValidatorForza_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadNumericTextBoxForzaAmministrativi.Value.HasValue
            && RadNumericTextBoxForzaTecnici.Value.HasValue
            && RadNumericTextBoxIndeterminatiParziali.Value.HasValue
            && RadNumericTextBoxIndeterminatiPieni.Value.HasValue
            && RadNumericTextBoxDeterminatiParziali.Value.HasValue
            && RadNumericTextBoxDeterminatiPieni.Value.HasValue)
        {
            if (
                (RadNumericTextBoxForzaAmministrativi.Value.Value
                    + RadNumericTextBoxForzaTecnici.Value.Value)
                !=
                (RadNumericTextBoxIndeterminatiParziali.Value.Value
                    + RadNumericTextBoxIndeterminatiPieni.Value.Value
                    + RadNumericTextBoxDeterminatiParziali.Value.Value
                    + RadNumericTextBoxDeterminatiPieni.Value.Value)
                )
            {
                args.IsValid = false;
            }
        }
    }
    #endregion
}