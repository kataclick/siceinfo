﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="QuestionarioImpiegati.aspx.cs" Inherits="AnagraficaImprese_QuestionarioImpiegati" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello Web"
        sottoTitolo="Questionario Impiegati" />
    <br />
    Il questionario è finalizzato alla rilevazione statistica dei dati relativi al personale
    impiegatizio occupato nel corso dell’anno 2012 con contratto di lavoro subordinato
    presso le imprese iscritte.
    <br />
    La rilevazione dei dati in forma aggregata (senza alcun riferimento diretto alla
    singola impresa od al singolo lavoratore) si concluderà il 15 luglio 2013.
    <br />
    <br />
    Una volta completata la compilazione del questionario, premere il pulsante <b>“Salva”</b>
    per confermare i dati imputati.
    <br />
    <br />
    <telerik:RadAjaxPanel ID="RadAjaxPanel" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <b>1. Numero impiegati complessivamente in forza </b>
                    <asp:CustomValidator ID="CustomValidatorForza" runat="server" ValidationGroup="questionario"
                        ErrorMessage="La somma totale degli impiegati indicati al punto 1 deve essere equivalente alla somma degli impiegati indicati ai punti 2 e 3"
                        ForeColor="Red" OnServerValidate="CustomValidatorForza_ServerValidate">
                    *
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Amministrativi<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxForzaAmministrativi"
                        Width="150px" MinValue="0" MaxValue="1000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorForzaAmministrativi" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di impiegati amministrativi complessivamente in forza"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxForzaAmministrativi">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Tecnici<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxForzaTecnici"
                        Width="150px" MinValue="0" MaxValue="1000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorForzaTecnici" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di impiegati tecnici complessivamente in forza"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxForzaTecnici">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>2. Numero impiegati con contratto a tempo indeterminato </b>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Con orario parziale<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxIndeterminatiParziali"
                        Width="150px" MinValue="0" MaxValue="1000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndeterminatiParziali" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di impiegati con contratto a tempo indeterminato con orario parziale"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxIndeterminatiParziali">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Con orario pieno<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxIndeterminatiPieni"
                        Width="150px" MinValue="0" MaxValue="1000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndeterminatiPieni" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di impiegati con contratto a tempo indeterminato con orario pieno"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxIndeterminatiPieni">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>3. Numero impiegati con contratto a tempo determinato </b>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Con orario parziale<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxDeterminatiParziali"
                        Width="150px" MinValue="0" MaxValue="1000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0" AutoPostBack="True" OnTextChanged="RadNumericTextBoxDeterminatiParziali_TextChanged">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeterminatiParziali" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di impiegati con contratto a tempo determinato con orario parziale"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxDeterminatiParziali">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 225px">
                    <small>Inserire la data di scadenza per i contratti 2012 </small>
                </td>
                <td valign="top">
                    <telerik:RadListView ID="RadListViewScadenzaParziali" runat="server" OnItemDataBound="RadListViewScadenzaParziali_ItemDataBound"
                        Width="100%">
                        <EmptyDataTemplate>
                            <small>---</small>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelNumero" runat="server" Text="#0">
                            </asp:Label>
                            Dal
                            <telerik:RadDatePicker ID="RadDatePickerInizioParziale" runat="server">
                            </telerik:RadDatePicker>
                            <asp:CompareValidator ID="CompareValidatorDeterminatiParzialiMinore" runat="server"
                                ValidationGroup="questionario" ErrorMessage="La data di inizio contratto non può essere successiva alla scadenza"
                                ForeColor="Red" ControlToValidate="RadDatePickerInizioParziale" Type="Date" Operator="LessThanEqual"
                                ControlToCompare="RadDatePickerScadenzaParziale">
                            *
                            </asp:CompareValidator>
                            Al<b>*</b>
                            <telerik:RadDatePicker ID="RadDatePickerScadenzaParziale" runat="server" MinDate="1/1/2012">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorScadenzaParziale" runat="server"
                                ValidationGroup="questionario" ErrorMessage="Indicare la data di scadenza" ForeColor="Red"
                                ControlToValidate="RadDatePickerScadenzaParziale">
                            *
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidatorDeterminatiParziali" runat="server" ValidationGroup="questionario"
                                ErrorMessage="La data di scadenza non può essere precedente alla giornata odierna"
                                ForeColor="Red" ControlToValidate="RadDatePickerScadenzaParziale" Type="Date"
                                Operator="GreaterThanEqual" ValueToCompare="1/1/2012">
                            *
                            </asp:CompareValidator>
                            <asp:Button
                                ID="ButtonDeterminatiParzialiAggiungiScadenza"
                                runat="server"
                                Text="+"
                                Font-Bold="true"
                                Width="30px"
                                OnClick="ButtonDeterminatiParzialiAggiungiScadenza_Click" />
                            <asp:Button
                                ID="ButtonDeterminatiParzialiRimuoviScadenza"
                                runat="server"
                                Text="-"
                                Font-Bold="true"
                                Width="30px"
                                OnClick="ButtonDeterminatiParzialiRimuoviScadenza_Click" />
                            <br />
                        </ItemTemplate>
                    </telerik:RadListView>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Con orario pieno<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxDeterminatiPieni"
                        Width="150px" MinValue="0" MaxValue="1000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0" AutoPostBack="True" OnTextChanged="RadNumericTextBoxDeterminatiPieni_TextChanged">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeterminatiPieni" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di impiegati con contratto a tempo determinato con orario pieno"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxDeterminatiPieni">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td valign="top" style="width: 225px">
                    <small>Inserire la data di scadenza per i contratti 2012</small>
                </td>
                <td valign="top">
                    <telerik:RadListView ID="RadListViewScadenzaPieni" runat="server" OnItemDataBound="RadListViewScadenzaPieni_ItemDataBound">
                        <EmptyDataTemplate>
                            <small>---</small>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <asp:Label ID="LabelNumero" runat="server" Text="#0">
                            </asp:Label>
                            Dal
                            <telerik:RadDatePicker ID="RadDatePickerInizioPieno" runat="server">
                            </telerik:RadDatePicker>
                            <asp:CompareValidator ID="CompareValidatorDeterminatiPienoMinore" runat="server"
                                ValidationGroup="questionario" ErrorMessage="La data di inizio contratto non può essere successiva alla scadenza"
                                ForeColor="Red" ControlToValidate="RadDatePickerInizioPieno" Type="Date" Operator="LessThanEqual"
                                ControlToCompare="RadDatePickerScadenzaPieno">
                            *
                            </asp:CompareValidator>
                            Al<b>*</b>
                            <telerik:RadDatePicker ID="RadDatePickerScadenzaPieno" runat="server" MinDate="1/1/2012">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorScadenzaPieno" runat="server"
                                ValidationGroup="questionario" ErrorMessage="Indicare la data di scadenza" ForeColor="Red"
                                ControlToValidate="RadDatePickerScadenzaPieno">
                            *
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidatorDeterminatiPieno" runat="server" ValidationGroup="questionario"
                                ErrorMessage="La data di scadenza non può essere precedente alla giornata odierna"
                                ForeColor="Red" ControlToValidate="RadDatePickerScadenzaPieno" Type="Date" Operator="GreaterThanEqual"
                                ValueToCompare="1/1/2012">
                            *
                            </asp:CompareValidator>
                            <asp:Button
                                ID="ButtonDeterminatiPienoAggiungiScadenza"
                                runat="server"
                                Text="+"
                                Font-Bold="true"
                                Width="30px"
                                OnClick="ButtonDeterminatiPienoAggiungiScadenza_Click" />
                            <asp:Button
                                ID="ButtonDeterminatiPienoRimuoviScadenza"
                                runat="server"
                                Text="-"
                                Font-Bold="true"
                                Width="30px"
                                OnClick="ButtonDeterminatiPienoRimuoviScadenza_Click" />
                            <br />
                        </ItemTemplate>
                    </telerik:RadListView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>4. Numero ore di assenza complessivamente effettuate dagli impiegati </b>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Per malattia<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxAssenzeMalattie"
                        Width="150px" MinValue="0" MaxValue="10000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAssenzeMalattie" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di ore di assenza complessivamente effettuate dagli impiegati per malattia"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxAssenzeMalattie">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 225px">
                    Per cassa integrazione guadagni<b>*</b>:
                </td>
                <td>
                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                        IncrementSettings-InterceptMouseWheel="true" runat="server" ID="RadNumericTextBoxAssenzeCassa"
                        Width="150px" MinValue="0" MaxValue="10000" Type="Number" NumberFormat-DecimalDigits="0"
                        Value="0">
                    </telerik:RadNumericTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAssenzeCassa" runat="server"
                        ValidationGroup="questionario" ErrorMessage="Indicare il numero di ore di assenza complessivamente effettuate dagli impiegati per cassa integrazione guadagni"
                        ForeColor="Red" ControlToValidate="RadNumericTextBoxAssenzeCassa">
                    *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    I dati contrassegnati da <b>*</b> sono obbligatori
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <table>
        <tr>
            <td valign="top" style="width: 180px">
                <asp:Button ID="RadButtonSalva" runat="server" Text="Salva" Width="150px"
                    ValidationGroup="questionario" OnClick="RadButtonSalva_Click">
                </asp:Button>
            </td>
            <td valign="top">
                <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red">
                </asp:Label>
                <asp:ValidationSummary ID="ValidationSummaryQuestionario" runat="server" ValidationGroup="questionario"
                    CssClass="messaggiErrore" />
            </td>
        </tr>
    </table>
</asp:Content>
