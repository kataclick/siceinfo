<%@ Application Language="C#" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type.Entities"%>
<%@ Import Namespace="TBridge.Cemi.TuteScarpe.Business"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Microsoft.Practices.EnterpriseLibrary.Data"%>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>

<script RunAt="server">
        private void Application_Start(Object sender, EventArgs e)
        {
            // Code that runs on application startup
        }

        private void Application_End(Object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        private void Application_Error(Object sender, EventArgs e)
        {
            //  Code that runs on application errors
        }

        private void Session_Start(Object sender, EventArgs e)
        {
            // Code that runs when a new session is started
        }

        private void Session_End(Object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
            
            try
            {
                //Cancelliamo la tabella temporanea per la creazione degli ordini

                TSBusiness tsBiz = new TSBusiness();
                Utente utente = Membership.GetUser() as Utente;
                if (utente != null)
                {
                    tsBiz.PulisciOrdineTemporaneo(utente.IdUtente);
                }
            }
            catch (Exception ex)
            {
            }
        }
</script>

