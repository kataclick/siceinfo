﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using System.Configuration;

public partial class AnagraficaLavoratori_Estrazioni : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Context.Items["tipoEstrazione"] != null && Context.Items["idLavoratore"] != null)
        {
            ReportViewerEstrazione.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerEstrazione.ServerReport.ReportPath = String.Format("/ReportLavoratoriCE/{0}", Context.Items["tipoEstrazione"]);

            ReportParameter[] listaParam = new ReportParameter[2];
            listaParam[0] = new ReportParameter("idLavoratore", Context.Items["idLavoratore"].ToString());
            listaParam[1] = new ReportParameter("drilled", "true");

            ReportViewerEstrazione.ServerReport.SetParameters(listaParam);
            

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            byte[] bytes = null;

            //PDF
            bytes = ReportViewerEstrazione.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", String.Format("attachment;filename={0}_{1}.pdf",Context.Items["tipoEstrazione"],Context.Items["idLavoratore"]));
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }
}