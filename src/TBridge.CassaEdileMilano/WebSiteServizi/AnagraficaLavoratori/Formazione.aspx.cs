﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;

public partial class AnagraficaLavoratori_Formazione : System.Web.UI.Page
{
    //public Int32? IdLavoratore 
    //{
    //    get
    //    {
    //        Int32? idLavoratore = null;
    //        if (ViewState["idLavoratore"] != null)
    //        {
    //            idLavoratore = (Int32)ViewState["idLavoratore"];
    //        }

    //        return idLavoratore;
    //    }

    //    set
    //    {
    //        ViewState["idLavoratore"] = value;
    //    }
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { 
            UtenteLavoratore utLav = (UtenteLavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            //IdLavoratore = utLav.IdLavoratore;
            FormazioneRicerca1.IdLavoratore = utLav.IdLavoratore;
            FormazioneRicerca1.Ricerca();
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/AnagraficaLavoratori/Scheda.aspx");
    }
}