﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AnagraficaLavoratori_Scheda : System.Web.UI.Page
{
    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    protected void Page_Load(object sender, EventArgs e)
    {

        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoLavoratore);

        #endregion

        if (!Page.IsPostBack)
        {
            UtenteLavoratore utLav = (UtenteLavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            Lavoratore lavoratore = biz.GetLavoratore(utLav.IdLavoratore);

            LabelIdLavoratore.Text = lavoratore.Id.ToString();
            DatiAnagrafici1.CaricaLavoratore(lavoratore);
            Recapiti1.CaricaLavoratore(lavoratore);
            SituazioneCEMI1.CaricaDati(lavoratore.Id);
            Prestazioni1.CaricaDati(lavoratore.Id);
            Formazione1.CaricaDati(lavoratore.Id);
        }
    }
}