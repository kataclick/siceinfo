﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Business;
using TBridge.Cemi.Geocode.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;


public partial class AnagraficaLavoratori_WebControls_RecapitiDettagli : System.Web.UI.UserControl
{
    private Int32? IdLavoratore
    {
        get
        {
            Int32? idLavoratore = null;
            if (ViewState["idLavoratore"] != null)
            {
                idLavoratore = (Int32)ViewState["idLavoratore"];
            }

            return idLavoratore;
        }

        set
        {
            ViewState["idLavoratore"] = value;
        }
    }

    private const Int32 Indicedati = 0;
    private const Int32 Indicemodifica = 1;
    private const Int32 Indicemodificaincorso = 2;

    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Per prevenire click multipli

        StringBuilder sb2 = new StringBuilder();
        sb2.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb2.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb2.Append(
            "if (Page_ClientValidate('" + ButtonSalvaRecapiti.ValidationGroup +
            "') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb2.Append("this.value = 'Attendere...';");
        sb2.Append("this.disabled = true;");
        sb2.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalvaRecapiti, null));
        sb2.Append(";");
        sb2.Append("return true;");
        ButtonSalvaRecapiti.Attributes.Add("onclick", sb2.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            ButtonSalvaRecapiti);



        #endregion

    }

    public void CaricaRecapiti(Int32 idLavoratore)
    {
        Lavoratore lavoratore = biz.GetLavoratore(idLavoratore);

        IdLavoratore = idLavoratore;

        RadTextBoxIndirizzo.Text = string.Format("{0}\n{1} {2} ({3})", lavoratore.indirizzoDenominazione, lavoratore.indirizzoCAP, lavoratore.indirizzoComune, lavoratore.indirizzoProvincia);
        RadTextBoxPresso.Text = lavoratore.presso;
        RadTextBoxTelefono.Text = lavoratore.telefono;
        RadTextBoxEmail.Text = lavoratore.eMail;
        RadTextBoxCellulare.Text = lavoratore.cellulare;
        RadButtonSms.Checked = lavoratore.ServizioSMS;
                

        Indirizzo indirizzo = new Indirizzo();
        indirizzo.NomeVia = String.Format("{0} {1}", lavoratore.TipoVia!=null?lavoratore.TipoVia.Descrizione:String.Empty, lavoratore.denominazioneIndirizzo).Trim();
        indirizzo.Civico = lavoratore.civicoIndirizzo;
        indirizzo.Cap = lavoratore.indirizzoCAP;
        indirizzo.Provincia = lavoratore.indirizzoProvincia;
        indirizzo.Comune = lavoratore.indirizzoComune;

        SelezioneIndirizzo1.PreCaricaIndirizzo(indirizzo);

        RadTextBoxPressoModifica.Text = lavoratore.presso;
        RadTextBoxTelefonoModifica.Text = lavoratore.telefono;
        RadTextBoxEmailModifica.Text = lavoratore.eMail;
        RadTextBoxCellulareModifica.Text = lavoratore.cellulare;
        RadButtonSmsModifica.Checked = lavoratore.ServizioSMS;

        VerificaRichiestePendenti(idLavoratore);
    }

    private void VerificaRichiestePendenti(Int32 idLavoratore)
    {
        List<LavoratoreRichiestaVariazioneRecapito> richiesteRecapito = biz.GetRichiesteVariazioniRecapitiPendenti(idLavoratore);
        if (richiesteRecapito.Count > 0)
        {
            LabelRichiestaVariazione.Visible = true;

        }
    }

    private LavoratoreRichiestaVariazioneRecapito CreaRichiestaIndirizzo(Indirizzo indirizzo)
    {
        LavoratoreRichiestaVariazioneRecapito lavoratoreRichiestaVariazioneRecapiti =
            new LavoratoreRichiestaVariazioneRecapito();

        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito = new LavoratoreRecapito();
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaCap = indirizzo != null ? indirizzo.Cap : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaCivico = indirizzo != null ? indirizzo.Civico : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaComuneCodiceCatastale = indirizzo != null
                                                                                       ? indirizzo.
                                                                                             ComuneCodiceCatastale
                                                                                       : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaComune = indirizzo != null ? indirizzo.Comune : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaProvincia = indirizzo != null
                                                                           ? indirizzo.Provincia
                                                                           : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaVia = indirizzo != null ? indirizzo.Via : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaLatitudine = indirizzo != null
                                                                            ? indirizzo.Latitudine
                                                                            : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaLongitudine = indirizzo != null
                                                                             ? indirizzo.Longitudine
                                                                             : null;

        lavoratoreRichiestaVariazioneRecapiti.IdLavoratore = IdLavoratore.Value;
        lavoratoreRichiestaVariazioneRecapiti.IdUtente = GestioneUtentiBiz.GetIdUtente();
        lavoratoreRichiestaVariazioneRecapiti.DataRichiesta = DateTime.Now;
        lavoratoreRichiestaVariazioneRecapiti.Gestito = false; //assegnazione superflua, solo per "logica"

        return lavoratoreRichiestaVariazioneRecapiti;
    }



    
    protected void ButtonSalvaRecapiti_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            RadMultiPageRecapiti.SelectedIndex = Indicemodificaincorso;

            Indirizzo indirizzo = SelezioneIndirizzo1.GetIndirizzo();

            LavoratoreRichiestaVariazioneRecapito lavoratoreRichiestaVariazioneRecapiti;



            if (CheckDatiVariati(out lavoratoreRichiestaVariazioneRecapiti))
            {
                biz.SaveRichiestaVariazioneRecapiti(lavoratoreRichiestaVariazioneRecapiti);
                AbilitaRecapiti(true);
            }
            else
            {
                if (CheckIscrizioneSmsVariata())
                {
                    lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito = null;
                    lavoratoreRichiestaVariazioneRecapiti.Gestito = true;
                    lavoratoreRichiestaVariazioneRecapiti.DataGestione = DateTime.Now;
                    biz.SaveRichiestaVariazioneRecapiti(lavoratoreRichiestaVariazioneRecapiti);
                    AbilitaRecapiti(true);
                }
            }
            
            CheckIscrizioneSms(RadButtonSms.Checked, RadTextBoxCellulare.Text, lavoratoreRichiestaVariazioneRecapiti);

        }
    }

    private bool CheckIscrizioneSmsVariata()
    { 
        return (RadButtonSms.Checked != RadButtonSmsModifica.Checked);
    }
        
    private Boolean CheckDatiVariati(out LavoratoreRichiestaVariazioneRecapito lavoratoreRichiestaVariazioneRecapiti)
    {
        Indirizzo indirizzo = SelezioneIndirizzo1.GetIndirizzo();

        lavoratoreRichiestaVariazioneRecapiti = CreaRichiestaIndirizzo(indirizzo);

        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Presso = Presenter.NormalizzaCampoTesto(RadTextBoxPressoModifica.Text);

        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Email =
            !string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(RadTextBoxEmailModifica.Text))
                ? Presenter.NormalizzaCampoTesto(RadTextBoxEmailModifica.Text)
                : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Telefono =
            !string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(RadTextBoxTelefonoModifica.Text))
                ? Presenter.NormalizzaCampoTesto(RadTextBoxTelefonoModifica.Text)
                : null;
        lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Cellulare =
            !string.IsNullOrEmpty(TBridge.Cemi.Business.Common.PulisciNumeroCellulare(RadTextBoxCellulareModifica.Text))
                ? TBridge.Cemi.Business.Common.PulisciNumeroCellulare(RadTextBoxCellulareModifica.Text)
                : null;

        if (string.Format("{0}\n{1} {2} ({3})",
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaVia,
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaCap,
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaComune,
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.ResidenzaProvincia) == RadTextBoxIndirizzo.Text &&
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Presso == RadTextBoxPresso.Text &&
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Telefono == RadTextBoxTelefono.Text &&
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Cellulare == RadTextBoxCellulare.Text &&
            lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Email == RadTextBoxEmail.Text
            )
        { 
            return false; 
        }
        return true;
    }

    private void CheckIscrizioneSms(bool statoIscrizioneSms, String cellulareIscrizioneSms, LavoratoreRichiestaVariazioneRecapito lavoratoreRichiestaVariazioneRecapiti)
    {
        LavoratoriManager managerSms = new LavoratoriManager();
        
        if (statoIscrizioneSms)
        {
            //Il lavoratore era iscritto al servizio SMS
            if (lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.IscrizioneSms)
            { 
                //Il lavoratore rimane iscritto. Verificare numero
                if (cellulareIscrizioneSms != lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Cellulare)
                {
                    Int32? stessoRecapito;
                    managerSms.SaveRecapitoSmsLavoratore(lavoratoreRichiestaVariazioneRecapiti.IdLavoratore, lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Cellulare, out stessoRecapito);

                    if (stessoRecapito > 0)
                        biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta, "Numero cellulare già utilizzato da altro lavoratore");
                    else
                        biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta, null);
                }
                else
                {
                    biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta, null);
                }
            }
            else
            {
                //Il lavoratore vuole disiscriversi.
                managerSms.DisabilitaRecapito(lavoratoreRichiestaVariazioneRecapiti.IdUtente);
                biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta, null);
            }

        }
        else
        { 
            //Il lavoratore NON era iscritto al servizio SMS
            if (lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.IscrizioneSms)
            {
                //Il lavoratore vuole iscriversi.
                Int32? stessoRecapito;
                managerSms.SaveRecapitoSmsLavoratore(lavoratoreRichiestaVariazioneRecapiti.IdLavoratore, lavoratoreRichiestaVariazioneRecapiti.LavoratoreRecapito.Cellulare, out stessoRecapito);

                if (stessoRecapito > 0)
                    biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta, "Numero cellulare già utilizzato da altro lavoratore");
                else
                    biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta, null);
            }
            else
            {
                //Il lavoratore vuole rimanere non iscritto.
                biz.SaveStatoVariazioneIscrizioneSms(lavoratoreRichiestaVariazioneRecapiti.IdRichiesta,null);
            }
        }
    }

    protected void ButtonAnnullaRecapiti_Click(object sender, EventArgs e)
    {
        AbilitaRecapiti(true);
        RadMultiPageRecapiti.SelectedIndex = Indicedati;
    }

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = SelezioneIndirizzo1.IndirizzoConfermato();
    }

    private void AbilitaRecapiti(Boolean abilitato)
    {
        ButtonModificaRecapiti.Enabled = abilitato;
    }
    protected void CustomValidatorCellulare_ServerValidate(object source, ServerValidateEventArgs args)
    {
        String numero = RadTextBoxCellulareModifica.Text;
        
        if (!String.IsNullOrEmpty(numero))
        {
            numero = TBridge.Cemi.Business.Common.PulisciNumeroCellulare(numero);
            args.IsValid = TBridge.Cemi.Business.Common.NumeroCellulareValido(numero);
        }
    }
/*    protected void CustomValidatorCellulareSmsModifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        String numero = RadTextCellulareSmsModifica.Text;

        if (!String.IsNullOrEmpty(numero))
        {
            numero = TBridge.Cemi.Business.Common.PulisciNumeroCellulare(numero);
            args.IsValid = TBridge.Cemi.Business.Common.NumeroCellulareValido(numero);
        }
    }
    protected void ButtonSalvaSms_Click(object sender, EventArgs e)
    {

    }
    protected void ButtonAnnullaSms_Click(object sender, EventArgs e)
    {
        AbilitaSms(true);
        RadMultiPageSms.SelectedIndex = Indicedati;
    }
    protected void ButtonModificaSMS_Click(object sender, EventArgs e)
    {
        AbilitaSms(false);
        RadMultiPageSms.SelectedIndex = Indicemodifica;
    }

    private void AbilitaSms(Boolean abilitato)
    {
        ButtonModificaSms.Enabled = abilitato;
    }

  */


    protected void ButtonModificaRecapiti_Click(object sender, EventArgs e)
    {
        AbilitaRecapiti(false);
        RadMultiPageRecapiti.SelectedIndex = Indicemodifica;
    }
}