﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;

public partial class AnagraficaLavoratori_WebControls_Recapiti : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    public void CaricaLavoratore(Lavoratore lavoratore)
    {
        LabelTelefono.Text = lavoratore.telefono;
        LabelCellulare.Text = lavoratore.cellulare;
        LabelServizioSMS.Text = lavoratore.ServizioSMS ? "Sì" : "No";
        LabelResidenza.Text = string.Format("{0}<br/>{1} {2} ({3})", lavoratore.indirizzoDenominazione, lavoratore.indirizzoCAP, lavoratore.indirizzoComune, lavoratore.indirizzoProvincia);
        LabelPresso.Text = lavoratore.presso;
        LabelDomicilio.Text = string.Empty;
        LabelEmail.Text = lavoratore.eMail;
    }
    protected void ImageButtonRecapitiDettagli_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/AnagraficaLavoratori/Recapiti.aspx");
    }
}