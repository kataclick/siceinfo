﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;

public partial class AnagraficaLavoratori_WebControls_Filters_FormazioneFiltri : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public FormazioneFilter GetFilter()
    {
        FormazioneFilter filtro = new FormazioneFilter();
        
        filtro.Dal = RadDateInputDal.SelectedDate;
        filtro.Al = RadDateInputAl.SelectedDate;        

        return filtro;

    }
}