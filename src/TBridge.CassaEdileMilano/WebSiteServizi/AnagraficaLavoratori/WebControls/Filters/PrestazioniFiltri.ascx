﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniFiltri.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_Filters_PrestazioniFiltri" %>

<style type="text/css">
    .style1
    {
        width: 80px;
    }
    
      .style2
    {
        width: 150px;
    }
</style>

<table class="standardTable">
    <tr>
        <td class="style1"> Dal 
        </td>
        <td><telerik:RadDateInput ID="RadDateInputDal" runat="server" MinDate="01/01/1950" MaxDate="01/01/2050" Width="100px"></telerik:RadDateInput>
        </td>
        <td class="style1"> Al
        </td>
        <td><telerik:RadDateInput ID="RadDateInputAl" runat="server" MinDate="01/01/1950" MaxDate="01/01/2050" Width="100px"></telerik:RadDateInput>
        </td>
        <td class="style2"> Tipo Prestazione
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTipoPrestazione" runat="server" AutoPostBack="false" Text="Selezionare tipo prestazione"></telerik:RadComboBox> 
        </td>
    </tr>
</table>
