﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;

public partial class AnagraficaLavoratori_WebControls_Filters_PrestazioniFiltri : System.Web.UI.UserControl
{
    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    public Int32? IdLavoratore
    {
        get
        {
            Int32? idLavoratore = null;
            if (ViewState["idLavoratore"] != null)
            {
                idLavoratore = (Int32)ViewState["idLavoratore"];
            }

            return idLavoratore;
        }

        set
        {
            ViewState["idLavoratore"] = value;
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipiPrestazione();
        }
    }

    private void CaricaTipiPrestazione()
    {
        Presenter.CaricaElementiInDropDown(RadComboBoxTipoPrestazione, biz.GetTipiPrestazione(IdLavoratore.Value), "Descrizione", "IdTipoPrestazione");
    }

    public PrestazioneFilter GetFilter()
    {
        PrestazioneFilter filtro = new PrestazioneFilter();

        filtro.Dal = RadDateInputDal.SelectedDate;
        filtro.Al = RadDateInputAl.SelectedDate;
        filtro.IdTipoPrestazione = RadComboBoxTipoPrestazione.SelectedValue;

        return filtro;

    }
}