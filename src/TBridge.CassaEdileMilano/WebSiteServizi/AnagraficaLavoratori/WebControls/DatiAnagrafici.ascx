﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiAnagrafici.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_DatiAnagrafici" %>
<style type="text/css">
    .style1
    {
        width: 150px;
    }
</style>
<div style="float: left">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Dati anagrafici">
                </asp:Label>
            </td>
        </tr>
    </table>
</div>
<div style="float: right">
    <asp:ImageButton ID="ImageButtonDatiAnagraficiDettagli" runat="server" ToolTip="Dettagli" ImageUrl="~/images/lente.png" OnClick="ImageButtonDatiAnagaraficiDettagli_Click" />
</div>
<table class="borderedTable">
        <tr>
            <td class="style1">
                Cognome:
            </td>
            <td>
                <b><asp:Label
                    ID="LabelCognome"
                    runat="server">
                </asp:Label></b>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Nome:
            </td>
            <td>
                <b><asp:Label
                    ID="LabelNome"
                    runat="server">
                </asp:Label></b>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Codice Fiscale:
            </td>
            <td>
                <asp:Label
                    ID="LabelCodiceFiscale"
                    runat="server">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Data di Nascita:
            </td>
            <td>
                <asp:Label
                    ID="LabelDataNascita"
                    runat="server">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Sesso:
            </td>
            <td>
                <asp:Label
                    ID="LabelSesso"
                    runat="server">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Luogo di Nascita:
            </td>
            <td>
                <asp:Label
                    ID="LabelLuogoNascita"
                    runat="server">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style1">
                IBAN:
            </td>
            <td>
                <asp:Label
                    ID="LabelIBAN"
                    runat="server">
                </asp:Label>
            </td>
        </tr>
    </table>