﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class AnagraficaLavoratori_WebControls_DatiAnagraficiDettagli : System.Web.UI.UserControl
{
    private Int32? IdLavoratore
    {
        get
        {
            Int32? idLavoratore = null;
            if (ViewState["idLavoratore"] != null)
            {
                idLavoratore = (Int32)ViewState["idLavoratore"];
            }

            return idLavoratore;
        }

        set
        {
            ViewState["idLavoratore"] = value;
        }
    }

    private const Int32 Indicedati = 0;
    private const Int32 Indicemodifica = 1;
    private const Int32 Indicemodificaincorso = 2;

    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    Common commonBiz = new Common();
    protected void Page_Load(object sender, EventArgs e)
    {
      /*  if (!Page.IsPostBack)
        {
            CaricaProvince();
            
        }
       */
    }

    public void CaricaAnagrafica(Int32 idLavoratore)
    {
        Lavoratore lavoratore = biz.GetLavoratore(idLavoratore);

        IdLavoratore = idLavoratore;

        RadTextBoxCognome.Text = lavoratore.Cognome;
        RadTextBoxNome.Text = lavoratore.Nome;
        RadTextBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;
        RadTextBoxDataNascita.Text = lavoratore.DataNascita.HasValue ? lavoratore.DataNascita.Value.ToString("dd/MM/yyyy") : null;
        RadButtonSessoM.Checked = lavoratore.sesso == "M";
        RadButtonSessoF.Checked = lavoratore.sesso == "F";
        RadTextLuogoNascita.Text = lavoratore.luogoNascita;

        RadTextBoxCognomeModifica.Text = lavoratore.Cognome;
        RadTextBoxNomeModifica.Text = lavoratore.Nome;
        RadTextBoxCodiceFiscaleModifica.Text = lavoratore.CodiceFiscale;
        RadTextBoxDataNascitaModifica.DateFormat = "dd/MM/yyyy";
        RadTextBoxDataNascitaModifica.DbSelectedDate = lavoratore.DataNascita.HasValue ? lavoratore.DataNascita.Value.ToString("dd/MM/yyyy") : null;
        RadButtonSessoModificaM.Checked = lavoratore.sesso == "M";
        RadButtonSessoModificaF.Checked = lavoratore.sesso == "F";

        ComuniSiceNew cm = biz.GetComune(lavoratore.luogoNascita,lavoratore.CodiceFiscale);

        CaricaProvince();
        RadComboBoxProvinciaNascitaModifica.SelectedValue = cm.Provincia;
        Presenter.CaricaComuniCodiceCatastale(RadComboBoxLuogoNascitaModifica, cm.Provincia);
        RadComboBoxLuogoNascitaModifica.SelectedValue = cm.CodiceCatastale;
        //RadComboBoxLuogoNascitaModifica.DataBind();
        
        VerificaRichiestePendenti(idLavoratore);
    }
    

    private void CaricaProvince()
    {
        Presenter.CaricaProvince(RadComboBoxProvinciaNascitaModifica);
        //Presenter.CaricaComuni(RadComboBoxLuogoNascitaModifica, RadComboBoxProvinciaNascitaModifica.SelectedValue);
    }

    private void AbilitaAnagrafica(Boolean abilitato)
    {
        ButtonModificaAnagrafica.Enabled = abilitato;
    }


    private LavoratoreRichiestaVariazioneAnagrafica CreaRichiestaVariazioneAnagrafica()
    {
        LavoratoreRichiestaVariazioneAnagrafica lavoratoreRichiestaVariazioneAnagrafica =
            new LavoratoreRichiestaVariazioneAnagrafica();

        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica = new LavoratoreVariazioneAnagrafica();
        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognomeModifica.Text);
        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNomeModifica.Text);
        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscaleModifica.Text);
        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica.Sesso = RadButtonSessoModificaM.Checked ? "M" : "F";
        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica.DataNascita = RadTextBoxDataNascitaModifica.SelectedDate.Value;
        lavoratoreRichiestaVariazioneAnagrafica.VariazioneAnagrafica.CodiceCatastaleLuogoNascita = RadComboBoxLuogoNascitaModifica.SelectedValue;

        lavoratoreRichiestaVariazioneAnagrafica.IdLavoratore = IdLavoratore.Value;
        lavoratoreRichiestaVariazioneAnagrafica.IdUtente = GestioneUtentiBiz.GetIdUtente();
        lavoratoreRichiestaVariazioneAnagrafica.DataRichiesta = DateTime.Now;
        lavoratoreRichiestaVariazioneAnagrafica.Gestito = false; //assegnazione superflua, solo per "logica"

        return lavoratoreRichiestaVariazioneAnagrafica;
    }

    private void VerificaRichiestePendenti(Int32 idLavoratore)
    {
        List<LavoratoreRichiestaVariazioneAnagrafica> richiesteAnagrafica = biz.GetRichiesteVariazioniAnagrafichePendenti(idLavoratore);
        if (richiesteAnagrafica.Count > 0)
        {
            LabelRichiestaVariazioneAnagrafica.Visible = true;

        }
    }


    #region Validatori
    protected void ValidatorCognomeModifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        String tmp = Presenter.NormalizzaCampoTestoSoloLettere(RadTextBoxCognomeModifica.Text);

        if (String.IsNullOrEmpty(tmp) || tmp.Length < 2)
        {
            args.IsValid = false;
        }

        //if(String.is
    }
    
    protected void ValidatorNomeModifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        String tmp = Presenter.NormalizzaCampoTestoSoloLettere(RadTextBoxNomeModifica.Text);

        if (String.IsNullOrEmpty(tmp) || tmp.Length < 2)
        {
            args.IsValid = false;
        }

    }

    protected void ValidatorCodiceFiscaleModifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        String sesso;
        String nome = Presenter.NormalizzaCampoTestoSoloLettere(RadTextBoxNomeModifica.Text);
        String cognome = Presenter.NormalizzaCampoTestoSoloLettere(RadTextBoxCognomeModifica.Text);
        DateTime? dataNascita = RadTextBoxDataNascitaModifica.SelectedDate;
        String codiceFiscale = Presenter.NormalizzaCampoTestoSoloLettere(RadTextBoxCodiceFiscaleModifica.Text);

        if (RadButtonSessoModificaM.Checked)
        {
            sesso = "M";
        }
        else
        {
            if (RadButtonSessoModificaF.Checked)
            {
                sesso = "F";
            }
            else
            {
                sesso = String.Empty;
            }
        }


        try
        {
            if (!String.IsNullOrEmpty(codiceFiscale)
                && !String.IsNullOrEmpty(nome)
                && !String.IsNullOrEmpty(cognome)
                && !String.IsNullOrEmpty(sesso)
                && dataNascita.HasValue
                && !CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(nome, cognome, sesso, dataNascita.Value, codiceFiscale))
            {
                args.IsValid = false;
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void ValidatorDataNascitaModifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!RadTextBoxDataNascitaModifica.SelectedDate.HasValue)
        {
            args.IsValid = false;
        }
    }

    protected void ValidatorLuogoNascitaModifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (String.IsNullOrEmpty(RadComboBoxLuogoNascitaModifica.SelectedValue))
        {
            args.IsValid = false;
        }
    }
    #endregion




    #region Bottoni
    protected void ButtonModificaAnagrafica_Click(object sender, EventArgs e)
    {
        AbilitaAnagrafica(false);
        RadMultiPageAnagrafica.SelectedIndex = Indicemodifica;
        //RadComboBoxLuogoNascitaModifica.DataBind();
    }

    protected void ButtonSalvaAnagrafica_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            RadMultiPageAnagrafica.SelectedIndex = Indicemodificaincorso;

            LavoratoreRichiestaVariazioneAnagrafica lavoratoreRichiestaVariazioneAnagrafica = CreaRichiestaVariazioneAnagrafica();
            if (biz.SaveRichiestaVariazioneAnagrafica(lavoratoreRichiestaVariazioneAnagrafica))
            {
                AbilitaAnagrafica(true);
            }
            
        }
    }

    protected void ButtonAnnullaAnagrafica_Click(object sender, EventArgs e)
    {
        AbilitaAnagrafica(true);
        RadMultiPageAnagrafica.SelectedIndex = Indicedati;
    }

    #endregion

    protected void RadComboBoxProvinciaNascitaModifica_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        Presenter.CaricaComuniCodiceCatastale(RadComboBoxLuogoNascitaModifica, RadComboBoxProvinciaNascitaModifica.SelectedValue);
    }
}