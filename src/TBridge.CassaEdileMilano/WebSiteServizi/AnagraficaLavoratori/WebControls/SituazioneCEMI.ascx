﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SituazioneCEMI.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_SituazioneCEMI" %>
<style type="text/css">
    .style1
    {
        width: 170px;
    }
</style>
<table class="filledtable">
    <tr>
        <td style="height: 16px">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Situazione CEMI">
            </asp:Label>
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td class="style1">
            
                Rapporto di Lavoro:

        </td>
        <td style="vertical-align:middle">
            <div style="float: left"><asp:Label ID="LabelRapporto" runat="server">
            </asp:Label></div>
                        <div style="float:right"><asp:ImageButton
        ID="ImageButtonEsportaRapporti"
        runat="server"
        ToolTip="Dettagli rapporti di lavoro" ImageUrl="~/images/pdf24.png" onclick="ImageButtonEsportaRapporti_Click"/></div>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Ultime Denunce:
        </td>
        <td>
            <%--<asp:Label
                    ID="LabelDenunce"
                    runat="server">
                </asp:Label>
            --%>
            <telerik:RadGrid ID="RadGridDenunce" runat="server" CellSpacing="0" GridLines="None" OnItemDataBound="RadGridDenunce_ItemDataBound" PageSize="3">
                <ExportSettings>
                    <Pdf PageWidth="" />
                </ExportSettings>
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="Data" DataFormatString="{0:MM/yyyy}" FilterControlAltText="Filter columnData column" HeaderText="Mese" UniqueName="columnData">
                            <ItemStyle Width="70px" Font-Bold="true" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter columnImpresa column" HeaderText="Impresa" UniqueName="columnImpresa">
                            <HeaderTemplate>
                                <div style="float:left">
                                    Impresa
                                </div>
                                <div style="float:right">
                                    <asp:ImageButton
                                        ID="ImageButtonEsportaOre"
                                        runat="server"
                                        ToolTip="Dettagli ore denunciate" ImageUrl="~/images/pdf24.png" onclick="ImageButtonEsportaOre_Click"/>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LabelIdImpresa" runat="server"></asp:Label>
                                -
                                <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle PageSizeControlType="RadComboBox" />
                </MasterTableView>
                <PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Delega attiva:
        </td>
        <td>
            <asp:Label ID="LabelDelega" runat="server">
            </asp:Label>
        </td>
    </tr>
</table>
