﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Prestazioni.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_Prestazioni" %>
<style type="text/css">
    .style1
    {
        width: 170px;
    }
</style>
    <div style="float:left">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Prestazioni">
                </asp:Label>
            </td>
        </tr>
    </table>
    </div>
    <div style="float:right">
    <asp:ImageButton
        ID="ImageButtonDettagli"
        runat="server"
        ToolTip="Dettagli" ImageUrl="~/images/lente.png" onclick="ImageButtonDettagli_Click" />
    </div>
    <table class="borderedTable">
        <tr>
            <td class="style1">
                Ultime prestazioni erogate:
            </td>
            <td>
                <%--<asp:Label
                    ID="LabelDenunce"
                    runat="server">
                </asp:Label>
                --%>
                <telerik:RadGrid
                    ID="RadGridPrestazioni"
                    runat="server" CellSpacing="0" GridLines="None" PageSize="3">
                    <ExportSettings>
                        <Pdf PageWidth="" />
                    </ExportSettings>
                    <MasterTableView>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn FilterControlAltText="Filter column column" HeaderText="Tipo prestazione" UniqueName="columnTipoPrestazione" DataField="TipoPrestazione.Descrizione">
                            <ItemStyle Font-Bold="true" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:dd/MM/yyyy}" FilterControlAltText="Filter column1 column" HeaderText="Data domanda" UniqueName="columnDataDomanda" DataField="DataDomanda">
                            <ItemStyle Font-Bold="true" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:C}" FilterControlAltText="Filter column2 column" HeaderText="Importo erogato netto" UniqueName="columnImportoNetto" DataField="ImportoErogato">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle PageSizeControlType="RadComboBox" />
                    </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>