﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;

public partial class AnagraficaLavoratori_WebControls_DatiAnagrafici : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaLavoratore(Lavoratore lavoratore)
    {
        LabelCognome.Text = lavoratore.Cognome;
        LabelNome.Text = lavoratore.Nome;
        LabelCodiceFiscale.Text = lavoratore.CodiceFiscale;
        LabelDataNascita.Text = lavoratore.DataNascita.HasValue ? lavoratore.DataNascita.Value.ToString("dd/MM/yyyy") : string.Empty;
        LabelLuogoNascita.Text = string.Format("{0} ({1})",lavoratore.luogoNascita,lavoratore.provinciaNascita);
        LabelSesso.Text = lavoratore.sesso;
        LabelIBAN.Text = lavoratore.IBAN;
    }
    protected void ImageButtonDatiAnagaraficiDettagli_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/AnagraficaLavoratori/DatiAnagrafici.aspx");
    }
}