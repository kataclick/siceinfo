﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;

public partial class AnagraficaLavoratori_WebControls_Prestazioni : System.Web.UI.UserControl
{
    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaDati(Int32 idLavoratore)
    {
        List<Prestazione> pre = biz.GetUltimePrestazioni(idLavoratore);
        
        Presenter.CaricaElementiInGridView(
            RadGridPrestazioni,
            pre);
    }
    protected void ImageButtonDettagli_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/AnagraficaLavoratori/Prestazioni.aspx");
    }
}