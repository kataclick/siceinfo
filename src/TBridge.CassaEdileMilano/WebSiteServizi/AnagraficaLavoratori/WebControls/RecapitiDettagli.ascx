﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecapitiDettagli.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_RecapitiDettagli" %>
<%@ Register Src="~/WebControls/SelezioneIndirizzo.ascx" TagName="SelezioneIndirizzo" TagPrefix="uc1" %>
<telerik:RadMultiPage ID="RadMultiPageRecapiti" runat="server" Width="100%" SelectedIndex="0" RenderSelectedPageOnly="true">
    <telerik:RadPageView ID="RadPageViewRecapitiDati" runat="server">
        <div>
            <table class="borderedTable">
                <tr>
                    <td class="anagraficaImpreseTd">
                        <b>Residenza</b>
                        <asp:Label ID="LabelRichiestaVariazione" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="ButtonModificaRecapiti" runat="server" CausesValidation="false" Text="Modifica" onclick="ButtonModificaRecapiti_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Indirizzo
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server" Width="250px" Enabled="False" ReadOnly="True" TextMode="MultiLine" Style="overflow: hidden" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Presso
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxPresso" runat="server" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Telefono
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxTelefono" runat="server" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Cellulare
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxCellulare" runat="server" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Iscrizione servizio SMS
                    </td>
                    <td>
                        <telerik:RadButton ID="RadButtonSms" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Email
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxEmail" runat="server" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewRecapitiModifica" runat="server">
        <div>
            <table class="borderedTableSelected">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    <b>Residenza</b>
                                </td>
                                <td class="anagraficaImpreseTd">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:SelezioneIndirizzo ID="SelezioneIndirizzo1" runat="server" />
                        <asp:CustomValidator ID="CustomValidatorIndirizzo" runat="server" ValidationGroup="controlliRecapiti" OnServerValidate="CustomValidatorIndirizzo_ServerValidate" ErrorMessage="Confermare l'indirizzo">
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="borderedTableSelected">
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Presso
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxPressoModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Telefono
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxTelefonoModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" /><asp:RegularExpressionValidator ID="RegularExpressionValidatorTelefono" runat="server" ControlToValidate="RadTextBoxTelefonoModifica" ErrorMessage="Il telefono inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliRecapiti" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="trCellulare" runat="server">
                                <td class="anagraficaImpreseTd">
                                    Cellulare
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCellulareModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" /><asp:CustomValidator ID="CustomValidatorCellulare" runat="server" ErrorMessage="Il cellulare inserito non è valido" ValidationGroup="controlliRecapiti" CssClass="messaggiErrore" OnServerValidate="CustomValidatorCellulare_ServerValidate">*</asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Iscrizione servizio SMS
                                </td>
                                <td>
                                    <telerik:RadButton ID="RadButtonSmsModifica" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" Width="250px" Enabled="True" ReadOnly="False" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Email*
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxEmailModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" /><asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server" ControlToValidate="RadTextBoxEmailModifica" ErrorMessage="Indirizzo Email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="controlliRecapiti" CssClass="messaggiErrore">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="RequiredFieldValidatorEmailModifica" runat="server" ControlToValidate="RadTextBoxEmailModifica" ErrorMessage="Inserire una email" ValidationGroup="controlliRecapiti">*</asp:RequiredFieldValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                </td>
                                <td>
                                    <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore" ValidationGroup="controlliRecapiti" />
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ButtonSalvaRecapiti" runat="server" Text="Salva" OnClick="ButtonSalvaRecapiti_Click" ValidationGroup="controlliRecapiti" />
                                            </td>
                                            <td>
                                                <asp:Button ID="ButtonAnnullaRecapiti" runat="server" Text="Annulla" OnClick="ButtonAnnullaRecapiti_Click" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewRecapitiModificaInCorso" runat="server">
        <div>
            <table class="borderedTable">
                <tr>
                    <td>
                        <b>Recapiti</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        La modifica eseguita è stata acquisita correttamente. E’ in corso l’aggiornamento dei dati che saranno resi disponibili a breve.
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
</telerik:RadMultiPage>
<%--<telerik:RadMultiPage ID="RadMultiPageSms" runat="server" Width="100%" SelectedIndex="0" RenderSelectedPageOnly="true">
    <telerik:RadPageView ID="RadPageViewSms" runat="server">
        <div>
            <table class="borderedTable">
                <tr>
                    <td class="anagraficaImpreseTd">
                        <b>Iscrizione Servizio SMS</b>
                        <asp:Label ID="LabelModificaSms" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="ButtonModificaSms" runat="server" CausesValidation="false" Text="Modifica" OnClick="ButtonModificaSMS_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Iscrizione servizio SMS
                    </td>
                    <td>
                        <telerik:RadButton ID="RadButtonSmsModifica" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" Width="250px" Enabled="True" ReadOnly="False" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Cellulare
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBox4" runat="server" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewSmsModifica" runat="server">
        <div>
            <table class="borderedTableSelected">
                <tr>
                    <td class="anagraficaImpreseTd">
                        Iscrizione servizio SMS
                    </td>
                    <td>
                        <telerik:RadButton ID="RadButtonIscrizioneSmsModifica" runat="server" ButtonType="ToggleButton" ToggleType="CheckBox" Width="250px" Enabled="True" ReadOnly="False" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Cellulare
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextCellulareSmsModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" /><asp:CustomValidator ID="CustomValidatorCellulareSmsModifica" runat="server" ErrorMessage="Il cellulare inserito non è valido" ValidationGroup="controlliSms" CssClass="messaggiErrore" OnServerValidate="CustomValidatorCellulareSmsModifica_ServerValidate">*</asp:CustomValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                    </td>
                    <td>
                        <asp:ValidationSummary ID="ValidationSummarySms" runat="server" CssClass="messaggiErrore" ValidationGroup="controlliSms" />
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="ButtonSalvaSms" runat="server" Text="Salva" OnClick="ButtonSalvaSms_Click" ValidationGroup="controlliSms" />
                                </td>
                                <td>
                                    <asp:Button ID="ButtonAnnullaSms" runat="server" Text="Annulla" OnClick="ButtonAnnullaSms_Click" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageView3" runat="server">
        <div>
            <table class="borderedTable">
                <tr>
                    <td>
                        <b>Iscrizione Servizio SMS</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        La modifica eseguita è stata acquisita correttamente.
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
</telerik:RadMultiPage>--%>