﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiAnagraficiDettagli.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_DatiAnagraficiDettagli" %>
<telerik:RadMultiPage ID="RadMultiPageAnagrafica" runat="server" Width="100%" SelectedIndex="0" RenderSelectedPageOnly="true">
    <telerik:RadPageView ID="RadPageViewAnagraficaDati" runat="server">
        <div>
            <table class="borderedTable">
                <tr>
                    <td class="anagraficaImpreseTd">
                        <b>Dati anagrafici</b>
                        <asp:Label ID="LabelRichiestaVariazioneAnagrafica" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="ButtonModificaAnagrafica" runat="server" CausesValidation="false" Text="Modifica" OnClick="ButtonModificaAnagrafica_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Cognome
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" Width="250px" Enabled="False" ReadOnly="True" MaxLength="30"/>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Nome
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxNome" runat="server" Width="250px" Enabled="False" ReadOnly="True" MaxLength="30" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Codice Fiscale
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" Width="250px" Enabled="False" ReadOnly="True" MaxLength="16" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Data di nascita
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataNascita" runat="server" Width="250px" Enabled="False" ReadOnly="True" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Sesso
                    </td>
                    <td>
                        <telerik:RadButton ID="RadButtonSessoM" ToggleType="Radio" ButtonType="ToggleButton" GroupName="sesso" runat="server" Enabled="False" ReadOnly="True" Text="M" />
                        <telerik:RadButton ID="RadButtonSessoF" ToggleType="Radio" ButtonType="ToggleButton" GroupName="sesso" runat="server" Enabled="False" ReadOnly="True" Text="F" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="anagraficaImpreseTd">
                        Luogo di nascita
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextLuogoNascita" runat="server" Width="250px" Enabled="False" ReadOnly="True"  MaxLength="35" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewRecapitiModifica" runat="server">
        <div>
            <table class="borderedTableSelected">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    <b>Dati Anagrafici</b>
                                </td>
                                <td class="anagraficaImpreseTd">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="borderedTableSelected">
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Cognome*
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCognomeModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" MaxLength="30"/>
                                    <asp:CustomValidator ID="ValidatorCognomeModifica" runat="server" ValidationGroup="AnagraficaValidationGroup" ErrorMessage="Cognome mancante" CssClass="messaggiErrore" onservervalidate="ValidatorCognomeModifica_ServerValidate">*</asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Nome*
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxNomeModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" MaxLength="30"/>
                                    <asp:CustomValidator ID="ValidatorNomeModifica" runat="server" ValidationGroup="AnagraficaValidationGroup" ErrorMessage="Nome mancante" CssClass="messaggiErrore" onservervalidate="ValidatorNomeModifica_ServerValidate">*</asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Codice Fiscale*
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCodiceFiscaleModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" MaxLength="16" />
                                    <asp:CustomValidator ID="ValidatorCodiceFiscaleModifica" runat="server" ValidationGroup="AnagraficaValidationGroup" ErrorMessage="CodiceFiscale mancante" CssClass="messaggiErrore" onservervalidate="ValidatorCodiceFiscaleModifica_ServerValidate">*</asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Data di nascita (gg/mm/aaaa)*
                                </td>
                                <td>
                                    <telerik:RadDateInput ID="RadTextBoxDataNascitaModifica" runat="server" Width="250px" MinDate="1940-01-01" Culture="it-IT" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" LabelWidth="100px" MaxDate="2040-12-31" />
                                    <asp:CustomValidator ID="ValidatorDataNascitaModifica" runat="server" ValidationGroup="AnagraficaValidationGroup" ErrorMessage="Data di nascita mancante" CssClass="messaggiErrore" onservervalidate="ValidatorDataNascitaModifica_ServerValidate">*</asp:CustomValidator>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Sesso*
                                </td>
                                <td>
                                    <telerik:RadButton ID="RadButtonSessoModificaM" ButtonType="ToggleButton" ToggleType="Radio" Text="M" runat="server" GroupName="SessoModifica" Checked="true" />
                                    <telerik:RadButton ID="RadButtonSessoModificaF" ButtonType="ToggleButton" ToggleType="Radio" Text="F" runat="server" GroupName="SessoModifica" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="TrPaeseNascitaModifica" runat="server">
                                <td class="anagraficaImpreseTd">
                                    Paese di nascita*
                                </td>
                                <td>
                                    <telerik:RadButton ID="RadButtonPaeseNascitaModificaItalia" ButtonType="ToggleButton" ToggleType="Radio" Text="Italia" runat="server" GroupName="PaeseModifica" Checked="true" />
                                    <telerik:RadButton ID="RadButtonPaeseNascitaModificaEstero" ButtonType="ToggleButton" ToggleType="Radio" Text="Estero" runat="server" GroupName="PaeseModifica" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="TrProvinciaNascitaModifica" runat="server">
                                <td class="anagraficaImpreseTd">
                                    Provincia di nascita*
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxProvinciaNascitaModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False" EmptyMessage="Selezionare la provincia" AutoPostBack="True" onselectedindexchanged="RadComboBoxProvinciaNascitaModifica_SelectedIndexChanged"/>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="TrLuogoNascitaModifica" runat="server">
                                <td class="anagraficaImpreseTd">
                                    Luogo di nascita*
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxLuogoNascitaModifica" runat="server" Width="250px" Enabled="True" ReadOnly="False"  EmptyMessage="Selezionare il luogo" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                </td>
                                <td>
                                    <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore" ValidationGroup="AnagraficaValidationGroup" />
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ButtonSalvaAnagrafica" runat="server" Text="Salva" OnClick="ButtonSalvaAnagrafica_Click" ValidationGroup="controlliAnagrafica" />
                                            </td>
                                            <td>
                                                <asp:Button ID="ButtonAnnullaAnagrafica" runat="server" Text="Annulla" OnClick="ButtonAnnullaAnagrafica_Click" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewAnagraficaModificaInCorso" runat="server">
        <div>
            <table class="borderedTable">
                <tr>
                    <td>
                        <b>Recapiti</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        La modifica eseguita è stata acquisita correttamente. E’ in corso l’aggiornamento dei dati che saranno resi disponibili a breve.
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadPageView>
</telerik:RadMultiPage>