﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;

public partial class AnagraficaLavoratori_WebControls_SituazioneCEMI : System.Web.UI.UserControl
{
    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    private Int32? IdLavoratore
    {
        get
        {
            Int32? idLavoratore = null;
            if (ViewState["idLavoratore"] != null)
            {
                idLavoratore = (Int32)ViewState["idLavoratore"];
            }

            return idLavoratore;
        }

        set
        {
            ViewState["idLavoratore"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    
    public void CaricaDati(Int32 idLavoratore)
    {
        IdLavoratore = idLavoratore;

        List<RapportoImpresaPersona> Rip = biz.GetRapporti(idLavoratore);

        if (Rip != null)
        {
            LabelRapporto.Text = string.Format("{0} - <b>{1}</b> ({2:dd/MM/yyyy} -> {3})", 
                                                Rip[0].IdImpresa, 
                                                Rip[0].Impresa.RagioneSociale, 
                                                Rip[0].DataInizioValiditaRapporto, 
                                                Rip[0].DataFineValiditaRapporto > new DateTime(2079, 06, 05) ? "In corso" : Rip[0].DataFineValiditaRapporto.ToString("dd/MM/yyyy"));
        }

        List<DenunciaLavoratore> dl = biz.GetUltimeDenunce(idLavoratore);
        Presenter.CaricaElementiInGridView(
            RadGridDenunce,
            dl);

        LavoratoreDelega delega = biz.GetDelegaAttiva(idLavoratore);
        if (delega != null)
            LabelDelega.Text = String.Format("<b>{0}</b> ({1:dd/MM/yyyy} -> In corso)", delega.SindacatoCorrente.Descrizione, delega.DataIscrizioneSindacato);
        else
            LabelDelega.Text = "Nessuna delega attiva";
    }

    protected void RadGridDenunce_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            DenunciaLavoratore denuncia = (DenunciaLavoratore)e.Item.DataItem;
            Label lID = (Label)e.Item.FindControl("LabelIdImpresa");
            Label lRagSoc = (Label)e.Item.FindControl("LabelRagioneSociale");

            lID.Text = denuncia.IdImpresa.ToString();
            lRagSoc.Text = denuncia.RagioneSocialeImpresa;
        }
    }

    protected void ImageButtonEsportaRapporti_Click(object sender, ImageClickEventArgs e)
    {
        Context.Items["idLavoratore"] = IdLavoratore;
        Context.Items["tipoEstrazione"] = "ReportRapporto";
        Server.Transfer("~/AnagraficaLavoratori/Estrazioni.aspx");
    }

    protected void ImageButtonEsportaOre_Click(object sender, ImageClickEventArgs e)
    {
        Context.Items["idLavoratore"] = IdLavoratore;
        Context.Items["tipoEstrazione"] = "ReportOre";
        Server.Transfer("~/AnagraficaLavoratori/Estrazioni.aspx");
    }
}