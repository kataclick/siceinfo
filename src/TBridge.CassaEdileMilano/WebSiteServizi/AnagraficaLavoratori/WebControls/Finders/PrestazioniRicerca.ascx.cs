﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Business;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;

public partial class AnagraficaLavoratori_WebControls_Finder_PrestazioniRicerca : System.Web.UI.UserControl
{
    private readonly LavoratoreAnagraficaManager biz = new LavoratoreAnagraficaManager();

    public Int32? IdLavoratore
    {
        get
        {
            Int32? idLavoratore = null;
            if (ViewState["idLavoratore"] != null)
            {
                idLavoratore = (Int32)ViewState["idLavoratore"];
            }

            return idLavoratore;
        }

        set
        {
            ViewState["idLavoratore"] = value;
            PrestazioniFiltri1.IdLavoratore = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonCerca_Click(object sender, EventArgs e)
    {
        Ricerca();
    }

    public void Ricerca()
    {
        PrestazioneFilter filtro = PrestazioniFiltri1.GetFilter();

        Presenter.CaricaElementiInGridView( 
            RadGridPrestazioni,
            biz.GetPrestazioni(IdLavoratore.Value, filtro));
    }
    protected void RadGridPrestazioni_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Ricerca();
    }
    protected void ImageButtonPDF_Click(object sender, ImageClickEventArgs e)
    {
        Ricerca();
        RadGridPrestazioni.MasterTableView.ExportToPdf();
    }
    protected void RadGridPrestazioni_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Prestazione pre = (Prestazione)e.Item.DataItem;

            Label lTipoPrestazione = (Label)e.Item.FindControl("LabelTipoPrestazione");
            lTipoPrestazione.Text = String.Format("{0} - {1}", pre.TipoPrestazione.IdTipoPrestazione, pre.TipoPrestazione.Descrizione);
        }
    }
}