﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PrestazioniRicerca.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_Finder_PrestazioniRicerca" %>

<%@ Register src="../Filters/PrestazioniFiltri.ascx" tagname="PrestazioniFiltri" tagprefix="uc1" %>

<div class="standardDiv">    
    <uc1:PrestazioniFiltri ID="PrestazioniFiltri1" runat="server" />    
</div>

<div class="standardDiv">
    <div style="float:left">
        <asp:Button ID="ButtonCerca" runat="server" Text="Cerca" Width="100px" onclick="ButtonCerca_Click"/>
    </div>
    <div style="float:right">
        <asp:ImageButton
            ID="ImageButtonPDF"
            runat="server"
            ToolTip="Estrazione PDF" ImageUrl="~/images/pdf24.png" onclick="ImageButtonPDF_Click" />
    </div>
</div>

<div class="standardDiv">
   <telerik:RadGrid
                    ID="RadGridPrestazioni"
                    runat="server" CellSpacing="0" GridLines="None" AllowPaging="True" onpageindexchanged="RadGridPrestazioni_PageIndexChanged" onitemdatabound="RadGridPrestazioni_ItemDataBound">
                    <ExportSettings>
                        <Pdf PageWidth="" />
                    </ExportSettings>
                    <MasterTableView>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn DataField="Descrizione" FilterControlAltText="Filter column column" HeaderText="Tipo Prestazione" UniqueName="columnPrestazione">
                                <ItemTemplate>
                                    <b><asp:Label ID="LabelTipoPrestazione" runat="server" ></asp:Label></b>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:dd/MM/yyyy}" FilterControlAltText="Filter column1 column" HeaderText="Data domanda" UniqueName="columnDataDomanda" DataField="DataDomanda">
                            <ItemStyle Width="80px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:C}" FilterControlAltText="Filter column1 column" HeaderText="Importo erogato lordo" UniqueName="columnImportoLordo" DataField="ImportoErogatoLordo">
                            <ItemStyle Width="80px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:C}" FilterControlAltText="Filter column1 column" HeaderText="Importo erogato netto" UniqueName="columnImportoNetto" DataField="ImportoErogato">
                            <ItemStyle Width="80px" Font-Bold="true" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" HeaderText="Modalità pagamento" UniqueName="columnModalitaPagamento" DataField="ModalitaPagamento">
                            <ItemStyle Width="100px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" HeaderText="Numero mandato/bonifico/carta" UniqueName="columnNumeroMandato" DataField="NumeroMandato">
                            <ItemStyle Width="100px" />
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle PageSizeControlType="RadComboBox" />
                    </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
                </telerik:RadGrid>
</div>