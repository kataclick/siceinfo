﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormazioneRicerca.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_Finder_FormazioneRicerca" %>
<%@ Register src="../Filters/FormazioneFiltri.ascx" tagname="FormazioneFiltri" tagprefix="uc1" %>

<div class="standardDiv">
    <uc1:FormazioneFiltri ID="FormazioneFiltri1" runat="server" />
</div>

<div class="standardDiv">
    <div style="float:left">
        <asp:Button ID="ButtonCerca" runat="server" Text="Cerca" Width="100px" onclick="ButtonCerca_Click"/>
    </div>
    <div style="float:right">
        <asp:ImageButton
            ID="ImageButtonPDF"
            runat="server"
            ToolTip="Estrazione PDF" ImageUrl="~/images/pdf24.png" onclick="ImageButtonPDF_Click" />
    </div>
</div>

<div class="standardDiv">
   <telerik:RadGrid
                    ID="RadGridCorsi"
                    runat="server" CellSpacing="0" GridLines="None" AllowPaging="True" onpageindexchanged="RadGridCorsi_PageIndexChanged">
                    <ExportSettings>
                        <Pdf PageWidth="" />
                    </ExportSettings>
                    <MasterTableView>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn DataField="DenominazioneCorso" FilterControlAltText="Filter column column" HeaderText="Corso" UniqueName="columnCorso">
                                <ItemTemplate>
                                    <b><asp:Label ID="LabelCodice" runat="server" Text='<%# Eval("CodiceCorso") %>'></asp:Label>&nbsp;-&nbsp;<asp:Label ID="LabelDenominazione" runat="server" Text='<%# Eval("DenominazioneCorso") %>'></asp:Label></b>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:dd/MM/yyyy}" FilterControlAltText="Filter column1 column" HeaderText="Data inizio corso" UniqueName="columnDataInizioCorso" DataField="DataInizioCorso">
                            <ItemStyle Width="80px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:dd/MM/yyyy}" FilterControlAltText="Filter column1 column" HeaderText="Data fine corso" UniqueName="columnDataFineCorso" DataField="DataFineCorso">
                            <ItemStyle Width="80px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" HeaderText="Sede" UniqueName="columnSedeCorso" DataField="Sede">
                            <ItemStyle Width="200px" />
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle PageSizeControlType="RadComboBox" />
                    </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
                </telerik:RadGrid>
</div>