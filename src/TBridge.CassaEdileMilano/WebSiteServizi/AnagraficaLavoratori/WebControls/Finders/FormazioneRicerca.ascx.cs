﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;

public partial class AnagraficaLavoratori_WebControls_Finder_FormazioneRicerca : System.Web.UI.UserControl
{
    private readonly AnagraficaCondivisaManager biz = new AnagraficaCondivisaManager();

    public Int32? IdLavoratore
    {
        get
        {
            Int32? idLavoratore = null;
            if (ViewState["idLavoratore"] != null)
            {
                idLavoratore = (Int32)ViewState["idLavoratore"];
            }

            return idLavoratore;
        }

        set
        {
            ViewState["idLavoratore"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonCerca_Click(object sender, EventArgs e)
    {
        Ricerca();
    }

    public void Ricerca()
    {
        FormazioneFilter filtro = FormazioneFiltri1.GetFilter();

        Presenter.CaricaElementiInGridView(
            RadGridCorsi,
            biz.GetLavoratoriCorsi(IdLavoratore.Value, filtro));
    }

    protected void ImageButtonPDF_Click(object sender, ImageClickEventArgs e)
    {
        Ricerca();
        RadGridCorsi.MasterTableView.ExportToPdf();
    }

    protected void RadGridCorsi_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Ricerca();
    }
}