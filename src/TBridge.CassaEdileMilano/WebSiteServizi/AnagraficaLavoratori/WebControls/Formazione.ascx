﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Formazione.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_Formazione" %>
<style type="text/css">
    .style1
    {
        width: 170px;
    }
</style>
    
    <div style="float:left">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Formazione">
                </asp:Label>
            </td>
        </tr>
    </table>
    </div>
    <div style="float:right">
    <asp:ImageButton
        ID="ImageButtonDettagli"
        runat="server"
        ToolTip="Dettagli" ImageUrl="~/images/lente.png" onclick="ImageButtonDettagli_Click" />
    </div>
    <table class="borderedTable">
        <tr>
            <td class="style1">
                Ultimi corsi seguiti:
            </td>
            <td>
                <%--<asp:Label
                    ID="LabelDenunce"
                    runat="server">
                </asp:Label>
                --%>
                <telerik:RadGrid
                    ID="RadGridCorsi"
                    runat="server" CellSpacing="0" GridLines="None" PageSize="3">
                    <ExportSettings>
                        <Pdf PageWidth="" />
                    </ExportSettings>
                    <MasterTableView>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn FilterControlAltText="Filter column column" HeaderText="Corso" UniqueName="columnCorso" DataField="DenominazioneCorso">
                            <ItemStyle Font-Bold="true" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataFormatString="{0:dd/MM/yyyy}" FilterControlAltText="Filter column1 column" HeaderText="Data inizio corso" UniqueName="columnDataCorso" DataField="DataInizioCorso">
                            <ItemStyle Width="80px" />
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle PageSizeControlType="RadComboBox" />
                    </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>