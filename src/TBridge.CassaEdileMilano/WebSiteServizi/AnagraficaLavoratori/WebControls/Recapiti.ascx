﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Recapiti.ascx.cs" Inherits="AnagraficaLavoratori_WebControls_Recapiti" %>
<style type="text/css">
    .style1
    {
        width: 150px;
    }
</style>
<div style="float: left">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Recapiti">
                </asp:Label>
            </td>
        </tr>
    </table>
</div>
<div style="float: right">
    <asp:ImageButton ID="ImageButtonRecapitiDettagli" runat="server" ToolTip="Dettagli" ImageUrl="~/images/lente.png" OnClick="ImageButtonRecapitiDettagli_Click" />
</div>
<table class="borderedTable">
    <tr>
        <td class="style1">
            Residenza:
        </td>
        <td>
            <asp:Label ID="LabelResidenza" runat="server">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Presso:
        </td>
        <td>
            <asp:Label ID="LabelPresso" runat="server">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Domicilio:
        </td>
        <td>
            <asp:Label ID="LabelDomicilio" runat="server">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Telefono:
        </td>
        <td>
            <asp:Label ID="LabelTelefono" runat="server">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Cellulare:
        </td>
        <td>
            <asp:Label ID="LabelCellulare" runat="server">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Iscrizione servizio SMS:
        </td>
        <td>
            <asp:Label ID="LabelServizioSMS" runat="server">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            e-Mail:
        </td>
        <td>
            <asp:Label ID="LabelEmail" runat="server">
            </asp:Label>
        </td>
    </tr>
</table>
