﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain.AnagraficaCondivisa;
using TBridge.Cemi.Type.Filters.AnagraficaCondivisa;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;

public partial class AnagraficaLavoratori_WebControls_Formazione : System.Web.UI.UserControl
{
    private readonly AnagraficaCondivisaManager biz = new AnagraficaCondivisaManager();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaDati(Int32 idLavoratore)
    {
        List<LavoratoreCorso> cor = biz.GetLavoratoriCorsi(idLavoratore, new FormazioneFilter());

        Presenter.CaricaElementiInGridView(
            RadGridCorsi,
            cor);
    }

    protected void ImageButtonDettagli_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/AnagraficaLavoratori/Formazione.aspx");
    }
}