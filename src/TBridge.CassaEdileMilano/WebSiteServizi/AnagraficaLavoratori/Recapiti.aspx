﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Recapiti.aspx.cs" Inherits="AnagraficaLavoratori_Recapiti" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="WebControls/RecapitiDettagli.ascx" tagname="RecapitiDettagli" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello web lavoratori" sottoTitolo="Recapiti" />
    <div>
        <uc2:RecapitiDettagli ID="RecapitiDettagli1" runat="server" />
    </div>
        <br />
    <asp:Button
        ID="ButtonIndietro" 
        runat="server"
        Text="Torna alla scheda"
        Width="150px" onclick="ButtonIndietro_Click" />

</asp:Content>



