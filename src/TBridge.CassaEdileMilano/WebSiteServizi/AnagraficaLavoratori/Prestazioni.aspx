﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Prestazioni.aspx.cs" Inherits="AnagraficaLavoratori_Prestazioni" %>

<%@ Register src="WebControls/Finders/PrestazioniRicerca.ascx" tagname="PrestazioniRicerca" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">    
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello web lavoratore" sottoTitolo="Prestazioni" />
    <br />
    <uc1:PrestazioniRicerca ID="PrestazioniRicerca1" runat="server" />
    <br />
    <asp:Button
        ID="ButtonIndietro" 
        runat="server"
        Text="Torna alla scheda"
        Width="150px" onclick="ButtonIndietro_Click" />
</asp:Content>


