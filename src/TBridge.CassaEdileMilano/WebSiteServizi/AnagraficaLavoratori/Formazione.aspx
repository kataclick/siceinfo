﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Formazione.aspx.cs" Inherits="AnagraficaLavoratori_Formazione" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register src="WebControls/Filters/FormazioneFiltri.ascx" tagname="FormazioneFiltri" tagprefix="uc1" %>

<%@ Register src="WebControls/Finders/FormazioneRicerca.ascx" tagname="FormazioneRicerca" tagprefix="uc3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello web lavoratore" sottoTitolo="Formazione" />
    <br />
    <uc3:FormazioneRicerca ID="FormazioneRicerca1" runat="server" />
    <br />
    <asp:Button
        ID="ButtonIndietro" 
        runat="server"
        Text="Torna alla scheda"
        Width="150px" onclick="ButtonIndietro_Click" />
</asp:Content>



