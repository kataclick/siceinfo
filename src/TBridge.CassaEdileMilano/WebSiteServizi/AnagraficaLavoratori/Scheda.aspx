﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Scheda.aspx.cs" Inherits="AnagraficaLavoratori_Scheda" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<%@ Register src="WebControls/DatiAnagrafici.ascx" tagname="DatiAnagrafici" tagprefix="uc1" %>

<%@ Register src="WebControls/Recapiti.ascx" tagname="Recapiti" tagprefix="uc2" %>

<%@ Register src="WebControls/SituazioneCEMI.ascx" tagname="SituazioneCEMI" tagprefix="uc3" %>

<%@ Register src="WebControls/Prestazioni.ascx" tagname="Prestazioni" tagprefix="uc4" %>

<%@ Register src="WebControls/Formazione.ascx" tagname="Formazione" tagprefix="uc5" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
<uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello web lavoratore" sottoTitolo="Situazione lavoratore" />
    <br />
    <b>
    <asp:Label
        ID="LabelIdLavoratore"
        runat="server">
    </asp:Label>
    </b>
    <br />
    <div>
        <uc1:DatiAnagrafici ID="DatiAnagrafici1" runat="server" />
    </div>
    <br />
    <div>
        <uc2:Recapiti ID="Recapiti1" runat="server" />
    </div>
    <br />
    <div>
        <uc3:SituazioneCEMI ID="SituazioneCEMI1" runat="server" />
    </div>
    <br />
    <div>
        <uc4:Prestazioni ID="Prestazioni1" runat="server" />
    </div>
    <br />
    <div>    
        <uc5:Formazione ID="Formazione1" runat="server" />    
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>

