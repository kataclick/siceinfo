﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtenteLavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class AnagraficaLavoratori_DatiAnagrafici : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            UtenteLavoratore utLav = (UtenteLavoratore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            //IdLavoratore = utLav.IdLavoratore;
            DatiAnagraficiDettagli1.CaricaAnagrafica(utLav.IdLavoratore);

        }

    }
    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/AnagraficaLavoratori/Scheda.aspx");
    }
}