<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HomeLavoratore.aspx.cs" Inherits="HomeLavoratore" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/MenuLavoratori.ascx" TagName="MenuLavoratori" TagPrefix="uc1" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <%--<uc1:MenuLavoratori ID="MenuLavoratori1" runat="server" />--%>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Anagrafica lavoratore"
        sottoTitolo="Dati anagrafici" />
    <div style="text-align: center;">
        <table class="standardTable">
            <tr>
                <td>
                    Cognome
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCognome" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Nome
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNome" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Codice Fiscale
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCF" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Data di nascita
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataNascita" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Luogo di nascita
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLuogoNascita" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Sesso
                </td>
                <td>
                    <asp:TextBox ID="TextBoxSesso" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Indirizzo
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIndirizzo" runat="server" ReadOnly="true" Width="319px"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="ButtonModificaIndirizzo" runat="server" Text="Segnala variazione"
                        OnClick="ButtonModificaIndirizzo_Click" CausesValidation="False" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left">
                    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <div style="text-align: center;">
        <asp:Panel ID="Panel1" runat="server" Width="125px">
            <table class="standardTable" id="TABLE1">
                <tr>
                    <td>
                        Indirizzo:
                    </td>
                    <td>
                        <!-- Tabellina per l'impostazione dell'indirizzo-->
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownListPreIndirizzo" runat="server" Width="130px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxIndirizzoModifica" runat="server" Width="360px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxIndirizzoModifica"
                                        ErrorMessage="Fornire un indirizzo"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListProvincia" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged"
                            Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Comune
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListComuni" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                            Width="400px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        CAP
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListCAP" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="ButtonAggiorna" runat="server" Text="Segnala aggiornamento" OnClick="ButtonAggiorna_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
