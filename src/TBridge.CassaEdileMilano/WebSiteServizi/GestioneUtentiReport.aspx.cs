﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class GestioneUtentiReport : Page
{
    private readonly GestioneReport biz = new GestioneReport();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaListaReport();
            CaricaListaUtenti();
        }
    }

    private void CaricaListaReport()
    {
        RadListBoxSource.DataTextField = "Nome";
        RadListBoxSource.DataValueField = "Id";
        RadListBoxSource.DataSource = biz.GetListaReport();
        RadListBoxSource.DataBind();
    }

    private void CaricaListaUtenti()
    {
        LabelTipoUtenteVisualizzato.Text = "Tipologia utente visualizzata: " + DropDownListTipoUtente.SelectedValue;
        BindUtenti();
    }

    private void BindUtenti()
    {
        switch (DropDownListTipoUtente.SelectedValue)
        {
            case "Lavoratore":
                RadMultiPage1.SelectedIndex = 0;

                break;
            case "Impresa":

                RadMultiPage1.SelectedIndex = 1;

                break;
            case "Dipendente":

                RadMultiPage1.SelectedIndex = 2;

                break;
            case "Ospite":
                RadMultiPage1.SelectedIndex = 3;

                break;

            case "Fornitore":
                RadMultiPage1.SelectedIndex = 4;

                break;

            case "Ispettore":
                RadMultiPage1.SelectedIndex = 5;

                break;

            case "Esattore":
                RadMultiPage1.SelectedIndex = 6;

                break;

            case "Consulente":
                RadMultiPage1.SelectedIndex = 7;

                break;

            case "Sindacalista":
                RadMultiPage1.SelectedIndex = 8;

                break;

            case "CassaEdile":
                RadMultiPage1.SelectedIndex = 9;

                break;

            case "ASL":
                RadMultiPage1.SelectedIndex = 10;

                break;
            case "Committente":
                RadMultiPage1.SelectedIndex = 11;

                break;
        }
    }

    protected void DropDownListTipoUtente_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaListaUtenti();
    }
}