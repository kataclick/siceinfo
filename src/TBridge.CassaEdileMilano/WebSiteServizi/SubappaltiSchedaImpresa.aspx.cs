using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Notifiche.Business;
using TBridge.Cemi.Subappalti.Business;

public partial class SubappaltiSchedaImpresa : Page
{
    /// <summary>
    /// Utente che sta utilizzando la pagina
    /// </summary>
    private int idUtente;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Controllo l'autorizzazione dell'utente a vedere la pagina ...
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.SubappaltiRicerca);


        // Ricavo l'utente che sta utilizzando la pagina
        //IUtente utente = ApplicationInstance.GetUtenteSistema();

        // Va settato sempre
        //idUtente = utente.IdUtente;
        idUtente = GestioneUtentiBiz.GetIdUtente();

        // Carico i dati dell'impresa automaticamente dal web control
        if (Session["Subappalti.IdImpresa"] == null)
        {
            // TODO: il Response.Write non va bene ... definire cosa fare
            // Una redirect? a che?
            Response.Write("Impossibile visualizzare la scheda impresa");
        }
        else
        {
            // Inserisce nello storico la ricerca effettuata
            // TODO: try catch
            if (!IsPostBack)
            {
                SubappaltiBusiness business = new SubappaltiBusiness();
                Session["Subappalti.IdRicerca"] =
                    business.InserisciRicerca(idUtente, int.Parse(Session["Subappalti.IdImpresa"].ToString()), "");

                //caricare i vecchi lavoratori
                DataTable dt =
                    business.RicercaStoricoLavoratori(idUtente, int.Parse(Session["Subappalti.IdImpresa"].ToString()));

                //caricare la combo
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string listaLavoratori = ((string) row[0]);
                        //TODO MURA
                        //Caricare nella combo solo il 

                        if (listaLavoratori.Length != 0)
                        {
                            string[] lavoratori = listaLavoratori.Split(';');

                            foreach (string lavoratore in lavoratori)
                            {
                                if (lavoratore != string.Empty)
                                {
                                    string[] nomeCognome = lavoratore.Split('_');
                                    ListItem li = new ListItem(nomeCognome[0]);

                                    if (!DropDownListCognomi.Items.Contains(li))
                                        DropDownListCognomi.Items.Add(li);
                                }
                            }
                        }
                    }
                }
            }
        }
    } // Page_Load

    protected void btnRicerca_Click(object sender, EventArgs e)
    {
        try
        {
            // Apertura lista dipendenti per l'impresa
            CaricaListaLavoratori();

            //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
            //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
            //TBridge.Cemi.ActivityTracking.Log.Write("Subappalti Ricerca lavoratori", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);
        }
        catch
        {
            //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
            //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
            //TBridge.Cemi.ActivityTracking.Log.Write("Errore Ricerca lavoratori", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);
        }
    }

    /// <summary>
    /// Carica la lista dei lavoratori in base ai parametri passati
    /// </summary>
    private void CaricaListaLavoratori()
    {
        // Controllo validit� campi, o almeno uno obbligatorio
        SubappaltiBusiness business = new SubappaltiBusiness();

        string criterioRicerca;

        string daRicercare;

        if (RadioButtonTesto.Checked)
            daRicercare = txtCognome.Value;
        else
            daRicercare = DropDownListCognomi.Text;

        LabelCognomeRicercato.Text = daRicercare;

        //if (string.IsNullOrEmpty(txtCodice.Text) && string.IsNullOrEmpty(txtCognome.Text))
        if ((string.IsNullOrEmpty(txtCodice.Value)) && string.IsNullOrEmpty(daRicercare))
        {
            // Segnalo obbligatorio almeno uno dei due campi ... 
            // purtroppo lo devo far fare al server
            if (RadioButtonTesto.Checked)
            {
                lblErrMessage.Visible = true;
                lblErrMessagePrec.Visible = false;
            }
            else
            {
                lblErrMessage.Visible = false;
                lblErrMessagePrec.Visible = true;
            }
        }
        else if ((!string.IsNullOrEmpty(txtCodice.Value)) && (RadioButtonTesto.Checked))
        {
            // Caso in cui sia stato valorizzato il codice

            // Resetto il messaggio di errore
            lblErrMessage.Visible = false;
            lblErrMessagePrec.Visible = false;

            int idLavoratore;
            if (int.TryParse(txtCodice.Value, out idLavoratore))
            {
                // Salvo la datasource nella session perch� mi serve per il paging
                Session["Subappalti.Datasource"] =
                    business.RicercaLavoratori(int.Parse(Session["Subappalti.IdImpresa"].ToString()),
                                               idLavoratore);
                gvLavoratori.DataSource = Session["Subappalti.Datasource"];
                //criterioRicerca = "Cod. Lavoratore = " + idLavoratore.ToString() + ";";
            }
        }
        else
        {
            // Caso in cui sia stato valorizzato il cognome

            // Resetto il messaggio di errore
            lblErrMessage.Visible = false;
            lblErrMessagePrec.Visible = false;

            // Salvo la datasource nella session perch� mi serve per il paging
            //Session["Subappalti.Datasource"] = business.RicercaLavoratori(int.Parse(Session["Subappalti.IdImpresa"].ToString()), 
            //                                                     txtCognome.Text, txtNome.Text);
            Session["Subappalti.Datasource"] =
                business.RicercaLavoratori(int.Parse(Session["Subappalti.IdImpresa"].ToString()),
                                           daRicercare, txtNome.Value);
            gvLavoratori.DataSource = Session["Subappalti.Datasource"];

            // Parte Notifiche
            NotificaBusiness biz = new NotificaBusiness();
            string nomeDaRicercare = null;
            if (!String.IsNullOrEmpty(txtNome.Value))
                nomeDaRicercare = txtNome.Value;
            GridViewNotifiche.DataSource =
                biz.RicercaPerSubappalti(daRicercare, nomeDaRicercare,
                                         Int32.Parse(Session["Subappalti.IdImpresa"].ToString()));
            GridViewNotifiche.DataBind();

            //criterioRicerca = "Cognome = " + txtCognome.Text;
            //if (!string.IsNullOrEmpty(txtNome.Text)) 
            //    criterioRicerca += " Nome = " + txtNome.Text;
            //criterioRicerca += ";";
        }

        gvLavoratori.DataBind();

        LabelRicerca.Visible = true;

        NotificaBusiness notbBiz = new NotificaBusiness();
        int mesiRitardo = notbBiz.MesiRitardoDati();

        LabelUltimoMeseDenunciato.Text = "Ultimo mese denunciato: " +
                                         DateTime.Today.AddMonths(-mesiRitardo).ToString("MM/yyyy");

        //TRIFFO 31/08/06: in base alle modifiche chieste da elmosi i criteri di ricerca dovranno in realt� memorizzare il nome di 
        //chi � stato trovato, nulla altrimenti
        criterioRicerca = string.Empty;

        foreach (DataRow lavoratoreRow in ((DataTable) gvLavoratori.DataSource).Rows)
        {
            criterioRicerca += lavoratoreRow["cognome"] + "_" + lavoratoreRow["nome"] + ";";
        }

        //Sempre per richeista di elmosi memorizziamo la ricerca solo se ha portato ad estrarre dei nomi
        if (criterioRicerca != string.Empty)
        {
            // Aggiungo allo storico i parametri della ricerca lavoratori
            // Giusto che esploda se non c'� Session["Subappalti.IdRicerca"]
            business.AggiungiCriterioRicerca(int.Parse(Session["Subappalti.IdRicerca"].ToString()), criterioRicerca);
        }
    } // CaricaLista

    protected void gvLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // A sto punto la datasource � nulla ... la devo ricaricare
        gvLavoratori.DataSource = Session["Subappalti.Datasource"];

        gvLavoratori.PageIndex = e.NewPageIndex;
        gvLavoratori.DataBind();
    }
} // Class