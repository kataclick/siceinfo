﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class Edilconnect_Edilconnect : Page
{
    //private const String CODICECE = "111113";
    //private const String PIVA = "02646210134";
    //private const String GUID = "12345678901234567890";
    //tipoUtente: 1=consulente; 2=impresa
    //private const String LOGINCE = "MI00";
    //private const String PASSWORD = "1qazxsw2";

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        if (!GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
            throw new Exception("Pagina non valida per l'utente");

        #endregion

        string usernameAuthEc = ConfigurationManager.AppSettings["AuthEcUsername"];
        string passwordAuthEc = ConfigurationManager.AppSettings["AuthEcPassword"];

        string codiceCe = String.Empty;
        string partitaIva = String.Empty;
        string guidEc = String.Empty;
        int tipoUtente = 0;

        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        if (utente is Impresa)
        {
            codiceCe = ((Impresa) utente).IdImpresa.ToString(CultureInfo.InvariantCulture);
            partitaIva = String.IsNullOrEmpty(((Impresa) utente).PartitaIVA)
                             ? "00000000000"
                             : ((Impresa)utente).PartitaIVA;
            guidEc = ((Impresa) utente).GuidEdilconnect;

            tipoUtente = 2;
        }
        else if (utente is Consulente)
        {
            codiceCe = ((Consulente) utente).IdConsulente.ToString(CultureInfo.InvariantCulture);
            partitaIva = String.IsNullOrEmpty(((Consulente) utente).CodiceFiscale)
                             ? "00000000000"
                             : ((Consulente) utente).CodiceFiscale;
            guidEc = ((Consulente) utente).GuidEdilconnect;

            tipoUtente = 1;
        }

        long longTimeUtc = DateTime.Now.ToFileTimeUtc();
        string stringValue = String.Format("{0} {1}", partitaIva, longTimeUtc);
        string stringEncoded = CEXChangeCryptography.cxAESEncryptToString(stringValue, passwordAuthEc);

        String url =
            String.Format(
                "https://www.edilconnect.it/Transfer.aspx?codicece={4}&guid={0}&partitaiva={1}&tipoutente={5}&logince={2}&passwordce={3}&t=beta",
                guidEc,
                partitaIva,
                usernameAuthEc,
                stringEncoded,
                codiceCe,
                tipoUtente);

        string param = String.Format("window.open('{0}','_blank','width=' + screen.width + ', height=' + (screen.height-100) + ', top=0, left=0, resizable=yes,menubar=no,toolbar=no,location=no,directories=no,status=no')", url);

        ButtonEdilConnect.Attributes.Add("onclick", param);

        //Window.open(url, ‘EdilConnect’, ‘menubar=no’)
        //javascript:window.open('<url completo>','_blank','width=' + screen.width + ', height=' + (screen.height-100) + ', top=0, left=0, resizable=yes,menubar=no,toolbar=no,location=no,directories=no,status=no');

        #region Audit

        EdilconnectAudit edilconnectAudit = new EdilconnectAudit();
        edilconnectAudit.LogAccess(utente.IdUtente, DateTime.Now, url);

        #endregion
    }
}