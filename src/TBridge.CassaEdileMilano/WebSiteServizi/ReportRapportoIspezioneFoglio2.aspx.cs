using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ReportRapportoIspezioneFoglio2 : Page
{
    private readonly CantieriBusiness biz = new CantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
        funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CantieriRapportoIspezioneFoglio2.aspx");

        if (Request.QueryString["idIspezione"] != null)
        {
            int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
            RapportoIspezione ispezione = biz.GetIspezioneByKey(idIspezione);

            ReportViewerRapportoIspezione.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            ReportViewerRapportoIspezione.ServerReport.ReportPath = "/ReportCantieri/RapportoIspezioneFoglio2";
            ReportParameter[] listaParam = new ReportParameter[2];
            listaParam[0] = new ReportParameter("idIspezione", ispezione.IdIspezione.ToString());
            listaParam[1] = new ReportParameter("idIspettore", ispezione.Ispettore.IdIspettore.ToString());

            ReportViewerRapportoIspezione.ServerReport.SetParameters(listaParam);
        }
    }
}