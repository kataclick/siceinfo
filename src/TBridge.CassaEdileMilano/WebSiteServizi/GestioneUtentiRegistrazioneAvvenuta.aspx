<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneUtentiRegistrazioneAvvenuta.aspx.cs" Inherits="GestioneUtentiRegistrazioneAvvenuta" %>

<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione utenti"
        sottoTitolo="Registrazione avvenuta" />
    <br />
    La registrazione � stata portata a termine con successo. Ricordarsi la username
    e la password per poter accedere al sito e ai servizi offerti.<br />
    <%--<br />
    <b>Username:</b>
    <asp:Label ID="LabelUsername" runat="server"></asp:Label>
    <br />
    <b>Password:</b>
    <asp:Label ID="LabelPassword" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Button ID="ButtonStampa" runat="server" Text="Stampa" onClientClick="window.print();"/>--%>
    <br />
    <br />
    <asp:HyperLink ID="HyperLinkHome" runat="server" NavigateUrl="Default.aspx">Torna alla pagina principale</asp:HyperLink>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
