<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportRapportoIspezioneFoglio1.aspx.cs" Inherits="ReportRapportoIspezioneFoglio1" %>

<%@ Register Src="WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc1" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Rapporto di Ispezione"
        titolo="Cantieri" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerRapportoIspezione" runat="server"
        ProcessingMode="Remote" width="100%">
    </rsweb:ReportViewer>
    </td></tr></table>
</asp:Content>

