﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="VisualizzaEstrattoConto.aspx.cs" Inherits="CigoTelematica_VisualizzaEstrattoConto" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cigo" sottoTitolo="Visualizzazione Estratto Conto Cigo" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerEstrattoConto" runat="server" ProcessingMode="Remote"
                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" Height="550pt"
                    Width="550pt" ShowParameterPrompts="false" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
