﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Business;
using Cemi.CigoTelematica.Type.Filters;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CigoTelematica_DomandeRicercaBackOffice : System.Web.UI.Page
{
    private BusinessEF _biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CigoTelematicaGestioneBackOffice);
        #endregion

        if (!IsPostBack)
        {
            DomandeFilter filter = UserControlFiltroRicercaDomandeBackOffice.CaricaFiltriSalvati();
            if (filter != null)
            {
                LoadDomande(filter);
            }
        }
        //if (!GestioneUtentiBiz.IsImpresa())
        //{
        //   // TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaCorrente = (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        //   // throw new Exception("Loggati come impresa!!!!!");
        //}

    }

    protected void RadGridDomande_ItemDataBound(object sender, GridItemEventArgs e)
    {
        String utenteincarico = "";

        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            CigoTelematicaDomanda domanda = (CigoTelematicaDomanda)e.Item.DataItem;
            Label labPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            Label labImpresa = (Label)e.Item.FindControl("LabelImpresa");
            Label labIndirizzo = (Label)e.Item.FindControl("LabelIndirizzo");
            //NManelli:aggiungo per ottenere lo stato della domanda
            Label labStato = (Label)e.Item.FindControl("LabelStato");
            Button bInCarico = (Button)e.Item.FindControl("ButtonInCarico");
            RadButton btnSeleziona = (RadButton)e.Item.FindControl("RadButtonSelezinona");

            Label labInCarico = (Label)e.Item.FindControl("LabelUtenteInCarico");


            labPeriodo.Text = String.Format("{0}/{1}", domanda.Mese, domanda.Anno);
            labImpresa.Text = String.Format("{0}", domanda.IdImpresa);
            labIndirizzo.Text = String.Format("{0} - {1}({2})", domanda.IndirizzoCantiere, domanda.ComuneCantiere, domanda.ProvinciaCantiere);
            //NManelli:aggiungo per ottenere lo stato della domanda
            labStato.Text = String.Format("{0}", domanda.TipoStatoPrestazione.Descrizione);
            //labStato.Text = String.Format("{0}", domanda.idStato);
            btnSeleziona.NavigateUrl = String.Format("DomandaGestione.aspx?id={0}", domanda.Id);
            labInCarico.Text = domanda.UtenteInCarico != null ? domanda.UtenteInCarico.Username : null;
            //if (domanda.idUtenteInCarico != null)
            //{
            //    utenteincarico = _biz.DescrizioneUtenteInCarico((Int32)domanda.Id, (Int32)domanda.idUtenteInCarico);
            //    labInCarico.Text = String.Format("{0}", utenteincarico);
            //}
            //else 
            //{
            //    labInCarico.Text = "";
            //}



            if (domanda.IdStato == "L" || domanda.IdStato == "D")
            {
                bInCarico.Enabled = false;
            }
            else
            {
                bInCarico.Enabled = true;
            }

            if (domanda.IdUtenteInCarico != null)
            {
                bInCarico.Text = "Rilascia";
                //bInCarico.Enabled = true;
            }
            else
            {
                bInCarico.Text = "In carico";
                //bInCarico.Enabled = false;

            }
          
        }
    }

    protected void RadGridDomande_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        LoadDomande();
    }


    protected void RadButtonCerca_Click(object sender, EventArgs e)
    {
        LoadDomande();
    }


    protected void RadButtonConferma_Click(object sender, EventArgs e)
    {
        Page.Validate("validaNuovaRichiesta");
        if (Page.IsValid)
        {
            SaveDomanda();
            
        }
    }

    private void LoadDomande()
    {
        DomandeFilter filter = UserControlFiltroRicercaDomandeBackOffice.GetFilter();
        LoadDomande(filter);
       

    }

    private void LoadDomande(DomandeFilter filter)
    {
        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaCorrente = (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            filter.IdImpresa = impresaCorrente.IdImpresa;

            Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeImpresa(filter));

        }
        else
        {

            Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeBackOffice(filter));
        }
    }


    private CigoTelematicaDomanda GetDomanda()
    {
        CigoTelematicaDomanda domanda = UserControlDatiDomanda1.GetDomanda();

        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaCorrente = (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            domanda.IdImpresa = impresaCorrente.IdImpresa;
            
        }

        domanda.IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente();
        domanda.IdStato = "I";  //immessa      
        domanda.DataInserimentoRecord = DateTime.Now;
        domanda.DataDomanda = DateTime.Now.Date;
                                            
        
        return domanda;
    }

    private void SaveDomanda()
    {
        CigoTelematicaDomanda domanda = GetDomanda();
        _biz.InsertNuovaDomanda(domanda);

        ScriptManager.RegisterStartupScript(this, typeof(CigoTelematica_DomandeRicercaBackOffice), "CloseScript", String.Format("CloseRadWindowAndGotoGestione('{0}')", domanda.Id), true);
       
    }


    protected void RadGridDomande_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "inCarico")
        {
            
                Int32 idDomanda = (Int32)RadGridDomande.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];
                Int32? idUtenteInCarico = (Int32?)RadGridDomande.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdUtenteInCarico"];

                

                if (idUtenteInCarico != null)
                {
                    _biz.UpdateCigoTelematicaDomandaUtenteInCarico(idDomanda, null);
                }
                else
                {
                    _biz.UpdateCigoTelematicaDomandaUtenteInCarico(idDomanda, GestioneUtentiBiz.GetIdUtente());

                }
                       
                Server.Transfer("~/CigoTelematica/DomandeRicercaBackOffice.aspx");
        }
    }
}