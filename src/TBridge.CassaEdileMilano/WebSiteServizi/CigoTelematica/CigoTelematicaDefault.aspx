﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CigoTelematicaDefault.aspx.cs" Inherits="CigoTelematica_CigoTelematicaDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCigoTelematica.ascx" TagName="MenuCigoTelematica"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCigoTelematica ID="MenuCigoTelematica1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="CIGO"
        sottoTitolo="CIGO" />
    <p class="DefaultPage">
        bla 
        bla 
        bla 
        CIGO 
        bla 
    </p>
<%--    <p class="DefaultPage">
        <strong>Gestione assenze e caricamento file xml</strong><br />
        <br />
        Le voci in oggetto consentono di scegliere l’assenza del lavoratore dipendente da
        giustificare e di inserire i relativi certificati medici o altri documenti da far
        pervenire in Cassa Edile al fine dell’ottenimento del rimborso.
        <br />
        <br />
        <strong>Estratto Conto</strong>
        <br />
        <br />
        La voce consente di:
        <ul>
            <li>ricercare per periodo (mese/anno); </li>
            <li>visualizzare; </li>
            <li>salvare o stampare </li>
        </ul>
        gli attestati relativi agli estratti conto dei rimborsi erogati da Cassa Edile relativamente
        ai trattamenti economici per:
        <ul>
            <li>malattia; </li>
            <li>infortunio e malattia professionale. </li>
        </ul>
        Si precisa che gli attestati sono ricercabili dal mese di novembre 2010 a seguire.
        <br />
        <br />
        <span style="color: #CD071E;"><strong>AVVISO</strong></span>
        <br />
        </b><br />
        Si informa che il <span style="color: #CD071E;"><strong>CARICAMENTO DEI CERTIFICATI
            MEDICI</strong></span> può essere effettuato, a seconda della tipologia di giustificativo,
        alle seguenti voci del menu:
        <ul>
            <li><b>"Gestione assenze"</b>: alla finestra <b>"Elenco giustificativi"</b> selezionare
                la voce <b>"Aggiungi giustificativi"</b>, inserire il numero di certificato e premere
                il pulsante <b>"Carica dati INPS"</b>;</li>
        </ul>
        oppure
        <ul>
            <li><b>"Caricamento file xml"</b> per l’invio del giustificativo nel solo formato "xml".</li>
        </ul>
        <br />
        Cliccare <a href="MalattiaTelematica.pdf">qui</a> per scaricare il manuale utente.<br />
    </p>--%>
</asp:Content>
