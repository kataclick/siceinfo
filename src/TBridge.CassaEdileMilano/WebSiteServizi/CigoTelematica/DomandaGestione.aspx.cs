﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Cemi.CigoTelematica.Type.Entities;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CigoTelematica_DomandaGestione : System.Web.UI.Page
{
    private BusinessEF _biz = new BusinessEF();
    private Business _b = new Business();
    private bool _isBackoffice;

    private int _idDomanda;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CigoTelematicaGestioneImpresa);
        funzionalita.Add(FunzionalitaPredefinite.CigoTelematicaGestioneBackOffice);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        string idDomandaStr = Request.QueryString["Id"];
        _idDomanda = int.Parse(idDomandaStr);

        _isBackoffice =  !(GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente());

        
        DocumentoInpsLista.OnAltroDocumentoSelected += DocumentoInpsLista_OnAltroDocumentoSelected;

        DocumentoInpsLista.OnDocumentoDeleted += DocumentoInpsLista_OnDocumentoDeleted;

        GestioneDocumentiInps.OnDocumentoInpsReturned += GestioneDocumentiInps_OnAltriDocumentiReturned;

        UserControlDettaglioLavoratore.LavoratoreSaved += new EventHandler(UserControlDettaglioLavoratore_LavoratoreSaved);

        
        //il codice nell'if viene eseguito SOLO al primo caricamento della pagina !
        if (!IsPostBack)
        {
            CigoTelematicaDomanda domanda = _biz.GetDomandaById(_idDomanda);
            if (domanda != null)
            {
                UserControlDatiDomanda.CaricaDatiDomanda(domanda);
                LoadLavoratoriAggiungi();
                LabelDenunciaDel.Text = string.Format("{0}/{1}", domanda.Mese, domanda.Anno);
                TextBoxNote.Text = domanda.Note;
                ViewState["IsImpresaRegolare"] = IsImpresaRegolare(domanda);
                List<LavoratoreDettagli> lavoratori = LoadLavoratori();

                //ViewState["IdDomandaCigo"] = domanda.Id;
                //ViewState["IdImpresa"] = domanda.IdImpresa;
                //ViewState["IdStato"] = domanda.idStato;
                //ViewState["Mese"] = domanda.mese;
                //ViewState["Anno"] = domanda.anno;
                //ViewState["DataDomanda"] = domanda.dataDomanda;

                Boolean abilita = _b.AbilitaControllo(domanda.IdStato);//_b.AbilitaControllo(_idDomanda);

                GestioneDocumentiInps.CaricaDomanda(domanda);
                //DocumentoInpsLista.CaricaAltriDocumenti(domanda.Id, abilita);
                GestisciAbilitazionePulsanti(domanda.IdStato ,lavoratori);
                GestisciValidazioni(domanda);
                
            }

        }
        else
        {
            String eventArg = Request["__EVENTARGUMENT"];

            if (eventArg != null && eventArg.Contains("@@@@@Rebind@@@@@"))
            {
                                
                    LoadLavoratori();
                
            }
 
        
        }

      
    
    }


    private void DocumentoInpsLista_OnDocumentoDeleted(object sender, EventArgs e)
    {
        CigoTelematicaDomanda domanda = _biz.GetDomandaById(_idDomanda);
        GestisciValidazioni(domanda);
    
    }


    private void GestisciAbilitazionePulsanti(string idStatoDomanda, List<LavoratoreDettagli> lavoratori)
    {

        //disattivo i pulsanti di conferma domanda, cancella domanda e aggiungi lavoratore se è nello stato
        //diverso da immessa (I)
        if (idStatoDomanda != "I")
        {
            RadButtonConferma.Enabled = false;
            RadButtonCancella.Enabled = false;
            RadButtonAggiungiLavoratore.Enabled = false;
            RadGridLavoratori.Columns[RadGridLavoratori.Columns.Count - 1].Visible = false;
            DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, false);
        }
            //aggiungo l'else NM.(02/10/2014)
        else 
        {
            RadButtonConferma.Enabled = true;
            RadButtonCancella.Enabled = true;
            RadButtonAggiungiLavoratore.Enabled = true;
            RadGridLavoratori.Columns[RadGridLavoratori.Columns.Count - 1].Visible = true;
            DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, true);
        
        }

        if (!_isBackoffice)
        {

            RadGridLavoratori.Columns[4].Visible = false;
            RadGridAggiungiLavoratori.Columns[3].Visible = false;
            RowBackOffice.Visible = false;
            LabelNote.Visible = false;
            TextBoxNote.Visible = false;
            ButtonSalvaNote.Visible = false;
        }
        else
        {
            if (idStatoDomanda == "I")
            {

                RowBackOffice.Visible = false;

            }
            else
            {
                RadButtonAggiungiLavoratore.Enabled = true;
                RowImpresa.Visible = false;
                RowBackOffice.Visible = true;
                switch (idStatoDomanda)
                {
                    case "A":
                        ButtonAccogli.Enabled = false;
                        ButtonAnnulla.Enabled = false;
                        ButtonInAttesa.Enabled = false;
                        ButtonImmessaPervenuta.Enabled = true;
                        ButtonRespingi.Enabled = false;
                        RadButtonAggiungiLavoratore.Enabled = false;
                        ButtonSalvaNote.Enabled = true;
                        TextBoxNote.ReadOnly = false;
                        DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, false);
                        break;
                                            
                    case "R":
                        ButtonAccogli.Enabled = false;
                        ButtonAnnulla.Enabled = false;
                        ButtonInAttesa.Enabled = false;
                        ButtonImmessaPervenuta.Enabled = false;
                        ButtonRespingi.Enabled = false;
                        RadButtonAggiungiLavoratore.Enabled = false;
                        ButtonSalvaNote.Enabled = true;
                        TextBoxNote.ReadOnly = false;
                        DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, false);
                        break;

                    case "O":
                        ButtonAccogli.Enabled =  true; //lavoratori.Any(x => IsLavoratoreAccoglibile(x));
                        ButtonAnnulla.Enabled = true;
                        ButtonInAttesa.Enabled = false;
                        ButtonImmessaPervenuta.Enabled = true;
                        ButtonRespingi.Enabled = true;
                        ButtonSalvaNote.Enabled = true;
                        TextBoxNote.ReadOnly = false;
                        DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, true);
                        break;
                    case "C":
                        ButtonAccogli.Enabled = false;
                        ButtonAnnulla.Enabled = false;
                        ButtonInAttesa.Enabled = false;
                        //NManelli(16042015): su richiesta di Bianchini quando è accolta posso ritornare a Immessa pervenuta
                        //                    per gestire di eventuali errori prima che passi a sicenew con il notturno
                        ButtonImmessaPervenuta.Enabled = true;
                        ButtonRespingi.Enabled = false;
                        RadButtonAggiungiLavoratore.Enabled = false;
                        ButtonSalvaNote.Enabled = true;
                        TextBoxNote.ReadOnly = false;
                        DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, false);
                        break;
                    case "T":
                        ButtonAccogli.Enabled = true;//lavoratori.Any(x => IsLavoratoreAccoglibile(x));
                        ButtonAnnulla.Enabled = true;
                        ButtonInAttesa.Enabled = true;
                        ButtonImmessaPervenuta.Enabled = false;
                        ButtonRespingi.Enabled = true;
                        ButtonSalvaNote.Enabled = true;
                        TextBoxNote.ReadOnly = false;
                        DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, true);
                        break;
                    default:
                        ButtonAccogli.Enabled = false;
                        ButtonAnnulla.Enabled = false;
                        ButtonInAttesa.Enabled = false;
                        ButtonImmessaPervenuta.Enabled = false;
                        ButtonSalvaNote.Enabled = true;
                        TextBoxNote.ReadOnly = false;
                        ButtonRespingi.Enabled = false;
                        RadButtonAggiungiLavoratore.Enabled = false;
                        DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, false);
                        break;
                }

                GestisciAbilitazioneAccogli(lavoratori);
            }
        }
    }

    private void GestisciAbilitazioneAccogli(List<LavoratoreDettagli> lavoratori)
    {
        //if (ButtonAccogli.Enabled == true)
        if (lavoratori.FirstOrDefault() != null && (lavoratori.First().IdStatoDomanda == "T" || lavoratori.First().IdStatoDomanda == "O"))
        {
            ButtonAccogli.Enabled = lavoratori.Any(x => IsLavoratoreAccoglibile(x));
        }
        //{
            

        //    //foreach (var item in lavoratori)
        //    //{
        //    //    if (!IsLavoratoreAccoglibile(item))
        //    //    {
        //    //        ButtonAccogli.Enabled = false;
        //    //        break;
        //    //    }
        //    //}
        //}
    }

    
    private Boolean IsImpresaRegolare(CigoTelematicaDomanda domanda)
    {
                
        DateTime dataFine = new DateTime(domanda.Anno.Value, domanda.Mese.Value, 1);
        DateTime dataInizio = dataFine.AddMonths(-4);


        decimal debito = RegolaritaContributivaManager.GetDebitoImpresa(domanda.IdImpresa, dataInizio, dataFine);

        return (debito < 30.0M);
       

    }


    private Boolean IsLavoratoreAccoglibile(LavoratoreDettagli lavoratore)
    {
        return (Boolean)ViewState["IsImpresaRegolare"] && lavoratore.ControlliValidita.RapportoDiLavoroValido
            && lavoratore.ControlliValidita.LavoratoreInDenuncia && lavoratore.ControlliValidita.LavoratoreApprendista
            && (lavoratore.IdStato != "R" && lavoratore.IdStato != "L");
    }

    private void GestisciStatoDomanda(CigoTelematicaDomanda domanda)
    {
        UserControlDatiDomanda.CaricaDatiDomanda(domanda);
        List<LavoratoreDettagli> lavoratori = _biz.GetLavoratoriInDomanda(domanda.Id, true);
        GestisciAbilitazionePulsanti(domanda.IdStato, lavoratori);
    
    }

    protected void RadGridLavoratori_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        LoadLavoratori();
    }


    private List<LavoratoreDettagli> LoadLavoratori()
    {
        List<LavoratoreDettagli> lavoratori = _biz.GetLavoratoriInDomanda(_idDomanda, true);
        Presenter.CaricaElementiInGridView(RadGridLavoratori, lavoratori);

        return lavoratori;
        
    }


    protected void RadGridLavoratori_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            LavoratoreDettagli lavoratore = (LavoratoreDettagli)e.Item.DataItem;
            Label LabelStato = (Label)e.Item.FindControl("LabelStato");
            Label LabelCodice = (Label)e.Item.FindControl("LabelCodice");
            Label LabelLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label LabelOreCigo = (Label)e.Item.FindControl("LabelOreCigo");
            Label LabelTipoOreCigo = (Label)e.Item.FindControl("LabelTipoOreCigo");


            RangeValidator validator = (RangeValidator)e.Item.FindControl("ValidatorStatoLavoratore");
            TextBox TextboxStatoHidden = (TextBox)e.Item.FindControl("TextboxStatoHidden");
            //RadButton btnSeleziona = (RadButton)e.Item.FindControl("RadButtonSeleziona");

            LabelStato.Text = String.Format("{0}", lavoratore.Stato);
            TextboxStatoHidden.Text = lavoratore.IdStato;
            LabelCodice.Text = String.Format("{0}", lavoratore.IdLavoratore);
            LabelLavoratore.Text = lavoratore.NomeCompleto;//String.Format("{0} {1}", lavoratore.n, lavoratore.Lavoratore.Cognome);
            LabelOreCigo.Text = String.Format("{0}", lavoratore.OreRichieste);
            LabelTipoOreCigo.Text = lavoratore.IdTipoOra;

            validator.ErrorMessage = String.Format("Lavoratore {0} incompleto (controllare la presenza di busta paga/ gg. cigo selezionati/ paga oraria dichiarata)", lavoratore.IdLavoratore);

            //if (domanda.idStato != "I")
            //{
            //    RadButton ButtonElimina = (RadButton)e.Item.FindControl("RadButtonElimina");
            //}
        }
    }


    protected void RadGridAggiungiLavoratori_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        LoadLavoratoriAggiungi();
    }


    private void LoadLavoratoriAggiungi()
    {
        Presenter.CaricaElementiInGridView(RadGridAggiungiLavoratori, _biz.GetLavoratoriConCigo(_idDomanda));
        

    }

    protected void RadGridAggiungiLavoratori_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            LavoratoreCigo lavoratore = (LavoratoreCigo)e.Item.DataItem;
            Label LabelCodice = (Label)e.Item.FindControl("LabelCodice");
            Label LabelLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label LabelOreCigo = (Label)e.Item.FindControl("LabelOreCigo");
            Label LabelTipoOreCigoAggiungi = (Label)e.Item.FindControl("LabelTipoOreCigoAggiungi");
            RadButton btnAggiungi = (RadButton)e.Item.FindControl("RadButtonAggiungi");

          
            LabelCodice.Text = String.Format("{0}", lavoratore.Lavoratore.Id);
            LabelLavoratore.Text = String.Format("{0} {1}", lavoratore.Lavoratore.Nome, lavoratore.Lavoratore.Cognome);
            LabelOreCigo.Text = String.Format("{0}", (int)lavoratore.OreCigo);
            LabelTipoOreCigoAggiungi.Text = lavoratore.TipoOre;
            btnAggiungi.Enabled = !lavoratore.Aggiunto;
            

        }
    }


    protected void ButtonSalvaNote_Click(object sender, EventArgs e)
    {
        _biz.ConfermaNote(_idDomanda, TextBoxNote.Text);
    }



    protected void ButtonAnnullaDomanda_Click(object sender, EventArgs e)
    {

        CigoTelematicaDomanda domanda = _biz.AnnullaDomanda(GetDomandaUpdate());
        GestisciStatoDomanda(domanda);
        PanelDettaglioLavoratore.Visible = false;
    }


    protected void ButtonInAttesa_Click(object sender, EventArgs e)
    {

        CigoTelematicaDomanda domanda = _biz.InAttesaDomanda(GetDomandaUpdate());
        GestisciStatoDomanda(domanda);
        PanelDettaglioLavoratore.Visible = false;
    }


    protected void ButtonImmessaPervenuta_Click(object sender, EventArgs e)
    {

        CigoTelematicaDomanda domanda = _biz.RiportaImmessaPervenuta(GetDomandaUpdate());
        GestisciStatoDomanda(domanda);
        PanelDettaglioLavoratore.Visible = false;
    }


    protected void ButtonRespingiDomanda_Click(object sender, EventArgs e)
    {
        CigoTelematicaDomanda domanda = _biz.RespingiDomanda(GetDomandaUpdate());
        GestisciStatoDomanda(domanda);
        PanelDettaglioLavoratore.Visible = false;
    }


    protected void ButtonAccogliDomanda_Click(object sender, EventArgs e)
    {
        CigoTelematicaDomanda domanda = _biz.AccogliDomanda(GetDomandaUpdate());
        GestisciStatoDomanda(domanda);
        PanelDettaglioLavoratore.Visible = false;
    }

    private CigoTelematicaDomanda GetDomandaUpdate()
    {
        return  new CigoTelematicaDomanda
        {
            Id = _idDomanda,
            DataRilascioAutorizzazione = UserControlDatiDomanda.DataRilascioInps,
            NumeroAutorizzazione = UserControlDatiDomanda.NumeroAutorizzazioneInps,
            Note = TextBoxNote.Text,
        };
    }


    protected void RadGridAggiungiLavoratori_OnItemCommand(object sender, GridCommandEventArgs e)
    {
        
        if (e.CommandName == "AddLavoratore")
        {
            String idTipoOre = RadGridAggiungiLavoratori.MasterTableView.Items[e.Item.ItemIndex].GetDataKeyValue("TipoOre").ToString();
            Label LabelCodice = (Label)e.Item.FindControl("LabelCodice");
            Label LabelLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label LabelOreCigo = (Label)e.Item.FindControl("LabelOreCigo");
            RadButton btnAggiungi = (RadButton)e.Item.FindControl("RadButtonAggiungi");
            int idUtente = GestioneUtentiBiz.GetIdentitaUtenteCorrente().IdUtente;

            _biz.InsertLavoratoreInDomanda(int.Parse(LabelCodice.Text), _idDomanda, int.Parse(LabelOreCigo.Text),idTipoOre, idUtente);
            btnAggiungi.Enabled = false;
        }
        
    }

    protected void RadGridLavoratori_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (String.Equals(e.CommandName, "select", StringComparison.CurrentCultureIgnoreCase))
        {
            PanelDettaglioLavoratore.Visible = true;

            int idLavoratore = int.Parse(((Label)e.Item.FindControl("LabelCodice")).Text);
            UserControlDettaglioLavoratore.CaricaDati(_idDomanda, idLavoratore);
            ScrollToEndOfPage();
        }

        else if (String.Equals(e.CommandName, "delete", StringComparison.CurrentCultureIgnoreCase))
        {
            PanelDettaglioLavoratore.Visible = false;
            int idLavoratore = int.Parse(((Label)e.Item.FindControl("LabelCodice")).Text);
            _biz.DeleteLavoratoreInDomanda(idLavoratore,_idDomanda);
            LoadLavoratori();
            foreach (GridItem item in RadGridAggiungiLavoratori.Items)
            {
                if (((Label)item.FindControl("LabelCodice")).Text == idLavoratore.ToString())
                {
                    ((RadButton)item.FindControl("RadButtonAggiungi")).Enabled = true;
                }
            }
        }
    }


    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        TornaAllaLista();
    }


    protected void ButtonCancella_Click(object sender, EventArgs e)
    {

        _biz.DeleteDomanda(_idDomanda);
        TornaAllaLista();
    }
    
    
    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        
        if (!IsValid)
        {
            return;
        }
        
        //richiamo la conferma domanda che salva i dati sul db e cambia lo stato in immessa pervenuta (ho passato i controlli della pagina)
        CigoTelematicaDomanda domanda = _biz.ConfermaDomandaCigo(_idDomanda, UserControlDatiDomanda.DataRilascioInps, UserControlDatiDomanda.NumeroAutorizzazioneInps);

        //disattivo il pannello dei dati domanda
        PanelDatiDomanda.Enabled = false;
        //refresh della pagina
        //Response.Redirect("~/CigoTelematica/DomandaGestione.aspx?id=" + _idDomanda);
        GestisciStatoDomanda(domanda);
        PanelDettaglioLavoratore.Visible = false;
    }


    protected void GestisciValidazioni(CigoTelematicaDomanda domanda)
    {
        if (domanda.IdStato == "I")
        {
            bool result = _biz.OperaiPresentiInDenuncia(domanda.Anno.Value, domanda.Mese.Value, domanda.IdImpresa);
            //il documento inps lo mettiamo non obbligatorio e quindi commentiamo il controllo che segue
            //ValidatorDocumentoInps.Enabled = result;

            UserControlDatiDomanda.AbilitaValidazioneInps(result && DocumentoInpsLista.NumeroDocumenti>0);
            
        }
    
    }


    private void TornaAllaLista()
    {
        if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
        {
            Server.Transfer("~/CigoTelematica/DomandeRicerca.aspx");
            
        }
        else //rimando alla pagina del backoffice 
        {
            Server.Transfer("~/CigoTelematica/DomandeRicercaBackOffice.aspx");
            
        }
    }


    private void DocumentoInpsLista_OnAltroDocumentoSelected()
    {

        PanelDocumentiInps.Visible = true;
        ScrollToEndOfPage();

    }

    private void GestioneDocumentiInps_OnAltriDocumentiReturned(Boolean carica)
    {

        PanelDocumentiInps.Visible = false;

        if (carica)
        {
           
            CigoTelematicaDomanda domandaCigo = _biz.GetDomandaById(_idDomanda);
            DocumentoInpsLista.CaricaAltriDocumenti(_idDomanda, _b.AbilitaControllo(domandaCigo.IdStato));
            GestisciValidazioni(domandaCigo);
        }
    }

    void UserControlDettaglioLavoratore_LavoratoreSaved(object sender, EventArgs e)
    {
        List<LavoratoreDettagli> lavoratori = LoadLavoratori();
        if (_isBackoffice)
        {
            GestisciAbilitazioneAccogli(lavoratori);
        }
    }

    protected void ValidatorDocumentoInps_ServerValidation(object source, ServerValidateEventArgs args)
    {
        args.IsValid = DocumentoInpsLista.NumeroDocumenti > 0;
    }


    private void ScrollToEndOfPage()
    {
        ClientScriptManager cs = Page.ClientScript;
        if (!cs.IsStartupScriptRegistered(this.GetType(), "scrollEnd"))
        {
            String script = "window.scrollTo(0, document.body.scrollHeight);";
            cs.RegisterStartupScript(this.GetType(), "scrollEnd", script, true);
        }
    }

}



 
