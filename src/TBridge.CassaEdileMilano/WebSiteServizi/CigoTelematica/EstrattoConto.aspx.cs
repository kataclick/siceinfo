﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using Cemi.CigoTelematica.Business;
using Cemi.CigoTelematica.Type.Filters;
//using TBridge.Cemi.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Cemi.CigoTelematica.Type.Entities;

public partial class CigoTelematica_EstrattoConto : System.Web.UI.Page
{
    private BusinessEF biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CigoTelematicaEstrattoConto);

        #endregion

        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            trImpresaDescrizioni.Visible = false;
            trImpresaInput.Visible = false;
            RadGridEstrattoContoCigo.Columns[0].Visible = false;
            RadGridEstrattoContoCigo.Columns[1].Visible = false;

        }
        if (!GestioneUtentiBiz.IsConsulente())
        {
            trConsulenti.Visible = false;
        }

       
    }

    protected void RadGridEstrattoContoCigo_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "stampa")
        {
            Int32 annoProtocollo = (Int32)RadGridEstrattoContoCigo.MasterTableView.DataKeyValues[e.Item.ItemIndex]["AnnoProtocolloDomanda"];
            Int32 numeroProtocollo = (Int32)RadGridEstrattoContoCigo.MasterTableView.DataKeyValues[e.Item.ItemIndex]["NumeroProtocolloDomanda"];

            Context.Items["tipoProtocollo"] = RadGridEstrattoContoCigo.MasterTableView.DataKeyValues[e.Item.ItemIndex]["TipoProtocolloDomanda"];
            Context.Items["annoProtocollo"] = annoProtocollo;
            Context.Items["numeroProtocollo"] = numeroProtocollo;

            Server.Transfer("~/CigoTelematica/VisualizzaEstrattoConto.aspx");
        }
    }



    protected void RadButtonOk_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            CaricaEstrattoConto();
        }
        //if (OnFiltroSelected != null)
        //{
        //    EstrattoContoFilter filtro = CreaFiltro();
        //    OnFiltroSelected(filtro);
        //}

        //biz.GetPrestazioneDomandaAssenza()
    }

    private void CaricaEstrattoConto()
    {
        EstrattoContoFilter filter  = CreaFiltro();
        Presenter.CaricaElementiInGridView(RadGridEstrattoContoCigo, biz.GetDomandeLavoratoriEstrattoConto(filter));
    }

    private EstrattoContoFilter CreaFiltro()
    {
        EstrattoContoFilter filtro = new EstrattoContoFilter();

        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                filtro.IdImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
            }
            if (GestioneUtentiBiz.IsImpresa())
            {
                filtro.IdImpresa = ((Impresa)GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdImpresa;
            }
        }
        else
        {
            if (RadNumericTextBoxIdImpresa.Value.HasValue)
            {
                filtro.IdImpresa = (Int32)RadNumericTextBoxIdImpresa.Value.Value;
            }
        }

        filtro.RagioneSocialeImpresa = RadTextBoxRagioneSociale.Text;
        filtro.CodiceFiscaleImpresa = RadTextBoxPIVA.Text;

        if (!String.IsNullOrEmpty(RadTextBoxPeriodoDa.Text))
        {
            filtro.PeriodoDa = DateTime.Parse("01/" + RadTextBoxPeriodoDa.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxPeriodoA.Text))
        {
            filtro.PeriodoA = DateTime.Parse("01/" + RadTextBoxPeriodoA.Text);
        }

        return filtro;
    }




    #region validator
    protected void customValidatorPeriodoA_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoA.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoA.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }

        }
    }

    protected void customValidatorPeriodoDa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoDa.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoDa.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }
    }

    protected void customValidatorSelezionaImpresa(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata() < 0)
        {
            args.IsValid = false;
        }
    }
#endregion



    protected void RadGridEstrattoConto_ItemDataBound(object sender, GridItemEventArgs e)
    {

        if (e.Item is GridDataItem)
        {
            DomandeEstrattoConto domanda = (DomandeEstrattoConto)e.Item.DataItem;
            Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            Label lIndirizzo = (Label)e.Item.FindControl("LabelIndirizzo");
            Label lComune = (Label)e.Item.FindControl("LabelComune");
            //RadGrid rgGiustificativi = (RadGrid)e.Item.FindControl("RadGridGiustificativi");
            //Button bInCarico = (Button) e.Item.FindControl("ButtonInCarico");

            // Periodo)
           
            lPeriodo.Text = String.Format("{0} - {1}", domanda.Mese, domanda.Anno);
            lIndirizzo.Text = domanda.IndirizzoCantiere;
            lComune.Text = String.Format("{0} ({1})", domanda.ComuneCantiere, domanda.ProvinciaCantiere);

        }
    }


 


    protected void RadGridEstrattoConto_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaEstrattoConto();
    }
}