﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EstrattoConto.aspx.cs" Inherits="CigoTelematica_EstrattoConto" MasterPageFile="~/MasterPage.master" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCigoTelematica.ascx" TagName="MenuCigoTelematica" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa" TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="CIGO" sottoTitolo="Ricerca estratto conto" />
    <br />
    <div class="standardDiv">
        <table class="standardTable">
            <tr runat="server" id="trConsulenti">
                <td colspan="3">
                    <uc2:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
                    <asp:CustomValidator ID="customValidator3" runat="server" ValidationGroup="Ricerca" ErrorMessage="Selezionare un'impresa" OnServerValidate="customValidatorSelezionaImpresa">*
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr runat="server" id="trImpresaDescrizioni">
                <td>
                    Codice Impresa
                </td>
                <td>
                    Rag. sociale Impresa
                </td>
                <td>
                    P.IVA/C.F. Impresa
                </td>
            </tr>
            <tr runat="server" id="trImpresaInput">
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresa" runat="server" Type="Number" DataType="System.Int32" MinValue="1">
                        <NumberFormat GroupSeparator="" DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" />
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxPIVA" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Periodo da (MM/AAAA)
                </td>
                <td>
                    Periodo a (MM/AAAA)
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxPeriodoDa" runat="server" MaxLength="7">
                    </telerik:RadTextBox>
                    <asp:CustomValidator ID="customValidatorPeriodoDa" runat="server" ValidationGroup="Ricerca" ControlToValidate="RadTextBoxPeriodoDa" ErrorMessage="Formato data errato" OnServerValidate="customValidatorPeriodoDa_ServerValidate">*
                    </asp:CustomValidator>
                    <%--<asp:CustomValidator ID="customValidatorPeriodoDaMin" runat="server" ValidationGroup="Ricerca" ControlToValidate="RadTextBoxPeriodoDa" ErrorMessage="Data minore di 10/2010" OnServerValidate="customValidatorPeriodoDaMin_ServerValidate">*
                    </asp:CustomValidator>--%>
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxPeriodoA" runat="server" MaxLength="7">
                    </telerik:RadTextBox>
                    <asp:CustomValidator ID="customValidatorPeriodoA" runat="server" ValidationGroup="Ricerca" ControlToValidate="RadTextBoxPeriodoA" ErrorMessage="Formato data errato" OnServerValidate="customValidatorPeriodoA_ServerValidate">*
                    </asp:CustomValidator>
                    <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadTextBoxPeriodoDa" ControlToValidate="RadTextBoxPeriodoA" EnableClientScript="False" ErrorMessage="Limiti periodi errati" Operator="GreaterThanEqual" ValidationGroup="Ricerca" Type="Date">*
                    </asp:CompareValidator>
                    <%-- <asp:CustomValidator ID="customValidatorPeriodoAMin" runat="server" ValidationGroup="Ricerca" ControlToValidate="RadTextBoxPeriodoA" ErrorMessage="Data minore di 10/2010" OnServerValidate="customValidatorPeriodoAMin_ServerValidate">*
                    </asp:CustomValidator>--%>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore" ValidationGroup="Ricerca" />
                </td>
                <td>
                    <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click" ValidationGroup="Ricerca" />
                </td>
            </tr>
        </table>
    </div>
    <div class="standardDiv">
        <telerik:RadGrid ID="RadGridEstrattoContoCigo" runat="server" Width="100%" GridLines="None" AllowPaging="True" PageSize="15" 
        OnItemDataBound="RadGridEstrattoConto_ItemDataBound" Font-Size="Small" 
        OnPageIndexChanged="RadGridEstrattoConto_PageIndexChanged" Visible="true" OnItemCommand="RadGridEstrattoContoCigo_ItemCommand">
            <ExportSettings IgnorePaging="false" OpenInNewWindow="true" ExportOnlyData="false" FileName="EstrattoContoCigo">
                <Pdf PageWidth="297mm" PageHeight="210mm" PageTitle="Estratto Conto CIGO" Title="EstrattoContoCigo" PageHeaderMargin="10mm" PageBottomMargin="10mm" PageLeftMargin="10mm" PageRightMargin="10mm" />
            </ExportSettings>
            <MasterTableView DataKeyNames="AnnoProtocolloDomanda,NumeroProtocolloDomanda,TipoProtocolloDomanda">
                <Columns>
                    <telerik:GridBoundColumn DataField="IdImpresa" FilterControlAltText="Filter numero column"
                    HeaderText="ID Impresa" UniqueName="idImpresa">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RagioneSocialeImpresa" FilterControlAltText="Filter numero column"
                    HeaderText="Ragione Sociale" UniqueName="ragioneSociale" >
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="periodo">
                        <ItemTemplate>
                            <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                     <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Cantiere"
                    UniqueName="cantiere" >
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelIndirizzo" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelComune" runat="server"></asp:Label>
                                </td>
                            </tr>
                           
                        </table>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridTemplateColumn>
                 <telerik:GridBoundColumn DataField="Importo" FilterControlAltText="Filter numero column"
                    HeaderText="Importo riconosciuto" UniqueName="importoRiconosciuto" DataFormatString="{0:C2}" >
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="stampa" FilterControlAltText="Filter column column"
                    ImageUrl="~/images/pdf24.png" UniqueName="column">
                </telerik:GridButtonColumn>
          
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuCigoTelematica ID="MenuCigoTelematica1" runat="server" />
</asp:Content>
