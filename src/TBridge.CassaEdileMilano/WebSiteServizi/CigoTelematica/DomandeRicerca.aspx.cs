﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Business;
using Cemi.CigoTelematica.Type.Filters;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CigoTelematica_DomandeRicerca : System.Web.UI.Page
{
    private BusinessEF _biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {

        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CigoTelematicaGestioneImpresa);
        //funzionalita.Add(FunzionalitaPredefinite.CigoTelematicaGestioneBackOffice);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        if (!GestioneUtentiBiz.IsConsulente())
        {
            trSelezioneImpresa.Visible = false;
        }


        if (!IsPostBack)
        {

            DomandeFilter filter = UserControlFiltroRicercaDomande.CaricaFiltriSalvati();
            if (filter != null)
            {
                LoadDomande(filter);
            }
        }
    }


    protected void customValidatorSelezionaImpresa(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (ConsulenteSelezioneImpresa.GetIdImpresaSelezionata() < 0)
        {
            args.IsValid = false;
        }
    }


    protected void RadGridDomande_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            CigoTelematicaDomanda domanda = (CigoTelematicaDomanda)e.Item.DataItem;
            Label labPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            Label labIndirizzo = (Label)e.Item.FindControl("LabelIndirizzo");
            //NManelli:aggiungo per ottenere lo stato della domanda
            Label labStato = (Label)e.Item.FindControl("LabelStato");
            RadButton btnSeleziona = (RadButton)e.Item.FindControl("RadButtonSelezinona");


            labPeriodo.Text = String.Format("{0}/{1}", domanda.Mese, domanda.Anno);
            labIndirizzo.Text = String.Format("{0} - {1}({2})", domanda.IndirizzoCantiere, domanda.ComuneCantiere, domanda.ProvinciaCantiere);
            //NManelli:aggiungo per ottenere lo stato della domanda
            labStato.Text = String.Format("{0}", domanda.TipoStatoPrestazione.Descrizione);

            btnSeleziona.NavigateUrl = String.Format("DomandaGestione.aspx?id={0}", domanda.Id);

        }
    }

    protected void RadGridDomande_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        LoadDomande();
    }
    protected void RadButtonCerca_Click(object sender, EventArgs e)
    {
        LoadDomande();
    }

    protected void RadButtonConferma_Click(object sender, EventArgs e)
    {
        Page.Validate("validaNuovaRichiesta");
        if (Page.IsValid)
        {
            SaveDomanda();
            
        }
    }

    private void LoadDomande()
    {
        DomandeFilter filter = UserControlFiltroRicercaDomande.GetFilter();
        
        //if (GestioneUtentiBiz.IsImpresa())
        //{
        //    TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaCorrente = (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        //    filter.IdImpresa = impresaCorrente.IdImpresa;
            
        //    Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeImpresa(filter));

        //}

        //if (GestioneUtentiBiz.IsConsulente())
        //{
        //    filter.IdImpresa = ConsulenteSelezioneImpresa.GetIdImpresaSelezionata();

        //    Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeImpresa(filter));

        //}

        LoadDomande(filter);

    }


    private void LoadDomande(DomandeFilter filter)
    {
        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaCorrente = (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            filter.IdImpresa = impresaCorrente.IdImpresa;

            Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeImpresa(filter));

        }

        else if (GestioneUtentiBiz.IsConsulente())
        {
            filter.IdImpresa = ConsulenteSelezioneImpresa.GetIdImpresaSelezionata();

            Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeImpresa(filter));

        }

        else
        {

            Presenter.CaricaElementiInGridView(RadGridDomande, _biz.GetDomandeBackOffice(filter));
        }
    }


    private CigoTelematicaDomanda GetDomanda()
    {
        CigoTelematicaDomanda domanda = UserControlDatiDomanda1.GetDomanda();

        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaCorrente = (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            domanda.IdImpresa = impresaCorrente.IdImpresa;
            domanda.IdUtenteInserimento = impresaCorrente.IdUtente;
            domanda.IdStato = "I";  //immessa      
            domanda.DataInserimentoRecord = DateTime.Now;
            domanda.DataDomanda = DateTime.Now.Date;

        }                                
        
        return domanda;
    }

    private void SaveDomanda()
    {
        CigoTelematicaDomanda domanda = GetDomanda();
        _biz.InsertNuovaDomanda(domanda);

        ScriptManager.RegisterStartupScript(this, typeof(CigoTelematica_DomandeRicerca), "CloseScript", String.Format("CloseRadWindowAndGotoGestione('{0}')", domanda.Id), true);
       // Page.ClientScript.RegisterClientScriptBlock(GetType(), "CloseScript", String.Format("CloseRadWindowAndGotoGestione('{0}')", domanda.Id), true);
    }
}