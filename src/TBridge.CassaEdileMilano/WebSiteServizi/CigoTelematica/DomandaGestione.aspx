﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DomandaGestione.aspx.cs" Theme="CETheme2009Wide" Inherits="CigoTelematica_DomandaGestione" MasterPageFile="~/MasterPage.master" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCigoTelematica.ascx" TagName="MenuCigoTelematica" TagPrefix="uc3" %>
<%@ Register Src="WebControls/UserControlDatiDomanda.ascx"  TagName="UserControlDatiDomanda" TagPrefix="uc4" %>
<%@ Register Src="WebControls/UserControlDettaglioLavoratore.ascx"  TagName="UserControlDettaglioLavoratore" TagPrefix="uc4" %>
<%@ Register Src="WebControls/UserControlDocumentoInps.ascx" TagName="GestioneDocumentiInps" TagPrefix="uc5" %>
<%@ Register Src="WebControls/UserControlDocumentoInpsLista.ascx" TagName="DocumentoInpsLista" TagPrefix="uc6" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
 <style type="text/css">
         .RadWindow .rwIcon { 
            height: 0!important; 
            width: 0!important; 
            } 
 </style>
<script type="text/javascript">
    function ShowRadWindowAggiungiLavoratore() {
        var oWnd = $find('<% =RadWindowAggiungiLavoratore.ClientID %>');
        oWnd.show();
    }


    function OnClientClose(oWnd, args) {
            window.__doPostBack('ObjAux', '@@@@@Rebind@@@@@');
    }


    function OnConfermaDomandaClick() {

        if (!CheckLavoratori()) {
            if (!confirm("Non sono stati inseriti lavoratori in domanda. Continuare ?")) {
                return false;
            }
        }
        
        if (!checkDocumentoInpsPresente()) {
            if (!confirm("Documento INPS mancante. Continuare ?")) {
                return false;
            }
        }

        var confirmed = confirm("La domanda sta per essere inviata a Cassa Edile e non potrà più essere modificata. Continuare ?");
        if (confirmed) {
            return Page_ClientValidate('validaConferma');
        }
        else {
            return confirm; 
        }
    }


    function CheckLavoratori() {
        var grid = $find("<%= RadGridLavoratori.ClientID %>");
        return grid.get_masterTableView().get_dataItems().length > 0;

    }
    

</script>




    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="CIGO" sottoTitolo="Gestione Domanda" />
    <br />

    <%--<telerik:RadWindow ID="RadWindowAggiungiLavoratore" runat="server" Width="880px" Height="660px"  AutoSize="false" Behaviors="Close" OnClientClose="OnClientClose">--%>
    <telerik:RadWindow ID="RadWindowAggiungiLavoratore" runat="server" AutoSizeBehaviors="Height" Width="750" Height="700" Modal="true"  Behaviors="Close" OnClientClose="OnClientClose">
        <ContentTemplate>
           <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                <div style=" width:670px; margin-left:auto; margin-right:auto; margin-top:10px; margin-bottom:10px">
                    <h3>
                        Ricerca per Impresa - Aggiunta lavoratore 
                    </h3>
                        Denuncia del <asp:Label ID="LabelDenunciaDel" runat="server" Font-Bold="true" />
                        <br />
                        <br />
                        <span style=" font-weight:bold; color:#B20000";>
                            (NOTA: dopo aver aggiunto i lavoratori in domanda, premere la X in alto a destra per chiudere questa finestra) <br /><br />
                        </span>
                    <span style=" font-weight:bold">
                        Lavoratori presenti in denuncia con ore CIGO
                    </span>
                    <telerik:RadGrid ID="RadGridAggiungiLavoratori" runat="server" Width="100%" GridLines="None" AllowPaging="True" PageSize="15"
                OnItemDataBound="RadGridAggiungiLavoratori_ItemDataBound" Font-Size="Small"  OnPageIndexChanged="RadGridAggiungiLavoratori_PageIndexChanged" Visible="true" 
                OnItemCommand="RadGridAggiungiLavoratori_OnItemCommand">
                <MasterTableView DataKeyNames="TipoOre">
                    <Columns>
                        
                        <telerik:GridTemplateColumn HeaderText="Codice" UniqueName="Codice" >
                        <ItemTemplate>
                            <asp:Label ID="LabelCodice" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore" >
                        <ItemTemplate>
                            <asp:Label ID="LabelLavoratore" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Ore Cigo" UniqueName="OreCigo" >
                        <ItemTemplate>
                            <asp:Label ID="LabelOreCigo" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Tipo Ore Cigo" UniqueName="TipoOreCigoAggiungi" >
                        <ItemTemplate>
                            <asp:Label ID="LabelTipoOreCigoAggiungi" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="" UniqueName="Aggiungi" >
                            <ItemTemplate>
                                <telerik:RadButton ID="RadButtonAggiungi" runat="server" Text="Aggiungi" CommandName="AddLavoratore" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

                </div>
           </telerik:RadAjaxPanel>
        </ContentTemplate>
    </telerik:RadWindow>

    
    <Asp:Button ID="RadButtonTornaRicerca" runat="server" Text="Torna alla Ricerca" Width="150px"  OnClick="ButtonIndietro_Click"/>
            


    <table style="margin-top:5px; margin-bottom: 20px; width:100%" class="borderedTable">
        <tr>
            <td colspan="5"  style="color:#B20000";>
                <b>Domanda Selezionata:</b>
            </td>
        </tr>

        
        <tr style="height:10px;">
            <td>
            </td> 
        </tr>

   
        <tr >
            <td colspan="5" >
            <asp:Panel ID="PanelDatiDomanda" runat="server" Visible="true">
                <uc4:UserControlDatiDomanda ID="UserControlDatiDomanda" runat ="server" />
            </asp:Panel>    
            </td>        
                              
        </tr>

      <%--  <tr>
            <td style="width: 50px">
                <asp:Label ID="LabelNote" runat="server" Text="Note BackOffice:"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="TextBoxNote" runat="server" TextMode="MultiLine" MaxLength="1000"
                    Height="40px" Width="100%"></asp:TextBox>
            </td>
            
        </tr>
       
        <tr>
            <td>
            </td>
            <td>
                <Asp:Button ID="ButtonSalvaNote" runat="server" Text="Salva Note" Width="150px" OnClick="ButtonSalvaNote_Click" />
            </td>
        </tr>--%>
        

        <tr>
            <td colspan="5">
                <uc6:DocumentoInpsLista ID="DocumentoInpsLista" runat="server" />
                <asp:CustomValidator ID="ValidatorDocumentoInps" runat="server" 
                        ValidationGroup = "validaConferma" 
                        OnServerValidate="ValidatorDocumentoInps_ServerValidation" 
                        ErrorMessage="Documento INPS mancante" Display="None" Enabled="false"></asp:CustomValidator>
            </td>     
        </tr>

        <tr>
            <td colspan="5">
                <table style="width:100%">
                    <tr>
                        <td style="width: 110px">
                            <asp:Label ID="LabelNote" runat="server" Text="Note BackOffice:"></asp:Label>
                        </td>
                        <td style="padding-right:10px" >
                            <asp:TextBox ID="TextBoxNote" runat="server"   TextMode="MultiLine" MaxLength="1000"
                                Height="40px" Width="100%"></asp:TextBox>
                        </td>
            
                    </tr>
       
                    <tr>
                        <td>
                        </td>
                        <td>
                            <Asp:Button ID="ButtonSalvaNote" runat="server" Text="Salva Note" Width="150px" OnClick="ButtonSalvaNote_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="height:25px">

            </td>
        </tr>

        <tr style="height:25px; " align="center" valign="bottom" id="RowImpresa" runat="server">
            
               
            <td>
                <Asp:Button ID="RadButtonConferma" runat="server" Text="Conferma Richiesta" Width="150px" ValidationGroup="validaConferma" OnClientClick="return OnConfermaDomandaClick()" OnClick="ButtonConferma_Click" />
            </td>
        
            <td>
                <Asp:Button ID="RadButtonCancella" runat="server" Text="Cancella Richiesta" Width="150px" OnClick="ButtonCancella_Click"/>
            </td>          
         
            
        </tr>

        <tr style="height:25px; " align="center" valign="bottom" id="RowBackOffice" runat="server">

            <td>
                <Asp:Button ID="ButtonAccogli" runat="server" Text="Accogli Domanda" Width="150px"  OnClick="ButtonAccogliDomanda_Click"/>
            </td>

            <td>
                <Asp:Button ID="ButtonRespingi" runat="server" Text="Respingi Domanda" Width="150px"  OnClick="ButtonRespingiDomanda_Click"/>
            </td>

            <td>
                <Asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla Domanda" Width="150px" OnClick="ButtonAnnullaDomanda_Click"/>
            </td>
            
            <td>
                <Asp:Button ID="ButtonInAttesa" runat="server" Text="In Attesa" Width="150px" OnClick="ButtonInAttesa_Click" />
            </td>

            <td>
                <Asp:Button ID="ButtonImmessaPervenuta" runat="server" 
                    Text="Torna a Imm. Pervenuta" Width="155px"  
                    OnClick="ButtonImmessaPervenuta_Click"/>
            </td>     
                                                
        </tr>

     
    <tr>
        <td colspan="5">
            <asp:ValidationSummary ID = "ValidationConfermaDomanda" runat = "server" ValidationGroup="validaConferma" ForeColor="Red" />
        </td>
    </tr>
            
    </table>



    <table width="100%">
        <tr>
            <td>
                <b>Lavoratori nella domanda</b>
            </td>
            <td align="right">
                <asp:Button ID="RadButtonAggiungiLavoratore" runat="server" Text="Aggiungi Lavoratore" OnClientClick="ShowRadWindowAggiungiLavoratore(); return false;" >
                </asp:Button>
            </td>
        </tr>
    </table>


    <div class="standardDiv" >
         <telerik:RadGrid ID="RadGridLavoratori" runat="server" Width="100%" GridLines="None" AllowPaging="True" PageSize="15" 
                OnItemDataBound="RadGridLavoratori_ItemDataBound" Font-Size="Small"  OnPageIndexChanged="RadGridLavoratori_PageIndexChanged" 
                Visible="true" OnItemCommand="RadGridLavoratori_ItemCommand">
                <MasterTableView>
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Stato" UniqueName="Stato" >
                        <ItemTemplate>
                            <asp:Label ID="LabelStato" runat="server" />
                            <asp:TextBox ID="TextboxStatoHidden" runat="server" Style="display:none" />
                            <asp:RangeValidator ID="ValidatorStatoLavoratore" runat="server" ControlToValidate="TextboxStatoHidden"  
                                    ValidationGroup = "validaConferma" MaximumValue="C" MinimumValue="C" Display="None"
                                    ></asp:RangeValidator>
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Codice" UniqueName="Codice" >
                        <ItemTemplate>
                            <asp:Label ID="LabelCodice" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore" >
                        <ItemTemplate>
                            <asp:Label ID="LabelLavoratore" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Ore Cigo" UniqueName="OreCigo" >
                        <ItemTemplate>
                            <asp:Label ID="LabelOreCigo" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Tipo Ore Cigo" UniqueName="TipoOreCigo" >
                        <ItemTemplate>
                            <asp:Label ID="LabelTipoOreCigo" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="" UniqueName="Seleziona" >
                            <ItemTemplate>
                                <telerik:RadButton ID="RadButtonSeleziona" runat="server" Text="Seleziona" CommandName="select"  />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="" UniqueName="Elimina" >
                            <ItemTemplate>
                                <telerik:RadButton ID="RadButtonElimina" runat="server" Text="Elimina" CommandName="delete"  />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>   
                    </Columns>
                </MasterTableView>
        </telerik:RadGrid>
    </div>

    <asp:panel ID="PanelDettaglioLavoratore" runat="server" Visible="False"> 
        <%--<table class="borderedTable" style=" margin-top:50px; padding:10px">
        <tr>
            <td> --%>
            <uc4:UserControlDettaglioLavoratore ID="UserControlDettaglioLavoratore" runat="server" />   
   <%--                 </td>
        </tr>
    </table>        --%>
    </asp:panel>


</asp:Content>


<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
      <uc3:MenuCigoTelematica ID="MenuCigoTelematica" runat="server" />  
</asp:Content>

     
 <asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage2">
        <asp:Panel ID="PanelDocumentiInps" runat="server" Visible="False">
            <uc5:GestioneDocumentiInps ID="GestioneDocumentiInps" runat="server" />
        </asp:Panel>
 </asp:Content>

