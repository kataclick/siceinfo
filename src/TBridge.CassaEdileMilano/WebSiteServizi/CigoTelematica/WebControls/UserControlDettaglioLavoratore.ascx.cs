﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Cemi.CigoTelematica.Type.Entities;
using Cemi.CigoTelematica.Type.Collections;

public partial class CigoTelematica_WebControls_UserControlDettaglioLavoratore : System.Web.UI.UserControl
{
    private BusinessEF _biz = new BusinessEF();
    private Business _b = new Business();
    private bool _isBackOffice;

    public event EventHandler LavoratoreSaved;


    protected void Page_Load(object sender, EventArgs e)
    {

        BustaPagaLista.OnBustaPagaSelected += BustaPagaLista_OnBustaPagaSelected;

        GestioneBustaPaga.OnDocumentoBustaPagaReturned += GestioneBustaPaga_OnBustaPagaReturned;

        _isBackOffice = !(GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente());

         if (!IsPostBack)
        {
            if (!_isBackOffice)
            {
                PanelControllilavoratore.Visible = false;
            }
            else
            {
                PanelControllilavoratore.Visible = true;
            }
  
        }

    }

    public void CaricaDati(int idDomanda, int idLavoratore)
    {
        Boolean eseguiControlli;

        if (!_isBackOffice)
        {
            eseguiControlli = false;
        }
        else 
        {
            eseguiControlli = true;
        }

        LavoratoreDettagli lavoratore =  _biz.GetLavoratoreInDomanda(idDomanda, idLavoratore,eseguiControlli);
        //LavoratoreDettagli lavoratore = _biz.GetLavoratoreInDomanda(idDomanda, idLavoratore);
        //RapportoImpresaPersona rapporto = lavoratore.Lavoratore.RapportiImpresaPersona.First();

        CigoTelematicaDomanda domanda = _biz.GetDomandaById(idDomanda);
        Boolean abilita = _b.AbilitaControllo(domanda.IdStato);

                
        if (eseguiControlli)
        {
            UserControlControlliLavoratore.AggiornaControlliLavoratore(lavoratore.ControlliValidita);
        }

        GestioneBustaPaga.CaricaDomanda(domanda, idLavoratore);
        BustaPagaLista.CaricaAltriDocumenti(idDomanda, idLavoratore, abilita);


        
        //Nmanelli - aggiungo i dati del lavoratore:
        LabelStatoLavoratore.Text = lavoratore.Stato;
        LabelLavoratore.Text = String.Format("{0} {1}", lavoratore.Nome, lavoratore.Cognome);
        LabelCodice.Text = lavoratore.IdLavoratore.ToString();


        LabelQualifica.Text = String.IsNullOrEmpty(lavoratore.Categoria) ? "N.D" : lavoratore.Categoria;

        //Paga oraria: impostato con la paga oraria in denuncia
        LabelPagaOraria.Text = lavoratore.PagaOrariaDenuncia.HasValue ? lavoratore.PagaOrariaDenuncia.Value.ToString() : "";

        if (!lavoratore.PercentualePT.HasValue)
        {
            LabelPercentualeParTime.Text = "N.D.";
        }
        else if (lavoratore.PercentualePT.Value == 0) 
        {
            LabelPercentualeParTime.Text = "Tempo pieno";
        }
        else
        {
            LabelPercentualeParTime.Text = lavoratore.PercentualePT.ToString();
        }

        //paga oraria dichiarata preimpostata con quella in denuncia ma modificabile
        TextBoxPagaOrariaDich.Text = lavoratore.PagaOrariaDichiarata.HasValue ? lavoratore.PagaOrariaDichiarata.Value.ToString() : lavoratore.PagaOrariaDenuncia.Value.ToString();       
        LabelOreCigoInDenuncia.Text = lavoratore.OreRichieste.HasValue ? lavoratore.OreRichieste.Value.ToString() : "0";
        LabelNumeroGiornateIntere.Text = CalcolaGiorniCigo(lavoratore.PercentualePT, lavoratore.OreRichieste.Value).ToString();
        UserControlGiorniCigo.CaricaGiorniCigo(lavoratore.IdLavoratore, lavoratore.IdDomanda, lavoratore.PercentualePT);
        ViewState["idLavoratore"] = idLavoratore;
        ViewState["idDomanda"] = idDomanda;
        ViewState["idStatoLavoratore"] = lavoratore.IdStato;
        //customValidatorGiorniCigo.ErrorMessage = String.Format("Si possono slezionare al massimo {0} giorni", LabelNumeroGiornateIntere.Text);

      //  GestisciStatoLavoratore(lavoratore.IdStato);
        GestisciStati(domanda.IdStato, _isBackOffice, lavoratore.IdStato);        
    }

    public void GestisciStati(string idStatoDomanda, bool isBackOffice, string idStatoLavoratore)
    {
       
        //se la domanda è nello stato diverso da Immessa disattivo la paga oraria, le caselline dei giorni di cigo e i bottoni
        //salva e passa al successivo/precedente

        if (!_isBackOffice)
        {
            Boolean enabled = idStatoDomanda == "I";
            ButtonRifiutaLavoratore.Visible = false;
            ButtonApprovaLavoratore.Visible = false;
            ButtonRitornaInCompletato.Visible = false;

            TextBoxPagaOrariaDich.Enabled = 
            PanelGiorniCigo.Enabled = 
            SalvaEPassaPrecedente.Enabled = enabled;

            BustaPagaLista.CaricaAltriDocumenti((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], enabled);

        }
        else 
        {
  
            if (idStatoDomanda == "S" || idStatoDomanda == "T" || idStatoDomanda == "O" )
            {
                GestisciStatoLavoratore(idStatoLavoratore);
            }
            else if (idStatoDomanda == "I")
            {
                TextBoxPagaOrariaDich.Enabled = true;
                PanelGiorniCigo.Enabled = true;
                //ButtonSalvaEPassaSuccessivo.Enabled = false;
                SalvaEPassaPrecedente.Enabled = true;
                ButtonRitornaInCompletato.Enabled = ButtonRifiutaLavoratore.Enabled = ButtonApprovaLavoratore.Enabled = false;
                BustaPagaLista.CaricaAltriDocumenti((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], true);
            }
            else
            {
                TextBoxPagaOrariaDich.Enabled = false;
                PanelGiorniCigo.Enabled = false;
                //ButtonSalvaEPassaSuccessivo.Enabled = false;
                SalvaEPassaPrecedente.Enabled = false;
                ButtonRitornaInCompletato.Enabled = ButtonRifiutaLavoratore.Enabled = ButtonApprovaLavoratore.Enabled = false;
                BustaPagaLista.CaricaAltriDocumenti((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], false);
            }
            // sospesa, immessa pervenuta, in attesa
            //ButtonRitornaInCompletato.Enabled = ButtonRifiutaLavoratore.Enabled = ButtonApprovaLavoratore.Enabled = (idStato == "S" || idStato == "T" || idStato == "O");

        }

    }

    private void GestisciStatoLavoratore(string idStatoLavoratore)
    {
        switch (idStatoLavoratore)
        {
            case "A":
            case "R":
                TextBoxPagaOrariaDich.Enabled = false;
                PanelGiorniCigo.Enabled = false;
                //ButtonSalvaEPassaSuccessivo.Enabled = false;
                SalvaEPassaPrecedente.Enabled = false;
                ButtonRifiutaLavoratore.Enabled = false;
                ButtonApprovaLavoratore.Enabled = false;
                
                ButtonRitornaInCompletato.Enabled = true;
                BustaPagaLista.CaricaAltriDocumenti((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], false);
                break;
                
            case "C":
            case "L":
                TextBoxPagaOrariaDich.Enabled = true;
                PanelGiorniCigo.Enabled = true;
                //ButtonSalvaEPassaSuccessivo.Enabled = false;
                SalvaEPassaPrecedente.Enabled = true;
                ButtonRifiutaLavoratore.Enabled = true;
                ButtonApprovaLavoratore.Enabled = true;
                ButtonRitornaInCompletato.Enabled = false;
                BustaPagaLista.CaricaAltriDocumenti((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], true);
                break;
            default:
                break;
        }

    }


    public void SalvaDati(string idStatoLavoratoreNew)
    {
        int idUtente = GestioneUtentiBiz.GetIdentitaUtenteCorrente().IdUtente;
        decimal paga = 0;
        decimal.TryParse(TextBoxPagaOrariaDich.Text.Replace('.',','), out paga);
        String idStatoLavoratoreAttuale = ViewState["idStatoLavoratore"].ToString();

        GiornoCigoCollection giorni = UserControlGiorniCigo.GetGiorniCigo();
        int giorniSelezionati = giorni.Count(x => x.Selected);

        string oreRic = UserControlGiorniCigo.OreRiconosciute((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"]);
        
        CigoTelematicaLavoratore lavoratore;

        if (String.IsNullOrEmpty(idStatoLavoratoreNew))
        {
            if (idStatoLavoratoreAttuale == "C" || idStatoLavoratoreAttuale == "L")
            {
                //NM (02/10/2014): se il documento busta paga può essere uno solo, scommentare la riga che segue e commentare la successiva
                //idStatoLavoratoreNew = (paga > 0 && giorniSelezionati >= int.Parse(LabelNumeroGiornateIntere.Text) && BustaPagaLista.NumeroDocumenti == 1) ? "C" : "L";
                //NM (23/10/2014): i giorni cigo segnati possono essere anche minori di quelli in denuncia perchè magari inserisce due domande per cantieri diversi
                //                 modifico la condizione sui giorniselezionati mettendo > 0 la riga che segue era quella con il controllo originale
                //idStatoLavoratoreNew = (paga > 0 && giorniSelezionati >= int.Parse(LabelNumeroGiornateIntere.Text) && BustaPagaLista.NumeroDocumenti >= 1) ? "C" : "L";
                idStatoLavoratoreNew = (paga > 0 && giorniSelezionati > 0 && BustaPagaLista.NumeroDocumenti >= 1) ? "C" : "L";
        
            }
            else
            {
                idStatoLavoratoreNew = idStatoLavoratoreAttuale;
            }
        }

        lavoratore = _biz.UpdateLavoratoreCigo((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], giorni, idUtente, paga, idStatoLavoratoreNew,oreRic);

        LabelStatoLavoratore.Text = lavoratore.CigoTelematicaStatoLavoratore.Descrizione;
        ViewState["idStatoLavoratore"] = idStatoLavoratoreNew;

        GestisciStati(lavoratore.CigoTelematicaDomanda.IdStato, _isBackOffice, idStatoLavoratoreNew);
        //GestisciStatoLavoratore(idStatoLavoratoreNew);

        if (LavoratoreSaved != null)
        {
            LavoratoreSaved(this, EventArgs.Empty);
        }

        if (_isBackOffice)
        {
            LavoratoreDettagli lavoratoredettagli = _biz.GetLavoratoreInDomanda((int)ViewState["idDomanda"], (int)ViewState["idLavoratore"], true);
            UserControlControlliLavoratore.AggiornaControlliLavoratore(lavoratoredettagli.ControlliValidita);
        }
    }


    private int CalcolaGiorniCigo(int? percPartTime, int oreGigo)
    {
        return percPartTime.HasValue ? (int)(percPartTime == 0 ? (oreGigo / 8) : (oreGigo / (8 * percPartTime / 100))) : 0;
    }

    
   
    public void SalvaEPassaPrecedente_Click(object sender, EventArgs e)
    {
        SalvaDati(null);
    }


    

    private void BustaPagaLista_OnBustaPagaSelected()
    {

        PanelBustaPaga.Visible = true;
        ScrollToEndOfPage();

    }

    private void GestioneBustaPaga_OnBustaPagaReturned(Boolean carica)
    {

        PanelBustaPaga.Visible = false;

        if (carica)
        {
            Int32 idDomandaCigo = (Int32)ViewState["idDomanda"];
            Int32 idLavoratore = (Int32)ViewState["idLavoratore"];
            //CigoTelematicaDomanda domandaCigo = _biz.GetDomandaById(idDomandaCigo);
            BustaPagaLista.CaricaAltriDocumenti(idDomandaCigo, idLavoratore, true);
        }
    }

    protected void ButtonApprovaLavoratore_Click(object sender, EventArgs e)
    {
        SalvaDati("A");
    }


    protected void ButtonRifiutaLavoratore_Click(object sender, EventArgs e)
    {
        SalvaDati("R");
    }

    protected void ButtonRitornaCompletato_Click(object sender, EventArgs e)
    {
        SalvaDati("C");
    }

    private void ScrollToEndOfPage()
    {
        ClientScriptManager cs = Page.ClientScript;
        if (!cs.IsStartupScriptRegistered(this.GetType(), "scrollEndBustaPaga"))
        {
            String script = "window.scrollTo(0, document.body.scrollHeight);";
            cs.RegisterStartupScript(this.GetType(), "scrollEndBustaPaga", script, true);
        }
    }

}