﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Business;
using Cemi.CigoTelematica.Type.Delegates;
using Cemi.CigoTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;

public partial class CigoTelematica_WebControls_UserControlBustaPaga : System.Web.UI.UserControl
{

    public event DocumentoBustaPagaReturnedEventHandler OnDocumentoBustaPagaReturned;

    private BusinessEF bizEF = new BusinessEF();
    private Business biz = new Business();

   
    protected void Page_Load(object sender, EventArgs e)
    {
        
               
    }


    public void CaricaDomanda(CigoTelematicaDomanda domanda, int idLavoratore)
    {
        ViewState["IdDomandaCigo"] = domanda.Id;
       // ViewState["IdImpresa"] = domanda.IdImpresa;
        ViewState["IdLavoratore"] = idLavoratore;
        RadButtonConferma.Enabled = biz.AbilitaControllo(domanda.IdStato);
    }



    private void Reset(Boolean carica)
    {
        RadTextBoxNome.Text = "";


        if (OnDocumentoBustaPagaReturned != null)
        {
            OnDocumentoBustaPagaReturned(carica);
        }
    }


    protected void RadButtonConferma_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            
                        
            CigoTelematicaBustaPaga bustaPaga = CreaAltroDocumento();

            //Inserimento
            try
            {
                bizEF.InsertBustaPaga(bustaPaga);

                Reset(true);
            }
            catch
            {
                ErroreNellInserimento();
            }

        }

    }

    private CigoTelematicaBustaPaga CreaAltroDocumento(object p)
    {
        throw new NotImplementedException();
    }



    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento del documento.";
    }


    //private CigoTelematicaBustaPaga CreaAltroDocumento(int idDomanda, int idLavoratore)
    private CigoTelematicaBustaPaga CreaAltroDocumento()
    {
        CigoTelematicaBustaPaga busta = new CigoTelematicaBustaPaga();

        busta.DataInserimentoRecord = DateTime.Now;
        busta.IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente();

        
        busta.Descrizione = RadTextBoxNome.Text;

        if (FileUploadBustaPaga.HasFile)
        {
            using (Stream sr = FileUploadBustaPaga.PostedFile.InputStream)
            {
                byte[] img = new byte[sr.Length];
                sr.Read(img, 0, (int)sr.Length);
                sr.Seek(0, SeekOrigin.Begin);
                busta.Immagine = img;
                busta.NomeFile = FileUploadBustaPaga.FileName;
            }
        }

        
        //busta.idDomandaCigo = idDomanda;
        busta.IdDomandaCigo = (Int32)ViewState["IdDomandaCigo"];
        busta.IdLavoratore = (Int32)ViewState["IdLavoratore"];
        //busta.idLavoratore = 710887; // da sistemare !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                
    
        return busta;

    }

    protected void RadButtonIndietro_Click(object sender, EventArgs e)
    {
        Reset(false);
    }




    public int idLavoratore { get; set; }
}