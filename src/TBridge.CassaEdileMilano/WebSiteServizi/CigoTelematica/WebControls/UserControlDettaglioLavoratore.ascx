﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlDettaglioLavoratore.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlDettaglioLavoratore" %>
<%@ Register Src="UserControlGiorniCigo.ascx"  TagName="UserControlGiorniCigo" TagPrefix="uc1" %>
<%@ Register Src="UserControlBustaPagaLista.ascx" TagName="BustaPagaLista" TagPrefix="uc2" %>
<%@ Register Src="UserControlBustaPaga.ascx" TagName="GestioneBustaPaga" TagPrefix="uc3" %>
<%@ Register Src="UserControlControlliLavoratore.ascx" TagName="UcControlliLavoratore" TagPrefix="uc4" %>
<script type="text/javascript">
    $ = $telerik.$

    function validateGiorniCigo(source, arguments) {
        var nGiorniSelezionati = $('input[id*="CheckBoxCigo"][checked]').length;
        var nGiorniDenuncia = parseInt($("#"+"<%= LabelNumeroGiornateIntere.ClientID %>").text());
        if (nGiorniSelezionati > nGiorniDenuncia) {
            arguments.IsValid = false;
        }
        else {
            arguments.IsValid = true;
        }
    }
</script>
<%--
    <table >
        <tr style="height:25px;">
            <td width="100">
            </td> 
        </tr>

    </table>--%>

<div style="border:solid; border-width:1px; margin-top: 25px">
 
    <table style="width:100%">
        
        <tr style="height:35px; color:#B20000;">
            <td width="200">
                <b>Lavoratore selezionato:</b>
            </td>

        </tr>
       
       <tr>
            <td colspan="5">
                <asp:Panel ID="PanelControllilavoratore" runat="server" Visible="true">
                    <uc4:UcControlliLavoratore ID="UserControlControlliLavoratore" runat="server" />
                </asp:Panel>
            </td>
            
        </tr>

        <tr style="height:25px;">
            <td width="140">
                Stato Lavoratore:
            </td>
            <td>
                <b><asp:Label ID="LabelStatoLavoratore" runat="server"></asp:Label></b>
            </td>
        </tr>

        <tr style="height:25px;">
            <td width="140">
                Lavoratore:
            </td>
            <td>
                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
            </td>
            
            <td width="100">
            
            </td>

            <td>
                Codice:
            </td>
            <td>
                <asp:Label ID="LabelCodice" runat="server"></asp:Label>
            </td>
        </tr>

<%--        <tr style="height:25px;">
           
        </tr>--%>

        <tr style="height:25px;">
            <td width="140">
                Qualifica:
            </td>
            <td>
                <asp:Label ID="LabelQualifica" runat="server"></asp:Label>
            </td>

            <td width="100">
            
            </td>

            <td>
                Percentuale Part-Time:
            </td>
            <td>
                <asp:Label ID="LabelPercentualeParTime" runat="server"></asp:Label>
            </td>

        </tr>

<%--        <tr style="height:25px;">
            
        </tr>--%>

<%--        <tr style="height:25px;">
            
        </tr>--%>
        <tr style="height:25px;">
            <td>
                Paga Oraria:
            </td>
            <td>
                <asp:Label ID="LabelPagaOraria" runat="server"></asp:Label>
            </td>
            <td width="100">
            
            </td>
            <td>
                Paga Oraria Dichiarata:
            </td>
            <td>
                <telerik:RadTextBox ID="TextBoxPagaOrariaDich" runat="server"> </telerik:RadTextBox>
                <asp:RegularExpressionValidator ID="regExValidatorPagaOraria" runat="server" 
                    ControlToValidate="TextBoxPagaOrariaDich" 
                    ValidationExpression="^\d+(\,\d{1,4})?$"
                    ErrorMessage="Paga oraria dichiarata non valida." 
                    ValidationGroup="salva">
                *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr style="height:25px;">

            <td>
                Ore Cigo in Denuncia:
            </td>
            <td>
                <asp:Label ID="LabelOreCigoInDenuncia" runat="server"></asp:Label>
            </td>

            <td width="100">
            
            </td>

            <td>
                N° Giornate intere:
            </td>
            <td>
                <asp:Label ID="LabelNumeroGiornateIntere" runat="server"></asp:Label>
            </td>
        </tr>

    </table>

    <table>
        <tr>
            <td colspan="3">
                <uc2:BustaPagaLista ID="BustaPagaLista" runat="server" />
            </td>
        </tr>
    </table>

    <asp:Panel ID="PanelGiorniCigo" runat="server" Visible="true">
        <uc1:UserControlGiorniCigo ID="UserControlGiorniCigo" runat="server"/>
    </asp:Panel>
    <%--<asp:CustomValidator ID="customValidatorGiorniCigo" runat="server"
                 ClientValidationFunction="validateGiorniCigo"
                 ValidationGroup="salva">
    </asp:CustomValidator>--%>
    
      

    <table>
        <tr style="height:25px;">
            <td>
                <asp:Button ID="SalvaEPassaPrecedente" runat="server" 
                    Text="Salva Lavoratore" Width="200px" 
                    OnClick="SalvaEPassaPrecedente_Click" ValidationGroup="salva"/>
            </td>
            <td>
                <asp:Button ID ="ButtonApprovaLavoratore" runat="server"  
                    Text="Approva Lavoratore" Width="200px" OnClick="ButtonApprovaLavoratore_Click" 
                    Visible="False" />
            </td>
            <td>
                <asp:Button ID ="ButtonRifiutaLavoratore" runat="server"  Text="Rifiuta Lavoratore" Width="200px" OnClick="ButtonRifiutaLavoratore_Click" />
            </td>
            <td>
                <asp:Button ID ="ButtonRitornaInCompletato" runat="server"  Text="Ritorna in Completato" Width="200px" OnClick="ButtonRitornaCompletato_Click" />
            </td>
            <%--<td>
                <asp:Button ID="ButtonSalvaEPassaSuccessivo" runat="server" Text="Salva e passa al successivo" Width="200px" ValidationGroup="salva"/>
            </td>--%>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ValidationSummary ID="ValidatorSummarySalva" runat="server" ValidationGroup="salva" ForeColor="Red" />
            </td>
        </tr>
    </table>

        

    <%--<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage2">--%>
        <asp:Panel ID="PanelBustaPaga" runat="server" Visible="False">
            <uc3:GestioneBustaPaga ID="GestioneBustaPaga" runat="server" />
        </asp:Panel>
        
    <%--</asp:Content>--%>


</div>