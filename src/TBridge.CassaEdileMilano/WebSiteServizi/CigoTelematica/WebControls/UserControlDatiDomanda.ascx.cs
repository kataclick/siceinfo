﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class CigoTelematica_WebControls_UserControlDatiDomanda : System.Web.UI.UserControl
{

    private Boolean _isBackoffice;

    public DateTime? DataRilascioInps 
    {
        
        //get { return (RadDatePickerDataRilascio.SelectedDate.Value) ; }
        //get { return (RadDatePickerDataRilascio.SelectedDate.HasValue ? RadDatePickerDataRilascio.SelectedDate.Value : Convert.ToDateTime(null)); }
        get { return (RadDatePickerDataRilascio.SelectedDate.HasValue ? RadDatePickerDataRilascio.SelectedDate.Value : (DateTime?)null); }
    }


    public string NumeroAutorizzazioneInps
    {
        //get { return RadTextBoxNumeroAutorizzazione.Text; }
        get { return (RadTextBoxNumeroAutorizzazione.Text); }
    }


    public string StatoDomanda
    {
        get { return LabelStatoDomanda.Text; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        // se lo stato è diverso da immessa rendo non editabli numero autorizzazione e data rilascio inps
        //if (StatoDomanda != "Immessa")
        //{
        //    RadTextBoxNumeroAutorizzazione.Enabled = false;
        //    RadDatePickerDataRilascio.Enabled = false;
        //}

        _isBackoffice = !GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente();

        if (!this.IsPostBack)
        {
            if (_isBackoffice)
            {
                RowIdImpresa.Visible = true;
                RequiredFieldValidatorIdImpresa.Enabled = true;
            }
            else
            {
                LabelTextIdDomanda.Visible = false;
                LabelIdDomanda.Visible = false;
            
            }
        //    // se lo stato è diverso da immessa rendo non editabli numero autorizzazione e data rilascio inps
        //    if (StatoDomanda != "Immessa")
        //    {
        //        RadTextBoxNumeroAutorizzazione.Enabled = false;
        //        RadDatePickerDataRilascio.Enabled = false;
        //    }

        }
    }


    public void AbilitaValidazioneInps(bool enable)
    {
        ValidatorNumeroAutorizzazione.Enabled = enable;
        ValidatorDataRilascio.Enabled = enable;
    }


    public CigoTelematicaDomanda GetDomanda()
    {
        CigoTelematicaDomanda domanda = new CigoTelematicaDomanda
                                        {
                                            Anno = RadMonthYearPickerPeriodo.SelectedDate.Value.Year,
                                            Mese = RadMonthYearPickerPeriodo.SelectedDate.Value.Month,
                                            ComuneCantiere = RadTextBoxComuneCantiere.Text,
                                            ProvinciaCantiere = RadTextBoxProvinciaCantiere.Text,
                                            IndirizzoCantiere = RadTextBoxIndirizzoCantiere.Text,
                                            DataRilascioAutorizzazione = RadDatePickerDataRilascio.SelectedDate,
                                            NumeroAutorizzazione = RadTextBoxNumeroAutorizzazione.Text

                                        };

        if (_isBackoffice)
        {
            domanda.IdImpresa = int.Parse(RadTextBoxIdImpresa.Text);
        }

        return domanda;


    }

    public void CaricaDatiDomanda(CigoTelematicaDomanda domanda)
    {
        RadMonthYearPickerPeriodo.SelectedDate = new DateTime(domanda.Anno.Value, domanda.Mese.Value, 1);
        RadTextBoxIdImpresa.Text = domanda.IdImpresa.ToString();
        RadTextBoxComuneCantiere.Text = domanda.ComuneCantiere;
        RadTextBoxProvinciaCantiere.Text = domanda.ProvinciaCantiere;
        RadTextBoxIndirizzoCantiere.Text = domanda.IndirizzoCantiere;
        RadDatePickerDataRilascio.SelectedDate = domanda.DataRilascioAutorizzazione;
        RadTextBoxNumeroAutorizzazione.Text = domanda.NumeroAutorizzazione;
        LabelStatoDomanda.Text = domanda.TipoStatoPrestazione.Descrizione;
        
        
        if (domanda.DataImmessaPervenuta.HasValue)
        {
            LabelDataInvio.Text = domanda.DataImmessaPervenuta.Value.ToShortDateString();
        }
        else
        {
            LabelDataInvio.Text = null;
        }

        LabelIdDomanda.Text = domanda.Id.ToString();
        
        RowStatoDomanda.Visible = true;
        RadTextBoxIdImpresa.Enabled = false;
        RadTextBoxComuneCantiere.Enabled = false;
        RadTextBoxProvinciaCantiere.Enabled = false;
        RadTextBoxIndirizzoCantiere.Enabled = false;
        RadMonthYearPickerPeriodo.Enabled = false;



        if (!String.Equals(domanda.IdStato, "I", StringComparison.CurrentCultureIgnoreCase))
        {
            RadTextBoxNumeroAutorizzazione.Enabled = false;
            RadDatePickerDataRilascio.Enabled = false;
        }


        //NM (03/10/2014): per il backoffice lascio editabili numero aut. e data rilascio per gli stati di Immessa e Immessa pervenuta
        if (!GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente() && ( String.Equals(domanda.IdStato, "I", StringComparison.CurrentCultureIgnoreCase)   
                                                                                     || String.Equals(domanda.IdStato, "T", StringComparison.CurrentCultureIgnoreCase)
                                                                                     || String.Equals(domanda.IdStato, "O", StringComparison.CurrentCultureIgnoreCase)))
        {
            RadTextBoxNumeroAutorizzazione.Enabled = true;
            RadDatePickerDataRilascio.Enabled = true;      
        }

    }
}