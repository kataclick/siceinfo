﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlControlliLavoratore.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlControlliLavoratore" %>
<table class="borderedTable" width="100%">
    <tr>
        <td colspan="3">
            <b>Controlli</b>
        </td>
    </tr>
    <tr>
        <td style="width:200px">
            Impresa Regolare:
        </td>
        <td colspan="2">
            <asp:Image ID="ImageControlloImpresaRegolare" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
    </tr>
    <tr>
        <td>
            Rapporto di Lavoro:
        </td>
        <td colspan="2">
            <asp:Image ID="ImageControlloRapportoLavoro" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
    </tr>
    <tr>
        <td>
            Lavoratore in Denuncia:
        </td>
        <td colspan="2">
            <asp:Image ID="ImageControlloLavoratoreInDenuncia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
    </tr>
    <tr>
        <td>
            Lavoratore Apprendista:
        </td>
        <td colspan="2">
            <asp:Image ID="ImageControlloLavoratoreApprendista" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
    </tr>
    <tr>
        <td>
            Ore CIGO:
        </td>
        <td colspan="2">
            <asp:Image ID="ImageControlloOreCigo" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
    </tr>
    <tr>
        <td>
            Monte Ore Mensile:
        </td>
        <td style=" width:50px">
            <asp:Image ID="ImageControlloMonteOreMensile" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
        <td>
            <asp:Label ID="LabelMonteOreMensili" runat="server"/> ore
        </td>
    </tr>
    <tr>
        <td>
            Monte Ore Annuo:
        </td>
        <td>
            <asp:Image ID="ImageControlloMonteOreAnnuo" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
        <td>
            <asp:Label ID="LabelMonteOreAnno" runat="server"/> ore
        </td>
    </tr>
    <tr>
        <td>
            Paga Oraria Dichiarata:
        </td>
        <td colspan="2">
            <asp:Image ID="ImageControlloPagaOraria" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
        </td>
    </tr>
</table>
