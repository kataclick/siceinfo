﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlDatiDomanda.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlDatiDomanda" %>
<%--<table class="standardTable">--%>


<%--<table class="borderedTable">--%>
<table>
<tr>
    <td colspan="2">
        <b>Dati Domanda</b>
    </td>
</tr>
<tr runat="server" id="RowStatoDomanda" visible="false" style="height:25px;">
    <td>
        Stato:
    </td>
    <td>
        <b><asp:Label ID="LabelStatoDomanda" runat="server" /></b>
        
    </td>
    <td>
        Data Invio a CE:
    </td>
    <td>
        <asp:Label ID="LabelDataInvio" runat="server" />
        
    </td>
    <td>
        <asp:Label ID="LabelTextIdDomanda" Text="ID Domanda:" runat="server" />
    </td>
    <td>
        <asp:Label ID="LabelIdDomanda" runat="server" />
        
    </td>
</tr>
<tr runat="server" id="RowIdImpresa" visible="false" style="height:25px;">
    <td>
        Codice Impresa:
    </td>
    <td>
        <telerik:RadTextBox ID = "RadTextBoxIdImpresa" runat="server"  InputType="Number">
        </telerik:RadTextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorIdImpresa" runat="server" ControlToValidate="RadTextBoxIdImpresa" 
                                        ErrorMessage="Inserire codice impresa" ValidationGroup="validaNuovaRichiesta" Enabled="false">
            *
            </asp:RequiredFieldValidator>
    </td>
</tr>
<tr style="height:25px;">
    <td>
        Periodo:
    </td>
    <td>
        <telerik:RadMonthYearPicker ID = "RadMonthYearPickerPeriodo" runat="server" >
        </telerik:RadMonthYearPicker>
        <asp:RequiredFieldValidator ID="reqFiledPeriodo" runat="server" ControlToValidate="RadMonthYearPickerPeriodo" 
                                        ErrorMessage="Inserire il periodo" ValidationGroup="validaNuovaRichiesta">
            *
            </asp:RequiredFieldValidator>
    </td>
</tr>
<tr>
    <td colspan="2">
        <b>Dati Cantiere</b>
    </td>
</tr>
<tr style="height:25px;">
    <td>
        Comune:
    </td>
    <td>
        <telerik:RadTextBox ID = "RadTextBoxComuneCantiere" runat="server" >
        </telerik:RadTextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldComune" runat="server" ControlToValidate="RadTextBoxComuneCantiere" 
                                        ErrorMessage="Inserire comune cantiere" ValidationGroup="validaNuovaRichiesta">
            *
            </asp:RequiredFieldValidator>
    </td>
    <td>
        Provincia:
    </td>
    <td>
        <telerik:RadTextBox ID = "RadTextBoxProvinciaCantiere" runat="server" >
        </telerik:RadTextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldProvincia" runat="server" ControlToValidate="RadTextBoxProvinciaCantiere" 
                                        ErrorMessage="Inserire provincia cantiere" ValidationGroup="validaNuovaRichiesta">
            *
            </asp:RequiredFieldValidator>
    </td>
    <td>
        Indirizzo:
    </td>
    <td>
        <telerik:RadTextBox ID = "RadTextBoxIndirizzoCantiere" runat="server" >
        </telerik:RadTextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldIndirizzo" runat="server" ControlToValidate="RadTextBoxIndirizzoCantiere" 
                                        ErrorMessage="Inserire indirizzo cantiere" ValidationGroup="validaNuovaRichiesta">
            *
            </asp:RequiredFieldValidator>
    </td>

</tr>
<tr style="height:25px;">
    <td colspan="2">
        <b>Dati Autorizzazione Inps</b>
    </td>
</tr>
<tr style="height:25px;">
    <td>
        Data Rilascio:
    </td>
    <td>
        <telerik:RadDatePicker ID = "RadDatePickerDataRilascio" runat="server" >
        </telerik:RadDatePicker>
         <%--<asp:RequiredFieldValidator ID="RequiredFieldDataRilascio" runat="server" ControlToValidate="RadDatePickerDataRilascio" 
                                        ErrorMessage="Inserire data rilascio autorizzazione INPS" ValidationGroup="validaNuovaRichiesta">
            *
            </asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="ValidatorDataRilascio" runat="server" ControlToValidate="RadDatePickerDataRilascio" 
                                        ErrorMessage="Inserire data rilascio autorizzazione INPS" ValidationGroup="validaConferma"  Enabled = "false">
            *
            </asp:RequiredFieldValidator>
    </td>
</tr>
<tr style="height:25px;">

    <td>
        Numero Autorizzazione INPS:
    </td>
    <td>
        <telerik:RadTextBox ID = "RadTextBoxNumeroAutorizzazione" runat="server" >
        </telerik:RadTextBox>
        <%--<asp:RequiredFieldValidator ID="RequiredFieldNumeroAutorizzazione" runat="server" ControlToValidate="RadTextBoxNumeroAutorizzazione" 
                                        ErrorMessage="Inserire il numero di autorizzazione INPS" ValidationGroup="validaNuovaRichiesta">
            *
            </asp:RequiredFieldValidator>--%>

            <asp:RequiredFieldValidator ID="ValidatorNumeroAutorizzazione" runat="server" ControlToValidate="RadTextBoxNumeroAutorizzazione" 
                                        ErrorMessage="Inserire il numero di autorizzazione INPS" ValidationGroup="validaConferma" Enabled="false">
            *
            </asp:RequiredFieldValidator>
    </td>

    
</tr>



</table>
