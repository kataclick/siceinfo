﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlBustaPaga.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlBustaPaga" %>
<table class="borderedTable">
    <tr>
        <td>
            <asp:Label ID="LabelTitolo" runat="server" Text="Busta Paga" Font-Bold="True"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Descrizione
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNome" runat="server">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadTextBoxNome"
                ErrorMessage="Nome documento richiesto" ValidationGroup="BustaPaga">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Selezione immagine
        </td>
        <td>
            <asp:FileUpload ID="FileUploadBustaPaga" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Selezionare un file"
                ControlToValidate="FileUploadBustaPaga" ValidationGroup="BustaPaga">**
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="RadButtonConferma" runat="server" Text="Conferma" ValidationGroup="BustaPaga"
                OnClick="RadButtonConferma_Click"></asp:Button>
            <asp:Button ID="RadButtonIndietro" runat="server" Text="Indietro" OnClick="RadButtonIndietro_Click">
            </asp:Button>
        </td>
        <td>
            <asp:ValidationSummary ID="ValidationSummaryBustaPaga" runat="server" CssClass="messaggiErrore"
                ValidationGroup="BustaPaga" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelMessaggio" runat="server"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
</table>
