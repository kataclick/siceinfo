﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlDocumentoInpsLista.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlDocumentoInpsLista" %>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
    <script type="text/javascript">
        //the following code use radconfirm to mimic the blocking of the execution thread.
        //The approach has the following limitations:
        //1. It works inly for elements that have *click* method, e.g. links and buttons
        //2. It cannot be used in if(!confirm) checks
        window.blockConfirm = function (text, mozEvent, oWidth, oHeight, callerObj, oTitle) {
            var ev = mozEvent ? mozEvent : window.event; //Moz support requires passing the event argument manually 
            //Cancel the event 
            ev.cancelBubble = true;
            ev.returnValue = false;
            if (ev.stopPropagation) ev.stopPropagation();
            if (ev.preventDefault) ev.preventDefault();

            //Determine who is the caller 
            var callerObj = ev.srcElement ? ev.srcElement : ev.target;

            //Call the original radconfirm and pass it all necessary parameters 
            debugger
            if (callerObj) {
                //Show the confirm, then when it is closing, if returned value was true, automatically call the caller's click method again. 
                var callBackFn = function (arg) {
                    debugger
                    if (arg) {
                        callerObj["onclick"] = "";
                        if (callerObj.click) callerObj.click(); //Works fine every time in IE, but does not work for links in Moz 
                        else if (callerObj.tagName == "A") //We assume it is a link button! 
                        {
                            try {
                                eval(callerObj.href)
                            }
                            catch (e) { }
                        }
                    }
                }

                radconfirm(text, callBackFn, oWidth, oHeight, callerObj, oTitle);
            }
            return false;
        }

        function confirmDeleteCb(args) {
           
           alert();
       }

       function checkDocumentoInpsAlert() {

           var grid = $find("<%= RadGridDocumentiInps.ClientID %>");
           var n = grid.get_masterTableView().get_dataItems().length;
//NM (02-10-2014): scommentare l'if se non si vogliono far aggiungere più di un documento inps
//           if (n > 0) 
//           {
//                alert("Attenzione: è già presente un documento INPS, eliminarlo per poterne inserirne uno nuovo.");
//                return false;
//           }

           return true;

       }

       function checkDocumentoInpsPresente() {
           var grid = $find("<%= RadGridDocumentiInps.ClientID %>");
           return grid.get_masterTableView().get_dataItems().length > 0;
       }

    </script>


<%--<table class="borderedTable">--%>
<table>
    <tr>
        <td style="font-weight: bold">
            Documenti Inps
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadGrid ID="RadGridDocumentiInps" runat="server" GridLines="None" CellSpacing="0"
                OnItemCommand="RadGridDocumentiInps_ItemCommand" 
                onitemdatabound="RadGridDocumentiInps_ItemDataBound">
                <MasterTableView DataKeyNames="Id">
                    <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="Descrizione" UniqueName="descrizione" DataField="Descrizione">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="visualizza" FilterControlAltText="Filter visualizza column"
                            ImageUrl="~/images/pdf24.png" UniqueName="visualizza">
                            <ItemStyle Width="10px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                            UniqueName="elimina">
                            <ItemTemplate>
                                <asp:ImageButton CssClass="PostbackButton" ID="ButtonElimina" runat="server" ImageUrl="~/images/editdelete.png" CommandName="elimina"
                                    OnClientClick="return confirm('Sei sicuro di voler eliminare il documento?');"  />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
                <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
                </HeaderContextMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="RadButtonAggiungiDocumenti" runat="server" Text="Aggiungi doc. Autorizzazione Inps" OnClientClick="return checkDocumentoInpsAlert();"
                OnClick="RadButtonAggiungiDocumenti_Click" >
            </asp:Button>    
                  
        </td>
        
    </tr>
</table>
