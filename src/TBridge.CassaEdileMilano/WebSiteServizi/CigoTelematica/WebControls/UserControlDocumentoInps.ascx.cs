﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Business;
using Cemi.CigoTelematica.Type.Delegates;
using Cemi.CigoTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;

public partial class CigoTelematica_WebControls_UserControlDocumentoInps : System.Web.UI.UserControl
{
    public event DocumentoInpsReturnedEventHandler OnDocumentoInpsReturned;

    private BusinessEF bizEF = new BusinessEF();
    private Business biz = new Business();


    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void CaricaDomanda(CigoTelematicaDomanda domanda)
    {
        ViewState["IdDomandaCigo"] = domanda.Id;
        ViewState["IdImpresa"] = domanda.IdImpresa;

        RadButtonConferma.Enabled = biz.AbilitaControllo(domanda.IdStato);
    }



    private void Reset(Boolean carica)
    {
        RadTextBoxNome.Text = "";


        if (OnDocumentoInpsReturned != null)
        {
            OnDocumentoInpsReturned(carica);
        }
    }

    protected void RadButtonConferma_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            CigoTelematicaDocumentoInps documentoInps = CreaAltroDocumento();

           
            //Inserimento
            try
            {
                bizEF.InsertDocumentoInps(documentoInps);
                
                Reset(true);
            }
            catch
            {
                ErroreNellInserimento();
            }
           
        }

    }

    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento del documento.";
    }

    private CigoTelematicaDocumentoInps CreaAltroDocumento()
    {
        CigoTelematicaDocumentoInps inps = new CigoTelematicaDocumentoInps();

        inps.DataInserimentoRecord = DateTime.Now;
        inps.IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente();
        

        inps.Descrizione = RadTextBoxNome.Text;

        if (FileUploadDocumentoInps.HasFile)
        {
            using (Stream sr = FileUploadDocumentoInps.PostedFile.InputStream)
            {
                byte[] img = new byte[sr.Length];
                sr.Read(img, 0, (int)sr.Length);
                sr.Seek(0, SeekOrigin.Begin);
                inps.Immagine = img;
                inps.NomeFile = FileUploadDocumentoInps.FileName;
            }
        }

        inps.IdDomandaCigo = (Int32)ViewState["IdDomandaCigo"];
        return inps;

    }


    protected void RadButtonIndietro_Click(object sender, EventArgs e)
    {
        Reset(false);
    }
}
