﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlGiorniCigo.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlGiorniCigo" %>
<script type="text/javascript">

    window.onload = document.onload = function () {


        $ = $telerik.$;
        var oreGiorn = parseInt($('#' + '<% =LabelOreGiornaliere.ClientID %>').text());
        var nGiorni = $(".checkboxCigo input[type='checkbox'][checked='checked']").length;
        $('#' + '<% =LabelTotaleOre.ClientID %>').text(oreGiorn * nGiorni);
               
        $(".checkboxCigo input[type='checkbox']").change(function () {

            var label = $('#' + '<% =LabelTotaleOre.ClientID %>');
            var oreGiorn = parseInt($('#' + '<% =LabelOreGiornaliere.ClientID %>').text());
            var totOre = parseInt(label.text());
            if ($(this).attr("checked")) {
                totOre = totOre + oreGiorn;
            }
            else {
                totOre = totOre - oreGiorn;
            }
            label.text(totOre)

        });
    }

  
    
</script>
<table>
    <tr>
        <td colspan="2" style="height:25px;">
            <b><telerik:RadGrid ID="RadGridCigo" runat="server"></telerik:RadGrid></b>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td style=" width:130px;height:25px;">
            Totale Ore Richieste:
        </td>
        <td style=" text-align:left;height:25px;">
            <asp:Label ID="LabelTotaleOre" runat="server" Text="0"></asp:Label>
            <asp:Label ID="LabelOreGiornaliere" runat="server" style="display:none"/>
        </td>  
    </tr>

    
    <tr runat="server" id="oreRiconosciute">      
        <td style=" width:150px;height:25px;">
            Totale Ore Riconosciute:
        </td>
        <td>
            <telerik:RadTextBox ID="TextBoxOreRiconosciute" runat="server"> 
            </telerik:RadTextBox>
                <asp:RegularExpressionValidator ID="regExValidatorOreRiconosciute" runat="server" 
                    ControlToValidate="TextBoxOreRiconosciute" 
                    ValidationExpression="[0-9]\d{0,3}"
                    ErrorMessage="Ore Riconosciute inserite non valide." 
                    ValidationGroup="salva">
                *
                </asp:RegularExpressionValidator>
        </td>       
    </tr>
   
    <tr>
        <td colspan="2" style=" width:150px;height:15px;">
        </td>
    </tr>

    
    
        
</table>