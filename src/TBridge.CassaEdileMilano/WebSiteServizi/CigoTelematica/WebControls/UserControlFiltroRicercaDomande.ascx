﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlFiltroRicercaDomande.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlFiltroRicercaDomande" %>
<table class="standardTable">
    <tr>
        <td colspan="4">
            <b>Periodo-Dal</b>
        </td>
        <td colspan="4">
            <b>Periodo-Al</b>
        </td>       
    </tr>
    <tr>
        <td>
            Anno*:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxAnno" runat="server" InputType="Number"/>
            <asp:RequiredFieldValidator ID="reqFiledValAnno" runat="server" ControlToValidate="RadTextBoxAnno" 
                                        ErrorMessage="Digitare l'anno" ValidationGroup="ricercaImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
        <td>
            Mese*:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxMese" runat="server" InputType="Number"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMese" runat="server" ControlToValidate="RadTextBoxMese"
                                            ErrorMessage="Digitare il mese" ValidationGroup="ricercaImpresa">
                *
                </asp:RequiredFieldValidator>
        </td>
                <td>
            Anno:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxAnnoAl" runat="server" InputType="Number"/>
            <%--<asp:RequiredFieldValidator ID="reqFiledValAnnoAl" runat="server" ControlToValidate="RadTextBoxAnnoAl" 
                                        ErrorMessage="Digitare l'anno" ValidationGroup="ricercaImpresa">
            *
            </asp:RequiredFieldValidator>--%>
        </td>
        <td>
            Mese:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxMeseAl" runat="server" InputType="Number"/>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorMeseAl" runat="server" ControlToValidate="RadTextBoxMeseAl"
                                            ErrorMessage="Digitare il mese" ValidationGroup="ricercaImpresa">
                *
                </asp:RequiredFieldValidator>--%>
        </td>
    </tr>
    <tr>
        <td colspan="4"> 
            <b>Cantiere</b>
        </td>
    </tr>
    <tr>
        <td>
            Comune:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxComune" runat="server"/>
        </td>
        <td>
            Provincia:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxProvincia" runat="server"/>
        </td>
        <td>
            Indirizzo:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server"/>
        </td>
    </tr>
</table>
