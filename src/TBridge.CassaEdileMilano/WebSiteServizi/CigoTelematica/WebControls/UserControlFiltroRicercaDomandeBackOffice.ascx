﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlFiltroRicercaDomandeBackOffice.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlFiltroRicercaDomandeBackOffice" %>
<table class="standardTable">
    <tr>
        <td colspan="4">
            <b>Periodo-Dal</b>
        </td>
        <td colspan="4">
            <b>Periodo-Al</b>
        </td>       
    </tr>
    <tr>
        <td>
            Anno:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxAnnoDal" runat="server" InputType="Number"/>
            <%--<asp:RequiredFieldValidator ID="reqFiledValAnnoDal" runat="server" ControlToValidate="RadTextBoxAnnoDal" 
                                        ErrorMessage="Digitare l'anno" ValidationGroup="ricercaImpresa">
            *
            </asp:RequiredFieldValidator>--%>
        </td>
        <td>
            Mese:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxMeseDal" runat="server" InputType="Number"/>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorMeseDal" runat="server" ControlToValidate="RadTextBoxMeseDal"
                                            ErrorMessage="Digitare il mese" ValidationGroup="ricercaImpresa">
                *
                </asp:RequiredFieldValidator>--%>
        </td>
        <td>
            Anno:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxAnnoAl" runat="server" InputType="Number"/>
            <%--<asp:RequiredFieldValidator ID="reqFiledValAnnoAl" runat="server" ControlToValidate="RadTextBoxAnnoAl" 
                                        ErrorMessage="Digitare l'anno" ValidationGroup="ricercaImpresa">
            *
            </asp:RequiredFieldValidator>--%>
        </td>
        <td>
            Mese:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxMeseAl" runat="server" InputType="Number"/>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorMeseAl" runat="server" ControlToValidate="RadTextBoxMeseAl"
                                            ErrorMessage="Digitare il mese" ValidationGroup="ricercaImpresa">
                *
                </asp:RequiredFieldValidator>--%>
        </td>
        
    </tr>

    <tr>
        <td colspan="4"> 
            <b>Stato Domanda</b>
        </td>
    </tr>
    <tr>
        <td>
            Stato:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxStato" runat="server" AppendDataBoundItems="true">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td colspan="4"> 
            <b>Cantiere</b>
        </td>
    </tr>
    <tr>
        <td>
            Comune:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxComune" runat="server"/>
        </td>
        <td>
            Provincia:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxProvincia" runat="server"/>
        </td>
        <td>
            Indirizzo:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server"/>
        </td>
    </tr>

    <tr>
        <td colspan="4"> 
            <b>Impresa</b>
        </td>
    </tr>

    <tr>
        <td>
            Codice:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadTextBoxCodImpresa" runat="server" Type="Number"
                    DataType="System.Int32" MinValue="1">
                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
            </telerik:RadNumericTextBox>
           
        </td>
        <td>
            Rag.Soc.:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxRagImpresa" runat="server"/>
        </td>
        <td>
            IVA/Cod.Fiscale:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server"/>
        </td>
    </tr>

    <tr>
        <td colspan="4"> 
            <b>Lavoratore</b>
        </td>
    </tr>

    <tr>
        <td>
            Codice:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadTextBoxCodLav" runat="server" Type="Number"
                    DataType="System.Int32" MinValue="1">
                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
            </telerik:RadNumericTextBox>
        </td>
        <td>
            Cognome:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCognomeLav" runat="server"/>
        </td>
        <td>
            Nome:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNomeLav" runat="server"/>
        </td>
        <td>
            Cod.Fiscale:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxFiscaleLav" runat="server"/>
        </td>
    </tr>
  

</table>
