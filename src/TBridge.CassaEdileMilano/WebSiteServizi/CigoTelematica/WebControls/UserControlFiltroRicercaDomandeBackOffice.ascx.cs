﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Type.Filters;
using TBridge.Cemi.Presenter;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Web.Script.Serialization;


public partial class CigoTelematica_WebControls_UserControlFiltroRicercaDomandeBackOffice : System.Web.UI.UserControl
{
    private BusinessEF _biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            CaricaTipiStatoCigoTelematica();
        }

    }



    private void CaricaTipiStatoCigoTelematica()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxStato, _biz.GetTipiStatoCigoTelematica(), "Descrizione", "Id");
    }



    public DomandeFilter GetFilter()
    {
        DomandeFilter filter = new DomandeFilter();

        if (!String.IsNullOrEmpty(RadTextBoxAnnoDal.Text))
        {
            filter.PeriodoAnnoDa = int.Parse(RadTextBoxAnnoDal.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxMeseDal.Text))
        {
            filter.PeriodoMeseDa = int.Parse(RadTextBoxMeseDal.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxAnnoAl.Text))
        {
            filter.PeriodoAnnoA = int.Parse(RadTextBoxAnnoAl.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxMeseAl.Text))
        {
            filter.PeriodoMeseA = int.Parse(RadTextBoxMeseAl.Text);
        }

        filter.StatoDomanda = RadComboBoxStato.SelectedValue;

        filter.ComuneCantiere = RadTextBoxComune.Text;
        filter.ProvinciaCantiere = RadTextBoxProvincia.Text;
        filter.IndirizzoCantiere = RadTextBoxIndirizzo.Text;

        if (RadTextBoxCodImpresa.Value.HasValue)
        {
            filter.IdImpresa = (Int32)RadTextBoxCodImpresa.Value.Value;
        }
               
        filter.RagioneSocialeImpresa = RadTextBoxRagImpresa.Text;
        filter.CodiceFiscaleImpresa = RadTextBoxCodiceFiscale.Text;

        if (RadTextBoxCodLav.Value.HasValue)
        {
            filter.IdLavoratore = (Int32)RadTextBoxCodLav.Value.Value;
        }

        filter.CognomeLavoratore = RadTextBoxCognomeLav.Text;
        filter.NomeLavoratore = RadTextBoxNomeLav.Text;
        filter.CodiceFiscaleLavoratore = RadTextBoxFiscaleLav.Text;
        
        SalvaFiltri(filter);

        return filter;

    }

    private void SalvaFiltri(DomandeFilter filtro)
    {
        
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        String filtroSerializzato = serializer.Serialize(filtro);

        HttpCookie cookie = new HttpCookie("Filter");
        cookie.Values["utente"] = GestioneUtentiBiz.GetIdUtente().ToString();
        cookie.Values["filtro"] = filtroSerializzato;
        cookie.Expires = DateTime.Now.AddMinutes(30);
        Response.Cookies.Add(cookie);

    }

    private DomandeFilter RecuperaFiltri()
    {
        DomandeFilter filtro = null;

        HttpCookie cookie = Request.Cookies["Filter"];
        if (cookie != null)
        { 
            string idutente = Server.HtmlEncode(cookie.Values["utente"]);

            if (idutente == GestioneUtentiBiz.GetIdUtente().ToString())
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                filtro = serializer.Deserialize<DomandeFilter>((cookie.Values["filtro"]));
                
            }
        }

        return filtro;
    
    }

    public DomandeFilter CaricaFiltriSalvati()
    {
        DomandeFilter filtro = RecuperaFiltri();

        if (filtro != null)
        {
            RadTextBoxAnnoDal.Text = filtro.PeriodoAnnoDa.ToString();
            RadTextBoxMeseDal.Text = filtro.PeriodoMeseDa.ToString();
            RadTextBoxAnnoAl.Text = filtro.PeriodoAnnoA.ToString();
            RadTextBoxMeseAl.Text = filtro.PeriodoMeseA.ToString();

            RadComboBoxStato.SelectedValue = filtro.StatoDomanda;

            RadTextBoxComune.Text = filtro.ComuneCantiere;
            RadTextBoxProvincia.Text = filtro.ProvinciaCantiere;
            RadTextBoxIndirizzo.Text = filtro.IndirizzoCantiere;

            RadTextBoxCodImpresa.Value = filtro.IdImpresa;

            RadTextBoxRagImpresa.Text = filtro.RagioneSocialeImpresa;
            RadTextBoxCodiceFiscale.Text = filtro.CodiceFiscaleImpresa;

            RadTextBoxCodLav.Value = filtro.IdLavoratore;
            RadTextBoxCognomeLav.Text = filtro.CognomeLavoratore;
            RadTextBoxNomeLav.Text = filtro.NomeLavoratore;
            RadTextBoxFiscaleLav.Text = filtro.CodiceFiscaleLavoratore;
        }

        return filtro;
    }
}