﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Business;
using Cemi.CigoTelematica.Type.Delegates;
using Cemi.CigoTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Business.Archidoc;
using System.IO;
using Telerik.Web.UI;



public partial class CigoTelematica_WebControls_UserControlBustaPagaLista : System.Web.UI.UserControl
{
    public event DocumentoBustaPagaSelectedEventHandler OnBustaPagaSelected;

    private readonly BusinessEF biz = new BusinessEF();

    public int NumeroDocumenti
    {
        get
        {
            return RadGridBustaPaga.Items.Count;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void CaricaAltriDocumenti(int idDomandaCigo, int idLavoratore, Boolean abilita)
    {
        ViewState["IdDomandaCigo"] = idDomandaCigo;
        ViewState["IdLavoratore"] = idLavoratore;
        ViewState["AbilitaControlli"] = abilita;

        CaricaAltriDocumenti();

        RadButtonAggiungiDocumentiBusta.Enabled = abilita;
  
    }

    private void CaricaAltriDocumenti()
    {
        Presenter.CaricaElementiInGridView(RadGridBustaPaga, biz.GetBustaPaga((Int32)ViewState["IdDomandaCigo"],(Int32)ViewState["IdLavoratore"]));

    }


    protected void RadGridBustaPaga_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "elimina":
                {
                    Int32 idAltroDocumento = (Int32)
                                             RadGridBustaPaga.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                    biz.DeleteBustaPaga(idAltroDocumento);

                    CaricaAltriDocumenti();

                   
                }
                break;
            case "visualizza":
                {
                    Int32 idAltroDocumento = (Int32)
                                             RadGridBustaPaga.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                    RestituisciFile(biz.GetImmagineBustaPaga(idAltroDocumento));

                   
                }
                break;
        }
    }


    private void RestituisciFile(Immagine img)
    {
        string idArchidoc = img.IdArchidoc;

        if (idArchidoc != null)
        {
            // Caricare il documento tramite il Web Service Archidoc
            try
            {
                IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                if (file != null)
                {
                    Presenter.RestituisciFileArchidoc(idArchidoc, file, Path.GetExtension(img.NomeFile).ToUpper(), this.Page);
                }
            }
            catch
            {
                //    LabelErroreVisualizzazioneDocumento.Visible = true;
            }

        }
        else
        {
            //Set the appropriate ContentType.
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", img.NomeFile));
            switch (Path.GetExtension(img.NomeFile).ToUpper())
            {
                case ".XML":
                    Response.ContentType = "text/plain";
                    break;
                case ".PDF":
                    Response.ContentType = "application/pdf";
                    break;
                default:
                    Response.ContentType = "application/download";
                    break;

            }
            //Write the file directly to the HTTP content output stream.
            Response.BinaryWrite(img.File);
            Response.Flush();
            Response.End();
        }
    }

    protected void RadButtonAggiungiDocumentiBusta_Click(object sender, EventArgs e)
    {
        // NM (02-10-2014): scommentare l'if che segue se si vuole controllare che non ci siano documenti già inseriti
        //if (RadGridBustaPaga.Items.Count == 0)
        //{
            if (OnBustaPagaSelected != null)
            {
                OnBustaPagaSelected();
            }
            
        //}

                   
    }


    protected void RadGridBustaPaga_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            ImageButton ibElimina = (ImageButton)e.Item.FindControl("ButtonElimina");

            ibElimina.Enabled = (Boolean)ViewState["AbilitaControlli"];
        }
    }


}