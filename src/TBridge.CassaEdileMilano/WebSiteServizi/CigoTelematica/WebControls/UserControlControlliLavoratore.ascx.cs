﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using Cemi.CigoTelematica.Business;
using Cemi.CigoTelematica.Type.Entities;

public partial class CigoTelematica_WebControls_UserControlControlliLavoratore : System.Web.UI.UserControl
{
    private const string SEMAFORO_VERDE = "~/images/semaforoVerde.png";
    private const string SEMAFORO_GIALLO = "~/images/semaforoGiallo.png";
    private const string SEMAFORO_ROSSO = "~/images/semaforoRosso.png";

    private BusinessEF _biz = new BusinessEF();
    private int _idDomanda;

    protected void Page_Load(object sender, EventArgs e)
    {
        string idDomandaStr = Request.QueryString["Id"];
        _idDomanda = int.Parse(idDomandaStr);
       
       
        CigoTelematicaDomanda domanda = _biz.GetDomandaById(_idDomanda);
        ControlloImpresaRegolare(domanda);

    }



    private void ControlloImpresaRegolare(CigoTelematicaDomanda domanda)
    {
        //regolare
        ImageControlloImpresaRegolare.ImageUrl = SEMAFORO_VERDE;

        DateTime dataFine = new DateTime(domanda.Anno.Value, domanda.Mese.Value, 1);
        DateTime dataInizio = dataFine.AddMonths(-4);


        decimal debito = RegolaritaContributivaManager.GetDebitoImpresa(domanda.IdImpresa, dataInizio, dataFine);

        if (debito >= 30.0M)
        {
            ImageControlloImpresaRegolare.ImageUrl = SEMAFORO_GIALLO;
        }

    }


    public void AggiornaControlliLavoratore(ControlliLavoratore controlli)
    {
        ImageControlloOreCigo.ImageUrl = controlli.OreCigoValide ? SEMAFORO_VERDE : SEMAFORO_GIALLO;
        ImageControlloMonteOreMensile.ImageUrl = controlli.MonteOreMensileValido ? SEMAFORO_VERDE : SEMAFORO_GIALLO;
        ImageControlloMonteOreAnnuo.ImageUrl = controlli.MonteOreAnnuoValido ? SEMAFORO_VERDE : SEMAFORO_GIALLO;
        ImageControlloLavoratoreApprendista.ImageUrl = controlli.LavoratoreApprendista ? SEMAFORO_VERDE : SEMAFORO_ROSSO;
        ImageControlloLavoratoreInDenuncia.ImageUrl = controlli.LavoratoreInDenuncia ? SEMAFORO_VERDE : SEMAFORO_ROSSO;
        ImageControlloPagaOraria.ImageUrl = controlli.PagaOrariaValida ? SEMAFORO_VERDE : SEMAFORO_GIALLO;
        ImageControlloRapportoLavoro.ImageUrl = controlli.RapportoDiLavoroValido ? SEMAFORO_VERDE : SEMAFORO_ROSSO;
        LabelMonteOreMensili.Text = controlli.TotaleOreMese.ToString();
        LabelMonteOreAnno.Text = controlli.TotaleOreAnnoRiconosciute.ToString();    
    }

    //private void ControlloRapportoLavoro()
    //{
    //    if ((String)ViewState["IdStato"] == "R" || (String)ViewState["IdStato"] == "7" || (String)ViewState["IdStato"] == "6")
    //    {
    //        ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoGiallo.png";
    //    }
    //    else
    //    {
    //        DateTime? inizioRapporto = (DateTime?)ViewState["InizioRapporto"];
    //        DateTime? fineRapporto = (DateTime?)ViewState["FineRapporto"];
    //        DateTime inizioAssenza = (DateTime)ViewState["DataInizioAssenza"];
    //        DateTime fineAssenza = (DateTime)ViewState["DataFineAssenza"];

    //        if (ViewState["DataAssunzione"] != null)
    //        {
    //            if (inizioAssenza >= inizioRapporto && inizioAssenza <= fineRapporto
    //                &&
    //                fineAssenza >= inizioRapporto && fineAssenza <= fineRapporto)
    //            {
    //                ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoVerde.png";
    //            }
    //            else
    //            {
    //                if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
    //                {
    //                    ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoGiallo.png";
    //                }
    //                else
    //                {
    //                    ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoRosso.png";
    //                }
    //            }
    //        }
    //        else
    //        {
    //            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
    //            {
    //                ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoGiallo.png";
    //            }
    //            else
    //            {
    //                ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoRosso.png";
    //            }
    //        }
    //    }
    //}
}

