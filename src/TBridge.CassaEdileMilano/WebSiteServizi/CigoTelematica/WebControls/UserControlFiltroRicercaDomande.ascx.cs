﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.CigoTelematica.Type.Filters;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Web.Script.Serialization;

public partial class CigoTelematica_WebControls_UserControlFiltroRicercaDomande : System.Web.UI.UserControl
{
    private BusinessEF _biz = new BusinessEF();
    private Business _b = new Business();

    protected void Page_Load(object sender, EventArgs e)
    {
    
        //DomandeFilter filtro = null;
        //Int32 pagina;

        //try
        //{
            
        //}

    }

    public DomandeFilter GetFilter()
    {
        DomandeFilter filter = new DomandeFilter();
        if (!String.IsNullOrEmpty(RadTextBoxAnno.Text))
        {
            filter.PeriodoAnnoDa = int.Parse(RadTextBoxAnno.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxMese.Text))
        {
            filter.PeriodoMeseDa = int.Parse(RadTextBoxMese.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxAnnoAl.Text))
        {
            filter.PeriodoAnnoA = int.Parse(RadTextBoxAnnoAl.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxMeseAl.Text))
        {
            filter.PeriodoMeseA = int.Parse(RadTextBoxMeseAl.Text);
        }
        filter.ComuneCantiere = RadTextBoxComune.Text;
        filter.ProvinciaCantiere = RadTextBoxProvincia.Text;
        filter.IndirizzoCantiere = RadTextBoxIndirizzo.Text;

        SalvaFiltri(filter);

        return filter;

    }

    private void SalvaFiltri(DomandeFilter filtro)
    {

        JavaScriptSerializer serializer = new JavaScriptSerializer();
        String filtroSerializzato = serializer.Serialize(filtro);

        HttpCookie cookie = new HttpCookie("Filter");
        cookie.Values["utente"] = GestioneUtentiBiz.GetIdUtente().ToString();
        cookie.Values["filtro"] = filtroSerializzato;
        cookie.Expires = DateTime.Now.AddMinutes(30);
        Response.Cookies.Add(cookie);

    }


    private DomandeFilter RecuperaFiltri()
    {
        DomandeFilter filtro = null;

        HttpCookie cookie = Request.Cookies["Filter"];
        if (cookie != null)
        {
            string idutente = Server.HtmlEncode(cookie.Values["utente"]);

            if (idutente == GestioneUtentiBiz.GetIdUtente().ToString())
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                filtro = serializer.Deserialize<DomandeFilter>((cookie.Values["filtro"]));

            }
        }

        return filtro;

    }

    public DomandeFilter CaricaFiltriSalvati()
    {
        DomandeFilter filtro = RecuperaFiltri();

        if (filtro != null)
        {
            RadTextBoxAnno.Text = filtro.PeriodoAnnoDa.ToString();
            RadTextBoxMese.Text = filtro.PeriodoMeseDa.ToString();
            RadTextBoxAnnoAl.Text = filtro.PeriodoAnnoA.ToString();
            RadTextBoxMeseAl.Text = filtro.PeriodoMeseA.ToString();

            RadTextBoxComune.Text = filtro.ComuneCantiere;
            RadTextBoxProvincia.Text = filtro.ProvinciaCantiere;
            RadTextBoxIndirizzo.Text = filtro.IndirizzoCantiere;

        }

        return filtro;
    }

}