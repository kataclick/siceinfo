﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Presenter;
using Cemi.CigoTelematica.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Business;

using Cemi.CigoTelematica.Business;
using Cemi.CigoTelematica.Type.Entities;

public partial class CigoTelematica_WebControls_UserControlGiorniCigo : System.Web.UI.UserControl
{
    private BusinessEF _biz = new BusinessEF();
    private bool _isBackOffice;


    protected void Page_Load(object sender, EventArgs e)
    {
        _isBackOffice = !(GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente());


        if (_isBackOffice)
        {
            TextBoxOreRiconosciute.Enabled = true;
        }
        else 
        {
            TextBoxOreRiconosciute.Enabled = false;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //if (Page.IsPostBack)
        //{
        //    return;
        //}


        #region Creazione del RadGrid di stato cigo

        RadGrid rdCigo = RadGridCigo;
        rdCigo.Skin = String.Empty;
        rdCigo.ItemDataBound += new GridItemEventHandler(rdAssenza_ItemDataBound);

        GridTemplateColumn descr = new GridTemplateColumn();
        descr.UniqueName = "descrizione";
        descr.HeaderText = "";// ((DateTime)ViewState["Periodo"]).ToString("MMM yyyy");
        descr.ItemTemplate = new CigoGrigliaDescrizioneTemplate();

        rdCigo.MasterTableView.Columns.Add(descr);

        for (int i = 0; i < 31; i++)
        {
            GridTemplateColumn grid = new GridTemplateColumn();
            grid.UniqueName = i.ToString();
            grid.ItemTemplate = new CigoGrigliaTemplate((i+1).ToString());
            grid.HeaderText = (i + 1).ToString();
            grid.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            grid.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            rdCigo.MasterTableView.Columns.Add(grid);
        }

        #endregion  

      
    }


    public void CaricaGiorniCigo(int idLavoratore, int idDomanda, int? orePT)
    {
        List<CigoTelematicaLavoratoreGiorno> giorni = _biz.GetLavoratoreGiorni(idLavoratore, idDomanda);

        CigoTelematicaLavoratore lav = _biz.OreRiconosciute(idDomanda, idLavoratore);
        CigoTelematicaDomanda dom = _biz.GetDomandaById(idDomanda);

        int ore =  orePT.HasValue ? ((int)(orePT == 0 ? 8 : (8 * orePT / 100))) : 0 ;
        LabelOreGiornaliere.Text = ore.ToString();

        Presenter.CaricaElementiInGridView(RadGridCigo, new List<Object>() { giorni });// new List<Object>() { lavoratore.CigoTelematicaLavoratoriGiorni.ToList() });

        if (_isBackOffice)
        {
            if (lav.OreRiconosciute != null)
            {
                TextBoxOreRiconosciute.Text = lav.OreRiconosciute.ToString();
            }
            else
            {
                if (dom.IdStato != "I")
                {
                    int i = 0;
                    foreach (var item in giorni)
                    {
                        i = i + 1;
                    }

                    TextBoxOreRiconosciute.Text = (ore * i).ToString();
                }
                else 
                {
                    TextBoxOreRiconosciute.Text = null;
                }
            }
        }
        else 
        {
            if (dom.IdStato == "R" || dom.IdStato == "L" || dom.IdStato == "D" || dom.IdStato == "C")
            {
                TextBoxOreRiconosciute.Visible = true;
                TextBoxOreRiconosciute.Text = lav.OreRiconosciute.ToString();
            }
            else 
            {
                oreRiconosciute.Visible = false;
                //TextBoxOreRiconosciute.Visible = false;              
            }
        }
    
    }


    public string OreRiconosciute(int idDomanda, int idLavoratore)
    {

        CigoTelematicaLavoratore lav = _biz.OreRiconosciute(idDomanda, idLavoratore);

        if (lav.OreRiconosciute.ToString() != null && lav.OreRiconosciute.ToString() != TextBoxOreRiconosciute.Text)
        {
            TextBoxOreRiconosciute.Text = TextBoxOreRiconosciute.Text;
        }
        else
        {
            //TextBoxOreRiconosciute.Text = lav.oreRiconosciute.ToString() == "" ? TextBoxOreRiconosciute.Text : lav.oreRiconosciute.ToString();
            TextBoxOreRiconosciute.Text = lav.OreRiconosciute.ToString() == null ? LabelTotaleOre.Text : lav.OreRiconosciute.ToString();
        }
        return TextBoxOreRiconosciute.Text;

            
    }
    
    
    public GiornoCigoCollection GetGiorniCigo()
    {
        GiornoCigoCollection giorni = new GiornoCigoCollection();

        for (int i = 1; i <= 31; i++)
        {
            CheckBox checkBox = (CheckBox)RadGridCigo.Items[0].FindControl(string.Format("CheckBoxCigo{0}", i));
            giorni.Add(new Cemi.CigoTelematica.Type.Entities.GiornoCigo
            {
                Giorno = i,
                Selected = checkBox.Checked,

            });

        }

        return giorni;
        
    }


    void rdAssenza_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem)
        {
            List<CigoTelematicaLavoratoreGiorno> giorni = e.Item.DataItem as List<CigoTelematicaLavoratoreGiorno>;

            foreach (var item in giorni)
            {
                CheckBox checkBox = (CheckBox)e.Item.FindControl(String.Format("CheckBoxCigo{0}", item.DataAssenza.Day));
                checkBox.Checked = true;

            }
        }
     
    }
}



public class CigoGrigliaTemplate : ITemplate
{
    public String progressivo { get; set; }

    protected CheckBox checkBoxCigo;

    public CigoGrigliaTemplate(String progressivo)
    {
        this.progressivo = progressivo;
    }

    public void InstantiateIn(Control container)
    {

        checkBoxCigo = new CheckBox();
        checkBoxCigo.ID = String.Format("CheckBoxCigo{0}", progressivo);
        checkBoxCigo.Attributes.Add("class", "checkboxCigo");
        checkBoxCigo.EnableViewState = true;

        Table table = new Table();

        table.Style.Add("padding", "0px");
        table.Width = new Unit("100%");
        
        TableRow row1 = new TableRow();

        TableCell cell1 = new TableCell();

        cell1.HorizontalAlign = HorizontalAlign.Center;
        cell1.Style.Add("padding", "0px");


        cell1.Controls.Add(checkBoxCigo);

        row1.Cells.Add(cell1);

        table.Rows.Add(row1);

        container.Controls.Add(table);

    }
}

public class CigoGrigliaDescrizioneTemplate : ITemplate
{
    protected LiteralControl lControlCigo;

    public CigoGrigliaDescrizioneTemplate()
    {
    }

    public void InstantiateIn(Control container)
    {
        lControlCigo = new LiteralControl();
        lControlCigo.ID = "Cigo";
      
        lControlCigo.Text = "GG. CIGO:";
                
        Table table = new Table();

        table.Style.Add("padding", "0px");
        table.Width = new Unit("100px");
        

        TableRow row1 = new TableRow();
        
        TableCell cell1 = new TableCell();

        cell1.Controls.Add(lControlCigo);

        row1.Cells.Add(cell1);

        table.Rows.Add(row1);

        container.Controls.Add(table);

    }
}