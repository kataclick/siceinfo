﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlDocumentoInps.ascx.cs" Inherits="CigoTelematica_WebControls_UserControlDocumentoInps" %>
<table class="borderedTable">
    <tr>
        <td>
            <asp:Label ID="LabelTitolo" runat="server" Text="Documento Inps" Font-Bold="True"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Descrizione
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNome" runat="server">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadTextBoxNome"
                ErrorMessage="Nome documento richiesto" ValidationGroup="Documentoinps">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Selezione immagine
        </td>
        <td>
            <asp:FileUpload ID="FileUploadDocumentoInps" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Selezionare un file"
                ControlToValidate="FileUploadDocumentoInps" ValidationGroup="Documentoinps">**
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="RadButtonConferma" runat="server" Text="Conferma" ValidationGroup="Documentoinps"
                OnClick="RadButtonConferma_Click"></asp:Button>
            <asp:Button ID="RadButtonIndietro" runat="server" Text="Indietro" OnClick="RadButtonIndietro_Click">
            </asp:Button>
        </td>
        <td>
            <asp:ValidationSummary ID="ValidationSummaryDocumentoInps" runat="server" CssClass="messaggiErrore"
                ValidationGroup="DocumentoInps" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelMessaggio" runat="server"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
</table>
