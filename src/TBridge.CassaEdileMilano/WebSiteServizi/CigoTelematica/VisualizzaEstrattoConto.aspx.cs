﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using Cemi.CigoTelematica.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CigoTelematica_VisualizzaEstrattoConto : System.Web.UI.Page
{
    private BusinessEF _biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {

        #region Autorizzazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CigoTelematicaEstrattoConto);

        #endregion

        if (!Page.IsPostBack)
        {
            Int32 annoProtocollo = 0;
            Int32 numeroProtocollo = 0;
            String tipoProtocollo = null;

            if (Context.Items["annoProtocollo"] != null)
            {
                annoProtocollo = (int)Context.Items["annoProtocollo"]; ;
            }

            if (Context.Items["numeroProtocollo"] != null)
            {
                numeroProtocollo = (int)Context.Items["numeroProtocollo"]; ;
            }

            if (Context.Items["tipoProtocollo"] != null)
            {
                tipoProtocollo = Context.Items["tipoProtocollo"].ToString();
            }

            _biz.InserisciProtocolloEstrattoConto(annoProtocollo, numeroProtocollo, tipoProtocollo);

            ReportViewerEstrattoConto.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerEstrattoConto.ServerReport.ReportPath = "/ReportCigo/EstrattoConto";

            ReportParameter[] listaParam = new ReportParameter[2];
            listaParam[0] = new ReportParameter("annoProtocollo", annoProtocollo.ToString());
            listaParam[1] = new ReportParameter("numeroProtocollo", numeroProtocollo.ToString());
            ReportViewerEstrattoConto.ServerReport.SetParameters(listaParam);


            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            //PDF

            byte[] bytes = ReportViewerEstrattoConto.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);


            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/pdf";

            Response.AppendHeader("Content-Disposition", "attachment;filename=EstrattoConto.pdf");
            Response.BinaryWrite(bytes);

            Response.Flush();
            Response.End();
        }
    }
}