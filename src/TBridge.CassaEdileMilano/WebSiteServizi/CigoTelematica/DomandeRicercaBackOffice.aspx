﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="CETheme2009Wide" CodeFile="DomandeRicercaBackOffice.aspx.cs" Inherits="CigoTelematica_DomandeRicercaBackOffice" MasterPageFile="~/MasterPage.master" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCigoTelematica.ascx" TagName="MenuCigoTelematica" TagPrefix="uc3" %>
<%@ Register Src="WebControls/UserControlFiltroRicercaDomandeBackOffice.ascx"  TagName="UserControlFiltroRicercaDomandeBackOffice" TagPrefix="uc4" %>
<%@ Register Src="WebControls/UserControlDatiDomanda.ascx"  TagName="UserControlDatiDomanda" TagPrefix="uc4" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">

    <style type="text/css">
         .RadWindow .rwIcon { 
            height: 0!important; 
            width: 0!important; 
            } 
    </style>
    <script type="text/javascript">
        function ShowRadWindowNuovaDomanda() {
            var oWnd = $find('<% =RadWindowNuovaDomanda.ClientID %>');
            oWnd.show();
        }

        function CloseRadWindowNuovaDomanda() {
            var oWnd = $find('<% =RadWindowNuovaDomanda.ClientID %>');
            oWnd.close();
        }

        function ButtonSelezionaOnClientClicking(button, args) {
            window.location = button.get_navigateUrl();
            args.set_cancel(true);
        }

        function CloseRadWindowAndGotoGestione(idDomanda) {
            var oWnd = $find('<% =RadWindowNuovaDomanda.ClientID %>');
            if (oWnd) {
                oWnd.close();
            }

            window.location = "DomandaGestione.aspx?id=" + idDomanda;
        }
    </script>
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="CIGO" sottoTitolo="Ricerca domande" />
    <%--<telerik:RadWindow ID="RadWindowNuovaDomanda" runat="server" Width="750px" Height="500px"  AutoSize="false" Behaviors="Close">--%>
    <telerik:RadWindow ID="RadWindowNuovaDomanda" runat="server" Width="895px" Height="440px"   Modal="true" Behaviors="Close" >
        <ContentTemplate>
           <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server">
                <div class="borderedDiv" >
                    <%--<b style="color:#B20000;" >Nuova Domanda Cigo</b>--%>
                    <span style="color:#B20000; font-weight:bold; margin-bottom:20px; display:block">Nuova Domanda Cigo</span>
                   
                    <table width="100%"  class="borderedTable">
                        <tr>
                            <td colspan="2">
                                 <uc4:UserControlDatiDomanda ID="UserControlDatiDomanda1" runat="server"  />    
                            </td>
                        </tr>
                        <tr style="height:80px" >
                            <td>
                                <asp:ValidationSummary ID="ValidationSummaryNuovaDomanda" runat="server" ValidationGroup="validaNuovaRichiesta"
                                                CssClass="messaggiErrore" />
                            </td>
                            <td align="right" valign="top">
                                <telerik:RadButton ID="RadButton1" runat="server" Text="Conferma" OnClick="RadButtonConferma_Click"/>                      
                                
                                <telerik:RadButton ID="RadButton2" runat="server" Text="Annulla" OnClientClicked="CloseRadWindowNuovaDomanda">                      
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table> 
                </div>
           </telerik:RadAjaxPanel>
        </ContentTemplate>
    </telerik:RadWindow>
    <div class="standardDiv">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <uc4:UserControlFiltroRicercaDomandeBackOffice ID="UserControlFiltroRicercaDomandeBackOffice" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ValidationSummary ID="ValidationSummaryLavoratore" runat="server" ValidationGroup="ricercaImpresa"
                                            CssClass="messaggiErrore" />
                </td>
                <td align="right">
                    <asp:Button ID="RadButtonCerca" runat="server" Text="Cerca" 
                                        Style=" margin-right:5px; margin-left:5px" Width="100px" 
                        ValidationGroup="ricercaImpresa" OnClick="RadButtonCerca_Click" ></asp:Button>
                    <asp:Button ID="RadButtonNuovaDomanda" runat="server" Text="Nuova richiesta" 
                        Width="105px" OnClientClick="ShowRadWindowNuovaDomanda(); return false;"></asp:Button>
                    
                </td>
            </tr>
        </table>
    </div>
    <div class="standardDiv">
         <telerik:RadGrid ID="RadGridDomande" runat="server" Width="100%" GridLines="None" AllowPaging="True" PageSize="15" 
                OnItemDataBound="RadGridDomande_ItemDataBound" Font-Size="Small"  OnItemCommand="RadGridDomande_ItemCommand" OnPageIndexChanged="RadGridDomande_PageIndexChanged" Visible="true">
                <MasterTableView DataKeyNames="Id,IdUtenteInCarico">
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="Periodo" >
                        <ItemTemplate>
                            <asp:Label ID="LabelPeriodo" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa" >
                        <ItemTemplate>
                            <asp:Label ID="LabelImpresa" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Indirizzo" UniqueName="Indirizzo" >
                        <ItemTemplate>
                            <asp:Label ID="LabelIndirizzo" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Stato" UniqueName="Stato" >
                        <ItemTemplate>
                            <asp:Label ID="LabelStato" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <%--<telerik:GridBoundColumn HeaderText="Stato" UniqueName="Stato" DataField="idStato">
                        </telerik:GridBoundColumn>--%>
                        
                        <telerik:GridTemplateColumn HeaderText="" UniqueName="Seleziona" >
                            <ItemTemplate>
                                <telerik:RadButton ID="RadButtonSelezinona"   runat="server" ToolTip="Seleziona" 
                                                    OnClientClicking="ButtonSelezionaOnClientClicking" 
                                                    Image-ImageUrl="~/images/edit.png" Image-EnableImageButton="true" Width="24px" Height="24px"/>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        
<%--                        <telerik:GridBoundColumn HeaderText="In carico" UniqueName="InCarico" DataField="IdUtenteInCarico">
                        </telerik:GridBoundColumn>--%>
                        
                        <telerik:GridTemplateColumn HeaderText="In carico" UniqueName="InCarico" >
                        <ItemTemplate>
                            <asp:Label ID="LabelUtenteInCarico" runat="server" />
                        </ItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridTemplateColumn HeaderText="" UniqueName="InCarico">
                            <ItemTemplate>
                                <asp:Button ID="ButtonInCarico" runat="server" Text="In carico" CommandName="inCarico"  Width="80px"/>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuCigoTelematica ID="MenuCigoTelematica1" runat="server" />
</asp:Content>

