using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class GestioneUtenti : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti);
        funzionalita.Add(FunzionalitaPredefinite.GestioneUtentiGestionePIN);
        funzionalita.Add(FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori);
        funzionalita.Add(FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "GestioneUtentiDefault.aspx");
    }
}