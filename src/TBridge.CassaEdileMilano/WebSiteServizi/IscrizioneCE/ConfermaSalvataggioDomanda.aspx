<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaSalvataggioDomanda.aspx.cs" Inherits="IscrizioneCE_ConfermaSalvataggioDomanda" %>

<%@ Register Src="../WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Conferma"
        titolo="Iscrizione imprese" />
    <br />
    La domanda � stata correttamente salvata. E' possibile modificarla scegliendo la
    voce di menu "Domande non confermate".&nbsp;
</asp:Content>

