using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneCE_ConfermaSalvataggioDomanda : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEIscrizione,
                                              "~/IscrizioneCE/ConfermaSalvataggioDomanda.aspx");
    }
}