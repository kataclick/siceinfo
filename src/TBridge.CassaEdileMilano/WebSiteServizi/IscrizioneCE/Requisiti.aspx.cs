using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneCE_Requisiti : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEIscrizione,
                                              "~/IscrizioneCE/Requisiti.aspx");
    }

    protected void ButtonProsegui_Click(object sender, EventArgs e)
    {
        string tipoModulo;
        /*
        if (RadioButtonModuloNormale.Checked)
            tipoModulo = "normale";
        else
            tipoModulo = "prevedi";
        */

        tipoModulo = "unico";

        Response.Redirect(String.Format("~/IscrizioneCE/IscrizioneCE.aspx?tipoModulo={0}", tipoModulo));
    }
}