<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaCambioStatoDomanda.aspx.cs" Inherits="IscrizioneCE_ConfermaCambioStatoDomanda" %>

<%@ Register Src="../WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione imprese" />
    <br />
    Attenzione, si � deciso di <asp:Label ID="LabelModalita" runat="server" Font-Bold="true"></asp:Label> la domanda di iscrizione della societ�
    <br />
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 200px">
                Ragione Sociale:
            </td>
            <td>
                <asp:Label ID="LabelRagioneSociale" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 200px">
                Partita IVA:
            </td>
            <td>
                <asp:Label ID="LabelPartitaIVA" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 200px">
                Codice fiscale:
            </td>
            <td>
                <asp:Label ID="LabelCodiceFiscale" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 200px">
                Tipo Impresa:
            </td>
            <td>
                <asp:Label ID="LabelTipoImpresa" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr id="trTipoIscrizione" runat="server" visible="false">
            <td style="width: 200px">
                Tipo iscrizione:
            </td>
            <td>
                <asp:DropDownList ID="DropDownListTipoIscrizione" runat="server" Width="300px" AppendDataBoundItems="True"></asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoIscrizione" runat="server"
                    ControlToValidate="DropDownListTipoIscrizione" ErrorMessage="Tipo iscrizione non selezionato"
                    ValidationGroup="azione"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 200px">
                
            </td>
            <td>
                <asp:Button ID="ButtonConferma" runat="server" Text="Conferma" OnClick="ButtonConferma_Click" ValidationGroup="azione" />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label></td>
        </tr>
    </table>
</asp:Content>

