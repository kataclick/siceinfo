using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneCE_IscrizioneCEDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.IscrizioneCEIscrizione);
        funzionalita.Add(FunzionalitaPredefinite.IscrizioneCEGestioneDomande);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "IscrizioneCEDefault.aspx");
    }
}