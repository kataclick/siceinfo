<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Requisiti.aspx.cs" Inherits="IscrizioneCE_Requisiti" %>

<%@ Register Src="../WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Requisiti"
        titolo="Iscrizione imprese" />
    <br />
    <div>
        <p>
            In questa pagina vengono riportate le informazioni indispensabili per la corretta e completa compilazione della domanda di iscrizione.
            <br />
            Perch� l'operazione vada a buon fine occorre accertarsi di disporre di tutte le informazioni sotto elencate. Si ricorda che la compilazione dei dati sotto riproposti � obbligatoria; pertanto, l'omissione anche di un solo dato render� impossibile il completamento della procedura.
        </p>
        <p>
            Dati obbligatori:
        </p>
        <ul>
            <li>
                DATI IMPRESA
                <br />
                <br />
                <ol>
                    <li>
                        ragione sociale, Codice Fiscale, P. IVA
                    </li>
                    <li>
                        indirizzo sede legale/amministrativa (Comune, Provincia, CAP)
                    </li>
                    <li>
                        recapiti (telefono, fax, e-mail, 
                        PEC)
                    </li>
                    <li>
                        Matricola INPS e 
                        codice ditta INAIL
                    </li>
                    <li>
                        numero e data iscrizione al Registro delle Imprese (o al Registro delle Imprese � sezione speciale per le imprese artigiane)
                    </li>
                    <li>
                        CCNL applicato (industria, artigianato, cooperativa)
                    </li>
                    <li>
                        forma giuridica
                    </li>
                    <li>
                        adesione ad una Organizzazione Imprenditoriale con n. posizione
                    </li>
                    <li>
                        codice attivit� ISTAT
                    </li>
                    <li>
                        coordinate bancarie (IBAN)
                    </li>
                    <li>
                        dati personali legale rappresentante della societ� (cognome e nome, C.F., data, Comune, Paese di nascita, Comune residenza, ecc.)
                    </li>
                    <li>
                        numero operai / impiegati occupati all�atto dell�iscrizione
                    </li>
                </ol>
            </li>
            <br />
            <br />
            <li style="text-align:justify; padding-right:10px">
               DATI CANTIERE
                <!--<br />
                <br />
                
                <ol>
                    <li>
                        Indirizzo del cantiere
                    </li>
                    <li>
                        numero operai occupati all'atto dell'iscrizione
                    </li>
                    <li>
                        Ragione Sociale Committente
                    </li>
                </ol>
                -->
                <p>Si ricorda che i dati del cantiere devono essere dichiarati tramite il portale Edilconnect (<a href="http://www.edilconnect.it" target="_blank">www.edilconnect.it</a>)<!-- o tramite il sistema di invio delle denunce (M.U.T. � Modulo Unico Telematico) a seconda dell�ubicazione dell�unit� produttiva.--></p>
                <!--<p>Se il cantiere � attivo nel territorio della Regione Lombardia, i dati relativi al sito produttivo devono essere dichiarati tramite il portale Edilconnect (<a href="http://www.edilconnect.it" target="_blank">www.edilconnect.it</a>) a seguito del rilascio del numero di iscrizione alla scrivente Cassa che verr� attribuito all�impresa richiedente al termine della presente procedura.</p>
                <p>Se il cantiere � aperto al di fuori del territorio della Regione Lombardia, i dati relativi al sito produttivo devono essere dichiarati tramite il portale M.U.T. (Modulo Unico Telematico <a href="https://mutssl2.cnce.it/mutuser/" target="_blank">https://mutssl2.cnce.it/mutuser/</a>).</p>-->

            </li>
        </ul>
        <% /*
        <br />        
        <table class="standardTable">
            <tr>
                <td>
                    Selezionare il tipo di domanda:
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="RadioButtonModuloNormale" runat="server" GroupName="tipoModulo" Text="con operai" Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="RadioButtonModuloPrevedi" runat="server" GroupName="tipoModulo" Text="con soli impiegati ai fini Prevedi" Checked="false" />
                </td>
            </tr>
        </table>
        */
            %>

        <p>
            <asp:Button ID="ButtonProsegui" runat="server" OnClick="ButtonProsegui_Click" Text="Prosegui" /></p>
        </div>
</asp:Content>

