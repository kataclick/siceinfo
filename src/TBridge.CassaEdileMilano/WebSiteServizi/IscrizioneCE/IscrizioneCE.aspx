﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IscrizioneCE.aspx.cs" Inherits="IscrizioneCE_IscrizioneCE"  %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/IscrizioneCEIscrizione.ascx" TagName="IscrizioneCEIscrizione"
    TagPrefix="uc2" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Iscrizione"
        titolo="Iscrizione imprese" />
    <br />
    <uc2:IscrizioneCEIscrizione ID="IscrizioneCEIscrizione1" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
</asp:Content>
