<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IscrizioneCEDefault.aspx.cs" Inherits="IscrizioneCE_IscrizioneCEDefault" %>

<%@ Register Src="../WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione imprese"
        sottoTitolo="Iscrizione" />
    <br />
    <p class="DefaultPage">
        In questa sezione � possibile effettuare l'iscrizione presso Cassa Edile di Milano.
        E' prevista la compilazione obbligatoria di tutti i campi, senza la quale non potr�
        essere portata a termine con successo la procedura.
    </p>
    <p class="DefaultPage">
        Per le imprese non � richiesta alcuna registrazione al sito internet e possono,
        quindi, procedere direttamente alla compilazione della domanda di iscrizione cliccando
        sulla voce "Iscrizione" riportata nel men� verticale alla sinistra del video.
    </p>
    <p class="DefaultPage">
        <b>L'operazione pu� essere eseguita anche da un consulente del lavoro che deve essersi
            preventivamente registrato in quest'area del sito.
            <br />
            Inserendo username e password nella finestra "Login", l'utente potr�
            procedere alla compilazione della domanda di iscrizione in nome e per conto dell'impresa
            richiedente. </b>
    </p>
    <p class="DefaultPage">
        <b><span style="color: red">I DATI INSERITI NELLA DOMANDA DI ISCRIZIONE TELEMATICA NON
            CONFERMATA ANDRANNO IRRIMEDIABILMENTE PERSI.</span></b>
    </p>
    <p class="DefaultPage">
        Una volta confermata la domanda, si ricorda che la stessa, per essere considerata completata, 
        dovr� essere trasmessa firmata dal legale rappresentante dell�impresa richiedente via Posta Elettronica Certificata, 
        unitamente ai seguenti allegati:
            <ul>
                <li>schema di consenso al trattamento dei dati sottoscritto dal legale rappresentante dell�impresa;</li>
                <li>originale del certificato d�iscrizione al Registro delle Imprese non anteriore ai 6 mesi dalla data della domanda di iscrizione;</li>
                <li>autocertificazione della comunicazione antimafia firmata dal legale rappresentante dell�impresa.</li>
            </ul>
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
