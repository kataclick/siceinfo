using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.IscrizioneCE.Business;
using TBridge.Cemi.IscrizioneCE.Type.Entities;
using TBridge.Cemi.IscrizioneCE.Type.Enums;
using TBridge.Cemi.IscrizioneCE.Type.Filters;

public partial class IscrizioneCE_GestioneDomandeConsulente : Page
{
    private const int INDICECANCELLAZIONE = 8;
    private const int INDICECODICE = 1;
    private const int INDICEMODIFICA = 7;
    private const int INDICEMODULO = 9;
    private const int INDICESTATO = 6;

    private readonly IscrizioniManager biz = new IscrizioniManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEIscrizione,
                                              "~/IscrizioneCE/IscrizioneCEGestioneDomandeConsulente.aspx");

        if (GestioneUtentiBiz.IsConsulente())
        {
            // Gestione domande per il consulente
            if (!Page.IsPostBack)
            {
                GridViewDomande.PageIndex = 0;

                CaricaDomande();
            }
        }
    }

    private void CaricaDomande()
    {
        if (Request.QueryString["modalita"] != null)
        {
            if (Request.QueryString["modalita"] == "daConfermare")
            {
                // Carico le domande inserite dal consulente non ancora confermate
                CaricaDomandeDaConfermare();
            }
            else if (Request.QueryString["modalita"] == "confermate")
            {
                // Carico le domande inserite dal consulente precedentemente confermate
                CaricaDomandeConfermate();
            }
        }
    }

    private void CaricaDomandeConfermate()
    {
        ModuloIscrizioneFilter filtro = new ModuloIscrizioneFilter();

        if (GestioneUtentiBiz.IsConsulente())
        {
            //Consulente consulente =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Consulente) HttpContext.Current.User.Identity).Entity;
            Consulente consulente =
                (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            filtro.IdConsulente = consulente.IdConsulente;
        }

        GridViewDomande.Columns[INDICEMODIFICA].Visible = false;
        GridViewDomande.Columns[INDICECANCELLAZIONE].Visible = false;

        if (!string.IsNullOrEmpty(TextBoxRicercaRagioneSociale.Text))
            filtro.RagioneSociale = TextBoxRicercaRagioneSociale.Text;

        filtro.Confermate = true;
        filtro.CompilateConsulente = true;

        GridViewDomande.DataSource = biz.GetDomande(filtro, null);
        GridViewDomande.DataBind();
    }

    private void CaricaDomandeDaConfermare()
    {
        ModuloIscrizioneFilter filtro = new ModuloIscrizioneFilter();

        GridViewDomande.Columns[INDICEMODULO].Visible = false;
        GridViewDomande.Columns[INDICESTATO].Visible = false;
        GridViewDomande.Columns[INDICECODICE].Visible = false;

        if (GestioneUtentiBiz.IsConsulente())
        {
            //Consulente consulente =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Consulente) HttpContext.Current.User.Identity).Entity;
            Consulente consulente =
                (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            filtro.IdConsulente = consulente.IdConsulente;
        }

        if (!string.IsNullOrEmpty(TextBoxRicercaRagioneSociale.Text))
            filtro.RagioneSociale = TextBoxRicercaRagioneSociale.Text;

        filtro.Confermate = false;
        filtro.CompilateConsulente = true;

        GridViewDomande.DataSource = biz.GetDomande(filtro, null);
        GridViewDomande.DataBind();
    }

    protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ModuloIscrizione modulo = (ModuloIscrizione) e.Row.DataItem;
            Label lStato = (Label) e.Row.FindControl("LabelStato");

            if (modulo.Stato.HasValue)
            {
                switch (modulo.Stato)
                {
                    case StatoDomanda.Accettata:
                        lStato.Text = "Accettata";
                        e.Row.Cells[INDICEMODULO].Enabled = false;
                        break;
                    case StatoDomanda.Rifiutata:
                        lStato.Text = "Rifiutata";
                        e.Row.Cells[INDICEMODULO].Enabled = false;
                        break;
                    case StatoDomanda.DaValutare:
                        lStato.Text = "In carico";
                        e.Row.Cells[INDICEMODULO].Enabled = true;
                        break;
                    case StatoDomanda.SospesaAttesaIntegrazione:
                        lStato.Text = "In attesa di integrazione";
                        e.Row.Cells[INDICEMODULO].Enabled = true;
                        break;
                    case StatoDomanda.SospesaDebiti:
                        lStato.Text = "Sospesa";
                        e.Row.Cells[INDICEMODULO].Enabled = true;
                        break;
                }
            }
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        GridViewDomande.PageIndex = 0;
        CaricaDomande();
    }

    protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewDomande.PageIndex = e.NewPageIndex;
        CaricaDomande();
    }

    protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int? idDomanda = (int?) GridViewDomande.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];
        TipoModulo tipoModulo = (TipoModulo) GridViewDomande.DataKeys[e.NewSelectedIndex].Values["TipoModulo"];

        if (idDomanda.HasValue)
        {
            Session["IdDomanda"] = idDomanda.Value;

            StringBuilder urlRedir = new StringBuilder();
            urlRedir.Append("~/IscrizioneCE/IscrizioneCE.aspx?modalita=modifica&tipoModulo=");
            if (tipoModulo == TipoModulo.Operai)
                urlRedir.Append("normale");
            else if (tipoModulo == TipoModulo.Impiegati)
                urlRedir.Append("prevedi");
            else
                urlRedir.Append("unico");

            Response.Redirect(urlRedir.ToString());
        }
    }

    protected void GridViewDomande_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int? idDomanda = (int?) GridViewDomande.DataKeys[e.RowIndex].Values["IdDomanda"];

        if (idDomanda.HasValue)
        {
            if (!biz.DeleteDomanda(idDomanda.Value))
                LabelErrore.Text = "Errore durante la cancellazione";
            else
            {
                LabelErrore.Text = string.Empty;
                CaricaDomande();
            }
        }
    }

    protected void GridViewDomande_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "modulo")
        {
            int indice = Int32.Parse(e.CommandArgument.ToString());
            int? idDomanda = (int?) GridViewDomande.DataKeys[indice].Values["IdDomanda"];
            TipoModulo tipoModulo = (TipoModulo) GridViewDomande.DataKeys[indice].Values["TipoModulo"];

            if (idDomanda.HasValue)
            {
                Session["IdDomanda"] = idDomanda.Value;

                StringBuilder urlRedir = new StringBuilder();
                urlRedir.Append("~/ReportIscrizioneCE.aspx?tipoModulo=");
                if (tipoModulo == TipoModulo.Operai)
                    urlRedir.Append("normale");
                else if (tipoModulo == TipoModulo.Impiegati)
                    urlRedir.Append("prevedi");
                else
                    urlRedir.Append("unico");

                // Redireziono su pagina di avvenuta comunicazione
                Response.Redirect(urlRedir.ToString());
            }
        }
    }
}