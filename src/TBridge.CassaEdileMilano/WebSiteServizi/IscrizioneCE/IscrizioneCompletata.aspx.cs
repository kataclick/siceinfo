﻿using System;
using System.Web.UI;

public partial class IscrizioneCE_IscrizioneCompletata : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonStampa_Click(object sender, EventArgs e)
    {
        string tipoModulo = Request.QueryString["tipoModulo"];
        Server.Transfer(String.Format("~/ReportIscrizioneCE.aspx?tipoModulo={0}", tipoModulo));
    }
}