﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IscrizioneCompletata.aspx.cs" Inherits="IscrizioneCE_IscrizioneCompletata" %>

<%@ Register Src="../WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa modulo"
        titolo="Iscrizione imprese" />
    <br />
    La procedura di richiesta di iscrizione è stata <b>completata correttamente</b>.
    E' possibile stampare direttamente la domanda cliccando su "<b>Stampa</b>".
    <br />
    <br />
    In aggiunta, la domanda verrà inviata all'indirizzo e-mail specificato.
    <br />
    <br />
    <asp:Button 
        ID="ButtonStampa" 
        runat="server" 
        OnClick="ButtonStampa_Click" 
        Text="Stampa"
        Width="150px" />
    <br />
</asp:Content>
