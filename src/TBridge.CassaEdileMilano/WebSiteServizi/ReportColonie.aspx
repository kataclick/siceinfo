<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportColonie.aspx.cs" Inherits="ReportColonie" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Check List partenza"
        titolo="Colonie" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:reportviewer id="ReportViewerColonie" runat="server"
        processingmode="Remote" width="100%"></rsweb:reportviewer>
        </td></tr></table>
    <br />
</asp:Content>

