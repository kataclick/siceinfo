﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RichiestaBollettinoFrecciaDettaglioReport.aspx.cs" Inherits="RichiestaBollettinoFrecciaDettaglioReport" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa Dettaglio"
        titolo="Bollettino Freccia - MAV" />
    <br />
    <br />
    <rsweb:ReportViewer ID="ReportViewerDettaglio" runat="server" ProcessingMode="Remote" Height="550pt" Width="550pt" ShowParameterPrompts="false" />
</asp:Content>
<asp:Content ID="Content6" runat="server" contentplaceholderid="MenuDettaglio">
</asp:Content>


