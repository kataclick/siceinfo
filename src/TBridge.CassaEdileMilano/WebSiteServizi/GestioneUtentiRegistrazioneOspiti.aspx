<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneOspiti.aspx.cs" Inherits="GestioneUtentiRegistrazioneOspiti" %>

<%@ Register Src="WebControls/GestioneRegistrazione.ascx" TagName="GestioneRegistrazione"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/RegistrazioneOspite.ascx" TagName="RegistrazioneOspite"
    TagPrefix="uc1" %>
    
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>

<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc4" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo2"  titolo="Gestione utenti" sottoTitolo="Registrazione ospite" runat="server" />
    <br />
    <uc2:GestioneRegistrazione ID="GestioneRegistrazione1" runat="server" Visible="false" />
    <br />
    <uc1:RegistrazioneOspite ID="RegistrazioneOspite1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc4:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
