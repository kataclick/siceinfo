<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EstrazioneAssimpredil.aspx.cs" Inherits="NotifichePreliminari_EstrazioneAssimpredil" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register src="../WebControls/MenuCpt.ascx" tagname="MenuCpt" tagprefix="uc2" %>
<%@ Register src="../WebControls/MenuNotifichePreliminari.ascx" tagname="MenuNotifichePreliminari" tagprefix="uc3" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Estrazione Assimpredil"
        titolo="Notifiche Preliminari" />
    <br />
    <b>
        Milano
    </b>
    <br />
    <asp:Label ID="LabelUltimoFileMilano" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownloadMilano" runat="server" OnClick="ButtonDownloadMilano_Click" Text="Download" />
    <br /><br />
    <b>
        Lodi
    </b>
    <br />
    <asp:Label ID="LabelUltimoFileLodi" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownloadLodi" runat="server" OnClick="ButtonDownloadLodi_Click" Text="Download" />
    <br /><br />
    <b>
        Monza Brianza
    </b>
    <br />
    <asp:Label ID="LabelUltimoFileMonzaBrianza" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownloadMonzaBrianza" runat="server" OnClick="ButtonDownloadMonzaBrianza_Click" Text="Download" />
</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="MenuDettaglio">
    <uc3:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>

