﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class NotifichePreliminari_NotifichePreliminariDefault : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaNotifiche);
        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri);
        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion
    }
}