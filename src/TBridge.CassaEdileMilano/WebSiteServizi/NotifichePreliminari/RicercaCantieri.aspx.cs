﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeOpenXml;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;
using Cemi.NotifichePreliminari.Types.Entities;
using TBridge.Cemi.Presenter;
using Cemi.NotifichePreliminari.Business;
using Cemi.NotifichePreliminari.Types.Filters;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Enums;
using Cemi.NotifichePreliminari.Types.Collections;
using System.IO;

public partial class NotifichePreliminari_RicercaCantieri : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager biz = new NotifichePreliminariManager();
    private readonly CptBusiness oldBiz = new CptBusiness();
    private const Int32 INDEXIMPRESA = 1;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariEstrazioneExcelCantieri))
            {
                ButtonEstrazione.Visible = true;
            }
            else
            {
                ButtonEstrazione.Visible = false;
            }
        }
    }

    protected void ButtonCerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            NotificaPreliminareFilter filtro = null;
            CantiereCollection cantieri;
            if (RadioButtonListOrdinamento.SelectedValue == "C")
            {
                RadGridCantieriPerCommittente.Visible = true;
                RadGridCantieriPerImpresa.Visible = false;
                
                filtro = CaricaCantieriPerCommittente(out cantieri);

            }
            else
            {
                RadGridCantieriPerCommittente.Visible = false;
                RadGridCantieriPerImpresa.Visible = true;
                //CantiereCollection cantieri;
                filtro = CaricaCantieriPerImpresa(out cantieri);
            }

            ButtonEstrazione.Enabled = (cantieri.Count > 0);

            // Log della ricerca
            if (filtro != null)
            {
                LogRicerca log = new LogRicerca();
                log.IdUtente = GestioneUtentiBiz.GetIdUtente();
                log.XmlFiltro = oldBiz.GetPropertiesDelFiltro(filtro);
                log.Sezione = SezioneLogRicerca.RicercaCantieri;

                oldBiz.InsertLogRicerca(log);
            }
        }
    }

    private NotificaPreliminareFilter CaricaCantieriPerCommittente(out CantiereCollection cantieri)
    {
        NotificaPreliminareFilter filtro = NotifichePreliminariRicercaNotificheFilter1.GetFilter();

        if (!String.IsNullOrWhiteSpace(filtro.ImpresaRagioneSociale)
                || !String.IsNullOrWhiteSpace(filtro.ImpresaCfPIva)
                || !String.IsNullOrWhiteSpace(filtro.ImpresaProvincia)
                || !String.IsNullOrWhiteSpace(filtro.ImpresaCap))
        {
            RadGridCantieriPerCommittente.Columns[INDEXIMPRESA].Visible = true;
        }

        cantieri = biz.RicercaCantieriPerCommittenteByFilter(filtro);

        Presenter.CaricaElementiInGridView(
            RadGridCantieriPerCommittente,
            cantieri);

        return filtro;
    }

    private NotificaPreliminareFilter CaricaCantieriPerImpresa(out CantiereCollection cantieri)
    {
        NotificaPreliminareFilter filtro = NotifichePreliminariRicercaNotificheFilter1.GetFilter();

        cantieri = biz.RicercaCantieriPerImpresaByFilter(filtro);

        Presenter.CaricaElementiInGridView(
            RadGridCantieriPerImpresa,
            cantieri);

        return filtro;
    }

    protected void RadGridCantieriPerCommittente_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Cantiere cantiere = (Cantiere) e.Item.DataItem;

            Label lCommittenteCognomeNome = (Label) e.Item.FindControl("LabelCommittenteCognomeNome");
            Label lCommittenteCodiceFiscale = (Label) e.Item.FindControl("LabelCommittenteCodiceFiscale");
            Label lIndirizzo = (Label) e.Item.FindControl("LabelIndirizzo");
            Label lComune = (Label) e.Item.FindControl("LabelComune");
            Label lDataInizio = (Label) e.Item.FindControl("LabelDataInizio");
            Label lDurata = (Label) e.Item.FindControl("LabelDurata");
            Label lNaturaOperaCategoria = (Label) e.Item.FindControl("LabelNaturaOperaCategoria");
            Label lNaturaOperaTipologia = (Label) e.Item.FindControl("LabelNaturaOperaTipologia");
            Label lImpresaRagioneSociale = (Label) e.Item.FindControl("LabelImpresaRagioneSociale");
            Label lImpresaCodiceFiscale = (Label) e.Item.FindControl("LabelImpresaCodiceFiscale");

            lCommittenteCognomeNome.Text = cantiere.Committente;
            lCommittenteCodiceFiscale.Text = cantiere.CommittenteCodiceFiscale;
            lImpresaRagioneSociale.Text = cantiere.ImpresaRagioneSociale;
            lImpresaCodiceFiscale.Text = cantiere.ImpresaCodiceFiscale;
            lIndirizzo.Text = Presenter.NormalizzaCampoTesto(cantiere.Indirizzo.NomeVia);
            lComune.Text = Presenter.NormalizzaCampoTesto(cantiere.Indirizzo.Comune);
            if (cantiere.DataInizio.HasValue)
            {
                lDataInizio.Text = cantiere.DataInizio.Value.ToShortDateString();
            }
            lDurata.Text = Presenter.NormalizzaCampoTesto(cantiere.DurataCompleta);
            lNaturaOperaCategoria.Text = Presenter.NormalizzaCampoTesto(cantiere.Categoria);
            lNaturaOperaTipologia.Text = Presenter.NormalizzaCampoTesto(cantiere.Tipologia);
        }
    }

    protected void RadGridCantieriPerCommittente_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CantiereCollection cantieri;
        CaricaCantieriPerCommittente(out cantieri);
    }

    protected void RadGridCantieriPerCommittente_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            Int32 index = e.Item.ItemIndex;
            Context.Items["NumeroNotifica"] = RadGridCantieriPerCommittente.MasterTableView.DataKeyValues[index]["NumeroNotifica"];
            Server.Transfer("~/NotifichePreliminari/DettaglioNotifica.aspx");
        }
    }

    protected void ButtonEstrazione_Click(object sender, EventArgs e)
    {
        CantiereCollection cantieri;
        if (RadioButtonListOrdinamento.SelectedValue == "C")
        {
            CaricaCantieriPerCommittente(out cantieri);
        }
        else
        {
            CaricaCantieriPerImpresa(out cantieri);
        }

        /*
        string stampa = PreparaStampa(cantieri);

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Cantieri.xls");
        Response.ContentType = "application/vnd.ms-excel";
        Response.Write(stampa);
        Response.End();
        */

        #region New Estrazione Excel

        GridView gv = new GridView();
        gv.ID = "gvCantieri";
        gv.AutoGenerateColumns = false;


        if (RadioButtonListOrdinamento.SelectedValue == "C")
        {
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Committente";
            bc1.DataField = "Committente";

            BoundField bc12 = new BoundField();
            bc12.HeaderText = "Codice Fiscale Committente";
            bc12.DataField = "CommittenteCodiceFiscale";

            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Impresa cercata";
            bc2.DataField = "ImpresaRagioneSociale";

            BoundField bc21 = new BoundField();
            bc21.HeaderText = "Impresa cercata partita IVA";
            bc21.DataField = "ImpresaCodiceFiscale";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc12);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc21);
        }
        else
        {
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Impresa";
            bc1.DataField = "ImpresaRagioneSociale";

            BoundField bc12 = new BoundField();
            bc12.HeaderText = "Impresa codice fiscale";
            bc12.DataField = "ImpresaCodiceFiscale";

            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Committente";
            bc2.DataField = "Committente";

            BoundField bc21 = new BoundField();
            bc21.HeaderText = "Codice Fiscale Committente";
            bc21.DataField = "CommittenteCodiceFiscale";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc12);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc21);
        }

        BoundField bc31 = new BoundField();
        bc31.HeaderText = "Indirizzo";
        bc31.DataField = "IndirizzoBase";
        BoundField bc32 = new BoundField();
        bc32.HeaderText = "Comune";
        bc32.DataField = "Comune";
        BoundField bc33 = new BoundField();
        bc33.HeaderText = "Provincia";
        bc33.DataField = "Provincia";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Data inizio lavori";
        bc4.DataField = "DataInizio";
        bc4.HtmlEncode = false;
        bc4.DataFormatString = "{0:dd/MM/yyyy}";
        BoundField bc5 = new BoundField();
        bc5.HeaderText = "Durata";
        bc5.DataField = "DurataCompleta";
        BoundField bc6 = new BoundField();
        bc6.HeaderText = "Categoria";
        bc6.DataField = "Categoria";
        BoundField bc61 = new BoundField();
        bc61.HeaderText = "Tipologia";
        bc61.DataField = "Tipologia";
        BoundField bc7 = new BoundField();
        bc7.HeaderText = "Numero previsto imprese";
        bc7.DataField = "NumeroImprese";
        BoundField bc8 = new BoundField();
        bc8.HeaderText = "Numero massimo lavoratori";
        bc8.DataField = "NumeroLavoratori";
        BoundField bc9 = new BoundField();
        bc9.HeaderText = "Ammontare";
        bc9.DataField = "AmmontareComplessivo";
        BoundField bc10 = new BoundField()
        {
            HeaderText = "Protocollo Regionale",
            DataField = "NumeroNotifica",
            //DataFormatString =  "'{0}'" ,
        };


        gv.Columns.Add(bc31);
        gv.Columns.Add(bc32);
        gv.Columns.Add(bc33);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc6);
        gv.Columns.Add(bc61);
        gv.Columns.Add(bc7);
        gv.Columns.Add(bc8);
        gv.Columns.Add(bc9);
        gv.Columns.Add(bc10);

        gv.DataSource = cantieri;
        gv.DataBind();

        EstraiExcel(gv,"Cantieri");


        
        #endregion
    }

    private void EstraiExcel(GridView gv, String NomeExcel)
    {
        ExcelPackage excel = new ExcelPackage();
        var workSheet = excel.Workbook.Worksheets.Add(NomeExcel);
        var totalCols = gv.Rows[0].Cells.Count;
        var totalRows = gv.Rows.Count;
        var headerRow = gv.HeaderRow;
        for (var i = 1; i <= totalCols; i++)
        {
            workSheet.Cells[1, i].Value = Server.HtmlDecode(headerRow.Cells[i - 1].Text);
            workSheet.Cells[1, i].Style.Font.Bold = true;
            workSheet.Cells[1, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
        }
        for (var j = 1; j <= totalRows; j++)
        {
            for (var i = 1; i <= totalCols; i++)
            {
                GridViewRow row = gv.Rows[j - 1];
                workSheet.Cells[j + 1, i].Value = Server.HtmlDecode(row.Cells[i - 1].Text);
            }
        }

        using (var memoryStream = new MemoryStream())
        {
            Response.ClearContent();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            String ne = String.Format("attachment;  filename={0}.xlsx", NomeExcel);
            Response.AddHeader("content-disposition", ne);
            excel.SaveAs(memoryStream);
            memoryStream.WriteTo(Response.OutputStream);
            Response.Flush();
            Response.End();
        }
    }

    private String PreparaStampa(CantiereCollection cantieri)
    {
        GridView gv = new GridView();
        gv.ID = "gvCantieri";
        gv.AutoGenerateColumns = false;


        if (RadioButtonListOrdinamento.SelectedValue == "C")
        {
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Committente";
            bc1.DataField = "Committente";

            BoundField bc12 = new BoundField();
            bc12.HeaderText = "Codice Fiscale Committente";
            bc12.DataField = "CommittenteCodiceFiscale";

            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Impresa cercata";
            bc2.DataField = "ImpresaRagioneSociale";

            BoundField bc21 = new BoundField();
            bc21.HeaderText = "Impresa cercata partita IVA";
            bc21.DataField = "ImpresaCodiceFiscale";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc12);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc21);
        }
        else
        {
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Impresa";
            bc1.DataField = "ImpresaRagioneSociale";

            BoundField bc12 = new BoundField();
            bc12.HeaderText = "Impresa codice fiscale";
            bc12.DataField = "ImpresaCodiceFiscale";

            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Committente";
            bc2.DataField = "Committente";

            BoundField bc21 = new BoundField();
            bc21.HeaderText = "Codice Fiscale Committente";
            bc21.DataField = "CommittenteCodiceFiscale";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc12);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc21);
        }

        BoundField bc31 = new BoundField();
        bc31.HeaderText = "Indirizzo";
        bc31.DataField = "IndirizzoBase";
        BoundField bc32 = new BoundField();
        bc32.HeaderText = "Comune";
        bc32.DataField = "Comune";
        BoundField bc33 = new BoundField();
        bc33.HeaderText = "Provincia";
        bc33.DataField = "Provincia";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Data inizio lavori";
        bc4.DataField = "DataInizio";
        bc4.HtmlEncode = false;
        bc4.DataFormatString = "{0:dd/MM/yyyy}";
        BoundField bc5 = new BoundField();
        bc5.HeaderText = "Durata";
        bc5.DataField = "DurataCompleta";
        BoundField bc6 = new BoundField();
        bc6.HeaderText = "Categoria";
        bc6.DataField = "Categoria";
        BoundField bc61 = new BoundField();
        bc61.HeaderText = "Tipologia";
        bc61.DataField = "Tipologia";
        BoundField bc7 = new BoundField();
        bc7.HeaderText = "Numero previsto imprese";
        bc7.DataField = "NumeroImprese";
        BoundField bc8 = new BoundField();
        bc8.HeaderText = "Numero massimo lavoratori";
        bc8.DataField = "NumeroLavoratori";
        BoundField bc9 = new BoundField();
        bc9.HeaderText = "Ammontare";
        bc9.DataField = "AmmontareComplessivo";
        BoundField bc10 = new BoundField()
        {
            HeaderText = "Protocollo Regionale",
            DataField = "NumeroNotifica",
            //DataFormatString =  "'{0}'" ,
        };


        gv.Columns.Add(bc31);
        gv.Columns.Add(bc32);
        gv.Columns.Add(bc33);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc6);
        gv.Columns.Add(bc61);
        gv.Columns.Add(bc7);
        gv.Columns.Add(bc8);
        gv.Columns.Add(bc9);
        gv.Columns.Add(bc10);

        gv.DataSource = cantieri;
        gv.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw.ToString();
    }



    protected void RadGridCantieriPerImpresa_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            Int32 index = e.Item.ItemIndex;
            Context.Items["NumeroNotifica"] = RadGridCantieriPerImpresa.MasterTableView.DataKeyValues[index]["NumeroNotifica"];
            Server.Transfer("~/NotifichePreliminari/DettaglioNotifica.aspx");
        }
    }

    protected void RadGridCantieriPerImpresa_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Cantiere cantiere = (Cantiere) e.Item.DataItem;

            Label lCommittenteCognomeNome = (Label) e.Item.FindControl("LabelCommittenteCognomeNome");
            Label lCommittenteCodiceFiscale = (Label) e.Item.FindControl("LabelCommittenteCodiceFiscale");
            Label lIndirizzo = (Label) e.Item.FindControl("LabelIndirizzo");
            Label lComune = (Label) e.Item.FindControl("LabelComune");
            Label lDataInizio = (Label) e.Item.FindControl("LabelDataInizio");
            Label lDurata = (Label) e.Item.FindControl("LabelDurata");
            Label lNaturaOperaCategoria = (Label) e.Item.FindControl("LabelNaturaOperaCategoria");
            Label lNaturaOperaTipologia = (Label) e.Item.FindControl("LabelNaturaOperaTipologia");
            Label lImpresaRagioneSociale = (Label) e.Item.FindControl("LabelImpresaRagioneSociale");
            Label lImpresaCodiceFiscale = (Label) e.Item.FindControl("LabelImpresaCodiceFiscale");

            lCommittenteCognomeNome.Text = cantiere.Committente;
            lCommittenteCodiceFiscale.Text = cantiere.CommittenteCodiceFiscale;
            lImpresaRagioneSociale.Text = cantiere.ImpresaRagioneSociale;
            lImpresaCodiceFiscale.Text = cantiere.ImpresaCodiceFiscale;
            lIndirizzo.Text = Presenter.NormalizzaCampoTesto(cantiere.Indirizzo.NomeVia);
            lComune.Text = Presenter.NormalizzaCampoTesto(cantiere.Indirizzo.Comune);
            if (cantiere.DataInizio.HasValue)
            {
                lDataInizio.Text = cantiere.DataInizio.Value.ToShortDateString();
            }
            lDurata.Text = Presenter.NormalizzaCampoTesto(cantiere.DurataCompleta);
            lNaturaOperaCategoria.Text = Presenter.NormalizzaCampoTesto(cantiere.Categoria);
            lNaturaOperaTipologia.Text = Presenter.NormalizzaCampoTesto(cantiere.Tipologia);
        }
    }

    protected void RadGridCantieriPerImpresa_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CantiereCollection cantieri;
        CaricaCantieriPerImpresa(out cantieri);
    }
}
