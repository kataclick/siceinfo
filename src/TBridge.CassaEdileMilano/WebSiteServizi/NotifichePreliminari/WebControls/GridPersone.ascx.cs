﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.NotifichePreliminari.Types.Collections;
using TBridge.Cemi.Presenter;

public partial class NotifichePreliminari_WebControls_GridPersone : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void RadGridPersone_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaPersone();
    }

    protected void RadGridPersone_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            // Dichiarazione dichiarazione = (Dichiarazione)e.Item.DataItem;

            Persona persona = (Persona)e.Item.DataItem;

            Label labCognomeNome = (Label)e.Item.FindControl("labelCognomeNome");
            Label labCf = (Label)e.Item.FindControl("labelCodiceFiscale");

            labCognomeNome.Text = persona.Nominativo;
            labCf.Text = persona.CodiceFiscale;
        }
    }

    private void CaricaPersone()
    {
        //TODO
    }

    public void CaricaPersone(PersonaCollection persone)
    {
        Presenter.CaricaElementiInGridView(RadGridPersone, persone);
    }
}