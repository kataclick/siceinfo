﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Types.Filters;
using TBridge.Cemi.Presenter;
using Cemi.NotifichePreliminari.Business;
using Telerik.Web.UI;


public partial class NotifichePreliminari_WebControls_NotifichePreliminariRicercaNotificheFilter : System.Web.UI.UserControl
{
    private NotifichePreliminariManager _biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaComboBoxes();
        }
    }

    public NotificaPreliminareFilter GetFilter()
    {
        NotificaPreliminareFilter filter = new NotificaPreliminareFilter();
        if (!String.IsNullOrEmpty(TextBoxAmmontare.Text))
        {
            filter.Ammontare = Decimal.Parse(TextBoxAmmontare.Text);
        }
        filter.NaturaOpera = TextBoxNaturaOpera.Text;
        filter.NumeroNotifica = TextBoxNumeroNotifica.Text;
        filter.CommittenteCognome = textBoxCommittenteCognome.Text;
        filter.CommittenteNome = textBoxCommittenteNome.Text;
        filter.CommittenteCodiceFiscale = textBoxCommittenteCodFiscale.Text;

        filter.CantiereIndirizzo = TextBoxIndirizzoIndirizzo.Text;
        filter.CantiereComune = TextBoxIndirizzoComune.Text;
        if (RadComboBoxIndirizzoProvincia.SelectedItem != null && RadComboBoxIndirizzoProvincia.SelectedIndex != 0)
        {
            filter.CantiereProvncia = RadComboBoxIndirizzoProvincia.SelectedValue;
        }

        filter.ImpresaCap = TextBoxImpresaCap.Text;
        filter.ImpresaCfPIva = TextBoxImpresaIvaFisc.Text;
        filter.ImpresaRagioneSociale = TextBoxImpresaRagSoc.Text;
        if (RadComboBoxImpresaProvincia.SelectedItem != null && RadComboBoxImpresaProvincia.SelectedIndex != 0)
        {
            filter.ImpresaProvincia = RadComboBoxImpresaProvincia.SelectedValue;
        }

        if (!String.IsNullOrEmpty(TextBoxAl.Text))
        {
            filter.DataComunicazioneAl = DateTime.ParseExact(TextBoxAl.Text, "ddMMyyyy", null).Date;
        }

        if (!String.IsNullOrEmpty(TextBoxDal.Text))
        {
            filter.DataComunicazioneDal = DateTime.ParseExact(TextBoxDal.Text, "ddMMyyyy", null).Date;
        }

        if (!String.IsNullOrEmpty(TextBoxInizioLavoriAl.Text))
        {
            filter.DataInizioLavoriAl = DateTime.ParseExact(TextBoxInizioLavoriAl.Text, "ddMMyyyy", null).Date; 
        }

        if (!String.IsNullOrEmpty(TextBoxInizioLavoriDal.Text))
        {
            filter.DataInizioLavoriDal = DateTime.ParseExact(TextBoxInizioLavoriDal.Text, "ddMMyyyy", null).Date; 
        }

        return filter;
    }

    private void CaricaComboBoxes()
    {
        if (RadComboBoxImpresaProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(RadComboBoxImpresaProvincia);
            RadComboBoxImpresaProvincia.Items.Insert(0, new RadComboBoxItem("-"));

            Presenter.CaricaProvince(RadComboBoxIndirizzoProvincia);
            RadComboBoxIndirizzoProvincia.Items.Insert(0, new RadComboBoxItem("-"));
        }
        
        
    }


}