﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridImprese.ascx.cs" Inherits="NotifichePreliminari_WebControls_GridImprese" %>

    <telerik:RadGrid ID="RadGridImprese" runat="server" Width="100%"
        OnPageIndexChanged="RadGridImprese_PageIndexChanged" GridLines="None"
        OnItemDataBound="RadGridImprese_ItemDataBound" CellSpacing="0" ShowFooter="false"
    >
        <MasterTableView >
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <%--<telerik:GridBoundColumn HeaderText="ID" UniqueName="NumeroNotifica" DataField="NumeroNotifica">
                    <ItemStyle Width="40px" />
                </telerik:GridBoundColumn>--%>
                <%--   <telerik:GridBoundColumn HeaderText="Attività" UniqueName="Attività" 
                    DataField="Attivita">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn HeaderText="Incarico" UniqueName="Incarico" DataField="TipoIncaricoDescrizione" ItemStyle-Width="30%" />
                <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                    <ItemTemplate>
                        <table>
                            <tr>
                                               
                                <td>
                                   <b> <asp:Label ID="labelRagioneSociale" runat="server" /> </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="labelCodiceFiscale" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="labelPartitaIva" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="40%" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Indirizzo" UniqueName="Indirizzo" DataField="IndirizzoCompleto"
                    >
                    <ItemStyle Width="30%" />
                </telerik:GridBoundColumn>
                                
               
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>