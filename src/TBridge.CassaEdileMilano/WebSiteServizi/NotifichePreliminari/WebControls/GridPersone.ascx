﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GridPersone.ascx.cs" Inherits="NotifichePreliminari_WebControls_GridPersone" %>

<telerik:RadGrid ID="RadGridPersone" runat="server" Width="100%"
                        OnPageIndexChanged="RadGridPersone_PageIndexChanged" GridLines="None"
                        OnItemDataBound="RadGridPersone_ItemDataBound" CellSpacing="0" ShowFooter="false" 
                       >
                        <MasterTableView >
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <%--<telerik:GridBoundColumn HeaderText="ID" UniqueName="NumeroNotifica" DataField="NumeroNotifica">
                                    <ItemStyle Width="40px" />
                                </telerik:GridBoundColumn>--%>
                                <%--   <telerik:GridBoundColumn HeaderText="Attività" UniqueName="Attività" 
                                    DataField="Attivita">
                                    <ItemStyle Width="50px" />
                                </telerik:GridBoundColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Ruolo" UniqueName="Ruolo" DataField="RuoloDescrizione" ItemStyle-Width="30%"/>
                                <telerik:GridTemplateColumn HeaderText="Persona" UniqueName="Persona">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <b><asp:Label ID="labelCognomeNome" runat="server" /></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td>
                                                    <asp:Label ID="labelCodiceFiscale" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <ItemStyle Width="40%" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn HeaderText="Indirizzo" UniqueName="Indirizzo" DataField="IndirizzoCompleto"
                                   >
                                    <ItemStyle Width="30%" />
                                </telerik:GridBoundColumn>
                                
                                <%--<telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore">
                                    <ItemTemplate>
                                        <b>
                                            <asp:Label ID="LabelLavoratoreCognomeNome" runat="server">
                                            </asp:Label>
                                        </b>
                                        <br />
                                        <small>
                                            <asp:Label ID="LabelLavoratoreDataNascita" runat="server">
                                            </asp:Label>
                                            <br />
                                            <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server">
                                            </asp:Label>
                                        </small>
                                    </ItemTemplate>
                                    <ItemStyle Width="190px" />
                                </telerik:GridTemplateColumn>--%>
                               <%-- <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                                    <ItemTemplate>
                                        <b>
                                            <asp:Label ID="LabelImpresaCodiceRagioneSociale" runat="server">
                                            </asp:Label>
                                        </b>
                                        <br />
                                        <small>
                                            <asp:Label ID="LabelImpresaPartitaIva" runat="server">
                                            </asp:Label>
                                            <br />
                                            <asp:Label ID="LabelImpresaCodiceFiscale" runat="server">
                                            </asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Stato" UniqueName="Stato">
                                    <ItemTemplate>
                                        <b>
                                            <asp:Label ID="LabelStato" runat="server">
                                            </asp:Label>
                                        </b>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" />
                                </telerik:GridTemplateColumn>--%>
                             <%--   <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Text="Dettaglio"
                                    UniqueName="column">
                                    <ItemStyle Width="10px" />
                                </telerik:GridButtonColumn>--%>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>