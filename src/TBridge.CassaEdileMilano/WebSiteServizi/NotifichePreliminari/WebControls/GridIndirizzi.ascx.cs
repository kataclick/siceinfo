﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.NotifichePreliminari.Types.Collections;
using TBridge.Cemi.Presenter;
using Cemi.NotifichePreliminari.Types.Helpers;

public partial class NotifichePreliminari_WebControls_GridIndirizzi : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void RadGridIndirizzi_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaIndirizzi();
    }



    protected void RadGridIndirizzi_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            // Dichiarazione dichiarazione = (Dichiarazione)e.Item.DataItem;

            Indirizzo indirizzo = (Indirizzo)e.Item.DataItem;

            Label labIndrizzo = (Label)e.Item.FindControl("LabelIndirizzo");
            labIndrizzo.Text = HelperMethods.FormattaIndirizzo(indirizzo);
        }
    }

    private void CaricaIndirizzi()
    {
        //throw new NotImplementedException();
    }

    public void CaricaIndirizzi(IndirizzoCollection indirizzi)
    {
        Presenter.CaricaElementiInGridView(RadGridIndizziCantieri, indirizzi);
    }
}