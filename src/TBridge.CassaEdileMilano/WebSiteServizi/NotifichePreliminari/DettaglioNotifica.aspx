﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="DettaglioNotifica.aspx.cs" Inherits="NotifichePreliminari_NotifichePreliminariDettaglioNotifica" %>

<%@ Register Src="~/NotifichePreliminari/WebControls/GridPersone.ascx" TagName="GridPersone"
    TagPrefix="uc1" %>
<%@ Register Src="~/NotifichePreliminari/WebControls/GridImprese.ascx" TagName="GridImprese"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc3" %>
<%@ Register Src="~/NotifichePreliminari/WebControls/GridIndirizzi.ascx" TagName="GridIndirizzi"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuNotifichePreliminari.ascx" TagName="MenuNotifichePreliminari"
    TagPrefix="uc5" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Dettaglio notifica"
        titolo="Notifiche preliminari" />
    <br />
    <table width="100%">
        <tr>
            <td colspan="2">
                <b>Dati Notifica</b>
            </td>
        </tr>
        <tr>
            <td style="width: 30%">
                Numero:
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelNumeroNotifica" runat="server" />
                </b>
            </td>
        </tr>
        <tr>
            <td>
                Data:
            </td>
            <td>
                <asp:Label ID="LabelDataComunicazione" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Data ultimo aggiornamento:
            </td>
            <td>
                <asp:Label ID="LabelDataAggiornamento" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Categoria:
            </td>
            <td>
                <asp:Label ID="LabelCategoria" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                Tipologia:
            </td>
            <td>
                <asp:Label ID="LabelTipologia" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Ammontare:
            </td>
            <td>
                <asp:Label ID="LabelAmmontare" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Numero lavoratori autonomi:
            </td>
            <td>
                <asp:Label ID="LabelLavoratriAutonomi" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Numero imprese:
            </td>
            <td>
                <asp:Label ID="LabelNumeroImprese" runat="server" />
            </td>
        </tr>
        <%--<tr>
            <td>
                Articolo 9011:
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxArt9011" runat="server" Checked="false" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td>
                Articolo 9011B:
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxArt9011B" runat="server" Checked="false" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td>
                Articolo 9011C:
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxArt9011C" runat="server" Checked="false" Enabled="false" />
            </td>
        </tr>--%>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Dichiarazioni</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ListView
                    runat="server"
                    ID="ListViewDichiarazioni">
                    <LayoutTemplate>
                        <ul>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <%# Container.DataItem.ToString() %>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
            </td>
        </tr>
    </table>
    <br />
    <%--    <table width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                        <b>Indirizzi</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <uc4:GridIndirizzi ID="GridIndirizziCantieri" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">       
                    <tr>
                        <td >
                        <b>Committenti</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:GridPersone ID="GridPersoneCommittenti" runat="server"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="rowAltrePersone">
            <td>
                <table width="100%">
                     <tr> 
                        <td>
                        <b>Altre persone</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:GridPersone ID="GridPersoneAltrePersone" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            <b>Imprese</b>
            </td>
        </tr>
        <tr>
            <td>
              <uc2:GridImprese ID="GridImpreseTutte" runat="server" />
            </td>
        </tr>
    </table>--%>
    <div>
        <div>
            <b>Indirizzi</b>
            <br />
            <uc4:GridIndirizzi ID="GridIndirizziCantieri" runat="server" />
            <br />
        </div>
        <div>
            <b>Committenti</b>
            <br />
            <uc1:GridPersone ID="GridPersoneCommittenti" runat="server" />
            <br />
        </div>
        <div runat="server" id="divAltrePersone">
            <b>Altre persone</b>
            <br />
            <uc1:GridPersone ID="GridPersoneAltrePersone" runat="server" />
            <br />
        </div>
        <div>
            <b>Imprese</b>
            <br />
            <uc2:GridImprese ID="GridImpreseTutte" runat="server" />
        </div>
    </div>
    <br />
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Torna indietro" />
    <asp:Button ID="ButtonReport" runat="server" Text="Report" OnClick="ButtonReport_Click" />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>
