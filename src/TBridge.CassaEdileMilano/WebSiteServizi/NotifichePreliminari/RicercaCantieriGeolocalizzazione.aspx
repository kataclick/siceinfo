﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RicercaCantieriGeolocalizzazione.aspx.cs" Inherits="NotifichePreliminari_RicercaCantieriGeolocalizzazione"
         MasterPageFile="~/MasterPage.master" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>
<%@ Register Src="~/NotifichePreliminari/WebControls/NotifichePreliminariRicercaNotificheFilter.ascx" TagPrefix="uc1" TagName="RicercaNotificheFilter" %>

<%@ Register src="../WebControls/MenuNotifichePreliminari.ascx" tagname="MenuNotifichePreliminari" tagprefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc3:titolosottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Localizzazione cantieri"
                            titolo="Notifiche preliminari" />
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1" Width="100%">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.661048;
                var longitudine = 9.685537;
                var zoom = 8;

                function LoadMap() {
                    var latitudine = 45.661048;
                    var longitudine = 9.685537;
                    var zoom = 8;
                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }
            </script>
        </telerik:RadCodeBlock>


        <table class="borderedTable" width="100%">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                     <td colspan="2">
                         <uc1:RicercaNotificheFilter ID="UserControlRicercaNotificheFilter1" runat="server" />
                     </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryRicercaNotifiche" runat="server" 
                                                ValidationGroup="ricercaNotifiche" ForeColor="Red" />
                        </td>
                        <td align="right">
                            <asp:CustomValidator id="CustomValidatorMaxPushpin" runat="server" 
                                Display="None" EnableClientScript="False" ValidationGroup="ricercaNotifiche" 
                                ErrorMessage="I cantieri trovati sono troppi e non possono essere visualizzati sulla mappa, filtrare maggiormemte" />
                            <asp:Button ID="ButtonNotificheCerca" runat="server" Text="Cerca" Width="100px"
                                        OnClick="ButtonNotificheCerca_Click" ValidationGroup="ricercaNotifiche" />
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCantieriTrovati" Font-Size="Small" Font-Bold="true" runat="server" Text="Cantieri trovati:" Visible="False"></asp:Label> 
            </td>
        </tr>
        <tr>
            <td>
                 
                <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
                </div>
            </td>
        </tr>
    </table>
        
    </telerik:RadAjaxPanel>
</asp:Content>

<asp:Content ID="Content7" runat="server" contentplaceholderid="MenuDettaglio">
    <uc2:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>


