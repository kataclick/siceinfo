﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Enums;
using Cemi.NotifichePreliminari.Types.Filters;

public partial class NotifichePreliminari_NotifichePreliminariRicercaNotifiche : System.Web.UI.Page
{
    private readonly NotifichePreliminariManager _biz = new NotifichePreliminariManager();
    private readonly CptBusiness oldBiz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaNotifiche);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion
    }


    protected void ButtonNotificheCerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche(true);

        }
    }

    protected void RadGridNotifiche_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaNotifiche();
    }

    protected void RadGridNotifiche_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
           // Dichiarazione dichiarazione = (Dichiarazione)e.Item.DataItem;

            NotificaPreliminare notifia = (NotificaPreliminare)e.Item.DataItem;

            Label labelNumeroNotifica = (Label)e.Item.FindControl("LabelNumeroNotifica");
            Label labelDataComunicazione = (Label)e.Item.FindControl("LabelDataComunicazione");
            Label labelDataAggiornamento = (Label) e.Item.FindControl("LabelDataAggiornamento");
            Label labelCategoria = (Label)e.Item.FindControl("LabelCategoria");
            Label labelTipologia = (Label)e.Item.FindControl("LabelTipologia");
            Label labelAmmontare = (Label)e.Item.FindControl("LabelAmmontare");
            Cantieri_WebControls_CantiereDaNotificaRegionale wcCantieriDaNotifica = (Cantieri_WebControls_CantiereDaNotificaRegionale) e.Item.FindControl("CantiereDaNotificaRegionale1");

            BulletedList bulletedListIndirizzi = (BulletedList)e.Item.FindControl("BulletListIndirizzi");
            ListView listViewCommittenti = (ListView)e.Item.FindControl("ListViewDatiGeneraliIndirizzi");

            Presenter.CaricaElementiInBulletedList(bulletedListIndirizzi, notifia.Indirizzi, "IndirizzoCompleto", "IdIndirizzo");
            listViewCommittenti.DataSource = notifia.Committenti;
            listViewCommittenti.DataBind();
            labelNumeroNotifica.Text = notifia.NumeroNotifica;
            labelDataComunicazione.Text = notifia.DataComunicazioneNotifica.ToString("dd/MM/yyyy");
            if (notifia.DataAggiornamentoNotifica.HasValue)
            {
                labelDataAggiornamento.Text = notifia.DataAggiornamentoNotifica.Value.ToString("dd/MM/yyyy");
            }
            if (!String.IsNullOrWhiteSpace(notifia.TipoCategoriaDescrizioneVisualizzazione)
                && notifia.TipoCategoriaDescrizioneVisualizzazione.Length > 80)
            {
                labelCategoria.Text = String.Format("{0}...", Presenter.NormalizzaCampoTesto(notifia.TipoCategoriaDescrizioneVisualizzazione.Substring(0, 80)));
            }
            else
            {
                labelCategoria.Text = Presenter.NormalizzaCampoTesto(notifia.TipoCategoriaDescrizioneVisualizzazione);
            }
            if (!String.IsNullOrWhiteSpace(notifia.TipoTipologiaDescrizioneVisualizzazione)
                && notifia.TipoTipologiaDescrizioneVisualizzazione.Length > 80)
            {
                labelTipologia.Text = String.Format("{0}...", Presenter.NormalizzaCampoTesto(notifia.TipoTipologiaDescrizioneVisualizzazione.Substring(0, 80)));
            }
            else
            {
                labelTipologia.Text = Presenter.NormalizzaCampoTesto(notifia.TipoTipologiaDescrizioneVisualizzazione);
            }
            labelAmmontare.Text = Presenter.NormalizzaCampoTesto(notifia.AmmontareComplessivo.ToString("N2"));

            #region Cantieri generati
            wcCantieriDaNotifica.OnCantiereGenerato += new TBridge.Cemi.Cantieri.Type.Delegates.CantiereGeneratoEventHandler(wcCantieriDaNotifica_OnCantiereGenerato);

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGenerazioneDaNotifiche)
                && !String.IsNullOrEmpty(notifia.NumeroNotifica))
            {
                TBridge.Cemi.Cantieri.Type.Collections.CantiereCollection indirizzi = _biz.GetCantieriGenerati(notifia.NumeroNotifica);
                wcCantieriDaNotifica.CaricaCantieri(indirizzi, notifia.NumeroNotifica);
            }
            else
            {
                wcCantieriDaNotifica.ForzaNonVisualizzazione();
            }
            #endregion
        }
    }

    void wcCantieriDaNotifica_OnCantiereGenerato()
    {
        CaricaNotifiche();
    }

    protected void listViewCommittenti_DataBound(object sender, ListViewItemEventArgs e)
    {
      //  throw new NotImplementedException();
        //ListViewItemEventArgs
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Cemi.NotifichePreliminari.Types.Entities.Persona persona = e.Item.DataItem as Cemi.NotifichePreliminari.Types.Entities.Persona;
            Label labelNome = (Label)e.Item.FindControl("LabelCommittenteNome");
            Label labelCf = (Label)e.Item.FindControl("LabelCommittanteCf");

            labelNome.Text = persona.Nominativo;
            labelCf.Text = persona.CodiceFiscale;
        }
    }

    protected void RadGridNotifiche_ItemCommand(object sender, GridCommandEventArgs e)
    {

        if (String.Equals(e.CommandName, "GoToDettaglio", StringComparison.CurrentCultureIgnoreCase))
        {
                Int32 indiceSelezionato = (e.Item.ItemIndex);
                Context.Items["NumeroNotifica"] = RadGridNotifiche.MasterTableView.DataKeyValues[indiceSelezionato]["NumeroNotifica"];
                Server.Transfer("~/NotifichePreliminari/DettaglioNotifica.aspx");
        }

    }

    private void CaricaNotifiche()
    {
        CaricaNotifiche(false);
    }

    private void CaricaNotifiche(Boolean logFiltro)
    {
        NotificaPreliminareFilter filter = UserControlRicercaNotificheFilter1.GetFilter();
        NotificaPreliminareCollection notifiche = _biz.RicercaNotificheByFilter(filter);

        if (filter != null && logFiltro)
        {
           
                LogRicerca log = new LogRicerca();
                log.IdUtente = GestioneUtentiBiz.GetIdUtente();
                log.XmlFiltro = oldBiz.GetPropertiesDelFiltro(filter);
                log.Sezione = SezioneLogRicerca.RicercaNotifiche;

                oldBiz.InsertLogRicerca(log);
            
        }

        RadGridNotifiche.Visible = true;
        Presenter.CaricaElementiInGridView(
            RadGridNotifiche,
            notifiche);

    }
}