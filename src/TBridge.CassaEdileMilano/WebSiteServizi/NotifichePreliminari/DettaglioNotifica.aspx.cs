﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Business;
using Cemi.NotifichePreliminari.Types.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class NotifichePreliminari_NotifichePreliminariDettaglioNotifica : System.Web.UI.Page
{

    NotifichePreliminariManager _biz = new NotifichePreliminariManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaNotifiche);
        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri);
        funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        #region Autorizzazione visulizzazione altrePersone
        divAltrePersone.Visible = TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariDatiPersone);
        #endregion

        ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

        if (!Page.IsPostBack)
        {
            String numNotifica;
            if (Context.Items["NumeroNotifica"] != null)
            {
                numNotifica = Context.Items["NumeroNotifica"].ToString();
            }
            else
            {
                // Cerco nella queryString per per gli hyperlink nella mappa della pagina RicercaCantieriGeolocalizzazione.aspx
                numNotifica = Request.QueryString["NumeroNotifica"];    
            }
            
            ViewState["NumeroNotifica"] = numNotifica;
            CaricaDatiNotifica(numNotifica);
        }
    }

    private void CaricaDatiNotifica(String numNotifica)
    {
        NotificaPreliminare notifica = _biz.GetNotficaByNumero(numNotifica);
        if (notifica != null)
        {
            LabelAmmontare.Text = notifica.AmmontareComplessivo.ToString("N2");
            LabelCategoria.Text = notifica.TipoCategoriaDescrizioneVisualizzazione;
            LabelDataComunicazione.Text = Presenter.StampaDataFormattataClassica( notifica.DataComunicazioneNotifica);
            if (notifica.DataAggiornamentoNotifica.HasValue)
            {
                LabelDataAggiornamento.Text = Presenter.StampaDataFormattataClassica(notifica.DataAggiornamentoNotifica);
            }
            LabelLavoratriAutonomi.Text = notifica.NumeroLavoratoriAutonomi.ToString();
            LabelNumeroImprese.Text = notifica.NumeroImprese.ToString();
            LabelNumeroNotifica.Text = notifica.NumeroNotifica;
            LabelTipologia.Text = notifica.TipoTipologiaDescrizioneVisualizzazione;
            
            //CheckBoxArt9011.Checked = notifica.Articolo9011;
            //CheckBoxArt9011B.Checked = notifica.Articolo9011B;
            //CheckBoxArt9011C.Checked = notifica.Articolo9011C;
    
            GridPersoneCommittenti.CaricaPersone(notifica.Committenti);
            if (divAltrePersone.Visible)
            {
                GridPersoneAltrePersone.CaricaPersone(notifica.AltrePersone);
            }
            GridImpreseTutte.CaricaImprese(notifica.Imprese);
            GridIndirizziCantieri.CaricaIndirizzi(notifica.Indirizzi);

            // Carico le dichiarazioni
            List<String> dichiarazioni = new List<string>();

            if (notifica.Articolo9011)
            {
                dichiarazioni.Add("Non si riportano i dati del CSP in quanto trattasi di lavori privati non soggetti a permesso di costruire in base alla normativa vigente e comunque di importo inferiore ad euro 100.000 a norma dell’art. 90 comma 11.");
            }
            if (notifica.Articolo9011B)
            {
                dichiarazioni.Add("Si dichiara una data inizio lavori antecedente a quella di comunicazione e non si riportano i dati del CSP in quanto il cantiere, inizialmente non soggetto all'obbligo di notifica, vi è ricaduto per effetto di varianti sopravvenute in corso d'opera, a norma dell'art. 99 comma 1 lett. b).");
            }
            if (notifica.Articolo9011C)
            {
                dichiarazioni.Add("Non si riportano i dati del CSP e del CSE in quanto nel cantiere opera un’unica impresa con entità presunta di lavoro non inferiore a 200 uomini/giorno, a norma dell’art. 99 comma 1 lett. c).");
            }

            if (dichiarazioni.Count == 0)
            {
                dichiarazioni.Add("Nessuna dichiarazione.");
            }

            Presenter.CaricaElementiInListView(
                ListViewDichiarazioni,
                dichiarazioni);
        }
    }

    protected void ButtonReport_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/NotifichePreliminari/ReportNotifica.aspx?idNotifica={0}&visualizzaAltriLavoratori={1}", Server.UrlEncode((String)ViewState["NumeroNotifica"]), divAltrePersone.Visible));
    }
}
