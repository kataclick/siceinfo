﻿using System;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Impersonate : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.Impersonate);
    }

    protected void ButtonImpersonifica_Click(object sender, EventArgs e)
    {
        ImpersonateManager.Impersonate(RadTextBoxUsername.Text);
    }
}