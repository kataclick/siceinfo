<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneDipendente.aspx.cs" Inherits="GestioneUtentiRegistrazioneDipendente" %>

<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc4" %>

<%@ Register Src="WebControls/GestioneRegistrazione.ascx" TagName="GestioneRegistrazione"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/RegistrazioneDipendente.ascx" TagName="RegistrazioneDipendente"
    TagPrefix="uc1" %>
    
    <%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">

    <uc3:TitoloSottotitolo ID="TitoloSottotitolo2"  titolo="Gestione utenti" sottoTitolo="Registrazione dipendente" runat="server" />
    &nbsp;<br />
    
    <uc1:RegistrazioneDipendente ID="RegistrazioneDipendente1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc4:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>

