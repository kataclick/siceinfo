using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Cpt_BarCodeGenerator : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita,
                                              "CptStampaCodiceNotifica.aspx");

        if (!string.IsNullOrEmpty(Request.QueryString["idNotifica"]))
        {
            //this.LabelIdNotifica.Text = Request.QueryString["idNotifica"];
            //this.LabelBarreNotifica.Text = "*" + Request.QueryString["idNotifica"] + "*";

            Code39 c39 = new Code39();

            // Create stream....
            using (MemoryStream ms = new MemoryStream())
            {
                c39.FontFamilyName = "Free 3 of 9";
                c39.FontFileName = Server.MapPath("FREE3OF9.TTF");
                c39.FontSize = 160;
                c39.ShowCodeString = true;
                //Diamo un font per la stringa testuale che indica il barcode
                c39.CodeStringFont = new Font("Verdana", 36);
                using (Bitmap objBitmap = c39.GenerateBarcode("*" + Request.QueryString["idNotifica"] + "*"))
                {
                    objBitmap.Save(ms, ImageFormat.Png);

                    //return bytes....
                    Response.BinaryWrite(ms.GetBuffer());
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}