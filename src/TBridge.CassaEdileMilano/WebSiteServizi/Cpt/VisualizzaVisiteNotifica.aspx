<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VisualizzaVisiteNotifica.aspx.cs" Inherits="Cpt_VisualizzaVisiteNotifica" %>

<%@ Register Src="../WebControls/CptAllegatiAttivita.ascx" TagName="CptAllegatiAttivita"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Visite"
        titolo="Notifiche preliminari" />
    <br />
    <asp:GridView ID="GridViewVisite" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewVisite_RowDataBound"
        width="100%" DataKeyNames="IdVisita" OnSelectedIndexChanging="GridViewVisite_SelectedIndexChanging" OnRowCommand="GridViewVisite_RowCommand" OnRowDeleting="GridViewVisite_RowDeleting">
        <Columns>
            <asp:BoundField DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data"
                HtmlEncode="False">
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="ente" HeaderText="Ente">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="Tipologia" HeaderText="Tipologia">
                <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="Esito" HeaderText="Esito">
                <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="GradoIrregolarita" HeaderText="Grado irr.">
                <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Allegati" Visible="false">
                <ItemTemplate>
                    <uc3:CptAllegatiAttivita ID="CptAllegatiAttivita1" runat="server" />
                </ItemTemplate>
                <ItemStyle Width="200px" />
            </asp:TemplateField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Modifica" ShowSelectButton="True">
                <ItemStyle Width="10px" />
            </asp:CommandField>
            <asp:TemplateField>
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:Button ID="ButtonElimina" runat="server" CommandName="delete" Text="Elimina" />
                            </td>
                        </tr>
                        <tr id="trConfermaEliminazione" runat="server" visible="false">
                            <td>
                                Confermi l'eliminazione? 
                                <asp:Button ID="ButtonConfermaEliminazioneSi" CommandName="confermaDeleteSi" runat="server" 
                                    Text="S�" />
                                <asp:Button ID="ButtonConfermaEliminazioneNo" CommandName="confermaDeleteNo" runat="server" 
                                    Text="No" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna visita trovata
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="150px" OnClick="ButtonIndietro_Click" />
</asp:Content>

