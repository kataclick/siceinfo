using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Cpt_CptFileNonDisponibile : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CptFileNonDisponibile.aspx");
    }
}