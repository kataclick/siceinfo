<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CptRicercaNotifiche.aspx.cs" Inherits="Cpt_CptRicercaNotifiche" %>

<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register src="../Cantieri/WebControls/CantiereDaNotifica.ascx" tagname="CantiereDaNotifica" tagprefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ricerca"
        titolo="Notifiche" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Dal (ggmmaaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidatorDal" runat="server"
                                ControlToValidate="TextBoxDal" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Al (ggmmaaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidatorAl" runat="server"
                                ControlToValidate="TextBoxAl" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Committente
                        </td>
                        <td>
                            Natura opera
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCommittente" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNatura" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td>
                            Indirizzo
                        </td>
                        <td>
                            Comune
                        </td>
                        <td>
                            Provincia
                        </td>
                        <td>
                            Cap
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxCap"
                                ErrorMessage="*" ValidationExpression="^\d{5}$" ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList
                                ID="DropDownListProvincia"
                                runat="server"
                                AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>  
                        <td>
                            <asp:TextBox ID="TextBoxCap" runat="server" MaxLength="5" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Inizio lavori (>)(ggmmaaaa)"></asp:Label>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizio" runat="server"
                                ControlToValidate="TextBoxDataInizioLavori" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text="Fine lavori (<)(ggmmaaaa)"></asp:Label>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFine" runat="server"
                                ControlToValidate="TextBoxDataFineLavori" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                        <td>
                            Numero appalto
                        </td>
                        <td>
                            Ammontare (&gt;)
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="*"
                                ValidationExpression="^[.][0-9]+$|[0-9]*[,]*[0-9]{0,2}$" ControlToValidate="TextBoxAmmontare"
                                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxDataInizioLavori" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataFineLavori" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNumeroAppalto" runat="server" MaxLength="20" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAmmontare" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rag.soc.impresa
                        </td>
                        <td>
                            P.IVA/Cod.fisc.impresa
                        </td>
                        <td>
                            Provincia impresa
                        </td>
                        <td>
                            CAP impresa
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCapImpresa" runat="server" ControlToValidate="TextBoxCapImpresa"
                                ErrorMessage="*" ValidationExpression="^\d{5}$" ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxImpresa" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIvaFisc" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList
                                ID="DropDownListProvinciaImpresa"
                                runat="server"
                                AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCapImpresa" runat="server" MaxLength="5" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Area
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" ControlToValidate="DropDownListArea"
                                ValidationGroup="ricercaNotifiche" ErrorMessage="*">*</asp:RequiredFieldValidator>
                        </td>
                        <td>
                            Protocollo regione
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownListArea" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxProtocolloRegione" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td>
                            <br />
                        </td>
                        <td align="right" colspan="2">
                            &nbsp;&nbsp;
                            <asp:Button ID="ButtonVisualizza" runat="server" Font-Size="Small" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaNotifiche" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Notifiche trovate" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <asp:GridView runat="server" ID="GridViewNotifiche" AutoGenerateColumns="False" ShowHeader="False"
                    Width="100%" OnRowDataBound="GridViewNotifiche_RowDataBound" DataKeyNames="IdNotifica,IdNotificaRiferimento"
                    OnRowCommand="GridViewNotifiche_RowCommand" AllowPaging="True" OnPageIndexChanging="GridViewNotifiche_PageIndexChanging"
                    PageSize="5">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td width="40%">
                                            Notifica prelim.
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelProtocolloPreliminare" runat="server" Font-Bold="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Data:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelData" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Ultimo agg.
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelProtocolloUltimo" runat="server" Font-Bold="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Data ultimo agg.
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelDataUltimoAggiornamento" runat="server" Font-Bold="False"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Natura opera:
                                        </td>
                                        <td>
                                            <b><asp:Label ID="LabelNaturaOpera" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Tipo opera:
                                        </td>
                                        <td>
                                            <b><asp:Label ID="LabelTipoOpera" runat="server"></asp:Label></b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Numero appalto:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelNumeroAppalto" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Committente:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">
                                            Indirizzi:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelIndirizzi" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="ButtonDettagliUltima" runat="server" Text="Dettagli ultimo agg."
                                                CommandName="dettagliUltimo" CommandArgument="<%# Container.DataItemIndex %>" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="520px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelAgg" runat="server" Font-Bold="True">Storia</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GridViewAggiornamenti" runat="server" AutoGenerateColumns="False"
                                                ShowHeader="False" DataKeyNames="IdNotifica" OnSelectedIndexChanging="GridViewAggiornamenti_SelectedIndexChanging"
                                                Width="100%" OnRowDataBound="GridViewAggiornamenti_RowDataBound" BorderColor="Black"
                                                BorderStyle="Solid" OnRowDeleting="GridViewAggiornamenti_RowDeleting">
                                                <Columns>
                                                    <asp:CheckBoxField DataField="ImpresaPresente" >
                                                    <ItemStyle Width="10px" />
                                                    </asp:CheckBoxField>
                                                    <asp:TemplateField HeaderText="Protocollo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelProtocollo" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="50px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Data" DataField="Data" 
                                                        DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                                                    </asp:BoundField>
                                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Dettagli"
                                                        ShowSelectButton="True">
                                                        <ControlStyle CssClass="bottoneGriglia" />
                                                        <ItemStyle Width="10px" />
                                                    </asp:CommandField>
                                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Documento"
                                                        ShowDeleteButton="True" >
                                                    <ControlStyle CssClass="bottoneGriglia" />
                                                    <ItemStyle Width="10px" />
                                                    </asp:CommandField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="borderedTable">
                                                <tr id="trVisiteASL" runat="server" visible="false">
                                                    <td>
                                                        Visite ASL:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelVisiteASL" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trVisiteCPT" runat="server" visible="false">
                                                    <td>
                                                        Visite CPT:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelVisiteCPT" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trVisiteDPL" runat="server" visible="false">
                                                    <td>
                                                        Visite DPL:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelVisiteDPL" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trVisiteASLERSLT" runat="server" visible="false">
                                                    <td>
                                                        Visite ASLE/RSLT:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelVisiteASLERSLT" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trVisiteCassaEdile" runat="server" visible="false">
                                                    <td>
                                                        Visite Cassa Edile:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelVisiteCassaEdile" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trVisiteBottoneVisualizza" runat="server" visible="false">
                                                    <td colspan="2">
                                                        <asp:Button ID="ButtonVisualizzaVisite" runat="server" Width="150px" Text="Visualizza visite"
                                                            CommandName="visualizzaVisite" CommandArgument="<%# Container.DataItemIndex %>" />
                                                    </td>
                                                </tr>
                                                <tr id="trVisiteBottoneInserisci" runat="server" visible="false">
                                                    <td colspan="2">
                                                        <asp:Button ID="ButtonInserisciVisita" runat="server" Width="150px" Text="Inserisci visita"
                                                            CommandName="inserisciVisita" CommandArgument="<%# Container.DataItemIndex %>" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc3:CantiereDaNotifica ID="CantiereDaNotifica1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna notifica trovata
                    </EmptyDataTemplate>
                </asp:GridView>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
