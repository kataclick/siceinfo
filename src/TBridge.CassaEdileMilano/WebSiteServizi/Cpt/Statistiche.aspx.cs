using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Microsoft.Reporting.WebForms;

public partial class Cpt_Statistiche : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CptStatistiche,
                                              "Statistiche.aspx");

        #endregion
        if (!Page.IsPostBack)
        {
            RadTextBoxAnno.MaxValue = DateTime.Today.Year;
            ReportViewerCpt.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            //ReportViewerCpt.ServerReport.ReportPath = "/ReportCpt/ReportStatistiche";
        }
    }
    protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
    {
        ReportViewerCpt.ServerReport.ReportPath = "/ReportCpt/ReportStatistiche";

        List<ReportParameter> listaParam = new List<ReportParameter>();

        ReportParameter param1 = new ReportParameter("anno",RadTextBoxAnno.Value.ToString());
            listaParam.Add(param1);
        
        ReportViewerCpt.ServerReport.SetParameters(listaParam.ToArray());
    }
}