using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using System.IO;

public partial class Cpt_EstrazioneBrescia : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CptEstrazioneBrescia);
        #endregion

        if (!Page.IsPostBack)
        {
            ButtonDownload.Enabled = false;

            try
            {
                FileInfo file = new FileInfo(ConfigurationManager.AppSettings["NotificheBresciaDownload"]);
                if (file.Exists)
                {
                    LabelUltimoFile.Text = String.Format("File aggiornato al: <b>{0}</b>", file.LastWriteTime.ToShortDateString());
                    ButtonDownload.Enabled = true;
                }
                else
                {
                    LabelUltimoFile.Text = "File non disponibile";
                }
            }
            catch(Exception exc)
            {
                LabelUltimoFile.Text = String.Format("Errore durante il recupero del file: {0}", exc.Message);
            }
        }
    }

    protected void ButtonDownload_Click(object sender, EventArgs e)
    {
        string filePath = ConfigurationManager.AppSettings["NotificheBresciaDownload"];

        Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", Path.GetFileName(filePath)));
        Response.ContentType = "text/plain";
        Response.TransmitFile(filePath);
        //TransmitFile invece di WriteFile perch� file grosso e transmit non carica in memoria
        Response.Flush();
        Response.End();
    }
}