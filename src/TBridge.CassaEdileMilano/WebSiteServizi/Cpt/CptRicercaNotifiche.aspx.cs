using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
//using ASL = TBridge.Cemi.GestioneUtenti.Business.Identities.ASL;

public partial class Cpt_CptRicercaNotifiche : Page
{
    private const int INDICECHECK = 0;
    private readonly CptBusiness biz = new CptBusiness();
    private int? idASL;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion

        if (GestioneUtentiBiz.IsASL())
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.ASL asl =
            //    ((ASL)HttpContext.Current.User.Identity).Entity;
            ASL asl =
                (ASL) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idASL = asl.IdASL;
        }

        if (!Page.IsPostBack)
        {
            CaricaAree();
            CaricaProvince();

            if (Session["NotificheFiltroRicerca"] != null && Session["NotifichePagina"] != null)
            {
                GridViewNotifiche.PageIndex = (int) Session["NotifichePagina"];
                CaricaNotifiche((NotificaFilter) Session["NotificheFiltroRicerca"]);
                CaricaFiltro((NotificaFilter) Session["NotificheFiltroRicerca"]);
            }

            SelezionaArea();
        }
    }

    private void CaricaProvince()
    {
        Presenter.CaricaProvince(DropDownListProvincia);
        Presenter.CaricaProvince(DropDownListProvinciaImpresa);
    }

    private void CaricaAree()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListArea,
            biz.GetAree(),
            "Descrizione",
            "IdArea");
    }

    private void SelezionaArea()
    {
        // Se l'utente � autorizzato all'inserimento di notifiche per una sola area
        // blocco la selezione
        if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicerca.ToString())
            ^
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicercaLodi.ToString())
            )
        {
            // Seleziono l'area Milano
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptRicerca.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "1";
            }

            // Seleziono l'area Lodi
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptRicercaLodi.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "2";
            }

            DropDownListArea.Enabled = false;
        }
    }

    private void CaricaFiltro(NotificaFilter notificaFilter)
    {
        if (notificaFilter.Dal.HasValue)
            TextBoxDal.Text = notificaFilter.Dal.Value.ToShortDateString();

        TextBoxCommittente.Text = notificaFilter.Committente;
        TextBoxIndirizzo.Text = notificaFilter.Indirizzo;
        TextBoxComune.Text = notificaFilter.IndirizzoComune;
        TextBoxCap.Text = notificaFilter.Cap;
        TextBoxImpresa.Text = notificaFilter.Impresa;
        TextBoxIvaFisc.Text = notificaFilter.FiscIva;

        if (notificaFilter.Ammontare.HasValue)
            TextBoxAmmontare.Text = notificaFilter.Ammontare.ToString();

        TextBoxNatura.Text = notificaFilter.NaturaOpera;

        if (notificaFilter.DataInizio.HasValue)
            TextBoxDataInizioLavori.Text = notificaFilter.DataInizio.Value.ToShortDateString();
        if (notificaFilter.DataFine.HasValue)
            TextBoxDataFineLavori.Text = notificaFilter.DataFine.Value.ToShortDateString();

        TextBoxNumeroAppalto.Text = notificaFilter.NumeroAppalto;

        DropDownListArea.SelectedValue = notificaFilter.IdArea.ToString();
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewNotifiche.PageIndex = 0;
            NotificaFilter filtro = CaricaNotifiche(null);
            Session["NotificheFiltroRicerca"] = filtro;
            Session["NotifichePagina"] = GridViewNotifiche.PageIndex;

            //TBridge.Cemi.GestioneUtenti.Business.Identities.IUtente utente =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.IUtente)
            //    HttpContext.Current.User.Identity);

            LogRicerca log = new LogRicerca();
            //log.IdUtente = utente.IdUtente;
            log.IdUtente = GestioneUtentiBiz.GetIdUtente();
            log.XmlFiltro = biz.GetPropertiesDelFiltro(filtro);
            log.Sezione = SezioneLogRicerca.RicercaNotifiche;

            biz.InsertLogRicerca(log);
        }
    }

    public NotificaFilter CaricaNotifiche(NotificaFilter filtroP)
    {
        NotificaFilter filtro = null;

        if (filtroP == null)
        {
            filtro = new NotificaFilter();

            string dalRic = TextBoxDal.Text.Trim();
            if (!string.IsNullOrEmpty(dalRic))
            {
                //string dataS = datRic.Substring(0, 2) + "/" + datRic.Substring(2, 2) + "/" + datRic.Substring(4, 4);
                //data = DateTime.Parse(dataS);
                filtro.Dal = DateTime.ParseExact(dalRic, "ddMMyyyy", null).Date;
            }

            string alRic = TextBoxAl.Text.Trim();
            if (!string.IsNullOrEmpty(alRic))
            {
                //string dataS = datRic.Substring(0, 2) + "/" + datRic.Substring(2, 2) + "/" + datRic.Substring(4, 4);
                //data = DateTime.Parse(dataS);
                filtro.Al = DateTime.ParseExact(alRic, "ddMMyyyy", null).Date;
            }

            string comRic = TextBoxCommittente.Text.Trim();
            if (!string.IsNullOrEmpty(comRic))
                filtro.Committente = comRic;

            string indRic = TextBoxIndirizzo.Text.Trim();
            if (!string.IsNullOrEmpty(indRic))
                filtro.Indirizzo = indRic;

            string impRic = TextBoxImpresa.Text.Trim();
            if (!string.IsNullOrEmpty(impRic))
                filtro.Impresa = impRic;

            string natRic = TextBoxNatura.Text.Trim();
            if (!string.IsNullOrEmpty(natRic))
                filtro.NaturaOpera = natRic;

            if (!string.IsNullOrEmpty(TextBoxAmmontare.Text))
                filtro.Ammontare = decimal.Parse(TextBoxAmmontare.Text);

            string dataInizioString = TextBoxDataInizioLavori.Text.Trim();
            if (!string.IsNullOrEmpty(dataInizioString))
            {
                filtro.DataInizio = DateTime.ParseExact(dataInizioString, "ddMMyyyy", null).Date;
            }
            string dataFineString = TextBoxDataFineLavori.Text.Trim();
            if (!string.IsNullOrEmpty(dataFineString))
            {
                filtro.DataFine = DateTime.ParseExact(dataFineString, "ddMMyyyy", null).Date;
            }

            string ivaRic = TextBoxIvaFisc.Text.Trim();
            if (!string.IsNullOrEmpty(ivaRic))
                filtro.FiscIva = ivaRic;

            string numAppRic = TextBoxNumeroAppalto.Text.Trim();
            if (!string.IsNullOrEmpty(numAppRic))
                filtro.NumeroAppalto = numAppRic;

            string comuRic = TextBoxComune.Text.Trim();
            if (!string.IsNullOrEmpty(comuRic))
                filtro.IndirizzoComune = comuRic;

            if (!string.IsNullOrEmpty(TextBoxCap.Text))
                filtro.Cap = TextBoxCap.Text;

            if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxProtocolloRegione.Text)))
            {
                filtro.ProtocolloRegione = Presenter.NormalizzaCampoTesto(TextBoxProtocolloRegione.Text);
            }

            filtro.IdASL = idASL;
            filtro.IdArea = Int16.Parse(DropDownListArea.SelectedValue);

            if (!String.IsNullOrEmpty(DropDownListProvinciaImpresa.Text))
            {
                filtro.ProvinciaImpresa = Presenter.NormalizzaCampoTesto(DropDownListProvinciaImpresa.Text);
            }

            if (!String.IsNullOrEmpty(TextBoxCapImpresa.Text))
            {
                filtro.CapImpresa = TextBoxCapImpresa.Text;
            }

            if (!String.IsNullOrEmpty(DropDownListProvincia.Text))
            {
                filtro.IndirizzoProvincia = DropDownListProvincia.Text;
            }
        }
        else
        {
            filtro = filtroP;
        }

        NotificaCollection notifiche =
            biz.RicercaNotifiche(filtro);

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Notifiche trovate: {0}", notifiche.Count);

        GridViewNotifiche.DataSource = notifiche;
        GridViewNotifiche.DataBind();

        return filtro;
    }

    protected void GridViewNotifiche_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notificaRif = (Notifica) e.Row.DataItem;

            Label lNotificaPrel = (Label) e.Row.FindControl("LabelProtocolloPreliminare");
            Label lNotificaUlti = (Label) e.Row.FindControl("LabelProtocolloUltimo");
            Label lData = (Label) e.Row.FindControl("LabelData");
            Label lNaturaOpera = (Label) e.Row.FindControl("LabelNaturaOpera");
            Label lTipoOpera = (Label) e.Row.FindControl("LabelTipoOpera");
            Label lNumeroAppalto = (Label) e.Row.FindControl("LabelNumeroAppalto");
            Label lCommittente = (Label) e.Row.FindControl("LabelCommittente");
            Label lDataUltimoAgg = (Label) e.Row.FindControl("LabelDataUltimoAggiornamento");
            Label lIndirizzi = (Label) e.Row.FindControl("LabelIndirizzi");
            GridView gvStoria = (GridView) e.Row.FindControl("GridViewAggiornamenti");
            Cantieri_WebControls_CantiereDaNotifica wcCantieriDaNotifica = (Cantieri_WebControls_CantiereDaNotifica)e.Row.FindControl("CantiereDaNotifica1");

            Label lVisiteASL = (Label) e.Row.FindControl("LabelVisiteASL");
            HtmlTableRow rigaASL = (HtmlTableRow) e.Row.FindControl("trVisiteASL");
            Label lVisiteCPT = (Label) e.Row.FindControl("LabelVisiteCPT");
            HtmlTableRow rigaCPT = (HtmlTableRow) e.Row.FindControl("trVisiteCPT");
            Label lVisiteDPL = (Label) e.Row.FindControl("LabelVisiteDPL");
            HtmlTableRow rigaDPL = (HtmlTableRow) e.Row.FindControl("trVisiteDPL");
            Label lVisiteASLERSLT = (Label) e.Row.FindControl("LabelVisiteASLERSLT");
            HtmlTableRow rigaASLERSLT = (HtmlTableRow) e.Row.FindControl("trVisiteASLERSLT");
            Label lVisiteCassaEdile = (Label) e.Row.FindControl("LabelVisiteCassaEdile");
            HtmlTableRow rigaCassaEdile = (HtmlTableRow) e.Row.FindControl("trVisiteCassaEdile");

            HtmlTableRow rigaBottoneVisualizza = (HtmlTableRow) e.Row.FindControl("trVisiteBottoneVisualizza");
            HtmlTableRow rigaBottoneInserisci = (HtmlTableRow) e.Row.FindControl("trVisiteBottoneInserisci");

            #region Cantieri generati
            wcCantieriDaNotifica.OnCantiereGenerato += new TBridge.Cemi.Cantieri.Type.Delegates.CantiereGeneratoEventHandler(wcCantieriDaNotifica_OnCantiereGenerato);

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGenerazioneDaNotifiche)
                && !String.IsNullOrEmpty(notificaRif.ProtocolloRegione))
            {
                IndirizzoCollection indirizzi = biz.GetCantieriGenerati(notificaRif.IdNotificaRiferimento);
                wcCantieriDaNotifica.CaricaCantieri(indirizzi, notificaRif.IdNotificaRiferimento);
            }
            else
            {
                wcCantieriDaNotifica.ForzaNonVisualizzazione();
            }
            #endregion

            if (TextBoxImpresa.Text != string.Empty || TextBoxIvaFisc.Text.Trim() != string.Empty)
                gvStoria.Columns[INDICECHECK].Visible = true;
            else
                gvStoria.Columns[INDICECHECK].Visible = false;

            if (String.IsNullOrEmpty(notificaRif.ProtocolloRegione))
            {
                lNotificaPrel.Text = notificaRif.IdNotificaPadre.ToString();
            }
            else
            {
                lNotificaPrel.Text = notificaRif.ProtocolloRegione;
            }

            if (notificaRif.IdNotifica != notificaRif.IdNotificaPadre)
            {
                lNotificaUlti.Text = notificaRif.IdNotifica.ToString();
                lData.Text = notificaRif.DataNotificaPadre.Value.ToShortDateString();
                lDataUltimoAgg.Text = notificaRif.Data.ToShortDateString();
            }
            else
            {
                if (String.IsNullOrEmpty(notificaRif.ProtocolloRegione))
                {
                    lData.Text = notificaRif.Data.ToShortDateString();
                }
                else
                {
                    if (notificaRif.DataPrimoInserimento.HasValue)
                    {
                        lData.Text = notificaRif.DataPrimoInserimento.Value.ToShortDateString();
                    }
                    else
                    {
                        lData.Text = notificaRif.Data.ToShortDateString();
                    }

                    lDataUltimoAgg.Text = notificaRif.Data.ToShortDateString();
                }
            }
            lNaturaOpera.Text = notificaRif.NaturaOpera;

            if (notificaRif.OperaPubblica.HasValue)
            {
                lTipoOpera.Text = notificaRif.OperaPubblica.Value ? "Pubblica" : "Privata";
            }

            lNumeroAppalto.Text = notificaRif.NumeroAppalto;
            lCommittente.Text =
                String.Format("{0}<BR/>{1} - {2} {3}", notificaRif.CommittenteRagioneSociale,
                              notificaRif.Committente.Indirizzo, notificaRif.Committente.Comune,
                              notificaRif.Committente.Provincia);

            foreach (Indirizzo indirizzo in notificaRif.Indirizzi)
            {
                lIndirizzi.Text += String.Format("- {0}<BR/>", indirizzo.IndirizzoCompleto);
            }

            gvStoria.DataSource = notificaRif.Storia;
            gvStoria.DataBind();

            // VISUALIZZAZIONE
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaASLVisualizzazione.ToString())
                && notificaRif.NumeroVisiteASL > 0
                )
            {
                rigaASL.Visible = true;
                rigaBottoneVisualizza.Visible = true;
                lVisiteASL.Text = notificaRif.NumeroVisiteASL.ToString();
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione.ToString())
                && notificaRif.NumeroVisiteCPT > 0
                )
            {
                rigaCPT.Visible = true;
                rigaBottoneVisualizza.Visible = true;
                lVisiteCPT.Text = notificaRif.NumeroVisiteCPT.ToString();
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione.ToString())
                && notificaRif.NumeroVisiteDPL > 0
                )
            {
                rigaDPL.Visible = true;
                rigaBottoneVisualizza.Visible = true;
                lVisiteDPL.Text = notificaRif.NumeroVisiteDPL.ToString();
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione.ToString())
                && notificaRif.NumeroVisiteASLERSLT > 0
                )
            {
                rigaASLERSLT.Visible = true;
                rigaBottoneVisualizza.Visible = true;
                lVisiteASLERSLT.Text = notificaRif.NumeroVisiteASLERSLT.ToString();
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaCassaEdileVisualizzazione.ToString())
                && notificaRif.NumeroVisiteCassaEdile > 0
                )
            {
                rigaCassaEdile.Visible = true;
                rigaBottoneVisualizza.Visible = true;
                lVisiteCassaEdile.Text = notificaRif.NumeroVisiteCassaEdile.ToString();
            }

            // INSERIMENTO
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptAttivitaASLInserimento.ToString())
                )
            {
                rigaBottoneInserisci.Visible = true;
            }
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptAttivitaCPTInserimento.ToString())
                )
            {
                rigaBottoneInserisci.Visible = true;
            }
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptAttivitaDPLInserimento.ToString())
                )
            {
                rigaBottoneInserisci.Visible = true;
            }
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptAttivitaASLERSLTInserimento.ToString())
                )
            {
                rigaBottoneInserisci.Visible = true;
            }
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptAttivitaCassaEdileInserimento.ToString())
                )
            {
                rigaBottoneInserisci.Visible = true;
            }
        }
    }

    void wcCantieriDaNotifica_OnCantiereGenerato()
    {
        CaricaNotifiche(null);
    }

    protected void GridViewAggiornamenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvStoria = (GridView) sender;
        int idNotifica = (int) gvStoria.DataKeys[e.NewSelectedIndex].Value;

        Response.Redirect(string.Format("~/Cpt/CptNotificaDettagli.aspx?idNotifica={0}", idNotifica));
    }

    protected void GridViewAggiornamenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notifica = (Notifica) e.Row.DataItem;

            Label lProtocollo = (Label)e.Row.FindControl("LabelProtocollo");

            if (!String.IsNullOrEmpty(notifica.ProtocolloRegione))
            {
                lProtocollo.Text = notifica.ProtocolloRegione;
            }
            else
            {
                lProtocollo.Text = notifica.IdNotifica.ToString();
            }

            if (notifica.IdNotificaPadre.HasValue)
                e.Row.ForeColor = Color.Gray;
        }
    }

    protected void GridViewNotifiche_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName != "Page")
        {
            Int32 indice = Int32.Parse(e.CommandArgument.ToString());
            Int32 idNotifica = -1; 
            Int32 idNotificaRiferimento = -1;

            switch (e.CommandName)
            {
                case "dettagliUltimo":
                    idNotifica = (int)GridViewNotifiche.DataKeys[indice % GridViewNotifiche.PageSize].Values["IdNotifica"];
                    Response.Redirect(String.Format("~/Cpt/CptNotificaDettagli.aspx?idNotifica={0}", idNotifica));
                    break;
                case "visualizzaVisite":
                    idNotificaRiferimento =
                        (Int32)
                        GridViewNotifiche.DataKeys[indice % GridViewNotifiche.PageSize].Values["IdNotificaRiferimento"];
                    Response.Redirect(String.Format("~/Cpt/VisualizzaVisiteNotifica.aspx?idNotifica={0}",
                                                    idNotificaRiferimento));
                    break;
                case "inserisciVisita":
                    idNotificaRiferimento =
                        (Int32)
                        GridViewNotifiche.DataKeys[indice % GridViewNotifiche.PageSize].Values["IdNotificaRiferimento"];
                    Response.Redirect(String.Format("~/Cpt/InserimentoAttivita.aspx?idNotifica={0}", idNotificaRiferimento));
                    break;
            }
        }
    }

    protected void GridViewNotifiche_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewNotifiche.PageIndex = e.NewPageIndex;
        Session["NotifichePagina"] = e.NewPageIndex;
        CaricaNotifiche(null);
    }

    protected void GridViewAggiornamenti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gvStoria = (GridView) sender;

        string percorsoFile = ConfigurationManager.AppSettings["NotifichePercorsoFile"];
        int idNotifica = (int) gvStoria.DataKeys[e.RowIndex].Value;

        if (!biz.IsNotificaTelematica(idNotifica))
        {
            try
            {
                string pathFile = String.Format("{0}{1}.pdf", percorsoFile, idNotifica);
                //Set the appropriate ContentType.
                Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.pdf", idNotifica));
                Response.ContentType = "application/pdf";
                //Write the file directly to the HTTP content output stream.
                Response.WriteFile(pathFile);
                Response.Flush();
                Response.End();
            }
            catch (Exception exc)
            {
                Response.Redirect("~/Cpt/CptFileNonDisponibile.aspx", false);
            }
        }
        else
        {
            Response.Redirect(String.Format("~/ReportCpt.aspx?idNotifica={0}", idNotifica));
        }
    }
}