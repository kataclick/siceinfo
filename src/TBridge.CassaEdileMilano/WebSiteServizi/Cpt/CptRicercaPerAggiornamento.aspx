<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CptRicercaPerAggiornamento.aspx.cs" Inherits="CptRicercaPerAggiornamento" %>

<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/CptRicercaNotificaPerAggiornamento.ascx" TagName="CptRicercaNotificaPerAggiornamento"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Aggiornamento notifica"
        titolo="Notifiche preliminari" />
    <br />
    <uc3:CptRicercaNotificaPerAggiornamento id="CptRicercaNotificaPerAggiornamento1"
        runat="server">
    </uc3:CptRicercaNotificaPerAggiornamento>
</asp:Content>

