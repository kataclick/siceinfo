<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RicercaVisite.aspx.cs" Inherits="Cpt_RicercaVisite" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CptRicercaVisite.ascx" TagName="CptRicercaVisite"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ricerca visite"
        titolo="Notifiche preliminari" />
    <br />
    <uc3:CptRicercaVisite id="CptRicercaVisite1" runat="server">
    </uc3:CptRicercaVisite>
    <br />
</asp:Content>

