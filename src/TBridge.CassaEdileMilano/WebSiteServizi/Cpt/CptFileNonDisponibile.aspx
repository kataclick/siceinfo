<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CptFileNonDisponibile.aspx.cs" Inherits="Cpt_CptFileNonDisponibile" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="File non disponibile"
        titolo="Notifiche" />
    <br />
    Il file della notifica preliminare o dell'aggiornamento desiderato non � disponibile
</asp:Content>

