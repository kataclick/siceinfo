<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CptRicercaNotificheControllo.aspx.cs" Inherits="Cpt_CptRicercaNotificheControllo" %>

<%@ Register Src="../WebControls/CptRicercaNotificaPerAggiornamento.ascx" TagName="CptRicercaNotificaPerAggiornamento"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ricerca" titolo="Notifiche" />
    <br />
    <uc4:CptRicercaNotificaPerAggiornamento ID="CptRicercaNotificaPerAggiornamento1"
        runat="server" />
    
</asp:Content>

