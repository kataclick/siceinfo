using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CPTStampaProtocolloNotifica : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita,
                                              "CPTStampaProtocolloNotifica.aspx");

        //ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");
        ButtonStampa.Attributes.Add("onclick", "window.print(); return false;");

        if (!string.IsNullOrEmpty(Request.QueryString["idNotifica"]))
        {
            LabelIdNotifica.Text = Request.QueryString["idNotifica"];
        }

        ButtonStampa.Attributes.Add("onclick",
                                    "window.open('" + ResolveUrl("~/Cpt/CptStampaCodiceNotifica.aspx") + "?idNotifica="
                                    + Request.QueryString["idNotifica"] +
                                    "', '', 'menubar=no,height=670,width=750'); return false;");
    }

    protected void ButtonProsegui_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cpt/CptDefault.aspx");
    }
}