using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;

public partial class Cpt_InserimentoAttivita : Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCPTInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaDPLInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLERSLTInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCassaEdileInserimento);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        #region Click Multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisci, null) + ";");
        sb.Append("return true;");
        ButtonInserisci.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisci);

        #endregion

        if (!Page.IsPostBack)
        {
            int idNotifica = Int32.Parse(Request.QueryString["idNotifica"]);
            CptDatiAttivita1.ImpostaIdNotifica(idNotifica);
        }
    }

    protected void ButtonInserisci_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Visita visita = CreaVisita();

            if (biz.InsertVisita(visita))
            {
                CptDatiAttivita1.BloccaCampi();
                ButtonInserisci.Enabled = false;
                PanelAllegati.Enabled = true;
                ViewState["IdVisita"] = visita.IdVisita.Value;
                CptAllegatiAttivita1.ImpostaIdVisita(visita.IdVisita.Value);

                LabelMessaggio.Visible = true;
            }
            else
            {
                LabelMessaggio.Visible = false;
            }
        }
    }

    private Visita CreaVisita()
    {
        return CptDatiAttivita1.CreaVisita();
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cpt/CptRicercaNotifiche.aspx");
    }
}