using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Cpt_VisualizzaVisiteNotifica : Page
{
    private const int INDICEELIMINA = 7;
    private const int INDICEMODIFICA = 6;
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCassaEdileVisualizzazione);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "~/Cpt/VisualizzaVisiteNotifica.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            int idNotificaRiferimento = Int32.Parse(Request.QueryString["idNotifica"]);
            ViewState["IdNotifica"] = idNotificaRiferimento;

            CaricaVisite(idNotificaRiferimento);
        }
    }

    private void CaricaVisite(int idNotificaRiferimento)
    {
        VisitaCollection visite = biz.GetVisite(idNotificaRiferimento);

        visite = FiltraVisite(visite);

        GridViewVisite.DataSource = visite;
        GridViewVisite.DataBind();
    }

    protected void GridViewVisite_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Visita visita = (Visita) e.Row.DataItem;
            WebControls_CptAllegatiAttivita allAtt =
                (WebControls_CptAllegatiAttivita) e.Row.FindControl("CptAllegatiAttivita1");

            Button bConfermaEliminazioneSi = (Button) e.Row.FindControl("ButtonConfermaEliminazioneSi");
            bConfermaEliminazioneSi.CommandArgument = e.Row.RowIndex.ToString();
            Button bConfermaEliminazioneNo = (Button) e.Row.FindControl("ButtonConfermaEliminazioneNo");
            bConfermaEliminazioneNo.CommandArgument = e.Row.RowIndex.ToString();

            allAtt.ImpostaAllegati(visita.Allegati);

            e.Row.Cells[INDICEMODIFICA].Enabled = false;
            e.Row.Cells[INDICEELIMINA].Enabled = false;

            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaASLInserimento.ToString())
                && (visita.Ente == EnteVisita.ASL)
                )
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = true;
                e.Row.Cells[INDICEELIMINA].Enabled = true;
            }

            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaCPTInserimento.ToString())
                && (visita.Ente == EnteVisita.CPT)
                )
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = true;
                e.Row.Cells[INDICEELIMINA].Enabled = true;
            }

            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaDPLInserimento.ToString())
                && (visita.Ente == EnteVisita.DPL)
                )
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = true;
                e.Row.Cells[INDICEELIMINA].Enabled = true;
            }

            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaASLERSLTInserimento.ToString())
                && (visita.Ente == EnteVisita.ASLERSLT)
                )
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = true;
                e.Row.Cells[INDICEELIMINA].Enabled = true;
            }

            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaCassaEdileInserimento.ToString())
                && (visita.Ente == EnteVisita.CassaEdile)
                )
            {
                e.Row.Cells[INDICEMODIFICA].Enabled = true;
                e.Row.Cells[INDICEELIMINA].Enabled = true;
            }
        }
    }

    private VisitaCollection FiltraVisite(VisitaCollection visite)
    {
        VisitaCollection visiteFiltrate = new VisitaCollection();

        foreach (Visita visita in visite)
        {
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaASLVisualizzazione.ToString())
                && visita.Ente == EnteVisita.ASL
                )
            {
                visiteFiltrate.Add(visita);
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione.ToString())
                && visita.Ente == EnteVisita.CPT
                )
            {
                visiteFiltrate.Add(visita);
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione.ToString())
                && visita.Ente == EnteVisita.DPL
                )
            {
                visiteFiltrate.Add(visita);
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione.ToString())
                && visita.Ente == EnteVisita.ASLERSLT
                )
            {
                visiteFiltrate.Add(visita);
            }
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptAttivitaCassaEdileVisualizzazione.ToString())
                && visita.Ente == EnteVisita.CassaEdile
                )
            {
                visiteFiltrate.Add(visita);
            }
        }

        return visiteFiltrate;
    }

    protected void GridViewVisite_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idVisita = (int) GridViewVisite.DataKeys[e.NewSelectedIndex].Values["IdVisita"];

        Context.Items["IdVisita"] = idVisita;
        Server.Transfer("~/Cpt/ModificaVisita.aspx");
    }

    protected void GridViewVisite_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int indice;

        switch (e.CommandName)
        {
            case "confermaDeleteSi":
                indice = Int32.Parse(e.CommandArgument.ToString());
                int idVisita = (int) GridViewVisite.DataKeys[indice].Values["IdVisita"];
                if (biz.DeleteVisita(idVisita))
                {
                    int idNotifica = (int) ViewState["IdNotifica"];
                    CaricaVisite(idNotifica);
                }
                break;
            case "confermaDeleteNo":
                indice = Int32.Parse(e.CommandArgument.ToString());
                HtmlTableRow rigaConferma =
                    (HtmlTableRow) GridViewVisite.Rows[indice].FindControl("trConfermaEliminazione");
                rigaConferma.Visible = false;
                break;
        }
    }

    protected void GridViewVisite_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HtmlTableRow rigaConferma = (HtmlTableRow) GridViewVisite.Rows[e.RowIndex].FindControl("trConfermaEliminazione");
        rigaConferma.Visible = true;
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Cpt/CptRicercaNotifiche.aspx");
    }
}