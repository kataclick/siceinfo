<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InserimentoAttivita.aspx.cs" Inherits="Cpt_InserimentoAttivita" %>

<%@ Register Src="../WebControls/CptAllegatiAttivita.ascx" TagName="CptAllegatiAttivita"
    TagPrefix="uc4" %>

<%@ Register Src="../WebControls/CptDatiAttivita.ascx" TagName="CptDatiAttivita"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento visita"
        titolo="Notifiche preliminari" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:CptDatiAttivita id="CptDatiAttivita1" runat="server">
                </uc3:CptDatiAttivita>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonInserisci" runat="server" OnClick="ButtonInserisci_Click" Text="Inserisci" ValidationGroup="visita" Width="150px" />&nbsp;
                <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Torna alla ricerca" Width="150px" />&nbsp;<asp:Label 
                    ID="LabelMessaggio" runat="server" ForeColor="Red" 
                    Text="Inserimento effettuato" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelAllegati" runat="server" Enabled="false" Visible="false">
                    <uc4:CptAllegatiAttivita ID="CptAllegatiAttivita1" runat="server" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>

