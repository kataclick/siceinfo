<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeFile="CptInserimentoNotifica.aspx.cs" Inherits="CptInserimentoNotifica" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc7" %>
<%@ Register Src="~/WebControls/CptGestioneIndirizzi.ascx" TagName="CptGestioneIndirizzi"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/CantieriRicercaCommittente.ascx" TagName="CantieriRicercaCommittente"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/CptCommittente.ascx" TagName="CptCommittente" TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/CptImpresa.ascx" TagName="CptImpresa" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresa.ascx" TagName="CantieriRicercaImpresa"
    TagPrefix="uc6" %>
<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <div>
        <uc7:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento nuova notifica"
            titolo="Notifiche" />
        <asp:UpdateProgress ID="UpdateProgressCPT" runat="server" AssociatedUpdatePanelID="UpdatePanelCPT"
            DisplayAfter="100">
            <ProgressTemplate>
                <center>
                    <asp:Image ID="ImageCPTProgress" runat="server" ImageUrl="~/images/loading6b.gif" />
                    <asp:Label ID="LabelCPTProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                        Text="In elaborazione...."></asp:Label>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanelCPT" runat="server">
            <ContentTemplate>
                <asp:Panel ID="PanelGenerale" runat="server" Width="100%">
                    <asp:Wizard ID="WizardInserimento" runat="server" ActiveStepIndex="0"
                        Width="100%" OnFinishButtonClick="Wizard1_FinishButtonClick" OnNextButtonClick="Wizard1_NextButtonClick"
                        OnSideBarButtonClick="Wizard1_SideBarButtonClick" StartNextButtonText="Avanti"
                        StepNextButtonText="Avanti" StepPreviousButtonText="Indietro" FinishCompleteButtonText="Inserisci"
                        FinishPreviousButtonText="Indietro" BorderWidth="1px" Font-Names="Verdana">
                        <StepStyle VerticalAlign="Top" />
                        <StartNavigationTemplate>
                            <asp:Button ID="btnSuccessivo" runat="server" Text="Avanti" CommandName="MoveNext"
                                Width="100px" CausesValidation="false" TabIndex="1" />
                        </StartNavigationTemplate>
                        <StepNavigationTemplate>
                            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                                Width="100px" CausesValidation="false" TabIndex="2" />
                            <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
                                CausesValidation="false" Width="100px" TabIndex="1" />
                        </StepNavigationTemplate>
                        <FinishNavigationTemplate>
                            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                                Width="100px" CausesValidation="false" TabIndex="2" />
                            <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Avanti"
                                CausesValidation="false" Width="150px" TabIndex="1" />
                        </FinishNavigationTemplate>
                        <WizardSteps>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="Dati generali">
                                <b>
                                    <asp:Label ID="LabelTitolo" runat="server" Text="INSERIMENTO NOTIFICA PRELIMINARE"></asp:Label>
                                    <br />
                                </b>
                                <br />
                                <asp:Panel ID="PanelAggiornamento" runat="server" Visible="False" Width="100%">
                                    <table class="borderedTable">
                                        <tr>
                                            <td colspan="2">
                                                Notifica in aggiornamento
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Area
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelNotificaPadreArea" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Data
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelNotificaPadreData" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Natura dell'opera
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelNotificaPadreNatura" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Committente
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelNotificaPadreCommittente" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <asp:Button ID="ButtonDatiGeneraliSblocca" runat="server" OnClick="ButtonDatiGeneraliSblocca_Click"
                                    Text="Modifica Dati Generali" Visible="False" />
                                <asp:Panel ID="PanelDatiGenerali" runat="server" Width="100%">
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                Area:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DropDownListArea" runat="server" Width="350px" AppendDataBoundItems="True"
                                                    CssClass="campoNotifica">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListArea"
                                                    ErrorMessage="Campo obbligatorio" ValidationGroup="step1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Data (ggmmaaaa)*
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxData" runat="server" Width="350px" MaxLength="8" CssClass="campoNotifica"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxData"
                                                    ErrorMessage="Campo obbligatorio" ValidationGroup="step1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxData"
                                                    ErrorMessage="Formato data" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                                    ValidationGroup="step1"></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                &nbsp; &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Natura dell'opera*
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxNaturaOpera" runat="server" Width="350px" Height="56px" TextMode="MultiLine"
                                                    MaxLength="500" CssClass="campoNotifica"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxNaturaOpera"
                                                    ErrorMessage="Campo obbligatorio" ValidationGroup="step1"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Numero appalto
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxNumeroAppalto" runat="server" Width="350px" MaxLength="20"
                                                    CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Ammontare complessivo dei lavori previsto (�)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxImporto" runat="server" Width="350px" MaxLength="20" CssClass="campoNotifica"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBoxImporto"
                                                    ErrorMessage="Formato valuta" ValidationExpression="^[.][0-9]+$|[0-9]*[,]*[0-9]{0,2}$"
                                                    ValidationGroup="step3"></asp:RegularExpressionValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Indirizzi*
                                            </td>
                                            <td colspan="2">
                                                <uc2:CptGestioneIndirizzi ID="CptGestioneIndirizzi1" runat="server" />
                                                <asp:Label ID="LabelControlloIndirizzi" runat="server" ForeColor="Red" Text="Inserire almeno un indirizzo"
                                                    Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Committente*
                                            </td>
                                            <td>
                                                <table class="standardTable">
                                                    <tr>
                                                        <td>
                                                            <table class="standardTable">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxCommittente" runat="server" Width="350px" Height="67px" TextMode="MultiLine"
                                                                            Enabled="False" ReadOnly="True" CssClass="campoNotifica" />
                                                                        <br />
                                                                        &nbsp;<asp:Label ID="LabelCommittenteEsistente" runat="server" ForeColor="Red"></asp:Label></td>
                                                                    <td>
                                                                        <asp:Button ID="ButtonModificaCommittente" runat="server" Text="Modifica" OnClick="ButtonModificaCommittente_Click"
                                                                            Visible="False" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" colspan="2">
                                                                        <asp:Label ID="LabelCommittenteSelezionato" runat="server" ForeColor="Red" Text="Campo obbligatorio"
                                                                            Visible="False"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdateProgress ID="UpdateProgressCommittente" runat="server" AssociatedUpdatePanelID="UpdatePanelCommittente"
                                                                DisplayAfter="100">
                                                                <ProgressTemplate>
                                                                    <center>
                                                                        <asp:Image ID="ImageCommittenteProgress" runat="server" ImageUrl="~/images/loading6b.gif" />
                                                                        <asp:Label ID="LabelCommittenteProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                                                                            Text="In elaborazione...."></asp:Label>
                                                                    </center>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                            <asp:UpdatePanel ID="UpdatePanelCommittente" runat="server">
                                                                <ContentTemplate>
                                                                    <uc3:CantieriRicercaCommittente ID="CantieriRicercaCommittente1" runat="server" Visible="true" />
                                                                    <br />
                                                                    <asp:Panel ID="PanelInserimentoCommittente" runat="server" Width="100%"
                                                                        Visible="False">
                                                                        <table class="filledtable">
                                                                            <tr>
                                                                                <td style="height: 16px">
                                                                                    <asp:Label ID="LabelTitoloInserimentoCommittente" runat="server" Font-Bold="True"
                                                                                        ForeColor="White" Text="Inserisci Committente"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table class="borderedTable">
                                                                            <tr>
                                                                                <td>
                                                                                    <uc4:CptCommittente ID="CantieriCommittente1" runat="server" />
                                                                                    <br />
                                                                                    <asp:Button ID="ButtonInserisciCommittente" runat="server" OnClick="ButtonInserisciCommittente_Click"
                                                                                        Text="Inserisci committente" Width="170px" ValidationGroup="ValidaInserimentoCommittente" />
                                                                                    <asp:Button ID="ButtonAnnullaInserimentoCommittente" runat="server" OnClick="ButtonAnnullaInserimentoCommittente_Click"
                                                                                        Text="Annulla" Width="170px" CausesValidation="False" /><br />
                                                                                    <asp:Label ID="LabelInserimentoCommittenteRes" runat="server" ForeColor="Red"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                * campi obbligatori</asp:WizardStep>
                            <asp:WizardStep ID="WizardStep2" runat="server" Title="Coordinatori">
                                <asp:Button ID="ButtonCoordinatoriSblocca" runat="server" Text="Modifica Coordinatori"
                                    Visible="False" OnClick="ButtonCoordinatoriSblocca_Click" />
                                <asp:Panel ID="PanelCoordinatori" runat="server">
                                    <table class="standardTable">
                                        <tr>
                                            <td style="width: 93px; height: 25px">
                                                Coordinatore alla sicurezza e salute durante la progettazione
                                            </td>
                                            <td style="height: 25px">
                                                <table class="standardTable">
                                                    <tr>
                                                        <td style="width: 40%">
                                                            Nominativo
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxProgettazioneNominativo" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Ragione sociale
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxProgettazioneRagioneSociale" runat="server" Width="300px"
                                                                CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Indirizzo
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxProgettazioneIndirizzo" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Telefono
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxProgettazioneTelefono" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Fax
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxProgettazioneFax" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="LabelProgettazioneNominativo" runat="server" ForeColor="Red" Text="Inserire il nominativo"
                                                    Visible="False"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 93px">
                                                Coordinatore alla sicurezza e salute durante la realizzazione
                                            </td>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanelRealizzazione" runat="server">
                                                    <ContentTemplate>
                                                        <asp:CheckBox ID="CheckBoxProgettazioneRealizzazione" runat="server" AutoPostBack="True"
                                                            Text="Stessi dati del coordinatore (progettazione)" OnCheckedChanged="CheckBoxProgettazioneRealizzazione_CheckedChanged"
                                                            Width="400px" />
                                                        <asp:Panel ID="PanelRealizzazione" runat="server">
                                                            <br />
                                                            <table class="standardTable">
                                                                <tr>
                                                                    <td style="width: 40%">
                                                                        Nominativo
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxRealizzazioneNominativo" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Ragione sociale
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxRealizzazioneRagioneSociale" runat="server" Width="300px"
                                                                            CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Indirizzo
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxRealizzazioneIndirizzo" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Telefono
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxRealizzazioneTelefono" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Fax
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxRealizzazioneFax" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Label ID="LabelRealizzazioneNominativo" runat="server" ForeColor="Red" Text="Inserire il nominativo"
                                                            Visible="False"></asp:Label>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 93px; height: 18px">
                                                &nbsp;Responsabile lavori
                                            </td>
                                            <td style="height: 18px">
                                                <asp:UpdatePanel ID="UpdatePanelResponsabile" runat="server">
                                                    <ContentTemplate>
                                                        <asp:CheckBox ID="CheckBoxProgettazioneResponsabile" runat="server" AutoPostBack="True"
                                                            Text="Stessi dati del coordinatore (progettazione)" OnCheckedChanged="CheckBoxProgettazioneResponsabile_CheckedChanged"
                                                            Width="400px" />
                                                        <asp:CheckBox ID="CheckBoxResponsabileCommittente" runat="server" AutoPostBack="True"
                                                            Text="Committente" OnCheckedChanged="CheckBoxResponsabileCommittente_CheckedChanged"
                                                            Width="400px" />
                                                        &nbsp;
                                                        <asp:Panel ID="PanelResponsabile" runat="server">
                                                            <br />
                                                            <table class="standardTable">
                                                                <tr>
                                                                    <td>
                                                                        Nominativo
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxResponsabileNominativo" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Ragione sociale
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxResponsabileRagioneSociale" runat="server" Width="300px"
                                                                            CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Indirizzo
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxResponsabileIndirizzo" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Telefono
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxResponsabileTelefono" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Fax
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxResponsabileFax" runat="server" Width="300px" CssClass="campoNotifica"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Label ID="LabelResponsabileNominativo" runat="server" ForeColor="Red" Text="Inserire il nominativo"
                                                            Visible="False"></asp:Label>
                                                        <asp:Label ID="LabelResponsabileDoppioCheck" runat="server" ForeColor="Red" Text="Non � possibile selezionare entrambi i flag"
                                                            Visible="False"></asp:Label>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:WizardStep>
                            <asp:WizardStep ID="WizardStep3" runat="server" Title="Durata &amp; numeri">
                                <asp:Button ID="ButtonDurataSblocca" runat="server" Text="Modifica Durata" Visible="False"
                                    OnClick="ButtonDurataSblocca_Click" />
                                <asp:Panel ID="PanelDurata" runat="server" Width="100%">
                                    <table class="standardTable">
                                        <tr>
                                            <td style="width: 40%">
                                                Data inizio lavori (ggmmaaaa)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxDataInizioLavori" runat="server" Width="150px" MaxLength="8"
                                                    CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBoxDataInizioLavori"
                                                    ErrorMessage="Formato data" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                                    ValidationGroup="step3"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Data fine lavori (ggmmaaaa)
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxDataFineLavori" runat="server" Width="150px" MaxLength="8"
                                                    CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBoxDataFineLavori"
                                                    ErrorMessage="Formato data" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                                                    ValidationGroup="step3"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Durata
                                            </td>
                                            <td colspan="2">
                                                <table class="standardTable">
                                                    <tr>
                                                        <td>
                                                            Giorni
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxDurataGiorni" runat="server" Width="150px" MaxLength="5"
                                                                CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                                ControlToValidate="TextBoxDurataGiorni" ErrorMessage="Formato numerico" ValidationExpression="^\d{1,6}$"
                                                                ValidationGroup="step3"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Settimane
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxDurataSettimane" runat="server" Width="150px" MaxLength="5"
                                                                CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxDurataSettimane"
                                                                ErrorMessage="Formato numerico" ValidationExpression="^\d{1,6}$" ValidationGroup="step3"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Mesi
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxDurataMesi" runat="server" Width="150px" MaxLength="5" CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TextBoxDurataMesi"
                                                                ErrorMessage="Formato numerico" ValidationExpression="^\d{1,6}$" ValidationGroup="step3"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Anni
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBoxDurataAnni" runat="server" Width="150px" MaxLength="5" CssClass="campoNotifica"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                                ControlToValidate="TextBoxDurataAnni" ErrorMessage="Formato numerico" ValidationExpression="^\d{1,6}$"
                                                                ValidationGroup="step3"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Numero totale uomini giorno
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxGiorniUomo" runat="server" Width="150px" MaxLength="5" CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                                    ControlToValidate="TextBoxGiorniUomo" ErrorMessage="Formato numerico" ValidationExpression="^\d{1,6}$"
                                                    ValidationGroup="step3"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Numero massimo lavoratori previsti
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxNumeroLavoratori" runat="server" Width="150px" MaxLength="5"
                                                    CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxNumeroLavoratori"
                                                    ErrorMessage="Formato numerico" ValidationExpression="^\d{1,5}$" ValidationGroup="step3"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Numero previsto imprese
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxNumeroImprese" runat="server" Width="150px" MaxLength="5"
                                                    CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxNumeroImprese"
                                                    ErrorMessage="Formato numerico" ValidationExpression="^\d{1,5}$" ValidationGroup="step3"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Numero lavoratori autonomi previsti
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxNumeroAutonomi" runat="server" Width="150px" MaxLength="5"
                                                    CssClass="campoNotifica"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBoxNumeroAutonomi"
                                                    ErrorMessage="Formato numerico" ValidationExpression="^\d{1,5}$" ValidationGroup="step3"></asp:RegularExpressionValidator>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                &nbsp;</asp:WizardStep>
                            <asp:WizardStep ID="WizardStep5" runat="server" Title="Appalti">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="ButtonAppaltiSblocca" runat="server" Text="Modifica Appalti" Visible="False"
                                            OnClick="ButtonAppaltiSblocca_Click" />
                                        <asp:Panel ID="PanelAppaltiEsterno" runat="server" DefaultButton="ButtonAggiungiAppalto" Width="100%">
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Text="Lista Appalti" Font-Bold="True"></asp:Label>
                                                        <asp:GridView ID="GridViewSubappalti" runat="server" AutoGenerateColumns="False"
                                                            OnRowDeleting="GridViewSubappalti_RowDeleting" Width="100%" OnRowDataBound="GridViewSubappalti_RowDataBound">
                                                            <Columns>
                                                                <asp:BoundField DataField="AppaltataStringa" HeaderText="Impresa selezionata" />
                                                                <asp:BoundField DataField="AppaltanteStringa" HeaderText="Appaltata da" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="ImageButtonDelete" runat="server" CommandName="Delete" ImageUrl="~/images/pallinoX.png"
                                                                            OnClick="ImageButtonDelete_Click" TabIndex="99" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                Nessun appalto presente
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                        <asp:Panel ID="PanelModificaDatiImpresa" runat="server" Visible="false" Width="100%">
                                                            Modifica dati impresa &nbsp;<asp:DropDownList ID="DropDownListModificaImprese" runat="server"
                                                                Width="368px">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="ButtonModificaImpresa" runat="server" CausesValidation="False" Text="Modifica"
                                                                OnClick="ButtonModificaImpresa_Click" /><br />
                                                            <asp:Label ID="LabelErroreModifica" runat="server" ForeColor="Red" Text="Non � possibile modificare un'impresa dell'anagrafica SiceNew"
                                                                Visible="False"></asp:Label><br />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="PanelModificaImpresa" runat="server" Visible="false" Width="100%">
                                                            <table class="filledtable">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label9" runat="server" Font-Bold="True" ForeColor="White" Text="Modifica impresa"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="borderedTable">
                                                                <tr>
                                                                    <td>
                                                                        <br />
                                                                        <uc5:CptImpresa ID="CptImpresaModificaImpresa" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Button ID="ButtonSalvaModifiche" runat="server" Text="Modifica impresa" Width="150px"
                                                                            OnClick="ButtonSalvaModifiche_Click" />&nbsp;
                                                                        <asp:Button ID="ButtonAnnullaModifica" runat="server" Text="Annulla" OnClick="ButtonAnnullaModifica_Click"
                                                                            Width="150px" />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="PanelAppalti" runat="server" Width="100%" Visible="True"
                                                            DefaultButton="ButtonAggiungiAppalto">
                                                            <table class="filledtable">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Crea appalto"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="borderedTable">
                                                                <tr>
                                                                    <td>
                                                                        Impresa selezionata
                                                                    </td>
                                                                    <td>
                                                                        <table class="standardTable">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltataSiceInfo" runat="server" Text="Anagrafica SiceNew"
                                                                                        Enabled="False" GroupName="scelta" />
                                                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltataCantieri" runat="server" Text="Anagrafica Notifiche"
                                                                                        Enabled="False" GroupName="scelta" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="TextBoxImpresaAppaltata" runat="server" Text="" Width="353px" Height="57px"
                                                                                        TextMode="MultiLine" Enabled="False" ReadOnly="True" CssClass="campoNotifica" />
                                                                                    <br />
                                                                                    <asp:Label ID="LabelImpresaSelezionataEsistente" runat="server" ForeColor="Red"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Imprese considerate
                                                                                    <asp:DropDownList ID="DropDownListImpreseGiaSelezionateSel" runat="server" Width="368px">
                                                                                    </asp:DropDownList>
                                                                                    &nbsp;<asp:Button ID="ButtonImpresaGiaSelezionataSel" runat="server" CausesValidation="False"
                                                                                        OnClick="ButtonImpresaGiaSelezionataSel_Click" Text="Seleziona" /><br />
                                                                                    <br />
                                                                                    <asp:UpdateProgress ID="UpdateProgressSubbalpatata" runat="server" AssociatedUpdatePanelID="UpdatePanelSubbalpatata">
                                                                                        <ProgressTemplate>
                                                                                            <center>
                                                                                                <asp:Image ID="ImageProgressSubappaltata" runat="server" ImageUrl="~/images/loading6b.gif" /><asp:Label
                                                                                                    ID="LabelProgressSubappaltata" runat="server" Font-Bold="True" Font-Size="Medium"
                                                                                                    Text="In elaborazione...."></asp:Label>
                                                                                            </center>
                                                                                        </ProgressTemplate>
                                                                                    </asp:UpdateProgress>
                                                                                    <asp:UpdatePanel ID="UpdatePanelSubbalpatata" runat="server">
                                                                                        <ContentTemplate>
                                                                                            <uc6:CantieriRicercaImpresa ID="CantieriRicercaImpresaSubappaltatrice" runat="server"
                                                                                                Visible="True" />
                                                                                            <br />
                                                                                            <asp:Panel ID="PanelInserisciImpresaSubappaltata" runat="server" Width="100%"
                                                                                                Visible="false">
                                                                                                <table class="filledtable">
                                                                                                    <tr>
                                                                                                        <td style="height: 16px">
                                                                                                            <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <table class="borderedTable">
                                                                                                    <tr>
                                                                                                        <td style="width: 730px">
                                                                                                            <uc5:CptImpresa ID="CantieriImpresaSubappaltatrice" runat="server" />
                                                                                                            <br />
                                                                                                            <asp:Button ID="ButtonInserisciNuovaImpresaSubappaltatrice" runat="server" Text="Inserisci nuova impresa"
                                                                                                                Width="170" OnClick="ButtonInserisciNuovaImpresaSubappaltatrice_Click" />
                                                                                                            <asp:Button ID="ButtonAnnullaNuovaImpresaSubappaltatrice" runat="server" Text="Annulla"
                                                                                                                Width="170" OnClick="ButtonAnnullaNuovaImpresaSubappaltatrice_Click" />
                                                                                                            <br />
                                                                                                            <asp:Label ID="LabelNuovaSubappaltatriceRes" runat="server" ForeColor="Red"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:Panel>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        Appaltata da
                                                                    </td>
                                                                    <td>
                                                                        <table class="standardTable">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltatriceSiceInfo" runat="server" Text="Anagrafica SiceNew"
                                                                                        Enabled="False" GroupName="scelta" />
                                                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltatriceCantieri" runat="server" Text="Anagrafica Notifiche"
                                                                                        Enabled="False" GroupName="scelta" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="height: 67px">
                                                                                    <asp:TextBox ID="TextBoxImpresaAppaltatrice" runat="server" Text="" Width="353px"
                                                                                        Height="57px" TextMode="MultiLine" Enabled="False" ReadOnly="True" CssClass="campoNotifica" />&nbsp;<br />
                                                                                    <asp:Label ID="LabelImpresaAppaltataDaEsistente" runat="server" ForeColor="Red"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="right">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Imprese considerate
                                                                                    <asp:DropDownList ID="DropDownListImpreseGiaSelezionateAta" runat="server" Width="368px">
                                                                                    </asp:DropDownList>
                                                                                    &nbsp;
                                                                                    <asp:Button ID="ButtonImpresaGiaSelezionataAta" runat="server" CausesValidation="False"
                                                                                        OnClick="ButtonImpresaGiaSelezionata_Click" Text="Seleziona" /><br />
                                                                                    <br />
                                                                                    <asp:UpdateProgress ID="UpdateProgressAppaltataDa" runat="server" AssociatedUpdatePanelID="UpdatePanelAppaltataDa">
                                                                                        <ProgressTemplate>
                                                                                            <center>
                                                                                                <asp:Image ID="ImageProgressAppaltataDa" runat="server" ImageUrl="~/images/loading6b.gif" /><asp:Label
                                                                                                    ID="LabelProgressAppaltataDa" runat="server" Font-Bold="True" Font-Size="Medium"
                                                                                                    Text="In elaborazione...."></asp:Label>
                                                                                            </center>
                                                                                        </ProgressTemplate>
                                                                                    </asp:UpdateProgress>
                                                                                    <asp:UpdatePanel ID="UpdatePanelAppaltataDa" runat="server">
                                                                                        <ContentTemplate>
                                                                                            &nbsp;<br />
                                                                                            <br />
                                                                                            <uc6:CantieriRicercaImpresa ID="CantieriRicercaImpresaAppaltataDa" runat="server"
                                                                                                Visible="True" />
                                                                                            <br />
                                                                                            <asp:Panel ID="PanelInserisciImpresaAppaltataDa" runat="server" Width="100%"
                                                                                                Visible="false">
                                                                                                <table class="filledtable">
                                                                                                    <tr>
                                                                                                        <td style="height: 16px">
                                                                                                            <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <table class="borderedTable">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <uc5:CptImpresa ID="CantieriImpresaAppaltataDa" runat="server" />
                                                                                                            <asp:Button ID="ButtonInserisciNuovaImpresaAppaltataDa" runat="server" Text="Inserisci nuova impresa"
                                                                                                                Width="170" OnClick="ButtonInserisciNuovaImpresaAppaltataDa_Click" />
                                                                                                            <asp:Button ID="ButtonAnnullaNuovaImpresaAppaltataDa" runat="server" Text="Annulla"
                                                                                                                Width="170" OnClick="ButtonAnnullaNuovaImpresaAppaltataDa_Click" CausesValidation="False" />
                                                                                                            <br />
                                                                                                            <asp:Label ID="LabelNuovaAppaltataDaRes" runat="server" ForeColor="Red"></asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </asp:Panel>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="height: 24px">
                                                                        <asp:Button ID="ButtonAggiungiAppalto" runat="server" OnClick="ButtonAggiungiAppalto_Click"
                                                                            Text="Salva appalto" ValidationGroup="aggiungiAppalto" Width="150px" />&nbsp;
                                                                        <br />
                                                                        <asp:Label ID="LabelRisultatoAppalto" runat="server" ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:WizardStep>
                            <asp:WizardStep runat="server" Title="Riassunto">
                                <asp:Label ID="LabelMessaggio2" runat="server" ForeColor="Red"></asp:Label>
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 40%">
                                            <asp:Label ID="LabelDatiGenerali" runat="server" Font-Bold="True">Dati generali</asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="ImageStatoDatiGeneraliNegativo" runat="server" ImageUrl="~/images/pallinoX.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoDatiGeneraliPositivo" runat="server" ImageUrl="~/images/PallinoVverde.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoDatiGeneraliNeutro" runat="server" ImageUrl="~/images/alert2.png"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Area
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasArea" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Data
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasData" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzi
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasIndirizzi" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Natura dell'opera
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasNaturaOpera" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Numero appalto
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasNumeroAppalto" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ammontare complessivo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasImporto" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 40%">
                                            <asp:Label ID="Label5" runat="server" Font-Bold="True">Committente</asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="ImageStatoCommittenteNegativo" runat="server" ImageUrl="~/images/pallinoX.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoCommittentePositivo" runat="server" ImageUrl="~/images/PallinoVverde.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoCommittenteNeutro" runat="server" ImageUrl="~/images/alert2.png"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ragione sociale
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComRagioneSociale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComIndirizzo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Comune
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComComune" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provincia
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComProvincia" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cap
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComCap" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Partita IVA
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComIva" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Codice fiscale
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComFiscale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telefono
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComTelefono" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fax
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComFax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Persona di riferimento
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasComRiferimento" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 40%">
                                            <asp:Label ID="Label6" runat="server" Font-Bold="True">Coordinatori</asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="ImageStatoCoordinatoriNegativo" runat="server" ImageUrl="~/images/pallinoX.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoCoordinatoriPositivo" runat="server" ImageUrl="~/images/PallinoVverde.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoCoordinatoriNeutro" runat="server" ImageUrl="~/images/alert2.png"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            - &nbsp;Coordinatore alla sicurezza e alla salute durante la progettazione
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nominativo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasProgNominativo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ragione sociale
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasProgRagioneSociale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasProgIndirizzo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telefono
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasProgTelefono" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fax
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasProgFax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            - Coordinatore alla sicurezza e alla salute durante la realizzazione
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nominativo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRealNominativo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ragione sociale
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRealRagioneSociale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRealIndirizzo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telefono
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRealTelefono" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fax
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRealFax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            - Responsabile dei lavori&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="CheckBoxRiasResponsabileCommittente" runat="server" Checked="false"
                                                Enabled="False" Text="Committente" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nominativo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRespNominativo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ragione sociale
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRespRagioneSociale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRespIndirizzo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Telefono
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRespTelefono" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fax
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasRespFax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 40%">
                                            <asp:Label ID="Label7" runat="server" Font-Bold="True">Durata</asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="ImageStatoDurataNegativo" runat="server" ImageUrl="~/images/pallinoX.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoDurataPositivo" runat="server" ImageUrl="~/images/PallinoVverde.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoDurataNeutro" runat="server" ImageUrl="~/images/alert2.png"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Data inizio lavori
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasDataInizio" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Data fine lavori
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasDataFine" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Durata dei lavori (giorni)
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasDurata" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Numero totale uomini giorno&nbsp;
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasGiorniUomo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Numero massimo lavoratori previsti
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasNumeroLavoratori" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Numero previsto imprese
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasNumeroImprese" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Numero lavoratori autonomi previsti
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRiasNumeroAutonomi" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 40%">
                                            <asp:Label ID="Label8" runat="server" Font-Bold="True">Appalti</asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:Image ID="ImageStatoAppaltiNegativo" runat="server" ImageUrl="~/images/pallinoX.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoAppaltiPositivo" runat="server" ImageUrl="~/images/PallinoVverde.png"
                                                Visible="False" />
                                            <asp:Image ID="ImageStatoAppaltiNeutro" runat="server" ImageUrl="~/images/alert2.png"
                                                Visible="False" />
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="RepeaterSubappalti" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    Impresa selezionata
                                                </td>
                                                <td>
                                                    <%# DataBinder.Eval(Container.DataItem, "AppaltataStringa") %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Appaltata da
                                                </td>
                                                <td>
                                                    <%# DataBinder.Eval(Container.DataItem, "AppaltanteStringa") %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                                <br />
                                <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label>
                            </asp:WizardStep>
                        </WizardSteps>
                        <SideBarTemplate>
                            <asp:DataList ID="SideBarList" runat="server">
                                <SelectedItemStyle Font-Bold="True" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="SideBarButton" runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:DataList>
                        </SideBarTemplate>
                        <SideBarStyle VerticalAlign="Top" Width="150px" Wrap="True" />
                        <NavigationButtonStyle BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" />
                        <SideBarButtonStyle Font-Names="Verdana" />
                        <HeaderStyle BorderStyle="Solid" BorderWidth="2px" Font-Bold="True" HorizontalAlign="Center" />
                        <StepNextButtonStyle Width="100px" />
                        <StartNextButtonStyle Width="100px" />
                        <FinishCompleteButtonStyle Width="150px" />
                        <FinishPreviousButtonStyle Width="100px" />
                        <StepPreviousButtonStyle Width="100px" />
                    </asp:Wizard>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        &nbsp;
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
