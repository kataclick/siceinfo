using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CptStampaCodiceNotifica : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita,
                                              "CptStampaCodiceNotifica.aspx");

        if (!string.IsNullOrEmpty(Request.QueryString["idNotifica"]))
        {
            //this.LabelIdNotifica.Text = Request.QueryString["idNotifica"];
            //this.LabelBarreNotifica.Text = "*" + Request.QueryString["idNotifica"] + "*";
            Img1.Src = string.Format("~/Cpt/BarCodeGenerator.aspx?idNotifica={0}", Request.QueryString["idNotifica"]);
        }
    }
}