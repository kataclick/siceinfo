﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Statistiche.aspx.cs" Inherits="Cpt_Statistiche" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche preliminari"
        sottoTitolo="Statistiche" />
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td class="centermain">
                    Anno:
                </td>
                <td>
                    <telerik:RadNumericTextBox NumberFormat-DecimalDigits="0" MinValue="1900" MaxValue="12"
                        NumberFormat-GroupSeparator="" ID="RadTextBoxAnno" runat="server" Width="220px">
                    </telerik:RadNumericTextBox>
                </td>
                <td />
                <td />
            </tr>
        </table>
        <div style="padding: 10px;">
            <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
                Text="Visualizza report" ValidationGroup="VisualizzazioneReport" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataDa" ControlToValidate="RadTextBoxAnno"
                ErrorMessage="Inserire l'anno per il quale si desidera ottenre l'informazione."
                ValidationGroup="VisualizzazioneReport" runat="server" />
        </div>
    </div>
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerCpt" runat="server" ProcessingMode="Remote" ShowFindControls="False"
                    ShowPageNavigationControls="False" ShowRefreshButton="False" ShowZoomControl="False"
                    Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
