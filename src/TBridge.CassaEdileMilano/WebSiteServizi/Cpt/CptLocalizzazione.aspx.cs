using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class Cpt_CptLocalizzazione : Page
{
    private readonly CptBusiness biz = new CptBusiness();
    private readonly int maxPushpin = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPushpin"]);
    private int? idASL;

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        if (GestioneUtentiBiz.IsASL())
        {
            //ASL asl =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.ASL) HttpContext.Current.User.Identity).Entity;
            ASL asl =
                (ASL) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idASL = asl.IdASL;
        }

        if (!Page.IsPostBack)
        {
            CaricaAree();
            SelezionaArea();
        }
    }

    private void CaricaAree()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListArea,
            biz.GetAree(),
            "Descrizione",
            "IdArea");
    }

    private void SelezionaArea()
    {
        // Se l'utente � autorizzato all'inserimento di notifiche per una sola area
        // blocco la selezione
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptRicerca.ToString())
            ^
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicercaLodi.ToString())
            )
        {
            // Seleziono l'area Milano
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicerca.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "1";
            }

            // Seleziono l'area Lodi
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicercaLodi.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "2";
            }

            DropDownListArea.Enabled = false;
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("LoadMap();");

            CantiereFilter filtro = CaricaCantieri(sb);
            RadAjaxPanel1.ResponseScripts.Add(sb.ToString());


            //var utente =
            //    ((IUtente)
            //     HttpContext.Current.User.Identity);
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

            var log = new LogRicerca();
            log.IdUtente = idUtente;
            log.XmlFiltro = biz.GetPropertiesDelFiltro(filtro);
            log.Sezione = SezioneLogRicerca.LocalizzazioneCantieri;

            biz.InsertLogRicerca(log);
        }
    }

    /// <summary>
    /// Carichiamo i cantieri
    /// </summary>
    public CantiereFilter CaricaCantieri(StringBuilder stringBuilder)
    {
        var filtro = new CantiereFilter();

        string comRic = TextBoxCommittente.Text.Trim();
        if (!string.IsNullOrEmpty(comRic))
            filtro.Committente = comRic;

        string indRic = TextBoxIndirizzo.Text.Trim();
        if (!string.IsNullOrEmpty(indRic))
            filtro.Indirizzo = indRic;

        string impRic = TextBoxImpresa.Text.Trim();
        if (!string.IsNullOrEmpty(impRic))
            filtro.Impresa = impRic;

        string natRic = TextBoxNatura.Text.Trim();
        if (!string.IsNullOrEmpty(natRic))
            filtro.NaturaOpera = natRic;

        if (!string.IsNullOrEmpty(TextBoxAmmontare.Text))
            filtro.Ammontare = decimal.Parse(TextBoxAmmontare.Text);

        string dataInizioString = TextBoxDataInizio.Text.Trim();
        if (!string.IsNullOrEmpty(dataInizioString))
        {
            filtro.DataInizio = DateTime.ParseExact(dataInizioString, "ddmmyyyy", null).Date;
        }

        string dataFineString = TextBoxDataFine.Text.Trim();
        if (!string.IsNullOrEmpty(dataFineString))
        {
            filtro.DataFine = DateTime.ParseExact(dataFineString, "ddmmyyyy", null).Date;
        }

        string comuRic = TextBoxComune.Text.Trim();
        if (!string.IsNullOrEmpty(comuRic))
            filtro.Comune = comuRic;

        filtro.Localizzati = true;
        filtro.IdASL = idASL;
        filtro.IdArea = Int16.Parse(DropDownListArea.SelectedValue);

        CantiereNotificaCollection cantieri =
            biz.RicercaCantieriPerCommittente(filtro);

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", cantieri.Count);

        //VEMapCantieri.Pushpins.Clear();

        if (cantieri.Count <= maxPushpin)
        {
            CaricaMappa(cantieri, stringBuilder);
            LabelErrore.Visible = false;
        }
        else
        {
            LabelErrore.Visible = true;
        }

        return filtro;
    }

    private string normalizzaStringa(string stringa, int lunghezza)
    {
        string res = string.Empty;

        if (stringa != null)
        {
            if (stringa.Length <= lunghezza)
                res = Server.HtmlEncode(stringa.Replace('\r', ' ').Replace('\n', ' '));
            else
                res =
                    Server.HtmlEncode(String.Format("{0}..",
                                                    stringa.Substring(0, lunghezza).Replace('\r', ' ').Replace('\n', ' ')));
        }

        return res;
    }

    private void CaricaMappa(CantiereNotificaCollection listaCantieri, StringBuilder stringBuilder)
    {
        try
        {
            int i = 0;
            foreach (CantiereNotifica cantiere in listaCantieri)
            {
                var descrizione = new StringBuilder();
                if (cantiere.Indirizzo.Latitudine.HasValue && cantiere.Indirizzo.Longitudine.HasValue)
                {
                    // Indirizzo del cantiere
                    descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Indirizzo1, 30));
                    descrizione.Append(" ");
                    descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Civico, 10));
                    if (!string.IsNullOrEmpty(cantiere.Indirizzo.InfoAggiuntiva))
                    {
                        descrizione.Append(" (");
                        descrizione.Append(normalizzaStringa(cantiere.Indirizzo.InfoAggiuntiva, 20));
                        descrizione.Append(")");
                    }
                    descrizione.Append("<br />");
                    descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Comune, 30));
                    descrizione.Append(" ");
                    if (!string.IsNullOrEmpty(cantiere.Indirizzo.Provincia))
                    {
                        descrizione.Append(" (");
                        descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Provincia, 20));
                        descrizione.Append(")");
                    }

                    // Informazioni aggiuntive
                    descrizione.Append("<br />");
                    descrizione.Append("<br />");
                    descrizione.Append("<b>");
                    descrizione.Append(normalizzaStringa(cantiere.NaturaOpera, 80));
                    descrizione.Append("</b>");
                    descrizione.Append("<br />");
                    descrizione.Append("<br />");
                    descrizione.Append("Comm.: ");
                    descrizione.Append(normalizzaStringa(cantiere.CommittenteRagioneSociale, 30));
                    descrizione.Append("<br />");
                    descrizione.Append("<br />");

                    AggiungiTabellaVisite(descrizione, cantiere);

                    // Link alla notifica
                    descrizione.Append("<br />");
                    descrizione.Append("<br />");
                    descrizione.Append("<a href=CptNotificaDettagli.aspx?idNotifica=" + cantiere.IdNotifica +
                                       ">Visualizza notifica</a>");

                    string immagineCantiere = "../images/cantiere24.gif";

                    stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", i,
                                                       cantiere.Latitudine.Replace(',', '.'),
                                                       cantiere.Longitudine.Replace(',', '.'), descrizione,
                                                       immagineCantiere));

                    i++;

                    Debug.Write(i);
                }
            }
        }
        catch (Exception exc)
        {
        }
    }


    private void AggiungiTabellaVisite(StringBuilder descrizione, CantiereNotifica cantiere)
    {
        // Tabellina con le visite
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaASLVisualizzazione.ToString())
            && cantiere.NumeroVisiteASL > 0
            )
        {
            descrizione.Append("Visite ASL: ");
            descrizione.Append(cantiere.NumeroVisiteASL);
            descrizione.Append("<br />");
        }

        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione.ToString())
            && cantiere.NumeroVisiteCPT > 0
            )
        {
            descrizione.Append("Visite CPT: ");
            descrizione.Append(cantiere.NumeroVisiteCPT);
            descrizione.Append("<br />");
        }

        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione.ToString())
            && cantiere.NumeroVisiteDPL > 0
            )
        {
            descrizione.Append("Visite DPL: ");
            descrizione.Append(cantiere.NumeroVisiteDPL);
            descrizione.Append("<br />");
        }

        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione.ToString())
            && cantiere.NumeroVisiteASLERSLT > 0
            )
        {
            descrizione.Append("Visite ASLE/RSLT: ");
            descrizione.Append(cantiere.NumeroVisiteASLERSLT);
            descrizione.Append("<br />");
        }

        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptAttivitaCassaEdileVisualizzazione.ToString())
            && cantiere.NumeroVisiteCassaEdile > 0
            )
        {
            descrizione.Append("Visite Cassa Edile: ");
            descrizione.Append(cantiere.NumeroVisiteCassaEdile);
            descrizione.Append("<br />");
        }
    }
}