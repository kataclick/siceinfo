<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CptNotificaDettagli.aspx.cs" Inherits="Cpt_CptNotificaDettagli" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CptRiassuntoTelematiche.ascx" TagName="CptRiassuntoTelematiche"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Dettagli notifica"
        titolo="Notifiche" />
    <br />
    <asp:Panel ID="PanelNotificaTradizionale" runat="server" Visible="true">
        <table class="standardTable">
            <tr>
                <td style="width: 40%">
                    <asp:Label ID="LabelDatiGenerali" runat="server" Font-Bold="True">Dati generali</asp:Label>
                </td>
                <td align="right">
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Tipo notifica
                </td>
                <td>
                    <asp:Label ID="LabelTipoNotifica" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Area
                </td>
                <td>
                    <asp:Label ID="LabelArea" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Protocollo notifica
                </td>
                <td>
                    <asp:Label ID="LabelProtocollo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Data
                </td>
                <td>
                    <asp:Label ID="LabelRiasData" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Indirizzi
                </td>
                <td>
                    <asp:Label ID="LabelRiasIndirizzi" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Natura dell'opera
                </td>
                <td>
                    <asp:Label ID="LabelRiasNaturaOpera" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero appalto
                </td>
                <td>
                    <asp:Label ID="LabelRiasNumeroAppalto" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero massimo lavoratori previsti
                </td>
                <td>
                    <asp:Label ID="LabelRiasNumeroLavoratori" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero previsto imprese
                </td>
                <td>
                    <asp:Label ID="LabelRiasNumeroImprese" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero lavoratori autonomi previsti
                </td>
                <td>
                    <asp:Label ID="LabelRiasNumeroAutonomi" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Ammontare complessivo
                </td>
                <td>
                    <asp:Label ID="LabelRiasImporto" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <hr />
        <br />
        <table class="standardTable">
            <tr>
                <td style="width: 40%">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True">Committente</asp:Label>
                </td>
                <td align="right">
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Ragione sociale
                </td>
                <td>
                    <asp:Label ID="LabelRiasComRagioneSociale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Indirizzo
                </td>
                <td>
                    <asp:Label ID="LabelRiasComIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Comune
                </td>
                <td>
                    <asp:Label ID="LabelRiasComComune" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia
                </td>
                <td>
                    <asp:Label ID="LabelRiasComProvincia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Cap
                </td>
                <td>
                    <asp:Label ID="LabelRiasComCap" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Partita IVA
                </td>
                <td>
                    <asp:Label ID="LabelRiasComIva" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Codice fiscale
                </td>
                <td>
                    <asp:Label ID="LabelRiasComFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Telefono
                </td>
                <td>
                    <asp:Label ID="LabelRiasComTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Fax
                </td>
                <td>
                    <asp:Label ID="LabelRiasComFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Persona di riferimento
                </td>
                <td>
                    <asp:Label ID="LabelRiasComRiferimento" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Panel
            ID="PanelPersone"
            runat="server"
            Visible="false">
        <hr />
        <br />
        <table class="standardTable">
            <tr>
                <td style="width: 40%">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True">Coordinatori</asp:Label>
                </td>
                <td align="right">
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    - &nbsp;Coordinatore alla sicurezza e alla salute durante la progettazione
                </td>
            </tr>
            <tr>
                <td>
                    Nominativo
                </td>
                <td>
                    <asp:Label ID="LabelRiasProgNominativo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Ragione sociale
                </td>
                <td>
                    <asp:Label ID="LabelRiasProgRagioneSociale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Indirizzo
                </td>
                <td>
                    <asp:Label ID="LabelRiasProgIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Telefono
                </td>
                <td>
                    <asp:Label ID="LabelRiasProgTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Fax
                </td>
                <td>
                    <asp:Label ID="LabelRiasProgFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    - Coordinatore alla sicurezza e alla salute durante la realizzazione
                </td>
            </tr>
            <tr>
                <td>
                    Nominativo
                </td>
                <td>
                    <asp:Label ID="LabelRiasRealNominativo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Ragione sociale
                </td>
                <td>
                    <asp:Label ID="LabelRiasRealRagioneSociale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Indirizzo
                </td>
                <td>
                    <asp:Label ID="LabelRiasRealIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Telefono
                </td>
                <td>
                    <asp:Label ID="LabelRiasRealTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Fax
                </td>
                <td>
                    <asp:Label ID="LabelRiasRealFax" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    - Responsabile dei lavori&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxRiasRespCommittente" runat="server" Enabled="False" Text="Committente" />
                </td>
            </tr>
            <tr>
                <td>
                    Nominativo
                </td>
                <td>
                    <asp:Label ID="LabelRiasRespNominativo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Ragione sociale
                </td>
                <td>
                    <asp:Label ID="LabelRiasRespRagioneSociale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Indirizzo
                </td>
                <td>
                    <asp:Label ID="LabelRiasRespIndirizzo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Telefono
                </td>
                <td>
                    <asp:Label ID="LabelRiasRespTelefono" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Fax
                </td>
                <td>
                    <asp:Label ID="LabelRiasRespFax" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        </asp:Panel>
        <hr />
        <br />
        <table class="standardTable">
            <tr>
                <td style="width: 40%">
                    <asp:Label ID="Label7" runat="server" Font-Bold="True">Durata presunta</asp:Label>
                </td>
                <td align="right">
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Data inizio lavori
                </td>
                <td>
                    <asp:Label ID="LabelRiasDataInizio" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Data fine lavori
                </td>
                <td>
                    <asp:Label ID="LabelRiasDataFine" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Durata dei lavori (giorni)
                </td>
                <td>
                    <asp:Label ID="LabelRiasDurata" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero totale uomini giorno&nbsp;
                </td>
                <td>
                    <asp:Label ID="LabelRiasGiorniUomo" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <hr />
        <br />
        <table class="standardTable">
            <tr>
                <td style="width: 40%">
                    <asp:Label ID="Label8" runat="server" Font-Bold="True">Appalti</asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <asp:Repeater ID="RepeaterSubappalti" runat="server" OnItemDataBound="RepeaterSubappalti_OnItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td>
                            Impresa selezionata&nbsp;
                        </td>
                        <td colspan="2">
                            <asp:Label ID="LabelAppaltataId" runat="server"></asp:Label>
                            &nbsp;<%# DataBinder.Eval(Container.DataItem, "AppaltataStringa") %></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            C.F.
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "AppaltataCodiceFiscale") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            P.IVA
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "AppaltataPartitaIva") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            Indirizzo
                        </td>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "AppaltataIndirizzo") %>
                        </td>
                    </tr>
                    <asp:Panel ID="PanelRip" runat="server">
                        <tr>
                            <td>
                                Appaltata da&nbsp;
                            </td>
                            <td colspan="2">
                                <asp:Label ID="LabelAppaltanteId" runat="server"></asp:Label>
                                &nbsp;<%# DataBinder.Eval(Container.DataItem, "AppaltanteStringa") %></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                C.F.
                            </td>
                            <td>
                                <%# DataBinder.Eval(Container.DataItem, "AppaltanteCodiceFiscale") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                P.IVA
                            </td>
                            <td>
                                <%# DataBinder.Eval(Container.DataItem, "AppaltantePartitaIva") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Indirizzo
                            </td>
                            <td>
                                <%# DataBinder.Eval(Container.DataItem, "AppaltanteIndirizzo") %>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelNotificaTelematica" runat="server" Visible="false">
        <uc3:CptRiassuntoTelematiche ID="CptRiassuntoTelematiche1" runat="server" />
    </asp:Panel>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Torna indietro" />
    <asp:Button ID="ButtonReport" runat="server" Text="Report" OnClick="ButtonReport_Click" />
    <asp:Button ID="ButtonDocumento" runat="server" Text="Documento" OnClick="ButtonDocumento_Click" />
</asp:Content>
