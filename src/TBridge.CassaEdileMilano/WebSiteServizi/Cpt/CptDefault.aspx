<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CptDefault.aspx.cs" Inherits="CptDefault" %>

<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Benvenuto nella sezione Notifiche Preliminari"
        titolo="Notifiche" />
    <br />
    <p class="DefaultPage">
        <i>
        Cassa Edile di Milano, in qualit� di organismo paritetico, acquisisce i dati relativi 
        alle notifiche preliminari di avvio lavori nei cantieri edili tramite consultazione del 
        sistema informatizzato di Regione Lombardia, in conformit� a quanto disposto dal Decreto 
        del Direttore Generale della Sanit� n. 9056 del 14 settembre 2009.
        <br />
        Ci� premesso, � opportuno precisare che Cassa Edile di Milano ripropone i contenuti delle 
        notifiche preliminari estratti dal sistema informatizzato predisposto da Regione Lombardia 
        e non si assume alcuna responsabilit� circa la veridicit�, l�esattezza, la completezza e 
        l�aggiornamento degli stessi.
        </i>
    </p>
    <p class="DefaultPage">
        Selezionando le voci riportate nel men� verticale di sinistra � possibile ricercare
        le notifiche preliminari disponibili a sistema ed i relativi cantieri, che grazie
        alla voce �Localizzazione cantieri� sono georeferenziati sulla cartina del comprensorio
        di pertinenza della Cassa (Comuni delle Province di Milano, Lodi, Monza e Brianza).
    </p>
    <p class="DefaultPage">
        La voce <a href="~/Cpt/CptRicercaNotifiche.aspx" runat="server">Ricerca notifiche</a>
        consente di effettuare una ricerca valorizzando uno o pi� campi riportati nel filtro
        di ricerca (data notifica, Committente, indirizzo, CAP e Comune dell�appalto, ragione
        sociale e P.IVA/Codice Fiscale dell'impresa esecutrice delle opere, valore dell�appalto,
        data inizio/fine lavori, n. appalto). L'esito della ricerca permette di visualizzare
        in sintesi il primo inserimento delle notifiche trovate con la possibilit� di accedere
        ai "<i>dettagli dell'ultimo aggiornamento</i>". Nel riquadro a destra collocato
        a fianco di ogni notifica, viene riportato lo storico della notifica preliminare
        con i dettagli riferiti a ciascun aggiornamento e la possibilit� di scaricare il
        file relativo alla documentazione originale della notifica ("<i>Documento</i>").
        Il comando "<i>Report</i>" riepiloga i dettagli della notifica e ne consente l�esportazione
        in diversi formati (excel, PDF, ecc.).
    </p>
    <p class="DefaultPage">
        Anche la <a href="~/Cpt/CptRicercaCantiere.aspx" runat="server">Ricerca cantieri</a>
        viene eseguita tramite inserimento di uno o pi� dati nel filtro di ricerca (Committente,
        impresa esecutrice dei lavori, Comune e indirizzo dell'appalto, tipologia di opere
        svolte, valore dell'appalto, data inizio/fine lavori). L�esito della ricerca, nella
        quale vengono elencati tutti i cantieri trovati sulla base dei dati imputati nel
        filtro, pu� essere ordinato per committente o per impresa.
    </p>
    <p class="DefaultPage">
        Infine, la voce <a id="A1" href="~/Cpt/CptLocalizzazione.aspx" runat="server">Localizzazione
            cantieri</a>, consente la visualizzazione di tutti i cantieri aperti sul territorio
        di competenza della Cassa che possono essere ricercati tramite i medesimi criteri
        di selezione gi� prospettati alla voce "<i>Ricerca cantieri</i>". I cantieri trovati
        vengono raffigurati dal simbolo dei lavori in corso. Cliccando pi� volte sulla cartina
        si ha un ingrandimento della zona geografica di interesse. Una volta individuato
        il cantiere ricercato � possibile consultarne la scheda riepilogativa dei principali
        dati (indirizzo e Comune del cantiere, committente, breve descrizione opere), posizionandosi
        con il mouse sul simbolo gi� citato dei lavori in corso. La dicitura "<i>Visualizza
            notifica</i>" riportata in fondo alla scheda, se selezionata, consente l'accesso
        ai dettagli. Anche in questo caso, sono disponibili i comandi "<i>Documento</i>"
        e "<i>Report</i>".
    </p>
    <br />
</asp:Content>
