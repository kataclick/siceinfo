<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CPTStampaProtocolloNotifica.aspx.cs" Inherits="CPTStampaProtocolloNotifica" %>

<%@ Register Src="~/WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa protocollo" titolo="Notifiche" />
    <br />
    <br />
    La notifica � stato corettamente inserita con il codice riportato di seguito:<br />
    <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Large">Protocollo notifica</asp:Label>
        <br />
        <br />
    &nbsp;<asp:Label ID="LabelIdNotifica" runat="server" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
                <br />
    <br />
    <br />
    Per stampare il numero di protocollo da allegare alla notifica premere stampa.<br />
                <br />
                <br />
        <asp:Button ID="ButtonStampa" runat="server" Text="Stampa" Width="170px" />
        <asp:Button ID="ButtonProsegui" runat="server" Text="Torna alla pagina principale" Width="190px" OnClick="ButtonProsegui_Click" />

</asp:Content>



