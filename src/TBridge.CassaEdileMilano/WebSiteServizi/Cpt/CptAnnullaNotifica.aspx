<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CptAnnullaNotifica.aspx.cs" Inherits="Cpt_CptAnnullaNotifica" %>

<%@ Register Src="../WebControls/CptDettagliNotifica.ascx" TagName="CptDettagliNotifica"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Annullamento notifiche"
        titolo="Notifiche" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Protocollo
            </td>
            <td>
                <asp:TextBox ID="TextBoxIdNotifica" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxIdNotifica"
                    ErrorMessage="Codice numerico" ValidationExpression="^\d{1,}$" ValidationGroup="seleziona"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
                <asp:Button ID="ButtonAnnulla" Text="Annulla notifica" runat="server" OnClick="ButtonAnnulla_Click" ValidationGroup="seleziona" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="PanelDettagli" runat="server" Visible="False"
                    width="100%">
                    <uc3:CptDettagliNotifica ID="CptDettagliNotifica1" runat="server" Visible="true" />
                    <asp:Button ID="ButtonConferma" runat="server" OnClick="ButtonConferma_Click" Text="Conferma annullamento" /></asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>

