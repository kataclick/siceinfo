using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Cpt_ModificaVisita : Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCPTInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaDPLInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLERSLTInserimento);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCassaEdileInserimento);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "~/Cpt/InserimentoAttivita.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdVisita"] != null)
            {
                int idVisita = (int) Context.Items["IdVisita"];
                ViewState["IdVisita"] = idVisita;

                CaricaVisita(idVisita);
            }
        }
    }

    private void CaricaVisita(int idVisita)
    {
        Visita visita = biz.GetVisita(idVisita);
        CptDatiAttivita1.CaricaVisita(visita);
        CptAllegatiAttivita1.ImpostaAllegati(visita.Allegati, visita.IdVisita.Value);
    }

    protected void ButtonModifica_Click(object sender, EventArgs e)
    {
        Visita visita = CptDatiAttivita1.CreaVisita();
        visita.IdVisita = (int) ViewState["IdVisita"];

        if (biz.UpdateVisita(visita))
        {
            LabelMessaggio.Text = "Aggiornamento effettuato";

            int idVisita = (int) ViewState["IdVisita"];
            CaricaVisita(idVisita);
        }
        else
        {
            LabelMessaggio.Text = "Errore durante l'aggiornamento";
        }
    }
}