<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CptRicercaCantiere.aspx.cs" Inherits="Cpt_CprRicercaCantiere" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ricerca cantieri"
        titolo="Notifiche" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Committente"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Impresa"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Indirizzo"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Comune"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxCommittente" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxImpresa" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxComune" runat="server" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Natura opera"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Ammontare (>)"></asp:Label>
                <asp:CompareValidator ID="CompareValidatorAmmontare" runat="server" ControlToValidate="TextBoxAmmontare"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage="*" ValidationGroup="ricercaCantieri">
                </asp:CompareValidator>
            </td>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Inizio lavori (>)(ggmmaaaa)"></asp:Label>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizio" runat="server"
                    ControlToValidate="TextBoxDataInizio" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                    ValidationGroup="ricercaCantieri"></asp:RegularExpressionValidator>
            </td>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Fine lavori (<)(ggmmaaaa)"></asp:Label>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFine" runat="server"
                    ControlToValidate="TextBoxDataFine" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                    ValidationGroup="ricercaCantieri"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxNatura" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAmmontare" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxDataInizio" runat="server" Width="100%" MaxLength="8"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxDataFine" runat="server" Width="100%" MaxLength="8"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Area
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" ControlToValidate="DropDownListArea"
                    ValidationGroup="ricercaCantieri" ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
            <td>
                Numero lavoratori (>)
                <asp:RangeValidator ID="CompareValidatorNUmeroLavoratori" runat="server" ControlToValidate="TextBoxNumeroLavoratori"
                    ValidationGroup="ricercaCantieri" ErrorMessage="*" Type="Integer" MinimumValue="0"
                    MaximumValue="9999">
                </asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="DropDownListArea" runat="server" AppendDataBoundItems="true"
                    Width="100%">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="TextBoxNumeroLavoratori" runat="server" Width="100%" MaxLength="4"></asp:TextBox>
            </td>
        </tr>
    </table>
    Tipo di ordinamento:
    <asp:RadioButton ID="RadioButtonCommittente" runat="server" Checked="True" Text="per committente"
        GroupName="radioTipoOrdinamento" />
    &nbsp;
    <asp:RadioButton ID="RadioButtonImpresa" runat="server" Text="per impresa" GroupName="radioTipoOrdinamento" />
    <br />
    <br />
    <asp:Button ID="ButtonRicerca" runat="server" OnClick="ButtonRicerca_Click" Text="Ricerca"
        Width="150px" ValidationGroup="ricercaCantieri" />
    &nbsp;
    <asp:Button ID="ButtonEstrai" runat="server" OnClick="ButtonEstrai_Click" Text="Estraz. Excel"
        Width="150px" ValidationGroup="ricercaCantieri" />
    <br />
    <br />
    <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Cantieri trovati" Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewCantieriCommittente" runat="server" Width="100%" AutoGenerateColumns="False"
        OnRowDataBound="GridViewCantieriCommittente_RowDataBound" AllowPaging="True"
        OnPageIndexChanging="GridViewCantieriCommittente_PageIndexChanging" DataKeyNames="IdNotifica"
        OnSelectedIndexChanging="GridViewCantieriCommittente_SelectedIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="Committente">
                <ItemTemplate>
                    <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Impresa">
                <ItemTemplate>
                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
            <asp:BoundField DataField="DataInizioLavori" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data inizio lav."
                HtmlEncode="False" />
            <asp:BoundField DataField="DataFineLavori" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data fine lav."
                HtmlEncode="False" />
            <asp:BoundField DataField="NaturaOpera" HeaderText="Natura opera" />
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Notifica"
                ShowSelectButton="True">
                <ItemStyle Width="10px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessun cantiere trovato<br />
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:GridView ID="GridViewCantieriImpresa" runat="server" Width="100%" AutoGenerateColumns="False"
        OnRowDataBound="GridViewCantieriImpresa_RowDataBound" Visible="False" AllowPaging="True"
        OnPageIndexChanging="GridViewCantieriImpresa_PageIndexChanging" DataKeyNames="IdNotifica"
        OnSelectedIndexChanging="GridViewCantieriImpresa_SelectedIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="Impresa">
                <ItemTemplate>
                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CommittenteRagioneSociale" HeaderText="Committente" />
            <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
            <asp:BoundField DataField="DataInizioLavori" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data inizio lav."
                HtmlEncode="False" />
            <asp:BoundField DataField="DataFineLavori" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data fine lav."
                HtmlEncode="False" />
            <asp:BoundField DataField="NaturaOpera" HeaderText="Natura opera" />
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Notifica"
                ShowSelectButton="True">
                <ItemStyle Width="10px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessun cantiere trovato
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
</asp:Content>
