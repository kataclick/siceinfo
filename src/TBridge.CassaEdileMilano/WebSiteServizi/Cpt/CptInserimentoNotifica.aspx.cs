using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Business;
using TBridge.Cemi.Cantieri.Type.Collections;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;
using Committente = TBridge.Cemi.Cantieri.Type.Entities.Committente;
using Impresa = TBridge.Cemi.Cantieri.Type.Entities.Impresa;
using Subappalto = TBridge.Cemi.Cpt.Type.Entities.Subappalto;
using SubappaltoCollection = TBridge.Cemi.Cpt.Type.Collections.SubappaltoCollection;

public partial class CptInserimentoNotifica : Page
{
    private const string APPALTI = "Appalti";
    private const string COORDINATORI = "Coordinatori";
    private const string DATIGENERALI = "Dati generali";
    private const string DURATA = "Durata & numeri";
    private const int indiceAPPALTI = 3;
    private const int indiceCOORDINATORI = 1;
    private const int indiceDATIGENERALI = 0;
    private const int indiceDURATA = 2;
    private const int indiceRiassunto = 4;

    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #region Registriamo gli eventi ai controlli

        //Registriamo gli eventi di click sul controllo ricerca committente per sapere quando
        //viene selzionato un committente e quando viene richiesto l'inserimento di un nuovo committente
        CantieriRicercaCommittente1.OnCommittenteSelected +=
            CantieriRicercaCommittente1_OnCommittenteSelected;
        CantieriRicercaCommittente1.OnNuovoCommittenteSelected +=
            CantieriRicercaCommittente1_OnNuovoCommittenteSelected;
        CantieriRicercaCommittente1.ModalitaNotifiche();

        //Registriamo gli eventi del controllo
        CantieriRicercaImpresaAppaltataDa.OnImpresaSelected +=
            CantieriRicercaImpresa1_OnImpresaSelected;
        CantieriRicercaImpresaAppaltataDa.OnNuovaImpresaSelected +=
            CantieriRicercaImpresaAppaltataDa_OnNuovaImpresaSelected;
        CantieriRicercaImpresaSubappaltatrice.OnImpresaSelected +=
            CantieriRicercaImpresa2_OnImpresaSelected;
        CantieriRicercaImpresaSubappaltatrice.OnNuovaImpresaSelected +=
            CantieriRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected;
        CantieriRicercaImpresaAppaltataDa.ModalitaNotifiche();
        CantieriRicercaImpresaSubappaltatrice.ModalitaNotifiche();

        #endregion

        #region impostiamo i validation group

        //Settiamo il validation group cross-control
        string validationGroupImpresa1 = "validationGroupImpresaAppDa";
        ButtonInserisciNuovaImpresaAppaltataDa.ValidationGroup = validationGroupImpresa1;
        CantieriImpresaAppaltataDa.SetValidationGroup(validationGroupImpresa1);

        //Settiamo il validation group cross-control
        string validationGroupImpresa2 = "validationGroupImpresaSubAp";
        ButtonInserisciNuovaImpresaSubappaltatrice.ValidationGroup = validationGroupImpresa2;
        CantieriImpresaSubappaltatrice.SetValidationGroup(validationGroupImpresa2);

        #endregion

        #region impostiamo le logiche di biz ai controlli

        CantieriCommittente1.TsBiz = new TSBusiness();
        CantieriCommittente1.CantieriBiz = new CantieriBusiness();

        CantieriImpresaAppaltataDa.TsBiz = new TSBusiness();
        CantieriImpresaAppaltataDa.CantieriBiz = new CantieriBusiness();
        CantieriImpresaSubappaltatrice.TsBiz = new TSBusiness();
        CantieriImpresaSubappaltatrice.CantieriBiz = new CantieriBusiness();

        #endregion

        //Carichiamo la lista dei subappalti
        CaricaSubappalti();

        if (!Page.IsPostBack)
        {
            CaricaAree();

            if (Request.QueryString["idNotificaPadre"] != null)
            {
                // E' un aggiornamento, devo impostare la notifica padre e bloccare tutti i pannelli
                LabelTitolo.Text = "AGGIORNAMENTO NOTIFICA PRELIMINARE";

                //impostiamo 
                PanelDatiGenerali.Enabled = false;
                //PanelCommittente.Enabled = false;
                PanelCoordinatori.Enabled = false;
                PanelDurata.Enabled = false;
                PanelAppaltiEsterno.Enabled = false;
                ButtonDatiGeneraliSblocca.Visible = true;
                //ButtonCommittenteSblocca.Visible = true;
                ButtonCoordinatoriSblocca.Visible = true;
                ButtonDurataSblocca.Visible = true;
                ButtonAppaltiSblocca.Visible = true;
                CaricaDatiNotificaPadre(Int32.Parse(Request.QueryString["idNotificaPadre"]));
            }
            else
            {
                SelezionaArea();
            }

            // Per mantenere la lista delle imprese gi� selezionate
            ViewState["Imprese"] = new ImpresaCollection();
        }

        # region Per prevenire click multipli

        Button bConferma =
            (Button)
            WizardInserimento.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");

        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(bConferma, null) + ";");
        sb.Append("return true;");
        bConferma.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(bConferma);

        #endregion
    }

    private void SelezionaArea()
    {
        // Se l'utente � autorizzato all'inserimento di notifiche per una sola area
        // blocco la selezione
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica.ToString())
            ^
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi.ToString())
            )
        {
            // Seleziono l'area Milano
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica.ToString()))
            {
                DropDownListArea.SelectedValue = "1";
            }

            // Seleziono l'area Lodi
            if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi.ToString()))
            {
                DropDownListArea.SelectedValue = "2";
            }

            DropDownListArea.Enabled = false;
        }
    }

    private void CaricaAree()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListArea,
            biz.GetAree(),
            "Descrizione",
            "IdArea");
    }

    private void CaricaImpreseGiaSelezionate()
    {
        DropDownListImpreseGiaSelezionateAta.DataSource = ViewState["Imprese"];
        DropDownListImpreseGiaSelezionateAta.DataTextField = "RagioneSociale";
        DropDownListImpreseGiaSelezionateAta.DataValueField = "IdImpresaComposto";
        DropDownListImpreseGiaSelezionateAta.DataBind();

        DropDownListImpreseGiaSelezionateSel.DataSource = ViewState["Imprese"];
        DropDownListImpreseGiaSelezionateSel.DataTextField = "RagioneSociale";
        DropDownListImpreseGiaSelezionateSel.DataValueField = "IdImpresaComposto";
        DropDownListImpreseGiaSelezionateSel.DataBind();

        DropDownListModificaImprese.DataSource = ViewState["Imprese"];
        DropDownListModificaImprese.DataTextField = "RagioneSociale";
        DropDownListModificaImprese.DataValueField = "IdImpresaComposto";
        DropDownListModificaImprese.DataBind();
    }

    /// <summary>
    ///   Carica nel wizard i dati derivati dalla notifica padre
    /// </summary>
    /// <param name = "idNotifica"></param>
    private void CaricaDatiNotificaPadre(int idNotifica)
    {
        ViewState["IdNotificaPadre"] = idNotifica;
        Notifica notificaPadre = biz.GetNotificaUltimaVersione(idNotifica);

        PanelAggiornamento.Visible = true;

        LabelNotificaPadreArea.Text = notificaPadre.Area.Descrizione;
        LabelNotificaPadreData.Text = notificaPadre.Data.ToShortDateString();
        LabelNotificaPadreNatura.Text = notificaPadre.NaturaOpera;
        LabelNotificaPadreCommittente.Text = notificaPadre.CommittenteRagioneSociale;

        // Dati generali
        DropDownListArea.SelectedValue = notificaPadre.Area.IdArea.ToString();
        DropDownListArea.Enabled = false;
        TextBoxData.Text = notificaPadre.Data.ToString("ddMMyyyy");
        CptGestioneIndirizzi1.CaricaIndirizzi(notificaPadre.Indirizzi);
        TextBoxNaturaOpera.Text = notificaPadre.NaturaOpera;
        TextBoxNumeroAppalto.Text = notificaPadre.NumeroAppalto;
        TextBoxNumeroLavoratori.Text = notificaPadre.NumeroMassimoLavoratori.ToString();
        TextBoxNumeroImprese.Text = notificaPadre.NumeroImprese.ToString();
        TextBoxNumeroAutonomi.Text = notificaPadre.NumeroLavoratoriAutonomi.ToString();
        TextBoxImporto.Text = notificaPadre.AmmontareComplessivo.ToString();

        // Committente
        CommittenteSelezionato(notificaPadre.Committente);

        // Coordinatori
        if (notificaPadre.CoordinatoreSicurezzaProgettazione != null)
        {
            TextBoxProgettazioneNominativo.Text = notificaPadre.CoordinatoreSicurezzaProgettazione.Nominativo;
            TextBoxProgettazioneRagioneSociale.Text = notificaPadre.CoordinatoreSicurezzaProgettazione.RagioneSociale;
            TextBoxProgettazioneIndirizzo.Text = notificaPadre.CoordinatoreSicurezzaProgettazione.Indirizzo;
            TextBoxProgettazioneTelefono.Text = notificaPadre.CoordinatoreSicurezzaProgettazione.Telefono;
            TextBoxProgettazioneFax.Text = notificaPadre.CoordinatoreSicurezzaProgettazione.Fax;
        }
        if (notificaPadre.CoordinatoreSicurezzaRealizzazione != null)
        {
            if (notificaPadre.CoordinatoreSicurezzaProgettazione != null &&
                notificaPadre.CoordinatoreSicurezzaProgettazione.IdPersona ==
                notificaPadre.CoordinatoreSicurezzaRealizzazione.IdPersona)
            {
                CheckBoxProgettazioneRealizzazione.Checked = true;
            }
            else
            {
                TextBoxRealizzazioneNominativo.Text = notificaPadre.CoordinatoreSicurezzaRealizzazione.Nominativo;
                TextBoxRealizzazioneRagioneSociale.Text =
                    notificaPadre.CoordinatoreSicurezzaRealizzazione.RagioneSociale;
                TextBoxRealizzazioneIndirizzo.Text = notificaPadre.CoordinatoreSicurezzaRealizzazione.Indirizzo;
                TextBoxRealizzazioneTelefono.Text = notificaPadre.CoordinatoreSicurezzaRealizzazione.Telefono;
                TextBoxRealizzazioneFax.Text = notificaPadre.CoordinatoreSicurezzaRealizzazione.Fax;
            }
        }
        if (notificaPadre.DirettoreLavori != null)
        {
            if (notificaPadre.CoordinatoreSicurezzaProgettazione != null &&
                notificaPadre.CoordinatoreSicurezzaProgettazione.IdPersona == notificaPadre.DirettoreLavori.IdPersona)
            {
                CheckBoxProgettazioneResponsabile.Checked = true;
            }
            else
            {
                TextBoxResponsabileNominativo.Text = notificaPadre.DirettoreLavori.Nominativo;
                TextBoxResponsabileRagioneSociale.Text = notificaPadre.DirettoreLavori.RagioneSociale;
                TextBoxResponsabileIndirizzo.Text = notificaPadre.DirettoreLavori.Indirizzo;
                TextBoxResponsabileTelefono.Text = notificaPadre.DirettoreLavori.Telefono;
                TextBoxResponsabileFax.Text = notificaPadre.DirettoreLavori.Fax;
            }
        }
        else
        {
            CheckBoxResponsabileCommittente.Checked = notificaPadre.ResponsabileCommittente;
        }

        // Durata
        if (notificaPadre.DataInizioLavori.HasValue)
            TextBoxDataInizioLavori.Text = notificaPadre.DataInizioLavori.Value.ToString("ddMMyyyy");
        if (notificaPadre.DataFineLavori.HasValue)
            TextBoxDataFineLavori.Text = notificaPadre.DataFineLavori.Value.ToString("ddMMyyyy");
        if (notificaPadre.Durata.HasValue)
            TextBoxDurataGiorni.Text = notificaPadre.Durata.ToString();
        if (notificaPadre.NumeroGiorniUomo.HasValue)
            TextBoxGiorniUomo.Text = notificaPadre.NumeroGiorniUomo.ToString();

        // Appalti
        if (notificaPadre.Subappalti != null)
            foreach (Subappalto subappalto in notificaPadre.Subappalti)
            {
                subappalto.IdSubappalto = null;
            }
        ViewState["Subappalti"] = notificaPadre.Subappalti;
        CaricaSubappalti();
    }

    /// <summary>
    ///   Carica la lista dei subappalti
    /// </summary>
    private void CaricaSubappalti()
    {
        if (ViewState["Subappalti"] == null)
            ViewState["Subappalti"] = new SubappaltoCollection();

        GridViewSubappalti.DataSource = ViewState["Subappalti"];
        GridViewSubappalti.DataBind();
    }

    /// <summary>
    ///   Salvataggio della notifica
    /// </summary>
    /// <param name = "sender"></param>
    /// <param name = "e"></param>
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        bool bloccato = (bool) ViewState["Bloccato"];

        if (!bloccato)
        {
            Notifica notifica = new Notifica();

            Utente utente = (Utente) Membership.GetUser();
            notifica.Utente = utente.UserName;

            // Dati generali
            notifica.Area = new Area();
            notifica.Area.IdArea = Int16.Parse(DropDownListArea.SelectedItem.Value);
            notifica.Area.Descrizione = DropDownListArea.SelectedItem.Text;

            string data = LabelRiasData.Text.Substring(0, 2) + "/" + LabelRiasData.Text.Substring(2, 2) + "/" +
                          LabelRiasData.Text.Substring(4, 4);
            notifica.Data = DateTime.Parse(data);
            notifica.Indirizzi = CptGestioneIndirizzi1.GetIndirizzi();
            notifica.NaturaOpera = LabelRiasNaturaOpera.Text.Trim().ToUpper();
            if (!string.IsNullOrEmpty(TextBoxNumeroAppalto.Text.Trim()))
                notifica.NumeroAppalto = TextBoxNumeroAppalto.Text.Trim().ToUpper();
            if (!string.IsNullOrEmpty(LabelRiasNumeroLavoratori.Text.Trim()))
                notifica.NumeroMassimoLavoratori = Int32.Parse(LabelRiasNumeroLavoratori.Text);
            if (!string.IsNullOrEmpty(LabelRiasNumeroImprese.Text.Trim()))
                notifica.NumeroImprese = Int32.Parse(LabelRiasNumeroImprese.Text);
            if (!string.IsNullOrEmpty(LabelRiasNumeroAutonomi.Text.Trim()))
                notifica.NumeroLavoratoriAutonomi = Int32.Parse(LabelRiasNumeroAutonomi.Text);
            if (!string.IsNullOrEmpty(LabelRiasImporto.Text.Trim()))
                notifica.AmmontareComplessivo = decimal.Parse(LabelRiasImporto.Text);

            // Committente
            notifica.Committente = (Committente) ViewState["committente"];

            // Corodinatori
            Persona coordinatoreProgettazione = null;
            Persona coordinatoreRealizzazione = null;
            Persona responsabileLavori = null;
            // Progettazione
            if (!string.IsNullOrEmpty(LabelRiasProgNominativo.Text.Trim()))
            {
                // Recupero dati responsabile progettazione
                coordinatoreProgettazione = new Persona();
                coordinatoreProgettazione.Nominativo = LabelRiasProgNominativo.Text.Trim().ToUpper();
                if (!string.IsNullOrEmpty(LabelRiasProgIndirizzo.Text.Trim()))
                    coordinatoreProgettazione.Indirizzo = LabelRiasProgIndirizzo.Text.Trim().ToUpper();
                if (!string.IsNullOrEmpty(LabelRiasProgRagioneSociale.Text.Trim()))
                    coordinatoreProgettazione.RagioneSociale = LabelRiasProgRagioneSociale.Text.Trim().ToUpper();
                if (!string.IsNullOrEmpty(LabelRiasProgTelefono.Text.Trim()))
                    coordinatoreProgettazione.Telefono = LabelRiasProgTelefono.Text.Trim().ToUpper();
                if (!string.IsNullOrEmpty(LabelRiasProgFax.Text.Trim()))
                    coordinatoreProgettazione.Fax = LabelRiasProgFax.Text.Trim().ToUpper();
            }
            // Realizzazione
            if (CheckBoxProgettazioneRealizzazione.Checked)
                coordinatoreRealizzazione = coordinatoreProgettazione;
            else
            {
                if (!string.IsNullOrEmpty(LabelRiasRealNominativo.Text.Trim()))
                {
                    // Recupero dati responsabile progettazione
                    coordinatoreRealizzazione = new Persona();
                    coordinatoreRealizzazione.Nominativo = LabelRiasRealNominativo.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRealIndirizzo.Text.Trim()))
                        coordinatoreRealizzazione.Indirizzo = LabelRiasRealIndirizzo.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRealRagioneSociale.Text.Trim()))
                        coordinatoreRealizzazione.RagioneSociale = LabelRiasRealRagioneSociale.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRealTelefono.Text.Trim()))
                        coordinatoreRealizzazione.Telefono = LabelRiasRealTelefono.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRealFax.Text.Trim()))
                        coordinatoreRealizzazione.Fax = LabelRiasRealFax.Text.Trim().ToUpper();
                }
            }
            // Responsabile lavori
            if (CheckBoxProgettazioneResponsabile.Checked)
                responsabileLavori = coordinatoreProgettazione;
            else
            {
                if (CheckBoxResponsabileCommittente.Checked)
                    notifica.ResponsabileCommittente = true;
                else if (!string.IsNullOrEmpty(LabelRiasRespNominativo.Text.Trim()))
                {
                    // Recupero dati responsabile progettazione
                    responsabileLavori = new Persona();
                    responsabileLavori.Nominativo = LabelRiasRespNominativo.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRespIndirizzo.Text.Trim()))
                        responsabileLavori.Indirizzo = LabelRiasRespIndirizzo.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRespRagioneSociale.Text.Trim()))
                        responsabileLavori.RagioneSociale = LabelRiasRespRagioneSociale.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRespTelefono.Text.Trim()))
                        responsabileLavori.Telefono = LabelRiasRespTelefono.Text.Trim().ToUpper();
                    if (!string.IsNullOrEmpty(LabelRiasRespFax.Text.Trim()))
                        responsabileLavori.Fax = LabelRiasRespFax.Text.Trim().ToUpper();
                }
            }
            notifica.CoordinatoreSicurezzaProgettazione = coordinatoreProgettazione;
            notifica.CoordinatoreSicurezzaRealizzazione = coordinatoreRealizzazione;
            notifica.DirettoreLavori = responsabileLavori;

            // Durata
            if (!string.IsNullOrEmpty(LabelRiasDataInizio.Text))
            {
                string dataInizio = LabelRiasDataInizio.Text.Substring(0, 2) + "/" +
                                    LabelRiasDataInizio.Text.Substring(2, 2) + "/" +
                                    LabelRiasDataInizio.Text.Substring(4, 4);
                notifica.DataInizioLavori = DateTime.Parse(dataInizio);
            }
            if (!string.IsNullOrEmpty(LabelRiasDataFine.Text))
            {
                string dataFine = LabelRiasDataFine.Text.Substring(0, 2) + "/" + LabelRiasDataFine.Text.Substring(2, 2) +
                                  "/" + LabelRiasDataFine.Text.Substring(4, 4);
                notifica.DataFineLavori = DateTime.Parse(dataFine);
            }
            if (!string.IsNullOrEmpty(LabelRiasDurata.Text))
                notifica.Durata = Int32.Parse(LabelRiasDurata.Text);
            if (!string.IsNullOrEmpty(LabelRiasGiorniUomo.Text))
                notifica.NumeroGiorniUomo = Int32.Parse(LabelRiasGiorniUomo.Text);

            // Appalti
            if (ViewState["Subappalti"] != null)
                notifica.Subappalti = (SubappaltoCollection) ViewState["Subappalti"];
            // Devo mettere a posto i link alle imprese per quelle inserite nuove
            for (int i = 0; i < notifica.Subappalti.Count; i++)
            {
                if (notifica.Subappalti[i].Appaltata.TipoImpresa == TipologiaImpresa.Cantieri &&
                    !notifica.Subappalti[i].Appaltata.IdImpresa.HasValue &&
                    notifica.Subappalti[i].Appaltata.IdImpresaTemporaneo.HasValue)
                {
                    for (int j = i + 1; j < notifica.Subappalti.Count; j++)
                    {
                        if (notifica.Subappalti[j].Appaltata.TipoImpresa == TipologiaImpresa.Cantieri &&
                            !notifica.Subappalti[j].Appaltata.IdImpresa.HasValue &&
                            notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo.HasValue &&
                            notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo ==
                            notifica.Subappalti[i].Appaltata.IdImpresaTemporaneo)
                        {
                            if (notifica.Subappalti[j].Appaltata != notifica.Subappalti[i].Appaltata)
                                notifica.Subappalti[j].Appaltata = notifica.Subappalti[i].Appaltata;

                            //notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo = null;
                        }

                        if (notifica.Subappalti[j].Appaltante != null &&
                            notifica.Subappalti[j].Appaltante.TipoImpresa == TipologiaImpresa.Cantieri &&
                            !notifica.Subappalti[j].Appaltante.IdImpresa.HasValue &&
                            notifica.Subappalti[j].Appaltante.IdImpresaTemporaneo.HasValue &&
                            notifica.Subappalti[j].Appaltante.IdImpresaTemporaneo ==
                            notifica.Subappalti[i].Appaltata.IdImpresaTemporaneo)
                        {
                            if (notifica.Subappalti[j].Appaltante != notifica.Subappalti[i].Appaltata)
                                notifica.Subappalti[j].Appaltante = notifica.Subappalti[i].Appaltata;

                            //notifica.Subappalti[j].Appaltante.IdImpresaTemporaneo = null;
                        }
                    }
                }

                if (notifica.Subappalti[i].Appaltante != null &&
                    notifica.Subappalti[i].Appaltante.TipoImpresa == TipologiaImpresa.Cantieri &&
                    !notifica.Subappalti[i].Appaltante.IdImpresa.HasValue &&
                    notifica.Subappalti[i].Appaltante.IdImpresaTemporaneo.HasValue)
                {
                    for (int j = i + 1; j < notifica.Subappalti.Count; j++)
                    {
                        if (notifica.Subappalti[j].Appaltata.TipoImpresa == TipologiaImpresa.Cantieri &&
                            !notifica.Subappalti[j].Appaltata.IdImpresa.HasValue &&
                            notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo.HasValue &&
                            notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo ==
                            notifica.Subappalti[i].Appaltante.IdImpresaTemporaneo)
                        {
                            if (notifica.Subappalti[j].Appaltata != notifica.Subappalti[i].Appaltante)
                                notifica.Subappalti[j].Appaltata = notifica.Subappalti[i].Appaltante;

                            //notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo = null;
                        }

                        if (notifica.Subappalti[j].Appaltante != null &&
                            notifica.Subappalti[j].Appaltante.TipoImpresa == TipologiaImpresa.Cantieri &&
                            !notifica.Subappalti[j].Appaltante.IdImpresa.HasValue &&
                            notifica.Subappalti[j].Appaltante.IdImpresaTemporaneo.HasValue &&
                            notifica.Subappalti[j].Appaltante.IdImpresaTemporaneo ==
                            notifica.Subappalti[i].Appaltante.IdImpresaTemporaneo)
                        {
                            if (notifica.Subappalti[j].Appaltante != notifica.Subappalti[i].Appaltante)
                                notifica.Subappalti[j].Appaltante = notifica.Subappalti[i].Appaltante;

                            //notifica.Subappalti[j].Appaltata.IdImpresaTemporaneo = null;
                        }
                    }
                }
            }

            // Notifica padre
            if (ViewState["IdNotificaPadre"] != null)
                notifica.IdNotificaPadre = (int) ViewState["IdNotificaPadre"];

            //chiamiamoa l'inserimento della notifica dello strato BIZ
            if (biz.InserisciNotifica(notifica))
            {
                Response.Redirect(String.Format("~/Cpt/CPTStampaProtocolloNotifica.aspx?idNotifica={0}",
                                                notifica.IdNotifica));
                Response.Flush();
            }
            else
            {
                LabelMessaggio.Text = "Errore durante l'inserimento";
                LabelMessaggio2.Text = LabelMessaggio.Text;
            }
        }
    }

    /// <summary>
    ///   Validazione dei vari step
    /// </summary>
    /// <param name = "CurrentStepIndex"></param>
    /// <returns></returns>
    private bool stepValidate(int CurrentStepIndex)
    {
        switch (CurrentStepIndex)
        {
            case indiceDATIGENERALI: //Dati generali
                LabelControlloIndirizzi.Visible = (CptGestioneIndirizzi1.GetIndirizzi().Count == 0);
                LabelCommittenteSelezionato.Visible = (ViewState["committente"] == null);
                Validate("step1");
                if (!IsValid || LabelControlloIndirizzi.Visible || ViewState["committente"] == null)
                {
                    WizardInserimento.WizardSteps[indiceDATIGENERALI].Title = DATIGENERALI;
                    return false;
                }
                else
                {
                    if (DatiGeneraliCompletati())
                        WizardInserimento.WizardSteps[indiceDATIGENERALI].Title = DATIGENERALI + " V";
                    else
                        WizardInserimento.WizardSteps[indiceDATIGENERALI].Title = DATIGENERALI + " !";
                }
                break;

                //case 1: //Committente
                //    LabelCommittenteSelezionato.Visible = (ViewState["committente"] == null);
                //    if (LabelCommittenteSelezionato.Visible)
                //        return false;
                //    else
                //        WizardInserimento.WizardSteps[1].Title = COMMITTENTE + " V";
                //    break;

            case indiceCOORDINATORI: //Coordinatore direttore lavori
                // Resp progettazione
                LabelProgettazioneNominativo.Visible = false;
                LabelRealizzazioneNominativo.Visible = false;
                LabelResponsabileNominativo.Visible = false;

                // Resp progettazione
                if (string.IsNullOrEmpty(TextBoxProgettazioneNominativo.Text)
                    && (!string.IsNullOrEmpty(TextBoxProgettazioneRagioneSociale.Text)
                        || !string.IsNullOrEmpty(TextBoxProgettazioneIndirizzo.Text)
                        || !string.IsNullOrEmpty(TextBoxProgettazioneTelefono.Text)
                        || !string.IsNullOrEmpty(TextBoxProgettazioneFax.Text)))
                {
                    LabelProgettazioneNominativo.Visible = true;
                    //WizardInserimento.WizardSteps[indiceCOORDINATORI].Title = COORDINATORI + " X";
                    return false;
                }
                else
                    LabelProgettazioneNominativo.Visible = false;

                // Resp realizzazione
                if (!CheckBoxProgettazioneRealizzazione.Checked &&
                    string.IsNullOrEmpty(TextBoxRealizzazioneNominativo.Text)
                    && (!string.IsNullOrEmpty(TextBoxRealizzazioneRagioneSociale.Text)
                        || !string.IsNullOrEmpty(TextBoxRealizzazioneIndirizzo.Text)
                        || !string.IsNullOrEmpty(TextBoxRealizzazioneTelefono.Text)
                        || !string.IsNullOrEmpty(TextBoxRealizzazioneFax.Text)))
                {
                    LabelRealizzazioneNominativo.Visible = true;
                    //WizardInserimento.WizardSteps[indiceCOORDINATORI].Title = COORDINATORI + " X";
                    return false;
                }
                else
                    LabelRealizzazioneNominativo.Visible = false;

                // Responsabile lavori
                if (!CheckBoxProgettazioneResponsabile.Checked &&
                    !CheckBoxResponsabileCommittente.Checked &&
                    string.IsNullOrEmpty(TextBoxResponsabileNominativo.Text)
                    && (!string.IsNullOrEmpty(TextBoxResponsabileRagioneSociale.Text)
                        || !string.IsNullOrEmpty(TextBoxResponsabileIndirizzo.Text)
                        || !string.IsNullOrEmpty(TextBoxResponsabileTelefono.Text)
                        || !string.IsNullOrEmpty(TextBoxResponsabileFax.Text)))
                {
                    LabelResponsabileNominativo.Visible = true;
                    //WizardInserimento.WizardSteps[indiceCOORDINATORI].Title = COORDINATORI + " X";
                    return false;
                }
                else
                {
                    LabelResponsabileNominativo.Visible = false;

                    if (CheckBoxProgettazioneResponsabile.Checked && CheckBoxResponsabileCommittente.Checked)
                    {
                        LabelResponsabileDoppioCheck.Visible = true;
                        return false;
                    }
                    else
                        LabelResponsabileDoppioCheck.Visible = false;
                }


                WizardInserimento.WizardSteps[indiceCOORDINATORI].Title = COORDINATORI + " !";

                if (!string.IsNullOrEmpty(TextBoxProgettazioneNominativo.Text)
                    &&
                    (CheckBoxProgettazioneRealizzazione.Checked ||
                     !string.IsNullOrEmpty(TextBoxRealizzazioneNominativo.Text))
                    &&
                    (CheckBoxProgettazioneResponsabile.Checked ||
                     !string.IsNullOrEmpty(TextBoxResponsabileNominativo.Text)))
                    WizardInserimento.WizardSteps[indiceCOORDINATORI].Title = COORDINATORI + " V";
                break;

            case indiceDURATA: //Durata
                Validate("step3");
                if (!IsValid)
                    return false;
                else
                {
                    if (DatiDurataControllo())
                    {
                        if (DatiDurataCompletati())
                            WizardInserimento.WizardSteps[indiceDURATA].Title = DURATA + " V";
                        else
                            WizardInserimento.WizardSteps[indiceDURATA].Title = DURATA + " !";
                    }
                    else
                        return false;
                }
                break;

            case indiceAPPALTI: //Appalti
                if (ViewState["Appalti"] == null || ((SubappaltoCollection) ViewState["Appalti"]).Count == 0)
                    WizardInserimento.WizardSteps[indiceAPPALTI].Title = APPALTI + " !";
                else
                    WizardInserimento.WizardSteps[indiceAPPALTI].Title = APPALTI + " V";
                break;
        }
        return true;
    }

    /// <summary>
    ///   Metodo per il controllo di coerenza delle inserite
    /// </summary>
    /// <returns></returns>
    private bool DatiDurataControllo()
    {
        if (!string.IsNullOrEmpty(TextBoxDataInizioLavori.Text) &&
            !string.IsNullOrEmpty(TextBoxDataFineLavori.Text))
        {
            string dataInizio = TextBoxDataInizioLavori.Text.Substring(0, 2) + "/" +
                                TextBoxDataInizioLavori.Text.Substring(2, 2) + "/" +
                                TextBoxDataInizioLavori.Text.Substring(4, 4);
            string dataFine = TextBoxDataFineLavori.Text.Substring(0, 2) + "/" +
                              TextBoxDataFineLavori.Text.Substring(2, 2) + "/" +
                              TextBoxDataFineLavori.Text.Substring(4, 4);
            DateTime inizio = DateTime.Parse(dataInizio);
            DateTime fine = DateTime.Parse(dataFine);

            if (inizio > fine)
            {
                LabelErrore.Text = "Data inizio e fine lavori non valide";
                return false;
            }
            else
                LabelErrore.Text = string.Empty;
        }

        if (!string.IsNullOrEmpty(TextBoxDataInizioLavori.Text) &&
            !string.IsNullOrEmpty(TextBoxDataFineLavori.Text) &&
            (!string.IsNullOrEmpty(TextBoxDurataGiorni.Text) || !string.IsNullOrEmpty(TextBoxDurataSettimane.Text) ||
             !string.IsNullOrEmpty(TextBoxDurataMesi.Text) || !string.IsNullOrEmpty(TextBoxDurataAnni.Text)))
        {
            string dataInizio = TextBoxDataInizioLavori.Text.Substring(0, 2) + "/" +
                                TextBoxDataInizioLavori.Text.Substring(2, 2) + "/" +
                                TextBoxDataInizioLavori.Text.Substring(4, 4);
            string dataFine = TextBoxDataFineLavori.Text.Substring(0, 2) + "/" +
                              TextBoxDataFineLavori.Text.Substring(2, 2) + "/" +
                              TextBoxDataFineLavori.Text.Substring(4, 4);
            DateTime inizio = DateTime.Parse(dataInizio);
            DateTime fine = DateTime.Parse(dataFine);
            int durata = CalcolaDurataIntero();

            if ((((fine - inizio)).Days + 1) < durata)
            {
                LabelErrore.Text = "Durata non compatibile con date di inizio e fine";
                return false;
            }
            else
                LabelErrore.Text = string.Empty;
        }

        return true;
    }

    /// <summary>
    ///   Controllo della completezza dei dati temporali relativi alla notifica
    /// </summary>
    /// <returns></returns>
    private bool DatiDurataCompletati()
    {
        if (!string.IsNullOrEmpty(TextBoxDataInizioLavori.Text) &&
            !string.IsNullOrEmpty(TextBoxDataFineLavori.Text) &&
            (!string.IsNullOrEmpty(TextBoxDurataGiorni.Text) || !string.IsNullOrEmpty(TextBoxDurataSettimane.Text) ||
             !string.IsNullOrEmpty(TextBoxDurataMesi.Text) || !string.IsNullOrEmpty(TextBoxDurataAnni.Text)) &&
            !string.IsNullOrEmpty(TextBoxGiorniUomo.Text) &&
            !string.IsNullOrEmpty(TextBoxNumeroLavoratori.Text) &&
            !string.IsNullOrEmpty(TextBoxNumeroImprese.Text) &&
            !string.IsNullOrEmpty(TextBoxNumeroAutonomi.Text) &&
            !string.IsNullOrEmpty(TextBoxImporto.Text))
            return true;
        else
            return false;
    }

    /// <summary>
    ///   Controllo della completezza dei dati generali relativi alla notifica
    /// </summary>
    /// <returns></returns>
    private bool DatiGeneraliCompletati()
    {
        if (!string.IsNullOrEmpty(TextBoxImporto.Text))
            return true;
        else
            return false;
    }

    protected void ButtonVisualizzaAppalto_Click(object sender, EventArgs e)
    {
        PanelAppalti.Visible = true;
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        PanelAppalti.Visible = false;
    }

    protected void GridViewSubappalti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int i = 0;
    }

    /// <summary>
    ///   Visualizziamo nel riassunto i dati raccolti durante il wizard
    /// </summary>
    private void RiempiRiassunto()
    {
        bool? statoDatiGenerali = null;
        bool? statoCommittente = null;
        bool? statoCoordinatori = null;
        bool? statoDurata = null;
        bool? statoSubappalti = null;

        // Dati generali

        LabelRiasArea.Text = DropDownListArea.SelectedItem.Text;
        LabelRiasData.Text = TextBoxData.Text;
        LabelRiasNaturaOpera.Text = TextBoxNaturaOpera.Text.ToUpper();
        LabelRiasNumeroAppalto.Text = TextBoxNumeroAppalto.Text.ToUpper();

        IndirizzoCollection indirizzi = CptGestioneIndirizzi1.GetIndirizzi();

        LabelRiasIndirizzi.Text = string.Empty;
        foreach (Indirizzo indirizzo in indirizzi)
        {
            LabelRiasIndirizzi.Text += indirizzo.IndirizzoCompleto + "<BR>";
        }

        LabelRiasImporto.Text = TextBoxImporto.Text;

        if (!string.IsNullOrEmpty(LabelRiasData.Text)
            && !string.IsNullOrEmpty(LabelRiasNaturaOpera.Text)
            && !string.IsNullOrEmpty(LabelRiasIndirizzi.Text))
        {
            // Tutti i campi riempiti
            ImageStatoDatiGeneraliNeutro.Visible = false;
            ImageStatoDatiGeneraliNegativo.Visible = false;
            ImageStatoDatiGeneraliPositivo.Visible = true;

            statoDatiGenerali = true;
        }
        else
        {
            if (!string.IsNullOrEmpty(LabelRiasData.Text) && !string.IsNullOrEmpty(LabelRiasNaturaOpera.Text))
            {
                // Campi obbligatori riempiti, mancano alcuni dati
                ImageStatoDatiGeneraliNeutro.Visible = true;
                ImageStatoDatiGeneraliNegativo.Visible = false;
                ImageStatoDatiGeneraliPositivo.Visible = false;

                statoDatiGenerali = null;
            }
            else
            {
                // Mancano i campi obbligatori
                ImageStatoDatiGeneraliNeutro.Visible = false;
                ImageStatoDatiGeneraliNegativo.Visible = true;
                ImageStatoDatiGeneraliPositivo.Visible = false;

                statoDatiGenerali = false;
            }
        }

        // Committente
        if (ViewState["committente"] != null)
        {
            Committente committente = (Committente) ViewState["committente"];
            LabelRiasComRagioneSociale.Text = committente.RagioneSociale;
            LabelRiasComIndirizzo.Text = committente.Indirizzo;
            LabelRiasComComune.Text = committente.Comune;
            LabelRiasComProvincia.Text = committente.Provincia;
            LabelRiasComCap.Text = committente.Cap;
            LabelRiasComIva.Text = committente.PartitaIva;
            LabelRiasComFiscale.Text = committente.CodiceFiscale;
            LabelRiasComTelefono.Text = committente.Telefono;
            LabelRiasComFax.Text = committente.Fax;
            LabelRiasComRiferimento.Text = committente.PersonaRiferimento;

            ImageStatoCommittenteNegativo.Visible = false;
            ImageStatoCommittentePositivo.Visible = true;
            ImageStatoCommittenteNeutro.Visible = false;

            statoCommittente = true;
        }
        else
        {
            ImageStatoCommittenteNegativo.Visible = true;
            ImageStatoCommittentePositivo.Visible = false;
            ImageStatoCommittenteNeutro.Visible = false;

            statoCommittente = false;
        }

        // Coordinatori
        //      Progettazione
        LabelRiasProgNominativo.Text = TextBoxProgettazioneNominativo.Text.ToUpper();
        LabelRiasProgRagioneSociale.Text = TextBoxProgettazioneRagioneSociale.Text.ToUpper();
        LabelRiasProgIndirizzo.Text = TextBoxProgettazioneIndirizzo.Text.ToUpper();
        LabelRiasProgTelefono.Text = TextBoxProgettazioneTelefono.Text.ToUpper();
        LabelRiasProgFax.Text = TextBoxProgettazioneFax.Text.ToUpper();
        //      Realizzazione
        if (!CheckBoxProgettazioneRealizzazione.Checked)
        {
            LabelRiasRealNominativo.Text = TextBoxRealizzazioneNominativo.Text.ToUpper();
            LabelRiasRealRagioneSociale.Text = TextBoxRealizzazioneRagioneSociale.Text.ToUpper();
            LabelRiasRealIndirizzo.Text = TextBoxRealizzazioneIndirizzo.Text.ToUpper();
            LabelRiasRealTelefono.Text = TextBoxRealizzazioneTelefono.Text.ToUpper();
            LabelRiasRealFax.Text = TextBoxRealizzazioneFax.Text.ToUpper();
        }
        else
        {
            LabelRiasRealNominativo.Text = TextBoxProgettazioneNominativo.Text.ToUpper();
            LabelRiasRealRagioneSociale.Text = TextBoxProgettazioneRagioneSociale.Text.ToUpper();
            LabelRiasRealIndirizzo.Text = TextBoxProgettazioneIndirizzo.Text.ToUpper();
            LabelRiasRealTelefono.Text = TextBoxProgettazioneTelefono.Text.ToUpper();
            LabelRiasRealFax.Text = TextBoxProgettazioneFax.Text.ToUpper();
        }
        //      Responsabile
        if (!CheckBoxProgettazioneResponsabile.Checked)
        {
            if (!CheckBoxResponsabileCommittente.Checked)
            {
                LabelRiasRespNominativo.Text = TextBoxResponsabileNominativo.Text.ToUpper();
                LabelRiasRespRagioneSociale.Text = TextBoxResponsabileRagioneSociale.Text.ToUpper();
                LabelRiasRespIndirizzo.Text = TextBoxResponsabileIndirizzo.Text.ToUpper();
                LabelRiasRespTelefono.Text = TextBoxResponsabileTelefono.Text.ToUpper();
                LabelRiasRespFax.Text = TextBoxResponsabileFax.Text.ToUpper();
            }
            else
            {
                // Il responsabile � il committente
                CheckBoxRiasResponsabileCommittente.Checked = true;
                LabelRiasRespNominativo.Text = LabelRiasComRagioneSociale.Text;
            }
        }
        else
        {
            LabelRiasRespNominativo.Text = TextBoxProgettazioneNominativo.Text.ToUpper();
            LabelRiasRespRagioneSociale.Text = TextBoxProgettazioneRagioneSociale.Text.ToUpper();
            LabelRiasRespIndirizzo.Text = TextBoxProgettazioneIndirizzo.Text.ToUpper();
            LabelRiasRespTelefono.Text = TextBoxProgettazioneTelefono.Text.ToUpper();
            LabelRiasRespFax.Text = TextBoxProgettazioneFax.Text.ToUpper();
        }

        if (!string.IsNullOrEmpty(LabelRiasProgNominativo.Text)
            && !string.IsNullOrEmpty(LabelRiasRealNominativo.Text)
            && !string.IsNullOrEmpty(LabelRiasRespNominativo.Text))
        {
            ImageStatoCoordinatoriPositivo.Visible = true;
            ImageStatoCoordinatoriNegativo.Visible = false;
            ImageStatoCoordinatoriNeutro.Visible = false;

            statoCoordinatori = true;
        }
        else
        {
            ImageStatoCoordinatoriPositivo.Visible = false;
            ImageStatoCoordinatoriNegativo.Visible = false;
            ImageStatoCoordinatoriNeutro.Visible = true;

            statoCoordinatori = null;
        }

        // Durata
        LabelRiasDataInizio.Text = TextBoxDataInizioLavori.Text;
        LabelRiasDataFine.Text = TextBoxDataFineLavori.Text;
        LabelRiasDurata.Text = CalcolaDurata();
        LabelRiasGiorniUomo.Text = TextBoxGiorniUomo.Text;
        LabelRiasNumeroLavoratori.Text = TextBoxNumeroLavoratori.Text;
        LabelRiasNumeroImprese.Text = TextBoxNumeroImprese.Text;
        LabelRiasNumeroAutonomi.Text = TextBoxNumeroAutonomi.Text;

        if (!string.IsNullOrEmpty(LabelRiasDataInizio.Text)
            && !string.IsNullOrEmpty(LabelRiasDataFine.Text)
            && !string.IsNullOrEmpty(LabelRiasDurata.Text)
            && !string.IsNullOrEmpty(LabelRiasGiorniUomo.Text)
            && !string.IsNullOrEmpty(LabelRiasNumeroLavoratori.Text)
            && !string.IsNullOrEmpty(LabelRiasNumeroImprese.Text)
            && !string.IsNullOrEmpty(LabelRiasNumeroAutonomi.Text))
        {
            ImageStatoDurataNegativo.Visible = false;
            ImageStatoDurataPositivo.Visible = true;
            ImageStatoDurataNeutro.Visible = false;

            statoDurata = true;
        }
        else
        {
            ImageStatoDurataNegativo.Visible = false;
            ImageStatoDurataPositivo.Visible = false;
            ImageStatoDurataNeutro.Visible = true;

            statoDurata = null;
        }

        // Subappalti
        if (ViewState["Subappalti"] != null)
        {
            SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["Subappalti"];
            RepeaterSubappalti.DataSource = subappalti;
            RepeaterSubappalti.DataBind();

            if (subappalti.Count == 0)
            {
                ImageStatoAppaltiNegativo.Visible = false;
                ImageStatoAppaltiPositivo.Visible = false;
                ImageStatoAppaltiNeutro.Visible = true;

                statoSubappalti = null;
            }
            else
            {
                ImageStatoAppaltiNegativo.Visible = false;
                ImageStatoAppaltiPositivo.Visible = true;
                ImageStatoAppaltiNeutro.Visible = false;

                statoSubappalti = true;
            }
        }
        else
        {
            ImageStatoAppaltiNegativo.Visible = false;
            ImageStatoAppaltiPositivo.Visible = false;
            ImageStatoAppaltiNeutro.Visible = true;

            statoSubappalti = null;
        }

        // Stabilisco cosa � possibile fare analizzando i vari stati
        bool inserimento = true;
        bool forzatura = false;

        #region Analisi Stati

        if (!statoDatiGenerali.HasValue || statoDatiGenerali.Value)
        {
            if (!statoDatiGenerali.HasValue)
            {
                forzatura = true;
            }

            // Dati generali ok
            if (statoCommittente.Value)
            {
                if (!statoCoordinatori.HasValue || statoCoordinatori.Value)
                {
                    if (!statoCoordinatori.HasValue)
                    {
                        forzatura = true;
                    }

                    if (!statoDurata.HasValue || statoDurata.Value)
                    {
                        if (!statoDurata.HasValue)
                        {
                            forzatura = true;
                        }

                        if (!statoSubappalti.HasValue || statoSubappalti.Value)
                        {
                            if (!statoSubappalti.HasValue)
                                forzatura = true;
                        }
                        else
                        {
                            inserimento = false;
                        }
                    }
                    else
                    {
                        inserimento = false;
                    }
                }
                else
                {
                    inserimento = false;
                }
            }
            else
            {
                inserimento = false;
            }
        }
        else
        {
            inserimento = false;
        }

        #endregion

        Button bConferma =
            (Button)
            WizardInserimento.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
        if (!inserimento)
        {
            bConferma.Text = "Controllare i dati";
            LabelMessaggio.Text = "Attenzione, mancano alcuni dati obbligatori, non � possibile proseguire.";
            LabelMessaggio2.Text = LabelMessaggio.Text;
            ViewState["Bloccato"] = true;
        }
        else
        {
            ViewState["Bloccato"] = false;

            if (forzatura)
            {
                if (!string.IsNullOrEmpty(TextBoxImporto.Text))
                {
                    decimal importo = decimal.Parse(TextBoxImporto.Text);
                    if (importo <= biz.GetLimiteImporto())
                    {
                        bConferma.Text = "Inserisci notifica";
                        LabelMessaggio.Text =
                            "La notifica ha un importo esiguo e si pu� proseguire senza immettere tutti i dati scegliendo il bottone \"Inserisci notifica\"";
                        LabelMessaggio2.Text = LabelMessaggio.Text;
                    }
                    else
                    {
                        bConferma.Text = "Forzare inserimento";
                        LabelMessaggio.Text =
                            "Attenzione, alcuni dati non sono stati compilati. Se la notifica � stata inserita completamente scegliere il bottone \"Forzare inserimento\"";
                        LabelMessaggio2.Text = LabelMessaggio.Text;
                    }
                }
                else
                {
                    bConferma.Text = "Forzare inserimento";
                    LabelMessaggio.Text =
                        "Attenzione, alcuni dati non sono stati compilati. Se la notifica � stata inserita completamente scegliere il bottone \"Forzare inserimento\"";
                    LabelMessaggio2.Text = LabelMessaggio.Text;
                }
            }
            else
                bConferma.Text = "Inserisci notifica";
        }
    }

    private string CalcolaDurata()
    {
        int durata = 0;

        if (!string.IsNullOrEmpty(TextBoxDurataGiorni.Text))
            durata += Int32.Parse(TextBoxDurataGiorni.Text);

        if (!string.IsNullOrEmpty(TextBoxDurataSettimane.Text))
            durata += 7*Int32.Parse(TextBoxDurataSettimane.Text);

        if (!string.IsNullOrEmpty(TextBoxDurataMesi.Text))
            durata += 30*Int32.Parse(TextBoxDurataMesi.Text);

        if (!string.IsNullOrEmpty(TextBoxDurataAnni.Text))
            durata += 365*Int32.Parse(TextBoxDurataAnni.Text);

        if (durata == 0)
            return string.Empty;
        else
            return durata.ToString();
    }

    private int CalcolaDurataIntero()
    {
        int durata = 0;

        if (!string.IsNullOrEmpty(TextBoxDurataGiorni.Text))
            durata += Int32.Parse(TextBoxDurataGiorni.Text);

        if (!string.IsNullOrEmpty(TextBoxDurataSettimane.Text))
            durata += 7*Int32.Parse(TextBoxDurataSettimane.Text);

        if (!string.IsNullOrEmpty(TextBoxDurataMesi.Text))
            durata += 30*Int32.Parse(TextBoxDurataMesi.Text);

        if (!string.IsNullOrEmpty(TextBoxDurataAnni.Text))
            durata += 365*Int32.Parse(TextBoxDurataAnni.Text);

        return durata;
    }

    protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton bElimina = (ImageButton) e.Row.FindControl("ImageButtonDelete");
            RadScriptManager scriptMan = (RadScriptManager) Page.Master.FindControl("RadScriptManagerMain");
            scriptMan.RegisterPostBackControl(bElimina);
        }
    }

    protected void ImageButtonDelete_Click(object sender, ImageClickEventArgs e)
    {
        int rowIndex = ((GridViewRow) ((ImageButton) sender).Parent.Parent).RowIndex;

        SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["Subappalti"];
        subappalti.RemoveAt(rowIndex);
        CaricaSubappalti();
    }

    protected void ButtonImpresaGiaSelezionata_Click(object sender, EventArgs e)
    {
        ImpresaCollection imprese = (ImpresaCollection) ViewState["Imprese"];

        if (imprese.Count > 0)
        {
            ImpresaAppaltataDaSelezionata(imprese[DropDownListImpreseGiaSelezionateAta.SelectedIndex]);
        }
    }

    protected void ButtonImpresaGiaSelezionataSel_Click(object sender, EventArgs e)
    {
        ImpresaCollection imprese = (ImpresaCollection) ViewState["Imprese"];

        if (imprese.Count > 0)
        {
            ImpresaSubappaltatriceSelezionata(imprese[DropDownListImpreseGiaSelezionateSel.SelectedIndex]);
        }
    }

    protected void ButtonModificaCommittente_Click(object sender, EventArgs e)
    {
        if (ViewState["committente"] != null)
        {
            Committente committente = (Committente) ViewState["committente"];
            CantieriCommittente1.ImpostaCommittente(committente);

            //committente.Modificato = true;
            //ViewState["committente"] = committente;

            LabelInserimentoCommittenteRes.Text = string.Empty;
            CantieriRicercaCommittente1.Visible = false;
            PanelInserimentoCommittente.Visible = true;
            LabelTitoloInserimentoCommittente.Text = "Modifica committente";
            ButtonInserisciCommittente.Text = "Modifica committente";
        }
    }

    protected void ButtonModificaImpresa_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(DropDownListModificaImprese.SelectedValue))
        {
            ImpresaCollection imprese = (ImpresaCollection) ViewState["Imprese"];
            if (imprese.Count > 0)
            {
                Impresa impresa = imprese[DropDownListModificaImprese.SelectedIndex];

                if (impresa.TipoImpresa == TipologiaImpresa.Cantieri)
                {
                    PanelAppalti.Visible = false;
                    PanelModificaImpresa.Visible = true;
                    CptImpresaModificaImpresa.ImpostaImpresa(impresa);
                    LabelErroreModifica.Visible = false;
                }
                else
                    LabelErroreModifica.Visible = true;
            }
        }
    }

    protected void ButtonAnnullaModifica_Click(object sender, EventArgs e)
    {
        PanelAppalti.Visible = true;
        CptImpresaModificaImpresa.Reset();
        PanelModificaImpresa.Visible = false;
    }

    protected void ButtonSalvaModifiche_Click(object sender, EventArgs e)
    {
        Impresa impresa = CptImpresaModificaImpresa.impresa;
        //bool modifica = false;

        SubappaltoCollection subappalti = null;
        if (ViewState["Subappalti"] != null)
            subappalti = (SubappaltoCollection) ViewState["Subappalti"];

        // Modifico i subappalti
        if (subappalti != null)
        {
            foreach (Subappalto subappalto in subappalti)
            {
                Impresa subappaltata = subappalto.Appaltata;
                if ((subappaltata.IdImpresa.HasValue && impresa.IdImpresa.HasValue &&
                     subappaltata.IdImpresaComposto == impresa.IdImpresaComposto)
                    ||
                    (subappaltata.IdImpresaTemporaneo.HasValue && impresa.IdImpresaTemporaneo.HasValue &&
                     subappaltata.IdImpresaTemporaneo == impresa.IdImpresaTemporaneo))
                {
                    subappalto.Appaltata = impresa;
                    //modifica = true;
                }

                Impresa subappaltante = subappalto.Appaltante;
                if (subappaltante != null)
                {
                    if ((subappaltante.IdImpresa.HasValue && impresa.IdImpresa.HasValue &&
                         subappaltante.IdImpresaComposto == impresa.IdImpresaComposto)
                        ||
                        (subappaltante.IdImpresaTemporaneo.HasValue && impresa.IdImpresaTemporaneo.HasValue &&
                         subappaltante.IdImpresaTemporaneo == impresa.IdImpresaTemporaneo))
                    {
                        subappalto.Appaltante = impresa;
                        //modifica = true;
                    }
                }

                //if (modifica)
                //break;
            }
        }

        // Modifico le imprese gi� selezionate
        if (ViewState["Imprese"] != null)
        {
            ImpresaCollection impreseSel = (ImpresaCollection) ViewState["Imprese"];
            for (int i = 0; i < impreseSel.Count; i++)
            {
                if ((impreseSel[i].IdImpresa.HasValue && impresa.IdImpresa.HasValue &&
                     impreseSel[i].IdImpresaComposto == impresa.IdImpresaComposto)
                    ||
                    (impreseSel[i].IdImpresaTemporaneo.HasValue && impresa.IdImpresaTemporaneo.HasValue &&
                     impreseSel[i].IdImpresaTemporaneo == impresa.IdImpresaTemporaneo))
                    impreseSel[i] = impresa;
            }
        }

        ViewState["Subappalti"] = subappalti;
        PanelAppalti.Visible = true;
        CptImpresaModificaImpresa.Reset();
        PanelModificaImpresa.Visible = false;
    }

    #region Eventi di sblocco form nel caso di Aggiornamento notifica

    //La gestione dello sblocco � stat voluta dal cliente per rendere pi� consapevole il personale che utilizzer� l'applicativo che
    //che stanno andando a modificare dati provenienti da una notifica precedentemente inserita
    protected void ButtonDatiGeneraliSblocca_Click(object sender, EventArgs e)
    {
        PanelDatiGenerali.Enabled = true;
        ButtonDatiGeneraliSblocca.Enabled = false;
    }

    protected void ButtonCommittenteSblocca_Click(object sender, EventArgs e)
    {
        //PanelCommittente.Enabled = true;
        //ButtonCommittenteSblocca.Enabled = false;
    }

    protected void ButtonCoordinatoriSblocca_Click(object sender, EventArgs e)
    {
        PanelCoordinatori.Enabled = true;
        ButtonCoordinatoriSblocca.Enabled = false;
    }

    protected void ButtonDurataSblocca_Click(object sender, EventArgs e)
    {
        PanelDurata.Enabled = true;
        ButtonDurataSblocca.Enabled = false;
    }

    protected void ButtonAppaltiSblocca_Click(object sender, EventArgs e)
    {
        PanelAppaltiEsterno.Enabled = true;
        ButtonAppaltiSblocca.Enabled = false;
    }

    #endregion

    #region eventi relativi alla gestione degli appalti

    protected void ButtonSelezionaAppaltatrice_Click(object sender, EventArgs e)
    {
        CantieriRicercaImpresaAppaltataDa.Visible = true;
        PanelInserisciImpresaAppaltataDa.Visible = false;
    }

    protected void ButtonSelezionaAppaltata_Click(object sender, EventArgs e)
    {
        CantieriRicercaImpresaSubappaltatrice.Visible = true;
        PanelInserisciImpresaSubappaltata.Visible = false;
    }

    protected void ButtonAnnullaNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        CantieriRicercaImpresaAppaltataDa.Visible = true;
        PanelInserisciImpresaAppaltataDa.Visible = false;
    }

    protected void ButtonAnnullaNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        CantieriRicercaImpresaSubappaltatrice.Visible = true;
        PanelInserisciImpresaSubappaltata.Visible = false;
    }

    protected void ButtonInserisciNuovaImpresaSubappaltatrice_Click(object sender, EventArgs e)
    {
        Impresa impresa = CantieriImpresaSubappaltatrice.impresa;

        if (impresa != null)
        {
            bool[] res = biz.EsisteIvaFiscImpresa(impresa.PartitaIva, impresa.CodiceFiscale);
            ControllaEsistenzaImpresa(LabelImpresaSelezionataEsistente, res);

            ImpresaSubappaltatriceSelezionata(impresa);
            CantieriImpresaSubappaltatrice.Reset();
        }
        else
            LabelNuovaSubappaltatriceRes.Text = CantieriImpresaSubappaltatrice.Errore;
        //= "Non tutti i campi sono corretti";
    }

    private void ImpresaSubappaltatriceSelezionata(Impresa impresa)
    {
        TextBoxImpresaAppaltata.Text =
            string.Format("{0}\n{1}\n{2} {3}", impresa.RagioneSociale, impresa.Indirizzo, impresa.Comune,
                          impresa.Provincia);
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaAppaltataSiceInfo.Checked = true;
        else
            RadioButtonImpresaAppaltataCantieri.Checked = true;
        ViewState["impresaAppaltata"] = impresa;

        CantieriRicercaImpresaSubappaltatrice.Reset();
        CantieriRicercaImpresaSubappaltatrice.Visible = true;
        PanelInserisciImpresaSubappaltata.Visible = false;
    }

    protected void ButtonInserisciNuovaImpresaAppaltataDa_Click(object sender, EventArgs e)
    {
        Impresa impresa = CantieriImpresaAppaltataDa.impresa;

        if (impresa != null)
        {
            bool[] res = biz.EsisteIvaFiscImpresa(impresa.PartitaIva, impresa.CodiceFiscale);
            ControllaEsistenzaImpresa(LabelImpresaSelezionataEsistente, res);

            ImpresaAppaltataDaSelezionata(impresa);
            CantieriImpresaAppaltataDa.Reset();
        }
        else
            LabelNuovaAppaltataDaRes.Text = CantieriImpresaAppaltataDa.Errore; //"Non tutti i campi sono corretti";
    }

    private void ControllaEsistenzaImpresa(Label label, bool[] res)
    {
        StringBuilder mess = new StringBuilder();

        if (res[0])
            mess.Append("Nell'anagrafica SiceNew esiste gi� un'impresa con la partita IVA inserita\n");
        if (res[1])
            mess.Append("Nell'anagrafica Notifiche esiste gi� un'impresa con la partita IVA inserita\n");
        if (res[2])
            mess.Append("Nell'anagrafica SiceNew esiste gi� un'impresa con il codice fiscale inserito\n");
        if (res[3])
            mess.Append("Nell'anagrafica Notifiche esiste gi� un'impresa con il codice fiscale inserito\n");

        label.Text = mess.ToString();
    }

    private void ControllaEsistenzaCommittente(Label label, bool[] res)
    {
        StringBuilder mess = new StringBuilder();

        if (res[0])
            mess.Append("Nell'anagrafica esiste gi� un committente con la partita IVA inserita\n");
        if (res[1])
            mess.Append("Nell'anagrafica esiste gi� un committente con il codice fiscale inserito\n");

        label.Text = mess.ToString();
    }

    private void ImpresaAppaltataDaSelezionata(Impresa impresa)
    {
        TextBoxImpresaAppaltatrice.Text =
            string.Format("{0}\n{1}\n{2} {3}", impresa.RagioneSociale, impresa.Indirizzo, impresa.Comune,
                          impresa.Provincia);
        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            RadioButtonImpresaAppaltatriceSiceInfo.Checked = true;
        else
            RadioButtonImpresaAppaltatriceCantieri.Checked = true;
        ViewState["impresaAppaltatrice"] = impresa;

        CantieriRicercaImpresaAppaltataDa.Reset();
        CantieriRicercaImpresaAppaltataDa.Visible = true;
        PanelInserisciImpresaAppaltataDa.Visible = false;
    }

    protected void ButtonAggiungiAppalto_Click(object sender, EventArgs e)
    {
        Impresa impresaAppaltatrice = null;
        Impresa impresaAppaltata = null;

        if (ViewState["impresaAppaltata"] != null)
        {
            impresaAppaltata = (Impresa) ViewState["impresaAppaltata"];

            if (ViewState["impresaAppaltatrice"] != null)
                impresaAppaltatrice = (Impresa) ViewState["impresaAppaltatrice"];

            ImpresaCollection imprese = (ImpresaCollection) ViewState["Imprese"];
            if (!imprese.Contains(impresaAppaltata))
            {
                if (impresaAppaltata.TipoImpresa == TipologiaImpresa.Cantieri && !impresaAppaltata.IdImpresa.HasValue)
                    impresaAppaltata.IdImpresaTemporaneo = imprese.Count;

                imprese.Add(impresaAppaltata);
            }
            if (impresaAppaltatrice != null && !imprese.Contains(impresaAppaltatrice))
            {
                if (impresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri &&
                    !impresaAppaltatrice.IdImpresa.HasValue)
                    impresaAppaltatrice.IdImpresaTemporaneo = imprese.Count;

                imprese.Add(impresaAppaltatrice);
            }
            CaricaImpreseGiaSelezionate();

            SubappaltoCollection subappalti = (SubappaltoCollection) ViewState["Subappalti"];
            subappalti.Add(new Subappalto(null, 0, impresaAppaltatrice, impresaAppaltata));

            ViewState["impresaAppaltatrice"] = null;
            TextBoxImpresaAppaltatrice.Text = string.Empty;
            ViewState["impresaAppaltata"] = null;
            TextBoxImpresaAppaltata.Text = string.Empty;
            ViewState["Subappalti"] = subappalti;
            //this.PanelAppalti.Visible = false;

            CaricaSubappalti();
            LabelRisultatoAppalto.Text = string.Empty;
        }
        else
            LabelRisultatoAppalto.Text = "Inserire impresa selezionata";
    }

    private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;
        ImpresaAppaltataDaSelezionata(impresa);
    }

    private void CantieriRicercaImpresa2_OnImpresaSelected(Impresa impresa)
    {
        LabelImpresaSelezionataEsistente.Text = string.Empty;
        ImpresaSubappaltatriceSelezionata(impresa);
    }

    private void CantieriRicercaImpresaSubappaltatrice_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        CantieriRicercaImpresaSubappaltatrice.Visible = false;
        PanelInserisciImpresaSubappaltata.Visible = true;
        LabelImpresaSelezionataEsistente.Text = string.Empty;

        // Riporto i parametri della ricerca nei campi
        CantieriImpresaSubappaltatrice.ImpostaRagioneSociale(CantieriRicercaImpresaSubappaltatrice.RagioneSociale);
    }

    private void CantieriRicercaImpresaAppaltataDa_OnNuovaImpresaSelected(object sender, EventArgs e)
    {
        CantieriRicercaImpresaAppaltataDa.Visible = false;
        PanelInserisciImpresaAppaltataDa.Visible = true;
        LabelImpresaAppaltataDaEsistente.Text = string.Empty;

        // Riporto i parametri della ricerca nei campi
        CantieriImpresaAppaltataDa.ImpostaRagioneSociale(CantieriRicercaImpresaAppaltataDa.RagioneSociale);
    }

    #endregion

    #region gestione eventi wizard

    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        //this.Validate();
        if (!stepValidate(e.CurrentStepIndex))
            e.Cancel = true;
        else
        {
            if (e.CurrentStepIndex == 0)
            {
                if (!string.IsNullOrEmpty(TextBoxImporto.Text))
                {
                    decimal importo = decimal.Parse(TextBoxImporto.Text);
                    if (importo <= biz.GetLimiteImporto())
                    {
                        RiempiRiassunto();
                        WizardInserimento.MoveTo(WizardInserimento.WizardSteps[indiceRiassunto]);
                    }
                }
            }
        }

        if (e.NextStepIndex == 4)
            RiempiRiassunto();
    }

    protected void Wizard1_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
    {
        //this.Validate();
        if (!stepValidate(e.CurrentStepIndex))
            e.Cancel = true;

        if (e.NextStepIndex == 4)
            RiempiRiassunto();
    }

    #endregion

    #region Eventi relativi alla gestione del committente

    protected void ButtonSelezionaCommittente_Click(object sender, EventArgs e)
    {
        PanelInserimentoCommittente.Visible = false;
        CantieriRicercaCommittente1.Visible = true;
    }

    protected void ButtonAnnullaInserimentoCommittente_Click(object sender, EventArgs e)
    {
        PanelInserimentoCommittente.Visible = false;
        CantieriRicercaCommittente1.Visible = true;
        LabelInserimentoCommittenteRes.Text = string.Empty;
    }

    protected void ButtonInserisciCommittente_Click(object sender, EventArgs e)
    {
        Committente committente = CantieriCommittente1.Committente;

        if (committente != null)
        {
            bool[] res = biz.EsisteIvaFiscCommittente(committente.PartitaIva, committente.CodiceFiscale);
            ControllaEsistenzaCommittente(LabelCommittenteEsistente, res);

            CommittenteSelezionato(committente);
            CantieriCommittente1.Reset();
        }
        else
            LabelInserimentoCommittenteRes.Text = CantieriCommittente1.Errore;
    }

    private void CantieriRicercaCommittente1_OnNuovoCommittenteSelected(object sender, EventArgs e)
    {
        LabelInserimentoCommittenteRes.Text = string.Empty;
        CantieriRicercaCommittente1.Visible = false;
        PanelInserimentoCommittente.Visible = true;
        LabelTitoloInserimentoCommittente.Text = "Inserisci committente";
        ButtonInserisciCommittente.Text = "Inserisci committente";
        LabelCommittenteEsistente.Text = string.Empty;

        // Riporto i parametri della ricerca nei campi
        CantieriCommittente1.ImpostaRagioneSociale(CantieriRicercaCommittente1.RagioneSociale);
    }

    private void CantieriRicercaCommittente1_OnCommittenteSelected(Committente committente)
    {
        LabelCommittenteEsistente.Text = string.Empty;

        CommittenteSelezionato(committente);
    }

    private void CommittenteSelezionato(Committente committente)
    {
        TextBoxCommittente.Text = committente.NomeCompleto;
        ViewState["committente"] = committente;

        CantieriRicercaCommittente1.Reset();
        CantieriRicercaCommittente1.Visible = true;
        PanelInserimentoCommittente.Visible = false;
    }

    #endregion

    #region Eventi per la gestione delle checkbox relative al coordinatore dei lavori

    /// <summary>
    ///   Evento checkbox
    /// </summary>
    /// <param name = "sender"></param>
    /// <param name = "e"></param>
    protected void CheckBoxProgettazioneRealizzazione_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxProgettazioneRealizzazione.Checked)
        {
            PanelRealizzazione.Enabled = false;
        }
        else
        {
            PanelRealizzazione.Enabled = true;
        }
    }

    /// <summary>
    ///   Evento checkbox
    /// </summary>
    /// <param name = "sender"></param>
    /// <param name = "e"></param>
    protected void CheckBoxProgettazioneResponsabile_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxProgettazioneResponsabile.Checked)
        {
            PanelResponsabile.Enabled = false;
        }
        else
        {
            PanelResponsabile.Enabled = true;
        }
    }

    protected void CheckBoxResponsabileCommittente_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxResponsabileCommittente.Checked)
        {
            PanelResponsabile.Enabled = false;
        }
        else
        {
            PanelResponsabile.Enabled = true;
        }
    }

    #endregion
}