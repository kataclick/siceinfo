<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ModificaVisita.aspx.cs" Inherits="Cpt_ModificaVisita" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/CptAllegatiAttivita.ascx" TagName="CptAllegatiAttivita"
    TagPrefix="uc4" %>

<%@ Register Src="../WebControls/CptDatiAttivita.ascx" TagName="CptDatiAttivita"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Modifica visita"
        titolo="Notifiche preliminari" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:CptDatiAttivita id="CptDatiAttivita1" runat="server">
                </uc3:CptDatiAttivita>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonModifica" runat="server" OnClick="ButtonModifica_Click" Text="Modifica" ValidationGroup="visita" Width="150px" />&nbsp;
                <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelAllegati" runat="server" Visible="false">
                    <uc4:CptAllegatiAttivita ID="CptAllegatiAttivita1" runat="server" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>

