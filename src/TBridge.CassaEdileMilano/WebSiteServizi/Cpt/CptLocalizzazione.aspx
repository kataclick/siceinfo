<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CptLocalizzazione.aspx.cs" Inherits="Cpt_CptLocalizzazione" %>

<%@ Register Src="../WebControls/MenuCpt.ascx" TagName="MenuCpt" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCpt ID="MenuCpt1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.661048;
                var longitudine = 9.685537;
                var zoom = 8;

                function LoadMap() {
                    var latitudine = 45.661048;
                    var longitudine = 9.685537;
                    var zoom = 8;
                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }
            </script>
        </telerik:RadCodeBlock>
        <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Localizzazione"
            titolo="Notifiche preliminari" />
        <br />
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Committente"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Impresa"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Indirizzo"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Comune"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="TextBoxCommittente" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxImpresa" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxComune" runat="server" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Natura opera"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Ammontare (>)"></asp:Label>
                    <asp:CompareValidator ID="CompareValidatorImporto" runat="server" ValidationGroup="localizzazione"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage="*" ControlToValidate="TextBoxAmmontare"></asp:CompareValidator>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Data inizio lavori (>)"></asp:Label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizio" runat="server"
                        ValidationGroup="localizzazione" ControlToValidate="TextBoxDataInizio" ErrorMessage="*"
                        ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Data fine lavori (<)"></asp:Label>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFine" runat="server"
                        ValidationGroup="localizzazione" ControlToValidate="TextBoxDataInizio" ErrorMessage="*"
                        ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="TextBoxNatura" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxAmmontare" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataInizio" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataFine" runat="server" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Area
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorArea" runat="server" ControlToValidate="DropDownListArea"
                        ValidationGroup="localizzazione" ErrorMessage="*">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="DropDownListArea" runat="server" AppendDataBoundItems="true"
                        Width="100%">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <asp:Button ID="ButtonRicerca" runat="server" OnClick="ButtonRicerca_Click" Text="Ricerca"
            ValidationGroup="localizzazione" Width="150px" />
        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="I cantieri da visualizzare sono troppi, filtrare maggiormemte"
            Visible="False"></asp:Label><br />
        <br />
        <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Cantieri trovati" Visible="False"></asp:Label><br />
        <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
        </div>
        <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
            <script type="text/javascript">
                LoadMap();
            </script>
        </telerik:RadCodeBlock>
        <br />
    </telerik:RadAjaxPanel>
</asp:Content>
