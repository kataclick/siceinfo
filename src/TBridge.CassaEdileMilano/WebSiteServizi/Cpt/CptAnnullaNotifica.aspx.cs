using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class Cpt_CptAnnullaNotifica : Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CptAnnullamentoNotifica,
                                              "CptAnnullaNotifica.aspx");
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int idNotifica = Int32.Parse(TextBoxIdNotifica.Text.Trim());
            Notifica notifica = biz.GetNotifica(idNotifica);
            List<int> notificheCorrelate = biz.GetIdNotificheCorrelate(idNotifica);

            if (notifica != null)
            {
                if (!notifica.Annullata)
                {
                    if (notificheCorrelate.Count == 0)
                    {
                        PanelDettagli.Visible = true;
                        CptDettagliNotifica1.CaricaNotifica(notifica);
                        ViewState["IdNotifica"] = notifica.IdNotifica.Value;
                        LabelErrore.Text = string.Empty;
                    }
                    else
                    {
                        StringBuilder mesErrore = new StringBuilder();
                        mesErrore.Append(
                            "La notifica non pu� essere eliminata. Esistono degli aggiornamenti della notifica che vanno precedentemente eliminati. I protocolli sono:");
                        foreach (int prot in notificheCorrelate)
                        {
                            mesErrore.Append(String.Format("  {0}", prot));
                        }

                        LabelErrore.Text = mesErrore.ToString();
                    }
                }
                else
                {
                    LabelErrore.Text = "Notifica gi� annullata";
                }
            }
            else
            {
                LabelErrore.Text = "Notifica non trovata negli archivi";
            }
        }
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        if (ViewState["IdNotifica"] != null)
        {
            int idNotifica = (int) ViewState["IdNotifica"];

            Utente utente = (Utente) Membership.GetUser();
            if (biz.AnnullaNotifica(idNotifica, utente.UserName))
            {
                PanelDettagli.Visible = false;
                TextBoxIdNotifica.Text = string.Empty;
                LabelErrore.Text =
                    String.Format("La notifica con protocollo {0} � stata annullata", idNotifica);
            }
            else
            {
                LabelErrore.Text = "Errore durante l'annullamento";
            }
        }
    }
}