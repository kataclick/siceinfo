using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
//using ASL = TBridge.Cemi.GestioneUtenti.Business.Identities.ASL;

public partial class Cpt_CprRicercaCantiere : Page
{
    private const int INDICEIMPRESA = 1;
    private readonly CptBusiness biz = new CptBusiness();
    private String committenteRagioneSociale;
    private int? idASL;
    private string impresa = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CptRicercaCantiere.aspx");

        #endregion

        if (GestioneUtentiBiz.IsASL())
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.ASL asl =
            //    ((ASL)HttpContext.Current.User.Identity).Entity;
            ASL asl =
                (ASL) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            idASL = asl.IdASL;
        }

        if (!Page.IsPostBack)
        {
            CaricaAree();

            SelezionaArea();

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CptEstrazioneExcelCantieri.ToString()))
            {
                ButtonEstrai.Visible = true;
            }
            else
            {
                ButtonEstrai.Visible = false;
            }
        }
    }

    private void CaricaAree()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListArea,
            biz.GetAree(),
            "Descrizione",
            "IdArea");
    }

    private void SelezionaArea()
    {
        // Se l'utente � autorizzato all'inserimento di notifiche per una sola area
        // blocco la selezione
        if (GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicerca.ToString())
            ^
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.CptRicercaLodi.ToString())
            )
        {
            // Seleziono l'area Milano
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptRicerca.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "1";
            }

            // Seleziono l'area Lodi
            if (GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.CptRicercaLodi.ToString())
                && DropDownListArea.SelectedIndex == 0
                )
            {
                DropDownListArea.SelectedValue = "2";
            }

            DropDownListArea.Enabled = false;
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            GridViewCantieriCommittente.PageIndex = 0;
            GridViewCantieriImpresa.PageIndex = 0;

            CantiereNotificaCollection cantieri;
            CantiereFilter filtro = CaricaCantieri(out cantieri);

            //TBridge.Cemi.GestioneUtenti.Business.Identities.IUtente utente =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.IUtente)
            //    HttpContext.Current.User.Identity);

            LogRicerca log = new LogRicerca();
            //log.IdUtente = utente.IdUtente;
            log.IdUtente = GestioneUtentiBiz.GetIdUtente();
            log.XmlFiltro = biz.GetPropertiesDelFiltro(filtro);
            log.Sezione = SezioneLogRicerca.RicercaCantieri;

            biz.InsertLogRicerca(log);
        }
    }

    /// <summary>
    /// Carichiamo i cantieri
    /// </summary>
    public CantiereFilter CaricaCantieri(out CantiereNotificaCollection cantieri)
    {
        CantiereFilter filtro = new CantiereFilter();
        cantieri = null;

        string comRic = TextBoxCommittente.Text.Trim();
        if (!string.IsNullOrEmpty(comRic))
            filtro.Committente = comRic;

        string indRic = TextBoxIndirizzo.Text.Trim();
        if (!string.IsNullOrEmpty(indRic))
            filtro.Indirizzo = indRic;

        string impRic = TextBoxImpresa.Text.Trim();
        if (!string.IsNullOrEmpty(impRic))
            filtro.Impresa = impRic;

        string natRic = TextBoxNatura.Text.Trim();
        if (!string.IsNullOrEmpty(natRic))
            filtro.NaturaOpera = natRic;

        if (!string.IsNullOrEmpty(TextBoxAmmontare.Text))
            filtro.Ammontare = decimal.Parse(TextBoxAmmontare.Text);

        string dataInizioString = TextBoxDataInizio.Text.Trim();
        if (!string.IsNullOrEmpty(dataInizioString))
        {
            filtro.DataInizio = DateTime.ParseExact(dataInizioString, "ddMMyyyy", null).Date;
        }

        string dataFineString = TextBoxDataFine.Text.Trim();
        if (!string.IsNullOrEmpty(dataFineString))
        {
            filtro.DataFine = DateTime.ParseExact(dataFineString, "ddMMyyyy", null).Date;
        }

        string comuRic = TextBoxComune.Text.Trim();
        if (!string.IsNullOrEmpty(comuRic))
            filtro.Comune = comuRic;

        filtro.IdASL = idASL;
        filtro.IdArea = Int16.Parse(DropDownListArea.SelectedValue);

        if (!String.IsNullOrEmpty(TextBoxNumeroLavoratori.Text))
        {
            filtro.NumeroLavoratori = Int32.Parse(TextBoxNumeroLavoratori.Text);
        }

        GridViewCantieriCommittente.Visible = false;
        GridViewCantieriImpresa.Visible = false;

        if (RadioButtonCommittente.Checked)
        {
            GridViewCantieriCommittente.Visible = true;

            cantieri =
                biz.RicercaCantieriPerCommittente(filtro);

            if (!string.IsNullOrEmpty(TextBoxImpresa.Text))
                GridViewCantieriCommittente.Columns[INDICEIMPRESA].Visible = true;
            else
                GridViewCantieriCommittente.Columns[INDICEIMPRESA].Visible = false;

            LabelCantieriTrovati.Visible = true;
            LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", cantieri.Count);

            GridViewCantieriCommittente.DataSource = cantieri;
            GridViewCantieriCommittente.DataBind();
        }
        else if (RadioButtonImpresa.Checked)
        {
            GridViewCantieriImpresa.Visible = true;

            //Se non viene valorizzato il campo impresa � inutile visualizzare la colonna
            //if (TextBoxImpresa.Text.Trim().Length == 0)
            //    this.GridViewCantieriImpresa.Columns[0].Visible = false;
            //else
            GridViewCantieriImpresa.Columns[0].Visible = true;

            cantieri =
                biz.RicercaCantieriPerImpresa(filtro);

            LabelCantieriTrovati.Visible = true;
            LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", cantieri.Count);

            GridViewCantieriImpresa.DataSource = cantieri;
            GridViewCantieriImpresa.DataBind();
        }

        return filtro;
    }

    protected void GridViewCantieriCommittente_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CantiereNotifica cantiere = (CantiereNotifica) e.Row.DataItem;
            Label lCommittente = (Label) e.Row.FindControl("LabelCommittente");
            Label lImpresa = (Label) e.Row.FindControl("LabelImpresa");

            if (String.IsNullOrEmpty(committenteRagioneSociale)
                || committenteRagioneSociale != cantiere.Committente.RagioneSociale)
            {
                committenteRagioneSociale = cantiere.Committente.RagioneSociale;
                lCommittente.Text = cantiere.Committente.RagioneSociale;
            }

            if (cantiere.ImpresaRicercata != null)
            {
                lImpresa.Text = cantiere.ImpresaRicercata.RagioneSociale;
            }
        }
    }

    protected void GridViewCantieriImpresa_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CantiereNotifica cantiere = (CantiereNotifica) e.Row.DataItem;
            Label lImpresa = (Label) e.Row.FindControl("LabelImpresa");

            if (impresa == string.Empty || impresa != cantiere.ImpresaRicercataRagioneSociale)
            {
                impresa = cantiere.ImpresaRicercataRagioneSociale;
                lImpresa.Text = cantiere.ImpresaRicercataRagioneSociale;
            }
        }
    }

    protected void GridViewCantieriCommittente_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewCantieriCommittente.PageIndex = e.NewPageIndex;
        CantiereNotificaCollection cantieri;
        CaricaCantieri(out cantieri);
    }

    protected void GridViewCantieriImpresa_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewCantieriImpresa.PageIndex = e.NewPageIndex;
        CantiereNotificaCollection cantieri;
        CaricaCantieri(out cantieri);
    }

    protected void GridViewCantieriCommittente_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idNotifica = (Int32) GridViewCantieriCommittente.DataKeys[e.NewSelectedIndex].Values["IdNotifica"];
        Response.Redirect(String.Format("~/Cpt/CptNotificaDettagli.aspx?idNotifica={0}", idNotifica));
    }

    protected void GridViewCantieriImpresa_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idNotifica = (Int32) GridViewCantieriImpresa.DataKeys[e.NewSelectedIndex].Values["IdNotifica"];
        Response.Redirect(String.Format("~/Cpt/CptNotificaDettagli.aspx?idNotifica={0}", idNotifica));
    }

    protected void ButtonEstrai_Click(object sender, EventArgs e)
    {
        CantiereNotificaCollection cantieri;
        CaricaCantieri(out cantieri);
        string stampa = PreparaStampa(cantieri);

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Cantieri.xls");
        Response.ContentType = "application/vnd.ms-excel";
        Response.Write(stampa);
        Response.End();
    }

    private String PreparaStampa(CantiereNotificaCollection cantieri)
    {
        GridView gv = new GridView();
        gv.ID = "gvCantieri";
        gv.AutoGenerateColumns = false;

        if (RadioButtonCommittente.Checked)
        {
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Committente";
            bc1.DataField = "CommittenteRagioneSociale";

            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Impresa cercata";
            bc2.DataField = "ImpresaRicercataRagioneSociale";

            BoundField bc21 = new BoundField();
            bc21.HeaderText = "Impresa cercata partita IVA";
            bc21.DataField = "ImpresaRicercataPartitaIva";

            BoundField bc22 = new BoundField();
            bc22.HeaderText = "Impresa cercata codice fiscale";
            bc22.DataField = "ImpresaRicercataCodiceFiscale";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc21);
            gv.Columns.Add(bc22);
        }
        else
        {
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Impresa";
            bc1.DataField = "ImpresaRicercataRagioneSociale";
            
            BoundField bc11 = new BoundField();
            bc11.HeaderText = "Impresa partita IVA";
            bc11.DataField = "ImpresaRicercataPartitaIva";

            BoundField bc12 = new BoundField();
            bc12.HeaderText = "Impresa codice fiscale";
            bc12.DataField = "ImpresaRicercataCodiceFiscale";

            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Committente";
            bc2.DataField = "CommittenteRagioneSociale";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc11);
            gv.Columns.Add(bc12);
            gv.Columns.Add(bc2);
        }

        BoundField bc3 = new BoundField();
        bc3.HeaderText = "Indirizzo";
        bc3.DataField = "IndirizzoCompleto";
        BoundField bc33 = new BoundField();
        bc33.HeaderText = "Indirizzo";
        bc33.DataField = "IndirizzoDenominazione";
        BoundField bc34 = new BoundField();
        bc34.HeaderText = "Comune";
        bc34.DataField = "IndirizzoComune";
        BoundField bc35 = new BoundField();
        bc35.HeaderText = "Provincia";
        bc35.DataField = "IndirizzoProvincia";
        BoundField bc36 = new BoundField();
        bc36.HeaderText = "Cap";
        bc36.DataField = "IndirizzoCap";
        BoundField bc31 = new BoundField();
        bc31.HeaderText = "Latitudine";
        bc31.DataField = "Latitudine";
        BoundField bc32 = new BoundField();
        bc32.HeaderText = "Longitudine";
        bc32.DataField = "Longitudine";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Data inizio lavori";
        bc4.DataField = "DataInizioLavori";
        bc4.HtmlEncode = false;
        bc4.DataFormatString = "{0:dd/MM/yyyy}";
        BoundField bc5 = new BoundField();
        bc5.HeaderText = "Data fine lavori";
        bc5.DataField = "DataFineLavori";
        bc5.HtmlEncode = false;
        bc5.DataFormatString = "{0:dd/MM/yyyy}";
        BoundField bc6 = new BoundField();
        bc6.HeaderText = "Natura dell'opera";
        bc6.DataField = "NaturaOpera";
        BoundField bc7 = new BoundField();
        bc7.HeaderText = "Numero previsto imprese";
        bc7.DataField = "NumeroImprese";
        BoundField bc8 = new BoundField();
        bc8.HeaderText = "Numero massimo lavoratori";
        bc8.DataField = "NumeroLavoratori";
        BoundField bc9 = new BoundField();
        bc9.HeaderText = "Ammontare";
        bc9.DataField = "Ammontare";
        BoundField bc10 = new BoundField()
                            {
                                HeaderText = "Protocollo Regionale",
                                DataField = "ProtocolloRegioneNotifica",
                                //DataFormatString =  "'{0}'"
                            };


        //gv.Columns.Add(bc3);
        gv.Columns.Add(bc33);
        gv.Columns.Add(bc34);
        gv.Columns.Add(bc35);
        gv.Columns.Add(bc36);
        gv.Columns.Add(bc31);
        gv.Columns.Add(bc32);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc6);
        gv.Columns.Add(bc7);
        gv.Columns.Add(bc8);
        gv.Columns.Add(bc9);
        gv.Columns.Add(bc10);

        gv.DataSource = cantieri;
        gv.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw.ToString();
    }
}