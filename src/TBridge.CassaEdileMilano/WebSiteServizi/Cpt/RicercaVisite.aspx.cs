using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Cpt_RicercaVisite : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCPTVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaDPLVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaASLERSLTVisualizzazione);
        funzionalita.Add(FunzionalitaPredefinite.CptAttivitaCassaEdileVisualizzazione);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "~/Cpt/RicercaVisite.aspx");

        #endregion
    }
}