using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.Cantieri.Type.Enums;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Subappalto=TBridge.Cemi.Cpt.Type.Entities.Subappalto;

public partial class Cpt_CptNotificaDettagli : Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);
        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "CptNotificaDettagli.aspx");

        #endregion

        ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

        if (Request.QueryString["idNotifica"] != null)
        {
            int idNotifica = Int32.Parse(Request.QueryString["idNotifica"]);
            ViewState["IdNotifica"] = idNotifica;

            if (!biz.IsNotificaTelematica(idNotifica))
            {
                CaricaNotifica(idNotifica);
            }
            else
            {
                CaricaNotificaTelematica(idNotifica);
            }
        }
    }

    private void CaricaNotificaTelematica(int idNotifica)
    {
        PanelNotificaTradizionale.Visible = false;
        PanelNotificaTelematica.Visible = true;
        ButtonDocumento.Visible = false;

        NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica);
        CptRiassuntoTelematiche1.CaricaNotifica(notifica);
    }

    private void CaricaNotifica(int idNotifica)
    {
        Notifica notifica = biz.GetNotifica(idNotifica);

        // Dati generali
        if (notifica.IdNotificaPadre.HasValue)
            LabelTipoNotifica.Text = "Aggiornamento";
        else
            LabelTipoNotifica.Text = "Notifica preliminare";

        LabelArea.Text = notifica.Area.Descrizione;
        LabelProtocollo.Text = notifica.IdNotifica.ToString();
        LabelRiasData.Text = notifica.Data.ToShortDateString();
        LabelRiasNaturaOpera.Text = notifica.NaturaOpera;
        LabelRiasNumeroAppalto.Text = notifica.NumeroAppalto;

        LabelRiasIndirizzi.Text = string.Empty;
        foreach (Indirizzo indirizzo in notifica.Indirizzi)
        {
            LabelRiasIndirizzi.Text += indirizzo.IndirizzoCompleto + "<BR>";
        }

        LabelRiasNumeroLavoratori.Text = notifica.NumeroMassimoLavoratori.ToString();
        LabelRiasNumeroImprese.Text = notifica.NumeroImprese.ToString();
        LabelRiasNumeroAutonomi.Text = notifica.NumeroLavoratoriAutonomi.ToString();
        if (notifica.AmmontareComplessivo.HasValue)
            LabelRiasImporto.Text = notifica.AmmontareComplessivo.Value.ToString("C");

        // Committente
        Committente committente = notifica.Committente;
        LabelRiasComRagioneSociale.Text = committente.RagioneSociale;
        LabelRiasComIndirizzo.Text = committente.Indirizzo;
        LabelRiasComComune.Text = committente.Comune;
        LabelRiasComProvincia.Text = committente.Provincia;
        LabelRiasComCap.Text = committente.Cap;
        LabelRiasComIva.Text = committente.PartitaIva;
        LabelRiasComFiscale.Text = committente.CodiceFiscale;
        LabelRiasComTelefono.Text = committente.Telefono;
        LabelRiasComFax.Text = committente.Fax;
        LabelRiasComRiferimento.Text = committente.PersonaRiferimento;

        // Coordinatori
        //      Progettazione
        if (notifica.CoordinatoreSicurezzaProgettazione != null)
        {
            LabelRiasProgNominativo.Text = notifica.CoordinatoreSicurezzaProgettazione.Nominativo;
            LabelRiasProgRagioneSociale.Text = notifica.CoordinatoreSicurezzaProgettazione.RagioneSociale;
            LabelRiasProgIndirizzo.Text = notifica.CoordinatoreSicurezzaProgettazione.Indirizzo;
            LabelRiasProgTelefono.Text = notifica.CoordinatoreSicurezzaProgettazione.Telefono;
            LabelRiasProgFax.Text = notifica.CoordinatoreSicurezzaProgettazione.Fax;
        }
        //      Realizzazione
        if (notifica.CoordinatoreSicurezzaRealizzazione != null)
        {
            LabelRiasRealNominativo.Text = notifica.CoordinatoreSicurezzaRealizzazione.Nominativo;
            LabelRiasRealRagioneSociale.Text = notifica.CoordinatoreSicurezzaRealizzazione.RagioneSociale;
            LabelRiasRealIndirizzo.Text = notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo;
            LabelRiasRealTelefono.Text = notifica.CoordinatoreSicurezzaRealizzazione.Telefono;
            LabelRiasRealFax.Text = notifica.CoordinatoreSicurezzaRealizzazione.Fax;
        }
        //      Responsabile
        if (notifica.DirettoreLavori != null)
        {
            LabelRiasRespNominativo.Text = notifica.DirettoreLavori.Nominativo;
            LabelRiasRespRagioneSociale.Text = notifica.DirettoreLavori.RagioneSociale;
            LabelRiasRespIndirizzo.Text = notifica.DirettoreLavori.Indirizzo;
            LabelRiasRespTelefono.Text = notifica.DirettoreLavori.Telefono;
            LabelRiasRespFax.Text = notifica.DirettoreLavori.Fax;
        }
        if (notifica.ResponsabileCommittente)
        {
            CheckBoxRiasRespCommittente.Checked = true;
            LabelRiasRespNominativo.Text = notifica.Committente.RagioneSociale;
        }

        // Durata
        if (notifica.DataInizioLavori.HasValue)
            LabelRiasDataInizio.Text = notifica.DataInizioLavori.Value.ToShortDateString();
        if (notifica.DataFineLavori.HasValue)
            LabelRiasDataFine.Text = notifica.DataFineLavori.Value.ToShortDateString();
        LabelRiasDurata.Text = notifica.Durata.ToString();
        LabelRiasGiorniUomo.Text = notifica.NumeroGiorniUomo.ToString();

        // Subappalti
        SubappaltoCollection subappalti = notifica.Subappalti;
        RepeaterSubappalti.DataSource = subappalti;
        RepeaterSubappalti.DataBind();
    }

    protected void ButtonReport_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/ReportCpt.aspx?idNotifica={0}", ViewState["IdNotifica"]));
    }

    protected void RepeaterSubappalti_OnItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Subappalto sub = (Subappalto) e.Item.DataItem;
            Panel pAppaltante = (Panel) e.Item.FindControl("PanelRip");
            Label lAppaltanteId = (Label) e.Item.FindControl("LabelAppaltanteId");
            Label lAppaltataId = (Label) e.Item.FindControl("LabelAppaltataId");

            if (sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                lAppaltataId.Text = sub.Appaltata.IdImpresa.ToString();

            if (sub.Appaltante != null)
            {
                pAppaltante.Visible = true;

                if (sub.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                    lAppaltanteId.Text = sub.Appaltante.IdImpresa.ToString();
            }
            else
                pAppaltante.Visible = false;
        }
    }

    protected void ButtonDocumento_Click(object sender, EventArgs e)
    {
        string percorsoFile = ConfigurationManager.AppSettings["NotifichePercorsoFile"];
        int idNotifica = Int32.Parse(Request.QueryString["idNotifica"]);

        try
        {
            string pathFile = String.Format("{0}{1}.pdf", percorsoFile, idNotifica);
            //Set the appropriate ContentType.
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.pdf", idNotifica));
            Response.ContentType = "application/pdf";
            //Write the file directly to the HTTP content output stream.
            Response.WriteFile(pathFile);
            Response.Flush();
            Response.End();
        }
        catch (Exception exc)
        {
            Response.Redirect("~/Cpt/CptFileNonDisponibile.aspx", false);
        }
    }
}