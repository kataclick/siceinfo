using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CptDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotifica);
        funzionalita.Add(FunzionalitaPredefinite.CptRicerca);
        funzionalita.Add(FunzionalitaPredefinite.CptInserimentoAggiornamentoNotificaLodi);
        funzionalita.Add(FunzionalitaPredefinite.CptRicercaLodi);
        funzionalita.Add(FunzionalitaPredefinite.CptStatistiche);
        funzionalita.Add(FunzionalitaPredefinite.CptEstrazioneExcelCantieri);
        funzionalita.Add(FunzionalitaPredefinite.CptEstrazioneBrescia);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "~/Cpt/CptDefault.aspx");
    }
}