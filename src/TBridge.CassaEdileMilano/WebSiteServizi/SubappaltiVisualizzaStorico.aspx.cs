using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Subappalti.Business;

/// <summary>
/// Visualizza lo storico delle ricerche - form usata dall'amministratore
/// </summary>
public partial class SubappaltiLeggiStorico : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Controllo l'autorizzazione dell'utente a vedere la pagina ...
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.SubappaltiRicerca,
                                              "SubappaltiVisualizzaStorico.aspx");
    }

    protected void btnRicerca_Click(object sender, EventArgs e)
    {
        VisualizzaStorico();
    }

    /// <summary>
    /// Carica lo storico nella GridView
    /// </summary>
    private void VisualizzaStorico()
    {
        SubappaltiBusiness business = new SubappaltiBusiness();

        int? idImpresa = null;
        int? idImpresaRicercante = null;
        DateTime? dataDa = null;
        DateTime? dataA = null;
        int test;
        DateTime testd;
        // Imposto i parametri
        if (!string.IsNullOrEmpty(txtIdImpresa.Text) && Int32.TryParse(txtIdImpresa.Text, out test))
            idImpresa = int.Parse(txtIdImpresa.Text);
        if (!string.IsNullOrEmpty(txtImpresaRicercante.Text) && Int32.TryParse(txtImpresaRicercante.Text, out test))
            idImpresaRicercante = int.Parse(txtImpresaRicercante.Text);
        if (!string.IsNullOrEmpty(txtDataDa.Text) && DateTime.TryParse(txtDataDa.Text, out testd))
            dataDa = DateTime.Parse(txtDataDa.Text);
        if (!string.IsNullOrEmpty(txtDataA.Text) && DateTime.TryParse(txtDataA.Text, out testd))
            dataA = DateTime.Parse(txtDataA.Text);

        // Salvo la datasource nella session perch� mi serve per il paging
        //Session["Subappalti.Datasource"] =
        //    business.VisualizzaStorico(null, idImpresa, idImpresaRicercante, dataDa, dataA);

        //gvStorico.DataSource = Session["Subappalti.Datasource"];
        gvStorico.DataSource = business.VisualizzaStorico(null, idImpresa, idImpresaRicercante, dataDa, dataA);

        LabelRicerca.Visible = true;

        gvStorico.DataBind();

        //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
        //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
        //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
        //TBridge.Cemi.ActivityTracking.Log.Write("Subbapalti visualizza storico", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);
    } // VisualizzaStorico

    protected void gvStorico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // A sto punto la datasource � nulla ... la devo ricaricare
        //gvStorico.DataSource = Session["Subappalti.Datasource"];

        gvStorico.PageIndex = e.NewPageIndex;
        VisualizzaStorico();
        //gvStorico.DataBind();
    }
} // Class