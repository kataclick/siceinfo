﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SOLDO.aspx.cs" Inherits="SOLDO_SOLDO" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuSOLDO.ascx" tagname="MenuSOLDO" tagprefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    </asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="SOLDO" sottoTitolo="Accedi a SOLDO" />
    <br />
    Accesso al e dal sistema SOLDO.
    <br />
    <br />
    <asp:Label
        ID="LabelMessaggio"
        runat="server"
        CssClass="messaggiErrore">
    </asp:Label>
    <br />
    <asp:Button
        ID="ButtonSOLDO"
        runat="server"
        Text="Accedi a SOLDO"
        Visible="false" 
        onclick="ButtonSOLDO_Click" />
</asp:Content>


