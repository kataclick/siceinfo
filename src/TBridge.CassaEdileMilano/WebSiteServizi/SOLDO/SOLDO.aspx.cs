﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using Impresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using Consulente = TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente;
using Utente = TBridge.Cemi.GestioneUtenti.Type.Entities.Utente;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class SOLDO_SOLDO : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            switch (Request.QueryString["direzione"].ToUpper())
            {
                case "SOLDO":
                    ButtonSOLDO.Visible = true;
                    PreparaCollegamentoSOLDO();
                    break;
                case "CEMI":
                    EffettuaLoginCEMI();
                    break;
            }
        }
    }

    private void EffettuaLoginCEMI()
    {
        String tipoUtenteCompl = Request.QueryString["txUtente"];
        String md5 = Request.QueryString["cemi"];

        if (!String.IsNullOrEmpty(tipoUtenteCompl))
        {
            String[] tipoUtenteSplitted = tipoUtenteCompl.Split('_');

            if (tipoUtenteSplitted.Length == 2)
            {
                String tipoUtente = tipoUtenteSplitted[0];
                String codice = tipoUtenteSplitted[1];
                Int32 codInt = -1;
                String ultimi7CarRagSoc = null;
                //UtentiManager utManager = new UtentiManager();
                
                if (Int32.TryParse(codice, out codInt))
                {
                    Common commonBiz = new Common();

                    // Recupero il codice utente per l'impresa e il consulente
                    switch (tipoUtente)
                    {
                        case "IMP":
                            codInt = commonBiz.GetIdUtenteByIdImpresa(codInt);
                            break;
                        case "CS":
                            codInt = commonBiz.GetIdUtenteByIdConsulente(codInt);
                            break;
                    }

                    Utente ut = GestioneUtentiBiz.GetIdentitaUtente(codInt);

                    if (ut != null)
                    {
                        switch (tipoUtente)
                        {
                            case "IMP":
                                Impresa imp = ut as Impresa;
                                if (imp != null)
                                {
                                    if (imp.RagioneSociale.Trim().Length > 7)
                                    {
                                        ultimi7CarRagSoc = imp.RagioneSociale.Trim().Substring(imp.RagioneSociale.Length - 7, 7);
                                    }
                                    else
                                    {
                                        ultimi7CarRagSoc = imp.RagioneSociale.Trim();
                                    }
                                }
                                break;
                            case "CS":
                                Consulente con = ut as Consulente;
                                if (con != null)
                                {
                                    if (con.RagioneSociale.Trim().Length > 7)
                                    {
                                        ultimi7CarRagSoc = con.RagioneSociale.Trim().Substring(con.RagioneSociale.Length - 7, 7);
                                    }
                                    else
                                    {
                                        ultimi7CarRagSoc = con.RagioneSociale.Trim();
                                    }
                                }
                                break;
                            case "ADMAMMTEC":
                                ultimi7CarRagSoc = "AMMTEC";
                                break;
                            case "ADMAMMAPP":
                                ultimi7CarRagSoc = "AMMAPP";
                                break;
                            case "ADMOPERAT":
                                ultimi7CarRagSoc = "OPERAT";
                                break;
                            default:
                                LabelMessaggio.Text = "Tipologia di utente non riconosciuta";
                                break;
                        }

                        DateTime ora = DateTime.Now;
                        String md5Calc = SOLDOManager.CalculateMD5Hash(String.Format("{0}{1}", ultimi7CarRagSoc, ora.ToString("yyyyMMdd")));
                        if (md5.ToUpper() == md5Calc.ToUpper())
                        {
                            ImpersonateManager.ImpersonateSOLDO(ut.UserName);
                        }
                        else
                        {
                            LabelMessaggio.Text = "Valore cemi non valido";
                        }
                    }
                    else
                    {
                        LabelMessaggio.Text = "Utente non trovato";
                    }
                }
                else
                {
                    LabelMessaggio.Text = "Codice dell'utenza non valido";
                }
            }
            else
            {
                LabelMessaggio.Text = "Valore txUtente non valido";
            }
        }
        else
        {
            LabelMessaggio.Text = "Valore txUtente non valido";
        }
    }

    private void PreparaCollegamentoSOLDO()
    {
        String urlBase = SOLDOManager.PaginaLoginSOLDO;
        String tipoUtente = null;
        String codice = null;
        String ultimi7CarRagSoc = null;
        String pagina = "3";

        // IMPRESA
        if (GestioneUtentiBiz.IsImpresa())
        {
            tipoUtente = "IMP";

            Impresa impresa = (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            codice = impresa.IdImpresa.ToString().PadLeft(6, '0');

            if (impresa.RagioneSociale.Trim().Length > 7)
            {
                ultimi7CarRagSoc = impresa.RagioneSociale.Trim().Substring(impresa.RagioneSociale.Length - 7, 7);
            }
            else
            {
                ultimi7CarRagSoc = impresa.RagioneSociale.Trim();
            }
        }
        else
        {
            // CONSULENTE
            if (GestioneUtentiBiz.IsConsulente())
            {
                tipoUtente = "CS";

                Consulente consulente = (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                codice = consulente.IdConsulente.ToString().PadLeft(6, '0');

                if (consulente.RagioneSociale.Trim().Length > 7)
                {
                    ultimi7CarRagSoc = consulente.RagioneSociale.Trim().Substring(consulente.RagioneSociale.Length - 7, 7);
                }
                else
                {
                    ultimi7CarRagSoc = consulente.RagioneSociale.Trim();
                }
            }
            else
            {
                Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                codice = utente.IdUtente.ToString().PadLeft(6, '0');

                // OPERATORI CEMI
                if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOAmministratoreTecnico))
                {
                    tipoUtente = "AMMTEC";
                    ultimi7CarRagSoc = "AMMTEC";
                }
                else
                {
                    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOAmministratoreApplicativo))
                    {
                        tipoUtente = "AMMAPP";
                        ultimi7CarRagSoc = "AMMAPP";
                    }
                    else
                    {
                        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOOperatore))
                        {
                            tipoUtente = "OPERAT";
                            ultimi7CarRagSoc = "OPERAT";
                        }
                        else
                        {
                            ButtonSOLDO.Visible = true;
                            LabelMessaggio.Text = "Non si è autorizzati ad accedere al sistema SOLDO, contattare un amministratore.";
                        }
                    }
                }
            }
        }

        DateTime ora = DateTime.Now;
        String md5 = SOLDOManager.CalculateMD5Hash(String.Format("{0}{1}", ultimi7CarRagSoc, ora.ToString("yyyyMMdd")));
        //String md5 = SOLDOManager.CalculateMD5Hash(ultimi7CarRagSoc);
        String url = String.Format("{0}?txUtente={1}_{2}&cemi={3}&pagina={4}", urlBase, tipoUtente, codice, md5, pagina);
        
        ViewState["urlSOLDO"] = url;
        //ButtonSOLDO.Attributes.Add("onClick", String.Format("window.open('{0}'); return false;", url));
        ButtonSOLDO.Attributes.Add("onClick", url);
    }

    protected void ButtonSOLDO_Click(object sender, EventArgs e)
    {
        Response.Redirect(ViewState["urlSOLDO"].ToString());
    }
}
