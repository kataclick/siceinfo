using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class ReportLavoratori : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoLavoratore, "ReportLavoratori.aspx");
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoLavoratore, "ReportLavoratori.aspx");
    }

    protected void ReportViewerLavoratori_Init(object sender, EventArgs e)
    {
        ReportViewerLavoratori.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        string tipoReport = Request.QueryString["tipo"];
        if (tipoReport == "ore")
        {
            ReportViewerLavoratori.ServerReport.ReportPath = "/ReportLavoratoriCE/ReportOre";
            TitoloSottotitolo1.sottoTitolo = "Ore accantonate";

            TableLabelOre.Visible = true;
            TableLabelRapporto.Visible = false;
            TableLabelPrestazioni.Visible = false;
        }
        else if (tipoReport == "rapporto")
        {
            ReportViewerLavoratori.ServerReport.ReportPath = "/ReportLavoratoriCE/ReportRapporto";
            TitoloSottotitolo1.sottoTitolo = "Rapporti di lavoro";

            TableLabelOre.Visible = false;
            TableLabelRapporto.Visible = true;
            TableLabelPrestazioni.Visible = false;
        }
        else if (tipoReport == "prestazioni")
        {
            ReportViewerLavoratori.ServerReport.ReportPath = "/ReportLavoratoriCE/ReportPrestazioni";
            TitoloSottotitolo1.sottoTitolo = "Pagamenti erogati";

            TableLabelOre.Visible = false;
            TableLabelRapporto.Visible = false;
            TableLabelPrestazioni.Visible = true;
        }

        int idLavoratore = ((Lavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdLavoratore;

        ReportParameter param = new ReportParameter("idLavoratore", idLavoratore.ToString());
        ReportParameter[] listaParam = new ReportParameter[1];
        listaParam[0] = param;

        ReportViewerLavoratori.ServerReport.SetParameters(listaParam);
    }
}