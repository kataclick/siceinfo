<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneFornitore.aspx.cs" Inherits="GestioneUtentiRegistrazioneFornitore" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RegistrazioneFornitore.ascx" TagName="RegistrazioneFornitore"
    TagPrefix="uc3" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione utenti" sottoTitolo="Registrazione fornitore"/>
    <br />
    <uc3:RegistrazioneFornitore ID="RegistrazioneFornitore1" runat="server" />
</asp:Content>

