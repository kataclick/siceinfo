using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Entities;

public partial class ReportBollettinoFrecciaStatistiche : Page
{
    private readonly Common bizCommon = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaStatistiche,
                                              "ReportBollettinoFrecciaStatistiche.aspx");

        #endregion

        //ReportViewerBollettinoFrecciaStatistiche.ServerReport.ReportServerUrl =
        //    new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        //ReportViewerBollettinoFrecciaStatistiche.ServerReport.ReportPath = "/ReportBollettinoFreccia/ReportBollettinoFrecciaStatisticheVideo";

    }

    protected void ReportViewerBollettinoFrecciaStatistiche_Init(object sender, EventArgs e)
    {
        ReportViewerBollettinoFrecciaStatistiche.ServerReport.ReportServerUrl =
           new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        ReportViewerBollettinoFrecciaStatistiche.ServerReport.ReportPath = "/ReportBollettinoFreccia/ReportBollettinoFrecciaStatisticheVideo";
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        Page.Validate("bollettino");

        if (Page.IsValid)
        {
            ReportViewerBollettinoFrecciaStatistiche.Reset();

            ReportViewerBollettinoFrecciaStatistiche.ServerReport.ReportServerUrl =
           new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            ReportViewerBollettinoFrecciaStatistiche.ServerReport.ReportPath = "/ReportBollettinoFreccia/ReportBollettinoFrecciaStatisticheVideo";

            List<ReportParameter> listaParam = new List<ReportParameter>();
            
            if (!string.IsNullOrEmpty(TextBoxDataRicercaDa.Text))
            {
                listaParam.Add(new ReportParameter("dataInizio",
                                                   DateTime.Parse(TextBoxDataRicercaDa.Text).ToShortDateString()));
            }
            else
            {
                listaParam.Add(new ReportParameter("dataInizio", DBNull.Value.ToString()));
            }

            if (!string.IsNullOrEmpty(TextBoxDataRicercaA.Text))
            {
                listaParam.Add(new ReportParameter("dataFine",
                                                   DateTime.Parse(TextBoxDataRicercaA.Text).ToShortDateString()));
            }
            else
            {
                listaParam.Add(new ReportParameter("dataFine", DBNull.Value.ToString()));
            }

            ReportViewerBollettinoFrecciaStatistiche.ServerReport.SetParameters(listaParam);

            ReportViewerBollettinoFrecciaStatistiche.Visible = true;
        }
        else
        {
            ReportViewerBollettinoFrecciaStatistiche.Visible = false;
        }
    }

}