﻿using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Corsi.Type.Entities;

public partial class VisualizzazioneMappa : Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Locazione locazione = new Locazione();

            locazione.Latitudine = (decimal?) 45.464044;
            locazione.Longitudine = (decimal?) 9.191567;

            if (!String.IsNullOrEmpty(Request.QueryString["latitudine"]))
            {
                locazione.Latitudine = Decimal.Parse(Request.QueryString["latitudine"]);
            }

            if (!String.IsNullOrEmpty(Request.QueryString["longitudine"]))
            {
                locazione.Longitudine = Decimal.Parse(Request.QueryString["longitudine"]);
            }

            if (!String.IsNullOrEmpty(Request.QueryString["note"]))
            {
                locazione.Note = Request.QueryString["note"];
            }

            if (!String.IsNullOrEmpty(Request.QueryString["indirizzo"]))
            {
                locazione.Indirizzo = Request.QueryString["indirizzo"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["comune"]))
            {
                locazione.Comune = Request.QueryString["comune"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["provincia"]))
            {
                locazione.Provincia = Request.QueryString["provincia"];
            }
            if (!String.IsNullOrEmpty(Request.QueryString["cap"]))
            {
                locazione.Cap = Request.QueryString["cap"];
            }

            if (locazione.Latitudine.HasValue && locazione.Longitudine.HasValue)
            {
                var descrizione = new StringBuilder();
                // Indirizzo del cantiere
                descrizione.Append(locazione.Indirizzo);
                descrizione.Append("<br />");
                descrizione.Append(locazione.Comune);
                descrizione.Append(" ");
                if (!string.IsNullOrEmpty(locazione.Provincia))
                {
                    descrizione.Append(" (");
                    descrizione.Append(locazione.Provincia);
                    descrizione.Append(")");
                }

                // Informazioni aggiuntive
                if (!String.IsNullOrEmpty(locazione.Note))
                {
                    descrizione.Append("<br />");
                    descrizione.Append("<br />");
                    descrizione.Append(locazione.Note);
                }

                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(String.Format("LoadMap('{0}','{1}');", locazione.Latitudine.ToString().Replace(',', '.'),
                                                   locazione.Longitudine.ToString().Replace(',', '.')));

                stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", 0,
                                                   locazione.Latitudine.ToString().Replace(',', '.'),
                                                   locazione.Longitudine.ToString().Replace(',', '.'), descrizione,
                                                   null));

                RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
            }
        }
    }
}