<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InformativaSindacatiDefault.aspx.cs" Inherits="InformativaSindacati_InformativaSindacatiDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Download dell'applicativo"
        titolo="InfoSindacati" />
    <br />
    <asp:Label ID="LabelUltimoFile" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownload" runat="server" OnClick="ButtonDownload_Click" Text="Download" />
    <br />
    <br />
    <strong>N.B.:</strong> Il file zip disponibile al download contiene:
    <ul>
        <li>L'export in formato Microsoft Excel dell'elenco dei Lavoratori</li>
        <li>L'export in formato Microsoft Access dell'elenco dei Lavoratori</li>
        <li>La nuova versione dell'applicativo con relativo database aggiornato</li>
    </ul>
    Va pertanto sostituita la precedente versione dell'applicativo con quella contenuta
    le file zip (cartella App)<br />
    Per il corretto funzionamento dell'applicativo � necessario il Microsoft .NET Framework
    4, disponibile tra gli aggiornamenti automatici di Windows.<br />
    Qualora non fosse gi� installato lo si pu� scaricare direttamente da Internet: <a
        href="http://www.microsoft.com/downloads/it-it/details.aspx?FamilyID=0a391abd-25c1-4fc0-919f-b21f31ab88b7">
        Microsoft .NET Framework 4 (programma di installazione autonomo)</a>
</asp:Content>