using System;
using System.Web.UI;
using TBridge.Cemi.Deleghe.Business;
using TBridge.Cemi.Deleghe.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Deleghe.Type.Enums;

public partial class Deleghe_GestioneDelegheSindacato : Page
{
    private readonly DelegheBusiness _biz = new DelegheBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione);

        DelegheRicercaDeleghe1.OnDelegaSelected += DelegheRicercaDeleghe1_OnDelegaSelected;

        PanelDettagli.Visible = false;

        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsSindacalista())
            {
                if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
                {
                    LabelInformazioniNormale.Visible = false;
                    LabelInformazioniStorico.Visible = true;
                    PanelLegenda.Visible = true;
                }

                // Mostro la ricerca nei casi in cui:
                // - Siamo in visualizzazione delle deleghe del mese corrente e abbiamo passato il giorno di apertura
                // - Siamo in visualizzazione dello storico
                if ((_biz.ApertaFaseInserimento() &&
                     (Request.QueryString["modalita"] == null || Request.QueryString["modalita"] != "storico")) ||
                    (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico"))
                {
                    //Sindacalista sindacalista =
                    //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista)
                    //     HttpContext.Current.User.Identity).Entity;
                    Sindacalista sindacalista =
                        (Sindacalista)
                        GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    if ((Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico") ||
                        !_biz.DelegheConfermate(DateTime.Now.Month, DateTime.Now.Year, sindacalista.Sindacato,
                                               sindacalista.ComprensorioSindacale))
                    {
                        PanelRicerca.Visible = true;
                        PanelMessaggio.Visible = false;
                    }
                    else
                    {
                        PanelRicerca.Visible = false;
                        PanelMessaggio.Visible = true;

                        if (sindacalista.ComprensorioSindacale != null)
                            LabelMessaggio.Text =
                                String.Format("Le deleghe del {0} sono gi� state confermate per questo mese.",
                                              sindacalista.ComprensorioSindacale.Descrizione);
                        else
                            LabelMessaggio.Text =
                                "Le deleghe di tutti i comprensori sindacali sono gi� state confermate per questo mese";
                    }
                }
                else
                {
                    PanelRicerca.Visible = false;
                    PanelMessaggio.Visible = true;

                    LabelMessaggio.Text =
                        "In questo periodo non � possibile la gestione delle deleghe.";
                }
            }
        }
    }

    private void DelegheRicercaDeleghe1_OnDelegaSelected(Delega delega)
    {
        if ((Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico") &&
            GestioneUtentiBiz.IsSindacalista())
        {
            PanelDettagli.Visible = true;

            if (delega.Stato.HasValue
                && delega.Stato.Value == StatoDelega.Confermata)
            {
                DelegheDettaglioDelega1.CaricaDelegaConIndirizzoAnagrafico(delega);
            }
            else
            {
                DelegheDettaglioDelega1.CaricaDelega(delega);
            }
        }
        else
        {
            Context.Items["IdDelega"] = delega.IdDelega;
            Server.Transfer("~/Deleghe/InserimentoDelega.aspx?modalita=modifica", true);
        }
    }

    protected void LinkButtonMostraLegenda_Click(object sender, EventArgs e)
    {
        PanelPuntiLegenda.Visible = !PanelPuntiLegenda.Visible;
    }
}