<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SbloccoDeleghe.aspx.cs" Inherits="Deleghe_SbloccoDeleghe" %>

<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Sblocco deleghe"
        titolo="Deleghe sindacali" />
    <br />
    <asp:Panel ID="PanelSblocco" runat="server" Visible="true">
        <table class="standardTable">
            <tr>
                <td>
                    Codice Sblocco
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodiceSblocco" MaxLength="8" runat="server" Width="300px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Immettere un codice di sblocco"
                        ValidationGroup="sblocco" ControlToValidate="TextBoxCodiceSblocco"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Il codice di sblocco deve essere composto da 8 caratteri"
                        ValidationExpression="^[a-zA-z0-9]{8}$" ValidationGroup="sblocco" ControlToValidate="TextBoxCodiceSblocco"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <asp:Button ID="ButtonSblocca" runat="server" Text="Cerca" OnClick="ButtonSblocca_Click"
                        ValidationGroup="sblocco" Width="150px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelConfermate" runat="server" Visible="false">
        <asp:Label ID="LabelGiaConfermate" runat="server" Text="Non � possibile accedere alla funzionalit�, le deleghe sono gi� state confermate"></asp:Label></asp:Panel>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelNonPresente" runat="server" Visible="false">
        <asp:Label ID="LabelDelegaNonPresente" runat="server" Font-Bold="true" Text="Non esiste nessuna delega associata al codice di sblocco fornito"></asp:Label>
    </asp:Panel>
    <asp:Panel ID="PanelDettagli" runat="server" Visible="false">
        <table class="standardTable">
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="LabelIdDelega" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Cognome
                </td>
                <td>
                    <asp:Label ID="LabelCognome" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Nome
                </td>
                <td>
                    <asp:Label ID="LabelNome" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Data di nascita
                </td>
                <td>
                    <asp:Label ID="LabelDataNascita" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Impresa
                </td>
                <td>
                    <asp:Label ID="LabelImpresa" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="ButtonConferma" runat="server" Text="Conferma sblocco" Width="150px"
                        OnClick="ButtonConferma_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
