using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Deleghe.Business;
using TBridge.Cemi.Deleghe.Type.Collections;
using TBridge.Cemi.Deleghe.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Deleghe_DelegheBloccate : Page
{
    private readonly DelegheBusiness biz = new DelegheBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestioneCE, "DelegheBloccate.aspx");

        if (!Page.IsPostBack)
        {
            GridViewDelegheBloccate.PageIndex = 0;
            CaricaDelegheBloccate();
        }
    }

    private void CaricaDelegheBloccate()
    {
        DelegaCollection deleghe = biz.GetDelegheBloccate();
        GridViewDelegheBloccate.DataSource = deleghe;
        GridViewDelegheBloccate.DataBind();
    }

    protected void GridViewDelegheBloccate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewDelegheBloccate.PageIndex = e.NewPageIndex;
        CaricaDelegheBloccate();
    }

    protected void GridViewDelegheBloccate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idDelega = (int) GridViewDelegheBloccate.DataKeys[e.NewSelectedIndex].Value;

        Delega delega = biz.GetDelega(idDelega);
        DelegheDatiDelega1.CaricaDelega(delega);
        PanelDettagli.Visible = true;
    }

    protected void GridViewDelegheBloccate_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idDelega = (int) GridViewDelegheBloccate.DataKeys[e.RowIndex].Value;
        if (biz.DeleteDelega(idDelega))
            CaricaDelegheBloccate();
    }
}