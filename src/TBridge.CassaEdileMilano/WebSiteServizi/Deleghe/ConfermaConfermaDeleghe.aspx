<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaConfermaDeleghe.aspx.cs" Inherits="Deleghe_ConfermaConfermaDeleghe" %>

<%@ Register Src="~/WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Conferma mensile" titolo="Gestione deleghe" />
    <br />
    <asp:Panel ID="PanelConferma" runat="server" width="100%">
        <b>Attenzione! Se si preme sul pulsante "Conferma deleghe" non sar� pi� possibile modificare le deleghe inserite a sistema.</b>
        <br />
        <br />
        <b>Stampare il file excel di riepilogo delle deleghe inserite ed allegarlo ai moduli cartacei di delega da consegnare a Cassa Edile di Milano.</b>
        <br />
        <br />
        <table class="standardTable">
            <tr>
                <td>
                    Sindacato di appartenenza:&nbsp;</td>
                <td>
                    <asp:Label ID="LabelSindacato" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Comprensorio:&nbsp;</td>
                <td>
                    <asp:Label ID="LabelComprensorio" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Numero deleghe che verranno confermate:&nbsp;</td>
                <td>
                    <asp:Label ID="LabelNumeroDeleghe" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <asp:Button ID="ButtonConferma" runat="server" Text="Conferma deleghe" OnClick="ButtonConferma_Click" />
        <br />
        <asp:Label ID="LabelErroreConferma" runat="server" ForeColor="Red" Text="Si � verificato un errore durante la conferma delle deleghe"
            Visible="False"></asp:Label></asp:Panel>
    <asp:Panel ID="PanelErrore" runat="server" width="100%">
        <asp:Label ID="LabelErrore" runat="server" Text="Nessun comprensorio associato, non � possibile confermare le deleghe"></asp:Label>
    </asp:Panel>
</asp:Content>

