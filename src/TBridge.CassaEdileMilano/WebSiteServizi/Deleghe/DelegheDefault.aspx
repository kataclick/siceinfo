<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DelegheDefault.aspx.cs" Inherits="Deleghe_DelegheDefault" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Benvenuto nella sezione Deleghe Sindacali"
        titolo="Deleghe sindacali" />
    <br />
    <p class="DefaultPage">
        In questa pagina � possibile inserire le deleghe sindacali dei lavoratori.
        <br />
        Il periodo in cui sar� possibile effettuare questa operazione � la seconda met�
        di tutti i mesi dell�anno. Selezionando le voci del men� verticale posto alla sinistra
        della videata � possibile effettuare tutte le attivit� disponibili per la gestione
        delle deleghe sindacali: "Inserimento delega", "Gestione deleghe" (per la modifica
        e la cancellazione delle deleghe), "Sblocco deleghe" (per i casi di omonimia), "Conferma
        mensile", "Esportazione" delle deleghe confermate in formato Excel e "Storico" per
        la visualizzazione delle deleghe confermate.
    </p>
</asp:Content>
