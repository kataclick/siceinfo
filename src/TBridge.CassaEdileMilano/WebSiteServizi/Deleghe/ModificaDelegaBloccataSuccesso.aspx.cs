using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Deleghe.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Deleghe_ModificaDelegaBloccataSuccesso : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione, "InserimentoDelegaSuccesso.aspx");

        Delega delega = (Delega) Context.Items["Delega"];
        List<Delega> deleghe = new List<Delega> {delega};
        GridViewDelegaAttuale.DataSource = deleghe;
        GridViewDelegaAttuale.DataBind();

        if (delega.IdDelega.HasValue)
            DelegheBloccateStoricoModifiche1.CaricaStoricoDelega(delega.IdDelega.Value);
    }
}