<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneDelegheCE.aspx.cs" Inherits="Deleghe_GestioneDelegheCE" %>

<%@ Register Src="../WebControls/DelegheDatiDelega.ascx" TagName="DelegheDatiDelega"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/DelegheRicercaDeleghe.ascx" TagName="DelegheRicercaDeleghe"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione"
        titolo="Gestione Deleghe" />
    <br />
    <asp:Label ID="LabelStampaEtichette" runat="server" ForeColor="Red" Text="Stampa etichette non disponibile"
        Visible="False"></asp:Label>
    <br />
    <uc3:DelegheRicercaDeleghe ID="DelegheRicercaDeleghe1" runat="server" />
    <br />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">

    <script type="text/javascript">
    function PrintBtnClicked()
    {
        var DymoAddIn, DymoLabel;
        try {
            DymoAddIn = new ActiveXObject('DYMO.DymoAddIn');
            DymoLabel = new ActiveXObject('DYMO.DymoLabels');
        }
        catch (e) {
        }
        
        if (DymoAddIn != null && DymoLabel != null) {
            DymoAddIn.SelectPrinter(DymoAddIn.GetCurrentPrinterName());

                var urlFormatoEtichetta = document.getElementById("<%= inputFileEtichetta.ClientID %>");
                var meseConferma = document.getElementById("<%= inputMeseConferma.ClientID %>");
                  var stampata = document.getElementById("<%= inputStampata.ClientID %>");
                var apertura = DymoAddIn.OpenURL(urlFormatoEtichetta.value);
                if (apertura) {
                    // Imposto la data sull'etichetta
                    DymoLabel.SetField("data", meseConferma.value);
                    var myTextBox = document.getElementById("<%= inputId.ClientID %>");
                    DymoLabel.SetField("codiceBarre", myTextBox.value);

                    if (stampata.value == "False" || window.confirm("La delega � gi� stata stampata, vuoi proseguire comunque?")) {
                        DymoAddIn.StartPrintJob();
                        DymoAddIn.Print(1, true);
                        DymoAddIn.EndPrintJob();

                        return true;
                    }
                }
                else
                    alert("Stampa etichette non possibile, file di formato etichette non trovato.");
            }
            else
                alert("Stampa etichette non possibile, mancano le librerie Dymo. Installare il CD in dotazione alla stampante.");
        
        return false;
	}
    </script>

    <a id="APanelDettagli"></a>
    <asp:Panel ID="PanelDettagli" runat="server" Width="100%" Visible="False">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <input type="hidden" id="inputId" runat="server" />
                    <input type="hidden" id="inputFileEtichetta" runat="server" />
                    <input type="hidden" id="inputStampata" runat="server" />
                    <asp:TextBox ID="TextBoxId" runat="server" Font-Bold="True" Visible="False"></asp:TextBox>
                    <input type="hidden" id="inputMeseConferma" runat="server" /><br />
                    <uc4:DelegheDatiDelega ID="DelegheDatiDelega1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="ButtonNonCorretta" runat="server" Text="Non corretta" OnClick="ButtonNonCorretta_Click"
                        Width="158px" />&nbsp;<asp:Button ID="ButtonNoCartaceo" runat="server" OnClick="ButtonNoCartaceo_Click"
                            Text="Non corredata da cartaceo" Width="221px" />
                    <asp:Button ID="ButtonRicevutaCE" runat="server" Text="Ricevuta CE" OnClick="ButtonRicevutaCE_Click"
                        Width="142px" />
                    <asp:Button ID="ButtonStampaEtichetta" runat="server" Text="Stampa etichetta" 
                        Width="142px" onclick="ButtonStampaEtichetta_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label><br />
    <br />
</asp:Content>
