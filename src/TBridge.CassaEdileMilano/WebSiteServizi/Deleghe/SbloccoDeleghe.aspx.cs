using System;
using System.Web.UI;
using TBridge.Cemi.Deleghe.Business;
using TBridge.Cemi.Deleghe.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class Deleghe_SbloccoDeleghe : Page
{
    private readonly DelegheBusiness biz = new DelegheBusiness();
    private Sindacalista sindacalista;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione, "SbloccoDeleghe.aspx");

        //sindacalista =
        //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista) HttpContext.Current.User.Identity).Entity;
        sindacalista =
            (Sindacalista) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        if (!Page.IsPostBack)
        {
            if (biz.ApertaFaseInserimento())
            {
                if (
                    !biz.DelegheConfermate(DateTime.Now.Month, DateTime.Now.Year, sindacalista.Sindacato,
                                           sindacalista.ComprensorioSindacale))
                {
                    PanelSblocco.Visible = true;
                    PanelConfermate.Visible = false;
                }
                else
                {
                    PanelSblocco.Visible = false;
                    PanelConfermate.Visible = true;
                }
            }
            else
            {
                PanelSblocco.Visible = false;
                PanelConfermate.Visible = true;
                LabelGiaConfermate.Text = "In questo periodo non � possibile lo sblocco delle deleghe.";
            }
        }
    }

    protected void ButtonSblocca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string codiceSblocco = TextBoxCodiceSblocco.Text.Trim().ToUpper();
            Delega delega = biz.GetDelega(codiceSblocco, sindacalista.Sindacato.Id);

            if (delega != null)
            {
                CaricaDettagliDelega(delega);
                EsisteDelega(true);
            }
            else
            {
                EsisteDelega(false);
            }
        }
    }

    private void EsisteDelega(bool esiste)
    {
        PanelDettagli.Visible = esiste;
        PanelNonPresente.Visible = !esiste;
    }

    private void CaricaDettagliDelega(Delega delega)
    {
        LabelIdDelega.Text = delega.IdDelega.ToString();
        LabelCognome.Text = delega.Lavoratore.Cognome;
        LabelNome.Text = delega.Lavoratore.Nome;
        LabelDataNascita.Text = delega.Lavoratore.DataNascita.Value.ToShortDateString();
        LabelImpresa.Text = delega.ImpresaLavoratore;
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        int idDelega = Int32.Parse(LabelIdDelega.Text);

        if (biz.SbloccaDelega(idDelega))
            Reset();
    }

    private void Reset()
    {
        PanelDettagli.Visible = false;
        PanelNonPresente.Visible = false;
        TextBoxCodiceSblocco.Text = null;

        LabelIdDelega.Text = null;
        LabelCognome.Text = null;
        LabelNome.Text = null;
        LabelDataNascita.Text = null;
        LabelImpresa.Text = null;
    }
}