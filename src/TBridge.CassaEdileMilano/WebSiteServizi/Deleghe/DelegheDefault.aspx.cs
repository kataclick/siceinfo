using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Deleghe_DelegheDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.DelegheConferma);
        funzionalita.Add(FunzionalitaPredefinite.DelegheGestione);
        funzionalita.Add(FunzionalitaPredefinite.DelegheGestioneCE);
        funzionalita.Add(FunzionalitaPredefinite.DelegheSblocco);
        funzionalita.Add(FunzionalitaPredefinite.DelegheVisioneCE);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "DelegheDefault.aspx");
    }
}