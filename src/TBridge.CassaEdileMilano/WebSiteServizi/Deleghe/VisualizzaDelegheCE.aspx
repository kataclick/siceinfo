﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="VisualizzaDelegheCE.aspx.cs" Inherits="Deleghe_VisualizzaDelegheCE"
     %>

<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/DelegheRicercaDeleghe.ascx" TagName="DelegheRicercaDeleghe"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Visualizzazione"
        titolo="Visualizzazione Deleghe" />
    <br />
    <uc3:DelegheRicercaDeleghe ID="DelegheRicercaDeleghe1" runat="server" />
    <br />
</asp:Content>
