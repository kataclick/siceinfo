using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Geocode.Business;
using TBridge.Cemi.Geocode.Type.Entities;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.Geocode.Type.Delegates;

public partial class WebControls_SelezioneIndirizzoDeleghe : UserControl
{
    private const Int32 IndicedatiIndirizzo = 0;
    private const Int32 IndiceGeocodifica = 1;
    private readonly Common _bizCommon = new Common();

    public event IndirizzoSelectedEventHandler OnIndirizzoSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        ImpostaNomeUnivocoPerValidationGroup();

        if (!Page.IsPostBack)
        {
            ImpostaNomeUnivocoPerValidationGroup();

            CaricaProvince();
        }
    }

    public void PreCaricaIndirizzo(Indirizzo indirizzo)
    {
        if (indirizzo != null)
        {
            RadTextBoxIndirizzo.Text = indirizzo.Via;
            RadTextBoxCivico.Text = indirizzo.Civico;
            RadComboBoxProvincia.SelectedValue = indirizzo.Provincia;
            //RadTextBoxProvincia.Text = indirizzo.Provincia;
            CaricaComuni();
            RadComboBoxItem itemComune = RadComboBoxComune.FindItemByText(indirizzo.Comune.ToUpper());
            if (itemComune != null)
            {
                RadComboBoxComune.SelectedIndex = itemComune.Index;
            }
            //RadTextBoxComune.Text = indirizzo.Comune;
            RadTextBoxCap.Text = indirizzo.Cap;
        }
    }

    public void CaricaProvinciaMilano()
    {
        Boolean confermato = false;
        if (ViewState["Confermato"] != null)
            confermato = (Boolean)ViewState["Confermato"];

        if (!confermato)
        {
            RadComboBoxProvincia.SelectedValue = "MI";
            CaricaComuni();
        }
    }

    private void ImpostaNomeUnivocoPerValidationGroup()
    {
        CustomValidatorIndirizzo.ValidationGroup = ID;
        CustomValidatorProvincia.ValidationGroup = ID;
        CustomValidatorComune.ValidationGroup = ID;
        CustomValidatorCap.ValidationGroup = ID;
        CustomValidatorCivico.ValidationGroup = ID;
        ValidationSummaryIndirizzo.ValidationGroup = ID;
        ButtonGeocodifica.ValidationGroup = ID;
    }

    public void CaricaProvince()
    {
        if (RadComboBoxProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(RadComboBoxProvincia);
            RadComboBoxProvincia.Items.Insert(0, new RadComboBoxItem("- Selezionare la Provincia -"));
        }
        RadAjaxManager.GetCurrent(Page).ClientEvents.OnResponseEnd = "";
    }

    private void CaricaComuni()
    {
        RadComboBoxComune.Text = null;
        RadComboBoxComune.ClearSelection();
        //RadComboBoxComune.Focus();

        Presenter.CaricaComuni(RadComboBoxComune, RadComboBoxProvincia.SelectedValue);
        RadComboBoxComune.Items.Insert(0, new RadComboBoxItem("- Selezionare il Comune -"));
    }

    public Boolean IndirizzoConfermato()
    {
        Boolean ret = false;

        if (ViewState["Confermato"] != null)
        {
            if ((Boolean) ViewState["Confermato"]) ret = true;
        }

        return ret;
    }

    protected void ButtonGeocodifica_Click(object sender, EventArgs e)
    {
        Page.Validate("geocodifica");

        if (Page.IsValid)
        {
			LabelSummary.Visible = false;
            LabelNoGeocodifica.Visible = false;

            if (ButtonGeocodifica.Text == "Geocodifica")
            {
                Indirizzo indirizzo = CreaIndirizzo();

                List<Indirizzo> indirizzi = GeocodeProvider.Geocode(indirizzo.IndirizzoCompletoGeocode);

                ViewState["IndirizziGeocodificati"] = indirizzi;
                Presenter.CaricaElementiInGridView(RadGridIndirizzi, indirizzi);

                if (indirizzi != null)
                {
                    if (indirizzi.Count > 0)
                    {
                        RadMultiPageSceltaIndirizzo.SelectedIndex = IndiceGeocodifica;
                    }
                    else
                    {
                        ConfermaIndirizzo(false, true);
                    }
                }
                else
                {
                    ConfermaIndirizzo(false, true);
                }
            }
            else
            {
                if (ButtonGeocodifica.Text == "Modifica")
                {
                    ViewState["Confermato"] = null;
                    ButtonGeocodifica.Text = "Geocodifica";
                    ButtonGeocodifica.CausesValidation = true;
                    //Mappa.Visible = false;

                    AbilitaIndirizzo();
                }
            }
        }
		else
		{
			LabelSummary.Text = "Per inserire la residenza, compilare tutti i campi: indirizzo, civico, comeune, provincia e CAP";
			LabelSummary.Visible = true;
		}
    }

    private Indirizzo CreaIndirizzo()
    {
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.NomeVia = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text);
        indirizzo.Civico = Presenter.NormalizzaCampoTesto(RadTextBoxCivico.Text);
        indirizzo.Cap = Presenter.NormalizzaCampoTesto(RadTextBoxCap.Text);
        if (RadComboBoxComune.SelectedIndex > 0)
            indirizzo.Comune = Presenter.NormalizzaCampoTesto(RadComboBoxComune.SelectedItem.Text);
        else
            indirizzo.Comune = null;
        indirizzo.Provincia = Presenter.NormalizzaCampoTesto(RadComboBoxProvincia.SelectedValue);
        //indirizzo.Comune = Presenter.NormalizzaCampoTesto(RadTextBoxComune.Text);
        //indirizzo.Provincia = Presenter.NormalizzaCampoTesto(RadTextBoxProvincia.Text);

        return indirizzo;
    }

    protected void RadComboBoxProvincia_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (RadComboBoxProvincia.SelectedItem != null)
        {
            CaricaComuni();

            StringBuilder script = new StringBuilder();
            script.Append("Sys.Application.add_load(function(){");
            script.Append("var combo2 = $find('" + RadComboBoxComune.ClientID + "');");
            script.Append("var enabled = $find('" + RadComboBoxComune.Enabled + "');");
            script.Append("if(combo2!=null && combo2.get_enabled()){combo2.get_inputDomElement().focus();");
            script.Append("combo2.selectText(0, combo2.get_text().length - 1);} });");

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectText", script.ToString(), true);
        }
    }


    protected void ButtonConfermaIndirizzo_Click(object sender, EventArgs e)
    {
        Page.Validate("confermaIndirizzo");

        if (Page.IsValid)
        {
            if (RadGridIndirizzi.SelectedIndexes.Count == 1)
            {
                LabelValidationIndirizzo.Visible = false;


                List<Indirizzo> indirizzi = (List<Indirizzo>) ViewState["IndirizziGeocodificati"];


                CaricaIndirizzo(indirizzi[Int32.Parse(RadGridIndirizzi.SelectedIndexes[0])], false);
            }

            if (RadGridIndirizzi.SelectedIndexes.Count == 0)
            {
                LabelValidationIndirizzo.Visible = true;
            }
        }
    }

    protected void ButtonMantieni_Click(object sender, EventArgs e)
    {
        ConfermaIndirizzo(true, true);
    }

    protected void RadGridIndirizzi_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem) e.Item;
            ImageButton buttonVisualizzaMappa = (ImageButton) item["ViewMapColumn"].Controls[0];

            string comando = String.Format(
                "openRadWindow('{0}','{1}','{2}','{3}','{4}','{5}','{6}'); return false;",
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["Latitudine"],
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["Longitudine"],
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["Via"],
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["Cap"],
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["Comune"],
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["Provincia"],
                item.OwnerTableView.DataKeyValues[item.ItemIndex]["InfoAggiuntiva"]
                );

            buttonVisualizzaMappa.Attributes.Add("OnClick", comando);
        }
    }

    private void CaricaIndirizzo(Indirizzo indirizzo, Boolean conCivico)
    {
        RadTextBoxIndirizzo.Text = indirizzo.Via;
        if (conCivico)
        {
            RadTextBoxCivico.Text = indirizzo.Civico;
        }
        RadTextBoxCap.Text = indirizzo.Cap;

        //Cerco la provincia e il comune nelle combo box, se non le trovo lascio la selezione com'era
        String provinciaSelezionata = RadComboBoxProvincia.SelectedValue;
        String codiceComuneSelezionato = RadComboBoxComune.SelectedItem.Value;

        //if (!String.IsNullOrEmpty(indirizzo.Provincia))
        //{
        //    RadTextBoxProvincia.Text = indirizzo.Provincia;
        //}
        //if (!String.IsNullOrEmpty(indirizzo.Comune))
        //{
        //    RadTextBoxComune.Text = indirizzo.Comune;
        //}

        ViewState["Latitudine"] = indirizzo.Latitudine;
        ViewState["Longitudine"] = indirizzo.Longitudine;

        const string provinciaLodi = "LO";
        const string denominazioneProvinciaLodi = "LODI";
        const string provinciaMilano = "MI";
        const string denominazioneProvinciaMilano = "MILANO";
        const string provinciaMonzaBrianza = "MB";
        const string denominazioneProvinciaMonzaBrianza = "MONZA BRIANZA";

        ////LODI
        if (indirizzo.Provincia.ToUpper() == denominazioneProvinciaLodi)
        {
            if (RadComboBoxProvincia.SelectedValue != provinciaLodi)
            {
                RadComboBoxProvincia.SelectedValue = provinciaLodi;
                CaricaComuni();
            }
        }

        ////MILANO
        if (indirizzo.Provincia.ToUpper() == denominazioneProvinciaMilano)
        {
            if (RadComboBoxProvincia.SelectedValue != provinciaMilano)
            {
                RadComboBoxProvincia.SelectedValue = provinciaMilano;
                CaricaComuni();
            }
        }

        ////MONZA BRIANZA
        if (indirizzo.Provincia.ToUpper() == denominazioneProvinciaMonzaBrianza)
        {
            if (RadComboBoxProvincia.SelectedValue != provinciaMonzaBrianza)
            {
                RadComboBoxProvincia.SelectedValue = provinciaMonzaBrianza;
                CaricaComuni();
            }
        }

        if (RadComboBoxComune.SelectedItem.Text != indirizzo.Comune)
        {
            RadComboBoxItem itemComune = RadComboBoxComune.FindItemByText(indirizzo.Comune.ToUpper());

            if (itemComune != null)
            {
                RadComboBoxComune.SelectedIndex = itemComune.Index;
            }
            else
            {
                // Ripristino situazione iniziale
                RadComboBoxProvincia.SelectedValue = provinciaSelezionata;
                CaricaComuni();
                RadComboBoxComune.SelectedValue = codiceComuneSelezionato;
            }
        }

        ConfermaIndirizzo(true, true);
    }

    private void AbilitaIndirizzo()
    {
        LabelNoGeocodifica.Visible = false;

        RadTextBoxIndirizzo.Enabled = true;
        RadTextBoxCivico.Enabled = true;
        RadComboBoxProvincia.Enabled = true;
        RadComboBoxComune.Enabled = true;
        //RadTextBoxProvincia.Enabled = true;
        //RadTextBoxComune.Enabled = true;
        RadTextBoxCap.Enabled = true;
    }

    public void DisabilitaIndirizzo()
    {
        RadTextBoxIndirizzo.Enabled = false;
        RadTextBoxCivico.Enabled = false;
        RadComboBoxProvincia.Enabled = false;
        RadComboBoxComune.Enabled = false;
        //RadTextBoxProvincia.Enabled = false;
        //RadTextBoxComune.Enabled = false;
        RadTextBoxCap.Enabled = false;
    }

    private void ConfermaIndirizzo(Boolean confermaUtente, Boolean modificabile)
    {
        RadMultiPageSceltaIndirizzo.SelectedIndex = IndicedatiIndirizzo;

        RadTextBoxIndirizzo.Enabled = false;
        RadTextBoxCivico.Enabled = false;
        RadComboBoxProvincia.Enabled = false;
        RadComboBoxComune.Enabled = false;
        //RadTextBoxProvincia.Enabled = false;
        //RadTextBoxComune.Enabled = false;
        RadTextBoxCap.Enabled = false;

        ButtonGeocodifica.Text = "Modifica";
        ButtonGeocodifica.CausesValidation = false;

        LabelNoGeocodifica.Visible = !confermaUtente;
        ButtonGeocodifica.Visible = modificabile;
        PanelIndirizzo.Enabled = modificabile;

        ViewState["Confermato"] = true;

        Indirizzo indirizzoConfermato = GetIndirizzo();
        if (OnIndirizzoSelected != null)
        {
            OnIndirizzoSelected(indirizzoConfermato);
        }
    }

    protected void CustomValidatorIndirizzoConfermato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (ViewState["Confermato"] == null)
        {
            args.IsValid = false;
        }
    }

    public void ImpostaValidationGroup(String validationGroup)
    {
        AbilitaValidationGroup(true);
        //CustomValidatorIndirizzoConfermato.ValidationGroup = validationGroup;
    }

    public void AbilitaValidationGroup(bool abilita)
    {
        CustomValidatorIndirizzo.Enabled = abilita;
        CustomValidatorCivico.Enabled = abilita;
        CustomValidatorProvincia.Enabled = abilita;
        CustomValidatorComune.Enabled = abilita;
        CustomValidatorCap.Enabled = abilita;
    }

    public void CaricaDatiIndirizzo(Indirizzo indirizzo)
    {
        if (indirizzo != null)
        {
            RadTextBoxIndirizzo.Text = indirizzo.NomeVia;
            RadTextBoxCivico.Text = indirizzo.Civico;
            RadComboBoxProvincia.SelectedValue = indirizzo.Provincia;
            CaricaComuni();
            if (indirizzo.Provincia != null)
            { 
                TBridge.Cemi.Type.Collections.ComuneSiceNewCollection ComuniTemp = _bizCommon.GetComuniSiceNew(indirizzo.Provincia);
                foreach (TBridge.Cemi.Type.Entities.ComuneSiceNew c in ComuniTemp)
                {
                    if (c.Comune.ToUpper() == indirizzo.Comune.ToUpper())
                    {
                        RadComboBoxComune.SelectedValue = c.CodiceCatastale.ToUpper();
                    }
                }
            }
            //RadComboBoxComune.SelectedValue = indirizzo.ComuneCodiceCatastale;

            //RadTextBoxProvincia.Text = indirizzo.Provincia;
            //RadTextBoxComune.Text = indirizzo.Comune;
            RadTextBoxCap.Text = indirizzo.Cap;

            ViewState["Latitudine"] = indirizzo.Latitudine;
            ViewState["Longitudine"] = indirizzo.Longitudine;

            ConfermaIndirizzo(true, true);
        }
    }

    public void Reset()
    {
        AbilitaIndirizzo();

        Presenter.SvuotaCampo(RadTextBoxIndirizzo);
        Presenter.SvuotaCampo(RadTextBoxCivico);
        RadComboBoxProvincia.Text = null;
        RadComboBoxProvincia.ClearSelection();
        RadComboBoxComune.Text = null;
        RadComboBoxComune.ClearSelection();
        CaricaComuni();
        //Presenter.SvuotaCampo(RadTextBoxProvincia);
        //Presenter.SvuotaCampo(RadTextBoxComune);
        Presenter.SvuotaCampo(RadTextBoxCap);

        ViewState["Latitudine"] = null;
        ViewState["Longitudine"] = null;

        ButtonGeocodifica.Text = "Geocodifica";
        ButtonGeocodifica.CausesValidation = true;
    }

    public Indirizzo GetIndirizzo()
    {
        Indirizzo indirizzo = new Indirizzo();

        indirizzo.NomeVia = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text.Trim());
        if (!string.IsNullOrEmpty(RadTextBoxCivico.Text.Trim()))
            indirizzo.Civico = Presenter.NormalizzaCampoTesto(RadTextBoxCivico.Text.Trim());
        indirizzo.Provincia = RadComboBoxProvincia.SelectedValue;
        indirizzo.ComuneCodiceCatastale = RadComboBoxComune.SelectedValue;

        if (RadComboBoxComune.SelectedIndex > 0)
            indirizzo.Comune = RadComboBoxComune.Text;
        else
            indirizzo.Comune = null;
        //indirizzo.Provincia = Presenter.NormalizzaCampoTesto(RadTextBoxProvincia.Text);
        //indirizzo.Comune = Presenter.NormalizzaCampoTesto(RadTextBoxComune.Text);
        if (!string.IsNullOrEmpty(RadTextBoxCap.Text.Trim()))
            indirizzo.Cap = RadTextBoxCap.Text.Trim();
        indirizzo.Latitudine = ViewState["Latitudine"] as decimal?;
        indirizzo.Longitudine = ViewState["Longitudine"] as decimal?;

        return indirizzo;
    }

    public bool CheckCompilazione()
    {
        bool ok = false;
        int sumChk = 0;

        if(!String.IsNullOrEmpty(RadTextBoxIndirizzo.Text))
            sumChk = sumChk + 1;
        if (!String.IsNullOrEmpty(RadTextBoxCivico.Text))
            sumChk = sumChk + 1;
        if (RadComboBoxProvincia.SelectedIndex > 0) 
        //if (String.IsNullOrEmpty(RadTextBoxProvincia.Text))
            sumChk = sumChk + 1;
        if (RadComboBoxComune.SelectedIndex > 0)
        //if (String.IsNullOrEmpty(RadTextBoxComune.Text))
            sumChk = sumChk + 1;
        int iCAP;
        //String cap = RadTextBoxCap.Text;
        if (!String.IsNullOrEmpty(RadTextBoxCap.Text) && RadTextBoxCap.Text.Length == 5 && Int32.TryParse(RadTextBoxCap.Text, out iCAP))
            sumChk = sumChk + 1;

        if (sumChk == 0 || sumChk == 5)
            ok = true;

        return ok;
    }



    #region Custom Validators

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (!String.IsNullOrEmpty(RadTextBoxIndirizzo.Text))
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorCivico_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (!String.IsNullOrEmpty(RadTextBoxCivico.Text))
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorProvincia_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        //if (!String.IsNullOrEmpty(RadTextBoxProvincia.Text))
        if (RadComboBoxProvincia.SelectedIndex > 0)        
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorComune_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        //if (!String.IsNullOrEmpty(RadTextBoxComune.Text))
        if (RadComboBoxComune.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorCAP_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            args.IsValid = false;
            CustomValidatorCap.ErrorMessage = "CAP non valido";
            int iCAP;
            String cap = RadTextBoxCap.Text;

            if (cap.Length == 5 && Int32.TryParse(cap, out iCAP))
            {
                args.IsValid = true;
            }
        }
        catch
        {
        }
    }


    #endregion
}