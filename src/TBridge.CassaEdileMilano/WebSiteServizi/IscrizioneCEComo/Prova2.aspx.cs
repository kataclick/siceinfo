using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.IscrizioneCE.Business;

public partial class IscrizioneCEComo_Prova2 : System.Web.UI.Page
{
    private IscrizioniManager biz = new IscrizioniManager();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void LinkButtonPdf_Click(object sender, EventArgs e)
    {
        Int32 idDomanda = (Int32)Session["IdDomanda"];
        //Int32 idDomanda = 3021;

        byte[] moduli = biz.GetModuloDomanda(idDomanda);
        Response.ClearContent();
        Response.AppendHeader("content-disposition", "attachment;filename=moduli.pdf");
        Response.ContentType = "application/pdf";
        Response.BinaryWrite(moduli);
        Response.Flush();
        Response.End();
    }
}
