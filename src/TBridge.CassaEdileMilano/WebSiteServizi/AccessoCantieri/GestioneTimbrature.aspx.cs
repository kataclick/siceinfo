﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using System.Collections.Generic;

public partial class AccessoCantieri_GestioneTimbrature : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                                                             FunzionalitaPredefinite.AccessoCantieriPerCommittente
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}