﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicercaCantieri.ascx.cs"
    Inherits="AccessoCantieri_WebControls_RicercaCantieri" %>

<asp:Panel ID="PanelRicercaCantieri" runat="server" DefaultButton="ButtonVisualizza" Width="100%">
<table class="standardTable">
    <tr>
        <td>
            Indirizzo cantiere
        </td>
        <td>
            Comune cantiere
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
        </td>
        <td align="right" colspan="3">
            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaCantieri"
                OnClick="ButtonVisualizza_Click" />
        </td>
    </tr>
</table>
<br />
<telerik:RadGrid ID="RadGridCantieri" runat="server" AllowPaging="True" PageSize="5"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" OnItemCommand="RadGridCantieri_ItemCommand"
    OnItemDataBound="RadGridCantieri_ItemDataBound" OnPageIndexChanged="RadGridCantieri_PageIndexChanged" Visible="false">
    <MasterTableView DataKeyNames="idWhiteList,indirizzo,comune,provincia">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridTemplateColumn FilterControlAltText="Filter Cantiere column" UniqueName="Cantiere">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td class="accessoCantieriTemplateTableTd">
                                Indirizzo:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Comune:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelComune" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Provincia:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn>
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td align="center">
                                <asp:ImageButton
                                    ID="ImageButtonSeleziona"
                                    runat="server"
                                    CommandName="SELEZIONA"
                                    ImageUrl="~/images/lente.png"
                                    ToolTip="Seleziona" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <small>
                                    Seleziona
                                </small>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </telerik:GridTemplateColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
</asp:Panel>
