﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

public partial class AccessoCantieri_WebControls_RicercaLavoratore : System.Web.UI.UserControl
{
    private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CantiereSelezionato(Int32 idCantiere)
    {
        ViewState["IdCantiere"] = idCantiere;
        Presenter.SvuotaCampo(TextBoxCognome);
        Presenter.SvuotaCampo(TextBoxNome);
        Presenter.SvuotaCampo(TextBoxCodiceFiscale);
        RadGridLavoratori.Visible = false;
    }

    protected void ButtonVisualizza_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaLavoratori(0);
            RadGridLavoratori.Visible = true;
        }
    }

    private LavoratoreFilter CreaFiltro()
    {
        LavoratoreFilter filtro = new LavoratoreFilter();

        filtro.IdCantiere = (Int32) ViewState["IdCantiere"];
        filtro.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        filtro.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        filtro.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);

        return filtro;
    }

    private void CaricaLavoratori(Int32 pagina)
    {
        LavoratoreFilter filtro = CreaFiltro();
        LavoratoreCollection lavoratori = biz.GetLavoratoriRicerca(filtro);

        Presenter.CaricaElementiInGridView(
            RadGridLavoratori,
            lavoratori);
    }

    protected void CustomValidatorIdCantiere_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["IdCantiere"] == null)
        {
            args.IsValid = false;
        }
    }

    protected void RadGridLavoratori_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Lavoratore lav = (Lavoratore) e.Item.DataItem;

            Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label lCodiceFiscale = (Label) e.Item.FindControl("LabelCodiceFiscale");
            Label lDataNascita = (Label) e.Item.FindControl("LabelDataNascita");
            Label lRagioneSociale = (Label) e.Item.FindControl("LabelRagioneSociale");
            Label lPartitaIva = (Label) e.Item.FindControl("LabelPartitaIva");
            Label lImpCodiceFiscale = (Label) e.Item.FindControl("LabelImpCodiceFiscale");
            Label lDataInizioAttivita = (Label) e.Item.FindControl("LabelDataInizioAttivita");
            Label lDataFineAttivita = (Label) e.Item.FindControl("LabelDataFineAttivita");

            lLavoratore.Text = lav.TipoLavoratore == TipologiaLavoratore.SiceNew ? String.Format("{0} {1} {2}", lav.IdLavoratore, lav.Cognome, lav.Nome) : String.Format("{0} {1}", lav.Cognome, lav.Nome);
            lCodiceFiscale.Text = lav.CodiceFiscale;
            if (lav.DataNascita.HasValue)
            {
                lDataNascita.Text = lav.DataNascita.Value.ToString("dd/MM/yyyy");
            }

            lRagioneSociale.Text = lav.Impresa.TipoImpresa == TipologiaImpresa.SiceNew ? String.Format("{0} {1}", lav.Impresa.IdImpresa, lav.Impresa.RagioneSociale) : lav.Impresa.RagioneSociale;
            lPartitaIva.Text = lav.Impresa.PartitaIva;
            lImpCodiceFiscale.Text = lav.Impresa.CodiceFiscale;
            if (lav.DataInizioAttivita.HasValue)
            {
                lDataInizioAttivita.Text = lav.DataInizioAttivita.Value.ToString("dd/MM/yyyy");
            }
            if (lav.DataFineAttivita.HasValue)
            {
                lDataFineAttivita.Text = lav.DataFineAttivita.Value.ToString("dd/MM/yyyy");
            }
        }
    }

    protected void RadGridLavoratori_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaLavoratori(e.NewPageIndex);
    }
}