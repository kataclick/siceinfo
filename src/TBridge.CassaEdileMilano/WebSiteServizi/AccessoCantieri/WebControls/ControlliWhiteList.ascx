﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ControlliWhiteList.ascx.cs"
    Inherits="AccessoCantieri_WebControls_ControlliWhiteList" %>
<table class="borderedTable">
    <tr>
        <td>
            Impresa
        </td>
        <td>
            Codice fiscale
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:DropDownList ID="DropDownListImpresa" runat="server" AppendDataBoundItems="True"
                AutoPostBack="False" Width="150px">
            </asp:DropDownList>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:ValidationSummary ID="ValidationSummaryWhiteList" runat="server" ValidationGroup="whiteList"
                CssClass="messaggiErrore" />
        </td>
        <td align="right">
            <asp:Button ID="ButtonVisualizzaTimb" runat="server" OnClick="ButtonVisualizzaTimb_Click"
                Text="Ricerca" ValidationGroup="whiteList" />
        </td>
    </tr>
</table>
<br />
<asp:GridView ID="GridViewTimbrature" runat="server" AutoGenerateColumns="False"
    Width="100%" OnRowDataBound="GridViewTimbrature_RowDataBound" AllowSorting="True"
    AllowPaging="True" OnPageIndexChanging="GridViewTimbrature_PageIndexChanging">
    <Columns>
        <asp:TemplateField HeaderText="Impresa">
            <HeaderStyle Width="200" />
            <ItemStyle Width="200" />
            <ItemTemplate>
                <table class="standardTable">
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Cod. C.E.:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelIdImpresa" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Rag.Soc.:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelImpresaRagioneSociale" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            P.IVA/Cod.Fisc.:
                        </td>
                        <td>
                            <asp:Label ID="LabelImpresaIvaCodFisc" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Stato attuale:
                        </td>
                        <td>
                            <asp:Label ID="LabelStatoImpresa" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Regolarità al
                            <asp:Label ID="LabelImpresaDebitiAl" runat="server"></asp:Label>:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelImpresaDebiti" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Ultima Den.:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelImpresaUltimaDenuncia" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
        <%-- <asp:TemplateField HeaderText="Controllo Impresa">
                            <ItemTemplate>
                                <asp:Label ID="LabelControlloImpresa" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
        <asp:TemplateField HeaderText="Lavoratore">
            <ItemTemplate>
                <headerstyle width="200" />
                <itemstyle width="200" />
                <table class="standardTable">
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Cod. C.E.:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelIdLavoratore" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Nome:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelLavoratoreCognomeNome" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Codice fiscale:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Nato il:
                        </td>
                        <td>
                            <asp:Label ID="LabelLavoratoreDataNascita" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Periodo:
                        </td>
                        <td>
                            <asp:Label ID="LabelLavoratorePeriodo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Rapp.Lav.:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelLavoratoreRapportoLavoro" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="accessoCantieriTemplateTableWhiteListTd">
                            Ultima Den.:
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelLavoratoreUltimaDenuncia" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Controllo Casse Edili Lombarde">
            <ItemTemplate>
                <table class="standardTable">
                    <tr>
                        <td class="accessoCantieriTemplateTableTd">
                            Ultima Denuncia:
                        </td>
                        <td>
                            <asp:Label ID="LabelUltimaDenuncia" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            C.E.:
                        </td>
                        <td>
                            <asp:Label ID="LabelCE" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        Nessun dato presente
    </EmptyDataTemplate>
    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
</asp:GridView>
