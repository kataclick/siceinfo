﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Presenter;

public partial class AccessoCantieri_WebControls_ControlliWhiteList : UserControl
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    private Int32 IdWhiteList
    {
        get
        {
            if (ViewState["IdWhiteList"] != null)
            {
                return (Int32) ViewState["IdWhiteList"];
            }
            throw new Exception("IdWhiteList non impostata");
        }

        set { ViewState["IdWhiteList"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaWhiteList(WhiteList whiteList)
    {
        // Caricamento filtro imprese
        ImpresaCollection imprese = new ImpresaCollection();
        foreach (Subappalto sub in whiteList.Subappalti)
        {
            if (sub.Appaltante != null)
                if (!imprese.Contains(sub.Appaltante))
                    imprese.Add(sub.Appaltante);

            if (sub.Appaltata != null)
                if (!imprese.Contains(sub.Appaltata))
                    imprese.Add(sub.Appaltata);
        }

        Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListImpresa, imprese,
                                                           "RagioneSociale", "idImpresa");

        if (whiteList.IdWhiteList != null) IdWhiteList = whiteList.IdWhiteList.Value;
        CaricaLavoratoriWhiteList(0);
    }

    public void Reset()
    {
        DropDownListImpresa.Items.Clear();
        Presenter.CaricaElementiInGridView(
            GridViewTimbrature,
            null,
            0);
    }

    protected void ButtonVisualizzaTimb_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaLavoratoriWhiteList(0);
        }
    }

    private void CaricaLavoratoriWhiteList(Int32 pagina)
    {
        ControlloIdentitaFilter filtro = CreaFiltro();
        ControlloWhiteListCollection controlli = _biz.GetControlliWhiteList(filtro);

        Presenter.CaricaElementiInGridView(
            GridViewTimbrature,
            controlli,
            pagina);
    }

    private ControlloIdentitaFilter CreaFiltro()
    {
        ControlloIdentitaFilter filtro = new ControlloIdentitaFilter
                                             {
                                                 IdCantiere = IdWhiteList,
                                                 CodiceFiscale =
                                                     Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text)
                                             };

        if (DropDownListImpresa.SelectedItem.ToString() != string.Empty)
            filtro.IdImpresa = Int32.Parse(DropDownListImpresa.SelectedValue);

        return filtro;
    }

    protected void GridViewTimbrature_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaLavoratoriWhiteList(e.NewPageIndex);
    }

    protected void GridViewTimbrature_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ControlloWhiteList controllo = (ControlloWhiteList) e.Row.DataItem;

            Label lIdLavoratore = (Label) e.Row.FindControl("LabelIdLavoratore");
            Label lLavoratoreCognomeNome = (Label) e.Row.FindControl("LabelLavoratoreCognomeNome");
            Label lLavoratoreCodiceFiscale = (Label) e.Row.FindControl("LabelLavoratoreCodiceFiscale");
            Label lLavoratoreDataNascita = (Label) e.Row.FindControl("LabelLavoratoreDataNascita");
            Label lLavoratoreUltimaDenuncia = (Label) e.Row.FindControl("LabelLavoratoreUltimaDenuncia");
            Label lLavoratoreRapportoLavoro = (Label) e.Row.FindControl("LabelLavoratoreRapportoLavoro");
            Label lLavoratorePeriodo = (Label) e.Row.FindControl("LabelLavoratorePeriodo");

            Label lIdImpresa = (Label) e.Row.FindControl("LabelIdImpresa");
            Label lImpresaRagioneSociale = (Label) e.Row.FindControl("LabelImpresaRagioneSociale");
            Label lImpresaIvaCodFisc = (Label) e.Row.FindControl("LabelImpresaIvaCodFisc");
            Label labelStatoImpresa = (Label) e.Row.FindControl("LabelStatoImpresa");

            Label lImpresaDebiti = (Label) e.Row.FindControl("LabelImpresaDebiti");
            Label lImpresaUltimaDenuncia = (Label) e.Row.FindControl("LabelImpresaUltimaDenuncia");
            Label lImpresaDebitiAl = (Label) e.Row.FindControl("LabelImpresaDebitiAl");

            lIdLavoratore.Text = controllo.Lavoratore.IdLavoratore.ToString();
            lLavoratoreCognomeNome.Text = String.Format("{0} {1}", controllo.Lavoratore.Cognome,
                                                        controllo.Lavoratore.Nome);
            lLavoratoreCodiceFiscale.Text = controllo.Lavoratore.CodiceFiscale;
            if (controllo.Lavoratore.DataNascita.HasValue)
            {
                lLavoratoreDataNascita.Text = controllo.Lavoratore.DataNascita.Value.ToString("dd/MM/yyyy");
            }
            if (controllo.Lavoratore.IdLavoratore.HasValue)
            {
                // Ultima denuncia trovata lavoratore impresa
                if (controllo.DataUltimaDenunciaLavoratore.HasValue)
                {
                    lLavoratoreUltimaDenuncia.Text = controllo.DataUltimaDenunciaLavoratore.Value.ToString("MM/yyyy");
                }
                else
                {
                    lLavoratoreUltimaDenuncia.Text = "Non trovata";
                    lLavoratoreUltimaDenuncia.ForeColor = Color.Red;
                }
                // Rapporto lavoratore impresa
                if (controllo.RapportoImpresaLavoratore)
                {
                    lLavoratoreRapportoLavoro.Text = "OK";
                    lLavoratoreRapportoLavoro.ForeColor = Color.Green;
                }
                else
                {
                    lLavoratoreRapportoLavoro.Text = "KO";
                    lLavoratoreRapportoLavoro.ForeColor = Color.Red;
                }
            }
            else
            {
                lLavoratoreUltimaDenuncia.ForeColor = Color.Red;
                lLavoratoreRapportoLavoro.ForeColor = Color.Red;
                lLavoratoreUltimaDenuncia.Text = "--";
                lLavoratoreRapportoLavoro.Text = "--";
            }
            // Periodo
            if (controllo.Lavoratore.DataInizioAttivita.HasValue && controllo.Lavoratore.DataFineAttivita.HasValue)
            {
                lLavoratorePeriodo.Text = String.Format("{0} - {1}",
                                                        controllo.Lavoratore.DataInizioAttivita.Value.ToShortDateString(),
                                                        controllo.Lavoratore.DataFineAttivita.Value.ToShortDateString());
            }

            lIdImpresa.Text = controllo.Impresa.IdImpresa.ToString();
            lImpresaRagioneSociale.Text = controllo.Impresa.RagioneSociale;
            if (String.IsNullOrEmpty(controllo.Impresa.CodiceFiscale))
            {
                lImpresaIvaCodFisc.Text = controllo.Impresa.PartitaIva;
            }
            else
            {
                lImpresaIvaCodFisc.Text = controllo.Impresa.CodiceFiscale;
            }

            string stato = controllo.Impresa.Stato;
            DateTime? dataSospensione = controllo.Impresa.DataSospensione;
            DateTime? dataCessazione = controllo.Impresa.DataCessazione;
            labelStatoImpresa.Text = stato;
            if (stato == "SOSPESA" || stato == "SOSP.UFF.")
            {
                labelStatoImpresa.ForeColor = Color.Red;
                if (dataSospensione != null)
                    labelStatoImpresa.Text = String.Format("{0} dal {1}", stato,
                                                           dataSospensione.Value.ToShortDateString());
            }
            else if (stato == "CESSATA")
            {
                labelStatoImpresa.ForeColor = Color.Red;
                if (dataCessazione != null)
                    labelStatoImpresa.Text = String.Format("{0} dal {1}", stato,
                                                           dataCessazione.Value.ToShortDateString());
            }
            else if (stato == "ATTIVA")
            {
                labelStatoImpresa.ForeColor = Color.Black;
            }
            else
            {
                labelStatoImpresa.ForeColor = Color.Red;
                labelStatoImpresa.Text = "--";
            }

            DateTime dataDaConsiderare = AccessoCantieriBusiness.GetDataDaConsiderarePerControlli(DateTime.Now);
            lImpresaDebitiAl.Text = dataDaConsiderare.ToString("MM/yyyy");

            if (controllo.Impresa.IdImpresa.HasValue)
            {
                // Debiti
                if (_biz.ControlloDebiti(controllo.Impresa.IdImpresa.Value, dataDaConsiderare.Year,
                                         dataDaConsiderare.Month))
                {
                    lImpresaDebiti.ForeColor = Color.Green;
                    lImpresaDebiti.Text = "OK";
                }
                else
                {
                    lImpresaDebiti.ForeColor = Color.Red;
                    lImpresaDebiti.Text = "KO";
                }

                // Ultima denuncia trovata
                if (controllo.DataUltimaDenuncia.HasValue)
                {
                    lImpresaUltimaDenuncia.Text = controllo.DataUltimaDenuncia.Value.ToString("MM/yyyy");
                }
                else
                {
                    lImpresaUltimaDenuncia.Text = "Non trovata";
                    lImpresaUltimaDenuncia.ForeColor = Color.Red;
                }
            }
            else
            {
                lImpresaDebiti.ForeColor = Color.Red;
                lImpresaUltimaDenuncia.ForeColor = Color.Red;
                lImpresaDebiti.Text = "--";
                lImpresaUltimaDenuncia.Text = "--";
            }
        }
    }
}