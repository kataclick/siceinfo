﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiAggiuntiviLavoratore.ascx.cs"
    Inherits="AccessoCantieri_WebControls_DatiAggiuntiviLavoratore" %>
<table class="borderedTable">
    <tr>
        <td colspan="3">
            <b>Informazioni di Base </b>
        </td>
    </tr>
    <tr>
        <td>
            Cognome<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" MaxLength="255" Width="250px">
            </telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognome" runat="server" ControlToValidate="RadTextBoxCognome"
                ErrorMessage="Digitare un cognome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNome" runat="server" MaxLength="50" Width="250px">
            </telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server" ControlToValidate="RadTextBoxNome"
                ErrorMessage="Digitare un nome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita (gg/mm/aaaa)<b>*</b>:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDateDataNascita" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataNascita" runat="server"
                ControlToValidate="RadDateDataNascita" ErrorMessage="Digitare una data di nascita"
                ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale<b>*</b>:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" MaxLength="16" Width="250px">
            </telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                ControlToValidate="RadTextBoxCodiceFiscale" ErrorMessage="Digitare un codice fiscale"
                ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodiceFiscale" runat="server"
                ControlToValidate="RadTextBoxCodiceFiscale" ErrorMessage="Formato codice fiscale errato"
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$"
                ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>--%>
           <%-- <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="RadTextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non coerente con la data di nascita" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate"
                ValidationGroup="lavoratore">*</asp:CustomValidator>--%>
           <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="RadTextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non coerente con i dati inseriti" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate"
                ValidationGroup="lavoratore">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Includi nei controlli di regolarità<b>*</b>:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxControlli" runat="server" Checked="true" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <b>Periodo di attività </b>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <small>Se la date di inizio e fine attività non vengono specificate si intendono valide
                quelle di inizio e fine lavori </small>
        </td>
    </tr>
    <tr>
        <td>
            Data Inizio attività (gg/mm/aaaa):
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDateDataInizioAttivita" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorDateDelCantiere" runat="server" ControlToValidate="RadDateDataInizioAttivita"
                ErrorMessage="Periodo non corente con le date del cantiere." OnServerValidate="CustomValidatorDateDelCantiere_ServerValidate"
                ValidationGroup="lavoratore">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data fine attività (gg/mm/aaaa):
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDateDataFineAttivita" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToCompare="RadDateDataFineAttivita"
                ControlToValidate="RadDateDataInizioAttivita" ErrorMessage="La data di inizio dell'attività non può essere superiore a quella di fine."
                Operator="LessThan" Type="Date" ValidationGroup="lavoratore">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <b>Per la stampa del badge </b><small>(in mancanza di uno dei seguenti dati non sarà
                possibile stampare il badge) </small>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <small>
                <asp:Label ID="LabelModificaNascita" runat="server" Text="Per modificare il paese, la provincia o il comune di nascita contattare Cassa Edile"
                    Visible="false">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr id="trPaeseNascita1" runat="server">
        <td>
            Paese di nascita:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListPaeseNascita" runat="server" AppendDataBoundItems="True"
                OnSelectedIndexChanged="DropDownListPaeseNascita_SelectedIndexChanged" AutoPostBack="True"
                Width="250px" />
        </td>
        <td>
        </td>
    </tr>
    <tr id="trPaeseNascita2" runat="server">
        <td colspan="3">
            <small>Se viene selezionato &quot;Italia&quot; occorre indicare provincia e comune di
                nascita</small>
        </td>
    </tr>
    <tr id="trProvinciaNascita" runat="server">
        <td>
            Provincia di nascita:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListProvinciaNascita" runat="server" AppendDataBoundItems="True"
                Enabled="false" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvinciaNascita_SelectedIndexChanged" />
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorItaliano" runat="server" ValidationGroup="lavoratore"
                ErrorMessage="Devono essere selezionate la provincia ed il comune di nascita"
                OnServerValidate="CustomValidatorItaliano_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr id="trLuogoNascita" runat="server">
        <td>
            Luogo di nascita:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListComuneNascita" runat="server" AppendDataBoundItems="True"
                Width="250px" Enabled="false" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Data assunzione (gg/mm/aaaa):
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDateDataAssunzione" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Fotografia (formato JPG, max 100kB):
        </td>
        <td>
            <telerik:RadUpload ID="RadUploadFoto" AllowedFileExtensions=".jpg" OnClientFileSelected="OnClientFileSelectedHandlerModifica"
                runat="server" MaxFileInputsCount="1" MaxFileSize="102400" ControlObjectsVisibility="None"
                Width="200px">
                <Localization Add="Aggiungi foto" Select="Seleziona" />
            </telerik:RadUpload>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorFoto" runat="server" ErrorMessage="Possono essere importati solo file con estensione .jpg e con dimensione inferiore a 100kB"
                OnServerValidate="CustomValidatorFoto_ServerValidate" ValidationGroup="lavoratore">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <telerik:RadBinaryImage runat="server" ID="RadBinaryImageFoto" Height="60px" AutoAdjustImageControlSize="False"
                ResizeMode="Fit" Width="55px" ImageUrl="~/images/noImg.JPG" />
            <asp:ImageButton ID="ButtonRemoveFoto" runat="server" CausesValidation="False" Text="Elimina foto"
                OnClick="ButtonRemoveFoto_Click" ImageUrl="~/images/pallinoX.png" />
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:ValidationSummary ID="ValidationSummaryLavoratore" runat="server" ValidationGroup="lavoratore"
                CssClass="messaggiErrore" />
        </td>
    </tr>
    <script type="text/javascript">

        function OnClientFileSelectedHandlerModifica(sender, eventArgs) {
            var input = eventArgs.get_fileInputField();
            if (sender.isExtensionValid(input.value)) {


                var radImg = document.getElementById('<%= RadBinaryImageFoto.ClientID %>');
                if (radImg) {
                    radImg.src = input.value;
                     <%= PostBackString %>
                }
            }
        }

    </script>
</table>
