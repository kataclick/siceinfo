﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class AccessoCantieri_WebControls_Committente : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaCommittente(Committente committente)
    {
        if (committente != null)
        {
            MultiViewCommittente.SetActiveView(ViewCommittenteSelezionato);

            LabelCognome.Text = committente.Cognome;
            LabelNome.Text = committente.Nome;
            LabelRagioneSociale.Text = committente.RagioneSociale;
            LabelCodiceFiscale.Text = committente.CodiceFiscale;
            LabelPartitaIva.Text = committente.PartitaIva;
        }
        else
        {
            MultiViewCommittente.SetActiveView(ViewCommittenteNonSelezionato);
        }
    }
}