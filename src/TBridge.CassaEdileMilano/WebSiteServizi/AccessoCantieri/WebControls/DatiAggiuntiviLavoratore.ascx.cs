﻿using System;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using TBridge.Cemi.Type.Collections;
//using TBridge.Cemi.AccessoCantieri.Business;

public partial class AccessoCantieri_WebControls_DatiAggiuntiviLavoratore : UserControl
{
    private readonly Common _bizCommon = new Common();
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
    protected string PostBackString;

    protected void Page_Load(object sender, EventArgs e)
    {
        PostBackString = Page.ClientScript.GetPostBackEventReference(this, null);

        if (!Page.IsPostBack)
        {
            //if (Request.QueryString["dataInizio"] != null && Request.QueryString["dataInizio"] != String.Empty && Request.QueryString["dataInizio"] != "undefined")
            //{
            //    DateTime dataInizio = DateTime.ParseExact(Request.QueryString["dataInizio"], "dd/MM/yyyy", null);
            //    ViewState["dataInizio"] = dataInizio;
            //}
            //if (Request.QueryString["dataFine"] != null && Request.QueryString["dataFine"] != String.Empty && Request.QueryString["dataFine"] != "undefined")
            //{
            //    DateTime dataFine = DateTime.ParseExact(Request.QueryString["dataFine"], "dd/MM/yyyy", null);
            //    ViewState["dataFine"] = dataFine;
            //}

            CaricaCombo();
        }
        else
        {
            if (RadUploadFoto.UploadedFiles.Count > 0)
            {
                byte[] bytes = new byte[RadUploadFoto.UploadedFiles[0].ContentLength];
                RadUploadFoto.UploadedFiles[0].InputStream.Read(bytes, 0, RadUploadFoto.UploadedFiles[0].ContentLength);

                ViewState["Foto"] = bytes;
                ViewState["Lung"] = RadUploadFoto.UploadedFiles[0].ContentLength;

                RadBinaryImageFoto.DataValue = bytes;
                RadBinaryImageFoto.DataBind();
            }
        }
    }

    public void CaricaDataInizio(DateTime dataInizio)
    {
        ViewState["dataInizio"] = dataInizio;
    }

    public void CaricaDataFine(DateTime dataInizio)
    {
        ViewState["dataFine"] = dataInizio;
    }

    public void CaricaIdCantiere(Int32 idCantiere)
    {
        ViewState["idCantiere"] = idCantiere;
    }

    public void CaricaIdImpresa(Int32 idImpresa)
    {
        ViewState["idImpresa"] = idImpresa;
    }

    public void CaricaTipoImpresa(TipologiaImpresa tipoImpresa)
    {
        ViewState["tipoImpresa"] = tipoImpresa;
    }

    private void CaricaCombo()
    {
        if (DropDownListProvinciaNascita.Items.Count == 0)
        {
            // Province
            Presenter.CaricaProvince(DropDownListProvinciaNascita);
        }

        if (DropDownListPaeseNascita.Items.Count == 0)
        {
            // Paese
            ListDictionary nazioni = _bizCommon.GetNazioni();
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListPaeseNascita,
                nazioni,
                "Value",
                "Key");
        }
    }

    public void CaricaLavoratore(Lavoratore lavoratore, Boolean edit)
    {
        CaricaCombo();

        if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
        {
            CustomValidatorCodiceFiscale.Enabled = false;
        }

        ViewState["TipologiaLavoratore"] = lavoratore.TipoLavoratore;
        ViewState["IdLavoratore"] = lavoratore.IdLavoratore;
        RadTextBoxCognome.Text = lavoratore.Cognome;
        RadTextBoxCognome.Enabled = false;
        RadTextBoxNome.Text = lavoratore.Nome;
        RadTextBoxNome.Enabled = false;
        RadDateDataNascita.SelectedDate = lavoratore.DataNascita;
        RadDateDataNascita.Enabled = false;
        RadTextBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;
        RadTextBoxCodiceFiscale.Enabled = false;

        if (edit)
        {
            CheckBoxControlli.Checked = lavoratore.EffettuaControlli;
            RadDateDataInizioAttivita.SelectedDate = lavoratore.DataInizioAttivita;
            RadDateDataFineAttivita.SelectedDate = lavoratore.DataFineAttivita;
            RadDateDataAssunzione.SelectedDate = lavoratore.DataAssunzione;

            Int32 idCantiere = Int32.Parse(ViewState["idCantiere"].ToString());
            Int32 idImpresa = Int32.Parse(ViewState["idImpresa"].ToString());
            TipologiaImpresa tipoImpresa = (TipologiaImpresa)(ViewState["tipoImpresa"]);

            byte[] photoNew = (byte[]) ViewState["Foto"];

            if (photoNew != null)
            {
                //lavoratore.Foto = photoNew;
                ViewState["Foto"] = photoNew;
                ViewState["Lung"] = photoNew.Length;
                RadBinaryImageFoto.DataValue = photoNew;
                RadBinaryImageFoto.DataBind();

            }
            else
            {
                if (lavoratore.IdLavoratore.HasValue)
                {
                    byte[] photo = _biz.GetFotoLavoratoreByLavoratoreImpresaCantiere(lavoratore.IdLavoratore.Value,
                                                                                     lavoratore.TipoLavoratore,
                                                                                     idImpresa, tipoImpresa, idCantiere);
                    //lavoratore.Foto = photo;

                    ViewState["Foto"] = photo;
                    if (photo != null)
                        ViewState["Lung"] = photo.Length;

                    RadBinaryImageFoto.DataValue = photo;
                    RadBinaryImageFoto.DataBind();
                }
            }

            if (lavoratore.Foto != null)
            {
                ViewState["Foto"] = lavoratore.Foto;
                ViewState["Lung"] = lavoratore.Foto.Length;
                RadBinaryImageFoto.DataValue = lavoratore.Foto;
                RadBinaryImageFoto.DataBind();
            }
        }

        // Gestione del paese di nascita
        if (!String.IsNullOrEmpty(lavoratore.PaeseNascita))
        {
            DropDownListPaeseNascita.SelectedValue = lavoratore.PaeseNascita;
        }
        else
        {
            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            {
                //if (lavoratore.ProvinciaNascita != null)
                //{
                //    if (lavoratore.ProvinciaNascita == "EE")
                //    {
                //        if (lavoratore.LuogoNascita != null)
                //        {
                //            ListItem paeseSel = DropDownListPaeseNascita.Items.FindByText(lavoratore.LuogoNascita);

                //            if (paeseSel != null)
                //                paeseSel.Selected = true;
                //        }
                //    }
                //}

                //if (DropDownListPaeseNascita.SelectedValue == "")
                //    DropDownListPaeseNascita.SelectedValue = "1";
            }
        }
        if (!String.IsNullOrEmpty(lavoratore.ProvinciaNascita))// && lavoratore.ProvinciaNascita != "EE")
        {
            if (lavoratore.ProvinciaNascita != "EE")
            {
                DropDownListProvinciaNascita.SelectedValue = lavoratore.ProvinciaNascita;
            }
            else
            {
                trProvinciaNascita.Visible = false;
            }
            Presenter.CaricaComuniCodiceCatastale(DropDownListComuneNascita, lavoratore.ProvinciaNascita);

            if (!String.IsNullOrEmpty(lavoratore.LuogoNascita))
            {
                ListItem luogoDaSel = null;

                if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    luogoDaSel = DropDownListComuneNascita.Items.FindByText(lavoratore.LuogoNascita);
                }
                else
                {
                    luogoDaSel = DropDownListComuneNascita.Items.FindByValue(lavoratore.LuogoNascita);
                }

                if (luogoDaSel != null)
                {
                    luogoDaSel.Selected = true;
                }
            }
        }

        // Disabilito i campi che vanno aggiornati su SiceNew
        if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
        {
            DropDownListPaeseNascita.Enabled = false;
            DropDownListProvinciaNascita.Enabled = false;
            DropDownListComuneNascita.Enabled = false;
            LabelModificaNascita.Visible = true;
            trPaeseNascita2.Visible = false;

            if (String.IsNullOrEmpty(DropDownListPaeseNascita.SelectedValue))
            {
                trPaeseNascita1.Visible = false;
            }

            CustomValidatorItaliano.Enabled = false;
        }
    }

    public Lavoratore GetLavoratore()
    {
        Lavoratore lavoratore = new Lavoratore();

        if (ViewState["TipologiaLavoratore"] != null)
        {
            lavoratore.TipoLavoratore = (TipologiaLavoratore) ViewState["TipologiaLavoratore"];
        }
        else
        {
            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
        }
        if (ViewState["IdLavoratore"] != null)
        {
            lavoratore.IdLavoratore = (Int32?) ViewState["IdLavoratore"];
        }
        lavoratore.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
        lavoratore.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
        if (RadDateDataNascita.SelectedDate != null) lavoratore.DataNascita = RadDateDataNascita.SelectedDate.Value;
        lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);

        // Altri dati
        lavoratore.DataInizioAttivita = RadDateDataInizioAttivita.SelectedDate;
        lavoratore.DataFineAttivita = RadDateDataFineAttivita.SelectedDate;
        lavoratore.EffettuaControlli = CheckBoxControlli.Checked;
        lavoratore.DataAssunzione = RadDateDataAssunzione.SelectedDate;
        lavoratore.PaeseNascita = DropDownListPaeseNascita.SelectedValue;
        if (lavoratore.TipoLavoratore != TipologiaLavoratore.SiceNew)
        {
            lavoratore.ProvinciaNascita = DropDownListProvinciaNascita.SelectedValue;
            lavoratore.LuogoNascita = DropDownListComuneNascita.SelectedValue;
        }
        else
        {
            if (!String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue) && String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue))
            {
                lavoratore.ProvinciaNascita = "EE";
            }
            else
            {
                lavoratore.ProvinciaNascita = DropDownListProvinciaNascita.SelectedValue;
            }
            if (!String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue))
            {
                lavoratore.LuogoNascita = DropDownListComuneNascita.SelectedItem.Text;
            }
        }

        //if ()

        //if (ViewState["Foto"] != null)
        //{
        //    lavoratore.Foto = (byte[]) ViewState["Foto"];
        //}

        return lavoratore;
    }

    public Lavoratore GetLavoratorePhoto()
    {
        Lavoratore lavoratore = new Lavoratore();

        if (ViewState["TipologiaLavoratore"] != null)
        {
            lavoratore.TipoLavoratore = (TipologiaLavoratore)ViewState["TipologiaLavoratore"];
        }
        else
        {
            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
        }
        if (ViewState["IdLavoratore"] != null)
        {
            lavoratore.IdLavoratore = (Int32?)ViewState["IdLavoratore"];
        }
        lavoratore.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
        lavoratore.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
        if (RadDateDataNascita.SelectedDate != null) lavoratore.DataNascita = RadDateDataNascita.SelectedDate.Value;
        lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);

        // Altri dati
        lavoratore.DataInizioAttivita = RadDateDataInizioAttivita.SelectedDate;
        lavoratore.DataFineAttivita = RadDateDataFineAttivita.SelectedDate;
        lavoratore.EffettuaControlli = CheckBoxControlli.Checked;
        lavoratore.DataAssunzione = RadDateDataAssunzione.SelectedDate;
        lavoratore.PaeseNascita = DropDownListPaeseNascita.SelectedValue;
        if (lavoratore.TipoLavoratore != TipologiaLavoratore.SiceNew)
        {
            lavoratore.ProvinciaNascita = DropDownListProvinciaNascita.SelectedValue;
            lavoratore.LuogoNascita = DropDownListComuneNascita.SelectedValue;
        }
        else
        {
            if (!String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue) && String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue))
            {
                lavoratore.ProvinciaNascita = "EE";
            }
            else
            {
                lavoratore.ProvinciaNascita = DropDownListProvinciaNascita.SelectedValue;
            }
            if (!String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue))
            {
                lavoratore.LuogoNascita = DropDownListComuneNascita.SelectedItem.Text;
            }
        }

        if (ViewState["Foto"] != null)
        {
            lavoratore.Foto = (byte[])ViewState["Foto"];
        }

        return lavoratore;
    }

    public byte[] GetPhotoLavoratore()
    {
        byte[] pht = (byte[]) ViewState["Foto"];
        //ViewState["Foto"] = null;
        //ViewState["Lung"] = null;
        return pht;
    }

    public void ResetPhotoLavoratore()
    {
        ViewState["Foto"] = null;
        ViewState["Lung"] = null;
    }

    protected void DropDownListPaeseNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Se Italia attivo ricerca province, comuni,...
        if (DropDownListPaeseNascita.SelectedValue == "1")
        {
            DropDownListProvinciaNascita.Enabled = true;
            DropDownListComuneNascita.Enabled = true;
        }
        else
        {
            DropDownListProvinciaNascita.Enabled = false;
            DropDownListComuneNascita.Enabled = false;
        }
    }

    protected void DropDownListProvinciaNascita_SelectedIndexChanged(object sender, EventArgs e)
    {
        Presenter.CaricaComuniCodiceCatastale(DropDownListComuneNascita, DropDownListProvinciaNascita.SelectedValue);
    }

    protected void ButtonRemoveFoto_Click(object sender, ImageClickEventArgs e)
    {
        ViewState["Foto"] = null;

        RadBinaryImageFoto.ImageUrl = "~/images/noImg.JPG";
        RadBinaryImageFoto.DataBind();

        RadBinaryImageFoto.ImageUrl = "~/images/noImg.JPG";
        RadBinaryImageFoto.DataBind();
    }

    #region Custom Validators

    protected void CustomValidatorFoto_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ViewState["Lung"] != null)
        {
            args.IsValid = RadUploadFoto.InvalidFiles.Count == 0 && (Int32.Parse(ViewState["Lung"].ToString()) < 102400);
        }
    }

    protected void CustomValidatorItaliano_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (DropDownListPaeseNascita.SelectedValue == "1"
            && (String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue)
                || String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue)))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDateDelCantiere_ServerValidate(object source, ServerValidateEventArgs args)
    {
        DateTime? dataInizio = null;
        if (RadDateDataInizioAttivita.SelectedDate.HasValue)
        {
            dataInizio = RadDateDataInizioAttivita.SelectedDate.Value;
        }

        DateTime? dataFine = null;
        if (RadDateDataFineAttivita.SelectedDate.HasValue)
        {
            dataFine = RadDateDataFineAttivita.SelectedDate.Value;
        }

        DateTime dataInizioCantiere = DateTime.Parse(ViewState["dataInizio"].ToString());
        DateTime dataFineCantiere = DateTime.Parse(ViewState["dataFine"].ToString());

        if (dataInizio.HasValue && dataFine.HasValue)
        {
            if ((dataInizio < dataInizioCantiere) || (dataFine > dataFineCantiere))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }
    }

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        //args.IsValid = true;

        //if ((RadDateDataNascita.SelectedDate.HasValue) && (RadTextBoxCodiceFiscale.Text.Length == 16))
        //{
        //    DateTime dataNascita = RadDateDataNascita.SelectedDate.Value;

        //    if (!string.IsNullOrEmpty(RadTextBoxNome.Text) && !string.IsNullOrEmpty(RadTextBoxCognome.Text))
        //    {
        //        string maschio = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(RadTextBoxNome.Text,
        //                                                                                   RadTextBoxCognome.Text, "M",
        //                                                                                   dataNascita);
        //        string femmina = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(RadTextBoxNome.Text,
        //                                                                                   RadTextBoxCognome.Text, "F",
        //                                                                                   dataNascita);

        //        if (maschio.Substring(0, 6) != RadTextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper() &&
        //            femmina.Substring(0, 6) != RadTextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper())
        //        {
        //            args.IsValid = false;
        //        }
        //    }

        //    String codiceMaschile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "M", dataNascita, "A000");
        //    String codiceFemminile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "F", dataNascita, "A000");

        //    if (codiceMaschile.Substring(6, 5).ToUpper() != RadTextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
        //        &&
        //        codiceFemminile.Substring(6, 5).ToUpper() != RadTextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
        //        )
        //    {
        //        args.IsValid = false;
        //    }
        //}

        args.IsValid = false;

        if (!string.IsNullOrEmpty(RadTextBoxNome.Text) && !string.IsNullOrEmpty(RadTextBoxCognome.Text))
        {
            if ((RadDateDataNascita.SelectedDate.HasValue) && (RadTextBoxCodiceFiscale.Text.Length == 16))
            {
            //    if (DropDownListPaeseNascita.SelectedIndex != 0)
            //    {
            //        string codiceIstat = null;
            //        if (DropDownListProvinciaNascita.Enabled && DropDownListComuneNascita.Enabled)
            //            codiceIstat = DropDownListComuneNascita.SelectedValue;
            //        else
            //            codiceIstat = _biz.GetCodiceCatastale(DropDownListPaeseNascita.SelectedItem.Text);

            //        if (codiceIstat != null)
            //            args.IsValid = CodiceFiscaleManager.VerificaCodiceFiscale(RadTextBoxNome.Text, RadTextBoxCognome.Text, "M",
            //                                                       RadDateDataNascita.SelectedDate.Value, codiceIstat,
            //                                                       RadTextBoxCodiceFiscale.Text);
            //    }
            //    else
            //    {
                args.IsValid = CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(RadTextBoxNome.Text, RadTextBoxCognome.Text, "M",
                                                              RadDateDataNascita.SelectedDate.Value,
                                                              RadTextBoxCodiceFiscale.Text)
                                ||
                                CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(RadTextBoxNome.Text, RadTextBoxCognome.Text, "F",
                                                              RadDateDataNascita.SelectedDate.Value,
                                                              RadTextBoxCodiceFiscale.Text);
            //    }
            }
        }

    }

    #endregion
}