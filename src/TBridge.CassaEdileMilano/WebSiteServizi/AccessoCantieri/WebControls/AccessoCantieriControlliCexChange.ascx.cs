﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Business;
using System.Drawing;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.AccessoCantieri.Type.Collections.CexChange;
using TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange;
using System.Web.UI.HtmlControls;

public partial class AccessoCantieri_WebControls_AccessoCantieriControlliCexChange : System.Web.UI.UserControl
{


    private readonly AccessoCantieriBusiness _accessoCantieriBiz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        RicercaCantieri1.OnCantiereSelected += RicercaCantieri1_OnCantiereSelected;

        //if (!Page.IsPostBack)
        //{
        //    CaricaAnomalie();
        //    CaricaRuoli();
        //}
    }

    private void CaricaGridTimbratureFilter(Int32 pagina)
    {
        RadGridTimbrature.Visible = true;

        ControlloIdentitaFilter filtro = CreaFiltro();
       // ControlloIdentitaCollection controlloIdentitaCollectionApp = _accessoCantieriBiz.GetControlloIdentita(filtro);

        ControlloIdentitaCexChangeCollection controlloIdentitaCollectionApp = _accessoCantieriBiz.GetControlloIdentitaCexchange(filtro);
        if (filtro.IdCantiere != null)
        {
            WhiteListImpresaCollection wlImp = _accessoCantieriBiz.GetLavoratoriInDomanda(filtro.IdCantiere.Value);

            ControlloIdentitaCexChangeCollection controlloIdentitaCollection = new ControlloIdentitaCexChangeCollection();
            foreach (ControlloIdentitaCexChange ctI in controlloIdentitaCollectionApp)
            {
                if (wlImp.EffettuaControlli(ctI.CodiceFiscaleLavoratore, ctI.PartitaIvaImpresa))
                {
                    controlloIdentitaCollection.Add(ctI);
                }
            }

            foreach (ControlloIdentitaCexChange ctrlId in controlloIdentitaCollection)
            {
                if (ctrlId.IdCantiere.HasValue)
                {
                    //LavoratoreCollection lavColl = biz.GetLavoratoreControlli(ctrlId.IdCantiere.Value, ctrlId.CodiceFiscale,
                    //                                                          null);

                    if (!string.IsNullOrEmpty(ctrlId.PartitaIvaImpresa))
                        ctrlId.ImpresaTimbratura = _accessoCantieriBiz.GetImpresaControlli(ctrlId.IdCantiere.Value,
                                                                                 ctrlId.CodiceFiscaleLavoratore,
                                                                                 ctrlId.PartitaIvaImpresa);

                    LavoratoreCollection lavColl;
                    if (ctrlId.ImpresaTimbratura != null)
                    {
                        if (ctrlId.ImpresaTimbratura.IdImpresa.HasValue)
                        {
                            lavColl = _accessoCantieriBiz.GetLavoratoreControlli(ctrlId.IdCantiere.Value,
                                                                                 ctrlId.CodiceFiscaleLavoratore,
                                                                                 ctrlId.ImpresaTimbratura.IdImpresa.Value);
                        }
                        else
                        {
                            lavColl = _accessoCantieriBiz.GetLavoratoreControlli(ctrlId.IdCantiere.Value,
                                                                                 ctrlId.CodiceFiscaleLavoratore,
                                                                                 null);
                        }
                    }
                    else
                    {
                        lavColl = _accessoCantieriBiz.GetLavoratoreControlli(ctrlId.IdCantiere.Value,
                                                                             ctrlId.CodiceFiscaleLavoratore,
                                                                             null);
                    }

                    Lavoratore lavSel = null;
                    foreach (Lavoratore lav in lavColl)
                    {
                        if ((lav.Incongruenze == false) && (lav.IdLavoratore != null))
                            lavSel = lav;
                    }

                    if (lavSel == null)
                        foreach (Lavoratore lav in lavColl)
                        {
                            if (lav.Incongruenze && (lav.IdLavoratore != null))
                                lavSel = lav;
                        }

                    if (lavSel == null)
                        foreach (Lavoratore lav in lavColl)
                        {
                            if (!string.IsNullOrEmpty(lav.Nome))
                                lavSel = lav;
                        }

                    ctrlId.LavoratoreTimbratura = lavSel;

                    //if (!string.IsNullOrEmpty(ctrlId.PartitaIvaImpresa))
                    //    ctrlId.Impresa = biz.GetImpresaControlli(ctrlId.IdCantiere.Value, ctrlId.CodiceFiscale,
                    //                                         ctrlId.PartitaIvaImpresa);
                }
            }

            if (pagina == 0)
            {
                RadGridTimbrature.CurrentPageIndex = 0;
            }

            Presenter.CaricaElementiInGridView(
                RadGridTimbrature,
                controlloIdentitaCollectionApp);

            ButtonEsportaPDF.Visible = (controlloIdentitaCollectionApp != null && controlloIdentitaCollectionApp.Count > 0);
        }
    }

    private void CaricaAnomalie()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListAnomalie,
            Enum.GetNames(typeof(TipologiaAnomaliaControlli)),
            "",
            "");
    }

    private void CaricaRuoli()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListRuoli,
            Enum.GetNames(typeof(TipologiaRuoloTimbratura)),
            "",
            "");
    }

    protected void ButtonVisualizzaTimb_Click(object sender, EventArgs e)
    {
        CaricaGridTimbratureFilter(0);
    }

    private void RicercaCantieri1_OnCantiereSelected(Int32 idWhiteList)
    {
        WhiteList whiteListIdDomanda = _accessoCantieriBiz.GetDomandaByKey(idWhiteList);
        ViewState["IdDomanda"] = idWhiteList;

        String indirizzo = whiteListIdDomanda.Indirizzo;
        String comune = whiteListIdDomanda.Comune;
        String provincia = whiteListIdDomanda.Provincia;

        LabelCantiere.Visible = true;
        PanelFiltroTimb.Visible = true;
        LabelCantiere.Text = string.Format("Cantiere selezionato: {0} {1} {2}", indirizzo, comune, provincia);

        PanelAggiungiRilevatore.Visible = true;

        //CaricaGridTimbratureFilter(0);

        PanelRicerca.Visible = false;

        PanelRilevatori.Visible = true;

        ImpresaCollection imprese = new ImpresaCollection();
        foreach (Subappalto sub in whiteListIdDomanda.Subappalti)
        {
            if (sub.Appaltante != null)
                if (!imprese.Contains(sub.Appaltante))
                    imprese.Add(sub.Appaltante);

            if (sub.Appaltata != null)
                if (!imprese.Contains(sub.Appaltata))
                    imprese.Add(sub.Appaltata);
        }

       // imprese.OrderBy(x => x.RagioneSociale).ToList();
        imprese.Sort((x,y) => (String.Compare(x.RagioneSociale, y.RagioneSociale)));
       
        Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListImpresa, imprese,
                                                           "RagioneSociale", "idImpresa");
    }

    private ControlloIdentitaFilter CreaFiltro()
    {
        ControlloIdentitaFilter filtro = new ControlloIdentitaFilter
        {
            IdCantiere = Int32.Parse(ViewState["IdDomanda"].ToString())
        };

        try
        {
            filtro.Mese = Int32.Parse(TextBoxMeseAnno.Text.Split('/')[0]);
        }
        catch
        {
            filtro.Mese = null;
        }

        try
        {
            filtro.Anno = Int32.Parse(TextBoxMeseAnno.Text.Split('/')[1]);
        }
        catch
        {
            filtro.Anno = null;
        }

        if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
        {
            Int32? idImpresa = Int32.Parse(DropDownListImpresa.SelectedValue);
            filtro.IdImpresa = idImpresa;
        }

        //if (DropDownListAnomalie.SelectedItem != null && DropDownListAnomalie.SelectedItem.ToString() != string.Empty)
        //{
        //    Int32? anomalia = (DropDownListAnomalie.SelectedIndex - 1);
        //    filtro.TipologiaAnomaliaControlli = new TipologiaAnomaliaControlli();
        //    filtro.TipologiaAnomaliaControlli = (TipologiaAnomaliaControlli?)anomalia;
        //}

        //if (DropDownListRuoli.SelectedItem != null && DropDownListRuoli.SelectedItem.ToString() != string.Empty)
        //{
        //    Int32? ruolo = (DropDownListRuoli.SelectedIndex - 1);
        //    filtro.TipologiaRuoloTimbratura = new TipologiaRuoloTimbratura();
        //    filtro.TipologiaRuoloTimbratura = (TipologiaRuoloTimbratura?)ruolo;
        //}

        filtro.TipologiaAnomaliaControlli = null;

        //SOLO LAVORATORI
        filtro.TipologiaRuoloTimbratura = TipologiaRuoloTimbratura.Lavoratore;

        filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

        filtro.PartitaIVA = TextBoxPIVA.Text;

        return filtro;
    }

    private void SetLabelOK(Label label,  String tooltipText)
    {
        label.Text = "OK";
        label.ToolTip = tooltipText;
        label.ForeColor = Color.Green;
    }

    private void SetLabelKO(Label label, String tooltipText)
    {
        label.Text = "KO";
        label.ToolTip = tooltipText;
        label.ForeColor = Color.Red;
    }

    private void SetLabelEmpty(Label label,  String tooltipText)
    {
        label.Text = "--";
        label.ToolTip = tooltipText;
        //label.ForeColor = Color.Red;
    }

    protected void RadGridCecChange_ItemDataBound(object sender, GridItemEventArgs e)
    {

        if (e.Item is GridDataItem)
        {
            ControlloDenunciaOreCexChange controllo = (ControlloDenunciaOreCexChange)e.Item.DataItem;

            Label lNomeCognome = (Label)e.Item.FindControl("LabelNomeCognome");
            Label lDataNascita = (Label)e.Item.FindControl("LabelDataNascita");
            Label lCodiceCeLav = (Label)e.Item.FindControl("LabelCodiceCE");
            Label lDenunciaImpresa = (Label)e.Item.FindControl("LabelDenuncia");
            Label lLavoratoreDenuncia = (Label)e.Item.FindControl("LabelLavoratoreDenuncia");
            Label lOreLavorate = (Label)e.Item.FindControl("LabelOreLavorate");
            Label lRegolarita = (Label)e.Item.FindControl("LabelRegolarita");
            Label lCodiceImpresa = (Label)e.Item.FindControl("LabelCodiceImpresa");
            Label lRagioneSocialeImpresa = (Label)e.Item.FindControl("LabelRagioneSocialeImpresa");
            Label labelStatoImpresa = (Label)e.Item.FindControl("LabelStatoImpresa");
           // Label lIvaCodFiscImpresa = (Label)e.Item.FindControl("LabelIvaCodFiscImpresa");
           
           
            //Label lRuolo = (Label)e.Item.FindControl("LabelRuolo");

            if (controllo.Impresa == null)
            {
                SetLabelEmpty(lCodiceImpresa, "Impresa non conosciuta");
                SetLabelEmpty(lRagioneSocialeImpresa, "Impresa non conosciuta");
               // SetLabelEmpty(lIvaCodFiscImpresa, "Impresa non conosciuta");
                SetLabelEmpty(labelStatoImpresa, "Non valutabile");
                SetLabelEmpty(lRegolarita, "Non valutabile");
                SetLabelEmpty(lDenunciaImpresa, "Non valutabile");
            }
            else
            {
                lCodiceImpresa.Text = controllo.Impresa.CodiceCEImpresa;
                lRagioneSocialeImpresa.Text = controllo.Impresa.RagioneSociale;
               // lIvaCodFiscImpresa.Text = controllo.Impresa.PartitaIva;

                switch (controllo.Impresa.Stato)
                {
                    case "A":
                        labelStatoImpresa.Text = "ATTIVA";
                        break;
                    case "S":
                        labelStatoImpresa.ForeColor = Color.Red;
                        labelStatoImpresa.Text = "SOSPESA";
                        break;
                    case "C":
                        labelStatoImpresa.ForeColor = Color.Red;
                        if (controllo.Impresa.DataCessazione.HasValue)
                        {
                            labelStatoImpresa.Text = String.Format("CESSATA dal {0}", controllo.Impresa.DataCessazione.Value.ToShortDateString());
                        }
                        else
                        {
                            labelStatoImpresa.Text = "CESSATA";
                        }
                        break;
                    default:
                        labelStatoImpresa.Text = "--";
                        labelStatoImpresa.ForeColor = Color.Red;
                        break;
                }

                if (!controllo.ImpresaIscrittaInPeriodo)
                {
                    SetLabelEmpty(lDenunciaImpresa, "Impresa non ancora iscritta");
                    SetLabelEmpty(lRegolarita, "Impresa non ancora iscritta");
                }
                else
                {
                    if (controllo.ImpresaPresenzaDenunceInPeriodo)
                    {
                        SetLabelOK(lDenunciaImpresa, String.Format("Presenza della denuncia effettuata dall'impresa {0} per il mese {1}",
                                                                    controllo.Impresa, controllo.Periodo));
                    }
                    else
                    {
                        SetLabelKO(lDenunciaImpresa, String.Format("NON è presente una denuncia effettuata dall'impresa {0} per il mese {1}",
                                                                    controllo.Impresa, controllo.Periodo));
                    }

                    // OLD REGOLARITà BNI
                    //if (!controllo.Impresa.PosizioneBNIRegolre.HasValue)
                    //{
                    //    SetLabelEmpty(lRegolarita, "Informazione non disponibile");
                    //}
                    //else if (controllo.Impresa.PosizioneBNIRegolre.Value)
                    //{
                    //    SetLabelOK(lRegolarita, String.Format("Regolarità dell'impresa {0} per BNI", controllo.Impresa));
                    //}
                    //else
                    //{
                    //    SetLabelKO(lRegolarita, String.Format("Impresa {0} NON regolare per BNI", controllo.Impresa));
                    //}

                    //// NEW REGOLARITà VERSAMENTO
                    if (controllo.ImpresaEsitoVersamentoInPeriodo == null )
                    {
                        SetLabelEmpty(lRegolarita, "Informazione non disponibile");
                    }
                    else
                    {
                        switch (controllo.ImpresaEsitoVersamentoInPeriodo.Id)
                        {
                            case 1:
                                SetLabelOK(lRegolarita, String.Format("L'impresa {0} ha versato nel perido {1}", controllo.Impresa, controllo.Periodo));
                                break;
                            case 2:
                                SetLabelKO(lRegolarita, String.Format("L'impresa {0} non ha versato nel perido {1}", controllo.Impresa, controllo.Periodo));
                                break;
                            default:
                                SetLabelEmpty(lRegolarita, "Informazione non disponibile");
                                break;
                        }
 
                    }
                }
            }


            if (controllo.Lavoratore == null)
            {
                SetLabelEmpty(lNomeCognome, "Lavoratore non conosciuto");
                SetLabelEmpty(lDataNascita, "Lavoratore non conosciuto");
                SetLabelEmpty(lCodiceCeLav, "Lavoratore non conosciuto");
                SetLabelEmpty(lLavoratoreDenuncia, "Non valutabile");
                SetLabelEmpty(lOreLavorate, "Non valutabile");
            }
            else
            {
                lNomeCognome.Text = controllo.Lavoratore.ToString();
                lDataNascita.Text = controllo.Lavoratore.DataNascita.HasValue ? controllo.Lavoratore.DataNascita.Value.ToString("dd/MM/yyyy") : null;
                lCodiceCeLav.Text = controllo.Lavoratore.CodiceCELavoratore;

                if (controllo.Impresa == null)
                {
                    SetLabelEmpty(lLavoratoreDenuncia, "Non valutabile");
                    SetLabelEmpty(lOreLavorate, "Non valutabile");
                }
                else if(!controllo.ImpresaIscrittaInPeriodo)
                {
                    SetLabelEmpty(lLavoratoreDenuncia, "Impresa non ancora iscritta");
                    SetLabelEmpty(lOreLavorate, "Impresa non ancora iscritta");
                }
                else if (controllo.DeunciaOre != null)
                {
                    SetLabelOK(lLavoratoreDenuncia, String.Format("Presenza del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                                                                 controllo.Lavoratore, controllo.Impresa, controllo.Periodo));

                    if (controllo.DeunciaOre.OreOrdinarie > 0)
                    {
                        SetLabelOK(lOreLavorate, String.Format("Presenza di ore lavorate del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                                                                controllo.Lavoratore, controllo.Impresa, controllo.Periodo));
                    }
                    else
                    {
                        SetLabelKO(lOreLavorate,  String.Format( "NON sono presenti ore lavorate del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                                                                controllo.Lavoratore, controllo.Impresa, controllo.Periodo));
                    }
                }
                else
                {
                    SetLabelKO(lLavoratoreDenuncia, String.Format( "NON è presente il lavoratore {0} nella denuncia effettuata dall impresa {1} per il mese {2}",
                                                                 controllo.Lavoratore, controllo.Impresa, controllo.Periodo));

                    SetLabelKO(lOreLavorate, String.Format("NON sono presenti ore lavorate del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                                                                controllo.Lavoratore, controllo.Impresa, controllo.Periodo));
                }
            }


         
        }
    }

    protected void RadGridTimbrature_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            ControlloIdentitaCexChange controlloIdentita = (ControlloIdentitaCexChange)e.Item.DataItem;

            Label lMeseAnno = (Label)e.Item.FindControl("LabelMeseAnno");
            Label lWlCodFiscLav = (Label)e.Item.FindControl("LabelWlCodFiscLav");
            Label lWlNomeCognomeLav = (Label)e.Item.FindControl("LabelWlNomeCognomeLav");
            Label lWlRagSocImpresa = (Label)e.Item.FindControl("LabelWlRagSocImpresa");
            Label lWlPivaImpresa = (Label)e.Item.FindControl("LabelWlPivaImpresa");
            RadGrid radGridCexChange = (RadGrid)e.Item.FindControl("RadGridCexChange");
            Label lPosizioneContributiva = (Label)e.Item.FindControl("LabelPosizionecontributiva");
            //Label lNomeCognome = (Label)e.Item.FindControl("LabelNomeCognome");
            //Label lDataNascita = (Label)e.Item.FindControl("LabelDataNascita");
            //Label lCodiceCe = (Label)e.Item.FindControl("LabelCodiceCE");
            //Label lDenuncia = (Label)e.Item.FindControl("LabelDenuncia");
            //Label lLavoratoreDenuncia = (Label)e.Item.FindControl("LabelLavoratoreDenuncia");
            //Label lOreLavorate = (Label)e.Item.FindControl("LabelOreLavorate");
            //Label lRegolarita = (Label)e.Item.FindControl("LabelRegolarita");
            //Label lCodiceImpresa = (Label)e.Item.FindControl("LabelCodiceImpresa");
            //Label lRagioneSocialeImpresa = (Label)e.Item.FindControl("LabelRagioneSocialeImpresa");
            //Label labelStatoImpresa = (Label)e.Item.FindControl("LabelStatoImpresa");
            //Label lIvaCodFiscImpresa = (Label)e.Item.FindControl("LabelIvaCodFiscImpresa");
            //Label lUltimaDenuncia = (Label) e.Item.FindControl("LabelUltimaDenuncia");
            //Label lCe = (Label) e.Item.FindControl("LabelCE");
            //Label lPosizioneContributiva = (Label)e.Item.FindControl("LabelPosizionecontributiva");
            //Label lRuolo = (Label)e.Item.FindControl("LabelRuolo");

            //BulletedList bulletedListCeCex = (BulletedList)e.Item.FindControl("BulletListCasseEdiliCexChange");
            //Control rowCeCex = e.Item.FindControl("RowCasseEdiliCex");
            //Control rowCeCexEmpty = e.Item.FindControl("RowCasseEdiliCexEmpty");

            lMeseAnno.Text = string.Format("{0}/{1}", controlloIdentita.Mese, controlloIdentita.Anno);
            lWlCodFiscLav.Text = controlloIdentita.CodiceFiscaleLavoratore;
            lWlPivaImpresa.Text = controlloIdentita.PartitaIvaImpresa;
            if (controlloIdentita.LavoratoreTimbratura != null)
            {
                lWlNomeCognomeLav.Text =  string.Format("{0} {1}", controlloIdentita.LavoratoreTimbratura.Nome,
                                                                    controlloIdentita.LavoratoreTimbratura.Cognome);
            }

            if (controlloIdentita.ImpresaTimbratura != null)
            {
                lWlRagSocImpresa.Text = controlloIdentita.ImpresaTimbratura.RagioneSociale;
            }


            if (radGridCexChange != null)
            {
                Presenter.CaricaElementiInGridView(radGridCexChange, controlloIdentita.ControlliDenunceCexChange);
            }



             #region INPS new

             lPosizioneContributiva.ForeColor = Color.Red;
             lPosizioneContributiva.Text = "--";
             lPosizioneContributiva.ToolTip = "Nessuna valutazione";

             if (controlloIdentita.ContribuzioniInps != null && controlloIdentita.ContribuzioniInps.Count > 0)
             {
                 DateTime? dataContrib =
                     controlloIdentita.ContribuzioniInps.ContainsImpresa(controlloIdentita.PartitaIvaImpresa,
                                                                         controlloIdentita.Mese, controlloIdentita.Anno);
                 bool? regolare =
                     controlloIdentita.ContribuzioniInps.ContainsImpresaAnnoMese(controlloIdentita.PartitaIvaImpresa,
                                                                                 controlloIdentita.Mese,
                                                                                 controlloIdentita.Anno);
                 if (dataContrib.HasValue)
                 {
                     if (regolare.HasValue)
                     {
                         if (regolare.Value)
                         {
                             lPosizioneContributiva.ForeColor = Color.Green;
                             lPosizioneContributiva.Text = "OK";
                             lPosizioneContributiva.ToolTip = "Posizione regolare";
                         }
                         else
                         {
                             if ((dataContrib.Value.Year < controlloIdentita.Anno) ||
                                 (dataContrib.Value.Month < controlloIdentita.Mese &&
                                  dataContrib.Value.Year == controlloIdentita.Anno))
                             {
                                 lPosizioneContributiva.ForeColor = Color.DarkOrange;
                                 lPosizioneContributiva.Text = string.Format("KO (OK fino al {0}/{1})",
                                                                             dataContrib.Value.Month,
                                                                             dataContrib.Value.Year);
                                 lPosizioneContributiva.ToolTip = "Posizione irregolare";
                             }
                             else
                             {
                                 lPosizioneContributiva.ForeColor = Color.Red;
                                 lPosizioneContributiva.ToolTip = "Posizione irregolare";
                                 lPosizioneContributiva.Text = "KO";
                             }
                         }
                     }
                 }
                 else
                 {
                     if (regolare.HasValue)
                     {
                         if (!regolare.Value)
                         {
                             lPosizioneContributiva.ForeColor = Color.Red;
                             lPosizioneContributiva.ToolTip = "Posizione irregolare";
                             lPosizioneContributiva.Text = "KO";
                         }
                     }
                     else
                     {
                         lPosizioneContributiva.ForeColor = Color.Red;
                         lPosizioneContributiva.Text = "--";
                         lPosizioneContributiva.ToolTip = "Nessuna valutazione";
                     }
                 }
             }

            //if ((controlloIdentita.Anno < 2010) || ((controlloIdentita.Anno == 2010) && (controlloIdentita.Mese < 4)))
            //{
            //    lPosizioneContributiva.ForeColor = Color.Red;
            //    lPosizioneContributiva.Text = "--";
            //    lPosizioneContributiva.ToolTip = "Nessuna valutazione";
            //}

             #endregion

        }
    }

    protected void ButtonEsportaPDF_Click(object sender, EventArgs e)
    {
        RadGridTimbrature.AllowPaging = false;
        CaricaGridTimbratureFilter(0);

        RadGridTimbrature.Columns.FindByUniqueName("timbratura").HeaderStyle.Width = new Unit("130px");
        RadGridTimbrature.Columns.FindByUniqueName("timbratura").ItemStyle.Width = new Unit("130px");
        RadGridTimbrature.Columns.FindByUniqueName("datiCexChange").HeaderStyle.Width = new Unit("450px");
        RadGridTimbrature.Columns.FindByUniqueName("datiCexChange").ItemStyle.Width = new Unit("450px");
        RadGridTimbrature.Columns.FindByUniqueName("controlloINPS").HeaderStyle.Width = new Unit("100px");
        RadGridTimbrature.Columns.FindByUniqueName("controlloINPS").ItemStyle.Width = new Unit("100px");

        foreach (GridHeaderItem colHeader in RadGridTimbrature.MasterTableView.GetItems(GridItemType.Header))
        {

            foreach (TableCell cell in colHeader.Cells)
            {
                cell.Style["text-align"] = "left";
            }
           // colHeader.HorizontalAlign = HorizontalAlign.Left;
           // colHeader.Style["text-align"] = "left";
            colHeader.Style["vertical-align"] = "top";
            //colHeader.Style["horizontal-align"] = "left";
            colHeader.Style["font-size"] = "8px";
            colHeader.Style["font-weight"] = "bold";
        }

        foreach (GridDataItem dItem in RadGridTimbrature.Items)
        {
            dItem.Style["font-family"] = "Courier New";
            dItem.Style["text-align"] = "left";
            dItem.Style["vertical-align"] = "top";
            dItem.Style["font-size"] = "6px";
            

            HtmlTableRow row = (HtmlTableRow)dItem.FindControl("rowPeriodo");
            row.Style["font-size"] = "6px";
            row = (HtmlTableRow)dItem.FindControl("rowLavoratore");
            row.Style["font-size"] = "6px";
            row = (HtmlTableRow)dItem.FindControl("rowImpresa");
            row.Style["font-size"] = "6px";

            RadGrid radGridCex = (RadGrid)dItem.FindControl("RadGridCexChange");      
            radGridCex.Columns.FindByUniqueName("cassaEdile").HeaderStyle.Width = new Unit("70px");
            radGridCex.Columns.FindByUniqueName("cassaEdile").ItemStyle.Width = new Unit("70px");
            radGridCex.Columns.FindByUniqueName("impresa").HeaderStyle.Width = new Unit("170px");
            radGridCex.Columns.FindByUniqueName("impresa").ItemStyle.Width = new Unit("170px");
            radGridCex.Columns.FindByUniqueName("controlliCEMI").HeaderStyle.Width = new Unit("160px");
            radGridCex.Columns.FindByUniqueName("controlliCEMI").ItemStyle.Width = new Unit("160px");

            foreach (GridHeaderItem colHeader in radGridCex.MasterTableView.GetItems(GridItemType.Header))
            {
                foreach (TableCell cell in colHeader.Cells)
                {
                    cell.Style["text-align"] = "left";
                }
            }

        }

        //RadGridTimbrature.ExportSettings.IgnorePaging = true;
        RadGridTimbrature.ExportSettings.Pdf.PageTitle =
            String.Format("Controlli Accesso Cantieri - stampato il {0} alle {1}", DateTime.Now.ToString("dd/MM/yyyy"),
                          DateTime.Now.ToString("HH:mm"));
        RadGridTimbrature.MasterTableView.ExportToPdf();
    }

    protected void RadGridTimbrature_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaGridTimbratureFilter(e.NewPageIndex);
    }
}