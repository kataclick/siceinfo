﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Presenze.ascx.cs" Inherits="AccessoCantieri_WebControls_Presenze" %>

<style type="text/css">
    .style1
    {
        width: 100px;
    }
    .style2
    {
        width: 210px;
    }
    .style3
    {
        width: 60px;
    }
    .style4
    {
        width: 225px;
    }
</style>

<asp:Panel
    ID="PanelRicerca"
    runat="server"
    DefaultButton="ButtonVisualizza">

<br />

<table class="standardTable">
    <tr>
        <td class="style2">
            Dal
            <asp:CustomValidator
                ID="CustomValidatorDal"
                runat="server"
                ValidationGroup="ricercaPresenze"
                ErrorMessage="Selezionare una data di inizio"
                ForeColor="Red" onservervalidate="CustomValidatorDal_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
        <td class="style4">
            Al
            <asp:CustomValidator
                ID="CustomValidatorAl"
                runat="server"
                ValidationGroup="ricercaPresenze"
                ErrorMessage="Selezionare una data di fine"
                ForeColor="Red" onservervalidate="CustomValidatorAl_ServerValidate">
                *
            </asp:CustomValidator>
            <asp:CustomValidator
                ID="CustomValidatorRangeValido"
                runat="server"
                ValidationGroup="ricercaPresenze"
                ErrorMessage="La data di fine ricerca non può essere precedente a quella di inizio"
                ForeColor="Red" 
                onservervalidate="CustomValidatorRangeValido_ServerValidate" >
                *
            </asp:CustomValidator>
        </td>
        <td>
            Codice fiscale
        </td>
    </tr>
    <tr>
        <td class="style2">
            <telerik:RadDatePicker
                ID="RadDateInputDal"
                runat="server">
            </telerik:RadDatePicker>
        </td>
        <td class="style4">
            <telerik:RadDatePicker
                ID="RadDateInputAl"
                runat="server">
            </telerik:RadDatePicker>
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxCodiceFiscale"
                runat="server"
                Width="140px"
                MaxLength="16">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <asp:ValidationSummary
                ID="ValidationSummaryRicerca"
                runat="server"
                ValidationGroup="ricercaPresenze"
                CssClass="messaggiErrore" />
        </td>
        <td class="style4">
        </td>
        <td align="right">
            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaPresenze"
                OnClick="ButtonVisualizza_Click" Width="100px" />
        </td>
    </tr>
</table>
<br />
<asp:Panel
    ID="PanelGriglia"
    runat="server"
    Visible="false">
<table class="standardTable">
    <tr>
        <td>
            Filtro impresa 
            <telerik:RadComboBox
                ID="RadComboBoxImpresa"
                runat="server"
                Width="300px"
                AppendDataBoundItems="true" AutoPostBack="True" 
                onselectedindexchanged="RadComboBoxImpresa_SelectedIndexChanged">
            </telerik:RadComboBox>
        </td>
        <td align="right">
            <asp:Button ID="ButtonEstrai" runat="server" Text="Estrai selez." ValidationGroup="ricercaCantieri"
               Width="100px" onclick="ButtonEstrai_Click" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridCantieri" runat="server" AllowPaging="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    OnPageIndexChanged="RadGridCantieri_PageIndexChanged"  
    onitemdatabound="RadGridCantieri_ItemDataBound">
    <GroupingSettings CollapseTooltip="Chiudi gruppo" 
        ExpandTooltip="Espandi gruppo" GroupContinuedFormatString="" 
        GroupContinuesFormatString="" GroupSplitDisplayFormat="" 
        UnGroupButtonTooltip="" UnGroupTooltip="" />
    <ExportSettings>
        <Pdf PageWidth="" />
    </ExportSettings>
    <MasterTableView>
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridTemplateColumn UniqueName="Lavoratore" HeaderText="Lavoratore">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td class="style1">
                                Nominativo:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelCognomeNome" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Data di Nascita:
                            </td>
                            <td>
                                <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Codice Fiscale:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="Impresa" HeaderText="Impresa">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td class="style3">
                                Rag. Soc.:
                            </td>
                            <td>
                                <b>
                                    <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td class="style3">
                                Part. Iva:
                            </td>
                            <td>
                                <asp:Label ID="LabelPartitaIva" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="230px" VerticalAlign="Top" />
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn DataType="System.DateTime" 
                FilterControlAltText="Filter DataPrimoAccesso column" 
                HeaderText="Data primo accesso" UniqueName="DataPrimoAccesso"
                DataField="DataPrimoAccesso" DataFormatString="{0:dd/MM/yyyy}">
                <ItemStyle Width="60px" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataType="System.DateTime" 
                FilterControlAltText="Filter DataUltimoAccesso column" 
                HeaderText="Data ultimo accesso" UniqueName="DataUltimoAccesso"
                DataField="DataUltimoAccesso" DataFormatString="{0:dd/MM/yyyy}">
                <ItemStyle Width="60px" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataType="System.Int32" 
                FilterControlAltText="Filter NumeroPresenze column" HeaderText="N° pres." 
                UniqueName="NumeroPresenze" DataField="NumeroPresenze">
                <ItemStyle Width="40px" HorizontalAlign="Center" />
            </telerik:GridBoundColumn>
        </Columns>
        <GroupByExpressions>
            <telerik:GridGroupByExpression>
                <SelectFields>
                    <telerik:GridGroupByField FieldAlias="Anno" FieldName="Anno" FormatString="" 
                        HeaderText="Anno" />
                    <telerik:GridGroupByField FieldAlias="Mese" FieldName="Mese" FormatString="" 
                        HeaderText="Mese" />
                </SelectFields>
                <GroupByFields>
                    <telerik:GridGroupByField FieldAlias="Anno" FieldName="Anno" FormatString="" 
                        HeaderText="" />
                    <telerik:GridGroupByField FieldAlias="Mese" FieldName="Mese" FormatString="" 
                        HeaderText="" />
                </GroupByFields>
            </telerik:GridGroupByExpression>
        </GroupByExpressions>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
        <PagerStyle PageSizeControlType="RadComboBox" />
    </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
</asp:Panel>
</asp:Panel>
