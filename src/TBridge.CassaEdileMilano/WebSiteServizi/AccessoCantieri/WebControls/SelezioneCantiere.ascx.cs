﻿using System;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Delegates;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;

public partial class AccessoCantieri_WebControls_SelezioneCantiere : UserControl
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
    public event WhiteListCompletaSelectedEventHandler OnCantiereSelected;
    public event CambiaCantiereSelectedEventHandler OnCambiaCantiereSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        RicercaCantieri1.OnCantiereSelected += RicercaCantieri1_OnCantiereSelected;
    }

    private void RicercaCantieri1_OnCantiereSelected(Int32 idWhiteList)
    {
        if (OnCantiereSelected != null)
        {
            ViewState["idDomanda"] = idWhiteList;
            WhiteList whiteList = _biz.GetDomandaByKey(idWhiteList);

            PanelRicerca.Visible = false;
            PanelCantiere.Visible = true;

            DatiSinteticiCantiere1.CaricaDatiCantiere(whiteList);

            OnCantiereSelected(whiteList);
        }
    }

    protected void ButtonAltroCantiere_Click(object sender, EventArgs e)
    {
        DatiSinteticiCantiere1.Reset();

        PanelCantiere.Visible = false;
        PanelRicerca.Visible = true;

        if (OnCambiaCantiereSelected != null)
        {
            OnCambiaCantiereSelected();
        }
    }
}