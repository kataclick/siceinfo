﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiSinteticiCantiere.ascx.cs" Inherits="AccessoCantieri_WebControls_DatiSinteticiCantiere" %>
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>
                Cantiere selezionato
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
        </td>
    </tr>
    <tr>
        <td class="accessoCantieriTemplateTableTd">
            Indirizzo:
        </td>
        <td>
            <b>
                <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Comune:
        </td>
        <td>
            <b>
                <asp:Label ID="LabelComune" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Provincia:
        </td>
        <td>
            <b>
                <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
</table>