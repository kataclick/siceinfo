﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using System.Collections.Generic;

public partial class AccessoCantieri_RiepilogoCantieri : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere);
        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriPerCommittente);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}