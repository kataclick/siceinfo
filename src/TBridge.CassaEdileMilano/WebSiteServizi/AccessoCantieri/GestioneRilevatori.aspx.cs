using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AccessoCantieri_GestioneRilevatori : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        throw new Exception("Pagina non pi� valida");

        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriAmministrazione);

        #endregion
    }
}