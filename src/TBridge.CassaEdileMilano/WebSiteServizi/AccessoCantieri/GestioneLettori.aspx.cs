﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Dto;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

public partial class AccessoCantieri_GestioneLettori : Page
{
    private readonly AccessoCantieriBusiness _accessoCantieriBiz = new AccessoCantieriBusiness();
    //private readonly BizEf _accessoCantieriBizEf = new BizEf();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione);

        if (!Page.IsPostBack)
        {
            CaricaFornitori();
        }

        #region Per prevenire click multipli

        StringBuilder sbInserisci = new StringBuilder();
        sbInserisci.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbInserisci.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbInserisci.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbInserisci.Append("this.value = 'Attendere...';");
        sbInserisci.Append("this.disabled = true;");
        sbInserisci.Append(Page.ClientScript.GetPostBackEventReference(RadButtonInserisci, null));
        sbInserisci.Append(";");
        sbInserisci.Append("return true;");
        RadButtonInserisci.Attributes.Add("onclick", sbInserisci.ToString());

        #endregion
    }

    private void CaricaFornitori()
    {
        FornitoreCollection fornitori = _accessoCantieriBiz.GetFornitori();
        Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxFornitore, fornitori, "RagioneSociale", "idFornitore");
    }

    protected void RadGridLettori_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        List<Lettore> lettori = BizEf.GetLettori();
        RadGridLettori.DataSource = lettori;
    }

    protected void RadGridAssegnazioni_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        int idLettore = Convert.ToInt32(RadTextBoxIdLettore.Text);
        List<AssegnazioneLettore> assegnazioni = BizEf.GetAssegnazioni(idLettore);
        RadGridAssegnazioni.DataSource = assegnazioni;
    }

    protected void RadGridCantieri_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        List<Cantiere> cantieri = BizEf.GetCantieriAttivi();
        RadGridCantieri.DataSource = cantieri;
    }

    protected void RadGridLettori_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "Dettagli")
        {
            int idLettore = (int) e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"];
            MostraDettagli(idLettore);
        }
    }

    protected void RadGridCantieri_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "Associa")
        {
            int idLettore = Convert.ToInt32(RadTextBoxIdLettore.Text);
            int idCantiere = (int) e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"];
            BizEf.AssociaLettore(idLettore, idCantiere);

            MostraDettagli(idLettore);
        }
    }

    protected void RadButtonInserisci_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            _accessoCantieriBiz.InsertRilevatore(RadTextBoxCodiceLettoreInserimento.Text,
                                                 Int32.Parse(RadComboBoxFornitore.SelectedValue),
                                                 CheckBoxInvioWhiteListInserimento.Checked);
            SvuotaCampiInserimento();
            PanelDettagli.Visible = false;
            PanelCantieri.Visible = false;
            RadGridLettori.Rebind();
        }
    }

    private void SvuotaCampiInserimento()
    {
        Presenter.SvuotaCampo(RadTextBoxCodiceLettoreInserimento);
        RadComboBoxFornitore.SelectedValue = String.Empty;
        CheckBoxInvioWhiteListInserimento.Checked = false;
    }

    protected void RadButtonDisassocia_Click(object sender, EventArgs e)
    {
        int idLettore = Convert.ToInt32(RadTextBoxIdLettore.Text);
        BizEf.DisassociaLettore(idLettore);
        MostraDettagli(idLettore);
    }

    protected void RadButtonAssocia_Click(object sender, EventArgs e)
    {
        MostraCantieriAttivi();
    }

    private void MostraDettagli(int idLettore)
    {
        RadButtonDisassocia.Enabled = false;
        RadButtonAssocia.Enabled = false;

        PanelDettagli.Visible = true;
        PanelCantieri.Visible = false;
        RadTextBoxIdLettore.Text = idLettore.ToString();
        Lettore lettore = BizEf.GetLettore(idLettore);
        RadTextBoxCodiceLettore.Text = lettore.Codice;
        RadTextBoxFornitoreLettore.Text = lettore.Fornitore;
        ViewState["CodiceLettore"] = lettore.Codice;
        ViewState["IdFornitore"] = lettore.IdFornitore;
        CheckBoxInvioWhiteList.Checked = lettore.InvioWhiteList;
        RadButtonResetWL.Enabled = lettore.InvioWhiteList;

        AssegnazioneLettore assegnazioneAttiva = BizEf.GetAssegnazioneAttiva(idLettore);
        if (assegnazioneAttiva != null)
        {
            RadButtonDisassocia.Enabled = true;
            RadButtonAssocia.Enabled = false;
        }
        else if (lettore.Assegnabile)
        {
            RadButtonDisassocia.Enabled = false;
            RadButtonAssocia.Enabled = true;
        }

        RadGridAssegnazioni.Rebind();
    }

    private void MostraCantieriAttivi()
    {
        PanelCantieri.Visible = true;
        RadGridCantieri.Rebind();
    }

    protected void RadButtonResetWL_Click(object sender, EventArgs e)
    {
        Int32 idLettore = Convert.ToInt32(RadTextBoxIdLettore.Text);
        String codiceLettore = (String) ViewState["CodiceLettore"];
        TipologiaFornitore fornitore = (TipologiaFornitore) Enum.Parse(typeof(TipologiaFornitore), ViewState["IdFornitore"].ToString());

        _accessoCantieriBiz.DeleteWhiteList(fornitore, codiceLettore);
    }
}