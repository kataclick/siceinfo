﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StampaBadge.aspx.cs" Inherits="AccessoCantieri_StampaBadge" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RicercaCantieri.ascx" TagName="RicercaCantieri" TagPrefix="uc3" %>
<%@ Register Src="WebControls/DatiSinteticiCantiere.ascx" TagName="DatiSinteticiCantiere"
    TagPrefix="uc4" %>
<%@ Register Src="WebControls/SelezioneCantiere.ascx" TagName="SelezioneCantiere"
    TagPrefix="uc5" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <br />
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa Badge"
        titolo="Accesso ai Cantieri" />
    <br />
    <uc5:SelezioneCantiere ID="SelezioneCantiere1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" runat="Server">
    <telerik:RadCodeBlock runat="server">
        <script language="javascript">
            function row_Click(sender, eventArgs) {
                //window.alert('prova');
                var item = eventArgs.get_gridDataItem();
                if (item._element.disabled)
                    eventArgs.set_cancel(true)
            }
        </script>
    </telerik:RadCodeBlock>
    <asp:Panel ID="PanelWhiteList" runat="server" Visible="false" Width="100%">
        <br />
        <br />
        <asp:Label ID="LabelImprese" runat="server" Text="Imprese"></asp:Label>
        <asp:DropDownList ID="DropDownListImpresa" runat="server" Width="300px" AutoPostBack="True"
            OnSelectedIndexChanged="DropDownListImpresa_SelectedIndexChanged" Visible="false">
        </asp:DropDownList>
        <telerik:RadComboBox
            ID="RadComboBoxImpresa"
            runat="server"
            Width="300px"
            AutoPostBack="true" 
            onselectedindexchanged="RadComboBoxImpresa_SelectedIndexChanged">
        </telerik:RadComboBox>
        <br />
        <br />
        <asp:Label ID="LabelImpresaSelezionata" runat="server" Text="Nessuna impresa selezionata"></asp:Label>
        <br />
        <br />
        <asp:Panel ID="PanelLavoratori" runat="server" Visible="false">
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:DropDownList ID="DropDownListFiltro" runat="server" Width="150px" AutoPostBack="true"
                            OnSelectedIndexChanged="DropDownListFiltro_SelectedIndexChanged">
                            <asp:ListItem Value="TUTTI">Tutti</asp:ListItem>
                            <asp:ListItem Value="NOSTAMPA">Non Stampati</asp:ListItem>
                            <asp:ListItem Value="STAMPA">Già Stampati</asp:ListItem>
                            <asp:ListItem Value="DASTAMPARE">Da stampare</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        il
                        <telerik:RadDateInput
                            ID="RadDateInputStampatiIl"
                            runat="server"
                            Width="150px"
                            Enabled="false" MinDate="2010-01-01">
                        </telerik:RadDateInput>
                        &nbsp;
                        <asp:Button
                            ID="ButtonAggiornaDataStampa"
                            runat="server"
                            Text="Aggiorna" 
                            Enabled="false"
                            onclick="ButtonAggiornaDataStampa_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadGrid ID="RadGridLavoratori" runat="server" Width="100%" GridLines="None"
                            AllowMultiRowSelection="true" AllowPaging="True" PageSize="15" OnItemDataBound="RadGridLavoratori_ItemDataBound"
                            AllowSorting="true" Font-Size="Small" Visible="true" 
                            OnPageIndexChanged="RadGridLavoratori_PageIndexChanged1">
                            <MasterTableView DataKeyNames="TipoLavoratore, Nome, Cognome, PaeseNascita, LuogoNascita, ProvinciaNascita, DataNascita, DataAssunzione, Foto, CodiceFiscale, IdLavoratore, Impresa">
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" />
                                    <telerik:GridTemplateColumn HeaderText="Lavoratore/i" UniqueName="Lavoratore/i">
                                        <ItemStyle />
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <b>Cod.: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Nome e cognome: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelLavoratoreNomeCognome" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Luogo nascita: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelLavoratoreLuogoNascita" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Data nascita: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelLavoratoreDataNascita" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Foto: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Image ID="ImageFoto" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                                        <ItemStyle />
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <b>Rag. Soc.: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="RagioneSociale" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>P.IVA: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="PIva" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Aut. n°: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="AutorizzazioneAlSubappalto" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Data assunzione: </b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="DataAssunzione" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <%-- <telerik:GridTemplateColumn>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageStampato" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="Stampa" UniqueName="Stampa">
                                        <ItemStyle />
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        Stampato il
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <small>
                                                            <asp:Label ID="DataStampaBadge" runat="server"></asp:Label>
                                                        </small>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="10px" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <ClientSettings EnableRowHoverStyle="true">
                                <Selecting AllowRowSelect="True" />
                                <ClientEvents OnRowSelecting="row_Click" />
                            </ClientSettings>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonStampaLavoratori" runat="server" OnClick="ButtonStampaLavoratori_Click"
                            Text="Esporta i dati per la stampa" Width="200px" />
                        &nbsp;
                        <asp:Button ID="ButtonResettaDataStampa" runat="server"
                            Text="Resetta data stampa" Width="200px" 
                            onclick="ButtonResettaDataStampa_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelNoSel" runat="server" CssClass="messaggiErrore" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
            <%--          <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                OnRowDataBound="GridViewLavoratori_RowDataBound" Width="100%" OnRowDeleting="GridViewLavoratori_RowDeleting"
                DataKeyNames="IdDomandaLavoratore" OnRowCommand="GridViewLavoratori_RowCommand"
                OnSelectedIndexChanged="GridViewLavoratori_SelectedIndexChanged">
                <Columns>
                    <asp:TemplateField HeaderText="Lavoratore/i">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <b>Cod.: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Nome e cognome: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelLavoratoreNomeCognome" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Luogo nascita: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelLavoratoreLuogoNascita" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Data nascita: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelLavoratoreDataNascita" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Foto: </b>
                                    </td>
                                    <td>
                                        <asp:Image ID="ImageFoto" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Impresa">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <b>Rag. Soc.: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="RagioneSociale" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>P.IVA: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PIva" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Aut. al subappalto: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="AutorizzazioneAlSubappalto" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Data assunzione: </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="DataAssunzione" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField AccessibleHeaderText="Stampa">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButtonStampa" runat="server" CommandName="Stampa" ImageUrl="~/images/printer.png" />
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>--%>
        </asp:Panel>
        <asp:Panel ID="PanelLavoratoriAutonomi" runat="server" Visible="false">
            <table>
                <tr>
                    <td>
                        <b>Lavoratore autonomo</b>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Nome e cognome:
                    </td>
                    <td>
                        <asp:Label ID="LabelNomeCognomeAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Luogo nascita:
                    </td>
                    <td>
                        <asp:Label ID="LabelLuogoNascitaAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Data nascita:
                    </td>
                    <td>
                        <asp:Label ID="LabelDataNascitaAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Rag.Soc.:
                    </td>
                    <td>
                        <asp:Label ID="LabelRagioneSocialeAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cod.Fisc.:
                    </td>
                    <td>
                        <asp:Label ID="LabelCodiceFiscaleAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        P.IVA:
                    </td>
                    <td>
                        <asp:Label ID="LabelPIvaAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Foto:
                    </td>
                    <td>
                        <asp:Image ID="ImageFotoAutonomo" runat="server"></asp:Image>
                    </td>
                </tr>
                <tr>
                    <td>
                        Aut. n°:
                    </td>
                    <td>
                        <asp:Label ID="LabelAutSubappaltoAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Committente:
                    </td>
                    <td>
                        <asp:Label ID="LabelCommittenteAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>
             <%--   <tr>
                    <td>
                        Data assunzione:
                    </td>
                    <td>
                        <asp:Label ID="LabelDataAssunzioneAutonomo" runat="server"></asp:Label>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Data ultima stampa badge:
                    </td>
                    <td>
                        <asp:Label ID="LabelDataStampaBadge" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button ID="ButtonStampaAutonomo" runat="server" Text="Esporta per la stampa"
                            Width="200px" OnClick="ButtonStampaAutonomo_Click" Enabled="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
