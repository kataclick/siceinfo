﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ControlliCexChange.aspx.cs" Inherits="AccessoCantieri_ControlliCexChange" 
 MasterPageFile="~/MasterPage.master"%>

 <%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/AccessoCantieriControlliCexChange.ascx" TagName="AccessoCantieriControlliCexChange"
    TagPrefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <div>
        <br />
        <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controlli CEMI"
                                titolo="Accesso ai Cantieri" />
        <br />
    </div>
    <div>
        <uc3:AccessoCantieriControlliCexChange ID="AccessoCantieriControlliCexChange1" runat="server"/>
    </div>
</asp:Content> 