﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneLettori.aspx.cs" Inherits="AccessoCantieri_GestioneLettori"
    EnableEventValidation="false" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione lettori"
        titolo="Accesso cantieri" />
    <br />
    <asp:Panel runat="server" ID="PanelInserimento" BorderStyle="Solid" BorderWidth="1">
        Inserimento nuovo lettore
        <table class="standardTable">
            <tr>
                <td>
                    Codice Lettore
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxCodiceLettoreInserimento" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceLettore" runat="server"
                        ErrorMessage="*" ControlToValidate="RadTextBoxCodiceLettoreInserimento" ValidationGroup="InserimentoLettore" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Fornitore
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxFornitore" runat="server" AppendDataBoundItems />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorFornitore" runat="server"
                        ErrorMessage="*" ControlToValidate="RadComboBoxFornitore" ValidationGroup="InserimentoLettore" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Invio WhiteList
                </td>
                <td>
                    <asp:CheckBox
                        ID="CheckBoxInvioWhiteListInserimento"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <telerik:RadButton runat="server" ID="RadButtonInserisci" Text="Inserisci" OnClick="RadButtonInserisci_Click" ValidationGroup="InserimentoLettore" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <telerik:RadGrid ID="RadGridLettori" runat="server" AutoGenerateColumns="False" AllowPaging="true"
        AllowSorting="true" AllowFilteringByColumn="true" OnItemCommand="RadGridLettori_ItemCommand"
        OnNeedDataSource="RadGridLettori_NeedDataSource">
        <GroupingSettings CaseSensitive="false" />
        <MasterTableView DataKeyNames="Id">
            <Columns>
                <telerik:GridBoundColumn DataField="Codice" HeaderText="Codice" />
                <telerik:GridBoundColumn DataField="Fornitore" HeaderText="Fornitore" />
                <telerik:GridCheckBoxColumn DataField="Assegnabile" HeaderText="Assegnabile" />
                <telerik:GridBoundColumn DataField="DataInserimento" HeaderText="Data Inserimento" />
                <telerik:GridCheckBoxColumn DataField="InvioWhiteList" HeaderText="Invio WhiteList" />
                <telerik:GridButtonColumn CommandName="Dettagli" Text="Dettagli" ButtonType="PushButton">
                    <ItemStyle Width="10px" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    <asp:Panel runat="server" ID="PanelDettagli" BorderStyle="Solid" BorderWidth="1"
        Visible="false">
        <table class="standardTable">
            <tr>
                <td>
                    Id
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="RadTextBoxIdLettore" Enabled="false" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Codice
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="RadTextBoxCodiceLettore" Enabled="false" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Fornitore
                </td>
                <td>
                    <telerik:RadTextBox runat="server" ID="RadTextBoxFornitoreLettore" Enabled="false" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Invio WhiteList
                </td>
                <td>
                    <asp:CheckBox
                        ID="CheckBoxInvioWhiteList"
                        runat="server"
                        Enabled="false" />
                    &nbsp;
                    <telerik:RadButton runat="server" ID="RadButtonResetWL" Text="Reset WL" 
                        CausesValidation="false" Enabled="false" onclick="RadButtonResetWL_Click" />
                </td>
            </tr>
        </table>
        <br />
        Assegnazioni:
        <br />
        <telerik:RadGrid ID="RadGridAssegnazioni" runat="server" AutoGenerateColumns="false"
            AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" OnNeedDataSource="RadGridAssegnazioni_NeedDataSource">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView>
                <Columns>
                    <telerik:GridBoundColumn DataField="Utente" HeaderText="Utente" />
                    <telerik:GridBoundColumn DataField="IndirizzoCantiere" HeaderText="Cantiere" />
                    <telerik:GridBoundColumn DataField="DataInizio" HeaderText="Data Inizio" />
                    <telerik:GridBoundColumn DataField="DataFine" HeaderText="Data Fine" />
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>  
        <br />
        <table class="standardTable">
            <tr>
                <td>
                    Gestione:
                </td>
                <td>
                    <telerik:RadButton runat="server" ID="RadButtonAssocia" Text="Associa" 
                        Enabled="false" onclick="RadButtonAssocia_Click" CausesValidation="false" />
                </td>
                <td>
                    <telerik:RadButton runat="server" ID="RadButtonDisassocia" Text="Disassocia" 
                        Enabled="false" onclick="RadButtonDisassocia_Click" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <br />
        <asp:Panel runat="server" ID="PanelCantieri" Visible="false">
            <telerik:RadGrid ID="RadGridCantieri" runat="server" AutoGenerateColumns="false"
            AllowPaging="true" AllowSorting="true" AllowFilteringByColumn="true" 
            OnNeedDataSource="RadGridCantieri_NeedDataSource"
            OnItemCommand="RadGridCantieri_ItemCommand" >
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView DataKeyNames="Id">
                <Columns>
                    <telerik:GridBoundColumn DataField="Utente" HeaderText="Utente" />
                    <telerik:GridBoundColumn DataField="IndirizzoCantiere" HeaderText="Cantiere" />
                    <telerik:GridBoundColumn DataField="DataInizio" HeaderText="Data Inizio" />
                    <telerik:GridBoundColumn DataField="DataFine" HeaderText="Data Fine" />
                    <telerik:GridButtonColumn CommandName="Associa" Text="Associa" ButtonType="PushButton">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
