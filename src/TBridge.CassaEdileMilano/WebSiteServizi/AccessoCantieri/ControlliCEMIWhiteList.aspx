﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ControlliCEMIWhiteList.aspx.cs" Inherits="AccessoCantieri_ControlliCEMIWhiteList" %>

<%@ Register src="../WebControls/MenuAccessoCantieri.ascx" tagname="MenuAccessoCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/RicercaCantieri.ascx" tagname="RicercaCantieri" tagprefix="uc3" %>
<%@ Register src="WebControls/DatiSinteticiCantiere.ascx" tagname="DatiSinteticiCantiere" tagprefix="uc4" %>
<%@ Register src="WebControls/ControlliWhiteList.ascx" tagname="ControlliWhiteList" tagprefix="uc5" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso ai Cantieri" sottoTitolo="Controlli CEMI White List" />
    <br />
    <asp:Panel ID="PanelRicerca" runat="server" Visible="true" Width="100%">
        <uc3:RicercaCantieri ID="RicercaCantieri1" runat="server" />
    </asp:Panel>
    <asp:Panel ID="PanelCantiere" runat="server" Visible="false" Width="100%">
        <uc4:DatiSinteticiCantiere ID="DatiSinteticiCantiere1" runat="server" />
        <asp:Button ID="ButtonAltroCantiere" runat="server" Width="150px" 
            Text="Cambia cantiere" onclick="ButtonAltroCantiere_Click" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" Runat="Server">
    <asp:Panel ID="PanelWhiteList" runat="server" Visible="false" Width="100%">
        <br />
        <uc5:ControlliWhiteList ID="ControlliWhiteList1" runat="server" />
    </asp:Panel>
</asp:Content>

