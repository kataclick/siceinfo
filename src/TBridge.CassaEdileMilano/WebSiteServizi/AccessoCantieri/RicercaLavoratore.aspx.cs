﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AccessoCantieri_RicercaLavoratore : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SelezioneCantiere1.OnCantiereSelected += new TBridge.Cemi.AccessoCantieri.Type.Delegates.WhiteListCompletaSelectedEventHandler(SelezioneCantiere1_OnCantiereSelected);
        SelezioneCantiere1.OnCambiaCantiereSelected += new TBridge.Cemi.AccessoCantieri.Type.Delegates.CambiaCantiereSelectedEventHandler(SelezioneCantiere1_OnCambiaCantiereSelected);
    }

    void SelezioneCantiere1_OnCambiaCantiereSelected()
    {
        PanelRicercaLavoratore.Visible = false;
    }

    void SelezioneCantiere1_OnCantiereSelected(TBridge.Cemi.AccessoCantieri.Type.Entities.WhiteList whiteList)
    {
        RicercaLavoratore1.CantiereSelezionato(whiteList.IdWhiteList.Value);
        PanelRicercaLavoratore.Visible = true;
    }
}