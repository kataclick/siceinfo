﻿using System;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AccessoCantieri_ControlliCEMIWhiteList : Page
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere);

        RicercaCantieri1.OnCantiereSelected += RicercaCantieri1_OnCantiereSelected;
    }

    private void RicercaCantieri1_OnCantiereSelected(Int32 idWhiteList)
    {
        WhiteList whiteList = _biz.GetDomandaByKey(idWhiteList);

        PanelRicerca.Visible = false;
        DatiSinteticiCantiere1.CaricaDatiCantiere(whiteList);
        ControlliWhiteList1.CaricaWhiteList(whiteList);
        PanelCantiere.Visible = true;
        PanelWhiteList.Visible = true;
    }

    protected void ButtonAltroCantiere_Click(object sender, EventArgs e)
    {
        DatiSinteticiCantiere1.Reset();
        ControlliWhiteList1.Reset();
        PanelCantiere.Visible = false;
        PanelWhiteList.Visible = false;
        PanelRicerca.Visible = true;
    }
}