﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RicercaLavoratore.aspx.cs" Inherits="AccessoCantieri_RicercaLavoratore" %>

<%@ Register src="../WebControls/MenuAccessoCantieri.ascx" tagname="MenuAccessoCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/SelezioneCantiere.ascx" tagname="SelezioneCantiere" tagprefix="uc3" %>
<%@ Register src="WebControls/RicercaLavoratore.ascx" tagname="RicercaLavoratore" tagprefix="uc4" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso Cantieri" sottoTitolo="Ricerca lavoratore" />
    <br />
    <br />
    <uc3:SelezioneCantiere ID="SelezioneCantiere1" runat="server" />
    <br />
    <asp:Panel
        ID="PanelRicercaLavoratore"
        runat="server"
        Visible="false">
        <uc4:RicercaLavoratore ID="RicercaLavoratore1" runat="server" />
    </asp:Panel>
</asp:Content>


