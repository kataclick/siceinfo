﻿<%@ Page Title="Accesso Cantieri - Gestione cantiere" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="GestioneCantiere.aspx.cs" Inherits="AccessoCantieri_GestioneCantiere"
    EnableEventValidation="true" MaintainScrollPositionOnPostBack="true"%>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AccessoCantieriCantiere.ascx" TagName="AccessoCantieriCantiere"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/AccessoCantieriImprese.ascx" TagName="AccessoCantieriImprese"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/AccessoCantieriImpresePeriodo.ascx" TagName="AccessoCantieriImpresePeriodo"
    TagPrefix="uc11" %>
<%@ Register Src="../WebControls/AccessoCantieriLavoratore.ascx" TagName="AccessoCantieriLavoratore"
    TagPrefix="uc5" %>
<%@ Register Src="../WebControls/AccessoCantieriDati.ascx" TagName="AccessoCantieriDati"
    TagPrefix="uc6" %>
<%@ Register Src="../WebControls/AccessoCantieriConferma.ascx" TagName="AccessoCantieriConferma"
    TagPrefix="uc7" %>
<%@ Register Src="../WebControls/AccessoCantieriAltrePersone.ascx" TagName="AccessoCantieriAltrePersone"
    TagPrefix="uc8" %>
<%@ Register Src="../WebControls/AccessoCantieriAssegnazioneRilevatori.ascx" TagName="AccessoCantieriAssegnazioneRilevatori"
    TagPrefix="uc9" %>
<%@ Register Src="../WebControls/AccessoCantieriReferenti.ascx" TagName="AccessoCantieriReferenti"
    TagPrefix="uc10" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso ai Cantieri"
        sottoTitolo="Gestione Cantiere" />
    <br />
    <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" BorderWidth="1px" Font-Names="Verdana"
        Height="200px" Width="100%" SideBarStyle-Width="120px" OnNextButtonClick="Wizard1_NextButtonClick"
        OnFinishButtonClick="Wizard1_FinishButtonClick" OnSideBarButtonClick="Wizard1_SideBarButtonClick"
        OnPreviousButtonClick="Wizard1_PreviousButtonClick">
        <StepStyle VerticalAlign="Top" />
        <SideBarStyle Width="120px"></SideBarStyle>
        <StartNavigationTemplate>
            <asp:Button ID="btnSuccessivoStart" runat="server" Text="Avanti" CommandName="MoveNext"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StepNavigationTemplate>
        <FinishNavigationTemplate>
            <asp:Button ID="btnPrecedenteFinish" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Conferma salvataggio"
                Width="150px" TabIndex="1" ValidationGroup="stop" />
        </FinishNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep ID="WizardStepCantiere" runat="server" Title="- Cantiere">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Cantiere </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorIndirizzo" runat="server" ErrorMessage="Non è stato comunicato l'indirizzo del cantiere"
                                OnServerValidate="CustomValidatorIndirizzo_ServerValidate" ValidationGroup="stop">&nbsp;</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc3:AccessoCantieriCantiere ID="AccessoCantieriCantiere1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErroriCantieri" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepReferenti" runat="server" Title="- Referenti">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Referenti del cantiere </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc10:AccessoCantieriReferenti ID="AccessoCantieriReferenti1" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepImprese" runat="server" Title="- Imprese">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Imprese </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc4:AccessoCantieriImprese ID="AccessoCantieriImprese1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErroriImprese" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepImpresePeriodo" runat="server" Title="- Imprese Periodi">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Imprese Periodi </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc11:AccessoCantieriImpresePeriodo ID="AccessoCantieriImpresePeriodo1" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepLavoratori" runat="server" Title="- Lavoratori">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Lavoratori </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc5:AccessoCantieriLavoratore ID="AccessoCantieriLavoratore1" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepAltrePersone" runat="server" Title="- Altre persone">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Altre persone abilitate all'accesso </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc8:AccessoCantieriAltrePersone ID="AccessoCantieriAltrePersone1" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepAssegnazioneRilevatori" runat="server" Title="- Rilevatori" >
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Assegnazione rilevatori </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc9:AccessoCantieriAssegnazioneRilevatori ID="AccessoCantieriAssegnazioneRilevatori1"
                                runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepConferma" runat="server" Title="- Salvataggio">
                <table class="standardTable">
                    <tr>
                        <td>
                            <b>Salvataggio </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc7:AccessoCantieriConferma ID="AccessoCantieriConferma1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelGiaInserita" runat="server" ForeColor="Red" Text="Il cantiere è già stato gestito. Per gestirne un nuovo cliccare sulla voce &quot;Gestione Cantiere&quot; nel menu laterale sinistro."
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
        </WizardSteps>
        <HeaderStyle BorderStyle="Solid" BorderWidth="2px" HorizontalAlign="Center" />
    </asp:Wizard>
    <br />
</asp:Content>
