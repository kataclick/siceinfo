﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class AccessoCantieri_Presenze : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AccessoCantieriControlli,
                                                             FunzionalitaPredefinite.AccessoCantieriPerCommittente
                                                         };
        
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        SelezioneCantiere1.OnCantiereSelected += new TBridge.Cemi.AccessoCantieri.Type.Delegates.WhiteListCompletaSelectedEventHandler(SelezioneCantiere1_OnCantiereSelected);
        SelezioneCantiere1.OnCambiaCantiereSelected += new TBridge.Cemi.AccessoCantieri.Type.Delegates.CambiaCantiereSelectedEventHandler(SelezioneCantiere1_OnCambiaCantiereSelected);
    }

    void SelezioneCantiere1_OnCambiaCantiereSelected()
    {
        Presenze1.Visible = false;
    }

    void SelezioneCantiere1_OnCantiereSelected(TBridge.Cemi.AccessoCantieri.Type.Entities.WhiteList whiteList)
    {
        Presenze1.SetIdCantiere(whiteList.IdWhiteList.Value);
        Presenze1.Visible = true;
    }
}