using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AccessoCantieri_ReportAccessoCantieriImprese : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriPerImpresa);

        #endregion

        if (!Page.IsPostBack)
        {
            ReportViewer1.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            ReportViewer1.ServerReport.ReportPath =
                "/ReportAccessoCantieri/ReportAccessoCantieriTimbratureFiltroImpresa";

            DateTime? dataInizio = null;
            if (Context.Items["dataInizio"] != null)
                dataInizio = (DateTime?) Context.Items["dataInizio"];

            DateTime? dataFine = null;
            if (Context.Items["dataFine"] != null)
                dataFine = (DateTime?) Context.Items["dataFine"];

            Int32? idImpresa = null;
            if (Context.Items["idImpresa"] != null)
                idImpresa = (Int32?) Context.Items["idImpresa"];

            String ragioneSocialeImpresa = string.Empty;
            if (Context.Items["ragioneSocialeImpresa"] != null)
                ragioneSocialeImpresa = (String) Context.Items["ragioneSocialeImpresa"];

            string codiceFiscale = string.Empty;
            if (Context.Items["codiceFiscale"] != null)
                codiceFiscale = (string) Context.Items["codiceFiscale"];

            ReportParameter[] listaParam = new ReportParameter[5];

            string nothing = null;


            if (dataInizio != null)
            {
                listaParam[0] = new ReportParameter("dataInizio", dataInizio.ToString());
            }
            else
            {
                listaParam[0] = new ReportParameter("dataInizio", nothing);
            }

            if (dataFine != null)
            {
                listaParam[1] = new ReportParameter("dataFine", dataFine.ToString());
            }
            else
            {
                listaParam[1] = new ReportParameter("dataFine", nothing);
            }

            if (idImpresa != null)
            {
                listaParam[2] = new ReportParameter("idImpresa", idImpresa.ToString());
            }
            else
            {
                listaParam[2] = new ReportParameter("idImpresa", nothing);
            }

            if (ragioneSocialeImpresa != string.Empty)
            {
                listaParam[3] = new ReportParameter("ragioneSocialeImpresa", ragioneSocialeImpresa);
            }
            else
            {
                listaParam[3] = new ReportParameter("ragioneSocialeImpresa", nothing);
            }

            if (codiceFiscale != string.Empty)
            {
                listaParam[4] = new ReportParameter("codiceFiscale", codiceFiscale);
            }
            else
            {
                listaParam[4] = new ReportParameter("codiceFiscale", nothing);
            }

            ReportViewer1.ServerReport.SetParameters(listaParam);
        }
    }
}