﻿<%@ Page Title="Accesso Cantieri" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="AccessoCantieriDefault.aspx.cs" Inherits="AccessoCantieri_AccessoCantieriDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Benvenuto nella sezione Accesso ai Cantieri"
        titolo="Accesso ai Cantieri" />
    <p>
        Il sistema di monitoraggio telematico delle presenze in cantiere consente all’impresa
        di appurare che l’ingresso di <strong>tutto</strong> il personale impiegato nell’unità
        produttiva sia autorizzato e di verificare la regolarità dei rapporti di lavoro
        delle maestranze presenti in cantiere con contratto dell’edilizia.
    </p>
</asp:Content>
