﻿<%@ Page Title="Accesso Cantieri - Riepilogo cantieri" Language="C#" MasterPageFile="~/MasterPage.master"
    AutoEventWireup="true" CodeFile="RiepilogoCantieri.aspx.cs" Inherits="AccessoCantieri_RiepilogoCantieri" MaintainScrollPositionOnPostBack="true"%>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AccessoCantieriRicerca.ascx" TagName="AccessoCantieriRicerca"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso ai cantieri"
        sottoTitolo="Riepilogo cantieri" />
    <br />
    <uc3:AccessoCantieriRicerca ID="AccessoCantieriRicerca1" runat="server" />
    <br />
</asp:Content>
