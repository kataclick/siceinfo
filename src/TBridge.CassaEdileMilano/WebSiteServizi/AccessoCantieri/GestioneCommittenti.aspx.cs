﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.AccessoCantieri.Business;

public partial class AccessoCantieri_GestioneCommittenti : System.Web.UI.Page
{
    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione);
        #endregion

        SelezioneCantiere1.OnCantiereSelected += new TBridge.Cemi.AccessoCantieri.Type.Delegates.WhiteListCompletaSelectedEventHandler(SelezioneCantiere1_OnCantiereSelected);
        SelezioneCantiere1.OnCambiaCantiereSelected += new TBridge.Cemi.AccessoCantieri.Type.Delegates.CambiaCantiereSelectedEventHandler(SelezioneCantiere1_OnCambiaCantiereSelected);
        RicercaCommittenti1.OnCommittenteSelected += new TBridge.Cemi.Type.Delegates.CommittenteSelectedEventHandler(RicercaCommittenti1_OnCommittenteSelected);
    }

    void SelezioneCantiere1_OnCantiereSelected(WhiteList whiteList)
    {
        ViewState["IdWhiteList"] = whiteList.IdWhiteList.Value;
        CaricaCommittente(whiteList);
    }

    void SelezioneCantiere1_OnCambiaCantiereSelected()
    {
        RicercaCommittenti1.Visible = false;
        ButtonSeleziona.Visible = false;
        Committente1.Visible = false;
    }

    void RicercaCommittenti1_OnCommittenteSelected(TBridge.Cemi.Type.Domain.Committente committente)
    {
        RicercaCommittenti1.Visible = false;

        Int32 idWhiteList = (Int32) ViewState["IdWhiteList"];
        BizEf.AssociaCommittente(idWhiteList, committente.IdCommittente);
        WhiteList wl = _biz.GetDomandaByKey(idWhiteList);

        CaricaCommittente(wl);
    }

    private void CaricaCommittente(WhiteList whiteList)
    {
        Committente1.Visible = true;
        ButtonSeleziona.Visible = true;
        Committente1.CaricaCommittente(whiteList.CommittenteUtente);
    }

    protected void ButtonSeleziona_Click(object sender, EventArgs e)
    {
        RicercaCommittenti1.Visible = true;
    }
}