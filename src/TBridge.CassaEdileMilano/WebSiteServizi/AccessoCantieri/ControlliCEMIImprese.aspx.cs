﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AccessoCantieri_ControlliCEMIImprese : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriPerImpresa);

        #endregion
    }
}