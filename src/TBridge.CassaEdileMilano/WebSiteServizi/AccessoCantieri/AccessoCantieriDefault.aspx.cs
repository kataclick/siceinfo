using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AccessoCantieri_AccessoCantieriDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere);
        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriPerImpresa);
        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriAmministrazione);
        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriControlli);
        funzionalita.Add(FunzionalitaPredefinite.AccessoCantieriPerCommittente);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion
    }
}