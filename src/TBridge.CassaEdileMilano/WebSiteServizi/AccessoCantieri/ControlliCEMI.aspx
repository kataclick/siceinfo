﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlliCEMI.aspx.cs" Inherits="AccessoCantieri_ControlliCEMI" Theme="CETheme2009Wide" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AccessoCantieriControlliCEMI.ascx" TagName="AccessoCantieriControlliCEMI"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <br />
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controlli CEMI"
        titolo="Accesso ai Cantieri" />
    <br />
    <uc3:AccessoCantieriControlliCEMI ID="AccessoCantieriControlliCEMI1" runat="server" />
</asp:Content>
