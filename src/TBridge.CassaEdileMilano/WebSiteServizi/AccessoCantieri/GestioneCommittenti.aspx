﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneCommittenti.aspx.cs" Inherits="AccessoCantieri_GestioneCommittenti" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>

<%@ Register src="WebControls/SelezioneCantiere.ascx" tagname="SelezioneCantiere" tagprefix="uc2" %>

<%@ Register src="WebControls/Committente.ascx" tagname="Committente" tagprefix="uc3" %>

<%@ Register src="../WebControls/RicercaCommittenti.ascx" tagname="RicercaCommittenti" tagprefix="uc4" %>

<%@ Register src="../WebControls/MenuAccessoCantieri.ascx" tagname="MenuAccessoCantieri" tagprefix="uc5" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc5:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso ai Cantieri" sottoTitolo="Gestione Committenti" />
    <br />
    <uc2:SelezioneCantiere ID="SelezioneCantiere1" runat="server" />
    <br />
    <br />
    <uc3:Committente ID="Committente1" runat="server" Visible="false" />
    <asp:Button
        ID="ButtonSeleziona"
        runat="server"
        Text="Seleziona"
        Width="150px" 
        Visible="false"
        onclick="ButtonSeleziona_Click" />
    <uc4:RicercaCommittenti ID="RicercaCommittenti1" runat="server" Visible="false" />
</asp:Content>


