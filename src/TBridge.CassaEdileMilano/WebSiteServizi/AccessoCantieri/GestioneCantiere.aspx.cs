using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.SelestaProvider;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Exceptions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;

public partial class AccessoCantieri_GestioneCantiere : Page
{
    private const Int32 INDICEDATI = 0;
    private const Int32 INDICEREFERENTI = 1;
    private const Int32 INDICEIMPRESE = 2;
    private const Int32 INDICEIMPRESEPERIODO = 3;
    private const Int32 INDICELAVORATORI = 4;
    private const Int32 INDICEALTREPERSONE = 5;
    private const Int32 INDICERILEVATORI = 6;
    private const Int32 INDICECONFERMA = 7;

    private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
    private static readonly TymbroConnector TymbroBiz = new TymbroConnector();
    private static readonly TrexomConnector TrexomBiz = new TrexomConnector();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Request["op"] == null)
        {
            Wizard1.WizardSteps.Remove(WizardStepAssegnazioneRilevatori);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere);

        #endregion

        #region Cambio colore link della sidebar

        Control lst1 = Wizard1.FindControl("SideBarContainer");
        DataList lst = (DataList) lst1.FindControl("SideBarList");
        lst.ItemCreated += lst_ItemCreated;

        #endregion

        AccessoCantieriLavoratore1.OnLavoratoreDeleted += new TBridge.Cemi.AccessoCantieri.Type.Delegates.LavoratoriDeletedEventHandler(AccessoCantieriLavoratore1_OnLavoratoreDeleted);
        AccessoCantieriAltrePersone1.OnLavoratoreDeleted += new TBridge.Cemi.AccessoCantieri.Type.Delegates.LavoratoriDeletedEventHandler(AccessoCantieriAltrePersone1_OnLavoratoreDeleted);
        
        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                ViewState["IdDomanda"] = Context.Items["IdDomanda"];
                ViewState["Modifica"] = true;

                CaricaDomanda((Int32) ViewState["IdDomanda"]);
            }

            ViewState["indiceWizard"] = 0;
            ViewState["GuidId"] = Guid.NewGuid();
        }

        #region Click multipli

        // Per prevenire click multipli
        Button bConferma =
            (Button)
            Wizard1.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(bConferma, null) + ";");
        sb.Append("return true;");
        bConferma.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(bConferma);

        #endregion
    }

    void AccessoCantieriAltrePersone1_OnLavoratoreDeleted(string codiceFiscale)
    {
        AggiungiLavoratoreEliminato(codiceFiscale);
    }

    void AccessoCantieriLavoratore1_OnLavoratoreDeleted(string codiceFiscale)
    {
        AggiungiLavoratoreEliminato(codiceFiscale);
    }

    private void AggiungiLavoratoreEliminato(String codiceFiscale)
    {
        List<String> lavoratoriCancellati = null;

        if (ViewState["LavoratoriCancellati"] == null)
        {
            lavoratoriCancellati = new List<String>();
        }
        else
        {
            lavoratoriCancellati = (List<String>) ViewState["LavoratoriCancellati"];
        }

        lavoratoriCancellati.Add(codiceFiscale);

        ViewState["LavoratoriCancellati"] = lavoratoriCancellati;
    }

    private void CaricaDomanda(int idDomanda)
    {
        WhiteList domanda = _biz.GetDomandaByKey(idDomanda);
        if (domanda == null) throw new NotImplementedException();
        const bool copia = false;

        AccessoCantieriCantiere1.CaricaDati(domanda, copia);
        AccessoCantieriImprese1.CaricaImprese(domanda.Subappalti);
        AccessoCantieriImprese1.CaricaDomanda(domanda.IdWhiteList);
        if (domanda.IdWhiteList != null)
        {
            ImpresaCollection imprese = _biz.GetImpreseSelezionateInSubappalto(domanda.IdWhiteList.Value);
            AccessoCantieriImpresePeriodo1.CaricaImprese(domanda.Subappalti);
            AccessoCantieriLavoratore1.CaricaImprese(imprese, domanda.Subappalti);
        }
        AccessoCantieriLavoratore1.CaricaDomanda(domanda.IdWhiteList);
        //WhiteListImpresaCollection domImpColl = _biz.GetLavoratoriInDomanda(idDomanda);
        WhiteListImpresaCollection domImpColl = _biz.GetLavoratoriInDomandaNoFoto(idDomanda);
        AccessoCantieriLavoratore1.CaricaLavoratori(domImpColl);
        if (domanda.IdWhiteList != null)
        {
            AltraPersonaCollection altrePersone = _biz.GetAltrePersone(domanda.IdWhiteList.Value);
            AccessoCantieriAltrePersone1.CaricaAltrePersone(altrePersone);
        }
        if (domanda.IdWhiteList != null)
        {
            ReferenteCollection referenti = _biz.GetReferenti(domanda.IdWhiteList.Value);
            AccessoCantieriReferenti1.CaricaReferenti(referenti);
        }
        if (domanda.IdWhiteList != null)
        {
            RilevatoreCantiereCollection rilevatori = _biz.GetRilevatoriCantieri(domanda.IdWhiteList.Value);
            AccessoCantieriAssegnazioneRilevatori1.CaricaRilevatori(rilevatori);
        }
    }

    protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        IndirizzoCollection indirizzi = AccessoCantieriCantiere1.GetIndirizzi();

        if (indirizzi == null || indirizzi.Count == 0)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
        else
        {
            ViewState["indiceWizard"] = e.NextStepIndex;

            if (e.NextStepIndex == INDICEIMPRESE)
            {
                AccessoCantieriImprese1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                AccessoCantieriImprese1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());
            }

            if (e.NextStepIndex == INDICEIMPRESEPERIODO)
            {
                AccessoCantieriImpresePeriodo1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                AccessoCantieriImpresePeriodo1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());

                //SubappaltoCollection subColl = AccessoCantieriImprese1.GetSubappalti();
                SubappaltoCollection subappalti = AccessoCantieriImprese1.GetSubappalti();
                SubappaltoCollection subappaltiPeriodo = AccessoCantieriImpresePeriodo1.GetSubappalti();

                foreach (Subappalto subappalto in subappalti)
                {
                    if (subappaltiPeriodo != null)
                    {
                        foreach (Subappalto subappaltoPeriodo in subappaltiPeriodo)
                        {
                            //if (subappalto.Equals(subappaltoPeriodo))
                            if (subappalto.PartitaIvaAppaltata == subappaltoPeriodo.PartitaIvaAppaltata &&
                                subappalto.PartitaIvaAppaltatrice == subappaltoPeriodo.PartitaIvaAppaltatrice)
                            {
                                subappalto.DataInizioAttivitaAppaltata = subappaltoPeriodo.DataInizioAttivitaAppaltata;
                                subappalto.DataFineAttivitaAppaltata = subappaltoPeriodo.DataFineAttivitaAppaltata;

                                subappalto.DataInizioAttivitaAppaltatrice =
                                    subappaltoPeriodo.DataInizioAttivitaAppaltatrice;
                                subappalto.DataFineAttivitaAppaltatrice = subappaltoPeriodo.DataFineAttivitaAppaltatrice;

                                if (subappaltoPeriodo.Appaltante != null)
                                {
                                    if (subappaltoPeriodo.Appaltante.LavoratoreAutonomo)
                                    {
                                        if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.PaeseNascita))
                                            subappalto.Appaltante.PaeseNascita =
                                                subappaltoPeriodo.Appaltante.PaeseNascita;
                                        if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.ProvinciaNascita))
                                            subappalto.Appaltante.ProvinciaNascita =
                                                subappaltoPeriodo.Appaltante.ProvinciaNascita;
                                        if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.LuogoNascita))
                                            subappalto.Appaltante.LuogoNascita =
                                                subappaltoPeriodo.Appaltante.LuogoNascita;

                                        subappalto.Appaltante.Foto = subappaltoPeriodo.Appaltante.Foto;
                                    }
                                }
                            }
                        }
                    }
                }

                //AccessoCantieriImpresePeriodo1.CaricaImprese(subColl);

                AccessoCantieriImpresePeriodo1.CaricaImprese(subappalti);
            }

            if (e.NextStepIndex == INDICELAVORATORI)
            {
                ImpresaCollection imprese = AccessoCantieriImprese1.GetImprese();
                SubappaltoCollection subappaltiPeriodo = AccessoCantieriImpresePeriodo1.GetSubappalti();
                AccessoCantieriLavoratore1.CaricaImprese(imprese, subappaltiPeriodo);

                AccessoCantieriLavoratore1.DeselezionaImpresa();

                //AccessoCantieriLavoratore1.CaricaDomanda(domanda.IdWhiteList);

                AccessoCantieriLavoratore1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                AccessoCantieriLavoratore1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());
            }

            if (e.NextStepIndex == INDICERILEVATORI)
            {
                ImpresaCollection imprese = AccessoCantieriImprese1.GetImprese();
                AccessoCantieriConferma1.CaricaImprese(imprese);
                AltraPersonaCollection altrePersone = AccessoCantieriAltrePersone1.GetAltrePersone();
                AccessoCantieriConferma1.CaricaAltrePersone(altrePersone);
                ReferenteCollection referenti = AccessoCantieriReferenti1.GetReferenti();
                AccessoCantieriConferma1.CaricaReferenti(referenti);
                RilevatoreCantiereCollection rilevatori = AccessoCantieriAssegnazioneRilevatori1.GetRilevatori();
                AccessoCantieriConferma1.CaricaRilevatori(rilevatori);
                WhiteList domanda = CreaDomanda();
                AccessoCantieriConferma1.CaricaDomanda(domanda);
            }

            if (e.NextStepIndex == INDICECONFERMA)
            {
                ImpresaCollection imprese = AccessoCantieriImprese1.GetImprese();
                AccessoCantieriConferma1.CaricaImprese(imprese);
                AltraPersonaCollection altrePersone = AccessoCantieriAltrePersone1.GetAltrePersone();
                AccessoCantieriConferma1.CaricaAltrePersone(altrePersone);
                ReferenteCollection referenti = AccessoCantieriReferenti1.GetReferenti();
                AccessoCantieriConferma1.CaricaReferenti(referenti);
                RilevatoreCantiereCollection rilevatori = AccessoCantieriAssegnazioneRilevatori1.GetRilevatori();
                AccessoCantieriConferma1.CaricaRilevatori(rilevatori);
                WhiteList domanda = CreaDomanda();
                AccessoCantieriConferma1.CaricaDomanda(domanda);
            }
        }
    }

    //private void UpdateWhitelistTymbro()
    //{
    //    FornitoreCollection fornitori = _biz.GetFornitori();

    //    int idFornitore = -1;
    //    foreach (Fornitore forn in fornitori)
    //    {
    //        if (forn.RagioneSociale == "Tymbro")
    //        {
    //            idFornitore = forn.IdFornitore;
    //            break;
    //        }
    //    }

    //    RilevatoreCollection rilevatori = _biz.GetRilevatoriAttiviByIdFornitore(idFornitore);

    //    foreach (Rilevatore ril in rilevatori)
    //    {
    //        if (ril.InvioWhitelist)
    //        {
    //            TymbroBiz.DeleteWhitelist(ril.Codice);
    //            List<string> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListByCodRilevatore(ril.Codice);
    //            TymbroBiz.CaricaWhitelist(codiciFiscaliAbilitatiList, ril.Codice);
    //        }
    //    }
    //}

    private void UpdateWhitelistTymbroSingoloRilevatore(Int32 idRilevatore, String codiceRilevatore)
    {
        Dictionary<String, Boolean> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListCompletaByCodRilevatore(idRilevatore);

        // Gestisco anche i cancellati        
        if (ViewState["LavoratoriCancellati"] != null)
        {
            List<String> lavoratoriCancellati = (List<String>) ViewState["LavoratoriCancellati"];

            foreach (String lav in lavoratoriCancellati)
            {
                if (!codiciFiscaliAbilitatiList.ContainsKey(lav))
                {
                    codiciFiscaliAbilitatiList.Add(lav, false);
                }
            }
        }

        TymbroBiz.CaricaWhitelist(codiciFiscaliAbilitatiList, codiceRilevatore);
    }

    private void UpdateWhitelistTrexomSingoloRilevatore(Int32 idRilevatore, String codiceRilevatore)
    {
        Dictionary<String, Boolean> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListCompletaByCodRilevatore(idRilevatore);

        // Gestisco anche i cancellati        
        if (ViewState["LavoratoriCancellati"] != null)
        {
            List<String> lavoratoriCancellati = (List<String>) ViewState["LavoratoriCancellati"];

            foreach (String lav in lavoratoriCancellati)
            {
                if (!codiciFiscaliAbilitatiList.ContainsKey(lav))
                {
                    codiciFiscaliAbilitatiList.Add(lav, false);
                }
            }
        }

        TrexomBiz.CaricaWhitelist(codiciFiscaliAbilitatiList, codiceRilevatore);
    }

    private void UpdateWhitelistClickFindSingoloRilevatore(Int32 idRilevatore, String codiceRilevatore)
    {
        try
        {
            bool ret;
            bool retSpec;

            WSTimbrature ws = new WSTimbrature();

            List<string> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListByCodRilevatore(idRilevatore);

            string[] codiciFiscaliAbilitati = new string[codiciFiscaliAbilitatiList.Count];

            int i = 0;
            foreach (string codFisc in codiciFiscaliAbilitatiList)
            {
                codiciFiscaliAbilitati[i] = codFisc;
                i++;
            }

            ws.insertAccessList(codiceRilevatore, codiciFiscaliAbilitati, out ret, out retSpec);
        }
        catch
        {
            
        }
    }

    private void UpdateWhitelistClickFind()
    {
            WSTimbrature ws = new WSTimbrature();

            FornitoreCollection fornitori = _biz.GetFornitori();

            int idFornitore = -1;
            foreach (Fornitore forn in fornitori)
            {
                if (forn.RagioneSociale == "Click & Find")
                {
                    idFornitore = forn.IdFornitore;
                    break;
                }
            }

            RilevatoreCollection rilevatori = _biz.GetRilevatoriAttiviByIdFornitore(idFornitore);

            foreach (Rilevatore ril in rilevatori)
            {
                if (ril.InvioWhitelist)
                {
                    bool ret;
                    bool retSpec;

                    List<string> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListByCodRilevatore(ril.IdRilevatore);

                    string[] codiciFiscaliAbilitati = new string[codiciFiscaliAbilitatiList.Count];

                    int i = 0;
                    foreach (string codFisc in codiciFiscaliAbilitatiList)
                    {
                        codiciFiscaliAbilitati[i] = codFisc;
                        i++;
                    }

                    ws.insertAccessList(ril.Codice, codiciFiscaliAbilitati, out ret, out retSpec);
                }
           }
      }
        


    //private void UpdateWhitelistClickFind()
    //{
    //    try
    //    {
    //        #region Click&FindWhitelist

    //        WSTimbrature ws = new WSTimbrature();

    //        FornitoreCollection fornitori = _biz.GetFornitori();

    //        int idFornitore = -1;
    //        foreach (Fornitore forn in fornitori)
    //        {
    //            if (forn.RagioneSociale == "Click & Find")
    //            {
    //                idFornitore = forn.IdFornitore;
    //                break;
    //            }
    //        }

    //        RilevatoreCollection rilevatori = _biz.GetRilevatoriAttiviByIdFornitore(idFornitore);

    //        foreach (Rilevatore ril in rilevatori)
    //        {
    //            //if (ril.Codice == "TORNELLO6")
    //            //{
    //                bool ret;
    //                bool retSpec;

    //                List<string> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListByCodRilevatore(ril.Codice);

    //                string[] codiciFiscaliAbilitati = new string[codiciFiscaliAbilitatiList.Count];

    //                int i = 0;
    //                foreach (string codFisc in codiciFiscaliAbilitatiList)
    //                {
    //                    codiciFiscaliAbilitati[i] = codFisc;
    //                    i++;
    //                }

    //                ws.insertAccessList(ril.Codice, codiciFiscaliAbilitati, out ret, out retSpec);
    //            //}
    //        }

    //        #endregion
    //    }
    //    catch
    //    {
    //    }
    //}


    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Page.Validate("stopConferma");

        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
        else
        {
            WhiteList domanda = CreaDomanda();

            if (domanda.IdWhiteList.HasValue)
            {

                WhiteListImpresaCollection domImpColl = _biz.GetLavoratoriInDomanda(domanda.IdWhiteList.Value);

                foreach (WhiteListImpresa domImp in domanda.Lavoratori)
                {
                    int i = 0;
                    while (i<domImpColl.Count)
                    {
                        if ((domImpColl[i].Impresa.IdImpresa == domImp.Impresa.IdImpresa) && (domImpColl[i].Impresa.TipoImpresa == domImp.Impresa.TipoImpresa)
                            && (domImpColl[i].IdDomanda == domImp.IdDomanda)
                            )
                        {
                            break;
                        }
                        i++;
                    }

                    int j = 0;
                    while (j < domImp.Lavoratori.Count)
                    {
                        Lavoratore labour = domImpColl.GetLavoratoreCodFisc(domImp.Lavoratori[j].CodiceFiscale, domImp.Impresa.TipoImpresa, domImp.Impresa.IdImpresa);

                        if (labour != null)
                        {
                            domImp.Lavoratori[j].Foto =
                                labour.Foto;
                            domImp.Lavoratori[j].DataStampaBadge =
                                labour.DataStampaBadge;
                        }

                        j++;
                    }
                }

                if (_biz.UpdateDomanda(domanda))
                {
                    foreach (RilevatoreCantiere rilevatoreCantiere in domanda.Rilevatori)
                    {
                        if ((!rilevatoreCantiere.DataFine.HasValue || rilevatoreCantiere.DataFine.Value > DateTime.Now)
                            && rilevatoreCantiere.InvioWhitelist)
                        {
                            try
                            {
                                switch (rilevatoreCantiere.RagioneSociale)
                                {
                                    case "Tymbro":
                                        UpdateWhitelistTymbroSingoloRilevatore(rilevatoreCantiere.IdRilevatore, rilevatoreCantiere.CodiceRilevatore);
                                        break;
                                    case "Trexom":
                                        UpdateWhitelistTrexomSingoloRilevatore(rilevatoreCantiere.IdRilevatore, rilevatoreCantiere.CodiceRilevatore);
                                        break;
                                    case "Click & Find":
                                        UpdateWhitelistClickFindSingoloRilevatore(rilevatoreCantiere.IdRilevatore, rilevatoreCantiere.CodiceRilevatore);
                                        break;
                                }
                            }
                            catch { }
                        }

                    }

                    Response.Redirect("~/AccessoCantieri/ConfermaInserimento.aspx");
                }
            }
            else
            {
                try
                {
                    if (_biz.InsertDomanda(domanda))
                    {
                        LabelGiaInserita.Visible = false;
                        //UpdateWhitelistClickFind();

                        Response.Redirect("~/AccessoCantieri/ConfermaInserimento.aspx");
                    }
                }
                catch (RichiestaGiaInseritaException)
                {
                    LabelGiaInserita.Visible = true;
                }
            }
        }
    }

    private WhiteList CreaDomanda()
    {
        WhiteList domanda = new WhiteList();

        if (ViewState["IdDomanda"] != null)
        {
            domanda.IdWhiteList = (Int32) ViewState["IdDomanda"];
        }

        domanda.GuidId = (Guid) ViewState["GuidId"];

        #region Riconoscimento dell'utente

        domanda.IdUtente = GestioneUtentiBiz.GetIdUtente();

        #endregion

        #region Dati Generali

        IndirizzoCollection indirizzi = AccessoCantieriCantiere1.GetIndirizzi();

        string descrizione = AccessoCantieriCantiere1.GetDescrizione();
        DateTime? dataInizio = AccessoCantieriCantiere1.GetDataInizio();
        DateTime? dataFine = AccessoCantieriCantiere1.GetDataFine();
        string autorizzazioneAlSubappalto = AccessoCantieriCantiere1.GetAutorizzazioneAlSubappalto();

        domanda.DataInizio = dataInizio;
        domanda.DataFine = dataFine;

        domanda.Descrizione = descrizione;

        domanda.AutorizzazioneAlSubappalto = autorizzazioneAlSubappalto;

        domanda.Cap = indirizzi[0].Cap;
        domanda.Civico = indirizzi[0].Civico;
        domanda.Comune = indirizzi[0].Comune;
        domanda.Provincia = indirizzi[0].Provincia;
        domanda.Indirizzo = indirizzi[0].Indirizzo1;
        domanda.InfoAggiuntiva = indirizzi[0].InfoAggiuntiva;
        domanda.Latitudine = indirizzi[0].Latitudine;
        domanda.Longitudine = indirizzi[0].Longitudine;

        #endregion

        #region Subappalti

        SubappaltoCollection subappalti = AccessoCantieriImprese1.GetSubappalti();
        SubappaltoCollection subappaltiPeriodo = AccessoCantieriImpresePeriodo1.GetSubappalti();

        foreach (Subappalto subappalto in subappalti)
        {
            foreach (Subappalto subappaltoPeriodo in subappaltiPeriodo)
            {
                //if (subappalto.Equals(subappaltoPeriodo))
                if (subappalto.PartitaIvaAppaltata == subappaltoPeriodo.PartitaIvaAppaltata && subappalto.PartitaIvaAppaltatrice == subappaltoPeriodo.PartitaIvaAppaltatrice)
                {
                    subappalto.DataInizioAttivitaAppaltata = subappaltoPeriodo.DataInizioAttivitaAppaltata;
                    subappalto.DataFineAttivitaAppaltata = subappaltoPeriodo.DataFineAttivitaAppaltata;

                    subappalto.DataInizioAttivitaAppaltatrice = subappaltoPeriodo.DataInizioAttivitaAppaltatrice;
                    subappalto.DataFineAttivitaAppaltatrice = subappaltoPeriodo.DataFineAttivitaAppaltatrice;

                    if (subappaltoPeriodo.Appaltante != null)
                    {
                        if (subappaltoPeriodo.Appaltante.LavoratoreAutonomo)
                        {
                            if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.PaeseNascita))
                                subappalto.Appaltante.PaeseNascita = subappaltoPeriodo.Appaltante.PaeseNascita;
                            if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.ProvinciaNascita))
                                subappalto.Appaltante.ProvinciaNascita =
                                    subappaltoPeriodo.Appaltante.ProvinciaNascita;
                            if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.LuogoNascita))
                                subappalto.Appaltante.LuogoNascita = subappaltoPeriodo.Appaltante.LuogoNascita;

                            subappalto.Appaltante.Foto = subappaltoPeriodo.Appaltante.Foto;
                        }
                    }
                }
            }
        }
        
        
        domanda.Subappalti = subappalti;

        #endregion

        #region Lavoratori

        WhiteListImpresaCollection domandeImprese = AccessoCantieriLavoratore1.GetDomandeImprese();
        domanda.Lavoratori = domandeImprese;

        #endregion

        #region AltrePersone

        AltraPersonaCollection altrePersone = AccessoCantieriAltrePersone1.GetAltrePersone();
        domanda.ListaAltrePersone = altrePersone;

        #endregion

        #region Rilevatori

        RilevatoreCantiereCollection rilevatori = AccessoCantieriAssegnazioneRilevatori1.GetRilevatori();
        if (rilevatori == null)
            rilevatori = new RilevatoreCantiereCollection();
        domanda.Rilevatori = rilevatori;

        #endregion

        #region Referenti

        ReferenteCollection referenti = AccessoCantieriReferenti1.GetReferenti();
        domanda.ListaReferenti = referenti;

        #endregion

        return domanda;
    }

    protected void Wizard1_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Page.Validate("stop");

        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
        else
        {
            if (ViewState["Modifica"] != null || e.CurrentStepIndex >= e.NextStepIndex)
            {
                if (e.NextStepIndex == INDICEIMPRESE)
                {
                    AccessoCantieriImprese1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                    AccessoCantieriImprese1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());

                    SubappaltoCollection subColl = AccessoCantieriImprese1.GetSubappalti();
                    AccessoCantieriImpresePeriodo1.CaricaImprese(subColl);
                }

                if (e.NextStepIndex == INDICEIMPRESEPERIODO)
                {
                    AccessoCantieriImpresePeriodo1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                    AccessoCantieriImpresePeriodo1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());

                    //SubappaltoCollection subColl = AccessoCantieriImprese1.GetSubappalti();
                    SubappaltoCollection subappalti = AccessoCantieriImprese1.GetSubappalti();
                    SubappaltoCollection subappaltiPeriodo = AccessoCantieriImpresePeriodo1.GetSubappalti();

                    foreach (Subappalto subappalto in subappalti)
                    {
                        foreach (Subappalto subappaltoPeriodo in subappaltiPeriodo)
                        {
                            //if (subappalto.Equals(subappaltoPeriodo))
                            if (subappalto.PartitaIvaAppaltata == subappaltoPeriodo.PartitaIvaAppaltata && subappalto.PartitaIvaAppaltatrice == subappaltoPeriodo.PartitaIvaAppaltatrice)
                            {
                                subappalto.DataInizioAttivitaAppaltata = subappaltoPeriodo.DataInizioAttivitaAppaltata;
                                subappalto.DataFineAttivitaAppaltata = subappaltoPeriodo.DataFineAttivitaAppaltata;

                                subappalto.DataInizioAttivitaAppaltatrice = subappaltoPeriodo.DataInizioAttivitaAppaltatrice;
                                subappalto.DataFineAttivitaAppaltatrice = subappaltoPeriodo.DataFineAttivitaAppaltatrice;

                                if (subappaltoPeriodo.Appaltante != null)
                                {
                                    if (subappaltoPeriodo.Appaltante.LavoratoreAutonomo)
                                    {
                                        if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.PaeseNascita))
                                            subappalto.Appaltante.PaeseNascita = subappaltoPeriodo.Appaltante.PaeseNascita;
                                        if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.ProvinciaNascita))
                                        subappalto.Appaltante.ProvinciaNascita =
                                            subappaltoPeriodo.Appaltante.ProvinciaNascita;
                                        if (!string.IsNullOrEmpty(subappaltoPeriodo.Appaltante.LuogoNascita))
                                            subappalto.Appaltante.LuogoNascita = subappaltoPeriodo.Appaltante.LuogoNascita;

                                        subappalto.Appaltante.Foto = subappaltoPeriodo.Appaltante.Foto;
                                    }
                                }
                            }
                        }
                    }

                    //AccessoCantieriImpresePeriodo1.CaricaImprese(subColl);

                    AccessoCantieriImpresePeriodo1.CaricaImprese(subappalti);
                }

                if (e.NextStepIndex == INDICELAVORATORI)
                {
                    ImpresaCollection imprese = AccessoCantieriImprese1.GetImprese();
                    SubappaltoCollection subappaltiPeriodo = AccessoCantieriImpresePeriodo1.GetSubappalti();
                    AccessoCantieriLavoratore1.CaricaImprese(imprese, subappaltiPeriodo);

                    AccessoCantieriLavoratore1.DeselezionaImpresa();

                    AccessoCantieriLavoratore1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                    AccessoCantieriLavoratore1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());
                }

                if (e.NextStepIndex == INDICERILEVATORI)
                {
                    ImpresaCollection imprese = AccessoCantieriImprese1.GetImprese();
                    AccessoCantieriConferma1.CaricaImprese(imprese);
                    AltraPersonaCollection altrePersone = AccessoCantieriAltrePersone1.GetAltrePersone();
                    AccessoCantieriConferma1.CaricaAltrePersone(altrePersone);
                    ReferenteCollection referenti = AccessoCantieriReferenti1.GetReferenti();
                    AccessoCantieriConferma1.CaricaReferenti(referenti);
                    //RilevatoreCantiereCollection rilevatori = AccessoCantieriAssegnazioneRilevatori1.GetRilevatori();
                    //AccessoCantieriConferma1.CaricaRilevatori(rilevatori);
                    WhiteList domanda = CreaDomanda();
                    AccessoCantieriConferma1.CaricaDomanda(domanda);
                }

                if (e.NextStepIndex == INDICECONFERMA)
                {
                   
                    ////SubappaltoCollection subColl = AccessoCantieriImpresePeriodo1.GetSubappalti();
                    ////AccessoCantieriImpresePeriodo1.CaricaImprese(subColl);

                    //AccessoCantieriImpresePeriodo1.CaricaDataInizio(AccessoCantieriCantiere1.GetDataInizio());
                    //AccessoCantieriImpresePeriodo1.CaricaDataFine(AccessoCantieriCantiere1.GetDataFine());

                    //SubappaltoCollection subColl = AccessoCantieriImprese1.GetSubappalti();
                    //AccessoCantieriImpresePeriodo1.CaricaImprese(subColl);

                    WhiteList domanda = CreaDomanda();
                    AccessoCantieriConferma1.CaricaDomanda(domanda);

                    ImpresaCollection imprese = AccessoCantieriImprese1.GetImprese();
                    AccessoCantieriConferma1.CaricaImprese(imprese);
                    AltraPersonaCollection altrePersone = AccessoCantieriAltrePersone1.GetAltrePersone();
                    AccessoCantieriConferma1.CaricaAltrePersone(altrePersone);
                    ReferenteCollection referenti = AccessoCantieriReferenti1.GetReferenti();
                    AccessoCantieriConferma1.CaricaReferenti(referenti);
                    RilevatoreCantiereCollection rilevatori = AccessoCantieriAssegnazioneRilevatori1.GetRilevatori();
                    AccessoCantieriConferma1.CaricaRilevatori(rilevatori);

                }

                ViewState["indiceWizard"] = e.NextStepIndex;
            }
            else
            {
                e.Cancel = true;
            }
        }
    }

    protected void Wizard1_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
    {
        ViewState["indiceWizard"] = e.NextStepIndex;
    }

    #region Cambio colore link della sidebar

    private void lst_ItemCreated(object sender, DataListItemEventArgs e)
    {
        // Per far funzionare il cambiamento di colore dei link della sidebar
        int indiceWizard = (int) ViewState["indiceWizard"];
        LinkButton button = (LinkButton) e.Item.FindControl("SideBarButton");

        if (ViewState["Modifica"] == null && e.Item.ItemIndex > indiceWizard)
            button.ForeColor = Color.LightGray;
    }

    #endregion
}
