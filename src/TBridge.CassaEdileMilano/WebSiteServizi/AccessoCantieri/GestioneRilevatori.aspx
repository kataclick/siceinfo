﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneRilevatori.aspx.cs" Inherits="AccessoCantieri_GestioneRilevatori" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/AccessoCantieriRilevatori.ascx" TagName="AccessoCantieriRilevatori"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <br />
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione rilevatori"
        titolo="Accesso ai Cantieri" />
    <br />
    <div>
    <uc3:AccessoCantieriRilevatori ID="AccessoCantieriRilevatori1" runat="server" />
    </div>
    <br />
</asp:Content>
