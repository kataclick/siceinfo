﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportAccessoCantieriImprese.aspx.cs" Inherits="AccessoCantieri_ReportAccessoCantieriImprese" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <br />
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione timbrature"
        titolo="Accesso ai Cantieri" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" ShowDocumentMapButton="false"
                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" Height="550pt"
                    Width="550pt">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
