﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class AccessoCantieri_VisualizzaWhiteList : Page
{
    private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                                                             FunzionalitaPredefinite.AccessoCantieriAmministrazione,
                                                             FunzionalitaPredefinite.AccessoCantieriPerCommittente
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        if (Context.Items["IdDomanda"] != null)
        {
            ViewState["idDomanda"] = (Int32) Context.Items["IdDomanda"];
            Int32 idDomanda = (Int32) Context.Items["IdDomanda"];
            CaricaDomanda(idDomanda);
        }
    }

    private void CaricaDomanda(int idDomanda)
    {
        WhiteList domanda = biz.GetDomandaByKey(idDomanda);
        AccessoCantieriDati1.CaricaDomanda(domanda);
    }
}