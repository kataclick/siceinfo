<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EstrattoContoLavoratore.aspx.cs" Inherits="EstrattoContoLavoratore" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/MenuLavoratori.ascx" TagName="MenuLavoratori" TagPrefix="uc1" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuLavoratori ID="MenuLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Estratto conto" />
    <br />
In questa sezione � possibile consultare l�estratto conto, riferito alla propria posizione in Cassa Edile, composto dalle seguenti informazioni:
<ul>
<li><a href="HomeLavoratore.aspx">anagrafica</a> (dove sono riportati i propri dati personali ed � consentito segnalare eventuali variazioni di indirizzo)
</li>
</ul>
    <ul>
<br />
<li><a href="ReportLavoratori.aspx?tipo=rapporto">rapporti di lavoro</a> (dove vengono elencati i rapporti di lavoro instaurati nel tempo presso le varie aziende con indicazione di inizio/fine rapporto, tipologia di contratto, categoria, qualifica e mansione)
</li>
    </ul>
    <ul>
<br />
<li><a href="ReportLavoratori.aspx?tipo=ore">ore accantonate</a> (con la visualizzazione delle ore lavorate presso ogni singola impresa)
</li>
    </ul>
    <ul>
<br />
<li><a href="ReportLavoratori.aspx?tipo=prestazioni">prestazioni erogate</a> (con il dettaglio riferito al tipo di prestazione, data di ricezione della domanda, stato domanda, importo erogato lordo e netto e modalit� di pagamento adottata).
</li>
    </ul>
<br />
Cliccando sul simbolo + collocato a fianco della ragione sociale dell�impresa si apre il dettaglio del rapporto di lavoro prestato presso l�azienda interessata (data inizio/fine rapporto, data assunzione/cessazione, tipologia contratto, ecc). Lo stesso simbolo posto a fianco del periodo, se cliccato, permette di visualizzare il dettaglio delle ore di lavoro ordinario effettuate in ciascun periodo (n. ore e tipo ore).
<br />
<br />
Per ogni chiarimento riguardante quanto risultante dall�estratto conto, l�Ufficio Prestazioni di Cassa Edile � a disposizione.
<br />
</asp:Content>


