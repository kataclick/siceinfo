﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using Cassa = TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class CasseLombardia_CasseLombardia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            PreparaCollegamentoCElombardia();
        }
    }


    private void PreparaCollegamentoCElombardia()
    {
        String urlBase = "http://ww3.cassaedilemilano.it/casseLombardia/default.aspx";
        String tipoUtente = null;
        String StaticCode = "Hello_Milan_ce";

        // CASSA
        if (GestioneUtentiBiz.IsCassaEdile())
        {
            Cassa cassa = (Cassa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            tipoUtente = cassa.UserName.ToString().ToUpper();
        }

        DateTime ora = DateTime.Now;
        String md5 = SOLDOManager.CalculateMD5Hash(String.Format("{0}{1}", StaticCode, ora.ToString("yyyyMMdd")));
        String url = String.Format("{0}?ocassa={1}&code={2}", urlBase, tipoUtente, md5);

        ViewState["urlCasseLombardia"] = url;
        Response.Redirect(url);
    }
}