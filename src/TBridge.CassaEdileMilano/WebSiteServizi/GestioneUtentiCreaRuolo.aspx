<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="GestioneUtentiCreaRuolo.aspx.cs" Inherits="GestioneUtentiCreaRuolo" %>

<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti" TagPrefix="uc1" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1"  titolo="Gestione utenti" sottoTitolo="Crea Ruolo" runat="server" />
    <br />
    <table class="standardTable">
    <tr>
        <td>
            <asp:Label ID="LabelNomeRuolo" runat="server" Text="Nome:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNomeRuolo" runat="server" Width="300"></asp:TextBox>
            (Senza spazi)</td>
    </tr>
    <tr>
    <td>
        <asp:Label ID="LabelDescrizioneRuolo" runat="server" Text="Descrizione:"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="TextBoxDescrizioneRuolo" runat="server" Width="300"></asp:TextBox>
    </td>
    </tr>
    </table>
    <asp:Label ID="Label2" runat="server" Text="Seleziona funzionalitą" Font-Bold="True"></asp:Label><br />
    <br />
    <asp:CheckBoxList ID="CheckBoxListFunzionalita" runat="server">
    </asp:CheckBoxList><br />
    <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla" />
    &nbsp;<asp:Button ID="ButtonCreaRuolo" runat="server" OnClick="ButtonCreaRuolo_Click"
        Text="Crea ruolo" />
    
    </asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>

