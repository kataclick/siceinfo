using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class ReportImprese : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        throw new Exception("Pagina non pi� disponibile");
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoImpresa);
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        throw new Exception("Pagina non pi� disponibile");
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoImpresa);
    }

    protected void ReportViewerImprese_Init(object sender, EventArgs e)
    {
        throw new Exception("Pagina non pi� disponibile");
        ReportViewerImprese.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        string tipoReport = Request.QueryString["tipo"];
        if (tipoReport == "orePeriodo")
        {
            ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportOrePeriodo";
            TitoloSottotitolo1.sottoTitolo = "Ore per periodo";
        }
        else if (tipoReport == "oreLavoratori")
        {
            ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportOreLavoratori";
            TitoloSottotitolo1.sottoTitolo = "Ore per lavoratori";
        }
        else if (tipoReport == "crediti")
        {
            ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportCrediti";
            TitoloSottotitolo1.sottoTitolo = "Crediti";
        }
        else if (tipoReport == "rapportiLavoro")
        {
            ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportRapportiLavoro";
            TitoloSottotitolo1.sottoTitolo = "Rapporti di Lavoro";
        }
        else if (tipoReport == "cartella")
        {
            TableLabelCartella.Visible = true;
            ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportCartella";
            TitoloSottotitolo1.sottoTitolo = "Riepilogo cartella";
        }
        else if (tipoReport == "premioFedelta")
        {
            TableLabelPremioFedelta.Visible = true;
            ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportPremioFedelta";
            TitoloSottotitolo1.sottoTitolo = "Premio fedelt�";
        }

        //int idImpresa = ((Impresa) (HttpContext.Current.User).Identity).Entity.IdImpresa;
        int idImpresa = ((Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdImpresa;

        ReportParameter param = new ReportParameter("idImpresa", idImpresa.ToString());
        ReportParameter[] listaParam = new ReportParameter[1];

        ReportViewerImprese.Visible = true;

        if (tipoReport == "cartella")
        {
            int? annoDaConsiderare = GetAnno();
            if (annoDaConsiderare.HasValue)
            {
                string semestreDaConsiderare = GetSemestre();

                //ReportViewerImprese.Visible = true;
                TableLabelCartella.Visible = true;
                TableLabelNoDati.Visible = false;

                if (!String.IsNullOrEmpty(semestreDaConsiderare))
                {
                    listaParam = new ReportParameter[3];
                    ReportParameter anno = new ReportParameter("anno", annoDaConsiderare.ToString());
                    ReportParameter semestre = new ReportParameter("semestre", semestreDaConsiderare);
                    listaParam[1] = anno;
                    listaParam[2] = semestre;
                }
            }
            else
            {
                ReportViewerImprese.Visible = false;
                TableLabelCartella.Visible = false;
                TableLabelNoDati.Visible = true;
            }
        }
        if (tipoReport == "premioFedelta")
        {
            const string dataLiquidazioneDaConsiderare = "28/9/2007";
            //if (!String.IsNullOrEmpty(dataLiquidazioneDaConsiderare))
            DateTime dataLiquidazioneDate = DateTime.Parse(dataLiquidazioneDaConsiderare);

            if (DateTime.Now >= dataLiquidazioneDate &&  DateTime.Now <= dataLiquidazioneDate.AddMonths(3))
            {
                listaParam = new ReportParameter[2];
                ReportParameter dataLiquidazione = new ReportParameter("dataLiquidazione", dataLiquidazioneDaConsiderare);
                listaParam[1] = dataLiquidazione;
            }
            else
            {
                ReportViewerImprese.Visible = false;
                TableLabelPremioFedelta.Visible = false;
                TableLabelNoDatiPremioFedelta.Visible = true;
            }
        }


        listaParam[0] = param;

        ReportViewerImprese.ServerReport.SetParameters(listaParam);
    }

    private static int? GetAnno()
    {
        int? anno = null;
        DateTime data = DateTime.Today;
        if (data.Month == 1)
            anno = data.Year - 1;
        if ((data.Month >= 6 && data.Month <= 9) || data.Month >= 11)
            anno = data.Year;

        return anno;
    }

    private static string GetSemestre()
    {
        string semestre = null;

        DateTime data = DateTime.Today;
        if (data.Month == 1 || data.Month >= 11)
            semestre = "SECONDO";
        if (data.Month >= 6 && data.Month <= 9)
            semestre = "PRIMO";

        return semestre;
    }
}