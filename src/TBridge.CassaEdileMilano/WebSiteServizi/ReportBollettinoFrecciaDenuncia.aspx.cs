using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Entities;

public partial class StampaBollettinoFreccia : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.BollettinoFrecciaRichiesta,
                                                             FunzionalitaPredefinite.BollettinoFrecciaStatistiche
                                                         };
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        
        #endregion

        BollettinoFreccia bollettino = (BollettinoFreccia) Context.Items["bollettino"];

        int anno = bollettino.Anno;
        int mese = bollettino.Mese;
        int sequenza = bollettino.Sequenza;
        string cip = bollettino.Cip;
        Decimal importo = bollettino.Importo;
        int idImpresa = bollettino.IdImpresa;

        importo = Decimal.Round(importo, 2);
        string importo9 = importo.ToString().Replace(",", string.Empty).PadLeft(9, '0');

        string cinImporto = CalcolaCin(importo9);
        string cinIntermedio = CalcolaCin(cip);

        //const string iban = "IT92P0306901629100000010033";
        const string iban = "IT87L0335901600100000129081";
        const string codiceEsenzione = "1";
        const string codiceDivisa = "E";


        string cinComplessivoAux = string.Format("{0}{1}{2}{3}{4}{5}{6}", importo9, cinImporto, cip, cinIntermedio, iban,
                                                 codiceEsenzione, codiceDivisa);

        string cinComplessivo = CalcolaCin(cinComplessivoAux);

        string codiceComplessivo = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}",
                                                 ">", importo9, "<", cinImporto, ">",
                                                 cip, "<", cinIntermedio, "+", iban, ">", codiceEsenzione, "<",
                                                 codiceDivisa, "+", cinComplessivoAux, "<");


        ReportViewerBollettinoFreccia.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        ReportViewerBollettinoFreccia.ServerReport.ReportPath = "/reportbollettinofreccia/ReportBollettinoFrecciaDenunciaUnico";

        ReportParameter[] listaParam = new ReportParameter[8];
        listaParam[0] = new ReportParameter("idImpresa", idImpresa.ToString());
        listaParam[1] = new ReportParameter("CINImporto", cinImporto);
        listaParam[2] = new ReportParameter("CINIntermedio", cinIntermedio);
        listaParam[3] = new ReportParameter("CINComplessivo", cinComplessivo);
        listaParam[4] = new ReportParameter("anno", anno.ToString());
        listaParam[5] = new ReportParameter("mese", mese.ToString());
        listaParam[6] = new ReportParameter("sequenza", sequenza.ToString());
        listaParam[7] = new ReportParameter("importo",bollettino.Importo.ToString());

        ReportViewerBollettinoFreccia.ServerReport.SetParameters(listaParam);

        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string extension;

        //PDF

        byte[] bytes = ReportViewerBollettinoFreccia.ServerReport.Render(
            "PDF", null, out mimeType, out encoding, out extension,
            out streamids, out warnings);

        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/pdf";

        Response.AppendHeader("Content-Disposition", "attachment;filename=BollettinoFrecciaDenuncia.pdf");
        Response.BinaryWrite(bytes);

        Response.Flush();
        Response.End();

        //
        //}
        //if (Int32.Parse(Context.Items["prova"].ToString()) == 1)
        //{
        //    Warning[] warnings;
        //    string[] streamids;
        //    string mimeType;
        //    string encoding;
        //    string extension;

        //    string testoUNICODE = "Prova testo Unicode";

        //    Encoding unicode = Encoding.Unicode;
        //    byte[] unicodeBytes = unicode.GetBytes(testoUNICODE);

        //    char[] unicodeChars = new char[unicode.GetCharCount(unicodeBytes, 0, unicodeBytes.Length)];
        //    unicode.GetChars(unicodeBytes, 0, unicodeBytes.Length, unicodeChars, 0);
        //    string unicodeString = new string(unicodeChars);

        //    ReportViewerBollettinoFreccia.ServerReport.ReportServerUrl =
        //        new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

        //    ReportViewerBollettinoFreccia.ServerReport.ReportPath = "/reportbollettinofreccia/ReportProva";

        //    ReportParameter[] listaParam = new ReportParameter[1];
        //    listaParam[0] = new ReportParameter("testoUNICODE", unicodeString);

        //    ReportViewerBollettinoFreccia.ServerReport.SetParameters(listaParam);


        //    //PDF

        //    byte[] bytes2 = ReportViewerBollettinoFreccia.ServerReport.Render(
        //       "PDF", null, out mimeType, out encoding, out extension,
        //       out streamids, out warnings);

        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.ContentType = "application/pdf";

        //    Response.AppendHeader("Content-Disposition", "attachment;filename=prova.pdf");
        //    Response.BinaryWrite(bytes2);

        //    Response.Flush();
        //    Response.End();
        //}


        ////TIFF

        //string devInfo = "<DeviceInfo>" + " <OutputFormat>TIFF</OutputFormat>"
        //                 + " <DpiX>800</DpiX>" + " <DpiY>800</DpiY>" + "<MarginBottom>0in</MarginBottom>" + "<MarginTop>0in</MarginTop>" + "<MarginLeft>0.39in</MarginLeft>" + "<MarginRight>0in</MarginRight>" + "</DeviceInfo>";

        //byte[] bytes = ReportViewerBollettinoFreccia.ServerReport.Render(
        //   "IMAGE", devInfo, out mimeType, out encoding, out extension,
        //   out streamids, out warnings);

        //Response.Clear();
        //Response.Buffer = true;
        //Response.ContentType = "image/tiff";

        //Response.AppendHeader("Content-Disposition", "attachment;filename=BollettinoFreccia.tiff");
        //Response.BinaryWrite(bytes);

        //Response.Flush();
        //Response.End();

        ////
    }

    private static string CalcolaCin(string valore)
    {
        if (string.IsNullOrEmpty(valore))
        {
            throw new Exception("Valore non ammesso");
        }

        const string lettere = "Z0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int somma = 0;
        for (int k = 0; k < valore.Length; k++)
        {
            char c = valore.Substring(k, 1).ToCharArray()[0];
            somma += c*(k + 1);
        }
        int i = (somma%36);
        return lettere.Substring(i, 1);
    }
}