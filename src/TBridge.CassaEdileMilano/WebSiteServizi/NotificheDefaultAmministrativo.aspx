<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NotificheDefaultAmministrativo.aspx.cs" Inherits="NotificheDefaultAmministrativo" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuNotifiche id="MenuNotifiche1" runat="server">
    </uc2:MenuNotifiche>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche" sottoTitolo="Gestione notifiche amministrazione"/>
    <br />
    In questa pagina il personale di Cassa Edile pu� visualizzare le anomalie derivanti da un utilizzo non corretto della funzionalit� "Notifiche" da parte delle imprese
</asp:Content>
