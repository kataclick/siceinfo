<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NotificheAnomalie.aspx.cs" Inherits="NotificheAnomalie" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuNotifiche ID="MenuNotifiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche" sottoTitolo="Gestione anomalie"/>
    <br />Seleziona tipologia di anomalia da rilevare
    <br />
    <br />
    <table width="95%">
        <tr>
            <td align="left">
               <asp:RadioButton ID="RadioButtonNonDenunciati" runat="server" Text="Lavoratori notificati ma non denunciati" GroupName="scelta"/>
            </td>
        </tr>
        <tr>
            <td align="left">
               <asp:RadioButton ID="RadioButtonCessatiDenunciati" runat="server" Text="Lavoratori cessati per notifica ma denunciati" GroupName="scelta"/>
            </td>
        </tr>
        <tr>
            <td align="left">
                <br />
                <asp:Button ID="ButtonCerca" runat="server" Text="Cerca" OnClick="ButtonCerca_Click" Width="157px" />
                <br />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelTitoloElenco" runat="server" Font-Bold="True" Text="Elenco anomalie"
        Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice impresa" />
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione Sociale Impresa" />
            <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" />
            <asp:BoundField DataField="DataNascita" HeaderText="Data nascita" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:BoundField DataField="DataAssunzione" HeaderText="Data assunzione" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:BoundField DataField="DataCessazione" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data cessazione" HtmlEncode="False" />
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>

