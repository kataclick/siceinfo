<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="ImpreseRegolariSegnalaAnomalie.aspx.cs" Inherits="ImpreseRegolariSegnalaAnomalie" %>

<%@ Register Src="~/WebControls/MenuBackToConenuti.ascx" TagName="MenuBackToConenuti"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/ImpreseRegolariMenu.ascx" TagName="ImpreseRegolariMenu"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/ImpreseRegolariMenuContent.ascx" TagName="ImpreseRegolariMenuContent"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Anomalie" sottoTitolo="Segnala anomalie" runat="server" />
    <br />
    Segnala l'anomalia riscontrata (max 1000 caratteri)<br />
    <asp:TextBox ID="TextBoxMessaggio" runat="server" Height="133px" TextMode="MultiLine"
        Width="391px" MaxLength="1000"></asp:TextBox><br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxMessaggio"
        ErrorMessage="Fornire una descrizione dell'anomalia da segnalare"></asp:RequiredFieldValidator><br />
    <br />
    <asp:Button ID="ButtonInviaAnomalia" runat="server" OnClick="ButtonInviaAnomalia_Click"
        Text="Invia anomalia" /></asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:ImpreseRegolariMenu ID="ImpreseRegolariMenu1" runat="server" />
    <br />
</asp:Content>

