<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ImpreseRegolariDefault.aspx.cs" Inherits="Default6" %>

<%@ Register Src="~/WebControls/ImpreseRegolariMenu.ascx" TagName="ImpreseRegolariMenu"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:ImpreseRegolariMenu ID="ImpreseRegolariMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo2" titolo="Imprese adempienti" sottoTitolo="Benvenuto nella sezione imprese adempienti"
        runat="server" />
    <div class="DefaultPage">
        <br />
        Tramite questo servizio l'utente potr� visualizzare l�elenco delle imprese �affidabili�
        e �corrette� in materia di adempimenti contributivi.<br />
        La lista � di dominio pubblico, quindi, liberamente consultabile, ma non ne � consentita
        l�esportazione.<br />
        Il parametro di inclusione del nominativo dell�impresa nell�elenco delle aziende
        regolari � il risultato del confronto dei dati comunque acquisiti da Cassa Edile
        di Milano e relativi alla correttezza sostanziale negli adempimenti contributivi
        alla data indicata.
        <br />
        <strong>I dati resi disponibili sono di circa due mesi antecedenti alla pubblicazione</strong>,
        anzitutto a causa delle modalit� di denuncia e pagamento vigenti.
        <br />
        Oltre all�elenco dei nominativi delle aziende vengono forniti anche dati sulla tipologia
        di attivit� prevalentemente svolta e sulla sede legale. Inoltre, tramite le funzioni
        di ricerca per: ragione sociale, Comune sede legale, Comune sede amministrativa,
        codice ISTAT per tipologia di attivit� (in fase di completamento) � possibile effettuare
        indagini mirate. L�affinamento delle stesse � ottenibile con la combinazione di
        pi� voci tra quelle sopra indicate tra parentesi. Eventuali anomalie potranno essere
        segnalate attraverso l�apposita funzione inserita nel programma.
        <br />
        Essere inclusi nelle imprese adempienti non � solo un mero titolo di merito, ma
        crea maggior <em>business</em> e valore per l�impresa!</div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" runat="Server">
</asp:Content>
