<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ImpreseAdempientiReport.aspx.cs" Inherits="Report" Theme="CETheme2009" %>

<%@ Register Src="~/WebControls/MenuBackToConenuti.ascx" TagName="MenuBackToConenuti"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/ImpreseRegolariMenu.ascx" TagName="ImpreseRegolariMenu"
    TagPrefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <table>
        <tr>
            <td>
            </td>
            <td>
                <br />
                <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Imprese adempienti" sottoTitolo="Lista imprese adempienti"
                    runat="server" />
                La maschera presentata di seguito permette di eseguire una ricerca mirata sulla
                base dei campi selezionati e compilati da parte dell'utente. Una volta definiti
                i parametri � possibile visualizzare l'esito della ricerca premendo il tasto "Visualizza
                report".
            </td>
        </tr>
    </table>
    <br />
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td>
                    Ragione sociale:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="220px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    Attivit� ISTAT:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxAttivitaIstat" runat="server" Width="220px" EmptyMessage="Selezionare un'attivit�"
                        MarkFirstMatch="true" AllowCustomText="false" EnableScreenBoundaryDetection="false">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Comune sede amministrativa:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxComuneSedeAmministrativa" runat="server" Width="220px"
                        EmptyMessage="Selezionare un comune" MarkFirstMatch="true" AllowCustomText="false"
                        EnableScreenBoundaryDetection="false">
                    </telerik:RadComboBox>
                </td>
                <td>
                    Comune sede legale:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxComuneSedeLegale" runat="server" Width="220px"
                        EmptyMessage="Selezionare un comune" MarkFirstMatch="true" AllowCustomText="false"
                        EnableScreenBoundaryDetection="false">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    Partita IVA:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxPartitaIVA" runat="server" Width="220px" MaxLength="11">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div style="padding: 10px;">
            <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
                Text="Visualizza report" />
            <br />
        </div>
    </div>
    <table style="height:600pt; width:550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerImpreseAdem" runat="server" Font-Names="Verdana"
                    Font-Size="8pt" OnInit="ReportViewer1_Init" ProcessingMode="Remote" ShowExportControls="False"
                    ShowPrintButton="False" SizeToReportContent="True" DocumentMapCollapsed="True"
                    Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:ImpreseRegolariMenu ID="ImpreseRegolariMenu1" runat="server" />
</asp:Content>
