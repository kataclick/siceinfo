using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.ImpreseAdem.Business;
using TBridge.Cemi.ImpreseAdemp.Type.Collections;
using TBridge.Cemi.ImpreseAdemp.Type.Entities;
using TBridge.Cemi.ImpreseAdemp.Type.Filters;
using Impresa=TBridge.Cemi.Cantieri.Type.Entities.Impresa;

public partial class ImpreseRegolariForzaInLista : Page
{
    private readonly ImpreseAdemBusiness biz = new ImpreseAdemBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRegolariModificaLista,
                                              "ImpreseAdempientiFarzainLista.aspx");

        CantieriRicercaImpresa1.ModalitaImpreseRegolari();
        CantieriRicercaImpresa1.OnImpresaSelected += CantieriRicercaImpresa1_OnImpresaSelected;

        CaricaEccezioni();
    }

    private void CaricaEccezioni()
    {
        EccezioneListaFilter filtro = new EccezioneListaFilter();

        if (!string.IsNullOrEmpty(TextBoxIdImpresa.Text.Trim()))
            filtro.IdImpresa = Int32.Parse(TextBoxIdImpresa.Text);
        if (!string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
            filtro.RagioneSociale = TextBoxRagioneSociale.Text;

        EccezioneListaCollection eccezioni = biz.GetEccezioniListaImpreseAdempienti(filtro);

        GridViewEccezioni.DataSource = eccezioni;
        GridViewEccezioni.DataBind();
    }

    private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        bool giaPresente;
        LabelGiaPresente.Visible = false;

        EccezioneLista eccezione = new EccezioneLista();
        eccezione.Impresa.IdImpresa = impresa.IdImpresa.Value;
        eccezione.Impresa.RagioneSociale = impresa.RagioneSociale;
        eccezione.Stato = RadioButtonForzata.Checked;

        if (biz.InsertEccezioneListaImpreseAdempienti(eccezione, out giaPresente))
        {
            CaricaEccezioni();
            LabelErrore.Visible = false;
        }
        else
        {
            if (!giaPresente)
                LabelErrore.Visible = true;
            else
                LabelGiaPresente.Visible = true;
        }
    }

    protected void ButtonCerca_Click(object sender, EventArgs e)
    {
        GridViewEccezioni.PageIndex = 0;
        CaricaEccezioni();
    }

    protected void GridViewEccezioni_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idImpresa = (int) GridViewEccezioni.DataKeys[e.RowIndex].Value;

        if (biz.DeleteEccezioneListaImpreseAdempienti(idImpresa))
        {
            CaricaEccezioni();
            LabelErrore.Visible = false;
        }
        else
        {
            LabelErrore.Visible = true;
        }
    }

    protected void GridViewEccezioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewEccezioni.PageIndex = e.NewPageIndex;
        CaricaEccezioni();
    }
}