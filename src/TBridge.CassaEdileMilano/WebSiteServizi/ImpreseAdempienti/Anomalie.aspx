<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Anomalie.aspx.cs" Inherits="Anomalie" %>


<%@ Register Src="~/WebControls/ImpreseRegolariMenu.ascx" TagName="ImpreseRegolariMenu" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:ImpreseRegolariMenu ID="ImpreseRegolariMenu1" runat="server" />    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <div>
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo2"  titolo="Gestione Anomalie" sottoTitolo="Anomalie" runat="server" />     
<table class="standardTable">
<tr>
    <td align="right"><asp:Label ID="Label3" runat="server" Text="Da data (gg/mm/aaaa):"></asp:Label></td>
    <td align="left">
        <asp:TextBox ID="TextBoxDa" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDa"
            Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator></td>
</tr>
<tr>
    <td align="right"><asp:Label ID="Label4" runat="server" Text="A data (gg/mm/aaaa)(esclusa):"></asp:Label></td>
    <td align="left">
        <asp:TextBox ID="TextBoxA" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxA"
            Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator></td>
</tr>
<tr>
    <td align="left" colspan="2"><asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Visualizza" /></td>
</tr>
</table>

        <br />
        <br />
        <asp:GridView ID="GridViewAnomalie" runat="server" width="100%" AutoGenerateColumns="False" AllowPaging="True" CellPadding="4" PageSize="20" OnPageIndexChanging="GridViewAnomalie_PageIndexChanging" OnSorting="GridViewAnomalie_Sorting">
                    
            <Columns>
                <asp:BoundField DataField="data" HeaderText="Data" ReadOnly="True" SortExpression="data" >
                    <ItemStyle Width="120px" />
                </asp:BoundField>
                <asp:BoundField DataField="descrizione" HeaderText="Messaggio" SortExpression="descrizione">
                    <ItemStyle Width="400px" />
                </asp:BoundField>
                <asp:BoundField DataField="loginUtente" HeaderText="Utente" SortExpression="loginUtente" >
                    <ItemStyle Width="200px" />
                </asp:BoundField>
            </Columns>
            
            <EmptyDataTemplate>
                Nessuna anomalia registrata
            </EmptyDataTemplate>
    
        </asp:GridView>
        <br />
       
    
    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>


