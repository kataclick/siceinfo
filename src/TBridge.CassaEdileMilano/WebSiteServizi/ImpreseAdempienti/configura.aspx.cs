using System;
using System.Data;
using System.Data.Common;
using System.Web.UI;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.ImpreseAdem.Business;

//using TBridge.Cemi.ActivityTracking;

public partial class configura : Page
{
    private readonly ImpreseAdemBusiness biz = new ImpreseAdemBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRegolariImpostaParametri,
                                                  "configura.aspx");
            caricaSalva();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        caricaSalva();
    }

    private void caricaSalva()
    {
        if (IsPostBack)
        {
            /* Ho i nuovi parametri */
            biz.UpdateParametriImpreseRegolari(TextBox1.Text, TextBox2.Text);
        }
        else
        {
            /* Raccolgo i dati vecchi */
            string singolo;
            string totale;
            biz.GetParametriImpreseRegolari(out singolo, out totale);
            TextBox1.Text = singolo;
            TextBox2.Text = totale;
        }
    }
}