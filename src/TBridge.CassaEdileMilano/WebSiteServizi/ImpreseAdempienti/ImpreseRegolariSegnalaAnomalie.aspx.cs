using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;

public partial class ImpreseRegolariSegnalaAnomalie : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRegolariSegnalaAnomalie);

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInviaAnomalia, null) + ";");
        sb.Append("return true;");
        ButtonInviaAnomalia.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInviaAnomalia);
    }

    protected void ButtonInviaAnomalia_Click(object sender, EventArgs e)
    {
        string messaggio;

        if (TextBoxMessaggio.Text != string.Empty)
        {
            try
            {
                int idUtente = GestioneUtentiBiz.GetIdUtente();
                string loginUtente = GestioneUtentiBiz.GetNomeUtente();

                AnomalieManager anomalieManager = new AnomalieManager();
                bool ret = anomalieManager.RegistraAnomalia(TextBoxMessaggio.Text, idUtente, loginUtente);

                if (ret)
                {
                    messaggio = "Anomalia inviata correttamente";
                }
                else
                {
                    messaggio =
                        "Errore durante la procedura di registrazione dell'anomalia. Si prega di riprovare pi� tardi.";
                }
            }
            catch (Exception ex)
            {
                messaggio =
                    "Errore durante la procedura di registrazione dell'anomalia. Si prega di riprovare pi� tardi.";
            }
        }
        else
        {
            //this.TextBoxMessaggio.Text = "E' necessario scrivere descrivere l'anomalia prima di effettuare l'invio.";
            messaggio = "E' necessario descrivere l'anomalia prima di effettuare l'invio.";
        }

        SettaMessaggio(messaggio);
        TextBoxMessaggio.Text = String.Empty;
    }


    public void SettaMessaggio(string messaggio)
    {
        Control mess = Master.FindControl("Messaggio1");
        mess.Visible = true;
        ((Label) (mess).FindControl("Label1")).Text = messaggio;
    }
}