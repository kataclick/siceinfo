<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ImpreseRegolariForzaInLista.aspx.cs" Inherits="ImpreseRegolariForzaInLista" %>

<%@ Register Src="../WebControls/ImpreseRegolariMenu.ascx" TagName="ImpreseRegolariMenu"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/CantieriRicercaImpresa.ascx" TagName="CantieriRicercaImpresa"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc3:ImpreseRegolariMenu ID="ImpreseRegolariMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Forzatura"
        titolo="Imprese regolari" />
    <br />
    <table class="standardTable">
        <tr>
            <td colspan="3">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Eccezioni"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Cod.:
            </td>
            <td>
                <asp:TextBox ID="TextBoxIdImpresa" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxIdImpresa"
                    ErrorMessage="*" ValidationExpression="^\d+$" ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
                Ragione sociale:
            </td>
            <td>
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonCerca" runat="server" Text="Cerca" OnClick="ButtonCerca_Click" ValidationGroup="ricerca" />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Errore durante l'operazione"
                    Visible="False"></asp:Label>
                <asp:Label ID="LabelGiaPresente" runat="server" ForeColor="Red" Text="L'impresa selezionata � gi� presente nella lista delle eccezioni"
                    Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="GridViewEccezioni" runat="server" AutoGenerateColumns="False" width="100%" AllowPaging="True" DataKeyNames="ImpresaId" OnRowDeleting="GridViewEccezioni_RowDeleting" OnPageIndexChanging="GridViewEccezioni_PageIndexChanging">
                    <Columns>
                        <asp:BoundField HeaderText="Cod." DataField="ImpresaId">
                            <ItemStyle Width="30px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Ragione sociale" DataField="ImpresaRagioneSociale">
                            <ItemStyle Width="500px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Stato" DataField="Stato" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina" ShowDeleteButton="True" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna eccezione presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label id="Label2" runat="server" Text="Aggiungi impresa" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Stato
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonForzata" runat="server" Text="Forza" GroupName="stato" Checked="True" />
                <asp:RadioButton ID="RadioButtonEscludi" runat="server" Text="Escludi" GroupName="stato" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc2:CantieriRicercaImpresa ID="CantieriRicercaImpresa1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

