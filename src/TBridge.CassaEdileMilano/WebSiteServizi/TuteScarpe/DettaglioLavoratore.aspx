﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DettaglioLavoratore.aspx.cs" Inherits="TuteScarpe_DettaglioLavoratore" %>

<%@ Register src="../WebControls/TuteScarpeMenu.ascx" tagname="TuteScarpeMenu" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/SelezioneTaglie.ascx" tagname="SelezioneTaglie" tagprefix="uc3" %>
<%@ Register src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" tagname="TuteScarpeConsulenteImpresaSelezionata" tagprefix="uc4" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Scheda d'ordine personale del lavoratore"/>
    <br />
    <uc4:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1" 
        runat="server" />
    <br />
    <uc3:SelezioneTaglie ID="SelezioneTaglie1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>

