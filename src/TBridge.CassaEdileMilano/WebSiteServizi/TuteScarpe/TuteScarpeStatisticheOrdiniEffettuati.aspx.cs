using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Microsoft.Reporting.WebForms;

public partial class TuteScarpeStatisticheOrdiniEffettuati : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeStatistiche,
                                              "TuteScarpeStatisticheOrdiniEffettuati.aspx");
    }

    protected void ReportViewerOrdiniEffettuati_Init(object sender, EventArgs e)
    {
        ReportViewerOrdiniEffettuati.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        //ReportViewerOrdiniEffettuati.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportOrdiniEffettuati";
    }

    protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
    {
        ReportViewerOrdiniEffettuati.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportOrdiniEffettuati";

        List<ReportParameter> listaParam = new List<ReportParameter>();
        if (RadDatePickerDataDa.SelectedDate != null)
        {
            ReportParameter param1 = new ReportParameter("dalpadre",
                                         RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());
            listaParam.Add(param1);
        }
        if (RadDatePickerDataA.SelectedDate != null)
        {
            ReportParameter param2 = new ReportParameter("alpadre", RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
            listaParam.Add(param2);
        }

        ReportViewerOrdiniEffettuati.ServerReport.SetParameters(listaParam.ToArray());
    }
}