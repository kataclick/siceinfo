﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SelezioneTaglie.aspx.cs" Inherits="TuteScarpe_SelezioneTaglie" Title="Untitled Page" %>

<%@ Register src="../WebControls/TuteScarpeMenu.ascx" tagname="TuteScarpeMenu" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Tute & Scarpe" sottoTitolo="Selezione Taglie" />
    <br />
    <asp:Label
        ID="LabelMessaggio"
        runat="server"
        Text="Situazione salvata"
        ForeColor="Red"
        Visible="false">
    </asp:Label>
    <br />
    Ogni volta che vengono modificate le taglie occorre salvare la situazione con il pulsante "Salva taglie".
    <br />
    <br />
    <telerik:RadGrid
        ID="RadGridSelezione"
        runat="server"
        Width="100%" GridLines="None" AutoGenerateColumns="False" 
        onitemdatabound="RadGridSelezione_ItemDataBound" 
        ondatabinding="RadGridSelezione_DataBinding">
        <MasterTableView DataKeyNames="TipoTaglia">
        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn UniqueName="TipoTaglia" HeaderText="Tipo Taglia" 
                    DataField="TipoTaglia" Visible="False">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn UniqueName="Indumenti" HeaderText="Indumenti">
                    <ItemTemplate>
                        <telerik:RadGrid
                            ID="RadGridIndumenti"
                            runat="server"
                            Width="100%" GridLines="None" AutoGenerateColumns="False" 
                            ShowHeader="False">
                            <MasterTableView>
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn UniqueName="Indumento" DataField="Descrizione">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </ItemTemplate>
                    <ItemStyle Width="300px" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn UniqueName="Selezione" 
                    HeaderText="Selezione Taglia">
                    <ItemTemplate>
                        <telerik:RadComboBox
                            ID="RadComboBoxSelezioneTaglia"
                            runat="server"
                            Width="95%"
                            AppendDataBoundItems="true">
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidatorTaglia"
                            runat="server"
                            ValidationGroup="salvaTaglie"
                            ErrorMessage="*"
                            ControlToValidate="RadComboBoxSelezioneTaglia">
                        </asp:RequiredFieldValidator>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    <asp:Button
        ID="ButtonSalvaSituazione"
        runat="server"
        Text="Salva Taglie"
        Width="150px" onclick="ButtonSalvaSituazione_Click" 
        ValidationGroup="salvaTaglie" />
</asp:Content>


