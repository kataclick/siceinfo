using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;

/// <summary>
/// Pagina per confermare la cancellazione di un ordine
/// </summary>
public partial class TuteScarpeCancellaOrdine : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini,
                                              "TuteScarpeCancellaOrdine.aspx");

        //?caricare in un gridview l'elenco dei fabbisogni confermati relativi all'ordine (i dati da visualizzare vanno decisi)
        if (!Page.IsPostBack)
        {
            CaricaOrdine();
        }
    }

    private void CaricaOrdine()
    {
        int idOrdine;
        if (Int32.TryParse(Request.QueryString["idOrdine"], out idOrdine))
        {
            Ordine ordine = tsBiz.GetOrdine(idOrdine);

            CaricaDettagli(ordine);

            TextBoxDescrizione.Text = ordine.Descrizione;
            TextBoxDataCreazione.Text = ordine.DataCreazione.ToShortDateString();
        }

        TextBoxIdOrdine.Text = idOrdine.ToString();
    }

    private void CaricaDettagli(Ordine ordine)
    {
        FabbisognoList listaFab = tsBiz.GetDettagliOrdine(ordine);
        GridViewDettagli.DataSource = listaFab;
        GridViewDettagli.DataBind();
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/TuteScarpe/TuteScarpeGestioneOrdini.aspx");
    }

    protected void ButtonConfermaCancellazione_Click(object sender, EventArgs e)
    {
        //1: eliminare tutti i riferimenti all'ordine dai fabbisogni confermati

        //2: eliminare l'ordine
        int idOrdine = int.Parse(TextBoxIdOrdine.Text);

        try
        {
            if (tsBiz.DeleteOrdine(idOrdine))
                LabelRisultato.Text = "Ordine cancellato";
            else
                LabelRisultato.Text = "L'ordine � gi� stato cancellato o confermato da un altro utente";

            ButtonConfermaCancellazione.Enabled = false;
        }
        catch (Exception exc)
        {
        }
    }

    protected void GridViewDettagli_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaOrdine();

        GridViewDettagli.PageIndex = e.NewPageIndex;
        GridViewDettagli.DataBind();
    }
}