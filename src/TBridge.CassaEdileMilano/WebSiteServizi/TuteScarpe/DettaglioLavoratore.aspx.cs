﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class TuteScarpe_DettaglioLavoratore : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno,
                                              "DettaglioLavoratore.aspx");
        #endregion

        if (!Page.IsPostBack)
        {
            Int32? idImpresa = Context.Items["IdImpresa"] as Int32?;
            Int32? idLavoratore = Context.Items["IdLavoratore"] as Int32?;

            if (idImpresa.HasValue && idLavoratore.HasValue)
            {
                SelezioneTaglie1.CaricaFabbisognoLavoratore(idImpresa.Value, idLavoratore.Value);
            }
            else
            {
                Server.Transfer("~/TuteScarpe/GestioneFabbisogno.aspx");
            }
        }
    }
}
