using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;

public partial class TuteScarpeDettagliOrdine : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();
    private int idLavoratore = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Validate("ricerca");
        if (Page.IsValid)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.TuteScarpeGestioneOrdini);
            funzionalita.Add(FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita,
                                                  "TuteScarpeDettaglioOrdine.aspx");

            int idOrdine = Int32.Parse(Request.QueryString["idOrdine"]);

            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ragioneSociale"]))
                    TextBoxRagSoc.Text = Request.QueryString["ragioneSociale"];
                if (!string.IsNullOrEmpty(Request.QueryString["codiceImpresa"]))
                    TextBoxCodice.Text = Request.QueryString["codiceImpresa"];
            }

            //Carichiamo la datagrid con i totali degli indumenti
            CaricaRiassuntoOrdine(idOrdine);
            GridViewOrdineRiassunto.DataBind();
            GridViewOrdineRiassuntoImpLav.DataBind();

            //Carichiamo la datagrid con i totali degli indumenti raggruppati per impresa
            CaricaIndumentiOrdine(idOrdine);
            GridViewIndumentiOrdine.DataBind();

            if (GestioneUtentiBiz.IsFornitore())
            {
                //ButtonMostraDettagli.Visible = false;

                ButtonMostraDettagli.Visible = true;

                CaricaFabbisogniOrdine(idOrdine);
                GridViewFabbisogniOrdine.DataBind();
            }
            else
            {
                ButtonMostraDettagli.Visible = true;

                CaricaFabbisogniOrdine(idOrdine);
                GridViewFabbisogniOrdine.DataBind();
            }
        }
    }

    private void CaricaRiassuntoOrdine(int idOrdine)
    {
        OrdineRiassunto riassunto = tsBiz.GetOrdineRiassunto(idOrdine);
        GridViewOrdineRiassunto.DataSource = riassunto.ListaIndumenti;

        OrdineRiassuntoImpLav riassuntoImpLav = tsBiz.GetOrdineRiassuntoImpLav(idOrdine);
        GridViewOrdineRiassuntoImpLav.DataSource = riassuntoImpLav.ListaElementi;
    }

    private void CaricaIndumentiOrdine(int idOrdine)
    {
        FileOrdine file = tsBiz.GetOrdineFile(idOrdine);
        GridViewIndumentiOrdine.DataSource = FiltraOrdini(file.ListaOrdiniImpresa);
    }

    private FileOrdineImpresaCollection FiltraOrdini(FileOrdineImpresaCollection ordini)
    {
        FileOrdineImpresaCollection filtrata = new FileOrdineImpresaCollection();

        if (!string.IsNullOrEmpty(TextBoxRagSoc.Text) || !string.IsNullOrEmpty(TextBoxCodice.Text))
            foreach (FileOrdineImpresa ordine in ordini)
            {
                if ((string.IsNullOrEmpty(TextBoxRagSoc.Text) ||
                     ordine.RagioneSociale1.ToUpper().Contains(TextBoxRagSoc.Text.ToUpper()))
                    &&
                    (string.IsNullOrEmpty(TextBoxCodice.Text) || ordine.CodiceImpresa.ToString() == TextBoxCodice.Text)
                    )
                    filtrata.Add(ordine);
            }
        else
            filtrata = ordini;

        return filtrata;
    }

    private FabbisognoList FiltraFabbisogni(FabbisognoList fabbisogni)
    {
        FabbisognoList filtrata = new FabbisognoList();

        if (!String.IsNullOrEmpty(TextBoxRagSoc.Text) 
            || !String.IsNullOrEmpty(TextBoxCodice.Text))
        {
            Int32? codice = null;

            if (!String.IsNullOrEmpty(TextBoxCodice.Text))
            {
                codice = Int32.Parse(TextBoxCodice.Text);
            }

            foreach (Fabbisogno fabb in fabbisogni)
            {
                if ((String.IsNullOrEmpty(TextBoxRagSoc.Text) || fabb.RagioneSociale.ToUpper().Contains(TextBoxRagSoc.Text.ToUpper()))
                    && (String.IsNullOrEmpty(TextBoxCodice.Text) || fabb.IdImpresa == codice.Value))
                {
                    filtrata.Add(fabb);
                }
            }
        }
        else
            filtrata = fabbisogni;

        return filtrata;
    }

    private void CaricaFabbisogniOrdine(int idOrdine)
    {
        Ordine ordine = tsBiz.GetOrdine(idOrdine);
        FabbisognoList listaFab = tsBiz.GetDettagliOrdine(ordine);
        GridViewFabbisogniOrdine.DataSource = FiltraFabbisogni(listaFab);
    }

    /// <summary>
    /// Carichiamo il dettaglio del fabbisogno confermato nell gridview
    /// </summary>
    /// <param name="idFabbisogno"></param>
    private void VisualizzaDettagliFabbisogno(int idFabbisogno)
    {
        VisualizzaDettagliFabbisogno(idFabbisogno, 0);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="idFabbisogno"></param>
    /// <param name="pageIndex"></param>
    private void VisualizzaDettagliFabbisogno(int idFabbisogno, int pageIndex)
    {
        GridViewDettagli.Visible = true;

        GridViewDettagli.DataSource = tsBiz.GetFabbisognoDettaglio(idFabbisogno);
        GridViewDettagli.PageIndex = pageIndex;
        GridViewDettagli.DataBind();
    }

    protected void GridViewIndumentiOrdine_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //
            GridView gv = (GridView) e.Row.FindControl("GridViewElencoTaglieIndumento");
            Label lRagioneSociale = (Label) e.Row.FindControl("LabelRagioneSociale");
            Label lIndirizzoSpedizione = (Label) e.Row.FindControl("LabelIndirizzoSpedizione");

            FileOrdineImpresa foi = (FileOrdineImpresa) e.Row.DataItem;
            gv.DataSource = foi.FileOrdineImpresaDettagli;
            gv.DataBind();

            lRagioneSociale.Text = foi.RagioneSociale1;

            if (string.IsNullOrEmpty(foi.RagioneSociale2))
                lIndirizzoSpedizione.Text = foi.IndirizzoCompletoSpedizione;
            else
                lIndirizzoSpedizione.Text = String.Format("Presso: {0}, {1}", foi.RagioneSociale2,
                                                          foi.IndirizzoCompletoSpedizione);

            //if (!(TextBoxRagSoc.Text == string.Empty) && !(foi.RagioneSociale1.ToUpper().Contains(TextBoxRagSoc.Text.ToUpper())))
            //    e.Row.Visible = false;
        }
    }

    protected void GridViewDettagli_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        FabbisognoDettaglio fab = (FabbisognoDettaglio) e.Row.DataItem;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (fab.IdLavoratore != idLavoratore)
            {
                e.Row.Cells[0].Text = fab.IdLavoratore.ToString();
                e.Row.Cells[1].Text = fab.NomeCompleto;
                idLavoratore = fab.IdLavoratore;
            }
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        int origine;
        string pagina;

        Int32.TryParse(Request.QueryString["Origine"], out origine);

        switch (origine)
        {
            case 0:
                pagina = "~/TuteScarpe/TuteScarpeGestioneOrdiniConfermati.aspx";
                break;
            case 1:
                pagina = "~/TuteScarpe/TuteScarpeGestioneOrdini.aspx";
                break;
            default:
                pagina = "~/TuteScarpe/TuteScarpeDefault.aspx";
                break;
        }

        Response.Redirect(pagina);
    }

    protected void ButtonMostraDettagli_Click(object sender, EventArgs e)
    {
        LabelFabbisogniOrdine.Visible = true;
        GridViewFabbisogniOrdine.Visible = true;
    }

    protected void GridViewFabbisogniOrdine_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int idOrdine = Int32.Parse(Request.QueryString["idOrdine"]);
        CaricaFabbisogniOrdine(idOrdine);

        GridViewFabbisogniOrdine.PageIndex = e.NewPageIndex;
        GridViewFabbisogniOrdine.DataBind();
    }

    protected void GridViewDettagli_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int idFabbisogno = (int) ViewState["idFabbisognoSelezionato"];

        VisualizzaDettagliFabbisogno(idFabbisogno, e.NewPageIndex);
    }

    protected void GridViewIndumentiOrdine_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int idOrdine = Int32.Parse(Request.QueryString["idOrdine"]);

        CaricaIndumentiOrdine(idOrdine);
        GridViewIndumentiOrdine.PageIndex = e.NewPageIndex;
        GridViewIndumentiOrdine.DataBind();
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        int idOrdine = Int32.Parse(Request.QueryString["idOrdine"]);

        CaricaIndumentiOrdine(idOrdine);
        GridViewIndumentiOrdine.DataBind();

        LabelFabbisogniOrdine.Visible = false;
        GridViewFabbisogniOrdine.Visible = false;
        GridViewDettagli.Visible = false;
    }

    protected void GridViewFabbisogniOrdine_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Fabbisogno fab = (Fabbisogno) e.Row.DataItem;

            //if (!(TextBoxRagSoc.Text == string.Empty) && !(fab.RagioneSociale.ToUpper().Contains(TextBoxRagSoc.Text.ToUpper())))
            //    e.Row.Visible = false;
        }
    }
    protected void GridViewFabbisogniOrdine_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName=="Dettaglio")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            ViewState["idFabbisognoSelezionato"] = (int)GridViewFabbisogniOrdine.DataKeys[index].Value;
            VisualizzaDettagliFabbisogno((int)ViewState["idFabbisognoSelezionato"]);
        }
    }
}