using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;

/// <summary>
/// Pagina per la modifica di un ordine non confermato: aggiungere/rimuovere fabbisogni confermati
/// FUNZIONALITA' della R2.1 !!!!!
/// </summary>
public partial class TuteScarpeModificaOrdine : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        // Autorizzazione Utenti
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini,
                                              "TuteScarpeModificaOrdini.aspx");

        if (!Page.IsPostBack)
        {
            CaricaOrdine();
        }
    }

    private void CaricaOrdine()
    {
        int idOrdine;
        if (Int32.TryParse(Request.QueryString["idOrdine"], out idOrdine))
        {
            Ordine ordine = tsBiz.GetOrdine(idOrdine);

            CaricaFabbisogni(ordine);
            CaricaDettagli(ordine);

            TextBoxDescrizione.Text = ordine.Descrizione;
            TextBoxDataCreazione.Text = ordine.DataCreazione.ToShortDateString();
        }

        TextBoxIdOrdine.Text = idOrdine.ToString();
    }

    /// <summary>
    /// Carica l'elenco di fabbisogni confermati che compongono l'ordine
    /// </summary>
    /// <param name="ordine"></param>
    private void CaricaDettagli(Ordine ordine)
    {
        FabbisognoList listaFab = tsBiz.GetDettagliOrdine(ordine);
        GridViewDettagli.DataSource = listaFab;
        GridViewDettagli.DataBind();
    }

    protected void GridViewDettagli_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewDettagli.PageIndex = e.NewPageIndex;
        CaricaOrdine();
    }

    protected void GridViewDettagli_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if ((GridViewDettagli.Rows.Count == 1) && (GridViewDettagli.PageCount == 1))
            LabelErrore.Visible = true;
        else
        {
            int idFab = (int) GridViewDettagli.DataKeys[e.RowIndex].Value;
            tsBiz.RimuoviFabbisognoDaOrdine(idFab);
            LabelErrore.Visible = false;
        }

        CaricaOrdine();
    }

    private void CaricaFabbisogni(Ordine ordine)
    {
        FabbisognoList listaFabbisogni;
        int idFabbCompl = tsBiz.GetFabbisognoComplessivoOrdine(ordine.IdOrdine);

        if (idFabbCompl != -1)
        {
            listaFabbisogni = tsBiz.GetFabbisogniConfermatiByComplessivo(idFabbCompl);
            GridViewFabbisogniConfermati.DataSource = listaFabbisogni;
            GridViewFabbisogniConfermati.DataBind();
        }
    }


    protected void GridViewFabbisogniConfermati_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewFabbisogniConfermati.PageIndex = e.NewPageIndex;
        CaricaOrdine();
    }
    protected void GridViewFabbisogniConfermati_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName=="Aggiungi")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            int idFab = (int)GridViewFabbisogniConfermati.DataKeys[index].Value;
            int idOrdine = int.Parse(TextBoxIdOrdine.Text);

            tsBiz.AggiungiFabbisognoAOrdine(idFab, idOrdine);

            CaricaOrdine();
            LabelErrore.Visible = false;
        }
    }
}