<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeDettagliOrdine.aspx.cs" Inherits="TuteScarpeDettagliOrdine" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc3" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Visualizzazione Ordini"/>
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Riassunto ordine:"></asp:Label><br />
    <asp:GridView ID="GridViewOrdineRiassuntoImpLav" runat="server" AutoGenerateColumns="False"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="NomeElemento" />
            <asp:BoundField DataField="Totale" HeaderText="Totale" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:GridView ID="GridViewOrdineRiassunto" runat="server" AutoGenerateColumns="False"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="Indumento" HeaderText="Indumento" />
            <asp:BoundField DataField="Totale" HeaderText="Quantita" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato<br />
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <table class="standardTable">
        <tr>
            <td colspan="3">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Filtro impresa:"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Codice:
            </td>
            <td>
                <asp:TextBox ID="TextBoxCodice" runat="server" Width="451px"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CompareValidatorCodice" runat="server" ControlToValidate="TextBoxCodice" ErrorMessage="*" Operator="GreaterThan" Type="Integer" ValidationGroup="ricerca" ValueToCompare="0"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>
                Ragione Sociale:
            </td>
            <td colspan="2">
                <asp:TextBox ID="TextBoxRagSoc" runat="server" Width="451px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click"
                    Text="Cerca" Width="83px" ValidationGroup="ricerca" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelOrdine" runat="server" Font-Bold="True" Text="Dettagli Ordine:"></asp:Label>&nbsp;<br />
    <asp:GridView ID="GridViewIndumentiOrdine" runat="server" AllowPaging="True"
        Width="100%" AutoGenerateColumns="False" OnRowDataBound="GridViewIndumentiOrdine_RowDataBound" OnPageIndexChanging="GridViewIndumentiOrdine_PageIndexChanging" PageSize="5">
        <Columns>
            <asp:BoundField DataField="CodiceImpresa" HeaderText="Codice Impresa" >
                <ItemStyle Width="30px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Ragione Sociale">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:Label ID="LabelRagioneSociale" runat="server" Font-Bold="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="Indirizzo di spedizione:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelIndirizzoSpedizione" runat="server" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Indumenti - Quantit&#224;">
                <ItemTemplate>
                    <asp:GridView ID="GridViewElencoTaglieIndumento" runat="server" AutoGenerateColumns="False" ShowHeader="False" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="DescrizioneIndumento" HeaderText="Indumento" />
                            <asp:BoundField DataField="Totale" HeaderText="Totale" />
                        </Columns>
                    </asp:GridView>
                </ItemTemplate>
                <ItemStyle Width="250px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto<br />
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button
        ID="ButtonMostraDettagli" runat="server" Text="Mostra dettagli dell'ordine suddiviso per fornitura e lavoratori" OnClick="ButtonMostraDettagli_Click" Width="427px" CausesValidation="False" /><br />
    <br />
    <asp:Label ID="LabelFabbisogniOrdine" runat="server" Font-Bold="True" Text="Ordine dettaglio fabbisogni:"
        Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewFabbisogniOrdine" runat="server" Width="100%" 
        AutoGenerateColumns="False" DataKeyNames="idFabbisogno" AllowPaging="True" 
        OnPageIndexChanging="GridViewFabbisogniOrdine_PageIndexChanging" PageSize="5" 
        Visible="False" OnRowDataBound="GridViewFabbisogniOrdine_RowDataBound" 
        onrowcommand="GridViewFabbisogniOrdine_RowCommand">
        <Columns>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice impresa" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
            <asp:BoundField DataField="progressivoFornitura" 
                HeaderText="Progressivo fornitura" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Codice fornitore" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataConfermaImpresa" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="Data conferma impresa" HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Dettaglio" Text="Dettagli" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:GridView ID="GridViewDettagli" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewDettagli_RowDataBound"
        Visible="False" Width="100%" AllowPaging="True" OnPageIndexChanging="GridViewDettagli_PageIndexChanging">
        <Columns>
            <asp:BoundField HeaderText="Codice lavoratore" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Nome" />
            <asp:BoundField DataField="Indumento" HeaderText="Indumento" >
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="Quantita" HeaderText="Quantit&#224;" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="Taglia" HeaderText="Taglia" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
        Width="190px" CausesValidation="False" />
</asp:Content>
