using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Image=System.Web.UI.WebControls.Image;

public partial class TuteScarpeGestioneFatture : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFatture,
                                              "TuteScarpeGestioneFatture.aspx");
    }

    protected void ButtonCerca_Click(object sender, EventArgs e)
    {
        bool? corrette = null;
        bool? forzate = null;
        DateTime dal = DateTime.Parse(TextBoxDal.Text);
        DateTime al = DateTime.Parse(TextBoxAl.Text);

        if (RadioButtonCorrette.Checked)
            corrette = true;
        else if (RadioButtonNonCorrette.Checked)
            corrette = false;

        if (RadioButtonForzate.Checked)
            forzate = true;
        else if (RadioButtonNonForzate.Checked)
            forzate = false;

        ViewState["Dal"] = dal;
        ViewState["Al"] = al;
        ViewState["Corrette"] = corrette;
        ViewState["Forzate"] = forzate;

        CaricaFatture();
        GridViewFatture.DataBind();
        if (GridViewFatture.Rows.Count > 0)
        {
            LabelFiltro.Visible = true;
            LabelRagSoc.Visible = true;
            TextBoxRagSoc.Visible = true;
        }

        LabelDettagliFattura.Visible = false;
        LabelCodiceFattura.Visible = false;
        LabelRiassunto.Visible = false;
        LabelDettagli.Visible = false;
        GridViewDettaglioFattura.Visible = false;
        GridViewRiassunto.Visible = false;
    }

    private void CaricaFatture()
    {
        if ((ViewState["Dal"] != null) && (ViewState["Al"] != null))
        {
            DateTime dal = (DateTime) ViewState["Dal"];
            DateTime al = (DateTime) ViewState["Al"];
            bool? corrette = null;
            bool? forzate = null;

            if (ViewState["Corrette"] != null)
                corrette = (bool) ViewState["Corrette"];
            if (ViewState["Forzate"] != null)
                forzate = (bool) ViewState["Forzate"];

            FatturaList listaFatture = tsBiz.GetFatture(dal, al, corrette, forzate);

            LabelElencoFatture.Visible = true;
            GridViewFatture.DataSource = listaFatture;
        }
    }

    protected void GridViewFatture_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        // Visualizzazione dettagli
        int idFattura = (int) GridViewFatture.DataKeys[e.RowIndex].Value;

        Fattura fattura = tsBiz.CreaFatturaDalDB(idFattura);
        tsBiz.VerificaFattura(fattura);

        LabelDettagliFattura.Visible = true;
        LabelCodiceFattura.Visible = true;
        GridViewDettaglioFattura.Visible = true;
        GridViewRiassunto.Visible = true;
        LabelRiassunto.Visible = true;
        LabelDettagli.Visible = true;

        LabelIdFattura.Text = idFattura.ToString();
        LabelCodiceFattura.Text = GridViewFatture.Rows[e.RowIndex].Cells[0].Text;
        GridViewDettaglioFattura.DataSource = FiltraBolle(fattura.Bolle);
        GridViewDettaglioFattura.DataBind();

        FatturaRiassunto riassunto = tsBiz.GetRiassuntoFattura(idFattura);
        GridViewRiassunto.DataSource = riassunto;
        GridViewRiassunto.DataBind();
    }

    private BollaList FiltraBolle(BollaList bolle)
    {
        BollaList filtrata = new BollaList();

        if (!string.IsNullOrEmpty(TextBoxRagSoc.Text))
            foreach (Bolla bolla in bolle)
            {
                if (bolla.RagioneSociale.ToUpper().Contains(TextBoxRagSoc.Text.ToUpper()))
                    filtrata.Add(bolla);
            }
        else
            filtrata = bolle;

        return filtrata;
    }

    protected void GridViewDettaglioFattura_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Bolla bolla = (Bolla) e.Row.DataItem;

            // Bind del grid view interno
            GridView gvInterno = (GridView) e.Row.FindControl("GridViewDettaglio");
            gvInterno.DataSource = bolla.VociBolla;
            gvInterno.DataBind();

            // Bind dello stato
            GridView gvStato = (GridView) e.Row.FindControl("GridViewStato");
            gvStato.DataSource = bolla.Stato;
            gvStato.DataBind();

            if ((bolla.Stato != null) && (bolla.Stato.Count > 0))
            {
                foreach (StatoBollaClass statoBolla in bolla.Stato)
                {
                    switch (statoBolla.Stato)
                    {
                        case StatoBolla.GiaGestita:
                            e.Row.Cells[0].ForeColor = Color.Red;
                            e.Row.Cells[1].ForeColor = Color.Red;
                            e.Row.Cells[2].ForeColor = Color.Red;
                            break;
                        case StatoBolla.ImpresaNonPresente:
                            e.Row.Cells[0].ForeColor = Color.Red;
                            e.Row.Cells[1].ForeColor = Color.Red;
                            break;
                        case StatoBolla.OrdineAltroFornitore:
                            e.Row.Cells[2].ForeColor = Color.Red;
                            break;
                        case StatoBolla.OrdineNonPresente:
                            e.Row.Cells[2].ForeColor = Color.Red;
                            break;
                    }
                }
            }
        }
    }

    protected void GridViewDettaglio_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            VoceBolla voce = (VoceBolla) e.Row.DataItem;

            if (voce.Quantita == voce.QuantitaFabbisogno)
            {
                e.Row.Cells[2].Text = "=";
                //e.Row.ForeColor = Color.Green;
                //e.Row.Visible = false;
            }
            if (voce.Quantita > voce.QuantitaFabbisogno)
            {
                e.Row.Cells[2].Text = ">";
                e.Row.ForeColor = Color.Red;
            }
            if (voce.Quantita < voce.QuantitaFabbisogno)
            {
                e.Row.Cells[2].Text = "<";
                e.Row.ForeColor = Color.Red;
            }
        }
    }

    protected void GridViewStato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            StatoBollaClass stato = (StatoBollaClass) e.Row.DataItem;
            e.Row.Cells[0].Text = stato.GetTesto();
        }
    }

    protected void GridViewFatture_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaFatture();
        GridViewFatture.PageIndex = e.NewPageIndex;
        GridViewFatture.DataBind();
    }

    protected void GridViewDettaglioFattura_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // Visualizzazione dettagli
        try
        {
            int idFattura = Int32.Parse(LabelIdFattura.Text);

            Fattura fattura = tsBiz.CreaFatturaDalDB(idFattura);
            tsBiz.VerificaFattura(fattura);

            LabelDettagliFattura.Visible = true;
            GridViewDettaglioFattura.DataSource = fattura.Bolle;
            GridViewDettaglioFattura.PageIndex = e.NewPageIndex;
            GridViewDettaglioFattura.DataBind();
        }
        catch
        {
        }
    }

    protected void GridViewFatture_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // Apertura del file presente nel DB
        int idFattura = (int) GridViewFatture.DataKeys[e.RowIndex]["IdFattura"];
        int codiceFattura = (int) GridViewFatture.DataKeys[e.RowIndex]["CodiceFattura"];

        Response.Redirect("~/TuteScarpe/TuteScarpeCancellaFattura.aspx?idFattura=" + idFattura +
                          "&codiceFattura=" + codiceFattura);
    }

    protected void GridViewFatture_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Fattura fattura = (Fattura) e.Row.DataItem;
            Image imageStato = (Image) e.Row.FindControl("ImageStato");
            Image imageForzata = (Image) e.Row.FindControl("ImageForzata");

            if (fattura.StatoInserimento)
                imageStato.ImageUrl = "~/images/PallinoVverde.png";
            else
                imageStato.ImageUrl = "~/images/PallinoXRosso.png";

            if (fattura.ForzataCorrettezza)
                imageForzata.ImageUrl = "~/images/PallinoVverde.png";
            else
                imageForzata.Visible = false;

            if (fattura.StatoInserimento || fattura.ForzataCorrettezza)
                e.Row.Cells[6].Enabled = false;
        }
    }

    protected void GridViewFatture_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Forza")
        {
            // Apertura del file presente nel DB
            int idFattura = (int) GridViewFatture.DataKeys[Int32.Parse(e.CommandArgument.ToString())]["IdFattura"];
            int codiceFattura =
                (int) GridViewFatture.DataKeys[Int32.Parse(e.CommandArgument.ToString())]["CodiceFattura"];

            Response.Redirect("~/TuteScarpe/TuteScarpeConfermaForzaturaFattura.aspx?idFattura=" + idFattura +
                              "&codiceFattura=" + codiceFattura);
        }

        if(e.CommandName == "Visualizza")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            // Apertura del file presente nel DB
            int idFattura = (int)GridViewFatture.DataKeys[index]["IdFattura"];
            int codiceFattura = (int)GridViewFatture.DataKeys[index]["CodiceFattura"];

            byte[] file = tsBiz.GetFileDellaFattura(idFattura);

            Encoding enc = Encoding.GetEncoding("ASCII");
            Response.ContentType = "text/plain";
            Response.AddHeader("Content-Disposition", @"attachment; filename=FV_" + codiceFattura + ".txt");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(enc.GetString(file));
            Response.End();
        }
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        //foreach (GridViewRow row in GridViewFatture.Rows)
        //{
        //    if (row.RowType == DataControlRowType.DataRow)
        //    {
        //        if (!(TextBoxRagSoc.Text == string.Empty) && !(row.Cells[2].Text.ToUpper().Contains(TextBoxRagSoc.Text.ToUpper())))
        //            row.Visible = false;
        //    }
        //}
        int i = 0;
    }
}