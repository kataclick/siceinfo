using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Fornitore=TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore;

/// <summary>
/// Pagina per la gestione degli ordini confermati. In questa fase R2.0 serve solo al fornitore per potersi scaricare il file 
/// .TXT corrispondente ad un certo ordine
/// </summary>
public partial class TuteScarpeGestioneOrdiniConfermati : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    private string codiceFornitore;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati);
        #endregion

        if (GestioneUtentiBiz.IsFornitore())
        {
            Fornitore
                fornitoreEntity =
                    (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            if (fornitoreEntity != null)
            {
                codiceFornitore = fornitoreEntity.Codice;
            }
        }
        else
        {
            codiceFornitore = null;
        }

/*        if (!Page.IsPostBack)
        {
            Ricerca(codiceFornitore, null, null, null, null);
        }
 */
    }

    protected void ButtonRicercaOrdiniConfermati_Click(object sender, EventArgs e)
    {
        Ricerca(0);
    }

    private void Ricerca(int pageIndex)
    {
        if (Page.IsValid)
        {
            DateTime? dal = null;
            DateTime? al = null;
            string ragioneSociale = null;
            int? idImpresa = null;

            try
            {
                if (!string.IsNullOrEmpty(TextBoxDal.Text))
                    dal = DateTime.Parse(TextBoxDal.Text);
                if (!string.IsNullOrEmpty(TextBoxAl.Text))
                    al = DateTime.Parse(TextBoxAl.Text);
                if (!string.IsNullOrEmpty(TextBoxRagSoc.Text))
                    ragioneSociale = TextBoxRagSoc.Text;
                if (!string.IsNullOrEmpty(TextBoxCodice.Text))
                    idImpresa = Int32.Parse(TextBoxCodice.Text);

                LabelFormatoDate.Visible = false;

                if ((!dal.HasValue) || (!al.HasValue) || (dal <= al))
                {
                    GridViewOrdiniConfermati.DataSource = Ricerca(codiceFornitore, dal, al, ragioneSociale, idImpresa);
                    GridViewOrdiniConfermati.PageIndex = pageIndex;
                    GridViewOrdiniConfermati.DataBind();

                    LabelErrore.Visible = false;
                }
                else
                    LabelErrore.Visible = true;
            }
            catch (FormatException)
            {
                LabelFormatoDate.Visible = true;
            }
        }
    }

    private OrdineList Ricerca(string codiceFornitore, DateTime? dal, DateTime? al, string ragioneSociale,
                               int? idImpresa)
    {
        OrdineList listaOrdini = tsBiz.GetOrdini(codiceFornitore, true, dal, al, ragioneSociale, idImpresa);
        return filtraLista(listaOrdini);
    }

    private OrdineList filtraLista(IEnumerable<Ordine> lista)
    {
        OrdineList listaRes = new OrdineList();

        foreach (Ordine ord in lista)
        {
            if (((ord.ScaricatoFornitore.HasValue) && (RadioButtonScaricati.Checked)) ||
                ((!ord.ScaricatoFornitore.HasValue) && (!RadioButtonScaricati.Checked)) || (RadioButtonTutti.Checked))
                listaRes.Add(ord);
        }

        return listaRes;
    }

    protected void GridViewOrdiniConfermati_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int idOrdine = (int) GridViewOrdiniConfermati.DataKeys[e.NewEditIndex].Value;

        FileOrdine file = tsBiz.GetOrdineFile(idOrdine);
        string res = file.ScriviFile();

        // Memorizzo che il file � stato scaricato
        if (GestioneUtentiBiz.IsFornitore())
            tsBiz.OrdineScaricatoDaFornitore(idOrdine);

        Response.ContentType = "text/plain";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.txt", idOrdine));
        Response.Charset = "";
        EnableViewState = false;
        Response.Write(res);
        Response.End();
    }

    protected void GridViewOrdiniConfermati_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Page.Validate("ricerca");

        if (Page.IsValid)
        {
            int idOrdine = (int) GridViewOrdiniConfermati.DataKeys[e.RowIndex].Value;

            Response.Redirect(
                string.Format(
                    "~/TuteScarpe/TuteScarpeDettagliOrdine.aspx?idOrdine={0}&Origine=0&ragioneSociale={1}&codiceImpresa={2}",
                    idOrdine, TextBoxRagSoc.Text, TextBoxCodice.Text));
        }
    }

    protected void GridViewOrdiniConfermati_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Page.Validate("ricerca");

        if (Page.IsValid)
        {
            Ricerca(e.NewPageIndex);
        }
    }
}