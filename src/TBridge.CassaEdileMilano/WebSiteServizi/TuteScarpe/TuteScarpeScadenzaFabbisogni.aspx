<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TuteScarpeScadenzaFabbisogni.aspx.cs" Inherits="TuteScarpeScadenzaFabbisogni" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe"
        sottoTitolo="Gestione data scadenza della proposta di fabbisogno" />
    &nbsp;<br />
    <asp:Label ID="LabelNoFabbisogno" runat="server" Text="Non esistono fabbisogni nel database"
        Visible="False"></asp:Label>&nbsp;
    <asp:Panel ID="Panel" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td style="width: 300px">
                    <asp:Label ID="Label2" runat="server" Text="Data generazione"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataGenerazione" runat="server" Width="235px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <asp:Label ID="Label3" runat="server" Text="Codice"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodice" runat="server" Width="235px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <asp:CheckBox ID="CheckBoxDataScadenza" runat="server" OnCheckedChanged="CheckBoxDataScadenza_CheckedChanged"
                        Text="Data scadenza (gg/mm/aaaa)" AutoPostBack="True"/>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataScadenza" runat="server" Width="233px"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidatorDataScadenza" runat="server" ControlToValidate="TextBoxDataScadenza"
                        Operator="DataTypeCheck" Type="Date" ErrorMessage="Formato data scadenza errato"
                        ValidationGroup="salvaScadenze">*</asp:CompareValidator>
                    <asp:CustomValidator ID="CustomValidatorDataScadenzaSuccessivaGenerazione" runat="server"
                        ErrorMessage="La data di scadenza deve essere successiva alla data di generazione"
                        ValidationGroup="salvaScadenze" OnServerValidate="CustomValidatorDataScadenzaSuccessivaGenerazione_ServerValidate">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                    <asp:CheckBox ID="CheckBoxDataConfermaAutomatica" runat="server" Text="Data conferma automatica (gg/mm/aaaa)"
                        AutoPostBack="True" Enabled="false" OnCheckedChanged="CheckBoxDataConfermaAutomatica_CheckedChanged" />
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataConfermaAutomatica" runat="server" Width="233px" Enabled="false"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidatorDataConfermaAutomatica" runat="server"
                        ControlToValidate="TextBoxDataConfermaAutomatica" Operator="DataTypeCheck" Type="Date"
                        ErrorMessage="Formato data conferma automatica errato" ValidationGroup="salvaScadenze">*</asp:CompareValidator>
                    <asp:CustomValidator ID="CustomValidatorDataScadenzaSuccessivaGenerazionePrecedenteScadenza"
                        runat="server" ErrorMessage="La data di conferma automatica deve essere compresa tra la data di generazione e quella di scadenza"
                        ValidationGroup="salvaScadenze" OnServerValidate="CustomValidatorDataScadenzaSuccessivaGenerazionePrecedenteScadenza_ServerValidate">*</asp:CustomValidator>
                    <asp:CustomValidator ID="CustomValidatorDataConfermaMaggioreOggi" runat="server"
                        ErrorMessage="La data di conferma automatica deve essere uguale o successiva alla data odierna"
                        ValidationGroup="salvaScadenze" OnServerValidate="CustomValidatorDataConfermaMaggioreOggi_ServerValidate">*</asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 300px">
                </td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummarySalvaScadenza" runat="server" ValidationGroup="salvaScadenze"
                        CssClass="messaggiErrore" />
                    <asp:Button ID="ButtonSalva" runat="server" OnClick="ButtonSalva_Click" Text="Salva scadenze"
                        Width="150px" ValidationGroup="salvaScadenze" /><br />
                    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="standardTable">
                        <tr>
                            <td style="width: 180px">
                            </td>
                            <td>
                                <asp:Label ID="LabelDataConfermaAutomaticaEffettuata" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 180px">
                                <asp:Button ID="ButtonInviaMail" runat="server" Width="150px" Text="Invia Mail" Enabled="false"
                                    OnClick="ButtonInviaMail_Click" />
                            </td>
                            <td>
                                <asp:Label ID="LabelMailInviata" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 180px">
                                <asp:Button ID="ButtonInviaSms" runat="server" Width="150px" Text="Invia SMS" 
                                    Enabled="true" onclick="ButtonInviaSms_Click" />
                            </td>
                            <td>
                                <asp:Label ID="LabelSmsInviati" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
</asp:Content>
