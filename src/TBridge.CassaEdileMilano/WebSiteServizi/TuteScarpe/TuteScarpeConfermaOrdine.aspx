<%@ Page Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeConfermaOrdine.aspx.cs" Inherits="TuteScarpeConfermaOrdine" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server"  titolo="Gestione Tute e Scarpe" sottoTitolo="Conferma ordine" />
    <br />
    Attenzione: s� scelto di confermare l'ordine. Con questa operazione, l'ordine non sar� pi� modificabile e potr� essere visualizzato dal fornitore.<br />
    <br />
    <table class="standardTable">
        <tr>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxIdOrdine" runat="server" Enabled="False" Visible="False"
                    Width="249px"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="height: 28px" align="left">
                Descrizione
            </td>
            <td style="width: 3px; height: 28px;" align="left">
                <asp:TextBox ID="TextBoxDescrizione" runat="server" Enabled="False" Width="249px"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
                Data creazione
            </td>
            <td style="width: 3px" align="left">
                <asp:TextBox ID="TextBoxDataCreazione" runat="server" Enabled="False" ReadOnly="True"
                    Width="249px"></asp:TextBox>
            </td>
            <td>
                </td>
        </tr>
        <tr>
            <td>
    Seleziona il fornitore che dovr� gestire l'ordine:</td>
            <td style="width: 3px" align="left">
    <asp:DropDownList ID="DropDownListFornitori" runat="server" Width="256px">
    </asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="DropDownListFornitori">Selezionare un fornitore</asp:RequiredFieldValidator></td>            
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red" Width="451px"></asp:Label><br />
    <br />
    <asp:Button ID="ButtonConferma" runat="server" OnClick="ButtonConferma_Click" Text="Conferma ordine"
        Width="153px" />
    <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Torna indietro"
        Width="153px" /></asp:Content>
