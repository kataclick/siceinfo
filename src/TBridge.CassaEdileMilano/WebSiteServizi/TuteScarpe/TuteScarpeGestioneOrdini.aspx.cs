using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.Presenter;

public partial class TuteScarpeGestioneOrdini : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini,
                                              "TuteScarpeGestioneOrdini.aspx");

        //Caricare subito la lista degli ordini con un filtro predefinito: ovvero tutti gli ordini non ancora confermati indipendentemente 
        //dal perdiodo e da un particolare fabbisogno complessivo)

        if (!Page.IsPostBack)
        {
            FiltroERicerca(0);
        }
    }

    protected void ButtonFiltraOrdini_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            FiltroERicerca(0);
        }
    }

    private void FiltroERicerca(Int32 pagina)
    {
        bool confermati = RadioButtonConfermati.Checked;
        DateTime? dal = null;
        DateTime? al = null;

        if (!string.IsNullOrEmpty(TextBoxDa.Text))
        {
            dal = DateTime.Parse(TextBoxDa.Text);
        }

        if (!string.IsNullOrEmpty(TextBoxDa.Text))
        {
            al = DateTime.Parse(TextBoxA.Text);
        }

        Ricerca(confermati, dal, al, pagina);
    }

    private void Ricerca(bool confermati, DateTime? dal, DateTime? al, Int32 pagina)
    {
        // Layout del gridview
        if (confermati)
        {
            GridViewOrdini.Columns[2].Visible = true;
            GridViewOrdini.Columns[6].Visible = true;
            GridViewOrdini.Columns[7].Visible = false;
            GridViewOrdini.Columns[8].Visible = false;
            GridViewOrdini.Columns[9].Visible = false;
        }
        else
        {
            GridViewOrdini.Columns[2].Visible = false;
            GridViewOrdini.Columns[6].Visible = false;
            GridViewOrdini.Columns[7].Visible = true;
            GridViewOrdini.Columns[8].Visible = true;
            GridViewOrdini.Columns[9].Visible = true;
        }

        OrdineList listaOrdini = tsBiz.GetOrdini(confermati, dal, al);
        Presenter.CaricaElementiInGridView(
            GridViewOrdini,
            listaOrdini,
            pagina);
    }

    protected void GridViewOrdini_RowEditing(object sender, GridViewEditEventArgs e)
    {
        // Redirect su Elimina
        //int indice = GridViewOrdini.PageIndex * GridViewOrdini.PageSize + e.NewEditIndex;
        int idOrdine = (int) GridViewOrdini.DataKeys[e.NewEditIndex].Value;

        Response.Redirect("~/TuteScarpe/TuteScarpeModificaOrdine.aspx?idOrdine=" + idOrdine);
    }

    protected void GridViewOrdini_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // Redirect su Elimina
        //int indice = GridViewOrdini.PageIndex * GridViewOrdini.PageSize + e.RowIndex;
        int idOrdine = (int) GridViewOrdini.DataKeys[e.RowIndex].Value;

        Response.Redirect("~/TuteScarpe/TuteScarpeCancellaOrdine.aspx?idOrdine=" + idOrdine);
    }

    protected void GridViewOrdini_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        // Redirect su Conferma
        //int indice = GridViewOrdini.PageIndex * GridViewOrdini.PageSize + e.RowIndex;
        int idOrdine = (int) GridViewOrdini.DataKeys[e.RowIndex].Value;

        Response.Redirect("~/TuteScarpe/TuteScarpeConfermaOrdine.aspx?idOrdine=" + idOrdine);
    }

    protected void GridViewOrdini_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        FiltroERicerca(e.NewPageIndex);
    }

    protected void GridViewOrdini_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int idOrdine = (int) GridViewOrdini.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Value;
            Response.Redirect("~/TuteScarpe/TuteScarpeDettagliOrdine.aspx?idOrdine=" + idOrdine +
                              "&Origine=1");
        }
    }
}
