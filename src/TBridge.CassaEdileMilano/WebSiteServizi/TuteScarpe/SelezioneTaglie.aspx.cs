﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Telerik.Web.UI;

public partial class TuteScarpe_SelezioneTaglie : Page
{
    private readonly TSBusiness biz = new TSBusiness();
    private TagliePerTipoCollection taglieSelezionate;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack
            && GestioneUtentiBiz.IsLavoratore())
        {
            if (GestioneUtentiBiz.IsLavoratore())
            {
                CaricaTaglieDisponibili();
                ButtonSalvaSituazione.Enabled = true;
            }
            else
            {
                ButtonSalvaSituazione.Enabled = false;
            }
        }
    }

    private void CaricaTaglieDisponibili()
    {
        Presenter.CaricaElementiInGridView(
            RadGridSelezione,
            biz.GetTagliePerTipoTaglia());
    }

    protected void RadGridSelezione_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            TagliePerTipo tagliePerTipo = (TagliePerTipo) e.Item.DataItem;

            RadGrid rgIndumenti = (RadGrid) e.Item.FindControl("RadGridIndumenti");
            Presenter.CaricaElementiInGridView(
                rgIndumenti,
                tagliePerTipo.Indumento);

            RadComboBox rcbSelezione = (RadComboBox) e.Item.FindControl("RadComboBoxSelezioneTaglia");
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                rcbSelezione,
                tagliePerTipo.Taglie,
                String.Empty,
                String.Empty);

            if (taglieSelezionate != null)
            {
                TagliePerTipo tpt = taglieSelezionate.FindByTipoTaglia(tagliePerTipo.TipoTaglia);

                if (tpt != null)
                {
                    rcbSelezione.SelectedValue = tpt.TagliaScelta;
                }
            }
        }
    }

    protected void ButtonSalvaSituazione_Click(object sender, EventArgs e)
    {
        LabelMessaggio.Visible = false;

        if (Page.IsValid)
        {
            TagliePerTipoCollection taglieSelezionate = CreaTaglieSelezionate();

            biz.InserisciTaglieSelezionate(taglieSelezionate);
            CaricaTaglieDisponibili();

            LabelMessaggio.Visible = true;
        }
    }

    private TagliePerTipoCollection CreaTaglieSelezionate()
    {
        TagliePerTipoCollection tpts = new TagliePerTipoCollection();

        Lavoratore lavoratore =
            (Lavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

        foreach (GridDataItem gdi in RadGridSelezione.MasterTableView.Items)
        {
            TagliePerTipo tpt = new TagliePerTipo();
            tpts.Add(tpt);

            RadComboBox rcbTaglia = (RadComboBox) gdi.FindControl("RadComboBoxSelezioneTaglia");

            tpt.TipoTaglia = (String) RadGridSelezione.MasterTableView.DataKeyValues[gdi.ItemIndex]["TipoTaglia"];
            tpt.IdLavoratore = lavoratore.IdLavoratore;
            tpt.TagliaScelta = rcbTaglia.SelectedValue;
        }

        return tpts;
    }

    protected void RadGridSelezione_DataBinding(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsLavoratore())
        {
            Lavoratore lavoratore =
                (Lavoratore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            taglieSelezionate = biz.GetTaglieSelezionateLavoratore(lavoratore.IdLavoratore);
        }
    }
}