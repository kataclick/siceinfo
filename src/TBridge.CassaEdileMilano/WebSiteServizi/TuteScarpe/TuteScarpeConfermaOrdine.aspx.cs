using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Telerik.Web.UI;

/// <summary>
/// Pagina per confermare l'ordine
/// </summary>
public partial class TuteScarpeConfermaOrdine : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini);

        //Caricare la lista dei fornitori nella dropdownlistFornitori
        if (!Page.IsPostBack)
        {
            int idOrdine;
            if (Int32.TryParse(Request.QueryString["idOrdine"], out idOrdine))
            {
                Ordine ordine = tsBiz.GetOrdine(idOrdine);

                TextBoxDescrizione.Text = ordine.Descrizione;
                TextBoxDataCreazione.Text = ordine.DataCreazione.ToShortDateString();

                FornitoreList listaFornitori = tsBiz.GetFornitori();
                DropDownListFornitori.DataSource = listaFornitori;
                DropDownListFornitori.DataTextField = "CodiceFornitore";
                DropDownListFornitori.DataValueField = "CodiceFornitore";
                DropDownListFornitori.DataBind();

                if (DropDownListFornitori.Items.FindByValue("OPEN") != null)
                {
                    DropDownListFornitori.SelectedValue = "OPEN";
                }
            }

            TextBoxIdOrdine.Text = idOrdine.ToString();
        }

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null) + ";");
        sb.Append("return true;");
        ButtonConferma.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonConferma);
        
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        int idOrdine = Int32.Parse(TextBoxIdOrdine.Text);
        string codiceFornitore = DropDownListFornitori.SelectedItem.Value;
        if ((idOrdine != -1) && (!string.IsNullOrEmpty(codiceFornitore)))
        {
            try
            {
                if (tsBiz.ConfermaOrdine(idOrdine, codiceFornitore))
                {
                    ButtonConferma.Enabled = false;
                    LabelRisultato.Text = "Ordine confermato";
                }
                else
                    LabelRisultato.Text = "L'ordine � stato gi� confermato da un altro utente";

                ButtonConferma.Enabled = false;
            }
            catch (Exception exc)
            {
                // Errore
            }
            //3: Mandare una mail al fornitore (se la mail � stata inserita)
        }
        //4: feedback all'utente
    }

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/TuteScarpe/TuteScarpeGestioneOrdini.aspx");
    }
}