<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeCreazioneOrdine.aspx.cs" Inherits="TuteScarpeCreazioneOrdine" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Creazione ordine"/>
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Creazione Ordine"></asp:Label><br />
    <br />
    <table class="standardTable">
        <tr>
            <td>
    Descrizione:</td>
            <td>
    <asp:TextBox ID="TextBoxDescrizioneOrdine" runat="server" Height="38px" Width="327px"></asp:TextBox></td>
            <td>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxDescrizioneOrdine"
        ErrorMessage="Fornire una descrizione" Width="7px"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                Fabbisogno complessivo:</td>
            <td>
    <asp:DropDownList ID="DropDownListElencoFabbisogni" runat="server" Width="332px" OnSelectedIndexChanged="DropDownListElencoFabbisogni_SelectedIndexChanged" AutoPostBack="True">
    </asp:DropDownList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="left">
                </td>
            <td align="left">
                <asp:CheckBox ID="CheckBoxConfermato" runat="server" Visible="False" /></td>
        </tr>
    </table>
    <br />
    <strong>Fabbisogni confermati</strong><asp:GridView 
        ID="GridViewFabbisogniConfermati" runat="server" AutoGenerateColumns="False" 
        Width="100%" DataKeyNames="IdFabbisogno" AllowPaging="True" 
        OnPageIndexChanging="GridViewFabbisogniConfermati_PageIndexChanging" 
        PageSize="6" onrowcommand="GridViewFabbisogniConfermati_RowCommand">
        <Columns>
            <asp:BoundField DataField="IdFabbisogno" HeaderText="Id Fabbisogno" 
                Visible="False" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Fornitore" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataConfermaImpresa" 
                HeaderText="Data conferma impresa" DataFormatString="{0:dd/MM/yyyy}" 
                HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Aggiungi" Text="Aggiungi all'ordine" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun fabbisogno disponibile<br />
        </EmptyDataTemplate>
    </asp:GridView>
    <table class="standardTable">
        <tr>
            <td align="right" style="width: 620px">
    
    <asp:Button ID="ButtonAggiungiTutti" runat="server" Text="Aggiungi tutti" CausesValidation="False" OnClick="ButtonAggiungiTutti_Click" Width="154px" /></td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Il fabbisogno � stato gi� inserito o � stato inserito da un altro utente in un ordine"
        Visible="False" Width="507px"></asp:Label><br />
    <b>Composizione ordine</b><br />
    <asp:GridView ID="GridViewOrdine" runat="server" AutoGenerateColumns="False"
        Width="100%" DataKeyNames="IdFabbisogno" AllowPaging="True" 
        OnPageIndexChanging="GridViewOrdine_PageIndexChanging" PageSize="6" 
        onrowcommand="GridViewOrdine_RowCommand">
        <Columns>
            <asp:BoundField DataField="IdFabbisogno" HeaderText="Id Fabbisogno" 
                Visible="False" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Fornitore" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataConfermaImpresa" 
                HeaderText="Data conferma impresa" DataFormatString="{0:dd/MM/yyyy}" 
                HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Rimuovi" Text="Rimuovi dall'ordine" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Ordine temporaneo vuoto&nbsp;
        </EmptyDataTemplate>
    </asp:GridView>
    <table class="standardTable">
        <tr>
            <td align="right" style="width: 620px">
                <asp:Button ID="ButtonRimuoviTutti" runat="server" CausesValidation="False" OnClick="ButtonRimuoviTutti_Click"
                    Text="Rimuovi tutti" Width="154px" /></td>
        </tr>
    </table>
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label><br />
    <asp:Button ID="ButtonCreaOrdine" runat="server" Text="Crea ordine" Width="151px" OnClick="ButtonCreaOrdine_Click" />
</asp:Content>

