using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;

public partial class TuteScarpeConfermaForzaturaFattura : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();
    private int codiceFattura = -1;
    private int idFattura = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFatture);

        try
        {
            idFattura = Int32.Parse(Request.QueryString["idFattura"]);
            codiceFattura = Int32.Parse(Request.QueryString["codiceFattura"]);
        }
        catch
        {
        }

        if ((idFattura != -1) && (codiceFattura != -1))
        {
            LabelCodiceFattura.Text = codiceFattura.ToString();
        }

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null) + ";");
        sb.Append("return true;");
        ButtonConferma.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonConferma);
        
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        if ((idFattura != -1) && (tsBiz.ForzaFattura(idFattura)))
        {
            ButtonConferma.Enabled = false;
            LabelRisultato.Text = "Fattura forzata correttamente";
            LabelRisultato.Visible = true;
        }
        else
        {
            LabelRisultato.Text = "Errore durante la forzatura";
            LabelRisultato.Visible = true;
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/TuteScarpe/TuteScarpeGestioneFatture.aspx");
    }
}