using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;

/// <summary>
///   Pagina che permette all'utente abilitato di selezionare una impresa e gestirne il fabbisogno in caso di ultimissime richieste di modifiche arrivate telefonicamente
///   Tali richieste sono limitate al cambio di taglia!
/// </summary>
public partial class TuteScarpeGestioneFabbisogniConfermati : Page
{
    private readonly Common _commonBiz = new Common();
    private readonly TSBusiness tsBiz = new TSBusiness();
    private int idLavoratore = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogniConfermati,
                                                       "TuteScarpeGestioneFabbisogniConfermati.aspx");

        // La pagina va caricata con i filtri attivi
        string codice = string.Empty;
        string ragioneSociale = string.Empty;

        if (!Page.IsPostBack)
        {
            Ricerca(0);
            CaricaProvince();

            int idProvincia = -1;
            if ((DropDownListProvincia.SelectedValue != null) &&
                (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                CaricaComuni(idProvincia);

            Int64 idComune = -1;
            if ((DropDownListComuni.SelectedValue != null) &&
                (Int64.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                CaricaCAP(idComune);
        }
    }

    private void Ricerca(Int32 pagina)
    {
        FabbisognoComplessivoList listaFabbisogni = null;
        TipoRicerca ricerca = CreaFiltro();

        switch (ricerca.Tipo)
        {
            case TipoRicercaEnum.Tutti:
                listaFabbisogni = tsBiz.GetFabbisogniConfermati();
                break;
            case TipoRicercaEnum.Codice:
                listaFabbisogni = tsBiz.GetFabbisogniConfermati(ricerca.Codice.Value);
                break;
            case TipoRicercaEnum.RagioneSociale:
                listaFabbisogni = tsBiz.GetFabbisogniConfermati(ricerca.RagioneSociale);
                break;
        }

        Presenter.CaricaElementiInGridView(
            GridViewElencoImprese,
            listaFabbisogni,
            pagina);
    }

    protected void GridViewElencoImprese_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem != null)
        {
            FabbisognoComplessivo fab = (FabbisognoComplessivo) e.Row.DataItem;

            GridView gv = (GridView) e.Row.FindControl("GridViewFabbisogni");
            gv.DataSource = fab.ListaFabbisogni;
            gv.DataBind();
        }
    }

    private void ModificaFabbisogno(int indiceEdit, bool filtra, Int32 pagina)
    {
        FabbisognoComplessivoList listaFabbisogni = null;

        // Modifica del fabbisogno selezionato
        Int32 idImpresa = (Int32) GridViewElencoImprese.DataKeys[indiceEdit].Values["IdImpresa"];
        Int32 idFabbisognoComplessivo =
            (Int32) GridViewElencoImprese.DataKeys[indiceEdit].Values["IdFabbisognoComplessivo"];
        String ragioneSocialeImpresa =
            (String) GridViewElencoImprese.DataKeys[indiceEdit].Values["RagioneSocialeImpresa"];

        Indirizzo ind = tsBiz.GetIndirizzoSpedizione(idImpresa, idFabbisognoComplessivo);
        if (ind != null)
        {
            CaricaIndirizzo(ind);
        }

        LabelIdImpresa.Text = idImpresa.ToString();
        LabelIdFabbisognoComplessivo.Text = idFabbisognoComplessivo.ToString();

        // Prendere tutti gli elementi del fabbisogno
        FabbisognoDettaglioList listaDettaglio = tsBiz.GetFabbisognoDettaglio(idImpresa, idFabbisognoComplessivo);

        if (filtra)
            listaDettaglio = Filtra(listaDettaglio);

        VisualizzaDettaglio(true, ragioneSocialeImpresa);

        Presenter.CaricaElementiInGridView(
            GridViewFabbisognoImpresa,
            listaDettaglio,
            pagina);
    }

    private void CaricaIndirizzo(Indirizzo ind)
    {
        TextBoxIndirizzo.Text = ind.IndirizzoVia;
        TextBoxPresso.Text = ind.Presso;
        TextBoxProvincia.Text = ind.Provincia;
        TextBoxComune.Text = ind.Comune;
        TextBoxCap.Text = ind.Cap;
        TextBoxTelefono.Text = ind.Telefono;
        TextBoxCellulare.Text = ind.Cellulare;
    }

    private FabbisognoDettaglioList Filtra(FabbisognoDettaglioList dettagli)
    {
        FabbisognoDettaglioList res = new FabbisognoDettaglioList();
        string filtro = string.Empty;
        int filtroId = -1;

        if (!string.IsNullOrEmpty(TextBoxCognomeNome.Text))
        {
            filtro = TextBoxCognomeNome.Text;
        }
        if (!string.IsNullOrEmpty(TextBoxIdLavoratore.Text))
        {
            filtroId = Int32.Parse(TextBoxIdLavoratore.Text);
        }

        for (int i = 0; i < dettagli.Count; i++)
        {
            if ((filtro == string.Empty || dettagli[i].NomeCompleto.ToUpper().Contains(filtro.ToUpper())) &&
                (filtroId == -1 || dettagli[i].IdLavoratore == filtroId))
                res.Add(dettagli[i]);
        }

        return res;
    }

    protected TipoRicerca CreaFiltro()
    {
        TipoRicerca ricerca = null;

        if (RadioButtonTutti.Checked)
        {
            ricerca = new TipoRicerca(TipoRicercaEnum.Tutti, null, null);
        }
        else
        {
            int codice;
            if ((RadioButtonCodice.Checked) && (int.TryParse(TextBoxCodice.Text, out codice)))
            {
                if (int.TryParse(TextBoxCodice.Text, out codice))
                    ricerca = new TipoRicerca(TipoRicercaEnum.Codice, codice, null);
            }
            else if ((RadioButtonRagioneSociale.Checked) && (TextBoxRagioneSociale.Text != string.Empty))
            {
                ricerca = new TipoRicerca(TipoRicercaEnum.RagioneSociale, null, TextBoxRagioneSociale.Text);
            }
        }

        return ricerca;
    }

    protected void ButtonFiltro_Click(object sender, EventArgs e)
    {
        Ricerca(0);

        GridViewFabbisognoImpresa.DataSource = null;
        GridViewFabbisognoImpresa.DataBind();
        VisualizzaDettaglio(false, string.Empty);
    }

    protected void GridViewFabbisognoImpresa_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            FabbisognoDettaglio fab = (FabbisognoDettaglio) e.Row.DataItem;
            DropDownList dropTaglie = (DropDownList) e.Row.FindControl("DropDownListTaglie");

            if (fab.IdLavoratore != idLavoratore)
            {
                e.Row.Cells[0].Text = fab.IdLavoratore.ToString();
                e.Row.Cells[1].Text = fab.NomeCompleto;
                idLavoratore = fab.IdLavoratore;
            }

            Presenter.CaricaElementiInDropDown(
                dropTaglie,
                fab.ListaTaglie,
                "DescrizioneTaglia",
                "DescrizioneTaglia");

            dropTaglie.SelectedValue = fab.Taglia;
        }
    }

    protected void GridViewElencoImprese_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Ricerca(e.NewPageIndex);

        GridViewFabbisognoImpresa.DataSource = null;
        GridViewFabbisognoImpresa.DataBind();
        VisualizzaDettaglio(false, string.Empty);
    }

    private void VisualizzaDettaglio(bool visualizza, string impresa)
    {
        GridViewFabbisognoImpresa.Visible = visualizza;
        LabelTitolo.Visible = visualizza;
        LabelRagioneSocialeImpresa.Visible = visualizza;
        LabelCognomeNome.Visible = visualizza;
        TextBoxCognomeNome.Visible = visualizza;
        ButtonFiltro2.Visible = visualizza;
        LabelRagioneSocialeImpresa.Text = impresa;
        LabelIdLavoratore.Visible = visualizza;
        TextBoxIdLavoratore.Visible = visualizza;
        PanelIndirizzo.Visible = visualizza;

        if (!visualizza)
        {
            ViewState["IndiceDettaglio"] = null;
        }
    }

    protected void GridViewFabbisognoImpresa_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["IndiceDettaglio"] != null)
        {
            ModificaFabbisogno((int) ViewState["IndiceDettaglio"], true, e.NewPageIndex);
        }
    }

    protected void ButtonFiltro2_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (ViewState["IndiceDettaglio"] != null)
            {
                ModificaFabbisogno((int) ViewState["IndiceDettaglio"], true, 0);
            }
        }
    }

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idComune = -1;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    protected void DropDownListPreIndirizzo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idProvincia = -1;

        if ((DropDownListProvincia.SelectedValue != null) &&
            (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
            CaricaComuni(idProvincia);

        int idComune = -1;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

    protected void ButtonSalvaIndirizzo_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Indirizzo indirizzoNuovo = null;
            int idImpresa = -1;
            int idFabbisognoComplessivo = -1;

            if (Int32.TryParse(LabelIdImpresa.Text, out idImpresa) &&
                Int32.TryParse(LabelIdFabbisognoComplessivo.Text, out idFabbisognoComplessivo))
            {
                indirizzoNuovo =
                    new Indirizzo(Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text), 
                        Presenter.NormalizzaCampoTesto(TextBoxProvincia.Text), 
                        Presenter.NormalizzaCampoTesto(TextBoxComune.Text), 
                        TextBoxCap.Text, 
                        false,
                        Presenter.NormalizzaCampoTesto(TextBoxPresso.Text));
                indirizzoNuovo.Telefono = Presenter.NormalizzaCampoTesto(TextBoxTelefono.Text);
                indirizzoNuovo.Cellulare = Presenter.NormalizzaCampoTesto(TextBoxCellulare.Text);

                if (tsBiz.AggiornaIndirizzoSpedizione(idImpresa, idFabbisognoComplessivo, indirizzoNuovo))
                    LabelRisultato.Text = "Indirizzo aggiornato correttamente";
                else
                    LabelRisultato.Text = "Errore nell'aggiornamento dell'indirizzo";
            }
        }
    }

    protected void GridViewFabbisognoImpresa_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Salva")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            int idFabbDett = Int32.Parse(((GridView) sender).DataKeys[index].Value.ToString());
            string nuovaTaglia =
                ((DropDownList) ((GridView) sender).Rows[index].FindControl("DropDownListTaglie")).SelectedValue;

            tsBiz.AggiornaDettagliFabbisognoDettagli(idFabbDett, nuovaTaglia);
        }
    }

    protected void GridViewElencoImprese_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Modifica")
        {
            int index = Convert.ToInt32(e.CommandArgument);

            ModificaFabbisogno(index, false, 0);
            ViewState["IndiceDettaglio"] = index;
        }
    }

    #region Caricamento comuni, province, cap

    private void CaricaProvince()
    {
        DataTable dtProvince = _commonBiz.GetProvince();

        DropDownListProvincia.DataSource = dtProvince;
        DropDownListProvincia.DataTextField = "sigla";
        DropDownListProvincia.DataValueField = "idProvincia";

        DropDownListProvincia.DataBind();
    }

    private void CaricaComuni(int idProvincia)
    {
        DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

        DropDownListComuni.DataSource = dtComuni;
        DropDownListComuni.DataTextField = "denominazione";
        DropDownListComuni.DataValueField = "idComune";

        DropDownListComuni.DataBind();
    }

    private void CaricaCAP(Int64 idComune)
    {
        DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

        DropDownListCAP.DataSource = dtCAP;
        DropDownListCAP.DataTextField = "cap";
        DropDownListCAP.DataValueField = "cap";

        DropDownListCAP.DataBind();
    }

    #endregion
}