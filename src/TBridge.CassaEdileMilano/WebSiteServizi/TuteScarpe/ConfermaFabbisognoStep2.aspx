<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ConfermaFabbisognoStep2.aspx.cs" Inherits="ConfermaFabbisognoStep2" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>
<%@ Register Src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" TagName="TuteScarpeConsulenteImpresaSelezionata"
    TagPrefix="uc6" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe"
        sottoTitolo="Immissione indirizzo di spedizione per conferma ordine" />
    <br />
    <uc6:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1"
        runat="server" />
    <br />
    L'ordine � stato completato correttamente. Per la conferma � necessario specificare
    l'indirizzo a cui dovr� essere spedito il fabbisogno.<br />
    <br />
    Impostare l'indirizzo a cui deve essere recapitato l'ordine:<br />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <b>Tipo indirizzo</b>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" Visible="False">
                    <asp:ListItem Selected="True" Value="1">Sede Amministrativa</asp:ListItem>
                    <asp:ListItem Value="-1">Altro</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <b>Indirizzo</b>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="RadioButtonSedeAmmi" runat="server" GroupName="TipoIndirizzo"
                    Checked="True" OnCheckedChanged="RadioButton1_CheckedChanged" Text="Sede Amministrativa"
                    AutoPostBack="True" />
            </td>
            <td>
                <!-- Tabella con l'indirizzo di sede amministrativa -->
                <table class="standardTable">
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzoSpedizione" runat="server" Enabled="False" Height="44px"
                                Width="450px" ReadOnly="True"></asp:TextBox><br />
                            <asp:Label ID="LabelIndirizzoErrore" runat="server" ForeColor="Red" Text="Dati indirizzo non completi"
                                Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="RadioButtonAltro" runat="server" GroupName="TipoIndirizzo" OnCheckedChanged="RadioButton2_CheckedChanged"
                    Text="Altro" AutoPostBack="True" />
            </td>
            <td>
                <!-- Tabella con l'indirizzo ALtro -->
                <table class="standardTable">
                    <tr>
                        <td>
                            Indirizzo:
                        </td>
                        <td>
                            <!-- Tabellina per l'impostazione dell'indirizzo-->
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DropDownListPreIndirizzo" runat="server" Width="130px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="360px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        C/O (opzionale)
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxPresso" runat="server" Width="250px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Provincia
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListProvincia" runat="server" OnSelectedIndexChanged="DropDownListPreIndirizzo_SelectedIndexChanged"
                                Width="150px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListComuni" runat="server" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                                AutoPostBack="True" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CAP
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListCAP" runat="server" Width="150px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Recapiti telefonici </b>
            </td>
        </tr>
        <tr>
            <td>
                Telefono:
            </td>
            <td>
                <asp:TextBox ID="TextBoxTelefono" runat="server" Width="150px" MaxLength="15">
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorTelefono" runat="server"
                    ControlToValidate="TextBoxTelefono" ErrorMessage="Telefono non corretto" ValidationExpression="^\+?[\d]{3,14}$"
                    ValidationGroup="confermaFabbisogno">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Cellulare*:
            </td>
            <td>
                <asp:TextBox ID="TextBoxCellulare" runat="server" Width="150px" MaxLength="15">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCellulare" runat="server" ControlToValidate="TextBoxCellulare"
                    ErrorMessage="Cellulare obbligatorio" ValidationGroup="confermaFabbisogno">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorCellulare" runat="server"
                    ControlToValidate="TextBoxCellulare" ErrorMessage="Cellulare non corretto" ValidationExpression="^((00|\+)39[\. ]??)??3\d{2}[\. ]??\d{6,7}$"
                    ValidationGroup="confermaFabbisogno">*</asp:RegularExpressionValidator>
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelIndirizzoVuoto" runat="server" ForeColor="Red" Text="Scrivere un indirizzo"
        Visible="False"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummaryConferma" runat="server" ValidationGroup="confermaFabbisogno"
        CssClass="messaggiErrore" />
    <br />
    <b>Attenzione: una volta confermato l'ordine non potr� pi� essere modificato.<br />
    </b>
    <br />
    <asp:Button ID="ButtonSalvaOrdine" runat="server" Text="Conferma ordine" OnClick="ButtonSalvaOrdine_Click"
        ValidationGroup="confermaFabbisogno" Width="200px" />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="200px" OnClick="ButtonIndietro_Click" /><br />
</asp:Content>
