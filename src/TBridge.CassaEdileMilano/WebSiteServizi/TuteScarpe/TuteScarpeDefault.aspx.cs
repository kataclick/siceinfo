using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class TuteScarpeDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.
                                                                 TuteScarpeGestioneFabbisogniConfermati,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneOrdini,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneConsulenti,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneSms,
                                                             FunzionalitaPredefinite.TuteScarpeSelezioneTaglie,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneScadenze
                                                         };

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        #endregion
    }
}