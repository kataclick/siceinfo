﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Entities;

public partial class TuteScarpe_TuteScarpeVisualizzaRichiesteSms : Page
{
    private readonly TSBusiness _biz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneSms);

        List<SmsRichiestaTs> elencoSms = GetElencoSms();
        if (elencoSms.Count > 0)
            ButtonEsporta.Enabled = true;
        GridViewElencoSms.DataSource = elencoSms;
        GridViewElencoSms.DataBind();
    }

    private List<SmsRichiestaTs> GetElencoSms()
    {
        return _biz.GetElencoSmsRichiesteTs();
    }

    private static string PreparaStampa(List<SmsRichiestaTs> elencoSms)
    {
        GridView gv = new GridView {ID = "gvSmsTs", AutoGenerateColumns = false};

        BoundField bc1 = new BoundField();
        bc1.HeaderText = "Data Ricezione";
        bc1.DataField = "DataRicezione";
        bc1.DataFormatString = "{0:dd/MM/yyyy}";
        bc1.HtmlEncode = false;
        BoundField bc2 = new BoundField();
        bc2.HeaderText = "Cod. Lavoratore";
        bc2.DataField = "IdLavoratore";
        BoundField bc3 = new BoundField();
        bc3.HeaderText = "Numero Telefono";
        bc3.DataField = "NumeroTelefono";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Testo";
        bc4.DataField = "TestoTaglia";
        BoundField bc5 = new BoundField();
        bc5.HeaderText = "Cod. Impresa";
        bc5.DataField = "IdImpresa";

        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc4);

        gv.DataSource = elencoSms;
        gv.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw.ToString();
    }

    protected void ButtonEsporta_Click(object sender, EventArgs e)
    {
        List<SmsRichiestaTs> elencoSms = GetElencoSms();
        string stampa = PreparaStampa(elencoSms);

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=SmsTs.xls");
        Response.ContentType = "application/vnd.ms-excel";
        Response.Write(stampa);
        Response.End();
    }
}