using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class ConfermaFabbisognoStep3 : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);
        #endregion

        bool salvataggioOk;
        Boolean.TryParse(Request.QueryString["ok"], out salvataggioOk);

        if (salvataggioOk)
        {
            LabelRisultato.Text = "Il salvataggio dell'ordine � avvenuto correttamente.";
            PanelRecapiti.Visible = true;
        }
        else
        {
            LabelRisultato.Text = "Il salvataggio dell'ordine non � andato a buon fine. Si prega di riprovare.";
        }
    }
}