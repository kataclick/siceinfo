using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Microsoft.Reporting.WebForms;

public partial class TuteScarpeStatisticheFabbisogniProposti : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeStatistiche,
                                              "TuteScarpeStatisticheFabbisogniProposti.aspx");
    }

    protected void ReportViewerFabbisogniNonConfermati_Init(object sender, EventArgs e)
    {
        ReportViewerFabbisogniProposti.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        //ReportViewerFabbisogniProposti.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportFabbisogniProposti";
    }

    protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
    {
        ReportViewerFabbisogniProposti.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportFabbisogniProposti";

        List<ReportParameter> listaParam = new List<ReportParameter>();
        if (RadDatePickerDataDa.SelectedDate != null)
        {
            ReportParameter param1 = new ReportParameter("dal",
                                         RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());
            listaParam.Add(param1);
        }
        if (RadDatePickerDataA.SelectedDate != null)
        {
            ReportParameter param2 = new ReportParameter("al", RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
            listaParam.Add(param2);
        }

        ReportViewerFabbisogniProposti.ServerReport.SetParameters(listaParam.ToArray());
    }
}