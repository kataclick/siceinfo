﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.EmailClient;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Type.Collections;
using TBridge.Cemi.TuteScarpe.Type.Entities;

public partial class TuteScarpe_TuteScarpeVisualizzazioneOrdiniAutomatici : Page
{
    private readonly TSBusiness biz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini);
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        ImpresaCollection imprese = biz.GetImpreseConOrdiniAutomatici();
        DateTime? dataScadenza = biz.UltimoFabbisognoValido();
        dataScadenza = new DateTime(2010,07,9); 
        if (dataScadenza.HasValue)
        {
            List<EmailMessageSerializzabile> emailList = new List<EmailMessageSerializzabile>();
            foreach (Impresa impresa in imprese)
            {
                EmailMessageSerializzabile email = InviaEmail(impresa, biz.GetOrdiniAutomatici(impresa.IdImpresa, null), dataScadenza);
                if (email!= null)
                    emailList.Add(email);
            }

            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            var service = new EmailInfoService();
            var credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;
            service.InviaEmailCollection(emailList.ToArray());
        }
    }

    private EmailMessageSerializzabile InviaEmail(Impresa impresa, OrdineAutomaticoCollection ordini, DateTime? dataScadenza)
    {
        EmailMessageSerializzabile email = null;

        // Per fare delle prove
        String destinatario = String.Empty;   
        String destinatariocc = String.Empty; 

        if (!String.IsNullOrEmpty(impresa.EmailImpresa))
        {
            destinatario = impresa.EmailImpresa;
            destinatariocc = impresa.EmailConsulente;
        }
        else
        {
            destinatario = impresa.EmailConsulente;
        }        

        if (!string.IsNullOrEmpty(destinatario))
        {
            email = new EmailMessageSerializzabile();

            var sbPlain = new StringBuilder();
            var sbHtml = new StringBuilder();

            #region Creiamo il plaintext della mail
            sbPlain.Append(String.Format("Spettabile impresa {0}({1}),", impresa.RagioneSociale,impresa.IdImpresa.ToString()));
            sbPlain.Append(Environment.NewLine + Environment.NewLine);
            sbPlain.Append(
                String.Format(
                    "comunichiamo che in data {0} provvederemo alla compilazione automatica della fornitura descritta in allegato, così come da ultimo fabbisogno da voi richiesto.",
                    dataScadenza.Value.AddDays(-7).ToShortDateString()));
            sbPlain.Append(Environment.NewLine + Environment.NewLine);

            sbPlain.Append(String.Format("La fornitura verrà recapitata all’indirizzo: {0}. ",
                                         impresa.Recapito.IndirizzoCompleto));
            sbPlain.Append(Environment.NewLine + Environment.NewLine);
            sbPlain.Append(
                "Qualora i dati indicati non fossero più aggiornati e, quindi, validi (taglie articoli o tipologia fornitura sbagliate, operai non più in forza, indirizzo non\n esatto), vi chiediamo di provvedere prima della data sopra evidenziata alla\n compilazione e alla conferma del fabbisogno tramite la procedura tradizionale (collegamento al sito internet <a href=\"http://www.cassaedilemilano.it/\">www.cassaedilemilano.it</a> area Servizi on-line, funzione ad accesso privato “Tute e Scarpe”, Gestione ultimo fabbisogno) e non rispondendo a questa email.");
            sbPlain.Append(Environment.NewLine + Environment.NewLine);
            sbPlain.Append("Cordiali saluti.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.Append("Ufficio Servizi ai lavoratori");

            #endregion
            email.BodyPlain = sbPlain.ToString();

            #region Creiamo l'htlm text della mail
            sbHtml.Append(String.Format("Spettabile impresa {0}({1}),", impresa.RagioneSociale, impresa.IdImpresa.ToString()));
            sbHtml.Append("<br />");
            sbHtml.Append(
                String.Format(
                    "comunichiamo che in data <b>{0}</b> provvederemo alla compilazione automatica della fornitura descritta in allegato, così come da ultimo fabbisogno da voi richiesto.",
                    dataScadenza.Value.AddDays(-7).ToShortDateString()));

            sbHtml.Append(String.Format("La fornitura verrà recapitata all’indirizzo: {0}.",
                                        impresa.Recapito.IndirizzoCompleto));
            sbHtml.Append("<br /><br />");
            sbHtml.Append(
                "Qualora i dati indicati non fossero più aggiornati e, quindi, validi (taglie articoli o tipologia fornitura sbagliate, operai non più in forza, indirizzo non esatto), vi chiediamo di provvedere prima della data sopra evidenziata alla compilazione e alla conferma del fabbisogno tramite la procedura tradizionale (collegamento al sito internet <a href=\"http://www.cassaedilemilano.it/\">www.cassaedilemilano.it</a> area Servizi on-line, funzione ad accesso privato “Tute e Scarpe”, Gestione ultimo fabbisogno) e non rispondendo a questa email.");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("Cordiali saluti.");
            sbHtml.Append("<br />");
            sbHtml.Append("Ufficio Servizi ai lavoratori");


            #endregion
            email.BodyHTML = sbHtml.ToString();

            email.Allegati = new List<EmailAttachment>();
            email.Allegati.Add(CreateAttachment(ordini));

            email.Destinatari = new List<EmailAddress>();
            email.Destinatari.Add(new EmailAddress());
            email.Destinatari[0].Indirizzo = destinatario;

            if (!String.IsNullOrEmpty(destinatariocc))
            {
                email.DestinatariCC = new List<EmailAddress>();
                email.DestinatariCC.Add(new EmailAddress());
                email.DestinatariCC[0].Indirizzo = destinatariocc;
            }
            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = "TuteScarpe@cassaedilemilano.it";
            email.Mittente.Nome = "Ufficio Servizi ai lavoratori";

            email.Oggetto = "Spedizione automatica fornitura indumenti e calzature da lavoro ";

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.High;
        }

        return email;
    }

    private EmailAttachment CreateAttachment(OrdineAutomaticoCollection ordini)
    {
        var gv = new GridView();
        gv.ID = "GridViewOrdini";
        gv.AutoGenerateColumns = false;

        var bc0 = new BoundField();
        bc0.HeaderText = "Cognome";
        bc0.DataField = "Cognome";
        var bc1 = new BoundField();
        bc1.HeaderText = "Nome";
        bc1.DataField = "Nome";
        var bc2 = new BoundField();
        bc2.HeaderText = "Codice Lavoratore";
        bc2.DataField = "IdLavoratore";
        var bc3 = new BoundField();
        bc3.HeaderText = "Tipologia Fornitura";
        bc3.DataField = "TipologiaFornitura";
        var bc4 = new BoundField();
        bc4.HeaderText = "Articolo";
        bc4.DataField = "Descrizione";
        var bc5 = new BoundField();
        bc5.HeaderText = "Taglia";
        bc5.DataField = "Taglia";

        gv.Columns.Add(bc0);
        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);

        gv.DataSource = ordini;
        gv.DataBind();

        var sw = new StringWriter();
        var htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);

        var encoding = new ASCIIEncoding();
        Byte[] bytes = encoding.GetBytes(sw.ToString());

        var attachment = new EmailAttachment("OrdineTuteScarpe.xls", bytes);

        return attachment;
    }
}