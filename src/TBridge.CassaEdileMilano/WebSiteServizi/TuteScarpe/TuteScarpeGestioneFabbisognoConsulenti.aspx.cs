using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Entities;

public partial class TuteScarpe_TuteScarpeGestioneFabbisognoConsulenti : Page
{
    private readonly TSBusiness biz = new TSBusiness();

    private DateTime? scadenzaProposta;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno,
                                              "TuteScarpeGestioneFabbisognoConsulenti.aspx");

        if (GestioneUtentiBiz.IsConsulente())
        {
            if (!Page.IsPostBack)
            {
                scadenzaProposta = biz.UltimoFabbisognoValido();
                if (scadenzaProposta.HasValue && scadenzaProposta < DateTime.Now)
                {
                    LabelMessaggio.Text =
                        "Il sistema � in fase di aggiornamento. La prossima proposta di fabbisogno verr� pubblicata in seguito.";

                    TuteScarpeConsulenteImpresaSelezionaImpresa1.Visible = false;
                }
                else if (!scadenzaProposta.HasValue || scadenzaProposta > DateTime.Now)
                {
                    if (scadenzaProposta.HasValue)
                    {
                        LabelMessaggio.Text =
                            String.Format(
                                "L�attuale proposta di fabbisogno scadr� il {0} e verr� riproposta aggiornata in seguito.",
                                scadenzaProposta.Value.ToShortDateString());
                    }

                    TuteScarpeConsulenteImpresaSelezionaImpresa1.Visible = true;
                }
            }
        }
    }
}