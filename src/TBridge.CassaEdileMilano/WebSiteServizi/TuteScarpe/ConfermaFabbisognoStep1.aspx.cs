using System;
using System.Data;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using CommonEntities = TBridge.Cemi.Type.Entities;

public partial class ConfermaFabbisognoStep1 : Page
{
    private int idUtente;
    private int idImpresa;
    private TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno,
                                              "ConfermaFabbisognoStep1.aspx");
        #endregion

        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaEnt =
                (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            idImpresa = impresaEnt.IdImpresa;
            idUtente = impresaEnt.IdUtente;
        }
        else if (GestioneUtentiBiz.IsConsulente())
        {
            Consulente consulente = (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            CommonEntities.Impresa impresa = tsBiz.GetImpresaSelezionataConsulente(consulente.IdConsulente);
            idImpresa = impresa.IdImpresa;
            idUtente = impresa.IdUtente;
        }
        else if (GestioneUtentiBiz.IsFornitore())
        {
            Fornitore fornitore =
                (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            //prendiamo da db l'impresa selezionata dal fornitore
            CommonEntities.Impresa impresa = tsBiz.GetImpresaSelezionataFornitore(fornitore.IdFornitore);
            idImpresa = impresa.IdImpresa;
            idUtente = impresa.IdUtente;
        }

        DataTable lavIncompleti = tsBiz.GetLavoratoriIncompleti(idImpresa);

        if (lavIncompleti != null)
        {
            if (lavIncompleti.Rows.Count > 0)
            {
                //Non si pu� salvare perch� alcuni lav non sono definiti
                Presenter.CaricaElementiInGridView(
                    RadGridLavoratoriIncompleti,
                    lavIncompleti);
            }
            else
            {
                Response.Redirect("~/TuteScarpe/ConfermaFabbisognoStep2.aspx");
            }
        }
    }


    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/TuteScarpe/GestioneFabbisogno.aspx");
    }
}