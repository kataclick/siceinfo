using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.TuteScarpe.Type.Entities;
using TBridge.Cemi.TuteScarpe.Type.Collections;
using TBridge.Cemi.Business.EmailClient;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using TBridge.Cemi.TuteScarpe.Data.Collections;

public partial class TuteScarpeScadenzaFabbisogni : Page
{
    private readonly TSBusiness tsBiz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione Utenti
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneScadenze);
        #endregion

        #region Per prevenire click multipli

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInviaMail, null));
        sb.Append(";");
        sb.Append("return true;");
        ButtonInviaMail.Attributes.Add("onclick", sb.ToString());

        #endregion

        
        #region Per prevenire click multipli

        System.Text.StringBuilder sbSms = new System.Text.StringBuilder();
        sbSms.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbSms.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbSms.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbSms.Append("this.value = 'Attendere...';");
        sbSms.Append("this.disabled = true;");
        sbSms.Append(Page.ClientScript.GetPostBackEventReference(ButtonInviaSms, null));
        sbSms.Append(";");
        sbSms.Append("return true;");
        ButtonInviaSms.Attributes.Add("onclick", sbSms.ToString());

        #endregion
        

        if (!Page.IsPostBack)
        {
            CaricaScadenze();
        }
    }

    private void CaricaScadenze()
    {
        FabbisognoComplessivoReale fabb = tsBiz.GetUltimoFabbisognoComplessivo();
        ButtonInviaMail.Enabled = false;
        ButtonInviaSms.Enabled = false;
        
        if (fabb != null)
        {
            Panel.Visible = true;
            LabelNoFabbisogno.Visible = false;

            TextBoxCodice.Text = fabb.CodiceFabbisognoComplessivo.ToString();
            TextBoxDataGenerazione.Text = fabb.DataGenerazione.ToShortDateString();

            if (fabb.DataScadenza.HasValue)
            {
                CheckBoxDataScadenza.Checked = true;
                TextBoxDataScadenza.Enabled = true;
                TextBoxDataScadenza.Text = fabb.DataScadenza.Value.ToShortDateString();

                CheckBoxDataConfermaAutomatica.Enabled = true;
                if (fabb.DataConfermaAutomatica.HasValue)
                {
                    CheckBoxDataConfermaAutomatica.Checked = true;
                    TextBoxDataConfermaAutomatica.Enabled = true;
                    TextBoxDataConfermaAutomatica.Text = fabb.DataConfermaAutomatica.Value.ToShortDateString();

                    if (fabb.DataConfermaAutomaticaEffettuata.HasValue)
                    {
                        CheckBoxDataScadenza.Enabled = false;
                        CheckBoxDataConfermaAutomatica.Enabled = false;
                        TextBoxDataConfermaAutomatica.Enabled = false;

                        LabelDataConfermaAutomaticaEffettuata.Text = String.Format("<b>Conferma automatica</b> effettuata il <b>{0}</b> alle <b>{1}</b>", fabb.DataConfermaAutomaticaEffettuata.Value.ToShortDateString(), fabb.DataConfermaAutomaticaEffettuata.Value.ToShortTimeString());
                    }
                    else
                    {
                        if (!fabb.DataInvioMailEffettuato.HasValue)
                        {
                            ButtonInviaMail.Enabled = true;
                        }
                        if (!fabb.DataInvioSmsEffettuato.HasValue)
                        {
                            ButtonInviaSms.Enabled = true;
                        }
                    }
                }

                if (fabb.DataInvioMailEffettuato.HasValue)
                {
                    LabelMailInviata.Text = String.Format("<b>Mail</b> inviata alle imprese il <b>{0}</b> alle <b>{1}</b>", fabb.DataInvioMailEffettuato.Value.ToShortDateString(), fabb.DataInvioMailEffettuato.Value.ToShortTimeString());
                }

                if (fabb.DataInvioSmsEffettuato.HasValue)
                {
                    LabelSmsInviati.Text = String.Format("<b>Sms</b> inviati ai lavoratori il <b>{0}</b> alle <b>{1}</b>", fabb.DataInvioSmsEffettuato.Value.ToShortDateString(), fabb.DataInvioSmsEffettuato.Value.ToShortTimeString());
                }
            }
            else
            {
                CheckBoxDataScadenza.Checked = false;
                TextBoxDataScadenza.Enabled = false;
            }
        }
        else
        {
            Panel.Visible = false;
            LabelNoFabbisogno.Visible = true;
        }
    }

    protected void CheckBoxDataScadenza_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxDataScadenza.Checked)
        {
            TextBoxDataScadenza.Enabled = true;
            TextBoxDataScadenza.Text = DateTime.Now.ToShortDateString();

            CheckBoxDataConfermaAutomatica.Enabled = true;
        }
        else
        {
            TextBoxDataScadenza.Enabled = false;
            TextBoxDataScadenza.Text = string.Empty;

            CheckBoxDataConfermaAutomatica.Checked = false;
            CheckBoxDataConfermaAutomatica.Enabled = false;
            Presenter.SvuotaCampo(TextBoxDataConfermaAutomatica);
        }
    }

    protected void CheckBoxDataConfermaAutomatica_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxDataConfermaAutomatica.Checked)
        {
            TextBoxDataConfermaAutomatica.Enabled = true;
        }
        else
        {
            Presenter.SvuotaCampo(TextBoxDataConfermaAutomatica);
            TextBoxDataConfermaAutomatica.Enabled = false;
        }
    }

    protected void ButtonSalva_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            bool res = false;

            try
            {
                int idFabb;

                if (Int32.TryParse(TextBoxCodice.Text, out idFabb))
                {
                    DateTime dataGenerazione = DateTime.Parse(TextBoxDataGenerazione.Text);
                    DateTime? dataScadenza = null;
                    DateTime dataS;
                    DateTime? dataConfermaAutomatica = null;
                    DateTime dataC;


                    if ((CheckBoxDataScadenza.Checked && DateTime.TryParse(TextBoxDataScadenza.Text, out dataS)))
                    {
                        dataScadenza = dataS;
                    }

                    if ((CheckBoxDataConfermaAutomatica.Checked && DateTime.TryParse(TextBoxDataConfermaAutomatica.Text, out dataC)))
                    {
                        dataConfermaAutomatica = dataC;
                    }

                    res = tsBiz.AggiornaScadenzaFabbisogni(idFabb, dataScadenza, dataConfermaAutomatica);
                    CaricaScadenze();
                }
            }
            finally
            {
                if (res)
                {
                    LabelRisultato.Text = "Scadenze modificate";
                    CaricaScadenze();
                }
                else
                {
                    LabelRisultato.Text = "Errore durante l'aggiornamento";
                }
            }
        }
    }

    #region Custom Validators
    protected void CustomValidatorDataScadenzaSuccessivaGenerazione_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        args.IsValid = true;

        DateTime dataGenerazione = DateTime.Parse(TextBoxDataGenerazione.Text);
        DateTime? dataScadenza = null;
        DateTime data;

        if ((CheckBoxDataScadenza.Checked && DateTime.TryParse(TextBoxDataScadenza.Text, out data)))
        {
            if (dataGenerazione >= dataScadenza)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorDataScadenzaSuccessivaGenerazionePrecedenteScadenza_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
    {
        args.IsValid = true;

        DateTime dataGenerazione = DateTime.Parse(TextBoxDataGenerazione.Text);
        DateTime dataScadenza = DateTime.Parse(TextBoxDataScadenza.Text);
        DateTime dataConfermaAutomatica;

        if ((CheckBoxDataConfermaAutomatica.Checked && DateTime.TryParse(TextBoxDataConfermaAutomatica.Text, out dataConfermaAutomatica)))
        {
            if (dataConfermaAutomatica < dataGenerazione || dataConfermaAutomatica > dataScadenza)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorDataConfermaMaggioreOggi_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!String.IsNullOrEmpty(TextBoxDataConfermaAutomatica.Text))
        {
            DateTime dataD;

            if (DateTime.TryParse(TextBoxDataConfermaAutomatica.Text, out dataD))
            {
                DateTime adesso = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                if (dataD < adesso)
                {
                    args.IsValid = false;
                }
            }
        }
    }
    #endregion

    protected void ButtonInviaMail_Click(object sender, EventArgs e)
    {
        ImpresaCollection imprese = tsBiz.GetImpreseConOrdiniAutomatici();
        FabbisognoComplessivoReale fabb = tsBiz.GetUltimoFabbisognoComplessivo();
        
        if (fabb.DataScadenza.HasValue && fabb.DataConfermaAutomatica.HasValue)
        {
            List<EmailMessageSerializzabile> emailList = new List<EmailMessageSerializzabile>();
            foreach (Impresa impresa in imprese)
            {
                EmailMessageSerializzabile email = InviaEmail(impresa, tsBiz.GetOrdiniAutomatici(impresa.IdImpresa, null), fabb.DataScadenza.Value, fabb.DataConfermaAutomatica.Value);
                if (email != null)
                {
                    emailList.Add(email);
                    tsBiz.MemorizzaInvioMail(impresa.IdImpresa);
                }
            }

            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            var service = new EmailInfoService();
            var credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;
            service.InviaEmailCollection(emailList.ToArray());

            tsBiz.MemorizzaDataInvioMailCompletamentoAutomatico();
        }

        CaricaScadenze();
    }

    #region Metodi per l'invio delle email alle imprese
    private EmailMessageSerializzabile InviaEmail(Impresa impresa, OrdineAutomaticoCollection ordini, DateTime dataScadenza, DateTime dataConfermaAutomatica)
    {
        EmailMessageSerializzabile email = null;

        // Per fare delle prove
        String destinatario = String.Empty;
        String destinatariocc = String.Empty;
        //String destinatario = "marco.catalano@cassaedilemilano.it";
        //String destinatariocc = "alessio.mura@itsinfinity.com";

        if (!String.IsNullOrEmpty(impresa.EmailImpresa))
        {
            destinatario = impresa.EmailImpresa;
            destinatariocc = impresa.EmailConsulente;
        }
        else
        {
            destinatario = impresa.EmailConsulente;
        }

        if (!string.IsNullOrEmpty(destinatario))
        {
            email = new EmailMessageSerializzabile();

            var sbPlain = new StringBuilder();
            var sbHtml = new StringBuilder();

            #region Creiamo il plaintext della mail
            sbPlain.Append(String.Format("Spettabile impresa {0} (Codice Impresa: {1}),", impresa.RagioneSociale, impresa.IdImpresa.ToString()));
            sbPlain.Append(Environment.NewLine + Environment.NewLine);
            sbPlain.Append(
                String.Format(
                    "comunichiamo che in data {0} provvederemo alla compilazione automatica della fornitura descritta in allegato, cos� come da ultimo fabbisogno da voi richiesto.",
                    dataConfermaAutomatica.ToShortDateString()));
            sbPlain.Append(Environment.NewLine + Environment.NewLine);

            sbPlain.Append(String.Format("La fornitura verr� recapitata all�indirizzo: {0}.\nIn caso di problemi con la consegna, il corriere prover� a contattarla al seguente numero: {1} ",
                                         impresa.Recapito.IndirizzoCompleto, impresa.Telefono));
            sbPlain.Append(Environment.NewLine + Environment.NewLine);
            sbPlain.Append(
                "Qualora i dati indicati non fossero pi� aggiornati e, quindi, validi (taglie articoli o tipologia fornitura sbagliate, operai non pi� in forza, indirizzo non\n esatto), vi chiediamo di provvedere prima della data sopra evidenziata alla\n compilazione e alla conferma del fabbisogno tramite la procedura tradizionale (collegamento al sito internet <a href=\"http://www.cassaedilemilano.it/\">www.cassaedilemilano.it</a> area Servizi on-line, funzione ad accesso privato �Tute e Scarpe�, Gestione ultimo fabbisogno).");
            sbPlain.Append(Environment.NewLine + Environment.NewLine);
			sbPlain.Append("Questa � una mail generata automaticamente. Si prega quindi di non rispondere a questo indirizzo.");
			sbPlain.Append(Environment.NewLine + Environment.NewLine);
            sbPlain.Append("Cordiali saluti.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.Append("Ufficio Servizi ai lavoratori");

            #endregion
            email.BodyPlain = sbPlain.ToString();

            #region Creiamo l'htlm text della mail
            sbHtml.Append(String.Format("Spettabile impresa {0} (Codice Impresa: {1}),", impresa.RagioneSociale, impresa.IdImpresa.ToString()));
            sbHtml.Append("<br />");
            sbHtml.Append(
                String.Format(
                    "comunichiamo che in data <b>{0}</b> provvederemo alla compilazione automatica della fornitura descritta in allegato, cos� come da ultimo fabbisogno da voi richiesto.",
                    dataConfermaAutomatica.ToShortDateString()));
            sbHtml.Append("<br /><br />");
            sbHtml.Append(String.Format("La fornitura verr� recapitata all�indirizzo: {0}.<br />In caso di problemi con la consegna, il corriere prover� a contattarla al seguente numero: {1}",
                                        impresa.Recapito.IndirizzoCompleto, impresa.Telefono));
            sbHtml.Append("<br /><br />");
            sbHtml.Append(
                "Qualora i dati indicati non fossero pi� aggiornati e, quindi, validi (taglie articoli o tipologia fornitura sbagliate, operai non pi� in forza, indirizzo non esatto), vi chiediamo di provvedere prima della data sopra evidenziata alla compilazione e alla conferma del fabbisogno tramite la procedura tradizionale (collegamento al sito internet <a href=\"http://www.cassaedilemilano.it/\">www.cassaedilemilano.it</a> area Servizi on-line, funzione ad accesso privato �Tute e Scarpe�, Gestione ultimo fabbisogno).");
            sbHtml.Append("<br /><br />");
			sbHtml.Append("<b>Questa � una mail generata automaticamente. Si prega quindi di non rispondere a questo indirizzo.</b>");
            sbHtml.Append("<br /><br />");
			sbHtml.Append("Cordiali saluti.");
            sbHtml.Append("<br />");
            sbHtml.Append("Ufficio Servizi ai lavoratori");


            #endregion
            email.BodyHTML = sbHtml.ToString();

            email.Allegati = new List<EmailAttachment>();
            email.Allegati.Add(CreateAttachment(ordini));

            email.Destinatari = new List<EmailAddress>();
            email.Destinatari.Add(new EmailAddress());
            email.Destinatari[0].Indirizzo = destinatario;

            if (!String.IsNullOrEmpty(destinatariocc))
            {
                email.DestinatariCC = new List<EmailAddress>();
                email.DestinatariCC.Add(new EmailAddress());
                email.DestinatariCC[0].Indirizzo = destinatariocc;
            }
            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = "TuteScarpe@cassaedilemilano.it";
            email.Mittente.Nome = "Ufficio Servizi ai lavoratori";

            email.Oggetto = "Spedizione automatica fornitura indumenti e calzature da lavoro ";

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.High;
        }

        return email;
    }

    private EmailAttachment CreateAttachment(OrdineAutomaticoCollection ordini)
    {
        var gv = new GridView();
        gv.ID = "GridViewOrdini";
        gv.AutoGenerateColumns = false;

        var bc0 = new BoundField();
        bc0.HeaderText = "Cognome";
        bc0.DataField = "Cognome";
        var bc1 = new BoundField();
        bc1.HeaderText = "Nome";
        bc1.DataField = "Nome";
        var bc2 = new BoundField();
        bc2.HeaderText = "Codice Lavoratore";
        bc2.DataField = "IdLavoratore";
        var bc3 = new BoundField();
        bc3.HeaderText = "Tipologia Fornitura";
        bc3.DataField = "TipologiaFornitura";
        var bc4 = new BoundField();
        bc4.HeaderText = "Articolo";
        bc4.DataField = "Descrizione";
        var bc5 = new BoundField();
        bc5.HeaderText = "Taglia";
        bc5.DataField = "Taglia";

        gv.Columns.Add(bc0);
        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);

        gv.DataSource = ordini;
        gv.DataBind();

        var sw = new StringWriter();
        var htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);

        var encoding = new ASCIIEncoding();
        Byte[] bytes = encoding.GetBytes(sw.ToString());

        var attachment = new EmailAttachment("OrdineTuteScarpe.xls", bytes);

        return attachment;
    }
    #endregion

    protected void ButtonInviaSms_Click(object sender, EventArgs e)
    {
        const string testoSms = "Cassa Edile di Milano ti comunica che � disponibile la fornitura tute e scarpe. Chiedi alla tua impresa di compilare la richiesta su www.cassaedilemilano.it";
        tsBiz.InviaSmsLavoratoriNonCompletabili(testoSms);
        tsBiz.MemorizzaDataInvioSmsLavoratori();

        CaricaScadenze();
    }
}