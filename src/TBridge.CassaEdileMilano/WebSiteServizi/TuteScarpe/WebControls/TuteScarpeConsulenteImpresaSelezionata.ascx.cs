﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Type.Entities;

public partial class TuteScarpe_WebControls_TuteScarpeConsulenteImpresaSelezionata : System.Web.UI.UserControl
{
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TSBusiness tsBiz = new TSBusiness();

            if (GestioneUtentiBiz.IsImpresa())
            {
                TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresa =
                    (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                impresa = impresa;

                this.Visible = false;
            }
            else if (GestioneUtentiBiz.IsConsulente())
            {
                TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente consulente =
                    (TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                Impresa impresa = tsBiz.GetImpresaSelezionataConsulente(consulente.IdConsulente);

                LabelIdImpresa.Text = impresa.IdImpresa.ToString();
                LabelRagioneSociale.Text = impresa.RagioneSociale;
            }
            else if (GestioneUtentiBiz.IsFornitore())
            {
                TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore fornitore =
                    (TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                //prendiamo da db l'idImpresa selezionata dal fornitore
                Impresa impresa = tsBiz.GetImpresaSelezionataFornitore(fornitore.IdFornitore);

                LabelIdImpresa.Text = impresa.IdImpresa.ToString();
                LabelRagioneSociale.Text = impresa.RagioneSociale;
            }
        }
    }
}
