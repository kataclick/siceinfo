﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VisualizzaFeedbackSms.aspx.cs" Inherits="TuteScarpe_VisualizzaFeedbackSms" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Richieste pervenute via SMS"
        titolo="Tute &amp; Scarpe" />
    <br />
    <asp:GridView ID="GridViewElencoSms" runat="server" AutoGenerateColumns="False" EmptyDataText="Messaggi non presenti"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="DataRicezione" HeaderText="Data ricezione" HtmlEncode="False" />
            <asp:BoundField DataField="IdLavoratore" HeaderText="Cod. Lav." />
            <asp:BoundField DataField="NumeroTelefono" HeaderText="Cell." />
            <asp:BoundField DataField="IdImpresa" HeaderText="Cod. Imp." />
            <asp:BoundField DataField="TestoTaglia" HeaderText="Testo" />
        </Columns>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonEsporta" runat="server" Text="Esporta" Enabled="False" OnClick="ButtonEsporta_Click" />
</asp:Content>

