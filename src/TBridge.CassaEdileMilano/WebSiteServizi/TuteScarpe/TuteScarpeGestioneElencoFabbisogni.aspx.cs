using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

/// <summary>
/// Credo sia stata creata per errore...
/// </summary>
public partial class GestioneTuteScarpeElencoFabbisogni : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini,
                                              "TuteScarpeGestioneElencoFabbisogni.aspx");
    }
}