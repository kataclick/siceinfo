using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Fornitore=TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore;

public partial class TuteScarpe_TuteScarpeGestioneFabbisognoFornitore : Page
{
    private const int BOTTONEMODIFICA = 4;
    private readonly TSBusiness biz = new TSBusiness();

    private DateTime? scadenzaProposta;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno,
                                              "TuteScarpeGestioneFabbisognoFornitore.aspx");

        if (GestioneUtentiBiz.IsFornitore())
        {
            if (!Page.IsPostBack)
            {
                scadenzaProposta = biz.UltimoFabbisognoValido();
                if (scadenzaProposta.HasValue && scadenzaProposta < DateTime.Now)
                {
                    LabelMessaggio.Text =
                        "Il sistema � in fase di aggiornamento. La prossima proposta di fabbisogno verr� pubblicata in seguito.";

                    TuteScarpeFornitoreImpresaSelezionaImpresa1.Visible = false;
                }
                else if (!scadenzaProposta.HasValue || scadenzaProposta > DateTime.Now)
                {
                    if (scadenzaProposta.HasValue)
                    {
                        LabelMessaggio.Text =
                            String.Format(
                                "L�attuale proposta di fabbisogno scadr� il {0} e verr� riproposta aggiornata in seguito.",
                                scadenzaProposta.Value.ToShortDateString());
                    }

                    TuteScarpeFornitoreImpresaSelezionaImpresa1.Visible = true;
                }
            }
        }
    }

//if (GestioneUtentiBiz.IsFornitore())
//        {
//            if (!Page.IsPostBack)
//            {
//                CaricaFabbisogni();
//            }
//        }
//    }

//    private void CaricaFabbisogni()
//    {
//        int? codice = null;
//        string ragioneSociale = null;

//        scadenzaProposta = biz.UltimoFabbisognoValido();
//        if (!string.IsNullOrEmpty(TextBoxCodice.Text))
//            codice = Int32.Parse(TextBoxCodice.Text);
//        ragioneSociale = TextBoxRagioneSociale.Text;

//        //TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore fornitore =
//        //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Fornitore)HttpContext.Current.User.Identity).Entity;
//        Fornitore fornitore =
//            (Fornitore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

//        RapportoFornitoreImpresaCollection rapporti = biz.GetFornitoreRapportiImpresa(fornitore.IdFornitore, true,
//                                                                                      codice, ragioneSociale);
//        RapportoFornitoreImpresaCollection rappportiFiltrati = FiltraRapporti(rapporti);
//        GridViewImpreseFornitore.DataSource = rappportiFiltrati;
//        GridViewImpreseFornitore.DataBind();

//        if (!scadenzaProposta.HasValue)
//        {
//            LabelMessaggio.Text =
//                "Il sistema � in fase di aggiornamento. La prossima proposta di fabbisogno verr� pubblicata in seguito.";
//        }
//        else
//        {
//            if (scadenzaProposta != new DateTime(1900, 1, 1))
//            {
//                LabelMessaggio.Text =
//                    String.Format(
//                        "L�attuale proposta di fabbisogno scadr� il {0} e verr� riproposta aggiornata in seguito.",
//                        scadenzaProposta.Value.ToShortDateString());
//            }
//        }
//    }

//    private RapportoFornitoreImpresaCollection FiltraRapporti(RapportoFornitoreImpresaCollection rapporti)
//    {
//        RapportoFornitoreImpresaCollection rapportiFiltrati = new RapportoFornitoreImpresaCollection();

//        if (DropDownListStato.SelectedValue == "TUTTE")
//        {
//            rapportiFiltrati = rapporti;
//        }

//        switch (DropDownListStato.SelectedValue)
//        {
//            case "TUTTE":
//                rapportiFiltrati = rapporti;
//                break;
//            default:
//                foreach (RapportoFornitoreImpresa rapporto in rapporti)
//                {
//                    if (biz.EsisteFabbisogno(rapporto.IdImpresa))
//                    {
//                        // Purtroppo era cos�
//                        DataTable dtLavoratori = biz.ElencoLavoratoriImpresa(rapporto.IdImpresa, null);
//                        if (dtLavoratori != null && dtLavoratori.Rows.Count > 0)
//                        {
//                            if (DropDownListStato.SelectedValue == "DACOMPILARE" ||
//                                DropDownListStato.SelectedValue == "TUTTE")
//                                rapportiFiltrati.Add(rapporto);
//                        }
//                        else
//                        {
//                            if (DropDownListStato.SelectedValue == "GIACONFERMATE" ||
//                                DropDownListStato.SelectedValue == "TUTTE")
//                                rapportiFiltrati.Add(rapporto);
//                        }
//                    }
//                    else
//                    {
//                        if (DropDownListStato.SelectedValue == "NOORE" || DropDownListStato.SelectedValue == "TUTTE")
//                            rapportiFiltrati.Add(rapporto);
//                    }
//                }
//                break;
//        }

//        return rapportiFiltrati;
//    }

//    protected void GridViewImpreseFornitore_RowDataBound(object sender, GridViewRowEventArgs e)
//    {
//        if (e.Row.RowType == DataControlRowType.DataRow)
//        {
//            if (!scadenzaProposta.HasValue)
//            {
//                e.Row.Cells[BOTTONEMODIFICA].Enabled = false;
//            }
//            else
//            {
//                RapportoFornitoreImpresa rapporto = (RapportoFornitoreImpresa) e.Row.DataItem;
//                Label lStato = (Label) e.Row.FindControl("LabelStato");

//                if (biz.EsisteFabbisogno(rapporto.IdImpresa))
//                {
//                    // Purtroppo era cos�
//                    DataTable dtLavoratori = biz.ElencoLavoratoriImpresa(rapporto.IdImpresa, null);
//                    if (dtLavoratori != null && dtLavoratori.Rows.Count > 0)
//                    {
//                        //if (DropDownListStato.SelectedValue != "DACOMPILARE" && DropDownListStato.SelectedValue != "TUTTE")
//                        //    e.Row.Visible = false;
//                    }
//                    else
//                    {
//                        e.Row.Cells[BOTTONEMODIFICA].Enabled = false;
//                        lStato.Text = "L'ordine di fabbisogno del mese corrente � gi� stato confermato.";

//                        //if (DropDownListStato.SelectedValue != "GIACONFERMATE" && DropDownListStato.SelectedValue != "TUTTE")
//                        //    e.Row.Visible = false;
//                    }
//                }
//                else
//                {
//                    e.Row.Cells[BOTTONEMODIFICA].Enabled = false;
//                    lStato.Text = "Non risultano soddisfatti i requisiti di accantonamento orario previsti.";

//                    //if (DropDownListStato.SelectedValue != "NOORE" && DropDownListStato.SelectedValue != "TUTTE")
//                    //    e.Row.Visible = false;
//                }
//            }
//        }
//    }

//    protected void ButtonRicerca_Click(object sender, EventArgs e)
//    {
//        GridViewImpreseFornitore.PageIndex = 0;
//        TuteScarpeInfoSpedizioneImpresa1.Reset();
//        TuteScarpeInfoSpedizioneImpresa1.Visible = false;
//        CaricaFabbisogni();
//    }

//    protected void GridViewImpreseFornitore_RowDeleting(object sender, GridViewDeleteEventArgs e)
//    {
//        int idImpresa = (int) GridViewImpreseFornitore.DataKeys[e.RowIndex].Values["IdImpresa"];
//        int idUtente = biz.GetIdUtenteByIdImpresa(idImpresa);

//        if (idUtente != -1)
//        {
//            Session["idImpresa"] = idImpresa;
//            Session["idUtente"] = idUtente;
//            Response.Redirect("~/TuteScarpe/TuteScarpeVisualizzaStorico.aspx");
//        }
//    }

//    protected void GridViewImpreseFornitore_PageIndexChanging(object sender, GridViewPageEventArgs e)
//    {
//        GridViewImpreseFornitore.PageIndex = e.NewPageIndex;
//        CaricaFabbisogni();
//    }

//    protected void GridViewImpreseFornitore_RowCommand(object sender, GridViewCommandEventArgs e)
//    {
//        switch (e.CommandName)
//        {
//            case "dettagli":
//                int indice = int.Parse(e.CommandArgument.ToString());
//                int idImpresa = (int) GridViewImpreseFornitore.DataKeys[indice].Values["IdImpresa"];

//                string ragioneSociale =
//                    (string) GridViewImpreseFornitore.DataKeys[indice].Values["RagioneSocialeImpresa"];
//                Indirizzo sedeLegale = (Indirizzo) GridViewImpreseFornitore.DataKeys[indice].Values["SedeLegale"];
//                Indirizzo sedeAmministrativa =
//                    (Indirizzo) GridViewImpreseFornitore.DataKeys[indice].Values["SedeAmministrativa"];

//                TuteScarpeInfoSpedizioneImpresa1.Visible = true;
//                TuteScarpeInfoSpedizioneImpresa1.CaricaDatiImpresa(ragioneSociale, sedeLegale, sedeAmministrativa);
//                break;
//        }
//    }

//    protected void GridViewImpreseFornitore_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
//    {
//        int idImpresa = (int) GridViewImpreseFornitore.DataKeys[e.NewSelectedIndex].Values["IdImpresa"];
//        int idUtente = biz.GetIdUtenteByIdImpresa(idImpresa);

//        if (idUtente != -1)
//        {
//            Session["idImpresa"] = idImpresa;
//            Session["idUtente"] = idUtente;
//            Response.Redirect("~/TuteScarpe/TuteScarpeGestioneFabbisogno.aspx");
//        }
//    }

//    protected void GridViewImpreseFornitore_RowEditing(object sender, GridViewEditEventArgs e)
//    {
//        int indice = e.NewEditIndex;
//        int idImpresa = (int) GridViewImpreseFornitore.DataKeys[indice].Values["IdImpresa"];

//        string ragioneSociale = (string) GridViewImpreseFornitore.DataKeys[indice].Values["RagioneSocialeImpresa"];
//        Indirizzo sedeLegale = (Indirizzo) GridViewImpreseFornitore.DataKeys[indice].Values["SedeLegale"];
//        Indirizzo sedeAmministrativa =
//            (Indirizzo) GridViewImpreseFornitore.DataKeys[indice].Values["SedeAmministrativa"];

//        TuteScarpeInfoSpedizioneImpresa1.Visible = true;
//        TuteScarpeInfoSpedizioneImpresa1.CaricaDatiImpresa(ragioneSociale, sedeLegale, sedeAmministrativa);
//    }
}