<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="GestioneFabbisogno.aspx.cs" Inherits="GestioneFabbisogno" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<%@ Register src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" tagname="TuteScarpeConsulenteImpresaSelezionata" tagprefix="uc6" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Elenco lavoratori aventi diritto"/>
    <br />
    <uc6:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1" 
        runat="server" />
    <br />
    <asp:Label ID="LabelInfo" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Label ID="LabelScadenza" runat="server" Font-Bold="True" Text="L�attuale proposta di fabbisogno scadr� "
        Visible="False"></asp:Label><br />
    <br />
    <asp:Label ID="Label1" runat="server" Text="Seleziona lavoratori:  " Visible="False"></asp:Label>
    <asp:DropDownList ID="DropDownListFiltro" runat="server" AutoPostBack="True" Width="188px" OnSelectedIndexChanged="DropDownListFiltro_SelectedIndexChanged" Visible="False">
        <asp:ListItem Selected="True" Value="TUTTI">Tutti</asp:ListItem>
        <asp:ListItem Value="NONCOMPLETATI">Non completati</asp:ListItem>
    </asp:DropDownList><br />
    <br />
    <telerik:RadGrid
        ID="RadGridElencoLavoratori"
        runat="server" GridLines="None" 
        onitemdatabound="RadGridElencoLavoratori_ItemDataBound" 
        ondeletecommand="RadGridElencoLavoratori_DeleteCommand" 
        oneditcommand="RadGridElencoLavoratori_EditCommand" AllowPaging="True" 
        onpageindexchanged="RadGridElencoLavoratori_PageIndexChanged">
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

<MasterTableView AutoGenerateColumns="False" 
            DataKeyNames="IdLavoratore,Eliminato,NomeLavoratore,IdImpresa">
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="IdLavoratore" 
            HeaderText="Codice Lavoratore" UniqueName="CodiceLavoratore">
            <ItemStyle Width="80px" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NomeLavoratore" HeaderText="Cognome e nome" 
            UniqueName="CognomeNome">
            <ItemStyle Font-Bold="True" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DataNascita" 
            DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data di nascita" 
            UniqueName="column1">
            <ItemStyle Width="80px" />
        </telerik:GridBoundColumn>
        <telerik:GridCheckBoxColumn DataField="Completato" HeaderText="Completato" ReadOnly="True">
            <ItemStyle Width="50px" />
        </telerik:GridCheckBoxColumn>
        <telerik:GridCheckBoxColumn DataField="Eliminato" HeaderText="Eliminato" ReadOnly="True">
            <ItemStyle Width="50px" />
        </telerik:GridCheckBoxColumn>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Delete" 
            Text="Elimina/Ripristina" UniqueName="Elimina" ButtonCssClass="bottoneGrigliaTuteScarpe">
            <ItemStyle Width="10px" />
        </telerik:GridButtonColumn>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Edit" 
            Text="Completa" UniqueName="Completa" ButtonCssClass="bottoneGrigliaTuteScarpe">
            <ItemStyle Width="10px" />
        </telerik:GridButtonColumn>
    </Columns>
</MasterTableView>
    </telerik:RadGrid>
    <asp:Button ID="ButtonStampa" runat="server" Text="Stampa situazione attuale" OnClick="ButtonStampa_Click" Visible="false" />
    <asp:ImageButton ID="ImageButtonStampa" runat="server" ImageUrl="~/images/printer.png" OnClick="ButtonStampa_Click" />
    <br />
    <br />
    <asp:Label ID="LabelConferma" runat="server" Text="Prima di confermare un'ordine si consiglia di stampare la situazione in quanto l'ordine non comparir� nello storico fino alla gestione dello stesso da parte di CASSA EDILE."></asp:Label><br />
    <br />
    
    
    <asp:Button ID="ButtonConferma" runat="server" OnClick="Button1_Click" Text="Conferma Ordine" Width="227px" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
