using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Impresa=TBridge.Cemi.Cantieri.Type.Entities.Impresa;

public partial class TuteScarpe_TuteScarpeConsulenteImprese : Page
{
    private readonly TSBusiness biz = new TSBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneConsulenti,
                                              "TuteScarpeConsulenteImprese.aspx");

        TuteScarpeRicercaConsulenti1.OnConsulenteSelected += TuteScarpeRicercaConsulenti1_OnConsulenteSelected;
        CantieriRicercaImpresa1.ModalitaTuteScarpe();
        CantieriRicercaImpresa1.OnImpresaSelected += CantieriRicercaImpresa1_OnImpresaSelected;
    }

    private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
    {
        //if (InserisciRapporto(impresa.IdImpresa.Value))
        //{
        //    CantieriRicercaImpresa1.Reset();
        //    PanelAltraImpresa.Visible = false;
        //    PanelSelezioneImpresa.Visible = false;
        //    PanelAltraImpresa.Visible = false;
        //}

        int idImpresa = impresa.IdImpresa.Value;
        string ragioneSociale = impresa.RagioneSociale;

        ViewState["IdImpresa"] = idImpresa;
        TextBoxImpresaSelezionata.Text = String.Format("{0} {1}", idImpresa, ragioneSociale);

        CantieriRicercaImpresa1.Reset();
        PanelAltraImpresa.Visible = false;
        //PanelSelezioneImpresa.Visible = false;
    }

    private bool InserisciRapporto(int idImpresa, string nota)
    {
        bool res = false;

        if (ViewState["Consulente"] != null)
        {
            Consulente consulente = (Consulente) ViewState["Consulente"];

            RapportoConsulenteImpresa nuovoRapporto = new RapportoConsulenteImpresa();
            nuovoRapporto.IdConsulente = consulente.IdConsulente;
            nuovoRapporto.IdImpresa = idImpresa;
            nuovoRapporto.Nota = nota;

            res = biz.InsertConsulenteRapportoImpresa(nuovoRapporto);
            if (res)
            {
                ViewState["IdImpresa"] = null;
                TextBoxImpresaSelezionata.Text = null;
                TextBoxNota.Text = null;

                LabelErrore.Visible = false;
                CaricaImprese(consulente.IdConsulente);
            }
            else
                LabelErrore.Visible = true;
        }

        return res;
    }

    private void TuteScarpeRicercaConsulenti1_OnConsulenteSelected(Consulente consulente)
    {
        ViewState["Consulente"] = consulente;
        TextBoxConsulente.Text = String.Format("{0} {1}", consulente.IdConsulente, consulente.RagioneSociale);
        PanelSelezionaConsulente.Visible = false;
        PanelDettagli.Visible = true;

        CaricaImprese(consulente.IdConsulente);
        CaricaImpreseSuggerite(consulente.IdConsulente);
    }

    private void CaricaImpreseSuggerite(int idConsulente)
    {
        RapportoConsulenteImpresaCollection rapportiSuggeriti = biz.GetConsulenteRapportiImpresaDenunce(idConsulente);
        RapportoConsulenteImpresaCollection rapportiAttivi = biz.GetConsulenteRapportiImpresa(idConsulente, true);

        int i = 0;
        while (i < rapportiSuggeriti.Count)
        {
            RapportoConsulenteImpresa rappPresente = rapportiAttivi.RapportoPresente(rapportiSuggeriti[i]);

            if (rappPresente != null)
                rapportiSuggeriti.RemoveAt(i);
            else
                i++;
        }

        GridViewImpreseDenunce.DataSource = rapportiSuggeriti;
        GridViewImpreseDenunce.DataBind();
    }

    private void CaricaImprese(int idConsulente)
    {
        bool? attivi = null;

        if (DropDownListFiltro.SelectedItem.Value == "ATTIVI")
            attivi = true;
        if (DropDownListFiltro.SelectedItem.Value == "NONATTIVI")
            attivi = false;

        RapportoConsulenteImpresaCollection rapporti = biz.GetConsulenteRapportiImpresa(idConsulente, attivi);

        GridViewImpreseConsulente.DataSource = rapporti;
        GridViewImpreseConsulente.DataBind();
    }

    protected void ButtonSelezionaImpresa_Click(object sender, EventArgs e)
    {
        PanelSelezioneImpresa.Visible = true;
    }

    protected void ButtonSelezionaConsulente_Click(object sender, EventArgs e)
    {
        PanelSelezionaConsulente.Visible = true;
    }

    protected void ButtonAltraImpresa_Click(object sender, EventArgs e)
    {
        PanelAltraImpresa.Visible = true;
    }

    protected void GridViewImpreseDenunce_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int idImpresa = (int) GridViewImpreseDenunce.DataKeys[e.NewSelectedIndex].Values["IdImpresa"];
        string ragioneSociale =
            (string) GridViewImpreseDenunce.DataKeys[e.NewSelectedIndex].Values["RagioneSocialeImpresa"];

        ViewState["IdImpresa"] = idImpresa;
        TextBoxImpresaSelezionata.Text = String.Format("{0} {1}", idImpresa, ragioneSociale);

        //if (InserisciRapporto(idImpresa))
        //{
        //    //PanelSelezioneImpresa.Visible = false;
        //    if (ViewState["Consulente"] != null)
        //    {
        //        Consulente consulente = (Consulente)ViewState["Consulente"];
        //        CaricaImpreseSuggerite(consulente.IdConsulente);
        //    }
        //}
    }

    protected void GridViewImpreseConsulente_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["Consulente"] != null)
        {
            Consulente consulente = (Consulente) ViewState["Consulente"];

            CaricaImprese(consulente.IdConsulente);
            GridViewImpreseConsulente.PageIndex = e.NewPageIndex;
            GridViewImpreseConsulente.DataBind();
        }
    }

    protected void GridViewImpreseDenunce_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        if (ViewState["Consulente"] != null)
        {
            Consulente consulente = (Consulente) ViewState["Consulente"];

            CaricaImpreseSuggerite(consulente.IdConsulente);
            GridViewImpreseDenunce.PageIndex = e.NewPageIndex;
            GridViewImpreseDenunce.DataBind();
        }
    }

    protected void GridViewImpreseConsulente_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RapportoConsulenteImpresa rapporto = (RapportoConsulenteImpresa) e.Row.DataItem;
            TextBox tbDataFine = (TextBox) e.Row.FindControl("TextBoxDataFine");
            TextBox tbNota = (TextBox) e.Row.FindControl("TextBoxNota");

            tbNota.Text = rapporto.Nota;

            if (rapporto.DataFine.HasValue)
            {
                e.Row.Enabled = false;
                tbDataFine.Text = rapporto.DataFine.Value.ToShortDateString();
            }
        }
    }

    protected void GridViewImpreseConsulente_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        TextBox tbDataFine = (TextBox) GridViewImpreseConsulente.Rows[e.NewSelectedIndex].FindControl("TextBoxDataFine");
        Consulente consulente = (Consulente) ViewState["Consulente"];

        Page.Validate();
        //if (!string.IsNullOrEmpty(tbDataFine.Text) && Page.IsValid)
        if (Page.IsValid)
        {
            int idRapporto =
                (int) GridViewImpreseConsulente.DataKeys[e.NewSelectedIndex].Values["IdRapportoConsulenteImpresa"];
            DateTime dataInizio = (DateTime) GridViewImpreseConsulente.DataKeys[e.NewSelectedIndex].Values["DataInizio"];
            Label lErrore = (Label) GridViewImpreseConsulente.Rows[e.NewSelectedIndex].FindControl("LabelErrore");
            TextBox tbNota = (TextBox) GridViewImpreseConsulente.Rows[e.NewSelectedIndex].FindControl("TextBoxNota");

            DateTime? dataFine = null;
            if (!string.IsNullOrEmpty(tbDataFine.Text))
                dataFine = DateTime.Parse(tbDataFine.Text);
            string nota = tbNota.Text;

            DateTime? dataInizioArr = null;
            DateTime? dataFineArr = null;
            if (dataFine.HasValue)
            {
                dataInizioArr = new DateTime(dataInizio.Year, dataInizio.Month, dataInizio.Day);
                dataFineArr = new DateTime(dataFine.Value.Year, dataFine.Value.Month, dataFine.Value.Day);
            }

            if ((!dataInizioArr.HasValue && !dataFineArr.HasValue) || dataFineArr >= dataInizioArr)
            {
                if (biz.UpdateRapportoConsulenteImpresa(idRapporto, dataFine, nota))
                {
                    lErrore.Text = string.Empty;
                    lErrore.Visible = false;
                    CaricaImprese(consulente.IdConsulente);
                    CaricaImpreseSuggerite(consulente.IdConsulente);
                    //PanelSelezioneImpresa.Visible = false;
                }
                else
                {
                    lErrore.Text = "Errore nell'aggiornamento";
                    lErrore.Visible = true;
                }
            }
            else
            {
                lErrore.Text = "Data fine <= data inizio";
                lErrore.Visible = true;
            }
        }
        else
            CaricaImprese(consulente.IdConsulente);
    }

    protected void DropDownListFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        Consulente consulente = (Consulente) ViewState["Consulente"];
        CaricaImprese(consulente.IdConsulente);
    }

    protected void ButtonAggiungiRapporto_Click(object sender, EventArgs e)
    {
        if (ViewState["IdImpresa"] != null)
        {
            int idImpresa = (int) ViewState["IdImpresa"];
            //string nota = TextBoxNota.Text;

            if (InserisciRapporto(idImpresa, TextBoxNota.Text))
            {
                //PanelSelezioneImpresa.Visible = false;
                if (ViewState["Consulente"] != null)
                {
                    Consulente consulente = (Consulente) ViewState["Consulente"];
                    CaricaImpreseSuggerite(consulente.IdConsulente);
                }
            }
        }
    }
}