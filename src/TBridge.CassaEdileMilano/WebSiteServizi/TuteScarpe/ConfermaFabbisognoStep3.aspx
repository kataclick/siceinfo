<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ConfermaFabbisognoStep3.aspx.cs" Inherits="ConfermaFabbisognoStep3" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>
<%@ Register Src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" TagName="TuteScarpeConsulenteImpresaSelezionata"
    TagPrefix="uc6" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe"
        sottoTitolo="Stato salvataggio ordine" />
    <br />
    <uc6:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1"
        runat="server" />
    <br />
    <asp:Label ID="LabelRisultato" runat="server"></asp:Label>
    &nbsp;
    <br />
    <br />
    <asp:Panel ID="PanelRecapiti" runat="server" Visible="false">
        Si ricorda che per comunicare variazioni di recapito (indirizzo, n. telefono, n. fax, indirizzo e-mail) occorre utilizzare l�apposita funzione ad accesso privato <a runat="server" href="~/AnagraficaImprese/GestioneRecapiti.aspx">Variazione Recapiti Impresa</a>
    </asp:Panel>
</asp:Content>
