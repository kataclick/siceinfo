<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="TuteScarpeDefault.aspx.cs" Inherits="TuteScarpeDefault" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe"
        sottoTitolo="Benvenuto nella sezione Tute e Scarpe" />
    <br />
    <p class="DefaultPage">
        Come disposto dalla normativa vigente in materia di prestazioni assistenziali, la Cassa Edile 
		garantisce, una volta all�anno, la fornitura di indumenti da lavoro a tutti i lavoratori che 
		al momento della distribuzione si trovino alle dipendenze di impresa iscritta alla Cassa Edile 
		di Milano, Lodi, Monza e Brianza e che abbiano maturato 1.800 o 500 ore di lavoro ordinario 
		rispettivamente nei 12 o nei 3 mesi precedenti il 1� settembre. 
		<br />
        <br />
		Indipendentemente dalle ore maturate, a tutti gli operai la Cassa Edile fornisce 1 paio di 
		scarpe antinfortunistiche. 
		<br />
        <br />
		La consegna della fornitura avviene direttamente presso l�indirizzo indicato dalle imprese 
		richiedenti.
        <br />
        <br />
        La fornitura annuale gratuita per i lavoratori dipendenti da imprese edili si compone di:
        <ul>
            <li>n. 1 felpa pile</li>
            <li>n. 1 paio di jeans</li>
            <li>n. 1 paio di scarpe antinfortunistiche</li>
        </ul>
        <br />
        Per i lavoratori dipendenti da imprese asfaltiste vengono forniti: 1 giubbino ad alta visibilit�, 1 paio di pantaloni ad alta visibilit� e 1 paio di scarpe antinfortunistiche. 
		<br />
        <br />
		La fornitura per la categoria imbianchini � composta da: 1 paio di pantaloni bianchi, 1 felpa pile, 1 T-shirt bianca e 1 paio di scarpe antinfortunistiche.
		<br />
        <br /> 
		Il sistema propone in automatico per ogni impresa iscritta, l�elenco dei dipendenti che hanno 
		maturato il diritto alla prestazione; ne consegue che la mancata visualizzazione del nominativo 
		di un dipendente pu� coincidere con l'avvenuta denuncia dello stesso a posteriori rispetto alla 
		pubblicazione della proposta di fabbisogno.
		<br />
        <br />
		Analogamente la mancata visualizzazione del campo vestiario equivale al mancato soddisfacimento 
		dei requisiti di accantonamento orario previsti, ovvero del numero minimo di ore di lavoro ordinario 
		maturate e pagate.
		I lavoratori che matureranno il diritto in seguito ed i neo-assunti verranno inclusi direttamente 
		dal sistema, senza la necessit� da parte dell'impresa di farne specifica richiesta.
		<br />
        <br />
		Qualora l�impresa non dovesse completare e confermare la proposta d�ordine in tempi utili, quest�ultima 
		scadr� e verr� ripresentata dal sistema automaticamente in seguito. 
		<br />
        <br />
		Scarica il manuale utente per la compilazione guidata della richiesta di indumenti e calzature da lavoro.
		<br />
		<a href="Vestiario-Manuale3.0.pdf">Clicca Qui</a>		
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
