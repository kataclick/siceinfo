using System;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Xml;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Telerik.Web.UI;
using Fornitore=TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore;
using TBridge.Cemi.Presenter;
using CommonEntities = TBridge.Cemi.Type.Entities;

public partial class ConfermaFabbisognoStep2 : Page
{
    private int idUtente;
    private int idImpresa;
    private TSBusiness tsBiz = new TSBusiness();
   Common _commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);
        #endregion

        if (GestioneUtentiBiz.IsImpresa())
        {
            TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa impresaEnt =
                (TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            idImpresa = impresaEnt.IdImpresa;
            idUtente = impresaEnt.IdUtente;
        }
        else if (GestioneUtentiBiz.IsConsulente())
        {
            Consulente consulente = (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            CommonEntities.Impresa impresa = tsBiz.GetImpresaSelezionataConsulente(consulente.IdConsulente);
            idImpresa = impresa.IdImpresa;
            idUtente = impresa.IdUtente;
        }
        else if (GestioneUtentiBiz.IsFornitore())
        {
            Fornitore fornitore =
                (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            //prendiamo da db l'impresa selezionata dal fornitore
            CommonEntities.Impresa impresa = tsBiz.GetImpresaSelezionataFornitore(fornitore.IdFornitore);
            idImpresa = impresa.IdImpresa;
            idUtente = impresa.IdUtente;
        }

        if (!Page.IsPostBack)
        {
            IndirizzoAmministrativo(idImpresa);

            CaricaPreIndirizzi();
            CaricaProvince();

            int idProvincia;
            if ((DropDownListProvincia.SelectedValue != null) &&
                (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                CaricaComuni(idProvincia);

            Int64 idComune;
            if ((DropDownListComuni.SelectedValue != null) &&
                (Int64.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                CaricaCAP(idComune);
        }

        AbilitaIndirizzo();

        #region Click multipli
        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalvaOrdine, null) + ";");
        sb.Append("return true;");
        ButtonSalvaOrdine.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalvaOrdine);
        #endregion
    }

    private void IndirizzoAmministrativo(int impresa)
    {
        Indirizzo indirizzo = tsBiz.GetIndirizzoAmministrativo(impresa);

        if (indirizzo != null)
        {
            if (indirizzo.Corretto)
            {
                TextBoxIndirizzoSpedizione.Text = indirizzo.IndirizzoCompleto;
                RadioButtonSedeAmmi.Checked = true;
                //RadioButtonList1.SelectedIndex = 0;
                RadioButtonList1.Items[0].Enabled = true;
                LabelIndirizzoErrore.Visible = false;

                ViewState["Indirizzo"] = indirizzo;
            }
            else
            {
                //RadioButtonList1.SelectedIndex = 1;
                RadioButtonList1.Items[0].Enabled = false;
                LabelIndirizzoErrore.Visible = true;
            }
        }
        else
        {
            RadioButtonAltro.Checked = true;
            //RadioButtonList1.SelectedIndex = 2;
        }
    }

    #region Metodi per caricamento DropDown
    private void CaricaProvince()
    {
        DataTable dtProvince = _commonBiz.GetProvince();

        Presenter.CaricaElementiInDropDown(
            DropDownListProvincia,
            dtProvince,
            "sigla",
            "idProvincia");
    }

    private void CaricaPreIndirizzi()
    {
        ListDictionary preIndirizzi = _commonBiz.GetPreIndirizzi();

        Presenter.CaricaElementiInDropDown(
            DropDownListPreIndirizzo,
            preIndirizzi,
            "Value",
            "Value");
    }

    private void CaricaComuni(int idProvincia)
    {
        DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

        Presenter.CaricaElementiInDropDown(
            DropDownListComuni,
            dtComuni,
            "denominazione",
            "idComune");
    }

    private void CaricaCAP(Int64 idComune)
    {
        DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

        Presenter.CaricaElementiInDropDown(
            DropDownListCAP,
            dtCAP,
            "cap",
            "cap");
    }
    #endregion

    private bool SalvaStorico()
    {
        bool ret = false;
        Indirizzo indirizzo = null;

        if (RadioButtonSedeAmmi.Checked && (ViewState["Indirizzo"] != null))
        {
            // Indirizzo Amministrativo
            indirizzo = (Indirizzo)ViewState["Indirizzo"];

            indirizzo.Telefono = Presenter.NormalizzaCampoTesto(TextBoxTelefono.Text);
            indirizzo.Cellulare = Presenter.NormalizzaCampoTesto(TextBoxCellulare.Text);
        }
        else
        {
            if (RadioButtonAltro.Checked)
            {
                if (!string.IsNullOrEmpty(TextBoxIndirizzo.Text))
                {
                    LabelIndirizzoVuoto.Visible = false;

                    // Altro indirizzo
                    string presso = string.Empty;
                    string ind = string.Empty;
                    string provincia = string.Empty;
                    string comune = string.Empty;
                    string cap = string.Empty;

                    if (TextBoxPresso.Text.Trim().Length != 0)
                        presso = "C/O " + TextBoxPresso.Text;

                    ind = DropDownListPreIndirizzo.Text + " " + TextBoxIndirizzo.Text;
                    provincia = DropDownListProvincia.SelectedItem.Text;
                    comune = DropDownListComuni.SelectedItem.Text;
                    cap = DropDownListCAP.SelectedItem.Text;

                    if ((ind != string.Empty) && (provincia != string.Empty) && (comune != string.Empty) &&
                        (cap != string.Empty))
                    {
                        indirizzo = new Indirizzo(ind, provincia, comune, cap, true, presso);
                        indirizzo.Telefono = Presenter.NormalizzaCampoTesto(TextBoxTelefono.Text);
                        indirizzo.Cellulare = Presenter.NormalizzaCampoTesto(TextBoxCellulare.Text);
                    }
                }
                else
                {
                    LabelIndirizzoVuoto.Visible = true;
                }
            }
        }
        if (indirizzo != null)
        {
            int? idConsulente = null;
            int? idFornitore = null;

            // Se chi ha compilato il fabbisogno � un consulente lo memorizzo
            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                idConsulente = consulente.IdConsulente;
            }
            else
            {
                // Se chi ha compilato il fabbisogno � un fornitore lo memorizzo
                if (GestioneUtentiBiz.IsFornitore())
                {
                    Fornitore fornitore =
                        (Fornitore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    idFornitore = fornitore.IdFornitore;
                }
            }


            if (tsBiz.SaveStorico(idImpresa, indirizzo, idConsulente, idFornitore) > 0)
                ret = true;
        }

        return ret;
    }

    private void AbilitaIndirizzo()
    {
        TextBoxIndirizzoSpedizione.Enabled = RadioButtonSedeAmmi.Checked;

        TextBoxIndirizzo.Enabled = RadioButtonAltro.Checked;
        DropDownListComuni.Enabled = RadioButtonAltro.Checked;
        DropDownListPreIndirizzo.Enabled = RadioButtonAltro.Checked;
        DropDownListProvincia.Enabled = RadioButtonAltro.Checked;
        DropDownListCAP.Enabled = RadioButtonAltro.Checked;
        TextBoxPresso.Enabled = RadioButtonAltro.Checked;
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/TuteScarpe/GestioneFabbisogno.aspx");
    }

    protected void ButtonSalvaOrdine_Click(object sender, EventArgs e)
    {
        //Effettuiamo il salvataggio dello storico
        if (SalvaStorico())
        {
            Response.Redirect("~/TuteScarpe/ConfermaFabbisognoStep3.aspx?ok=true");
        }
        else
            Response.Redirect("~/TuteScarpe/ConfermaFabbisognoStep3.aspx?ok=false");
    }

    protected void DropDownListPreIndirizzo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idProvincia = -1;

        if ((DropDownListProvincia.SelectedValue != null) &&
            (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
            CaricaComuni(idProvincia);

        int idComune = -1;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }

/*
    private string creaXmlSchema(int impresa)
    {
        string xmlSchema;
        StringBuilder stringBuilder = new StringBuilder();

        using (XmlWriter writer = XmlWriter.Create(stringBuilder))
        {
            DataTable gruppi = tsBiz.getGruppiSelezionati(impresa);
            gruppi.WriteXmlSchema(writer);

            xmlSchema = stringBuilder.ToString();
        }

        return xmlSchema;
    }
*/

/*
    private string creaXmlOrdine(int idImpresa)
    {
        string xmlOrdine;
        StringBuilder stringBuilder = new StringBuilder();

        using (XmlWriter writer = XmlWriter.Create(stringBuilder))
        {
            DataTable gruppi = tsBiz.getGruppiSelezionati(idImpresa);
            gruppi.WriteXml(writer);

            xmlOrdine = stringBuilder.ToString();
        }

        return xmlOrdine;
    }
*/

    protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idComune;

        if ((DropDownListComuni.SelectedValue != null) &&
            (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
            CaricaCAP(idComune);
    }


    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaIndirizzo();
    }

    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {
        AbilitaIndirizzo();
    }
}
