<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="VisualizzaStorico.aspx.cs" Inherits="TuteScarpeVisualizzaStorico" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<%@ Register src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" tagname="TuteScarpeConsulenteImpresaSelezionata" tagprefix="uc6" %>


<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
  <telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        function onSelectedIndexChanged(sender, eventArgs) {
            sender.selectText(0, 1);
        }

        function openRadWindow(idImpresa, idOrdine) {
            var oWindow = radopen("DettaglioStatoConsegna.aspx?idImpresa=" + idImpresa + "&idOrdine=" + idOrdine);
            oWindow.set_title("Cassa Edile");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 260);
            oWindow.center();
        }
    </script>

</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/images/favicon.png" />
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Visualizzazione dello storico dei fabbisogni"/>
    <br />
    <uc6:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1" 
        runat="server" />
    <br />
    <b >
        Elenco dei fabbisogni o delle parti di un fabbisogno non ancora gestite
    </b>
    <br />
    <telerik:RadGrid
        ID="RadGridFabbisogniNonGestiti"
        runat="server" GridLines="None" AllowPaging="True" 
        onitemdatabound="RadGridFabbisogniNonGestiti_ItemDataBound" 
        onpageindexchanged="RadGridFabbisogniNonGestiti_PageIndexChanged" 
        PageSize="5" 
        onselectedindexchanged="RadGridFabbisogniNonGestiti_SelectedIndexChanged">
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

        <MasterTableView DataKeyNames="IdFabbisognoComplessivo">
            <Columns>
                <telerik:GridBoundColumn HeaderText="Data proposta di fabbisogno" 
                    UniqueName="DataPropostaFabbisogno" DataField="dataGenerazione" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Data conferma impresa" 
                    UniqueName="DataConfermaImpresa" DataField="dataConfermaImpresa" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Confermato da" 
                    UniqueName="ConfermatoDa">
                    <ItemTemplate>
                        <asp:Label ID="LabelConfermatoDa" runat="server"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" 
                    Text="Dettaglio" UniqueName="Dettaglio" ButtonCssClass="bottoneGrigliaTuteScarpe">
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    <b>
        Elenco dei fabbisogni gestiti
    </b>
    <telerik:RadGrid
        ID="RadGridStorico"
        runat="server"
        AllowPaging="True"
        PageSize="5" ondeletecommand="RadGridStorico_DeleteCommand" 
        onitemdatabound="RadGridStorico_ItemDataBound" 
        onpageindexchanged="RadGridStorico_PageIndexChanged" 
        onselectedindexchanged="RadGridStorico_SelectedIndexChanged" 
        GridLines="None">
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

        <MasterTableView DataKeyNames="idStoricoTuteScarpe,idTuteScarpeOrdine,idImpresa,ragioneSociale">
            <Columns>
                <telerik:GridBoundColumn HeaderText="Data proposta di fabbisogno" 
                    UniqueName="DataPropostaFabbisogno" DataField="dataGenerazione" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Protocollo ordine" 
                    UniqueName="ProtocolloOrdine" DataField="idTuteScarpeOrdine">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Data conferma ordine" 
                    UniqueName="DataConfermaOrdine" DataField="dataConferma" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Confermato da" 
                    UniqueName="ConfermatoDa">
                    <ItemTemplate>
                        <asp:Label ID="LabelConfermatoDa" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="260px" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Stato dell'ordine" 
                    UniqueName="StatoOrdine">
                    <ItemTemplate>
                        <%#ShowStatus((bool)DataBinder.Eval(Container.DataItem, "stato"))%>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" 
                    Text="Dettaglio" UniqueName="Dettaglio" ButtonCssClass="bottoneGrigliaTuteScarpe">
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn ButtonType="PushButton" Text="Consegna" ButtonCssClass="bottoneGrigliaTuteScarpe" UniqueName="StatoConsegnaColumn">
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    Lo stato "scaduto" indica che l'ordine non � stato completato e confermato nei tempi
    utili previsti e verr� riproposto successivamente.<br />
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <asp:Label ID="LabelDettagliStorico" runat="server" Font-Bold="True" Text="Dettaglio"
        Visible="False"></asp:Label>
    <br />
    <telerik:RadGrid
        ID="RadGridOrdine"
        runat="server" AllowPaging="True" 
        onpageindexchanged="RadGridOrdine_PageIndexChanged" 
        ondatabound="RadGridOrdine_DataBound" GridLines="None">
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

        <MasterTableView DataKeyNames="idLavoratore, nomeGruppo">
            <Columns>
                <telerik:GridBoundColumn HeaderText="Codice lavoratore" 
                    UniqueName="IdLavoratore" DataField="idLavoratore">
                    <ItemStyle Width="50px" Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Cognome e nome" 
                    UniqueName="CognomeNome" DataField="nomeLavoratore">
                    <ItemStyle Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Codice fiscale" 
                    UniqueName="CodiceFiscale" DataField="codiceFiscale">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Data di nascita" 
                    UniqueName="DataNascita" DataField="dataNascita" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Gruppo" 
                    UniqueName="Gruppo" DataField="nomeGruppo">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Indumento" 
                    UniqueName="Indumento" DataField="descrizione">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Taglia" 
                    UniqueName="Taglia" DataField="tagliaSel">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Qta" 
                    UniqueName="Quantita" DataField="quantita">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    <asp:Button ID="ButtonStampa" runat="server" Text="Stampa" OnClick="ButtonStampa_Click" Visible="False" Width="150px" />
    <asp:Button ID="ButtonExcel" runat="server" OnClick="ButtonExcel_Click" Text="Esporta in Excel" Visible="False" Width="150px" />&nbsp;<asp:Button
        ID="ButtonWord" runat="server" OnClick="ButtonWord_Click" Text="Esporta in Word" Visible="False" Width="150px" /><br />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
