using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;
using GUEntity = TBridge.Cemi.Type.Entities;

/// <summary>
///   Pagina che permette alla singola impresa di completare il suo fabbisogno
/// </summary>
public partial class GestioneFabbisogno : Page
{
    private readonly TSBusiness tsBusiness = new TSBusiness();
    private int idImpresa;
    private int idUtente;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);

        if (GestioneUtentiBiz.IsImpresa())
        {
            Impresa impresaEnt =
                (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            idImpresa = impresaEnt.IdImpresa;
            idUtente = impresaEnt.IdUtente;
        }
        else if (GestioneUtentiBiz.IsConsulente())
        {
            Consulente consulente = (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            TBridge.Cemi.Type.Entities.Impresa impresa =
                tsBusiness.GetImpresaSelezionataConsulente(consulente.IdConsulente);
            idImpresa = impresa.IdImpresa;
            idUtente = impresa.IdUtente;
        }
        else if (GestioneUtentiBiz.IsFornitore())
        {
            Fornitore fornitore =
                (Fornitore) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            //prendiamo da db l'impresa selezionata dal fornitore
            TBridge.Cemi.Type.Entities.Impresa impresa = tsBusiness.GetImpresaSelezionataFornitore(fornitore.IdFornitore);
            idImpresa = impresa.IdImpresa;
            idUtente = impresa.IdUtente;
        }

        if (!Page.IsPostBack)
        {
            DateTime? scadenzaProposta = tsBusiness.UltimoFabbisognoValido();
            if (!scadenzaProposta.HasValue || scadenzaProposta > DateTime.Now)
            {
                if (tsBusiness.EsisteFabbisogno(idImpresa))
                {
                    DataTable dtLavoratori = CaricaElencoLavoratori();

                    if (dtLavoratori != null && dtLavoratori.Rows.Count > 0)
                    {
                        if (scadenzaProposta != new DateTime(1900, 1, 1))
                        {
                            if (scadenzaProposta.HasValue)
                                LabelScadenza.Text += "il " + scadenzaProposta.Value.ToShortDateString();
                            else LabelScadenza.Text += "in data da definire";
                            LabelScadenza.Text += " e verr� riproposta aggiornata in seguito.";
                            LabelScadenza.Visible = true;
                        }

                        ViewState["dataFabbisognoComplessivo"] = (DateTime) dtLavoratori.Rows[0]["dataGenerazione"];

                        LabelInfo.Text =
                            "Digitando il pulsante �elimina� si ha la possibilit� di cancellare il nominativo di un lavoratore non pi� alle proprie dipendenze." +
                            Environment.NewLine + Environment.NewLine +
                            "La funzione �completa� consente, invece, di accedere alla schermata successiva, relativa alla compilazione dell�ordine del dipendente selezionato.";

                        Label1.Visible = true;
                        DropDownListFiltro.Visible = true;
                    }
                    else
                    {
                        RadGridElencoLavoratori.Visible = false;
                        ButtonStampa.Visible = false;
                        ButtonConferma.Visible = false;
                        TitoloSottotitolo1.sottoTitolo = "Stato salvataggio fabbisogno";
                        LabelInfo.Text =
                            "L'ordine di fabbisogno del mese corrente � gi� stato confermato. Non appena Cassa Edile avr� elaborato l'ordine, questo sar� visualizzabile alla voce \"Visualizza storico fabbisogni\" presente nel menu a lato.";
                        LabelConferma.Visible = false;
                        ImageButtonStampa.Visible = false;
                    }
                }
                else
                {
                    RadGridElencoLavoratori.Visible = false;
                    ButtonStampa.Visible = false;
                    ButtonConferma.Visible = false;
                    TitoloSottotitolo1.sottoTitolo = "Stato fabbisogno";
                    LabelInfo.Text = "Non risultano soddisfatti i requisiti di accantonamento orario previsti.";
                    LabelConferma.Visible = false;
                    ImageButtonStampa.Visible = false;
                }
            }
            else
            {
                RadGridElencoLavoratori.Visible = false;
                ButtonStampa.Visible = false;
                ButtonConferma.Visible = false;
                TitoloSottotitolo1.sottoTitolo = "Stato fabbisogno";
                LabelInfo.Text =
                    "Il sistema � in fase di aggiornamento. La prossima proposta di fabbisogno verr� pubblicata in seguito.";
                LabelConferma.Visible = false;
                ImageButtonStampa.Visible = false;
            }
        }
    }

    private DataTable CaricaElencoLavoratori()
    {
        DataTable dtLavoratori = null;

        if (DropDownListFiltro.SelectedValue == "TUTTI")
        {
            dtLavoratori = tsBusiness.ElencoLavoratori(idUtente, null);
        }
        if (DropDownListFiltro.SelectedValue == "NONCOMPLETATI")
        {
            dtLavoratori = tsBusiness.ElencoLavoratori(idUtente, false);
        }

        Presenter.CaricaElementiInGridView(
            RadGridElencoLavoratori,
            dtLavoratori);

        return dtLavoratori;
    }

    protected void ButtonStampa_Click(object sender, EventArgs e)
    {
        DateTime data = DateTime.MinValue;

        if (ViewState["dataFabbisognoComplessivo"] != null)
            data = (DateTime) ViewState["dataFabbisognoComplessivo"];

        //string s =
        //    String.Format("~/ReportStampa.aspx?idImpresa={0}&titolo={1}", idImpresa,
        //                  "Proposta di fabbisogno del " + data.ToShortDateString());
        //Response.Redirect(s);
        string s =
            String.Format("~/ReportStampa.aspx?titolo={1}", idImpresa,
                          "Proposta di fabbisogno del " + data.ToShortDateString());
        Context.Items["IdImpresa"] = idImpresa;
        Server.Transfer(s);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //Deleghiamo tutti i controlli alla pagina Step1
        Response.Redirect("~/TuteScarpe/ConfermaFabbisognoStep1.aspx");
    }

    public string creaXml(int impresa)
    {
        //TODO controllare formato

        DataTable gruppi = tsBusiness.getGruppiSelezionati(impresa);
        string lastUtente = "";
        string lastGruppo = "";
        string lastIndumento = "";
        string xml = "<selezionati>";
        bool firstGruppo = true;
        for (int i = 0; i < gruppi.Rows.Count; i++)
        {
            xml += "<item lavoratore=\"" + gruppi.Rows[i]["idLavoratore"] + "\" gruppo=\"" +
                   gruppi.Rows[i]["nomeGruppo"] + "\" elemento=\"" + gruppi.Rows[i]["idIndumento"] +
                   "\" taglia=\"" + gruppi.Rows[i]["tagliaSel"] + "\"/>";
        }

        return xml += "</selezionati>";
    }

    public string creaXmlOrdine(int idImpresa)
    {
        string xmlOrdine = String.Empty;
        StringBuilder stringBuilder = new StringBuilder();

        using (XmlWriter writer = XmlWriter.Create(stringBuilder))
        {
            DataTable gruppi = tsBusiness.getGruppiSelezionati(idImpresa);
            gruppi.WriteXml(writer);

            xmlOrdine = stringBuilder.ToString();
        }

        return xmlOrdine;
    }

    protected void DropDownListFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        CaricaElencoLavoratori();
    }

    protected void RadGridElencoLavoratori_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            DataRowView row = (DataRowView) e.Item.DataItem;
            if ((bool) row["eliminato"])
            {
                ((Button) e.Item.Cells[7].Controls[0]).Text = "Ripristina";
                ((e.Item.Cells[8])).Enabled = false;
            }
            else
            {
                ((Button) e.Item.Cells[7].Controls[0]).Text = "Elimina";
                ((e.Item.Cells[8])).Enabled = true;
            }
        }
    }

    protected void RadGridElencoLavoratori_DeleteCommand(object source, GridCommandEventArgs e)
    {
        Int32 idLavoratore =
            (Int32) RadGridElencoLavoratori.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdLavoratore"];
        Int32 idImpresa = (Int32) RadGridElencoLavoratori.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
        String nomeLavoratore =
            (String) RadGridElencoLavoratori.MasterTableView.DataKeyValues[e.Item.ItemIndex]["NomeLavoratore"];
        Boolean eliminato =
            (Boolean) RadGridElencoLavoratori.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Eliminato"];

        Context.Items["IdLavoratore"] = idLavoratore;
        Context.Items["IdImpresa"] = idImpresa;
        Context.Items["NomeLavoratore"] = nomeLavoratore;

        if (!eliminato)
        {
            Server.Transfer("~/TuteScarpe/EliminaLavoratoreDaFabbisogno.aspx");
        }
        else
        {
            Server.Transfer("~/TuteScarpe/RipristinaLavoratoreInFabbisogno.aspx");
        }
    }

    protected void RadGridElencoLavoratori_EditCommand(object source, GridCommandEventArgs e)
    {
        Int32 idLavoratore =
            (Int32) RadGridElencoLavoratori.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdLavoratore"];

        Context.Items["IdImpresa"] = idImpresa;
        Context.Items["IdLavoratore"] = idLavoratore;
        Server.Transfer("~/TuteScarpe/DettaglioLavoratore.aspx");
    }

    protected void RadGridElencoLavoratori_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaElencoLavoratori();
    }
}