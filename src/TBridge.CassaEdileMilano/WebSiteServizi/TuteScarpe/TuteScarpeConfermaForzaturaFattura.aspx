<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeConfermaForzaturaFattura.aspx.cs" Inherits="TuteScarpeConfermaForzaturaFattura" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Conferma fattura"/>
    <br />
    Si � scelto di accettare la fattura numero
    <asp:Label ID="LabelCodiceFattura" runat="server" Font-Bold="True"></asp:Label>
    anche se presenta delle inesattezze. Continuare con la conferma?<br />
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red" Visible="False"></asp:Label><br />
    <br />
    <asp:Button ID="ButtonConferma" runat="server" Text="Conferma fattura" OnClick="ButtonConferma_Click" />
    &nbsp;<asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="149px" OnClick="ButtonIndietro_Click" /></asp:Content>

