 <%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="TuteScarpeCancellaFattura.aspx.cs" Inherits="TuteScarpeCancellaFattura" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc2" %>
<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc3" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc4" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
    &nbsp;
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc4:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server"  titolo = "Gestione Tute e Scarpe" sottoTitolo="Conferma cancellazione fattura"/>
    &nbsp;<br />
    Si � scelto di eliminare la fattura numero
    <asp:Label ID="LabelCodiceFattura" runat="server" Font-Bold="True"></asp:Label>
    dal sistema. Continuare con la cancellazione?<br />
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red" Visible="False"></asp:Label><br />
    <br />
    <asp:Button ID="ButtonConferma" runat="server" Text="Conferma cancellazione" OnClick="ButtonConferma_Click" />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="201px" OnClick="ButtonIndietro_Click" />
</asp:Content>
