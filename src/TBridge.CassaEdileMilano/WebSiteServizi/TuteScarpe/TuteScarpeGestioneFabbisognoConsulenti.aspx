<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeGestioneFabbisognoConsulenti.aspx.cs" Inherits="TuteScarpe_TuteScarpeGestioneFabbisognoConsulenti" %>

<%@ Register Src="../WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register src="WebControls/TuteScarpeConsulenteImpresaSelezionaImpresa.ascx" tagname="TuteScarpeConsulenteImpresaSelezionaImpresa" tagprefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione fabbisogni"
        titolo="Tute e Scarpe" />
    <br />
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label><br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Imprese associate al consulente"></asp:Label>
    <br />
    <uc3:TuteScarpeConsulenteImpresaSelezionaImpresa ID="TuteScarpeConsulenteImpresaSelezionaImpresa1" 
        runat="server" />
</asp:Content>

