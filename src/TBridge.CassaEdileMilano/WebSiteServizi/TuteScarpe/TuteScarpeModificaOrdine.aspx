<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeModificaOrdine.aspx.cs" Inherits="TuteScarpeModificaOrdine" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Modifica ordine" />
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 270px">
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxIdOrdine" runat="server" Enabled="False" Visible="False"
                    Width="249px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" style="width: 270px; height: 28px">
                Descrizione
            </td>
            <td align="left" style="width: 3px; height: 28px">
                <asp:TextBox ID="TextBoxDescrizione" runat="server" Enabled="False" Width="249px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" style="width: 270px">
                Data creazione
            </td>
            <td align="left" style="width: 3px">
                <asp:TextBox ID="TextBoxDataCreazione" runat="server" Enabled="False" ReadOnly="True"
                    Width="249px"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Dettagli" Width="226px"></asp:Label>
    <br />
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="L'ordine deve contenere almeno un fabbisogno confermato"
        Visible="False"></asp:Label>
    <asp:GridView
        ID="GridViewDettagli" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        OnPageIndexChanging="GridViewDettagli_PageIndexChanging" Width="100%" PageSize="8" DataKeyNames="IdFabbisogno" OnRowDeleting="GridViewDettagli_RowDeleting">
        <Columns>
            <asp:BoundField DataField="IdFabbisogno" HeaderText="Id fabbisogno" 
                Visible="False" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione Sociale" />
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Fornitore" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataConfermaImpresa" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="Data Conferma" HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Delete" Text="Rimuovi" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Fabbisogni confermati"
        Width="226px"></asp:Label><br />
    <asp:GridView ID="GridViewFabbisogniConfermati" runat="server" AllowPaging="True"
        AutoGenerateColumns="False" DataKeyNames="IdFabbisogno" 
        OnPageIndexChanging="GridViewFabbisogniConfermati_PageIndexChanging" 
        PageSize="8" Width="100%" 
        onrowcommand="GridViewFabbisogniConfermati_RowCommand">
        <Columns>
            <asp:BoundField DataField="IdFabbisogno" HeaderText="Id Fabbisogno" 
                Visible="False" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Fornitore" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataConfermaImpresa" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="Data conferma impresa" HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Aggiungi" Text="Aggiungi all'ordine" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun fabbisogno disponibile<br />
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
