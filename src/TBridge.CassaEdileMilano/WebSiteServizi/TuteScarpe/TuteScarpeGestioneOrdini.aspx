<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TuteScarpeGestioneOrdini.aspx.cs" Inherits="TuteScarpeGestioneOrdini" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Gestione ordini - modifica, cancellazione e conferma"/>
    <br />
    <asp:Label ID="LabelFiltroOrdini" runat="server" Font-Bold="True" Text="Filtro ordini"></asp:Label><br />
    <br />
    <table class="standardTable">
        <tr>
            <td align="left">
                Stato:</td>
            <td align="left">
                <asp:RadioButton ID="RadioButtonConfermati" runat="server" Text="Confermati" GroupName="scelta" /></td>
            <td></td>
            <td>
            </td>
            <td align="left">
            <asp:RadioButton ID="RadioButtonNonConfermati" runat="server" Text="Non confermati" Checked="True" GroupName="scelta" /></td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                Dal (gg/mm/aaaa)&nbsp;</td>
            <td align="left">
                <asp:TextBox ID="TextBoxDa" runat="server"></asp:TextBox></td>
            <td><asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="TextBoxDa" Display="Dynamic" 
                    ErrorMessage="Formato data non valido" 
                    ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d" 
                    ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>
            <td align="left">
                Al (gg/mm/aaa)</td>
            <td align="left">
                <asp:TextBox ID="TextBoxA" runat="server"></asp:TextBox></td>
            <td><asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" 
                    ControlToValidate="TextBoxA" Display="Dynamic" 
                    ErrorMessage="Formato data non valido" 
                    ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d" 
                    ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonFiltraOrdini" runat="server" OnClick="ButtonFiltraOrdini_Click"
        Text="Ricerca ordini" Width="145px" ValidationGroup="ricerca" /><br />
    <br />
    <asp:Label ID="LabelElencoOrdini" runat="server" Font-Bold="True" Text="Elenco ordini"></asp:Label><br />
    <asp:GridView ID="GridViewOrdini" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDeleting="GridViewOrdini_RowDeleting" OnRowEditing="GridViewOrdini_RowEditing" DataKeyNames="IdOrdine" OnRowUpdating="GridViewOrdini_RowUpdating" AllowPaging="True" OnPageIndexChanging="GridViewOrdini_PageIndexChanging" PageSize="8" OnRowCommand="GridViewOrdini_RowCommand">
        <EmptyDataTemplate>
            Nessun ordine presente
        </EmptyDataTemplate>
        <Columns>
            <asp:BoundField DataField="IdOrdine" HeaderText="Protocollo ordine" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataCreazione" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data creazione"
                HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="DataConferma" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data conferma"
                HtmlEncode="False" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Gestore" />
            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
            <asp:BoundField DataField="ScaricatoFornitore" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="File scaricato dal fornitore" HtmlEncode="False" >
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="View" Text="Visualizza" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Edit" Text="Modifica" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Delete" Text="Cancella" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Update" Text="Conferma" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
    </asp:GridView>
</asp:Content>

