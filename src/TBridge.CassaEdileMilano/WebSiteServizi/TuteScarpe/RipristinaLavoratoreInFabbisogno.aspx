<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RipristinaLavoratoreInFabbisogno.aspx.cs" Inherits="RipristinaLavoratoreInFabbisogno" %>

<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>

<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Ripristino lavoratore"/>
    <br />
    Il lavoratore
    <asp:Label ID="LabelNomeLavoratore" runat="server" Font-Bold="True"></asp:Label>,
    codice
    <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="True"></asp:Label>,
    sta per essere ripristinato nell'elenco dei lavoratori aventi diritto al fabbisogno.
    Continuare con il ripristino?<br />
    <br />
    <center>
        <asp:Button ID="ButtonConfermaRipristino" runat="server" OnClick="ButtonConfermaRipristino_Click"
            Text="Conferma ripristino" Width="200px" />
        <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
            Width="200px" />
    </center>
</asp:Content>
