using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class VodDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.VodElencoImpreseConsulenti);
        funzionalita.Add(FunzionalitaPredefinite.VodElencoImpreseMedie);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "VodDefault.aspx");
    }
}