using System;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Notifiche.Business;
using TBridge.Cemi.Notifiche.Type.Collections;
using TBridge.Cemi.Notifiche.Type.Entities;
using Telerik.Web.UI;

public partial class NotificheCessazione : Page
{
    private readonly NotificaBusiness biz = new NotificaBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.NNotificheGestione);

        #endregion

        NotificheImpresaSelezionata1.OnImpresaSelected += NotificheImpresaSelezionata1_OnImpresaSelected;

        // Imposto il limite per la data assunzione a oggi
        CompareValidatorDataCessazione.ValueToCompare = DateTime.Now.ToShortDateString();

        #region Click Multipli

        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonOk, null) + ";");
        sb.Append("return true;");
        ButtonOk.Attributes.Add("onclick", sb.ToString());

        ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonOk);

        #endregion

        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idConsulente = consulente.IdConsulente;

                PanelImprese.Visible = true;
            }
            else
            {
                PanelImprese.Visible = false;
            }
        }
    }

    private void NotificheImpresaSelezionata1_OnImpresaSelected()
    {
        Page.Validate("");

        AzzeraCampi();
        LabelTitoloElenco.Visible = false;

        GridViewLavoratori.DataSource = null;
        GridViewLavoratori.DataBind();
        GridViewLavoratori.Visible = false;
    }

    private void CaricaDettagli(NotificaCollection listaNotifiche, int indice)
    {
        Notifica not = listaNotifiche[indice];
        Session["Notifica"] = not;

        TextBoxCognomeL.Text = not.Cognome;
        TextBoxNomeL.Text = not.Nome;
        if (not.DataNascita != new DateTime())
            TextBoxDataNascitaL.Text = not.DataNascita.ToShortDateString();
        TextBoxComuneNascitaL.Text = not.LuogoNascita;
        if (!String.IsNullOrEmpty(not.Sesso))
        {
            RadioButtonListSessoL.SelectedValue = not.Sesso;
        }
        TextBoxCodiceFiscaleL.Text = not.CodiceFiscale;
        if (not.DataAssunzione.HasValue)
            TextBoxDataAssunzioneL.Text = not.DataAssunzione.Value.ToShortDateString();
    }

    protected void GridViewLavoratori_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int indiceNotifica = GridViewLavoratori.PageIndex*GridViewLavoratori.PageSize + e.NewEditIndex;
        NotificaCollection listaNotifiche = (NotificaCollection) Session["ListaNotifiche"];

        CaricaDettagli(listaNotifiche, indiceNotifica);
        PanelDettagli.Visible = true;
        LabelMessaggio.Visible = true;
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Page.Validate("selezioneImpresa");

            if (Page.IsValid)
            {
                GridViewLavoratori.PageIndex = 0;
                Ricerca();

                LabelRisultato.Visible = false;
                LabelTitoloElenco.Visible = true;

                GridViewLavoratori.Visible = true;
            }
        }
    }

    private void Ricerca()
    {
        AzzeraCampi();

        string codiceFiscale = null;
        if ((TextBoxCF.Text != string.Empty) && (RadioButtonCF.Checked))
            codiceFiscale = TextBoxCF.Text;

        string cognome = null;
        string nome = null;
        DateTime? dataNascita = null;

        if (RadioButtonNome.Checked)
        {
            if (TextBoxCognome.Text != string.Empty)
                cognome = TextBoxCognome.Text;

            if (TextBoxNome.Text != string.Empty)
                nome = TextBoxNome.Text;

            DateTime dt;
            if (DateTime.TryParse(TextBoxDataNascita.Text, out dt))
                dataNascita = dt;
        }

        NotificaCollection listaNotifiche =
            biz.Ricerca(codiceFiscale, cognome, nome, dataNascita, (int) Session["idImpresa"]);
        GridViewLavoratori.DataSource = listaNotifiche;
        Session["ListaNotifiche"] = listaNotifiche;
        GridViewLavoratori.DataBind();
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewLavoratori.PageIndex = e.NewPageIndex;
        Ricerca();
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            try
            {
                if (Session["Notifica"] != null)
                {
                    bool err = false;
                    DateTime dataCessazione = DateTime.Parse(TextBoxDataCessazioneL.Text);
                    Notifica notifica = (Notifica) Session["Notifica"];

                    if ((notifica.DataAssunzione.HasValue) && (notifica.TipoNotifica == TipoNotifica.CessazioneNotifica))
                    {
                        DateTime dataLimiteTemp = DateTime.Now.AddDays(-DateTime.Now.Day + 1).AddMonths(-1);
                        DateTime dataLimite =
                            new DateTime(dataLimiteTemp.Year, dataLimiteTemp.Month, dataLimiteTemp.Day,
                                         new GregorianCalendar());

                        if (dataCessazione < dataLimite)
                        {
                            LabelErrore.Text = "La data di cessazione non puo' essere precedente al " +
                                               dataLimite.ToShortDateString();
                            err = true;
                        }
                        else
                        {
                            if (dataCessazione < notifica.DataAssunzione)
                            {
                                LabelErrore.Text =
                                    "La data di cessazione non puo' essere precedente alla data di assunzione";
                                err = true;
                            }
                        }
                    }
                    else if (notifica.TipoNotifica == TipoNotifica.CessazioneDenuncia)
                    {
                        DateTime dataLimiteTemp = DateTime.Now.AddDays(-DateTime.Now.Day + 1).AddMonths(-1);
                        DateTime dataLimite =
                            new DateTime(dataLimiteTemp.Year, dataLimiteTemp.Month, dataLimiteTemp.Day,
                                         new GregorianCalendar());

                        if (dataCessazione < dataLimite)
                        {
                            LabelErrore.Text = "La data di cessazione non puo' essere precedente al " +
                                               dataLimite.ToShortDateString();
                            err = true;
                        }
                    }

                    if (!err)
                    {
                        notifica.DataCessazione = dataCessazione;
                        try
                        {
                            biz.Cessazione(notifica);
                            AzzeraCampi();
                            LabelRisultato.Visible = true;
                            //Request.QueryString["idNotifica"] = null;
                            Ricerca();
                        }
                        catch
                        {
                            LabelErrore.Text = "Errore nell'inserimento della notifica";
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }

    private void AzzeraCampi()
    {
        TextBoxCognomeL.Text = string.Empty;
        TextBoxNomeL.Text = string.Empty;
        TextBoxDataNascitaL.Text = string.Empty;
        TextBoxComuneNascitaL.Text = string.Empty;
        TextBoxCodiceFiscaleL.Text = string.Empty;
        TextBoxDataAssunzioneL.Text = string.Empty;
        TextBoxDataCessazioneL.Text = string.Empty;
        LabelErrore.Text = string.Empty;
        PanelDettagli.Visible = false;
        LabelMessaggio.Visible = false;
    }

    public string StringaTipo(TipoNotifica tipo)
    {
        string res = string.Empty;

        switch (tipo)
        {
            case TipoNotifica.CessazioneDenuncia:
                res = "Denuncia";
                break;
            case TipoNotifica.CessazioneNotifica:
                res = "Notifica";
                break;
        }

        return res;
    }

    protected void CustomValidatorImpresaSelezionata_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (Session["idImpresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
}