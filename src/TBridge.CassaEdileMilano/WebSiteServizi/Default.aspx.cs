using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        if (utente is Impresa)
        {
            PanelAutenticato.Visible = false;
            PanelImpresa.Visible = true;
            PanelLavoratore.Visible = false;
            PanelPubblico.Visible = false;
        }
        else if (utente is Consulente)
        {
            PanelAutenticato.Visible = false;
            PanelImpresa.Visible = true;
            PanelLavoratore.Visible = false;
            PanelPubblico.Visible = false;
        }
        else if (utente is Lavoratore)
        {
            PanelAutenticato.Visible = false;
            PanelImpresa.Visible = false;
            PanelLavoratore.Visible = true;
            PanelPubblico.Visible = false;
        }
        else if (utente != null)
        {
            PanelAutenticato.Visible = true;
            PanelImpresa.Visible = false;
            PanelLavoratore.Visible = false;
            PanelPubblico.Visible = false;
        }
    }
}