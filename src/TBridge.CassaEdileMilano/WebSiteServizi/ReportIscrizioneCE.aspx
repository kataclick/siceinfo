<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportIscrizioneCE.aspx.cs" Inherits="ReportIscrizioneCE" %>

<%@ Register Src="WebControls/IscrizioneCEMenu.ascx" TagName="IscrizioneCEMenu" TagPrefix="uc1" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa modulo"
        titolo="Iscrizione imprese" />
    <br />
    ATTENZIONE: Stampare il modulo e consegnarlo a Cassa Edile di Milano<br />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerIscrizioneCE" runat="server" ProcessingMode="Remote"
                    Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>