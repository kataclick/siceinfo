using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Subappalti.Business;

/// <summary>
/// Visualizza lo storico delle ricerche - form usata dall'impresa per vedere le ricerche su di s�
/// </summary>
public partial class SubappaltiVisualizzaStoricoPropriaImpresa : Page
{
    /// <summary>
    /// Utente che sta visualizzando la pagina
    /// </summary>
    private int idUtente;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Controllo l'autorizzazione dell'utente a vedere la pagina ...
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.SubappaltiRicerca,
                                              "SubappaltiVisualizzaStoricoPropriaImpresa.aspx");

        // Ricavo l'utente che sta utilizzando la pagina
        //IUtente utente = ApplicationInstance.GetUtenteSistema();

        // Va settato sempre
        //idUtente = utente.IdUtente;

        idUtente = GestioneUtentiBiz.GetIdUtente();
    }

    protected void btnRicerca_Click(object sender, EventArgs e)
    {
        //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
        //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
        //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
        //TBridge.Cemi.ActivityTracking.Log.Write("Subappalti Visualizza ricerche propria impresa", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);

        // TODO: try catch
        VisualizzaStorico();
    }

    /// <summary>
    /// Carica lo storico nella GridView
    /// </summary>
    private void VisualizzaStorico()
    {
        SubappaltiBusiness business = new SubappaltiBusiness();

        int? idImpresa = null;
        DateTime? dataDa = null;
        DateTime? dataA = null;
        int test;
        DateTime testd;
        // Imposto i parametri
        if (!string.IsNullOrEmpty(txtIdImpresa.Text) && Int32.TryParse(txtIdImpresa.Text, out test))
            idImpresa = int.Parse(txtIdImpresa.Text);
        if (!string.IsNullOrEmpty(txtDataDa.Text) && DateTime.TryParse(txtDataDa.Text, out testd))
            dataDa = DateTime.Parse(txtDataDa.Text);
        if (!string.IsNullOrEmpty(txtDataA.Text) && DateTime.TryParse(txtDataA.Text, out testd))
            dataA = DateTime.Parse(txtDataA.Text);

        // Salvo la datasource nella session perch� mi serve per il paging
        Session["Subappalti.Datasource"] = business.VisualizzaStorico(idUtente, null, idImpresa, dataDa, dataA);
        gvStorico.DataSource = Session["Subappalti.Datasource"];

        gvStorico.DataBind();

        LabelRicerca.Visible = true;
    } // VisualizzaStorico

    protected void gvStorico_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // A sto punto la datasource � nulla ... la devo ricaricare
        gvStorico.DataSource = Session["Subappalti.Datasource"];

        gvStorico.PageIndex = e.NewPageIndex;
        gvStorico.DataBind();
    }
}