﻿using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Telerik.Web.UI;

public partial class VisualizzaReport : Page
{
    private readonly GestioneReport _gestioneReportBiz = new GestioneReport();
    private Report ReportGenerico { get; set; }

    protected override void OnPreInit(EventArgs e)
    {
        string nomeReport = Request["nomeReport"];
        if (!String.IsNullOrEmpty(nomeReport))
        {
            GestioneAutorizzazioneReport.ReportAutorizzato(nomeReport);

            ReportGenerico = _gestioneReportBiz.GetReport(nomeReport);
            Theme = ReportGenerico.Theme;
        }

        base.OnPreInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Master.FindControl("MainFooter").Visible = false;

        if (ReportGenerico != null)
        {
            if (!Page.IsPostBack)
            {
                ReportViewerContenitore.ServerReport.ReportPath = ReportGenerico.Path;
            }
        }
        else
        {
            ReportViewerContenitore.Visible = false;
            PanelListaReport.Visible = true;
            RadGridReport.NeedDataSource += RadGridReport_NeedDataSource;
        }
    }

    private void RadGridReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        int idUtente = GestioneUtentiBiz.GetIdUtente();
        RadGridReport.DataSource = _gestioneReportBiz.GetListaReport(idUtente);
    }

    protected void ReportViewer_Init(object sender, EventArgs e)
    {
        ReportViewerContenitore.ServerReport.ReportServerUrl =
            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
    }

    protected void RadGridReport_OnItemCommand(object source, GridCommandEventArgs e)
    {
        if (e.CommandName == "Link")
        {
            int index = e.Item.ItemIndex;
            String nomeReport = (string) RadGridReport.MasterTableView.DataKeyValues[index]["Nome"];
            Response.Redirect(String.Format("~/VisualizzaReport.aspx?nomeReport={0}", nomeReport));
        }
    }

    protected void ButtonClearFilter_Click(object sender, ImageClickEventArgs e)
    {
        RadGridReport.MasterTableView.FilterExpression = string.Empty;

        foreach (GridColumn column in RadGridReport.MasterTableView.RenderColumns)
        {
            if (column is GridBoundColumn)
            {
                GridBoundColumn boundColumn = column as GridBoundColumn;
                boundColumn.CurrentFilterValue = string.Empty;
            }
        }

        RadGridReport.MasterTableView.Rebind();
    }
}