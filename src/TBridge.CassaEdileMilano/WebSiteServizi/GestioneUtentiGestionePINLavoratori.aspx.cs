using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Filters;

public partial class GestioneUtentiGestionePINLavoratori : Page
{
    private GestioneUtentiBiz _gu;
    private TBridge.Cemi.Business.Common _biz;
    private List<LavoratoreConImpresaSmsCredenziali> _lavoratori;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori);

        _gu = new GestioneUtentiBiz();
        _biz = new TBridge.Cemi.Business.Common();

        if (!Page.IsPostBack)
        {
            DataBind();
        }
        else
        {
            // Verifico se � un Refresh fatto con Javascript
            String eventArg = Request["__EVENTARGUMENT"];

            if (eventArg != null)
            {
                int offset = eventArg.IndexOf("@@@@@");

                if (offset > -1)
                {
                    LoadLavoratoriPin();
                }
            }
        }
    }

    protected void ButtonFiltra_Click(object sender, EventArgs e)
    {
        LoadLavoratoriPin();
    }

    private void LoadLavoratoriPin()
    {
        //Creo il filtro
        FilterLavoratore filter = CreateFilter();

        //Faccio la richiesta passando il filtro
        //lavoratori = gu.GetLavoratori(filter);
        //lavoratori = gu.GetLavoratoriConImpresa(filter);
        //lavoratori = gu.GetLavoratoriConImpresaSms(filter);
        _lavoratori = _gu.GetLavoratoriConImpresaSmsCredenziali(filter);

        GridViewLavoratori.DataSource = _lavoratori;
        GridViewLavoratori.DataBind();

        if (_lavoratori != null && _lavoratori.Count > 0)
        {
            LinkButtonDeselezionaTutti.Visible = true;
            LinkButtonSelezionaTutti.Visible = true;
            ButtonEsportaExcel.Visible = true;
            ButtonFileTutti.Visible = true;
            ButtonGeneraPIN.Visible = true;
            ButtonGeneraTutti.Visible = true;
            LabelEsporta.Visible = true;
            LabelGenera.Visible = true;
        }
        else
        {
            LinkButtonDeselezionaTutti.Visible = false;
            LinkButtonSelezionaTutti.Visible = false;
            ButtonEsportaExcel.Visible = false;
            ButtonFileTutti.Visible = false;
            ButtonGeneraPIN.Visible = false;
            ButtonGeneraTutti.Visible = false;
            LabelEsporta.Visible = false;
            LabelGenera.Visible = false;
        }
    }

    private FilterLavoratore CreateFilter()
    {
        FilterLavoratore filter = new FilterLavoratore();

        try
        {
            DateTime dataGenerazionePinDal;
            DateTime dataGenerazionePinAl;

            if (TextBoxCodLavoratore.Text != "")
                filter.IdLavoratore = Int32.Parse(TextBoxCodLavoratore.Text);
            if (TextBoxCognome.Text != "")
                filter.Cognome = TextBoxCognome.Text.Replace('*', '%');
            if (TextBoxNome.Text != "")
                filter.Nome = TextBoxNome.Text.Replace('*', '%');
            if (TextBoxCodiceFiscale.Text != "")
                filter.CodiceFiscale = TextBoxCodiceFiscale.Text;
            if (DropDownListTipoRicerca.SelectedValue == "SENZAPIN")
                filter.WithPIN = false;
            if (DropDownListTipoRicerca.SelectedValue == "CONPIN")
                filter.WithPIN = true;
            if ((TextBoxDal.Text != "") && (DateTime.TryParse(TextBoxDal.Text, out dataGenerazionePinDal)))
                filter.DataGenerazionePinDa =
                    new DateTime(dataGenerazionePinDal.Year, dataGenerazionePinDal.Month, dataGenerazionePinDal.Day, 0,
                                 0, 0);
            if ((TextBoxAl.Text != "") && (DateTime.TryParse(TextBoxAl.Text, out dataGenerazionePinAl)))
                filter.DataGenerazionePinA =
                    new DateTime(dataGenerazionePinAl.Year, dataGenerazionePinAl.Month, dataGenerazionePinAl.Day, 0, 0,
                                 0);
            if (TextBoxCodImpresa.Text != "")
                filter.IdImpresa = Int32.Parse(TextBoxCodImpresa.Text);
            if (TextBoxRagioneSociale.Text != "")
                filter.RagioneSociale = TextBoxRagioneSociale.Text.Replace('*', '%');
            if (DropDownListIscrittoWeb.SelectedValue == "ISCRITTI")
            {
                filter.IscrittoSitoWeb = true;
            }
            else if (DropDownListIscrittoWeb.SelectedValue == "NONISCRITTI")
            {
                filter.IscrittoSitoWeb = false;
            }
            else
            {
                filter.IscrittoSitoWeb = null;
            }
            if (!String.IsNullOrEmpty(TextBoxCell.Text))
            {
                filter.NumeroCelluare = TextBoxCell.Text;
                //if (!filter.NumeroCelluare.StartsWith("+39"))
                //    filter.NumeroCelluare = "+39" + filter.NumeroCelluare;
            }
        }
        catch
        {
        }

        return filter;
    }

    protected void LinkButtonSelezionaTutti_Click(object sender, EventArgs e)
    {
        LoadLavoratoriPin();

        for (int i = 0; i < GridViewLavoratori.Rows.Count; i++)
        {
            // Recupero la check box nella griglia
            CheckBox cbNelFile = (CheckBox) GridViewLavoratori.Rows[i].FindControl("CheckBoxNelFile");
            cbNelFile.Checked = true;
        }
    }

    protected void LinkButtonDeselezionaTutti_Click(object sender, EventArgs e)
    {
        LoadLavoratoriPin();

        for (int i = 0; i < GridViewLavoratori.Rows.Count; i++)
        {
            // Recupero la check box nella griglia
            CheckBox cbNelFile = (CheckBox) GridViewLavoratori.Rows[i].FindControl("CheckBoxNelFile");
            cbNelFile.Checked = false;
        }
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        LoadLavoratoriPin();

        GridViewLavoratori.PageIndex = e.NewPageIndex;
        GridViewLavoratori.DataBind();
    }

    protected void ButtonEsportaExcel_Click(object sender, EventArgs e)
    {
        StringBuilder stringaCostruzione = new StringBuilder();

        // Titoli
        stringaCostruzione.Append(
            "Codice Impresa\tRagione sociale\tCodice Lavoratore\tCognome\tNome\tCodice fiscale\tPIN\tData generazione\tIndirizzo\tComune\tCap\tProvincia\n");

        for (int i = 0; i < GridViewLavoratori.Rows.Count; i++)
        {
            // Recupero la check box nella griglia
            CheckBox cbNelFile = (CheckBox) GridViewLavoratori.Rows[i].FindControl("CheckBoxNelFile");

            if (cbNelFile.Checked)
            {
                // Recupero i dati da memorizzare nel file

                string codLav;
                string cognome;
                string nome;
                string codiceFiscale;
                string pin;
                string dataGen;
                string codImp;
                string ragioneSociale;
                string indirizzoDenominazione = string.Empty;
                string indirizzoComune = string.Empty;
                string indirizzoCap = string.Empty;
                string indirizzoProvincia = string.Empty;

                codLav = GridViewLavoratori.Rows[i].Cells[0].Text;
                cognome = GridViewLavoratori.Rows[i].Cells[1].Text;
                nome = GridViewLavoratori.Rows[i].Cells[2].Text;
                codiceFiscale = GridViewLavoratori.Rows[i].Cells[3].Text;
                codImp = GridViewLavoratori.Rows[i].Cells[4].Text;
                ragioneSociale = GridViewLavoratori.Rows[i].Cells[5].Text;
                pin = GridViewLavoratori.Rows[i].Cells[7].Text;
                dataGen = GridViewLavoratori.Rows[i].Cells[8].Text;

                if (GridViewLavoratori.DataKeys[i].Values["IndirizzoDenominazione"] != null)
                    indirizzoDenominazione = GridViewLavoratori.DataKeys[i].Values["IndirizzoDenominazione"].ToString();
                if (GridViewLavoratori.DataKeys[i].Values["IndirizzoComune"] != null)
                    indirizzoComune = GridViewLavoratori.DataKeys[i].Values["IndirizzoComune"].ToString();
                if (GridViewLavoratori.DataKeys[i].Values["IndirizzoCap"] != null)
                    indirizzoCap = GridViewLavoratori.DataKeys[i].Values["IndirizzoCap"].ToString();
                if (GridViewLavoratori.DataKeys[i].Values["IndirizzoProvincia"] != null)
                    indirizzoProvincia = GridViewLavoratori.DataKeys[i].Values["IndirizzoProvincia"].ToString();

                stringaCostruzione.Append(codImp + "\t" + ragioneSociale + "\t" + codLav + "\t" + cognome + "\t" + nome +
                                          "\t" + codiceFiscale + "\t" + pin + "\t" + dataGen + "\t" +
                                          indirizzoDenominazione + "\t" + indirizzoComune + "\t" + indirizzoCap + "\t" +
                                          indirizzoProvincia + "\n");
            }
        }

        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment; filename=PIN.xls");
        Response.Charset = "";
        EnableViewState = false;
        Response.Write(stringaCostruzione.ToString());
        Response.End();
    }

    protected void ButtonFileTutti_Click(object sender, EventArgs e)
    {
        //Creo il filtro
        FilterLavoratore filter = CreateFilter();

        //Faccio la richiesta passando il filtro
        //lavoratori = gu.GetLavoratoriConImpresaSms(filter);
        _lavoratori = _gu.GetLavoratoriConImpresaSmsCredenziali(filter);

        GridView gv = new GridView();
        gv.AutoGenerateColumns = false;

        BoundField bc1 = new BoundField();
        bc1.HeaderText = "Codice lavoratore";
        bc1.DataField = "IdLavoratore";
        BoundField bc2 = new BoundField();
        bc2.HeaderText = "Cognome";
        bc2.DataField = "Cognome";
        BoundField bc3 = new BoundField();
        bc3.HeaderText = "Nome";
        bc3.DataField = "Nome";
        BoundField bc4 = new BoundField();
        bc4.HeaderText = "Codice fiscale";
        bc4.DataField = "CodiceFiscale";
        BoundField bc5 = new BoundField();
        bc5.HeaderText = "PIN";
        bc5.DataField = "Pin";
        BoundField bc6 = new BoundField();
        bc6.HeaderText = "Data generazione";
        bc6.DataField = "dataGenerazionePIN";
        BoundField bc7 = new BoundField();
        bc7.HeaderText = "Codice impresa";
        bc7.DataField = "IdImpresa";
        BoundField bc8 = new BoundField();
        bc8.HeaderText = "Ragione sociale";
        bc8.DataField = "RagioneSocialeImpresa";
        BoundField bc9 = new BoundField();
        bc9.HeaderText = "Indirizzo";
        bc9.DataField = "IndirizzoDenominazione";
        BoundField bc10 = new BoundField();
        bc10.HeaderText = "Comune";
        bc10.DataField = "IndirizzoComune";
        BoundField bc11 = new BoundField();
        bc11.HeaderText = "CAP";
        bc11.DataField = "IndirizzoCAP";
        BoundField bc12 = new BoundField();
        bc12.HeaderText = "Provincia";
        bc12.DataField = "IndirizzoProvincia";

        gv.Columns.Add(bc7);
        gv.Columns.Add(bc8);
        gv.Columns.Add(bc1);
        gv.Columns.Add(bc2);
        gv.Columns.Add(bc3);
        gv.Columns.Add(bc4);
        gv.Columns.Add(bc5);
        gv.Columns.Add(bc6);
        gv.Columns.Add(bc9);
        gv.Columns.Add(bc10);
        gv.Columns.Add(bc11);
        gv.Columns.Add(bc12);

        gv.DataSource = _lavoratori;
        gv.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=PIN.xls");
        Response.ContentType = "application/vnd.ms-excel";
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void ButtonGeneraPIN_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < GridViewLavoratori.Rows.Count; i++)
        {
            int id;

            if (Int32.TryParse(GridViewLavoratori.Rows[i].Cells[0].Text, out id))
            {
                _gu.GeneraPinLavoratore(id);
            }
        }

        if (GridViewLavoratori.Rows.Count > 0)
        {
            DropDownListTipoRicerca.SelectedValue = "CONPIN";

            LoadLavoratoriPin();
        }
    }

    protected void ButtonGeneraTutti_Click(object sender, EventArgs e)
    {
        LoadLavoratoriPin();

        List<Lavoratore> lista = (List<Lavoratore>) GridViewLavoratori.DataSource;

        foreach (Lavoratore lav in lista)
        {
            _gu.GeneraPinLavoratore(lav.IdLavoratore);
        }

        if (GridViewLavoratori.Rows.Count > 0)
        {
            DropDownListTipoRicerca.SelectedValue = "CONPIN";

            LoadLavoratoriPin();
        }
    }

    protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LavoratoreConImpresaSmsCredenziali lavoratore = (LavoratoreConImpresaSmsCredenziali) e.Row.DataItem;
            Button bImpersona = (Button) e.Row.FindControl("ButtonImpersona");

            if (String.IsNullOrEmpty(lavoratore.Username))
            {
                bImpersona.Enabled = false;
            }

            string comando = String.Format("openRadWindowSms('{0}'); return false;", lavoratore.IdLavoratore);
            Button buttonSms = (Button) e.Row.FindControl("ButtonSms");
            buttonSms.Attributes.Add("OnClick", comando);
        }
    }

    protected void GridViewLavoratori_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Genera")
        {
            int id;

            int index = Convert.ToInt32(e.CommandArgument);

            if (Int32.TryParse(GridViewLavoratori.Rows[index].Cells[0].Text, out id))
            {
                _gu.GeneraPinLavoratore(id);

                LoadLavoratoriPin();
                //FiltraImprese();
            }
        }
        else if (e.CommandName == "Impersona")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            ImpersonateManager.Impersonate(HttpUtility.HtmlDecode(GridViewLavoratori.Rows[index].Cells[6].Text));
        }


        //if (e.CommandName == "Genera")
        //{
        //    Page.Validate("CellVal");
        //    if (Page.IsValid)
        //    {
        //        int id;

        //        int index = Convert.ToInt32(e.CommandArgument);

        //        if (Int32.TryParse(GridViewLavoratori.Rows[index].Cells[0].Text, out id))
        //        {
        //            if (((CheckBox) GridViewLavoratori.Rows[index].FindControl("CheckBoxPin")).Checked)
        //            {
        //                _gu.GeneraPinLavoratore(id);
        //                LabelError.Text = string.Empty;
        //            }

        //            if (((CheckBox) GridViewLavoratori.Rows[index].FindControl("CheckBoxSms")).Checked)
        //            {
        //                string numTel =
        //                    ((TextBox) GridViewLavoratori.Rows[index].FindControl("TextBoxNumeroTel")).Text;
        //                if (!String.IsNullOrEmpty(numTel))
        //                {
        //                    //numTel = numTel.Replace("/", "").Replace(".", "").Trim().Replace("(", "").Replace(")", "");
        //                    //string regExCell = @"^\+?[\d]{3,19}$";
        //                    //if (Regex.IsMatch(numTel, regExCell))
        //                    //{}
        //                    int ret = _smsBusiness.RegistraLavoratoreSms(id, numTel);

        //                    string tipoErroreSms = string.Empty;

        //                    switch (ret)
        //                    {
        //                        case -1:
        //                            tipoErroreSms = "Lavoratore non presente";
        //                            break;
        //                        case -2:
        //                            tipoErroreSms = "Numero di telefono gi� in uso";
        //                            break;
        //                        case -3:
        //                            tipoErroreSms = "Lavoratore gi� registrato al servizio";
        //                            break;
        //                    }

        //                    if (ret != 0)
        //                        LabelError.Text =
        //                            string.Format(
        //                                "Errore durante l'iscrizione al servizio sms per il lavoratore con codice {0}\n{1}Codice errore: {2} - {3}",
        //                                id, Environment.NewLine, ret, tipoErroreSms);
        //                    else
        //                        LabelError.Text = string.Empty;
        //                }
        //                else
        //                    LabelError.Text =
        //                        string.Format(
        //                            "Errore durante l'iscrizione al servizio sms per il lavoratore con codice {0}:\n {1}Inserire il numero di Telefono",
        //                            id, Environment.NewLine);
        //            }

        //            LoadLavoratoriPin();
        //        }
        //    }
        //}
        //else if (e.CommandName == "Impersona")
        //{
        //    int index = Convert.ToInt32(e.CommandArgument);
        //    ImpersonateManager.Impersonate(HttpUtility.HtmlDecode(GridViewLavoratori.Rows[index].Cells[6].Text));
        //}
        //else if (e.CommandName == "PinSms")
        //{
        //    int index = Convert.ToInt32(e.CommandArgument);
        //    string pin = HttpUtility.HtmlDecode(GridViewLavoratori.Rows[index].Cells[7].Text);
        //    string numeroTelefono = ((TextBox)GridViewLavoratori.Rows[index].FindControl("TextBoxNumeroTel")).Text;
        //    if (pin.Length != 8)
        //    {
        //        LabelError.Text = "Generare prima un PIN!";
        //    }
        //    else if (String.IsNullOrEmpty(numeroTelefono))
        //    {
        //        LabelError.Text = "Indicare un numero di telefono!";
        //    }
        //    else
        //    {
        //        //TODO: Da verificare che sia pulito
        //        SmsBusiness smsBusiness = new SmsBusiness();
        //        SentSms sms = new SentSms
        //                          {
        //                              Tipo = SmsType.Immediato,
        //                              Testo = String.Format(ConfigurationManager.AppSettings["PINLAVORATORE"], pin),
        //                              Destinatari = new SmsAddresseeCollection {new SmsAddressee {Numero = numeroTelefono}},
        //                              Data = DateTime.Now
        //                          };
        //        smsBusiness.InviaSms(sms);

        //        LabelError.Text = String.Format("PIN inviato via SMS al numero {0}", numeroTelefono);
        //    }
        //}
    }

    protected String RecuperaPostBackCode()
    {
        return Page.ClientScript.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
        //return Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
    }

    protected void CellValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!String.IsNullOrEmpty(TextBoxCell.Text))
            args.IsValid = TBridge.Cemi.Business.Common.NumeroCellulareValido(TextBoxCell.Text);
        else
            args.IsValid = true;
    }
}