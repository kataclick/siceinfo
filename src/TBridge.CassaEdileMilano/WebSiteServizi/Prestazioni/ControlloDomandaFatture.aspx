<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlloDomandaFatture.aspx.cs" Inherits="Prestazioni_ControlloDomandaFatture" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/PrestazioniDatiDomanda.ascx" TagName="PrestazioniDatiDomanda"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/PrestazioniRicercaFattureFisicheNonAssociate.ascx"
    TagName="PrestazioniRicercaFattureFisicheNonAssociate" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PrestazioniDatiFattura.ascx" TagName="PrestazioniDatiFattura"
    TagPrefix="uc5" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda - Fatture"
        titolo="Prestazioni" />
    <br />
    <asp:Button ID="ButtonAggiungiFatturaDichiarata" runat="server" OnClick="ButtonAggiungiFatturaDichiarata_Click"
        Text="Aggiungi fattura dichiarata" Width="200px" />&nbsp;
    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
        Width="150px" />
    <br />
    Il tasto &quot;Aggiungi fattura dichiarata&quot; permette di aggiungere una fattura
    qualora il lavoratore se ne fosse dimenticato in fase di compilazione della domanda.<br />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc4:PrestazioniDatiDomanda ID="PrestazioniDatiDomanda1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelImportiPrestazione" runat="server" Visible="false">
                    <table class="standardTable">
                        <tr>
                            <td colspan="5">
                                <asp:Label ID="LabelTitoloImportiPrestazione" runat="server" Text="Importi della prestazione"
                                    Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30px">
                                <asp:Image ID="ImageStatoTotale" runat="server" />
                            </td>
                            <td>
                                Totale Fatture:
                            </td>
                            <td style="width: 65px">
                                <asp:Label ID="LabelTotaleFatture" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>
                                Totale diviso per importi
                            </td>
                            <td>
                                <asp:Label ID="LabelTotaleImporti" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:GridView ID="GridViewImportiPrestazione" runat="server" AutoGenerateColumns="False"
                                    Width="100%" DataKeyNames="IdTipoImporto,IdDomanda,IdImporto" OnRowDataBound="GridViewImportiPrestazione_RowDataBound"
                                    OnSelectedIndexChanging="GridViewImportiPrestazione_SelectedIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Stato">
                                            <ItemTemplate>
                                                <asp:Image ID="ImageStato" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle Width="10px" />
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Descrizione" DataField="Descrizione" />
                                        <asp:TemplateField HeaderText="Valore">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TextBoxValoreImporto" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorImporto" runat="server" ControlToValidate="TextBoxValoreImporto"
                                                    ErrorMessage="*" ValidationGroup="modificaValoreImportoPrestazione">
                                                </asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxValoreImporto"
                                                    ErrorMessage="*" Operator="DataTypeCheck" Type="Currency" ValidationGroup="modificaValoreImportoPrestazione">
                                                </asp:CompareValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="170px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:Button ID="ButtonSalvaImporto" runat="server" CausesValidation="True" ValidationGroup="modificaValoreImportoPrestazione"
                                                    CommandName="Select" Text="Salva" CssClass="bottoneGriglia" />
                                            </ItemTemplate>
                                            <ItemStyle Width="30px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <b>Fatture dichiarate dal lavoratore </b>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewFattureLavoratore" runat="server" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="data,numero,importoTotale,idPrestazioniFattura,dataAnnullamento,idPrestazioniFatturaDichiarata"
                    OnRowCommand="GridViewFattureLavoratore_RowCommand" OnRowDataBound="GridViewFattureLavoratore_RowDataBound"
                    OnDataBound="GridViewFattureLavoratore_DataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Image ID="ImageStato" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 80px">
                                            Data:
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label
                                                    ID="LabelDData"
                                                    runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 80px">
                                            Numero:
                                        </td>
                                        <td>
                                            <asp:Label
                                                ID="LabelDNumero"
                                                runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 80px">
                                            Importo totale:
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label
                                                    ID="LabelDImportoTotale"
                                                    runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 80px">
                                            Inserita il:
                                        </td>
                                        <td>
                                            <asp:Label
                                                ID="LabelDInseritaIl"
                                                runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 80px">
                                            Inserita da:
                                        </td>
                                        <td>
                                            <asp:Label
                                                ID="LabelDInseritaDa"
                                                runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField DataField="data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data" />
                        <asp:BoundField DataField="numero" HeaderText="Numero" />
                        <asp:BoundField DataField="importoTotale" HeaderText="Importo totale" DataFormatString="{0:0.00}" />--%>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla" CommandName="FatturaAnnulla"
                                    Width="80px" />
                                <asp:Button ID="ButtonRipristina" runat="server" Text="Rispristina" CommandName="FatturaRipristina"
                                    Width="80px" />
                            </ItemTemplate>
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonAssocia" runat="server" CommandName="FatturaAssocia" Text="Associa"
                                    Width="80px" />
                                <asp:Button ID="ButtonDisassocia" runat="server" CommandName="FatturaDisassocia"
                                    OnClientClick="return confirm('Confermi la dissociazione ?');" Text="Disassocia"
                                    Width="80px" />
                            </ItemTemplate>
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="FatturaValida"
                            Text="Validazione">
<ControlStyle CssClass="bottoneGriglia" Width="80px"></ControlStyle>

                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                        <asp:TemplateField HeaderText="Fattura fisica">
                            <ItemTemplate>
                                <table class="borderedTable">
                                    <tr>
                                        <td style="width: 75px">
                                            Data:
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelData" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 75px">
                                            Numero:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelNumero" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 75px">
                                            Importo:
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelImporto" runat="server"></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 75px">
                                            Valida:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelValida" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 75px">
                                            Scansionata il:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelScansionata" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:GridView ID="GridViewImporti" runat="server" Width="100%" AutoGenerateColumns="False"
                                                ShowHeader="False">
                                                <Columns>
                                                    <asp:BoundField DataField="Descrizione" HeaderText="Tipo importo" />
                                                    <asp:BoundField DataField="Valore" DataFormatString="{0:0.00}" HeaderText="Importo">
                                                        <ItemStyle Width="50px" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <small>Nessun importo associato.</small>
                                                    <br />
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="FatturaVedi"
                            Text="Vedi" >
<ControlStyle CssClass="bottoneGriglia" Width="40px"></ControlStyle>
                        <ItemStyle Width="10px" />
                        </asp:ButtonField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna fattura presente<br />
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelFattureFisicheAssociate" runat="server" Visible="false">
                    <table class="standardTable">
                        <tr>
                            <td colspan="2">
                                <strong>Fattura Fisica associata</strong>
                                <br />
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Data:
                            </td>
                            <td>
                                <asp:Label ID="LabelFatturaFisicaData" runat="server" Font-Bold="True" Text="Data"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Numero:
                            </td>
                            <td>
                                <asp:Label ID="LabelFatturaFisicaNumero" runat="server" Font-Bold="True" Text="Numero"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Originale:
                            </td>
                            <td>
                                <asp:Label ID="LabelOriginale" runat="server" Font-Bold="True" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Importo Totale:
                            </td>
                            <td>
                                <asp:Label ID="LabelFatturaFisicaImporto" runat="server" Font-Bold="True" Text="Importo"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Stato:
                            </td>
                            <td>
                                &nbsp;<asp:Image ID="ImageStatoValidazione" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Importi Riconosciuti per Fattura Fisica</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="GridViewImportiFattura" runat="server" AutoGenerateColumns="False"
                                    AllowSorting="True" AllowPaging="True" Width="100%" DataKeyNames="idPrestazioniFattura,idTipoPrestazione,idTipoPrestazioneImportoFattura,idPrestazioneImportoFattura"
                                    OnRowCommand="GridViewImportiFattura_RowCommand" OnRowDataBound="GridViewImportiFattura_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="descrizione" HeaderText="Descrizione" />
                                        <asp:TemplateField HeaderText="Valore">
                                            <ItemTemplate>
                                                <asp:TextBox ID="TextBoxValore" runat="server" Width="100px"></asp:TextBox>
                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxValore"
                                                    ErrorMessage="*" Operator="DataTypeCheck" Type="Currency" ValidationGroup="modificaValore"></asp:CompareValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" Wrap="True" />
                                        </asp:TemplateField>
                                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" CommandName="FatturaImportoEdit"
                                            Text="Salva" ButtonType="Button" ValidationGroup="modificaValore">
                                            <ItemStyle Width="10px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        Nessuna fattura Fisica associata o Nessuna Fattura dichiarata dal lavoratore selezionata
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="ButtonFatturaFisicaValida" runat="server" Text="Valida" OnClick="ButtonFatturaFisicaValida_Click"
                                    Width="150px" />
                                <asp:Button ID="ButtonFatturaFisicaNonValida" runat="server" OnClick="ButtonFatturaFisicaNonValida_Click"
                                    Text="Non valida" Width="150px" />
                                <asp:Button ID="ButtonFatturaFisicaDisassocia" runat="server" OnClick="ButtonFatturaFisicaDisassocia_Click"
                                    Text="Disassocia" Width="150px" />
                                <br />
                                <asp:Label ID="LabelFatturaTroppoVecchiaValidazione" runat="server" ForeColor="Red" Text="La fattura &#232; troppo antecedente la data della domanda. Per validarla lo stesso premere nuovamente il pulsante &quot;Valida&quot;."
                                                            Visible="False"></asp:Label>
                                <br />
                                Valida: la fattura � valida e correttamente associata alla dichiarazione del lavoratore.<br />
                                Non valida: la fattura � associata correttamente, ma non contiene importi validi
                                per nessuna prestazione.<br />
                                Disassocia: la fattura associata non riguarda la fattura dichiarata dal lavoratore,
                                ma pu� essere legata ad altre domande<br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="height: 18px">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelFattureFisicheRicevute" runat="server" Visible="false">
                    <uc3:PrestazioniRicercaFattureFisicheNonAssociate ID="PrestazioniRicercaFattureFisicheNonAssociate1"
                        runat="server" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelNuovaFatturaDichiarata" runat="server" Visible="false">
                    <uc5:PrestazioniDatiFattura ID="PrestazioniDatiFattura1" runat="server" />
                    <asp:Button ID="ButtonAggiungiNuovaFatturaDichirata" runat="server" Text="Aggiungi nuova fattura dichiarata"
                        OnClick="ButtonAggiungiNuovaFatturaDichirata_Click" />
                    <asp:Button ID="ButtonAggiungiNuovaFatturaDichirataAnnulla" runat="server" Text="Annulla"
                        OnClick="ButtonAggiungiNuovaFatturaDichirataAnnulla_Click" Width="153px" />
                    <br />
                    <asp:Label ID="LabelFatturaTroppoVecchia" runat="server" ForeColor="Red" Text="La fattura &#232; troppo antecedente la data della domanda. Per caricarla lo stesso premere nuovamente il pulsante &quot;Aggiungi nuova fattura dichiarata&quot;."
                            Visible="False"></asp:Label>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
