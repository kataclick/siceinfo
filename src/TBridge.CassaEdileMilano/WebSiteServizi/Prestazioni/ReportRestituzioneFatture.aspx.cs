﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Configuration;
using Microsoft.Reporting.WebForms;

public partial class Prestazioni_ReportRestituzioneFatture : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.PrestazioniGestioneDomande);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                Int32 idDomanda = (Int32) Context.Items["IdDomanda"];
                ViewState["IdDomanda"] = Context.Items["IdDomanda"];
                
                ReportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportRestituzioneFatture";

                ReportParameter[] listaParam = new ReportParameter[1];
                listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());
                
                ReportViewer.ServerReport.SetParameters(listaParam);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;
                byte[] bytes = null;

                //PDF
                bytes = ReportViewer.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=RestituzioneFatture {0}.pdf", idDomanda));
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
            else
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
        }
    }
}