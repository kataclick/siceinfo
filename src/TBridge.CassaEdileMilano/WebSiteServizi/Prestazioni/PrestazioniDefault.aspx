<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PrestazioniDefault.aspx.cs" Inherits="Prestazioni_PrestazioniDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuPrestazioni ID="MenuPrestazioni1" runat="server"></uc2:MenuPrestazioni>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Benvenuto nella sezione Richiesta prestazioni"
        titolo="Richiesta prestazioni" />
    <br />
    <p class="DefaultPage">
        Tramite questa funzione potrai compilare in modo veloce e semplice una domanda di
        prestazione.
    </p>
    <p class="DefaultPage">
        Dopo la conferma, ricordati di stampare il modulo e di firmarlo sul retro.
        <br />
        La domanda compilata e firmata deve essere consegnata <strong>IN ORIGINALE</strong> a Cassa Edile (per posta o
        a mano) provvista di tutta la documentazione specificata nel modulo.
    </p>
    <p class="DefaultPage">
        Ti ricordiamo che le seguenti domande di prestazione possono essere inserite solo nei periodi sotto specificati:<br /><br />
        <asp:Table ID="Table1" runat="server" Width="100%" CellPadding="4" CellSpacing="0" GridLines="Both" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
            <asp:TableHeaderRow Font-Bold="true" BorderStyle="Solid" BorderWidth="1" BorderColor="Black">
                <asp:TableCell Width="50%">Prestazione</asp:TableCell>
                <asp:TableCell Width="50%">Periodo di inserimento della domanda</asp:TableCell>
            </asp:TableHeaderRow>
            <asp:TableRow BorderStyle="Solid" BorderWidth="1" BorderColor="Black">
                <asp:TableCell>Contributo per spese didattiche di accesso dei figli alla scuola secondaria di 1� grado</asp:TableCell>
                <asp:TableCell>dal 1� settembre dell�anno scolastico di frequenza al 31 marzo dell�anno successivo all�inizio dell�anno scolastico</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow BorderStyle="Solid" BorderWidth="1" BorderColor="Black">
                <asp:TableCell>Borsa di studio per scuole secondarie di 2� grado o rimborso spese per attrezzature didattiche</asp:TableCell>
                <asp:TableCell>dal 30 giugno al 31 dicembre dell�anno in cui � terminato l�anno scolastico</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow BorderStyle="Solid" BorderWidth="1" BorderColor="Black">
                <asp:TableCell>Borsa di studio universitaria</asp:TableCell>
                <asp:TableCell>dal 1� ottobre dell�anno accademico di frequenza al 31 marzo dell�anno successivo al termine dell�anno accademico</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow BorderStyle="Solid" BorderWidth="1" BorderColor="Black">
                <asp:TableCell>Contributo per portatori di handicap</asp:TableCell>
                <asp:TableCell>Dal 1� gennaio al 30 giugno di ogni anno</asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </p>
    <p class="DefaultPage">
        Procedi selezionando la voce "Compilazione domanda" visualizzabile nel men� alla
        tua sinistra.
    </p>
    <p class="DefaultPage">
        Se desideri scaricare il manuale d'uso per l'utilizzo di questo servizio, <a href="http://ww2.cassaedilemilano.it/Portals/0/pdf/Prestazioni - Manuale utente Ver.3.00 del 01_09_16.pdf"
            target="_blank">CLICCA QUI!!</a>
    </p>
</asp:Content>
