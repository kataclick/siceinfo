using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class Prestazioni_GestioneDomandePersonali : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore,
                                              "~/Prestazioni/ControlloDomandePersonali.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            //TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore lavoratore =
            //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore)
            //        HttpContext.Current.User.Identity).Entity;
            Lavoratore lavoratore =
                (Lavoratore)
                GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            PrestazioniRicercaDomandeLavoratore1.ImpostaIdLavoratore(lavoratore.IdLavoratore);
        }
    }
}