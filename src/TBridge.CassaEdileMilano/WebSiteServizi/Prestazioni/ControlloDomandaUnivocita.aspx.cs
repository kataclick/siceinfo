using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Prestazioni.Type.Filters;

public partial class Prestazioni_ControlloDomandaUnivocita : Page
{
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande,
                                              "~/Prestazioni/ControlloDomanda.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            //Nel caso in cui il contesto non fosse presente, torniamo alla pagina di ricerca
            if (Context.Items["IdDomanda"] == null)
            {
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
            }

            int idDomanda = (int) Context.Items["IdDomanda"];
            Domanda domanda = biz.GetDomanda(idDomanda);
            ViewState["IdDomanda"] = domanda.IdDomanda.Value;
            ViewState["IdLavoratore"] = domanda.Lavoratore.IdLavoratore;
            ViewState["IdTipoPrestazione"] = domanda.IdTipoPrestazione;

            PrestazioniDatiDomanda1.CaricaDomanda(domanda);

            if (domanda.ControlloUnivocitaPrestazione.HasValue && domanda.ControlloUnivocitaPrestazione.Value)
                ButtonForza.Enabled = false;
            else
                ButtonForza.Enabled = true;

            CaricaDomande();
            CaricaPrestazioni();
            ControllaStatoDomanda(domanda);
        }
    }

    /// <summary>
    /// in funzione dello stato della domanda decidiamo quali azioni lasciare all'utente
    /// </summary>
    /// <param name="domanda"></param>
    private void ControllaStatoDomanda(Domanda domanda)
    {
        if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
        {
        }
        else
        {
            ButtonForza.Enabled = false;
        }
    }

    private void CaricaPrestazioni()
    {
        int idLavoratore = (int) ViewState["IdLavoratore"];

        PrestazioneErogataFilter filtro = new PrestazioneErogataFilter();
        filtro.IdLavoratore = idLavoratore;

        GridViewPrestazioniErogate.DataSource = biz.GetPrestazioniErogate(filtro);
        GridViewPrestazioniErogate.DataBind();
    }

    private void CaricaDomande()
    {
        int idLavoratore = (int) ViewState["IdLavoratore"];
        string idTipoPrestazione = (string) ViewState["IdTipoPrestazione"];

        DomandaFilter filtro = new DomandaFilter();
        filtro.IdLavoratore = idLavoratore;
        filtro.IdTipoPrestazione = idTipoPrestazione;

        DomandaCollection domande = biz.GetDomande(filtro);
        GridViewDomandePresentate.DataSource = domande;
        GridViewDomandePresentate.DataBind();
    }

    protected void GridViewDomandePresentate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewDomandePresentate.PageIndex = e.NewPageIndex;
        CaricaDomande();
    }

    protected void GridViewDomandePresentate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Domanda domanda = (Domanda) e.Row.DataItem;

            Label lBeneficiario = (Label) e.Row.FindControl("LabelBeneficiario");
            Label lStato = (Label) e.Row.FindControl("LabelStato");

            lStato.Text = domanda.Stato.Descrizione;

            if (domanda.Beneficiario != "L")
            {
                if (domanda.Familiare != null)
                {
                    lBeneficiario.Text = domanda.Familiare.NomeCompleto;
                }
                else
                {
                    if (domanda.FamiliareFornito != null)
                    {
                        lBeneficiario.Text = domanda.FamiliareFornito.NomeCompleto;
                    }
                }
            }
            else
            {
                lBeneficiario.Text = "LAVORATORE";
            }
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Context.Items["IdDomanda"] = (int) ViewState["IdDomanda"];
        Server.Transfer("~/Prestazioni/ControlloDomanda.aspx");
    }

    protected void ButtonForza_Click(object sender, EventArgs e)
    {
        int idDomanda = (int) ViewState["IdDomanda"];
        Domanda domanda = biz.GetDomanda(idDomanda);

        if (biz.ForzaUnivocita(idDomanda))
        {
            biz.ControllaDomandaUnivocita(domanda);
            ButtonForza.Enabled = false;
        }
    }

    protected void GridViewPrestazioniErogate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewPrestazioniErogate.PageIndex = e.NewPageIndex;
        CaricaPrestazioni();
    }

    protected void GridViewPrestazioniErogate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PrestazioneErogata prestazione = (PrestazioneErogata) e.Row.DataItem;

            Label lBeneficiario = (Label) e.Row.FindControl("LabelBeneficiario");

            if (prestazione.Familiare == null)
            {
                lBeneficiario.Text = "Lavoratore";
            }
            else
            {
                lBeneficiario.Text = prestazione.Familiare.NomeCompleto;
            }
        }
    }
}