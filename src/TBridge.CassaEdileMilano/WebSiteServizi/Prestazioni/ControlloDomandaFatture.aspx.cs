using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Delegates;
using TBridge.Cemi.Prestazioni.Type.Entities;
using Image=System.Web.UI.WebControls.Image;
using TBridge.Cemi.Business;

public partial class Prestazioni_ControlloDomandaFatture : Page
{
    private const int INDICEANNULLA = 2;
    private const int INDICEASSOCIA = 3;
    private const int INDICEVALIDA = 4;
    private const int INDICERIASSUNTO = 5;
    private const int INDICEVEDI = 6;

    private const int INDICEFISICAVALIDA = 4;
    private const int INDICEFISICAVEDI = 6;

    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();
    private Fattura currentFattura;
    private Domanda domanda = null;
    private int idDomanda = 0;
    private int idPrestazioneImportoFattura = 0;
    private int idPrestazioniFattura = 0;
    private int idPrestazioniFatturaDichiarata = 0;
    private string idTipoPrestazione = "";
    private Decimal importoFatture = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande,
                                              "~/Prestazioni/ControlloDomandaFatture.aspx");

        #endregion

        PrestazioniRicercaFattureFisicheNonAssociate1.OnFatturaSelected +=
            new FatturaSelectedEventHandler(PrestazioniRicercaFattureFisicheNonAssociate1_OnFatturaSelected);

        if (!Page.IsPostBack)
        {
            if(Context.Items["IdDomanda"] == null)
            {
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
            }

            idDomanda = (int) Context.Items["IdDomanda"];
            ViewState["idDomanda"] = idDomanda;

            if (idDomanda > 0)
            {
                Domanda domanda = biz.GetDomanda(idDomanda);

                ViewState["Configurazione"] = biz.GetConfigurazionePrestazione(domanda.IdTipoPrestazione, domanda.Beneficiario, domanda.Lavoratore.IdLavoratore.Value);

                //Verifichiamo se la domanda pu� essere gestita
                ViewState.Add("permettiGestioneDomanda", AbilitaGestione(domanda));

                ViewState.Add("idDomanda", idDomanda);
                caricaDatiBase();

                //TODO Da migliorare: purtroppo serve fare nuovamente una verifica della gestione perch� il caricadatibase modifica la visibilit� di alcune azioni disabilitate in precedenza da abilitagestione
                ViewState.Add("permettiGestioneDomanda", AbilitaGestione(domanda));
            }
        }
    }

    /// <summary>
    /// Abilita le azioni che pu� fare l'utente in funzione del ruolo e della propriet� della domanda 
    /// </summary>
    /// <param name="domanda">Domanda in gestione</param>
    private bool AbilitaGestione(Domanda domanda)
    {
        //IUtente utente = ApplicationInstance.GetUtenteSistema();
        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
        //Se l'utente � chi ha la domanda in carico o ha funzioni amministrative, pu� getire la domanda
        if (idUtente == domanda.IdUtenteInCarico ||
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin.ToString()))
        {
            //Tutto normale
            return true;
        }
        else
        {
            //Tutto viene disabilitato
            PanelImportiPrestazione.Enabled = false;
            ButtonAggiungiFatturaDichiarata.Enabled = false;
            return false;
        }
    }

    /// <summary>
    /// in funzione dello stato della domanda decidiamo quali azioni lasciare all'utente
    /// </summary>
    /// <param name="domanda"></param>
    private void ControllaStatoDomanda(Domanda domanda)
    {
        if (StatoDomanda.StatoDomandaModificabile(domanda.Stato.IdStato))
        {
            PanelImportiPrestazione.Enabled = true;
            ButtonAggiungiFatturaDichiarata.Enabled = true;
        }
        else
        {
            PanelImportiPrestazione.Enabled = false;
            ButtonAggiungiFatturaDichiarata.Enabled = false;
        }
    }

    private void caricaDatiBase(Domanda domanda)
    {
        ViewState.Add("idTipoPrestazione", domanda.IdTipoPrestazione);
        CaricaDomanda(domanda);

        PrestazioniDatiDomanda1.CaricaDomanda(domanda);
        CaricaImportiPrestazione(domanda.Importi);

        //appena ricaricati i dati facciamo i controlli sugli importi totale per gestire il semaforo generale
        ControllaTotali();

        ControllaStatoDomanda(domanda);
        ViewState["stato"] = domanda.Stato.IdStato;
    }

    private void caricaDatiBase()
    {
        //PanelFattureFisicheAssociate.Visible = false;
        //PanelFattureFisicheRicevute.Visible = false;
        //PanelFatturaImportiDettaglio.Visible = false;

        idDomanda = (int) ViewState["idDomanda"];

        if (idDomanda > 0)
        {
            domanda = biz.GetDomanda(idDomanda);
            ViewState.Add("idTipoPrestazione", domanda.IdTipoPrestazione);
            CaricaDomanda(domanda);

            PrestazioniDatiDomanda1.CaricaDomanda(domanda);
            CaricaImportiPrestazione(domanda.Importi);

            //appena ricaricati i dati facciamo i controlli sugli importi totale per gestire il semaforo generale
            ControllaTotali();

            ControllaStatoDomanda(domanda);
            ViewState["stato"] = domanda.Stato.IdStato;
        }
    }

    private void CaricaImportiPrestazione(PrestazioneImportoCollection importi)
    {
        if (importi == null || importi.Count == 0)
        {
            PanelImportiPrestazione.Visible = false;
        }
        else
        {
            PanelImportiPrestazione.Visible = true;

            Presenter.CaricaElementiInGridView(GridViewImportiPrestazione, importi, 0);
            LabelTotaleImporti.Text = importi.Totale().ToString("0.00");
        }
    }


    private void CaricaDomanda(Domanda domanda)
    {
        Presenter.CaricaElementiInGridView(GridViewFattureLavoratore, domanda.FattureDichiarate, 0);
    }

    protected void GridViewFattureLavoratore_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            FatturaDichiarata fattura = (FatturaDichiarata) e.Row.DataItem;
            Image iStato = (Image) e.Row.FindControl("ImageStato");
            Button bAnnulla = (Button) e.Row.FindControl("ButtonAnnulla");
            Button bRipristina = (Button) e.Row.FindControl("ButtonRipristina");
            Button bAssocia = (Button) e.Row.FindControl("ButtonAssocia");
            Button bDisassocia = (Button) e.Row.FindControl("ButtonDisassocia");

            Label lDData = (Label) e.Row.FindControl("LabelDData");
            Label lDNumero = (Label) e.Row.FindControl("LabelDNumero");
            Label lDImporto = (Label) e.Row.FindControl("LabelDImportoTotale");
            Label lDInseritaIl = (Label) e.Row.FindControl("LabelDInseritaIl");
            Label lDInseritaDa = (Label) e.Row.FindControl("LabelDInseritaDa");

            bAnnulla.CommandArgument = e.Row.RowIndex.ToString();
            bRipristina.CommandArgument = e.Row.RowIndex.ToString();
            bAssocia.CommandArgument = e.Row.RowIndex.ToString();
            bDisassocia.CommandArgument = e.Row.RowIndex.ToString();

            iStato.ImageUrl = biz.ConvertiBoolInSemaforo(null);
            lDData.Text = fattura.Data.ToString("dd/MM/yyyy");
            lDNumero.Text = fattura.Numero;
            lDImporto.Text = fattura.ImportoTotale.ToString("C");
            lDInseritaIl.Text = fattura.DataInserimentoRecord.ToString("dd/MM/yyyy");
            lDInseritaDa.Text = fattura.UtenteInserimento;

            e.Row.Cells[INDICEASSOCIA].Enabled = true;
            e.Row.Cells[INDICEANNULLA].Enabled = true;
            e.Row.Cells[INDICEVALIDA].Enabled = false;
            e.Row.Cells[INDICEVEDI].Enabled = false;

            //Controllo se ho gi� impostato la Fattura Fisica
            if (fattura.IdPrestazioniFattura.HasValue)
            {
                e.Row.Cells[INDICEANNULLA].Enabled = false;
                e.Row.Cells[INDICEVALIDA].Enabled = true;

                if (!string.IsNullOrEmpty(fattura.IdFatturaArchidoc))
                {
                    e.Row.Cells[INDICEVEDI].Enabled = true;
                }
                else
                {
                    e.Row.Cells[INDICEVEDI].Enabled = false;
                }

                bAssocia.Enabled = false;
                bDisassocia.Enabled = true;

                FatturaCollection f = biz.getFattureFisiche((int) fattura.IdPrestazioniFatturaDichiarata);
                if (f.Count > 0)
                {
                    Label lData = (Label) e.Row.FindControl("LabelData");
                    Label lNumero = (Label) e.Row.FindControl("LabelNumero");
                    Label lImporto = (Label) e.Row.FindControl("LabelImporto");
                    Label lValida = (Label) e.Row.FindControl("LabelValida");
                    Label lScansionata = (Label) e.Row.FindControl("LabelScansionata");

                    if (f[0].Data.HasValue)
                        lData.Text = f[0].Data.Value.ToShortDateString();
                    lNumero.Text = f[0].Numero;
                    lImporto.Text = f[0].ImportoTotale.ToString("C");

                    if (f[0].Valida.HasValue)
                    {
                        if (f[0].Valida.Value)
                            lValida.Text = "S�";
                        else lValida.Text = "No";
                    }
                    else
                        lValida.Text = "?";

                    if (f[0].DataScansione.HasValue)
                    {
                        lScansionata.Text = f[0].DataScansione.Value.ToString("dd/MM/yyyy");
                    }

                    iStato.ImageUrl = biz.ConvertiBoolInSemaforo(f[0].Valida);
                }
                FatturaImportoCollection fc = biz.getFattureFisicheImporti((int) fattura.IdPrestazioniFattura);

                //Gli importi vanno sommati solo per le fatture valide
                if (f.Count > 0 && f[0].Valida.HasValue && f[0].Valida.Value)
                    importoFatture += fc.TotaleFattureConImportoTotale();

                GridView gvImporti = (GridView) e.Row.FindControl("GridViewImporti");
                gvImporti.DataSource = fc;
                gvImporti.DataBind();
            }
            else
            {
                e.Row.Cells[INDICEANNULLA].Enabled = true;
                e.Row.Cells[INDICEASSOCIA].Enabled = true;
                e.Row.Cells[INDICEVALIDA].Enabled = false;
                e.Row.Cells[INDICEVEDI].Enabled = false;

                bAssocia.Enabled = true;
                bDisassocia.Enabled = false;
            }


            //Controllo se ho annullato la fattura Dichiarata
            if (fattura.DataAnnullamento.HasValue)
            {
                e.Row.ForeColor = Color.LightGray;
                bAnnulla.Enabled = false;
                bRipristina.Enabled = true;
                e.Row.Cells[INDICEASSOCIA].Enabled = false;
            }
            else
            {
                bAnnulla.Enabled = true;
                bRipristina.Enabled = false;
            }

            //se la domanda � gi� stata accolta o l'utente che sta visualizzando la pagina non ha in carico la domanda 
            //permettiamo solo la visualizzazione
            //Partiamo dal risultato degli stati dei bottoni precedente ed applichiamo altre regole pi� restrittive
            if (StatoDomanda.StatoDomandaModificabile(domanda.Stato.IdStato)
                && (bool) ViewState["permettiGestioneDomanda"])
                //(string)ViewState["stato"] == "I" || (string)ViewState["stato"] == "T" || (string)ViewState["stato"] == "O")
            {
                //Se la domanda non � in uno stato definito lasciamo tutto com'�
            }
            else
            {
                e.Row.Cells[INDICEANNULLA].Enabled = false;
                e.Row.Cells[INDICEASSOCIA].Enabled = false;
                e.Row.Cells[INDICEVALIDA].Enabled = false;
                //il VEDI rimane condizionato dalla condizione dell'idarchido della fattura
            }
        }
    }

    protected void GridViewFattureLavoratore_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = 0;
        Int32.TryParse(e.CommandArgument.ToString(), out rowIndex);
        if (Page.IsPostBack)
        {
            if (true)
            {
                switch (e.CommandName)
                {
                    case "FatturaValida":
                        {
                            idPrestazioniFattura =
                                (int) GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFattura"];
                            idPrestazioniFatturaDichiarata =
                                (int)
                                GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFatturaDichiarata"];

                            caricaDatiBase();

                            GridViewFattureLavoratore.Rows[rowIndex].BackColor = Color.Yellow;

                            PanelFattureFisicheAssociate.Visible = true;

                            FatturaCollection f = biz.getFattureFisiche(idPrestazioniFatturaDichiarata);

                            if (f.Count > 0)
                            {
                                if (f[0].Data.HasValue)
                                {
                                    LabelFatturaFisicaData.Text = f[0].Data.Value.ToShortDateString();
                                }
                                else
                                {
                                    LabelFatturaFisicaData.Text = String.Empty;
                                }
                                LabelFatturaFisicaNumero.Text = f[0].Numero.ToString();
                                LabelFatturaFisicaImporto.Text = f[0].ImportoTotale.ToString("0.00");

                                if (f[0].Originale)
                                    LabelOriginale.Text = "S�";
                                else
                                    LabelOriginale.Text = "No";

                                ImageStatoValidazione.ImageUrl = biz.ConvertiBoolInSemaforo(f[0].Valida);

                                currentFattura = f[0];
                                ViewState.Add("CurrentFattura", currentFattura);
                                ViewState.Add("idPrestazioniFatturaDichiarata", idPrestazioniFatturaDichiarata);
                                ViewState.Add("idPrestazioniFattura", idPrestazioniFattura);

                                GridViewImportiFattura.DataSource = biz.getFattureFisicheImporti(idPrestazioniFattura);
                                GridViewImportiFattura.DataBind();
                            }


                            break;
                        }
                    case "FatturaAnnulla":
                        {
                            int idPrestazioniFatturaDichiarata =
                                (int)
                                GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFatturaDichiarata"];
                            biz.AnnullaFatturaDichiarata(idPrestazioniFatturaDichiarata);

                            caricaDatiBase();
                            biz.ControllaDomandaFatture(domanda);

                            //Notifichiamo al CRM un cambio nella documentazione (in questo caso annullamento di una fattura)
                            //le eccezioni del WS non vengono gestire a questo livello
                            if (!Common.Sviluppo)
                            {
                                biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                            }
                            break;
                        }
                    case "FatturaRipristina":
                        {
                            int idPrestazioniFatturaDichiarata =
                                (int)
                                GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFatturaDichiarata"];
                            biz.RipristinaAnnullamentoFatturaDichiarata(idPrestazioniFatturaDichiarata);

                            caricaDatiBase();
                            biz.ControllaDomandaFatture(domanda);

                            //Notifichiamo al CRM un cambio nella documentazione (in questo caso fattura nulla torna valida)
                            //le eccezioni del WS non vengono gestire a questo livello
                            if (!Common.Sviluppo)
                            {
                                biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                            }
                            break;
                        }
                    case "FatturaAssocia":
                        {
                            int idPrestazioniFatturaDichiarata =
                                (int)
                                GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFatturaDichiarata"];
                            ViewState.Add("idPrestazioniFatturaDichiarata", idPrestazioniFatturaDichiarata);

                            caricaDatiBase();

                            GridViewFattureLavoratore.Rows[rowIndex].BackColor = Color.Yellow;

                            PanelFattureFisicheAssociate.Visible = false;
                            PanelFattureFisicheRicevute.Visible = true;
                            break;
                        }
                    case "FatturaDisassocia":
                        {
                            int idPrestazioniFatturaDichiarata =
                                (int)
                                GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFatturaDichiarata"];

                            biz.fatturaDichiarataDisassociaFatturaFisica(idPrestazioniFatturaDichiarata);
                            PanelFattureFisicheAssociate.Visible = false;

                            caricaDatiBase();
                            biz.ControllaDomandaFatture(domanda);

                            //Notifichiamo al CRM un cambio nella documentazione (in questo caso disassociamento di una fattura)
                            //le eccezioni del WS non vengono gestire a questo livello
                            if (!Common.Sviluppo)
                            {
                                biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                            }
                            break;
                        }
                    case "FatturaVedi":
                        {
                            int idPrestazioniFatturaDichiarata =
                                (int)
                                GridViewFattureLavoratore.DataKeys[rowIndex].Values["idPrestazioniFatturaDichiarata"];
                            FatturaCollection f = biz.getFattureFisiche(idPrestazioniFatturaDichiarata);

                            string idArchidoc = string.Empty;
                            if (f.Count > 0)
                            {
                                idArchidoc = f[0].IdArchidoc;
                            }

                            // Caricare il documento tramite il Web Service Archidoc
                            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                            byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                            if (file != null)
                            {
                                Presenter.RestituisciFileArchidoc(idArchidoc, file, Page);
                            }
                            break;
                        }
                }
            }

            Configurazione configurazione = (Configurazione) ViewState["Configurazione"];
            AggiornaDataRiferimento(configurazione, domanda);
        }
    }

    /// <summary>
    /// Metodo legato all'evento OnFatturaSelected del controllo PrestazioniRicercaFattureFisicheNonAssociate 
    /// L'evento viene lanciato quando l'utente ha richiesto l'associazione di una fattura
    /// </summary>
    /// <param name="idFattura"></param>
    private void PrestazioniRicercaFattureFisicheNonAssociate1_OnFatturaSelected(int idFattura)
    {
        idPrestazioniFattura = idFattura;
        idPrestazioniFatturaDichiarata = (int) ViewState["idPrestazioniFatturaDichiarata"];
        if (biz.fatturaDichiarataAssociaFatturaFisica(idPrestazioniFattura, idPrestazioniFatturaDichiarata))
        {
            idTipoPrestazione = (string) ViewState["idTipoPrestazione"];
            biz.fattureImportiInsertImportiDefault(idTipoPrestazione, idPrestazioniFattura);
            caricaDatiBase();

            biz.ControllaDomandaFatture(domanda);

            //Notifichiamo al CRM un cambio nella documentazione, in questo caso l'associazione di una fattura (quindi un documento mancante in meno)
            //le eccezioni del WS non vengono gestire a questo livello
            if (!Common.Sviluppo)
            {
                biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
            }
        }

        PrestazioniRicercaFattureFisicheNonAssociate1.Reset();
        PanelFattureFisicheRicevute.Visible = false;
    }

    protected void GridViewImportiFattura_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Page.Validate("modificaValore");

        int rowIndex = 0;
        Int32.TryParse(e.CommandArgument.ToString(), out rowIndex);
        if (Page.IsPostBack)
        {
            if (true)
            {
                switch (e.CommandName)
                {
                    case "FatturaImportoEdit":
                        if (Page.IsValid)
                        {
                            idPrestazioneImportoFattura =
                                (int) GridViewImportiFattura.DataKeys[rowIndex].Values["idPrestazioneImportoFattura"];

                            TextBox tbImporto =
                                (TextBox) GridViewImportiFattura.Rows[rowIndex].FindControl("TextBoxValore");
                            decimal importo = decimal.Parse(tbImporto.Text);

                            idPrestazioniFattura = (int) ViewState["idPrestazioniFattura"];
                            if ((importo >= 0) && (idPrestazioneImportoFattura > 0))
                            {
                                biz.fattureImportiUpdatePerIdPrestazioneImportoFattura(idPrestazioneImportoFattura,
                                                                                       importo);

                                GridViewImportiFattura.DataSource = biz.getFattureFisicheImporti(idPrestazioniFattura);
                                GridViewImportiFattura.DataBind();

                                caricaDatiBase();
                            }
                        }
                        break;
                }
            }
        }
    }

    protected void ButtonFatturaFisicaValida_Click(object sender, EventArgs e)
    {
        ValidaFattura(true);
        //currentFattura  = (Fattura)ViewState["CurrentFattura"];

        //biz.PrestazioniFatturaAggiornaValidazione((int)currentFattura.IdPrestazioniFattura, true);
        ////ButtonFatturaFisicaValida.Enabled = false;
        ////ImageStatoValidazione.ImageUrl = biz.ConvertiBoolInSemaforo(currentFattura.Valida);
        //PanelFattureFisicheAssociate.Visible = false;

        //caricaDatiBase();
        //biz.ControllaDomandaFatture(domanda);
    }

    private void ValidaFattura(bool stato)
    {
        currentFattura = (Fattura) ViewState["CurrentFattura"];
        Int32 idFatturaDichiarata = (Int32)ViewState["idPrestazioniFatturaDichiarata"];
        Int32 idDomanda = (Int32)ViewState["idDomanda"];
        Domanda domanda = biz.GetDomanda(idDomanda);
        Configurazione configurazione = biz.GetConfigurazionePrestazione(domanda.IdTipoPrestazione, domanda.Beneficiario, domanda.Lavoratore.IdLavoratore.Value);
        FatturaDichiarata fatturaDichiarata = null;

        foreach (FatturaDichiarata fd in domanda.FattureDichiarate)
        {
            if (fd.IdPrestazioniFatturaDichiarata.Value == idFatturaDichiarata)
            {
                fatturaDichiarata = fd;
                break;
            }
        }

        if (fatturaDichiarata != null)
        {
            if (stato && fatturaDichiarata.Data < domanda.DataDomanda.Value.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda)
                            && !LabelFatturaTroppoVecchiaValidazione.Visible)
            {
                LabelFatturaTroppoVecchiaValidazione.Visible = true;
            }
            else
            {
                LabelFatturaTroppoVecchiaValidazione.Visible = false;

                biz.PrestazioniFatturaAggiornaValidazione((int)currentFattura.IdPrestazioniFattura, stato);
                //ButtonFatturaFisicaValida.Enabled = false;
                //ImageStatoValidazione.ImageUrl = biz.ConvertiBoolInSemaforo(currentFattura.Valida);
                PanelFattureFisicheAssociate.Visible = false;

                caricaDatiBase();
                biz.ControllaDomandaFatture(domanda);
            }
        }
    }

    protected void ButtonFatturaFisicaNonValida_Click(object sender, EventArgs e)
    {
        ValidaFattura(false);
    }

    protected void GridViewImportiFattura_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            FatturaImporto importo = (FatturaImporto) e.Row.DataItem;
            TextBox tbImporto = (TextBox) e.Row.FindControl("TextBoxValore");

            tbImporto.Text = importo.Valore.ToString("0.00");
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Context.Items["IdDomanda"] = ViewState["idDomanda"];

        Server.Transfer("~/Prestazioni/ControlloDomanda.aspx");
    }

    protected void GridViewImportiPrestazione_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            PrestazioneImporto importo = (PrestazioneImporto) e.Row.DataItem;
            TextBox tbImporto = (TextBox) e.Row.FindControl("TextBoxValoreImporto");
            Image iStato = (Image) e.Row.FindControl("ImageStato");
            Button bSalva = (Button) e.Row.FindControl("ButtonSalvaImporto");

            tbImporto.Text = importo.Valore.ToString("0.00");
            iStato.ImageUrl = biz.ConvertiBoolInSemaforo(importo.IdImporto.HasValue);

            #region Per prevenire click multipli

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('modificaValoreImportoPrestazione') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Salva';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(bSalva, null));
            sb.Append(";");
            sb.Append("return true;");
            bSalva.Attributes.Add("onclick", sb.ToString());

            #endregion
        }
    }

    protected void GridViewImportiPrestazione_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Page.Validate("modificaValoreImportoPrestazione");

        if (Page.IsValid)
        {
            TextBox tbValore =
                (TextBox) GridViewImportiPrestazione.Rows[e.NewSelectedIndex].FindControl("TextBoxValoreImporto");
            PrestazioneImporto importo = new PrestazioneImporto();

            importo.IdTipoImporto =
                (Int32) GridViewImportiPrestazione.DataKeys[e.NewSelectedIndex].Values["IdTipoImporto"];
            importo.IdDomanda = (Int32) GridViewImportiPrestazione.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];
            importo.IdImporto = (Int32?) GridViewImportiPrestazione.DataKeys[e.NewSelectedIndex].Values["IdImporto"];
            importo.Valore = Decimal.Parse(tbValore.Text);

            if (biz.SalvaImportoPrestazione(importo))
            {
                Int32 idDomanda = (Int32) ViewState["idDomanda"];
                Domanda domanda = biz.GetDomanda(idDomanda);
                biz.ControllaDomandaFatture(domanda);

                caricaDatiBase();
                ControllaTotali();
            }
        }
    }

    protected void GridViewFattureLavoratore_DataBound(object sender, EventArgs e)
    {
        LabelTotaleFatture.Text = importoFatture.ToString("0.00");
        ControllaTotali();
    }

    private void ControllaTotali()
    {
        decimal totaleFatture;
        Decimal.TryParse(LabelTotaleFatture.Text, out totaleFatture);
        decimal totaleImporti;
        Decimal.TryParse(LabelTotaleImporti.Text, out totaleImporti);

        ImageStatoTotale.ImageUrl = biz.ConvertiBoolInSemaforo(totaleFatture > 0 && totaleFatture == totaleImporti);
            //LabelTotaleFatture.Text == LabelTotaleImporti.Text);
    }


    protected void ButtonFatturaFisicaDisassocia_Click(object sender, EventArgs e)
    {
        LabelFatturaTroppoVecchiaValidazione.Visible = false;
        int idPrestazioniFatturaDichiarata = (int) ViewState["idPrestazioniFatturaDichiarata"];

        biz.fatturaDichiarataDisassociaFatturaFisica(idPrestazioniFatturaDichiarata);
        PanelFattureFisicheAssociate.Visible = false;

        caricaDatiBase();
        biz.ControllaDomandaFatture(domanda);

        //Notifichiamo al CRM un cambio nella documentazione (in questo caso disassociamento di una fattura)
        //le eccezioni del WS non vengono gestire a questo livello
        if (!Common.Sviluppo)
        {
            biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
        }
    }

    protected void ButtonAggiungiNuovaFatturaDichirata_Click(object sender, EventArgs e)
    {
        FatturaDichiarata fatturaDichiarata = PrestazioniDatiFattura1.CreaFattura();
        idDomanda = (int) ViewState["idDomanda"];
        Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
        domanda = biz.GetDomanda(idDomanda);

        if (fatturaDichiarata.Data < domanda.DataDomanda.Value.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda)
                        && !LabelFatturaTroppoVecchia.Visible)
        {
            LabelFatturaTroppoVecchia.Visible = true;
        }
        else
        {
            //Impstiamo che questa fattura dichiarata � stata aggiunta lato operatore e non dal lavoratore.
            fatturaDichiarata.AggiuntaOperatore = true;

            biz.AggiungiFatturaDichiarata(fatturaDichiarata, idDomanda);
            PanelNuovaFatturaDichiarata.Visible = false;

            domanda = biz.GetDomanda(idDomanda);
            AggiornaDataRiferimento(configurazione, domanda);

            caricaDatiBase();
            biz.ControllaDomandaFatture(domanda);

            //Notifichiamo al CRM un cambio nella documentazione (in questo caso aggiunta di una nuova fattura da richiedere)
            //le eccezioni del WS non vengono gestire a questo livello
            if (!Common.Sviluppo)
            {
                biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
            }

            LabelFatturaTroppoVecchia.Visible = false;
        }
    }

    protected void ButtonAggiungiNuovaFatturaDichirataAnnulla_Click(object sender, EventArgs e)
    {
        PrestazioniDatiFattura1.ResetCampi();
        PanelNuovaFatturaDichiarata.Visible = false;
    }

    protected void ButtonAggiungiFatturaDichiarata_Click(object sender, EventArgs e)
    {
        PanelNuovaFatturaDichiarata.Visible = true;
        PrestazioniDatiFattura1.ResetCampi();
        PrestazioniDatiFattura1.ImpostaValidatorPerDataFattura(24);
    }

    protected void AggiornaDataRiferimento(Configurazione configurazione, Domanda domanda)
    {
        if (configurazione != null && configurazione.RichiestaFattura)
        {
            if (domanda.FattureDichiarate.Count > 0)
            {
                DateTime? dataRifFattura = domanda.DataDomanda.Value;

                //Nel caso di asilo nido prendiamo l'ultima data fattura
                if (domanda.TipoPrestazione.IdTipoPrestazione == "C-ANID")
                {
                    //siamo sicuri che almeno una fattura � valorizzata quindi possiamo prendere il valore del tipo nullable senza controlli
                    if (domanda.FattureDichiarate.MaxDataFattura.HasValue)
                    {
                        dataRifFattura = domanda.FattureDichiarate.MaxDataFattura;
                    }

                }
                // per tutte le altre prendiamo la data fattura minima
                else
                {
                    if (domanda.FattureDichiarate.MinDataFattura.HasValue)
                    {
                        dataRifFattura = domanda.FattureDichiarate.MinDataFattura;
                    }
                }

                biz.UpdateDomandaDataRiferimento(idDomanda, dataRifFattura.Value);
            }
            else
            {
                biz.UpdateDomandaDataRiferimento(idDomanda, domanda.DataDomanda.Value);
            }
        }
    }
}