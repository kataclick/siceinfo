<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlloDomandaScolastiche.aspx.cs" Inherits="Prestazioni_ControlloDomandaScolastiche" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/PrestazioniRicercaFamiliare.ascx" TagName="PrestazioniRicercaFamiliare"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda - Scolastiche"
        titolo="Prestazioni" />
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 200px">
                Tipo prestazione
            </td>
            <td>
                <asp:Label ID="LabelTipoPrestazione" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Beneficiario
            </td>
            <td>
                <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <asp:Panel ID="PanelAzioni" runat="server">
                    <asp:Panel ID="PanelC003123" runat="server" Visible="false">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    Diritto alle detrazioni
                                </td>
                                <td>
                                    <asp:CheckBox
                                        ID="CheckBoxC003123RedditoSi"
                                        runat="server"
                                        Enabled="false"
                                        Text="S�" />
                                    <asp:CheckBox
                                        ID="CheckBoxC003123RedditoNo"
                                        runat="server"
                                        Enabled="false"
                                        Text="No" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelC0031" runat="server" Visible="false">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    Tipo scuola:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListTipoScuolaC0031" runat="server" AutoPostBack="True"
                                        OnSelectedIndexChanged="DropDownListTipoScuolaC0031_SelectedIndexChanged" Width="350px"
                                        AppendDataBoundItems="true">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoScuolaC0031" runat="server"
                                        ControlToValidate="DropDownListTipoScuolaC0031" ErrorMessage="Selezionare un tipo scuola"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Tipo promozione:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListTipoPromozioneC0031" runat="server" Width="350px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoPromozioneC0031" runat="server"
                                        ControlToValidate="DropDownListTipoPromozioneC0031" ErrorMessage="Selezionare un tipo promozione"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Anno frequenza (aaaa):
                                    <br />
                                    (estremo destro)
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnnoFrequenzaC0031" runat="server" MaxLength="4" Width="250px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnnoFrequenzaC0031" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC0031" ErrorMessage="Indicare l'anno di frequenza"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaC0031Maggiore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC0031" ErrorMessage="Anno frequenza non valido"
                                        Operator="GreaterThanEqual" Type="Integer" ValidationGroup="salvataggio" ValueToCompare="2005">*</asp:CompareValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaC0031Minore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC0031" ErrorMessage="Anno frequenza non valido"
                                        Operator="LessThanEqual" Type="Integer" ValidationGroup="salvataggio" ValueToCompare="2100">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Classe conclusa:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxClasseConclusaC0031" runat="server" MaxLength="1" Width="250px"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorClasseConclusaC0031" runat="server"
                                        ControlToValidate="TextBoxClasseConclusaC0031" ErrorMessage="Indicare la classe conclusa"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorClasseConclusaC0031" runat="server" ControlToValidate="TextBoxClasseConclusaC0031"
                                        ErrorMessage="Formato della classe conclusa non valido" Operator="DataTypeCheck"
                                        Type="Integer" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Applica deduzione:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListApplicaDeduzioneC0031" runat="server" Width="50px">
                                        <asp:ListItem Value="N">No</asp:ListItem>
                                        <asp:ListItem Value="S" Selected="True">Si</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelC006" runat="server" Visible="false">
                        <table class="standardTable">
                            <tr>
                                <td>
                                    Anno scolastico comunicato
                                </td>
                                <td>
                                    <b>
                                    <asp:Label
                                        ID="LabelAnnoScolasticoC006"
                                        runat="server">
                                    </asp:Label>
                                    </b>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Denominazione istituto
                                </td>
                                <td>
                                    <asp:Label
                                        ID="LabelIstitutoDenominazioneC006"
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Indirizzo istituto
                                </td>
                                <td>
                                    <asp:Label
                                        ID="LabelIstitutoIndirizzoC006"
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Localit� istituto
                                </td>
                                <td>
                                    <asp:Label
                                        ID="LabelIstitutoLocalitaC006"
                                        runat="server">
                                    </asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Anno frequenza (aaaa):
                                    <br />
                                    (estremo destro)
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnnoFrequenzaC006" runat="server" Width="250px" MaxLength="4"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnnoFrequenzaC006" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC006" ErrorMessage="Indicare l'anno di frequenza"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaC006Maggiore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC006" ValueToCompare="2005" Operator="GreaterThanEqual"
                                        Type="Integer" ErrorMessage="Anno non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaC006Minore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC006" ValueToCompare="2100" Operator="LessThanEqual"
                                        Type="Integer" ErrorMessage="Anno frequenza non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelC00323" runat="server" Visible="false">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    Anno frequenza (aaaa):
                                    <br />
                                    (estremo destro)
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnnoFrequenzaC00323" runat="server" Width="250px" MaxLength="4"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnnoFrequenzaC00323" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC00323" ErrorMessage="Indicare l'anno di frequenza"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaC00323Maggiore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC00323" ValueToCompare="2005" Operator="GreaterThanEqual"
                                        Type="Integer" ErrorMessage="Anno frequenza non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaC00323Minore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaC00323" ValueToCompare="2100" Operator="LessThanEqual"
                                        Type="Integer" ErrorMessage="Anno frequenza non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Anno immatricolazione (aaaa):
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnnoImmatricolazioneC00323" runat="server" Width="250px"
                                        MaxLength="4"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnnoImmatricolazioneC00323"
                                        runat="server" ControlToValidate="TextBoxAnnoImmatricolazioneC00323" ErrorMessage="Indicare l'anno di immatricolazione"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoImmatricolazioneC00323Maggiore" runat="server"
                                        ControlToValidate="TextBoxAnnoImmatricolazioneC00323" ValueToCompare="1900" Operator="GreaterThanEqual"
                                        Type="Integer" ErrorMessage="Anno immatricolazione non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoImmatricolazioneC00323Minore" runat="server"
                                        ControlToValidate="TextBoxAnnoImmatricolazioneC00323" ValueToCompare="2100" Operator="LessThanEqual"
                                        Type="Integer" ErrorMessage="Anno immatricolazione non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Numero anni frequentati:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnniFrequentatiC00323" runat="server" Width="250px" MaxLength="2"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnniFrequentatiC00323" runat="server"
                                        ControlToValidate="TextBoxAnniFrequentatiC00323" ErrorMessage="Indicare il numero di anni frequentati"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnniFrequentatiC00323" runat="server" ControlToValidate="TextBoxAnniFrequentatiC00323"
                                        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Formato numero di anni frequentati non valido"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Numero anni fuori corso:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnniFuoriCorsoC00323" runat="server" Width="250px" MaxLength="2"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnniFuoriCorsoC00323" runat="server"
                                        ControlToValidate="TextBoxAnniFuoriCorsoC00323" ErrorMessage="Indicare il numero di anni fuori corso"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnniFuoriCorsoC00323" runat="server" ControlToValidate="TextBoxAnniFuoriCorsoC00323"
                                        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Formato numero di anni fuori corso non valido"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Somma voti conseguiti:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxMediaVotoC00323" runat="server" Width="250px" MaxLength="5"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorMediaVotoC00323" runat="server"
                                        ControlToValidate="TextBoxMediaVotoC00323" ErrorMessage="Indicare la media voto"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorMediaVotoC00323" runat="server" ControlToValidate="TextBoxMediaVotoC00323"
                                        Operator="DataTypeCheck" Type="Double" ErrorMessage="Formato della media voto non valido"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Numero esami sostenuti:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxEsamiSostenutiC00323" runat="server" Width="250px" MaxLength="2"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEsamiSostenutiC00323" runat="server"
                                        ControlToValidate="TextBoxEsamiSostenutiC00323" ErrorMessage="Indicare il numero di esami sostenuti"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxEsamiSostenutiC00323"
                                        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Formato numero esami sostenuti non valido"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    CFU previsti all&#39;anno di corso:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxCFUPrevistiC00323" runat="server" Width="250px" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCFUPrevistiC00323" runat="server"
                                        ControlToValidate="TextBoxCFUPrevistiC00323" ErrorMessage="Indicare il numero di CFU previsti"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorCFUPrevistiC00323" runat="server" ControlToValidate="TextBoxCFUPrevistiC00323"
                                        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Formato numero CFU previsti non valido"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    CFU conseguiti:
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxCFUConseguitiC00323" runat="server" Width="250px" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCFUConseguitiC00323" runat="server"
                                        ControlToValidate="TextBoxCFUConseguitiC00323" ErrorMessage="Indicare il numero di CFU conseguiti"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorCFUConseguitiC00323" runat="server" ControlToValidate="TextBoxCFUConseguitiC00323"
                                        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Formato numero CFU conseguiti non valido"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Applica deduzione:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListApplicaDeduzioneC00323" runat="server" Width="50px">
                                        <asp:ListItem Value="N">No</asp:ListItem>
                                        <asp:ListItem Value="S" Selected="True">Si</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelCANID" runat="server" Visible="false">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    Anno frequenza (aaaa):
                                    <br />
                                    (estremo destro)
                                </td>
                                <td>
                                    <asp:Image ID="ImageControlloAnnoFrequenza" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                    <asp:Button ID="ButtonForzaAnnoFrequenza" runat="server" 
                                        Text="Forza" Enabled="false" onclick="ButtonForzaAnnoFrequenza_Click" />
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnnoFrequenzaCANID" runat="server" Width="250px" MaxLength="4"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnnoFrequenzaCANID" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaCANID" ErrorMessage="Indicare l'anno di frequenza"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaCANIDMaggiore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaCANID" ValueToCompare="2005" Operator="GreaterThanEqual"
                                        ErrorMessage="Anno non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                    <asp:CompareValidator ID="CompareValidatorAnnoFrequenzaCANIDMinore" runat="server"
                                        ControlToValidate="TextBoxAnnoFrequenzaCANID" ValueToCompare="2100" Operator="LessThanEqual"
                                        ErrorMessage="Anno non valido" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Numero mesi frequentati:
                                </td>
                                <td>
                                    <asp:Image ID="ImageControlloMesiFrequentati" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxNumeroMesiFrequentatiCANID" runat="server" Width="250px"
                                        MaxLength="2"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNumeroMesiFrequentatiCANID"
                                        runat="server" ControlToValidate="TextBoxNumeroMesiFrequentatiCANID" ErrorMessage="Indicare il numero di mesi frequentati"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidatorNumeroMesiFrequentatiCANID" runat="server"
                                        ControlToValidate="TextBoxNumeroMesiFrequentatiCANID" Operator="DataTypeCheck"
                                        Type="Integer" ErrorMessage="Il valore deve essere numerico" ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Periodo di frequenza:
                                </td>
                                <td>
                                    <small>180gg da domanda</small>
                                    <br />
                                    <asp:Image ID="ImageControlloPeriodoFrequenza180Giorni" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                    <asp:Button ID="ButtonForzaPeriodoFrequenza180Giorni" runat="server" 
                                        Text="Forza" Enabled="false" 
                                        onclick="ButtonForzaPeriodoFrequenza180Giorni_Click" />
                                    <br />
                                    <small>Periodo nei 3 anni</small>
                                    <br />
                                    <asp:Image ID="ImageControlloPeriodoFrequenza3Anni" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                    <asp:Button ID="ButtonForzaPeriodoFrequenza3Anni" runat="server" Text="Forza" 
                                        Enabled="false" onclick="ButtonForzaPeriodoFrequenza3Anni_Click" />
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                Dal(mm/aaaa):
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxPeriodoDal" runat="server" Width="60px" MaxLength="7"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Al(mm/aaaa):
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBoxPeriodoAl" runat="server" Width="60px" MaxLength="7"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPeriodoDal"
                                        runat="server" ControlToValidate="TextBoxPeriodoDal" ErrorMessage="Indicare l'inizio del periodo di frequenza"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPeriodoDal" runat="server"
                                            ErrorMessage="Formato dell'inizio del periodo di frequenza non valido" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="salvataggio"
                                            ControlToValidate="TextBoxPeriodoDal">*</asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPeriodoAl"
                                        runat="server" ControlToValidate="TextBoxPeriodoAl" ErrorMessage="Indicare la fine del periodo di frequenza"
                                        ValidationGroup="salvataggio">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPeriodoAl" runat="server"
                                            ErrorMessage="Formato della fine del periodo di frequenza non valido" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="salvataggio"
                                            ControlToValidate="TextBoxPeriodoAl">*</asp:RegularExpressionValidator>
                                    <asp:CustomValidator ID="CustomValidatorPeriodo" runat="server" ErrorMessage="L'inizio del periodo di frequenza deve essere precedente alla fine"
                                        ValidationGroup="salvataggio" 
                                        onservervalidate="CustomValidatorPeriodo_ServerValidate">*</asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelC014" runat="server" Visible="False">
                        <table class="standardTable">
                            <tr>
                                <td style="width: 200px">
                                    Anno da erogare (aaaa):
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAnnoDaErogare" runat="server" Width="250px" MaxLength="4"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator
                                        ID="RequiredFieldValidatorAnnoDaErogareC014"
                                        runat="server"
                                        ControlToValidate="TextBoxAnnoDaErogare"
                                        ErrorMessage="Indicare l'anno da erogare"
                                        ValidationGroup="salvataggio">
                                        *
                                    </asp:RequiredFieldValidator>
                                    <asp:RangeValidator
                                        ID="CompareValidatorAnnoDaErogareC014"
                                        runat="server"
                                        ControlToValidate="TextBoxAnnoDaErogare"
                                        ErrorMessage="Anno da erogare non valido"
                                        ValidationGroup="salvataggio"
                                        Type="Integer"
                                        MinimumValue="2008"
                                        MaximumValue="2020">
                                        *
                                    </asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Tipo erogazione:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListTipoErogazione" runat="server" Height="18px" Width="248px">
                                        <asp:ListItem Value="1">Totale</asp:ListItem>
                                        <asp:ListItem Value="2">Parziale</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelC002" runat="server" Visible="False">
                        <table class="standardTable">
                            <tr runat="server" id="trFamiliareErede">
                                <td style="width: 200px">
                                    Familiare che eredita:
                                </td>
                                <td>
                                    <asp:Label ID="LabelFamiliareErede" runat="server"></asp:Label>
                                    <br />
                                    Per selezionare l&#39;erede premere:
                                    <asp:Button ID="ButtonSelezionaFamiliareErede" runat="server" Text="Seleziona erede"
                                        OnClick="ButtonSelezionaFamiliareErede_Click" />
                                    <asp:Panel ID="PanelFamiliareErede" runat="server" Visible="false">
                                        <uc3:PrestazioniRicercaFamiliare ID="PrestazioniRicercaFamiliareErede" runat="server" />
                                    </asp:Panel>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Data decesso (dd/mm/aaaa):
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxDataDecesso" runat="server" Width="126px" MaxLength="10"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:CompareValidator ID="CompareValidatorDataDecesso" runat="server" ControlToValidate="TextBoxDataDecesso"
                                        ErrorMessage="La data decesso non � corretta" Operator="DataTypeCheck" Type="Date"
                                        ValidationGroup="salvataggio">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Causa del decesso:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListTipoDecesso" runat="server" Width="210px">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 200px">
                                    Applica deduzione:
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListApplicaDeduzioneC002" runat="server" Width="50px">
                                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                                        <asp:ListItem Value="S">Si</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:BulletedList ID="BulletedListControlli" runat="server">
                </asp:BulletedList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ValidationSummary ID="ValidationSummaryErroriSalvataggio" runat="server" ValidationGroup="salvataggio"
                    CssClass="messaggiErrore" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="ButtonSalvataggio" runat="server" ValidationGroup="salvataggio" Text="Salva"
                    Width="150px" OnClick="ButtonSalvataggio_Click" />&nbsp;
                <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="150px" OnClick="ButtonIndietro_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Visible="False">Errore nel salvataggio dei dati</asp:Label>
                <asp:Label ID="LabelOk" runat="server" ForeColor="Red" Visible="False">Salvataggio effettuato</asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
