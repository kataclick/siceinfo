<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneDomandePersonali.aspx.cs" Inherits="Prestazioni_GestioneDomandePersonali" %>

<%@ Register Src="../WebControls/PrestazioniRicercaDomandeLavoratore.ascx" TagName="PrestazioniRicercaDomandeLavoratore"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Domande effettuate"
        titolo="Prestazioni" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:PrestazioniRicercaDomandeLavoratore id="PrestazioniRicercaDomandeLavoratore1"
                    runat="server">
                </uc3:PrestazioniRicercaDomandeLavoratore></td>
        </tr>
    </table>
    <br />
</asp:Content>

