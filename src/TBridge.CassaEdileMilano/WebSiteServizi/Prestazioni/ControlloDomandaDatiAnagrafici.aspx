<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlloDomandaDatiAnagrafici.aspx.cs" Inherits="Prestazioni_ControlloDomandaDatiAnagrafici" %>

<%@ Register Src="../WebControls/PrestazioniDatiDomanda.ascx" TagName="PrestazioniDatiDomanda"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda - Dati anagrafici"
        titolo="Prestazioni" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:PrestazioniDatiDomanda ID="PrestazioniDatiDomanda1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelAzioni" runat="server">
                    <table class="standardTable">
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="width: 193px">
                                <b>Anagrafica</b>
                            </td>
                            <td>
                                <b>Comunicato</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px" rowspan="5">
                                Indirizzo
                            </td>
                            <td rowspan="5">
                                <asp:Image ID="ImageControlloIndirizzo" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                <asp:Button ID="ButtonForzaIndirizzo" runat="server" Text="Forza" Enabled="false"
                                    OnClick="ButtonForzaIndirizzo_Click" />
                            </td>
                            <td style="width: 193px">
                                <asp:Label ID="LabelAnagraficaIndirizzo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoIndirizzo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 193px">
                                <asp:Label ID="LabelAnagraficaProvincia" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoProvincia" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 193px">
                                <asp:Label ID="LabelAnagraficaComune" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoComune" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 193px">
                                <asp:Label ID="LabelAnagraficaFrazione" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoFrazione" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 193px">
                                <asp:Label ID="LabelAnagraficaCap" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoCap" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                Email
                            </td>
                            <td>
                                <asp:Image ID="ImageControlloEmail" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                <asp:Button ID="ButtonForzaEmail" runat="server" Text="Forza" Enabled="false" OnClick="ButtonForzaEmail_Click" />
                            </td>
                            <td>
                                <asp:Label ID="LabelAnagraficaEmail" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoEmail" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                Cellulare
                            </td>
                            <td>
                                <asp:Image ID="ImageControlloCellulare" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                <asp:Button ID="ButtonForzaCellulare" runat="server" Text="Forza" Enabled="false"
                                    OnClick="ButtonForzaCellulare_Click" />
                            </td>
                            <td>
                                <asp:Label ID="LabelAnagraficaCellulare" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="LabelComunicatoCellulare" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:BulletedList ID="BulletedListControlli" runat="server">
                                </asp:BulletedList>
                            </td>
                        </tr>
                    </table>
                    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
                        Width="150px" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
