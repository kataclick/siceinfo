<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CompilazioneDomanda.aspx.cs" Inherits="Prestazioni_CompilazioneDomanda" %>

<%@ Register Src="../WebControls/PrestazioniDatiLavoratore.ascx" TagName="PrestazioniDatiLavoratore"
    TagPrefix="uc7" %>
<%@ Register Src="../WebControls/PrestazioniSelezioneCasseEdili.ascx" TagName="PrestazioniSelezioneCasseEdili"
    TagPrefix="uc6" %>
<%@ Register Src="../WebControls/PrestazioniRicercaFamiliare.ascx" TagName="PrestazioniRicercaFamiliare"
    TagPrefix="uc5" %>
<%@ Register Src="../WebControls/PrestazioniDatiFattura.ascx" TagName="PrestazioniDatiFattura"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/PrestazioniDatiFamiliare.ascx" TagName="PrestazioniDatiFamiliare"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server"></uc1:MenuPrestazioni>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Compilazione domanda"
        titolo="Prestazioni" />
    <br />
    <asp:Wizard ID="WizardInserimento" runat="server" ActiveStepIndex="0" 
        Height="200px" SideBarStyle-Width="160px"
        Width="100%" StartNextButtonText="Avanti" StepNextButtonText="Avanti" StepPreviousButtonText="Indietro"
        FinishCompleteButtonText="Inserisci" FinishPreviousButtonText="Indietro" BorderWidth="1px"
        Font-Names="Verdana" OnSideBarButtonClick="WizardInserimento_SideBarButtonClick"
        OnNextButtonClick="WizardInserimento_NextButtonClick" OnPreviousButtonClick="WizardInserimento_PreviousButtonClick"
        OnFinishButtonClick="WizardInserimento_FinishButtonClick">
        <StepStyle VerticalAlign="Top" />
        <StartNavigationTemplate>
            <asp:Button ID="btnSuccessivoStart" runat="server" Text="Avanti" CommandName="MoveNext"
                Width="100px" CausesValidation="true" TabIndex="1" ValidationGroup="stop" />
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" CausesValidation="false" />
            <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
                CausesValidation="true" Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StepNavigationTemplate>
        <FinishNavigationTemplate>
            <asp:Button ID="btnPrecedenteFinish" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" CausesValidation="false" />
            <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Conferma domanda"
                CausesValidation="false" Width="150px" TabIndex="1" />
        </FinishNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep ID="WizardStepDatiIniziali" runat="server" Title="- Identificazione"
                StepType="Start">
                <!-- DATI INIZIALI -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelDatiIniziali" runat="server" Text="Identificazione" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorDatiInizialiLavoratorePresenteAnagrafica"
                                runat="server" ErrorMessage="Lavoratore non riconosciuto. Controllare i dati inseriti e riprovare. Se il problema persiste contattare Cassa Edile."
                                OnServerValidate="CustomValidatorDatiInizialiLavoratorePresenteAnagrafica_ServerValidate"
                                ValidationGroup="stop">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="PanelDatiIniziali" runat="server">
                                <table class="standardTable">
                                    <tr id="trDatiInizialiMessaggio" runat="server" visible="false">
                                        <td colspan="3">
                                            Campi automaticamente proposti, non modificabili. Per procedere clicca su "Avanti".
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Codice fiscale:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDatiInizialiCodiceFiscale" runat="server" MaxLength="16"
                                                Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiInizialiCodiceFiscale"
                                                runat="server" ValidationGroup="stop" ControlToValidate="TextBoxDatiInizialiCodiceFiscale"
                                                ErrorMessage="Codice fiscale mancante">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDatiInizialiCodiceFiscale"
                                                runat="server" ControlToValidate="TextBoxDatiInizialiCodiceFiscale" ErrorMessage="Formato del codice fiscale non valido"
                                                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cognome:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDatiInizialiCognome" runat="server" MaxLength="30" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiGeneraliCognome" runat="server"
                                                ControlToValidate="TextBoxDatiInizialiCognome" ErrorMessage="Cognome mancante"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Nome:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDatiInizialiNome" runat="server" MaxLength="30" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiInizialiNome" runat="server"
                                                ControlToValidate="TextBoxDatiInizialiNome" ErrorMessage="Nome mancante" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Data di nascita (gg/mm/aaaa):
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDatiInizialiDataNascita" runat="server" MaxLength="10" Width="300px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiInizialiDataNascita" runat="server"
                                                ControlToValidate="TextBoxDatiInizialiDataNascita" ErrorMessage="Data di nascita mancante"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidatorDatiInizialiDataNascita" runat="server"
                                                ErrorMessage="Formato della data di nascita errato" Operator="DataTypeCheck"
                                                Type="Date" ValidationGroup="stop" ControlToValidate="TextBoxDatiInizialiDataNascita">*</asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryDatiIniziali" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepDatiLavoratore" runat="server" StepType="Step" Title="- Indirizzo">
                <!-- DATI LAVORATORE -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelDatiLavoratore" runat="server" Text="Indirizzo" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc7:PrestazioniDatiLavoratore ID="PrestazioniDatiLavoratore1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="PanelDatiLavoratore" runat="server">
                                <table class="standardTable">
                                    <tr id="trDatiLavoratoreMessaggio" runat="server" visible="false">
                                        <td colspan="3">
                                            I campi riferiti a Indirizzo, Provincia, Localit�, Frazione, CAP sono modificabili.
                                            In caso di variazione selezionare il bottone "Modifica i dati" e aggiornare i dati.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDatiLavoratoreIndirizzo" runat="server" MaxLength="40" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiLavoratoreIndirizzo" runat="server"
                                                ErrorMessage="Indirizzo mancante" ControlToValidate="TextBoxDatiLavoratoreIndirizzo"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListDatiLavoratoreProvincia" runat="server" Width="350px"
                                                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListDatiLavoratoreProvincia_SelectedIndexChanged" />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiLavoratoreProvincia" runat="server"
                                                ErrorMessage="Provincia non selezionata" ControlToValidate="DropDownListDatiLavoratoreProvincia"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Localit�:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListDatiLavoratoreLocalita" runat="server" Width="350px"
                                                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListDatiLavoratoreLocalita_SelectedIndexChanged" />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiLavoratoreLocalita" runat="server"
                                                ErrorMessage="Localit&#224; non selezionata" ControlToValidate="DropDownListDatiLavoratoreLocalita"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Frazione:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListDatiLavoratoreFrazione" runat="server" Width="350px"
                                                AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListDatiLavoratoreFrazione_SelectedIndexChanged"
                                                AutoPostBack="True" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            CAP:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDatiLavoratoreCap" runat="server" MaxLength="5" Width="350px" />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiLavoratoreCap" runat="server"
                                                ErrorMessage="Cap mancante" ControlToValidate="TextBoxDatiLavoratoreCap" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDatiLavoratoreCap"
                                                ControlToValidate="TextBoxDatiLavoratoreCap" ValidationGroup="stop" runat="server"
                                                ErrorMessage="Cap non corretto" ValidationExpression="^\d{5}$">*</asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="CustomValidatorDatiLavoratoreCap" runat="server" ControlToValidate="TextBoxDatiLavoratoreCap"
                                                ErrorMessage="Non pu&#242; essere utilizzato un CAP generico" OnServerValidate="CustomValidatorCAP_ServerValidate"
                                                ValidationGroup="stop">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="ButtonDatiLavoratoreModifica" runat="server" Visible="false" Width="100px"
                                OnClick="ButtonDatiLavoratoreModifica_Click" Text="Modifica i dati" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryDatiLavoratore" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepDatiComunicazioni" runat="server" StepType="Step" Title="- Contatti e dati bancari">
                <!-- DATI COMUNICAZIONI -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelDatiComunicazioni" runat="server" Text="Comunicazioni" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc7:PrestazioniDatiLavoratore ID="PrestazioniDatiLavoratore2" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="PanelDatiComunicazioni" runat="server">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Cellulare:
                                        </td>
                                        <td style="width: 453px">
                                            <asp:TextBox ID="TextBoxDatiComunicazioniCellulare" runat="server" MaxLength="14"
                                                Width="350px" />
                                        </td>
                                        <td style="width: 48px">
                                            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidatorDatiComunicazioniCellulare"
                                                runat="server" ControlToValidate="TextBoxDatiComunicazioniCellulare" ErrorMessage="Il formato del cellulare non � corretto"
                                                ValidationExpression="^([+]39)?3\d{8,11}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="CustomValidatorDatiComunicazioniCellulare"
                                                runat="server" ErrorMessage="Cellulare mancante" ValidationGroup="stop" 
                                                OnServerValidate="CustomValidatorDatiComunicazioniCellulare_ServerValidate">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            Inserire il numero di telefono senza spazi o simboli (trattino, barra, ecc.).
                                            <br />
                                            <br />
                                            Se inserisci il numero di cellulare potrai essere contattato in caso di necessit�
                                            e ricevere informazioni sullo stato della tua domanda.<br />
                                            <br />
                                            Riporta qui sotto il tuo indirizzo e-mail se sei interessato a ricevere il promemoria
                                            dei documenti da allegare alle domande di prestazioni che stai compilando.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td style="width: 453px">
                                            <asp:TextBox ID="TextBoxDatiComunicazioniEmail" runat="server" MaxLength="40" Width="350px" />
                                        </td>
                                        <td style="width: 48px">
                                            &nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidatorDatiComunicazioniEmail"
                                                runat="server" ErrorMessage="Formato dell'indirizzo email errato" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ValidationGroup="stop" ControlToValidate="TextBoxDatiComunicazioniEmail">*</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr id="trTipoPagamento" runat="server">
                                        <td>
                                            Tipo pagamento:
                                        </td>
                                        <td style="width: 453px">
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="RadioButtonContoCorrente" runat="server" GroupName="tipoPagamento"
                                                            Text="versamento su conto corrente" AutoPostBack="True" OnCheckedChanged="RadioButtonContoCorrente_CheckedChanged" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="PanelDatiComunicazioniTipoPagamentoContoCorrenteDettagli" runat="server"
                                                            Enabled="false">
                                                            <table class="standardTable">
                                                                <tr>
                                                                    <td>
                                                                        IBAN:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TextBoxDatiComunicazioniTipoPagamentoIBAN" runat="server" Width="350px"
                                                                            MaxLength="34"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CustomValidator ID="CustomValidatorRequiredDatiComunicazioniTipoPagamentoIBAN"
                                                                            runat="server" ControlToValidate="TextBoxDatiComunicazioniTipoPagamentoIBAN"
                                                                            ErrorMessage="Codice IBAN mancante" OnServerValidate="CustomValidatorRequiredDatiComunicazioniTipoPagamentoIBAN_ServerValidate"
                                                                            ValidationGroup="stop" ValidateEmptyText="True">*</asp:CustomValidator>
                                                                        <asp:CustomValidator ID="CustomValidatorDatiComunicazioniTipoPagamentoIBAN" runat="server"
                                                                            ValidationGroup="stop" ErrorMessage="Codice IBAN non corretto" ControlToValidate="TextBoxDatiComunicazioniTipoPagamentoIBAN"
                                                                            OnServerValidate="CustomValidatorDatiComunicazioniTipoPagamentoIBAN_ServerValidate">*</asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RadioButton ID="RadioButtonCartaPrepagata" runat="server" GroupName="tipoPagamento"
                                                            Text="richiedi carta prepagata" AutoPostBack="True" OnCheckedChanged="RadioButtonContoCorrente_CheckedChanged" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Panel ID="PanelDatiComunicazioniTipoPagamentoCartaPrepagataDettagli" runat="server"
                                                            Enabled="false">
                                                            <table class="standardTable">
                                                                <tr>
                                                                    <td>
                                                                        Tipo di carta:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="DropDownListDatiComunicazioniTipoCarta" runat="server" Width="350px">
                                                                            <asp:ListItem></asp:ListItem>
                                                                            <asp:ListItem Value="C3">Carta prepagata +ma (Banca Popolare di Sondrio)</asp:ListItem>
                                                                            <asp:ListItem Value="C4">Carta prepagata Be1card (Banca Popolare di Milano)</asp:ListItem>
                                                                            <asp:ListItem Value="C5">Carta prepagata Enjoy (Gruppo UBI Banca)</asp:ListItem>
                                                                            <asp:ListItem Value="C6">Carta prepagata K2 (Banca Popolare di Lodi)</asp:ListItem>
                                                                            <asp:ListItem Value="C4">Carta prepagata Superflash (Gruppo Intesa-Sanpaolo)</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CustomValidator ID="RequiredDatiComunicazioniTipoPagamentoTipoCarta" runat="server"
                                                                            ControlToValidate="DropDownListDatiComunicazioniTipoCarta" ErrorMessage="Tipologia di carta non selezionata"
                                                                            OnServerValidate="RequiredDatiComunicazioniTipoPagamentoTipoCarta_ServerValidate"
                                                                            ValidationGroup="stop" ValidateEmptyText="True">*</asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <p>
                                                                            Per maggiori informazioni sulle caratteristiche dei prodotti vai alla pagina
                                                                            web del sito "<a href="http://ww2.cassaedilemilano.it/Lavoratori/InformazioniOperative/Modalit�dipagamento/Carteprepagate/tabid/87/language/it-IT/Default.aspx">Carta
                                                                                Prepagata</a>"
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 48px">
                                            <asp:CustomValidator ID="CustomValidatorTipoPagamento" runat="server" ErrorMessage="Selezionare una modalit&#224; di pagamento"
                                                OnServerValidate="CustomValidatorTipoPagamento_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="ButtonDatiComunicazioniModifica" runat="server" Width="100px" OnClick="ButtonDatiComunicazioniModifica_Click"
                                Text="Modifica i dati" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryDatiComunicazioni" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepDatiCasseEdili" runat="server" StepType="Step" Title="- Casse edili">
                <!-- DATI CASSE EDILI -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelDatiCasseEdili" runat="server" Text="Casse edili" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc7:PrestazioniDatiLavoratore ID="PrestazioniDatiLavoratore3" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="PanelDatiCasseEdili" runat="server">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <p>
                                                Se negli ultimi 5 anni hai lavorato per imprese iscritte presso altre Casse Edili,
                                                seleziona le Casse di provenienza in modo tale che le ore di lavoro ordinario registrate
                                                presso altre Casse possano essere sommate alle ore accantonate presso Cassa Edile
                                                di Milano, Lodi, Monza e Brianza. Ci� ti aiuter� a raggiungere il numero minimo
                                                di ore previsto per beneficiare della prestazione assistenziale richiesta.
                                            </p>
                                            <p>
                                                Una volta individuata la Cassa di provenienza cliccare su "Aggiungi lista". L'operazione
                                                andr� ripetuta per tutte le Casse dove sei stato iscritto.
                                            </p>
                                            <p>
                                                Al termine dell'operazione cliccare su "Avanti"
                                            </p>
                                            <p>
                                                Se hai lavorato sempre e solo per imprese iscritte in Cassa Edile di Milano, Lodi,
                                                Monza e Brianza, clicca direttamente su "Avanti".
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc6:PrestazioniSelezioneCasseEdili ID="PrestazioniSelezioneCasseEdili1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryDatiCasseEdili" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepTipoPrestazione" runat="server" StepType="Step" Title="- Tipo prestazione">
                <!-- TIPO PRESTAZIONE -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelTipoPrestazione" runat="server" Text="Tipo prestazione" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc7:PrestazioniDatiLavoratore ID="PrestazioniDatiLavoratore4" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="PanelTipoPrestazione" runat="server">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Per chi richiedi la prestazione?:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListTipoPrestazioneBeneficiario" runat="server" Width="350px"
                                                AutoPostBack="True" OnSelectedIndexChanged="DropDownListTipoPrestazioneBeneficiario_SelectedIndexChanged">
                                                <asp:ListItem></asp:ListItem>
                                                <asp:ListItem Value="L">Per te stesso</asp:ListItem>
                                                <asp:ListItem Value="C">Per tua moglie</asp:ListItem>
                                                <asp:ListItem Value="F">Per tuo/a figlio/a</asp:ListItem>
                                                <asp:ListItem Value="O">Genitori conviventi</asp:ListItem>
                                                <asp:ListItem Value="G">Genitori non conviventi</asp:ListItem>
                                                <asp:ListItem Value="A">Parenti affini fino al 2� grado</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            (in caso di contributo per spese in caso di decesso, indicare il grado di parentela della
                                            persona deceduta)
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoPrestazioneBeneficiario"
                                                runat="server" ControlToValidate="DropDownListTipoPrestazioneBeneficiario" ErrorMessage="Beneficiario non selezionato"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tipo prestazione:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListTipoPrestazioneTipoPrestazione" runat="server"
                                                Width="350px" AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListTipoPrestazioneTipoPrestazione_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoPrestazioneTipoPrestazione"
                                                runat="server" ControlToValidate="DropDownListTipoPrestazioneTipoPrestazione"
                                                ErrorMessage="Tipologia di prestazione non selezionata" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <p>
                                                Se hai gi� richiesto prestazioni per tua moglie o i tuoi figli, seleziona semplicemente
                                                il familiare che ti viene proposto automaticamente oppure inserisci direttamente
                                                i dati anagrafici del familiare per il quale stai richiedendo la prestazione tramite
                                                il tasto "Nuovo familiare"
                                            </p>
                                            <p>
                                                In caso dovessi selezionare il familiare sbagliato, riclicca su "Seleziona familiare
                                                da anagrafica" e riseleziona il familiare corretto.
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Panel ID="PanelDatiFamiliare" runat="server" Enabled="False">
                                                <table class="standardTable">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="ButtonDatiFamiliareSelezionaFamiliare" runat="server" Text="Seleziona familiare da anagrafica"
                                                                OnClick="ButtonDatiFamiliareSelezionaFamiliare_Click" Visible="False" Width="200px" />
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="ButtonDatiFamiliareNuovo" runat="server" Text="Nuovo familiare" Visible="False"
                                                                Width="200px" OnClick="ButtonDatiFamiliareNuovo_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uc5:PrestazioniRicercaFamiliare ID="PrestazioniRicercaFamiliare1" runat="server"
                                                                Visible="False" />
                                                                <br />
                                                                <asp:Label runat="server" ID="LabelDataNascitaFiglio" Visible="false" Text="La data di nascita del familiare &#232; troppo antecedente la data della domanda."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <uc3:PrestazioniDatiFamiliare ID="PrestazioniDatiFamiliare1" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryTipoPrestazione" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryDatiFamiliare" runat="server" ValidationGroup="datiFamiliare"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepFatture" runat="server" Title="- Dati aggiuntivi" StepType="Step">
                <!-- FATTURE -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelFatture" runat="server" Text="Dati aggiuntivi" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorFatture" runat="server" ErrorMessage="Le fatture devono tutte far parte dello stesso anno di riferimento"
                                OnServerValidate="CustomValidatorFatture_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                            &nbsp;<asp:CustomValidator ID="CustomValidatorAlmenoUnaFattura" runat="server" ErrorMessage="Va inserita almeno una fattura"
                                OnServerValidate="CustomValidatorAlmenoUnaFattura_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc7:PrestazioniDatiLavoratore ID="PrestazioniDatiLavoratore5" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="PanelFatture" runat="server" Visible="true">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <p>
                                                Una volta inseriti i dati della fattura, clicca su "Aggiungi la fattura alla lista"
                                                e seleziona "Avanti". E' possibile inserire pi� fatture <b>che poi andranno allegate
                                                    alla domanda di prestazione</b>. Alla fine di ogni inserimento, occorrer� cliccare
                                                su "Aggiungi la fattura alla lista". Al termine dell'operazione, cliccare su "Avanti".
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelTitoloListaFatture" runat="server" Font-Bold="true" Text="Fatture caricate"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="PanelFattureNuova" runat="server">
                                                <table class="standardTable">
                                                    <tr>
                                                        <td colspan="2">
                                                            <uc4:PrestazioniDatiFattura ID="PrestazioniDatiFattura1" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="LabelFatturaGiaPresente" runat="server" ForeColor="Red" Text="La fattura &#232; gi&#224; stata utilizzata in un'altra domanda. Non &#232; possibile caricarla."
                                                                Visible="False"></asp:Label>
                                                            <asp:Label ID="LabelFatturaGiaCaricata" runat="server" ForeColor="Red" Text="La fattura &#232; gi&#224; stata caricata."
                                                                Visible="False"></asp:Label>
                                                            <asp:Label ID="LabelFatturaNonCorrettaGenerico" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Button ID="ButtonFattureNuovaInserisci" runat="server" Text="Aggiungi la fattura alla lista"
                                                                ValidationGroup="nuovaFattura" OnClick="ButtonFattureNuovaInserisci_Click" Width="200px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:ValidationSummary ID="ValidationSummaryFattureNuova" runat="server" ValidationGroup="nuovaFattura"
                                                                CssClass="messaggiErrore" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GridViewFatture" runat="server" AutoGenerateColumns="False" Width="100%"
                                                OnRowDeleting="GridViewFatture_RowDeleting" OnRowDataBound="GridViewFatture_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data">
                                                        <ItemStyle Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Numero" HeaderText="Numero" />
                                                    <asp:BoundField DataField="ImportoTotale" HeaderText="Importo" >
                                                    <ItemStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Tipo" Visible="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelTipoFattura" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rimuovi"
                                                        ShowDeleteButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                                                        <ItemStyle Width="10px" />
                                                    </asp:CommandField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    Nessuna fattura caricata<br />
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="PanelLenti" runat="server" Visible="false" Width="100%">
                                                <table class="standardTable">
                                                    <tr>
                                                        <td style="width: 150px">
                                                            Importo delle lenti:
                                                        </td>
                                                        <td style="width: 203px">
                                                            <asp:TextBox ID="TextBoxImportoLenti" runat="server" MaxLength="6"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:CompareValidator ID="CompareValidatorImportoLenti" runat="server" ControlToValidate="TextBoxImportoLenti"
                                                                ErrorMessage="Formato dell'importo non valido" Operator="DataTypeCheck" Type="Currency"
                                                                ValidationGroup="stop">*</asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="PanelScolastiche" runat="server" Visible="false">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Tipologia di scuola frequentata:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListTipologiaScuola" runat="server" AppendDataBoundItems="True"
                                                AutoPostBack="True" OnSelectedIndexChanged="DropDownListTipologiaScuola_SelectedIndexChanged"
                                                Width="350px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipologiaScuola" runat="server"
                                                ControlToValidate="DropDownListTipologiaScuola" ErrorMessage="Selezionare un tipo di scuola"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tipologia di promozione
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListTipologiaPromozione" runat="server" AppendDataBoundItems="True"
                                                Width="350px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipologiaPromozione" runat="server"
                                                ControlToValidate="DropDownListTipologiaPromozione" ErrorMessage="Selezionare un tipo di promozione"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel
                                ID="PanelScolasticheSecondarie1"
                                runat="server"
                                Visible="false">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Anno scolastico:
                                        </td>
                                        <td>
                                            <asp:RadioButtonList
                                                ID="RadioButtonListAnnoScolastico"
                                                runat="server">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:CustomValidator ID="CustomValidatorAnnoScolastico" runat="server"
                                                ErrorMessage="Selezionare l'anno scolastico"
                                                ValidationGroup="stop" 
                                                OnServerValidate="RequiredFieldValidatorAnnoScolastico_ServerValidate">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Denominazione istituto scolastico
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxIstitutoScolastico" runat="server" MaxLength="50"
                                                Width="350px">
                                            </asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIstitutoScolastico" runat="server"
                                                ControlToValidate="TextBoxIstitutoScolastico" ErrorMessage="Indicare l'istituto scolastico"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxIstitutoScolasticoIndirizzo" runat="server" MaxLength="50"
                                                Width="350px">
                                            </asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIstitutoScolasticoIndirizzo" runat="server"
                                                ControlToValidate="TextBoxIstitutoScolasticoIndirizzo" ErrorMessage="Indicare l'indirizzo dell'istituto scolastico"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Provincia:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListIstitutoScolasticoProvincia" runat="server" Width="350px"
                                                AppendDataBoundItems="True" AutoPostBack="True" 
                                                OnSelectedIndexChanged="DropDownListIstitutoScolasticoProvincia_SelectedIndexChanged"  />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIstitutoScolasticoProvincia" runat="server"
                                                ErrorMessage="Provincia dell'istituto scolastico non selezionata" ControlToValidate="DropDownListIstitutoScolasticoProvincia"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Localit�:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListIstitutoScolasticoLocalita" runat="server" Width="350px"
                                                AppendDataBoundItems="True" />
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIstitutoScolasticoLocalita" runat="server"
                                                ErrorMessage="Localit&#224; non selezionata" ControlToValidate="DropDownListIstitutoScolasticoLocalita"
                                                ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="PanelScolasticheUnico" runat="server" Visible="false">
                                <br />
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                Dichiarazione dello studente ai fini delle detrazioni di imposta spettanti (art. 22, D.P.R. 29/09/1973, n. 600 e s.m.i.)
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Lo studente dichiara di aver diritto per l'anno in corso alle ALTRE DETRAZIONI (art.13 TUIR)
                                            <asp:CustomValidator 
                                                    ID="CustomValidatorReddito" 
                                                    runat="server" 
                                                    ErrorMessage="Selezionare se si ha diritto o meno alle detrazioni"
                                                    ValidationGroup="inserimentoDomanda" 
                                                OnServerValidate="CustomValidatorReddito_ServerValidate">
                                                    *
                                                </asp:CustomValidator>:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButton
                                                ID="RadioButtonRedditoSi"
                                                runat="server"
                                                GroupName="reddito"
                                                Text="S�" />
                                            <br />
                                            <asp:RadioButton
                                                ID="RadioButtonRedditoNo"
                                                runat="server"
                                                GroupName="reddito"
                                                Text="No" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            (barrare "S�" nel caso in cui lo studente NON abbia rapporti di lavoro o di collaborazione nell'anno di presentazione della domanda; in caso contrario barrare "No").
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="PanelFunerario" runat="server" Visible="false">
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Data del decesso:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="TextBoxDataDecesso" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Inserire la data del decesso"
                                                ControlToValidate="TextBoxDataDecesso" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidatorDataDecessoCheck" runat="server" ControlToValidate="TextBoxDataDecesso"
                                                ErrorMessage="La data non � corretta" Operator="DataTypeCheck" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                                            <asp:CompareValidator ID="CompareValidatorDataDecessoMinima" runat="server" ControlToValidate="TextBoxDataDecesso"
                                                ErrorMessage="La data del decesso non pu� essere antecedente pi� di 6 mesi dalla data della domanda"
                                                Operator="GreaterThanEqual" Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                                            <asp:CompareValidator ID="CompareValidatorDataDecessoMassima" runat="server" ControlToValidate="TextBoxDataDecesso"
                                                ErrorMessage="La data non pu� essere maggiore della data odierna" Operator="LessThanEqual"
                                                Type="Date" ValidationGroup="stop">*</asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Causa del decesso:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListTipoDecesso" runat="server" Width="210px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="PanelNessunDato" runat="server" Visible="false">
                                Per la tipologia di prestazione selezionata non sono previsti dati aggiuntivi. Premere
                                su "Avanti".</asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryFatture" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore"></asp:ValidationSummary>
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStepConferma" runat="server" Title="- Conferma" StepType="Finish">
                <!-- CONFERMA -->
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="LabelConferma" runat="server" Text="Conferma" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:BulletedList ID="BullettedListResoconto" runat="server" CssClass="messaggiErrore">
                            </asp:BulletedList>
                            <br />
                            <asp:Label ID="LabelControlliSupplementari" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Per confermare la domanda premere il pulsante "Conferma domanda"
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="standardTable">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Dati lavoratore"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Codice fiscale
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoCodiceFiscale" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cognome
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoCognome" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nome
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoNome" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Data di nascita
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoDataNascita" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cellulare
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoCellulare" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoEmail" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Indirizzo"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Indirizzo
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoIndirizzo" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Provincia
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoProvincia" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Localit�
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoLocalita" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Frazione
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoFrazione" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        CAP
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoCAP" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Dati prestazione"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Per chi hai richiesto la prestazione?&nbsp;
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoBeneficiario" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tipo prestazione
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoTipoPrestazione" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cognome beneficiario
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoBeneficiarioCognome" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nome beneficiario
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoBeneficiarioNome" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Data di nascita beneficiario
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoBeneficiarioDataNascita" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Codice fiscale beneficiario
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoBeneficiarioCodiceFiscale" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trFatture1" runat="server">
                                    <td colspan="2">
                                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Fatture inserite e da allegare alla domanda"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trFatture2" runat="server">
                                    <td>
                                        Numero
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelRiassuntoNumeroFatture" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trFatture3" runat="server">
                                    <td colspan="2">
                                        <asp:GridView ID="GridViewRiassuntoFatture" runat="server" AutoGenerateColumns="False"
                                            Width="500px" OnRowDataBound="GridViewRiassuntoFatture_RowDataBound">
                                            <Columns>
                                                <asp:BoundField DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data">
                                                    <ItemStyle Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Numero" HeaderText="Numero" />
                                                <asp:BoundField DataField="ImportoTotale" HeaderText="Importo" />
                                                <asp:TemplateField HeaderText="Tipo" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelTipoFattura" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                Nessuna fattura caricata<br />
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Documenti da allegare alla domanda</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridViewDocumentiRichiesti" runat="server" Width="500px" AutoGenerateColumns="False"
                                OnRowDataBound="GridViewDocumentiRichiesti_RowDataBound">
                                <EmptyDataTemplate>
                                    Nessuno
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo documento" />
                                    <asp:TemplateField HeaderText="Relativo a">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Stato">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelStato" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>                                
                                Occorre stampare il modulo, <b>firmarlo</b> e spedirlo o consegnarlo <b>IN ORIGINALE</b> alla Cassa
                                Edile provvisto dei documenti sopraindicati (fatture e documenti).
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Vuoi stampare il certificato di famiglia nel formato Cassa Edile?
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButtonCertificatoFamigliaSi" runat="server" GroupName="certificatoFamiglia"
                                            Text="S�" />
                                        <asp:RadioButton ID="RadioButtonCertificatoFamigliaNo" runat="server" GroupName="certificatoFamiglia"
                                            Text="No" Checked="true" />
                                    </td>
                                </tr>
                                <tr id="trModuloDentista" runat="server">
                                    <td>
                                        Vuoi stampare il modulo per il dentista?
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="RadioButtonModuloDentistaSi" runat="server" GroupName="moduloDentista"
                                            Text="S�" />
                                        <asp:RadioButton ID="RadioButtonModuloDentistaNo" runat="server" GroupName="moduloDentista"
                                            Text="No" Checked="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
        </WizardSteps>

<SideBarStyle Width="160px"></SideBarStyle>
        <SideBarTemplate>
            <asp:DataList ID="SideBarList" runat="server" Width="160px">
                <SelectedItemStyle Font-Bold="True" />
                <ItemTemplate>
                    <asp:LinkButton ID="SideBarButton" runat="server" CausesValidation="true" ValidationGroup="stop"></asp:LinkButton>
                </ItemTemplate>
            </asp:DataList>
        </SideBarTemplate>
        <NavigationButtonStyle BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" />
        <HeaderStyle BorderStyle="Solid" BorderWidth="2px" Font-Bold="True" HorizontalAlign="Center" />
        <StepNextButtonStyle Width="100px" />
        <StartNextButtonStyle Width="100px" />
        <FinishCompleteButtonStyle Width="150px" />
        <FinishPreviousButtonStyle Width="100px" />
        <StepPreviousButtonStyle Width="100px" />
    </asp:Wizard>
    <br />
    <asp:Button ID="ButtonSalvaTemporaneamente" runat="server" Text="Salva temporaneamente la domanda"
        OnClick="ButtonSalvaTemporaneamente_Click" ValidationGroup="stop" Visible="False" /><br />
</asp:Content>
