<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ConfermaSalvataggioTemporaneo.aspx.cs" Inherits="Prestazioni_ConfermaSalvataggioTemporaneo"  %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Conferma salvataggio temporaneo"
        titolo="Prestazioni" />
    <br />
    <p>
        Il salvataggio temporaneo � avvenuto correttamente. La domanda pu� essere recuperata dalla
        <a href="~/Prestazioni/GestioneDomandeNonConfermate.aspx" runat="server">Gestione domande non confermate</a>.
    </p>
</asp:Content>

