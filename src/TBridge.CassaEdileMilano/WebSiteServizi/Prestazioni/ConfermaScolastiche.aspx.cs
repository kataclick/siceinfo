using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Prestazioni_ConfermaScolastiche : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda,
                                              "~/Prestazioni/ReportPrestazioniSanitarie.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                Int32 idDomanda = (Int32) Context.Items["IdDomanda"];
                string modulo = (string) Context.Items["TipoModulo"];
                ViewState["IdDomanda"] = idDomanda;
                ViewState["TipoModulo"] = modulo;

                //ReportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                //ReportViewer.ServerReport.ReportPath = modulo;

                //ReportParameter[] listaParam = new ReportParameter[1];
                //listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());

                //ReportViewer.ServerReport.SetParameters(listaParam);

                ClientScriptManager cs = Page.ClientScript;
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "auto_postback", "window.onload = function() { " + cs.GetPostBackClientHyperlink(ButtonStampaRicevutaNascosto, "first", false) + "; }; ", true);
            }
            else
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Prestazioni/CompilazioneDomanda.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Prestazioni/PrestazioniDefault.aspx");
    }

    protected void ButtonStampaModulo_Click(object sender, EventArgs e)
    {
        CaricaModulo();
    }

    protected void ButtonStampaRicevutaNascosto_Click(object sender, EventArgs e)
    {
        CaricaModulo();
    }

    private void CaricaModulo()
    {
        Context.Items["IdDomanda"] = ViewState["IdDomanda"];
        Context.Items["TipoModulo"] = ViewState["TipoModulo"];

        Server.Transfer("~/Prestazioni/ReportPrestazioniScolastiche.aspx");
    }
}