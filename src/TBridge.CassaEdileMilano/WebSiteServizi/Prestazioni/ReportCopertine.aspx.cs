using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Entities;
using System.Text;
using TBridge.Cemi.Prestazioni.Type.Collections;

public partial class Prestazioni_ReportCopertine : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.PrestazioniGestioneDomande);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                Int32 idDomanda = (Int32) Context.Items["IdDomanda"];
                String tipiDocumentoList = ParameterValuesListToString("TipiDocumentoList");
                String fattureList = ParameterValuesListToString("FattureDichiarateList");
                
                ReportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportCopertinaMaster";

                List<ReportParameter> parametri = new List<ReportParameter>();
                
                parametri.Add(new ReportParameter("idDomanda", idDomanda.ToString()));

                //if (Context.Items["Consegnati"] != null)
                //{
                //    parametri.Add(new ReportParameter("consegnati", "1"));
                //}

                if (!String.IsNullOrEmpty(tipiDocumentoList))
                {
                    parametri.Add(new ReportParameter("idTipiDocumentoList", tipiDocumentoList));
                }

                if (!String.IsNullOrEmpty(fattureList))
                {
                    parametri.Add(new ReportParameter("idFattureDichiarateList", fattureList));
                }

                String utente = GestioneUtentiBiz.GetNomeUtente();
                parametri.Add(new ReportParameter("utente", utente));
                
                ReportViewer.ServerReport.SetParameters(parametri.ToArray());

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;
                byte[] bytes = null;

                //PDF
                bytes = ReportViewer.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Copertine {0}.pdf", idDomanda));
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
            else
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
        }
    }

    private String ParameterValuesListToString(String contextParamName)
    {

        DocumentoStampaCopertinaCollection valuesList = Context.Items[contextParamName] as DocumentoStampaCopertinaCollection;

        if (valuesList != null && valuesList.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DocumentoStampaCopertina item in valuesList)
            {
                if (!String.IsNullOrEmpty(item.Id))
                {  
                    sb.AppendFormat("%{0}", item.Id);
                    if (item.Originale.HasValue)
                    {
                        sb.AppendFormat("!{0}", item.Originale.Value ? "1" : "0");
                    }
                }
            }
            return sb.ToString();
        }

        return null;     
    }
      
}