using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class Prestazioni_ReportPrestazioniSanitarie : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda,
                                              "~/Prestazioni/ReportPrestazioniSanitarie.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                int idDomanda = (int) Context.Items["IdDomanda"];

                ReportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportModuliPrestazioniSanitarie";
                ReportParameter[] listaParam = new ReportParameter[1];
                listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());

                ReportViewer.ServerReport.SetParameters(listaParam);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;
                byte[] bytes = null;

                //PDF
                bytes = ReportViewer.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Domanda {0}.pdf", idDomanda));
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
            else
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
        }
    }
}