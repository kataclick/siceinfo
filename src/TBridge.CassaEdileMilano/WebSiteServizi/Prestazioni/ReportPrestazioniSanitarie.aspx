<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportPrestazioniSanitarie.aspx.cs" Inherits="Prestazioni_ReportPrestazioniSanitarie" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Moduli"
        titolo="Prestazioni" />
    <rsweb:reportviewer id="ReportViewer" runat="server" height="600px"
        processingmode="Remote" width="100%" ShowDocumentMapButton="False" ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:reportviewer>
</asp:Content>

