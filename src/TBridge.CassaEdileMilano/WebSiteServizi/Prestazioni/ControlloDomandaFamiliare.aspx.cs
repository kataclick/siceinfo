using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Business;

public partial class Prestazioni_ControlloDomandaFamiliare : Page
{
    private const string URLSEMAFOROGIALLO = "~/images/semaforoGiallo.png";
    private const string URLSEMAFOROROSSO = "~/images/semaforoRosso.png";
    private const string URLSEMAFOROVERDE = "~/images/semaforoVerde.png";
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande,
                                              "~/Prestazioni/ControlloDomandaFamiliare.aspx");

        #endregion

        PrestazioniRicercaFamiliare1.OnFamiliareSelected += PrestazioniRicercaFamiliare1_OnFamiliareSelected;

        if (!Page.IsPostBack)
        {
            if(Context.Items["IdDomanda"]!=null)
            {
                int idDomanda = (int) Context.Items["IdDomanda"];
                Domanda domanda = biz.GetDomanda(idDomanda);
                biz.ControllaDomandaFamiliare(domanda);
                CaricaDomanda(domanda);

                ControllaStatoDomanda(domanda);

                AbilitaGestione(domanda);
            }
            else
            {
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
            }
        }
    }

    /// <summary>
    /// Abilita le azioni che pu� fare l'utente in funzione del ruolo e della propriet� della domanda 
    /// </summary>
    /// <param name="domanda">Domanda in gestione</param>
    private void AbilitaGestione(Domanda domanda)
    {
        //IUtente utente = ApplicationInstance.GetUtenteSistema();
        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
        //Se l'utente � chi ha la domanda in carico o ha funzioni amministrative, pu� getire la domanda
        if (idUtente == domanda.IdUtenteInCarico ||
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin.ToString()))
        {
            //Tutto normale
        }
        else
        {
            //Tutto viene disabilitato
            PanelAzioni.Enabled = false;
        }
    }

    /// <summary>
    /// in funzione dello stato della domanda decidiamo quali azioni lasciare all'utente
    /// </summary>
    /// <param name="domanda"></param>
    private void ControllaStatoDomanda(Domanda domanda)
    {
        if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
        {
            PanelAzioni.Enabled = true;
        }
        else
        {
            PanelAzioni.Enabled = false;
        }
    }

    private void CaricaDomanda(Domanda domanda)
    {
        ViewState["Domanda"] = domanda;

        // Familiare
        if (domanda.Familiare != null)
        {
            LabelAnagraficaFamiliareCodice.Text = domanda.Familiare.IdFamiliare.ToString();
            LabelAnagraficaFamiliareCognome.Text = domanda.Familiare.Cognome;
            LabelAnagraficaFamiliareNome.Text = domanda.Familiare.Nome;
            if (domanda.Familiare.DataNascita.HasValue)
                LabelAnagraficaFamiliareDataNascita.Text = domanda.Familiare.DataNascita.Value.ToShortDateString();
            if (domanda.Familiare.DataDecesso.HasValue)
                LabelAnagraficaFamiliareDataDecesso.Text = domanda.Familiare.DataDecesso.Value.ToShortDateString();
            LabelAnagraficaFamiliareCodiceFiscale.Text = domanda.Familiare.CodiceFiscale;
            LabelAnagraficaFamiliareGradoParentela.Text = domanda.Familiare.GradoParentela;
            LabelAnagraficaFamiliareACarico.Text = domanda.Familiare.ACarico;
        }

        // Familiare fornito
        if (domanda.FamiliareFornito != null)
        {
            LabelComunicatoFamiliareCognome.Text = domanda.FamiliareFornito.Cognome;
            LabelComunicatoFamiliareNome.Text = domanda.FamiliareFornito.Nome;
            if (domanda.FamiliareFornito.DataNascita.HasValue)
                LabelComunicatoFamiliareDataNascita.Text =
                    domanda.FamiliareFornito.DataNascita.Value.ToShortDateString();
            LabelComunicatoFamiliareCodiceFiscale.Text = domanda.FamiliareFornito.CodiceFiscale;
            LabelComunicatoFamiliareGradoParentela.Text = domanda.Beneficiario;
        }

        if (domanda.Familiare != null)
        {
            if (!domanda.Familiare.ControlloDataDecesso.HasValue)
                ButtonForzaDataDecesso.Enabled = true;
            else
                ButtonForzaDataDecesso.Enabled = false;
            if (!domanda.Familiare.ControlloDataNascita.HasValue)
                ButtonForzaDataNascita.Enabled = true;
            else
                ButtonForzaDataNascita.Enabled = false;
            //if (!domanda.Familiare.ControlloCodiceFiscale.HasValue)
            ButtonForzaCodiceFiscale.Enabled = true;
            //else
            //    ButtonForzaCodiceFiscale.Enabled = false;
            if (!domanda.Familiare.ControlloACarico.HasValue || !domanda.Familiare.ControlloACarico.Value)
                ButtonForzaACarico.Enabled = true;
            else
                ButtonForzaACarico.Enabled = false;
        }

        if (domanda.FamiliareFornito != null)
        {
            if (!domanda.FamiliareFornito.ControlloDataDecesso.HasValue)
                ButtonForzaDataDecesso.Enabled = true;
            else
                ButtonForzaDataDecesso.Enabled = false;
            if (!domanda.FamiliareFornito.ControlloDataNascita.HasValue)
                ButtonForzaDataNascita.Enabled = true;
            else
                ButtonForzaDataNascita.Enabled = false;
            if (!domanda.FamiliareFornito.ControlloCodiceFiscale.HasValue)
                ButtonForzaCodiceFiscale.Enabled = true;
            else
                ButtonForzaCodiceFiscale.Enabled = false;
        }

        CaricaSemafori(domanda);
        PrestazioniDatiDomanda1.CaricaDomanda(domanda);
    }

    private void CaricaSemafori(Domanda domanda)
    {
        BulletedListControlli.Items.Clear();

        if (domanda.IdTipoPrestazione == "PRENAT" && domanda.Familiare != null && !domanda.Familiare.DataNascita.HasValue)
            ButtonForzaDataNascita.Enabled = false;

        if (domanda.Familiare != null)
        {
            ImageControlloFamiliareSelezionato.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.Familiare.ControlloFamiliareSelezionato);
            if (domanda.Familiare.ControlloFamiliareSelezionato.HasValue &&
                !domanda.Familiare.ControlloFamiliareSelezionato.Value)
                BulletedListControlli.Items.Add("La domanda non � stata abbinata a nessun familiare dell'anagrafica");

            ImageControlloDataDecesso.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.Familiare.ControlloDataDecesso);
            if (domanda.Familiare.ControlloDataDecesso.HasValue && !domanda.Familiare.ControlloDataDecesso.Value)
                BulletedListControlli.Items.Add("Il familiare a cui � stata abbinata la domanda risulta deceduto");

            ImageControlloGradoParentela.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.Familiare.ControlloGradoParentela);
            if (domanda.Familiare.ControlloGradoParentela.HasValue && !domanda.Familiare.ControlloGradoParentela.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non ha un grado di parentela nell'anagrafica");

            ImageControlloFamiliareDataNascita.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.Familiare.ControlloDataNascita);
            if (domanda.Familiare.ControlloDataNascita.HasValue && !domanda.Familiare.ControlloDataNascita.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non ha una data di nascita nell'anagrafica");

            ImageControlloFamiliareCodiceFiscale.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.Familiare.ControlloCodiceFiscale);
            if (domanda.Familiare.ControlloCodiceFiscale.HasValue && !domanda.Familiare.ControlloCodiceFiscale.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non ha un codice fiscale nell'anagrafica");

            ImageControlloACarico.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.Familiare.ControlloACarico);
            if (domanda.Familiare.ControlloACarico.HasValue && !domanda.Familiare.ControlloACarico.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non risulta a carico del lavoratore");
        }
        else if (domanda.FamiliareFornito != null)
        {
            ImageControlloFamiliareSelezionato.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.FamiliareFornito.ControlloFamiliareSelezionato);
            if (domanda.FamiliareFornito.ControlloFamiliareSelezionato.HasValue &&
                !domanda.FamiliareFornito.ControlloFamiliareSelezionato.Value)
                BulletedListControlli.Items.Add("La domanda non � stata abbinata a nessun familiare dell'anagrafica");

            ImageControlloDataDecesso.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.FamiliareFornito.ControlloDataDecesso);
            if (domanda.FamiliareFornito.ControlloDataDecesso.HasValue &&
                !domanda.FamiliareFornito.ControlloDataDecesso.Value)
                BulletedListControlli.Items.Add("Il familiare a cui � stata abbinata la domanda risulta deceduto");

            ImageControlloGradoParentela.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.FamiliareFornito.ControlloGradoParentela);
            if (domanda.FamiliareFornito.ControlloGradoParentela.HasValue &&
                !domanda.FamiliareFornito.ControlloGradoParentela.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non ha un grado di parentela nell'anagrafica");

            ImageControlloFamiliareDataNascita.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.FamiliareFornito.ControlloDataNascita);
            if (domanda.FamiliareFornito.ControlloDataNascita.HasValue &&
                !domanda.FamiliareFornito.ControlloDataNascita.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non ha una data di nascita nell'anagrafica");

            ImageControlloFamiliareCodiceFiscale.ImageUrl =
                biz.ConvertiBoolInSemaforo(domanda.FamiliareFornito.ControlloCodiceFiscale);
            if (domanda.FamiliareFornito.ControlloCodiceFiscale.HasValue &&
                !domanda.FamiliareFornito.ControlloCodiceFiscale.Value)
                BulletedListControlli.Items.Add(
                    "Il familiare a cui � stata abbinata la domanda non ha un codice fiscale nell'anagrafica");
        }
    }

    protected void ButtonSelezionaFamiliare_Click(object sender, EventArgs e)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];

        PrestazioniRicercaFamiliare1.Visible = true;
        FamiliareCollection familiari = biz.GetFamiliariPerGradoParentela(
            domanda.Lavoratore.IdLavoratore.Value, domanda.Beneficiario, false);
        PrestazioniRicercaFamiliare1.CaricaFamiliari(familiari);
    }

    private void PrestazioniRicercaFamiliare1_OnFamiliareSelected(Familiare familiare)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];

        if (biz.UpdateDomandaSetFamiliareDaAnagrafica(domanda.IdDomanda.Value, familiare.IdFamiliare.Value))
        {
            //if ( domanda.IdTipoPrestazione
            Domanda domandaAgg = biz.GetDomanda(domanda.IdDomanda.Value);
            biz.ControllaDomandaFamiliare(domandaAgg);
            biz.ControllaDomandaDocumenti(domandaAgg);
            biz.ControllaDomandaUnivocita(domandaAgg);
            ResetSelezioneFamiliare();
            CaricaDomanda(domandaAgg);

            //comunichiamo al CRM il cambio di beneficiario, le eccezioni del WS non vengono gestire a questo livello
            if (!Common.Sviluppo)
            {
                biz.CallCrmWsPrestazioni(domandaAgg.IdDomanda.Value);
            }
        }
    }

    private void ResetSelezioneFamiliare()
    {
        PrestazioniRicercaFamiliare1.Visible = false;

        LabelAnagraficaFamiliareCodice.Text = null;
        LabelAnagraficaFamiliareCodiceFiscale.Text = null;
        LabelAnagraficaFamiliareCognome.Text = null;
        LabelAnagraficaFamiliareDataDecesso.Text = null;
        LabelAnagraficaFamiliareDataNascita.Text = null;
        LabelAnagraficaFamiliareGradoParentela.Text = null;
        LabelAnagraficaFamiliareNome.Text = null;
    }

    protected void ButtonForzaDataNascita_Click(object sender, EventArgs e)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];

        if (biz.ForzaFamiliareDataNascita(domanda.IdDomanda.Value))
        {
            biz.ControllaDomandaFamiliare(domanda);
            CaricaDomanda(domanda);
        }
    }

    protected void ButtonForzaDataDecesso_Click(object sender, EventArgs e)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];

        if (biz.ForzaFamiliareDataDecesso(domanda.IdDomanda.Value))
        {
            biz.ControllaDomandaFamiliare(domanda);
            CaricaDomanda(domanda);
        }
    }

    protected void ButtonForzaCodiceFiscale_Click(object sender, EventArgs e)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];

        if (biz.ForzaFamiliareCodiceFiscale(domanda.IdDomanda.Value))
        {
            biz.ControllaDomandaFamiliare(domanda);
            CaricaDomanda(domanda);
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];
        Context.Items["IdDomanda"] = domanda.IdDomanda.Value;

        Server.Transfer("~/Prestazioni/ControlloDomanda.aspx");
    }

    protected void ButtonForzaACarico_Click(object sender, EventArgs e)
    {
        Domanda domanda = (Domanda) ViewState["Domanda"];

        if (biz.ForzaFamiliareACarico(domanda.IdDomanda.Value))
        {
            biz.ControllaDomandaFamiliare(domanda);
            CaricaDomanda(domanda);
        }
    }
}