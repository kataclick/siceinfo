using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Delegates;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Business;

public partial class Prestazioni_ControlloDomandaDocumenti : Page
{
    private const int INDICEANNULLA = 8;
    private const int INDICEORIGINALE = 5;
    private const int INDICERICERCA = 7;
    private const int INDICEVISUALIZZA = 6;

    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande,
                                              "~/Prestazioni/ControlloDomandaDocumenti.aspx");

        #endregion

        PrestazioniRicercaDocumenti1.OnDocumentoSelected += PrestazioniRicercaDocumenti1_OnDocumentoSelected;
        PrestazioniAssociazioneDocumento1.OnAssociazioneEffettuata +=
            new AssociazioneEffettuataEventHandler(PrestazioniAssociazioneDocumento1_OnAssociazioneEffettuata);

        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDomanda"] != null)
            {
                int idDomanda = (int) Context.Items["IdDomanda"];
                ViewState["IdDomanda"] = idDomanda;

                Domanda domanda = biz.GetDomanda(idDomanda);
                ViewState["stato"] = domanda.Stato.IdStato;

                //Verifichiamo se la domanda pu� essere gestita
                ViewState.Add("permettiGestioneDomanda", AbilitaGestione(domanda));

                CaricaDocumenti(domanda);

                ControllaStatoDomanda(domanda);

                ViewState.Add("stato", domanda.Stato.IdStato);

                //Verifichiamo se la domanda pu� essere gestita
                //TODO non � detto che qui serva
                ViewState.Add("permettiGestioneDomanda", AbilitaGestione(domanda));
            }
            else
            {
                Server.Transfer("~/Prestazioni/GestioneDomande.aspx");
            }
        }
    }

    /// <summary>
    /// Abilita le azioni che pu� fare l'utente in funzione del ruolo e della propriet� della domanda 
    /// </summary>
    /// <param name="domanda">Domanda in gestione</param>
    private bool AbilitaGestione(Domanda domanda)
    {
        //IUtente utente = ApplicationInstance.GetUtenteSistema();
        Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
        //Se l'utente � chi ha la domanda in carico o ha funzioni amministrative, pu� getire la domanda
        if (idUtente == domanda.IdUtenteInCarico ||
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin.ToString()))
        {
            //Tutto normale
            return true;
        }
        else
        {
            //Tutto viene disabilitato
            PanelControllaDomandaAzioni.Enabled = false;
            //GridViewDocumenti.Enabled = false; 
            //Non va bene i documenti devono poter essere visualizzabili, quindi non si pu� disabilitare tutto il gridview
            //in alternativa viene usato il ViewState["permettiGestioneDomanda"].
            return false;
        }
    }

    /// <summary>
    /// in funzione dello stato della domanda decidiamo quali azioni lasciare all'utente5
    /// </summary>
    /// <param name="domanda"></param>
    private void ControllaStatoDomanda(Domanda domanda)
    {
        if (StatoDomanda.StatoDomandaModificabile(domanda.Stato.IdStato))
            //domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O"
        {
            PanelControllaDomandaAzioni.Enabled = true;
        }
        else
        {
            PanelControllaDomandaAzioni.Enabled = false;
        }
    }

    private void CaricaDocumenti(Domanda domanda)
    {
        DocumentoCollection documentiNecessari = biz.GetDocumentiNecessariPiuRecenti(domanda.IdDomanda.Value);

        if (documentiNecessari.Count == 0)
            ButtonAccettaDocumentazione.Enabled = false;

        if (domanda.ControlloPresenzaDocumenti.HasValue && domanda.ControlloPresenzaDocumenti.Value)
            ButtonAccettaDocumentazione.Enabled = false;

        GridViewDocumenti.DataSource = documentiNecessari;
        GridViewDocumenti.DataBind();

        PrestazioniDatiDomanda1.CaricaDomanda(domanda);
    }

    protected void ButtonAccettaDocumentazione_Click(object sender, EventArgs e)
    {
        int idDomanda = (int) ViewState["IdDomanda"];
        bool ok = biz.ForzaPresenzaDocumenti(idDomanda);

        Domanda domanda = biz.GetDomanda(idDomanda);

        biz.ControllaDomandaDocumenti(domanda);

        CaricaDocumenti(domanda);
    }

    protected void GridViewDocumenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Documento documento = (Documento) e.Row.DataItem;

            Image iPresente = (Image) e.Row.FindControl("ImagePresente");
            Label lDataScansione = (Label) e.Row.FindControl("LabelDataScansione");
            CheckBox checkBoxOriginale = (CheckBox) e.Row.FindControl("CheckBoxOriginale");

            //se il documento ha un valore "Originale" visualizziamo il templatefield e il valore
            if (documento.Originale.HasValue)
            {
                checkBoxOriginale.Visible = true;
                checkBoxOriginale.Checked = documento.Originale.Value;
            }

            if (documento.DataScansione.HasValue)
            {
                lDataScansione.Text = documento.DataScansione.Value.ToString("dd/MM/yyyy");
            }

            //Prima disabilitiamo tutti i tasti poi in funzione delle variabili modifichiamo
            e.Row.Cells[INDICEVISUALIZZA].Enabled = false;
            e.Row.Cells[INDICEANNULLA].Enabled = false;
            e.Row.Cells[INDICERICERCA].Enabled = false;

            //Se il documento richiesto non ha un documento associato permetttiamo l'associazione
            if (string.IsNullOrEmpty(documento.IdArchidoc))
            {
                e.Row.Cells[INDICERICERCA].Enabled = true;
            }
            else
            {
                //altrimenti permettiamo la visualizzazione e l'annullamento dell'associazione
                e.Row.Cells[INDICEVISUALIZZA].Enabled = true;
                e.Row.Cells[INDICEANNULLA].Enabled = true;
            }

            //se la domanda � gi� stata accolta o l'utente che sta visualizzando la pagina non ha in carico la domanda 
            //permettiamo solo la visualizzazione
            //Partiamo dal risultato degli stati dei bottoni precedente ed applichiamo altre regole pi� restrittive
            if (StatoDomanda.StatoDomandaModificabile((string) ViewState["stato"])
                && (bool) ViewState["permettiGestioneDomanda"])
                //(string)ViewState["stato"] == "I" || (string)ViewState["stato"] == "T" || (string)ViewState["stato"] == "O")
            {
                //Se la domanda non � in uno stato definito lasciamo tutto com'�
            }
            else
            {
                //disabilitiamo ricerca e annullamento del documento
                e.Row.Cells[INDICERICERCA].Enabled = false;
                e.Row.Cells[INDICEANNULLA].Enabled = false;
                //la visualizzazione rimane condizionata dalla presenza dell'idarchidoc
            }

            //Gestiamo il semaforo del documento
            iPresente.ImageUrl = biz.ConvertiBoolInSemaforo(!string.IsNullOrEmpty(documento.IdArchidoc));
                //prima, quando idarch era int, il controllo era .HasValue;
        }
    }

    protected void GridViewDocumenti_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        LabelErroreVisualizzazioneDocumento.Visible = false;
        int indice = Int32.Parse(e.CommandArgument.ToString());
        bool perPrestazione = (bool) GridViewDocumenti.DataKeys[indice].Values["PerPrestazione"];
        string relativoA = (string) GridViewDocumenti.DataKeys[indice].Values["RiferitoA"];
        TipoDocumento tipoDocumento = (TipoDocumento) GridViewDocumenti.DataKeys[indice].Values["TipoDocumento"];

        switch (e.CommandName)
        {
            case "visualizza":
                string idArchidoc = (string) GridViewDocumenti.DataKeys[indice].Values["IdArchidoc"];
                // Caricare il documento tramite il Web Service Archidoc
                try
                {
                    IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                    byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                    if (file != null)
                    {
                        Presenter.RestituisciFileArchidoc(idArchidoc, file, this);
                    }
                }
                catch
                {
                    LabelErroreVisualizzazioneDocumento.Visible = true;
                }
                break;
            case "ricerca":
                PanelRicercaDocumenti.Visible = true;
                PrestazioniRicercaDocumenti1.PreImpostaTipoDocumento(tipoDocumento, perPrestazione, relativoA);

                PrestazioniAssociazioneDocumento1.ResetCampiAssociazione();
                PanelAssociazione.Visible = false;
                break;
            case "annulla":
                int idDocumento = (int) GridViewDocumenti.DataKeys[indice].Values["IdDocumento"];

                biz.DisassociaDocumento(tipoDocumento.IdTipoDocumento, idDocumento);
                int idDomanda = (int) ViewState["IdDomanda"];
                Domanda domanda = biz.GetDomanda(idDomanda);

                biz.ControllaDomandaDocumenti(domanda);
                CaricaDocumenti(domanda);

                //Notifichiamo al CRM un cambio nella documentazione (in questo caso disassociazione di un documento)
                //le eccezioni del WS non vengono gestire a questo livello
                if (!Common.Sviluppo)
                {
                    biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                }
                break;
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        int idDomanda = (int) ViewState["IdDomanda"];
        Context.Items["IdDomanda"] = idDomanda;

        Server.Transfer("~/Prestazioni/ControlloDomanda.aspx");
    }

    private void PrestazioniRicercaDocumenti1_OnDocumentoSelected(Documento documento)
    {
        PanelAssociazione.Visible = true;
        int idDomanda = (int) ViewState["IdDomanda"];

        Domanda domanda = biz.GetDomanda(idDomanda);
        PrestazioniAssociazioneDocumento1.ImpostaAssociazione(documento, domanda);
    }

    private void PrestazioniAssociazioneDocumento1_OnAssociazioneEffettuata()
    {
        int idDomanda = (int) ViewState["IdDomanda"];
        Domanda domanda = biz.GetDomanda(idDomanda);

        PanelRicercaDocumenti.Visible = false;
        biz.ControllaDomandaDocumenti(domanda);
        CaricaDocumenti(domanda);
    }
}