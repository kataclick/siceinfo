<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlloDomanda.aspx.cs" Inherits="Prestazioni_ControlloDomanda" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda"
        titolo="Prestazioni" />
    <br />
    <asp:Panel ID="PanelControllaDomanda" runat="server">
        <asp:Button
            ID="ButtonIndietro"
            runat="server"
            Text="Torna alla Ricerca"
            Width="150px" onclick="ButtonIndietro_Click" />
        <br />
        <br />
        <table class="borderedTable">
            <tr>
                <td style="width: 207px">
                    Tipo prestazione
                </td>
                <td colspan="2">
                    <b>
                        <asp:Label ID="LabelTipoPrestazione" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Protocollo
                </td>
                <td colspan="2">
                    <b>
                        <asp:Label ID="LabelProtocollo" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Stato domanda
                </td>
                <td colspan="2">
                    <b>
                        <asp:Label ID="LabelStato" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr id="trProtesiCure" runat="server" visible="false">
                <td style="width: 207px">
                </td>
                <td colspan="2">
                    <table class="standardTable">
                        <tr>
                            <td style="width: 80px">
                                Protesi:
                            </td>
                            <td>
                                <asp:Label ID="LabelStatoPL" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 80px">
                                Cure:
                            </td>
                            <td>
                                <asp:Label ID="LabelStatoCL" runat="server" Text="-"></asp:Label>
                            </td>
                        </tr>
                    </table>    
                </td>
            </tr>
            <tr>
                <td style="width: 207px; vertical-align:top;">
                    Nota
                </td>
                <td colspan="2">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:ImageButton
                                ID="ImageButtonEdit"
                                runat="server" Height="16px" ImageUrl="~/images/edit.png" 
                                onclick="ImageButtonEdit_Click" Width="16px" />
                            <asp:Label ID="LabelNota" runat="server"></asp:Label>
                            <br />
                            <asp:Panel
                                ID="PanelNotaEdit"
                                runat="server"
                                Visible="false">
                                <asp:TextBox
                                    ID="TextBoxNota"
                                    runat="server"
                                    Width="300px"
                                    MaxLength="500">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidatorNota"
                                    runat="server"
                                    ErrorMessage="*"
                                    ForeColor="Red"
                                    ControlToValidate="TextBoxNota"
                                    ValidationGroup="salvaNota">
                                </asp:RequiredFieldValidator>
                                &nbsp;
                                <asp:Button
                                    ID="ButtonNotaSalva"
                                    runat="server"
                                    Text="Salva"
                                    CausesValidation="true"
                                    ValidationGroup="salvaNota"
                                    Width="70px" onclick="ButtonNotaSalva_Click" />
                                &nbsp;
                                <asp:Button
                                    ID="ButtonNotaAnnulla"
                                    runat="server"
                                    Text="Annulla"
                                    CausesValidation="false"
                                    Width="70px" onclick="ButtonNotaAnnulla_Click" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr runat="server" id="trTipoCausale" visible="false">
                <td style="width: 207px">
                    Causale
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelTipoCausale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Data presentazione
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelDataConferma" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Data riferimento
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelDataRiferimento" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Data Gestione
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelDataGestione" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Beneficiario
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Codice lavoratore
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelLavoratoreCodice" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Lavoratore
                </td>
                <td colspan="2">
                    <b>
                    <asp:Label ID="LabelLavoratoreCognome" runat="server"></asp:Label>
                    &nbsp;<asp:Label ID="LabelLavoratoreNome" runat="server"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Numero fatture dichiarato alla consegna
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelNumeroFatture" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Modalit� di inserimento domanda
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelTipoInserimento" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table class="borderedTable">
            <tr>
                <td style="width: 207px">
                    Dati anagrafici&nbsp;
                </td>
                <td>
                    <asp:Image ID="ImageControlloIndirizzo" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloIndirizzo" runat="server" Text="Controlla" Width="160px"  OnClick="ButtonControlloIndirizzo_Click" />
                </td>
            </tr>
            <tr id="tdControlloFamiliare" runat="server">
                <td style="width: 207px">
                    Controllo familiare
                </td>
                <td>
                    <asp:Image ID="ImageControlloFamiliare" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloFamiliare" runat="server" Text="Controlla" Width="160px"  OnClick="ButtonControlloFamiliare_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Controllo presenza documenti
                </td>
                <td>
                    <asp:Image ID="ImageControlloPresenzaDocumenti" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloPresenzaDocumenti" runat="server" Text="Controlla" Width="160px" 
                        OnClick="ButtonControlloPresenzaDocumenti_Click" />
                </td>
            </tr>
            <tr id="trControlloFatture" runat="server">
                <td style="width: 207px">
                    Controllo fatture
                </td>
                <td>
                    <asp:Image ID="ImageControlloFatture" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloFatture" runat="server" Text="Controlla" Width="160px"  OnClick="ButtonControlloFatture_Click" />
                </td>
            </tr>
            <tr id="trControlloScolastiche" runat="server">
                <td style="width: 207px">
                    Controllo dati aggiuntivi (scolastici, funerario, handicap)
                </td>
                <td>
                    <asp:Image ID="ImageControlloScolastiche" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloScolastiche" runat="server" Text="Controlla" Width="160px"  OnClick="ButtonControlloScolastiche_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    Controllo univocit� prestazione
                </td>
                <td>
                    <asp:Image ID="ImageControlloUnivocita" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloUnivocitaPrestazione" runat="server" Text="Controlla" Width="160px" 
                        OnClick="ButtonControlloUnivocitaPrestazione_Click" />
                </td>
            </tr>
            <tr id="tdControlloOreCNCE" runat="server">
                <td style="width: 207px">
                    Controllo ore CNCE
                </td>
                <td>
                    <asp:Image ID="ImageControlloOreCNCE" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Button ID="ButtonControlloOreCNCE" runat="server" Text="Controlla" Width="160px"  OnClick="ButtonControlloOreCNCE_Click" />
                </td>
            </tr>
        </table>
        <br />
        <table class="borderedTable">
            <tr id="TrRecuperaModulo" runat="server">
                <td style="width: 140px">
                    Recupera modulo domanda
                </td>
                <td>
                    <asp:ImageButton ID="ImageButtonRecuperaModulo" runat="server" ImageUrl="~/images/pdf.gif"
                        OnClick="ImageButtonRecuperaModulo_Click" Style="height: 16px" />
                </td>
                <td>
                </td>
            </tr>
            <tr id="TrRecuperaRicevuta" runat="server">
                <td style="width: 140px">
                    Recupera ricevuta
                </td>
                <td>
                    <asp:ImageButton ID="ImageButtonRecuperaRicevuta" runat="server" ImageUrl="~/images/txt.gif"
                        OnClick="ImageButtonRecuperaRicevuta_Click" />
                </td>
                <td>
                    <asp:Button ID="ButtonGestioneRicevuta" runat="server" Text="Gestione ricevuta" Width="160px" OnClick="ButtonGestioneRicevuta_Click" />
                </td>
            </tr>
            <tr id="trRestituzioneFatture" runat="server">
                <td style="width: 140px">
                    Restituzione fatture
                </td>
                <td>
                    <asp:ImageButton ID="ImageButtonRestituzioneFatture" runat="server" 
                        ImageUrl="~/images/txt.gif" onclick="ImageButtonRestituzioneFatture_Click"
                         />
                </td>
                <td>
                    
                </td>
            </tr>
            <tr id="trCopertine" runat="server">
                <td style="width: 140px">
                    Copertine
                </td>
                <td>
                    <asp:ImageButton ID="ImageButtonCopertine" runat="server" 
                        ImageUrl="~/images/cartella.png" onclick="ImageButtonCopertine_Click" 
                         />
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Panel 
                        ID="PanelControllaDomandaAzioni" 
                        runat="server">
                        <asp:Button 
                            ID="ButtonAccogli" 
                            runat="server" 
                            Enabled="false" 
                            OnClick="ButtonAccogli_Click"
                            Text="Accogli la domanda" 
                            Width="160px" />
                        <asp:Button 
                            ID="ButtonAnnulla" 
                            runat="server" 
                            Enabled="false" 
                            OnClientClick="return confirm('La domanda verr� annullata. Confermi?');"
                            OnClick="ButtonAnnulla_Click" 
                            Text="Annulla la domanda" 
                            Width="160px" />
                        <br />
                        <asp:Button 
                            ID="ButtonAttesa" 
                            runat="server" 
                            Enabled="false" 
                            OnClick="ButtonAttesa_Click"
                            Text="In attesa documentazione" 
                            Width="160px" />
                        <asp:Button 
                            ID="ButtonAttesaDenuncia" 
                            runat="server" 
                            Enabled="false" 
                            Text="In attesa denuncia" 
                            Width="160px" 
                            onclick="ButtonAttesaDenuncia_Click" />
                        <br />
                        <asp:Button 
                            ID="ButtonEsame" 
                            runat="server" 
                            Enabled="true" 
                            Text="In esame" 
                            Width="160px" 
                            onclick="ButtonEsame_Click"
                            Visible="true" />
                        <br />
                        <br />
                        Nel caso la domanda non sia corretta, selezionare prima la causale:<br />
                        <asp:DropDownList ID="DropDownListCausaleRifiuto" runat="server" Width="300px" AppendDataBoundItems="True"
                            ValidationGroup="respingimentoDomanda">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListCausaleRifiuto"
                            ErrorMessage="Impostare la causale" ValidationGroup="respingimentoDomanda">*</asp:RequiredFieldValidator>
                        <br />
                        e poi premere il tasto "Respingi":<br />
                        <asp:Button ID="ButtonRespingi" runat="server" Enabled="false" OnClientClick="return confirm('La domanda verr� respinta. Confermi?');"
                            OnClick="ButtonRespingi_Click" Text="Respingi" ValidationGroup="respingimentoDomanda"
                            Width="300px" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
