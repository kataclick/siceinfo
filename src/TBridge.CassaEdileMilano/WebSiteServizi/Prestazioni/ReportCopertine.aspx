﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ReportCopertine.aspx.cs" Inherits="Prestazioni_ReportCopertine" %>

<%@ Register src="../WebControls/MenuPrestazioni.ascx" tagname="MenuPrestazioni" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Prestazioni" sottoTitolo="Ricevuta Fast" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:reportviewer id="ReportViewer" runat="server" height="600px"
        processingmode="Remote" width="100%" ShowDocumentMapButton="False" ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:reportviewer>
    </td></tr></table>
    
</asp:Content>


