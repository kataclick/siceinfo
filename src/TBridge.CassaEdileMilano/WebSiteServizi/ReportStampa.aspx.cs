using System;
using System.Web.UI;

public partial class ReportStampa : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["titolo"] != null)
        {
            string titolo = Request.QueryString["titolo"];
            Title = titolo;

            LabelTitolo.Text = titolo;
        }
    }
}