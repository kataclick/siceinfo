<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneLavoratori.aspx.cs" Inherits="GestioneUtentiRegistrazioneLavoratori" %>

<%@ Register Src="WebControls/GestioneRegistrazione.ascx" TagName="GestioneRegistrazione"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/RegistrazioneLavoratore.ascx" TagName="RegistrazioneLavoratore"
    TagPrefix="uc1" %>
    
    <%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>

    
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">

    <uc3:TitoloSottotitolo ID="TitoloSottotitolo2"  titolo="Gestione utenti" sottoTitolo="Registrazione lavoratore" runat="server" />
    <br />
    
    <uc2:GestioneRegistrazione ID="GestioneRegistrazione1" runat="server" Visible="false" />
    <br />
    
    <uc1:RegistrazioneLavoratore ID="RegistrazioneLavoratore1" runat="server" />
</asp:Content>

