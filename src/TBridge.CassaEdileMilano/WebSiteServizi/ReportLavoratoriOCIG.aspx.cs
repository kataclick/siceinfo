using System;
using System.Configuration;
using System.Web.UI;

public partial class ReportLavoratoriOCIG : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //barbirato / damicco
        if ((TextBox2.Text == "barbirato" && TextBox1.Text == "ba15toapr")
            ||
            (TextBox2.Text == "damicco" && TextBox1.Text == "da83coapr")
            ||
            (TextBox2.Text == "edilgacemi" && TextBox1.Text == "edilgacemi"))
        {
            Label3.Text = "Accesso consentito";
            ReportViewerColonie.Visible = true;
            Panel1.Visible = false;

            ReportViewerColonie.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            ReportViewerColonie.ServerReport.ReportPath = "/R_AnagraficaLavoratori/AnagraficaLavoratoriOCIG";
        }
        else Label3.Text = "Accesso negato";
    }
}