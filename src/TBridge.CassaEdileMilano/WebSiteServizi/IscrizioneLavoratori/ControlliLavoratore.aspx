﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlliLavoratore.aspx.cs" Inherits="IscrizioneLavoratori_ControlliLavoratore" %>

<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/VerificaDettagliLavoratore.ascx" TagName="VerificaDettagliLavoratore"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori"
        sottoTitolo="Controlli Lavoratore" />
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Torna al dettaglio" Width="200px"
        OnClick="ButtonIndietro_Click" />
    <br />
    <br />
    <uc3:VerificaDettagliLavoratore ID="VerificaDettagliLavoratore1" runat="server" />
</asp:Content>
