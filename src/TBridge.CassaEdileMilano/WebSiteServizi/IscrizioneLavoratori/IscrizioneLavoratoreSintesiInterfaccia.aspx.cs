﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using System.Configuration;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class IscrizioneLavoratori_IscrizioneLavoratoreSintesiInterfaccia : System.Web.UI.Page
{
    private readonly SintesiManager _sintesiManager = new SintesiManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);

        #endregion
    }


    protected void ButtonCaricaComunicazione_OnClick(object sender, EventArgs e)
    {
        CaricaRapportoDiLavoro();
    }


    private void CaricaRapportoDiLavoro()
    {
        //string username = ConfigurationManager.AppSettings["SintesiUsernameWS"];
        //string password = ConfigurationManager.AppSettings["SintesiPasswordWS"];
        string idComunicazione = TextBoxCodiceComunicazione.Text;
        
        RapportoLavoro rapportoLavoro;

        //RapportoLavoro rapportoLavoro = sintesiManager.GetRapportoLavoro(idComunicazione, username, password);

        //if (rapportoLavoro != null)
        //{
        //    //IscrizioneLavoratore1.RapportoLavoro = rapportoLavoro;
        //    //IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;

        //    if (!(rapportoLavoro.Item is Cessazione || rapportoLavoro.Item is Contratto))
        //    {
        //        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneNonGestita.aspx");
        //    }
        //    else if (!IsComuniazioneValidaPerImpresa(rapportoLavoro))
        //    {
        //        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneImpresaErrata.aspx");
        //    }
        //    else
        //    {
        //        IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;
        //        IscrizioneLavoratore1.CaricaRapportoDiLavoro(rapportoLavoro);
        //        PanelCodiceComunicaione.Visible = false;
        //        IscrizioneLavoratore1.Visible = true;
        //    }
        //}
        //else
        //{
        //    Response.Redirect("~/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx");
        //}

        TBridge.Cemi.IscrizioneLavoratori.Type.Entities.SintesiServiceResult result = _sintesiManager.GetRapportoLavoro(idComunicazione,  out rapportoLavoro);
        switch (result.Error)
        {
            case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.NoError:
                if (!(rapportoLavoro.Item is Contratto))
                {
                    Server.Transfer("~/IscrizioneLavoratori/ComunicazioneNonGestita.aspx");
                }
                else if (!IsComuniazioneValidaPerImpresa(rapportoLavoro))
                {
                    Server.Transfer("~/IscrizioneLavoratori/ComunicazioneImpresaErrata.aspx");
                }
                else if (!IsComuniazioneValidaPerConsulente(rapportoLavoro))
                {
                    Context.Items["ErrorMessage"] = "La comunicazione risulta associata ad un'impresa non di competenza.";
                    Server.Transfer("~/IscrizioneLavoratori/ComunicazioneErrore.aspx");
                }
                else if (_sintesiManager.DichiarazioneExistByIdComunicazioneSintesi(idComunicazione))
                {
                    Server.Transfer("~/IscrizioneLavoratori/ComunicazioneGiaEffettuata.aspx");
                }
                else if (!_sintesiManager.IsDataAssunzioneValida(rapportoLavoro))
                {
                    Server.Transfer("~/IscrizioneLavoratori/ComunicazioneDataAssunzioneNonValida.aspx");
                }
                else
                {
                    IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;
                    IscrizioneLavoratore1.CaricaRapportoDiLavoro(rapportoLavoro);
                    PanelCodiceComunicaione.Visible = false;
                    IscrizioneLavoratore1.Visible = true;
                }
                break;
            case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.ServiceCallFailed:
            case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.InvalidResponse:
                Server.Transfer("~/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx");
                break;
            case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.ServiceErrorMessage:
                Context.Items["ErrorMessage"] = result.Message;
                Server.Transfer("~/IscrizioneLavoratori/ComunicazioneErrore.aspx");
                break;
            case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.InvalidId:
                Context.Items["ErrorMessage"] = "Id comunicazione non valido.";
                Server.Transfer("~/IscrizioneLavoratori/ComunicazioneErrore.aspx");
                break;
            default:
                break;
        }
    }


    private Boolean IsComuniazioneValidaPerImpresa(RapportoLavoro rapportoLavoro)
    {
        Boolean result = true;

        if (GestioneUtentiBiz.IsImpresa())
        {
            Impresa impresaCorrente = (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            result = (String.Equals(rapportoLavoro.DatoreLavoro.codiceFiscale, impresaCorrente.CodiceFiscale, StringComparison.CurrentCultureIgnoreCase));

        }

        return result;

    }

    private Boolean IsComuniazioneValidaPerConsulente(RapportoLavoro rapportoLavoro)
    {
        Boolean result = true;

        if (GestioneUtentiBiz.IsConsulente())
        {
            Consulente consulenteCorrente = (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            result = _sintesiManager.IsImpresaCompetenzaConsulente(consulenteCorrente.IdConsulente, rapportoLavoro.DatoreLavoro.codiceFiscale);

        }

        return result;

    }
}