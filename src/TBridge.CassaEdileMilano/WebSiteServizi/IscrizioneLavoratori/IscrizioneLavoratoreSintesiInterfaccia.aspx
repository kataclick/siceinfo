﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreSintesiInterfaccia.aspx.cs" Inherits="IscrizioneLavoratori_IscrizioneLavoratoreSintesiInterfaccia"
    MasterPageFile="~/MasterPage.master"  MaintainScrollPositionOnPostback="true" EnableEventValidation="true" Title=""%>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/Sintesi/IscrizioneLavoratoreSintesi.ascx" TagName="IscrizioneLavoratoreSintesi"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori"
    TagPrefix="uc3" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Iscrizione via Sintesi"
        titolo="Notifiche lavoratori" />
    <br />
    <asp:Panel ID="PanelCodiceComunicaione" runat="server">
        <table>
            <tr>
                <td colspan="2">
                    Codice comunicazione:
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceComunicazione" runat="server"
                         ValidationGroup="datiComunicazione"
                         ControlToValidate ="TextBoxCodiceComunicazione" 
                         ErrorMessage = "Inserire codice comunicazione" 
                         ForeColor="Red"
                         >
                    *
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodiceComunicazione" runat="server"
                            ValidationExpression="^\d{16}$"
                         ValidationGroup="datiComunicazione"
                         ControlToValidate ="TextBoxCodiceComunicazione" 
                         ErrorMessage = "Codice comunicazione non valido" 
                         ForeColor="Red"
                         >
                         *
                    </asp:RegularExpressionValidator>

                </td>               
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="TextBoxCodiceComunicazione" runat="server" Width="250px"></asp:TextBox>
                </td>
                <td >
                    <asp:Button ID="ButtonCaricaComunicazione" runat="server" 
                        OnClick="ButtonCaricaComunicazione_OnClick" Text="Carica comunicazione"
                                   OnClientClick="javascript :if ( Page_ClientValidate() ) {this.disabled = true; this.value = 'Attendi...';}"
                                    UseSubmitBehavior="false"
                                 Width="190px"/>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummaryCominicazione" runat="server" ValidationGroup="datiComunicazione" ForeColor="Red"/>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <uc2:IscrizioneLavoratoreSintesi ID="IscrizioneLavoratore1" runat="server" Visible="false"  />
</asp:Content>

<asp:Content ID="Content8" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>