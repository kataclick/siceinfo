﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ComunicazioneNonGestita.aspx.cs" Inherits="IscrizioneLavoratori_ComunicazioneNonGestita" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Errore comunicazione" />
    <br />
    <table class="standardTable">
    <tr>
    <td>
    Il tipo di rapporto non è gestito.
    </td>
    </tr>
    </table>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>


