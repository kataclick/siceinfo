using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneLavoratori_ControlliLavoratore : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE,
                                              "GestioneCE.aspx");

        #endregion

        if (!Page.IsPostBack)
        {
            ViewState["IdDichiarazione"] = Context.Items["IdDichiarazione"];
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Context.Items["IdDichiarazione"] = ViewState["IdDichiarazione"];
        Server.Transfer("~/IscrizioneLavoratori/DettaglioDichiarazione.aspx");
    }
}