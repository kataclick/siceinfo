<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IscrizioneLavoratoriDefault.aspx.cs" Inherits="IscrizioneLavoratori_IscrizioneLavoratoriDefault" Title="Iscrizione telematica lavoratore" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" 
        sottoTitolo="Benvenuto nella sezione Iscrizione Lavoratori" 
        titolo="Notifiche lavoratori" />
    <br />
    Cassa Edile di Milano ha sottoscritto apposite convenzioni con le Province della propria circoscrizione territoriale (Milano, Lodi, Monza e Brianza), al fine di recepire le informazioni riportate nella Comunicazione Obbligatoria (Unificato Lav) di instaurazione di rapporto di lavoro (dati identificativi del datore di lavoro, del lavoratore e del rapporto di lavoro) da inoltrare per via telematica ai Servizi per l�impiego.
In questa sezione � possibile completare e confermare i dati derivanti dall�applicativo delle Province �S.Inte.S.I.� (<b>S</b>istema <b>Int</b>egrato dei <b>S</b>ervizi per l�<b>I</b>mpiego) relativamente alla tipologia di comunicazione sopra riportata.
I dati, previa verifica e validazione da parte degli operatori Cassa Edile, verranno trasmessi in automatico alla denuncia telematica mensile di manodopera occupata (MUT) del mese di riferimento.

</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>

