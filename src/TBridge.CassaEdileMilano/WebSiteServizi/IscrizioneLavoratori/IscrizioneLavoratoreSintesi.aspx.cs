using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;

public partial class IscrizioneLavoratori_IscrizioneLavoratoreSintesi : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FormsAuthentication.SetAuthCookie("UtenteSintesi", false);
            //string username = ConfigurationManager.AppSettings["SintesiUsernameWS"];
            //string password = ConfigurationManager.AppSettings["SintesiPasswordWS"];
            string idComunicazione = Request["idComunicazione"];
            SintesiManager sintesiManager = new SintesiManager();
            RapportoLavoro rapportoLavoro;
            //RapportoLavoro rapportoLavoro = sintesiManager.GetRapportoLavoro(idComunicazione, username, password);

            //if (rapportoLavoro != null)
            //{
            //    IscrizioneLavoratore1.RapportoLavoro = rapportoLavoro;
            //    IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;
            //    FormsAuthentication.SetAuthCookie("UtenteSintesi", false);
            //    Master.FindControl("LoginMain").Visible = false;
            //    if (!(rapportoLavoro.Item is Cessazione || rapportoLavoro.Item is Contratto))
            //    {
            //        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneNonGestita.aspx");
            //    }
            //}
            //else
            //{
            //    Response.Redirect("~/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx");
            //}

         


            SintesiServiceResult result = sintesiManager.GetRapportoLavoro(idComunicazione,  out rapportoLavoro);
            switch (result.Error)
            {
                case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.NoError:
                    if (!(rapportoLavoro.Item is Contratto))
                    {
                        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneNonGestita.aspx");
                    }
                    else if (sintesiManager.DichiarazioneExistByIdComunicazioneSintesi(idComunicazione))
                    {
                        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneGiaEffettuata.aspx");
                    }
                    else if (!sintesiManager.IsDataAssunzioneValida(rapportoLavoro))
                    {
                        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneDataAssunzioneNonValida.aspx");
                    }

                    IscrizioneLavoratore1.RapportoLavoro = rapportoLavoro;
                    IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;
                    Master.FindControl("LoginMain").Visible = false;
                   
                    break;
                case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.ServiceCallFailed:
                case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.InvalidResponse:
                    Response.Redirect("~/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx");
                    break;
                case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.ServiceErrorMessage:
                    //Context.Items["ErrorMessage"] = result.Message;
                    Response.Redirect(String.Format("~/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage={0}", result.Message));
                    break;
                case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.InvalidId:
                    String msg = "Id comunicazione non valido.";
                    Response.Redirect(String.Format("~/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage={0}",msg));
                    break;
                default:
                    break;
            }
         
        }
    }
}
