﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ComunicazioneImpresaNonIscritta.aspx.cs" Inherits="IscrizioneLavoratori_ComunicazioneImpresaNonIscritta" %>

<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Errore comunicazione" />
    <br />
    <table class="standardTable">
    <tr>
    <td>
    L'impresa non è iscritta e dunque non è possibile effettuare l'operazione richiesta.
    <br />
    Per effettuare l'iscrizione dell'impresa vai alla <a id="A1" href="~/IscrizioneCE/IscrizioneCEDefault.aspx" runat="server">procedura di iscrizione</a>
    </td>
    </tr>
    </table>

</asp:Content>


