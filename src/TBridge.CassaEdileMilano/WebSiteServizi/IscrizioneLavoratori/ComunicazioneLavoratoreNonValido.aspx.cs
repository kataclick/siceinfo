using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneLavoratori_ComunicazioneLavoratoreNonValido : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        //GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione,
        //                                      "IscrizioneLavoratore.aspx");
        System.Collections.Generic.List<FunzionalitaPredefinite> autorizzazioni = new System.Collections.Generic.List<FunzionalitaPredefinite>();
        autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
        autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
        GestioneAutorizzazionePagine.PaginaAutorizzata(autorizzazioni);

        #endregion
    }
}