using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneLavoratori_IscrizioneLavoratoriDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
        funzionalita.Add(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE);
        funzionalita.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita, "IscrizioneLavoratoriDefault.aspx");

        #endregion
    }
}