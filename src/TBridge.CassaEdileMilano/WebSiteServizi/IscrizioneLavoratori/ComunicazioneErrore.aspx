﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/MasterPage.master" CodeFile="ComunicazioneErrore.aspx.cs" Inherits="IscrizioneLavoratori_ComunicazioneErrore" %>

<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Errore comunicazione" />
    <br />
    <div>
        <asp:Label ID="labelError" runat="server" />
    </div>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
