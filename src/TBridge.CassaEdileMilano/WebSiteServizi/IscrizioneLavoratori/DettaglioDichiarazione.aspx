﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DettaglioDichiarazione.aspx.cs" Inherits="IscrizioneLavoratori_DettaglioDichiarazione"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RiepilogoDettagliDichiarazione.ascx" TagName="RiepilogoDettagliDichiarazione"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/RiepilogoDettagliLavoratore.ascx" TagName="RiepilogoDettagliLavoratore"
    TagPrefix="uc4" %>
<%@ Register Src="WebControls/RiepilogoDettagliImpresa.ascx" TagName="RiepilogoDettagliImpresa"
    TagPrefix="uc5" %>
<%@ Register Src="WebControls/RiepilogoDettagliConsulente.ascx" TagName="RiepilogoDettagliConsulente"
    TagPrefix="uc6" %>
<%@ Register Src="WebControls/RiepilogoDettagliCantiere.ascx" TagName="RiepilogoDettagliCantiere"
    TagPrefix="uc7" %>
<%@ Register Src="WebControls/RiepilogoDettagliRapportoDiLavoro.ascx" TagName="RiepilogoDettagliRapportoDiLavoro"
    TagPrefix="uc8" %>
<%@ Register Src="WebControls/RiepilogoDettagliIndirizzo.ascx" TagName="RiepilogoDettagliIndirizzo"
    TagPrefix="uc9" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori"
        sottoTitolo="Dettaglio" />
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Torna alla Ricerca" Width="200px"
        OnClick="ButtonIndietro_Click" />
    &nbsp;<asp:Button ID="ButtonDettaglio" runat="server" Text="Dettaglio Controlli"
        Width="200px" OnClick="ButtonDettaglio_Click" />
    <br />
    <br />
    <div>
        <uc3:RiepilogoDettagliDichiarazione ID="RiepilogoDettagliDichiarazione1" runat="server" />
    </div>
    <br />
    <div>
        <uc4:RiepilogoDettagliLavoratore ID="RiepilogoDettagliLavoratore1" runat="server" />
    </div>
    <br />
    <div>
        <uc9:RiepilogoDettagliIndirizzo ID="RiepilogoDettagliIndirizzo1" runat="server" />
    </div>
    <br />
    <div>
        <uc5:RiepilogoDettagliImpresa ID="RiepilogoDettagliImpresa1" runat="server" />
    </div>
    <br />
    <div id="divConsulente" runat="server">
        <uc6:RiepilogoDettagliConsulente ID="RiepilogoDettagliConsulente1" runat="server" />
    </div>
    <br />
    <div>
        <uc7:RiepilogoDettagliCantiere ID="RiepilogoDettagliCantiere1" runat="server" />
    </div>
    <br />
    <div>
        <uc8:RiepilogoDettagliRapportoDiLavoro ID="RiepilogoDettagliRapportoDiLavoro1" runat="server" />
    </div>
    <br />
    <div>
        <asp:Button ID="ButtonApprova" runat="server" Text="Approva" Width="200px" OnClick="ButtonApprova_Click" />
        &nbsp;<asp:Button ID="ButtonInAttesaDocumentazione" runat="server" Text="In Attesa di Documentazione"
            Width="200px" OnClick="ButtonInAttesaDocumentazione_Click" />
        &nbsp;<asp:Button ID="ButtonRifiuta" runat="server" Text="Rifiuta" Width="200px"
            OnClick="ButtonRifiuta_Click" />
        <br />
        <asp:Label ID="LabelErroreCambioStato" runat="server" Text="Lo stato non pu� essere variato perch� nel frattempo � stato modificato. Ricaricare la dichiarazione."
            Visible="false">
        </asp:Label>
    </div>
</asp:Content>
