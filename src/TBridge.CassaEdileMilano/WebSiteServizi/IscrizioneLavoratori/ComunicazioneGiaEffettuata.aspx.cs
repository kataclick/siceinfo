using System;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class IscrizioneLavoratori_ComunicazioneGiaEffettuata : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        System.Collections.Generic.List<FunzionalitaPredefinite> autorizzazioni = new System.Collections.Generic.List<FunzionalitaPredefinite>();
        autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
        autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
        GestioneAutorizzazionePagine.PaginaAutorizzata(autorizzazioni);
        
        Utente utente = (Utente)Membership.GetUser();

        if (utente != null)
        {
            if (utente.IdUtente > 0)
            {
                GestioneUtentiBiz biz = new GestioneUtentiBiz();
                RuoliCollection ruoli = biz.GetRuoliUtente(utente.IdUtente);
                if (ruoli.Count == 1 &&
                    (ruoli[0].Nome == "IscrizioneLavoratoreSintesi"))
                    MenuIscrizioneLavoratori1.Visible = false;
            }
        }

        #endregion
    }
}