﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreSintesi.aspx.cs" Inherits="IscrizioneLavoratori_IscrizioneLavoratoreSintesi" MaintainScrollPositionOnPostback="true" EnableEventValidation="true" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/Sintesi/IscrizioneLavoratoreSintesi.ascx" TagName="IscrizioneLavoratoreSintesi"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori"
    TagPrefix="uc3" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Iscrizione"
        titolo="Notifiche lavoratori" />
    <br />
    <uc2:IscrizioneLavoratoreSintesi ID="IscrizioneLavoratore1" runat="server" />
</asp:Content>




