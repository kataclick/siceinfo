﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ComunicazioneEffettuata.aspx.cs" Inherits="IscrizioneLavoratori_ComunicazioneEffettuata"
    Title="Iscrizione telematica lavoratore" %>

<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori"
        sottoTitolo="Conferma comunicazione" />
    <br />
    <div>
        <table class="standardTable">
            <tr>
                <td>
                    La comunicazione è stata ricevuta dalla Cassa Edile e verrà processata al più presto.
                    <br />
                    Le informazioni, una volta verificate e validate, saranno inviate alla denuncia mensile (M.U.T. - Modulo Unico Telematico) di manodopera occupata, del mese di riferimento.

                    <asp:Label ID="LabelNuovaComunicazione" runat="server" Visible="false">Per effettuare una nuova comunicazione cliccare <a href="~/IscrizioneLavoratori/IscrizioneLavoratore.aspx"
                        runat="server">qui</a></asp:Label><br />
                    <br />
                    <asp:Button ID="ButtonCorsi" runat="server" Text="Prosegui con l'iscrizione ai corsi"
                        Visible="False" OnClick="ButtonCorsi_Click" />
                    <br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
