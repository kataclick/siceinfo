﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeFile="ComunicazioneDataAssunzioneNonValida.aspx.cs" Inherits="IscrizioneLavoratori_ComunicazioneDataAssunzioneNonValida" %>

<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Errore comunicazione" />
    <br />
    <div>
        <table class="standardTable">
        <tr>
            <td>
                COB notificata in ritardo: data di assunzione non gestibile. 
                <br />
                La comunicazione deve essere inserita entro il mese di appartenenza della data di assunzione 
                o al pi&ugrave entro i primi cinque giorni del mese successivo.
                <br /><br />
                Per ulteriori informazioni contattare l’
                <a href="http://ww2.cassaedilemilano.it/Chisiamo/Lorganizzazione/tabid/117/language/it-IT/Default.aspx " target="_blank" >Ufficio Servizi ad Imprese</a>
            </td>
        </tr>
        </table>
    </div>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
