using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class IscrizioneLavoratori_Ricerca : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazioni

        //GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione,
        //                                      "Ricerca.aspx");
        System.Collections.Generic.List<FunzionalitaPredefinite> autorizzazioni = new System.Collections.Generic.List<FunzionalitaPredefinite>();
        autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
        autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
        GestioneAutorizzazionePagine.PaginaAutorizzata(autorizzazioni);

        #endregion

        #region Eventi in ascolto

        ConsulenteSelezioneImpresa1.OnImpresaSelected += ConsulenteSelezioneImpresa1_OnImpresaSelected;

        #endregion

        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                Int32 idImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();

                if (idImpresa > 0)
                {
                    RicercaNotificaPubblica1.CaricaImpresa(idImpresa);
                }
            }
            else
            {
                ConsulenteSelezioneImpresa1.Visible = false;
            }
        }
    }

    private void ConsulenteSelezioneImpresa1_OnImpresaSelected(int idImpresa, string codiceRagioneSociale)
    {
        RicercaNotificaPubblica1.CaricaImpresa(idImpresa);
    }
}