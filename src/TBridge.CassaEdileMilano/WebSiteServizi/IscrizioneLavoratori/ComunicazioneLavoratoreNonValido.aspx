﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ComunicazioneLavoratoreNonValido.aspx.cs" Inherits="IscrizioneLavoratori_ComunicazioneLavoratoreNonValido" %>

<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Errore comunicazione" />
    <br />
    <table class="standardTable">
    <tr>
    <td>
    Il lavoratore non è iscritto o non ha un rapporto di lavoro attivo e dunque non è possibile effettuare l'operazione richiesta.
    </td>
    </tr>
    </table>

</asp:Content>


