﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreAttivitaSintesi.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreAttivitaSintesi" %>
<br />
Benvenuti nella funzione dedicata alla gestione dell’instaurazione / cessazione di rapporti di lavoro di operai edili.
<br />
<br />
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Tipo di attività</b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <asp:RadioButtonList ID="RadioButtonListTipoAttivita" runat="server" AutoPostBack="True"
                Enabled="false">
                <asp:ListItem Selected="True" Value="1">Assunzione</asp:ListItem>
                <asp:ListItem Value="2">Cessazione</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
</table>
