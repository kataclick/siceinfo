﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreDocumentiSintesi.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreDocumentiSintesi" %>
<style type="text/css">
    .style1
    {
        width: 300px;
    }
</style>
<asp:Panel ID="PanelDatiDocumenti" runat="server" Width="100%">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Documenti</b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Da compilare se il lavoratore è extra-UE
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style1">
                Tipo di documento:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipoDocumento" runat="server" Width="250px" EmptyMessage="Selezionare il tipo di documento"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <%--<asp:CustomValidator
                    ID="CustomValidatorTipoDocumento2"
                    runat="server"
                    ValidationGroup="datiAnagrafici"
                    ErrorMessage="Selezionare il tipo di documento" 
                    onservervalidate="CustomValidatorTipoDocumento2_ServerValidate">
                    *
                </asp:CustomValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Numero del documento:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxNumeroDocumento" runat="server" Width="250px" Enabled="false">
                </telerik:RadTextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Motivo del permesso:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxMotivoPermesso" runat="server" Width="250px"
                    EmptyMessage="Selezionare il motivo del permesso" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Data richiesta permesso di soggiorno:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataRichiestaPermesso" runat="server" Width="250px" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="style1">
                Data scadenza permesso:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataScadenzaPermesso" runat="server" Width="250px"
                    Enabled="false" MaxDate="2999-12-31" />
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorDataScadenzaPermesso" runat="server" ControlToValidate="RadDatePickerDataScadenzaPermesso"
                    ValidationGroup="datiAnagrafici" ErrorMessage="La data di scadenza non può essere inferiore a quella di richiesta"
                    OnServerValidate="CustomValidatorDataScadenzaPermesso_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
