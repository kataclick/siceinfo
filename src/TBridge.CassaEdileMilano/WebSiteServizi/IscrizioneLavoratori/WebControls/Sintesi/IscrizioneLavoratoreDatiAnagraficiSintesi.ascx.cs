﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using Telerik.Web.UI;
using SintesiBusiness=TBridge.Cemi.Sintesi.Business.SintesiBusiness;

public partial class IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreDatiAnagraficiSintesi : UserControl
{
    private readonly Common commonBiz = new Common();
    private readonly SintesiBusiness sintesiBiz = new SintesiBusiness();

    private ComuneSiceNew ComuneNascita { get; set; }
    public TipoAttivita TipoAttivita { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaCombo();
        }
    }

    private void CaricaCombo()
    {
        //CaricaProvince();
        CaricaNazionalita();
        CaricaStatiCivili();
        CaricaLingue();
        CaricaTitoliDiStudio();
    }

    private void CaricaNazionalita()
    {
        if (RadComboBoxCittadinanza.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCittadinanza,
                commonBiz.GetNazioni(),
                "Value",
                "Key");
        }
        RadComboBoxCittadinanza.Items.Insert(0, new RadComboBoxItem("- Selezionare la cittadinanza -"));
    }

    private void CaricaTitoliDiStudio()
    {
        if (RadComboBoxIstruzione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxIstruzione,
                sintesiBiz.GetTitoliDiStudio(),
                "descrizione",
                "codice");
        }
        RadComboBoxIstruzione.Items.Insert(0, new RadComboBoxItem("- Selezionare il livello di istruzione -"));
    }

    private void CaricaLingue()
    {
        if (RadComboBoxLingua.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxLingua,
                commonBiz.GetLingue(),
                "descrizione",
                "IdLingua");
        }
        RadComboBoxLingua.Items.Insert(0, new RadComboBoxItem("- Selezionare la lingua -"));
    }

    private void CaricaStatiCivili()
    {
        if (RadComboBoxStatoCivile.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxStatoCivile,
                commonBiz.GetStatiCivili(),
                "descrizione",
                "IdStatoCivile");
        }
        RadComboBoxStatoCivile.Items.Insert(0, new RadComboBoxItem("- Selezionare lo stato civile -"));
    }

    public void CaricaDatiSintesi(DatiLavoratore lavoratore, int idImpresa)
    {
        //if(!TipoAttivita.Equals(TipoAttivita.Assunzione))
        //{
        //    Lavoratore lavoratoreIscritto = sintesiManager.GetLavoratore(lavoratore.AnagraficaCompleta.codicefiscale,idImpresa);
        //    if(lavoratoreIscritto.IdLavoratore == null)
        //    {
        //        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneLavoratoreNonValido.aspx");
        //    }
        //    RadTextBoxCodice.Text = lavoratoreIscritto.IdLavoratore.ToString();
        //}

        //ListDictionary nazionalita = commonBiz.GetNazionalita(lavoratore.AnagraficaCompleta.cittadinanza);
        //TitoloDiStudio titoloDiStudio = sintesiBiz.GetTitoloDiStudio(lavoratore.LivelloIstruzione);
        ComuneNascita = commonBiz.GetComuneSiceNew(lavoratore.AnagraficaCompleta.nascita.comune);
        RadTextBoxNome.Text = lavoratore.AnagraficaCompleta.nome;
        RadTextBoxCognome.Text = lavoratore.AnagraficaCompleta.cognome;
        RadTextBoxCodiceFiscale.Text = lavoratore.AnagraficaCompleta.codicefiscale;
        RadDatePickerDataNascita.SelectedDate = lavoratore.AnagraficaCompleta.nascita.data;
        RadComboBoxComuneNascita.Items.Insert(0,
                                              new RadComboBoxItem(ComuneNascita.Comune, ComuneNascita.CodiceCatastale));
        RadComboBoxComuneNascita.SelectedIndex = 0;
        RadTextBoxProvinciaNascita.Text = ComuneNascita.Provincia;
        NazionalitaSiceNew nazionalita = sintesiBiz.GetNazionalita(lavoratore.AnagraficaCompleta.cittadinanza);
        RadComboBoxCittadinanza.Items.Insert(0,
                                             new RadComboBoxItem(
                                                 nazionalita.Nazionalita,
                                                 nazionalita.IdNazione));
        RadComboBoxCittadinanza.SelectedIndex = 0;


        switch (lavoratore.AnagraficaCompleta.sesso)
        {
            case Sesso.M:
                RadioButtonListSesso.SelectedIndex = 0;
                break;
            case Sesso.F:
                RadioButtonListSesso.SelectedIndex = 1;
                break;
        }

        CheckBoxItaliano.Checked = !ComuneNascita.Provincia.Equals("EE");


        CaricaTitoliDiStudio();
        if (RadComboBoxIstruzione.Items.FindItemByValue(lavoratore.LivelloIstruzione, true) != null)
            {
                RadComboBoxIstruzione.SelectedIndex =
                    RadComboBoxIstruzione.Items.FindItemByValue(lavoratore.LivelloIstruzione, true).Index;
                RadComboBoxIstruzione.Enabled = false;
            }
        
        //RadTextBoxIstruzione.Text = titoloDiStudio.Descrizione;
    }

    public Lavoratore GetLavoratore()
    {
        Lavoratore lavoratore = new Lavoratore();

        if (!String.IsNullOrEmpty(RadTextBoxCodice.Text))
        {
            lavoratore.IdLavoratore = Int32.Parse(RadTextBoxCodice.Text);
            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
        }
        else
        {
            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
        }
        lavoratore.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
        lavoratore.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
        lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);

        lavoratore.Italiano = CheckBoxItaliano.Checked;

        if (RadDatePickerDataNascita.SelectedDate.HasValue)
        {
            lavoratore.DataNascita = RadDatePickerDataNascita.SelectedDate.Value;
        }
        if (!String.IsNullOrEmpty(RadTextBoxProvinciaNascita.Text))
        {
            lavoratore.ProvinciaNascita = RadTextBoxProvinciaNascita.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxComuneNascita.SelectedValue))
        {
            lavoratore.ComuneNascita = RadComboBoxComuneNascita.SelectedValue;
        }
        lavoratore.Sesso = RadioButtonListSesso.SelectedValue[0];
        if (!String.IsNullOrEmpty(RadComboBoxStatoCivile.SelectedValue))
        {
            lavoratore.IdStatoCivile = RadComboBoxStatoCivile.SelectedValue;
        }
        if (!String.IsNullOrEmpty(RadComboBoxCittadinanza.SelectedValue))
        {
            lavoratore.PaeseNascita = RadComboBoxCittadinanza.SelectedValue;
            lavoratore.Cittadinanza = RadComboBoxCittadinanza.SelectedValue;
        }

        //if (!String.IsNullOrEmpty(RadComboBoxIstruzione.SelectedValue))
        //{
        //    lavoratore.TipoDocumento = RadComboBoxIstruzione.SelectedValue;
        //}
        if (!String.IsNullOrEmpty(RadComboBoxIstruzione.Text))
        {
            lavoratore.LivelloIstruzione = RadComboBoxIstruzione.SelectedValue;
        }
        if (!String.IsNullOrEmpty(RadComboBoxLingua.SelectedValue))
        {
            lavoratore.IdLingua = RadComboBoxLingua.SelectedValue;
        }

        return lavoratore;
    }

    protected void CustomValidatorCittadinanza_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxCittadinanza.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    public int? GetIdLavoratore()
    {
        return (int?) RadTextBoxCodice.Value;
    }
}