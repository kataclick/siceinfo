﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Sintesi.Type;

public partial class IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreDatiLocalizzazioneSintesi :
    UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaAtiivita(TipoAttivita tipoAttivita)
    {
        PanelAltriDati.Enabled = false;
        if(tipoAttivita.Equals(TipoAttivita.Assunzione))
            PanelAltriDati.Enabled = true;
    }

    public void CaricaDatiSintesi(IndirizzoItaliano indirizzoLavoratore)
    {
        IscrizioneLavoratoreIndirizzo1.CaricaDatiSintesi(indirizzoLavoratore);
    }

    public void CompletaLavoratore(Lavoratore lavoratore)
    {
        lavoratore.Indirizzo = IscrizioneLavoratoreIndirizzo1.GetIndirizzo();
        lavoratore.NumeroTelefono = Presenter.NormalizzaCampoTesto(RadTexBoxTelefono.Text);
        lavoratore.NumeroTelefonoCellulare = Presenter.NormalizzaCampoTesto(RadTexBoxCellulare.Text);
        lavoratore.Email = RadTexBoxEmail.Text;
    }

    protected void CustomValidatorServizioSms_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CheckBoxServizioSms.Checked && String.IsNullOrEmpty(RadTexBoxCellulare.Text))
        {
            args.IsValid = false;
        }
    }
}