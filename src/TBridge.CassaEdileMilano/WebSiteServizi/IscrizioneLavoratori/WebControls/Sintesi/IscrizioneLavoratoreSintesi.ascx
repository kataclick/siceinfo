﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreSintesi.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreSintesi" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="IscrizioneLavoratoreAttivitaSintesi.ascx" TagName="IscrizioneLavoratoreImpresaSintesi"
    TagPrefix="uc1" %>
<%@ Register Src="IscrizioneLavoratoreImpresaSintesi.ascx" TagName="IscrizioneLavoratoreImpresaSintesi"
    TagPrefix="uc2" %>
<%@ Register Src="IscrizioneLavoratoreIndirizzoSintesi.ascx" TagName="IscrizioneLavoratoreIndirizzoSintesi"
    TagPrefix="uc3" %>
<%@ Register Src="IscrizioneLavoratoreDatiLocalizzazioneSintesi.ascx" TagName="IscrizioneLavoratoreDatiLocalizzazioneSintesi"
    TagPrefix="uc9" %>
<%@ Register Src="IscrizioneLavoratoreDatiAnagraficiDocumentiSintesi.ascx" TagName="IscrizioneLavoratoreDatiAnagraficiDocumentiSintesi"
    TagPrefix="uc4" %>
<%@ Register Src="IscrizioneLavoratoreRapportoDiLavoroSintesi.ascx" TagName="IscrizioneLavoratoreRapportoDiLavoroSintesi"
    TagPrefix="uc5" %>
<%@ Register Src="../IscrizioneLavoratoreInformazioniAggiuntive.ascx" TagName="IscrizioneLavoratoreInformazioniAggiuntive"
    TagPrefix="uc10" %>
<%@ Register Src="../IscrizioneLavoratoreRiepilogo.ascx" TagName="IscrizioneLavoratoreRiepilogo"
    TagPrefix="uc6" %>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <div style="width: 100%;">
        <telerik:RadTabStrip ID="RadTabStripIscrizioneLavoratore" runat="server" MultiPageID="RadMultiPageIscrizioneLavoratore"
            SelectedIndex="0" Width="100%" OnTabClick="RadTabStripIscrizioneLavoratore_TabClick"
            Skin="Default" Font-Size="Smaller">
            <Tabs>
                <telerik:RadTab Text="Attività">
                </telerik:RadTab>
                <telerik:RadTab Text="Impresa">
                </telerik:RadTab>
                <telerik:RadTab Text="Sede operativa">
                </telerik:RadTab>
                <telerik:RadTab Text="Dati">
                </telerik:RadTab>
                <telerik:RadTab Text="Indirizzo">
                </telerik:RadTab>
                <telerik:RadTab Text="Rapp. di lavoro">
                </telerik:RadTab>
                <telerik:RadTab Text="Altre info">
                </telerik:RadTab>
                <telerik:RadTab Text="Conferma">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <br />
        <telerik:RadMultiPage ID="RadMultiPageIscrizioneLavoratore" runat="server" SelectedIndex="0"
            Width="100%" RenderSelectedPageOnly="True">
            <telerik:RadPageView ID="RadPageViewAttivita" runat="server">
                <div>
                    <b>Attività </b>
                </div>
                <br />
                <div>
                    <uc1:IscrizioneLavoratoreImpresaSintesi ID="IscrizioneLavoratoreAttivita1" runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryAttivita" runat="server" ValidationGroup="attivita"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewAnagraficaImpresa" runat="server">
                <div>
                    <b>Anagrafica Impresa </b>
                </div>
                <br />
                <div>
                    <uc2:IscrizioneLavoratoreImpresaSintesi ID="IscrizioneLavoratoreImpresa1" runat="server" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewCantiere" runat="server">
                <div>
                    <b>Indirizzo sede operativa</b>
                </div>
                <br />
                <div>
                    <uc3:IscrizioneLavoratoreIndirizzoSintesi ID="IscrizioneLavoratoreCantiere1" runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryCantiere" runat="server" ValidationGroup="cantiere"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewLavoratoreDatiAnagrafici" runat="server">
                <div>
                    <b>Lavoratore (dati anagrafici e documenti) </b>
                </div>
                <br />
                <div>
                    <uc4:IscrizioneLavoratoreDatiAnagraficiDocumentiSintesi ID="IscrizioneLavoratoreDatiAnagraficiDocumenti1"
                        runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryDatiAnagrafici" runat="server" ValidationGroup="datiAnagrafici"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewLavoratoreDatiLocalizzazione" runat="server">
                <div>
                    <b>Lavoratore (dati localizzazione) </b>
                </div>
                <br />
                <div>
                    <uc9:IscrizioneLavoratoreDatiLocalizzazioneSintesi ID="IscrizioneLavoratoreDatiLocalizzazione1"
                        runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryDatiLocalizzazione" runat="server" ValidationGroup="datiLocalizzazione"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewRapportoDiLavoro" runat="server">
                <div>
                    <b>Rapporto di Lavoro </b>
                </div>
                <br />
                <div>
                    <uc5:IscrizioneLavoratoreRapportoDiLavoroSintesi ID="IscrizioneLavoratoreRapportoDiLavoro1"
                        runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryRapportoDiLavoro" runat="server" ValidationGroup="rapportoDiLavoro"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewLavoratoreInfoAggiuntive" runat="server">
                <div>
                    <b>Lavoratore (info aggiuntive) </b>
                </div>
                <br />
                <div>
                    <uc10:IscrizioneLavoratoreInformazioniAggiuntive ID="IscrizioneLavoratoreInformazioniAggiuntive"
                        runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryConferma" runat="server" ValidationGroup="infoAggiuntive"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewConferma" runat="server">
                <div>
                    <b>Conferma </b>
                </div>
                <br />
                <div>
                    <uc6:IscrizioneLavoratoreRiepilogo ID="IscrizioneLavoratoreRiepilogo1" runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryStop" runat="server" ValidationGroup="conferma"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <div style="text-align: right;">
        <br />
        <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="150px" OnClick="ButtonIndietro_Click"
            Visible="False" />
        &nbsp;
        <asp:Button ID="ButtonAvanti" runat="server" Text="Avanti" Width="150px" OnClick="ButtonAvanti_Click"
            ValidationGroup="stop" />
    </div>
</telerik:RadAjaxPanel>
