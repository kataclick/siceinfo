﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreRapportoDiLavoroSintesi.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreRapportoDiLavoroSintesi" %>
<table class="borderedTable">
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data comunicazione (gg/mm/aaaa):
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDataInvio" runat="server" Width="250px" MaxLength="10"
                Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
</table>
<br />
<asp:Panel ID="PanelDatiRapporto" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Rapporto di lavoro </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data assunzione<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDateInputDataAssunzione" runat="server" Width="250px"
                    Enabled="false" />
            </td>
            <td>
                <asp:Label ID="ErrorLabel" ForeColor="Red" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data inizio rapporto di lavoro<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDateInputDataInizioRapportoLavoro" runat="server" Enabled="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                <asp:Label Text="Data fine rapporto di lavoro:" ID="LabelDataFineRapporto" Visible="false" runat="server" /> 
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataFineRapporto" runat="server" Enabled="false" Visible="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Contratto applicato<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipologiaContrattuale" runat="server" Width="250px"
                    Enabled="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <table class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Tipologia di rapporto </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Tipologia contrattuale di assunzione:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipologiaInizioRapporto" runat="server" Width="250px"
                    Enabled="false">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Tipologia orario Part-time:
            </td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListPartTime" runat="server" RepeatDirection="Horizontal"
                    Enabled="false">
                    <asp:ListItem>SI</asp:ListItem>
                    <asp:ListItem Selected="True">NO</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                % Part-time:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxOrarioMedioSettimanale" runat="server" Width="250px"
                    MaxLength="5">
                </telerik:RadTextBox>
            </td>
            <td>
            <asp:CustomValidator ID="CustomValidatorPercentualePartTime" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Inserire una percentuale Part time" OnServerValidate="CustomValidatorPercentualePartTime_ServerValidate">
                *
                </asp:CustomValidator>
               <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Inserire una percentuale Part time corretta (0 - 1,0)"
                    MaximumValue="1,0" MinimumValue="0" ValidationGroup="rapportoDiLavoro" ControlToValidate="RadTextBoxOrarioMedioSettimanale"
                    Type="Double">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Livello di inquadramento:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxLivelloInquadramento" runat="server" Width="250px"
                    Enabled="false">
                </telerik:RadTextBox>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Tipologia di rapporto </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Categoria<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxCategoria" runat="server" Width="250px" EmptyMessage="Selezionare una categoria"
                    MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="true" EnableScreenBoundaryDetection="false" OnSelectedIndexChanged="RadComboBoxCategoria_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorTipologiaCategoria" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una categoria" OnServerValidate="CustomValidatorTipologiaCategoria_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Qualifica<b>*</b>:
            </td>
            <%--<td>
                <telerik:RadComboBox ID="RadComboBoxQualifica" runat="server" Width="250px" Enabled="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>--%>
            <td>
                <telerik:RadComboBox ID="RadComboBoxQualifica" runat="server" Width="250px" EmptyMessage="Selezionare la qualifica"
                    MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="false" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorQualifica" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una qualifica" OnServerValidate="CustomValidatorQualifica_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Mansione<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxMansione" runat="server" Width="250px" EmptyMessage="Selezionare la mansione"
                    MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="false" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorMansione" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una mansione" OnServerValidate="CustomValidatorMansione_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<telerik:RadMultiPage ID="RadMultiPageAttivita" runat="server" Width="100%" SelectedIndex="0"
    RenderSelectedPageOnly="true">
    <telerik:RadPageView ID="RadPageViewAssunzione" runat="server">
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewCessazione" runat="server">
        <table class="borderedTable">
            <tr>
                <td colspan="3">
                    <b>Dati cessazione </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Data cessazione (gg/mm/aaaa)<b>*</b>:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDateInputDataCessazione" runat="server" Width="250px"
                        Enabled="false" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Causa<b>*</b>:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxCausa" runat="server" Width="250px" EmptyMessage="Selezionare una causa"
                        MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="false" EnableScreenBoundaryDetection="false"
                        Enabled="false">
                    </telerik:RadComboBox>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewTrasformazione" runat="server">
        <table class="borderedTable">
            <tr>
                <td colspan="3">
                    <b>Dati trasformazione </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Data trasformazione<b>*</b>:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDateInputDataTrasformazione" runat="server" Width="250px"
                        Enabled="false" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Codice trasformazione<b>*</b>:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxCodiceTrasformazione" runat="server" Width="250px"
                        EmptyMessage="Selezionare un codice trasformazione" Filter="Contains" MarkFirstMatch="true"
                        AllowCustomText="true" Enabled="false">
                    </telerik:RadComboBox>
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewProroga" runat="server">
    </telerik:RadPageView>
</telerik:RadMultiPage>
