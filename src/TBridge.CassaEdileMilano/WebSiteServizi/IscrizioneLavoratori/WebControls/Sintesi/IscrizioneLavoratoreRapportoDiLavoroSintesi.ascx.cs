﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using Geocoding = TBridge.Cemi.Cantieri.Business.CantieriGeocoding;
using SintesiBusiness=TBridge.Cemi.Sintesi.Business.SintesiBusiness;

public partial class IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreRapportoDiLavoroSintesi : UserControl
{
    private const String VALIDATIONGROUP = "rapportoDiLavoro";
    private readonly Common commonBiz = new Common();
    private readonly RapportoDiLavoro RapportoDiLavoroSintesi = new RapportoDiLavoro();
    private readonly SintesiBusiness sintesiBiz = new SintesiBusiness();
    private readonly SintesiManager sintesiManager = new SintesiManager();

    private TipoContratto ContrattoOriginaleSintesi
    {
        set { ViewState["ContrattoOriginaleSintesi"] = value; }
        get { return ViewState["ContrattoOriginaleSintesi"] as TipoContratto; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            RadTextBoxDataInvio.Text = DateTime.Now.ToString("dd/MM/yyyy");

            CaricaCombo();
        }
    }

    private void ResetCompleto()
    {
        Presenter.SvuotaCampo(RadTextBoxOrarioMedioSettimanale);
        Presenter.SvuotaCampo(RadTextBoxLivelloInquadramento);

        ResetDatiAttivita();
    }

    private void ResetDatiAttivita()
    {
        RadComboBoxCausa.ClearSelection();

        RadComboBoxCodiceTrasformazione.ClearSelection();
    }

    public void CaricaTipoAttivita(TipoAttivita tipoAttivita)
    {
        ResetCompleto();

        switch (tipoAttivita)
        {
            case TipoAttivita.Assunzione:
                AbilitazioneValidator(tipoAttivita);
                PanelDatiRapporto.Enabled = true;
                break;
                //Da aggiungere se ritorna la validazione diretta da dati sintesi
                //default:
                //    AbilitazioneValidator(tipoAttivita);
                //    PanelDatiRapporto.Enabled = false;
                //    break;
        }

        RadMultiPageAttivita.SelectedIndex = (Int32) tipoAttivita - 1;
    }

    public RapportoDiLavoro GetRapportoDiLavoro()
    {
        RapportoDiLavoro rapporto = new RapportoDiLavoro();

        if (RadDateInputDataAssunzione.SelectedDate != null)
        {
            rapporto.DataAssunzione = RadDateInputDataAssunzione.SelectedDate;
        }
        if (RadDateInputDataInizioRapportoLavoro.SelectedDate != null)
        {
            rapporto.DataInizioValiditaRapporto = RadDateInputDataInizioRapportoLavoro.SelectedDate;
        }
        if (RadDatePickerDataFineRapporto.SelectedDate != null)
        {
            rapporto.DataFineValiditaRapporto = RadDatePickerDataFineRapporto.SelectedDate.Value;
        }
        
        if (!String.IsNullOrEmpty(RadComboBoxTipologiaContrattuale.SelectedValue))
        {
            rapporto.Contratto = new TipoContratto();
            rapporto.Contratto.IdContratto = RadComboBoxTipologiaContrattuale.SelectedValue;
            rapporto.Contratto.Descrizione = RadComboBoxTipologiaContrattuale.Text;
        }

        rapporto.ContrattoOriginaleSintesi = this.ContrattoOriginaleSintesi;

        if (!String.IsNullOrEmpty(RadComboBoxCategoria.SelectedValue))
        {
            rapporto.Categoria = new TipoCategoria();
            rapporto.Categoria.IdCategoria = RadComboBoxCategoria.SelectedValue;
            rapporto.Categoria.Descrizione = RadComboBoxCategoria.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxQualifica.SelectedValue))
        {
            rapporto.Qualifica = new TipoQualifica();
            rapporto.Qualifica.IdQualifica = RadComboBoxQualifica.SelectedValue;
            rapporto.Qualifica.Descrizione = RadComboBoxQualifica.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxMansione.SelectedValue))
        {
            rapporto.Mansione = new TipoMansione();
            rapporto.Mansione.IdMansione = RadComboBoxMansione.SelectedValue;
            rapporto.Mansione.Descrizione = RadComboBoxMansione.Text;
        }
        if (RadDateInputDataCessazione.SelectedDate != null)
        {
            rapporto.DataCessazione = RadDateInputDataCessazione.SelectedDate;
        }
        if (RadDateInputDataTrasformazione.SelectedDate != null)
        {
            rapporto.DataTrasformazione = RadDateInputDataTrasformazione.SelectedDate;
        }
        if (!String.IsNullOrEmpty(RadComboBoxCausa.SelectedValue))
        {
            rapporto.TipoFineRapporto = new TipoFineRapporto();
            rapporto.TipoFineRapporto.IdTipoFineRapporto = RadComboBoxCausa.SelectedValue;
            rapporto.TipoFineRapporto.Descrizione = RadComboBoxCausa.Text;
        }

        if (!String.IsNullOrEmpty(RadComboBoxTipologiaInizioRapporto.SelectedValue))
        {
            rapporto.TipoInizioRapporto = new TipoInizioRapporto();
            rapporto.TipoInizioRapporto.IdTipoInizioRapporto = RadComboBoxTipologiaInizioRapporto.SelectedValue;
            rapporto.TipoInizioRapporto.Descrizione = RadComboBoxTipologiaInizioRapporto.Text;
        }

        rapporto.PartTime = (RadioButtonListPartTime.SelectedIndex == 0);

        if (!String.IsNullOrEmpty(RadTextBoxOrarioMedioSettimanale.Text) && rapporto.PartTime)
        {
            rapporto.PercentualePartTime = Decimal.Parse(RadTextBoxOrarioMedioSettimanale.Text);
        }

        if (!String.IsNullOrEmpty(RadTextBoxLivelloInquadramento.Text))
        {
            rapporto.LivelloInquadramento = RadTextBoxLivelloInquadramento.Text;
        }

        return rapporto;
    }


    private void AbilitazioneValidator(TipoAttivita evento)
    {
        switch (evento)
        {
            case TipoAttivita.Assunzione:
                foreach (BaseValidator validator in Page.Validators)
                {
                    if (validator.ValidationGroup == VALIDATIONGROUP)
                    {
                        if (validator.ID != "RequiredFieldValidatorDataCessazione"
                            && validator.ID != "RangeValidatorDataCessazione"
                            && validator.ID != "RequiredFieldValidatorCausa"
                            && validator.ID != "CustomValidatorDataCessazione"
                            && validator.ID != "RequiredFieldValidatorDataTrasformazione")
                        {
                            validator.Enabled = true;
                        }
                        else
                        {
                            validator.Enabled = false;
                        }
                    }
                }
                break;
            case TipoAttivita.Cessazione:
                foreach (BaseValidator validator in Page.Validators)
                {
                    if (validator.ValidationGroup == VALIDATIONGROUP)
                    {
                        if (validator.ID == "RequiredFieldValidatorDataCessazione"
                            || validator.ID == "RangeValidatorDataCessazione"
                            || validator.ID == "RequiredFieldValidatorCausa"
                            || validator.ID == "CustomValidatorDataCessazione")
                        {
                            validator.Enabled = true;
                        }
                        else
                        {
                            validator.Enabled = false;
                        }
                    }
                }
                break;
        }
    }

    protected void RadComboBoxTipologiaInizioRapporto_SelectedIndexChanged(object o,
                                                                           RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (RapportoDiLavoroSintesi.TipoInizioRapporto.IdTipoInizioRapporto == "3")
        {
            RadioButtonListPartTime.SelectedIndex = 0;
            RadioButtonListPartTime.Enabled = false;
        }
        else
        {
            //RadioButtonListPartTime.SelectedIndex = 1;
            RadioButtonListPartTime.Enabled = true;
        }
    }

    public void CaricaDatiSintesi(object item, int? idLavoratore, int idImpresa)
    {
        if (item is RapportoLavoroInizioRapporto)
        {
            RapportoLavoroInizioRapporto assunzione = (RapportoLavoroInizioRapporto) item;
            RadioButtonListPartTime.SelectedIndex = 0;
            
            //Se fulltime o non definto
            if (assunzione.tipoOrario.codice.Equals("F") || assunzione.tipoOrario.codice.Equals("N"))
            {
                RadioButtonListPartTime.SelectedIndex = 1;
                RadTextBoxOrarioMedioSettimanale.Enabled = false;
            }
            
            //RadTextBoxOrarioMedioSettimanale.Text = assunzione.tipoOrario.oreSettimanaliMedie;
            RadTextBoxLivelloInquadramento.Text = assunzione.livelloInquadramento;

            RapportoDiLavoroSintesi.TipoInizioRapporto =
                sintesiBiz.GetTipoInizioRapporto(assunzione.TipologiaContrattuale);
            sintesiManager.ImpostaTipoContratto(RapportoDiLavoroSintesi, sintesiBiz.GetTipoContratto(assunzione.ccnl), idImpresa);
            this.ContrattoOriginaleSintesi = RapportoDiLavoroSintesi.ContrattoOriginaleSintesi;
         //   RapportoDiLavoroSintesi.Contratto = sintesiBiz.GetTipoContratto(assunzione.ccnl);
            
            RadComboBoxTipologiaInizioRapporto.Items.Insert(0,
                                                            new RadComboBoxItem(
                                                                RapportoDiLavoroSintesi.TipoInizioRapporto.Descrizione,
                                                                RapportoDiLavoroSintesi.TipoInizioRapporto.
                                                                    IdTipoInizioRapporto));
            RadComboBoxTipologiaInizioRapporto.SelectedIndex = 0;
            RadComboBoxTipologiaContrattuale.Items.Insert(0,
                                                          new RadComboBoxItem(
                                                              RapportoDiLavoroSintesi.Contratto.Descrizione,
                                                              RapportoDiLavoroSintesi.Contratto.IdContratto));
            RadComboBoxTipologiaContrattuale.SelectedIndex = 0;

            RadDateInputDataAssunzione.SelectedDate = assunzione.dataInizio;
            RadDateInputDataInizioRapportoLavoro.SelectedDate = assunzione.dataInizio;


            if (String.Equals(RapportoDiLavoroSintesi.TipoInizioRapporto.Descrizione, "Tempo determinato"))
            {
                LabelDataFineRapporto.Visible = true;
                RadDatePickerDataFineRapporto.Visible = true;
                RadDatePickerDataFineRapporto.SelectedDate = assunzione.dataFine;
            }

            if (RadComboBoxCategoria.Items.Count == 0)
            {
                CaricaTipiCategoria(RadComboBoxTipologiaContrattuale.SelectedValue);
            }

            TipoMansione tipoMansione = commonBiz.GetTipoMansione(assunzione.qualificaProfessionale);
            if (tipoMansione != null)
            {
                RadComboBoxMansione.Items.Insert(0, new RadComboBoxItem(tipoMansione.Descrizione, tipoMansione.IdMansione));
                RadComboBoxMansione.SelectedIndex = 0;
                RadComboBoxMansione.Enabled = false;
            }
        }
        if (item is Cessazione)
        {
            Cessazione cessazione = (Cessazione) item;

            //RapportoDiLavoro rapportoDiLavoro = sintesiManager.GetRapportoLavoro(idLavoratore,idImpresa);


            //RadComboBoxQualifica.Items.Clear();
            //RadComboBoxQualifica.Items.Insert(0,
            //                                  new RadComboBoxItem(rapportoDiLavoro.Qualifica.Descrizione,
            //                                                      rapportoDiLavoro.Qualifica.IdQualifica));
            //RadComboBoxQualifica.SelectedIndex = 0;

            //RadComboBoxCategoria.Items.Clear();
            //RadComboBoxCategoria.Items.Insert(0,
            //                                  new RadComboBoxItem(rapportoDiLavoro.Categoria.Descrizione,
            //                                                      rapportoDiLavoro.Categoria.IdCategoria));
            //RadComboBoxCategoria.SelectedIndex = 0;

            //RadComboBoxMansione.Items.Clear();
            //RadComboBoxMansione.Items.Insert(0,
            //                                  new RadComboBoxItem(rapportoDiLavoro.Mansione.Descrizione,
            //                                                      rapportoDiLavoro.Mansione.IdMansione));
            //RadComboBoxMansione.SelectedIndex = 0;

            //RadioButtonListPartTime.SelectedIndex = 0;
            //if (cessazione.Contratto.tipoOrario.codice.Equals("F"))
            //{
            //    RadioButtonListPartTime.SelectedIndex = 1;
            //    RadTextBoxOrarioMedioSettimanale.Enabled = false;
            //}
            //RadTextBoxOrarioMedioSettimanale.Text = cessazione.Contratto.tipoOrario.oreSettimanaliMedie;


            RadTextBoxLivelloInquadramento.Text = cessazione.Contratto.livelloInquadramento;

            RapportoDiLavoroSintesi.TipoInizioRapporto =
                sintesiBiz.GetTipoInizioRapporto(cessazione.Contratto.TipologiaContrattuale);
            RapportoDiLavoroSintesi.Contratto = sintesiBiz.GetTipoContratto(cessazione.Contratto.ccnl);
            RadComboBoxTipologiaInizioRapporto.Items.Insert(0,
                                                            new RadComboBoxItem(
                                                                RapportoDiLavoroSintesi.TipoInizioRapporto.Descrizione,
                                                                RapportoDiLavoroSintesi.TipoInizioRapporto.
                                                                    IdTipoInizioRapporto));
            RadComboBoxTipologiaInizioRapporto.SelectedIndex = 0;
            RadComboBoxTipologiaContrattuale.Items.Insert(0,
                                                          new RadComboBoxItem(
                                                              RapportoDiLavoroSintesi.Contratto.Descrizione,
                                                              RapportoDiLavoroSintesi.Contratto.IdContratto));
            RadComboBoxTipologiaContrattuale.SelectedIndex = 0;
            //RadComboBoxQualifica.Items.Insert(0,
            //                                  new RadComboBoxItem(RapportoDiLavoroSintesi.Qualifica.Descrizione,
            //                                                      RapportoDiLavoroSintesi.Qualifica.IdQualifica));
            //RadComboBoxQualifica.SelectedIndex = 0;

            if (RadComboBoxCategoria.Items.Count == 0)
            {
                CaricaTipiCategoria(RadComboBoxTipologiaContrattuale.SelectedValue);
            }

            TipoMansione tipoMansione = commonBiz.GetTipoMansione(cessazione.Contratto.qualificaProfessionale);
            if (tipoMansione != null)
            {
                RadComboBoxMansione.Items.Insert(0, new RadComboBoxItem(tipoMansione.Descrizione, tipoMansione.IdMansione));
                RadComboBoxMansione.SelectedIndex = 0;
                RadComboBoxMansione.Enabled = false;
            }

            RadDateInputDataAssunzione.SelectedDate = cessazione.Contratto.dataInizio;
            RadDateInputDataInizioRapportoLavoro.SelectedDate = cessazione.Contratto.dataInizio;

            //Dati Cessazione
            TipoFineRapporto tipoFineRapporto = sintesiManager.GetTipoFineRapporto(cessazione.codiceCausa);
            RadComboBoxCausa.Items.Insert(0,
                                          new RadComboBoxItem(tipoFineRapporto.Descrizione,
                                                              tipoFineRapporto.IdTipoFineRapporto));
            RadComboBoxCausa.SelectedIndex = 0;
            RadDateInputDataCessazione.SelectedDate = cessazione.data;
        }
    }

    #region Custom Validators

    protected void CustomValidatorPercentualePartTime_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadioButtonListPartTime.SelectedIndex == 0)
        {
            if (RadTextBoxOrarioMedioSettimanale.Text == string.Empty)
                args.IsValid = false;
        }
    }

    protected void CustomValidatorDataAssunzione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDateInputDataAssunzione.SelectedDate.HasValue
            && RadDateInputDataInizioRapportoLavoro.SelectedDate.HasValue
            && RadDateInputDataAssunzione.SelectedDate > RadDateInputDataInizioRapportoLavoro.SelectedDate)
        {
            args.IsValid = false;
        }
    }


    protected void CustomValidatorTipologiaCategoria_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxCategoria.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorQualifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxQualifica.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorMansione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxMansione.SelectedIndex >= 0 && !String.IsNullOrEmpty(RadComboBoxMansione.SelectedItem.Value))
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorDataCessazione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDateInputDataInizioRapportoLavoro.SelectedDate != null
            && RadDateInputDataCessazione.SelectedDate != null)
        {
            if (RadDateInputDataCessazione.SelectedDate < RadDateInputDataInizioRapportoLavoro.SelectedDate)
            {
                args.IsValid = false;
            }
        }
    }

    #endregion

    #region Caricamento ComboBox

    private void CaricaCombo()
    {
        CaricaTipiTrasformazione();
        //CaricaTipiQualifica();
        CaricaTipiCessazione();
        CaricaTipiMansione();
        //CaricaTipiCategoria();
        if (!String.IsNullOrEmpty(RadComboBoxTipologiaContrattuale.SelectedValue))
        {
            CaricaTipiCategoria(RadComboBoxTipologiaContrattuale.SelectedValue);
        }
    }

    private void CaricaTipiCategoria(String tipoContratto)
    {
        RadComboBoxCategoria.Items.Clear();

        Presenter.CaricaElementiInDropDown(
            RadComboBoxCategoria,
            sintesiBiz.GetTipiCategoria(tipoContratto),
            "Descrizione",
            "IdCategoria");
        RadComboBoxCategoria.Items.Insert(0, new RadComboBoxItem("- Selezionare la categoria -"));

        RadComboBoxCategoria.SelectedIndex = 0;
    }

    private void CaricaTipiQualifica(String tipoCategoria)
    {
        RadComboBoxQualifica.Items.Clear();

        Presenter.CaricaElementiInDropDown(
            RadComboBoxQualifica,
            sintesiBiz.GetTipiQualifica(tipoCategoria),
            "Descrizione",
            "IdQualifica");
        RadComboBoxQualifica.Items.Insert(0, new RadComboBoxItem("- Selezionare la qualifica -"));
        RadComboBoxQualifica.SelectedIndex = 0;
    }


    private void CaricaTipiCategoria()
    {
        if (RadComboBoxCategoria.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCategoria,
                commonBiz.GetTipiCategoria(),
                "Descrizione",
                "IdCategoria");
            RadComboBoxCategoria.Items.Insert(0, new RadComboBoxItem("- Selezionare la categoria -"));
        }
    }

    //private void CaricaTipiQualifica()
    //{
    //    if (RadComboBoxQualifica.Items.Count == 0)
    //    {
    //        Presenter.CaricaElementiInDropDown(
    //            RadComboBoxQualifica,
    //            commonBiz.GetTipiQualifica(),
    //            "Descrizione",
    //            "IdQualifica");
    //        RadComboBoxQualifica.Items.Insert(0, new RadComboBoxItem("- Selezionare la qualifica -"));
    //    }
    //}

    private void CaricaTipiMansione()
    {
        if (RadComboBoxMansione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxMansione,
                commonBiz.GetTipiMansione(),
                "Descrizione",
                "IdMansione");
            RadComboBoxMansione.Items.Insert(0, new RadComboBoxItem("- Selezionare la mansione -"));
        }
    }

    private void CaricaTipiCessazione()
    {
        if (RadComboBoxCausa.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCausa,
                commonBiz.GetTipiFineRapporto(),
                "Descrizione",
                "IdTipoFineRapporto");
            RadComboBoxCausa.Items.Insert(0, new RadComboBoxItem("- Selezionare la causa -"));
        }
    }

    private void CaricaTipiTrasformazione()
    {
        if (RadComboBoxCodiceTrasformazione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCodiceTrasformazione,
                sintesiBiz.GetTrasformazioniRL(),
                "descrizione",
                "codice");
            RadComboBoxCodiceTrasformazione.Items.Insert(0,
                                                         new RadComboBoxItem("- Selezionare il codice trasformazione -"));
        }
    }

    protected void RadComboBoxCategoria_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        CaricaTipiQualifica(RadComboBoxCategoria.SelectedValue);
    }

    #endregion
}