﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreDatiLocalizzazioneSintesi.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreDatiLocalizzazioneSintesi" %>
<%@ Register Src="IscrizioneLavoratoreIndirizzoSintesi.ascx" TagName="IscrizioneLavoratoreIndirizzoSintesi"
    TagPrefix="uc1" %>
<style type="text/css">
    .style3
    {
        width: 180px;
    }
</style>
<div>
    <uc1:IscrizioneLavoratoreIndirizzoSintesi ID="IscrizioneLavoratoreIndirizzo1" runat="server" />
</div>
<asp:Panel ID="PanelAltriDati" runat="server" Width="100%">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Altri recapiti </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style3">
                Telefono:
                <td>
                    <telerik:RadTextBox ID="RadTexBoxTelefono" runat="server" Width="250px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorTelefono" runat="server"
                        ControlToValidate="RadTexBoxTelefono" ErrorMessage="Telefono non corretto" ValidationExpression="^\+?[\d]{3,19}$"
                        ValidationGroup="datiLocalizzazione">
                    *
                    </asp:RegularExpressionValidator>
                </td>
        </tr>
        <tr>
            <td class="style3">
                Cellulare:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTexBoxCellulare" runat="server" Width="250px">
                </telerik:RadTextBox>
            </td>
            <td colspan="2">
                <asp:CheckBox ID="CheckBoxServizioSms" runat="server" Text="Iscrizione servizio SMS"
                    Checked="false" />
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorCellulare" runat="server"
                    ControlToValidate="RadTexBoxCellulare" ErrorMessage="Cellulare non corretto"
                    ValidationExpression="^\+?[\d]{9,16}$" ValidationGroup="datiLocalizzazione">
                    *
                </asp:RegularExpressionValidator>
                <asp:CustomValidator ID="CustomValidatorServizioSms" runat="server" ValidationGroup="datiLocalizzazione"
                    ErrorMessage="Inserire un cellulare." OnServerValidate="CustomValidatorServizioSms_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="style3">
                E-Mail:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTexBoxEmail" runat="server" Width="250px">
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server"
                    ControlToValidate="RadTexBoxEmail" ErrorMessage="E-Mail non valida" ValidationGroup="datiLocalizzazione"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
