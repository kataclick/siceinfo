﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using SintesiBusiness=TBridge.Cemi.Sintesi.Business.SintesiBusiness;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.Type.Enums;

public partial class IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreImpresaSintesi : UserControl
{
    private readonly Common commonBiz = new Common();
    //private readonly SintesiBusiness sintesiBiz = new SintesiBusiness();
    private readonly IscrizioneLavoratoriManager _iscrizioneLavoratorBiz = new IscrizioneLavoratoriManager();
    private ComuneSiceNew Comune { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
      
    }

    public void CaricaDatiSintesi(RapportoLavoroDatoreLavoro datoreLavoro, Contratto contratto)
    {
        Comune = commonBiz.GetComuneSiceNew(datoreLavoro.SedeLegale.Comune);
        //Impresa impresa = GetImpresa("PTZLLG41C22E270B");
        TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa impresa = _iscrizioneLavoratorBiz.GetImpresa(datoreLavoro.codiceFiscale);
        if (String.IsNullOrEmpty(impresa.RagioneSociale))
            Response.Redirect("~/IscrizioneLavoratori/ComunicazioneImpresaNonIscritta.aspx");
        RadTextBoxCodice.Text = impresa.IdImpresa.ToString();
        RadTextBoxCap.Text = datoreLavoro.SedeLegale.cap;
        RadTextBoxCodiceFiscale.Text = datoreLavoro.codiceFiscale;
        RadTextBoxComune.Text = Comune.Comune;
        RadTextBoxProvincia.Text = Comune.Provincia;
        RadTextBoxPartitaIva.Text = datoreLavoro.codiceFiscale;
        RadTextBoxIndirizzo.Text = datoreLavoro.SedeLegale.Indirizzo;
        RadTextBoxEMail.Text = datoreLavoro.SedeLegale.email;
        RadTextBoxTelefono.Text = datoreLavoro.SedeLegale.telefono;
        RadTextBoxRagioneSociale.Text = datoreLavoro.denominazione;
        RadTextBoxFax.Text = datoreLavoro.SedeLegale.fax;
        // TipoContratto tipoContratto = sintesiBiz.GetTipoContratto(contratto.ccnl);
        if (impresa.Contratto != null)
        {
            RadComboBoxTipologiaContrattuale.Items.Insert(0,
                                                          new RadComboBoxItem(impresa.Contratto.Descrizione,
                                                                              impresa.Contratto.IdContratto));
        }
        if (String.Equals(impresa.Stato, StatoImpresa.ATTIVA.ToString(), StringComparison.CurrentCultureIgnoreCase))
        {
            ImageStato.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
            RowImpresaNonAttiva.Visible = false;  

        }
        LabelErroreImpresa.Text = String.Concat("L'impresa risulta ", impresa.Stato);
        LabelStato.Text = impresa.Stato;
    }

    public int GetIdImpresa()
    {
        return Int32.Parse(RadTextBoxCodice.Text);
    }

    //public Impresa GetImpresa()
    //{
    //    return GetImpresa(RadTextBoxCodiceFiscale.Text);
    //}

    //private Impresa GetImpresa(string codiceFiscale)
    //{
    //    return commonBiz.GetImpresa(codiceFiscale);
    //}
}