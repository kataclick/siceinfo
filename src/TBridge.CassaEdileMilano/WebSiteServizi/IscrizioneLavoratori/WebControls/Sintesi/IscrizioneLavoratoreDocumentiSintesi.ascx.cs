﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Sintesi.Business;
using TBridge.Cemi.Sintesi.Type;
using TBridge.Cemi.Type.Entities.Sintesi;
using Telerik.Web.UI;

public partial class IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreDocumentiSintesi : UserControl
{
    private readonly SintesiBusiness sintesiBiz = new SintesiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaStatusStraniero();
            CaricaMotiviPermesso();
        }
    }

    public void CaricaDatiSintesi(DatiExtraCE datiExtraCe)
    {
         CaricaStatusStraniero();
        CaricaMotiviPermesso();
        if (datiExtraCe != null)
        {
            RadTextBoxNumeroDocumento.Text = datiExtraCe.numeroDocumento;
            //RadTextBoxMotivoPermesso.Text =  motivoPermesso.Descrizione

            if (RadComboBoxMotivoPermesso.Items.FindItemByValue(datiExtraCe.motivoPermesso, true) != null)
            {
                RadComboBoxMotivoPermesso.SelectedIndex =
                    RadComboBoxMotivoPermesso.Items.FindItemByValue(datiExtraCe.motivoPermesso, true).Index;
                RadComboBoxMotivoPermesso.Enabled = false;
            }

            if (RadComboBoxTipoDocumento.Items.FindItemByValue(datiExtraCe.tipoDocumento, true) != null)
            {
                RadComboBoxTipoDocumento.SelectedIndex =
                    RadComboBoxTipoDocumento.Items.FindItemByValue(datiExtraCe.tipoDocumento, true).Index;
                RadComboBoxTipoDocumento.Enabled = false;
            }
            RadDatePickerDataScadenzaPermesso.SelectedDate = datiExtraCe.dataScadenzaPS;
        }
    }

    public void CompletaLavoratore(Lavoratore lavoratore)
    {
        if (!String.IsNullOrEmpty(RadComboBoxTipoDocumento.Text))
        {
            lavoratore.TipoDocumento = RadComboBoxTipoDocumento.SelectedValue;
        }
        lavoratore.NumeroDocumento = Presenter.NormalizzaCampoTesto(RadTextBoxNumeroDocumento.Text);

        if (!String.IsNullOrEmpty(RadComboBoxMotivoPermesso.Text))
        {
            lavoratore.MotivoPermesso = RadComboBoxMotivoPermesso.SelectedValue;
        }
        //if (!String.IsNullOrEmpty(RadTextBoxMotivoPermesso.Text))
        //{
        //    lavoratore.MotivoPermesso = RadTextBoxMotivoPermesso.Text;
        //}
        if (RadDatePickerDataRichiestaPermesso.SelectedDate.HasValue)
        {
            lavoratore.DataRichiestaPermesso = RadDatePickerDataRichiestaPermesso.SelectedDate.Value;
        }
        if (RadDatePickerDataScadenzaPermesso.SelectedDate.HasValue)
        {
            lavoratore.DataScadenzaPermesso = RadDatePickerDataScadenzaPermesso.SelectedDate.Value;
        }
    }

    private void CaricaStatusStraniero()
    {
        if (RadComboBoxTipoDocumento.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipoDocumento,
                sintesiBiz.GetStatusStraniero(),
                "Descrizione",
                "Codice");
            RadComboBoxTipoDocumento.Items.Insert(0, new RadComboBoxItem("- Selezionare il tipo di documento -"));
        }
    }

    private void CaricaMotiviPermesso()
    {
        if (RadComboBoxMotivoPermesso.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxMotivoPermesso,
                sintesiBiz.GetMotiviPermesso(),
                "Descrizione",
                "Codice");
            RadComboBoxMotivoPermesso.Items.Insert(0, new RadComboBoxItem("- Selezionare il motivo del permesso -"));
        }
    }

    protected void CustomValidatorTipoDocumento2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadComboBoxTipoDocumento.SelectedIndex <= 0
            && !String.IsNullOrEmpty(RadTextBoxNumeroDocumento.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDataScadenzaPermesso_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDatePickerDataRichiestaPermesso.SelectedDate.HasValue &&
            RadDatePickerDataScadenzaPermesso.SelectedDate.HasValue)
        {
            if (RadDatePickerDataRichiestaPermesso.SelectedDate > RadDatePickerDataScadenzaPermesso.SelectedDate)
            {
                args.IsValid = false;
            }
        }
    }
}