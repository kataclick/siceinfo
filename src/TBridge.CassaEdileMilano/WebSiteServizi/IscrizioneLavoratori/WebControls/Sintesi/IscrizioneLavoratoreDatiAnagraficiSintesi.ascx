﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreDatiAnagraficiSintesi.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_Sintesi_IscrizioneLavoratoreDatiAnagraficiSintesi" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

    <script type="text/javascript">
    function CFUpperCaseOnly(sender, eventArgs) {
        window.setTimeout("$find('<%= RadTextBoxCodiceFiscale.ClientID %>').set_value($find('<%= RadTextBoxCodiceFiscale.ClientID %>').get_value().toUpperCase());", 50);
    }
    </script>

</telerik:RadScriptBlock>
<asp:Panel ID="PanelDatiAnagrafici" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Dati anagrafici</b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Codice:
            </td>
            <td>
                <telerik:RadNumericTextBox ID="RadTextBoxCodice" runat="server" Width="250px" Enabled="False" NumberFormat-GroupSeparator="" NumberFormat-DecimalDigits="0">
                </telerik:RadNumericTextBox>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Cognome<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" Width="250px" MaxLength="30"
                    Enabled="false">
                </telerik:RadTextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Nome<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxNome" runat="server" Width="250px" MaxLength="30"
                    Enabled="false">
                </telerik:RadTextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Sesso<b>*</b>:
            </td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListSesso" runat="server" RepeatDirection="Horizontal"
                    Enabled="false">
                    <asp:ListItem Selected="True">M</asp:ListItem>
                    <asp:ListItem>F</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data di nascita (gg/mm/aaaa)<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataNascita" runat="server" Width="250px" Enabled="false" />
            
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Codice fiscale<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" Width="250px" MaxLength="16"
                    Enabled="false">
                    <ClientEvents OnValueChanged="CFUpperCaseOnly" />
                </telerik:RadTextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Italiano<b>*</b>:
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxItaliano" runat="server" AutoPostBack="True" Checked="True"
                    Enabled="false" />
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Provincia di nascita<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxProvinciaNascita" runat="server" Width="250px"
                    Enabled="false">
                </telerik:RadTextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Comune di nascita (o Nazione)<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxComuneNascita" runat="server" Enabled="false"
                    Width="250px">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Stato civile:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxStatoCivile" runat="server" EmptyMessage="Selezionare lo stato civile"
                    Width="250px" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Cittadinanza<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxCittadinanza" runat="server" Width="250px" Enabled="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
             <%--<td>
                <telerik:RadComboBox ID="RadComboBoxCittadinanza" runat="server" EmptyMessage="Selezionare la Cittadinanza"
                    Width="250px" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorCittadinanza" runat="server" ControlToValidate="RadComboBoxCittadinanza"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare la cittadinanza" OnServerValidate="CustomValidatorCittadinanza_ServerValidate">
                *
                </asp:CustomValidator>
            </td>--%>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Livello di istruzione:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxIstruzione" runat="server" Width="250px" EmptyMessage="Selezionare il livello di istruzione"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Lingua parlata:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxLingua" runat="server" Width="250px" EmptyMessage="Selezionare la lingua parlata"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Panel>
