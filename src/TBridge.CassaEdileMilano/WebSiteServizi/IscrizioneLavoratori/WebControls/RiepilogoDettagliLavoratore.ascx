﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiepilogoDettagliLavoratore.ascx.cs"
    Inherits="WebControls_IscrizioneLavoratori_RiepilogoDettagliLavoratore" %>
<style type="text/css">
    .style1
    {
        width: 30px;
    }
</style>
<table class="borderedTable">
    <tr>
        <td>
            <b>Dati lavoratore</b>
        </td>
        <td>
            <table id="tableControlli" runat="server" class="standardTable">
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloSelezioneLavoratore" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Gestita selezione </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloAnagraficaDubbia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Anagrafica dubbia </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloAnagraficaUgualeSenzaOmocodia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Anagrafica uguale senza omocodia </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloAnagraficaUgualeConOmocodia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Anagrafica uguale con omocodia </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloAnagraficaDoppione" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Doppione </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloAnagraficaSdoppione" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Sdoppione </small>
                    </td>
                </tr>
               <%-- <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloTesseraSanitaria" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Emissione tessera sanitaria </small>
                    </td>
                </tr>--%>
               <%-- <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloIbanDiverso" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>IBAN diverso </small>
                    </td>
                </tr>--%>
               
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Codice
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCodice" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Cognome e nome
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCognomeNome" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Sesso
        </td>
        <td>
            <b>
                <asp:Label ID="LabelSesso" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data di nascita
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDataNascita" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Codice fiscale
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCodiceFiscale" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Italiano
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxItaliano" runat="server" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Comune e provincia di nascita
        </td>
        <td>
            <b>
                <asp:Label ID="LabelProvinciaComuneNascita" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Stato civile
        </td>
        <td>
            <b>
                <asp:Label ID="LabelStatoCivile" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Cittadinanza
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCittadinanza" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Livello di istruzione
        </td>
        <td>
            <b>
                <asp:Label ID="LabelLivelloIstruzione" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Lingua di comunicazione
        </td>
        <td>
            <b>
                <asp:Label ID="LabelLinguaComunicazione" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Tipo documento
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTipoDocumento" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            N documento
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDocumento" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Motivo del permesso
        </td>
        <td>
            <b>
                <asp:Label ID="LabelMotivoPermesso" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Validità documento
        </td>
        <td>
            <b>
                <asp:Label ID="LabelPeriodoValidita" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <%--<tr>
        <td class="iscrizioneLavoratoriTd">
            IBAN
        </td>
        <td>
            <b>
                <asp:Label ID="LabelIBAN" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>--%>
   <%-- <tr>
        <td class="iscrizioneLavoratoriTd">
            Cointestatario
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCointestatario" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>--%>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Richiesta informazioni carta prepagata
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxInformazioniCartaPrepagata" runat="server" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Tipo carta prepagata
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTipoPrepagata" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Taglia felpa/husky
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTagliaFelpaHusky" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Taglia pantaloni
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTagliaPantaloni" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Taglia scarpe
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTagliaScarpe" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Richiesta adesioni corsi 16 ore
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxAdesioneCorsi" runat="server" Enabled="false" />
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Note
        </td>
        <td>
            <b>
                <asp:Label ID="LabelNote" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Posizione Valida
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxPosizioneValida" runat="server" Enabled="false" Visible="false" />
        </td>
    </tr>
</table>
