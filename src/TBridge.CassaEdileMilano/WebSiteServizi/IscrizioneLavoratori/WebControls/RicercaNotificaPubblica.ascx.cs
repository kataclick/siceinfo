using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;
using Telerik.Web.UI;
using Impresa=TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;

public partial class IscrizioneLavoratori_WebControls_RicercaNotificaPubblica : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsImpresa())
            {
                Impresa impresa =
                    (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                RadNumericTextBoxCodiceImpresa.Value = impresa.IdImpresa;
                RadTextBoxPartitaIVA.Text = impresa.PartitaIVA;
                RadNumericTextBoxCodiceImpresa.CssClass = "campoDisabilitato";
                RadTextBoxPartitaIVA.CssClass = "campoDisabilitato";
                RadNumericTextBoxCodiceImpresa.Enabled = false;
                RadTextBoxPartitaIVA.Enabled = false;
            }
            else
            {
                if (GestioneUtentiBiz.IsConsulente())
                {
                    RadNumericTextBoxCodiceImpresa.CssClass = "campoDisabilitato";
                    RadTextBoxPartitaIVA.CssClass = "campoDisabilitato";
                    RadNumericTextBoxCodiceImpresa.Enabled = false;
                    RadTextBoxPartitaIVA.Enabled = false;
                }
                else
                {
                    RadNumericTextBoxCodiceImpresa.CssClass = null;
                    RadTextBoxPartitaIVA.CssClass = null;
                    RadNumericTextBoxCodiceImpresa.Enabled = true;
                    RadTextBoxPartitaIVA.Enabled = true;
                }
            }
        }
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche();
        }
    }

    private void CaricaNotifiche()
    {
        RadGridNotifiche.Visible = true;
        DichiarazioneFilter filtro = GetFiltro();

        DichiarazioneCollection dichiarazioni = biz.GetDichiarazioni(filtro);

        RadGridNotifiche.DataSource = dichiarazioni;
        RadGridNotifiche.DataBind();
    }

    private DichiarazioneFilter GetFiltro()
    {
        DichiarazioneFilter filtro = new DichiarazioneFilter();


        if (RadDatePickerDataInvioA.SelectedDate != null)
        {
            filtro.DataInvioA = RadDatePickerDataInvioA.SelectedDate;
        }

        if (RadDatePickerDataInvioDa.SelectedDate != null)
        {
            filtro.DataInvioDa = RadDatePickerDataInvioDa.SelectedDate;
        }

        filtro.TipoAttivita = null;
        if (RadioButtonListAttivita.SelectedValue != "0")
        {
            filtro.TipoAttivita =
                (TipoAttivita) Enum.Parse(typeof (TipoAttivita), RadioButtonListAttivita.SelectedValue);
        }

        if (GestioneUtentiBiz.IsImpresa())
        {
            Impresa impresa =
                (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            filtro.CodiceImpresa = impresa.IdImpresa.ToString();
        }
        if (GestioneUtentiBiz.IsConsulente())
        {
            if (ViewState["IdImpresa"] != null)
            {
                filtro.CodiceImpresa = ViewState["IdImpresa"].ToString();
            }
            else
            {
                filtro.CodiceImpresa = "-1";
            }
        }

        if (!String.IsNullOrEmpty(RadTextBoxCodiceFiscale.Text))
        {
            filtro.CodiceFiscale = RadTextBoxCodiceFiscale.Text;
        }

        if (!String.IsNullOrEmpty(RadTextBoxCognome.Text))
        {
            filtro.Cognome = RadTextBoxCognome.Text;
        }

        filtro.StatoGestionePratica = null;
        if (RadioButtonListStato.SelectedValue != "T")
        {
            filtro.StatoGestionePratica = (TipoStatoGestionePratica) Int32.Parse(RadioButtonListStato.SelectedValue);
        }

        return filtro;
    }

    protected void RadGridNotifiche_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaNotifiche();
    }

    public void Reset()
    {
        ResetFiltro();
        RadGridNotifiche.Visible = false;
    }

    private void ResetFiltro()
    {
        RadDatePickerDataInvioA.SelectedDate = null;
        RadDatePickerDataInvioDa.SelectedDate = null;
        RadioButtonListAttivita.SelectedIndex = 2;
        RadNumericTextBoxCodiceImpresa.Text = null;
        RadTextBoxPartitaIVA.Text = null;
        RadTextBoxCodiceFiscale.Text = null;
        RadTextBoxCognome.Text = null;
        RadioButtonListStato.SelectedIndex = 3;
    }

    protected void RadGridNotifiche_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Dichiarazione dichiarazione = (Dichiarazione) e.Item.DataItem;

            Label lLavoratoreCognomeNome = (Label) e.Item.FindControl("LabelLavoratoreCognomeNome");
            Label lLavoratoreDataNascita = (Label) e.Item.FindControl("LabelLavoratoreDataNascita");
            Label lLavoratoreCodiceFiscale = (Label) e.Item.FindControl("LabelLavoratoreCodiceFiscale");
            Label lImpresaCodiceRagioneSociale = (Label) e.Item.FindControl("LabelImpresaCodiceRagioneSociale");
            Label lImpresaPartitaIva = (Label) e.Item.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label) e.Item.FindControl("LabelImpresaCodiceFiscale");
            Label lStato = (Label) e.Item.FindControl("LabelStato");

            lLavoratoreCognomeNome.Text = dichiarazione.Lavoratore.CognomeNome;
            lLavoratoreCodiceFiscale.Text = dichiarazione.Lavoratore.CodiceFiscale;
            lLavoratoreDataNascita.Text = dichiarazione.Lavoratore.DataNascita.ToString("dd/MM/yyyy");
            lImpresaCodiceRagioneSociale.Text = String.Format("{0} {1}", dichiarazione.Impresa.IdImpresa,
                                                              dichiarazione.Impresa.RagioneSociale);
            lImpresaPartitaIva.Text = dichiarazione.Impresa.PartitaIva;
            lImpresaCodiceFiscale.Text = dichiarazione.Impresa.CodiceFiscale;
            lStato.Text = dichiarazione.Stato.ToString();
        }
    }

    public void CaricaImpresa(Int32 idImpresa)
    {
        RadNumericTextBoxCodiceImpresa.Value = idImpresa;
        ViewState["IdImpresa"] = idImpresa;

        CaricaNotifiche();
    }
}