﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiepilogoDettagliIndirizzo.ascx.cs"
    Inherits="IscrizioneLavoratori_WebControls_RiepilogoDettagliIndirizzo" %>
<style type="text/css">
    .style1
    {
        width: 30px;
    }
</style>
<table width="98%" class="borderedTable">
    <tr>
        <td>
            <b>Indirizzo</b>
        </td>
        <td>
            <table id="tableControlli" runat="server" class="standardTable" width="100%">
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloIndirizzo" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Indirizzo diverso </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloCellulare" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Cellulare diverso </small>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloEmail" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Email diversa </small>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Indirizzo
        </td>
        <td>
            <b>
                <asp:Label ID="LabelIndirizzo" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Telefono
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTelefono" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Rchiesta adesione servizio SMS
        </td>
        <td>
            <b>
                <asp:CheckBox ID="CheckBoxSms" runat="server" Enabled="false" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Cellulare
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCellulare" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Cellulare SMS
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCellulareSMS" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Email
        </td>
        <td>
            <b>
                <asp:Label ID="LabelEmail" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
</table>
