using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Filters;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using Impresa=TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using Lavoratore=TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;

public partial class WebControls_IscrizioneLavoratori_IscrizioneLavoratoreRiepilogo : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
    private readonly GestioneUtentiBiz gestioneUtentiBiz = new GestioneUtentiBiz();

    public bool HasAvvertimenti
    {
        get { return BulletedListAvvertimenti.Items.Count > 0; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaAttivita(TipoAttivita tipoAttivita)
    {
        LabelTipoAttivita.Text = tipoAttivita.ToString();

        switch (tipoAttivita)
        {
            case TipoAttivita.Assunzione:
            case TipoAttivita.Proroga:
                trCessazione1.Visible = false;
                trCessazione2.Visible = false;
                trTrasformazione1.Visible = false;
                trTrasformazione2.Visible = false;
                break;
            case TipoAttivita.Cessazione:
                trCessazione1.Visible = true;
                trCessazione2.Visible = true;
                trTrasformazione1.Visible = false;
                trTrasformazione2.Visible = false;
                break;
            case TipoAttivita.Trasformazione:
                trCessazione1.Visible = false;
                trCessazione2.Visible = false;
                trTrasformazione1.Visible = true;
                trTrasformazione2.Visible = true;
                break;
        }
    }

    public void CaricaImpresa(Impresa impresa)
    {
        if (impresa != null)
        {
            LabelImpresaDenominazione.Text = String.Format("{0} - {1}", impresa.IdImpresa, impresa.RagioneSociale);
            LabelImpresaPartitaIva.Text = impresa.PartitaIva;
            LabelImpresaCodiceFiscale.Text = impresa.CodiceFiscale;
            LabelImpresaIndirizzo.Text = impresa.SedeLegale;
        }
        else
        {
            ResetImpresa();
        }
    }

    private void ResetImpresa()
    {
        LabelImpresaDenominazione.Text = null;
        LabelImpresaPartitaIva.Text = null;
        LabelImpresaCodiceFiscale.Text = null;
        LabelImpresaIndirizzo.Text = null;
    }

    public void CaricaLavoratore(Lavoratore lavoratore)
    {
        if (lavoratore != null)
        {
            LabelLavoratoreDenominazione.Text = lavoratore.CognomeNome;
            LabelLavoratoreCodiceFiscale.Text = lavoratore.CodiceFiscale;
            LabelLavoratoreIndirizzo.Text = lavoratore.IndirizzoCompleto;

            VerificaSituazioneLavoratore(lavoratore);

            //ControllaDatiAnagraficiDiscordanti(lavoratore);
        }
        else
        {
            ResetLavoratore();
        }
    }

    private void ResetLavoratore()
    {
        LabelLavoratoreDenominazione.Text = null;
        LabelLavoratoreCodiceFiscale.Text = null;
        LabelLavoratoreIndirizzo.Text = null;
    }

    public void CaricaRapportoDiLavoro(RapportoDiLavoro rapportoDiLavoro)
    {
        ResetRapportoDiLavoro();

        if (rapportoDiLavoro.Contratto != null)
        {
            LabelRapportoDiLavoroContratto.Text = rapportoDiLavoro.Contratto.Descrizione;
        }
        if (rapportoDiLavoro.DataInizioValiditaRapporto.HasValue)
        {
            LabelRapportoDiLavoroDataInizio.Text =
                rapportoDiLavoro.DataInizioValiditaRapporto.Value.ToString("dd/MM/yyyy");
        }
        
        if (rapportoDiLavoro.DataFineValiditaRapporto.HasValue)
        {
            LabelAl.Visible = true;
            LabelRapportoDiLavoroDataFine.Visible = true;
            LabelRapportoDiLavoroDataFine.Text =
                rapportoDiLavoro.DataFineValiditaRapporto.Value.ToString("dd/MM/yyyy");
        }

        if (rapportoDiLavoro.Categoria != null)
        {
            LabelRapportoDiLavoroCategoria.Text = rapportoDiLavoro.Categoria.Descrizione;
        }
        if (rapportoDiLavoro.Qualifica != null)
        {
            LabelRapportoDiLavoroQualifica.Text = rapportoDiLavoro.Qualifica.Descrizione;
        }
        if (rapportoDiLavoro.Mansione != null)
        {
            LabelRapportoDiLavoroMansione.Text = rapportoDiLavoro.Mansione.Descrizione;
        }
    }

    public void CaricaDatiCessazione(RapportoDiLavoro rapportoDiLavoro)
    {
        if (rapportoDiLavoro.DataCessazione.HasValue)
        {
            LabelDatiCessazioneData.Text = rapportoDiLavoro.DataCessazione.Value.ToString("dd/MM/yyyy");
        }
        if (rapportoDiLavoro.TipoFineRapporto != null)
        {
            LabelDatiCessazioneCausa.Text = rapportoDiLavoro.TipoFineRapporto.Descrizione;
        }
    }

    public void CaricaDatiTrasformazione(RapportoDiLavoro rapportoDiLavoro)
    {
        if (rapportoDiLavoro.DataTrasformazione.HasValue)
        {
            LabelDatiTrasformazioneData.Text = rapportoDiLavoro.DataTrasformazione.Value.ToString("dd/MM/yyyy");
        }
    }

    private void ResetRapportoDiLavoro()
    {

        LabelAl.Visible = false;
        LabelRapportoDiLavoroDataFine.Visible = false;
        LabelRapportoDiLavoroDataFine.Text = null;

        LabelRapportoDiLavoroContratto.Text = null;
        LabelRapportoDiLavoroDataInizio.Text = null;
        LabelRapportoDiLavoroCategoria.Text = null;
        LabelRapportoDiLavoroQualifica.Text = null;
        LabelRapportoDiLavoroMansione.Text = null;

        LabelDatiCessazioneData.Text = null;
        LabelDatiCessazioneCausa.Text = null;

        LabelDatiTrasformazioneCodice.Text = null;
        LabelDatiTrasformazioneData.Text = null;
    }

    private void VerificaSituazioneLavoratore(Lavoratore lavoratore)
    {
        if (!lavoratore.IdLavoratore.HasValue)
        {
            // Lavoratore nuovo, verifico se esistono gi� dei rapporti aperti per il lavoratore
            // RapportoDiLavoroCollection rapportiAperti = biz.GetRapportiDiLavoroAperti(lavoratore.CodiceFiscale);
        }
    }

    public void ResetAvvertimenti()
    {
        BulletedListAvvertimenti.Items.Clear();
    }

    public void ResetAvvertimento(string messaggio)
    {
        for (int i = 0; i < BulletedListAvvertimenti.Items.Count; i++)
        {
            if (BulletedListAvvertimenti.Items[i].Text == messaggio) BulletedListAvvertimenti.Items.RemoveAt(i);
        }
    }

    public void ResetAvvertimentoLavoratore(string messaggio)
    {
        for (int i = 0; i < BulletedListAvvertimenti.Items.Count; i++)
        {
            if (BulletedListAvvertimenti.Items[i].Text.Contains(messaggio)) BulletedListAvvertimenti.Items.RemoveAt(i);
        }
    }

    public void CaricaAvvertimenti(string messaggio)
    {
        BulletedListAvvertimenti.Items.Add(messaggio);
    }

    public void ControllaDatiAnagraficiDiscordanti(Lavoratore lavoratore)
    {
        string ret = string.Empty;

        if (!lavoratore.IdLavoratore.HasValue)
        {
            FilterLavoratore filterLavoratore = new FilterLavoratore();

            filterLavoratore.CodiceFiscale = lavoratore.CodiceFiscale;

            LavoratoriConImpresaCollection lavColl = gestioneUtentiBiz.GetLavoratoriConImpresa(filterLavoratore);

            if (lavColl != null)
            {
                foreach (LavoratoreConImpresa lav in lavColl)
                {
                    if (lav.Cognome != lavoratore.Cognome)
                        ret = ret + "cognome ";

                    if (lav.Nome != lavoratore.Nome)
                        ret = ret + "nome ";

                    if (lav.DataNascita.Date != lavoratore.DataNascita.Date)
                        ret = ret + "data di nascita ";

                    if ((lav.IndirizzoCAP != lavoratore.Indirizzo.Cap) ||
                        (lav.IndirizzoProvincia != lavoratore.Indirizzo.Provincia) ||
                        (lav.IndirizzoDenominazione !=
                         String.Format("{0} {1}", lavoratore.Indirizzo.Indirizzo1, lavoratore.Indirizzo.Cap)))
                        ret = ret + "indirizzo ";

                    if (ret != string.Empty) break;
                }

                ResetAvvertimentoLavoratore(
                    "Esiste gi� un altro lavoratore con il codice fiscale dichiarato. Controllare");
                if (ret != string.Empty)
                    CaricaAvvertimenti(string.Format("{0} {1}",
                                                     "Esiste gi� un altro lavoratore con il codice fiscale dichiarato. Controllare",
                                                     ret));
            }
        }
    }

    protected void CustomValidatorDisclaimer_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = CheckBoxDisclaimer.Checked;
    }
}