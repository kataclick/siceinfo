using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

public partial class WebControls_IscrizioneLavoratori_RiepilogoDettagliLavoratore : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public Boolean CaricaDichiarazione(Dichiarazione dichiarazione)
    {
        Boolean abilitaTastoAccetta = false;

        if (dichiarazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
        {
            LabelCodice.Text = dichiarazione.Lavoratore.IdLavoratore.ToString();
        }
        else if (dichiarazione.IdLavoratoreSelezionato.HasValue)
        {
            LabelCodice.Text = dichiarazione.IdLavoratoreSelezionato.Value.ToString();
        }

        LabelCognomeNome.Text = dichiarazione.Lavoratore.CognomeNome;
        LabelSesso.Text = dichiarazione.Lavoratore.Sesso.ToString();
        LabelDataNascita.Text = dichiarazione.Lavoratore.DataNascita.ToString("dd/MM/yyyy");
        LabelCodiceFiscale.Text = dichiarazione.Lavoratore.CodiceFiscale;
        CheckBoxItaliano.Checked = dichiarazione.Lavoratore.Italiano;
        LabelProvinciaComuneNascita.Text = String.Format("{0} - {1}", dichiarazione.Lavoratore.ProvinciaNascita,
                                                         dichiarazione.Lavoratore.ComuneNascita);
        LabelStatoCivile.Text = dichiarazione.Lavoratore.IdStatoCivile;
        LabelCittadinanza.Text = dichiarazione.Lavoratore.Cittadinanza;
        LabelLivelloIstruzione.Text = dichiarazione.Lavoratore.LivelloIstruzione;
        LabelLinguaComunicazione.Text = dichiarazione.Lavoratore.LinguaComunicazione;

        LabelTipoDocumento.Text = dichiarazione.Lavoratore.TipoDocumento;
        LabelDocumento.Text = dichiarazione.Lavoratore.NumeroDocumento;
        LabelMotivoPermesso.Text = dichiarazione.Lavoratore.MotivoPermesso;
        if (dichiarazione.Lavoratore.DataRichiestaPermesso.HasValue &&
            dichiarazione.Lavoratore.DataScadenzaPermesso.HasValue)
            LabelPeriodoValidita.Text = String.Format("{0} - {1}",
                                                      dichiarazione.Lavoratore.DataRichiestaPermesso.Value.ToString(
                                                          "dd/MM/yyyy"),
                                                      dichiarazione.Lavoratore.DataScadenzaPermesso.Value.ToString(
                                                          "dd/MM/yyyy"));

        //LabelIBAN.Text = dichiarazione.Lavoratore.IBAN;
        //LabelCointestatario.Text = dichiarazione.Lavoratore.CognomeCointestatario;
        CheckBoxInformazioniCartaPrepagata.Checked = dichiarazione.Lavoratore.RichiestaInfoCartaPrepagata;
        if (dichiarazione.Lavoratore.TipoPrepagata != null)
        {
            LabelTipoPrepagata.Text = dichiarazione.Lavoratore.TipoPrepagata.Descrizione;
        }

        LabelTagliaFelpaHusky.Text = dichiarazione.Lavoratore.TagliaFelpaHusky;
        LabelTagliaPantaloni.Text = dichiarazione.Lavoratore.TagliaPantaloni;
        LabelTagliaScarpe.Text = dichiarazione.Lavoratore.TagliaScarpe;

        CheckBoxAdesioneCorsi.Checked = dichiarazione.Lavoratore.RichiestaIscrizioneCorsi16Ore;
        LabelNote.Text = dichiarazione.Lavoratore.Note;
        if (dichiarazione.NuovoLavoratore && dichiarazione.Lavoratore.PosizioneValida != null)
        {
            CheckBoxPosizioneValida.Visible = true;
            CheckBoxPosizioneValida.Checked = (bool)dichiarazione.Lavoratore.PosizioneValida;
        }
        else
        {
            CheckBoxPosizioneValida.Visible = false;
        }

        EffettuaControlli(dichiarazione);

        if (ImageControlloSelezioneLavoratore.ImageUrl == IscrizioneLavoratoriManager.URLSEMAFOROVERDE
           // && ImageControlloAnagraficaDoppione.ImageUrl == IscrizioneLavoratoriManager.URLSEMAFOROVERDE  //in caso di doppioni � sufficiente selezionare un lavoratore
           // && ImageControlloIbanDiverso.ImageUrl == IscrizioneLavoratoriManager.URLSEMAFOROVERDE         //IBAN non pi� richiesto
            )
        {
            abilitaTastoAccetta = true;
        }

        return abilitaTastoAccetta;
    }

    private void EffettuaControlli(Dichiarazione dichiarazione)
    {
        switch (dichiarazione.Attivita)
        {
            case TipoAttivita.Assunzione:
            case TipoAttivita.Cessazione:

                tableControlli.Visible = true;

                if (dichiarazione.Stato == TipoStatoGestionePratica.Approvata)
                {
                    // Se la comunicazione � gia stata approvata mostro tutti i semafori verdi
                    // Non ripeto i controlli perch� servono solo a bloccare l'approvazione

                    ImageControlloSelezioneLavoratore.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
                    ImageControlloAnagraficaUgualeSenzaOmocodia.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
                    ImageControlloAnagraficaUgualeConOmocodia.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
                    ImageControlloAnagraficaDoppione.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
                    ImageControlloAnagraficaSdoppione.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
                    ImageControlloAnagraficaDubbia.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
                }
                else
                {
                    ImageControlloSelezioneLavoratore.ImageUrl = biz.LavoratoreSelezionatoONuovo(dichiarazione);

                    LavoratoreCollection lavoratoriStessoCodiceFiscale = biz.GetLavoratoriConStessoCodiceFiscale(
                        dichiarazione.Lavoratore.CodiceFiscale);
                    LavoratoreCollection lavoratoriStessiDatiAnagrafici = biz.GetLavoratoriConStessiDatiAnagrafici(
                        dichiarazione.Lavoratore.Cognome,
                        dichiarazione.Lavoratore.Nome,
                        dichiarazione.Lavoratore.Sesso,
                        dichiarazione.Lavoratore.DataNascita,
                        dichiarazione.Lavoratore.ComuneNascita);
                    LavoratoreCollection lavoratoriStessiDatiAnagraficiECodiceFiscale = biz.
                        GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
                        dichiarazione.Lavoratore.Cognome,
                        dichiarazione.Lavoratore.Nome,
                        dichiarazione.Lavoratore.Sesso,
                        dichiarazione.Lavoratore.DataNascita,
                        dichiarazione.Lavoratore.ComuneNascita,
                        dichiarazione.Lavoratore.CodiceFiscale);
                    Lavoratore lavoratore = null;
                    if (dichiarazione.IdLavoratoreSelezionato.HasValue)
                    {
                        lavoratore = biz.GetLavoratore(dichiarazione.IdLavoratoreSelezionato.Value);
                    }

                    ImageControlloAnagraficaUgualeSenzaOmocodia.ImageUrl = biz.AnagraficaUgualeSenzaOmocodia(dichiarazione,
                                                                                                             lavoratoriStessiDatiAnagrafici);
                    ImageControlloAnagraficaUgualeConOmocodia.ImageUrl = biz.AnagraficaUgualeConOmocodia(dichiarazione,
                                                                                                         lavoratoriStessiDatiAnagrafici);
                    ImageControlloAnagraficaDoppione.ImageUrl = biz.AnagraficaDoppione(dichiarazione,
                                                                                       lavoratoriStessiDatiAnagraficiECodiceFiscale);
                    ImageControlloAnagraficaSdoppione.ImageUrl = biz.AnagraficaSdoppione(dichiarazione,
                                                                                         lavoratoriStessoCodiceFiscale,
                                                                                         lavoratoriStessiDatiAnagrafici);
                    ImageControlloAnagraficaDubbia.ImageUrl = biz.AnagraficaDubbia(dichiarazione, lavoratore);

                    //ImageControlloTesseraSanitaria.ImageUrl = biz.EmissioneTesseraSanitaria(dichiarazione);

                    // ImageControlloIbanDiverso.ImageUrl = biz.IbanDiverso(dichiarazione, lavoratore);
                    //ImageControlloInformazioniPrepagata.ImageUrl = biz.InformazioniPrepagata(dichiarazione);
                }
                break;

            default:
                tableControlli.Visible = false;
                break;
        }
    }
}