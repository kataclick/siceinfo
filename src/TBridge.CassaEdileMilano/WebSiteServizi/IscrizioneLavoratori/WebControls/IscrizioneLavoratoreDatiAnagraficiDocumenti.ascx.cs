using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Delegates;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;

public partial class WebControls_IscrizioneLavoratori_IscrizioneLavoratoreDatiAnagraficiDocumenti : UserControl
{
    public event LavoratoreCompletoSelectedEventHandler OnLavoratoreSelected;

    protected void Page_Load(object sender, EventArgs e)
    {
        RicercaLavoratore1.OnLavoratoreSelected += RicercaLavoratore1_OnLavoratoreSelected;
    }

    public void Abilita(Boolean ricerca, Boolean visualizzazione)
    {
        PanelRicerca.Enabled = ricerca;
        PanelVisualizzazione.Enabled = visualizzazione;
        PanelDocumenti.Enabled = visualizzazione;

        PanelRicerca.Visible = ricerca;
        PanelVisualizzazione.Visible = !ricerca;
        PanelDocumenti.Visible = !ricerca;

        IscrizioneLavoratoreDatiAnagrafici1.StatoCampi(!ricerca);
        //IscrizioneLavoratoreDocumenti1.StatoCampi(!visualizzazione);
    }

    private void RicercaLavoratore1_OnLavoratoreSelected(Int32 idLavoratore)
    {
        Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagrafici1.CaricaLavoratore(idLavoratore);
        IscrizioneLavoratoreDocumenti1.CaricaLavoratore(lavoratore);
        RicercaLavoratore1.Reset();

        if (OnLavoratoreSelected != null)
        {
            OnLavoratoreSelected(lavoratore);
        }
    }

    public void Reset()
    {
        IscrizioneLavoratoreDatiAnagrafici1.Reset();
        IscrizioneLavoratoreDocumenti1.Reset();
    }

    public Lavoratore GetLavoratore()
    {
        Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagrafici1.GetLavoratore();
        IscrizioneLavoratoreDocumenti1.CompletaLavoratore(lavoratore);

        return lavoratore;
    }

    public Boolean ControlloScadenzaDocumento()
    {
        return IscrizioneLavoratoreDocumenti1.ControlloScadenzaDocumento();
    }
}