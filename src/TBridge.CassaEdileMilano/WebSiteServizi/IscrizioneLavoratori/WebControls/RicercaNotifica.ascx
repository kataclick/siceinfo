﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicercaNotifica.ascx.cs"
    Inherits="IscrizioneLavoratori_RicercaNotifica"  %>
<style type="text/css">
    .style1
    {
        width: 30%;
    }
        .style2
    {
        width: 30%;
    }
        .style3
    {
        width: 20%;

    }
        .style4
    {
        width: 20%;
    }
</style>
<script type="text/jscript">

    function ManageDuplicazione() {

        var radioButtonList = document.getElementById('<%= RadioButtonListStato.ClientID %>');
        var buttons = radioButtonList.getElementsByTagName("input");

        var checkBox1 = document.getElementById('<%= CheckBoxCfUgualeRapportoAperto.ClientID %>');
        var checkBox2 = document.getElementById( '<%= CheckBoxCfUgualeDatiDiversi.ClientID %>');
        var checkBox3 = document.getElementById('<%= CheckBoxCfDiversoDatiUguali.ClientID %>');
        var checkBox4 = document.getElementById('<%= CheckBoxCfUgualeDatiUguali.ClientID %>');

        var disabled = (buttons[0].checked == false);

        ManageCheckBox(checkBox1, disabled);
        ManageCheckBox(checkBox2, disabled);
        ManageCheckBox(checkBox3, disabled);
        ManageCheckBox(checkBox4, disabled);
    }

    function ManageCheckBox(checkBox, disabled) {

        checkBox.disabled = disabled;
        if (checkBox.parentElement.tagName == 'SPAN')
            checkBox.parentElement.disabled = disabled;
        if (disabled == true)
            checkBox.checked = false;
    }
</script>
<asp:Panel ID="PanelRicercaNotifiche" runat="server" DefaultButton="ButtonRicerca">
    <table class="filledtable">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Dichiarazioni"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td colspan="2">
                            <b>Periodo </b>
                        </td>
                        <td colspan="2">
                            <b>Attività </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Dal
                        </td>
                        <td class="style2">
                            Al
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <telerik:RadDatePicker ID="RadDatePickerDataInvioDa" runat="server" Width="130px">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td class="style2">
                            <telerik:RadDatePicker ID="RadDatePickerDataInvioA" runat="server" Width="130px">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                                </DateInput>
                            </telerik:RadDatePicker>
                        </td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="RadioButtonListAttivita" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="3">
                                <asp:ListItem Value="Assunzione">Assunzione</asp:ListItem>
                                <asp:ListItem Value="Cessazione">Cessazione</asp:ListItem>
                                <asp:ListItem Selected="True" Value="0">Tutte</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Impresa </b>
                        </td>
                        <td colspan="2">
                            <b>Lavoratore </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            Codice
                        </td>
                        <td class="style2">
                            Partita Iva
                        </td>
                        <td class="style3">
                            Codice fiscale
                        </td>
                        <td class="style4">
                            Cognome
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxCodiceImpresa" runat="server" Width="130px"
                                NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" NumberFormat-AllowRounding="false">
                                <NumberFormat AllowRounding="False" DecimalDigits="0" GroupSeparator="" />
                            </telerik:RadNumericTextBox>
                        </td>
                        <td class="style2">
                            <telerik:RadTextBox ID="RadTextBoxPartitaIVA" runat="server" MaxLength="11" Width="130px">
                            </telerik:RadTextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" ValidationGroup="ricerca"
                                ErrorMessage="*" ControlToValidate="RadTextBoxPartitaIVA" ValidationExpression="^\d{11}$">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td class="style3">
                            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" MaxLength="16" Width="130px">
                            </telerik:RadTextBox>
                        </td>
                        <td class="style4">
                            <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" MaxLength="30" Width="130px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Stato </b>
                        </td>
                        <td class="style3">
                            <b>ID </b>
                        </td>
                        <td class="style4">
                            <b>Nazionalità</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                           <%-- onchange non funziona con IE8--%>
                            <asp:RadioButtonList ID="RadioButtonListStato" runat="server" RepeatDirection="Horizontal"  
                                                onclick="ManageDuplicazione()">
                                <asp:ListItem Value="0" Selected="True" >Da valutare</asp:ListItem>
                                <asp:ListItem Value="1">Approvata</asp:ListItem>
                                <asp:ListItem Value="3">In attesa di documentazione</asp:ListItem>
                                <asp:ListItem  Value="T">Tutte</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="style3">
                            <telerik:RadTextBox ID="RadTextBoxID" runat="server" MaxLength="30" Width="130px">
                            </telerik:RadTextBox>
                        </td>
                        <td class="style4">
                            <telerik:RadComboBox ID="ComboBoxNazionalita" runat="server" Width="130px" EmptyMessage="QUALSIASI" Filter="StartsWith"/>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td colspan="4">
                            <b>Duplicazione</b>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:CheckBox ID="CheckBoxCfUgualeRapportoAperto" runat="server" Text="Codice fiscale uguale - Rapporto di lavoro aperto" />
                        </td>
                        <td colspan="3">
                            <asp:CheckBox ID="CheckBoxCfUgualeDatiDiversi" runat="server" Text="Codice fiscale uguale - Dati anagrafici diversi" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                             <asp:CheckBox ID="CheckBoxCfDiversoDatiUguali" runat="server" Text="Codice fiscale diverso - Dati anagrafici uguali" />
                        </td>
                        <td colspan="3">
                           <asp:CheckBox ID="CheckBoxCfUgualeDatiUguali" runat="server" Text="Codice fiscale uguale - Dati anagrafici uguali" />
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                   <tr>
                        <td colspan="2">
                            <b>Duplicazione</b>
                        </td>
                    </tr>
                     <tr>
                        <td style="width:50%">
                        <asp:Panel ID="panel1" runat="server">
                            <asp:CheckBox ID="CheckBoxCfUgualeRapportoAperto" runat="server" Text="(1) Codice fiscale uguale - Rapporto di lavoro aperto" Enabled="false" />
                            </asp:Panel>
                            
                        </td>
                        <td style="width:50%" >
                            <asp:CheckBox ID="CheckBoxCfUgualeDatiDiversi" runat="server" Text="(2) Codice fiscale uguale - Dati anagrafici diversi" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%">
                             <asp:CheckBox ID="CheckBoxCfDiversoDatiUguali" runat="server" Text="(3) Codice fiscale diverso - Dati anagrafici uguali" Enabled="false" />
                        </td>
                        <td style="width:50%">
                           <asp:CheckBox ID="CheckBoxCfUgualeDatiUguali" runat="server" Text="(4) Codice fiscale uguale - Dati anagrafici uguali" Enabled="false" />
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca"  OnClick="ButtonRicerca_Click"
                    Width="150px" ValidationGroup="ricerca" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="RadGridNotifiche" runat="server" Width="100%" AllowPaging="True"
                    OnPageIndexChanged="RadGridNotifiche_PageIndexChanged" GridLines="None" Visible="False"
                    OnItemDataBound="RadGridNotifiche_ItemDataBound" OnSelectedIndexChanged="RadGridNotifiche_SelectedIndexChanged">
                    <MasterTableView DataKeyNames="IdDichiarazione">
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="ID" UniqueName="idDichiarazione" DataField="idDichiarazione">
                                <ItemStyle Width="20px" />
                            </telerik:GridBoundColumn>
                            <%--   <telerik:GridBoundColumn HeaderText="Attività" UniqueName="Attività" 
                                DataField="Attivita">
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>--%>
                            <telerik:GridTemplateColumn HeaderText="Attività" UniqueName="Attività">
                                <ItemTemplate>
                                    <asp:Label ID="LabelAttivitaTipo" runat="server">
                                    </asp:Label>
                                    <br />
                                    <b>
                                        <asp:Label ID="LabelRettifica" runat="server">
                                        </asp:Label>
                                    </b>
                                </ItemTemplate>
                                <ItemStyle Width="110px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Data" UniqueName="Data" DataField="DataInvio"
                                DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime" HtmlEncode="False">
                                <ItemStyle Width="40px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="LabelLavoratoreCognomeNome" runat="server">
                                        </asp:Label>
                                    </b>
                                    <br />
                                    <small>
                                        <asp:Label ID="LabelLavoratoreDataNascita" runat="server">
                                        </asp:Label>
                                        <br />
                                        <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server">
                                        </asp:Label>
                                    </small>
                                </ItemTemplate>
                                <ItemStyle Width="190px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="LabelImpresaCodiceRagioneSociale" runat="server">
                                        </asp:Label>
                                    </b>
                                    <br />
                                    <small>
                                        <asp:Label ID="LabelImpresaPartitaIva" runat="server">
                                        </asp:Label>
                                        <br />
                                        <asp:Label ID="LabelImpresaCodiceFiscale" runat="server">
                                        </asp:Label>
                                    </small>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Stato" UniqueName="Stato">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="LabelStato" runat="server">
                                        </asp:Label>
                                    </b>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Text="Dettaglio"
                                UniqueName="column">
                                <ItemStyle Width="10px" />
                            </telerik:GridButtonColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
