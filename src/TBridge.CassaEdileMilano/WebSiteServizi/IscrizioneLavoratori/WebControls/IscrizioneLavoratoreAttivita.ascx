﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreAttivita.ascx.cs" Inherits="IscrizioneLavoratori_IscrizioneLavoratoreAttivita" %>

<br />
Benvenuti nella funzione dedicata alla gestione dell’instaurazione / cessazione di rapporti di lavoro di operai edili.
<br />
<br />

<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Tipo di attività</b>
        </td>
    </tr>

    <tr>
        <td>
            
        </td>
        <td>
            <asp:RadioButtonList
                ID="RadioButtonListTipoAttivita"
                runat="server" 
                onselectedindexchanged="RadioButtonListTipoAttivita_SelectedIndexChanged" 
                AutoPostBack="True">
                <asp:ListItem Selected="True" Value="1">Assunzione</asp:ListItem>
                <asp:ListItem Value="2">Cessazione</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
</table>