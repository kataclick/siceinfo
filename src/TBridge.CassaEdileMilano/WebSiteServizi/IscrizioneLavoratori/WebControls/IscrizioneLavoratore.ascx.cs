using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using Telerik.Web.UI;
using Consulente=TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente;
using Impresa=TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;

public partial class WebControls_IscrizioneLavoratori_IscrizioneLavoratore : UserControl
{
    private const Int32 INDICESTEPATTIVITA = 0;
    private const Int32 INDICESTEPCANTIERE = 2;
    private const Int32 INDICESTEPCONFERMA = 7;
    private const Int32 INDICESTEPDATIANAGRAFICI = 3;
    private const Int32 INDICESTEPDATILOCALIZZAZIONE = 4;
    private const Int32 INDICESTEPIMPRESA = 1;
    private const Int32 INDICESTEPINFOAGGIUNTIVE = 6;
    private const Int32 INDICESTEPRAPPORTOLAVORO = 5;

    private const String VALIDATIONGROUPATTIVITA = "attivita";
    private const String VALIDATIONGROUPCANTIERE = "cantiere";
    private const String VALIDATIONGROUPCONFERMA = "conferma";
    private const String VALIDATIONGROUPDATIANAGRAFICI = "datiAnagrafici";
    private const String VALIDATIONGROUPDATILOCALIZZAZIONE = "datiLocalizzazione";
    private const String VALIDATIONGROUPINFOAGGIUNTIVE = "infoAggiuntive";
    private const String VALIDATIONGROUPRAPPORTODILAVORO = "rapportoDiLavoro";
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
    private readonly CptBusiness bizCpt = new CptBusiness();
    //private readonly GestioneUtentiBiz bizGU = new GestioneUtentiBiz();
    //private readonly Common commonBiz = new Common();

    private Boolean[] TABATTIVI
    {
        get { return (Boolean[]) ViewState["TABATTIVI"]; }
        set { ViewState["TABATTIVI"] = value; }
    }

    private Int32 indiceCorrenteTab
    {
        get { return (Int32) ViewState["IndiceCorrenteTab"]; }
        set { ViewState["IndiceCorrenteTab"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ConsulenteSelezioneImpresa1.OnImpresaSelected +=
            ConsulenteSelezioneImpresa1_OnImpresaSelected;
        IscrizioneLavoratoreAttivita1.OnAttivitaSelected +=
            IscrizioneLavoratoreAttivita1_OnAttivitaSelected;
        IscrizioneLavoratoreDatiAnagraficiDocumenti1.OnLavoratoreSelected +=
            IscrizioneLavoratoreDatiAnagraficiDocumenti1_OnLavoratoreSelected;

        if (!Page.IsPostBack)
        {
            #region Inizializzazione TABATTIVI

            Boolean[] nuoviTabAttivi = new Boolean[RadTabStripIscrizioneLavoratore.Tabs.Count];
            for (Int32 i = 0; i < RadTabStripIscrizioneLavoratore.Tabs.Count; i++)
            {
                nuoviTabAttivi[i] = true;
            }
            TABATTIVI = nuoviTabAttivi;

            indiceCorrenteTab = 0;

            #endregion

            ImpostaValidationGroupControlli();
            ModalitaAssunzione();

            if (GestioneUtentiBiz.IsConsulente())
            {
                PanelSelezioneImpresa.Visible = true;

                Int32 idImpresaSelezionata = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
                Reset(idImpresaSelezionata);
            }
            if (GestioneUtentiBiz.IsImpresa())
            {
                Impresa impresa =
                    (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                Reset(impresa.IdImpresa);
            }
        }

        GestisciAbilitazionePassi();
    }

    private void GestisciAbilitazionePassi()
    {
        foreach (RadTab tab in RadTabStripIscrizioneLavoratore.Tabs)
        {
            if (tab.Index > RadTabStripIscrizioneLavoratore.SelectedIndex)
            {
                tab.Enabled = false;
            }
            else
            {
                tab.Enabled = TABATTIVI[tab.Index];
            }
        }
    }

    private void ImpostaValidationGroupControlli()
    {
        switch (IscrizioneLavoratoreAttivita1.GetAttivita())
        {
            case TipoAttivita.Assunzione:
                IscrizioneLavoratoreCantiere1.ImpostaValidationGroup(VALIDATIONGROUPCANTIERE);
                IscrizioneLavoratoreDatiLocalizzazione1.ImpostaValidationGroup(VALIDATIONGROUPDATILOCALIZZAZIONE);
                break;
            case TipoAttivita.Cessazione:
                IscrizioneLavoratoreDatiLocalizzazione1.AbilitaValidationGroup(false);
                break;
        }
    }

    private void IscrizioneLavoratoreAttivita1_OnAttivitaSelected(TipoAttivita tipoEvento)
    {
        IscrizioneLavoratoreDatiAnagraficiDocumenti1.Reset();
        IscrizioneLavoratoreRapportoDiLavoro1.CaricaTipoAttivita(tipoEvento);

        ImpostaValidationGroupControlli(); //Bonfi per validationGroup
        switch (tipoEvento)
        {
            case TipoAttivita.Assunzione:
                ModalitaAssunzione();
                break;
            case TipoAttivita.Cessazione:
                ModalitaCessazione();
                break;
            case TipoAttivita.Trasformazione:
                ModalitaTrasformazione();
                break;
            case TipoAttivita.Proroga:
                ModalitaProroga();
                break;
        }
    }

    private void IscrizioneLavoratoreDatiAnagraficiDocumenti1_OnLavoratoreSelected(Lavoratore lavoratore)
    {
        IscrizioneLavoratoreDatiLocalizzazione1.CaricaDatiLavoratore(lavoratore);
        IscrizioneLavoratoreInformazioniAggiuntive.CaricaLavoratore(lavoratore);
        //IscrizioneLavoratoreRiepilogo1.CaricaLavoratore(lavoratore);
        CaricaDatiRapportoDiLavoro(lavoratore);
    }

    private void CaricaDatiRapportoDiLavoro(Lavoratore lavoratore)
    {
        if (ViewState["IdImpresa"] != null)
        {
            RapportoDiLavoro rapporto =
                IscrizioneLavoratoreRapportoDiLavoro1.CaricaDatiLavoratore(lavoratore.IdLavoratore.Value,
                                                                           (Int32) ViewState["IdImpresa"]);
            //IscrizioneLavoratoreRiepilogo1.CaricaRapportoDiLavoro(rapporto);
        }
    }

    private void ModalitaProroga()
    {
        #region Abilitazione/Disabilitazione Step

        Boolean[] nuoviTabAttivi = TABATTIVI;

        nuoviTabAttivi[INDICESTEPATTIVITA] = true;
        nuoviTabAttivi[INDICESTEPIMPRESA] = true;
        nuoviTabAttivi[INDICESTEPCANTIERE] = false;
        nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
        nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
        nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
        nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = false;
        nuoviTabAttivi[INDICESTEPCONFERMA] = true;

        TABATTIVI = nuoviTabAttivi;

        #endregion

        IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Proroga);
        IscrizioneLavoratoreDatiAnagraficiDocumenti1.Abilita(true, false);
    }

    private void ModalitaTrasformazione()
    {
        #region Abilitazione/Disabilitazione Step

        Boolean[] nuoviTabAttivi = TABATTIVI;

        nuoviTabAttivi[INDICESTEPATTIVITA] = true;
        nuoviTabAttivi[INDICESTEPIMPRESA] = true;
        nuoviTabAttivi[INDICESTEPCANTIERE] = false;
        nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
        nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
        nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
        nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = false;
        nuoviTabAttivi[INDICESTEPCONFERMA] = true;

        TABATTIVI = nuoviTabAttivi;

        #endregion

        IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Trasformazione);
        IscrizioneLavoratoreDatiAnagraficiDocumenti1.Abilita(true, false);
    }

    private void ModalitaCessazione()
    {
        #region Abilitazione/Disabilitazione Step

        Boolean[] nuoviTabAttivi = TABATTIVI;

        nuoviTabAttivi[INDICESTEPATTIVITA] = true;
        nuoviTabAttivi[INDICESTEPIMPRESA] = true;
        nuoviTabAttivi[INDICESTEPCANTIERE] = false;
        nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
        nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
        nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
        nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = false;
        nuoviTabAttivi[INDICESTEPCONFERMA] = true;

        TABATTIVI = nuoviTabAttivi;

        #endregion

        IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Cessazione);
        IscrizioneLavoratoreDatiAnagraficiDocumenti1.Abilita(true, false);
    }

    private void ModalitaAssunzione()
    {
        #region Abilitazione/Disabilitazione Step

        Boolean[] nuoviTabAttivi = TABATTIVI;

        nuoviTabAttivi[INDICESTEPATTIVITA] = true;
        nuoviTabAttivi[INDICESTEPIMPRESA] = true;
        nuoviTabAttivi[INDICESTEPCANTIERE] = true;
        nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
        nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
        nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
        nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = true;
        nuoviTabAttivi[INDICESTEPCONFERMA] = true;

        TABATTIVI = nuoviTabAttivi;

        #endregion

        IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Assunzione);
        IscrizioneLavoratoreDatiAnagraficiDocumenti1.Abilita(false, true);
    }

    private void ConsulenteSelezioneImpresa1_OnImpresaSelected(int idImpresa, string codiceRagioneSociale)
    {
        Reset(idImpresa);
    }

    private void Reset(Int32 idImpresa)
    {
        // Vanno svuotati tutti i campi legati in qualche modo all'impresa
        ViewState["IdImpresa"] = idImpresa;

        if (idImpresa > 0)
        {
            TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa impresa =
                IscrizioneLavoratoreImpresa1.CaricaImpresa(idImpresa);

            IscrizioneLavoratoreDatiLocalizzazione1.CaricaImpresa(impresa);
            IscrizioneLavoratoreRiepilogo1.CaricaImpresa(impresa);
            IscrizioneLavoratoreDatiAnagraficiDocumenti1.Reset();
            IscrizioneLavoratoreRapportoDiLavoro1.CaricaTipoContratto(impresa);
            ButtonAvanti.Enabled = true;
        }
        else
        {
            ButtonAvanti.Enabled = false;
        }
    }

    protected void RadTabStripIscrizioneLavoratore_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (RadTabStripIscrizioneLavoratore.SelectedIndex > indiceCorrenteTab)
        {
            RadTabStripIscrizioneLavoratore.SelectedIndex = indiceCorrenteTab;
            RadMultiPageIscrizioneLavoratore.SelectedIndex = indiceCorrenteTab;
        }

        VerificaVisualizzazioneTastoIndietro();
        VerificaVisualizzazioneTastoAvanti();

        indiceCorrenteTab = RadTabStripIscrizioneLavoratore.SelectedIndex;
        GestisciAbilitazionePassi();
    }

    protected void ButtonAvanti_Click(object sender, EventArgs e)
    {
        Boolean conferma = false;

        if (RadTabStripIscrizioneLavoratore.SelectedIndex == (INDICESTEPCONFERMA - 1))
        {
            ButtonAvanti.Text = "Conferma";
        }
        else
        {
            if (ButtonAvanti.Text == "Conferma")
            {
                conferma = true;
            }

            ButtonAvanti.Text = "Avanti";
        }

        switch (RadTabStripIscrizioneLavoratore.SelectedIndex)
        {
            case INDICESTEPATTIVITA:
                IscrizioneLavoratoreCantiere1.CaricaProvinciaMilano();
                if (!conferma)
                {
                    Page.Validate(VALIDATIONGROUPATTIVITA);
                }
                else
                {
                    Page.Validate(VALIDATIONGROUPCONFERMA);
                }
                break;
            case INDICESTEPCANTIERE:
                Page.Validate(VALIDATIONGROUPCANTIERE);
                break;
            case INDICESTEPDATIANAGRAFICI:
                Page.Validate(VALIDATIONGROUPDATIANAGRAFICI);
                break;
            case INDICESTEPDATILOCALIZZAZIONE:
                Page.Validate(VALIDATIONGROUPDATILOCALIZZAZIONE);
                break;
            case INDICESTEPRAPPORTOLAVORO:
                Page.Validate(VALIDATIONGROUPRAPPORTODILAVORO);
                break;
            case INDICESTEPINFOAGGIUNTIVE:
                {
                    Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
                    IscrizioneLavoratoreDatiLocalizzazione1.CompletaLavoratore(lavoratore);
                    IscrizioneLavoratoreRiepilogo1.ControllaDatiAnagraficiDiscordanti(lavoratore);
                    if (!IscrizioneLavoratoreRiepilogo1.HasAvvertimenti)
                        IscrizioneLavoratoreRiepilogo1.FindControl("LabelAvvertimenti").Visible = false;
                }
                Page.Validate(VALIDATIONGROUPINFOAGGIUNTIVE);
                break;
            case INDICESTEPCONFERMA:
                Page.Validate(VALIDATIONGROUPCONFERMA);

                if (Page.IsValid)
                {
                    Dichiarazione dichiarazione = CreaDichiarazione();
                    dichiarazione.IdUtente = GestioneUtentiBiz.GetIdUtente();

                    DichiarazioneCollection dichiarazioni =
                        biz.GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(dichiarazione.IdImpresa, dichiarazione.Lavoratore.CodiceFiscale, dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto, dichiarazione.RapportoDiLavoro.DataCessazione);
                    if (dichiarazioni.Count == 0)
                    {
                        if (biz.InsertDichiarazione(dichiarazione))
                        {
                            //Context.Items["Dichiarazione"] = dichiarazione;
                            //Response.Redirect("~/IscrizioneLavoratori/ComunicazioneEffettuata.aspx");
                            //Server.Transfer("~/IscrizioneLavoratori/ComunicazioneEffettuata.aspx");

                            Session["Dichiarazione"] = dichiarazione;
                            Response.Redirect("~/IscrizioneLavoratori/ComunicazioneEffettuata.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("~/IscrizioneLavoratori/ComunicazioneGiaEffettuata.aspx");
                    }
                }
                break;
        }

        if (Page.IsValid)
        {
            CompletaRiassunto();

            do
            {
                RadTabStripIscrizioneLavoratore.SelectedIndex++;
                RadMultiPageIscrizioneLavoratore.SelectedIndex++;
            } while (!TABATTIVI[RadTabStripIscrizioneLavoratore.SelectedIndex]);

            VerificaVisualizzazioneTastoIndietro();
            VerificaVisualizzazioneTastoAvanti();
        }

        indiceCorrenteTab = RadTabStripIscrizioneLavoratore.SelectedIndex;
        GestisciAbilitazionePassi();
    }

    private void CompletaRiassunto()
    {
        TipoAttivita attivita = IscrizioneLavoratoreAttivita1.GetAttivita();
        Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
        IscrizioneLavoratoreDatiLocalizzazione1.CompletaLavoratore(lavoratore);
        RapportoDiLavoro rapporto = IscrizioneLavoratoreRapportoDiLavoro1.GetRapportoDiLavoro();


        IscrizioneLavoratoreRiepilogo1.CaricaLavoratore(lavoratore);
        IscrizioneLavoratoreRiepilogo1.CaricaRapportoDiLavoro(rapporto);
        switch (attivita)
        {
            case TipoAttivita.Cessazione:
                IscrizioneLavoratoreRiepilogo1.CaricaDatiCessazione(rapporto);
                break;
            case TipoAttivita.Trasformazione:
                IscrizioneLavoratoreRiepilogo1.CaricaDatiTrasformazione(rapporto);
                break;
        }
    }

    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        do
        {
            RadTabStripIscrizioneLavoratore.SelectedIndex--;
            RadMultiPageIscrizioneLavoratore.SelectedIndex--;
        } while (!TABATTIVI[RadTabStripIscrizioneLavoratore.SelectedIndex]);

        VerificaVisualizzazioneTastoIndietro();
        VerificaVisualizzazioneTastoAvanti();

        indiceCorrenteTab = RadTabStripIscrizioneLavoratore.SelectedIndex;
        GestisciAbilitazionePassi();
    }

    private void VerificaVisualizzazioneTastoIndietro()
    {
        if (RadTabStripIscrizioneLavoratore.SelectedIndex == 0 && RadMultiPageIscrizioneLavoratore.SelectedIndex == 0)
        {
            ButtonIndietro.Visible = false;
        }
        else
        {
            ButtonIndietro.Visible = true;
        }
    }

    private void VerificaVisualizzazioneTastoAvanti()
    {
        Int32 indiceUltimoPasso = RadTabStripIscrizioneLavoratore.Tabs.Count - 1;

        if (RadTabStripIscrizioneLavoratore.SelectedIndex == indiceUltimoPasso
            && RadMultiPageIscrizioneLavoratore.SelectedIndex == indiceUltimoPasso)
        {
            ButtonAvanti.Text = "Conferma";

            #region Click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('");
            sb.Append(VALIDATIONGROUPCONFERMA);
            sb.Append("') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonAvanti, null) + ";");
            sb.Append("return true;");
            ButtonAvanti.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAvanti);

            #endregion
        }
        else
        {
            ButtonAvanti.Text = "Avanti";
        }
    }

    private Dichiarazione CreaDichiarazione()
    {
        Dichiarazione dichiarazione = new Dichiarazione();

        dichiarazione.Attivita = IscrizioneLavoratoreAttivita1.GetAttivita();

        dichiarazione.Stato = TipoStatoGestionePratica.DaValutare;
        if (dichiarazione.Attivita == TipoAttivita.Cessazione)
        {
            dichiarazione.Stato = TipoStatoGestionePratica.Approvata;
        }

        dichiarazione.IdImpresa = IscrizioneLavoratoreImpresa1.GetIdImpresa();
        dichiarazione.Lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
        IscrizioneLavoratoreDatiLocalizzazione1.CompletaLavoratore(dichiarazione.Lavoratore);
        IscrizioneLavoratoreInformazioniAggiuntive.CompletaLavoratore(dichiarazione.Lavoratore);
        dichiarazione.RapportoDiLavoro = IscrizioneLavoratoreRapportoDiLavoro1.GetRapportoDiLavoro();

        if (GestioneUtentiBiz.IsConsulente())
        {
            Consulente consulente =
                (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            dichiarazione.IdConsulente = consulente.IdConsulente;
        }

        switch (dichiarazione.Attivita)
        {
            case TipoAttivita.Assunzione:
                dichiarazione.Cantiere = IscrizioneLavoratoreCantiere1.GetIndirizzo();
                break;
        }

        return dichiarazione;
    }


    private Boolean ControlloPresenzaInNotifiche(Indirizzo indirizzo)
    {
        Boolean ret = true;

        NotificaFilter filtro = new NotificaFilter();
        filtro.Indirizzo = indirizzo.Indirizzo1;
        filtro.IndirizzoComune = indirizzo.ComuneDescrizione;
        NotificaCollection notifiche =
            bizCpt.RicercaNotifiche(filtro);
        if (notifiche == null || notifiche.Count == 0)
        {
            //BulletedListAvvertimenti.Items.Add(
            //    "L'indirizzo utilizzato per il cantiere non � presente nelle notifiche preliminari");

            ret = false;
        }

        return ret;
    }

    #region Custom Validators

    protected void CustomValidatorSelezioneImpresa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (GestioneUtentiBiz.IsConsulente())
        {
            Int32 idImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();

            if (idImpresa <= 0)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();

        if (String.IsNullOrEmpty(lavoratore.Cognome) && !lavoratore.IdLavoratore.HasValue)
        {
            args.IsValid = false;
        }

        if (!IscrizioneLavoratoreDatiAnagraficiDocumenti1.ControlloScadenzaDocumento())
        {
            //args.IsValid = false;

            IscrizioneLavoratoreRiepilogo1.ResetAvvertimento("Il documento inserito per il lavoratore risulta scaduto.");
            IscrizioneLavoratoreRiepilogo1.CaricaAvvertimenti(
                "Il documento inserito per il lavoratore risulta scaduto.");
        }
        else
            IscrizioneLavoratoreRiepilogo1.ResetAvvertimento("Il documento inserito per il lavoratore risulta scaduto.");
    }

    protected void CustomValidatorCantiere_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        Indirizzo indirizzo = IscrizioneLavoratoreCantiere1.GetIndirizzo();

        if (indirizzo != null && !ControlloPresenzaInNotifiche(indirizzo))
        {
            //args.IsValid = false;

            IscrizioneLavoratoreRiepilogo1.ResetAvvertimento(CustomValidatorCantiere.ErrorMessage);
            IscrizioneLavoratoreRiepilogo1.CaricaAvvertimenti(
                CustomValidatorCantiere.ErrorMessage);
        }
        else IscrizioneLavoratoreRiepilogo1.ResetAvvertimento(CustomValidatorCantiere.ErrorMessage);
    }

    protected void CustomValidatorIndirizzoCantiere_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IscrizioneLavoratoreCantiere1.IndirizzoConfermato();
    }


    protected void CustomValidatorIndirizzoLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = IscrizioneLavoratoreDatiLocalizzazione1.IndirizzoConfermato();
    }

    protected void CustomValidatorCantiereIndirizziImpresaConsulente_ServerValidate(object source,
                                                                                    ServerValidateEventArgs args)
    {
        // Controllo che l'indirizzo del cantiere non sia uguale a:
        // - sede legale impresa
        // - sede amministrativa impresa
        // - sede consulente (se � consulente ad inserire)

        args.IsValid = true;

        Indirizzo indirizzo = IscrizioneLavoratoreCantiere1.GetIndirizzo();

        Consulente consulente;
        try
        {
            consulente =
                (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        }
        catch
        {
            consulente = null;
        }

        TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa impresa = IscrizioneLavoratoreImpresa1.GetImpresa();

        if (indirizzo != null)
        {
            if (((impresa.SedeLegaleIndirizzo == string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)) &&
                 (impresa.SedeLegaleComune == indirizzo.ComuneDescrizione) &&
                 (impresa.SedeLegaleProvincia == indirizzo.Provincia))
                ||
                ((impresa.SedeAmministrativaIndirizzo ==
                  string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)) &&
                 (impresa.SedeAmministrativaComune == indirizzo.ComuneDescrizione) &&
                 (impresa.SedeAmministrativaProvincia == indirizzo.Provincia))
                ||
                ((consulente != null) &&
                 (consulente.Indirizzo == string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)))
                )
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorLavoratoreIndirizziImpresaConsulente_ServerValidate(object source,
                                                                                      ServerValidateEventArgs args)
    {
        // Controllo che l'indirizzo del lavoratore non sia uguale a:
        // - sede legale impresa
        // - sede amministrativa impresa
        // - sede consulente (se � consulente ad inserire)

        args.IsValid = true;

        Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
        Indirizzo indirizzo = lavoratore.Indirizzo;

        Consulente consulente;
        try
        {
            consulente =
                (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();
        }
        catch
        {
            consulente = null;
        }

        TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa impresa = IscrizioneLavoratoreImpresa1.GetImpresa();

        if (indirizzo != null)
        {
            if (((impresa.SedeLegaleIndirizzo == string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)) &&
                 (impresa.SedeLegaleComune == indirizzo.ComuneDescrizione) &&
                 (impresa.SedeLegaleProvincia == indirizzo.Provincia))
                ||
                ((impresa.SedeAmministrativaIndirizzo ==
                  string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)) &&
                 (impresa.SedeAmministrativaComune == indirizzo.ComuneDescrizione) &&
                 (impresa.SedeAmministrativaProvincia == indirizzo.Provincia))
                ||
                ((consulente != null) &&
                 (consulente.Indirizzo == string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)))
                )
            {
                args.IsValid = false;
            }
        }
    }

    #endregion
}