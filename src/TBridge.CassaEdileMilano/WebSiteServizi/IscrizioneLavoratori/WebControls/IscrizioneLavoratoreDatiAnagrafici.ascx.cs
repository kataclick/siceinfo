using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Sintesi.Business;
using Telerik.Web.UI;

public partial class IscrizioneLavoratori_IscrizioneLavoratoreDatiAnagrafici : UserControl
{
    private const String ValidationGroup = "datiAnagrafici";
    private readonly Common _commonBiz = new Common();
    private readonly IscrizioneLavoratoriManager _iscrizioneBiz = new IscrizioneLavoratoriManager();
    private readonly SintesiBusiness _sintesiBiz = new SintesiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaCombo();
        }
    }

    private void CaricaCombo()
    {
        CaricaProvince();
        CaricaTitoliDiStudio();
        CaricaStatiCivili();
        CaricaNazionalita();
        CaricaLingue();
    }

    private void CaricaLingue()
    {
        if (RadComboBoxLingua.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxLingua,
                _commonBiz.GetLingue(),
                "descrizione",
                "IdLingua");
        }
        RadComboBoxLingua.Items.Insert(0, new RadComboBoxItem("- Selezionare la lingua -"));
    }

    private void CaricaNazionalita()
    {
        if (RadComboBoxCittadinanza.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCittadinanza,
                _commonBiz.GetNazioni(),
                "Value",
                "Key");
        }
        RadComboBoxCittadinanza.Items.Insert(0, new RadComboBoxItem("- Selezionare la cittadinanza -"));
    }

    private void CaricaStatiCivili()
    {
        if (RadComboBoxStatoCivile.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxStatoCivile,
                _commonBiz.GetStatiCivili(),
                "descrizione",
                "IdStatoCivile");
        }
        RadComboBoxStatoCivile.Items.Insert(0, new RadComboBoxItem("- Selezionare lo stato civile -"));
    }

    private void CaricaTitoliDiStudio()
    {
        if (RadComboBoxIstruzione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxIstruzione,
                _sintesiBiz.GetTitoliDiStudio(),
                "descrizione",
                "codice");
        }
        RadComboBoxIstruzione.Items.Insert(0, new RadComboBoxItem("- Selezionare il livello di istruzione -"));
    }

    private void CaricaProvince()
    {
        if (RadComboBoxProvinciaNascita.Items.Count == 0)
        {
            Presenter.CaricaProvince(RadComboBoxProvinciaNascita);
            RadComboBoxProvinciaNascita.Items.Insert(0, new RadComboBoxItem("- Seleziona la Provincia -"));
        }
    }

    public void StatoCampi(Boolean abilita)
    {
        //PanelDatiAnagrafici.Enabled = abilita;
        AbilitazioneValidator(abilita);
    }

    public Lavoratore CaricaLavoratore(Int32 idLavoratore)
    {
        Reset();
        CaricaCombo();
        //AbilitazioneValidator(false);

        Lavoratore lavoratore = _iscrizioneBiz.GetLavoratore(idLavoratore);

        RadTextBoxCodice.Text = lavoratore.IdLavoratore.ToString();
        RadTextBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;
        RadTextBoxCognome.Text = lavoratore.Cognome;
        RadTextBoxNome.Text = lavoratore.Nome;
        RadDatePickerDataNascita.SelectedDate = lavoratore.DataNascita;
        RadioButtonListSesso.SelectedValue = lavoratore.Sesso.ToString();
        if (!String.IsNullOrEmpty(lavoratore.ProvinciaNascita))
        {
            if (lavoratore.ProvinciaNascita == "EE")
            {
                CheckBoxItaliano.Checked = false;
                RadComboBoxProvinciaNascita.Text = "EE";
                Presenter.CaricaComuni(RadComboBoxComuneNascita, "EE");
            }
            else
            {
                CheckBoxItaliano.Checked = true;
                RadComboBoxProvinciaNascita.SelectedValue = lavoratore.ProvinciaNascita;
                Presenter.CaricaComuni(RadComboBoxComuneNascita, RadComboBoxProvinciaNascita.SelectedValue);
            }
            RadComboBoxProvinciaNascita.Enabled = CheckBoxItaliano.Checked;
        }
        else
        {
            RadComboBoxProvinciaNascita.Text = " ";
        }
        if (!String.IsNullOrEmpty(lavoratore.ComuneNascita))
        {
            RadComboBoxComuneNascita.SelectedIndex =
                RadComboBoxComuneNascita.FindItemByText(lavoratore.ComuneNascita).Index;
        }
        else
        {
            RadComboBoxComuneNascita.Text = " ";
        }
        if (!String.IsNullOrEmpty(lavoratore.IdStatoCivile))
        {
            RadComboBoxStatoCivile.SelectedValue = lavoratore.IdStatoCivile;
        }
        else
        {
            RadComboBoxStatoCivile.Text = " ";
        }
        if (!String.IsNullOrEmpty(lavoratore.NazioneNascita))
        {
            RadComboBoxCittadinanza.SelectedValue = lavoratore.NazioneNascita;
        }
        else
        {
            RadComboBoxCittadinanza.Text = " ";
        }
        RadComboBoxIstruzione.Text = " ";
        RadComboBoxLingua.Text = " ";

        return lavoratore;
    }

    protected void RadComboBoxProvinciaNascita_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        CaricaComuni();

        StringBuilder script = new StringBuilder();
        script.Append("Sys.Application.add_load(function(){");
        script.Append("var combo2 = $find('" + RadComboBoxComuneNascita.ClientID + "');");
        script.Append("if(combo2!=null && combo2.get_enabled()){combo2.get_inputDomElement().focus();");
        script.Append("combo2.selectText(0, combo2.get_text().length - 1); }});");

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectText", script.ToString(), true);
    }

    private void CaricaComuni()
    {
        RadComboBoxComuneNascita.Text = null;
        RadComboBoxComuneNascita.ClearSelection();

        Presenter.CaricaComuni(RadComboBoxComuneNascita, RadComboBoxProvinciaNascita.SelectedValue);
        RadComboBoxComuneNascita.Items.Insert(0, new RadComboBoxItem("- Selezionare il Comune -"));
    }

    protected void CheckBoxItaliano_CheckedChanged(object sender, EventArgs e)
    {
        RadComboBoxProvinciaNascita.Enabled = CheckBoxItaliano.Checked;

        if (!CheckBoxItaliano.Checked)
        {
            RadComboBoxProvinciaNascita.ClearSelection();
            RadComboBoxProvinciaNascita.Text = "EE";
            Presenter.CaricaComuni(RadComboBoxComuneNascita, "EE");
            RadComboBoxComuneNascita.ClearSelection();
            RadComboBoxComuneNascita.Text = null;
            RadComboBoxComuneNascita.Items.Insert(0, new RadComboBoxItem("- Selezionare la Nazione -"));
        }
        else
        {
            RadComboBoxProvinciaNascita.Text = "";
            RadComboBoxProvinciaNascita.ClearSelection();
            RadComboBoxComuneNascita.Items.Clear();
            RadComboBoxComuneNascita.ClearSelection();
            RadComboBoxComuneNascita.Text = null;
        }
    }

    public void Reset()
    {
        RadTextBoxCodice.Text = null;
        RadTextBoxCognome.Text = null;
        RadTextBoxNome.Text = null;
        RadTextBoxCodiceFiscale.Text = null;
        RadioButtonListSesso.SelectedValue = "M";
        RadDatePickerDataNascita.SelectedDate = null;
        CheckBoxItaliano.Checked = true;
        RadComboBoxProvinciaNascita.Text = null;
        RadComboBoxProvinciaNascita.Enabled = true;
        RadComboBoxProvinciaNascita.ClearSelection();
        RadComboBoxComuneNascita.Items.Clear();
        RadComboBoxComuneNascita.Text = null;
        RadComboBoxComuneNascita.ClearSelection();
        RadComboBoxStatoCivile.ClearSelection();
        RadComboBoxCittadinanza.ClearSelection();
        RadComboBoxIstruzione.ClearSelection();
        RadComboBoxLingua.ClearSelection();

        AbilitazioneValidator(true);
    }

    private void AbilitazioneValidator(Boolean abilita)
    {
        foreach (BaseValidator validator in Page.Validators)
        {
            if (validator.ValidationGroup == ValidationGroup
                && validator.ID != "CustomValidatorLavoratore")
            {
                validator.Enabled = abilita;
            }
        }
    }

    public Lavoratore GetLavoratore()
    {
        Lavoratore lavoratore = new Lavoratore();

        if (!String.IsNullOrEmpty(RadTextBoxCodice.Text))
        {
            lavoratore.IdLavoratore = Int32.Parse(RadTextBoxCodice.Text);
            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
        }
        else
        {
            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
        }
        lavoratore.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
        lavoratore.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
        lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);

        lavoratore.Italiano = CheckBoxItaliano.Checked;

        if (RadDatePickerDataNascita.SelectedDate.HasValue)
        {
            lavoratore.DataNascita = RadDatePickerDataNascita.SelectedDate.Value;
        }
        if (!String.IsNullOrEmpty(RadComboBoxProvinciaNascita.SelectedValue))
        {
            lavoratore.ProvinciaNascita = RadComboBoxProvinciaNascita.SelectedValue;
        }
        if (!String.IsNullOrEmpty(RadComboBoxComuneNascita.SelectedValue))
        {
            lavoratore.ComuneNascita = RadComboBoxComuneNascita.SelectedValue;
        }
        lavoratore.Sesso = RadioButtonListSesso.SelectedValue[0];
        if (!String.IsNullOrEmpty(RadComboBoxStatoCivile.SelectedValue))
        {
            lavoratore.IdStatoCivile = RadComboBoxStatoCivile.SelectedValue;
        }
        if (!String.IsNullOrEmpty(RadComboBoxCittadinanza.SelectedValue))
        {
            lavoratore.PaeseNascita = RadComboBoxCittadinanza.SelectedValue;
            lavoratore.Cittadinanza = RadComboBoxCittadinanza.SelectedValue;
        }
        if (!String.IsNullOrEmpty(RadComboBoxIstruzione.SelectedValue))
        {
            lavoratore.LivelloIstruzione = RadComboBoxIstruzione.SelectedValue;
        }
        if (!String.IsNullOrEmpty(RadComboBoxLingua.SelectedValue))
        {
            lavoratore.IdLingua = RadComboBoxLingua.SelectedValue;
        }

        return lavoratore;
    }

    #region Custom Validators

    protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (RadDatePickerDataNascita.SelectedDate.HasValue)
            {
                if (!CodiceFiscaleManager.VerificaPrimi12CaratteriCodiceFiscale(
                    RadComboBoxProvinciaNascita.SelectedValue,
                    RadComboBoxComuneNascita.Text,
                    Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text),
                    Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text),
                    RadioButtonListSesso.SelectedValue,
                    RadDatePickerDataNascita.SelectedDate.Value,
                    Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CodiceFiscaleCarattereControllo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(RadTextBoxCodiceFiscale.Text))
            {
                args.IsValid = false;
            }
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorProvinciaNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxProvinciaNascita.SelectedIndex > 0 || (!CheckBoxItaliano.Checked))
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorComuneNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxComuneNascita.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    //protected void CustomValidatorStatoCivile_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    if (RadComboBoxStatoCivile.SelectedItem == null 
    //        && !String.IsNullOrEmpty(RadComboBoxStatoCivile.Text))
    //    {
    //        args.IsValid = false;
    //    }
    //}

    protected void CustomValidatorCittadinanza_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxCittadinanza.SelectedIndex > 0)
        {
            args.IsValid = true;
            //Per verifica appartenenza unione europea - Ci perso tempo ma non lo vogliono fare...
            //if (RadComboBoxCittadinanza.SelectedIndex == 1)
            //  Page.Validate("datiAnagraficiExtraUE");
        }
    }

    //protected void CustomValidatorIstruzione_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    if (RadComboBoxIstruzione.SelectedItem == null
    //        && !String.IsNullOrEmpty(RadComboBoxIstruzione.Text))
    //    {
    //        args.IsValid = false;
    //    }
    //}

    //protected void CustomValidatorLingua_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    if (RadComboBoxLingua.SelectedItem == null
    //        && !String.IsNullOrEmpty(RadComboBoxLingua.Text))
    //    {
    //        args.IsValid = false;
    //    }
    //}

    protected void CustomValidatorDataNascita_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (RadDatePickerDataNascita.SelectedDate != null)
            if (RadDatePickerDataNascita.SelectedDate.Value > DateTime.Now.AddYears(-16))
            {
                args.IsValid = false;
            }
    }

    #endregion
}