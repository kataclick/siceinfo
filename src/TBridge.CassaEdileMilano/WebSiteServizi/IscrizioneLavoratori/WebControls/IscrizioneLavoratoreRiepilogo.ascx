﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreRiepilogo.ascx.cs"
    Inherits="WebControls_IscrizioneLavoratori_IscrizioneLavoratoreRiepilogo" %>
<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        function openRadWindow() {
            var oWindow = radopen("http://ww2.cassaedilemilano.it/LinkClick.aspx?fileticket=r7nVjPl1WVQ%3d&tabid=147&language=it-IT", null);
            oWindow.set_title("Cassa Edile");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 600);
            oWindow.center();
        }
    </script>

</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" AutoSize="true" IconUrl="~/images/favicon.png" />
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Riepilogo dei dati</b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Attività
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTipoAttivita" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Impresa
        </td>
        <td>
            <b>
                <asp:Label ID="LabelImpresaDenominazione" runat="server">
                </asp:Label>
            </b>
            <br />
            <asp:Label ID="LabelImpresaPartitaIva" runat="server" CssClass="campoPiccolo">
            </asp:Label>
            -
            <asp:Label ID="LabelImpresaCodiceFiscale" runat="server" CssClass="campoPiccolo">
            </asp:Label>
            <br />
            <asp:Label ID="LabelImpresaIndirizzo" runat="server" CssClass="campoPiccolo">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Lavoratore
        </td>
        <td>
            <b>
                <asp:Label ID="LabelLavoratoreDenominazione" runat="server">
                </asp:Label>
            </b>
            <br />
            <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server" CssClass="campoPiccolo">
            </asp:Label>
            <br />
            <asp:Label ID="LabelLavoratoreIndirizzo" runat="server" CssClass="campoPiccolo">
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Rapporto di lavoro
        </td>
        <td>
            <b>
                <asp:Label ID="LabelRapportoDiLavoroContratto" runat="server">
                </asp:Label>
            </b>&nbsp;dal&nbsp; <b>
                <asp:Label ID="LabelRapportoDiLavoroDataInizio" runat="server">
                </asp:Label>
            </b>
            <asp:Label ID="LabelAl" Visible="false" runat="server" >&nbsp;al&nbsp;</asp:Label>
            <b>
            <asp:Label ID="LabelRapportoDiLavoroDataFine" Visible="false" runat="server" />
            </b>
            <br />
            <asp:Label ID="LabelRapportoDiLavoroCategoria" runat="server" CssClass="campoPiccolo">
            </asp:Label>
            <br />
            <asp:Label ID="LabelRapportoDiLavoroQualifica" runat="server" CssClass="campoPiccolo">
            </asp:Label>
            <br />
            <asp:Label ID="LabelRapportoDiLavoroMansione" runat="server" CssClass="campoPiccolo">
            </asp:Label>
        </td>
    </tr>
    <tr id="trCessazione1" runat="server" visible="false">
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr id="trCessazione2" runat="server" visible="false">
        <td>
            Dati cessazione
        </td>
        <td>
            Il&nbsp; <b>
                <asp:Label ID="LabelDatiCessazioneData" runat="server">
                </asp:Label>
            </b>&nbsp;per&nbsp; <b>
                <asp:Label ID="LabelDatiCessazioneCausa" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr id="trTrasformazione1" runat="server" visible="false">
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr id="trTrasformazione2" runat="server" visible="false">
        <td>
            Dati trasformazione
        </td>
        <td>
            Il&nbsp; <b>
                <asp:Label ID="LabelDatiTrasformazioneData" runat="server">
                </asp:Label>
            </b>&nbsp;per&nbsp; <b>
                <asp:Label ID="LabelDatiTrasformazioneCodice" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelAvvertimenti" Font-Bold="true" runat="server">Avvertimenti</asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:BulletedList ID="BulletedListAvvertimenti" runat="server">
            </asp:BulletedList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:CheckBox ID="CheckBoxDisclaimer" runat="server" />
            <asp:LinkButton ID="Button1" runat="server" Text="Autorizzazione al trattamento dei dati ai sensi del d.lgs. 196/2003"
                OnClientClick="openRadWindow(); return false;" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:CustomValidator ID="CustomValidatorDisclaimer" runat="server" ValidationGroup="conferma"
                ErrorMessage="Per proseguire è necessario accettare le condizioni" OnServerValidate="CustomValidatorDisclaimer_ServerValidate">&nbsp;</asp:CustomValidator>
        </td>
    </tr>
</table>
