using System;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class IscrizioneLavoratori_IscrizioneLavoratoreImpresa : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public Impresa CaricaImpresa(Int32 idImpresa)
    {
        Impresa impresa = null;

        Reset();

        if (idImpresa > 0)
        {
            impresa = biz.GetImpresa(idImpresa);

            RadTextBoxCodice.Text = impresa.IdImpresa.ToString();
            RadTextBoxRagioneSociale.Text = impresa.RagioneSociale;
            RadTextBoxPartitaIva.Text = impresa.PartitaIva;
            RadTextBoxCodiceFiscale.Text = impresa.CodiceFiscale;
            RadComboBoxTipologiaContrattuale.SelectedValue = impresa.CodiceContratto;
            RadTextBoxIndirizzo.Text = impresa.SedeLegaleIndirizzo;
            RadTextBoxComune.Text = impresa.SedeLegaleComune;
            RadTextBoxProvincia.Text = impresa.SedeLegaleProvincia;
            RadTextBoxCap.Text = impresa.SedeLegaleCap;
            RadTextBoxTelefono.Text = impresa.SedeLegaleTelefono;
            RadTextBoxFax.Text = impresa.SedeLegaleFax;
            RadTextBoxEMail.Text = impresa.SedeLegaleEmail;
        }

        return impresa;
    }

    private void CaricaTipiContratto()
    {
        if (RadComboBoxTipologiaContrattuale.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipologiaContrattuale,
                commonBiz.GetTipiContratto(),
                "Descrizione",
                "IdContratto");
        }
    }

    private void Reset()
    {
        Presenter.SvuotaCampo(RadTextBoxCodice);
        Presenter.SvuotaCampo(RadTextBoxRagioneSociale);
        Presenter.SvuotaCampo(RadTextBoxPartitaIva);
        Presenter.SvuotaCampo(RadTextBoxCodiceFiscale);
        Presenter.SvuotaCampo(RadTextBoxIndirizzo);
        Presenter.SvuotaCampo(RadTextBoxComune);
        Presenter.SvuotaCampo(RadTextBoxProvincia);
        Presenter.SvuotaCampo(RadTextBoxCap);
        Presenter.SvuotaCampo(RadTextBoxTelefono);
        Presenter.SvuotaCampo(RadTextBoxFax);
        Presenter.SvuotaCampo(RadTextBoxEMail);
        RadComboBoxTipologiaContrattuale.Text = "";
        RadComboBoxTipologiaContrattuale.ClearSelection();
        CaricaTipiContratto();
    }

    public Int32 GetIdImpresa()
    {
        return Int32.Parse(RadTextBoxCodice.Text);
    }

    public Impresa GetImpresa()
    {
        Impresa impresa = new Impresa();

        impresa.IdImpresa = Int32.Parse(Presenter.NormalizzaCampoTesto(RadTextBoxCodice.Text));
        impresa.RagioneSociale = Presenter.NormalizzaCampoTesto(RadTextBoxRagioneSociale.Text);
        impresa.PartitaIva = Presenter.NormalizzaCampoTesto(RadTextBoxPartitaIva.Text);
        impresa.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);
        impresa.SedeLegaleIndirizzo = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text);
        impresa.SedeLegaleComune = Presenter.NormalizzaCampoTesto(RadTextBoxComune.Text);
        impresa.SedeLegaleProvincia = Presenter.NormalizzaCampoTesto(RadTextBoxProvincia.Text);
        impresa.SedeLegaleCap = Presenter.NormalizzaCampoTesto(RadTextBoxCap.Text);
        impresa.SedeLegaleTelefono = Presenter.NormalizzaCampoTesto(RadTextBoxTelefono.Text);
        impresa.SedeLegaleFax = Presenter.NormalizzaCampoTesto(RadTextBoxFax.Text);
        impresa.SedeLegaleEmail = Presenter.NormalizzaCampoTesto(RadTextBoxEMail.Text);


        return impresa;
    }
}