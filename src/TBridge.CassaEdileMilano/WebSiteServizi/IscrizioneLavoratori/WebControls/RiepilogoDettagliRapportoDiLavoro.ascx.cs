using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

public partial class WebControls_IscrizioneLavoratori_RiepilogoDettagliRapportoDiLavoro : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public Boolean CaricaDichiarazione(Dichiarazione dichiarazione)
    {
        Boolean abilitaTastoAccetta = false;
        RapportoDiLavoro rapporto = dichiarazione.RapportoDiLavoro;

        if (rapporto != null)
        {
            if (rapporto.DataAssunzione.HasValue)
            {
                LabelDataAssunzione.Text = rapporto.DataAssunzione.Value.ToString("dd/MM/yyyy");
            }
            if (rapporto.DataInizioValiditaRapporto.HasValue)
            {
                LabelDataInizioRapporto.Text = rapporto.DataInizioValiditaRapporto.Value.ToString("dd/MM/yyyy");
            }
            if (rapporto.DataFineValiditaRapporto.HasValue)
            {
                LabelDataFineRapporto.Text = rapporto.DataFineValiditaRapporto.Value.ToString("dd/MM/yyyy");
            }
            if (rapporto.Contratto != null)
            {
                LabelContratto.Text = rapporto.Contratto.Descrizione;
            }
            //if (rapporto.TipoInizioRapporto != null)
            if (rapporto.Contratto != null)
            {
                LabelTipoContrattualeAssunzione.Text = rapporto.TipoInizioRapporto.Descrizione;
                //LabelTipoContrattualeAssunzione.Text = rapporto.Contratto.Descrizione;
            }
            if (rapporto.Categoria != null)
            {
                LabelCategoria.Text = rapporto.Categoria.Descrizione;
            }
            if (rapporto.Qualifica != null)
            {
                LabelQualifica.Text = rapporto.Qualifica.Descrizione;
            }
            if (rapporto.Mansione != null)
            {
                LabelMansione.Text = rapporto.Mansione.Descrizione;
            }
            if (rapporto.LivelloInquadramento != null)
            {
                LabelLivelloInquadramento.Text = rapporto.LivelloInquadramento;
            }
            if (rapporto.PercentualePartTime != null)
            {
                LabelPercentualePartTime.Text = rapporto.PercentualePartTime.ToString();
            }
            if (rapporto.PartTime != null)
            {
                LabelPartTime.Text = "NO";
                if (rapporto.PartTime)
                    LabelPartTime.Text = "SI";
            }
            if (rapporto.DataCessazione != null)
            {
                PannelCessazione.Visible = true;
                LabelDataCessazione.Text = rapporto.DataCessazione.Value.ToShortDateString();
            }
            if (rapporto.TipoFineRapporto != null)
            {
                LabelCausa.Text = rapporto.TipoFineRapporto.Descrizione;
            }

            EffettuaControlli(dichiarazione);

            //if (rapporto.DataTrasformazione != null)
            //{
            //    PanelTrasformazione.Visible = true;
            //    LabelDataTrasformazione.Text = rapporto.DataTrasformazione.ToString();
            //}

            if (ImageControlloRapportoAssociato.ImageUrl == IscrizioneLavoratoriManager.URLSEMAFOROVERDE)
            {
                abilitaTastoAccetta = true;
            }
        }

        return abilitaTastoAccetta;
    }

    private void EffettuaControlli(Dichiarazione dichiarazione)
    {
        switch (dichiarazione.Attivita)
        {
            case TipoAttivita.Assunzione:
                tableControlli.Visible = false;
                //ImageControlloRapportoAssociato.ImageUrl = biz.ControlloRapportoCessazione(dichiarazione);
                break;
            default:
                tableControlli.Visible = true;
                ImageControlloRapportoAssociato.ImageUrl = biz.ControlloRapportoCessazione(dichiarazione);
                break;
        }
    }
}