﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RiepilogoDettagliRapportoDiLavoro.ascx.cs"
    Inherits="WebControls_IscrizioneLavoratori_RiepilogoDettagliRapportoDiLavoro" %>
<table class="borderedTable">
    <tr>
        <td>
            <b>Dati rapporto di lavoro</b>
        </td>
        <td>
            <table id="tableControlli" runat="server" class="standardTable">
                <tr>
                    <td>
                        <asp:Image ID="ImageControlloRapportoAssociato" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Rapporto di lavoro associato alla cessazione </small>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data assunzione
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDataAssunzione" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data inizio rapporto di lavoro
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDataInizioRapporto" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data fine rapporto di lavoro
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDataFineRapporto" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Contratto applicato
        </td>
        <td>
            <b>
                <asp:Label ID="LabelContratto" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Tipologia contrattuale di assunzione
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTipoContrattualeAssunzione" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Tipologia orario part-time
        </td>
        <td>
            <b>
                <asp:Label ID="LabelPartTime" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Percentuale part-time
        </td>
        <td>
            <b>
                <asp:Label ID="LabelPercentualePartTime" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Livello inquadramento
        </td>
        <td>
            <b>
                <asp:Label ID="LabelLivelloInquadramento" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Categoria
        </td>
        <td>
            <b>
                <asp:Label ID="LabelCategoria" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Qualifica
        </td>
        <td>
            <b>
                <asp:Label ID="LabelQualifica" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Mansione
        </td>
        <td>
            <b>
                <asp:Label ID="LabelMansione" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
<asp:Panel ID="PannelCessazione" runat="server" Visible="false">
    <br />
    <table class="borderedTable">
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data cessazione
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelDataCessazione" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Causa
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelCausa" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="PanelTrasformazione" runat="server" Visible="false">
    <br />
    <table class="borderedTable">
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data trasformazione
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelDataTrasformazione" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Codice
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelCodiceTrasformazione" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
