﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratore.ascx.cs"
    Inherits="WebControls_IscrizioneLavoratori_IscrizioneLavoratore" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="IscrizioneLavoratoreAttivita.ascx" TagName="IscrizioneLavoratoreAttivita"
    TagPrefix="uc1" %>
<%@ Register Src="IscrizioneLavoratoreImpresa.ascx" TagName="IscrizioneLavoratoreImpresa"
    TagPrefix="uc2" %>
<%@ Register Src="IscrizioneLavoratoreIndirizzo.ascx" TagName="IscrizioneLavoratoreIndirizzo"
    TagPrefix="uc3" %>
<%@ Register Src="../../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa"
    TagPrefix="uc7" %>
<%@ Register Src="IscrizioneLavoratoreDatiLocalizzazione.ascx" TagName="IscrizioneLavoratoreDatiLocalizzazione"
    TagPrefix="uc9" %>
<%@ Register Src="IscrizioneLavoratoreDatiAnagraficiDocumenti.ascx" TagName="IscrizioneLavoratoreDatiAnagraficiDocumenti"
    TagPrefix="uc4" %>
<%@ Register Src="IscrizioneLavoratoreRapportoDiLavoro.ascx" TagName="IscrizioneLavoratoreRapportoDiLavoro"
    TagPrefix="uc5" %>
<%@ Register Src="IscrizioneLavoratoreInformazioniAggiuntive.ascx" TagName="IscrizioneLavoratoreInformazioniAggiuntive"
    TagPrefix="uc10" %>
<%@ Register Src="IscrizioneLavoratoreRiepilogo.ascx" TagName="IscrizioneLavoratoreRiepilogo"
    TagPrefix="uc6" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <div>
        <telerik:RadTabStrip ID="RadTabStripIscrizioneLavoratore" runat="server" MultiPageID="RadMultiPageIscrizioneLavoratore"
            SelectedIndex="0" OnTabClick="RadTabStripIscrizioneLavoratore_TabClick"  
            Width="100%">
            <Tabs>
                <telerik:RadTab Text="Attività" Width="85px">
                </telerik:RadTab>
                <telerik:RadTab Text="Impresa" Width="80px">
                </telerik:RadTab>
                <telerik:RadTab Text="Sede operativa" Width="105px">
                </telerik:RadTab>
                <telerik:RadTab Text="Dati" Width="75px" >
                </telerik:RadTab>
                <telerik:RadTab Text="Indirizzo" Width="90px">
                </telerik:RadTab>
                <telerik:RadTab Text="Rapp. di lavoro" Width="110px">
                </telerik:RadTab>
                <telerik:RadTab Text="Altre info" Width="90px">
                </telerik:RadTab>
                <telerik:RadTab Text="Conferma" Width="92px">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPageIscrizioneLavoratore" runat="server" 
            SelectedIndex="0"    CssClass="radWizard"
            Width="96%" RenderSelectedPageOnly="True"  >
            <telerik:RadPageView ID="RadPageViewAttivita" runat="server">
                <div>
                    <br />
                    <b>Attività </b>
                </div>
                <br />
                <div>
                    <asp:Panel ID="PanelSelezioneImpresa" runat="server" Visible="false">
                        <uc7:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
                        <asp:CustomValidator ID="CustomValidatorSelezioneImpresa" runat="server" ValidationGroup="attivita"
                            ErrorMessage="Selezionare un Impresa" OnServerValidate="CustomValidatorSelezioneImpresa_ServerValidate">
                        &nbsp;
                        </asp:CustomValidator>
                    </asp:Panel>
                    <uc1:IscrizioneLavoratoreAttivita ID="IscrizioneLavoratoreAttivita1" runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryAttivita" runat="server" ValidationGroup="attivita"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewAnagraficaImpresa" runat="server">
                <div>
                    <b>Anagrafica Impresa </b>
                </div>
                <br />
                <div>
                    <uc2:IscrizioneLavoratoreImpresa ID="IscrizioneLavoratoreImpresa1" runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryImpresa" runat="server" ValidationGroup="impresa"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewCantiere" runat="server">
                <div>
                    <b>Indirizzo sede operativa </b>
                </div>
                <br />
                <div>
                    <uc3:IscrizioneLavoratoreIndirizzo ID="IscrizioneLavoratoreCantiere1" runat="server" />
                    <asp:CustomValidator ID="CustomValidatorCantiere" runat="server" ValidationGroup="cantiere"
                        OnServerValidate="CustomValidatorCantiere_ServerValidate" ErrorMessage="L'indirizzo utilizzato per il cantiere non è presente nelle notifiche preliminari">
                    &nbsp;
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="CustomValidatorCantiereIndirizziImpresaConsulente" runat="server"
                        ValidationGroup="cantiere" OnServerValidate="CustomValidatorCantiereIndirizziImpresaConsulente_ServerValidate"
                        ErrorMessage="L'indirizzo non può essere: la sede legale dell'impresa, la sede amministrativa dell'impresa o la sede del consulente (se presente)">
                    &nbsp;
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="CustomValidatorIndirizzoCantiere" runat="server" ValidationGroup="cantiere"
                        OnServerValidate="CustomValidatorIndirizzoCantiere_ServerValidate" ErrorMessage="Inserire un indirizzo e avviare la geocodifica">
                    &nbsp;
                    </asp:CustomValidator>
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryCantiere" runat="server" ValidationGroup="cantiere"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewLavoratoreDatiAnagrafici" runat="server">
                <div>
                    <b>Lavoratore (dati anagrafici e documenti) </b>
                </div>
                <br />
                <div>
                    <uc4:IscrizioneLavoratoreDatiAnagraficiDocumenti ID="IscrizioneLavoratoreDatiAnagraficiDocumenti1"
                        runat="server" />
                    <asp:CustomValidator ID="CustomValidatorLavoratore" runat="server" ValidationGroup="datiAnagrafici"
                        OnServerValidate="CustomValidatorLavoratore_ServerValidate">
                    &nbsp;
                    </asp:CustomValidator>
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryDatiAnagrafici" runat="server" ValidationGroup="datiAnagrafici"
                        CssClass="messaggiErrore" />
                    <asp:ValidationSummary ID="ValidationSummarydatiAnagraficiExtraUE" runat="server"
                        ValidationGroup="datiAnagraficiExtraUE" CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewLavoratoreDatiLocalizzazione" runat="server">
                <div>
                    <b>Lavoratore (dati localizzazione) </b>
                </div>
                <br />
                <div>
                    <uc9:IscrizioneLavoratoreDatiLocalizzazione ID="IscrizioneLavoratoreDatiLocalizzazione1"
                        runat="server" />
                    <asp:CustomValidator ID="CustomValidatorLavoratoreIndirizziImpresaConsulente" runat="server"
                        ValidationGroup="datiLocalizzazione" OnServerValidate="CustomValidatorLavoratoreIndirizziImpresaConsulente_ServerValidate"
                        ErrorMessage="L'indirizzo non può essere: la sede legale dell'impresa, la sede amministrativa dell'impresa o la sede del consulente (se presente)">
                    &nbsp;
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="CustomValidatorIndirizzoLavoratore" runat="server" ValidationGroup="datiLocalizzazione"
                        OnServerValidate="CustomValidatorIndirizzoLavoratore_ServerValidate" ErrorMessage="Inserire un indirizzo e avviare la geocodifica">
                    &nbsp;
                    </asp:CustomValidator>
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryDatiLocalizzazione" runat="server" ValidationGroup="datiLocalizzazione"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewRapportoDiLavoro" runat="server">
                <div>
                    <b>Rapporto di Lavoro </b>
                </div>
                <br />
                <div>
                    <uc5:IscrizioneLavoratoreRapportoDiLavoro ID="IscrizioneLavoratoreRapportoDiLavoro1"
                        runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryRapportoDiLavoro" runat="server" ValidationGroup="rapportoDiLavoro"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewLavoratoreInfoAggiuntive" runat="server">
                <div>
                    <b>Lavoratore (info aggiuntive) </b>
                </div>
                <br />
                <div>
                    <uc10:IscrizioneLavoratoreInformazioniAggiuntive ID="IscrizioneLavoratoreInformazioniAggiuntive"
                        runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryConferma" runat="server" ValidationGroup="infoAggiuntive"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewConferma" runat="server">
                <div>
                    <b>Conferma </b>
                </div>
                <br />
                <div>
                    <uc6:IscrizioneLavoratoreRiepilogo ID="IscrizioneLavoratoreRiepilogo1" runat="server" />
                </div>
                <div>
                    <asp:ValidationSummary ID="ValidationSummaryStop" runat="server" ValidationGroup="conferma"
                        CssClass="messaggiErrore" />
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <div style="text-align: right;">
        <br />
        <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="150px" OnClick="ButtonIndietro_Click"
            Visible="False" />
        &nbsp;
        <asp:Button ID="ButtonAvanti" runat="server" Text="Avanti" Width="150px" OnClick="ButtonAvanti_Click"
            ValidationGroup="stop" />
    </div>
</telerik:RadAjaxPanel>
