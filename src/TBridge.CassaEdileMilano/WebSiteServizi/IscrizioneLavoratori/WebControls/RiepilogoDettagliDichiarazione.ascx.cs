using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class WebControls_IscrizioneLavoratori_RiepilogoDettagliDichiarazione : UserControl
{
    private GestioneUtentiBiz _gestioneUtentiBiz = new GestioneUtentiBiz();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void CaricaDichiarazione(Dichiarazione dichiarazione)
    {
        Utente ut;
        LabelID.Text = dichiarazione.IdDichiarazione.ToString();
        LabelDataInserimento.Text = dichiarazione.DataInvio.ToString("dd/MM/yyyy");
        LabelTipoAttivita.Text = dichiarazione.Attivita.ToString();
        LabelStato.Text = dichiarazione.Stato.ToString();

        if (dichiarazione.DataUltimoCambioStato.HasValue)
        {
            LabelDataCambioStato.Text = dichiarazione.DataUltimoCambioStato.Value.ToString("dd/MM/yyyy");
        }
        else
        {
            LabelDataCambioStato.Text = "N.D.";
        }

        if (dichiarazione.UltimoCambioStatoIdUtente.HasValue)
        {
            ut = _gestioneUtentiBiz.GetUtenteById(dichiarazione.UltimoCambioStatoIdUtente.Value);
            LabelUtenteCambioStato.Text = ut.UserName;
        }
        else
        {
            LabelUtenteCambioStato.Text = "N.D.";
        }
        
        ut = _gestioneUtentiBiz.GetUtenteById(dichiarazione.IdUtente);
        LabelProvenienza.Text = ut.UserName;
       // LabelProvenienza.Text = GestioneUtentiBiz.GetIdentitaUtente(dichiarazione.IdUtente).UserName;
        if (dichiarazione.IsRettifica)
        {
            LabelRettifica.Text = string.Format("{0}{1}", "Rettifica di: ",
                                                dichiarazione.IdDichiarazionePrecedente.HasValue ?
                                                dichiarazione.IdDichiarazionePrecedente.Value.ToString() : "N.D.");
        }

        if (dichiarazione.IsRettificata)
        {
            if (!string.IsNullOrEmpty(LabelRettifica.Text))
            {
                LabelRettifica.Text = string.Format("{0}{1}{2}{3}", LabelRettifica.Text, "<br/>", "Rettificata da: ",
                                                 dichiarazione.IdDichiarazioneSuccessiva.HasValue ?
                                                 dichiarazione.IdDichiarazioneSuccessiva.Value.ToString() : "N.D.");
            }
            else
            {
                LabelRettifica.Text = string.Format("{0}{1}", "Rettificata da: ",
                                                dichiarazione.IdDichiarazioneSuccessiva.HasValue ?
                                                dichiarazione.IdDichiarazioneSuccessiva.Value.ToString() : "N.D.");
            }

        }

        //if (dichiarazione.IsRettificata)
        //{
        //    if (!string.IsNullOrEmpty(LabelRettifica.Text))
        //        LabelRettifica.Text = string.Format("{0}{1}", LabelRettifica.Text, " ");
        //    LabelRettifica.Text = string.Format("{0}{1}{2}", LabelRettifica.Text, "Rettificata da: ",
        //                                    dichiarazione.IdDichiarazioneSuccessiva.Value.ToString());
        //}
    }
}