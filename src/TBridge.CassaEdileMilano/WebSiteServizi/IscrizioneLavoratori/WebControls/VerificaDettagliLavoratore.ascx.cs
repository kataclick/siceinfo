using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using Telerik.Web.UI;

public partial class WebControls_IscrizioneLavoratori_VerificaDettagliLavoratore : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
    private readonly GestioneUtentiBiz gestioneUtentiBiz = new GestioneUtentiBiz();
    private bool? PosizioneValida;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Context.Items["IdDichiarazione"] != null)
            {
                Int32 idDichiarazione = (Int32) Context.Items["IdDichiarazione"];
                ViewState["IdDichiarazione"] = idDichiarazione;

                Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);

                RadPageViewLavoratoreContrattiCessazione.Visible = (dichiarazione.Attivita == TipoAttivita.Cessazione);
                tabRapporti.Visible = (dichiarazione.Attivita == TipoAttivita.Cessazione);

                CaricaDichiarazione(dichiarazione);
            }
        }
    }

    public void CaricaDichiarazione(Dichiarazione dichiarazione)
    {
        Reset();

        LabelCognomeNome.Text = dichiarazione.Lavoratore.CognomeNome;
        LabelSesso.Text = dichiarazione.Lavoratore.Sesso.ToString();
        LabelDataNascita.Text = dichiarazione.Lavoratore.DataNascita.ToString("dd/MM/yyyy");
        LabelCodiceFiscale.Text = dichiarazione.Lavoratore.CodiceFiscale;

        LabelProvinciaComuneNascita.Text = String.Format("{0} - {1}", dichiarazione.Lavoratore.ProvinciaNascita,
                                                         dichiarazione.Lavoratore.ComuneNascita);

        //LabelIBANDichiarato.Text = dichiarazione.Lavoratore.IBAN;

        if (dichiarazione.Lavoratore.TipoPrepagata != null)
        {
            //LabelCartaPrepagataDichiarata.Text = dichiarazione.Lavoratore.TipoPrepagata.Descrizione;
        }

        LabelEMailDichiarato.Text = dichiarazione.Lavoratore.Email;
        LabelCellulareDichiarato.Text = dichiarazione.Lavoratore.NumeroTelefonoCellulare;
        LabelCellulareSMSSiceInfo.Text = dichiarazione.CellulareSMSSiceInfo;

        //RadioButtonListIBAN.ClearSelection();
        RadioButtonListIndirizzo.ClearSelection();

        //RadioButtonListIBAN.Enabled = true;
        RadioButtonListIndirizzo.Enabled = true;

        //if (dichiarazione.MantieniIbanAnagrafica.HasValue)
        //{
        //    if (!dichiarazione.MantieniIbanAnagrafica.Value)
        //    {
        //        RadioButtonListIBAN.SelectedIndex = 0;
        //    }
        //    else
        //    {
        //        RadioButtonListIBAN.SelectedIndex = 1;
        //    }
        //}

        if (dichiarazione.MantieniIndirizzoAnagrafica.HasValue)
        {
            if (!dichiarazione.MantieniIndirizzoAnagrafica.Value)
            {
                RadioButtonListIndirizzo.SelectedIndex = 0;
            }
            else
            {
                RadioButtonListIndirizzo.SelectedIndex = 1;
            }
        }

        if (dichiarazione.Lavoratore != null)
        {
            CheckBoxAderisceSMS.Checked = dichiarazione.Lavoratore.AderisceServizioSMS;
        }

        EffettuaControlli(dichiarazione);
    }

    private void Reset()
    {
        LabelCognomeNomeAnagrafica.Text = null;
        LabelSessoAnagrafica.Text = null;
        LabelDataNascitaAnagrafica.Text = null;
        LabelCodiceFiscaleAnagrafica.Text = null;
        LabelProvinciaComuneNascitaAnagrafica.Text = null;
       // LabelIBANAnagrafica.Text = null;
       // LabelCartaPrepagataAnagrafica.Text = null;
        LabelIndirizzoAnagrafica.Text = null;
        LabelEMailAnagrafica.Text = null;
        LabelCellulareAnagrafica.Text = null;
        LabelCellulareSMSSiceInfo.Text = null;
        CheckBoxAderisceSMS.Checked = false;
        LabelIndirizzoAnagraficaValidità.Text = null;
        LabelIndirizzoAnagraficaValidità.Visible = false;
    }

    private void CaricaGrid(LavoratoreCollection lavoratoriStessoCodiceFiscale,
                            LavoratoreCollection lavoratoriStessiDatiAnagrafici,
                            LavoratoreCollection lavoratoriStessiDatiAnagraficiECodiceFiscale,
                            RapportoDiLavoroCollection rapportiAperti)
    {
        RadGridAnagraficaUguale.DataSource = lavoratoriStessiDatiAnagrafici;
        RadGridAnagraficaUguale.DataBind();

        RadGridCodiceFiscaleUguale.DataSource = lavoratoriStessoCodiceFiscale;
        RadGridCodiceFiscaleUguale.DataBind();

        RadGridAnagraficaUgualeTutto.DataSource = lavoratoriStessiDatiAnagraficiECodiceFiscale;
        RadGridAnagraficaUgualeTutto.DataBind();

        RadGridSdoppione.DataSource = rapportiAperti;
        RadGridSdoppione.DataBind();

        if (rapportiAperti.Count > 0)
        {
            RadioButtonListControlloSdoppione.Visible = true;
        }
        else
        {
            RadioButtonListControlloSdoppione.Visible = false;
        }

        Int32 idDichiarazione = (Int32) ViewState["IdDichiarazione"];
        Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);

        if (dichiarazione.IdLavoratoreSelezionato.HasValue)
        {
            SelezioneGrigliaMultipla(RadGridAnagraficaUguale, dichiarazione.IdLavoratoreSelezionato.Value);
            SelezioneGrigliaMultipla(RadGridAnagraficaUgualeTutto, dichiarazione.IdLavoratoreSelezionato.Value);
            SelezioneGrigliaMultipla(RadGridCodiceFiscaleUguale, dichiarazione.IdLavoratoreSelezionato.Value);
            SelezioneGrigliaMultipla(RadGridSdoppione, dichiarazione.IdLavoratoreSelezionato.Value);

            if (dichiarazione.Attivita == TipoAttivita.Cessazione)
                CaricaGridRapporti(dichiarazione);

            if (dichiarazione.RapportoAssociatoCessazione)
            {
                if (!dichiarazione.NuovoLavoratore)
                {
                    foreach (GridDataItem it in RadGridRapportDiLavoroCessazione.Items)
                    {
                        if (
                            (RadGridRapportDiLavoroCessazione.MasterTableView.DataKeyValues[it.ItemIndex]["IdLavoratore"
                                 ].ToString() == dichiarazione.IdLavoratoreRapportoSelezionatoCessazione.ToString())
                            &&
                            (RadGridRapportDiLavoroCessazione.MasterTableView.DataKeyValues[it.ItemIndex]["IdImpresa"].
                                 ToString() == dichiarazione.IdImpresaRapportoSelezionatoCessazione.ToString())
                            &&
                            (RadGridRapportDiLavoroCessazione.MasterTableView.DataKeyValues[it.ItemIndex][
                                 "DataFineValiditaRapporto"].ToString() ==
                             dichiarazione.DataFineRapportoSelezionatoCessazione.ToString())
                            )
                        {
                            it.Selected = true;
                            ImageControlloRapportoCessazione.ImageUrl = biz.ControlloRapportoCessazione(dichiarazione);
                            break;
                        }
                        it.Selected = false;
                    }
                }
                else
                    ImageControlloRapportoCessazione.ImageUrl = biz.ControlloRapportoCessazione(dichiarazione);
            }
        }
        else
        {
            ResetGrid();
            ImageControlloRapportoCessazione.ImageUrl = biz.ControlloRapportoCessazione(dichiarazione);
            RadGridRapportDiLavoroCessazione.DataSource = new RapportoDiLavoroCollection();
            RadGridRapportDiLavoroCessazione.DataBind();
        }
    }

    private void EffettuaControlli(Dichiarazione dichiarazione)
    {
        switch (dichiarazione.Attivita)
        {
            case TipoAttivita.Assunzione:
            case TipoAttivita.Cessazione:
                tableControlli.Visible = true;
                tableControlliDati.Visible = true;

                if (!dichiarazione.IdLavoratoreSelezionato.HasValue) LabelSelezioneLavoratore.Text = "Nessuna selezione";

                ImageControlloSelezioneLavoratore.ImageUrl = biz.LavoratoreSelezionatoONuovo(dichiarazione);
                if (ImageControlloSelezioneLavoratore.ImageUrl == IscrizioneLavoratoriManager.URLSEMAFOROVERDE)
                {
                    if (dichiarazione.NuovoLavoratore)
                    {
                        if(PosizioneValida != null)
                        LabelSelezioneLavoratore.Text = (bool)PosizioneValida ? "Creazione nuova anagrafica (posizione valida)" : "Creazione nuova anagrafica (posizione non valida)";
                    }
                    else
                    {
                        if (dichiarazione.IdLavoratoreSelezionato.HasValue)
                            LabelSelezioneLavoratore.Text = String.Format("Codice selezionato: {0}",
                                                                          dichiarazione.IdLavoratoreSelezionato);

                        else LabelSelezioneLavoratore.Text = "Nessuna selezione";
 
                    }
                }

                LavoratoreCollection lavoratoriStessoCodiceFiscale = biz.GetLavoratoriConStessoCodiceFiscale(
                    dichiarazione.Lavoratore.CodiceFiscale);
                LavoratoreCollection lavoratoriStessiDatiAnagrafici = biz.GetLavoratoriConStessiDatiAnagrafici(
                    dichiarazione.Lavoratore.Cognome,
                    dichiarazione.Lavoratore.Nome,
                    dichiarazione.Lavoratore.Sesso,
                    dichiarazione.Lavoratore.DataNascita,
                    dichiarazione.Lavoratore.ComuneNascita);
                LavoratoreCollection lavoratoriStessiDatiAnagraficiECodiceFiscale = biz.
                    GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
                    dichiarazione.Lavoratore.Cognome,
                    dichiarazione.Lavoratore.Nome,
                    dichiarazione.Lavoratore.Sesso,
                    dichiarazione.Lavoratore.DataNascita,
                    dichiarazione.Lavoratore.ComuneNascita,
                    dichiarazione.Lavoratore.CodiceFiscale);
                Lavoratore lavoratore = null;
                if (dichiarazione.IdLavoratoreSelezionato.HasValue)
                {
                    lavoratore = biz.GetLavoratore(dichiarazione.IdLavoratoreSelezionato.Value);
                }

                ImageControlloAnagraficaUgualeSenzaOmocodia.ImageUrl = biz.AnagraficaUgualeSenzaOmocodia(dichiarazione,
                                                                                                         lavoratoriStessiDatiAnagrafici);

                ImageControlloAnagraficaUgualeConOmocodia.ImageUrl = biz.AnagraficaUgualeConOmocodia(dichiarazione,
                                                                                                     lavoratoriStessiDatiAnagrafici);

                ImageControlloAnagraficaDoppione.ImageUrl = biz.AnagraficaDoppione(dichiarazione,
                                                                                   lavoratoriStessiDatiAnagraficiECodiceFiscale);

                ImageControlloAnagraficaSdoppione.ImageUrl = biz.AnagraficaSdoppione(dichiarazione,
                                                                                     lavoratoriStessoCodiceFiscale,
                                                                                     lavoratoriStessiDatiAnagrafici);

                ImageControlloRapportoCessazione.ImageUrl = biz.ControlloRapportoCessazione(dichiarazione);

                List<Int32> lavoratoriTrovati = new List<Int32>();
                foreach (Lavoratore lav in lavoratoriStessoCodiceFiscale)
                {
                    Boolean inserisci = true;

                    for (Int32 i = 0; i < lavoratoriTrovati.Count; i++)
                    {
                        if (lav.IdLavoratore.Value == lavoratoriTrovati[i])
                        {
                            inserisci = false;
                            break;
                        }
                    }

                    if (inserisci)
                    {
                        lavoratoriTrovati.Add(lav.IdLavoratore.Value);
                    }
                }
                foreach (Lavoratore lav in lavoratoriStessiDatiAnagrafici)
                {
                    Boolean inserisci = true;

                    for (Int32 i = 0; i < lavoratoriTrovati.Count; i++)
                    {
                        if (lav.IdLavoratore.Value == lavoratoriTrovati[i])
                        {
                            inserisci = false;
                            break;
                        }
                    }

                    if (inserisci)
                    {
                        lavoratoriTrovati.Add(lav.IdLavoratore.Value);
                    }
                }

                RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();
                foreach (Int32 idLav in lavoratoriTrovati)
                {
                    rapportiAperti.AddRange(
                        biz.GetRapportiDiLavoroAperti(idLav,
                                                      dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto.Value));
                }
                rapportiAperti.AddRange(biz.GetDichiarazioniAperte(dichiarazione.IdDichiarazione.Value,
                                                                   dichiarazione.Lavoratore.CodiceFiscale,
                                                                   dichiarazione.RapportoDiLavoro.
                                                                       DataInizioValiditaRapporto.Value));

                CaricaGrid(lavoratoriStessoCodiceFiscale, lavoratoriStessiDatiAnagrafici,
                           lavoratoriStessiDatiAnagraficiECodiceFiscale, rapportiAperti);

                ImageControlloAnagraficaDubbia.ImageUrl = biz.AnagraficaDubbia(dichiarazione, lavoratore);
               // ImageEmissioneTesseraSanitaria.ImageUrl = biz.EmissioneTesseraSanitaria(dichiarazione);
                if (lavoratore != null)
                {
                    LabelCognomeNomeAnagrafica.Text = lavoratore.CognomeNome;
                    LabelSessoAnagrafica.Text = lavoratore.Sesso.ToString();
                    LabelDataNascitaAnagrafica.Text = lavoratore.DataNascita.ToString("dd/MM/yyyy");
                    LabelCodiceFiscaleAnagrafica.Text = lavoratore.CodiceFiscale;
                    LabelProvinciaComuneNascitaAnagrafica.Text = String.Format("{0} - {1}", lavoratore.ProvinciaNascita,
                                                                               lavoratore.ComuneNascita);
                }

                ImageControlloAnagraficaCellulare.ImageUrl = biz.CellulareDiverso(dichiarazione, lavoratore);
                LabelCellulareSMSSiceInfo.Text = dichiarazione.CellulareSMSSiceInfo;
                LabelCellulareDichiarato.Text = dichiarazione.Lavoratore.NumeroTelefonoCellulare;
                if (lavoratore != null)
                {
                    LabelCellulareAnagrafica.Text = lavoratore.NumeroTelefonoCellulare;
                }

                ImageControlloAnagraficaEMail.ImageUrl = biz.EmailDiversa(dichiarazione, lavoratore);
                LabelEMailDichiarato.Text = dichiarazione.Lavoratore.Email;
                if (lavoratore != null)
                {
                    LabelEMailAnagrafica.Text = lavoratore.Email;
                }

                ImageControlloAnagraficaIndirizzo.ImageUrl = biz.IndirizzoDiverso(dichiarazione, lavoratore);
                LabelIndirizzoDichiarato.Text = dichiarazione.Lavoratore.IndirizzoCompleto;
                if (lavoratore != null)
                {
                    LabelIndirizzoAnagrafica.Text =
                        lavoratore.IndirizzoCompleto;
                    if (lavoratore.Indirizzo.DataInizioValidità.HasValue)
                    {
                        LabelIndirizzoAnagraficaValidità.Visible = true;
                        LabelIndirizzoAnagraficaValidità.Text = String.Concat("Valido dal: ", lavoratore.Indirizzo.DataInizioValidità.Value.ToString("dd/MM/yyyy"));
                    }

                    if (string.IsNullOrEmpty(lavoratore.IndirizzoCompleto))
                    {
                        RadioButtonListIndirizzo.SelectedIndex = 0;
                        RadioButtonListIndirizzo.Enabled = false;
                        biz.CambiaIndirizzoDichiarato((Int32) ViewState["IdDichiarazione"], false);
                    }
                    else
                    {
                        //RadioButtonListIndirizzo.ClearSelection();
                        RadioButtonListIndirizzo.Enabled = true;
                    }
                }
                //tableIndirizzo.Visible = true;

                //ImageControlloAnagraficaPrepagata.ImageUrl = biz.InformazioniPrepagata(dichiarazione);

                //ImageControlloAnagraficaIBAN.ImageUrl = biz.IbanDiverso(dichiarazione, lavoratore);
                //LabelIBANDichiarato.Text = dichiarazione.Lavoratore.IBAN;
                //if (lavoratore != null)
                //{
                //    LabelIBANAnagrafica.Text = lavoratore.IBAN;
                //    if (string.IsNullOrEmpty(lavoratore.IBAN))
                //    {
                //        RadioButtonListIBAN.SelectedIndex = 0;
                //        RadioButtonListIBAN.Enabled = false;
                //        biz.CambiaIBANDichiarato((Int32) ViewState["IdDichiarazione"], false);
                //    }
                //    else
                //    {
                //        RadioButtonListIBAN.ClearSelection();
                //        RadioButtonListIBAN.Enabled = true;
                //    }
                //}

                ImageControlloCantiereNotifiche.ImageUrl = biz.CantiereInNotifica(dichiarazione);
                LabelCantiereNotifiche.Visible =
                    !(ImageControlloCantiereNotifiche.ImageUrl != IscrizioneLavoratoriManager.URLSEMAFOROVERDE);

                // Gestione dei radio button per la scelta dell'iban, indirizzo
                //if (String.IsNullOrEmpty(dichiarazione.Lavoratore.IBAN))
                //{
                //    RadioButtonListIBAN.SelectedIndex = 1;
                //    RadioButtonListIBAN.Enabled = false;
                //}
                //else
                //{
                //    if (lavoratore == null || String.IsNullOrEmpty(lavoratore.IBAN))
                //    {
                //        RadioButtonListIBAN.SelectedIndex = 0;
                //        RadioButtonListIBAN.Enabled = false;
                //    }
                //}

                if (String.IsNullOrEmpty(dichiarazione.Lavoratore.IndirizzoCompleto))
                {
                    RadioButtonListIndirizzo.SelectedIndex = 1;
                    RadioButtonListIndirizzo.Enabled = false;
                }
                else
                {
                    if (lavoratore == null || String.IsNullOrEmpty(lavoratore.IndirizzoCompleto))
                    {
                        RadioButtonListIndirizzo.SelectedIndex = 0;
                        RadioButtonListIndirizzo.Enabled = false;
                    }
                }

                if (dichiarazione.Lavoratore != null)
                {
                    CheckBoxAderisceSMS.Checked = dichiarazione.Lavoratore.AderisceServizioSMS;
                }

                break;
            default:
                tableControlli.Visible = false;
                tableControlliDati.Visible = false;
                break;
        }
    }

    private void CaricaGridRapporti(Dichiarazione dichiarazione)
    {
        if (dichiarazione.IdLavoratoreSelezionato != null)
        {
            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();
           
                rapportiAperti.AddRange(
                    biz.GetRapportiDiLavoroAperti(dichiarazione.IdLavoratoreSelezionato.Value,
                                                  dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto.Value));
            
            //rapportiAperti.AddRange(biz.GetDichiarazioniAperte(dichiarazione.IdDichiarazione.Value,
            //                                                   dichiarazione.Lavoratore.CodiceFiscale,
            //                                                   dichiarazione.RapportoDiLavoro.
            //                                                       DataInizioValiditaRapporto.Value));

            RadGridRapportDiLavoroCessazione.DataSource = rapportiAperti;
            RadGridRapportDiLavoroCessazione.DataBind();
        }
    }

    protected void RadGridRapportoDiLavoroCessazione_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            RapportoDiLavoro rapporto = (RapportoDiLavoro)e.Item.DataItem;

            Label lTipoRapportoLavoro = (Label)e.Item.FindControl("LabelTipoRapportoLavoro");
            Label lStato = (Label)e.Item.FindControl("LabelStato");
            Label lLavoratoreCodice = (Label)e.Item.FindControl("LabelLavoratoreCodice");
            Label lLavoratoreCognomeNome = (Label)e.Item.FindControl("LabelLavoratoreCognomeNome");
            Label lLavoratoreDataNascita = (Label)e.Item.FindControl("LabelLavoratoreDataNascita");
            Label lLavoratoreCodiceFiscale = (Label)e.Item.FindControl("LabelLavoratoreCodiceFiscale");

            Label lImpresaCodiceRagioneSociale = (Label)e.Item.FindControl("LabelImpresaCodiceRagioneSociale");
            Label lImpresaPartitaIva = (Label)e.Item.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label)e.Item.FindControl("LabelImpresaCodiceFiscale");

            Label lRapportoPeriodo = (Label)e.Item.FindControl("LabelRapportoPeriodo");

            if (rapporto.TipoRapportoLavoro == TipologiaRapportoLavoro.SiceNew)
            {
                lLavoratoreCodice.Text = rapporto.IdLavoratore.ToString();
            }

            lTipoRapportoLavoro.Text = rapporto.TipoRapportoLavoro.ToString();
            lStato.Text = rapporto.StatoPratica.ToString();
            lLavoratoreCognomeNome.Text = string.Format("{0} {1}", rapporto.Lavoratore.Cognome, rapporto.Lavoratore.Nome);
            lLavoratoreCodiceFiscale.Text = rapporto.Lavoratore.CodiceFiscale;
            lLavoratoreDataNascita.Text = rapporto.Lavoratore.DataNascita.ToString("dd/MM/yyyy");

            lImpresaCodiceRagioneSociale.Text = String.Format("{0} {1}", rapporto.Impresa.IdImpresa,
                                                              rapporto.Impresa.RagioneSociale);
            lImpresaPartitaIva.Text = rapporto.Impresa.PartitaIva;
            lImpresaCodiceFiscale.Text = rapporto.Impresa.CodiceFiscale;

            if (rapporto.DataFineValiditaRapporto != null)
            {
                if (rapporto.DataFineValiditaRapporto != new DateTime(2079, 6, 6))
                {
                    lRapportoPeriodo.Text = string.Format("{0} - {1}",
                                                          rapporto.DataInizioValiditaRapporto.Value.ToString(
                                                              "dd/MM/yyyy"),
                                                          rapporto.DataFineValiditaRapporto.Value.ToString("dd/MM/yyyy"));
                }
                else
                    lRapportoPeriodo.Text = string.Format("{0} ->",
                                                      rapporto.DataInizioValiditaRapporto.Value.ToString("dd/MM/yyyy"));
            }
            else
            {
                lRapportoPeriodo.Text = string.Format("{0} ->",
                                                      rapporto.DataInizioValiditaRapporto.Value.ToString("dd/MM/yyyy"));
            }
        }
    }

    protected void RadGridSdoppione_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            RapportoDiLavoro rapporto = (RapportoDiLavoro) e.Item.DataItem;

            Label lTipoRapportoLavoro = (Label) e.Item.FindControl("LabelTipoRapportoLavoro");
            Label lStato = (Label) e.Item.FindControl("LabelStato");
            Label lLavoratoreCodice = (Label) e.Item.FindControl("LabelLavoratoreCodice");
            Label lLavoratoreCognomeNome = (Label) e.Item.FindControl("LabelLavoratoreCognomeNome");
            Label lLavoratoreDataNascita = (Label) e.Item.FindControl("LabelLavoratoreDataNascita");
            Label lLavoratoreCodiceFiscale = (Label) e.Item.FindControl("LabelLavoratoreCodiceFiscale");

            Label lImpresaCodiceRagioneSociale = (Label) e.Item.FindControl("LabelImpresaCodiceRagioneSociale");
            Label lImpresaPartitaIva = (Label) e.Item.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label) e.Item.FindControl("LabelImpresaCodiceFiscale");
            Label lImpresaUltimaDenuncia = (Label)e.Item.FindControl("LabelImpresaUltimaDenuncia");

            Label lRapportoPeriodo = (Label) e.Item.FindControl("LabelRapportoPeriodo");

            if (rapporto.TipoRapportoLavoro == TipologiaRapportoLavoro.SiceNew)
            {
                lLavoratoreCodice.Text = rapporto.IdLavoratore.ToString();
            }

            lTipoRapportoLavoro.Text = rapporto.TipoRapportoLavoro.ToString();
            lStato.Text = rapporto.StatoPratica.ToString();
            lLavoratoreCognomeNome.Text = string.Format("{0} {1}", rapporto.Lavoratore.Cognome, rapporto.Lavoratore.Nome);
            lLavoratoreCodiceFiscale.Text = rapporto.Lavoratore.CodiceFiscale;
            lLavoratoreDataNascita.Text = rapporto.Lavoratore.DataNascita.ToString("dd/MM/yyyy");

            lImpresaCodiceRagioneSociale.Text = String.Format("{0} {1}", rapporto.Impresa.IdImpresa,
                                                              rapporto.Impresa.RagioneSociale);
            lImpresaPartitaIva.Text = rapporto.Impresa.PartitaIva;
            lImpresaCodiceFiscale.Text = rapporto.Impresa.CodiceFiscale;
            lImpresaUltimaDenuncia.Text = rapporto.Impresa.DataUltimaDenuncia.HasValue ?
                rapporto.Impresa.DataUltimaDenuncia.Value.ToString("MM/yyyy") : "N.D.";

            if (rapporto.DataFineValiditaRapporto != null)
            {

                if (rapporto.DataFineValiditaRapporto != new DateTime(2079,6,6))
                {
                    lRapportoPeriodo.Text = string.Format("{0} - {1}",
                                                          rapporto.DataInizioValiditaRapporto.Value.ToString(
                                                              "dd/MM/yyyy"),
                                                          rapporto.DataFineValiditaRapporto.Value.ToString("dd/MM/yyyy"));
                }
                else
                    lRapportoPeriodo.Text = string.Format("{0} ->",
                                                      rapporto.DataInizioValiditaRapporto.Value.ToString("dd/MM/yyyy"));
            }
            else
            {
                lRapportoPeriodo.Text = string.Format("{0} ->",
                                                      rapporto.DataInizioValiditaRapporto.Value.ToString("dd/MM/yyyy"));
            }
        }
    }

    protected void RadGridSdoppione_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridSdoppione.SelectedIndexes[0]);
        Int32 idLavoratore =
            (Int32)
            RadGridSdoppione.MasterTableView.DataKeyValues[indiceSelezionato]["IdLavoratore"];

        biz.CambiaLavoratoreSelezionato((Int32) ViewState["IdDichiarazione"], idLavoratore, false);

        Dichiarazione dichiarazione = biz.GetDichiarazione((Int32) ViewState["IdDichiarazione"]);
        CaricaDichiarazione(dichiarazione);

        SelezioneGrigliaMultipla(RadGridAnagraficaUguale, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUgualeTutto, idLavoratore);
        SelezioneGrigliaMultipla(RadGridCodiceFiscaleUguale, idLavoratore);
        SelezioneGrigliaMultipla(RadGridSdoppione, idLavoratore);
    }

    protected void RadGridRapportDiLavoroCessazione_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridRapportDiLavoroCessazione.SelectedIndexes[0]);
        Int32 idLavoratore =
            (Int32)
           RadGridRapportDiLavoroCessazione.MasterTableView.DataKeyValues[indiceSelezionato]["IdLavoratore"];
        Int32 idImpresa =
            (Int32)
           RadGridRapportDiLavoroCessazione.MasterTableView.DataKeyValues[indiceSelezionato]["IdImpresa"];
        DateTime dataFineValiditaRapporto =
            (DateTime)
           RadGridRapportDiLavoroCessazione.MasterTableView.DataKeyValues[indiceSelezionato]["DataFineValiditaRapporto"];

        biz.CambiaSelezioneRapportoCessazione((Int32)ViewState["IdDichiarazione"], true, idLavoratore, idImpresa, dataFineValiditaRapporto);

        biz.CambiaLavoratoreSelezionato((Int32)ViewState["IdDichiarazione"], idLavoratore, false);

        Dichiarazione dichiarazione = biz.GetDichiarazione((Int32)ViewState["IdDichiarazione"]);
        CaricaDichiarazione(dichiarazione);
    }

    protected void RadGridAnagraficaUguale_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridAnagraficaUguale.SelectedIndexes[0]);
        Int32 idLavoratore =
            (Int32)
            RadGridAnagraficaUguale.MasterTableView.DataKeyValues[indiceSelezionato]["IdLavoratore"];

        biz.CambiaLavoratoreSelezionato((Int32) ViewState["IdDichiarazione"], idLavoratore, false);

        Dichiarazione dichiarazione = biz.GetDichiarazione((Int32) ViewState["IdDichiarazione"]);
        CaricaDichiarazione(dichiarazione);

        SelezioneGrigliaMultipla(RadGridSdoppione, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUgualeTutto, idLavoratore);
        SelezioneGrigliaMultipla(RadGridCodiceFiscaleUguale, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUguale, idLavoratore);
    }

    protected void RadGridAnagraficaUgualeTutto_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridAnagraficaUgualeTutto.SelectedIndexes[0]);
        Int32 idLavoratore =
            (Int32)
            RadGridAnagraficaUgualeTutto.MasterTableView.DataKeyValues[indiceSelezionato]["IdLavoratore"];

        biz.CambiaLavoratoreSelezionato((Int32) ViewState["IdDichiarazione"], idLavoratore, false);

        Dichiarazione dichiarazione = biz.GetDichiarazione((Int32) ViewState["IdDichiarazione"]);
        CaricaDichiarazione(dichiarazione);

        SelezioneGrigliaMultipla(RadGridSdoppione, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUguale, idLavoratore);
        SelezioneGrigliaMultipla(RadGridCodiceFiscaleUguale, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUgualeTutto, idLavoratore);
    }

    protected void RadGridCodiceFiscaleUguale_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridCodiceFiscaleUguale.SelectedIndexes[0]);
        Int32 idLavoratore =
            (Int32)
            RadGridCodiceFiscaleUguale.MasterTableView.DataKeyValues[indiceSelezionato]["IdLavoratore"];

        biz.CambiaLavoratoreSelezionato((Int32) ViewState["IdDichiarazione"], idLavoratore, false);

        Dichiarazione dichiarazione = biz.GetDichiarazione((Int32) ViewState["IdDichiarazione"]);
        CaricaDichiarazione(dichiarazione);

        SelezioneGrigliaMultipla(RadGridSdoppione, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUguale, idLavoratore);
        SelezioneGrigliaMultipla(RadGridAnagraficaUgualeTutto, idLavoratore);
        SelezioneGrigliaMultipla(RadGridCodiceFiscaleUguale, idLavoratore);
    }



    private void SelezioneGrigliaMultipla(RadGrid griglia, Int32 valore)
    {
        foreach (GridDataItem it in griglia.Items)
        {
            if (griglia.MasterTableView.DataKeyValues[it.ItemIndex]["IdLavoratore"].ToString() == valore.ToString())
            {
                it.Selected = true;
                break;
            }
            it.Selected = false;
        }
    }

    protected void ButtonNuovoLavoratoreValido_Click(object sender, EventArgs e)
    {
        PosizioneValida = true;
        ImpostaNuovoLavoratore(true);
        
    }

    protected void ButtonNuovoLavoratoreNonValido_Click(object sender, EventArgs e)
    {
        PosizioneValida = false;
        ImpostaNuovoLavoratore(false);
    }

    private void ImpostaNuovoLavoratore(bool posizioneValida)
    {
        biz.CambiaLavoratoreSelezionato((Int32)ViewState["IdDichiarazione"], null, true);

        Dichiarazione dichiarazione = biz.GetDichiarazione((Int32)ViewState["IdDichiarazione"]);
        CaricaDichiarazione(dichiarazione);
        ResetGrid();
        //RadioButtonListIBAN.SelectedIndex = 0;
        RadioButtonListIndirizzo.SelectedIndex = 0;

        //RadioButtonListIBAN.Enabled = false;
        RadioButtonListIndirizzo.Enabled = false;

        biz.CambiaIBANDichiarato((Int32)ViewState["IdDichiarazione"], false);
        biz.CambiaIndirizzoDichiarato((Int32)ViewState["IdDichiarazione"], false);
        biz.CambiaSelezioneRapportoCessazione((Int32)ViewState["IdDichiarazione"], false, null, null, null);
        biz.CambiaPosizioneValida((Int32)ViewState["IdDichiarazione"],posizioneValida);
        
    }

    protected void ButtonAnnullaModifiche_Click(object sender, EventArgs e)
    {
        Int32 idDichiarazione = (Int32)ViewState["IdDichiarazione"];
        biz.ControlloSdoppioneEffettuato(idDichiarazione, false);

        biz.CambiaLavoratoreSelezionato(idDichiarazione, null, false);

        biz.CambiaIBANDichiarato(idDichiarazione, false);
        biz.CambiaIndirizzoDichiarato(idDichiarazione, false);
        biz.CambiaPosizioneValida(idDichiarazione,null);

        biz.CambiaSelezioneRapportoCessazione(idDichiarazione, false, null, null, null);

        ResetGrid();

        Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);
        CaricaDichiarazione(dichiarazione);
    }

    private void ResetGrid()
    {
        foreach (GridDataItem it in RadGridAnagraficaUguale.Items)
        {
            it.Selected = false;
        }
        foreach (GridDataItem it2 in RadGridAnagraficaUgualeTutto.Items)
        {
            it2.Selected = false;
        }
        foreach (GridDataItem it3 in RadGridCodiceFiscaleUguale.Items)
        {
            it3.Selected = false;
        }
        foreach (GridDataItem it4 in RadGridSdoppione.Items)
        {
            it4.Selected = false;
        }
    }

    protected void RadioButtonListControlloSdoppione_SelectedIndexChanged(object sender, EventArgs e)
    {
        Boolean controlloEffettuato = false;
        if (RadioButtonListControlloSdoppione.SelectedIndex == 0)
            controlloEffettuato = true;

        Int32 idDichiarazione = (Int32) ViewState["IdDichiarazione"];
        biz.ControlloSdoppioneEffettuato(idDichiarazione, controlloEffettuato);

        Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);
        CaricaDichiarazione(dichiarazione);
    }

    //protected void RadioButtonListControlloRapportoCessazione_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Boolean controlloEffettuato = false;
    //    if (RadioButtonListControlloRapportoCessazione.SelectedIndex == 0)
    //        controlloEffettuato = true;

    //    //Int32 idDichiarazione = (Int32)ViewState["IdDichiarazione"];
    //    //biz.ControlloSdoppioneEffettuato(idDichiarazione, controlloEffettuato);

    //    //Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);
    //    //CaricaDichiarazione(dichiarazione);
    //}

    //protected void RadioButtonListIBAN_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Int32 idDichiarazione = (Int32)ViewState["IdDichiarazione"];

    //    if (RadioButtonListIBAN.SelectedIndex == 1)
    //        biz.CambiaIBANDichiarato(idDichiarazione, true);
    //    else
    //        biz.CambiaIBANDichiarato(idDichiarazione, false);

    //    Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);
    //    CaricaDichiarazione(dichiarazione);
    //}

    protected void RadioButtonListIndirizzo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 idDichiarazione = (Int32) ViewState["IdDichiarazione"];

        if (RadioButtonListIndirizzo.SelectedIndex == 1)
            biz.CambiaIndirizzoDichiarato(idDichiarazione, true);
        else
            biz.CambiaIndirizzoDichiarato(idDichiarazione, false);

        Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);
        CaricaDichiarazione(dichiarazione);
    }
    protected void ButtonAnnullaSelezioneRapporto_Click(object sender, EventArgs e)
    {
        Int32 idDichiarazione = (Int32)ViewState["IdDichiarazione"];
        
        biz.CambiaSelezioneRapportoCessazione(idDichiarazione, false, null, null, null);

        foreach (GridDataItem it in RadGridRapportDiLavoroCessazione.Items)
        {
            it.Selected = false;
        }

        Dichiarazione dichiarazione = biz.GetDichiarazione(idDichiarazione);
        CaricaDichiarazione(dichiarazione);
    }
}