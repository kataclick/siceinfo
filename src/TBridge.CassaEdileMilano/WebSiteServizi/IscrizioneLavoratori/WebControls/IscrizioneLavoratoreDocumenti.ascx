﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreDocumenti.ascx.cs"
    Inherits="WebControls_IscrizioneLavoratori_IscrizioneLavoratoreDocumenti" %>
<asp:Panel ID="PanelDatiDocumenti" runat="server" Width="100%">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Documenti</b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Da compilare se il lavoratore è extra-UE
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Tipo di documento:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipoDocumento" runat="server" Width="250px" EmptyMessage="Selezionare il tipo di documento"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorTipoDocumento2" runat="server" ValidationGroup="datiAnagrafici"
                    ErrorMessage="Selezionare il tipo di documento" OnServerValidate="CustomValidatorTipoDocumento2_ServerValidate">
                    *
                </asp:CustomValidator>
                <%--<asp:CustomValidator
                    ID="CustomValidatorTipoDocumento1"
                    runat="server"
                    ValidationGroup="datiAnagrafici"
                    ErrorMessage="Selezionare il tipo di documento" 
                    onservervalidate="CustomValidatorTipoDocumento1_ServerValidate">
                    *
                </asp:CustomValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Numero del documento:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxNumeroDocumento" runat="server" Width="250px">
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorNumeroDocumento" runat="server" ValidationGroup="datiAnagrafici"
                    ErrorMessage="Numero del documento mancante" OnServerValidate="CustomValidatorNumeroDocumento_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Motivo del permesso:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxMotivoPermesso" runat="server" Width="250px"
                    EmptyMessage="Selezionare il motivo del permesso" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorMotivoPermesso" runat="server" ControlToValidate="RadComboBoxMotivoPermesso"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare il motivo del permesso"
                    OnServerValidate="CustomValidatorMotivoPermesso_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data richiesta permesso di soggiorno:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataRichiestaPermesso" runat="server" Width="250px" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data scadenza permesso:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataScadenzaPermesso" runat="server" Width="250px"  MaxDate="01/01/2200" />
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorDataScadenzaPermesso" runat="server" ControlToValidate="RadDatePickerDataScadenzaPermesso"
                    ValidationGroup="datiAnagrafici" ErrorMessage="La data di scadenza non può essere inferiore a quella di richiesta"
                    OnServerValidate="CustomValidatorDataScadenzaPermesso_ServerValidate">
                    *
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
