using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Presenter;
using Telerik.Web.UI;
using Geocoding = TBridge.Cemi.Cantieri.Business.CantieriGeocoding;

public partial class IscrizioneLavoratori_IscrizioneLavoratoreIndirizzo : UserControl
{
    private const Int32 INDICEDATIINDIRIZZO = 0;
    private const Int32 INDICEGEOCODIFICA = 1;
    private readonly Common bizCommon = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        ImpostaNomeUnivocoPerValidationGroup();

        if (!Page.IsPostBack)
        {
            ImpostaNomeUnivocoPerValidationGroup();

            CaricaProvince();
        }
    }

    public void PreCaricaIndirizzo(Indirizzo indirizzo)
    {
        if (indirizzo != null)
        {
            RadTextBoxIndirizzo.Text = indirizzo.Indirizzo1;
            RadTextBoxCivico.Text = indirizzo.Civico;
            RadComboBoxProvincia.SelectedValue = indirizzo.Provincia;
            CaricaComuni();
            RadComboBoxItem itemComune = RadComboBoxComune.FindItemByText(indirizzo.Comune.ToUpper());
            if (itemComune != null)
            {
                RadComboBoxComune.SelectedIndex = itemComune.Index;
            }
            RadTextBoxCap.Text = indirizzo.Cap;
        }
    }

    public void CaricaProvinciaMilano()
    {
        Boolean confermato = false;
        if (ViewState["Confermato"] != null)
            confermato = (Boolean) ViewState["Confermato"];

        if (!confermato)
        {
            RadComboBoxProvincia.SelectedValue = "MI";
            CaricaComuni();
        }
    }

    private void ImpostaNomeUnivocoPerValidationGroup()
    {
        RequiredFieldValidatorIndirizzo.ValidationGroup = ID;
        CustomValidatorProvincia.ValidationGroup = ID;
        CustomValidatorComune.ValidationGroup = ID;
        CustomValidatorCap.ValidationGroup = ID;
        ValidationSummaryIndirizzo.ValidationGroup = ID;
        ButtonGeocodifica.ValidationGroup = ID;
    }

    public void CaricaProvince()
    {
        if (RadComboBoxProvincia.Items.Count == 0)
        {
            Presenter.CaricaProvince(RadComboBoxProvincia);
            RadComboBoxProvincia.Items.Insert(0, new RadComboBoxItem("- Selezionare la Provincia -"));
        }
        RadAjaxManager.GetCurrent(Page).ClientEvents.OnResponseEnd = "";
    }

    private void CaricaComuni()
    {
        RadComboBoxComune.Text = null;
        RadComboBoxComune.ClearSelection();
        //RadComboBoxComune.Focus();

        Presenter.CaricaComuni(RadComboBoxComune, RadComboBoxProvincia.SelectedValue);
        RadComboBoxComune.Items.Insert(0, new RadComboBoxItem("- Selezionare il Comune -"));
    }

    public Boolean IndirizzoConfermato()
    {
        Boolean ret = false;

        if (ViewState["Confermato"] != null)
        {
            if ((Boolean) ViewState["Confermato"]) ret = true;
        }

        return ret;
    }

    protected void ButtonGeocodifica_Click(object sender, EventArgs e)
    {
        Page.Validate("geocodifica");

        if (Page.IsValid)
        {
            LabelNoGeocodifica.Visible = false;

            if (ButtonGeocodifica.Text == "Geocodifica")
            {
                Indirizzo indirizzo = CreaIndirizzo();

                IndirizzoCollection indirizzi = Geocoding.GeoCodeGoogleMultiplo(indirizzo.IndirizzoPerGeocoderGenerico);

                ViewState["IndirizziGeocodificati"] = indirizzi;
                Presenter.CaricaElementiInGridView(RadGridIndirizzi, indirizzi);

                if (indirizzi != null)
                {
                    if (indirizzi.Count > 0)
                    {
                        RadMultiPageSceltaIndirizzo.SelectedIndex = INDICEGEOCODIFICA;
                    }
                    else
                    {
                        ConfermaIndirizzo(false, true);
                    }
                }
                else
                {
                    ConfermaIndirizzo(false, true);
                }
            }
            else
            {
                if (ButtonGeocodifica.Text == "Modifica")
                {
                    ViewState["Confermato"] = null;
                    ButtonGeocodifica.Text = "Geocodifica";
                    //Mappa.Visible = false;

                    AbilitaIndirizzo();
                }
            }
        }
    }

    private Indirizzo CreaIndirizzo()
    {
        Indirizzo indirizzo = new Indirizzo();
        indirizzo.Indirizzo1 = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text);
        indirizzo.Civico = Presenter.NormalizzaCampoTesto(RadTextBoxCivico.Text);
        indirizzo.Cap = Presenter.NormalizzaCampoTesto(RadTextBoxCap.Text);
        indirizzo.Comune = Presenter.NormalizzaCampoTesto(RadComboBoxComune.SelectedItem.Text);
        indirizzo.Provincia = Presenter.NormalizzaCampoTesto(RadComboBoxProvincia.SelectedValue);

        return indirizzo;
    }

    protected void RadComboBoxProvincia_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (RadComboBoxProvincia.SelectedItem != null)
        {
            CaricaComuni();

            StringBuilder script = new StringBuilder();
            script.Append("Sys.Application.add_load(function(){");
            script.Append("var combo2 = $find('" + RadComboBoxComune.ClientID + "');");
            script.Append("var enabled = $find('" + RadComboBoxComune.Enabled + "');");
            script.Append("if(combo2!=null && combo2.get_enabled()){combo2.get_inputDomElement().focus();");
            script.Append("combo2.selectText(0, combo2.get_text().length - 1);} });");

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectText", script.ToString(), true);
        }
    }


    protected void ButtonConfermaIndirizzo_Click(object sender, EventArgs e)
    {
        Page.Validate("confermaIndirizzo");

        if (Page.IsValid)
        {
            if (RadGridIndirizzi.SelectedIndexes.Count == 1)
            {
                LabelValidationIndirizzo.Visible = false;
                IndirizzoCollection indirizzi = (IndirizzoCollection) ViewState["IndirizziGeocodificati"];
                CaricaIndirizzo(indirizzi[Int32.Parse(RadGridIndirizzi.SelectedIndexes[0])], false);
            }

            if (RadGridIndirizzi.SelectedIndexes.Count == 0)
            {
                LabelValidationIndirizzo.Visible = true;
            }
        }
    }

    protected void ButtonMantieni_Click(object sender, EventArgs e)
    {
        ConfermaIndirizzo(true, true);
    }

    protected void RadGridIndirizzi_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem) e.Item;
            ImageButton buttonVisualizzaMappa = (ImageButton)item["ViewMapColumn"].Controls[0];

            string comando = String.Format(
    "openRadWindow('{0}','{1}','{2}','{3}','{4}','{5}','{6}'); return false;",
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["Latitudine"],
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["Longitudine"],
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["Indirizzo1"],
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["Cap"],
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["Comune"],
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["Provincia"],
    item.OwnerTableView.DataKeyValues[item.ItemIndex]["InfoAggiuntiva"]
    );

            buttonVisualizzaMappa.Attributes.Add("OnClick",comando);

        }
    }

    private void CaricaIndirizzo(Indirizzo indirizzo, Boolean conCivico)
    {
        RadTextBoxIndirizzo.Text = indirizzo.Indirizzo1;
        if (conCivico)
        {
            RadTextBoxCivico.Text = indirizzo.Civico;
        }
        RadTextBoxCap.Text = indirizzo.Cap;

        // Cerco la provincia e il comune nelle combo box, se non le trovo lascio la selezione com'era
        String provinciaSelezionata = RadComboBoxProvincia.SelectedValue;
        String codiceComuneSelezionato = RadComboBoxComune.SelectedItem.Value;
        String descrizioneComuneSelezionato = RadComboBoxComune.SelectedItem.Text;

        ViewState["Latitudine"] = indirizzo.Latitudine;
        ViewState["Longitudine"] = indirizzo.Longitudine;

        String provinciaLodi = "LO";
        String denominazioneProvinciaLodi = "LODI";
        String provinciaMilano = "MI";
        String denominazioneProvinciaMilano = "MILANO";
        String provinciaMonzaBrianza = "MB";
        String denominazioneProvinciaMonzaBrianza = "MONZA BRIANZA";

        // LODI
        if (indirizzo.Provincia.ToUpper() == denominazioneProvinciaLodi)
        {
            if (RadComboBoxProvincia.SelectedValue != provinciaLodi)
            {
                RadComboBoxProvincia.SelectedValue = provinciaLodi;
                CaricaComuni();
            }
        }

        // MILANO
        if (indirizzo.Provincia.ToUpper() == denominazioneProvinciaMilano)
        {
            if (RadComboBoxProvincia.SelectedValue != provinciaMilano)
            {
                RadComboBoxProvincia.SelectedValue = provinciaMilano;
                CaricaComuni();
            }
        }

        // MONZA BRIANZA
        if (indirizzo.Provincia.ToUpper() == denominazioneProvinciaMonzaBrianza)
        {
            if (RadComboBoxProvincia.SelectedValue != provinciaMonzaBrianza)
            {
                RadComboBoxProvincia.SelectedValue = provinciaMonzaBrianza;
                CaricaComuni();
            }
        }

        if (RadComboBoxComune.SelectedItem.Text != indirizzo.Comune)
        {
            RadComboBoxItem itemComune = RadComboBoxComune.FindItemByText(indirizzo.Comune.ToUpper());

            if (itemComune != null)
            {
                RadComboBoxComune.SelectedIndex = itemComune.Index;
            }
            else
            {
                // Ripristino situazione iniziale
                RadComboBoxProvincia.SelectedValue = provinciaSelezionata;
                CaricaComuni();
                RadComboBoxComune.SelectedValue = codiceComuneSelezionato;
            }
        }

        ConfermaIndirizzo(true, true);
    }

    private void AbilitaIndirizzo()
    {
        LabelNoGeocodifica.Visible = false;

        RadTextBoxIndirizzo.Enabled = true;
        RadTextBoxCivico.Enabled = true;
        RadComboBoxProvincia.Enabled = true;
        RadComboBoxComune.Enabled = true;
        RadTextBoxCap.Enabled = true;
    }

    private void ConfermaIndirizzo(Boolean confermaUtente, Boolean modificabile)
    {
        RadMultiPageSceltaIndirizzo.SelectedIndex = INDICEDATIINDIRIZZO;

        RadTextBoxIndirizzo.Enabled = false;
        RadTextBoxCivico.Enabled = false;
        RadComboBoxProvincia.Enabled = false;
        RadComboBoxComune.Enabled = false;
        RadTextBoxCap.Enabled = false;

        ButtonGeocodifica.Text = "Modifica";

        LabelNoGeocodifica.Visible = !confermaUtente;
        ButtonGeocodifica.Visible = modificabile;
        PanelIndirizzo.Enabled = modificabile;

        ViewState["Confermato"] = true;
    }

    protected void CustomValidatorIndirizzoConfermato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (ViewState["Confermato"] == null)
        {
            args.IsValid = false;
        }
    }

    public void ImpostaValidationGroup(String validationGroup)
    {
        AbilitaValidationGroup(true);
        //CustomValidatorIndirizzoConfermato.ValidationGroup = validationGroup;
    }

    public void AbilitaValidationGroup(bool abilita)
    {
        RequiredFieldValidatorIndirizzo.Enabled = abilita;
        CustomValidatorProvincia.Enabled = abilita;
        CustomValidatorComune.Enabled = abilita;
        CustomValidatorCap.Enabled = abilita;
    }

    public void CaricaDatiIndirizzo(TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo indirizzo)
    {
        if (indirizzo != null)
        {
            RadTextBoxIndirizzo.Text = indirizzo.Indirizzo1;
            RadTextBoxCivico.Text = indirizzo.Civico;
            RadComboBoxProvincia.SelectedValue = indirizzo.Provincia;
            CaricaComuni();
            RadComboBoxComune.SelectedValue = indirizzo.Comune;
            RadTextBoxCap.Text = indirizzo.Cap;

            ConfermaIndirizzo(true, false);
        }
    }

    public void Reset()
    {
        AbilitaIndirizzo();

        RadTextBoxIndirizzo.Text = null;
        RadTextBoxCivico.Text = null;
        RadComboBoxProvincia.Text = null;
        RadComboBoxProvincia.ClearSelection();
        RadComboBoxComune.Text = null;
        RadComboBoxComune.ClearSelection();
        CaricaComuni();
        RadTextBoxCap.Text = null;

        ViewState["Latitudine"] = null;
        ViewState["Longitudine"] = null;

        ButtonGeocodifica.Text = "Geocodifica";
    }

    public TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo GetIndirizzo()
    {
        TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo indirizzo =
            new TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo();

        indirizzo.Indirizzo1 = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text);
        indirizzo.Civico = Presenter.NormalizzaCampoTesto(RadTextBoxCivico.Text);
        indirizzo.Provincia = RadComboBoxProvincia.SelectedValue;
        indirizzo.Comune = RadComboBoxComune.SelectedValue;
        //if (RadComboBoxComune.SelectedItem != null)
        if (RadComboBoxComune.SelectedIndex > 0)
        {
            indirizzo.ComuneDescrizione = RadComboBoxComune.Text; //Cambiato Bonfi: RadComboBoxComune.SelectedItem.Text;
        }
        indirizzo.Cap = RadTextBoxCap.Text;

        indirizzo.Latitudine = ViewState["Latitudine"] as decimal?;
        indirizzo.Longitudine = ViewState["Longitudine"] as decimal?;

        return indirizzo;
    }

    #region Custom Validators

    protected void CustomValidatorProvincia_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxProvincia.SelectedItem != null && RadComboBoxProvincia.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorComune_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxComune.SelectedItem != null && RadComboBoxComune.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorCAP_ServerValidate(object source, ServerValidateEventArgs args)
    {
        try
        {
            args.IsValid = false;
            CustomValidatorCap.ErrorMessage = "CAP non valido";
            int iCAP;
            String cap = RadTextBoxCap.Text;

            if (cap.Length == 5 && Int32.TryParse(cap, out iCAP))
            {
                args.IsValid = true;

                string codiceComboComune = RadComboBoxComune.SelectedItem.Value;

                if (bizCommon.IsGrandeCitta(codiceComboComune) && cap[2] == '1' && cap[3] == '0' && cap[4] == '0')
                {
                    args.IsValid = false;
                    CustomValidatorCap.ErrorMessage = "Non pu&#242; essere utilizzato un CAP generico";
                }
            }
        }
        catch
        {
        }
    }

    //protected void CustomValidatorSingoloIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    //{
    //    args.IsValid = false;

    //    if ((RadGridIndirizzi.SelectedIndexes.Count == 1) || (RadGridIndirizzi.SelectedIndexes.Count == 0))
    //    {
    //        args.IsValid = true;
    //    }
    //}

    #endregion
}