using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Sintesi.Business;
using Telerik.Web.UI;

public partial class WebControls_IscrizioneLavoratori_IscrizioneLavoratoreDocumenti : UserControl
{
    private const String ValidationGroup = "datiAnagrafici";
    private readonly SintesiBusiness _sintesiBiz = new SintesiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        //RangeValidatorDataRichiestaPermesso.MaximumValue = DateTime.Now.ToString("yyyy-MM-dd-00-00-00");
        //RangeValidatorDataScadenzaPermesso.MinimumValue = DateTime.Now.ToString("yyyy-MM-dd-00-00-00");
        //RadDateInputDataRichiestaPermesso.MaxDate = DateTime.Now;
        //RadDateInputDataScadenzaPermesso.MinDate = DateTime.Now;

        if (!Page.IsPostBack)
        {
            CaricaCombo();
        }
    }

    private void CaricaCombo()
    {
        CaricaStatusStraniero();
        CaricaMotiviPermesso();
    }

    private void CaricaMotiviPermesso()
    {
        if (RadComboBoxMotivoPermesso.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxMotivoPermesso,
                _sintesiBiz.GetMotiviPermesso(),
                "Descrizione",
                "Codice");
            RadComboBoxMotivoPermesso.Items.Insert(0, new RadComboBoxItem("- Selezionare il motivo del permesso -"));
        }
    }

    private void CaricaStatusStraniero()
    {
        if (RadComboBoxTipoDocumento.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipoDocumento,
                _sintesiBiz.GetStatusStraniero(),
                "Descrizione",
                "Codice");
            RadComboBoxTipoDocumento.Items.Insert(0, new RadComboBoxItem("- Selezionare il tipo di documento -"));
        }
    }

    public void StatoCampi(Boolean abilita)
    {
        PanelDatiDocumenti.Enabled = abilita;
    }

    public void Reset()
    {
        RadComboBoxTipoDocumento.ClearSelection();
        RadTextBoxNumeroDocumento.Text = null;
        RadComboBoxMotivoPermesso.ClearSelection();
        RadDatePickerDataRichiestaPermesso.Clear();
        RadDatePickerDataScadenzaPermesso.Clear();

        AbilitazioneValidator(true);
    }

    public void CaricaLavoratore(Lavoratore lavoratore)
    {
        Reset();
        CaricaCombo();

        RadComboBoxTipoDocumento.Text = " ";
        RadComboBoxMotivoPermesso.Text = " ";

        AbilitazioneValidator(false);
    }

    public void CompletaLavoratore(Lavoratore lavoratore)
    {
        if (RadComboBoxTipoDocumento.SelectedIndex > 0)
        {
            lavoratore.TipoDocumento = RadComboBoxTipoDocumento.SelectedValue;
        }
        lavoratore.NumeroDocumento = Presenter.NormalizzaCampoTesto(RadTextBoxNumeroDocumento.Text);
        if (RadComboBoxMotivoPermesso.SelectedIndex > 0)
        {
            lavoratore.MotivoPermesso = RadComboBoxMotivoPermesso.SelectedValue;
        }
        if (RadDatePickerDataRichiestaPermesso.SelectedDate.HasValue)
        {
            lavoratore.DataRichiestaPermesso = RadDatePickerDataRichiestaPermesso.SelectedDate.Value;
        }
        if (RadDatePickerDataScadenzaPermesso.SelectedDate.HasValue)
        {
            lavoratore.DataScadenzaPermesso = RadDatePickerDataScadenzaPermesso.SelectedDate.Value;
        }
    }

    private void AbilitazioneValidator(Boolean abilita)
    {
        foreach (BaseValidator validator in Page.Validators)
        {
            if (validator.ValidationGroup == ValidationGroup)
            {
                validator.Enabled = abilita;
            }
        }
    }

    public Boolean ControlloScadenzaDocumento()
    {
        Boolean ret = true;

        if (RadDatePickerDataScadenzaPermesso.SelectedDate.HasValue)
        {
            if (RadDatePickerDataScadenzaPermesso.SelectedDate < DateTime.Now)
            {
                ret = false;
            }
        }

        return ret;
    }

    #region Custom Validators

    protected void CustomValidatorMotivoPermesso_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadComboBoxMotivoPermesso.SelectedIndex <= 0 && RadComboBoxTipoDocumento.SelectedValue == "1")
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorNumeroDocumento_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadComboBoxTipoDocumento.SelectedIndex > 0
            && String.IsNullOrEmpty(RadTextBoxNumeroDocumento.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorTipoDocumento1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadComboBoxTipoDocumento.SelectedIndex <= 0)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorTipoDocumento2_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadComboBoxTipoDocumento.SelectedIndex <= 0
            && !String.IsNullOrEmpty(RadTextBoxNumeroDocumento.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorDataScadenzaPermesso_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDatePickerDataRichiestaPermesso.SelectedDate.HasValue &&
            RadDatePickerDataScadenzaPermesso.SelectedDate.HasValue)
        {
            if (RadDatePickerDataRichiestaPermesso.SelectedDate > RadDatePickerDataScadenzaPermesso.SelectedDate)
            {
                args.IsValid = false;
            }
        }
    }

    #endregion
}