using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Delegates;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;
using Telerik.Web.UI;
using TBridge.Cemi.Presenter;

public partial class IscrizioneLavoratori_RicercaNotifica : UserControl
{
    private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
    private readonly Common commonBiz = new Common();
   // public event DichiarazioneSelectedEventHandler OnDichiarazioneSelected;

    

    protected void Page_Load(object sender, EventArgs e)
    {
        //RadNumericTextBoxCodice.DataType = typeof (Int32);
        if (!IsPostBack)
        {
            CaricaNazionalita();
            CaricaRicercaPrecendete();
        }
        
        ClientScriptManager cs = Page.ClientScript;
        if (!cs.IsStartupScriptRegistered(this.GetType(), "scriptGestDuplOnLoad"))
        {
            cs.RegisterStartupScript(this.GetType(), "scriptGestDuplOnLoad", "javascript:ManageDuplicazione(); ", true);
        }

       // ScriptManager.RegisterStartupScript(this, this.GetType(), "scriptGestDuplOnLoad", "javascript:ManageDuplicazione(); ", true);
      
    }

    //private void GestisciAbilitazioneFiltriDuplicazione()
    //{
    //    Boolean enabled = RadioButtonListStato.SelectedValue == ((int)TipoStatoGestionePratica.DaValutare).ToString();
    //    if (enabled)
    //    {
    //        CheckBoxCfDiversoDatiUguali.Checked = false;
    //        CheckBoxCfUgualeDatiDiversi.Checked = false;
    //        CheckBoxCfUgualeDatiUguali.Checked = false;
    //        CheckBoxCfUgualeRapportoAperto.Checked = false;
    //    }

    //    CheckBoxCfDiversoDatiUguali.Enabled = enabled;
    //    CheckBoxCfUgualeDatiDiversi.Enabled = enabled;
    //    CheckBoxCfUgualeDatiUguali.Enabled = enabled;
    //    CheckBoxCfUgualeRapportoAperto.Enabled = enabled;
    //}

    //public void Reset()
    //{
    //    ResetFiltro();
    //    RadGridNotifiche.Visible = false;
    //}

    //private void ResetFiltro()
    //{
    //    RadDatePickerDataInvioA.SelectedDate = null;
    //    RadDatePickerDataInvioDa.SelectedDate = null;
    //    RadioButtonListAttivita.SelectedIndex = 2;
    //    RadNumericTextBoxCodiceImpresa.Text = null;
    //    RadTextBoxPartitaIVA.Text = null;
    //    RadTextBoxCodiceFiscale.Text = null;
    //    RadTextBoxCognome.Text = null;
    //    RadioButtonListStato.SelectedIndex = 3;
    //    RadTextBoxID.Text = null;
    //}

    private void CaricaNazionalita()
    {
        if (ComboBoxNazionalita.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                ComboBoxNazionalita,
                commonBiz.GetNazioni(),
                "Value",
                "Key");
        }
        ComboBoxNazionalita.Items.Insert(0, new RadComboBoxItem("QUALSIASI"));
    }

    private void VisualizzaFiltro(DichiarazioneFilter filtroo)
    {

        RadDatePickerDataInvioA.SelectedDate = filtroo.DataInvioA;

        RadDatePickerDataInvioDa.SelectedDate = filtroo.DataInvioDa;


        if (filtroo.TipoAttivita.HasValue)
        {
            RadioButtonListAttivita.SelectedValue = filtroo.TipoAttivita.Value.ToString();
        }


        RadNumericTextBoxCodiceImpresa.Text = filtroo.CodiceImpresa;
        
        RadTextBoxPartitaIVA.Text = filtroo.PartitaIVA;


        RadTextBoxCodiceFiscale.Text = filtroo.CodiceFiscale;



        RadTextBoxCognome.Text = filtroo.Cognome;

        if (filtroo.IdDichiarazione.HasValue)
        {
            RadTextBoxID.Text = filtroo.IdDichiarazione.Value.ToString();
        }


        if (filtroo.StatoGestionePratica.HasValue)
        {
            RadioButtonListStato.SelectedValue = ((int)filtroo.StatoGestionePratica.Value).ToString();

            if (filtroo.StatoGestionePratica.Value == TipoStatoGestionePratica.DaValutare)
            {
                CheckBoxCfDiversoDatiUguali.Checked = filtroo.DuplicazioneCfDiversoDatiUguali;
                CheckBoxCfUgualeDatiDiversi.Checked = filtroo.DuplicazioneCfUgualeDatiDiversi;
                CheckBoxCfUgualeDatiUguali.Checked = filtroo.DuplicazioneCfUgualeDatiUguali;
                CheckBoxCfUgualeRapportoAperto.Checked = filtroo.DuplicazioneCfUgualeRapportoAperto;
            }
        }
        else
        {
            //TUTTE
            RadioButtonListStato.SelectedValue = "T";
        }
        //GestisciAbilitazioneFiltriDuplicazione();

        if (!String.IsNullOrEmpty(filtroo.IdNazionalita))
        {
            ComboBoxNazionalita.SelectedValue = filtroo.IdNazionalita;
        }

        
    }

    private void CaricaRicercaPrecendete()
    {
        DichiarazioneFilter filtroOld = Session["FiltroOld"] as DichiarazioneFilter;
        Session["FiltroOld"] = null;
        if (filtroOld != null)
        {
            VisualizzaFiltro(filtroOld);
            CaricaNotifiche(filtroOld);

        }
    }

    private void CaricaNotifiche(DichiarazioneFilter filtro)
    {
        RadGridNotifiche.Visible = true;

        DichiarazioneCollection dichiarazioni = biz.GetDichiarazioni(filtro);

        Presenter.CaricaElementiInGridView(
            RadGridNotifiche,
            dichiarazioni);
    }

    private void CaricaNotifiche()
    {
        //RadGridNotifiche.Visible = true;
        //DichiarazioneFilter filtro = GetFiltro();

        //DichiarazioneCollection dichiarazioni = biz.GetDichiarazioni(filtro);

        //Presenter.CaricaElementiInGridView(
        //    RadGridNotifiche,
        //    dichiarazioni);
        CaricaNotifiche(GetFiltro());
    }

    private DichiarazioneFilter GetFiltro()
    {
        DichiarazioneFilter filtro = new DichiarazioneFilter();


        if (RadDatePickerDataInvioA.SelectedDate != null)
        {
            filtro.DataInvioA = RadDatePickerDataInvioA.SelectedDate;
        }

        if (RadDatePickerDataInvioDa.SelectedDate != null)
        {
            filtro.DataInvioDa = RadDatePickerDataInvioDa.SelectedDate;
        }

        filtro.TipoAttivita = null;
        if (RadioButtonListAttivita.SelectedValue != "0")
        {
            filtro.TipoAttivita =
                (TipoAttivita) Enum.Parse(typeof (TipoAttivita), RadioButtonListAttivita.SelectedValue);
        }

        if (!String.IsNullOrEmpty(RadNumericTextBoxCodiceImpresa.Text))
        {
            filtro.CodiceImpresa = RadNumericTextBoxCodiceImpresa.Text;
        }

        if (!String.IsNullOrEmpty(RadTextBoxPartitaIVA.Text))
        {
            filtro.PartitaIVA = RadTextBoxPartitaIVA.Text;
        }

        if (!String.IsNullOrEmpty(RadTextBoxCodiceFiscale.Text))
        {
            filtro.CodiceFiscale = RadTextBoxCodiceFiscale.Text;
        }

        if (!String.IsNullOrEmpty(RadTextBoxCognome.Text))
        {
            filtro.Cognome = RadTextBoxCognome.Text;
        }

        if (ComboBoxNazionalita.SelectedIndex >= 1)
        {
            filtro.IdNazionalita = ComboBoxNazionalita.SelectedValue;
        }

        filtro.StatoGestionePratica = null;
        if (RadioButtonListStato.SelectedValue != "T")
        {
            filtro.StatoGestionePratica = (TipoStatoGestionePratica)Int32.Parse(RadioButtonListStato.SelectedValue);
        }


        // Checkbox duplicazione abilitate solo per comunicazioni "da valutare"
        if (filtro.StatoGestionePratica.HasValue && filtro.StatoGestionePratica.Value == TipoStatoGestionePratica.DaValutare)
        {
            // This is a known problem with ASP.NET - for some reason ASP.NET won't update a checkbox on postback 
            // if it was disabled during page load and not checked for postback. 
            // I don't know exactly why that is though - if you make the checkbox unselected by default and select it, 
            // the value is changed on the server correctly.

            CheckBoxCfDiversoDatiUguali.Checked = String.Equals(Request.Form[CheckBoxCfDiversoDatiUguali.UniqueID], "on", StringComparison.CurrentCultureIgnoreCase);
            CheckBoxCfUgualeDatiDiversi.Checked = String.Equals(Request.Form[CheckBoxCfUgualeDatiDiversi.UniqueID], "on", StringComparison.CurrentCultureIgnoreCase);
            CheckBoxCfUgualeDatiUguali.Checked = String.Equals(Request.Form[CheckBoxCfUgualeDatiUguali.UniqueID], "on", StringComparison.CurrentCultureIgnoreCase);
            CheckBoxCfUgualeRapportoAperto.Checked = String.Equals(Request.Form[CheckBoxCfUgualeRapportoAperto.UniqueID], "on", StringComparison.CurrentCultureIgnoreCase);


            filtro.DuplicazioneCfDiversoDatiUguali = CheckBoxCfDiversoDatiUguali.Checked;
            filtro.DuplicazioneCfUgualeDatiDiversi = CheckBoxCfUgualeDatiDiversi.Checked;
            filtro.DuplicazioneCfUgualeDatiUguali = CheckBoxCfUgualeDatiUguali.Checked;
            filtro.DuplicazioneCfUgualeRapportoAperto = CheckBoxCfUgualeRapportoAperto.Checked; 
        }

        try
        {
            if (!String.IsNullOrEmpty(RadTextBoxID.Text))
            {
                filtro.IdDichiarazione = Int32.Parse(RadTextBoxID.Text);
            }
        }
        catch (Exception)
        {
            
            //lanciare eccezione
        }



        return filtro;
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche();
        }
    }

    protected void RadGridNotifiche_PageIndexChanged(object source, GridPageChangedEventArgs e)
    {
        CaricaNotifiche();
    }


    protected void RadGridNotifiche_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            Dichiarazione dichiarazione = (Dichiarazione) e.Item.DataItem;

            Label lLavoratoreCognomeNome = (Label) e.Item.FindControl("LabelLavoratoreCognomeNome");
            Label lLavoratoreDataNascita = (Label) e.Item.FindControl("LabelLavoratoreDataNascita");
            Label lLavoratoreCodiceFiscale = (Label) e.Item.FindControl("LabelLavoratoreCodiceFiscale");
            Label lImpresaCodiceRagioneSociale = (Label) e.Item.FindControl("LabelImpresaCodiceRagioneSociale");
            Label lImpresaPartitaIva = (Label) e.Item.FindControl("LabelImpresaPartitaIva");
            Label lImpresaCodiceFiscale = (Label) e.Item.FindControl("LabelImpresaCodiceFiscale");
            Label lStato = (Label) e.Item.FindControl("LabelStato");
            Label lAttivitaTipo= (Label) e.Item.FindControl("LabelAttivitaTipo");
            Label lRettifica = (Label) e.Item.FindControl("LabelRettifica");
           

            lLavoratoreCognomeNome.Text = dichiarazione.Lavoratore.CognomeNome;
            lLavoratoreCodiceFiscale.Text = dichiarazione.Lavoratore.CodiceFiscale;
            lLavoratoreDataNascita.Text = dichiarazione.Lavoratore.DataNascita.ToString("dd/MM/yyyy");
            lImpresaCodiceRagioneSociale.Text = String.Format("{0} {1}", dichiarazione.Impresa.IdImpresa,
                                                              dichiarazione.Impresa.RagioneSociale);
            lImpresaPartitaIva.Text = dichiarazione.Impresa.PartitaIva;
            lImpresaCodiceFiscale.Text = dichiarazione.Impresa.CodiceFiscale;
            lStato.Text = dichiarazione.Stato.ToString();
            
            switch (dichiarazione.Attivita)
            {
                case TipoAttivita.Assunzione:
                    lAttivitaTipo.Text = "Assunzione";
                    break;
                case TipoAttivita.Cessazione:
                    lAttivitaTipo.Text = "Cessazione";
                    break;
            }
            
            if (dichiarazione.IsRettifica)

                lRettifica.Text = string.Format("{0}{1}", "Rettifica di: ",
                                                dichiarazione.IdDichiarazionePrecedente.HasValue ?
                                                dichiarazione.IdDichiarazionePrecedente.Value.ToString() : "N.D.");

            if (dichiarazione.IsRettificata)
            {
                if(!string.IsNullOrEmpty(lRettifica.Text))
                {
                    lRettifica.Text = string.Format("{0}{1}{2}{3}",lRettifica.Text, "<br/>", "Rettificata da: ",
                                                     dichiarazione.IdDichiarazioneSuccessiva.HasValue ?
                                                     dichiarazione.IdDichiarazioneSuccessiva.Value.ToString() : "N.D.");
                }
                else
                {
                    lRettifica.Text = string.Format("{0}{1}", "Rettificata da: ",
                                                    dichiarazione.IdDichiarazioneSuccessiva.HasValue ?
                                                    dichiarazione.IdDichiarazioneSuccessiva.Value.ToString() : "N.D.");
                }
               
            }
        }
    }

    protected void RadGridNotifiche_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 indiceSelezionato = Int32.Parse(RadGridNotifiche.SelectedIndexes[0]);
        Int32 idDichiarazione =
            (Int32) RadGridNotifiche.MasterTableView.DataKeyValues[indiceSelezionato]["IdDichiarazione"];

        Session["FiltroOld"] = GetFiltro();
        Context.Items["IdDichiarazione"] = idDichiarazione;
        Server.Transfer("~/IscrizioneLavoratori/DettaglioDichiarazione.aspx");
    }
}