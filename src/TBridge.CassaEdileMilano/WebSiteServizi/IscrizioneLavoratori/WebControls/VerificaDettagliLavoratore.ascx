﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VerificaDettagliLavoratore.ascx.cs"
    Inherits="WebControls_IscrizioneLavoratori_VerificaDettagliLavoratore" %>
<telerik:RadTabStrip ID="RadTabStripLavoratore" runat="server" MultiPageID="RadMultiPageLavoratore"
    SelectedIndex="1" Width="100%" Font-Size="Smaller">
    <Tabs>
        <telerik:RadTab Text="Definizione lavoratore" Selected="True">
        </telerik:RadTab>
        <telerik:RadTab Text="Dati Lavoratore" >
        </telerik:RadTab>
        <telerik:RadTab Text="Rapporti di lavoro" ID="tabRapporti" runat="server" Visible="false">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageLavoratore" runat="server" SelectedIndex="0"
    CssClass="radWizard" Width="96%">
    <telerik:RadPageView ID="RadPageViewLavoratore" runat="server">
        <table runat="server" id="tableControlli" class="standardTable">
            <tr>
                <td>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:Image ID="ImageControlloSelezioneLavoratore" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                            </td>
                            <td>
                                <b>Selezione lavoratore</b>
                            </td>
                            <td>
                                <b><small>&nbsp;<asp:Label ID="LabelSelezioneLavoratore" runat="server">
                                </asp:Label></small></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="ButtonNuovoLavoratoreValido" runat="server" OnClick="ButtonNuovoLavoratoreValido_Click"
                                    Text="Nuovo Lavoratore valido" Width="150pt" />
                            </td>
                            <td>
                                <asp:Button ID="ButtonAnnullaModifiche" runat="server" OnClick="ButtonAnnullaModifiche_Click"
                                    Text="Annulla modifiche effettuate" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="ButtonNuovoLavoratoreNonValido" runat="server" OnClick="ButtonNuovoLavoratoreNonValido_Click"
                                    Text="Nuovo Lavoratore non valido" Width="150pt" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="borderedTable">
                        <tr>
                            <td>
                                <b>Lavoratori uguali per codice fiscale</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="RadGridCodiceFiscaleUguale" runat="server" Width="100%" GridLines="None"
                                    PageSize="5" OnSelectedIndexChanged="RadGridCodiceFiscaleUguale_SelectedIndexChanged">
                                    <MasterTableView DataKeyNames="IdLavoratore">
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px" />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="IdLavoratore" HeaderText="Codice" UniqueName="Codice">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Cognome" HeaderText="Cognome" UniqueName="column1">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" UniqueName="column2">
                                                <ItemStyle Width="200px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Sesso" HeaderText="Sesso" UniqueName="column3">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DataNascita" HeaderText="Data nascita" UniqueName="column4"
                                                DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ComuneNascita" HeaderText="Luogo nascita" UniqueName="column5">
                                                <ItemStyle Width="200px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CodiceFiscale" HeaderText="Codice Fiscale" UniqueName="column6">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Text="Seleziona"
                                                UniqueName="column">
                                                <ItemStyle Width="10px" />
                                            </telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="False" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Lavoratori uguali per dati anagrafici</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="ImageControlloAnagraficaUgualeSenzaOmocodia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                Anagrafica uguale senza omocodia
                                <asp:Image ID="ImageControlloAnagraficaUgualeConOmocodia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                Anagrafica uguale con omocodia
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="RadGridAnagraficaUguale" runat="server" Width="100%" AllowPaging="False"
                                    GridLines="None" PageSize="5" OnSelectedIndexChanged="RadGridAnagraficaUguale_SelectedIndexChanged">
                                    <MasterTableView DataKeyNames="IdLavoratore">
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px" />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="IdLavoratore" HeaderText="Codice" UniqueName="Codice">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Cognome" HeaderText="Cognome" UniqueName="column1">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" UniqueName="column2">
                                                <ItemStyle Width="200px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Sesso" HeaderText="Sesso" UniqueName="column3">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DataNascita" HeaderText="Data nascita" UniqueName="column4"
                                                DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ComuneNascita" HeaderText="Luogo nascita" UniqueName="column5">
                                                <ItemStyle Width="200px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CodiceFiscale" HeaderText="Codice Fiscale" UniqueName="column6">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Text="Seleziona"
                                                UniqueName="column">
                                                <ItemStyle Width="10px" />
                                            </telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="False" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Lavoratori uguali per dati anagrafici e codice fiscale</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="ImageControlloAnagraficaDoppione" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                Doppione
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="RadGridAnagraficaUgualeTutto" runat="server" Width="100%" AllowPaging="False"
                                    GridLines="None" PageSize="5" OnSelectedIndexChanged="RadGridAnagraficaUgualeTutto_SelectedIndexChanged">
                                    <MasterTableView DataKeyNames="IdLavoratore">
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px" />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="IdLavoratore" HeaderText="Codice" UniqueName="Codice">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Cognome" HeaderText="Cognome" UniqueName="column1">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" UniqueName="column2">
                                                <ItemStyle Width="200px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Sesso" HeaderText="Sesso" UniqueName="column3">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="DataNascita" HeaderText="Data nascita" UniqueName="column4"
                                                DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ComuneNascita" HeaderText="Luogo nascita" UniqueName="column5">
                                                <ItemStyle Width="200px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="CodiceFiscale" HeaderText="Codice Fiscale" UniqueName="column6">
                                                <ItemStyle Width="50px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Text="Seleziona"
                                                UniqueName="column">
                                                <ItemStyle Width="10px" />
                                            </telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="False" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>RapportiTrovati</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="ImageControlloAnagraficaSdoppione" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                Sdoppione&nbsp;
                                <asp:RadioButtonList ID="RadioButtonListControlloSdoppione" runat="server" RepeatDirection="Horizontal"
                                    Visible="false" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListControlloSdoppione_SelectedIndexChanged">
                                    <asp:ListItem>Controllo effettuato</asp:ListItem>
                                    <asp:ListItem>Controllo non effettuato</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <telerik:RadGrid ID="RadGridSdoppione" runat="server" Width="100%" GridLines="None"
                                    PageSize="5" OnItemDataBound="RadGridSdoppione_ItemDataBound" OnSelectedIndexChanged="RadGridSdoppione_SelectedIndexChanged">
                                    <MasterTableView DataKeyNames="IdLavoratore">
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px" />
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px" />
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn HeaderText="Codice" UniqueName="Codice">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelLavoratoreCodice" runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="10px" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="TipoRapportoLavoro" HeaderText="Fonte" UniqueName="column">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelTipoRapportoLavoro" runat="server"></asp:Label>
                                                    <br />
                                                    <small>
                                                        <asp:Label ID="LabelStato" runat="server"></asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="LabelLavoratoreCognomeNome" runat="server">
                                                        </asp:Label>
                                                    </b>
                                                    <br />
                                                    <small>
                                                        <asp:Label ID="LabelLavoratoreDataNascita" runat="server">
                                                        </asp:Label>
                                                        <br />
                                                        <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server">
                                                        </asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="LabelImpresaCodiceRagioneSociale" runat="server">
                                                        </asp:Label>
                                                    </b>
                                                    <br />
                                                    <small>
                                                        <asp:Label ID="LabelImpresaPartitaIva" runat="server">
                                                        </asp:Label>
                                                        <br />
                                                        <asp:Label ID="LabelImpresaCodiceFiscale" runat="server">
                                                        </asp:Label>
                                                    </small>
                                                    <br />
                                                    <small>
                                                        Ultima denuncia:
                                                        <b>
                                                            <asp:Label ID="LabelImpresaUltimaDenuncia" runat="server" />
                                                        </b>  
                                                    </small>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="Periodo">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="LabelRapportoPeriodo" runat="server">
                                                        </asp:Label>
                                                    </b>
                                                </ItemTemplate>
                                                <ItemStyle Width="120px" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings>
                                        <Selecting AllowRowSelect="False" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewLavoratoreDati" runat="server">
        <table runat="server" id="tableControlliDati" class="standardTable">
            <tr>
                <td>
                    <b>Dati anagrafici</b>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="borderedTable">
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageControlloAnagraficaDubbia" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            Anagrafica dubbia
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td width="20%">
                                            <b></b>
                                        </td>
                                        <td width="30%">
                                            <b>Dichiarato </b>
                                        </td>
                                        <td width="30%">
                                            <b>Anagrafica </b>
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cognome e nome
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCognomeNome" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCognomeNomeAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sesso
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelSesso" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelSessoAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Data di nascita
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelDataNascita" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelDataNascitaAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Comune e provincia di nascita
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelProvinciaComuneNascita" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelProvinciaComuneNascitaAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Codice fiscale
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCodiceFiscale" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCodiceFiscaleAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                <table width="280">
                                    <tr>
                                        <td>
                                            <asp:Button ID="Button1" runat="server" Text="Verifica corrispondenza dati anagrafici" Width="100%"
                                                 OnClientClick="javascript:
                                                var leftPos = screen.width/2 + 20;
                                                var widthWin = screen.width/2 - 50;
                                                NewWindow=window.open('https://telematici.agenziaentrate.gov.it/VerificaCF/Scegli.do?parameter=verificaCfPf','NewWindow','width='+widthWin+',height=850,resizable=yes,scrollbars=yes,left='+leftPos); 
                                                NewWindow.focus();
                                                return false;"  />
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                     <tr>
                                        <td >
                                            <asp:Button ID="Button2" runat="server" Text="Verifica codice fiscale" Width="100%" OnClientClick="javascript:
                                                var leftPos = screen.width/2 + 20;
                                                var widthWin = screen.width/2 - 50;
                                                NewWindow=window.open('https://telematici.agenziaentrate.gov.it/VerificaCF/Scegli.do?parameter=verificaCf','NewWindow','width='+widthWin+',height=850,resizable=yes,scrollbars=yes,left='+leftPos); 
                                                NewWindow.focus();
                                                return false;"  />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                       
                       <%-- <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageEmissioneTesseraSanitaria" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            Tessera sanitaria
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>--%>
                        <%--<tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>--%>
                        <%--<tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageControlloAnagraficaIBAN" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            IBAN diverso
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td width="20%">
                                            <b></b>
                                        </td>
                                        <td width="30%">
                                            <b>Dichiarato </b>
                                        </td>
                                        <td width="30%">
                                            <b>Anagrafica </b>
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            IBAN
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelIBANDichiarato" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelIBANAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="RadioButtonListIBAN" runat="server" RepeatDirection="Vertical"
                                                AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListIBAN_SelectedIndexChanged">
                                                <asp:ListItem>Dichiarato</asp:ListItem>
                                                <asp:ListItem>Anagrafica</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <b>Indirizzo</b>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="borderedTable">
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageControlloAnagraficaIndirizzo" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            Indirizzo diverso
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td width="20%">
                                            <b></b>
                                        </td>
                                        <td width="30%">
                                            <b>Dichiarato </b>
                                        </td>
                                        <td width="30%">
                                            <b>Anagrafica </b>
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Indirizzo
                                        </td>
                                        <td style="vertical-align:top">
                                            <b>
                                                <asp:Label ID="LabelIndirizzoDichiarato" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td style="vertical-align:top">
                                            <b>
                                                <asp:Label ID="LabelIndirizzoAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                            <br />
                                            <asp:Label ID="LabelIndirizzoAnagraficaValidità" runat="server" Visible="false" />
                                        </td>
                                        <td style="vertical-align:top">
                                            <asp:RadioButtonList ID="RadioButtonListIndirizzo" runat="server" RepeatDirection="Vertical"
                                                AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListIndirizzo_SelectedIndexChanged">
                                                <asp:ListItem>Dichiarato</asp:ListItem>
                                                <asp:ListItem>Anagrafica</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageControlloAnagraficaEMail" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            E-Mail diversa
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td width="20%">
                                            <b></b>
                                        </td>
                                        <td width="30%">
                                            <b>Dichiarato </b>
                                        </td>
                                        <td width="30%">
                                            <b>Anagrafica </b>
                                        </td>
                                        <td width="20%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            E Mail
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelEMailDichiarato" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelEMailAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageControlloAnagraficaCellulare" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            Cellulare diverso
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td width="20%">
                                            <b></b>
                                        </td>
                                        <td width="20%">
                                            <b>Dichiarato </b>
                                        </td>
                                        <td width="20%">
                                            <b>Anagrafica </b>
                                        </td>
                                        <td width="20%">
                                            <b>SMS SiceInfo </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cellulare
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCellulareDichiarato" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCellulareAnagrafica" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                                <asp:Label ID="LabelCellulareSMSSiceInfo" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxAderisceSMS" runat="server" Enabled="false" 
                                                Text="Aderisce servizio SMS" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <b>Altri dati</b>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="borderedTable">
                        <tr>
                            <td>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Image ID="ImageControlloCantiereNotifiche" runat="server" ImageUrl="~/images/semaforoGiallo.png" />
                                        </td>
                                        <td>
                                            Cantiere nelle Notifiche
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <asp:Label ID="LabelCantiereNotifiche" runat="server">Il cantiere non è presente nelle notifiche</asp:Label>
                                </b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewLavoratoreContrattiCessazione" runat="server"
        Visible="false">
        <table runat="server" id="table1" class="standardTable">
            <tr>
                <td>
                    <b>Rapporti di Lavoro</b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="ImageControlloRapportoCessazione" runat="server" ImageUrl="~/images/semaforoGiallo.gif" />
                    Rapporto di lavoro&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadGrid ID="RadGridRapportDiLavoroCessazione" runat="server" Width="100%"
                        GridLines="None" PageSize="5" OnItemDataBound="RadGridRapportoDiLavoroCessazione_ItemDataBound"
                        OnSelectedIndexChanged="RadGridRapportDiLavoroCessazione_SelectedIndexChanged">
                        <MasterTableView DataKeyNames="IdLavoratore, IdImpresa, DataFineValiditaRapporto">
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn HeaderText="Codice" UniqueName="Codice">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelLavoratoreCodice" runat="server">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="TipoRapportoLavoro" HeaderText="Fonte" UniqueName="column">
                                    <ItemTemplate>
                                        <asp:Label ID="LabelTipoRapportoLavoro" runat="server"></asp:Label>
                                        <br />
                                        <small>
                                            <asp:Label ID="LabelStato" runat="server"></asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore">
                                    <ItemTemplate>
                                        <b>
                                            <asp:Label ID="LabelLavoratoreCognomeNome" runat="server">
                                            </asp:Label>
                                        </b>
                                        <br />
                                        <small>
                                            <asp:Label ID="LabelLavoratoreDataNascita" runat="server">
                                            </asp:Label>
                                            <br />
                                            <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server">
                                            </asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                                    <ItemTemplate>
                                        <b>
                                            <asp:Label ID="LabelImpresaCodiceRagioneSociale" runat="server">
                                            </asp:Label>
                                        </b>
                                        <br />
                                        <small>
                                            <asp:Label ID="LabelImpresaPartitaIva" runat="server">
                                            </asp:Label>
                                            <br />
                                            <asp:Label ID="LabelImpresaCodiceFiscale" runat="server">
                                            </asp:Label>
                                        </small>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="Periodo">
                                    <ItemTemplate>
                                        <b>
                                            <asp:Label ID="LabelRapportoPeriodo" runat="server">
                                            </asp:Label>
                                        </b>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Select" Text="Seleziona"
                                    UniqueName="column">
                                    <ItemStyle Width="10px" />
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                        <ClientSettings>
                            <Selecting AllowRowSelect="False" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonAnnullaSelezioneRapporto" runat="server" OnClick="ButtonAnnullaSelezioneRapporto_Click"
                        Text="Annulla selezione" />
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
</telerik:RadMultiPage>
