﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IscrizioneLavoratoreInformazioniAggiuntive.ascx.cs"
    Inherits="IscrizioneLavoratori_IscrizioneLavoratoreInformazioniAggiuntive" %>
<style type="text/css">
    .style5
    {
        width: 270px;
    }
    .style6
    {
        width: 18px;
    }
</style>
<!--<asp:Panel ID="PanelInformazioniAggiuntive" runat="server">-->
<table class="borderedTable">
    <tr>
        <td colspan="3">
            <b>Informazioni bancarie </b>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
    <td colspan="3">
    <asp:Panel ID ="panelIban" runat ="server" Visible="false">
    <table>
    <tr>
        <td class="style5">
            IBAN:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxIBAN" runat="server" Width="250px" Rows="1" MaxLength="34">
            </telerik:RadTextBox>
        </td>
        <td class="style6">
            <asp:CustomValidator ID="CustomValidatorBancaIBAN" runat="server" ControlToValidate="RadTextBoxIBAN"
                ErrorMessage="Codice IBAN non corretto" ValidationGroup="infoAggiuntive" OnServerValidate="CustomValidatorBancaIBAN_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Esempio IBAN italiano
            <table style="color: Gray; padding-left: 50px;">
                <tr>
                    <td class="tdIban">
                        ITPPCAAAAABBBBBNNNNNNNNNNNN
                    </td>
                </tr>
                <tr>
                    <td class="tdIban">
                        <b>IT73J0615513000000000012345 </b>
                    </td>
                </tr>
            </table>
            <table style="color: Gray; padding-left: 50px; width: 260px;">
                <tr>
                    <td align="left">
                        IT
                    </td>
                    <td>
                        Codice paese (IT)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        PP
                    </td>
                    <td>
                        Cifra di controllo (73)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        C
                    </td>
                    <td>
                        CIN (J)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        AAAAA
                    </td>
                    <td>
                        ABI (06155)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        BBBBB
                    </td>
                    <td>
                        CAB (13000)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        NNNNNNNNNNNN
                    </td>
                    <td>
                        CONTO (000000012345)
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
        <td class="style6">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style5">
            Co-intestato:
        </td>
        <td>
            <asp:RadioButtonList ID="RadioButtonListCoIntestato" runat="server" RepeatDirection="Horizontal"
                AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListCoIntestato_SelectedIndexChanged">
                <asp:ListItem>SI</asp:ListItem>
                <asp:ListItem Selected="True">NO</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td class="style6">
            <asp:CustomValidator ID="CustomValidatorCointestato" runat="server" ErrorMessage="Può essere indicato il cointestatario solo se viene fornito l'IBAN"
                ValidationGroup="infoAggiuntive" OnServerValidate="CustomValidatorCointestato_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Cognome cointestatario:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCognomeCointestatario" runat="server" Width="250px"
                MaxLength="30" Rows="1" Enabled="False">
            </telerik:RadTextBox>
        </td>
        <td class="style6">
            <asp:CustomValidator ID="CustomValidatorCognomeCointestatario1" runat="server" ErrorMessage="Indicare il cognome del cointestatario"
                ValidationGroup="infoAggiuntive" OnServerValidate="CustomValidatorCognomeCointestatario1_ServerValidate">
                *
            </asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidatorCognomeCointestatario2" runat="server" ErrorMessage="Indicare che il conto corrente è cointestato"
                ValidationGroup="infoAggiuntive" OnServerValidate="CustomValidatorCognomeCointestatario2_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    </table>
    </asp:Panel>
    </td>
    </tr>
    <tr>
        <td class="style5">
            Richiesta informazioni carta prepagata:
        </td>
        <td>
            <asp:RadioButtonList ID="RadioButtonListInfoPrepagata" runat="server" RepeatDirection="Horizontal"
                AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListInfoPrepagata_SelectedIndexChanged" Enabled="false">
                <asp:ListItem>SI</asp:ListItem>
                <asp:ListItem Selected="True">NO</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Tipologia prepagata:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTipologiaPrepagata" runat="server" Width="250px"
                EmptyMessage="Selezionare la tipologia" MarkFirstMatch="true" EnableScreenBoundaryDetection="false"
                Enabled="False">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorTipoPrepagata" runat="server" ErrorMessage="Indicare la tipologia di carta prepagata"
                ValidationGroup="infoAggiuntive" OnServerValidate="CustomValidatorTipoPrepagata_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>
<br />
<table class="borderedTable">
    <tr>
        <td colspan="3">
            <b>Informazioni indumenti e calzature da lavoro </b>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style5">
            Taglia felpa/husky
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTagliaFelpaHusky" runat="server" Width="250px"
                MarkFirstMatch="true" AllowCustomText="false" EmptyMessage="Selezionare una taglia per la Felpa/Husky"
                EnableScreenBoundaryDetection="false">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorTagliaFelpaHusky" runat="server" ValidationGroup="infoAggiuntive"
                ErrorMessage="Selezionare la taglia Felpa/Husky" MarkFirstMatch="true" EnableScreenBoundaryDetection="false"
                OnServerValidate="CustomValidatorTagliaFelpaHusky_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Taglia pantaloni:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTagliaPantaloni" runat="server" Width="250px"
                AllowCustomText="false" MarkFirstMatch="true" EnableScreenBoundaryDetection="false"
                EmptyMessage="Selezionare una taglia per i Pantaloni">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorTagliaPantaloni" runat="server" ValidationGroup="infoAggiuntive"
                ErrorMessage="Selezionare la taglia pantaloni" OnServerValidate="CustomValidatorTagliaPantaloni_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Taglia scarpe:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTagliaScarpe" runat="server" Width="250px" EnableScreenBoundaryDetection="false"
                MarkFirstMatch="true" AllowCustomText="false" EmptyMessage="Selezionare una taglia per le Scarpe">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorTagliaScarpe" runat="server" ValidationGroup="infoAggiuntive"
                ErrorMessage="Selezionare la taglia scarpe" OnServerValidate="CustomValidatorTagliaScarpe_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>
<br />
<table runat="server" width="100%" class="borderedTable" visible="false">
    <tr>
        <td colspan="3">
            <b>Informazioni sindacato </b>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style5">
            Sindacato:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxSindacato" runat="server" Width="250px" Filter="StartsWith"
                MarkFirstMatch="true" AllowCustomText="true" EmptyMessage="Selezionare un Sindacato">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Data adesione:
        </td>
        <td>
            <telerik:RadDateInput ID="RadDateInputDataAdesione" runat="server" Width="250px">
            </telerik:RadDateInput>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Data disdetta:
        </td>
        <td>
            <telerik:RadDateInput ID="RadDateInputDataDisdetta" runat="server" Width="250px">
            </telerik:RadDateInput>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Numero delega:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNumeroDelega" runat="server" Width="250px">
            </telerik:RadTextBox>
        </td>
    </tr>
</table>
<asp:Panel ID="PanelIscrizioneCorsi" runat="server">
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Altre informazioni </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style5">
            Richiesta iscrizione corsi 16 ore:
        </td>
        <td>
            <asp:RadioButtonList ID="RadioButtonListIscrizioneCorsi16Ore" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem>SI</asp:ListItem>
                <asp:ListItem Selected="True">NO</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="style5">
            Note:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNote" runat="server" Width="250px" MaxLength="100">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
        </td>
    </tr>
</table>
</asp:Panel>
<!--</asp:Panel>-->
