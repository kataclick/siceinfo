using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using Impresa = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using SintesiBusiness = TBridge.Cemi.Sintesi.Business.SintesiBusiness;

public partial class IscrizioneLavoratori_IscrizioneLavoratoreRapportoDiLavoro : UserControl
{
    private const String ValidationGroup = "rapportoDiLavoro";
    private readonly Common _commonBiz = new Common();
    private readonly IscrizioneLavoratoriManager _iscrizioneBiz = new IscrizioneLavoratoriManager();
    private readonly SintesiBusiness _sintesiBiz = new SintesiBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        LabelDataAssunzione.Text =
            String.Format(
                "La data di inizio rapporto deve essere compresa tra il {0} e il {1}, la data di assunzione deve essere inferiore al {1}",
                DateTime.Today.AddMonths(-1).ToShortDateString(),
                DateTime.Today.AddMonths(1).AddDays(1).ToShortDateString());

        RadDateInputDataInizioRapportoLavoro.MinDate = DateTime.Today.AddMonths(-1).AddDays(-1);
        RangeValidatorDataInizioRapportoLavoro.MinimumValue =
            DateTime.Today.AddMonths(-1).AddDays(-1).ToString("yyyy-MM-dd-00-00-00");

        RadDateInputDataInizioRapportoLavoro.MaxDate = DateTime.Today.AddMonths(1).AddDays(1);
        RangeValidatorDataInizioRapportoLavoro.MaximumValue =
            DateTime.Today.AddMonths(1).AddDays(1).ToString("yyyy-MM-dd-00-00-00");


        RadDateInputDataAssunzione.MinDate = new DateTime(1945, 1, 1).AddDays(-1);
        //RangeValidatorDataAssunzione.MinimumValue = DateTime.Today.AddMonths(-1).ToString("yyyy-MM-dd-00-00-00");
        RangeValidatorDataAssunzione.MinimumValue = new DateTime(1945, 1, 1).AddDays(-1).ToString("yyyy-MM-dd-00-00-00");

        RadDateInputDataAssunzione.MaxDate = DateTime.Today.AddMonths(1).AddDays(1);
        RangeValidatorDataAssunzione.MaximumValue =
            DateTime.Today.AddMonths(1).AddDays(1).ToString("yyyy-MM-dd-00-00-00");


        if (!Page.IsPostBack)
        {
            RadTextBoxDataInvio.Text = DateTime.Now.ToString("dd/MM/yyyy");
            RadDatePickerDataFineRapporto.Visible = false;
            LabelDataFineRapporto.Visible = false;
            CaricaCombo();
        }
    }

    private void ResetCompleto()
    {
        //AbilitazioneValidator(TipoEventoSelezionato.Assunzione);

        //Presenter.SvuotaCampo(RadDateInputDataInizioRapportoLavoro);
        //Presenter.SvuotaCampo(RadDateInputDataAssunzione);
        RadComboBoxTipologiaContrattuale.ClearSelection();
        RadComboBoxTipologiaInizioRapporto.ClearSelection();
        Presenter.SvuotaCampo(RadTextBoxOrarioMedioSettimanale);
        //RadComboBoxContrattoCCNLApplicato.ClearSelection();
        Presenter.SvuotaCampo(RadTextBoxLivelloInquadramento);
        //RadComboBoxQualificaISTAT.ClearSelection();

        ResetDatiAttivita();
    }

    private void ResetDatiAttivita()
    {
        // Cessazione
        //Presenter.SvuotaCampo(RadDateInputDataCessazione);
        RadComboBoxCausa.ClearSelection();

        // Trasformazione
        //Presenter.SvuotaCampo(RadDateInputDataTrasformazione);
        RadComboBoxCodiceTrasformazione.ClearSelection();
    }

    public void CaricaTipoAttivita(TipoAttivita tipoAttivita)
    {
        ResetCompleto();

        switch (tipoAttivita)
        {
            case TipoAttivita.Assunzione:
                AbilitazioneValidator(tipoAttivita);
                PanelDatiRapporto.Enabled = true;
                break;
            default:
                AbilitazioneValidator(tipoAttivita);
                PanelDatiRapporto.Enabled = false;
                break;
        }

        RadMultiPageAttivita.SelectedIndex = ((Int32) tipoAttivita) - 1;
    }

    public void CaricaTipoContratto(Impresa impresa)
    {
        RadComboBoxTipologiaContrattuale.Enabled = true;

        if (!String.IsNullOrEmpty(impresa.CodiceContratto))
        {
            // Artigiano
            if (impresa.CodiceContratto == "02")
            {
                CaricaTipiContrattoPerArtigiano();
            }
            else
            {
                CaricaTipiContratto(true);
                RadComboBoxTipologiaContrattuale.Enabled = false;
                RadComboBoxTipologiaContrattuale.SelectedValue = impresa.CodiceContratto;
                CaricaTipiCategoria(impresa.CodiceContratto);
            }
        }
    }

    public RapportoDiLavoro CaricaDatiLavoratore(Int32 idLavoratore, Int32 idImpresa)
    {
        CaricaCombo();
        ResetCompleto();
        RapportoDiLavoro rapporto = _iscrizioneBiz.GetRapportoDiLavoro(idLavoratore, idImpresa);

        if (rapporto != null)
        {
            if (rapporto.DataAssunzione.HasValue)
            {
                RadDateInputDataAssunzione.MinDate = rapporto.DataAssunzione.Value;
                RadDateInputDataAssunzione.SelectedDate = rapporto.DataAssunzione.Value;
            }

            if (rapporto.DataInizioValiditaRapporto.HasValue)
            {
                RadDateInputDataInizioRapportoLavoro.MinDate = rapporto.DataInizioValiditaRapporto.Value;
                RadDateInputDataInizioRapportoLavoro.SelectedDate = rapporto.DataInizioValiditaRapporto.Value;
            }
            if (rapporto.Contratto != null)
            {
                RadComboBoxTipologiaContrattuale.SelectedValue = rapporto.Contratto.IdContratto;
            }
            else
            {
                RadComboBoxTipologiaContrattuale.Text = " ";
            }

            if (rapporto.Mansione != null)
            {
                RadComboBoxMansione.SelectedValue = rapporto.Mansione.IdMansione;
            }
            else
            {
                RadComboBoxMansione.Text = " ";
            }

            if (rapporto.PercentualePartTime != null)
            {
                RadTextBoxOrarioMedioSettimanale.Text = rapporto.PercentualePartTime.Value.ToString();
            }
            if (rapporto.TipoInizioRapporto != null)
            {
                RadComboBoxTipologiaInizioRapporto.SelectedValue = rapporto.TipoInizioRapporto.IdTipoInizioRapporto;
                if (rapporto.TipoInizioRapporto.IdTipoInizioRapporto.Equals("3"))
                    rapporto.PartTime = true;
            }
            else
            {
                RadComboBoxTipologiaInizioRapporto.SelectedIndex = 0;
            }
            if (rapporto.PartTime)
            {
                RadioButtonListPartTime.SelectedIndex = 0;
            }
            else
            {
                RadioButtonListPartTime.SelectedIndex = 1;
            }
            if (!String.IsNullOrEmpty(rapporto.LivelloInquadramento))
            {
                RadTextBoxLivelloInquadramento.Text = rapporto.LivelloInquadramento;
            }

            if (rapporto.Categoria != null)
            {
                RadComboBoxCategoria.SelectedValue = rapporto.Categoria.IdCategoria;
            }
            else
            {
                RadComboBoxCategoria.Text = " ";
            }
            if (rapporto.Qualifica != null)
            {
                RadComboBoxQualifica.SelectedValue = rapporto.Qualifica.IdQualifica;
            }
            else
            {
                RadComboBoxQualifica.Text = " ";
            }
        }

        return rapporto;
    }

    public RapportoDiLavoro GetRapportoDiLavoro()
    {
        RapportoDiLavoro rapporto = new RapportoDiLavoro();

        if (RadDateInputDataAssunzione.SelectedDate != null)
        {
            rapporto.DataAssunzione = RadDateInputDataAssunzione.SelectedDate;
        }
        if (RadDateInputDataInizioRapportoLavoro.SelectedDate != null)
        {
            rapporto.DataInizioValiditaRapporto = RadDateInputDataInizioRapportoLavoro.SelectedDate;
        }

        rapporto.DataFineValiditaRapporto = RadDatePickerDataFineRapporto.SelectedDate;

        if (!String.IsNullOrEmpty(RadComboBoxTipologiaContrattuale.SelectedValue))
        {
            rapporto.Contratto = new TipoContratto();
            rapporto.Contratto.IdContratto = RadComboBoxTipologiaContrattuale.SelectedValue;
            rapporto.Contratto.Descrizione = RadComboBoxTipologiaContrattuale.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxCategoria.SelectedValue))
        {
            rapporto.Categoria = new TipoCategoria();
            rapporto.Categoria.IdCategoria = RadComboBoxCategoria.SelectedValue;
            rapporto.Categoria.Descrizione = RadComboBoxCategoria.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxQualifica.SelectedValue))
        {
            rapporto.Qualifica = new TipoQualifica();
            rapporto.Qualifica.IdQualifica = RadComboBoxQualifica.SelectedValue;
            rapporto.Qualifica.Descrizione = RadComboBoxQualifica.Text;
        }
        if (!String.IsNullOrEmpty(RadComboBoxMansione.SelectedValue))
        {
            rapporto.Mansione = new TipoMansione();
            rapporto.Mansione.IdMansione = RadComboBoxMansione.SelectedValue;
            rapporto.Mansione.Descrizione = RadComboBoxMansione.Text;
        }
        if (RadDateInputDataCessazione.SelectedDate != null)
        {
            rapporto.DataCessazione = RadDateInputDataCessazione.SelectedDate;
        }
        if (RadDateInputDataTrasformazione.SelectedDate != null)
        {
            rapporto.DataTrasformazione = RadDateInputDataTrasformazione.SelectedDate;
        }
        if (!String.IsNullOrEmpty(RadComboBoxCausa.SelectedValue))
        {
            rapporto.TipoFineRapporto = new TipoFineRapporto();
            rapporto.TipoFineRapporto.IdTipoFineRapporto = RadComboBoxCausa.SelectedValue;
            rapporto.TipoFineRapporto.Descrizione = RadComboBoxCausa.Text;
        }

        if (!String.IsNullOrEmpty(RadComboBoxTipologiaInizioRapporto.SelectedValue))
        {
            rapporto.TipoInizioRapporto = new TipoInizioRapporto();
            rapporto.TipoInizioRapporto.IdTipoInizioRapporto = RadComboBoxTipologiaInizioRapporto.SelectedValue;
            rapporto.TipoInizioRapporto.Descrizione = RadComboBoxTipologiaInizioRapporto.Text;
        }

        rapporto.PartTime = (RadioButtonListPartTime.SelectedIndex == 0);

        if (!String.IsNullOrEmpty(RadTextBoxOrarioMedioSettimanale.Text))
        {
            rapporto.PercentualePartTime = Decimal.Parse(RadTextBoxOrarioMedioSettimanale.Text);
        }

        if (!String.IsNullOrEmpty(RadTextBoxLivelloInquadramento.Text))
        {
            rapporto.LivelloInquadramento = RadTextBoxLivelloInquadramento.Text;
        }

        return rapporto;
    }

    private void AbilitazioneValidator(TipoAttivita evento)
    {
        switch (evento)
        {
            case TipoAttivita.Assunzione:
                foreach (BaseValidator validator in Page.Validators)
                {
                    if (validator.ValidationGroup == ValidationGroup)
                    {
                        if (validator.ID != "RequiredFieldValidatorDataCessazione"
                            && validator.ID != "RangeValidatorDataCessazione"
                            && validator.ID != "RequiredFieldValidatorCausa"
                            && validator.ID != "CustomValidatorDataCessazione"
                            && validator.ID != "RequiredFieldValidatorDataTrasformazione")
                        {
                            validator.Enabled = true;
                        }
                        else
                        {
                            validator.Enabled = false;
                        }
                    }
                }
                break;
            case TipoAttivita.Cessazione:
                foreach (BaseValidator validator in Page.Validators)
                {
                    if (validator.ValidationGroup == ValidationGroup)
                    {
                        if (validator.ID == "RequiredFieldValidatorDataCessazione"
                            || validator.ID == "RangeValidatorDataCessazione"
                            || validator.ID == "RequiredFieldValidatorCausa"
                            || validator.ID == "CustomValidatorDataCessazione")
                        {
                            validator.Enabled = true;
                        }
                        else
                        {
                            validator.Enabled = false;
                        }
                    }
                }
                break;
        }
    }

    protected void RadComboBoxTipologiaInizioRapporto_SelectedIndexChanged(object o,
                                                                           RadComboBoxSelectedIndexChangedEventArgs e)
    {
        LabelDataFineRapporto.Visible = false;
        RadDatePickerDataFineRapporto.Visible = false;
        RadDatePickerDataFineRapporto.SelectedDate = null;
        CompareValidatorDataFineRapporto.Enabled = false;
        RequiredFieldValidatorDataFineRapporto.Enabled = false;
        switch (RadComboBoxTipologiaInizioRapporto.SelectedValue)
        {
            default:
                RadioButtonListPartTime.Enabled = true;
                break;
            case "3":
                RadioButtonListPartTime.SelectedIndex = 0;
                RadioButtonListPartTime.Enabled = false;
                GestisciAbilitazionePercentualePartTime();
                break;
            case "2":
                RadioButtonListPartTime.Enabled = true;
                LabelDataFineRapporto.Visible = true;
                RadDatePickerDataFineRapporto.Visible = true;
                CompareValidatorDataFineRapporto.Enabled = true;
                RequiredFieldValidatorDataFineRapporto.Enabled = true;
                break;
        }
    }

    protected void RadioButtonListPartTime_SelectedIndexChanged(object sender, EventArgs e)
    {
        GestisciAbilitazionePercentualePartTime();
    }

    private void GestisciAbilitazionePercentualePartTime()
    {
        if (RadioButtonListPartTime.SelectedValue == "SI")
        {
            RadTextBoxOrarioMedioSettimanale.Enabled = true;
        }
        else
        {
            RadTextBoxOrarioMedioSettimanale.Text = null;
            RadTextBoxOrarioMedioSettimanale.Enabled = false;
        }
    }

    protected void RadComboBoxCategoria_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        CaricaTipiQualifica(RadComboBoxCategoria.SelectedValue);
    }

    protected void RadComboBoxTipologiaContrattuale_SelectedIndexChanged(object o,
                                                                         RadComboBoxSelectedIndexChangedEventArgs e)
    {
        CaricaTipiCategoria(RadComboBoxTipologiaContrattuale.SelectedValue);
    }

    #region Custom Validators

    protected void CustomValidatorPercentualePartTime_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadioButtonListPartTime.SelectedIndex == 0)
        {
            if (RadTextBoxOrarioMedioSettimanale.Text == string.Empty)
                args.IsValid = false;
        }
    }

    protected void CustomValidatorDataAssunzione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDateInputDataAssunzione.SelectedDate.HasValue
            && RadDateInputDataInizioRapportoLavoro.SelectedDate.HasValue
            && RadDateInputDataAssunzione.SelectedDate > RadDateInputDataInizioRapportoLavoro.SelectedDate)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorTipologiaContrattuale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxTipologiaContrattuale.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorTipologiaCategoria_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxCategoria.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorQualifica_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxQualifica.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorMansione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        if (RadComboBoxMansione.SelectedIndex > 0)
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorDataCessazione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (RadDateInputDataInizioRapportoLavoro.SelectedDate != null
            && RadDateInputDataCessazione.SelectedDate != null)
        {
            if (RadDateInputDataCessazione.SelectedDate < RadDateInputDataInizioRapportoLavoro.SelectedDate)
            {
                args.IsValid = false;
            }
        }
    }

    #endregion

    #region Caricamento ComboBox

    private void CaricaCombo()
    {
        //CaricaTipiContrattoSintesi();
        CaricaTipiInizioRapporto();
        //CaricaCCNL();
        CaricaTipiTrasformazione();
        CaricaTipiCessazione();

        CaricaTipiContratto(false);
        //CaricaTipiQualifica();
        CaricaTipiMansione();
        CaricaTipiCategoria(RadComboBoxTipologiaContrattuale.SelectedValue);
    }

    private void CaricaTipiCategoria(String tipoContratto)
    {
        //if (RadComboBoxCategoria.Items.Count == 0)
        //{

        RadComboBoxCategoria.Items.Clear();

        Presenter.CaricaElementiInDropDown(
            RadComboBoxCategoria,
            _sintesiBiz.GetTipiCategoria(tipoContratto),
            "Descrizione",
            "IdCategoria");
        RadComboBoxCategoria.Items.Insert(0, new RadComboBoxItem("- Selezionare la categoria -"));

        RadComboBoxCategoria.SelectedIndex = 0;
        //}
    }

    private void CaricaTipiQualifica(String tipoCategoria)
    {
        //if (RadComboBoxQualifica.Items.Count == 0)
        //{

        RadComboBoxQualifica.Items.Clear();

        Presenter.CaricaElementiInDropDown(
            RadComboBoxQualifica,
            _commonBiz.GetTipiQualifica(tipoCategoria),
            "Descrizione",
            "IdQualifica");
        RadComboBoxQualifica.Items.Insert(0, new RadComboBoxItem("- Selezionare la qualifica -"));
        RadComboBoxQualifica.SelectedIndex = 0;

        //}
    }

    private void CaricaTipiMansione()
    {
        if (RadComboBoxMansione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxMansione,
                _commonBiz.GetTipiMansione(),
                "Descrizione",
                "IdMansione");
            RadComboBoxMansione.Items.Insert(0, new RadComboBoxItem("- Selezionare la mansione -"));
        }
    }

    private void CaricaTipiCessazione()
    {
        if (RadComboBoxCausa.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCausa,
                _commonBiz.GetTipiFineRapporto(),
                "Descrizione",
                "IdTipoFineRapporto");
            RadComboBoxCausa.Items.Insert(0, new RadComboBoxItem("- Selezionare la causa -"));
        }
    }

    private void CaricaTipiTrasformazione()
    {
        if (RadComboBoxCodiceTrasformazione.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxCodiceTrasformazione,
                _sintesiBiz.GetTrasformazioniRL(),
                "descrizione",
                "codice");
            RadComboBoxCodiceTrasformazione.Items.Insert(0,
                                                         new RadComboBoxItem("- Selezionare il codice trasformazione -"));
        }
    }

/*
    private void CaricaCCNL()
    {
        if (RadComboBoxTipologiaContrattuale.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipologiaContrattuale,
                _sintesiBiz.GetCCNL(),
                "descrizione",
                "codice");
        }
    }
*/

    private void CaricaTipiInizioRapporto()
    {
        if (RadComboBoxTipologiaInizioRapporto.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipologiaInizioRapporto,
                _commonBiz.GetTipiInizioRapporto(),
                "Descrizione",
                "IdTipoInizioRapporto");
            RadComboBoxTipologiaInizioRapporto.Items.Insert(0, new RadComboBoxItem("- Selezionare la tipologia -"));
        }
    }

/*
    private void CaricaTipiContrattoSintesi()
    {
        if (RadComboBoxTipologiaContrattuale.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipologiaContrattuale,
                _sintesiBiz.GetTipiContratto(),
                "descrizione",
                "codice");
        }
    }
*/

    private void CaricaTipiContratto(Boolean forzatura)
    {
        if (forzatura || RadComboBoxTipologiaContrattuale.Items.Count == 0)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxTipologiaContrattuale,
                _commonBiz.GetTipiContratto(),
                "Descrizione",
                "IdContratto");
            RadComboBoxTipologiaContrattuale.Items.Insert(0,
                                                          new RadComboBoxItem(
                                                              "- Selezionare la tipologia contrattuale -"));
        }
    }

    private void CaricaTipiContrattoPerArtigiano()
    {
        TipoContrattoCollection tipiContratto = _commonBiz.GetTipiContratto();
        RadComboBoxTipologiaContrattuale.Items.Clear();

        foreach (TipoContratto tipoContratto in tipiContratto)
        {
            if (tipoContratto.IdContratto == "01"
                || tipoContratto.IdContratto == "02")
            {
                RadComboBoxTipologiaContrattuale.Items.Add(
                    new RadComboBoxItem(tipoContratto.Descrizione, tipoContratto.IdContratto));
            }
        }
        RadComboBoxTipologiaContrattuale.Items.Insert(0,
                                                      new RadComboBoxItem("- Selezionare la tipologia contrattuale -"));
    }

    #endregion
}