<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneConsulente.aspx.cs" Inherits="GestioneUtentiRegistrazioneConsulente" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/GestioneRegistrazione.ascx" TagName="GestioneRegistrazione"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RegistrazioneConsulente.ascx" TagName="RegistrazioneConsulente"
    TagPrefix="uc3" %>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Registrazione consulente"
        titolo="Gestione utenti" />
    <br />
    <uc2:GestioneRegistrazione ID="GestioneRegistrazione1" runat="server" Visible="false" />
    <br />
    <uc3:RegistrazioneConsulente id="RegistrazioneConsulente1" runat="server">
    </uc3:RegistrazioneConsulente>
</asp:Content>

