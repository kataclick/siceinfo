﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InformativaPrivacy.aspx.cs" Inherits="InformativaPrivacy" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <style type="text/css">
    .style1
    {
        text-decoration: underline;
    }
</style>
<p><img id="Img1" alt="header" src="images/banner1024.jpg" runat="server" /></p>
<p>
    <strong>Informativa cookie
     </strong>
    <br />
    In questa pagina si descrivono le modalità di gestione del sito con riferimento all’utilizzo dei cookie e al relativo trattamento dei dati personali degli utenti che lo consultano. 
</p>
<p>
    I cookie sono piccoli file di testo che i siti visitati inviano al terminale (desktop, tablet, smartphone, notebook) dell&#39;utente, dove vengono memorizzati, per poi essere ritrasmessi agli stessi siti alla visita successiva. Sono usati per eseguire autenticazioni informatiche, monitoraggio di sessioni e memorizzazione di informazioni sui siti (senza l&#39;uso dei cookie &quot;tecnici&quot; alcune operazioni risulterebbero molto complesse o impossibili da eseguire). Nel corso della navigazione su un sito, l&#39;utente può ricevere sul suo terminale anche cookie che sono inviati da siti o da web server diversi (c.d. &quot;terze parti&quot;), sui quali possono risiedere alcuni elementi (quali, ad esempio, immagini, mappe, suoni, specifici link a pagine di altri domini) presenti sul sito che lo stesso sta visitando. 
</p>
<p>
    I 
    cookie che utilizziamo sono suddivisi nelle seguenti categorie: 
</p>
<p>
    <strong>Cookie tecnici</strong><br />
    I cookie tecnici permettono un agevole utilizzo del sito ed un più facile reperimento delle informazioni, semplificando la connessione e le trasmissioni di dati tra utente e sito. In particolare, utilizziamo cookie di navigazione o di sessione, che garantiscono la normale navigazione e fruizione del sito web permettendo, ad esempio, di autenticarsi per accedere ad aree riservate. Ti informiamo che, in ogni momento, puoi autorizzare, limitare o bloccare i cookie attraverso le impostazioni del browser, tuttavia, se imposterai il tuo dispositivo in modo da rifiutare questi cookie, alcuni servizi del sito potrebbero non essere visualizzati correttamente o funzionare in maniera non ottimale. In particolare, le operazioni che consentono di identificare l&#39;utente e mantenerne l&#39;identificazione nell&#39;ambito della sessione potrebbero essere più complesse da svolgere e meno sicure in 
    assenza di cookie tecnici. 
</p>
<p>
    <strong>Cookie analytics</strong><br />
    Usiamo cookie analytics persistenti, di terze parti (es. Google Analytics), per raccogliere informazioni sull’utilizzo del nostro sito. Tali cookie, assimilabili ai cookie tecnici, sono trattati in forma aggregata per monitorare le aree del sito maggiormente visitate, migliorare i contenuti del sito, facilitarne l’uso da parte dell’utente e monitorare il corretto funzionamento del sito. Ti informiamo che, in ogni momento, puoi autorizzare, limitare o bloccare i cookie attraverso le impostazioni del browser. 
</p>
<p>
    I partner sotto elencati utilizzano cookie sul nostro sito web con la finalità di migliorare i nostri prodotti.<br />
    Google: <a href="http://www.google.com/policies/privacy/" target="_blank">http://www.google.com/policies/privacy/</a>
</p>
        <p>
            Per maggiori informazioni si rimanda al provvedimento del Garante per la protezione dei dati personali dell’8 maggio 2014 &quot;Individuazione delle modalità semplificate per l’informativa e l’acquisizione del consenso per l’uso dei cookie&quot;<br />
            <a href="http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3118884">http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3118884</a></p>
<p>
    <strong>I cookie e le impostazioni del browser </strong>
    <br />
    In questa sezione trovi le informazioni per disattivare i cookie sul tuo browser. Ti ricordiamo che disattivando i cookie alcune parti del 
    sito potrebbero non funzionare correttamente. Se il tuo browser non si trova nell&#39;elenco sotto riportato, ti preghiamo di consultare le istruzione riportate sul tuo browser in merito alla gestione dei cookie. 
</p>
<p class="style1">
    Internet Explorer versione 6 o superiore:</p>
<ul>
    <li>Seleziona “Strumenti” nella barra del tuo browser </li>
    <li>Seleziona “Opzioni Internet” </li>
    <li>Seleziona la voce “Privacy” e poi clicca su “Avanzate” </li>
    <li>Seleziona “Sostituisci gestione automatica cookie” </li>
    <li>Disattiva i “Cookie dei siti Web visualizzati” selezionando la voce “Blocca” </li>
    <li>Disattiva i “Cookie di terze parti” selezionando la voce “Blocca” </li>
    <li>Disattiva i “Cookie di sessione” deselezionando la voce “Accetta sempre i cookie della sessione” </li>
    <li>Clicca su “OK” </li>
</ul>
<p class="style1">
    Firefox versione 9 o superiore 
</p>
<ul>
    <li>Seleziona “Strumenti” nella barra del tuo browser </li>
    <li>Seleziona “Opzioni” </li>
    <li>Selezione la voce “Privacy” </li>
    <li>Nell&#39;area “Cronologia” scegli dal menù a tendina l&#39;opzione “utilizza impostazioni personalizzate” </li>
    <li>Disattiva i cookies deselezionando la voce “Accetta i 
    cookie dai siti” </li>
    <li>Clicca su “OK” </li>
</ul>
<p style="text-decoration: underline">
    Google Chrome versione 24 o superiore 
</p>
<ul>
    <li>Seleziona “Menù Chrome” nella barra del tuo browser </li>
    <li>Selezione “impostazioni” </li>
    <li>Seleziona “Mostra impostazione avanzate” </li>
    <li>Nella sezione “Privacy” clicca su “Impostazione contenuti” </li>
    <li>Disattiva tutti i cookies selezionando “Impedisci ai siti di impostare dati” e “Blocca cookie di terze parti e dati dei siti” </li>
    <li>Clicca su “OK” </li>
</ul>
    </div>
    </form>
</body>
</html>
