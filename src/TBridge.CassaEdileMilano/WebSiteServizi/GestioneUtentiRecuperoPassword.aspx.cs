using System;
using System.Drawing;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Configuration;

public partial class GestioneUtentiRecuperoPassword : Page
{
    private readonly GestioneUtentiBiz _gestioneUtentiBiz =
        new GestioneUtentiBiz();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Label
        //LabelResponse.Text = string.Empty;
        Master.FindControl("LoginMain").Visible = false;
    }

    protected void ButtonConferma_Click(object sender, EventArgs e)
    {
        Page.Validate("cambioPassword");

        if (Page.IsValid)
        {
            Int32? idUtente;
            bool ret = _gestioneUtentiBiz.ResetPassword(TextBoxLogin.Text, TextBoxPIN.Text, TextBoxNuovaPassword.Text, out idUtente);

            if (ret)
            {
                if (idUtente.HasValue)
                {
                    int numeroMesi = Convert.ToInt32(ConfigurationManager.AppSettings["MesiValiditaPassword"]);
                    new UtentiManager().AggiornaScadenzaPassword(idUtente.Value, numeroMesi);
                }

                PanelDati.Visible = false;
                //PanelLogin.Visible = true;
                LabelResponse.Visible = true;
                LabelResponse.ForeColor = Color.Black;
                LabelResponse.Text =
                    "Modifica della password effettuata con successo. Ricordarsi username e password per poter accedere al sito e ai servizi offerti.";
                //LabelLogin.Text = TextBoxLogin.Text;
                //LabelPassword.Text = TextBoxNuovaPassword.Text;
            }

            else
            {
                PanelDati.Visible = true;
                //PanelLogin.Visible = false;
                LabelResponse.Visible = true;
                //LabelLogin.Text = string.Empty;
                //LabelPassword.Text = string.Empty;
                LabelResponse.Text = "Impossibile effettuare l'operazione, controllare Username e PIN inseriti.";
            }
        }
    }
}