using System;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class EstrattoContoLavoratore : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoLavoratore, "HomeLavoratore.aspx");

        if (GestioneUtentiBiz.IsLavoratore())
        {
            Utente utente = (Utente) Membership.GetUser();
            TitoloSottotitolo1.sottoTitolo = String.Format("Benvenuto {0}", utente.UserName);
        }
    }
}