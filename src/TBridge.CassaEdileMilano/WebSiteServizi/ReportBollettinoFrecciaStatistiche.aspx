﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportBollettinoFrecciaStatistiche.aspx.cs" Inherits="ReportBollettinoFrecciaStatistiche" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Statistiche bollettino freccia"
        titolo="BollettinoFreccia" />
    <br />
    <div>
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelDataRicercaDa" runat="server" Text="Data ricerca da (gg/mm/aaaa)"
                        Font-Names="Tahoma" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataRicercaDa" runat="server" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBoxDataRicercaDa"
                        ErrorMessage="Data ricerca da: formato non corretto." Operator="DataTypeCheck"
                        Type="Date" ValidationGroup="bollettino">*</asp:CompareValidator>
                </td>
                <td>
                    <asp:Label ID="LabelDataRicercaA" runat="server" Text="Data ricerca a (gg/mm/aaaa)"
                        Font-Names="Tahoma" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataRicercaA" runat="server" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Data ricerca a: formato non corretto."
                        Operator="DataTypeCheck" Type="Date" ControlToValidate="TextBoxDataRicercaA"
                        ValidationGroup="bollettino">*</asp:CompareValidator>
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToCompare="TextBoxDataRicercaDa"
                        ControlToValidate="TextBoxDataRicercaA" ErrorMessage="Data ricerca a minore di data ricerca da"
                        Operator="GreaterThanEqual" ValidationGroup="bollettino">*</asp:CompareValidator>
                </td>
            </tr>
            </table>
        <table class="standardTable">
            <tr>
                <td width="100">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="bollettino"
                        Font-Names="Tahoma"  Width="300px" 
                        CssClass="messaggiErrore" />
                    <br />
                    <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                        Text="Visualizza" />
                </td>
            </tr>
        </table>
        <br />
        <table height="600px" width="820px"><tr><td>
        <rsweb:ReportViewer ID="ReportViewerBollettinoFrecciaStatistiche" runat="server"
            ProcessingMode="Remote" Width="820px" ShowExportControls="True" ShowFindControls="False"
            ShowPrintButton="False" ShowRefreshButton="False" ShowToolBar="True" 
            ShowZoomControl="False" 
            oninit="ReportViewerBollettinoFrecciaStatistiche_Init" 
            ShowDocumentMapButton="False" Visible="False">
        </rsweb:ReportViewer>
        </td></tr></table>
    </div>
    <br />
</asp:Content>
