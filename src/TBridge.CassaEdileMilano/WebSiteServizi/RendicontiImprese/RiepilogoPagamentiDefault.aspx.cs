﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class RendicontiImprese_RiepilogoPagamentiDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoImpresa);
        #endregion

        ConsulenteSelezioneImpresa1.OnImpresaSelected += new TBridge.Cemi.Type.Delegates.ImpresaSelectedEventHandler(ConsulenteSelezioneImpresa1_OnImpresaSelected);

        if (!Page.IsPostBack)
        {
            if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            {
                Int32 idImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
                if (idImpresa > 0)
                {
                    ViewState["IdImpresa"] = idImpresa;
                    MenuRiepilogoPagamenti1.SetIdImpresa(idImpresa);
                }
            }

            if (!TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            {
                ConsulenteSelezioneImpresa1.Visible = false;
            }
        }
    }

    void ConsulenteSelezioneImpresa1_OnImpresaSelected(int idImpresa, string codiceRagioneSociale)
    {
        ViewState["IdImpresa"] = idImpresa;
        MenuRiepilogoPagamenti1.SetIdImpresa(idImpresa);
        //ButtonGeneraPerConsulente.Enabled = true;
    }
}