﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuRiepilogoPagamenti.ascx.cs" Inherits="RendicontiImprese_WebControls_MenuRiepilogoPagamenti" ViewStateMode="Enabled" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.InfoImpresa))
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>
            Riepilogo pagamenti
        </td>
    </tr>
    <tr>
        <td>
            <asp:LinkButton
                ID="LinkButtonCartella" 
                runat="server"
                Text="Cartella" onclick="LinkButtonCartella_Click">
            </asp:LinkButton>
            <%--<a id="A1" href="ReportImprese.aspx?tipo=cartella">Cartella</a>--%>
        </td>
    </tr>
    <tr>
        <td>
            <asp:LinkButton
                ID="LinkButtonPremioFedelta" 
                runat="server"
                Text="Premio fedeltà" onclick="LinkButtonPremioFedelta_Click">
            </asp:LinkButton>
            <%--<a id="A2" href="ReportImprese.aspx?tipo=premioFedelta">Premio fedeltà</a>--%>
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa())
        {
    %>
    <tr>
        <td>
            <a id="A3" href="ReportImprese.aspx?tipo=trattamentoCIGO">Prestazione sociale trattamento C.I.G.O.</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>