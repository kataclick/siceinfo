﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuOreAccantonate.ascx.cs"
    Inherits="RendicontiImprese_WebControls_MenuOreAccantonate" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.InfoImpresa) && GestioneUtentiBiz.IsImpresa())
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>
            Ore accantonate
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa())
        {
    %>
    <tr>
        <td>
            <a id="A1" href="ReportImprese.aspx?tipo=orePeriodo">Per periodo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="ReportImprese.aspx?tipo=oreLavoratori">Per lavoratore</a>
        </td>
    </tr>
    <%
        }
    %>
    <!--<tr>
        <td>
            <asp:LinkButton
                ID="A3" 
                runat="server"
                Text="EVR" 
                onclick="A3_Click">
            </asp:LinkButton>
            <%--<a id="A3" href="ReportImprese.aspx?tipo=evr" runat="server">EVR</a>--%>
        </td>
    </tr>-->
</table>
<%
    }
%>