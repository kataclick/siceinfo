﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ReportImprese.aspx.cs" Inherits="RendicontiImprese_ReportImprese" EnableEventValidation="false" %>

<%@ Register Src="~/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/MenuOreAccantonate.ascx" TagName="MenuOreAccantonate"
    TagPrefix="uc3" %>

<%@ Register Src="~/WebControls/MenuImprese.ascx" TagName="MenuImprese" TagPrefix="uc2" %>
<%@ Register Src="~/RendicontiImprese/WebControls/MenuRiepilogoPagamenti.ascx" TagName="MenuRiepilogoPagamenti"
    TagPrefix="uc4" %>
<%@ Register src="../WebControls/ConsulenteSelezioneImpresa.ascx" tagname="ConsulenteSelezioneImpresa" tagprefix="uc5" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <%--<uc2:MenuImprese ID="MenuImprese1" runat="server" />--%>
    <uc3:MenuOreAccantonate ID="MenuOreAccantonate1" runat="server" Visible="false" />
    <uc4:MenuRiepilogoPagamenti ID="MenuRiepilogoPagamenti1" runat="server" Visible="false" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Reportistica imprese" />
    <br />
    <uc5:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" 
        runat="server" />
    <br />
    <table class="borderedTable" id="TableLabelCartella" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelCartella" runat="server">Riportiamo di seguito l’elenco dei Vostri dipendenti che hanno percepito il trattamento economico per ferie/la gratifica natalizia per l’anno in corso con i relativi importi. Nel ricordarvi che abbiamo provveduto a far pervenire direttamente ai lavoratori quanto di loro spettanza tramite bonifico su conto corrente o carta prepagata intestati ai beneficiari stessi, sottolineiamo che gli importi erogati devono corrispondere a quelli effettivamente accantonati per il periodo in oggetto. Provvediamo, altresì, ad elencare i nominativi dei Vostri dipendenti che, per ragioni varie a noi non note, non hanno incassato quanto di loro competenza. Vi saremmo, pertanto, grati se voleste avvertirli di prendere contatto con i nostri uffici. La situazione è aggiornata alla data riportata nel report. </asp:Label>--%>
                <asp:Label ID="LabelCartella" runat="server">Riportiamo di seguito l’elenco dei vostri dipendenti che hanno percepito il trattamento economico per ferie/la gratifica natalizia per l’anno in corso con i relativi importi. Nel ricordarvi che abbiamo provveduto a far pervenire direttamente ai lavoratori quanto di loro spettanza tramite bonifico su conto corrente o carta prepagata intestati ai beneficiari stessi, certifichiamo che gli importi erogati sono corrispondenti a quelli effettivamente accantonati per il periodo in oggetto. Provvediamo, altresì, ad elencare i nominativi dei Vostri dipendenti che non hanno incassato quanto di loro competenza in quanto non ci è stata fornita indicazione del mezzo di pagamento elettronico prescelto (conto corrente o carta prepagata). Vi saremmo, pertanto, grati se voleste avvertirli di prendere contatto con i nostri uffici. I dati riportati potranno subire variazioni nel corso delle successive consultazioni recependo gli aggiornamenti di volta in volta intercorsi. La situazione è aggiornata alla data riportata nel prospetto sottostante. </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelNoDati" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelNoDati" runat="server">I periodi di consultazione dei report sono: <ul><li><b>report</b> relativo al trattamento economico per ferie (<b>“cartella” di luglio</b>): visualizzabile dall’inizio dell’erogazione del pagamento <b>fino a fine settembre</b>;</li><li><b>report</b> relativo alla gratifica natalizia (<b>“cartella” di dicembre</b>): visualizzabile dall’inizio dell’erogazione del pagamento <b>fino a fine gennaio.</b></li></ul>Ne approfittiamo per precisare che i periodi di contribuzione delle “cartelle” sono:<ul><li>“cartella” di luglio: da ottobre a marzo</li><li>“cartella” di dicembre: da aprile a settembre</li></ul>Per ulteriori informazioni l’impresa può contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 <b>“Servizi ai Lavoratori”</b>.</asp:Label>--%>
                <asp:Label ID="LabelNoDati" runat="server">I periodi di consultazione dei report sono: <ul><li><b>riepilogo</b> relativo al trattamento economico per ferie (<b>“cartella” di luglio</b>): visualizzabile dall’inizio dell’erogazione del pagamento <b>fino a fine settembre</b>;</li><li><b>riepilogo</b> relativo alla gratifica natalizia (<b>“cartella” di dicembre</b>): visualizzabile dall’inizio dell’erogazione del pagamento <b>fino a fine gennaio.</b></li></ul>Ne approfittiamo per precisare che i periodi di contribuzione delle “cartelle” sono:<ul><li>“cartella” di luglio: da ottobre a marzo</li><li>“cartella” di dicembre: da aprile a settembre</li></ul>Per ulteriori informazioni l’impresa può contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 <b>“Servizi ai Lavoratori”</b>.</asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelPremioFedelta" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelCartella" runat="server">Riportiamo di seguito l’elenco dei Vostri dipendenti che hanno percepito il trattamento economico per ferie/la gratifica natalizia per l’anno in corso con i relativi importi. Nel ricordarvi che abbiamo provveduto a far pervenire direttamente ai lavoratori quanto di loro spettanza tramite bonifico su conto corrente o carta prepagata intestati ai beneficiari stessi, sottolineiamo che gli importi erogati devono corrispondere a quelli effettivamente accantonati per il periodo in oggetto. Provvediamo, altresì, ad elencare i nominativi dei Vostri dipendenti che, per ragioni varie a noi non note, non hanno incassato quanto di loro competenza. Vi saremmo, pertanto, grati se voleste avvertirli di prendere contatto con i nostri uffici. La situazione è aggiornata alla data riportata nel report. </asp:Label>--%>
                <%-- <asp:Label ID="LabelPremioFedelta" runat="server">
                Riportiamo di seguito l’elenco dei nominativi dei Vostri dipendenti assegnatari del Premio di Fedeltà relativo all’anno corrente.
                <br /><br />
                Vi ricordiamo che il Premio Fedeltà consiste in un’erogazione annuale dell’importo fisso pari a € 259,74 lordi (€ 200,00 netti) per beneficiario da destinare ai lavoratori dipendenti da almeno otto anni da impresa iscritta per lo stesso periodo alla Cassa Edile di Milano.
                <br /><br />
                L’individuazione dei nominativi è stata effettuata sulla base dei requisiti di accantonamento orario necessari per la fruizione della prestazione in oggetto, riportati in dettaglio sul nostro sito internet nella sezione <b>“Lavoratori”</b> alla voce <b>"Informazioni Operative”</b>, <b>“Prestazioni e Modulistica”</b>, <b>“Normativa e Modulistica”</b>.
                </asp:Label>--%>
                <asp:Label ID="LabelPremioFedelta" runat="server">
                Riportiamo di seguito l’elenco dei nominativi dei Vostri dipendenti assegnatari del Premio di Fedeltà relativo all’anno corrente.
                <br /><br />
                Vi ricordiamo che il Premio Fedeltà consiste in un’erogazione annuale dell’importo fisso pari a € 259,74 lordi (€ 200,00 netti) per beneficiario da destinare ai lavoratori dipendenti da almeno otto anni da impresa iscritta per lo stesso periodo alla Cassa Edile di Milano, Lodi, Monza e Brianza.
                <br /><br />
                L’individuazione dei nominativi è stata effettuata sulla base dei requisiti di accantonamento orario necessari per la fruizione della prestazione in oggetto, riportati in dettaglio sul nostro sito internet nella sezione “<b>Lavoratori</b>” alla voce “<b>Informazioni Operative</b>” => “<b>Prestazioni e Modulistica</b>” => “<b>Normativa e Modulistica</b>”.
                <br /><br />
                Il periodo di consultazione del prospetto è dalla data di erogazione della prestazione (che avviene nel mese di ottobre) entro il 31 dicembre.
                <br /><br />
                Per ulteriori informazioni contattare i nostri uffici al numero di telefono: 02.584961, tasto 2 <b>“Servizi ai Lavoratori”</b>.
                </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelNoDatiPremioFedelta" runat="server" visible="false">
        <tr>
            <td>
                <%--<asp:Label ID="LabelNoDati" runat="server">I periodi di consultazione dei report sono: <ul><li><b>report</b> relativo al trattamento economico per ferie (<b>“cartella” di luglio</b>): visualizzabile dall’inizio dell’erogazione del pagamento <b>fino a fine settembre</b>;</li><li><b>report</b> relativo alla gratifica natalizia (<b>“cartella” di dicembre</b>): visualizzabile dall’inizio dell’erogazione del pagamento <b>fino a fine gennaio.</b></li></ul>Ne approfittiamo per precisare che i periodi di contribuzione delle “cartelle” sono:<ul><li>“cartella” di luglio: da ottobre a marzo</li><li>“cartella” di dicembre: da aprile a settembre</li></ul>Per ulteriori informazioni l’impresa può contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 <b>“Servizi ai Lavoratori”</b>, tasto 1 <b>“Cartella e Anzianità Professionale Edile”.</b></asp:Label>--%>
                <%--<asp:Label ID="LabelLabelNoDatiPremioFedelta" runat="server">Per ulteriori informazioni l’impresa può contattare i nostri uffici al numero di telefono: <b>02.584961</b>, tasto 2 “<b>Servizi ai Lavoratori</b>”.</asp:Label>--%>
                <asp:Label ID="LabelLabelNoDatiPremioFedelta" runat="server">
                Il periodo di consultazione del prospetto è dalla data di erogazione della prestazione (che avviene nel mese di ottobre) <b>entro il 31 dicembre</b>.
                <br /><br />
                Ne approfittiamo per ricordare che il Premio Fedeltà consiste in un’erogazione annuale dell’importo fisso pari a € 259,74 lordi (€ 200,00 netti) per beneficiario da destinare ai lavoratori dipendenti da almeno otto anni da impresa iscritta per lo stesso periodo alla Cassa Edile di Milano, Lodi, Monza e Brianza.
                <br /><br />
                Per ulteriori dettagli sulla prestazione si rimanda alla sezione “<b>Lavoratori</b>” => “<b>Informazioni Operative</b>” => “<b>Prestazioni e Modulistica</b>” => “<b>Normativa e Modulistica</b>” del nostro sito internet o al numero di telefono: 02.584961, tasto 2 “<b>Servizi ai Lavoratori</b>”.
                </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableTrattamentoCIGO" runat="server" visible="false">
        <tr>
            <td>
                <!--<p>
                    Il periodo di consultazione del prospetto è dalla data di erogazione della prestazione <b>fino al 30 Giugno 2016</b>.
                </p> -->
                <p>
                    A decorrere dal mese di marzo 2013, con riferimento alle sospensioni dal lavoro per mancanza di lavoro, fine cantiere 
                    o fine fase lavorativa, iniziate entro il 31 Dicembre 2015, Cassa Edile di Milano, Lodi, Monza e Brianza riconosce 
                    agli operai iscritti, ai quali l’impresa non anticipi il trattamento di erogazione salariale a carico INPS, una prestazione 
                    sociale in cifra fissa su base oraria, determinata dalle Parti Sociali (pari a € 3,82 per 
                    l’anno 2014 e a € 3,83 per il 2015), a valere sul trattamento di Cassa Integrazione Guadagni Ordinaria (C.I.G.O.), nei termini e nelle condizioni 
                    riportate nei verbali di accordo provinciali del 1° marzo 2013, del 17 dicembre 2013, del 9 febbraio 2015 e del 14 luglio 2015.
                </p>
                <p>
                    Per ulteriori dettagli sulla prestazione si rimanda alla sezione "<b>Lavoratori</b>" => "<b>Informazioni Operative</b>" => 
                    "<b>Prestazioni e Modulistica</b>" => "<b>Normativa e Modulistica</b>" del nostro sito internet o al numero di telefono: 
                    02.584961, tasto 2 "<b>Servizi ai Lavoratori</b>".
                </p>
                Anno:<asp:DropDownList
                    ID="DropDownListAnno"
                    runat="server"
                    Width="80px">
                </asp:DropDownList>
                <br />
                <asp:Button
                    ID="ButtonTrattamentoCIGO"
                    runat="server"
                    Text="Genera prospetto"
                    Width="200px" 
                    onclick="ButtonTrattamentoCIGO_Click" />
            </td>
        </tr>
    </table>
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerImprese" runat="server" Visible="false" OnInit="ReportViewerImprese_Init"
                    ProcessingMode="Remote" Font-Names="Verdana" Font-Size="8pt" SizeToReportContent="True"
                    DocumentMapCollapsed="True" Height="550pt" Width="550pt" ShowParameterPrompts="false">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
