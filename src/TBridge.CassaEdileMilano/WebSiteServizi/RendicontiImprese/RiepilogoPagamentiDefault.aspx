﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RiepilogoPagamentiDefault.aspx.cs" Inherits="RendicontiImprese_RiepilogoPagamentiDefault" %>

<%@ Register Src="WebControls/MenuRiepilogoPagamenti.ascx" TagName="MenuRiepilogoPagamenti"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register src="../WebControls/ConsulenteSelezioneImpresa.ascx" tagname="ConsulenteSelezioneImpresa" tagprefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Rendiconti"
        sottoTitolo="Riepilogo pagamenti" />
    <br />
    <uc3:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" 
        runat="server" />
    <p>
        Per consultare il report del pagamento erogato in favore dei propri dipendenti a
        titolo di trattamento economico per ferie e gratifica natalizia, sulla base degli
        accantonamenti effettuati da parte dell’impresa di appartenenza.<br />
        Per accedere al report selezionare la voce "cartella" dal menu verticale di sinistra.<br />
        Il secondo report disponibile ("Premio Fedeltà"), riguarda l’erogazione della prestazione
        assistenziale integrativa denominata "Premio di Fedeltà per gli operai", effettuata
        sulla base dei contributi versati in Cassa Edile.
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuRiepilogoPagamenti ID="MenuRiepilogoPagamenti1" runat="server" />
</asp:Content>
