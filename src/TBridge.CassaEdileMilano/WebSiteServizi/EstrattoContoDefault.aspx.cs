using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;

public partial class EstrattoContoDefault : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.InfoImpresa);
        funzionalita.Add(FunzionalitaPredefinite.InfoLavoratore);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        if (
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.InfoLavoratore.ToString()))
        {
            Response.Redirect("EstrattoContoLavoratore.aspx");
        }
        if (
            GestioneUtentiBiz.Autorizzato(
                FunzionalitaPredefinite.InfoImpresa.ToString()))
        {
            Response.Redirect("HomeImpresa.aspx");
        }
    }
}