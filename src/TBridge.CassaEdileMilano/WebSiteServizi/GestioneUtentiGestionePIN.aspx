<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneUtentiGestionePIN.aspx.cs" Inherits="GestioneUtentiGestionePIN" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc1" %>
<%@ Register src="WebControls/MenuGestionePin.ascx" tagname="MenuGestionePin" tagprefix="uc3" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione utenti"
        sottoTitolo="Gestione PIN per le aziende" />
    <br />
    <table width="90%">
        <tr>
            <td style="width: 119px" align="left">
                Codice Impresa:
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxCodImp" runat="server" Width="300px"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Formato non valido"
                    ControlToValidate="TextBoxCodImp" Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 119px" align="left">
                Ragione Sociale:
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 119px; height: 24px;" align="left">
                P.IVA:
            </td>
            <td style="height: 24px; width: 432px;" align="left">
                <asp:TextBox ID="TextBoxPIVA" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                Codice Fiscale:
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                CAP
            </td>
            <td style="width: 432px" align="left">
                <asp:DropDownList ID="DropDownListCAP" runat="server" Width="306px" AppendDataBoundItems="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 119px" align="left">
                Tipo imprese:
            </td>
            <td style="width: 432px" align="left">
                <asp:DropDownList ID="DropDownListTipoRicerca" runat="server" Width="306px">
                    <asp:ListItem Value="TUTTE">Tutte</asp:ListItem>
                    <asp:ListItem Value="SENZAPIN">Imprese senza PIN assegnato</asp:ListItem>
                    <asp:ListItem Value="CONPIN">Imprese con PIN assegnato</asp:ListItem>
                    <asp:ListItem Value="NUOVESENZAPIN">Imprese nuove senza PIN</asp:ListItem>
                    <asp:ListItem Value="ISCRITTESENZAPIN">Imprese regolarmente iscritte senza PIN</asp:ListItem>
                    <asp:ListItem Value="FABBSENZAPIN">Imprese con fabbisogno senza PIN</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                PIN generato
            </td>
            <td style="width: 432px">
            </td>
        </tr>
        <tr>
            <td align="right">
                Dal (gg/mm/aaaa):
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxDal"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right">
                Al (gg/mm/aaaa):
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxAl"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Inserita a sistema
            </td>
            <td align="right" style="width: 432px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                Dal (gg/mm/aaaa):
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxIscrittaDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Formato non valido"
                    ControlToValidate="TextBoxIscrittaDal" Operator="DataTypeCheck" Type="Date" />
        </tr>
        <tr>
            <td align="right">
                Al (gg/mm/aaaa):
            </td>
            <td style="width: 432px" align="left">
                <asp:TextBox ID="TextBoxIscrittaAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Formato non valido"
                    ControlToValidate="TextBoxIscrittaAl" Operator="DataTypeCheck" Type="Date" />
        </tr>
        <tr>
            <td>
                Iscritta al sito web
            </td>
            <td align="left" style="width: 432px">
                <asp:DropDownList ID="DropDownListIscrittaWeb" runat="server" Width="306px">
                    <asp:ListItem Value="NONDEF">Tutte</asp:ListItem>
                    <asp:ListItem Value="ISCRITTE">Iscritte</asp:ListItem>
                    <asp:ListItem Value="NONISCRITTE">Non iscritte</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" style="width: 432px">
                <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Ricerca" />
            </td>
        </tr>
    </table>
    &nbsp;&nbsp;<br />
    <b>Risultati della ricerca</b>
    <asp:GridView ID="GridViewImprese" runat="server" AutoGenerateColumns="False" DataKeyNames="IdImpresa,PIN,codiceFiscale,partitaIVA,indirizzoSedeLegale,capSedeLegale,localitaSedeLegale,provinciaSedeLegale,indirizzoSedeAmministrazione,capSedeAmministrazione,localitaSedeAmministrazione,provinciaSedeAmministrazione,pressoSedeAmministrazione"
        Width="100%" OnPageIndexChanging="GridViewImprese_PageIndexChanging" AllowPaging="True"
        PageSize="20" OnRowCommand="GridViewImprese_RowCommand" 
        onrowdatabound="GridViewImprese_RowDataBound">
        <Columns>
            <asp:BoundField HeaderText="Ragione Sociale" DataField="RagioneSociale" SortExpression="RagioneSociale">
                <ItemStyle Width="30%" />
                <HeaderStyle Width="30%" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Codice Impresa" DataField="IdImpresa" SortExpression="IdImpresa">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Username" DataField="Username" SortExpression="Username">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField HeaderText="PIN" DataField="PIN" SortExpression="PIN">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField DataField="DataGenerazionePIN" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="Data generazione PIN" HtmlEncode="False" />
            <asp:BoundField DataField="partitaIVA" HeaderText="Partita IVA" SortExpression="partitaIva"
                Visible="False">
                <ItemStyle Width="15%" />
                <HeaderStyle Width="15%" />
            </asp:BoundField>
            <asp:BoundField DataField="codiceFiscale" HeaderText="Codice Fiscale" SortExpression="codiceFiscale"
                Visible="False">
                <ItemStyle Width="15%" />
                <HeaderStyle Width="15%" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Inserisci nel file">
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxNelFile" runat="server" />
                </ItemTemplate>
                <ItemStyle Width="5%" />
                <HeaderStyle Width="5%" />
            </asp:TemplateField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" Text="Genera PIN"
                CommandName="Genera">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="15%" />
            </asp:ButtonField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="ButtonImpersona" runat="server" CausesValidation="false" 
                        CommandName="Impersona" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" Text="Impersona" />
                </ItemTemplate>
                <ControlStyle CssClass="bottoneGriglia" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna impresa trovata
        </EmptyDataTemplate>
    </asp:GridView>
    <table width="80%">
        <tr>
            <td align="right" style="width: 517px">
                <asp:LinkButton ID="LinkButtonSelezionaTutti" runat="server" OnClick="LinkButtonSelezionaTutti_Click"
                    Visible="False">Seleziona tutti</asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="LinkButtonDeselezionaTutti" runat="server" OnClick="LinkButtonDeselezionaTutti_Click"
                    Visible="False">Deseleziona tutti</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="width: 517px">
                <table class="standardTable">
                    <tr>
                        <td align="left">
                            <asp:Label ID="LabelEsporta" runat="server" Text="Esporta Excel" Visible="False"></asp:Label>&nbsp;
                        </td>
                        <td style="width: 141px">
                        </td>
                        <td align="left">
                            <asp:Label ID="LabelGenera" runat="server" Text="Genera PIN" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="ButtonEsportaExcel" runat="server" OnClick="ButtonEsportaExcel_Click"
                                Text="Solo selezionati" Visible="False" Width="120px" />
                        </td>
                        <td style="width: 141px" align="left">
                        </td>
                        <td>
                            <asp:Button ID="ButtonGeneraPIN" runat="server" OnClick="ButtonGeneraPIN_Click" Text="Nella pagina"
                                Visible="False" Width="120px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="ButtonFileTutti" runat="server" OnClick="ButtonFileTutti_Click" Text="Tutti"
                                Visible="False" Width="120px" />
                        </td>
                        <td style="width: 141px" align="left">
                        </td>
                        <td>
                            <asp:Button ID="ButtonGeneraTutti" runat="server" OnClick="ButtonGeneraTutti_Click"
                                Text="Tutti" Visible="False" Width="120px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="MenuDettaglio">
    <uc3:MenuGestionePin ID="MenuGestionePin1" runat="server" />
</asp:Content>

