<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="ErroreLogin.aspx.cs" Inherits="ErroreLogin" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <br />
    &nbsp;Errore durante l'autenticazione.<br />
    <br />
    La Username e la Password inserite non sono state riconosciute. Riprovare.<br />
    <br />
    Se non sei ancora registrato
    <asp:LinkButton ID="LinkButtonRegistrati" runat="server" OnClick="LinkButtonRegistrati_Click">clicca qui</asp:LinkButton></asp:Content>


