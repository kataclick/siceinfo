﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;

public partial class MalattiaTelematica_VisualizzaRicevuta : System.Web.UI.Page
{
    private readonly BusinessEF biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Int32 idAssenza = 0;
            
            if (Context.Items["IdAssenza"] != null)
            {
                ViewState["IdAssenza"] = (int)Context.Items["IdAssenza"]; ;
            }

          
            //idAssenza = (Int32)ViewState["IdAssenza"];
           
            CaricaDati();
            
        }
    }

    protected void CaricaDati()
    {
        int idAssenza = (Int32)ViewState["IdAssenza"];

        MalattiaTelematicaAssenza assenza = biz.GetMalattiaTelematicaAssenzaRicevuta(idAssenza);


        LabelRichiedente.Text = assenza.Impresa.RagioneSociale;
        LabelCodiceImpresa.Text = assenza.IdImpresa.ToString();
        LabelPiva.Text = assenza.Impresa.PartitaIVA;
        switch (assenza.Tipo)
        {
            case "MA":
                LabelDomanda.Text = "MALATTIA";
                break;
            case "IN":
                LabelDomanda.Text = "INFORTUNIO";
                break;
            default:
                LabelDomanda.Text = "MALATTIA PROFESSIONALE";
                break;
        }

        LabelCognomeNome.Text = String.Format("{0} {1}", assenza.Lavoratore.Cognome, assenza.Lavoratore.Nome);
        LabelIdLavoratore.Text = assenza.IdLavoratore.ToString();

        LabelPeriodi.Text = string.Format("{0} - {1}", assenza.DataInizio.Value.ToShortDateString(),
                                          assenza.DataFine.Value.ToShortDateString());
       
        if (assenza.DataImmessaPervenuta != null)
            LabelDataImmessaPervenuta.Text = assenza.DataImmessaPervenuta.Value.ToShortDateString();
    }


    protected void ButtonStampa_OnClick(object sender, EventArgs e)
    {
        int idAssenza  = (Int32)ViewState["IdAssenza"];

        ReportViewerRicevuta.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
        ReportViewerRicevuta.ServerReport.ReportPath = "/ReportMalattiaTelematica/Ricevuta";

        ReportParameter[] listaParam = new ReportParameter[1];
        listaParam[0] = new ReportParameter("idAssenza", idAssenza.ToString());
        ReportViewerRicevuta.ServerReport.SetParameters(listaParam);


        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string extension;

        //PDF

        byte[] bytes = ReportViewerRicevuta.ServerReport.Render(
            "PDF", null, out mimeType, out encoding, out extension,
            out streamids, out warnings);


        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/pdf";

        Response.AppendHeader("Content-Disposition", "attachment;filename=EstrattoConto.pdf");
        Response.BinaryWrite(bytes);

        Response.Flush();
        Response.End();
    }

    protected void ButtonIndietro_OnClick(object sender, EventArgs e)
    {
        Context.Items["IdAssenza"] = (Int32)ViewState["IdAssenza"];
        Server.Transfer("~/MalattiaTelematica/GestioneAssenza.aspx");
       
    }

    protected void ButtonTornaLista_OnClick(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
        {
            Server.Transfer("~/MalattiaTelematica/RicercaAssenze.aspx");

        }
        else
        {
            Server.Transfer("~/MalattiaTelematica/RicercaAssenzeBackOffice.aspx");

        }
    }
}