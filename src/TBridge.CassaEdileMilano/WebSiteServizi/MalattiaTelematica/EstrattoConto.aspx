﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EstrattoConto.aspx.cs" Inherits="MalattiaTelematica_EstrattoConto" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>

<%@ Register src="WebControls/RicercaEstrattoConto.ascx" tagname="RicercaEstrattoConto" tagprefix="uc2" %>

<%@ Register src="../WebControls/MenuMalattiaTelematica.ascx" tagname="MenuMalattiaTelematica" tagprefix="uc3" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio" sottoTitolo="Ricerca estratto conto" />
    <br />
    <uc2:RicercaEstrattoConto ID="RicercaEstrattoConto1" runat="server" />
    
    
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
