﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using System.Globalization;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;

public partial class MalattiaTelematica_RicercaErrori : System.Web.UI.Page
{
    Business biz = new Business();


    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.MalattiaTelematicaRicercaMessaggi);

        #endregion

     
    }

  



    protected void RadButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            //if (OnFiltroSelected != null)
            //{
            //    AssenzeFilter filtro = CreaFiltro();
            //    OnFiltroSelected(filtro);
            //}
            CaricaMessaggiAssenze();
            CaricaMessaggiCertificatiMedici();
            CaricaMessaggiMalattieInfortuni();
        }
    }
    private void CaricaMessaggiAssenze()
    {
        Presenter.CaricaElementiInGridView(RadGridAssenze, biz.GetAssenzaErrori(RadDatePickerGiorno.SelectedDate.Value.Date));
    }

    private void CaricaMessaggiCertificatiMedici()
    {
        Presenter.CaricaElementiInGridView(RadGridCertificatiMedici, biz.GetCertificatoMedicoErrori(RadDatePickerGiorno.SelectedDate.Value.Date));
    }

    private void CaricaMessaggiMalattieInfortuni()
    {
        Presenter.CaricaElementiInGridView(RadGridMalattieInfortuni, biz.GetMalattiaInfortunioErrori(RadDatePickerGiorno.SelectedDate.Value.Date));
    }

    protected void RadGridAssenze_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaMessaggiAssenze();
    }

    protected void RadGridCertificatiMedici_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaMessaggiCertificatiMedici();
    }

    protected void RadGridMalattieInfortuni_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaMessaggiMalattieInfortuni();
    }

    protected void RadGridAssenze_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

        if (e.Item is GridDataItem)
        {
            AssenzaErrori err = (AssenzaErrori)e.Item.DataItem;
            Label lIdImpresa = (Label)e.Item.FindControl("LabelIdImpresa");
            Label lImpresa = (Label)e.Item.FindControl("LabelImpresa");
            Label lIdLavoratore = (Label)e.Item.FindControl("LabelIdLavoratore");
            Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label lProtocollo = (Label)e.Item.FindControl("LabelProtocollo");
            Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            Label lInizioMalattia = (Label)e.Item.FindControl("LabelInizioMalattia");

            lIdImpresa.Text = String.Format("Codice: {0}", err.IdImpresa);
            lImpresa.Text = err.RagioneSociale;
            lIdLavoratore.Text = String.Format("Codice: {0}", err.IdLavoratore);
            lLavoratore.Text = String.Format("{0} {1}", err.Cognome, err.Nome);
            lProtocollo.Text = String.Format("{0} {1}/{2}/{3}", err.IdCassaEdile, err.TipoProtocollo, err.NumeroProtocollo.ToString(), err.AnnoProtocollo.ToString());
            lPeriodo.Text = String.Format("{0} - {1}", err.DataInizio.ToShortDateString(), err.DataFine.ToShortDateString());
            lInizioMalattia.Text = String.Format("Data inizio malattia: {0}", err.DataInizioMalattia.ToShortDateString());

        }

    }

    protected void RadGridCertificatiMedici_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

        if (e.Item is GridDataItem)
        {
            CertificatoMedicoErrori err = (CertificatoMedicoErrori)e.Item.DataItem;
            Label lIdImpresa = (Label)e.Item.FindControl("LabelIdImpresa");
            Label lImpresa = (Label)e.Item.FindControl("LabelImpresa");
            Label lIdLavoratore = (Label)e.Item.FindControl("LabelIdLavoratore");
            Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label lProtocollo = (Label)e.Item.FindControl("LabelProtocollo");
            Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            Label lInizioMalattia = (Label)e.Item.FindControl("LabelInizioMalattia");

            Label lNumero = (Label)e.Item.FindControl("LabelNumero");
            Label lPeriodoCM = (Label)e.Item.FindControl("LabelPeriodoCM");
            Label lRilascio = (Label)e.Item.FindControl("LabelRilascio");

            lIdImpresa.Text = String.Format("Codice: {0}", err.IdImpresa);
            lImpresa.Text = err.RagioneSociale;
            lIdLavoratore.Text = String.Format("Codice: {0}", err.IdLavoratore);
            lLavoratore.Text = String.Format("{0} {1}", err.Cognome, err.Nome);
            lProtocollo.Text = String.Format("{0} {1}/{2}/{3}", err.IdCassaEdile, err.TipoProtocollo, err.NumeroProtocollo.ToString(), err.AnnoProtocollo.ToString());
            lPeriodo.Text = String.Format("{0} - {1}", err.DataInizio.ToShortDateString(), err.DataFine.ToShortDateString());
            lInizioMalattia.Text = String.Format("Data inizio malattia: {0}", err.DataInizioMalattia.ToShortDateString());

            lNumero.Text = String.Format("Numero: {0}", err.NumeroCertificato); 
            lPeriodoCM.Text = String.Format("{0} - {1}", err.DataInizioCertificato.ToShortDateString(), err.DataFineCertificato.ToShortDateString());
            lRilascio.Text = String.Format("Data Rilascio: {0}", err.DataRilascioCertificato.ToShortDateString());
        }

    }

    protected void RadGridMalattieInfortuni_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

        if (e.Item is GridDataItem)
        {
            MalattiaInfortunioErrori err = (MalattiaInfortunioErrori)e.Item.DataItem;
            Label lIdImpresa = (Label)e.Item.FindControl("LabelIdImpresa");
            Label lImpresa = (Label)e.Item.FindControl("LabelImpresa");
            Label lIdLavoratore = (Label)e.Item.FindControl("LabelIdLavoratore");
            Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
            Label lProtocollo = (Label)e.Item.FindControl("LabelProtocollo");
            Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            Label lInizioMalattia = (Label)e.Item.FindControl("LabelInizioMalattia");

            Label lPeriodoMI = (Label)e.Item.FindControl("LabelPeriodoMI");

            lIdImpresa.Text = String.Format("Codice: {0}", err.IdImpresa);
            lImpresa.Text = err.RagioneSociale;
            lIdLavoratore.Text = String.Format("Codice: {0}", err.IdLavoratore);
            lLavoratore.Text = String.Format("{0} {1}", err.Cognome, err.Nome);
            lProtocollo.Text = String.Format("{0} {1}/{2}/{3}", err.IdCassaEdile, err.TipoProtocollo, err.NumeroProtocollo.ToString(), err.AnnoProtocollo.ToString());
            lPeriodo.Text = String.Format("{0} - {1}", err.DataInizio.ToShortDateString(), err.DataFine.ToShortDateString());
            lInizioMalattia.Text = String.Format("Data inizio malattia: {0}", err.DataInizioMalattia.ToShortDateString());

            lPeriodoMI.Text = String.Format("{0} - {1}", err.DataInizioEvento.ToShortDateString(), err.DataFineEvento.ToShortDateString());
        }

    }

}