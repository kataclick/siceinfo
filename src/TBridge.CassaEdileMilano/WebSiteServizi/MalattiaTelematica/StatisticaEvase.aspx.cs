﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using Telerik.Web.UI;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using System.Globalization;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;

public partial class MalattiaTelematica_StatisticaEvase : System.Web.UI.Page
{
    Business biz = new Business();
    private readonly BusinessEF bizEF = new BusinessEF();

   
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.MalattiaTelematicaStatisticaLiquidazioni);

        #endregion

        if (!Page.IsPostBack)
        {
            CaricaTipiAssenza();
        }
    }

    protected void RadButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
           
            //if (OnFiltroSelected != null)
            //{
            //    AssenzeFilter filtro = CreaFiltro();
            //    OnFiltroSelected(filtro);
            //}
            CaricaStatistica();
        }
    }

    private void CaricaTipiAssenza()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxTipo, bizEF.GetTipiAssenze(), "Descrizione", "Id");
    }

    private void CaricaStatistica()
    {
        Presenter.CaricaElementiInGridView(RadGridLiquidazioni, biz.GetStatisticaEvase(RadDatePickerPeriodoDa.SelectedDate.Value.Date, RadDatePickerPeriodoA.SelectedDate.Value.Date, RadComboBoxTipo.SelectedValue));
    }


    protected void RadGridLiquidazioni_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaStatistica();
    }

  
}