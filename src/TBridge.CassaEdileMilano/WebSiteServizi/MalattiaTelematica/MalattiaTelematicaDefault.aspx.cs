﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class MalattiaTelematica_MalattiaTelematicaDefault : System.Web.UI.Page
{
    private readonly BusinessEF _bizEf = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autorizzazione

        List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

        funzionalita.Add(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeImpresa);
        funzionalita.Add(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeBackOffice);
        funzionalita.Add(FunzionalitaPredefinite.MalattiaTelematicaEstrattoConto);

        GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

        #endregion

       
        if (!Page.IsPostBack)
        {
            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
            {
                TestoEmail.Visible = true;
                TableEmail.Visible = true;
                TableConteggi.Visible = false;
                CaricaEmail();
            }
            else
            {
                TableConteggi.Visible = true;
                TableEmail.Visible = false;
                TestoEmail.Visible = false;
                CaricaConteggi();
            }
            
        }
    }
    protected void ButtonSalvaEmail_Click(object sender, EventArgs e)
    {
        if (TextBoxEmailModifica.Text != string.Empty)
        {
            int? idImpresa = null;
            int? idConsulente = null;
            if (GestioneUtentiBiz.IsConsulente())
            {
                idConsulente =
                    ((Consulente) GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdConsulente;
            }

            if (GestioneUtentiBiz.IsImpresa())
            {
                idImpresa = ((Impresa) GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdImpresa;
            }

            _bizEf.UpdateMalattiaTelematicaEmail(idImpresa, idConsulente, TextBoxEmailModifica.Text);
        }

        CaricaEmail();
    }
    private void CaricaConteggi()
    {
        Int32? idUtente = GestioneUtentiBiz.GetIdUtente();

        
        DateTime oggi = DateTime.Now.Date;

        TextBoxInAttesa.Text = _bizEf.GetAssenzeInAttesa(idUtente).ToString();

        TextBoxPrimoSollecito.Text = _bizEf.GetAssenzePrimoSollecito(idUtente, oggi).ToString();

        TextBoxRespingimento.Text = _bizEf.GetAssenzeRespingimento(idUtente, oggi).ToString();

    }

    private void CaricaEmail()
    {
        int? idImpresa = null;
        int? idConsulente = null;
        if (GestioneUtentiBiz.IsConsulente())
        {
            idConsulente =
                ((Consulente)GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdConsulente;
        }

        if (GestioneUtentiBiz.IsImpresa())
        {
            idImpresa = ((Impresa)GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdImpresa;
        }

        TextBoxEmailOriginale.Text = _bizEf.GetMalattiaTelematicaEmail(idImpresa, idConsulente);
        TextBoxEmailModifica.Text = string.Empty;
    }
}