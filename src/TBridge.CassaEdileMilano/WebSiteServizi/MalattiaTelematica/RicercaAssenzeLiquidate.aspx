﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RicercaAssenzeLiquidate.aspx.cs" Inherits="MalattiaTelematica_RicercaAssenze" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/RicercaAssenzeLiquidateImprese.ascx" TagName="RicercaAssenzeLiquidateImprese"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Ricerca assenze per estratto conto" />
    <br />
    <uc2:RicercaAssenzeLiquidateImprese ID="RicercaAssenzeImprese1" runat="server" />
</asp:Content>
