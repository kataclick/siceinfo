﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StatisticaEvase.aspx.cs" Inherits="MalattiaTelematica_StatisticaEvase"
    Theme="CETheme2009" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Statistica per prestazioni evase" />
    <br />
    <table class="standardTable">
        <tr runat="server">
            <td>
                Periodo da (gg/mm/aaaa)
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerPeriodoDa" runat="server">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data valida"
                    ValidationGroup="Ricerca" ControlToValidate="RadDatePickerPeriodoDa">*</asp:RequiredFieldValidator>
            </td>
            <td>
                Periodo a (gg/mm/aaaa)
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerPeriodoA" runat="server">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Immettere una data valida"
                    ValidationGroup="Ricerca" ControlToValidate="RadDatePickerPeriodoA">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadDatePickerPeriodoDa"
                    ControlToValidate="RadDatePickerPeriodoA" EnableClientScript="False" ErrorMessage="Limiti periodi errati"
                    Operator="GreaterThanEqual" ValidationGroup="Ricerca" Type="Date">*
                </asp:CompareValidator>
            </td>
        </tr>
        <tr runat="server">
            <td>
                Tipo Assenza
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipo" runat="server" AppendDataBoundItems="true">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                    ValidationGroup="Ricerca" />
            </td>
        </tr>
        <tr runat="server">
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                    ValidationGroup="Ricerca" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <div class="standardDiv">
        <telerik:RadGrid ID="RadGridLiquidazioni" runat="server" GridLines="None" Width="100%"
            AllowPaging="True" OnPageIndexChanged="RadGridLiquidazioni_PageIndexChanged"
            CellSpacing="0">
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                        <telerik:GridBoundColumn HeaderText="Stato" UniqueName="stato" DataField="Stato">
                        <HeaderStyle HorizontalAlign="NotSet" />
                        <ItemStyle HorizontalAlign="NotSet" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Utente" UniqueName="utente" DataField="utente">
                        <HeaderStyle HorizontalAlign="NotSet" />
                        <ItemStyle HorizontalAlign="NotSet" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Numero assenze" UniqueName="numeroAssenze" DataField="numeroAssenze">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
