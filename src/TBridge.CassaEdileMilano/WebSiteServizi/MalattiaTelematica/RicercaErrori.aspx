﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RicercaErrori.aspx.cs" Inherits="MalattiaTelematica_RicercaErrori"
    Theme="CETheme2009Wide" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Ricerca errori" />
    <br />
    <div class="standardDiv">
        <table class="standardTable">
            <colgroup>
                <col />
                <col />
                <col width="100px" />
            </colgroup>
            <tr id="Tr1" runat="server">
                <td>
                    Giorno
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerGiorno" runat="server">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data"
                        ValidationGroup="Ricerca" ControlToValidate="RadDatePickerGiorno">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                        ValidationGroup="Ricerca" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                        ValidationGroup="Ricerca" />
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <div>
            <telerik:RadTabStrip ID="RadTabStripErrori" runat="server" MultiPageID="RadMultiPageErrore"
                SelectedIndex="0" Width="100%">
                <Tabs>
                    <telerik:RadTab Text="Assenze" Width="90px" Selected="True">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Malattie / Infortuni" Width="150px">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Certificati Medici" Width="150px">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="RadMultiPageErrore" runat="server" SelectedIndex="0" CssClass="radWizard"
                Width="96%" RenderSelectedPageOnly="False">
                <telerik:RadPageView ID="RadPageViewAssenze" runat="server">
                    <div>
                        <telerik:RadGrid ID="RadGridAssenze" runat="server" GridLines="None" OnItemDataBound="RadGridAssenze_ItemDataBound"
                            Width="100%" AllowPaging="True" OnPageIndexChanged="RadGridAssenze_PageIndexChanged"
                            CellSpacing="0">
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" HeaderText="Impresa"
                                        UniqueName="impresa" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelIdImpresa" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Lavoratore"
                                        UniqueName="lavoratore" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelLavoratore" runat="server" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Assenza"
                                        UniqueName="assenza" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelProtocollo" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelPeriodo" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelInizioMalattia" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Descrizione" UniqueName="descrizione" DataField="descrizione">
                                        <ItemStyle Width="100px" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter dataImportazione column" HeaderText="Data Importazione"
                                        UniqueName="dataImportazione" DataField="dataInserimento" DataFormatString="{0:dd/MM/yyyy}"
                                        ItemStyle-VerticalAlign="Top">
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </div>
                    <br />
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewMalattieInfortuni" runat="server">
                    <div>
                        <telerik:RadGrid ID="RadGridMalattieInfortuni" runat="server" GridLines="None" OnItemDataBound="RadGridMalattieInfortuni_ItemDataBound"
                            Width="100%" AllowPaging="True" OnPageIndexChanged="RadGridMalattieInfortuni_PageIndexChanged"
                            CellSpacing="0">
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" HeaderText="Impresa"
                                        UniqueName="impresa" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelIdImpresa" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Lavoratore"
                                        UniqueName="lavoratore" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelLavoratore" runat="server" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Assenza"
                                        UniqueName="assenza" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelProtocollo" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelPeriodo" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelInizioMalattia" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter Evento column"
                                        HeaderText="Evento" UniqueName="Evento" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                              
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelPeriodoMI" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Descrizione" UniqueName="descrizione" DataField="descrizione">
                                        <ItemStyle Width="100px" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter dataImportazione column" HeaderText="Data Importazione"
                                        UniqueName="dataImportazione" DataField="dataInserimento" DataFormatString="{0:dd/MM/yyyy}"
                                        ItemStyle-VerticalAlign="Top">
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </div>
                    <br />
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewCertificatiMedici" runat="server">
                    <div>
                        <telerik:RadGrid ID="RadGridCertificatiMedici" runat="server" GridLines="None" OnItemDataBound="RadGridCertificatiMedici_ItemDataBound"
                            Width="100%" AllowPaging="True" OnPageIndexChanged="RadGridCertificatiMedici_PageIndexChanged"
                            CellSpacing="0">
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" HeaderText="Impresa"
                                        UniqueName="impresa" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelIdImpresa" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Lavoratore"
                                        UniqueName="lavoratore" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelLavoratore" runat="server" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Assenza"
                                        UniqueName="assenza" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelProtocollo" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelPeriodo" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelInizioMalattia" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn FilterControlAltText="Filter CertificatoMedico column"
                                        HeaderText="Certificato medico" UniqueName="certificatoMedico" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelNumero" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelPeriodoCM" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelRilascio" runat="server" Font-Bold="false"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Descrizione" UniqueName="descrizione" DataField="descrizione">
                                        <ItemStyle Width="100px" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn FilterControlAltText="Filter dataImportazione column" HeaderText="Data Importazione"
                                        UniqueName="dataImportazione" DataField="dataInserimento" DataFormatString="{0:dd/MM/yyyy}"
                                        ItemStyle-VerticalAlign="Top">
                                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </div>
                    <br />
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </div>
    </telerik:RadAjaxPanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
