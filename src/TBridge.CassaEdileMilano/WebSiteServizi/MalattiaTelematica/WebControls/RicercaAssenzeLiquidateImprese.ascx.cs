﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Filters;
using TBridge.Cemi.Business.Reporting.ReportingService2005;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using Cemi.MalattiaTelematica.Type.Entities;

public partial class MalattiaTelematica_WebControls_RicercaAssenzeLiquidateImprese : System.Web.UI.UserControl
{
    private readonly BusinessEF biz = new BusinessEF();

    protected void Page_Load(object sender, EventArgs e)
    {
        RicercaAssenzeFiltri1.OnFiltroSelected += RicercaAssenzeFiltri1_OnFiltroSelected;

   
    }

    private void RicercaAssenzeFiltri1_OnFiltroSelected(AssenzeFilter filtro)
    {
        ViewState["filtro"] = filtro;

        CaricaAssenzeLiquidate();

    }

    private void CaricaAssenzeLiquidate()
    {

        AssenzeFilter filtro = (AssenzeFilter) ViewState["filtro"];

        Presenter.CaricaElementiInGridView(RadGridAssenzeLiquidate, biz.GetAssenzeLiquidate(filtro));
    }

    protected void RadGridAssenzeLiquidate_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            AssenzaCertificati assenza = (AssenzaCertificati)e.Item.DataItem;
            Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            
            //AssenzaCertificati assenzaCertificati = (AssenzaCertificati)e.Item.DataItem;
            //Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
            //RadGrid rgGiustificativi = (RadGrid)e.Item.FindControl("RadGridGiustificativi");

            // Periodo
            if (assenza.Assenza.DataInizioAssenzaDenuncia.HasValue && assenza.Assenza.DataFineAssenzaDenuncia.HasValue)
            {
                lPeriodo.Text = String.Format("{0} - {1}",
                    assenza.Assenza.DataInizioAssenzaDenuncia.Value.ToString("dd/MM/yyyy"),
                    assenza.Assenza.DataFineAssenzaDenuncia.Value.ToString("dd/MM/yyyy"));
            }


            //// Giustificativi
            //Presenter.CaricaElementiInGridView(
            //    rgGiustificativi,
            //    assenzaCertificati.Certificati);
        }
    }
    protected void RadGridAssenzeLiquidate_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        CaricaAssenzeLiquidate();
    }
    protected void RadGridAssenzeLiquidate_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "stampa")
        {
            Int32 idAssenza =
                (Int32) RadGridAssenzeLiquidate.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdAssenza"];


            Context.Items["IdAssenza"] = idAssenza;
            //Context.Items["Tutti"] = false;

            Server.Transfer("~/MalattiaTelematica/VisualizzaEstrattoConto.aspx");
        }

    }
}