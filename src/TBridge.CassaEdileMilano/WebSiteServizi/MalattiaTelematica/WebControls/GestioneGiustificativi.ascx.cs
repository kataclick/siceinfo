﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Delegates;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;

[System.Runtime.InteropServices.GuidAttribute("5BF23CD1-07C3-456B-8CC6-58A950BD6451")]
public partial class MalattiaTelematica_WebControls_GestioneGiustificativi : UserControl
{
    private readonly BusinessEF bizEF = new BusinessEF();
    private readonly Business biz = new Business();
    public event CertificatiReturnedEventHandler OnCertificatiReturned;

    protected void Page_Load(object sender, EventArgs e)
    {
        Boolean isImpresa = GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente();
       
        RadioButtonMalattia.Enabled = !isImpresa;
        RadioButtonMalattiaProfessionale.Enabled = !isImpresa;
        RadioButtonInfortunio.Enabled = !isImpresa;

        //RadioButtonInizio.Enabled = !isImpresa;
        //RadioButtonContinuazione.Enabled = !isImpresa;
        //RadioButtonRicaduta.Enabled = !isImpresa;
        
        //ButtonCaricaDatiInps.Visible = !isImpresa;
    }


    public void CaricaAssenza(AssenzaDettagli assenza)
    {
        ViewState["IdAssenza"] = assenza.IdAssenza;
        ViewState["DataInizioAssenza"] = assenza.DataInizioAssenza;
        ViewState["DataInizioMalattia"] = assenza.DataInizioMalattia;
        ViewState["IdLavoratore"] = assenza.IdLavoratore;
        ViewState["IdImpresa"] = assenza.IdImpresa;
        ViewState["IdTipo"] = assenza.IdTipo;
        ViewState["Tipo"] = assenza.Tipo;
        ViewState["TipoAssenza"] = assenza.TipoAssenza;
        ViewState["CodiceFiscale"] = assenza.CodiceFiscale;

        RadButtonConferma.Enabled = biz.AbilitaControllo(assenza.IdStato);
    }

    public void CaricaGiustificativo(Int32 idCertificato)
    {
        if (idCertificato != -1)
        {
            ViewState["IdCertificato"] = idCertificato;

            MalattiaTelematicaCertificatoMedico cert = bizEF.GetCertificatoMedico(idCertificato);

            RadTextBoxNumero.Text = cert.Numero;
            RadDatePickerInizio.SelectedDate = cert.DataInizio;
            RadDatePickerFine.SelectedDate = cert.DataFine;
            RadDatePickerRilascio.SelectedDate = cert.DataRilascio;
            //RadDatePickerRicezione.SelectedDate = cert.DataRicezione;

            if (cert.TipoAssenza == "MA")
                RadioButtonMalattia.Checked = true;
            else if (cert.TipoAssenza == "MP")
                RadioButtonMalattiaProfessionale.Checked = true;
            else if (cert.TipoAssenza == "IN")
                RadioButtonInfortunio.Checked = true;


            if (cert.Tipo == "I")
                RadioButtonInizio.Checked = true;
            else if (cert.Tipo == "C")
                RadioButtonContinuazione.Checked = true;
            else if (cert.Tipo == "R")
                RadioButtonRicaduta.Checked = true;

            LabelFileName.Text = cert.NomeFile;
            if (!string.IsNullOrEmpty(cert.NomeFile))
            {
                ImageAllegato.Visible = true;

                ViewState["ImmagineCertificato"] = cert.Immagine;
                ViewState["NomeFileCertificato"] = cert.NomeFile;
            }
            CheckBoxRicovero.Checked = cert.Ricovero.HasValue ? cert.Ricovero.Value : false;

        }
        else
        {
            ViewState["IdCertificato"] = null;

            if ((String)ViewState["IdTipo"] == "MA")
                RadioButtonMalattia.Checked = true;
            else if ((String)ViewState["IdTipo"] == "MP")
                RadioButtonMalattiaProfessionale.Checked = true;
            else if ((String)ViewState["IdTipo"] == "IN")
                RadioButtonInfortunio.Checked = true;

            if ((String)ViewState["TipoAssenza"] == "Inizio")
                RadioButtonInizio.Checked = true;
            else if ((String)ViewState["TipoAssenza"] == "Continuazione")
                RadioButtonContinuazione.Checked = true;
            else if ((String)ViewState["TipoAssenza"] == "Ricaduta")
                RadioButtonRicaduta.Checked = true;

            CheckBoxRicovero.Checked = false;

            ImageAllegato.Visible = false;
        }
    }

    protected void RadButtonConferma_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            MalattiaTelematicaCertificatoMedico certificato = CreaCertificato();
            if (String.IsNullOrEmpty(certificato.NomeFile))
            {
                certificato.NomeFile = (String)ViewState["NomeFileCertificato"];
                certificato.Immagine = (Byte[])ViewState["ImmagineCertificato"];
            }
            //ViewState["ImmagineCertificato"] = certificato.Immagine;
            //ViewState["NomeFileCertificato"] = certificato.NomeFile;

            // controllo sovrapposizione
            //if (bizEF.CertificatiMediciSovrapposti((Int32)ViewState["IdLavoratore"], certificato.DataInizio.Value, certificato.DataFine.Value,
            //                         certificato.Id))
            //{
            //    LabelMessaggio.Text = "Sovrapposizione di certificati.";
            //}
            // controllo continuazione giorno successivo
            if (certificato.Tipo == "C")
            {
                if (bizEF.CertificatiMediciContinuazione((Int32)ViewState["IdLavoratore"], certificato.DataInizio.Value, certificato.Id))
                {
                    LabelMessaggio.Text = "Continuazione di certificati.";
                } 
            }

            //controllo ricaduta entro 30 GG
            if (certificato.Tipo == "R")
            {
                if (bizEF.CertificatiMediciRicaduta((Int32)ViewState["IdLavoratore"], certificato.DataInizio.Value, certificato.Id))
                {
                    //LabelMessaggio.Text = "Ricaduta di certificati.";

                    
                } 
            }


            if (certificato.DataInizio.Value.Date < (DateTime) ViewState["DataInizioMalattia"])
            {
                LabelMessaggio.Text = "Inizio certificato anteriore ad inizio malattia/infortunio.";
                RadButtonOK.Visible = true;
            }
            else
            {
                SalvaCertificato(certificato);    
            }
            
            
        }
    }

    private void SalvaCertificato(MalattiaTelematicaCertificatoMedico certificato)
    {
        // ricaduta oltre 30 gg forzo assenza
        if (certificato.Tipo == "R")
        {
            if (bizEF.CertificatiMediciRicaduta((Int32) ViewState["IdLavoratore"], certificato.DataInizio.Value,
                                                certificato.Id))
            {

                bizEF.UpdateAssenzeForzatura((Int32)ViewState["IdAssenza"], true);
            }
        }

        if (ViewState["IdCertificato"] == null)
        {
            //Inserimento
            try
            {
                bizEF.InsertCertificatoMedico(certificato);

                Reset(true);
            }
            catch
            {
                ErroreNellInserimento();
            }
        }
        else
        {
            //Aggiornamento
            certificato.Id = (Int32)ViewState["IdCertificato"];
            try
            {
                biz.UpdateMalattiaTelematicaCertificatoMedico(certificato);

                Reset(true);
            }
            catch
            {
                ErroreNellInserimento();
            }
        } 
    }

    private void PulisciControlli()
    {
        RadTextBoxNumero.Text = "";
        RadDatePickerInizio.SelectedDate = null;
        RadDatePickerFine.SelectedDate = null;
        RadDatePickerRilascio.SelectedDate = null;
        //RadDatePickerRicezione.SelectedDate = null;

        RadioButtonMalattia.Checked = true;

        //if (ViewState["Tipo"].ToString() =="I")
        //{
        RadioButtonInizio.Checked = true;
        //}
        //else if (ViewState["Tipo"].ToString() == "C")
        //{
        //    RadioButtonContinuazione.Checked = true;
        //}
        //else if (ViewState["Tipo"].ToString() == "R")
        //{
        //    RadioButtonRicaduta.Checked = true;
        //}

        CheckBoxRicovero.Checked = false;
        
        LabelFileName.Text = "";
        ImageAllegato.Visible = false;

        LabelMessaggio.Text = "";
        RadButtonOK.Visible = false;
        // FileUploadCertificatoMedico. ??
    }


    private MalattiaTelematicaCertificatoMedico CreaCertificato()
    {
        MalattiaTelematicaCertificatoMedico cert = new MalattiaTelematicaCertificatoMedico();

        cert.DataInizio = new DateTime(RadDatePickerInizio.SelectedDate.Value.Year,
                                       RadDatePickerInizio.SelectedDate.Value.Month,
                                       RadDatePickerInizio.SelectedDate.Value.Day);
        cert.DataFine = new DateTime(RadDatePickerFine.SelectedDate.Value.Year,
                                     RadDatePickerFine.SelectedDate.Value.Month,
                                     RadDatePickerFine.SelectedDate.Value.Day);

        cert.DataRilascio = new DateTime(RadDatePickerRilascio.SelectedDate.Value.Year,
                                         RadDatePickerRilascio.SelectedDate.Value.Month,
                                         RadDatePickerRilascio.SelectedDate.Value.Day);

        cert.Numero = RadTextBoxNumero.Text;

        if (RadioButtonMalattia.Checked)
            cert.TipoAssenza = "MA";
        else if (RadioButtonMalattiaProfessionale.Checked)
            cert.TipoAssenza = "MP";
        else if (RadioButtonInfortunio.Checked)
            cert.TipoAssenza = "IN";

        if (RadioButtonInizio.Checked)
            cert.Tipo = "I";
        else if (RadioButtonRicaduta.Checked)
            cert.Tipo = "R";
        else if (RadioButtonContinuazione.Checked)
            cert.Tipo = "C";
        
        cert.Ricovero = CheckBoxRicovero.Checked;
        cert.IdImpresa = (Int32) ViewState["IdImpresa"];
        cert.IdLavoratore = (Int32) ViewState["IdLavoratore"];
        cert.IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente();
        cert.DataInserimentoRecord = DateTime.Now;

        if (FileUploadCertificatoMedico.HasFile)
        {
            using (Stream sr = FileUploadCertificatoMedico.PostedFile.InputStream)
            {
                byte[] img = new byte[sr.Length];
                sr.Read(img, 0, (int) sr.Length);
                sr.Seek(0, SeekOrigin.Begin);
                cert.Immagine = img;
                cert.NomeFile = FileUploadCertificatoMedico.FileName;

                ViewState["ImmagineCertificato"] = cert.Immagine;
                ViewState["NomeFileCertificato"] = cert.NomeFile;
            }
        }

        List<MalattiaTelematicaAssenza> assenze = new List<MalattiaTelematicaAssenza>();
        assenze.Add(new MalattiaTelematicaAssenza {Id = (Int32) ViewState["IdAssenza"]});
        cert.MalattiaTelematicaAssenze = assenze;
        return cert;
    }

    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento del giustificativo.";
    }

    
    private void AvvertimentoNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento del giustificativo.";
    }

    private void Reset(Boolean carica)
    {
        PulisciControlli();

        if (OnCertificatiReturned != null)
        {
            OnCertificatiReturned(carica);
        }
    }


    protected void RadButtonIndietro_Click(object sender, EventArgs e)
    {
        Reset(false);
    }

    protected void CustomValidatorAllegato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        
        if (FileUploadCertificatoMedico.HasFile || !string.IsNullOrEmpty(LabelFileName.Text))
        {
            args.IsValid = true;
        }
    }

    protected void CustomValidatorDate_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;


        if (bizEF.CertificatoMedicoFuoriAssenza((Int32)ViewState["IdAssenza"], RadDatePickerInizio.SelectedDate.Value.Date, RadDatePickerFine.SelectedDate.Value.Date))
        {
            args.IsValid = false;
        }
    }

    protected void RadButtonOK_Click(object sender, EventArgs e)
    {
        MalattiaTelematicaCertificatoMedico certificato = CreaCertificato();
        if (String.IsNullOrEmpty(certificato.NomeFile))
        {
            certificato.NomeFile = (String)ViewState["NomeFileCertificato"];
            certificato.Immagine = (Byte[])ViewState["ImmagineCertificato"];
        }

        SalvaCertificato(certificato);

        Reset(true);
    }

    protected void ButtonCaricaDatiInps_Click(object sender, EventArgs e)
    {
        if (RadTextBoxNumero.Text != "")
        {
            String cf = (String)ViewState["CodiceFiscale"];
            MalattiaTelematicaCertificatoMedico cert = biz.GetCertificatoMedicoFromINPS(cf, RadTextBoxNumero.Text);
             
            if (cert.Numero != null) // se trova il certificato dal sito dell'inps
            {
                RadTextBoxNumero.Text = cert.Numero;
                RadDatePickerInizio.SelectedDate = cert.DataInizio;
                RadDatePickerFine.SelectedDate = cert.DataFine;
                RadDatePickerRilascio.SelectedDate = cert.DataRilascio;
                //RadDatePickerRicezione.SelectedDate = cert.DataRicezione;

                if (cert.TipoAssenza == "MA")
                    RadioButtonMalattia.Checked = true;
                else if (cert.TipoAssenza == "MP")
                    RadioButtonMalattiaProfessionale.Checked = true;
                else if (cert.TipoAssenza == "IN")
                    RadioButtonInfortunio.Checked = true;


                if (cert.Tipo == "I")
                    RadioButtonInizio.Checked = true;
                else if (cert.Tipo == "C")
                    RadioButtonContinuazione.Checked = true;
                else if (cert.Tipo == "R")
                    RadioButtonRicaduta.Checked = true;

                CheckBoxRicovero.Checked = false;
                LabelFileName.Text = cert.NomeFile;
                if (!string.IsNullOrEmpty(cert.NomeFile))
                {
                    ImageAllegato.Visible = true;

                    ViewState["ImmagineCertificato"] = cert.Immagine;
                    ViewState["NomeFileCertificato"] = cert.NomeFile;
                }
            }
        }

    }
}