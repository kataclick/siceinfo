﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DettagliAssenza.ascx.cs"
    Inherits="MalattiaTelematica_WebControls_DettagliAssenza" %>
<table class="borderedTable">
    <tr runat="server" id="trPrestazione" style="height: 25px">
        <td style="width: 207px">
            Prestazione
        </td>
        <td colspan="2">
            <asp:Label ID="LabelPrestazione" runat="server"></asp:Label>
        </td>
    </tr>
    <tr runat="server" id="trImpresa" style="height: 25px">
        <td style="width: 207px">
            Impresa
        </td>
        <td colspan="2">
            <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
        </td>
    </tr>
    <%--<tr style="height: 25px">
        <td style="width: 207px">
            Codice lavoratore
        </td>
        <td colspan="2">
            <asp:Label ID="LabelIdLavoratore" runat="server"></asp:Label>
        </td>
    </tr>--%>
    <tr style="height: 25px">
        <td style="width: 207px">
            Lavoratore
        </td>
        <td colspan="2">
            <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Data assunzione
        </td>
        <td>
            <asp:Label ID="LabelDataAssunzione" runat="server"></asp:Label>
        </td>
        <td colspan="2">
            <asp:Label ID="LabelNuovoAssunto" runat="server" Text="Nuovo assunto" Font-Bold="True"
                Font-Italic="True" ForeColor="Red" Visible="False"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Rapporto di lavoro
        </td>
        <td>
            <asp:Label ID="LabelRapportoLavoro" runat="server"></asp:Label>
        </td>
        <td colspan="2">
            <asp:Label ID="LabelPartTime" runat="server" Text="Part-time 50%" Font-Bold="True"
                Font-Italic="True" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Data inizio malattia/infortunio
        </td>
        <td colspan="2">
            <asp:Label ID="LabelInizioAssenza" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Periodo assenza
        </td>
        <td colspan="2">
            <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Tipo
        </td>
        <td colspan="2">
            <asp:Label ID="LabelTipo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Stato
        </td>
        <td colspan="2">
            <asp:Label ID="LabelStato" runat="server"></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td style="width: 207px">
            Data invio
        </td>
        <td>
            <asp:Label ID="LabelDataInvio" runat="server"></asp:Label>
        </td>
        <td>
            <asp:Button ID="ButtonRicevuta" Text = "Ricevuta" runat="server" OnClick="ButtonRicevuta_OnCLick"></asp:Button>
        </td>
    </tr>
    <tr style="height: 30px; background-color: #FFFF99;">
        <td style="border-style: solid none solid solid; border-width: 1px; border-color: #000000;">
            Ore settimanali
        </td>
        <td style="border-style: solid none solid none; border-width: 1px; border-color: #000000;">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxOreSettimanali" runat="server" MaxLength="2"
                MinValue="0" MaxValue="50" Value="0">
            </telerik:RadNumericTextBox>
        </td>
        <td style="border-style: solid solid solid none; border-width: 1px; border-color: #000000;">
            <asp:Button ID="ButtonOreSettimanali" runat="server" Text="Salva ore settimanali"
                OnClick="ButtonOreSettimanali_Click" />
        </td>
    </tr>
</table>
