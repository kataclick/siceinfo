﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Delegates;
using Cemi.MalattiaTelematica.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

public partial class MalattiaTelematica_WebControls_RicercaAssenzeFiltri : System.Web.UI.UserControl
{
    public event AssenzeFilterSelectedEventHandler OnFiltroSelected;

    private readonly BusinessEF bizEF = new BusinessEF();
    private readonly Business biz = new Business();

    protected void Page_Load(object sender, EventArgs e)
    {




        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            trFiltriImpresa1.Visible = false;
            trFiltriImpresa2.Visible = false;
            trFiltriImpresa3.Visible = false;
            trFiltriImpresa4.Visible = false;

            RadComboBoxInCarico.Visible = false;
        }
        if (!GestioneUtentiBiz.IsConsulente())
        {
            trSelezioneImpresa.Visible = false;
        }


        if (!Page.IsPostBack)
        {
            CaricaTipiAssenza();

            
            CaricaTipStatoMalattiaTelematica();

        }

    }

    private void CaricaTipStatoMalattiaTelematica()
    {
        Boolean sospesa;
        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            sospesa = false;
        }
        else
        {
            sospesa = true;
        }
        Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxStato, bizEF.GetTipiStatoMalattiaTelematica(sospesa),"Descrizione", "Id" );
    }

    private void CaricaTipiAssenza()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxTipo, bizEF.GetTipiAssenze(), "Descrizione", "Id");
    }


    protected void RadButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (OnFiltroSelected != null)
            {

                //AssenzeFilter filtro = NotificheFilter1.GetFiltro(false);

                
                AssenzeFilter filtro = CreaFiltro();
                biz.InsertFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), filtro, 0);
                
                OnFiltroSelected(filtro);
            }
        }
    }

    public void ImpostaFiltro(String idImpresa, String ragioneSociale, String codiceFiscale, 
                              String idLavoratore, String cognome, String nome, String dataNascita,
                              String statoAssenza, String tipoAssenza, String periodoDa, String periodoA,
                              String dataInvioDa, String dataInvioA)
    {

        RadNumericTextBoxIdImpresa.Text = idImpresa;
        RadTextBoxRagioneSociale.Text = ragioneSociale;
        RadTextBoxPIVA.Text = codiceFiscale;
        
        RadNumericTextBoxIdLavoratore.Text = idLavoratore;
        RadTextBoxCognomeLavoratore.Text = cognome;
        RadTextBoxNomeLavoratore.Text = nome;
        if (dataNascita != "")
        {
            RadDatePickerNascita.SelectedDate = DateTime.Parse(dataNascita);
        }
        RadTextBoxPeriodoDa.Text = periodoDa;
        RadTextBoxPeriodoA.Text = periodoA;

        RadComboBoxStato.SelectedValue = statoAssenza;
        RadComboBoxTipo.SelectedValue = tipoAssenza;
        if (!String.IsNullOrEmpty(dataInvioDa))
        {
            RadDatePickerInvioDa.SelectedDate = DateTime.Parse(dataInvioDa);
        }
        if (!String.IsNullOrEmpty(dataInvioA))
        {
            RadDatePickerInvioA.SelectedDate = DateTime.Parse(dataInvioA);
        }
        AssenzeFilter filtro = CreaFiltro();

        OnFiltroSelected(filtro);
    }

    private AssenzeFilter CreaFiltro()
    {
        AssenzeFilter filtro = new AssenzeFilter();


        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                filtro.IdImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
            }

            if (GestioneUtentiBiz.IsImpresa())
            {
                filtro.IdImpresa = ((Impresa)GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdImpresa;
            }
        }
        else
        {
            if (RadNumericTextBoxIdImpresa.Value.HasValue)
            {
                filtro.IdImpresa = (Int32)RadNumericTextBoxIdImpresa.Value.Value;
            }
        }
        filtro.RagioneSocialeImpresa = RadTextBoxRagioneSociale.Text;
        filtro.CodiceFiscaleImpresa = RadTextBoxPIVA.Text;

        if (RadNumericTextBoxIdLavoratore.Value.HasValue)
        {
            filtro.IdLavoratore = (Int32)RadNumericTextBoxIdLavoratore.Value.Value;
        }

        filtro.NomeLavoratore = Presenter.NormalizzaCampoTesto(RadTextBoxNomeLavoratore.Text);
        filtro.CognomeLavoratore = Presenter.NormalizzaCampoTesto(RadTextBoxCognomeLavoratore.Text);


        filtro.DataNascitaLavoratore = RadDatePickerNascita.SelectedDate;
       
        filtro.StatoAssenza = RadComboBoxStato.SelectedValue;
        filtro.TipoAssenza = RadComboBoxTipo.SelectedValue;
        if (!String.IsNullOrEmpty(RadTextBoxPeriodoDa.Text))
        {
            filtro.PeriodoDa = DateTime.Parse("01/" + RadTextBoxPeriodoDa.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxPeriodoA.Text))
        {
            filtro.PeriodoA = DateTime.Parse("01/" + RadTextBoxPeriodoA.Text);
        }
        if (RadComboBoxInCarico.SelectedValue == "InCarico")
        {
            filtro.IdUtente = GestioneUtentiBiz.GetIdUtente();
        }
        else if (RadComboBoxInCarico.SelectedValue == "NonInCarico")
        {
            filtro.IdUtente = -1;
        }

        filtro.DataInvioDa = RadDatePickerInvioDa.SelectedDate;
        filtro.DataInvioA = RadDatePickerInvioA.SelectedDate;

        return filtro;
    }

    #region custom validator
    protected void customValidatorPeriodoDa_ServerValidate(object source, ServerValidateEventArgs args)
    {

        args.IsValid = true;
        if (RadTextBoxPeriodoDa.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoDa.Text.PadLeft(7,'0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);

            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }
    }

    protected void customValidatorPeriodoA_ServerValidate(object source, ServerValidateEventArgs args)
    {

        args.IsValid = true;
        if (RadTextBoxPeriodoA.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoA.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }

        }

    }

    protected void customValidatorSelezionaImpresa(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata() < 0)
        {
            args.IsValid = false;
        }
    }

    protected void customValidatorPeriodoDaPeriodoMin_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoDa.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoDa.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
                if (data < new DateTime(2010,10,1))
                {
                    args.IsValid = false;
                }

            }
            catch (Exception)
            {
                //args.IsValid = false;
            }
        }
    }

    protected void customValidatorPeriodoAPeriodoMin_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoA.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoA.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
                if (data < new DateTime(2010, 10, 1))
                {
                    args.IsValid = false;
                }

            }
            catch (Exception)
            {
                //args.IsValid = false;
            }
        }
    }
    #endregion
}