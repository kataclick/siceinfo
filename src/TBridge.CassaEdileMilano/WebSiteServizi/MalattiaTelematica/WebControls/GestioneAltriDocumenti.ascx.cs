﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Delegates;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;

public partial class MalattiaTelematica_WebControls_GestioneAltriDocumenti : System.Web.UI.UserControl
{
    public event AltriDocumentiReturnedEventHandler OnAltriDocumentiReturned;
   
    private BusinessEF bizEF = new BusinessEF();
    private Business biz = new Business();
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CaricaAssenza(AssenzaDettagli assenza)
    {
        ViewState["IdAssenza"] = assenza.IdAssenza;
        ViewState["IdLavoratore"] = assenza.IdLavoratore;
        ViewState["IdImpresa"] = assenza.IdImpresa;

        RadButtonConferma.Enabled = biz.AbilitaControllo(assenza.IdStato);
    }


    private void Reset(Boolean carica)
    {
        RadTextBoxNome.Text = "";


        if (OnAltriDocumentiReturned != null)
        {
            OnAltriDocumentiReturned(carica);
        }
    }

    protected void RadButtonConferma_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            MalattiaTelematicaAltroDocumento altroDocumento = CreaAltroDocumento();

           
            //Inserimento
            try
            {
                bizEF.InsertAltroDocumento(altroDocumento);
                
                Reset(true);
            }
            catch
            {
                ErroreNellInserimento();
            }
           
        }

    }

    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento del documento.";
    }

    private MalattiaTelematicaAltroDocumento CreaAltroDocumento()
    {
        MalattiaTelematicaAltroDocumento altro = new MalattiaTelematicaAltroDocumento();

        altro.DataInserimentoRecord = DateTime.Now;
        altro.IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente();

        altro.Descrizione = RadTextBoxNome.Text;

        if (FileUploadAltroDocumento.HasFile)
        {
            using (Stream sr = FileUploadAltroDocumento.PostedFile.InputStream)
            {
                byte[] img = new byte[sr.Length];
                sr.Read(img, 0, (int)sr.Length);
                sr.Seek(0, SeekOrigin.Begin);
                altro.Immagine = img;
                altro.NomeFile = FileUploadAltroDocumento.FileName;
            }
        }

        altro.IdMalattiaTelematicaAssenza = (Int32) ViewState["IdAssenza"];
        return altro;

    }


    protected void RadButtonIndietro_Click(object sender, EventArgs e)
    {
        Reset(false);
    }
}