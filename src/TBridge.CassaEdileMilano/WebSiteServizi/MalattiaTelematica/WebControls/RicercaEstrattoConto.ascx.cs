﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Delegates;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.MalattiaTelematica.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using System.Text;
using Cemi.MalattiaTelematica.Type.Collections;

public partial class MalattiaTelematica_WebControls_RicercaEstrattoConto : System.Web.UI.UserControl
{

    public Business biz = new Business();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            trFiltriImpresa1.Visible = false;
            trFiltriImpresa2.Visible = false;

        }
        if (!GestioneUtentiBiz.IsConsulente())
        {
            trSelezioneImpresa.Visible = false;
        }

        #region Per prevenire click multipli

        StringBuilder sbInserisci = new StringBuilder();
        sbInserisci.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sbInserisci.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
        sbInserisci.Append(
            "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
        sbInserisci.Append("this.value = 'Attendere...';");
        sbInserisci.Append("this.disabled = true;");
        sbInserisci.Append(Page.ClientScript.GetPostBackEventReference(RadButtonOk, null));
        sbInserisci.Append(";");
        sbInserisci.Append("return true;");
        RadButtonOk.Attributes.Add("onclick", sbInserisci.ToString());

        #endregion

    }

    protected void RadButtonOk_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            CaricaEstrattoConto();
        }
        //if (OnFiltroSelected != null)
        //{
        //    EstrattoContoFilter filtro = CreaFiltro();
        //    OnFiltroSelected(filtro);
        //}

        //biz.GetPrestazioneDomandaAssenza()
    }

    private void CaricaEstrattoConto()
    {
        EstrattoContoFilter filtro = CreaFiltro();
        Presenter.CaricaElementiInGridView(RadGridEstrattoConto, biz.GetPrestazioneDomandaAssenza(filtro));
    }

    private EstrattoContoFilter CreaFiltro()
    {
        EstrattoContoFilter filtro = new EstrattoContoFilter();

        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                filtro.IdImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
            }
            if (GestioneUtentiBiz.IsImpresa())
            {
                filtro.IdImpresa = ((Impresa)GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdImpresa;
            }
        }
        else
        {
            if (RadNumericTextBoxIdImpresa.Value.HasValue)
            {
                filtro.IdImpresa = (Int32)RadNumericTextBoxIdImpresa.Value.Value;
            }
        }

        filtro.RagioneSocialeImpresa = RadTextBoxRagioneSociale.Text;
        filtro.CodiceFiscaleImpresa = RadTextBoxPIVA.Text;

        if (!String.IsNullOrEmpty(RadTextBoxPeriodoDa.Text))
        {
            filtro.PeriodoDa = DateTime.Parse("01/" + RadTextBoxPeriodoDa.Text);
        }
        if (!String.IsNullOrEmpty(RadTextBoxPeriodoA.Text))
        {
            filtro.PeriodoA = DateTime.Parse("01/" + RadTextBoxPeriodoA.Text);
        }

        return filtro;
    }

    protected void customValidatorPeriodoA_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoA.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoA.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }

        }
    }

    protected void customValidatorPeriodoDa_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoDa.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoDa.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
            }
            catch (Exception)
            {
                args.IsValid = false;
            }
        }
    }

    protected void RadGridEstrattoConto_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "stampa")
        {
            Int32 idAssenza = (Int32) RadGridEstrattoConto.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdAssenza"];


            Context.Items["IdAssenza"] = idAssenza;
            Context.Items["Tutti"] = true;

            Server.Transfer("~/MalattiaTelematica/VisualizzaEstrattoConto.aspx");
        }
    }

    protected void RadGridEstrattoConto_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            PrestazioneDomandaAssenza ass = (PrestazioneDomandaAssenza) e.Item.DataItem;
            Label lPrestazione = (Label) e.Item.FindControl("LabelPrestazione");

            
            // Prestazione

            lPrestazione.Text = String.Format("{0} - {1} / {2}",
                                              ass.TipoProtocollo, ass.NumeroProtocollo, ass.AnnoProtocollo);

        }
    }

    protected void RadGridEstrattoConto_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
       CaricaEstrattoConto();
    }

    protected void customValidatorSelezionaImpresa(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata() < 0)
        {
            args.IsValid = false;
        }
    }

    protected void customValidatorPeriodoDaMin_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoDa.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoDa.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
                if (data <= new DateTime(2010, 10, 1))
                {
                    args.IsValid = false;
                }
            }
            catch (Exception)
            {
                //args.IsValid = false;
            }
        }
    }

    protected void customValidatorPeriodoAMin_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        if (RadTextBoxPeriodoA.Text != "")
        {
            DateTime data;

            string newDate = "01/" + RadTextBoxPeriodoA.Text.PadLeft(7, '0');
            try
            {
                data = DateTime.ParseExact(newDate, "dd/MM/yyyy", null);
                if (data <= new DateTime(2010, 10, 1))
                {
                    args.IsValid = false;
                }
            }
            catch (Exception)
            {
                //args.IsValid = false;
            }
        }
    }
}