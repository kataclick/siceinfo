﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Delegates;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using System.IO;

public partial class MalattiaTelematica_WebControls_AltriDocumentiLista : UserControl
{
    public event AltriDocumentiSelectedEventHandler OnAltroDocumentoSelected;
   
    private readonly BusinessEF biz = new BusinessEF();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public void CaricaAltriDocumenti(int idAssenza, Boolean abilita)
    {
        ViewState["IdAssenza"] = idAssenza;
        ViewState["AbilitaControlli"] = abilita;

        CaricaAltriDocumenti();
        
        RadButtonAggiungiDocumenti.Enabled = abilita;
    }

    private void CaricaAltriDocumenti()
    {
        Presenter.CaricaElementiInGridView(RadGridAltriDocumenti, biz.GetAltriDocumenti((Int32)ViewState["IdAssenza"]));
        
    }


    protected void RadGridAltriDocumenti_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "elimina":
                {
                    Int32 idAltroDocumento = (Int32)
                                             RadGridAltriDocumenti.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                    biz.DeleteAltroDocumento(idAltroDocumento);

                    CaricaAltriDocumenti();
                }
                break;
            case "visualizza":
                {
                    Int32 idAltroDocumento = (Int32)
                                             RadGridAltriDocumenti.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                    RestituisciFile(biz.GetImmagineAltroDocumento(idAltroDocumento));
                }
                break;
        }
    }

    private void RestituisciFile(Immagine img)
    {
        string idArchidoc = img.IdArchidoc;

        if (idArchidoc != null)
        {
            // Caricare il documento tramite il Web Service Archidoc
            try
            {
                IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                if (file != null)
                {
                    Presenter.RestituisciFileArchidoc(idArchidoc, file, Path.GetExtension(img.NomeFile).ToUpper(), this.Page);
                }
            }
            catch
            {
                //    LabelErroreVisualizzazioneDocumento.Visible = true;
            }

        }
        else
        {
            //Set the appropriate ContentType.
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", img.NomeFile));
            switch (Path.GetExtension(img.NomeFile).ToUpper())
            {
                case ".XML":
                    Response.ContentType = "text/plain";
                    break;
                case ".PDF":
                    Response.ContentType = "application/pdf";
                    break;
                default:
                    Response.ContentType = "application/download";
                    break;

            }
            //Write the file directly to the HTTP content output stream.
            Response.BinaryWrite(img.File);
            Response.Flush();
            Response.End();
        }
    }

    protected void RadButtonAggiungiDocumenti_Click(object sender, EventArgs e)
    {
        if (OnAltroDocumentoSelected != null)
        {
            OnAltroDocumentoSelected();
        }
    }

    protected void ButtonElimina_Click(object sender, EventArgs e)
    {

    }
    protected void RadGridAltriDocumenti_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            ImageButton ibElimina = (ImageButton)e.Item.FindControl("ButtonElimina");
            
            ibElimina.Enabled = (Boolean)ViewState["AbilitaControlli"];
        }
    }
}