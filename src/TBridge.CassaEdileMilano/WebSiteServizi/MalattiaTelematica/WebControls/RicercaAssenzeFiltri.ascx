﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicercaAssenzeFiltri.ascx.cs"
    Inherits="MalattiaTelematica_WebControls_RicercaAssenzeFiltri" %>
<%@ Register Src="../../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa"
    TagPrefix="uc1" %>
<asp:Panel ID="Panel1" runat="server" DefaultButton="RadButtonOk">
    <table class="standardTable">
        <tr runat="server" id="trSelezioneImpresa">
            <td colspan="4">
                <uc1:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
                <asp:CustomValidator ID="customValidator3" runat="server" ValidationGroup="Ricerca"
                    ErrorMessage="Selezionare un'impresa" OnServerValidate="customValidatorSelezionaImpresa">*
                </asp:CustomValidator>
            </td>
        </tr>
        <tr runat="server" id="trFiltriImpresa1">
            <td>
                Codice Impresa
            </td>
            <td>
                Rag. sociale Impresa
            </td>
            <td>
                P.IVA/C.F. Impresa
            </td>
            <td>
            </td>
        </tr>
        <tr runat="server" id="trFiltriImpresa2">
            <td>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresa" runat="server" Type="Number"
                    DataType="System.Int32" MinValue="1">
                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                </telerik:RadNumericTextBox>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" />
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPIVA" runat="server" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Codice Lav.
            </td>
            <td>
                Cognome Lav.
            </td>
            <td>
                Nome Lav.
            </td>
            <td>
                Data nascita Lav.
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxIdLavoratore" runat="server" Type="Number"
                    DataType="System.Int32" MinValue="1">
                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                </telerik:RadNumericTextBox>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxCognomeLavoratore" runat="server" />
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxNomeLavoratore" runat="server" />
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerNascita" runat="server">
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td>
                Stato
            </td>
            <td>
                Tipo assenza
            </td>
            <td>
                Periodo da (MM/AAAA)
            </td>
            <td>
                Periodo a (MM/AAAA)
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadComboBox ID="RadComboBoxStato" runat="server" AppendDataBoundItems="true">
                </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipo" runat="server" AppendDataBoundItems="true">
                </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPeriodoDa" runat="server" MaxLength="7">
                </telerik:RadTextBox>
                <asp:CustomValidator ID="customValidatorPeriodoDa" runat="server" ValidationGroup="Ricerca"
                    ErrorMessage="Formato data errato" OnServerValidate="customValidatorPeriodoDa_ServerValidate">*
                </asp:CustomValidator>
                <asp:CustomValidator ID="customValidatorPeriodoDaMin" runat="server" ValidationGroup="Ricerca"
                    ErrorMessage="Data minore di 10/2010" OnServerValidate="customValidatorPeriodoDaPeriodoMin_ServerValidate">*
                </asp:CustomValidator>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPeriodoA" runat="server" MaxLength="7">
                </telerik:RadTextBox>
                <asp:CustomValidator ID="customValidatorPeriodoA" runat="server" ValidationGroup="Ricerca"
                    ErrorMessage="Formato data errato" OnServerValidate="customValidatorPeriodoA_ServerValidate">*
                </asp:CustomValidator>
                <asp:CustomValidator ID="customValidatorPeriodoAMin" runat="server" ValidationGroup="Ricerca"
                    ErrorMessage="Data minore di 10/2010" OnServerValidate="customValidatorPeriodoAPeriodoMin_ServerValidate">*
                </asp:CustomValidator>
                <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadTextBoxPeriodoDa"
                    ControlToValidate="RadTextBoxPeriodoA" EnableClientScript="False" ErrorMessage="Limiti periodi errati"
                    Operator="GreaterThanEqual" ValidationGroup="Ricerca" Type="Date">*
                </asp:CompareValidator>
            </td>
        </tr>
        <tr runat="server" id="trFiltriImpresa3">
            <td>
                In Carico
            </td>
            <td>
                Data Invio da
            </td>
            <td>
                Data Invio a
            </td>
            <td>
            </td>
        </tr>
        <tr runat="server" id="trFiltriImpresa4">
            <td>
                <telerik:RadComboBox ID="RadComboBoxInCarico" runat="server">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutte" Value="Tutte" />
                        <telerik:RadComboBoxItem runat="server" Text="In carico" Value="InCarico" />
                        <telerik:RadComboBoxItem runat="server" Text="Non in carico" Value="NonInCarico" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerInvioDa" runat="server">
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerInvioA" runat="server">
                </telerik:RadDatePicker>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                    ValidationGroup="Ricerca" />
            </td>
            <td>
                <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                    ValidationGroup="Ricerca" />
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:View> 