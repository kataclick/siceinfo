﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicercaAssenzeLiquidateImprese.ascx.cs"
    Inherits="MalattiaTelematica_WebControls_RicercaAssenzeLiquidateImprese" %>
<%@ Register Src="RicercaAssenzeFiltri.ascx" TagName="RicercaAssenzeFiltri" TagPrefix="uc1" %>
<div class="standardDiv">
    <uc1:RicercaAssenzeFiltri ID="RicercaAssenzeFiltri1" runat="server" />
</div>
<div class="standardDiv">
    <telerik:RadGrid ID="RadGridAssenzeLiquidate" runat="server" GridLines="None" OnItemDataBound="RadGridAssenzeLiquidate_ItemDataBound"
        Width="100%" AllowPaging="True" OnPageIndexChanged="RadGridAssenzeLiquidate_PageIndexChanged"
        CellSpacing="0" OnItemCommand="RadGridAssenzeLiquidate_ItemCommand">
        <MasterTableView DataKeyNames="IdAssenza">
            <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Codice Lav." UniqueName="idLavoratore" DataField="Lavoratore.Id">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Cognome" UniqueName="cognomeLavoratore" DataField="Lavoratore.Cognome">
                    <ItemStyle Width="100px" Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Nome" UniqueName="nomeLavoratore" DataField="Lavoratore.Nome">
                    <ItemStyle Width="100px" Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Stato" UniqueName="stato" DataField="TipoStatoMalattiaTelematica.Descrizione">
                    <ItemStyle Width="50px" />
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Tipo assenza" UniqueName="tipoAssenza" DataField="TipoAssenza.Descrizione">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="periodo">
                    <ItemTemplate>
                        <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="GG" UniqueName="gg" DataField="giorniAssenza">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Importo" UniqueName="importo" DataField="Assenza.ImportoAmmissibile"
                    DataFormatString="{0:C2}">
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="stampa" FilterControlAltText="Filter column column"
                    ImageUrl="~/images/pdf24.png" UniqueName="column">
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
</div>
