﻿using System;
using System.Web.UI;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.GestioneUtenti.Business;

public partial class MalattiaTelematica_WebControls_DettagliAssenza : UserControl
{
    private readonly BusinessEF biz = new BusinessEF();

    Boolean? OreSettimanaliEnabled
    {
        set { ViewState["OreSettimanaliEnabled"] = value; }
        get { return ViewState["OreSettimanaliEnabled"] as Boolean?;}
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
        //if (!Page.IsPostBack)
        //{
        //    CaricaAssenza((int) ViewState["IdAssenza"]);
        //}
        ButtonOreSettimanali.Enabled = OreSettimanaliEnabled.HasValue ? (Boolean)OreSettimanaliEnabled : true;
        RadNumericTextBoxOreSettimanali.Enabled = OreSettimanaliEnabled.HasValue ? (Boolean)OreSettimanaliEnabled : true;
        if (GestioneUtentiBiz.IsImpresa())
        {
            trImpresa.Visible = false;
        }

        if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
        {
            trPrestazione.Visible = false;
        }
    }



    public void EnabledOreSettimanali(Boolean enabled)
    {
        OreSettimanaliEnabled = enabled;
    }


    public void CaricaAssenza(AssenzaDettagli assenzaDet)
    {
        ViewState["IdAssenza"] = assenzaDet.IdAssenza;
        ViewState["IdLavoratore"] = assenzaDet.IdLavoratore;
        ViewState["IdImpresa"] = assenzaDet.IdImpresa;


        LabelPrestazione.Text = assenzaDet.Prestazione;
        LabelImpresa.Text = String.Format("{0} - {1}",assenzaDet.IdImpresa.ToString(), assenzaDet.RagioneSocialeImpresa);

       // LabelIdLavoratore.Text = ;
        LabelLavoratore.Text = String.Format("{0} - {1}", assenzaDet.IdLavoratore.ToString(), assenzaDet.NomeCompleto);

        if (assenzaDet.DataAssunzione.HasValue)
        {
            LabelDataAssunzione.Text = assenzaDet.DataAssunzione.Value.ToShortDateString();
            if (assenzaDet.DataAssunzione.Value.AddMonths(3) >= assenzaDet.DataInizioAssenza.Value)
            {
                LabelNuovoAssunto.Visible = true;
            }
        }

        LabelRapportoLavoro.Text = assenzaDet.RapportoLavoro;
        LabelPartTime.Text = assenzaDet.PartTime;

        if (assenzaDet.DataInizioMalattia != null)
        {
            LabelInizioAssenza.Text = assenzaDet.DataInizioMalattia.Value.ToShortDateString() ;
        }

        if (assenzaDet.DataInizioAssenza.HasValue && assenzaDet.DataFineAssenza.HasValue)
        {
            LabelPeriodo.Text = assenzaDet.DataInizioAssenza.Value.ToShortDateString() + " - " +
                                assenzaDet.DataFineAssenza.Value.ToShortDateString() + "   [" + assenzaDet.TipoAssenza + "]";
        }
        LabelTipo.Text = assenzaDet.Tipo;

        LabelStato.Text = assenzaDet.Stato;

        if (assenzaDet.DataInvio.HasValue)
        {
            LabelDataInvio.Text = assenzaDet.DataInvio.Value.ToShortDateString();
        }

        ButtonRicevuta.Enabled = assenzaDet.DataInvio.HasValue;

        Double? ore = 0;
        if (assenzaDet.OreSettimanali == null)
        {
            if (assenzaDet.PercentualePT == 0)
            {
                ore = 40;
            }
            else
            {
                ore = 40*assenzaDet.PercentualePT/100;
            }
        }
        else
        {
            ore = assenzaDet.OreSettimanali;
        }

        
        RadNumericTextBoxOreSettimanali.Value = ore.HasValue ? ore : 0;
    }


    protected void ButtonOreSettimanali_Click(object sender, EventArgs e)
    {
        ConfermaOreSettimanali();
    }

    public void ConfermaOreSettimanali()
    {
        if (RadNumericTextBoxOreSettimanali.Value.HasValue)
        {
            biz.UpdateMalattiaTelematicaAssenzeOreSettimanali((Int32)ViewState["IdAssenza"], (Int32)RadNumericTextBoxOreSettimanali.Value);
        }
    }

    protected void ButtonRicevuta_OnCLick(object sender, EventArgs e)
    {
        Context.Items["IdAssenza"] = (int)ViewState["IdAssenza"];
        Server.Transfer("~/MalattiaTelematica/VisualizzaRicevuta.aspx");
    }
}