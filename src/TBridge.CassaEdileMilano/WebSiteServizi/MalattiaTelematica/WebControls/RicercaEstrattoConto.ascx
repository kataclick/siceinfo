﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RicercaEstrattoConto.ascx.cs"
    Inherits="MalattiaTelematica_WebControls_RicercaEstrattoConto" %>
<%@ Register TagPrefix="uc1" TagName="consulenteselezioneimpresa" Src="~/WebControls/ConsulenteSelezioneImpresa.ascx" %>
<div class="standardDiv">
    <table class="standardTable">
        <tr runat="server" id="trSelezioneImpresa">
            <td colspan="4">
                <uc1:consulenteselezioneimpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
                <asp:CustomValidator ID="customValidator3" runat="server" ValidationGroup="Ricerca"
                    ErrorMessage="Selezionare un'impresa" OnServerValidate="customValidatorSelezionaImpresa">*
                </asp:CustomValidator>
            </td>
        </tr>
        <tr runat="server" id="trFiltriImpresa1">
            <td>
                Codice Impresa
            </td>
            <td>
                Rag. sociale Impresa
            </td>
            <td>
                P.IVA/C.F. Impresa
            </td>
        </tr>
        <tr runat="server" id="trFiltriImpresa2">
            <td>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresa" runat="server" Type="Number"
                    DataType="System.Int32" MinValue="1">
                    <NumberFormat GroupSeparator="" DecimalDigits="0" />
                </telerik:RadNumericTextBox>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" />
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPIVA" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Periodo da (MM/AAAA)
            </td>
            <td>
                Periodo a (MM/AAAA)
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPeriodoDa" runat="server" MaxLength="7">
                </telerik:RadTextBox>
                <asp:CustomValidator ID="customValidatorPeriodoDa" runat="server" ValidationGroup="Ricerca"
                    ControlToValidate="RadTextBoxPeriodoDa" ErrorMessage="Formato data errato" OnServerValidate="customValidatorPeriodoDa_ServerValidate">*
                </asp:CustomValidator>
                <asp:CustomValidator ID="customValidatorPeriodoDaMin" runat="server" ValidationGroup="Ricerca"
                    ControlToValidate="RadTextBoxPeriodoDa" ErrorMessage="Data minore di 10/2010"
                    OnServerValidate="customValidatorPeriodoDaMin_ServerValidate">*
                </asp:CustomValidator>
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPeriodoA" runat="server" MaxLength="7">
                </telerik:RadTextBox>
                <asp:CustomValidator ID="customValidatorPeriodoA" runat="server" ValidationGroup="Ricerca"
                    ControlToValidate="RadTextBoxPeriodoA" ErrorMessage="Formato data errato" OnServerValidate="customValidatorPeriodoA_ServerValidate">*
                </asp:CustomValidator>
                <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadTextBoxPeriodoDa"
                    ControlToValidate="RadTextBoxPeriodoA" EnableClientScript="False" ErrorMessage="Limiti periodi errati"
                    Operator="GreaterThanEqual" ValidationGroup="Ricerca" Type="Date">*
                </asp:CompareValidator>
                <asp:CustomValidator ID="customValidatorPeriodoAMin" runat="server" ValidationGroup="Ricerca"
                    ControlToValidate="RadTextBoxPeriodoA" ErrorMessage="Data minore di 10/2010"
                    OnServerValidate="customValidatorPeriodoAMin_ServerValidate">*
                </asp:CustomValidator>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan= "2">
                <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                    ValidationGroup="Ricerca" />
            </td>
            <td>
                <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                    ValidationGroup="Ricerca" />
            </td>
        </tr>
    </table>
</div>
<br />
<div class="standardDiv">
    <telerik:RadGrid ID="RadGridEstrattoConto" runat="server" GridLines="None" OnItemDataBound="RadGridEstrattoConto_ItemDataBound"
        Width="100%" OnPageIndexChanged="RadGridEstrattoConto_PageIndexChanged" CellSpacing="0"
        OnItemCommand="RadGridEstrattoConto_ItemCommand" AllowPaging="True">
        <MasterTableView DataKeyNames="IdAssenza,AnnoProtocollo,NumeroProtocollo">
            <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Codice Impresa" UniqueName="idImpresa" DataField="IdImpresa">
					<ItemStyle Width="40px" />
				</telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Ragione Sociale" UniqueName="ragioneSociale"
                    DataField="RagioneSociale">
                    <ItemStyle Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Codice fiscale/ Partita IVA" UniqueName="codiceFiscale"
                    DataField="CodiceFiscale">
 					<ItemStyle Width="100px" />
               </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Prestazione" UniqueName="prestazione">
                    <ItemTemplate>
                        <asp:Label ID="LabelPrestazione" runat="server"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Data" UniqueName="data" DataField="Data" DataType="System.DateTime"
                    DataFormatString="{0:d}">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Numero assenze" UniqueName="numero" DataField="Numero">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Importo" UniqueName="importo" DataField="Importo"
                    DataFormatString="{0:C2}">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" Width="70px"/>
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="stampa" FilterControlAltText="Filter column column"
                    ImageUrl="~/images/pdf24.png" UniqueName="column">
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
</div>
