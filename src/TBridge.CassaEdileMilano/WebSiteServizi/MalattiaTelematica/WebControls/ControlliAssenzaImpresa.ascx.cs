﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Type.Domain;

public partial class MalattiaTelematica_WebControls_ControlliAssenzaImpresa : System.Web.UI.UserControl
{

    private readonly Business biz = new Business();
    private readonly BusinessEF bizEF = new BusinessEF();
    private readonly Common commonBiz = new Common();
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Controlli();
        }
    }

    private void Controlli()
    {
        CaricaCasseEdili();

        ControlloAssenzaSovrapposta();

        ControlloCongruenzaPeriodi();

        ControlloRapportoLavoro();

        ControlloImpresaRegolare();

        ControlloAssenzaPrecedente();

        if (ViewState["PercentualePT"] != null)
        {
            ControlloOre((Int32)ViewState["IdLavoratore"], (DateTime)ViewState["DataInizioMalattia"],
                         (Int32)ViewState["PercentualePT"], (Int32)ViewState["IdAssenza"]);
        }

        RicadutaMalattiaOltre30GG();
    }

    public void CaricaAssenza(AssenzaDettagli assenza)
    {
        ViewState["IdAssenza"] = assenza.IdAssenza;
        ViewState["IdLavoratore"] = assenza.IdLavoratore;
        ViewState["IdImpresa"] = assenza.IdImpresa;
        ViewState["DataInizioMalattia"] = assenza.DataInizioMalattia;
        ViewState["DataInizioAssenza"] = assenza.DataInizioAssenza;
        ViewState["DataFineAssenza"] = assenza.DataFineAssenza;
        ViewState["PercentualePT"] = assenza.PercentualePT;
        ViewState["TipoAssenza"] = assenza.TipoAssenza;
        ViewState["Ricaduta"] = assenza.Ricaduta;
        ViewState["IdTipo"] = assenza.IdTipo;
        ViewState["DataAssunzione"] = assenza.DataAssunzione;
        ViewState["IdStato"] = assenza.IdStato;

        ViewState["InizioRapporto"] = assenza.DataInizio;
        ViewState["FineRapporto"] = assenza.DataFine;
        

        if (Context.Items["FiltroIdLavoratore"] != null)
        {
            ViewState["FiltroIdLavoratore"] = Context.Items["FiltroIdLavoratore"];
        }
        if (Context.Items["FiltroCognome"] != null)
        {
            ViewState["FiltroCognome"] = Context.Items["FiltroCognome"];
        }
        if (Context.Items["FiltroNome"] != null)
        {
            ViewState["FiltroNome"] = Context.Items["FiltroNome"];
        }
        if (Context.Items["FiltroDataNascita"] != null)
        {
            ViewState["FiltroDataNascita"] = Context.Items["FiltroDataNascita"];
        }
        if (Context.Items["FiltroStatoAssenza"] != null)
        {
            ViewState["FiltroStatoAssenza"] = Context.Items["FiltroStatoAssenza"];
        }
        if (Context.Items["FiltroTipoAssenza"] != null)
        {
            ViewState["FiltroTipoAssenza"] = Context.Items["FiltroTipoAssenza"];
        }
        if (Context.Items["FiltroPeriodoDa"] != null)
        {
            ViewState["FiltroPeriodoDa"] = Context.Items["FiltroPeriodoDa"];
        }
        if (Context.Items["FiltroPeriodoA"] != null)
        {
            ViewState["FiltroPeriodoA"] = Context.Items["FiltroPeriodoA"];
        }
        if (Context.Items["FiltroRagioneSociale"] != null)
        {
            ViewState["FiltroRagioneSociale"] = Context.Items["FiltroRagioneSociale"];
        }
        if (Context.Items["FiltroCodiceFiscale"] != null)
        {
            ViewState["FiltroCodiceFiscale"] = Context.Items["FiltroCodiceFiscale"];
        }
        if (Context.Items["FiltroIdImpresa"] != null)
        {
            ViewState["FiltroIdImpresa"] = Context.Items["FiltroIdImpresa"];
        }
        Controlli();
    }


    private void CaricaCasseEdili()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCassaEdile,
            commonBiz.GetCasseEdili(),
            "Descrizione",
            "IdCassaEdile");
    }

    private void ControlloAssenzaSovrapposta()
    {
        if (bizEF.AssenzeSovrapposte((Int32) ViewState["IdLavoratore"], (DateTime) ViewState["DataInizioAssenza"],
                                     (DateTime) ViewState["DataFineAssenza"], (Int32) ViewState["IdAssenza"]))
        {
            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
            {
                ImageSovrapposizioneAssenza.ImageUrl = "~/images/semaforoGiallo.png";
            }
            else
            {
                ImageSovrapposizioneAssenza.ImageUrl = "~/images/semaforoRosso.png";
            }
        }
        else
        {
            ImageSovrapposizioneAssenza.ImageUrl = "~/images/semaforoVerde.png";
        }
    }

    private void ControlloCongruenzaPeriodi()
    {
        if (bizEF.PeriodiCongruenti((Int32)ViewState["IdAssenza"], (DateTime)ViewState["DataInizioMalattia"]))
        {
            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
            {
                ImageCongruenzaPeriodi.ImageUrl = "~/images/semaforoGiallo.png";
            }
            else
            {

                ImageCongruenzaPeriodi.ImageUrl = "~/images/semaforoRosso.png";
            }
        }
        else
        {
            ImageCongruenzaPeriodi.ImageUrl = "~/images/semaforoVerde.png";
        }
    }

    private void ControlloRapportoLavoro()
    {
        if ((String)ViewState["IdStato"] == "R" || (String)ViewState["IdStato"] == "7" || (String)ViewState["IdStato"] == "6")
        {
            ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoGiallo.png";
        }
        else
        {
            DateTime? inizioRapporto = (DateTime?) ViewState["InizioRapporto"];
            DateTime? fineRapporto =  (DateTime?) ViewState["FineRapporto"];
            DateTime inizioAssenza = (DateTime)ViewState["DataInizioAssenza"];
            DateTime fineAssenza = (DateTime)ViewState["DataFineAssenza"];

            if (ViewState["DataAssunzione"] != null)
            {
                if (inizioAssenza >= inizioRapporto && inizioAssenza <= fineRapporto
                    &&
                    fineAssenza >= inizioRapporto && fineAssenza <= fineRapporto)
                {
                    ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoVerde.png";
                }
                else
                {
                    if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
                    {
                        ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoGiallo.png";
                    }
                    else
                    {
                        ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoRosso.png";
                    }
                }
            }
            else
            {
                if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
                {
                    ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoGiallo.png";
                }
                else
                {
                    ImageControlloRapportoLavoro.ImageUrl = "~/images/semaforoRosso.png";
                }
            }
        }
    }

    private void ControlloImpresaRegolare()// todo implementare
    {
        //regolare
        ImageControlloImpresaRegolare.ImageUrl = "~/images/semaforoVerde.png";
        ButtonFreccia.Visible = false;

        int idImpresa = (int)ViewState["IdImpresa"];
        DateTime dataFine = (DateTime)ViewState["DataInizioMalattia"];
        DateTime dataInizio = dataFine.AddMonths(-4);


        decimal debito = RegolaritaContributivaManager.GetDebitoImpresa(idImpresa, dataInizio, dataFine);

        if (debito >= 30.0M)
        {
            ImageControlloImpresaRegolare.ImageUrl = "~/images/semaforoGiallo.png";
        }


        //non regolare
        //ImageControlloImpresaRegolare.ImageUrl = "~/images/semaforoGiallo.png";
        //ButtonFreccia.Enabled = true;
        
    }

    private void ControlloOre (Int32 idLav, DateTime inizio, Int32 percPT, Int32 idAss)
    {

        Decimal oreLAv = biz.GetOreLavorate(idLav, inizio, (DateTime)ViewState["DataAssunzione"], idAss);

        if (percPT == 0)
        {
            percPT = 100;
        }

        LabelOreLavorate.Text = oreLAv.ToString("N0");

        if (oreLAv >= (biz.GetOreDaLavorare() / 100.0M * percPT))
        {
            ImageControlloOre.ImageUrl = "~/images/semaforoVerde.png";
            //ButtonControlloOreLavorate.Enabled = false;
        }
        else
        {
            ImageControlloOre.ImageUrl = "~/images/semaforoGiallo.png";
            //ButtonControlloOreLavorate.Enabled = true;
        }
    }

    private void ControlloAssenzaPrecedente()
    {
        ButtonAltreCE.Enabled = false;
        LabelAltraCE.Text = "";
        ImageAltraCE.ImageUrl = "~/images/semaforoVerde.png";
        Boolean semaforo = false;
        if ((DateTime)ViewState["DataInizioMalattia"] != (DateTime)ViewState["DataInizioAssenza"])
        {
            if (bizEF.AssenzaMancante((Int32)ViewState["IdAssenza"], (Int32)ViewState["IdLavoratore"],
                                      (DateTime)ViewState["DataInizioMalattia"],
                                      (DateTime)ViewState["DataInizioAssenza"],
                                      (Boolean)ViewState["Ricaduta"],
                                      (String)ViewState["IdTipo"]))
            {
                ButtonAltreCE.Enabled = true;
                semaforo = true;
                if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
                {
                    ImageAltraCE.ImageUrl = "~/images/semaforoGiallo.png";
                    LabelAltraCE.Text = "Verrete contattati da CE";
                }
                else
                {
                    ImageAltraCE.ImageUrl = "~/images/semaforoRosso.png";
                }
            }

            if (!semaforo)
            {
                string s = bizEF.StatoAssenzaPrecedente((Int32)ViewState["IdAssenza"], (Int32)ViewState["IdLavoratore"], (DateTime)ViewState["DataInizioAssenza"]);

                if (s == "I" || s == "6")
                {
                    ImageAltraCE.ImageUrl = "~/images/semaforoRosso.png";
                    LabelAltraCE.Text = "Attenzione, non è stato confermato il periodo di assenza precedente";
                }
                if (s == "8" || s == "7" || s == "9" || s == "M" || s == "S")
                {
                    ImageAltraCE.ImageUrl = "~/images/semaforoGiallo.png";
                }
            }
        }

        if (!GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
        {
            ButtonAltreCE.Enabled = true;
        }

    }

    private void RicadutaMalattiaOltre30GG()
    {
        //DateTime inizioMalattia = (DateTime) ViewState["DataInizioMalattia"];
        //DateTime inizioAssenza = (DateTime) ViewState["DataInizioAssenza"];

        ImageRicadutaOltre30GG.ImageUrl = "~/images/semaforoVerde.png";
        LabelRicadutaOltre30GG.Text = "";

        if ((String)ViewState["IdTipo"] == "MA")
        {
            if ((Boolean)ViewState["Ricaduta"])
            {
                if (bizEF.RicadutaOltre30GG((Int32)ViewState["IdAssenza"], (Int32)ViewState["IdLavoratore"],
                                     (DateTime)ViewState["DataInizioMalattia"],
                                     (DateTime)ViewState["DataInizioAssenza"]))

                    if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
                    {
                        ImageRicadutaOltre30GG.ImageUrl = "~/images/semaforoGiallo.png";
                        LabelRicadutaOltre30GG.Text = "Verrete contattati da CE";
                    }
                    else
                    {
                        ImageRicadutaOltre30GG.ImageUrl = "~/images/semaforoRosso.png";
                    }

            }
        }
    }

    protected void ButtonControlloOreLavorate_Click(object sender, EventArgs e)
    {
        Context.Items["IdAssenza"] = (Int32)ViewState["IdAssenza"];
        
        //Context.Items["FiltroRagioneSociale"] = ViewState["FiltroRagioneSociale"].ToString();
        //Context.Items["FiltroCodiceFiscale"] = ViewState["FiltroCodiceFiscale"].ToString();
        //Context.Items["FiltroIdLavoratore"] = ViewState["FiltroIdLavoratore"].ToString();
        //Context.Items["FiltroCognome"] = ViewState["FiltroCognome"].ToString();
        //Context.Items["FiltroNome"] = ViewState["FiltroNome"].ToString();
        //Context.Items["FiltroDataNascita"] = ViewState["FiltroDataNascita"].ToString();
        //Context.Items["FiltroStatoAssenza"] = ViewState["FiltroStatoAssenza"].ToString();
        //Context.Items["FiltroTipoAssenza"] = ViewState["FiltroTipoAssenza"].ToString();
        //Context.Items["FiltroPeriodoDa"] = ViewState["FiltroPeriodoDa"].ToString();
        //Context.Items["FiltroPeriodoA"] = ViewState["FiltroPeriodoA"].ToString();
        
        Server.Transfer("~/MalattiaTelematica/ControlloOreCNCE.aspx");
    }
    protected void ButtonAltreCE_Click(object sender, EventArgs e)
    {
        PanelAltraCE.Visible = true;
    }

    protected void RadButtonConfermaAltraCe_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            MalattiaTelematicaAssenza assenza = CreaAssenza();
            RadButtonOK.Visible = true;

            //Inserimento
            try
            {
                bizEF.InsertAssenzaAltraCE(assenza);

                LabelMessaggio.Text = "Ricordarsi di inserire i giustificativi per questo periodo di assenza.";
            }
            catch
            {
                ErroreNellInserimento();
            }

        }

    }

    protected void RadButtonOK_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void RadButtonAnnullaAltraCe_Click(object sender, EventArgs e)
    {
        Reset();
    }

    private void Reset()
    {
        RadDatePickerPeriodoDa.SelectedDate = null;
        RadDatePickerPeriodoA.SelectedDate = null;
        DropDownListCassaEdile.SelectedValue = null;

        LabelMessaggio.Text = "";
        RadButtonOK.Visible = false;

        PanelAltraCE.Visible = false;

    }

    private void ErroreNellInserimento()
    {
        LabelMessaggio.Text = "Errore durante l'inserimento dell'assenza.";
    }

    private MalattiaTelematicaAssenza CreaAssenza()
    {
        MalattiaTelematicaAssenza assenza = new MalattiaTelematicaAssenza();

        assenza.DataInserimentoRecord = DateTime.Now;
        assenza.AltraCE = DropDownListCassaEdile.SelectedItem.ToString();
        assenza.DataInizio = RadDatePickerPeriodoDa.SelectedDate;
        assenza.DataFine = RadDatePickerPeriodoA.SelectedDate;
        assenza.Tipo = (String) ViewState["IdTipo"];

        assenza.Stato = "6";
        assenza.IdImpresa = (Int32)ViewState["IdImpresa"];
        assenza.IdLavoratore = (Int32)ViewState["IdLavoratore"];
        assenza.DataInizioMalattia = (DateTime)ViewState["DataInizioMalattia"];

        return assenza;

    }
}