﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RicercaAssenzeBackOffice.aspx.cs" Inherits="MalattiaTelematica_RicercaAssenzeBackOffice"
    Theme="CETheme2009Wide" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/RicercaAssenzeBackOffice.ascx" TagName="RicercaAssenzeBackOffice"
    TagPrefix="uc3" %>
<%@ Register src="../WebControls/MenuMalattiaTelematica.ascx" tagname="MenuMalattiaTelematica" tagprefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Gestione assenze" />
    <br />
    <uc3:RicercaAssenzeBackOffice ID="RicercaAssenzeBackOffice1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc2:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>

