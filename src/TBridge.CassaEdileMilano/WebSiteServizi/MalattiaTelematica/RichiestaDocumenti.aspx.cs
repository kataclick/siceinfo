﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using System.Configuration;
using System.Net;
using TBridge.Cemi.Presenter;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using System.Text.RegularExpressions;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type.Entities;


public partial class MalattiaTelematica_RichiestaDocumenti : System.Web.UI.Page
{
    private readonly BusinessEF bizEF = new BusinessEF();

    String impresaRagioneSociale
    {
        set { ViewState["impresaRagioneSociale"] = value;  }
        get { return ViewState["impresaRagioneSociale"] as String; }
    }
    String impresaEmail
    {
        set { ViewState["impresaEmail"] = value; }
        get { return ViewState["impresaEmail"] as String; }
    }
    String lavoratoreCognome
    {
        set { ViewState["lavoratoreCognome"] = value; }
        get { return ViewState["lavoratoreCognome"] as String; }
    }
    String lavoratoreNome
    {
        set { ViewState["lavoratoreNome"] = value; }
        get { return ViewState["lavoratoreNome"] as String; }
    }
    DateTime? lavoratoreDataNascita
    {
        set { ViewState["lavoratoreDataNascita"] = value; }
        get { return ViewState["lavoratoreDataNascita"] as DateTime?; }
      
    }
    //DateTime lavoratoreDataNascita = new DateTime(1970, 1, 1);
    DateTime? malattiaInizio
    {
        set { ViewState["malattiaInizio"] = value; }
        get { return ViewState["malattiaInizio"] as DateTime?; }
    }
    //DateTime? malattiaInizio = new DateTime(2012, 2, 1);
    Int32? lavoratoreCodice
    {
        set { ViewState["lavoratoreCodice"] = value; }
        get { return ViewState["lavoratoreCodice"] as Int32?; }
    }

    DateTime? inAttesa
    {
        set { ViewState["inAttesa"] = value; }
        get { return ViewState["inAttesa"] as DateTime?; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!Page.IsPostBack)
        {
            int idAssenza = -1;
            int tipoRichiesta = -1;

            if (Context.Items["IdAssenza"] != null)
            {
                idAssenza = (int)Context.Items["IdAssenza"];
                ViewState["IdAssenza"] = idAssenza;

            }

            /*
             * 10 = messa in attesa - ritorno lista in attesa
             * 20 = primo sollecito - ritorno lista in attesa
             * 11 = messa in attesa - ritorno gestione assenza
             * 21 = primo sollecito - ritorno gestione assenza
             */

            if (Context.Items["TipoRichiesta"] != null)
            {
                tipoRichiesta = (int)Context.Items["TipoRichiesta"];
                ViewState["TipoRichiesta"] = tipoRichiesta;

            }
            //if (Context.Items["FiltroIdLavoratore"] != null)
            //{
            //    ViewState["FiltroIdLavoratore"] = Context.Items["FiltroIdLavoratore"];
            //}
            //if (Context.Items["FiltroCognome"] != null)
            //{
            //    ViewState["FiltroCognome"] = Context.Items["FiltroCognome"];
            //}
            //if (Context.Items["FiltroNome"] != null)
            //{
            //    ViewState["FiltroNome"] = Context.Items["FiltroNome"];
            //}
            //if (Context.Items["FiltroDataNascita"] != null)
            //{
            //    ViewState["FiltroDataNascita"] = Context.Items["FiltroDataNascita"];
            //}
            //if (Context.Items["FiltroStatoAssenza"] != null)
            //{
            //    ViewState["FiltroStatoAssenza"] = Context.Items["FiltroStatoAssenza"];
            //}
            //if (Context.Items["FiltroTipoAssenza"] != null)
            //{
            //    ViewState["FiltroTipoAssenza"] = Context.Items["FiltroTipoAssenza"];
            //}
            //if (Context.Items["FiltroPeriodoDa"] != null)
            //{
            //    ViewState["FiltroPeriodoDa"] = Context.Items["FiltroPeriodoDa"];
            //}
            //if (Context.Items["FiltroPeriodoA"] != null)
            //{
            //    ViewState["FiltroPeriodoA"] = Context.Items["FiltroPeriodoA"];
            //}
            //if (Context.Items["FiltroRagioneSociale"] != null)
            //{
            //    ViewState["FiltroRagioneSociale"] = Context.Items["FiltroRagioneSociale"];
            //}
            //if (Context.Items["FiltroCodiceFiscale"] != null)
            //{
            //    ViewState["FiltroCodiceFiscale"] = Context.Items["FiltroCodiceFiscale"];
            //}
            //if (Context.Items["FiltroIdImpresa"] != null)
            //{
            //    ViewState["FiltroIdImpresa"] = Context.Items["FiltroIdImpresa"];
            //}

            AssenzaDettagli assenzaDettagli = bizEF.GetAssenzaDettagli(idAssenza);
            
            lavoratoreCognome = assenzaDettagli.Cognome;
            lavoratoreNome = assenzaDettagli.Nome;
            lavoratoreDataNascita = assenzaDettagli.DataNascita;
            lavoratoreCodice = assenzaDettagli.IdLavoratore;
            malattiaInizio = assenzaDettagli.DataInizioMalattia;
            impresaRagioneSociale = assenzaDettagli.RagioneSocialeImpresa;
            inAttesa = assenzaDettagli.DataInAttesa;

            //impresaEmail =;
            //impresaEmail = "matteo.garbarino@itsinfinity.com";
            impresaEmail = bizEF.GetMalattiaTelematicaEmail(assenzaDettagli.IdImpresa, null);

            //ButtonInvia.Enabled = !String.IsNullOrEmpty(impresaEmail);

            trInAttesa.Visible = tipoRichiesta != 10 && tipoRichiesta != 11;
            
            ButtonInvia.Text = tipoRichiesta == 10 || tipoRichiesta == 11 ? "Invia la mail" : "Invia la mail e segna";

            // Recuperare l'entità contenente i dati
            CaricaDati();
        }
    }

    private void CaricaDati()
    {
        LabelImpresaRagioneSociale.Text = impresaRagioneSociale; //String.Format("{0} [{1}] ", impresaRagioneSociale, impresaEmail);
        TextBoxEmail.Text = impresaEmail;
        LabelLavoratoreCognomeNome.Text = String.Format("{0} {1} ({2:dd/MM/yyyy})", lavoratoreCognome, lavoratoreNome, lavoratoreDataNascita);
        LabelMalattiaInizio.Text = malattiaInizio.HasValue ? ((DateTime) malattiaInizio).ToString("dd/MM/yyyy") : String.Empty;

        LabelInAttesa.Text = inAttesa.HasValue ? ((DateTime)inAttesa).ToString("dd/MM/yyyy") : String.Empty;
    }

    protected void ButtonInvia_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            InviaMail();
        }
    }

    private void InviaMail()
    {
        int tipoRichiesta = (int) ViewState["TipoRichiesta"];

        impresaEmail = TextBoxEmail.Text;

        EmailMessageSerializzabile email = new EmailMessageSerializzabile();

        email.Destinatari = new List<EmailAddress>();
        email.Destinatari.Add(new EmailAddress(impresaEmail, impresaEmail));

        var sbPlain = new StringBuilder();
        var sbHtml = new StringBuilder();

        #region Creiamo il plaintext della mail
        sbPlain.AppendLine(String.Format("Spettabile Impresa {0},", impresaRagioneSociale));
        sbPlain.Append(Environment.NewLine);
        if (tipoRichiesta == 10 || tipoRichiesta == 11)
        {
            sbPlain.AppendLine(
                String.Format(
                    "Al fine di poter accogliere la Vostra richiesta relativa al rimborso per l’assenza del lavoratore {0} {1}, codice di iscrizione in Cassa Edile n. {2}), iniziata il {3:dd/MM/yyyy}, Vi invitiamo a farci pervenire quanto prima al presente indirizzo di posta elettronica la seguente documentazione:",
                    lavoratoreNome, lavoratoreCognome, lavoratoreCodice, malattiaInizio));
        }
        else
        {
            sbPlain.AppendLine(
                String.Format(
                    "In riferimento alla precedente richiesta di integrazione documentale, inviataVi via mail in data {0:dd/MM/yyyy}, con la presente Vi ricordiamo che siamo ancora in attesa di ricevere la seguente documentazione:"
                    , inAttesa));

        }
        sbPlain.Append(Environment.NewLine);
        
        // DOCUMENTI
        if (CheckBoxCertificatoMedico.Checked)
        {
            sbPlain.AppendLine(String.Format("- Certificato medico leggibile di assenza dal lavoro per il periodo dal {0:dd/MM/yyyy} al {1:dd/MM/yyyy}", RadDatePickerDal.SelectedDate, RadDatePickerAl.SelectedDate).ToUpper());
        }

        if (CheckBoxCertificatoDimissione.Checked)
        {
            sbPlain.AppendLine(String.Format("- Certificato di dimissione dall’Ospedale, relativo al ricovero del {0:dd/MM/yyyy}", RadDatePickerGiorno.SelectedDate).ToUpper());
        }

        if (CheckBoxCertificatoProspettoLiquidazione.Checked)
        {
            sbPlain.AppendLine("- Prospetto di liquidazione INAIL".ToUpper());
        }

        if (CheckBoxAltro.Checked)
        {
            sbPlain.AppendLine(String.Format("- {0}", RadTextBoxAltro.Text).ToUpper());
        }

        if (tipoRichiesta == 20 || tipoRichiesta == 21)
        {
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(
                String.Format(
                    "relativa all’assenza del lavoratore {0} {1}, codice di iscrizione in Cassa Edile n. {2}, iniziata il {3:dd/MM/yyyy}."
                    , lavoratoreNome, lavoratoreCognome, lavoratoreCodice, malattiaInizio));
        }
        sbPlain.Append(Environment.NewLine);
        if (tipoRichiesta == 10 || tipoRichiesta == 11)
        {
            sbPlain.AppendLine(
                "In attesa di ricevere quanto sopra indicato per poter completare ed espletare la Vostra pratica nel minor tempo possibile, cogliamo l’occasione per porgere i nostri migliori saluti.");
        }
        else
        {
            sbPlain.AppendLine(
                "Con l’occasione Vi comunichiamo che in caso di mancata trasmissione della documentazione mancante sopra specificata ENTRO TRE MESI DALLA DATA DI RICEZIONE DELLA PRESENTE, LA RICHIESTA DI RIMBORSO VERRA’ ARCHIVIATA SENZA ULTERIORE PREAVVISO.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(
                "Nel ricordarVi che l’invio della documentazione richiesta è indispensabile per poter completare ed espletare la Vostra pratica nel minor tempo possibile, cogliamo l’occasione per porgere i nostri migliori saluti.");
        }
        sbPlain.Append(Environment.NewLine);
        sbPlain.Append(Environment.NewLine);
        sbPlain.AppendLine("Ufficio Servizi ai Lavoratori");
        sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");

        #endregion
        email.BodyPlain = sbPlain.ToString();

        #region Creiamo l'html text della mail
        sbHtml.Append(String.Format("Spettabile Impresa <b>{0}</b>,", impresaRagioneSociale));
        sbHtml.Append("<br /><br />");
        if (tipoRichiesta == 10 || tipoRichiesta == 11)
        {
            sbHtml.Append(
                String.Format(
                    "Al fine di poter accogliere la Vostra richiesta relativa al rimborso per l’assenza del lavoratore <b>{0} {1}</b>, codice di iscrizione in Cassa Edile n. <b>{2}</b>, iniziata il <b>{3:dd/MM/yyyy}</b>, Vi invitiamo a farci pervenire quanto prima al presente indirizzo di posta elettronica la seguente documentazione:",
                    lavoratoreNome, lavoratoreCognome, lavoratoreCodice, malattiaInizio));
        }
        else
        {
            sbHtml.Append(
                String.Format(
                    "In riferimento alla precedente richiesta di integrazione documentale, inviataVi via mail in data <b>{0:dd/MM/yyyy}</b>, con la presente Vi ricordiamo che siamo ancora in attesa di ricevere la seguente documentazione:"
                    , inAttesa));

        }
        sbHtml.Append("<br /><br />");

        // DOCUMENTI
        if (CheckBoxCertificatoMedico.Checked)
        {
            sbHtml.Append(String.Format("- Certificato medico leggibile di assenza dal lavoro per il periodo dal {0:dd/MM/yyyy} al {1:dd/MM/yyyy}<br />", RadDatePickerDal.SelectedDate, RadDatePickerAl.SelectedDate).ToUpper());
        }

        if (CheckBoxCertificatoDimissione.Checked)
        {
            sbHtml.Append(String.Format("- Certificato di dimissione dall’Ospedale, relativo al ricovero del {0:dd/MM/yyyy}<br />", RadDatePickerGiorno.SelectedDate).ToUpper());
        }

        if (CheckBoxCertificatoProspettoLiquidazione.Checked)
        {
            sbHtml.Append("- Prospetto di liquidazione INAIL<br />".ToUpper());
        }

        if (CheckBoxAltro.Checked)
        {
            sbHtml.Append(String.Format("- {0}<br />", RadTextBoxAltro.Text).ToUpper());
        }
        if (tipoRichiesta == 20 || tipoRichiesta == 21)
        {
            sbHtml.Append("<br />");
            sbHtml.Append(
                String.Format(
                    "relativa all’assenza del lavoratore <b>{0} {1}</b>, codice di iscrizione in Cassa Edile n. <b>{2}</b>, iniziata il <b>{3:dd/MM/yyyy}</b>."
                    , lavoratoreNome, lavoratoreCognome, lavoratoreCodice, malattiaInizio));
        }
        sbHtml.Append("<br />");
        if (tipoRichiesta == 10 || tipoRichiesta == 11)
        {
            sbHtml.Append(
                "In attesa di ricevere quanto sopra indicato per poter completare ed espletare la Vostra pratica nel minor tempo possibile, cogliamo l’occasione per porgere i nostri migliori saluti.");
        }
        else
        {
            sbHtml.Append("<br />");
            sbHtml.Append(
                "Con l’occasione Vi comunichiamo che in caso di mancata trasmissione della documentazione mancante sopra specificata ENTRO TRE MESI DALLA DATA DI RICEZIONE DELLA PRESENTE, LA RICHIESTA DI RIMBORSO VERRA’ ARCHIVIATA SENZA ULTERIORE PREAVVISO.");
            sbHtml.Append("<br /><br />");
            sbHtml.Append(
                "Nel ricordarVi che l’invio della documentazione richiesta è indispensabile per poter completare ed espletare la Vostra pratica nel minor tempo possibile, cogliamo l’occasione per porgere i nostri migliori saluti.");
        }
        sbHtml.Append("<br /><br />");
        sbHtml.Append("Ufficio Servizi ai Lavoratori");
        sbHtml.Append("<br />");
        sbHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

        #endregion
        email.BodyHTML = sbHtml.ToString();

        if (!Common.Sviluppo)
        {
            Dipendente dip = (Dipendente) GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente());

            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = dip.EMail; //"ServiziLavoratori@cassaedilemilano.it";

            email.DestinatariBCC = new List<EmailAddress>();
            email.DestinatariBCC.Add(new EmailAddress(dip.EMail, dip.EMail));

            email.Mittente.Nome = "Ufficio Servizi ai Lavoratori";

            if (tipoRichiesta == 10 || tipoRichiesta == 11)
            {
                email.Oggetto = "Richiesta documentazione mancante";
            }
            else
            {
                email.Oggetto =
                    "Rinnovo richiesta documentazione mancante – preavviso di archiviazione della domanda di rimborso";
            }

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.Normal;

            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            var service = new EmailInfoService();
            var credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;

            service.InviaEmail(email);
        }
        if (tipoRichiesta == 20 || tipoRichiesta == 21)
        {
            bizEF.UpdateMalattiaTelematicaAssenzeDataInvioPrimoSollecito((int) ViewState["IdAssenza"], DateTime.Now.Date);
        }

        TornaIndietro(true);
    }
    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        TornaIndietro(false);
    }
    private void TornaIndietro(Boolean mailInviata)
    {
        //Response.Redirect("~/MalattiaTelematica/GestioneAssenza.aspx?idAssenza=" + ViewState["IdAssenza"].ToString() +
        //            "&idLavoratore=" + ViewState["FiltroIdLavoratore"].ToString() +
        //            "&cognome=" + ViewState["FiltroCognome"].ToString() +
        //            "&nome=" + ViewState["FiltroNome"].ToString() +
        //            "&dataNascita=" + ViewState["FiltroDataNascita"].ToString() +

        //            "&statoAssenza=" + ViewState["FiltroStatoAssenza"].ToString() +
        //            "&tipoAssenza=" + ViewState["FiltroTipoAssenza"].ToString() +
        //            "&periodoDa=" + ViewState["FiltroPeriodoDa"].ToString() +
        //            "&periodoA=" + ViewState["FiltroPeriodoA"].ToString() +

        //            "&ragioneSociale=" + ViewState["FiltroRagioneSociale"].ToString() +
        //            "&codiceFiscale=" + ViewState["FiltroCodiceFiscale"].ToString() +
        //            "&idImpresa=" + ViewState["FiltroIdImpresa"].ToString() +

        //            "&inviata=" + mailInviata.ToString()
        //      );

        if ((int)ViewState["TipoRichiesta"] == 11 || (int)ViewState["TipoRichiesta"] == 21)
        {
            Context.Items["IdAssenza"] = (int) ViewState["IdAssenza"];
            Context.Items["Provenienza"] = 1;

            Server.Transfer("~/MalattiaTelematica/GestioneAssenza.aspx");
        }
        else
        {
            Server.Transfer("~/MalattiaTelematica/RicercaAssenzaInAttesa.aspx");
        }
    }

    #region Cambio stato CheckBox
    protected void CheckBoxCertificatoMedico_CheckedChanged(object sender, EventArgs e)
    {
        PanelCertificatoMedicoPeriodo.Enabled = CheckBoxCertificatoMedico.Checked;

        if (!PanelCertificatoMedicoPeriodo.Enabled)
        {
            RadDatePickerDal.Clear();
            RadDatePickerAl.Clear();
        }
    }

    protected void CheckBoxCertificatoDimissione_CheckedChanged(object sender, EventArgs e)
    {
        PanelCertificatoDimissioneGiorno.Enabled = CheckBoxCertificatoDimissione.Checked;

        if (!PanelCertificatoDimissioneGiorno.Enabled)
        {
            RadDatePickerGiorno.Clear();
        }
    }

    protected void CheckBoxAltro_CheckedChanged(object sender, EventArgs e)
    {
        PanelCertificatoAltro.Enabled = CheckBoxAltro.Checked;

        if (!PanelCertificatoAltro.Enabled)
        {
            Presenter.SvuotaCampo(RadTextBoxAltro);
        }
    }
    #endregion

    #region Custom Validators
    protected void CustomValidatorAlmenoUno_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (!CheckBoxCertificatoMedico.Checked
            && !CheckBoxCertificatoDimissione.Checked
            && !CheckBoxCertificatoProspettoLiquidazione.Checked
            && !CheckBoxAltro.Checked)
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorCertificatoMedicoDalAl_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CheckBoxCertificatoMedico.Checked)
        {
            if (!RadDatePickerDal.SelectedDate.HasValue
                || !RadDatePickerAl.SelectedDate.HasValue
                || RadDatePickerDal.SelectedDate.Value > RadDatePickerAl.SelectedDate.Value)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorCertificatoDimissione_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CheckBoxCertificatoDimissione.Checked)
        {
            if (!RadDatePickerGiorno.SelectedDate.HasValue)
            {
                args.IsValid = false;
            }
        }
    }

    protected void CustomValidatorAltro_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (CheckBoxAltro.Checked)
        {
            if (String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(RadTextBoxAltro.Text)))
            {
                args.IsValid = false;
            }
        }
    }
    #endregion
}