﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RichiestaDocumenti.aspx.cs" Inherits="MalattiaTelematica_RichiestaDocumenti" %>

<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Richiesta Documenti" />
    <br />
    <b>Dati</b>
    <table class="standardTable">
        <tr>
            <td>
                Impresa:
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelImpresaRagioneSociale" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td>
                E-mail Impresa:
            </td>
            <td>
                <asp:TextBox ID="TextBoxEmail" runat="server" Width="400px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Indirizzo e-mail errato"
                    ControlToValidate="TextBoxEmail" ValidationExpression="^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,6}$"
                    ValidationGroup="invia">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Indirizzo e-mail mancante"
                    ControlToValidate="TextBoxEmail" ValidationGroup="invia">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Lavoratore:
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelLavoratoreCognomeNome" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td>
                Inizio Malattia:
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelMalattiaInizio" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
         <tr runat="server" id="trInAttesa">
            <td>
                Data 'In Attesa':
            </td>
            <td>
                <b>
                    <asp:Label ID="LabelInAttesa" runat="server">
                    </asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <b>Certificati Richiesti</b>
    <table class="standardTable">
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="RadAjaxPanelCertificatoMedico" runat="server" Width="100%">
                    <table class="standardTable">
                        <colgroup>
                            <col width="20px" />
                            <col />
                        </colgroup>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="CheckBoxCertificatoMedico" runat="server" AutoPostBack="true" Text="Certificato medico leggibile di assenza dal lavoro per il periodo "
                                    OnCheckedChanged="CheckBoxCertificatoMedico_CheckedChanged" />
                                <asp:CustomValidator ID="CustomValidatorCertificatoMedicoDalAl" runat="server" ValidationGroup="invia"
                                    ErrorMessage="Indicare un periodo valido per il certificato medico" OnServerValidate="CustomValidatorCertificatoMedicoDalAl_ServerValidate">
                                    *
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Panel ID="PanelCertificatoMedicoPeriodo" runat="server" Enabled="false">
                                    <table>
                                        <colgroup>
                                            <col width="50px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Dal
                                            </td>
                                            <td>
                                                <telerik:RadDatePicker ID="RadDatePickerDal" runat="server">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Al
                                            </td>
                                            <td>
                                                <telerik:RadDatePicker ID="RadDatePickerAl" runat="server">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="RadAjaxPanelCertificatoDimissione" runat="server" Width="100%">
                    <table class="standardTable">
                        <colgroup>
                            <col width="20px" />
                            <col />
                        </colgroup>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="CheckBoxCertificatoDimissione" runat="server" AutoPostBack="true"
                                    Text="Certificato di dimissione dall’Ospedale, relativo al ricovero del" OnCheckedChanged="CheckBoxCertificatoDimissione_CheckedChanged" />
                                <asp:CustomValidator ID="CustomValidatorCertificatoDimissione" runat="server" ValidationGroup="invia"
                                    ErrorMessage="Indicare un giorno per il certificato di dimissione" OnServerValidate="CustomValidatorCertificatoDimissione_ServerValidate">
                                    *
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Panel ID="PanelCertificatoDimissioneGiorno" runat="server" Enabled="false">
                                    <table>
                                        <colgroup>
                                            <col width="50px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Giorno
                                            </td>
                                            <td>
                                                <telerik:RadDatePicker ID="RadDatePickerGiorno" runat="server">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="CheckBoxCertificatoProspettoLiquidazione" runat="server" Text="Prospetto di liquidazione INAIL" />
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadAjaxPanel ID="RadAjaxPanelAltro" runat="server" Width="100%">
                    <table class="standardTable">
                        <colgroup>
                            <col width="20px" />
                            <col />
                        </colgroup>
                        <tr>
                            <td colspan="2">
                                <asp:CheckBox ID="CheckBoxAltro" runat="server" AutoPostBack="true" Text="Altro (indicare il tipo documento)"
                                    OnCheckedChanged="CheckBoxAltro_CheckedChanged" />
                                <asp:CustomValidator ID="CustomValidatorAltro" runat="server" ValidationGroup="invia"
                                    ErrorMessage="Indicare i certificati necessari" OnServerValidate="CustomValidatorAltro_ServerValidate">
                                    *
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:Panel ID="PanelCertificatoAltro" runat="server" Enabled="false">
                                    <table>
                                        <colgroup>
                                            <col width="50px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td>
                                                Doc.
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxAltro" runat="server">
                                                </telerik:RadTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </telerik:RadAjaxPanel>
            </td>
        </tr>
    </table>
    <asp:CustomValidator ID="CustomValidatorAlmenoUno" runat="server" ValidationGroup="invia"
        ErrorMessage="Selezionare almeno un certificato" OnServerValidate="CustomValidatorAlmenoUno_ServerValidate">
        *
    </asp:CustomValidator>
    <br />
    <asp:Button ID="ButtonInvia" runat="server" Text="Invia la mail" Width="150px" ValidationGroup="invia"
        OnClick="ButtonInvia_Click" />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="150px" OnClick="ButtonIndietro_Click" />
    <br />
    <br />
    <asp:ValidationSummary ID="ValidationSummaryDocumenti" runat="server" CssClass="messaggiErrore"
        ValidationGroup="invia" />
</asp:Content>
