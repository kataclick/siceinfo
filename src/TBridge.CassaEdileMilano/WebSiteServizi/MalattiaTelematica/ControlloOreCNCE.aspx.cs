using System;
using System.Configuration;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Entities;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Collections;

public partial class Prestazioni_ControlloOreCNCE : Page
{
    private const int INDICERICHIEDIORE = 2;
    private readonly PrestazioniBusiness biz = new PrestazioniBusiness();
    private readonly BusinessEF bizEF = new BusinessEF();
    private readonly Business bizMT = new Business();
    private readonly Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Autenticazione

        //GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande,
        //                                      "~/Prestazioni/ControlloDomandaOreCNCE.aspx");

        #endregion

        if (!Page.IsPostBack)
        {

            DettagliAssenza1.EnabledOreSettimanali(false);


            //Nel caso in cui il contesto non fosse presente, torniamo alla pagina di ricerca
            if (Context.Items["IdAssenza"] == null)
            {
                Server.Transfer("~/MalattiaTelematica/GestioneAssenza.aspx");
            }

            //if (Context.Items["FiltroIdLavoratore"] != null)
            //{
            //    ViewState["FiltroIdLavoratore"] = Context.Items["FiltroIdLavoratore"];
            //}
            //if (Context.Items["FiltroCognome"] != null)
            //{
            //    ViewState["FiltroCognome"] = Context.Items["FiltroCognome"];
            //}
            //if (Context.Items["FiltroNome"] != null)
            //{
            //    ViewState["FiltroNome"] = Context.Items["FiltroNome"];
            //}
            //if (Context.Items["FiltroDataNascita"] != null)
            //{
            //    ViewState["FiltroDataNascita"] = Context.Items["FiltroDataNascita"];
            //}
            //if (Context.Items["FiltroStatoAssenza"] != null)
            //{
            //    ViewState["FiltroStatoAssenza"] = Context.Items["FiltroStatoAssenza"];
            //}
            //if (Context.Items["FiltroTipoAssenza"] != null)
            //{
            //    ViewState["FiltroTipoAssenza"] = Context.Items["FiltroTipoAssenza"];
            //}
            //if (Context.Items["FiltroPeriodoDa"] != null)
            //{
            //    ViewState["FiltroPeriodoDa"] = Context.Items["FiltroPeriodoDa"];
            //}
            //if (Context.Items["FiltroPeriodoA"] != null)
            //{
            //    ViewState["FiltroPeriodoA"] = Context.Items["FiltroPeriodoA"];
            //}
            //if (Context.Items["FiltroRagioneSociale"] != null)
            //{
            //    ViewState["FiltroRagioneSociale"] = Context.Items["FiltroRagioneSociale"];
            //}
            //if (Context.Items["FiltroCodiceFiscale"] != null)
            //{
            //    ViewState["FiltroCodiceFiscale"] = Context.Items["FiltroCodiceFiscale"];
            //}
            //if (Context.Items["FiltroIdImpresa"] != null)
            //{
            //    ViewState["FiltroIdImpresa"] = Context.Items["FiltroIdImpresa"];
            //}

            int idAssenza = (int)Context.Items["IdAssenza"];

            AssenzaDettagli assenzaDet = bizEF.GetAssenzaDettagli(idAssenza);

          
            CaricaAssenzaDettagli(assenzaDet);
            CaricaCasseEdili();
        }
    }

    private void CaricaCasseEdili()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListCassaEdile,
            commonBiz.GetCasseEdiliSenzaMilano(),
            "Descrizione",
            "IdCassaEdile");
    }

    private void CaricaDomanda(Domanda dom)
    {
        
    }

    private void CaricaAssenzaDettagli(AssenzaDettagli assenzaDet)
    {
        ViewState["IdAssenza"] = assenzaDet.IdAssenza;

        //if (domanda.ControlloOreCnce.HasValue && domanda.ControlloOreCnce.Value)
        //    ButtonForzaTermineRecuperoOre.Enabled = false;
       
        //CassaEdileCollection  co = new CassaEdileCollection();
        //foreach(CasseEdili ce in assenzaDet.CasseEdili)
        //{
            
        //    co.Add(new CassaEdile(ce.CE.IdCassaEdile, ce.Descrizione));

        //}

        Boolean abilita = true;
        if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
        {
            abilita = false;
        }

        OreCNCEControllo1.CaricaDati(assenzaDet.CasseEdili,
            assenzaDet.IdLavoratore.Value,
            assenzaDet.Nome,
            assenzaDet.Cognome,
            assenzaDet.CodiceFiscale,
            assenzaDet.DataInizioMalattia.Value,
            abilita
            );


       
        ViewState["IdLavoratore"] = assenzaDet.IdLavoratore;

        CaricaOre(assenzaDet.IdLavoratore.Value);

        DettagliAssenza1.CaricaAssenza(assenzaDet);
    }

    private void CaricaOre(int idLavoratore)
    {
        //// Carico le ore gi� presenti nel nostro DB
        //OreMensiliCNCECollection ore = OreCNCEManager.GetOreMensiliCNCE(idLavoratore);
        //GridViewOreCaricate.DataSource = ore;
        //GridViewOreCaricate.DataBind();

        //if (ore.Count > 0)
        //{
        //    LabelSegnalazione.Visible = true;
        //}
        //else
        //{
        //    LabelSegnalazione.Visible = false;
        //}
    }

    //protected void GridViewCasseEdili_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        CassaEdile cassaEdile = (CassaEdile) e.Row.DataItem;

    //        if (cassaEdile.Cnce)
    //            ButtonRichiediOreCnce.Enabled = true;

    //    }
    //}

    //protected void GridViewCasseEdili_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    string idCassaEdile = (string) GridViewCasseEdili.DataKeys[e.RowIndex].Values["IdCassaEdile"];
    //    PrestazioniCaricamentoManualeOre1.ImpostaCassaEdileELavoratore(idCassaEdile, (int) ViewState["IdLavoratore"]);
    //    PanelCaricamentoManuale.Visible = true;
    //}

    protected void ButtonCaricaOre_Click(object sender, EventArgs e)
    {
        //if (Page.IsValid)
        //{
        //    bool oreDuplicate;
        //    OreMensiliCNCE ore = PrestazioniCaricamentoManualeOre1.CreaOre();

        //    if (OreCNCEManager.InsertOreMensili(ore, out oreDuplicate))
        //    {
        //        int idLavoratore = (int) ViewState["IdLavoratore"];
        //        CaricaOre(idLavoratore);
        //        PrestazioniCaricamentoManualeOre1.ResetCampi();
        //        PanelCaricamentoManuale.Visible = false;
        //    }
        //    else
        //    {
        //        if (oreDuplicate)
        //        {
        //        }
        //    }
        //}
    }

    protected void GridViewOreCaricate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //GridViewOreCaricate.PageIndex = e.NewPageIndex;

        //int idLavoratore = (int) ViewState["IdLavoratore"];
        //CaricaOre(idLavoratore);
    }

    //protected void GridViewCasseEdili_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    string idCassaEdile = (string) GridViewCasseEdili.DataKeys[e.NewSelectedIndex].Values["IdCassaEdile"];
    //    int idLavoratore = (int) ViewState["IdLavoratore"];

    //    // Va effettuata la chiamata al Web Service e bisogna capire come gestire eventuali aggiornamenti..
    //}


    protected void ButtonIndietro_Click(object sender, EventArgs e)
    {
        Context.Items["IdAssenza"] = (int)ViewState["IdAssenza"];

        //Context.Items["FiltroIdImpresa"] = ViewState["FiltroIdImpresa"].ToString();
        //Context.Items["FiltroRagioneSociale"] = ViewState["FiltroRagioneSociale"].ToString();
        //Context.Items["FiltroCodiceFiscale"] = ViewState["FiltroCodiceFiscale"].ToString();
        //Context.Items["FiltroIdLavoratore"] = ViewState["FiltroIdLavoratore"].ToString();
        //Context.Items["FiltroCognome"] = ViewState["FiltroCognome"].ToString();
        //Context.Items["FiltroNome"] = ViewState["FiltroNome"].ToString();
        //Context.Items["FiltroDataNascita"] = ViewState["FiltroDataNascita"].ToString();
        //Context.Items["FiltroStatoAssenza"] = ViewState["FiltroStatoAssenza"].ToString();
        //Context.Items["FiltroTipoAssenza"] = ViewState["FiltroTipoAssenza"].ToString();
        //Context.Items["FiltroPeriodoDa"] = ViewState["FiltroPeriodoDa"].ToString();
        //Context.Items["FiltroPeriodoA"] = ViewState["FiltroPeriodoA"].ToString();

        Server.Transfer("~/MalattiaTelematica/GestioneAssenza.aspx");
    }

    //todo da implementare per malattia telematica
    protected void ButtonForzaTermineRecuperoOre_Click(object sender, EventArgs e)
    {
        int idDomanda = (int) ViewState["IdDomanda"];

        if (biz.ForzaControlloOreCNCE(idDomanda, true))
        {
            ButtonForzaTermineRecuperoOre.Enabled = false;
        }
    }

    //todo da implementare per malattia telematica
    protected void ButtonRichiediOreCnce_Click(object sender, EventArgs e)
    {
        
        int idAssenza = (int) ViewState["IdAssenza"];
        AssenzaDettagli assenzaDet = bizEF.GetAssenzaDettagli(idAssenza);

        string loginCNCE = ConfigurationManager.AppSettings["CNCEUserName"];
        string passwordCNCE = ConfigurationManager.AppSettings["CNCEPassword"];
        string urlCNCE = ConfigurationManager.AppSettings["CNCEUrl"];

        // todo chiamata al ws
        //bizMT.GestisciOreCNCE(assenzaDet, loginCNCE, passwordCNCE, urlCNCE);
        //biz.GestisciOreCNCE(assenzaDet, loginCNCE, passwordCNCE, urlCNCE);
        CaricaAssenzaDettagli(assenzaDet);
    }

    protected void GridViewOreCaricate_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    OreMensiliCNCE ore = (OreMensiliCNCE) e.Row.DataItem;

        //    RadButton bConfermaEliminazioneSi = (RadButton) e.Row.FindControl("ButtonConfermaEliminazioneSi");
        //    bConfermaEliminazioneSi.CommandArgument = e.Row.RowIndex.ToString();
        //    RadButton bConfermaEliminazioneNo = (RadButton)e.Row.FindControl("ButtonConfermaEliminazioneNo");
        //    bConfermaEliminazioneNo.CommandArgument = e.Row.RowIndex.ToString();

        //    if ((ore.OreLavorate + ore.OreFerie + ore.OreInfortunio + ore.OreMalattia + ore.OreCassaIntegrazione + ore.OrePermessoRetribuito) > 200)
        //        e.Row.ForeColor = Color.Red;
        //}
    }

    protected void GridViewOreCaricate_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //int indice;

        //switch (e.CommandName)
        //{
        //    case "confermaDeleteSi":
        //        indice = Int32.Parse(e.CommandArgument.ToString());

        //        int idLavoratore = (int) GridViewOreCaricate.DataKeys[indice].Values["IdLavoratore"];
        //        string idCassaEdile = (string) GridViewOreCaricate.DataKeys[indice].Values["IdCassaEdile"];
        //        int anno = (int) GridViewOreCaricate.DataKeys[indice].Values["Anno"];
        //        int mese = (int) GridViewOreCaricate.DataKeys[indice].Values["Mese"];

        //        if (OreCNCEManager.DeleteOreCNCE(idLavoratore, idCassaEdile, anno, mese))
        //        {
        //            CaricaOre(idLavoratore);
        //            //Domanda domanda = biz.GetDomanda((int) ViewState["IdDomanda"]);
        //            //CaricaDomanda(domanda);
        //        }
        //        break;
        //    case "confermaDeleteNo":
        //        indice = Int32.Parse(e.CommandArgument.ToString());
        //        HtmlTableRow rigaConferma =
        //            (HtmlTableRow) GridViewOreCaricate.Rows[indice].FindControl("trConfermaEliminazione");
        //        rigaConferma.Visible = false;
        //        break;
        //    case "Modifica":
        //        indice = Convert.ToInt32(e.CommandArgument);
        //        int idLav = (int) GridViewOreCaricate.DataKeys[indice].Values["IdLavoratore"];
        //        string idCe = (string) GridViewOreCaricate.DataKeys[indice].Values["IdCassaEdile"];
        //        int year = (int) GridViewOreCaricate.DataKeys[indice].Values["Anno"];
        //        int month = (int) GridViewOreCaricate.DataKeys[indice].Values["Mese"];

        //        OreMensiliCNCE ore = OreCNCEManager.GetOreMensiliCNCE(idLav, idCe, year, month);
        //        PrestazioniCaricamentoManualeOre1.CaricaOre(ore);
        //        PanelCaricamentoManuale.Visible = true;
        //        break;
        //}
    }

    protected void GridViewOreCaricate_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //HtmlTableRow rigaConferma =
        //    (HtmlTableRow) GridViewOreCaricate.Rows[e.RowIndex].FindControl("trConfermaEliminazione");
        //rigaConferma.Visible = true;
    }

    protected void ButtonAggiungiCassaEdile_Click(object sender, EventArgs e)
    {
        PanelNuovaCassaEdile.Visible = true;
    }

    protected void ButtonNuovaCassaEdile_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String idCassaEdile = DropDownListCassaEdile.SelectedValue;

            Int32 idAssenza = (Int32) ViewState["IdAssenza"];
            AssenzaDettagli assenza = bizEF.GetAssenzaDettagli(idAssenza);

            if (!assenza.PresenteCassaEdileNellaLista(idCassaEdile))
            {
                if (bizMT.InsertCassaEdile(idAssenza, idCassaEdile))
                {
                    DropDownListCassaEdile.SelectedIndex = 0;
                    PanelNuovaCassaEdile.Visible = false;

                    assenza = bizEF.GetAssenzaDettagli(idAssenza);
                    CaricaAssenzaDettagli(assenza);
                }
            }
        }
    }
    protected void RadGridCasseEdili_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        //if (e.CommandName == "carica")
        //{
        //    string idCassaEdile = (string)
        //                             RadGridCasseEdili.MasterTableView.DataKeyValues[e.Item.ItemIndex]["CE.IdCassaEdile"];

        //   // string idCassaEdile = (string) GridViewCasseEdili.DataKeys[e.RowIndex].Values["IdCassaEdile"];
        //    PrestazioniCaricamentoManualeOre1.ImpostaCassaEdileELavoratore(idCassaEdile, (int) ViewState["IdLavoratore"]);
        //    PanelCaricamentoManuale.Visible = true;
        //}
        //if (e.CommandName == "elimina")
        //{
        //    int idAssenza = (int)ViewState["IdAssenza"];
        //    int idLavoratore = (int)ViewState["IdLavoratore"];
        //    string idCassaEdile = (string)
        //                             RadGridCasseEdili.MasterTableView.DataKeyValues[e.Item.ItemIndex]["CE.IdCassaEdile"];

        //    bizMT.DeleteCassaEdile(idAssenza, idCassaEdile, idLavoratore);
        //    AssenzaDettagli assenza = bizEF.GetAssenzaDettagli(idAssenza);
        //    CaricaAssenzaDettagli(assenza);
        //}
    }
    protected void RadGridCasseEdili_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Int32 indiceSelezionato = Int32.Parse(RadGridCasseEdili.SelectedIndexes[0]);
        //string idCassaEdile =
        //    (string) RadGridCasseEdili.MasterTableView.DataKeyValues[indiceSelezionato]["IdCassaEdile"];
        //int idLavoratore = (int) ViewState["IdLavoratore"];

        // Va effettuata la chiamata al Web Service e bisogna capire come gestire eventuali aggiornamenti..
    }
    protected void RadGridCasseEdili_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            CasseEdili ce = (CasseEdili)e.Item.DataItem;
            Button bCaricaManuale = (Button)e.Item.FindControl("ButtonCaricaManuale");

//            if (ce.Cnce)
//                ButtonRichiediOreCnce.Enabled = true;

            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
                bCaricaManuale.Enabled = false;
            else
                bCaricaManuale.Enabled = true;

        }
    }
}