<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ControlloOreCNCE.aspx.cs" Inherits="Prestazioni_ControlloOreCNCE" Theme="CETheme2009Wide" %>

<%--<%@ Register Src="../WebControls/OreCNCECaricamentoManuale.ascx" TagName="PrestazioniCaricamentoManualeOre"
    TagPrefix="uc3" %>--%>
<%@ Register Src="../WebControls/OreCNCEControllo.ascx" TagName="OreCNCEControllo"
    TagPrefix="uc1" %>
<%--<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni"
    TagPrefix="uc1" %>--%>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/DettagliAssenza.ascx" TagName="DettagliAssenza" TagPrefix="uc5" %>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>--%>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo Ore CNCE"
        titolo="Malattia / Infortunio" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc5:DettagliAssenza ID="DettagliAssenza1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonForzaTermineRecuperoOre" runat="server" Text="Caricamento ore concluso"
                    OnClick="ButtonForzaTermineRecuperoOre_Click" Width="200px" Visible="False" />
                &nbsp;
                <asp:Button ID="ButtonAggiungiCassaEdile" runat="server" Text="Aggiungi cassa edile"
                    Width="200px" OnClick="ButtonAggiungiCassaEdile_Click" />
                &nbsp;
                <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click"
                    Text="Indietro" Width="200px" />
            </td>
        </tr>
        <tr>
            <td class="borderedTable">
                <asp:Panel ID="PanelNuovaCassaEdile" runat="server" Visible="false">
                    <asp:DropDownList ID="DropDownListCassaEdile" runat="server" AppendDataBoundItems="true"
                        Width="400px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNuovaCassa" ControlToValidate="DropDownListCassaEdile"
                        runat="server" ValidationGroup="nuovaCassa">*</asp:RequiredFieldValidator>
                    <asp:Button ID="ButtonNuovaCassaEdile" runat="server" Text="Aggiungi" Width="150px"
                        ValidationGroup="nuovaCassa" OnClick="ButtonNuovaCassaEdile_Click" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:OreCNCEControllo ID="OreCNCEControllo1" runat="server" />
            </td>
        </tr>

    </table>
</asp:Content>
