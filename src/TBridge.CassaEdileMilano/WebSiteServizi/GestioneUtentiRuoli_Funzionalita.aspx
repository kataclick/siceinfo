<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="GestioneUtentiRuoli_Funzionalita.aspx.cs" Inherits="GestioneRuoli_Funzionalita" %>

<%@ Register Src="WebControls/Messaggio.ascx" TagName="Messaggio" TagPrefix="uc3" %>
<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo2" titolo="Gestione utenti" sottoTitolo="Associazione funzionalitą ad un ruolo"
        runat="server" />
    &nbsp;
    <br />
    <asp:Label ID="Label1" runat="server" Text="Seleziona ruolo: "></asp:Label>&nbsp;
    <asp:DropDownList ID="DropDownListRuoli" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:Button ID="ButtonModificaRuolo" runat="server" OnClick="ButtonModificaRuolo_Click"
        Text="Vai" /><br />
    <br />
    <uc3:Messaggio ID="MessaggioTitolo" runat="server" Visible="false" />
    <asp:Panel ID="Panel1" runat="server">
        <asp:Label ID="LabelNomeRuolo" runat="server" Text="Nome:"></asp:Label>
        <asp:TextBox ID="TextBoxNomeRuolo" runat="server"></asp:TextBox><br />
        <asp:Label ID="LabelDescrizioneRuolo" runat="server" Text="Descrizione:"></asp:Label>
        <asp:TextBox ID="TextBoxDescrizioneRuolo" runat="server" Width="375px"></asp:TextBox><br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Seleziona funzionalitą per il ruolo"
            Font-Bold="True"></asp:Label><br />
        <asp:CheckBoxList ID="CheckBoxListFunzionalita" runat="server">
        </asp:CheckBoxList>
        <br />
        <asp:Button ID="ButtonAggiornaRuolo" runat="server" OnClick="Button1_Click" Text="Aggiorna ruolo" />
        <asp:Button ID="ButtonEliminaRuolo" runat="server" OnClick="ButtonEliminaRuolo_Click"
            Text="Elimina ruolo" /><br />
    </asp:Panel>
    &nbsp;&nbsp;
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
