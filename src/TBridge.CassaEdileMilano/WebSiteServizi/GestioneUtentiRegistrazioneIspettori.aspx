<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeFile="GestioneUtentiRegistrazioneIspettori.aspx.cs" Inherits="RegistrazioneIspettori" %>

<%@ Register Src="WebControls/RegistrazioneIspettore.ascx" TagName="RegistrazioneIspettore"
    TagPrefix="uc1" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione utenti" sottoTitolo="Registrazione ispettore"/>
    <br />
    <uc1:RegistrazioneIspettore id="RegistrazioneIspettore1" runat="server">
    </uc1:RegistrazioneIspettore>
    
</asp:Content>

