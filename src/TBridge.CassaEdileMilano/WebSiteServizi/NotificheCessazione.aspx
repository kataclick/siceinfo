<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="NotificheCessazione.aspx.cs" Inherits="NotificheCessazione" %>

<%@ Register Src="WebControls/NotificheImpresaSelezionata.ascx" TagName="NotificheImpresaSelezionata"
    TagPrefix="uc3" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/MenuNotifiche.ascx" TagName="MenuNotifiche" TagPrefix="uc1" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche" sottoTitolo="Gestione notifiche di cessazione"/>
    <br />
    <asp:Panel ID="PanelImprese" runat="server" Width="609px">
        <uc3:NotificheImpresaSelezionata id="NotificheImpresaSelezionata1" runat="server">
        </uc3:NotificheImpresaSelezionata>
        <br />
        <asp:CustomValidator ID="CustomValidatorImpresaSelezionata" runat="server" ErrorMessage="Seleziona una impresa"
            OnServerValidate="CustomValidatorImpresaSelezionata_ServerValidate" ValidationGroup="selezioneImpresa"></asp:CustomValidator></asp:Panel>
    <br />
    Per segnalare la cessazione di un rapporto di lavoro, ricercare il lavoratore interessato
    per codice fiscale o cognome, nome e data di nascita.<br />
    La fonte indica la modalit� con cui � pervenuta l'informazione.<br />
    <br />
    <table class="standardTable">
        <tr>
            <td align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonCF" runat="server" Checked="True" GroupName="scelta" /></td>
            <td align="left">Codice fiscale:
            </td>
            <td>
                <asp:TextBox ID="TextBoxCF" runat="server" MaxLength="16"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td rowspan="2" valign="middle" align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonNome" runat="server" GroupName="scelta" /></td>
            <td align="left">Cognome:
            </td>
            <td>
                <asp:TextBox ID="TextBoxCognome" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">Nome:
            </td>
            <td>
                <asp:TextBox ID="TextBoxNome" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="left">Data nascita (gg/mm/aaaa):
            </td>
            <td>
                <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td><asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d" ErrorMessage="Formato data non valido" Display="Dynamic" ControlToValidate="TextBoxDataNascita" ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" OnClick="ButtonRicerca_Click" ValidationGroup="ricerca" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelTitoloElenco" runat="server" Font-Bold="True" Text="Elenco lavoratori" Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
        OnRowEditing="GridViewLavoratori_RowEditing" AllowPaging="True" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging" PageSize="8" width="100%" DataKeyNames="IdNotifica">
        <Columns>
            <asp:TemplateField HeaderText="Fonte">
                <ItemTemplate>
                    <%#StringaTipo((TBridge.Cemi.Notifiche.Type.Entities.TipoNotifica)DataBinder.Eval(Container.DataItem, "TipoNotifica"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Cognome" DataField="cognome" />
            <asp:BoundField HeaderText="Nome" DataField="nome" />
            <asp:BoundField HeaderText="Data nascita" DataField="dataNascita" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:BoundField HeaderText="Comune nascita" DataField="luogoNascita" Visible="False" />
            <asp:BoundField HeaderText="Sesso" DataField="sesso" Visible="False" />
            <asp:BoundField HeaderText="Codice fiscale" DataField="codiceFiscale" />
            <asp:BoundField HeaderText="Data assunzione" DataField="dataAssunzione" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" Visible="False" />
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="Edit" Text="Cessazione" >
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red" Visible="False">Notifica inserita correttamente</asp:Label>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <asp:Label ID="LabelMessaggio" Text="Per confermare l'avvenuta cessazione del rapporto di lavoro, inserire la data di cessazione e dare conferma, cliccando su &quot;Prosegui&quot;." runat="server" Visible="False"></asp:Label>
    <br />
    <asp:Panel ID="PanelDettagli" runat="server" Visible="False" Width="125px">
        <table style="width: 566px">
        <tr>
            <td align="left">
                Cognome:
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxCognomeL" runat="server" Enabled="False" Width="177px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                Nome:
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxNomeL" runat="server" Enabled="False" Width="177px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                Data nascita (gg/mm/aaaa):
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxDataNascitaL" runat="server" Enabled="False" Width="177px"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                Comune nascita:
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxComuneNascitaL" runat="server" Enabled="False" Width="177px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                Sesso:
            </td>
            <td align="left">
                <asp:RadioButtonList ID="RadioButtonListSessoL" runat="server" RepeatDirection="Horizontal" Enabled="False">
                    <asp:ListItem>M</asp:ListItem>
                    <asp:ListItem>F</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="left">
                Codice fiscale:
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxCodiceFiscaleL" runat="server" Enabled="False" Width="177px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                Data assunzione (gg/mm/aaaa):
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxDataAssunzioneL" runat="server" Enabled="False" Width="177px"></asp:TextBox>
            </td>
            <td></td>
        </tr>
        <tr>
            <td align="left">
                Data cessazione (gg/mm/aaaa):
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxDataCessazioneL" runat="server" Width="177px" MaxLength="10"></asp:TextBox>
            </td>
            <td align="left">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxDataCessazioneL"
                    ErrorMessage="*" ValidationGroup="cessazione"></asp:RequiredFieldValidator><asp:CompareValidator
                        ID="CompareValidatorDataCessazioneTipo" runat="server" ControlToValidate="TextBoxDataCessazioneL"
                        ErrorMessage="Formato data non valido." Operator="DataTypeCheck" Type="Date"
                        ValidationGroup="cessazione"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidatorDataCessazione" runat="server" ControlToValidate="TextBoxDataCessazioneL"
                    ErrorMessage="La data di cessazione non pu� essere successiva alla data odierna"
                    Operator="LessThanEqual" Type="Date" ValidationGroup="cessazione"></asp:CompareValidator></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Button ID="ButtonOk" runat="server" Text="Prosegui" OnClick="ButtonOk_Click" Width="101px" ValidationGroup="cessazione" />
            </td>
            <td>
                </td>
        </tr>
    </table>
    </asp:Panel>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuNotifiche ID="MenuNotifiche1" runat="server" />
</asp:Content>
