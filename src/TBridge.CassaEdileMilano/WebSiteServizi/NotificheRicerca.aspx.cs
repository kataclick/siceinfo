using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Notifiche.Business;
using TBridge.Cemi.Notifiche.Type.Collections;
using TBridge.Cemi.Notifiche.Type.Entities;

public partial class NotificheRicerca : Page
{
    private readonly NotificaBusiness biz = new NotificaBusiness();
    private int idImpresa;

    protected void Page_Load(object sender, EventArgs e)
    {
        GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.NNotificheGestione, "NotificheRicerca.aspx");

        NotificheImpresaSelezionata1.OnImpresaSelected += NotificheImpresaSelezionata1_OnImpresaSelected;

        if (Session["idImpresa"] != null)
            idImpresa = (int) Session["idImpresa"];

        if (Page.IsPostBack)
        {
            Ricerca();
        }
        else
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idConsulente = consulente.IdConsulente;

                PanelImprese.Visible = true;
            }
            else
            {
                PanelImprese.Visible = false;
            }
        }
    }

    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Ricerca();

            LabelTitoloElenco.Visible = true;
        }
    }

    private void Ricerca()
    {
        if (idImpresa != 0)
        {
            NotificaCollection listaNotifiche = null;

            if (RadioButtonCessazioni.Checked)
            {
                listaNotifiche = biz.RapportiCessazioni(idImpresa);
                DateVisibili(true);
            }
            if (RadioButtonAssunzioni.Checked)
            {
                listaNotifiche = biz.RapportiAssunzioni(idImpresa);
                DateVisibili(true);
            }

            GridViewLavoratori.DataSource = listaNotifiche;
            GridViewLavoratori.DataBind();

            GridViewLavoratori.Visible = true;

            if (listaNotifiche.Count > 0)
                VisualizzaBottoniEsporta(true);
            else
                VisualizzaBottoniEsporta(false);
        }
    }

    public void VisualizzaBottoniEsporta(bool visualizza)
    {
        ButtonExcel.Visible = visualizza;
        ButtonStampa.Visible = visualizza;
        ButtonWord.Visible = visualizza;
    }

    public void DateVisibili(bool visibili)
    {
        GridViewLavoratori.Columns[6].Visible = visibili;
        GridViewLavoratori.Columns[7].Visible = visibili;
    }

    public string StringaTipo(TipoNotifica tipo)
    {
        string res = string.Empty;

        switch (tipo)
        {
            case TipoNotifica.CessazioneDenuncia:
                res = "Denuncia";
                break;
            case TipoNotifica.CessazioneNotifica:
                res = "Notifica";
                break;
        }

        return res;
    }

    protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewLavoratori.PageIndex = e.NewPageIndex;
        GridViewLavoratori.DataBind();
    }

    protected void ButtonStampa_Click(object sender, EventArgs e)
    {
        StringWriter sw = PrepareStampa();

        Response.Write(sw.ToString());
        Response.End();
    }

    protected void ButtonExcel_Click(object sender, EventArgs e)
    {
        StringWriter sw = PrepareStampa();

        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=Lavoratori.xls");
        Response.ContentType = "application/vnd.ms-excel";
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void ButtonWord_Click(object sender, EventArgs e)
    {
        StringWriter sw = PrepareStampa();

        Response.ClearContent();
        Response.AppendHeader("content-disposition", "attachment; filename=Lavoratori.doc");
        Response.ContentType = "application/vnd.ms-word";
        Response.Write(sw.ToString());
        Response.End();
    }

    private StringWriter PrepareStampa()
    {
        GridView gv = new GridView();
        gv.ID = "gvProducts";

        DataTable dt = new DataTable();
        dt.Columns.Add("Tipo");
        dt.Columns.Add("Cognome");
        dt.Columns.Add("Nome");
        dt.Columns.Add("DataNascita");
        dt.Columns.Add("ComuneNascita");
        dt.Columns.Add("Sesso");
        dt.Columns.Add("Codice fiscale");
        dt.Columns.Add("DataAssunzione");
        dt.Columns.Add("DataCessazione");

        foreach (Notifica notifica in (NotificaCollection) GridViewLavoratori.DataSource)
        {
            DataRow dr = dt.NewRow();
            dr["Tipo"] = StringaTipo(notifica.TipoNotifica);
            dr["Cognome"] = notifica.Cognome;
            dr["Nome"] = notifica.Nome;
            dr["DataNascita"] = notifica.DataNascita.ToShortDateString();
            dr["ComuneNascita"] = notifica.LuogoNascita;
            dr["Sesso"] = notifica.Sesso;
            dr["Codice fiscale"] = notifica.CodiceFiscale;
            if (notifica.DataAssunzione.HasValue)
                dr["DataAssunzione"] = notifica.DataAssunzione.Value.ToShortDateString();
            if (notifica.DataCessazione.HasValue)
                dr["DataCessazione"] = notifica.DataCessazione.Value.ToShortDateString();

            dt.Rows.Add(dr);
        }

        //metto in ordine i campi
        //BoundField bfCognome = new BoundField();
        //bfCognome.DataField = "Cognome";
        //bfCognome.HeaderText = "Cognome";
        //gv.Columns.Add(bfCognome);

        //BoundField bfNome = new BoundField();
        //bfNome.DataField = "Nome";
        //bfNome.HeaderText = "Nome";
        //gv.Columns.Add(bfNome);
        ////

        //gv.AutoGenerateColumns = false;
        //gv.DataSource = GridViewLavoratori.DataSource;
        gv.DataSource = dt;
        gv.DataBind();

        //
        //string idLav = gv.Rows[0].Cells[0].Text;
        //string nomeGruppo = gv.Rows[0].Cells[4].Text;

        //for (int i = 1; i < gv.Rows.Count; i++)
        //{
        //    if (gv.Rows[i].Cells[0].Text == idLav)
        //    {
        //        gv.Rows[i].Cells[0].Text = "";
        //        gv.Rows[i].Cells[1].Text = "";
        //        gv.Rows[i].Cells[2].Text = "";
        //        gv.Rows[i].Cells[3].Text = "";
        //    }
        //    else
        //    {
        //        idLav = gv.Rows[i].Cells[0].Text;
        //        nomeGruppo = gv.Rows[i].Cells[4].Text;
        //        continue;
        //    }

        //    if (gv.Rows[i].Cells[4].Text == nomeGruppo)
        //    {
        //        gv.Rows[i].Cells[4].Text = "";
        //    }
        //    else
        //    {
        //        nomeGruppo = gv.Rows[i].Cells[4].Text;
        //    }
        //}
        //

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        return sw;
    }

    protected void CustomValidatorImprese_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (Session["idImpresa"] != null)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }

    private void NotificheImpresaSelezionata1_OnImpresaSelected()
    {
        Page.Validate("selezioneImpresa");

        GridViewLavoratori.DataSource = null;
        GridViewLavoratori.DataBind();

        GridViewLavoratori.Visible = false;

        LabelTitoloElenco.Visible = false;

        VisualizzaBottoniEsporta(false);
    }
}