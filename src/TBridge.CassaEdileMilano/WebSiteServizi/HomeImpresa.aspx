<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HomeImpresa.aspx.cs" Inherits="HomeImpresa" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/MenuImprese.ascx" TagName="MenuImprese" TagPrefix="uc1" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuImprese ID="MenuImprese1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Estratto conto" />
    <br />
In questa sezione, l�impresa pu� visualizzare le ore denunciate e pagate riferite ad uno specifico periodo o ad un singolo dipendente.
<br />
<br />
I criteri di ricerca sono, quindi, per periodo (a partire dal mese di dicembre 2005) o per dipendente.
<br />
<br />
Cliccando sul simbolo + si ha la possibilit� di aprire la specifica riferita al numero ed alla tipologia di ore denunciate e pagate per periodo e per lavoratore.
<br />
<br />
<%--Infine, la voce �Crediti� consente di visualizzare l�eventuale l�importo a proprio credito risultato dalla differenza tra dovuto/versato riferito ad un preciso periodo.--%>
<br />
</asp:Content>


