﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public static class GestioneAutorizzazioneReport
    {
        private static HttpServerUtility Server
        {
            get
            {
                if (HttpContext.Current != null)
                    return HttpContext.Current.Server;
                return null;
            }
        }

        private static HttpRequest Request
        {
            get
            {
                if (HttpContext.Current != null)
                    return HttpContext.Current.Request;
                return null;
            }
        }

        public static void ReportAutorizzato(string nomeReport)
        {
            if (!GestioneReport.Autorizzato(nomeReport))
            {
                //Trasferiamo su una pagina di errore
                Server.Transfer(String.Format("~/DefaultErroreAutenticazione.aspx?ReturnUrl={0}", Request.Url));
            }
        }
    }
}
