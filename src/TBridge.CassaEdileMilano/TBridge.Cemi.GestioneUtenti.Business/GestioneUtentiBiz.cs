using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using System.Web.UI.WebControls;
using TBridge.Cemi.Data;
using TBridge.Cemi.GestioneUtenti.Data;
using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Type.Filters;
using RuoliPredefiniti = TBridge.Cemi.GestioneUtenti.Type.RuoliPredefiniti;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public class GestioneUtentiBiz
    {
        private static GestioneUtentiBiz _gestioneUtentiBiz;

        private readonly TBridge.Cemi.Business.Common commonBiz = new TBridge.Cemi.Business.Common();

        private readonly GestioneUtentiAccess gestioneUtentiDataAccess;

        public GestioneUtentiBiz()
        {
            gestioneUtentiDataAccess = new GestioneUtentiAccess();
        }

        #region Metodi Statici

        /// <summary>
        ///   Recupera il nome dell'utente loggato
        /// </summary>
        /// <returns></returns>
        public static String GetNomeUtente()
        {
            try
            {
                Utente utente = (Utente) Membership.GetUser();
                return utente.UserName;
            }
            catch
            {
                throw new Exception("GetNomeUtente: impossibile recuperare la login dell'utente");
            }
        }

        /// <summary>
        ///   Otteniamo l'id dell'utente loggato
        /// </summary>
        /// <returns></returns>
        public static Int32 GetIdUtente()
        {
            try
            {
                Utente utente = (Utente) Membership.GetUser();
                return utente.IdUtente;
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        ///   Recupera l'identit� dell'utente corrente
        /// </summary>
        /// <returns></returns>
        public static Utente GetIdentitaUtenteCorrente()
        {
            Utente utente = (Utente) Membership.GetUser();

            if (utente != null)
            {
                return GetIdentitaUtente(utente.IdUtente);
            }

            return null;
        }

        /// <summary>
        ///   Recupera l'identit� dell'utente passato come parametro
        /// </summary>
        /// <param name = "idUtente"></param>
        /// <returns></returns>
        public static Utente GetIdentitaUtente(Int32 idUtente)
        {
            return (GetGestioneUtenti().GetUtente(idUtente));
        }

        public Utente GetUtente(Int32 idUtente)
        {
            return gestioneUtentiDataAccess.GetUtente(idUtente);
        }

        /// <summary>
        /// Verifica se l'utente loggato � autorizzato alla funzionalit� passata come parametro, nel caso di nessun utente loggato si considerano valide le funzionalit� dell'Utente pubblico
        /// </summary>
        /// <param name = "funzionalita"></param>
        /// <returns></returns>
        [Obsolete("Utilizzare il metoto che prende in input enum")]
        public static bool Autorizzato(string funzionalita)
        {
            bool autorizzato = false;

            try
            {
                MembershipUser user = Membership.GetUser();

                if (user != null)
                {
                    autorizzato = Roles.IsUserInRole(user.UserName, funzionalita);
                }
                else
                {
                    // Se l'utente corrente non � riconosciuto autorizzo le funzionalit� dell'utente pubblico

                    // Utente pubblico idRuolo = 8
                    FunzionalitaCollection funzionalitaUtentePubblico = (new GestioneUtentiBiz()).GetFunzionalitaRuolo(8);
                    if (funzionalitaUtentePubblico.Any(funz => funz.Nome == funzionalita))
                    {
                        autorizzato = true;
                    }
                }
            }
            catch
            {
            }

            return autorizzato;
        }

        /// <summary>
        /// Verifica se l'utente loggato � autorizzato alla funzionalit� passata come parametro, nel caso di nessun utente loggato si considerano valide le funzionalit� dell'Utente pubblico
        /// </summary>
        /// <param name = "funzionalita"></param>
        /// <returns></returns>
        public static bool Autorizzato(FunzionalitaPredefinite funzionalita)
        {
            return Autorizzato(funzionalita.ToString());
        }

        /// <summary>
        ///   Ritorna true se l'utente � un'impresa
        /// </summary>
        /// <returns></returns>
        public static bool IsImpresa()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Impresa))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///   Ritorna true se l'utente � un'impresa
        /// </summary>
        /// <returns></returns>
        public static bool IsConsulente()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Consulente))
            {
                return true;
            }

            return false;
        }

        public static bool IsLavoratore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Lavoratore))
            {
                return true;
            }

            return false;
        }

        public static bool IsFornitore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Fornitore))
            {
                return true;
            }

            return false;
        }

        public static bool IsIspettore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Ispettore))
            {
                return true;
            }

            return false;
        }

        public static bool IsSindacalista()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Sindacalista))
            {
                return true;
            }

            return false;
        }

        public static bool IsCommittente()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Committente))
            {
                return true;
            }

            return false;
        }

        public static bool IsASL()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(ASL))
            {
                return true;
            }

            return false;
        }

        public static bool IsCassaEdile()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(CassaEdile))
            {
                return true;
            }

            return false;
        }

        public static bool IsEsattore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Esattore))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///   Assegna il ruolo LavoratoreSMS all'utente del sistema individuato
        /// </summary>
        /// <param name = "idUtente"></param>
        public static bool AssegnaRuoloLavoratoreSMS(int idUtente)
        {
            bool res = false;

            try
            {
                if (GetGestioneUtenti().gestioneUtentiDataAccess.EsisteUtente(idUtente))
                {
                    int idRuolo =
                        (GetGestioneUtenti()).gestioneUtentiDataAccess.GetIdRuolo(
                            RuoliPredefiniti.LavoratoreSMS.ToString());

                    (GetGestioneUtenti()).gestioneUtentiDataAccess.AssociaUtenteRuolo(idUtente, idRuolo);

                    res = true;
                }
            }
            catch (Exception ex)
            {
                //
            }

            return res;
        }

        /// <summary>
        ///   Toglie il ruolo LavoratoreSMS all'utente del sistema individuato
        /// </summary>
        /// <param name = "idUtente"></param>
        /// <returns></returns>
        public static bool DisassociaRuoloLavoratoreSMS(int idUtente)
        {
            bool res = false;
            try
            {
                if (GetGestioneUtenti().gestioneUtentiDataAccess.EsisteUtente(idUtente))
                {
                    int idRuolo =
                        (GetGestioneUtenti()).gestioneUtentiDataAccess.GetIdRuolo(
                            RuoliPredefiniti.LavoratoreSMS.ToString());

                    (GetGestioneUtenti()).gestioneUtentiDataAccess.DisassociaUtenteRuolo(idUtente, idRuolo);

                    //TODO da correggere quando la ExecuteNonQuery funzioner�, per ora siamo ottimisti...
                    res = true;
                }
            }
            catch (Exception ex)
            {
                //
            }

            return res;
        }

        #endregion

        #region Metodi per la gestione dell'utente/ruoli/funzionalit�

        /// <summary>
        ///   Ritorna tutte le imprese
        /// </summary>
        /// <returns></returns>
        public ImpreseCollection GetImpreseAll()
        {
            ImpreseCollection imprese = null;

            try
            {
                imprese = gestioneUtentiDataAccess.GetImpreseAll();
                return imprese;
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetImpreseAll", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

                return imprese;
            }
        }

        /// <summary>
        ///   Ritorna la lista dei CAP compreso il comune e la provincia presenti nelle imprese del DB
        /// </summary>
        public CAPCollection GetImpreseCAP()
        {
            CAPCollection listaCap = null;

            try
            {
                listaCap = gestioneUtentiDataAccess.GetImpreseCAP();
                return listaCap;
            }
            catch (Exception ex)
            {
                return listaCap;
            }
        }

        public UtentiCollection GetUtenti()
        {
            UtentiCollection utenti;

            try
            {
                utenti = gestioneUtentiDataAccess.GetUtenti();
            }
            catch (Exception ex)
            {
                //Inizializzo la struttua di modo che non debba essere gestito il caso null
                utenti = new UtentiCollection();
            }

            return utenti;
        }

        public Utente GetUtente(string username)
        {
            return gestioneUtentiDataAccess.GetUtente(username);
        }

        /// <summary>
        /// </summary>
        /// <param name = "login"></param>
        /// <param name = "password"></param>
        /// <param name = "nuovaPassword"></param>
        /// <returns></returns>
        public bool CambiaPassword(string login, string password, string nuovaPassword)
        {
            try
            {
                Utente utente = gestioneUtentiDataAccess.GetUtente(login, password);

                if (utente != null)
                {
                    utente.Password = nuovaPassword;

                    gestioneUtentiDataAccess.AggiornaUtente(utente);

                    return true;
                }


                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public LavoratoriCollection GetUtentiLavoratori()
        {
            return gestioneUtentiDataAccess.GetUtentiLavoratori();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ImpreseCollection GetUtentiImprese()
        {
            return gestioneUtentiDataAccess.GetUtentiImprese();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ConsulentiCollection GetUtentiConsulenti()
        {
            return gestioneUtentiDataAccess.GetUtentiConsulenti();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public SindacalistiCollection GetUtentiSindacalisti()
        {
            return gestioneUtentiDataAccess.GetUtentiSindacalisti();
        }

        public ASLCollection GetUtentiASL()
        {
            return gestioneUtentiDataAccess.GetUtentiASL();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public CommittenteCollection GetUtentiCommittenti()
        {
            return gestioneUtentiDataAccess.GetUtentiCommittenti();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public DipendentiCollection GetUtentiDipendenti()
        {
            return gestioneUtentiDataAccess.GetUtentiDipendenti();
        }

        public IspettoriCollection GetUtentiIspettori()
        {
            return gestioneUtentiDataAccess.GetUtentiIspettori();
        }

        public EsattoriCollection GetUtentiEsattori()
        {
            return gestioneUtentiDataAccess.GetUtentiEsattori();
        }

        /// <summary>
        ///   Ritorna l'elenco dei fornitori utenti
        /// </summary>
        /// <returns></returns>
        public FornitoriCollection GetUtentiFornitori()
        {
            return gestioneUtentiDataAccess.GetUtentiFornitori();
        }

        /// <summary>
        ///   Ritorna l'elenco degli ospiti utenti
        /// </summary>
        /// <returns></returns>
        public OspitiCollection GetUtentiOspiti()
        {
            return gestioneUtentiDataAccess.GetUtentiOspiti();
        }

        /// <summary>
        ///   Ritorna l'elenco degli utenti Cassa Edile
        /// </summary>
        /// <returns></returns>
        public CasseEdiliCollection GetUtentiCasseEdili()
        {
            return gestioneUtentiDataAccess.GetUtentiCasseEdili();
        }

        /// <summary>
        ///   Permette di aggiornare il ruolo e contemporaneamente le funzionalit� associate al ruolo
        /// </summary>
        /// <param name = "ruolo"></param>
        /// <param name = "funzionalita"></param>
        public bool AggiornaFunzionalitaRuolo(Ruolo ruolo, FunzionalitaCollection funzionalita)
        {
            try
            {
                //Aggiorniamo il ruolo
                gestioneUtentiDataAccess.AggiornaRuolo(ruolo);

                //Aggiorniamo le funzionalit� associate al ruolo
                int res = gestioneUtentiDataAccess.AssociaFuzionalitaRuolo(ruolo.IdRuolo, funzionalita);
                if (res > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "idUtente"></param>
        /// <param name = "ruoli"></param>
        public void AssegnaRuoliUtente(int idUtente, RuoliCollection ruoli)
        {
            gestioneUtentiDataAccess.AssociaUtenteRuoli(idUtente, ruoli);
        }

        /// <summary>
        /// </summary>
        /// <param name = "utente"></param>
        /// <param name = "ruoli"></param>
        public void AssegnaRuoliUtente(Utente utente, RuoliCollection ruoli)
        {
            gestioneUtentiDataAccess.AssociaUtenteRuoli(utente.IdUtente, ruoli);
        }

        /// <summary>
        /// </summary>
        /// <param name = "utente"></param>
        /// <param name = "ruolo"></param>
        public void AssociaRuoloUtente(Utente utente, Ruolo ruolo)
        {
            gestioneUtentiDataAccess.AssociaUtenteRuolo(utente.IdUtente, ruolo.IdRuolo);
        }

        /// <summary>
        /// </summary>
        /// <param name = "utente"></param>
        /// <param name = "ruolo"></param>
        public void DisassociaUtenteRuolo(Utente utente, Ruolo ruolo)
        {
            gestioneUtentiDataAccess.DisassociaUtenteRuolo(utente.IdUtente, ruolo.IdRuolo);
        }

        #region Abilita disabilita utente

        /// <summary>
        /// </summary>
        /// <param name = "utente"></param>
        /// <returns></returns>
        public bool DisabilitaUtente(Utente utente)
        {
            try
            {
                //Assegnamo il ruolo di utente disabilitato
                Ruolo ruoloUtenteDisabilitato =
                    gestioneUtentiDataAccess.GetRuolo(RuoliPredefiniti.UtenteDisabilitato.ToString());

                if (ruoloUtenteDisabilitato != null)
                {
                    gestioneUtentiDataAccess.AssociaUtenteRuolo(utente.IdUtente, ruoloUtenteDisabilitato.IdRuolo);
                    return true;
                }

                //Se non siamo usciti dal metodo con true usciamo con false per segnalare l'errore
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "utente"></param>
        /// <returns></returns>
        public bool AbilitaUtente(Utente utente)
        {
            try
            {
                //Disassociamo il ruolo di utente disabilitato
                Ruolo ruoloUtenteDisabilitato =
                    gestioneUtentiDataAccess.GetRuolo(RuoliPredefiniti.UtenteDisabilitato.ToString());
                if (ruoloUtenteDisabilitato != null)
                {
                    int res =
                        gestioneUtentiDataAccess.DisassociaUtenteRuolo(utente.IdUtente, ruoloUtenteDisabilitato.IdRuolo);

                    if (res > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Metodi per la registrazione degli utenti al sistema

        /// <summary>
        ///   Metodo per assegnare username e password ad utenti gi� presenti nel sistema
        /// </summary>
        /// <param name = "impresa"></param>
        /// <returns></returns>
        public ErroriRegistrazione RegistraImpresa(Impresa impresa)
        {
            try
            {
                int idUtente = ChallengeImpresaPIN(impresa.IdImpresa, impresa.PIN);

                impresa.IdUtente = idUtente;

                if (idUtente != -1)
                {
                    if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(impresa.IdUtente))
                    {
                        try
                        {
                            Membership.UpdateUser(impresa);
                            gestioneUtentiDataAccess.AssociaUtenteRuolo(impresa.IdUtente,
                                                                        RuoliPredefiniti.Impresa.ToString());

                            return ErroriRegistrazione.RegistrazioneEffettuata;
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.RegistrazioneGiaPresente;
                    }
                }
                else
                {
                    return ErroriRegistrazione.ChallengeNonPassato;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(SqlException))
                {
                    if (((SqlException) (ex)).Number == 2627)
                        return ErroriRegistrazione.LoginPresente;
                }
                return ErroriRegistrazione.Errore;
            }

            #region old

            //try
            //{
            //    //int idUtente = ChallengeImpresa(impresa.RagioneSociale, impresa.PartitaIVA);
            //    int idUtente = ChallengeImpresaPIN(impresa.IdImpresa, impresa.PIN);

            //    impresa.IdUtente = idUtente;

            //    if (idUtente != -1)
            //    {
            //        Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //        Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            //        if (!patternUsername.IsMatch(impresa.UserName)) return ErroriRegistrazione.LoginNonCorretta;
            //        if (!patternPassword.IsMatch(impresa.Password)) return ErroriRegistrazione.PasswordNonCorretta;

            //        if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(impresa.IdUtente))
            //        {
            //            //Impresa ricosciuta, quindi deve essere utente del sistema (c'� un trigger che lo fa)
            //            if (gestioneUtentiDataAccess.RegistraUtente(impresa))
            //            {
            //                //Registriamo il ruolo Impresa di default
            //                gestioneUtentiDataAccess.AssociaUtenteRuolo(impresa.IdUtente,
            //                                                            RuoliPredefiniti.Impresa.ToString());
            //                {
            //                    //ACTIVITY TRACKING
            //                    //LogItemCollection logItemCollection2 = new LogItemCollection();
            //                    //logItemCollection2.Add("Login", impresa.Username);
            //                    //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //                    //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //                    //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //                    //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //                    //logItemCollection2.Add("PIN", impresa.PIN);
            //                    //Log.Write("Impresa registrata", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGGING);

            //                    //return true;
            //                    return ErroriRegistrazione.RegistrazioneEffettuata;
            //                }
            //            }
            //            else
            //                return ErroriRegistrazione.LoginPresente;
            //        }
            //        else
            //        {
            //            //ACTIVITY TRACKING
            //            //LogItemCollection logItemCollection2 = new LogItemCollection();
            //            //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //            //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //            //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //            //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //            //logItemCollection2.Add("PIN", impresa.PIN);
            //            //Log.Write("Utente Impresa gi� presente", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //            //return false;
            //            return ErroriRegistrazione.RegistrazioneGiaPresente;
            //        }
            //    }
            //    else
            //    {
            //        //L'impresa non ha passato la sfida quindi i suoi dati non sono nel db di cassa edile

            //        //ACTIVITY TRACKING
            //        //LogItemCollection logItemCollection2 = new LogItemCollection();
            //        //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //        //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //        //logItemCollection2.Add("PIN", impresa.PIN);
            //        //Log.Write("Impresa challenge non passato", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //        //return false;
            //        return ErroriRegistrazione.ChallengeNonPassato;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ACTIVITY TRACKING
            //    //LogItemCollection logItemCollection = new LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.RegistraImpresa", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

            //    if (ex.GetType() == typeof (SqlException))
            //    {
            //        if (((SqlException) (ex)).Number == 2627)
            //            return ErroriRegistrazione.LoginPresente;
            //    }
            //    //return false;
            //    return ErroriRegistrazione.Errore;
            //}

            #endregion
        }

        public ErroriRegistrazione RegistraConsulente(Consulente consulente)
        {
            try
            {
                int idUtente = ChallengeConsulentePIN(consulente.IdConsulente, consulente.PIN);

                consulente.IdUtente = idUtente;

                if (idUtente != -1)
                {
                    if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(consulente.IdUtente))
                    {
                        try
                        {
                            Membership.UpdateUser(consulente);
                            gestioneUtentiDataAccess.AssociaUtenteRuolo(consulente.IdUtente,
                                                                        RuoliPredefiniti.Consulente.ToString());

                            return ErroriRegistrazione.RegistrazioneEffettuata;
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.RegistrazioneGiaPresente;
                    }
                }
                else
                {
                    return ErroriRegistrazione.ChallengeNonPassato;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(SqlException))
                {
                    if (((SqlException) (ex)).Number == 2627)
                        return ErroriRegistrazione.LoginPresente;
                }
                return ErroriRegistrazione.Errore;
            }

            #region old

            //try
            //{
            //    //int idUtente = ChallengeImpresa(impresa.RagioneSociale, impresa.PartitaIVA);
            //    int idUtente = ChallengeConsulentePIN(consulente.IdConsulente, consulente.PIN);

            //    consulente.IdUtente = idUtente;

            //    if (idUtente != -1)
            //    {
            //        Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //        Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            //        if (!patternUsername.IsMatch(consulente.UserName)) return ErroriRegistrazione.LoginNonCorretta;
            //        if (!patternPassword.IsMatch(consulente.Password)) return ErroriRegistrazione.PasswordNonCorretta;

            //        if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(consulente.IdUtente))
            //        {
            //            //Impresa ricosciuta, quindi deve essere utente del sistema (c'� un trigger che lo fa)
            //            if (gestioneUtentiDataAccess.RegistraUtente(consulente))
            //            {
            //                //Registriamo il ruolo Impresa di default
            //                gestioneUtentiDataAccess.AssociaUtenteRuolo(consulente.IdUtente,
            //                                                            RuoliPredefiniti.Consulente.ToString());
            //                {
            //                    //ACTIVITY TRACKING
            //                    //LogItemCollection logItemCollection2 = new LogItemCollection();
            //                    //logItemCollection2.Add("Login", impresa.Username);
            //                    //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //                    //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //                    //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //                    //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //                    //logItemCollection2.Add("PIN", impresa.PIN);
            //                    //Log.Write("Impresa registrata", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGGING);

            //                    //return true;
            //                    return ErroriRegistrazione.RegistrazioneEffettuata;
            //                }
            //            }
            //            else
            //                return ErroriRegistrazione.LoginPresente;
            //        }
            //        else
            //        {
            //            //ACTIVITY TRACKING
            //            //LogItemCollection logItemCollection2 = new LogItemCollection();
            //            //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //            //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //            //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //            //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //            //logItemCollection2.Add("PIN", impresa.PIN);
            //            //Log.Write("Utente Impresa gi� presente", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //            //return false;
            //            return ErroriRegistrazione.RegistrazioneGiaPresente;
            //        }
            //    }
            //    else
            //    {
            //        //L'impresa non ha passato la sfida quindi i suoi dati non sono nel db di cassa edile

            //        //ACTIVITY TRACKING
            //        //LogItemCollection logItemCollection2 = new LogItemCollection();
            //        //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //        //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //        //logItemCollection2.Add("PIN", impresa.PIN);
            //        //Log.Write("Impresa challenge non passato", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //        //return false;
            //        return ErroriRegistrazione.ChallengeNonPassato;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ACTIVITY TRACKING
            //    //LogItemCollection logItemCollection = new LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.RegistraImpresa", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

            //    if (ex.GetType() == typeof (SqlException))
            //    {
            //        if (((SqlException) (ex)).Number == 2627)
            //            return ErroriRegistrazione.LoginPresente;
            //    }
            //    //return false;
            //    return ErroriRegistrazione.Errore;
            //}

            #endregion
        }

        /// <summary>
        /// </summary>
        /// <param name = "lavoratore"></param>
        /// <returns></returns>
        public ErroriRegistrazione RegistraLavoratore(Lavoratore lavoratore)
        {
            try
            {
                int idUtente =
                    ChallengeLavoratore(lavoratore.Nome, lavoratore.Cognome, lavoratore.CodiceFiscale, lavoratore.Pin);

                lavoratore.IdUtente = idUtente;

                if (idUtente != -1)
                {
                    if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(lavoratore.IdUtente))
                    {
                        try
                        {
                            Membership.UpdateUser(lavoratore);
                            gestioneUtentiDataAccess.AssociaUtenteRuolo(lavoratore.IdUtente,
                                                                        RuoliPredefiniti.Lavoratore.ToString());

                            return ErroriRegistrazione.RegistrazioneEffettuata;
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.RegistrazioneGiaPresente;
                    }
                }
                else
                {
                    return ErroriRegistrazione.ChallengeNonPassato;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(SqlException))
                {
                    if (((SqlException) (ex)).Number == 2627)
                        return ErroriRegistrazione.LoginPresente;
                }
                return ErroriRegistrazione.Errore;
            }

            #region old

            //try
            //{
            //    int idUtente =
            //        ChallengeLavoratore(lavoratore.Nome, lavoratore.Cognome, lavoratore.CodiceFiscale, lavoratore.Pin);

            //    lavoratore.IdUtente = idUtente;

            //    if (idUtente != -1)
            //    {
            //        Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //        Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            //        if (!patternUsername.IsMatch(lavoratore.UserName)) return ErroriRegistrazione.LoginNonCorretta;
            //        if (!patternPassword.IsMatch(lavoratore.Password)) return ErroriRegistrazione.PasswordNonCorretta;

            //        if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(lavoratore.IdUtente))
            //        {
            //            //lavoratore ricosciuta, quindi deve essere utente del sistema (c'� un trigger che lo fa)
            //            if (gestioneUtentiDataAccess.RegistraUtente(lavoratore))
            //            {
            //                //Registriamo il ruolo lavoratore di default
            //                gestioneUtentiDataAccess.AssociaUtenteRuolo(lavoratore.IdUtente,
            //                                                            RuoliPredefiniti.Lavoratore.ToString());

            //                //ACTIVITY TRACKING
            //                //LogItemCollection logItemCollection2 = new LogItemCollection();
            //                //logItemCollection2.Add("Login", lavoratore.Username);
            //                //logItemCollection2.Add("IdUtente", lavoratore.IdUtente.ToString());
            //                //logItemCollection2.Add("IdLavoratore", lavoratore.IdLavoratore.ToString());
            //                //logItemCollection2.Add("Nome", lavoratore.Nome);
            //                //logItemCollection2.Add("Cognome", lavoratore.Cognome);
            //                //logItemCollection2.Add("CodiceFiscale", lavoratore.CodiceFiscale);
            //                //Log.Write("Lavoratore registrata", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGGING);

            //                //return true;
            //                return ErroriRegistrazione.RegistrazioneEffettuata;
            //            }
            //            return ErroriRegistrazione.LoginPresente;
            //        }
            //        else
            //        {
            //            //ACTIVITY TRACKING
            //            //LogItemCollection logItemCollection2 = new LogItemCollection();
            //            //logItemCollection2.Add("Nome", lavoratore.Nome);
            //            //logItemCollection2.Add("Cognome", lavoratore.Cognome);
            //            //logItemCollection2.Add("CodiceFiscale", lavoratore.CodiceFiscale);
            //            //Log.Write("Utente Lavoratore gi� presente", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //            //return false; 
            //            return ErroriRegistrazione.RegistrazioneGiaPresente;
            //        }
            //    }
            //    else
            //    {
            //        //Il lavoratore non ha passato la sfida quindi i suoi dati non sono nel db di cassa edile

            //        //ACTIVITY TRACKING
            //        //LogItemCollection logItemCollection2 = new LogItemCollection();
            //        //logItemCollection2.Add("Nome", lavoratore.Nome);
            //        //logItemCollection2.Add("Cognome", lavoratore.Cognome);
            //        //logItemCollection2.Add("CodiceFiscale", lavoratore.CodiceFiscale);
            //        //Log.Write("Lavoratore challenge non passato", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //        //return false;
            //        return ErroriRegistrazione.ChallengeNonPassato;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ACTIVITY TRACKING
            //    //LogItemCollection logItemCollection = new LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.RegistraLavoratore", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

            //    if (ex.GetType() == typeof (SqlException))
            //    {
            //        if (((SqlException) (ex)).Number == 2627)
            //            return ErroriRegistrazione.LoginPresente;
            //    }
            //    //return false;
            //    return ErroriRegistrazione.Errore;
            //}

            #endregion
        }

        /// <summary>
        ///   Inserimento di un dipendente
        /// </summary>
        /// <param name = "dipendente"></param>
        /// <returns></returns>
        public ErroriRegistrazione InserisciDipendente(Dipendente dipendente)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = gestioneUtentiDataAccess.InserisciDipendente(dipendente);

                    if (res > 0)
                    {
                        try
                        {
                            dipendente.IdUtente = res;
                            Membership.UpdateUser(dipendente);

                            try
                            {
                                gestioneUtentiDataAccess.AssociaUtenteRuolo(dipendente.IdUtente,
                                                                            RuoliPredefiniti.Dipendente.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.Errore;
                    }
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        ///   Inserimento di un ispettore
        /// </summary>
        /// <param name = "ispettore"></param>
        /// <returns>True se il dipendente � stato inserito correttemente</returns>
        public ErroriRegistrazione InserisciIspettore(Ispettore ispettore)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = gestioneUtentiDataAccess.InserisciIspettore(ispettore);

                    if (res > 0)
                    {
                        try
                        {
                            ispettore.IdUtente = res;
                            Membership.UpdateUser(ispettore);

                            try
                            {
                                gestioneUtentiDataAccess.AssociaUtenteRuolo(ispettore.IdUtente,
                                                                            RuoliPredefiniti.Dipendente.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.Errore;
                    }
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }

            #region old

            //try
            //{
            //    int res = gestioneUtentiDataAccess.InserisciIspettore(ispettore);

            //    if (res > 0)
            //    {
            //        //Registriamo il ruolo dipendente all'ispettore di default
            //        gestioneUtentiDataAccess.AssociaUtenteRuolo(ispettore.IdUtente,
            //                                                    RuoliPredefiniti.Dipendente.ToString());

            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}

            #endregion
        }

        /// <summary>
        ///   Inserimento di un sindacalista
        /// </summary>
        /// <param name = "sindacalista"></param>
        /// <returns>True se il dipendente � stato inserito correttemente</returns>
        public ErroriRegistrazione InserisciSindacalista(Sindacalista sindacalista)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = gestioneUtentiDataAccess.InserisciSindacalista(sindacalista);

                    if (res > 0)
                    {
                        try
                        {
                            sindacalista.IdUtente = res;
                            Membership.UpdateUser(sindacalista);

                            try
                            {
                                gestioneUtentiDataAccess.AssociaUtenteRuolo(sindacalista.IdUtente,
                                                                            RuoliPredefiniti.Sindacalista.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.Errore;
                    }
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        ///   Inserimento di un committente
        /// </summary>
        /// <param name = "committente"></param>
        /// <returns></returns>
        public ErroriRegistrazione InserisciCommittente(Committente committente)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = gestioneUtentiDataAccess.InserisciCommittente(committente);

                    if (res > 0)
                    {
                        try
                        {
                            committente.IdUtente = res;
                            Membership.UpdateUser(committente);

                            try
                            {
                                gestioneUtentiDataAccess.AssociaUtenteRuolo(committente.IdUtente,
                                                                            RuoliPredefiniti.Committente.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.Errore;
                    }
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        public bool ModificaIspettore(Ispettore ispettore)
        {
            try
            {
                bool res = gestioneUtentiDataAccess.ModificaIspettore(ispettore);

                return res;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///   Inserimento di un fornitore
        /// </summary>
        /// <param name = "fornitore"></param>
        /// <returns>True se il fornitore � stato inserito correttemente</returns>
        public ErroriRegistrazione InserisciFornitore(Fornitore fornitore)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = gestioneUtentiDataAccess.InserisciFornitore(fornitore);

                    if (res > 0)
                    {
                        try
                        {
                            fornitore.IdUtente = res;
                            Membership.UpdateUser(fornitore);

                            try
                            {
                                gestioneUtentiDataAccess.AssociaUtenteRuolo(fornitore.IdUtente,
                                                                            RuoliPredefiniti.Fornitore.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.Errore;
                    }
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "ospite"></param>
        /// <returns></returns>
        public ErroriRegistrazione InserisciOspite(Ospite ospite)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = gestioneUtentiDataAccess.InserisciOspite(ospite);

                    if (res > 0)
                    {
                        try
                        {
                            ospite.IdUtente = res;
                            Membership.UpdateUser(ospite);

                            try
                            {
                                gestioneUtentiDataAccess.AssociaUtenteRuolo(ospite.IdUtente,
                                                                            RuoliPredefiniti.Ospite.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    else
                    {
                        return ErroriRegistrazione.Errore;
                    }
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected int ChallengeLavoratore(string nome, string cognome, string codiceFiscale, string pin)
        {
            //ritorna l'idutente
            try
            {
                int res = gestioneUtentiDataAccess.ChallengeLavoratore(nome, cognome, codiceFiscale, pin);

                return res;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        //protected int ChallengeImpresa(string ragioneSociale, string partitaIVA)
        //{
        //    //ritorna l'idutente
        //    try
        //    {
        //        int res = gestioneUtentiDataAccess.ChallengeImpresa(ragioneSociale, partitaIVA);

        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        return -1;
        //    }
        //}

        /// <summary>
        ///   Ritorna l'idUtente nel caso in cui nel DB sia presente un impresa con l'id passato e il PIN passato
        /// </summary>
        /// <param name = "idImpresa"></param>
        /// <param name = "PIN"></param>
        /// <returns></returns>
        protected int ChallengeImpresaPIN(int idImpresa, string PIN)
        {
            //ritorna l'idutente
            try
            {
                int res = gestioneUtentiDataAccess.ChallengeImpresaPIN(idImpresa, PIN);

                return res;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        /// <summary>
        ///   Ritorna l'Utente dato il suo id
        /// </summary>
        /// <param name = "idUtente"></param>
        /// <returns></returns>
        public Utente GetUtenteById(int idUtente)
        {
            try
            {
                Utente res = gestioneUtentiDataAccess.GetUtenteById(idUtente);

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        ///   Ritorna l'idUtente nel caso in cui nel DB sia presente un impresa con l'id passato e il PIN passato
        /// </summary>
        /// <param name = "idConsulente"></param>
        /// <param name = "PIN"></param>
        /// <returns></returns>
        protected int ChallengeConsulentePIN(int idConsulente, string PIN)
        {
            //ritorna l'idutente
            try
            {
                int res = gestioneUtentiDataAccess.ChallengeConsulentePIN(idConsulente, PIN);

                return res;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        #endregion

        #region Generazione PIN

        public string GeneraPin()
        {
            return Guid.NewGuid().ToString().ToUpper().Substring(0, 8);
        }

        /// <summary>
        ///   Genera un PIN per l'impresa
        /// </summary>
        /// <param name = "id">Id dell'impresa</param>
        public string GeneraPinImpresa(int id)
        {
            string pin = GeneraPin();
            gestioneUtentiDataAccess.AggiornaPinImpresa(id, pin);
            return pin;
        }

        /// <summary>
        ///   Genera un PIN per il lavoratore
        /// </summary>
        /// <param name = "id">Id del lavoratore</param>
        public string GeneraPinLavoratore(int id)
        {
            string pin = GeneraPin();
            gestioneUtentiDataAccess.AggiornaPinLavoratore(id, pin);
            return pin;
        }

        /// <summary>
        ///   Genera un PIN per il consulente
        /// </summary>
        /// <param name = "id">Id del consulente</param>
        public string GeneraPinConsulente(int id)
        {
            string pin = GeneraPin();
            gestioneUtentiDataAccess.AggiornaPinConsulente(id, pin);
            return pin;
        }

        #endregion

        #endregion

        #region Gestione Ruoli/Funzionalita

        /// <summary>
        /// </summary>
        /// <param name = "idUtente"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaUtente(int idUtente)
        {
            try
            {
                return gestioneUtentiDataAccess.GetFunzionalitaUtente(idUtente);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "idUtente"></param>
        /// <returns></returns>
        public RuoliCollection GetRuoliUtente(int idUtente)
        {
            try
            {
                return gestioneUtentiDataAccess.GetRuoliUtente(idUtente);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// </summary>
        /// <returns></returns>
        public void CreaRuolo(Ruolo ruolo, FunzionalitaCollection funzionalita)
        {
            try
            {
                int idRuolo = gestioneUtentiDataAccess.CreaRuolo(ruolo);

                AssociaFunzionalitaRuolo(idRuolo, funzionalita);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public RuoliCollection GetRuoli()
        {
            try
            {
                return gestioneUtentiDataAccess.GetRuoli();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "idRuolo"></param>
        /// <returns></returns>
        public Ruolo GetRuolo(int idRuolo)
        {
            return gestioneUtentiDataAccess.GetRuolo(idRuolo);
        }

        /// <summary>
        /// </summary>
        /// <param name = "nomeRuolo"></param>
        /// <returns></returns>
        public Ruolo GetRuolo(string nomeRuolo)
        {
            return gestioneUtentiDataAccess.GetRuolo(nomeRuolo);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalita()
        {
            try
            {
                return gestioneUtentiDataAccess.GetFunzionalita();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "idRuolo"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaRuolo(int idRuolo)
        {
            try
            {
                return gestioneUtentiDataAccess.GetFunzionalitaRuolo(idRuolo);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        ///   Aggiorna solamente le funzoinalit� associate ad un ruolo senza modificare il ruolo
        /// </summary>
        /// <param name = "idRuolo"></param>
        /// <param name = "funzionalita"></param>
        /// <returns></returns>
        public bool AssociaFunzionalitaRuolo(int idRuolo, FunzionalitaCollection funzionalita)
        {
            try
            {
                gestioneUtentiDataAccess.AssociaFuzionalitaRuolo(idRuolo, funzionalita);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "idRuolo"></param>
        public void CancellaRuolo(int idRuolo)
        {
            gestioneUtentiDataAccess.CancellaRuolo(idRuolo);
        }

        #endregion

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected static GestioneUtentiBiz GetGestioneUtenti()
        {
            if (_gestioneUtentiBiz == null)
                _gestioneUtentiBiz = new GestioneUtentiBiz();

            return _gestioneUtentiBiz;
        }

        public LavoratoriConImpresaCollection GetLavoratoriConImpresa(FilterLavoratore filter)
        {
            return gestioneUtentiDataAccess.GetLavoratoriConImpresa(filter);
        }

        public LavoratoriCollection GetLavoratoriConTel(FilterLavoratore filter)
        {
            return gestioneUtentiDataAccess.GetLavoratoriConTel(filter);
        }

        public LavoratoriCollection GetLavoratoriConTelPrest(FilterLavoratorePrest filter)
        {
            return gestioneUtentiDataAccess.GetLavoratoriConTelPrest(filter);
        }

        public bool RichiediAggiornamentoIndirizzoLavoratore(int idLavoratore, string indirizzoDenominazione,
                                                             string indirizzoCAP, string indirizzoProvincia,
                                                             string indirizzoComune)
        {
            return
                gestioneUtentiDataAccess.RichiediAggiornamentoIndirizzoLavoratore(idLavoratore, indirizzoDenominazione,
                                                                                  indirizzoCAP, indirizzoProvincia,
                                                                                  indirizzoComune);
        }

        public DataSet CaricaCodiciDismessi()
        {
            return gestioneUtentiDataAccess.CaricaCodiciDismessi();
        }

        public DataSet CaricaOperazioni(int codDismesso, int codValido)
        {
            return gestioneUtentiDataAccess.CaricaOperazioni(codDismesso, codValido);
        }

        public ConsulentiCollection GetConsulenti(string ragioneSociale, string codiceFiscale, bool? conPIN,
                                                  DateTime? dal, DateTime? al, int? idConsulente)
        {
            ConsulentiCollection consulenti = null;

            try
            {
                consulenti =
                    gestioneUtentiDataAccess.GetConsulenti(ragioneSociale, codiceFiscale, conPIN, dal, al, idConsulente);
            }
            catch (Exception exc)
            {
            }

            return consulenti;
        }

        public List<ConsulenteConCredenziali> GetConsulentiConCredenziali(ConsulenteFilter filter)
        {
            return gestioneUtentiDataAccess.GetConsulentiConCredenziali(filter);
        }

        public LavoratoriConImpresaSmsCollection GetLavoratoriConImpresaSms(FilterLavoratore filter)
        {
            return gestioneUtentiDataAccess.GetLavoratoriConImpresaSms(filter);
        }

        public List<ImpresaConCredenziali> GetImprese(ImpresaFilter filtro)
        {
            return gestioneUtentiDataAccess.GetImprese(filtro);
        }

        public List<LavoratoreConImpresaSmsCredenziali> GetLavoratoriConImpresaSmsCredenziali(FilterLavoratore filter)
        {
            return gestioneUtentiDataAccess.GetLavoratoriConImpresaSmsCredenziali(filter);
        }

        public void CaricaProvinceInDropDown(DropDownList dropDownProvince)
        {
            DataTable dtProvince = commonBiz.GetProvince();

            dropDownProvince.Items.Clear();
            dropDownProvince.Items.Add(new ListItem(string.Empty, null));

            dropDownProvince.DataSource = dtProvince;
            dropDownProvince.DataTextField = "sigla";
            dropDownProvince.DataValueField = "idProvincia";

            dropDownProvince.DataBind();
        }

        public void CaricaComuniInDropDown(DropDownList dropDownComuni, int idProvincia)
        {
            dropDownComuni.Items.Clear();
            dropDownComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idProvincia > 0)
            {
                DataTable dtComuni = commonBiz.GetComuniDellaProvincia(idProvincia);

                dropDownComuni.DataSource = dtComuni;
                dropDownComuni.DataTextField = "denominazione";
                dropDownComuni.DataValueField = "idComune";
            }

            dropDownComuni.DataBind();
        }

        public void CaricaCapInDropDown(DropDownList dropDownCap, Int64 idComune)
        {
            dropDownCap.Items.Clear();
            dropDownCap.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idComune > 0)
            {
                DataTable dtCAP = commonBiz.GetCAPDelComune(idComune);

                dropDownCap.DataSource = dtCAP;
                dropDownCap.DataTextField = "cap";
                dropDownCap.DataValueField = "cap";
            }

            dropDownCap.DataBind();
        }

        public void CambioSelezioneDropDownProvincia(DropDownList ddlProvincia, DropDownList ddlComune,
                                                     DropDownList ddlCap)
        {
            int idProvincia = -1;

            if (ddlProvincia.SelectedValue != null)
                Int32.TryParse(ddlProvincia.SelectedValue, out idProvincia);

            CaricaComuniInDropDown(ddlComune, idProvincia);

            CaricaCapInDropDown(ddlCap, -1);
        }

        public void CambioSelezioneDropDownComune(DropDownList ddlComune, DropDownList ddlCap)
        {
            long idComune = -1;

            if (ddlComune.SelectedValue != null)
                Int64.TryParse(ddlComune.SelectedValue, out idComune);

            CaricaCapInDropDown(ddlCap, idComune);
        }

        public LavoratoreCredential GetLavoratoreCredential(int idLavoratore)
        {
            LavoratoreCredential credential = new LavoratoreCredential { IdLavoratore = idLavoratore };

            FilterLavoratore filter = new FilterLavoratore { IdLavoratore = idLavoratore };
            List<LavoratoreConImpresaSmsCredenziali> lavoratori = GetLavoratoriConImpresaSmsCredenziali(filter);

            if (lavoratori.Count > 0)
            {
                credential.Username = lavoratori[0].Username;
                credential.Password = lavoratori[0].Password;
            }

            return credential;
        }

        public bool ResetPassword(string login, string pin, string nuovaPassword, out Int32? idUtente)
        {
            return gestioneUtentiDataAccess.ResetPassword(login, pin, nuovaPassword, out idUtente);
        }

        public static bool ControllaFormatoPassword(string password)
        {
            return !string.IsNullOrEmpty(password);
        }

        public DateTime GetScadenzaPasswordFromUsername(string username)
        {
            return gestioneUtentiDataAccess.GetScadenzaPasswordFromUsername(username);
        }

        public void AggiornaScadenzaPassword(int idUtente, int numeroMesi)
        {
            gestioneUtentiDataAccess.AggiornaScadenzaPassword(idUtente, numeroMesi);
        }

        public static String GetUserNamePerEdilConnect(Int32 tipoUtente, Int32 codice, String guid, String partitaIva)
        {
            String userName = null;

            switch (tipoUtente)
            {
                case 1:
                    // Consulente
                    using (SICEEntities context = new SICEEntities())
                    {
                        var queryConsulenti = from consulente in context.Consulenti
                                              where consulente.Id == codice
                                                && consulente.CodiceFiscale == partitaIva
                                                && consulente.GuidEdilconnect == new Guid(guid)
                                                && !String.IsNullOrEmpty(consulente.Utenti.Password)
                                                && (!consulente.Utenti.DataScadenzaPassword.HasValue || consulente.Utenti.DataScadenzaPassword > DateTime.Now)
                                              select consulente;

                        try
                        {
                            TBridge.Cemi.Type.Domain.Consulente consulente = queryConsulenti.Single();
                            if (consulente != null)
                            {
                                userName = consulente.Utenti.Username;
                            }
                        }
                        catch { }
                    }
                    break;

                case 2:
                    // Impresa
                    using (SICEEntities context = new SICEEntities())
                    {
                        var queryImprese = from impresa in context.Imprese
                                           where impresa.Id == codice
                                                && impresa.PartitaIVA == partitaIva
                                                && impresa.GuidEdilconnect == new Guid(guid)
                                                && !String.IsNullOrEmpty(impresa.Utente.Password)
                                                && (!impresa.Utente.DataScadenzaPassword.HasValue || impresa.Utente.DataScadenzaPassword > DateTime.Now)
                                           select impresa;

                        try
                        {
                            TBridge.Cemi.Type.Domain.Impresa impresa = queryImprese.Single();
                            if (impresa != null)
                            {
                                userName = impresa.Utente.Username;
                            }
                        }
                        catch { }
                    }
                    break;
            }

            return userName;
        }
    }
}