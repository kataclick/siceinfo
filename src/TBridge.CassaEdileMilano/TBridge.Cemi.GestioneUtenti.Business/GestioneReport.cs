﻿using System;
using System.Linq;
using System.Web.Security;
using TBridge.Cemi.Data;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public class GestioneReport
    {
        private static Data.GestioneReport gestioneReportData = new Data.GestioneReport();
        public static bool Autorizzato(string nomeReport)
        {
            bool autorizzato = false;

            try
            {
                Utente utente = (Utente) Membership.GetUser();

                if (utente != null)
                {
                    autorizzato = Autorizzato(utente.IdUtente, nomeReport);
                }
            }
            catch
            {
            }

            return autorizzato;
        }

        private static bool Autorizzato(int idUtente, string nomeReport)
        {
            return gestioneReportData.Autorizzato(idUtente,nomeReport);
        }

        public Report GetReport(string nomeReport)
        {
            return gestioneReportData.GetReport(nomeReport);
        }

        public Cemi.Type.Domain.Report GetReport(int idReport)
        {
            using (SICEEntities context = new SICEEntities())
            {
                Cemi.Type.Domain.Report report = (from rep in context.Reports
                                                  where rep.Id == idReport
                                                  select rep).SingleOrDefault();

                return report;
            }
        }

        public ReportCollection GetListaReport()
        {
            return gestioneReportData.GetListaReport();
        }

        public ReportCollection GetListaReport(Int32 idUtente)
        {
            return gestioneReportData.GetListaReport(idUtente);
        }

        public void DeleteUtenteReport(int? idUtente, int idReport)
        {
            gestioneReportData.DeleteUtenteReport(idUtente,idReport);
        }

        public void DeleteReport(int idReport)
        {
            gestioneReportData.DeleteReport(idReport);
        }

        public int InsertReport(Cemi.Type.Domain.Report report)
        {
            //return gestioneReportData.InsertReport(report);

            using (SICEEntities context = new SICEEntities())
            {
                context.Reports.AddObject(report);
                return context.SaveChanges();
            }
        }

        public int AssociaUtenteReport(int idUtente, int idReport)
        {
            //return gestioneReportData.AssociaUtenteReport(idUtente, idReport);

            using (SICEEntities context = new SICEEntities())
            {
                Cemi.Type.Domain.Utente utente = (from ut in context.Utenti
                              where ut.Id == idUtente
                              select ut).SingleOrDefault();

                Cemi.Type.Domain.Report report = (from rep in context.Reports
                              where rep.Id == idReport
                              select rep).SingleOrDefault();

                utente.Reports.Add(report);

                return context.SaveChanges();
            }
        }

        #region GetUtentiReport
        
        public IspettoriCollection GetUtentiReportIspettori(int? idReport)
        {
            return gestioneReportData.GetUtentiReportIspettori(idReport);
        }

        public LavoratoriCollection GetUtentiReportLavoratori(int? idReport)
        {
            return gestioneReportData.GetUtentiReportLavoratori(idReport);
        }

        public ImpreseCollection GetUtentiReportImprese(int? idReport)
        {
            return gestioneReportData.GetUtentiReportImprese(idReport);
        }

        public CommittenteCollection GetUtentiReportCommittenti(int? idReport)
        {
            return gestioneReportData.GetUtentiReportCommittenti(idReport);
        }

        public ConsulentiCollection GetUtentiReportConsulenti(int? idReport)
        {
            return gestioneReportData.GetUtentiReportConsulenti(idReport);
        }

        public CasseEdiliCollection GetUtentiReportCasseEdili(int? idReport)
        {
            return gestioneReportData.GetUtentiReportCasseEdili(idReport);
        }

        public ASLCollection GetUtentiReportAsl(int? idReport)
        {
            return gestioneReportData.GetUtentiReportAsl(idReport);
        }

        public DipendentiCollection GetUtentiReportDipendenti(int? idReport)
        {
            return gestioneReportData.GetUtentiReportDipendenti(idReport);
        }

        public FornitoriCollection GetUtentiReportFornitori(int? idReport)
        {
            return gestioneReportData.GetUtentiReportFornitori(idReport);
        }

        public EsattoriCollection GetUtentiReportEsattori(int? idReport)
        {
            return gestioneReportData.GetUtentiReportEsattori(idReport);
        }

        public OspitiCollection GetUtentiReportOspiti(int? idReport)
        {
            return gestioneReportData.GetUtentiReportOspiti(idReport);
        }

        public SindacalistiCollection GetUtentiReportSindacalisti(int? idReport)
        {
            return gestioneReportData.GetUtentiReportSindacalisti(idReport);
        }

        #endregion

        public int UpdateReport(Cemi.Type.Domain.Report report)
        {
            using (SICEEntities context = new SICEEntities())
            {
                Cemi.Type.Domain.Report reportPersistito = (from rep in context.Reports
                                                  where rep.Id == report.Id
                                                  select rep).SingleOrDefault();

                reportPersistito.Nome = report.Nome;
                reportPersistito.Theme = report.Theme;
                
                return context.SaveChanges();
            }
        }
    }
}