﻿using System;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public class LavoratoriManager
    {
        public Lavoratore GetLavoratoreById(int idLavoratore)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from lavoratore in context.Lavoratori
                            where lavoratore.Id == idLavoratore
                            select lavoratore;

                return query.SingleOrDefault();
            }
        }

        public SmsRecapitoUtente GetRecapitoSmsLavoratore(int idUtente)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from recapito in context.SmsRecapitiUtenti
                            where recapito.IdUtente == idUtente && recapito.DataFineValidita == null
                            select recapito;

                return query.SingleOrDefault();
            }
        }

        public bool SaveRecapitoSmsLavoratore(int idLavoratore, string numero, out Int32? idLavoratoreStessoRecapito)
        {
            int num = -1;
            idLavoratoreStessoRecapito = null;

            numero = Cemi.Business.Common.PulisciNumeroCellulare(numero);

            if (Cemi.Business.Common.NumeroCellulareValido(numero))
            {
                using (SICEEntities context = new SICEEntities())
                {
                    var queryUtente = from lavoratore in context.Lavoratori
                                      where lavoratore.Id == idLavoratore
                                      select lavoratore.IdUtente;

                    int? idUtente = queryUtente.SingleOrDefault();

                    if (idUtente.HasValue)
                    {
                        var queryNumeroNonDisponibile = from smsRecapitoUtente in context.SmsRecapitiUtenti
                                                        join lavoratore in context.Lavoratori on smsRecapitoUtente.IdUtente equals lavoratore.IdUtente
                                                        where
                                                            smsRecapitoUtente.Numero == numero &&
                                                            smsRecapitoUtente.DataFineValidita == null
                                                        select lavoratore.Id;

                        var recapitiStessoNumeroAttivi = queryNumeroNonDisponibile.ToList();

                        if (recapitiStessoNumeroAttivi.Count == 0)
                        {
                            var queryRecapitoAttivo = from recapitoUtente in context.SmsRecapitiUtenti
                                                      where
                                                          recapitoUtente.IdUtente == idUtente &&
                                                          recapitoUtente.DataFineValidita == null
                                                      select recapitoUtente;

                            SmsRecapitoUtente smsRecapitoUtenteAttivo = queryRecapitoAttivo.SingleOrDefault();

                            if (smsRecapitoUtenteAttivo == null || smsRecapitoUtenteAttivo.Numero != numero)
                            {
                                if (smsRecapitoUtenteAttivo != null && smsRecapitoUtenteAttivo.Numero != numero)
                                {
                                    smsRecapitoUtenteAttivo.DataFineValidita = DateTime.Now;
                                }

                                SmsRecapitoUtente nuovoRecapito = new SmsRecapitoUtente
                                                                      {
                                                                          IdUtente = idUtente.Value,
                                                                          Numero = numero,
                                                                          DataInizioValidita = DateTime.Now,
                                                                          DataFineValidita = null,
                                                                          DataInserimentoRecord = DateTime.Now
                                                                      };

                                context.SmsRecapitiUtenti.AddObject(nuovoRecapito);
                            }
                        }
                        else
                        {
                            idLavoratoreStessoRecapito = recapitiStessoNumeroAttivi[0];
                        }
                    }

                    num = context.SaveChanges();
                }
            }

            return (num > 0);
        }

        public bool SaveNotificaInvioSmsPin(int idLavoratore)
        {
            int num;
            using (SICEEntities context = new SICEEntities())
            {
                var queryLavoratore = from lavoratore in context.Lavoratori
                                      where lavoratore.Id == idLavoratore
                                      select lavoratore;

                Lavoratore lav = queryLavoratore.SingleOrDefault();

                if (lav != null)
                    lav.DataInvioSmsPin = DateTime.Now;

                num = context.SaveChanges();
            }

            return (num > 0);
        }

        public bool DisabilitaRecapito(int idUtente)
        {
            int num = 0;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from recapito in context.SmsRecapitiUtenti
                            where recapito.IdUtente == idUtente && recapito.DataFineValidita == null
                            select recapito;

                SmsRecapitoUtente smsRecapitoUtente = query.SingleOrDefault();
                if (smsRecapitoUtente != null)
                {
                    smsRecapitoUtente.DataFineValidita = DateTime.Now;

                    num = context.SaveChanges();
                }
            }

            return num > 0;
        }
    }
}
