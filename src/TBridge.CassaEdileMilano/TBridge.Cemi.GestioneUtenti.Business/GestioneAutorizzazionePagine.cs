﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TBridge.Cemi.GestioneUtenti.Type;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public static class GestioneAutorizzazionePagine
    {
        private static HttpServerUtility Server
        {
            get
            {
                if (HttpContext.Current != null)
                    return HttpContext.Current.Server;
                return null;
            }
        }

        private static HttpRequest Request
        {
            get
            {
                if (HttpContext.Current != null)
                    return HttpContext.Current.Request;
                return null;
            }
        }

        /// <summary>
        ///   Controlliamo se l'utente è autorizzato a visualizzare la pagina, se è tutto a posto non succede nulla, altrimenti si viene reindirizzati
        ///   su una pagina di errori
        /// </summary>
        /// <param name = "funzionalita">Nome della funzionalità per cui si deve essere autorizzati</param>
        /// <param name = "nomePagina"></param>
        [Obsolete("Utilizzare il metodo senza nomePagina")]
        public static void PaginaAutorizzata(string funzionalita, string nomePagina)
        {
            PaginaAutorizzata(funzionalita);
        }

        [Obsolete("Utilizzare il metoto che prende in input enum")]
        public static void PaginaAutorizzata(string funzionalita)
        {
            //Controlliamo se l'utente è autorizzato alla  visualizzazioene della pagina
            if (!GestioneUtentiBiz.Autorizzato(funzionalita))
            {
                //Trasferiamo su una pagina di errore
                Server.Transfer(String.Format("~/DefaultErroreAutenticazione.aspx?ReturnUrl={0}", Request.Url));
            }
        }

        /// <summary>
        ///   Controlliamo se l'utente è autorizzato a visualizzare la pagina, se è tutto a posto non succede nulla, altrimenti si viene reindirizzati
        ///   su una pagina di errori
        /// </summary>
        /// <param name = "funzionalita">Nome della funzionalità per cui si deve essere autorizzati</param>
        /// <param name = "nomePagina"></param>
        [Obsolete("Utilizzare il metodo senza nomePagina")]
        public static void PaginaAutorizzata(FunzionalitaPredefinite funzionalita, string nomePagina)
        {
            PaginaAutorizzata(funzionalita.ToString(), nomePagina);
        }

        public static void PaginaAutorizzata(FunzionalitaPredefinite funzionalita)
        {
            PaginaAutorizzata(funzionalita.ToString());
        }

        [Obsolete("Utilizzare il metodo senza nomePagina")]
        public static void PaginaAutorizzata(string[] funzionalita, string nomePagina)
        {
            PaginaAutorizzata(funzionalita);
        }

        [Obsolete("Utilizzare il metoto che prende in input la lista di enum")]
        public static void PaginaAutorizzata(string[] funzionalita)
        {
            bool ok = funzionalita.Any(GestioneUtentiBiz.Autorizzato);

            //Controlliamo se l'utente è autorizzato alla  visualizzazioene della pagina
            if (!ok)
            {
                //Trasferiamo su una pagina di errore
                Server.Transfer(String.Format("~/DefaultErroreAutenticazione.aspx?ReturnUrl={0}", Request.Url));
            }
        }

        [Obsolete("Utilizzare il metodo senza nomePagina")]
        public static void PaginaAutorizzata(List<FunzionalitaPredefinite> funzionalitaList, string nomePagina)
        {
            PaginaAutorizzata(funzionalitaList);
        }

        public static void PaginaAutorizzata(List<FunzionalitaPredefinite> funzionalitaList)
        {
            string[] funzionalita = new string[funzionalitaList.Count];

            int i = 0;
            foreach (FunzionalitaPredefinite funz in funzionalitaList)
            {
                funzionalita[i] = funz.ToString();
                i++;
            }

            PaginaAutorizzata(funzionalita);
        }
    }
}