//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class AccessoCantieriWhiteList
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual string Indirizzo
        {
            get;
            set;
        }
    
        public virtual string Civico
        {
            get;
            set;
        }
    
        public virtual string Comune
        {
            get;
            set;
        }
    
        public virtual string Provincia
        {
            get;
            set;
        }
    
        public virtual string Cap
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> latitudine
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> longitudine
        {
            get;
            set;
        }
    
        public virtual string infoAggiuntiva
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataInizio
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataFine
        {
            get;
            set;
        }
    
        public virtual string Descrizione
        {
            get;
            set;
        }
    
        public virtual Nullable<System.Guid> guid
        {
            get;
            set;
        }
    
        public virtual int IdUtente
        {
            get;
            set;
        }
    
        public virtual Nullable<int> IdAccessoCantieriCommittente
        {
            get;
            set;
        }
    
        public virtual System.DateTime dataInserimentoRecord
        {
            get;
            set;
        }
    
        public virtual string autorizzazioneAlSubappalto
        {
            get;
            set;
        }
    
        public virtual Nullable<int> IdCommittente
        {
            get { return _idCommittente; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idCommittente != value)
                    {
                        if (Committente != null && Committente.IdCommittente != value)
                        {
                            Committente = null;
                        }
                        _idCommittente = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _idCommittente;

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<AccessoCantieriRilevatoriCantiere> AccessoCantieriRilevatoriCantieri
        {
            get
            {
                if (_accessoCantieriRilevatoriCantieri == null)
                {
                    var newCollection = new FixupCollection<AccessoCantieriRilevatoriCantiere>();
                    newCollection.CollectionChanged += FixupAccessoCantieriRilevatoriCantieri;
                    _accessoCantieriRilevatoriCantieri = newCollection;
                }
                return _accessoCantieriRilevatoriCantieri;
            }
            set
            {
                if (!ReferenceEquals(_accessoCantieriRilevatoriCantieri, value))
                {
                    var previousValue = _accessoCantieriRilevatoriCantieri as FixupCollection<AccessoCantieriRilevatoriCantiere>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAccessoCantieriRilevatoriCantieri;
                    }
                    _accessoCantieriRilevatoriCantieri = value;
                    var newValue = value as FixupCollection<AccessoCantieriRilevatoriCantiere>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAccessoCantieriRilevatoriCantieri;
                    }
                }
            }
        }
        private ICollection<AccessoCantieriRilevatoriCantiere> _accessoCantieriRilevatoriCantieri;
    
        public virtual Committente Committente
        {
            get { return _committente; }
            set
            {
                if (!ReferenceEquals(_committente, value))
                {
                    var previousValue = _committente;
                    _committente = value;
                    FixupCommittente(previousValue);
                }
            }
        }
        private Committente _committente;

        #endregion
        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupCommittente(Committente previousValue)
        {
            if (previousValue != null && previousValue.AccessoCantieriWhiteList.Contains(this))
            {
                previousValue.AccessoCantieriWhiteList.Remove(this);
            }
    
            if (Committente != null)
            {
                if (!Committente.AccessoCantieriWhiteList.Contains(this))
                {
                    Committente.AccessoCantieriWhiteList.Add(this);
                }
                if (IdCommittente != Committente.IdCommittente)
                {
                    IdCommittente = Committente.IdCommittente;
                }
            }
            else if (!_settingFK)
            {
                IdCommittente = null;
            }
        }
    
        private void FixupAccessoCantieriRilevatoriCantieri(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (AccessoCantieriRilevatoriCantiere item in e.NewItems)
                {
                    item.AccessoCantieriWhiteList = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (AccessoCantieriRilevatoriCantiere item in e.OldItems)
                {
                    if (ReferenceEquals(item.AccessoCantieriWhiteList, this))
                    {
                        item.AccessoCantieriWhiteList = null;
                    }
                }
            }
        }

        #endregion
    }
}
