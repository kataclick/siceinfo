//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class CantieriAssegnazionePriorita
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual string Descrizione
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<CantieriAssegnazione> CantieriAssegnazioni
        {
            get
            {
                if (_cantieriAssegnazioni == null)
                {
                    var newCollection = new FixupCollection<CantieriAssegnazione>();
                    newCollection.CollectionChanged += FixupCantieriAssegnazioni;
                    _cantieriAssegnazioni = newCollection;
                }
                return _cantieriAssegnazioni;
            }
            set
            {
                if (!ReferenceEquals(_cantieriAssegnazioni, value))
                {
                    var previousValue = _cantieriAssegnazioni as FixupCollection<CantieriAssegnazione>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCantieriAssegnazioni;
                    }
                    _cantieriAssegnazioni = value;
                    var newValue = value as FixupCollection<CantieriAssegnazione>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCantieriAssegnazioni;
                    }
                }
            }
        }
        private ICollection<CantieriAssegnazione> _cantieriAssegnazioni;

        #endregion
        #region Association Fixup
    
        private void FixupCantieriAssegnazioni(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CantieriAssegnazione item in e.NewItems)
                {
                    item.CantieriAssegnazionePriorita = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CantieriAssegnazione item in e.OldItems)
                {
                    if (ReferenceEquals(item.CantieriAssegnazionePriorita, this))
                    {
                        item.CantieriAssegnazionePriorita = null;
                    }
                }
            }
        }

        #endregion
    }
}
