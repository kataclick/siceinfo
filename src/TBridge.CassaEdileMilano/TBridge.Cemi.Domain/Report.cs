//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class Report
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual string Nome
        {
            get;
            set;
        }
    
        public virtual string Path
        {
            get;
            set;
        }
    
        public virtual string Theme
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataInserimentoRecord
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<Utente> Utenti
        {
            get
            {
                if (_utenti == null)
                {
                    var newCollection = new FixupCollection<Utente>();
                    newCollection.CollectionChanged += FixupUtenti;
                    _utenti = newCollection;
                }
                return _utenti;
            }
            set
            {
                if (!ReferenceEquals(_utenti, value))
                {
                    var previousValue = _utenti as FixupCollection<Utente>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupUtenti;
                    }
                    _utenti = value;
                    var newValue = value as FixupCollection<Utente>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupUtenti;
                    }
                }
            }
        }
        private ICollection<Utente> _utenti;

        #endregion
        #region Association Fixup
    
        private void FixupUtenti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Utente item in e.NewItems)
                {
                    if (!item.Reports.Contains(this))
                    {
                        item.Reports.Add(this);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Utente item in e.OldItems)
                {
                    if (item.Reports.Contains(this))
                    {
                        item.Reports.Remove(this);
                    }
                }
            }
        }

        #endregion
    }
}
