//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Data.Objects;
using System.Data.EntityClient;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Data
{
    public partial class SICEEntities : ObjectContext
    {
        public const string ConnectionString = "name=SICEEntities";
        public const string ContainerName = "SICEEntities";
    
        #region Constructors
    
        public SICEEntities()
            : base(ConnectionString, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        public SICEEntities(string connectionString)
            : base(connectionString, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        public SICEEntities(EntityConnection connection)
            : base(connection, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        #endregion
    
        #region ObjectSet Properties
    
        public ObjectSet<CantiereSegnalazioneMotivazione> CantieriSegnalazioneMotivazioni
        {
            get { return _cantieriSegnalazioneMotivazioni  ?? (_cantieriSegnalazioneMotivazioni = CreateObjectSet<CantiereSegnalazioneMotivazione>("CantieriSegnalazioneMotivazioni")); }
        }
        private ObjectSet<CantiereSegnalazioneMotivazione> _cantieriSegnalazioneMotivazioni;
    
        public ObjectSet<CantiereSegnalazionePriorita> CantieriSegnalazionePriorita
        {
            get { return _cantieriSegnalazionePriorita  ?? (_cantieriSegnalazionePriorita = CreateObjectSet<CantiereSegnalazionePriorita>("CantieriSegnalazionePriorita")); }
        }
        private ObjectSet<CantiereSegnalazionePriorita> _cantieriSegnalazionePriorita;
    
        public ObjectSet<Impresa> Imprese
        {
            get { return _imprese  ?? (_imprese = CreateObjectSet<Impresa>("Imprese")); }
        }
        private ObjectSet<Impresa> _imprese;
    
        public ObjectSet<ImpresaRichiestaVariazioneRecapiti> ImpreseRichiesteVariazioneRecapiti
        {
            get { return _impreseRichiesteVariazioneRecapiti  ?? (_impreseRichiesteVariazioneRecapiti = CreateObjectSet<ImpresaRichiestaVariazioneRecapiti>("ImpreseRichiesteVariazioneRecapiti")); }
        }
        private ObjectSet<ImpresaRichiestaVariazioneRecapiti> _impreseRichiesteVariazioneRecapiti;
    
        public ObjectSet<Utente> Utenti
        {
            get { return _utenti  ?? (_utenti = CreateObjectSet<Utente>("Utenti")); }
        }
        private ObjectSet<Utente> _utenti;
    
        public ObjectSet<TipoVia> TipiVia
        {
            get { return _tipiVia  ?? (_tipiVia = CreateObjectSet<TipoVia>("TipiVia")); }
        }
        private ObjectSet<TipoVia> _tipiVia;
    
        public ObjectSet<CantieriGruppoIspezione> CantieriGruppiIspezione
        {
            get { return _cantieriGruppiIspezione  ?? (_cantieriGruppiIspezione = CreateObjectSet<CantieriGruppoIspezione>("CantieriGruppiIspezione")); }
        }
        private ObjectSet<CantieriGruppoIspezione> _cantieriGruppiIspezione;
    
        public ObjectSet<Ispettore> Ispettori
        {
            get { return _ispettori  ?? (_ispettori = CreateObjectSet<Ispettore>("Ispettori")); }
        }
        private ObjectSet<Ispettore> _ispettori;
    
        public ObjectSet<CantieriAssegnazioneMotivazione> CantieriAssegnazioneMotivazioni
        {
            get { return _cantieriAssegnazioneMotivazioni  ?? (_cantieriAssegnazioneMotivazioni = CreateObjectSet<CantieriAssegnazioneMotivazione>("CantieriAssegnazioneMotivazioni")); }
        }
        private ObjectSet<CantieriAssegnazioneMotivazione> _cantieriAssegnazioneMotivazioni;
    
        public ObjectSet<CantieriAssegnazionePriorita> CantieriAssegnazionePriorita
        {
            get { return _cantieriAssegnazionePriorita  ?? (_cantieriAssegnazionePriorita = CreateObjectSet<CantieriAssegnazionePriorita>("CantieriAssegnazionePriorita")); }
        }
        private ObjectSet<CantieriAssegnazionePriorita> _cantieriAssegnazionePriorita;
    
        public ObjectSet<ImpresaRecapito> ImpreseRecapiti
        {
            get { return _impreseRecapiti  ?? (_impreseRecapiti = CreateObjectSet<ImpresaRecapito>("ImpreseRecapiti")); }
        }
        private ObjectSet<ImpresaRecapito> _impreseRecapiti;
    
        public ObjectSet<TipoRecapito> TipiRecapito
        {
            get { return _tipiRecapito  ?? (_tipiRecapito = CreateObjectSet<TipoRecapito>("TipiRecapito")); }
        }
        private ObjectSet<TipoRecapito> _tipiRecapito;
    
        public ObjectSet<CantieriSegnalazione> CantieriSegnalazioni
        {
            get { return _cantieriSegnalazioni  ?? (_cantieriSegnalazioni = CreateObjectSet<CantieriSegnalazione>("CantieriSegnalazioni")); }
        }
        private ObjectSet<CantieriSegnalazione> _cantieriSegnalazioni;
    
        public ObjectSet<Cantiere> CantieriCantieri
        {
            get { return _cantieriCantieri  ?? (_cantieriCantieri = CreateObjectSet<Cantiere>("CantieriCantieri")); }
        }
        private ObjectSet<Cantiere> _cantieriCantieri;
    
        public ObjectSet<CantieriTipologiaAttivita> CantieriCalendarioAttivitaTipologieAttivita
        {
            get { return _cantieriCalendarioAttivitaTipologieAttivita  ?? (_cantieriCalendarioAttivitaTipologieAttivita = CreateObjectSet<CantieriTipologiaAttivita>("CantieriCalendarioAttivitaTipologieAttivita")); }
        }
        private ObjectSet<CantieriTipologiaAttivita> _cantieriCalendarioAttivitaTipologieAttivita;
    
        public ObjectSet<CantieriAssegnazione> CantieriAssegnazioni
        {
            get { return _cantieriAssegnazioni  ?? (_cantieriAssegnazioni = CreateObjectSet<CantieriAssegnazione>("CantieriAssegnazioni")); }
        }
        private ObjectSet<CantieriAssegnazione> _cantieriAssegnazioni;
    
        public ObjectSet<CantieriCalendarioAttivita> CantieriCalendarioAttivita
        {
            get { return _cantieriCalendarioAttivita  ?? (_cantieriCalendarioAttivita = CreateObjectSet<CantieriCalendarioAttivita>("CantieriCalendarioAttivita")); }
        }
        private ObjectSet<CantieriCalendarioAttivita> _cantieriCalendarioAttivita;
    
        public ObjectSet<CantieriCalendarioAttivitaRifiuti> CantieriCalendarioAttivitaRifiuti
        {
            get { return _cantieriCalendarioAttivitaRifiuti  ?? (_cantieriCalendarioAttivitaRifiuti = CreateObjectSet<CantieriCalendarioAttivitaRifiuti>("CantieriCalendarioAttivitaRifiuti")); }
        }
        private ObjectSet<CantieriCalendarioAttivitaRifiuti> _cantieriCalendarioAttivitaRifiuti;
    
        [Obsolete("Usare nuovo context DbFirst")]
        public ObjectSet<ImpresaRichiestaVariazioneStato> ImpreseRichiesteVariazioneStato
        {
            get { return _impreseRichiesteVariazioneStato  ?? (_impreseRichiesteVariazioneStato = CreateObjectSet<ImpresaRichiestaVariazioneStato>("ImpreseRichiesteVariazioneStato")); }
        }
        [Obsolete("Usare nuovo context DbFirst")]
        private ObjectSet<ImpresaRichiestaVariazioneStato> _impreseRichiesteVariazioneStato;
    
        public ObjectSet<TipoStatoGestionePratica> TipiStatoGestionePratica
        {
            get { return _tipiStatoGestionePratica  ?? (_tipiStatoGestionePratica = CreateObjectSet<TipoStatoGestionePratica>("TipiStatoGestionePratica")); }
        }
        private ObjectSet<TipoStatoGestionePratica> _tipiStatoGestionePratica;
    
        public ObjectSet<TipoStatoImpresa> TipiStatoImpresa
        {
            get { return _tipiStatoImpresa  ?? (_tipiStatoImpresa = CreateObjectSet<TipoStatoImpresa>("TipiStatoImpresa")); }
        }
        private ObjectSet<TipoStatoImpresa> _tipiStatoImpresa;
    
        public ObjectSet<Lavoratore> Lavoratori
        {
            get { return _lavoratori  ?? (_lavoratori = CreateObjectSet<Lavoratore>("Lavoratori")); }
        }
        private ObjectSet<Lavoratore> _lavoratori;
    
        public ObjectSet<Consulente> Consulenti
        {
            get { return _consulenti  ?? (_consulenti = CreateObjectSet<Consulente>("Consulenti")); }
        }
        private ObjectSet<Consulente> _consulenti;
    
        public ObjectSet<CantieriIspezioni> CantieriIspezioni
        {
            get { return _cantieriIspezioni  ?? (_cantieriIspezioni = CreateObjectSet<CantieriIspezioni>("CantieriIspezioni")); }
        }
        private ObjectSet<CantieriIspezioni> _cantieriIspezioni;
    
        public ObjectSet<CantieriIspezioniGruppi> CantieriIspezioniGruppi
        {
            get { return _cantieriIspezioniGruppi  ?? (_cantieriIspezioniGruppi = CreateObjectSet<CantieriIspezioniGruppi>("CantieriIspezioniGruppi")); }
        }
        private ObjectSet<CantieriIspezioniGruppi> _cantieriIspezioniGruppi;
    
        public ObjectSet<ImpersonateLog> ImpersonateLogs
        {
            get { return _impersonateLogs  ?? (_impersonateLogs = CreateObjectSet<ImpersonateLog>("ImpersonateLogs")); }
        }
        private ObjectSet<ImpersonateLog> _impersonateLogs;
    
        public ObjectSet<Report> Reports
        {
            get { return _reports  ?? (_reports = CreateObjectSet<Report>("Reports")); }
        }
        private ObjectSet<Report> _reports;
    
        public ObjectSet<DurcSollecito> DurcSolleciti
        {
            get { return _durcSolleciti  ?? (_durcSolleciti = CreateObjectSet<DurcSollecito>("DurcSolleciti")); }
        }
        private ObjectSet<DurcSollecito> _durcSolleciti;
    
        public ObjectSet<TipoAssenza> TipiAssenze
        {
            get { return _tipiAssenze  ?? (_tipiAssenze = CreateObjectSet<TipoAssenza>("TipiAssenze")); }
        }
        private ObjectSet<TipoAssenza> _tipiAssenze;
    
        public ObjectSet<TipoStatoMalattiaTelematica> TipiStatoMalattiaTelematica
        {
            get { return _tipiStatoMalattiaTelematica  ?? (_tipiStatoMalattiaTelematica = CreateObjectSet<TipoStatoMalattiaTelematica>("TipiStatoMalattiaTelematica")); }
        }
        private ObjectSet<TipoStatoMalattiaTelematica> _tipiStatoMalattiaTelematica;
    
        public ObjectSet<TipoCertificatoMedico> TipCertificatiMedici
        {
            get { return _tipCertificatiMedici  ?? (_tipCertificatiMedici = CreateObjectSet<TipoCertificatoMedico>("TipCertificatiMedici")); }
        }
        private ObjectSet<TipoCertificatoMedico> _tipCertificatiMedici;
    
        public ObjectSet<RapportoImpresaPersona> RapportiImpresaPersona
        {
            get { return _rapportiImpresaPersona  ?? (_rapportiImpresaPersona = CreateObjectSet<RapportoImpresaPersona>("RapportiImpresaPersona")); }
        }
        private ObjectSet<RapportoImpresaPersona> _rapportiImpresaPersona;
    
        public ObjectSet<TipoCategoria> TipiCategoria
        {
            get { return _tipiCategoria  ?? (_tipiCategoria = CreateObjectSet<TipoCategoria>("TipiCategoria")); }
        }
        private ObjectSet<TipoCategoria> _tipiCategoria;
    
        public ObjectSet<MalattiaTelematicaAltroDocumento> MalattiaTelematicaAltriDocumenti
        {
            get { return _malattiaTelematicaAltriDocumenti  ?? (_malattiaTelematicaAltriDocumenti = CreateObjectSet<MalattiaTelematicaAltroDocumento>("MalattiaTelematicaAltriDocumenti")); }
        }
        private ObjectSet<MalattiaTelematicaAltroDocumento> _malattiaTelematicaAltriDocumenti;
    
        public ObjectSet<MalattiaTelematicaGiornoNonIndennizzabile> MalattiaTelematicaGiorniNonIndennizzabili
        {
            get { return _malattiaTelematicaGiorniNonIndennizzabili  ?? (_malattiaTelematicaGiorniNonIndennizzabili = CreateObjectSet<MalattiaTelematicaGiornoNonIndennizzabile>("MalattiaTelematicaGiorniNonIndennizzabili")); }
        }
        private ObjectSet<MalattiaTelematicaGiornoNonIndennizzabile> _malattiaTelematicaGiorniNonIndennizzabili;
    
        public ObjectSet<CassaEdile> CasseEdili
        {
            get { return _casseEdili  ?? (_casseEdili = CreateObjectSet<CassaEdile>("CasseEdili")); }
        }
        private ObjectSet<CassaEdile> _casseEdili;
    
        public ObjectSet<MalattiaTelematicaCassaEdile> MalattiaTelematicaCasseEdili
        {
            get { return _malattiaTelematicaCasseEdili  ?? (_malattiaTelematicaCasseEdili = CreateObjectSet<MalattiaTelematicaCassaEdile>("MalattiaTelematicaCasseEdili")); }
        }
        private ObjectSet<MalattiaTelematicaCassaEdile> _malattiaTelematicaCasseEdili;
    
        public ObjectSet<CNCEACE> CNCEACE
        {
            get { return _cNCEACE  ?? (_cNCEACE = CreateObjectSet<CNCEACE>("CNCEACE")); }
        }
        private ObjectSet<CNCEACE> _cNCEACE;
    
        public ObjectSet<MalattiaTelematicaAssenza> MalattiaTelematicaAssenze
        {
            get { return _malattiaTelematicaAssenze  ?? (_malattiaTelematicaAssenze = CreateObjectSet<MalattiaTelematicaAssenza>("MalattiaTelematicaAssenze")); }
        }
        private ObjectSet<MalattiaTelematicaAssenza> _malattiaTelematicaAssenze;
    
        public ObjectSet<MalattiaTelematicaCertificatoMedico> MalattiaTelematicaCertificatiMedici
        {
            get { return _malattiaTelematicaCertificatiMedici  ?? (_malattiaTelematicaCertificatiMedici = CreateObjectSet<MalattiaTelematicaCertificatoMedico>("MalattiaTelematicaCertificatiMedici")); }
        }
        private ObjectSet<MalattiaTelematicaCertificatoMedico> _malattiaTelematicaCertificatiMedici;
    
        public ObjectSet<AccessoCantieriFornitore> AccessoCantieriFornitori
        {
            get { return _accessoCantieriFornitori  ?? (_accessoCantieriFornitori = CreateObjectSet<AccessoCantieriFornitore>("AccessoCantieriFornitori")); }
        }
        private ObjectSet<AccessoCantieriFornitore> _accessoCantieriFornitori;
    
        public ObjectSet<AccessoCantieriRilevatore> AccessoCantieriRilevatori
        {
            get { return _accessoCantieriRilevatori  ?? (_accessoCantieriRilevatori = CreateObjectSet<AccessoCantieriRilevatore>("AccessoCantieriRilevatori")); }
        }
        private ObjectSet<AccessoCantieriRilevatore> _accessoCantieriRilevatori;
    
        public ObjectSet<AccessoCantieriRilevatoriCantiere> AccessoCantieriRilevatoriCantieri
        {
            get { return _accessoCantieriRilevatoriCantieri  ?? (_accessoCantieriRilevatoriCantieri = CreateObjectSet<AccessoCantieriRilevatoriCantiere>("AccessoCantieriRilevatoriCantieri")); }
        }
        private ObjectSet<AccessoCantieriRilevatoriCantiere> _accessoCantieriRilevatoriCantieri;
    
        public ObjectSet<AccessoCantieriWhiteList> AccessoCantieriWhiteListes
        {
            get { return _accessoCantieriWhiteListes  ?? (_accessoCantieriWhiteListes = CreateObjectSet<AccessoCantieriWhiteList>("AccessoCantieriWhiteListes")); }
        }
        private ObjectSet<AccessoCantieriWhiteList> _accessoCantieriWhiteListes;
    
        public ObjectSet<SmsRecapitoUtente> SmsRecapitiUtenti
        {
            get { return _smsRecapitiUtenti  ?? (_smsRecapitiUtenti = CreateObjectSet<SmsRecapitoUtente>("SmsRecapitiUtenti")); }
        }
        private ObjectSet<SmsRecapitoUtente> _smsRecapitiUtenti;
    
        public ObjectSet<ImpresaConsulente> ImpreseConsulenti
        {
            get { return _impreseConsulenti  ?? (_impreseConsulenti = CreateObjectSet<ImpresaConsulente>("ImpreseConsulenti")); }
        }
        private ObjectSet<ImpresaConsulente> _impreseConsulenti;
    
        public ObjectSet<Denuncia> Denunce
        {
            get { return _denunce  ?? (_denunce = CreateObjectSet<Denuncia>("Denunce")); }
        }
        private ObjectSet<Denuncia> _denunce;
    
        public ObjectSet<OreDenunciateAggregateCompleteImpresa> OreDenunciateAggregateCompleteImprese
        {
            get { return _oreDenunciateAggregateCompleteImprese  ?? (_oreDenunciateAggregateCompleteImprese = CreateObjectSet<OreDenunciateAggregateCompleteImpresa>("OreDenunciateAggregateCompleteImprese")); }
        }
        private ObjectSet<OreDenunciateAggregateCompleteImpresa> _oreDenunciateAggregateCompleteImprese;
    
        public ObjectSet<OreDenunciateCompleteImpresa> OreDenunciateCompleteImprese
        {
            get { return _oreDenunciateCompleteImprese  ?? (_oreDenunciateCompleteImprese = CreateObjectSet<OreDenunciateCompleteImpresa>("OreDenunciateCompleteImprese")); }
        }
        private ObjectSet<OreDenunciateCompleteImpresa> _oreDenunciateCompleteImprese;
    
        public ObjectSet<SituazioneContabileImpresa> SituazioniContabiliImprese
        {
            get { return _situazioniContabiliImprese  ?? (_situazioniContabiliImprese = CreateObjectSet<SituazioneContabileImpresa>("SituazioniContabiliImprese")); }
        }
        private ObjectSet<SituazioneContabileImpresa> _situazioniContabiliImprese;
    
        public ObjectSet<Assenza> Assenze
        {
            get { return _assenze  ?? (_assenze = CreateObjectSet<Assenza>("Assenze")); }
        }
        private ObjectSet<Assenza> _assenze;
    
        public ObjectSet<ColoniePersonaleMansione> ColoniePersonaleMansioni
        {
            get { return _coloniePersonaleMansioni  ?? (_coloniePersonaleMansioni = CreateObjectSet<ColoniePersonaleMansione>("ColoniePersonaleMansioni")); }
        }
        private ObjectSet<ColoniePersonaleMansione> _coloniePersonaleMansioni;
    
        public ObjectSet<ColoniePersonaleTitoloStudio> ColoniePersonaleTitoliStudio
        {
            get { return _coloniePersonaleTitoliStudio  ?? (_coloniePersonaleTitoliStudio = CreateObjectSet<ColoniePersonaleTitoloStudio>("ColoniePersonaleTitoliStudio")); }
        }
        private ObjectSet<ColoniePersonaleTitoloStudio> _coloniePersonaleTitoliStudio;
    
        public ObjectSet<ColonieTipoDestinazione> ColonieTipiDestinazione
        {
            get { return _colonieTipiDestinazione  ?? (_colonieTipiDestinazione = CreateObjectSet<ColonieTipoDestinazione>("ColonieTipiDestinazione")); }
        }
        private ObjectSet<ColonieTipoDestinazione> _colonieTipiDestinazione;
    
        public ObjectSet<ColonieTipoVacanza> ColonieTipiVacanza
        {
            get { return _colonieTipiVacanza  ?? (_colonieTipiVacanza = CreateObjectSet<ColonieTipoVacanza>("ColonieTipiVacanza")); }
        }
        private ObjectSet<ColonieTipoVacanza> _colonieTipiVacanza;
    
        public ObjectSet<ColonieTurno> ColonieTurni
        {
            get { return _colonieTurni  ?? (_colonieTurni = CreateObjectSet<ColonieTurno>("ColonieTurni")); }
        }
        private ObjectSet<ColonieTurno> _colonieTurni;
    
        public ObjectSet<ColonieVacanza> ColonieVacanze
        {
            get { return _colonieVacanze  ?? (_colonieVacanze = CreateObjectSet<ColonieVacanza>("ColonieVacanze")); }
        }
        private ObjectSet<ColonieVacanza> _colonieVacanze;
    
        public ObjectSet<ColoniePersonaleRichiesta> ColoniePersonaleRichieste
        {
            get { return _coloniePersonaleRichieste  ?? (_coloniePersonaleRichieste = CreateObjectSet<ColoniePersonaleRichiesta>("ColoniePersonaleRichieste")); }
        }
        private ObjectSet<ColoniePersonaleRichiesta> _coloniePersonaleRichieste;
    
        public ObjectSet<ColoniePersonaleRichiesteEsperienza> ColoniePersonaleRichiesteEsperienze
        {
            get { return _coloniePersonaleRichiesteEsperienze  ?? (_coloniePersonaleRichiesteEsperienze = CreateObjectSet<ColoniePersonaleRichiesteEsperienza>("ColoniePersonaleRichiesteEsperienze")); }
        }
        private ObjectSet<ColoniePersonaleRichiesteEsperienza> _coloniePersonaleRichiesteEsperienze;
    
        public ObjectSet<MalattiaTelematicaMessaggio> MalattiaTelematicaMessaggi
        {
            get { return _malattiaTelematicaMessaggi  ?? (_malattiaTelematicaMessaggi = CreateObjectSet<MalattiaTelematicaMessaggio>("MalattiaTelematicaMessaggi")); }
        }
        private ObjectSet<MalattiaTelematicaMessaggio> _malattiaTelematicaMessaggi;
    
        public ObjectSet<ColoniePersonaleColloquio> ColoniePersonaleColloqui
        {
            get { return _coloniePersonaleColloqui  ?? (_coloniePersonaleColloqui = CreateObjectSet<ColoniePersonaleColloquio>("ColoniePersonaleColloqui")); }
        }
        private ObjectSet<ColoniePersonaleColloquio> _coloniePersonaleColloqui;
    
        public ObjectSet<ColoniePersonaleValutazione> ColoniePersonaleValutazioni
        {
            get { return _coloniePersonaleValutazioni  ?? (_coloniePersonaleValutazioni = CreateObjectSet<ColoniePersonaleValutazione>("ColoniePersonaleValutazioni")); }
        }
        private ObjectSet<ColoniePersonaleValutazione> _coloniePersonaleValutazioni;
    
        public ObjectSet<ComuniSiceNew> ComuniSiceNew
        {
            get { return _comuniSiceNew  ?? (_comuniSiceNew = CreateObjectSet<ComuniSiceNew>("ComuniSiceNew")); }
        }
        private ObjectSet<ComuniSiceNew> _comuniSiceNew;
    
        public ObjectSet<ColonieDestinazioni> ColonieDestinazioni
        {
            get { return _colonieDestinazioni  ?? (_colonieDestinazioni = CreateObjectSet<ColonieDestinazioni>("ColonieDestinazioni")); }
        }
        private ObjectSet<ColonieDestinazioni> _colonieDestinazioni;
    
        public ObjectSet<ColoniePersonaleMansioniRetribuzione> ColoniePersonaleMansioniRetribuzioni
        {
            get { return _coloniePersonaleMansioniRetribuzioni  ?? (_coloniePersonaleMansioniRetribuzioni = CreateObjectSet<ColoniePersonaleMansioniRetribuzione>("ColoniePersonaleMansioniRetribuzioni")); }
        }
        private ObjectSet<ColoniePersonaleMansioniRetribuzione> _coloniePersonaleMansioniRetribuzioni;
    
        public ObjectSet<ColoniePersonaleProposta> ColoniePersonaleProposte
        {
            get { return _coloniePersonaleProposte  ?? (_coloniePersonaleProposte = CreateObjectSet<ColoniePersonaleProposta>("ColoniePersonaleProposte")); }
        }
        private ObjectSet<ColoniePersonaleProposta> _coloniePersonaleProposte;
    
        public ObjectSet<ColoniePersonaleLuogoAppuntamento> ColoniePersonaleLuoghiAppuntamento
        {
            get { return _coloniePersonaleLuoghiAppuntamento  ?? (_coloniePersonaleLuoghiAppuntamento = CreateObjectSet<ColoniePersonaleLuogoAppuntamento>("ColoniePersonaleLuoghiAppuntamento")); }
        }
        private ObjectSet<ColoniePersonaleLuogoAppuntamento> _coloniePersonaleLuoghiAppuntamento;
    
        public ObjectSet<Committente> Committenti
        {
            get { return _committenti  ?? (_committenti = CreateObjectSet<Committente>("Committenti")); }
        }
        private ObjectSet<Committente> _committenti;
    
        public ObjectSet<ImpresaQuestionarioImpiegati> ImpresaQuestionarioImpiegati
        {
            get { return _impresaQuestionarioImpiegati  ?? (_impresaQuestionarioImpiegati = CreateObjectSet<ImpresaQuestionarioImpiegati>("ImpresaQuestionarioImpiegati")); }
        }
        private ObjectSet<ImpresaQuestionarioImpiegati> _impresaQuestionarioImpiegati;
    
        public ObjectSet<ImpresaQuestionarioImpiegatiDeterminatiScadenza> ImpresaQuestionarioImpiegatiDeterminatiScadenze
        {
            get { return _impresaQuestionarioImpiegatiDeterminatiScadenze  ?? (_impresaQuestionarioImpiegatiDeterminatiScadenze = CreateObjectSet<ImpresaQuestionarioImpiegatiDeterminatiScadenza>("ImpresaQuestionarioImpiegatiDeterminatiScadenze")); }
        }
        private ObjectSet<ImpresaQuestionarioImpiegatiDeterminatiScadenza> _impresaQuestionarioImpiegatiDeterminatiScadenze;
    
        public ObjectSet<MalattiaTelematicaConsulente> MalattiaTelematicaConsulenti
        {
            get { return _malattiaTelematicaConsulenti  ?? (_malattiaTelematicaConsulenti = CreateObjectSet<MalattiaTelematicaConsulente>("MalattiaTelematicaConsulenti")); }
        }
        private ObjectSet<MalattiaTelematicaConsulente> _malattiaTelematicaConsulenti;
    
        public ObjectSet<MalattiaTelematicaImpresa> MalattiaTelematicaImprese
        {
            get { return _malattiaTelematicaImprese  ?? (_malattiaTelematicaImprese = CreateObjectSet<MalattiaTelematicaImpresa>("MalattiaTelematicaImprese")); }
        }
        private ObjectSet<MalattiaTelematicaImpresa> _malattiaTelematicaImprese;
    
        public ObjectSet<CigoTelematicaDomanda> CigoTelematicaDomande
        {
            get { return _cigoTelematicaDomande  ?? (_cigoTelematicaDomande = CreateObjectSet<CigoTelematicaDomanda>("CigoTelematicaDomande")); }
        }
        private ObjectSet<CigoTelematicaDomanda> _cigoTelematicaDomande;
    
        public ObjectSet<CigoTelematicaLavoratore> CigoTelematicaLavoratori
        {
            get { return _cigoTelematicaLavoratori  ?? (_cigoTelematicaLavoratori = CreateObjectSet<CigoTelematicaLavoratore>("CigoTelematicaLavoratori")); }
        }
        private ObjectSet<CigoTelematicaLavoratore> _cigoTelematicaLavoratori;
    
        public ObjectSet<CigoTelematicaLavoratoreGiorno> CigoTelematicaLavoratoriGiorni
        {
            get { return _cigoTelematicaLavoratoriGiorni  ?? (_cigoTelematicaLavoratoriGiorni = CreateObjectSet<CigoTelematicaLavoratoreGiorno>("CigoTelematicaLavoratoriGiorni")); }
        }
        private ObjectSet<CigoTelematicaLavoratoreGiorno> _cigoTelematicaLavoratoriGiorni;
    
        public ObjectSet<CigoTelematicaStatoLavoratore> CigoTelematicaStatiLavoratore
        {
            get { return _cigoTelematicaStatiLavoratore  ?? (_cigoTelematicaStatiLavoratore = CreateObjectSet<CigoTelematicaStatoLavoratore>("CigoTelematicaStatiLavoratore")); }
        }
        private ObjectSet<CigoTelematicaStatoLavoratore> _cigoTelematicaStatiLavoratore;
    
        public ObjectSet<CigoDomanda> CigoDomandaSet
        {
            get { return _cigoDomandaSet  ?? (_cigoDomandaSet = CreateObjectSet<CigoDomanda>("CigoDomandaSet")); }
        }
        private ObjectSet<CigoDomanda> _cigoDomandaSet;
    
        public ObjectSet<CigoLavoratore> CigoLavoratoreSet
        {
            get { return _cigoLavoratoreSet  ?? (_cigoLavoratoreSet = CreateObjectSet<CigoLavoratore>("CigoLavoratoreSet")); }
        }
        private ObjectSet<CigoLavoratore> _cigoLavoratoreSet;
    
        public ObjectSet<TipoStatoPrestazione> TipiStatoPrestazione
        {
            get { return _tipiStatoPrestazione  ?? (_tipiStatoPrestazione = CreateObjectSet<TipoStatoPrestazione>("TipiStatoPrestazione")); }
        }
        private ObjectSet<TipoStatoPrestazione> _tipiStatoPrestazione;
    
        public ObjectSet<CigoTelematicaDocumentoInps> CigoTelematicaDocumentiInps
        {
            get { return _cigoTelematicaDocumentiInps  ?? (_cigoTelematicaDocumentiInps = CreateObjectSet<CigoTelematicaDocumentoInps>("CigoTelematicaDocumentiInps")); }
        }
        private ObjectSet<CigoTelematicaDocumentoInps> _cigoTelematicaDocumentiInps;
    
        public ObjectSet<CigoTelematicaBustaPaga> CigoTelematicaBustePaga
        {
            get { return _cigoTelematicaBustePaga  ?? (_cigoTelematicaBustePaga = CreateObjectSet<CigoTelematicaBustaPaga>("CigoTelematicaBustePaga")); }
        }
        private ObjectSet<CigoTelematicaBustaPaga> _cigoTelematicaBustePaga;
    
        public ObjectSet<RapportoLavoratoreImpresaCompleto> RapportiLavoratoreImpresaCompleti
        {
            get { return _rapportiLavoratoreImpresaCompleti  ?? (_rapportiLavoratoreImpresaCompleti = CreateObjectSet<RapportoLavoratoreImpresaCompleto>("RapportiLavoratoreImpresaCompleti")); }
        }
        private ObjectSet<RapportoLavoratoreImpresaCompleto> _rapportiLavoratoreImpresaCompleti;
    
        public ObjectSet<TipiOra> TipiOra
        {
            get { return _tipiOra  ?? (_tipiOra = CreateObjectSet<TipiOra>("TipiOra")); }
        }
        private ObjectSet<TipiOra> _tipiOra;
    
        public ObjectSet<CigoParametri> CigoParametri
        {
            get { return _cigoParametri  ?? (_cigoParametri = CreateObjectSet<CigoParametri>("CigoParametri")); }
        }
        private ObjectSet<CigoParametri> _cigoParametri;
    
        public ObjectSet<CigoTelematicaEstrattoContoProtocollo> CigoTelematicaEstrattoContoProtocollo
        {
            get { return _cigoTelematicaEstrattoContoProtocollo  ?? (_cigoTelematicaEstrattoContoProtocollo = CreateObjectSet<CigoTelematicaEstrattoContoProtocollo>("CigoTelematicaEstrattoContoProtocollo")); }
        }
        private ObjectSet<CigoTelematicaEstrattoContoProtocollo> _cigoTelematicaEstrattoContoProtocollo;
    
        public ObjectSet<PrestazioniComplete> PrestazioniComplete
        {
            get { return _prestazioniComplete  ?? (_prestazioniComplete = CreateObjectSet<PrestazioniComplete>("PrestazioniComplete")); }
        }
        private ObjectSet<PrestazioniComplete> _prestazioniComplete;
    
        public ObjectSet<LavoratoreDelega> LavoratoriDeleghe
        {
            get { return _lavoratoriDeleghe  ?? (_lavoratoriDeleghe = CreateObjectSet<LavoratoreDelega>("LavoratoriDeleghe")); }
        }
        private ObjectSet<LavoratoreDelega> _lavoratoriDeleghe;
    
        public ObjectSet<Sindacato> Sindacati
        {
            get { return _sindacati  ?? (_sindacati = CreateObjectSet<Sindacato>("Sindacati")); }
        }
        private ObjectSet<Sindacato> _sindacati;
    
        public ObjectSet<Prestazione> Prestazioni
        {
            get { return _prestazioni  ?? (_prestazioni = CreateObjectSet<Prestazione>("Prestazioni")); }
        }
        private ObjectSet<Prestazione> _prestazioni;
    
        public ObjectSet<TipoPrestazione> TipiPrestazione
        {
            get { return _tipiPrestazione  ?? (_tipiPrestazione = CreateObjectSet<TipoPrestazione>("TipiPrestazione")); }
        }
        private ObjectSet<TipoPrestazione> _tipiPrestazione;
    
        public ObjectSet<LavoratoreRecapito> LavoratoriRecapiti
        {
            get { return _lavoratoriRecapiti  ?? (_lavoratoriRecapiti = CreateObjectSet<LavoratoreRecapito>("LavoratoriRecapiti")); }
        }
        private ObjectSet<LavoratoreRecapito> _lavoratoriRecapiti;
    
        public ObjectSet<LavoratoreRichiestaVariazioneRecapito> LavoratoriRichiesteVariazioneRecapiti
        {
            get { return _lavoratoriRichiesteVariazioneRecapiti  ?? (_lavoratoriRichiesteVariazioneRecapiti = CreateObjectSet<LavoratoreRichiestaVariazioneRecapito>("LavoratoriRichiesteVariazioneRecapiti")); }
        }
        private ObjectSet<LavoratoreRichiestaVariazioneRecapito> _lavoratoriRichiesteVariazioneRecapiti;
    
        public ObjectSet<LavoratoreRichiestaVariazioneAnagrafica> LavoratoriRichiesteVariazioniAnagrafica
        {
            get { return _lavoratoriRichiesteVariazioniAnagrafica  ?? (_lavoratoriRichiesteVariazioniAnagrafica = CreateObjectSet<LavoratoreRichiestaVariazioneAnagrafica>("LavoratoriRichiesteVariazioniAnagrafica")); }
        }
        private ObjectSet<LavoratoreRichiestaVariazioneAnagrafica> _lavoratoriRichiesteVariazioniAnagrafica;
    
        public ObjectSet<LavoratoreVariazioneAnagrafica> LavoratoriVariazioniAnagrafica
        {
            get { return _lavoratoriVariazioniAnagrafica  ?? (_lavoratoriVariazioniAnagrafica = CreateObjectSet<LavoratoreVariazioneAnagrafica>("LavoratoriVariazioniAnagrafica")); }
        }
        private ObjectSet<LavoratoreVariazioneAnagrafica> _lavoratoriVariazioniAnagrafica;
    
        public ObjectSet<TipoCausaleRespintaVariazioneStatoImpresa> TipiCausaliRespintaVariazioneStatoImpresa
        {
            get { return _tipiCausaliRespintaVariazioneStatoImpresa  ?? (_tipiCausaliRespintaVariazioneStatoImpresa = CreateObjectSet<TipoCausaleRespintaVariazioneStatoImpresa>("TipiCausaliRespintaVariazioneStatoImpresa")); }
        }
        private ObjectSet<TipoCausaleRespintaVariazioneStatoImpresa> _tipiCausaliRespintaVariazioneStatoImpresa;

        #endregion

    }
}
