﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.CigoTelematica.Type.Filters
{
    public class EstrattoContoFilter
    {
        public Int32? IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String CodiceFiscaleImpresa { get; set; }
        public DateTime? PeriodoDa { get; set; }
        public DateTime? PeriodoA { get; set; }
    }
}
