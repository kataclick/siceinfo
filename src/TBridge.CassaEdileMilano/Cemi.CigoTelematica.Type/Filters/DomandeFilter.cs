﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.CigoTelematica.Type.Filters
{
    public class DomandeFilter
    {
        public Int32? IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String CodiceFiscaleImpresa { get; set; }
        public Int32? PeriodoAnnoDa { get; set; }
        public Int32? PeriodoAnnoA { get; set; }
        public Int32? PeriodoMeseDa { get; set; }
        public Int32? PeriodoMeseA { get; set; }
        public String ComuneCantiere { get; set; }
        public String ProvinciaCantiere { get; set; }
        public String IndirizzoCantiere { get; set; }
        public Int32? IdLavoratore { get; set; }
        public String CognomeLavoratore { get; set; }
        public String NomeLavoratore { get; set; }
        public String CodiceFiscaleLavoratore { get; set; }
        public String StatoDomanda { get; set; }

    }
}
