﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.CigoTelematica.Type.Filters;
using TBridge.Cemi.Type.Domain;

namespace Cemi.CigoTelematica.Type.Delegates
{

        
    //documenti Inps
    public delegate void DocumentoInpsSelectedEventHandler();
        
    public delegate void DocumentoInpsReturnedEventHandler(Boolean carica);

    //documenti busta paga
    public delegate void DocumentoBustaPagaSelectedEventHandler();

    public delegate void DocumentoBustaPagaReturnedEventHandler(Boolean carica);
}
