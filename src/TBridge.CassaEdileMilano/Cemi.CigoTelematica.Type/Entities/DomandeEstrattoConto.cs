﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Domain;

namespace Cemi.CigoTelematica.Type.Entities
{
    public class DomandeEstrattoConto
    {
        public Int32 IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }

        public int Anno { get; set; }

        public int Mese { get; set; }

        public String IndirizzoCantiere { get; set; }
        public String ComuneCantiere { get; set; }
        public String ProvinciaCantiere { get; set; }

        public Decimal Importo { get; set; }

        public int AnnoProtocolloDomanda { get; set; }

        public int NumeroProtocolloDomanda { get; set; }

        public String TipoProtocolloDomanda { get; set; }
        //public List<LavoratoriEstrattoConto> Lavoratori { get; set; }

    }
}
