﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.CigoTelematica.Type.Entities
{
    public class ControlliLavoratore
    {

        public bool PagaOrariaValida { get; set; }

        public bool LavoratoreInDenuncia { get; set; }

        public bool LavoratoreApprendista { get; set; }

        public bool OreCigoValide { get; set; }

        public bool MonteOreMensileValido { get; set; }

        public bool MonteOreAnnuoValido { get; set; }

        //public bool ImpresaRegolare { get; set; }

        public bool RapportoDiLavoroValido { get; set; }

        public int TotaleOreMese { get; set; }

        public int TotaleOreAnnoRiconosciute { get; set; }
    }
}
