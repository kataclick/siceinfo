﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.CigoTelematica.Type.Entities
{
    public class GiornoCigo
    {
        public int Giorno { get; set; }
        public Boolean Selected { get; set; }
    }
}
