﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Domain;

namespace Cemi.CigoTelematica.Type.Entities
{
    public class LavoratoreDettagli
    {
        //public AssenzaDettagli(Assenza ass, DateTime? dataAss, DateTime? dataInizioValiditaRapporto, DateTime dataFineValiditaRapporto, string descrizioneCategoria, int? partTimePercentuale)
        //{
        //    //throw new NotImplementedException();
        //    IdLavoratore = ass.IdLavoratore;
        //    IdImpresa = ass.IdImpresa;
            


        //    dataAssunzione = dataAss;
        //    dataInizio = dataInizioValiditaRapporto;
        //    dataFine = dataFineValiditaRapporto;
        //    categoria = descrizioneCategoria;
        //    percentualePT = partTimePercentuale;

        //}

        public Int32 IdLavoratore { get; set; }
        public Int32? IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String EmailImpresa { get; set; }
        public Int32 IdDomanda { get; set; }
        public String IdStatoDomanda { get; set; }
        public String Cognome { get; set; }
        public String Nome { get; set; }
        public String CodiceFiscale { get; set; }
        public DateTime DataNascita { get; set; }
        public String Stato { get; set; }
        public String IdStato { get; set; }
        public DateTime? DataDomanda { get; set; }
        public Int32 Anno { get; set; }
        public Int32 Mese { get; set; }
        
        public DateTime? DataAssunzione { get; set; }
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public String Categoria { get; set; }
        public Int32? PercentualePT { get; set; }
        public String Note { get; set; }

        public Decimal? PagaOrariaDenuncia { get; set; }
        public Decimal? PagaOrariaDichiarata { get; set; }
        public Int32? OreRichieste { get; set; }

        public String IdTipoOra { get; set; }
        public String DescrizioneTipoOra { get; set; }
      
        public String NomeCompleto
        {
            get { return String.Format("{0} {1}", this.Cognome, this.Nome); }
        }

        public String RapportoLavoro
        {
            get
            {
                String ret = this.Categoria;

                if (DataInizio.HasValue)
                    ret += String.Format(" dal {0}", this.DataInizio.Value.ToShortDateString());

                if (DataFine.HasValue)
                    if (DataFine.Value != new DateTime(2079, 6, 6))
                        ret += String.Format(" al {0}", this.DataFine.Value.ToShortDateString());

                return ret;
            }
        }

        public String PartTime
        {
            get
            {
                String ret = "";

                if (PercentualePT.HasValue)
                    if (PercentualePT != 0) 
                        ret += String.Format("Part-time al {0} %", this.PercentualePT.ToString());


                return ret;
            }
        }

        public ControlliLavoratore ControlliValidita { get; set; }

    }
}
