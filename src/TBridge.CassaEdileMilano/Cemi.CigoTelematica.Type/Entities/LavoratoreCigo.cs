﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Domain;

namespace Cemi.CigoTelematica.Type.Entities
{
    public class LavoratoreCigo
    {
        public Lavoratore Lavoratore { set; get; }
        public int OreCigo { get; set; }
        public bool Aggiunto { get; set; }
        public string TipoOre { get; set; }
        public string TipoOreDescrizione { get; set; }

    }
}
