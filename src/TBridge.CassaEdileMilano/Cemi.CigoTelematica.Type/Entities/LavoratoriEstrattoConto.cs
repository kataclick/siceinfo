﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Domain;

namespace Cemi.CigoTelematica.Type.Entities
{
    public class LavoratoriEstrattoConto
    {
        public Int32? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public decimal Importo { get; set; }

    }
}
