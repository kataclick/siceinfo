﻿using System.ServiceProcess;

namespace TBridge.Cemi.AccessoCantieri.Service
{
    internal static class Program
    {
        /// <summary>
        ///   The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            ServiceBase[] servicesToRun = new ServiceBase[]
                                              {
                                                  new AccessoCantieriService()
                                              };
            ServiceBase.Run(servicesToRun);
        }
    }
}