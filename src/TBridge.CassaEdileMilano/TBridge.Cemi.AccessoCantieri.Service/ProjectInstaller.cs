﻿using System.ComponentModel;
using System.Configuration.Install;

namespace TBridge.Cemi.AccessoCantieri.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}