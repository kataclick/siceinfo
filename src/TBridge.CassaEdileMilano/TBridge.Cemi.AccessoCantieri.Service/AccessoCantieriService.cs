﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

namespace TBridge.Cemi.AccessoCantieri.Service
{
    internal partial class AccessoCantieriService : ServiceBase
    {
        private readonly Timer timerAccessoCantieri;

        public AccessoCantieriService()
        {
            InitializeComponent();

            timerAccessoCantieri = new Timer();
            timerAccessoCantieri.Elapsed += timerAccessoCantieri_Elapsed;
        }

        protected override void OnStart(string[] args)
        {
            KillExistingProcesses();

            try
            {
                StartExecutable();

                timerAccessoCantieri.Interval = Int32.Parse(ConfigurationManager.AppSettings["timer"]);
                timerAccessoCantieri.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected override void OnStop()
        {
            KillExistingProcesses();
        }

        private void timerAccessoCantieri_Elapsed(object sender, ElapsedEventArgs e)
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            if (processes.Length == 0)
            {
                StartExecutable();
            }
        }

        private static void StartExecutable()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.WorkingDirectory = ConfigurationManager.AppSettings["working_directory"];
            startInfo.FileName = ConfigurationManager.AppSettings["executable_name"];
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            Process.Start(startInfo);
        }

        private void KillExistingProcesses()
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }
    }
}