using System;
using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Deleghe.Type.Enums;
using TBridge.Cemi.Type;

namespace TBridge.Cemi.Deleghe.Business.Office
{
    public class WordManager
    {
        private readonly string _pathGenerazioneDocumenti = ConfigurationManager.AppSettings["PathGenerazioneDocumenti"];
        private readonly string _pathNonIscritti = ConfigurationManager.AppSettings["PathLetteraDelegheNonIscritti"];
        private readonly string _pathScadute = ConfigurationManager.AppSettings["PathLetteraDelegheScadute"];

        public string CreateWord(TipologiaLettera tipoLettera, List<WordField> ds)
        {
            string ret;

            switch (tipoLettera)
            {
                case TipologiaLettera.Scadute:
                    ret = Cemi.Business.Office.WordManager.CreateWord(_pathScadute, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.NonIscritti:
                    ret = Cemi.Business.Office.WordManager.CreateWord(_pathNonIscritti, ds, _pathGenerazioneDocumenti);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("tipoLettera");
            }

            return ret;
        }
    }
}