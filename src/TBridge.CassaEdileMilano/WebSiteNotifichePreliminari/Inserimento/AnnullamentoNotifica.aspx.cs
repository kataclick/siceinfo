using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Business;
using System.Web.Security;
using TBridge.Cemi.Presenter;
using System.Text;

public partial class Inserimento_AnnullamentoNotifica : System.Web.UI.Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelpETitolo(String help, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(help);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelpETitolo(
            "In questa pagina � possibile annullare le notifiche preliminari (e gli aggiornamenti) inseriti.",
            "Annullamento notifica");
    }
    #endregion

    protected void ButtonAnnulla_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            Int32 idNotifica = Int32.Parse(TextBoxProtocollo.Text);

            Presenter.SvuotaCampo(LabelErrore);
            CaricaNotifica(idNotifica);
        }
    }

    private void CaricaNotifica(Int32 idNotifica)
    {
        try
        {
            NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica);

            if (notifica.IdUtenteTelematiche == (Guid)Membership.GetUser().ProviderUserKey)
            {
                if (!notifica.Annullata)
                {
                    List<int> notificheCorrelate = biz.GetIdNotificheCorrelate(notifica.IdNotifica.Value);

                    if (notificheCorrelate.Count == 0)
                    {
                        CaricaDatiNotifica(notifica);
                    }
                    else
                    {
                        StringBuilder mesErrore = new StringBuilder();
                        mesErrore.Append(
                            "La notifica non pu� essere eliminata. Esistono degli aggiornamenti della notifica che vanno precedentemente eliminati. I protocolli sono:");
                        foreach (int prot in notificheCorrelate)
                        {
                            mesErrore.Append(String.Format("  {0}", prot));
                        }

                        LabelErrore.Text = mesErrore.ToString();
                    }
                }
                else
                {
                    LabelErrore.Text = "Notifica gi� annullata";
                }
            }
            else
            {
                LabelErrore.Text = "Annullamento notifica non consentito";
            }
        }
        catch
        {
            LabelErrore.Text = "Annullamento notifica non consentito";
        }
    }

    private void CaricaDatiNotifica(NotificaTelematica notifica)
    {
        PanelDatiNotifica.Visible = true;

        LabelProtocollo.Text = notifica.IdNotifica.ToString();
        LabelNaturaOpera.Text = notifica.NaturaOpera;
        LabelDataComunicazione.Text = notifica.Data.ToShortDateString();
        LabelCommittente.Text = notifica.Committente.ToString();
        Presenter.CaricaElementiInBulletedList(
            BulletedListIndirizzi,
            notifica.Indirizzi,
            "IndirizzoCompleto",
            "");
    }

    protected void ButtonConfermaAnnulla_Click(object sender, EventArgs e)
    {
        Int32 idNotifica = Int32.Parse(LabelProtocollo.Text);

        if (biz.AnnullaNotifica(idNotifica, "TELEMATICHE"))
        {
            LabelErrore.Text = "Annullamento effettuato correttamente";
            PanelDatiNotifica.Visible = false;
            ResetCampi();
        }
        else
        {
            LabelErrore.Text = "Errore durante l'inserimento";
        }
    }

    private void ResetCampi()
    {
        Presenter.SvuotaCampo(LabelProtocollo);

        Presenter.SvuotaCampo(LabelProtocollo);
        Presenter.SvuotaCampo(LabelNaturaOpera);
        Presenter.SvuotaCampo(LabelDataComunicazione);
        Presenter.SvuotaCampo(LabelCommittente);
        BulletedListIndirizzi.Items.Clear();
    }
}
