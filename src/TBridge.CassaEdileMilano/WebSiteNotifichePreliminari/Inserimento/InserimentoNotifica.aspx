<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InserimentoNotifica.aspx.cs" Inherits="InserimentoNotifica" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="~/WebControls/DatiGenerali.ascx" tagname="DatiGenerali" tagprefix="uc1" %>
<%@ Register src="~/WebControls/Ricerca/Committente.ascx" tagname="Committente" tagprefix="uc2" %>
<%@ Register src="~/WebControls/DateNumeri.ascx" tagname="DateNumeri" tagprefix="uc3" %>
<%@ Register src="~/WebControls/Ricerca/Impresa.ascx" tagname="Impresa" tagprefix="uc4" %>
<%@ Register src="~/WebControls/ResponsabileDeiLavori.ascx" tagname="ResponsabileDeiLavori" tagprefix="uc6" %>
<%@ Register src="~/WebControls/CoordinatoreSicurezzaProgettazione.ascx" tagname="CoordinatoreSicurezzaProgettazione" tagprefix="uc7" %>
<%@ Register src="~/WebControls/CoordinatoreSicurezzaEsecuzione.ascx" tagname="CoordinatoreSicurezzaEsecuzione" tagprefix="uc8" %>

<%@ Register src="~/WebControls/ImpreseAffidatarie.ascx" tagname="ImpreseAffidatarie" tagprefix="uc5" %>

<%@ Register src="~/WebControls/ImpreseEsecutrici.ascx" tagname="ImpreseEsecutrici" tagprefix="uc9" %>

<%@ Register src="~/WebControls/Riassunto.ascx" tagname="Riassunto" tagprefix="uc10" %>

<%@ Register src="~/WebControls/Visualizzazione/NotificaInAggiornamento.ascx" tagname="NotificaInAggiornamento" tagprefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    <asp:Panel 
        ID="PanelAggiornamento"
        runat="server"
        Visible="false">
        
        <uc11:NotificaInAggiornamento ID="NotificaInAggiornamento1" runat="server" />
        
        <br />
    </asp:Panel>
    I campi contrassegnati da <b>*</b> sono obbligatori
    <br />
    <br />
    <center class="passi">
        <table class="tabellaConBordo">
            <tr>
                <td>
                    Passo 
                    <b>
                        <asp:Label
                            ID="LabelPasso"
                            runat="server">
                        </asp:Label>
                    </b>
                    di
                    <b>
                        <asp:Label
                            ID="LabelTotalePassi"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
        </table>
    </center>
    <br />
    <asp:Wizard ID="WizardInserimentoNotifica" runat="server" Width="100%" 
        ActiveStepIndex="0" CssClass="wizard"
        onactivestepchanged="WizardInserimentoNotifica_ActiveStepChanged" 
        onfinishbuttonclick="WizardInserimentoNotifica_FinishButtonClick" 
        onnextbuttonclick="WizardInserimentoNotifica_NextButtonClick" 
        onsidebarbuttonclick="WizardInserimentoNotifica_SideBarButtonClick" >
        <SideBarTemplate>
            <asp:DataList ID="SideBarList" runat="server">
                <ItemTemplate>
                    <asp:LinkButton ID="SideBarButton" runat="server"></asp:LinkButton>
                </ItemTemplate>
                <SelectedItemStyle Font-Bold="True" />
            </asp:DataList>
        </SideBarTemplate>
        <StepStyle VerticalAlign="Top" />
        <StartNavigationTemplate>
            <asp:Button ID="btnSuccessivoStart" runat="server" Text="Avanti" CommandName="MoveNext"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StartNavigationTemplate>
        <StepNavigationTemplate>
            <asp:Button ID="btnPrecedente" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivo" runat="server" CommandName="MoveNext" Text="Avanti"
                Width="100px" TabIndex="1" ValidationGroup="stop" />
        </StepNavigationTemplate>
        <SideBarStyle CssClass="wizardSideBar" />
        <FinishNavigationTemplate>
            <asp:Button ID="btnPrecedenteFinish" runat="server" Text="Indietro" CommandName="MovePrevious"
                Width="100px" TabIndex="2" />
            <asp:Button ID="btnSuccessivoFinish" runat="server" CommandName="MoveComplete" Text="Conferma"
                Width="150px" TabIndex="1" />
        </FinishNavigationTemplate>
        <WizardSteps>
            <asp:WizardStep ID="datiGenerali" runat="server" StepType="Start" 
                Title="- Dati Generali">
                <table>
                    <tr>
                        <td>
                            <b>
                                Dati generali
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:DatiGenerali ID="DatiGenerali1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryDatiGenerali"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="committente" runat="server" StepType="Step" 
                Title="- Committente">
                <table>
                    <tr>
                        <td>
                            <b>
                                Committente
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <uc2:Committente ID="Committente1" runat="server" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryCommittente"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="responsabileLavori" runat="server" StepType="Step" 
                Title="- Responsabile dei Lavori">
                <table>
                    <tr>
                        <td>
                            <b>
                                Responsabile dei Lavori
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc6:ResponsabileDeiLavori ID="ResponsabileDeiLavori1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryResponsabileDeiLavori"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep runat="server" 
                Title="- Coord. Sicurezza durante la Progettazione" StepType="Step" 
                ID="coordinatoreSicurezzaProgettazione">
                <table>
                    <tr>
                        <td>
                            <b>
                                Coordinatore della Sicurezza durante la Progettazione
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc7:CoordinatoreSicurezzaProgettazione ID="CoordinatoreSicurezzaProgettazione1" 
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryCoordinatoreSicurezzaProgettazione"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep runat="server" Title="- Coord. Sicurezza durante l'Esecuzione" 
                StepType="Step" ID="coordinatoreSicurezzaEsecuzione">
                <table>
                    <tr>
                        <td>
                            <b>
                                Coordinatore della Sicurezza durante l&#39;Esecuzione
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc8:CoordinatoreSicurezzaEsecuzione ID="CoordinatoreSicurezzaEsecuzione1" 
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryCoordinatoreSicurezzaEsecuzione"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="dateNumeri" runat="server" StepType="Step" 
                Title="- Date e Numeri">
                <table>
                    <tr>
                        <td>
                            <b>
                                Date e numeri
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc3:DateNumeri ID="DateNumeri1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary
                                ID="ValidationSummaryDateNumeri"
                                runat="server"
                                ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="impreseAffidatarie" runat="server" StepType="Step" 
                Title="- Imprese Affidatarie">
                <table>
                    <tr>
                        <td>
                            <b>
                                Imprese Affidatarie
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc5:ImpreseAffidatarie ID="ImpreseAffidatarie1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="impreseEsecutrici" runat="server" 
                Title="- Imprese Esecutrici">
                <table>
                    <tr>
                        <td>
                            <b>
                                Imprese Esecutrici
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc9:ImpreseEsecutrici ID="ImpreseEsecutrici1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
            <asp:WizardStep ID="conferma" runat="server" StepType="Finish" 
                Title="- Conferma">
                <table>
                    <tr>
                        <td>
                            <b>
                                Conferma
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                            <uc10:Riassunto ID="Riassunto1" runat="server" />
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label
                                ID="LabelMessaggio"
                                runat="server"
                                Text="La notifica � gi� stata comunicata. Per comunicare una nuova notifica scegliere la voce Notifica Preliminare dal menu."
                                Visible="false"
                                CssClass="messaggiErrore">
                            </asp:Label>
                            <asp:Label
                                ID="LabelErrore"
                                runat="server"
                                Text="Si � verificato un errore. Riprovare pi� tardi."
                                Visible="false"
                                CssClass="messaggiErrore">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:WizardStep>
        </WizardSteps>
    </asp:Wizard>
    <asp:Button
        ID="ButtonSalvaTemporaneo"
        runat="server"
        Text="Salva temporaneamente" onclick="ButtonSalvaTemporaneo_Click" />
</asp:Content>

