using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Filters;

public partial class LocalizzaIndirizzo : Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonIndietro.Attributes.Add("onclick", "javascript:history.back(); return false;");

        if (!Page.IsPostBack)
        {
            CaricaTitolo();

            Int32? idIndirizzo = Context.Items["IdIndirizzo"] as Int32?;
            if (idIndirizzo.HasValue)
            {
                CaricaIndirizzo(idIndirizzo.Value);
            }
        }
    }

    private void CaricaIndirizzo(Int32 idIndirizzo)
    {
        CantiereFilter filtro = new CantiereFilter();
        filtro.IdIndirizzo = idIndirizzo;
        filtro.IdArea = 1;

        CantiereNotificaCollection cantieri = biz.RicercaCantieriPerCommittente(filtro);
        if (cantieri.Count == 1)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("LoadMap();");

            stringBuilder.Append(Localizzazione1.CaricaCantiere(cantieri[0], true));

            RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto

    private void CaricaStringaTitolo(String titolo)
    {
        MasterPage master = (MasterPage) Master;
        master.CaricaTitolo(titolo);
    }

    private void CaricaTitolo()
    {
        CaricaStringaTitolo("Localizzazione indirizzo");
    }

    #endregion
}