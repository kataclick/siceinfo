using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Business;
using System.Web.Security;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Exceptions;

public partial class InserimentoNotifica : System.Web.UI.Page
{
    private CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Click multipli

        // Per prevenire click multipli
        Button bConferma = (Button)WizardInserimentoNotifica.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
        // Per prevenire click multipli
        StringBuilder sb = new StringBuilder();
        sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
        sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
        sb.Append("this.value = 'Attendere...';");
        sb.Append("this.disabled = true;");
        sb.Append(Page.GetPostBackEventReference(bConferma));
        sb.Append(";");
        bConferma.Attributes.Add("onclick", sb.ToString());

        #endregion

        #region Registrazione eventi
        ResponsabileDeiLavori1.OnResponsabileDeiLavori += new TBridge.Cemi.Cpt.Type.Delegates.ResponsabileDeiLavoriEventHandler(ResponsabileDeiLavori1_OnResponsabileDeiLavori);
        CoordinatoreSicurezzaProgettazione1.OnCoordinatoreSicurezzaProgettazioneNonNominato += new TBridge.Cemi.Cpt.Type.Delegates.CoordinatoreSicurezzaProgettazioneEventHandler(CoordinatoreSicurezzaProgettazione1_OnCoordinatoreSicurezzaProgettazioneNonNominato);
        CoordinatoreSicurezzaProgettazione1.OnStessiDatiResponsabileLavori += new TBridge.Cemi.Cpt.Type.Delegates.CoordinatoreSicurezzaProgettazioneComeResponsabileLavoriEventHandler(CoordinatoreSicurezzaProgettazione1_OnStessiDatiResponsabileLavori);
        CoordinatoreSicurezzaEsecuzione1.OnCoordinatoreSicurezzaEsecuzioneNonNominato += new TBridge.Cemi.Cpt.Type.Delegates.CoordinatoreSicurezzaEsecuzioneEventHandler(CoordinatoreSicurezzaEsecuzione1_OnCoordinatoreSicurezzaEsecuzioneNonNominato);
        CoordinatoreSicurezzaEsecuzione1.OnStessiDatiResponsabileLavori += new TBridge.Cemi.Cpt.Type.Delegates.CoordinatoreSicurezzaEsecuzioneComeResponsabileLavoriEventHandler(CoordinatoreSicurezzaEsecuzione1_OnStessiDatiResponsabileLavori);
        CoordinatoreSicurezzaEsecuzione1.OnStessiDatiCoordinatoreProgettazione += new TBridge.Cemi.Cpt.Type.Delegates.CoordinatoreSicurezzaEsecuzioneComeCoordinatoreSicurezzaProgettazioneEventHandler(CoordinatoreSicurezzaEsecuzione1_OnStessiDatiCoordinatoreProgettazione);
        ImpreseEsecutrici1.OnSubappaltoCreatoOCancellato += new TBridge.Cemi.Cpt.Type.Delegates.SubappaltoCreatedOrDeletedEventHandler(ImpreseEsecutrici1_OnSubappaltoCreato);
        #endregion

        if (!Page.IsPostBack)
        {
            CaricaHelpDatiGenerali();
            CaricaTitolo("Inserimento Notifica");
            ViewState["Guid"] = Guid.NewGuid();

            Int32? idNotificaInAggiornamento = Context.Items["IdNotifica"] as Int32?;
            if (idNotificaInAggiornamento.HasValue)
            {
                // Sono in aggiornamento di notifica
                ModalitaAggiornamento(idNotificaInAggiornamento.Value);
            }

            Int32? idNotificaTemporanea = Context.Items["IdNotificaTemporanea"] as Int32?;
            if (idNotificaTemporanea.HasValue)
            {
                // Sono in modifica di una notifica temporanea
                ModalitaDaTemporanea(idNotificaTemporanea.Value);
            }

            VerificaSeUtenteCommittente();

            LabelTotalePassi.Text = WizardInserimentoNotifica.WizardSteps.Count.ToString();
        }
    }

    private void VerificaSeUtenteCommittente()
    {
        UtenteNotificheTelematiche 
            utente = biz.GetUtenteTelematiche((Guid)Membership.GetUser().ProviderUserKey);

        if (utente != null && utente.CommittenteTelematiche != null)
        {
            Committente1.CaricaCommittente(utente.CommittenteTelematiche);
            Committente1.DisabilitaRicerca();
        }
    }

    private void ModalitaAggiornamento(Int32 idNotificaInAggiornamento)
    {
        ButtonSalvaTemporaneo.Visible = false;
        NotificaTelematica notifica = biz.GetNotificaTelematicaUltimaVersione(idNotificaInAggiornamento);

        CaricaTitolo("Aggiornamento Notifica");
        PanelAggiornamento.Visible = true;
        NotificaInAggiornamento1.CaricaInformazioniNotifica(notifica);
        CaricaNotifica(notifica);
    }

    private void ModalitaDaTemporanea(Int32 idNotificaTemporanea)
    {
        NotificaTelematica notifica = biz.GetNotificaTemporanea(idNotificaTemporanea);
        CaricaNotifica(notifica);
    }

    private void CaricaNotifica(NotificaTelematica notifica)
    {
        if (notifica.IdNotificaRiferimento != 0)
        {
            ViewState["IdNotificaRiferimento"] = notifica.IdNotificaRiferimento;
        }
        ViewState["IdNotificaTemporanea"] = notifica.IdNotificaTemporanea;
        
        DatiGenerali1.CaricaNotifica(notifica);
        Committente1.CaricaCommittente(notifica.Committente);
        ResponsabileDeiLavori1.CaricaResponsabileDeiLavori(notifica);
        CoordinatoreSicurezzaProgettazione1.CaricaCoordinatoreProgettazione(notifica);
        CoordinatoreSicurezzaEsecuzione1.CaricaCoordinatoreEsecuzione(notifica);
        DateNumeri1.CaricaDati(notifica);
        ImpreseAffidatarie1.CaricaImpreseAffidatarie(notifica);
        ImpreseEsecutrici1.CaricaImpreseEsecutrici(notifica);
    }

    void CoordinatoreSicurezzaEsecuzione1_OnCoordinatoreSicurezzaEsecuzioneNonNominato(bool nonNominato)
    {   
    }

    void CoordinatoreSicurezzaProgettazione1_OnCoordinatoreSicurezzaProgettazioneNonNominato(bool nonNominato)
    {
        CoordinatoreSicurezzaEsecuzione1.GestisciStessiDatiCoordinatoreSicurezzaProgettazione(nonNominato);
    }

    void CoordinatoreSicurezzaEsecuzione1_OnStessiDatiCoordinatoreProgettazione(bool stato)
    {
        if (stato)
        {
            CoordinatoreSicurezzaEsecuzione1.CaricaPersona(CoordinatoreSicurezzaProgettazione1.GetCoordinatoreProgettazione());
        }
        else
        {
            CoordinatoreSicurezzaEsecuzione1.ResetTuttiCampi();
        }
    }

    void CoordinatoreSicurezzaEsecuzione1_OnStessiDatiResponsabileLavori(bool stato)
    {
        if (stato)
        {
            CoordinatoreSicurezzaEsecuzione1.CaricaPersona(ResponsabileDeiLavori1.GetResponsabileLavori());
        }
        else
        {
            CoordinatoreSicurezzaEsecuzione1.ResetCampi();
        }
    }

    void CoordinatoreSicurezzaProgettazione1_OnStessiDatiResponsabileLavori(Boolean stato)
    {
        if (stato)
        {
            CoordinatoreSicurezzaProgettazione1.CaricaPersona(ResponsabileDeiLavori1.GetResponsabileLavori());
        }
        else
        {
            CoordinatoreSicurezzaProgettazione1.ResetCampi();
        }
    }

    void ResponsabileDeiLavori1_OnResponsabileDeiLavori(Boolean nonNominato)
    {
        CoordinatoreSicurezzaProgettazione1.GestisciStessiDatiResponsabileLavori(nonNominato);
        CoordinatoreSicurezzaEsecuzione1.GestisciStessiDatiResponsabileLavori(nonNominato);
    }

    void ImpreseEsecutrici1_OnSubappaltoCreato(SubappaltoNotificheTelematicheCollection subappalti)
    {
        ImpreseAffidatarie1.ForzaCaricamento(subappalti);
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaTitolo(String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaTitolo(titolo);
    }

    private void CaricaStringaHelpETitolo(String messaggio)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
    }

    private void CaricaAiuto()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        if (WizardInserimentoNotifica.ActiveStep.Equals(datiGenerali))
        {
            CaricaHelpDatiGenerali();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(committente))
        {
            CaricaHelpCommittenti();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(responsabileLavori))
        {
            CaricaHelpResponsabileLavori();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(coordinatoreSicurezzaProgettazione))
        {
            CaricaHelpCoordinatoreProgettazione();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(coordinatoreSicurezzaEsecuzione))
        {
            CaricaHelpCoordinatoreRealizzazione();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(dateNumeri))
        {
            CaricaHelpDateNumeri();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(impreseAffidatarie))
        {
            CaricaHelpImpreseAffidatarie();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(impreseEsecutrici))
        {
            CaricaHelpImpreseEsecutrici();
        }

        if (WizardInserimentoNotifica.ActiveStep.Equals(conferma))
        {
            CaricaHelpConferma();
        }
    }

    // Le stringhe di aiuto possono essere memorizzate nel DB o in un file di configurazione
    private void CaricaHelpConferma()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Conferma</b> vengono visualizzati tutti i dati inseriti.<br /><br /><hr /><br /> Per confermare la notifica cliccare sul tasto \"Conferma\"");
    }

    private void CaricaHelpImpreseEsecutrici()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Imprese Affidatarie</b> vanno ricercate le imprese nell'anagrafica che operano nel cantiere e, se non trovate, inserito come nuovo.<br /><br /><hr /><br /> Una volta selezionate tutte le imprese o inseriti i dati cliccare sul tasto \"Avanti\"");
    }

    private void CaricaHelpImpreseAffidatarie()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Imprese Affidatarie</b> vanno ricercate le imprese nell'anagrafica che operano nel cantiere e, se non trovate, inserito come nuovo.<br /><br />Possono essere eliminate solo le imprese non considerate nelle Imprese Esecutrici.<br /><br /><hr /><br /> Una volta selezionate tutte le imprese o inseriti i dati cliccare sul tasto \"Avanti\".");
    }

    private void CaricaHelpDateNumeri()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Date e Numeri</b> vanno inseriti l'ammontare complessivo dei lavori, la data presunta di inizio lavori, la durata presunta dei lavori, la data presunta di fine lavori, il numero massimo presunto di lavoratori nel cantiere, il numero previsto di imprese nel cantiere, il numero previsto di lavoratori autonomi nel cantiere e il numero totale di uomini / giorno.<br /><br /><hr /><br /> Una volta inseriti i dati cliccare sul tasto \"Avanti\"");
    }

    private void CaricaHelpCoordinatoreRealizzazione()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Coord. Sicurezza durante l'Esecuzione</b> vanno inseriti tutti i dati relativi. Se i dati sono gli stessi del Responsabile dei Lavori mettere una spunta su \"stessi dati del Responsabile dei Lavori\"; se sono gli stessi del Coordinatore della Sicurezza durante la Progettazione mettere una spunta su \"stessi dati del Coordinatore per la Progettazione\".<br /><br /><hr /><br /> Una volta inseriti i dati cliccare sul tasto \"Avanti\"");
    }

    private void CaricaHelpCoordinatoreProgettazione()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Coord. Sicurezza durante la Progettazione</b> vanno inseriti tutti i dati relativi. Se i dati sono gli stessi del Responsabile dei Lavori mettere una spunta su \"stessi dati del Responsabile dei Lavori\".<br /><br /><hr /><br /> Una volta inseriti i dati cliccare sul tasto \"Avanti\"");
    }

    private void CaricaHelpResponsabileLavori()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Responsabile dei Lavori</b> vanno inseriti tutti i dati relativi. Se non � stato nominato mettere una spunta su \"Non nominato\".<br /><br /><hr /><br /> Una volta inseriti i dati cliccare sul tasto \"Avanti\"");
    }

    private void CaricaHelpCommittenti()
    {
        CaricaStringaHelpETitolo(
            "Nella sezione <b>Committente</b> va ricercato un committente nell'anagrafica e, se non trovato, inserito come nuovo.<br /><br /><hr /><br /> Una volta selezionato un committente o inseriti i dati cliccare sul tasto \"Avanti\"");
    }

    private void CaricaHelpDatiGenerali()
    {
        CaricaStringaHelpETitolo(
            "In questa pagina � possibile inserire una nuova notifica preliminare, nella sezione <b>Dati generali</b> vanno inseriti la natura dell'opera, il numero dell'appalto (se pubblico) e gli indirizzi dei cantieri coinvolti.<br /><br />Per aggiungere un indirizzo alla lista cliccare sul pulsante <b>Aggiungi indirizzo</b><br /><br /><hr /><br /> Una volta inseriti i dati cliccare sul tasto \"Avanti\"");
    }
    #endregion

    #region Eventi del wizard
    protected void WizardInserimentoNotifica_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        // Conferma della notifica
        NotificaTelematica notifica = CreaNotifica();

        try
        {
            if (biz.InserisciNotifica(notifica))
            {
                LabelErrore.Visible = false;
                LabelMessaggio.Visible = false;
                Context.Items["IdNotifica"] = notifica.IdNotifica;
                Server.Transfer("~/Inserimento/RicevutaNotifica.aspx");
            }
            else
            {
                LabelErrore.Visible = true;
            }
        }
        catch (NotificaGiaInseritaException exc)
        {
            LabelMessaggio.Visible = true;
        }
    }

    protected void WizardInserimentoNotifica_ActiveStepChanged(object sender, EventArgs e)
    {
        // Carico il messaggio di aiuto
        CaricaAiuto();

        LabelPasso.Text = (WizardInserimentoNotifica.ActiveStepIndex + 1).ToString();

        // Sono nel coordinatore alla progettazione verifico se devo caricare i dati del direttore lavori
        if (WizardInserimentoNotifica.ActiveStep.Equals(coordinatoreSicurezzaProgettazione))
        {
            if (CoordinatoreSicurezzaProgettazione1.StessiDatiResponsabileLavori)
            {
                CoordinatoreSicurezzaProgettazione1.CaricaPersona(ResponsabileDeiLavori1.GetResponsabileLavori());
            }
        }

        // Sono nel coordinatore all'esecuzione verifico se devo caricare i dati del direttore lavori
        if (WizardInserimentoNotifica.ActiveStep.Equals(coordinatoreSicurezzaEsecuzione))
        {
            if (CoordinatoreSicurezzaEsecuzione1.StessiDatiResponsabileLavori)
            {
                CoordinatoreSicurezzaProgettazione1.CaricaPersona(ResponsabileDeiLavori1.GetResponsabileLavori());
            }
            else
            {
                if (CoordinatoreSicurezzaEsecuzione1.StessiDatiCoordinatoreProgettazione)
                {
                    CoordinatoreSicurezzaEsecuzione1.CaricaPersona(CoordinatoreSicurezzaProgettazione1.GetCoordinatoreProgettazione());
                }
            }
        }
        
        // Sono in conferma devo riempire il riassunto
        if (WizardInserimentoNotifica.ActiveStep.Equals(conferma))
        {
            RiempiPaginaConferma();
        }

        // Sono nelle imprese esecutrici, devo riempire la combo delle affidatarie
        if (WizardInserimentoNotifica.ActiveStep.Equals(impreseEsecutrici))
        {
            ImpreseEsecutrici1.CaricaImpreseAffidatarie(ImpreseAffidatarie1.GetImpreseAffidatarie());
        }
    }

    protected void WizardInserimentoNotifica_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        // Se non passa i validator non faccio proseguire
        Page.Validate("stop");

        if (!Page.IsValid)
        {
            e.Cancel = true;
        }
    }

    protected void WizardInserimentoNotifica_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (e.NextStepIndex > e.CurrentStepIndex)
        {
            e.Cancel = true;
        }
    }
    #endregion

    private void RiempiPaginaConferma()
    {
        NotificaTelematica notifica = CreaNotifica();

        // Riempire la pagina di riassunto dei dati
        Riassunto1.CaricaNotifica(notifica);
    }

    private NotificaTelematica CreaNotifica()
    {
        NotificaTelematica notifica = new NotificaTelematica();

        // Recuperare i dati della notifica
        if (ViewState["IdNotificaRiferimento"] != null)
        {
            notifica.IdNotificaPadre = (Int32)ViewState["IdNotificaRiferimento"];
        }

        if (ViewState["IdNotificaTemporanea"] != null)
        {
            notifica.IdNotificaTemporanea = (Int32)ViewState["IdNotificaTemporanea"];
        }

        notifica.Committente = Committente1.CreaCommittente();
        DatiGenerali1.IntegraNotificaConDatiGenerali(notifica);
        ResponsabileDeiLavori1.IntegraNotificaConResponsabileLavori(notifica);
        CoordinatoreSicurezzaProgettazione1.IntegraNotificaConCoordinatoreProgettazione(notifica);
        CoordinatoreSicurezzaEsecuzione1.IntegraNotificaConCoordinatoreEsecuzione(notifica);
        DateNumeri1.IntegraNotificaConDateENumeri(notifica);
        notifica.ImpreseAffidatarie = ImpreseAffidatarie1.GetImpreseAffidatarie();
        notifica.ImpreseEsecutrici = ImpreseEsecutrici1.GetImpreseEsecutrici();

        // Imposto l'area di Milano
        notifica.Area = new Area();
        notifica.Area.IdArea = 1;

        // Imposto la data della comunicazione
        notifica.Data = DateTime.Now;

        MembershipUser utente = Membership.GetUser();
        
        notifica.Utente = "Telematica";

        // Utente telematico
        notifica.IdUtenteTelematiche = (Guid)utente.ProviderUserKey;

        // Guid per evitare inserimenti doppi
        notifica.Guid = (Guid)ViewState["Guid"];

        return notifica;
    }

    protected void ButtonSalvaTemporaneo_Click(object sender, EventArgs e)
    {
        MembershipUser utente = Membership.GetUser();
        NotificaTelematica notifica = CreaNotifica();
        
        if (!notifica.IdNotificaTemporanea.HasValue)
        {
            if (biz.InsertNotificaTemporanea((Guid)utente.ProviderUserKey, notifica))
            {
                Response.Redirect("~/Inserimento/RicercaNotificheTemporanee.aspx");
            }
        }
        else
        {
            if (biz.UpdateNotificaTemporanea(notifica))
            {
                Response.Redirect("~/Inserimento/RicercaNotificheTemporanee.aspx");
            }
        }
    }
}
