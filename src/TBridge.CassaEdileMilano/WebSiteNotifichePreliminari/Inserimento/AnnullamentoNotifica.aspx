﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AnnullamentoNotifica.aspx.cs" Inherits="Inserimento_AnnullamentoNotifica" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <asp:Panel
        ID="PanelProtocollo"
        runat="server"
        DefaultButton="ButtonAnnulla"
        Width="100%">
        <table>
            <tr>
                <td style="width: 200px">
                    Protocollo Notifica:
                </td>
                <td style="width: 155px">
                    <asp:TextBox 
                        ID="TextBoxProtocollo"
                        runat="server"
                        Width="150px"
                        MaxLength="8">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidatorProtocollo"
                        runat="server"
                        ControlToValidate="TextBoxProtocollo"
                        ErrorMessage="Digitare un numero di protocollo"
                        ValidationGroup="annullamentoNotifica">
                        *
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator
                        ID="CompareValidatorProtocollo"
                        runat="server"
                        ControlToValidate="TextBoxProtocollo"
                        Type="Integer"
                        Operator="DataTypeCheck"
                        ErrorMessage="Protocollo errato"
                        ValidationGroup="annullamentoNotifica">
                        *
                    </asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button 
                        ID="ButtonAnnulla"
                        runat="server"
                        Text="Annulla la Notifica"
                        Width="150px"
                        ValidationGroup="annullamentoNotifica" onclick="ButtonAnnulla_Click" />
                </td>
                <td colspan="2">
                    <asp:ValidationSummary
                        ID="ValidationSummaryErroriAnnullamento"
                        runat="server"
                        ValidationGroup="annullamentoNotifica"
                        CssClass="messaggiErrore" />
                    <asp:Label
                        ID="LabelErrore"
                        runat="server"
                        CssClass="messaggiErrore">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel
        ID="PanelDatiNotifica"
        runat="server"
        Visible="false"
        DefaultButton="ButtonConfermaAnnulla"
        Width="100%">
        <hr />
        <table>
            <tr>
                <td style="width: 166px">
                    Protocollo:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelProtocollo"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 166px">
                    Natura dell'opera:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelNaturaOpera"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 166px">
                    Data comunicazione:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelDataComunicazione"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 166px">
                    Committente:
                </td>
                <td>
                    <b>
                        <asp:Label
                            ID="LabelCommittente"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td style="width: 166px">
                    Indirizzi:
                </td>
                <td>
                    <b>
                        <asp:BulletedList
                            ID="BulletedListIndirizzi"
                            runat="server">
                        </asp:BulletedList>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button
                        ID="ButtonConfermaAnnulla"
                        runat="server"
                        Text="Conferma annullamento"
                        Width="150px" onclick="ButtonConfermaAnnulla_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

