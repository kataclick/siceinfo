using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AggiornamentoNotifica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Notifica1.ModalitaAggiornamento();
        Notifica1.OnNotificaSelected += new TBridge.Cemi.Cpt.Type.Delegates.NotificaSelectedEventHandler(Notifica1_OnNotificaSelected);

        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    void Notifica1_OnNotificaSelected(Int32 idNotifica, Int32 idNotificaRiferimento)
    {
        Context.Items["IdNotifica"] = idNotificaRiferimento;
        Server.Transfer("~/Inserimento/InserimentoNotifica.aspx");
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            String.Format("In questa pagina � possibile cercare tra le notifiche preliminari precedentemente inserite per effettuarne un aggiornamento.<br /><br />Impostando i filtri di ricerca e premendo il taso \"<b>Ricerca</b>\" vengono mostrate tutte le notifiche (o aggiornamenti) trovati.<br /><br />L'aggiornamento di una notifica pu� essere effettuato cliccando sulla notifica preliminare o su uno qualsiasi dei suoi precedenti aggiornamenti<br /><br /><hr /><b>Legenda</b><br /><br /><img src=\"{0}\" /> Visualizza la mappa<br /><img src=\"{1}\" /> Aggiorna la notifica<br /><img src=\"{2}\" /> Ricevuta",
                ResolveUrl("~/Images/localizzazione24.png"),
                ResolveUrl("~/Images/aggiornaNotifica24.png"),
                ResolveUrl("~/Images/report24.png")),
            "Aggiornamento notifica");
    }
    #endregion
}
