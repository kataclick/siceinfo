﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LocalizzaIndirizzo.aspx.cs" Inherits="LocalizzaIndirizzo" %>
<%@ Register src="~/WebControls/Visualizzazione/Localizzazione.ascx" tagname="Localizzazione" tagprefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <br />
    <asp:Button
        ID="ButtonIndietro"
        runat="server"
        Text="Torna indietro"
        Width="150px" />
    <br />
    <br />
    <uc1:Localizzazione ID="Localizzazione1" runat="server" />
    </telerik:RadAjaxPanel>
</asp:Content>

