﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RicevutaNotifica.aspx.cs" Inherits="RicevutaNotifica" EnableSessionState="True" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@ Register src="~/WebControls/Visualizzazione/Ricevuta.ascx" tagname="Ricevuta" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    Siete pregati di <b>STAMPARE</b> e <b>CONSERVARE</b> il presente documento a titolo di
    ricevuta.
    <br />
    <br />
    <uc1:Ricevuta ID="Ricevuta1" runat="server" />
</asp:Content>

