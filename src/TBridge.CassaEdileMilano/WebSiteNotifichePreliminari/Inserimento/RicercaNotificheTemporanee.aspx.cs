using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inserimento_RicercaNotificheTemporanee : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
                String.Format("In questa pagina � possibile recuperare le notifiche precedentemente inserite ma non ancora confermate.<br /><br /><hr /><b>Legenda</b><br /><br /><img width=\"24px\" heigth=\"24px\" src=\"{0}\" /> Completa la notifica<br /><img width=\"20px\" heigth=\"20px\" src=\"{1}\" /> Elimina la notifica",
                ResolveUrl("~/Images/modifica.png"),
                ResolveUrl("~/Images/pallinoElimina.png")),
                "Notifiche non confermate");
    }
    #endregion
}
