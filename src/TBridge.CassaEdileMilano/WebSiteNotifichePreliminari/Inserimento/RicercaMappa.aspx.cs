using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RicercaMappa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaAiutoETitolo();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaHelp(String messaggio, String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaStringaHelp(messaggio);
        master.CaricaTitolo(titolo);
    }

    private void CaricaAiutoETitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaHelp(
            "In questa pagina � possibile cercare tra le notifiche preliminari precedentemente inserite e aggiornamenti.<br /><br />Impostando i filtri di ricerca e premendo il taso \"<b>Ricerca</b>\" vengono mostrate sulla <b>mappa</b> tutte le notifiche trovate.<br /><br />Muovendosi con il mouse sopra all'icona di un cantiere compariranno alcune informazioni relative alla notifica corrispondente.",
            "Ricerca su Mappa");
    }
    #endregion
}
