using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Business;

public partial class DettagliNotifica : System.Web.UI.Page
{
    private CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonIndietro.Attributes.Add("onclick", "javascript:history.back(); return false;");

        if (!Page.IsPostBack)
        {
            CaricaTitolo();

            if (Context.Items["IdNotifica"] != null)
            {
                Int32 idNotifica = (Int32)Context.Items["IdNotifica"];

                NotificaTelematica notifica = biz.GetNotificaTelematica(idNotifica);
                Riassunto1.CaricaNotifica(notifica);
            }
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaTitolo(String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaTitolo(titolo);
    }

    private void CaricaTitolo()
    {
        CaricaStringaTitolo("Dettagli della notifica");
    }
    #endregion
}
