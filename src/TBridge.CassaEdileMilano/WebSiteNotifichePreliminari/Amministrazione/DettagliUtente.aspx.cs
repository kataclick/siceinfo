using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Business;

public partial class Amministrazione_DettagliUtente : System.Web.UI.Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTitolo();

            if (Context.Items["userID"] != null)
            {
                Guid userID = (Guid)Context.Items["userID"];

                UtenteNotificheTelematiche utente = biz.GetUtenteTelematiche(userID);
                CaricaUtente(utente);
            }
        }
    }

    private void CaricaUtente(UtenteNotificheTelematiche utente)
    {
        Utente1.CaricaUtente(utente);
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaTitolo(String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaTitolo(titolo);
    }

    private void CaricaTitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaTitolo(
            "Dettagli utente");
    }
    #endregion
}
