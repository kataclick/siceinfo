﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Registrazione.aspx.cs" Inherits="Registrazione" %>

<%@ Register src="WebControls/Visualizzazione/Utente.ascx" tagname="Utente" tagprefix="uc1" %>

<%@ Register src="WebControls/Visualizzazione/Committente.ascx" tagname="Committente" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <br />
    I campi contrassegnati da <b>*</b> sono obbligatori
    <br />
    <br />
    <center class="passi">
        <table class="tabellaConBordo">
            <tr>
                <td>
                    Passo 
                    <b>
                        <asp:Label
                            ID="LabelPasso"
                            runat="server">1</asp:Label>
                    </b>
                    di
                    <b>
                        <asp:Label
                            ID="LabelTotalePassi"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
        </table>
    </center>
    <br />
    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" Width="100%" 
        AnswerLabelText="" CancelButtonText="Annulla" 
        CompleteSuccessText="Il tuo utente è stato creato correttamente. A seguito dell'approvazione dell'amministratore del portale potrai accedere." 
        ConfirmPasswordCompareErrorMessage="La Password e la Password confermata devono essere uguali." 
        ConfirmPasswordLabelText="Conferma Password:" 
        ConfirmPasswordRequiredErrorMessage="Password di conferma mancante." 
        CreateUserButtonText="Registrati" DisableCreatedUser="True" 
        DuplicateEmailErrorMessage="L'indirizzo e-mail fornito è già utilizzato da un altro utente. Immettere un altro indirizzo e-mail." 
        DuplicateUserNameErrorMessage="Il Nome Utente è già utilizzato. Immettere un altro  Nome Utente" 
        EmailRegularExpressionErrorMessage="E-mail non valida." 
        EmailRequiredErrorMessage="E-mail mancante." 
        InvalidEmailErrorMessage="E-mail non valida." 
        InvalidPasswordErrorMessage="Lunghezza minima della Password: {0}. Caratteri alfanumerici richiesti: {1}." 
        LoginCreatedUser="False" 
        PasswordRegularExpressionErrorMessage="Immettere una Password diversa." 
        PasswordRequiredErrorMessage="Password mancante." StartNextButtonText="Avanti" 
        StepNextButtonText="Avanti" StepPreviousButtonText="Indietro" 
        UnknownErrorMessage="Il tuo account non è stato creato. Riprova più tardi." 
        UserNameLabelText="Nome Utente:" 
        UserNameRequiredErrorMessage="Nome Utente mancante." DisplaySideBar="True"
        CssClass="wizard" 
        onactivestepchanged="CreateUserWizard1_ActiveStepChanged" 
    oncreateduser="CreateUserWizard1_CreatedUser" 
    oncreateusererror="CreateUserWizard1_CreateUserError" 
        ContinueButtonText="Torna alla Home Page" 
        ContinueDestinationPageUrl="~/Default.aspx">
        <SideBarStyle CssClass="wizardNoLink" />
        <StepNextButtonStyle Width="150px" />
        <ContinueButtonStyle Width="150px" />
        <TextBoxStyle Width="300px" />
        <CreateUserButtonStyle Width="150px" />
        <TitleTextStyle CssClass="titoloCreazioneMenu" Font-Bold="True" />
        <StartNextButtonStyle Width="150px" />
        <StepPreviousButtonStyle Width="150px" />
    <WizardSteps>
        <asp:WizardStep runat="server" Title="Dati">
            <uc1:Utente ID="Utente1" runat="server" />
        </asp:WizardStep>
        <asp:WizardStep runat="server" Title="Committente">
            <table>
                <tr>
                    <td colspan="2">
                        <b>
                            Committente
                        </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        Utente Committente:
                    </td>
                    <td>
                        <asp:CheckBox
                            ID="CheckBoxUtenteCommittente"
                            runat="server" AutoPostBack="True" 
                            oncheckedchanged="CheckBoxUtenteCommittente_CheckedChanged" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Panel
                            ID="PanelCommittente"
                            runat="server"
                            Enabled="false">
                            <uc1:Committente ID="Committente1" runat="server" />
                            <asp:ValidationSummary
                                ID="ValidationSummaryCommittente"
                                runat="server"
                                CssClass="messaggiErrore" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:CreateUserWizardStep runat="server" Title="Creazione Account" />
        <asp:CompleteWizardStep ID="stepComplete" runat="server" Title="Registrazione effettuata">
            <ContentTemplate>
                La <b>registrazione</b> è stata completata con <b>successo</b>.
                <br />
                <br />
                L'account non sarà utilizzabile fino all'approvazione dell'Amministratore del portale Web.
                <br />
                <br />
                I dati scelti per l'accesso sono:
                <br />
                <br />
                Username: <b><asp:Label ID="LabelLogin" runat="server"></asp:Label></b>
                <asp:Panel
                    ID="PanelPassword"
                    runat="server"
                    Visible="false">
                <br />
                Password: <b><asp:Label ID="LabelPassword" runat="server"></asp:Label></b>
                </asp:Panel>
            </ContentTemplate>
        </asp:CompleteWizardStep>
    </WizardSteps>
</asp:CreateUserWizard>
</asp:Content>

