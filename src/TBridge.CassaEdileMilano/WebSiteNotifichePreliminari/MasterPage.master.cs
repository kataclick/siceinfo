using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using System.Web.UI.HtmlControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                UtenteNotificheTelematiche utente = biz.GetUtenteTelematiche((Guid)user.ProviderUserKey);

                if (utente != null && utente.CommittenteTelematiche != null)
                {
                    HtmlAnchor link = (HtmlAnchor)LoginViewLogin.FindControl("LinkModificaDati");
                    link.Visible = true;
                }
            }
        }
    }

    public void CaricaStringaHelp(string messaggio)
    {
        Label labelAiuto = this.LoginViewAiuto.FindControl("LabelAiuto") as Label;

        if (labelAiuto != null)
        {
            labelAiuto.Text = messaggio;
        }
    }

    public void CaricaTitolo(String titolo)
    {
        Label labelTitolo = this.FindControl("LabelTitolo") as Label;

        if (labelTitolo != null)
        {
            labelTitolo.Text = titolo;
        }
    }
}
