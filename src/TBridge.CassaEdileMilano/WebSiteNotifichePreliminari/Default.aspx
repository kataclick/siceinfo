﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMenuSinistro" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContenuto" Runat="Server">
    <p>
        Benvenuti sul sito per la comunicazione delle <b>Notifiche preliminari</b>.
    </p>
    <p>
        Da queste pagine si possono comunicare in via telematica le notifiche preliminari e i relativi aggiornamenti. 
        L'accesso è consentito agli utenti autorizzati.
        Per ottenere l'accesso al portale occorre seguire la procedura di <b>Registrazione</b> cliccando sull'apposita voce nel menu in alto.
    </p>
    <p>
        La Registrazione non garantisce l'accesso diretto al portale Web che deve essere autorizzato dall'Amministratore del sistema.
    </p>
</asp:Content>

