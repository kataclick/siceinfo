﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateNumeri.ascx.cs" Inherits="WebControls_DateNumeri" %>
<style type="text/css">
    .style1
    {
        width: 200px;
    }
</style>
<table width="100%">
    <tr>
        <td class="style1">
            Ammontare complessivo presunto dei lavori<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxAmmontareComplessivo" 
                runat="server" 
                Width="300px" 
                MaxLength="14">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorAmmontare"
                runat="server"
                ControlToValidate="TextBoxAmmontareComplessivo"
                ValidationGroup="stop"
                ErrorMessage="Ammontare complessivo presunto dei lavori mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                ID="CompareValidatorAmmontare" 
                runat="server" 
                ControlToValidate="TextBoxAmmontareComplessivo"
                Type="Currency" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato ammontare complessivo errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
            <asp:RangeValidator 
                ID="RangeValidatorAmmontare" 
                runat="server" 
                ControlToValidate="TextBoxAmmontareComplessivo"
                Type="Currency" 
                MinimumValue="0,01"
                MaximumValue="9999999999999"
                ErrorMessage="L'ammontare complessivo deve essere maggiore di zero" 
                ValidationGroup="stop">
                *
            </asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Data presunta <b>inizio</b> lavori (gg/mm/aaaa)<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxDataInizioLavori" 
                runat="server" 
                Width="300px" 
                MaxLength="10">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorDataInizioLavori"
                runat="server"
                ControlToValidate="TextBoxDataInizioLavori"
                ValidationGroup="stop"
                ErrorMessage="Data presunta inizio lavori mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                ID="CompareValidatorDataInizioLavori" 
                runat="server" 
                ControlToValidate="TextBoxDataInizioLavori"
                Type="Date" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato data inizio lavori errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            <b>Durata</b> presunta dei lavori (in giorni solari)<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxDurataLavori" 
                runat="server" 
                Width="300px"
                MaxLength="5">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorDurataLavori"
                runat="server"
                ControlToValidate="TextBoxDurataLavori"
                ValidationGroup="stop"
                ErrorMessage="Durata presunta dei lavori mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                ID="CompareValidatorDurataLavori" 
                runat="server" 
                ControlToValidate="TextBoxDurataLavori"
                Type="Integer" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato durata presunta dei lavori errato"
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
            <asp:RangeValidator 
                ID="RangeValidatorDurataLavori" 
                runat="server" 
                ControlToValidate="TextBoxDurataLavori"
                Type="Integer" 
                MinimumValue="1"
                MaximumValue="99999"
                ErrorMessage="La durata presunta dei lavori deve essere maggiore di zero" 
                ValidationGroup="stop">
                *
            </asp:RangeValidator>
        </td>
    </tr>
    <tr runat="server" visible="false">
        <td class="style1">
            Data presunta <b>fine</b> lavori (gg/mm/aaaa):
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxDataFineLavori" 
                runat="server" 
                Width="300px" 
                MaxLength="10" Enabled="False"></asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator 
                ID="CompareValidatorDataFineLavori" 
                runat="server" 
                ControlToValidate="TextBoxDataFineLavori"
                Type="Date" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato data presunta fine lavori errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            N°massimo presunto di lavoratori sul cantiere<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroLavoratori" 
                runat="server" 
                Width="300px" 
                MaxLength="4">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNumeroLavoratori"
                runat="server"
                ControlToValidate="TextBoxNumeroLavoratori"
                ValidationGroup="stop"
                ErrorMessage="Numero massimo presunto lavoratori sul cantiere mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                ID="CompareValidatorNumeroLavoratori" 
                runat="server" 
                ControlToValidate="TextBoxNumeroLavoratori"
                Type="Integer" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato numero massimo presunto lavoratori sul cantiere errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
            <asp:RangeValidator 
                ID="RangeValidatorNumeroLavoratori" 
                runat="server" 
                ControlToValidate="TextBoxNumeroLavoratori"
                Type="Integer" 
                MinimumValue="1"
                MaximumValue="9999"
                ErrorMessage="Il numero massimo presunto lavoratori sul cantiere deve essere maggiore di zero" 
                ValidationGroup="stop">
                *
            </asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            N°previsto di imprese sul cantiere<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroImprese" 
                runat="server" 
                Width="300px" 
                MaxLength="3">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNumeroImprese"
                runat="server"
                ControlToValidate="TextBoxNumeroImprese"
                ValidationGroup="stop"
                ErrorMessage="Numero previsto di imprese sul cantiere mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                ID="CompareValidatorNumeroImprese" 
                runat="server" 
                ControlToValidate="TextBoxNumeroImprese"
                Type="Integer" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato numero previsto imprese errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
            <asp:RangeValidator 
                ID="RangeValidatorNumeroImprese" 
                runat="server" 
                ControlToValidate="TextBoxNumeroImprese"
                Type="Integer" 
                MinimumValue="0"
                MaximumValue="999"
                ErrorMessage="Il numero previsto imprese deve essere maggiore di zero" 
                ValidationGroup="stop">
                *
            </asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            N°previsto di lavoratori autonomi sul cantiere<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroLavoratoriAutonomi" 
                runat="server" 
                Width="300px" 
                MaxLength="4">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNumeroLavoratoriAutonomi"
                runat="server"
                ControlToValidate="TextBoxNumeroLavoratoriAutonomi"
                ValidationGroup="stop"
                ErrorMessage="Numero previsto di lavoratori autonomi sul cantiere mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CompareValidator 
                ID="CompareValidatorNumeroLavoratoriAutonomi" 
                runat="server" 
                ControlToValidate="TextBoxNumeroLavoratoriAutonomi"
                Type="Integer" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato numero lavoratori autonomi errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
            <asp:RangeValidator 
                ID="RangeValidatorNumeroLavoratoriAutonomi" 
                runat="server" 
                ControlToValidate="TextBoxNumeroLavoratoriAutonomi"
                Type="Integer" 
                MinimumValue="0"
                MaximumValue="999"
                ErrorMessage="Il numero lavoratori autonomi deve essere maggiore di zero" 
                ValidationGroup="stop">
                *
            </asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            N°totale uomini / giorno:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroUominiGiorno" 
                runat="server" 
                Width="300px" 
                MaxLength="9">
            </asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator 
                ID="CompareValidatorNumeroUominiGiorno" 
                runat="server" 
                ControlToValidate="TextBoxNumeroUominiGiorno"
                Type="Integer" 
                Operator="DataTypeCheck" 
                ErrorMessage="Formato numero uomini giorno errato" 
                ValidationGroup="stop">
                *
            </asp:CompareValidator>
            <asp:RangeValidator 
                ID="RangeValidatorNumeroUominiGiorno" 
                runat="server" 
                ControlToValidate="TextBoxNumeroUominiGiorno"
                Type="Integer" 
                MinimumValue="0"
                MaximumValue="999999999"
                ErrorMessage="Il numero uomini giorno deve essere maggiore di zero" 
                ValidationGroup="stop">
                *
            </asp:RangeValidator>
        </td>
    </tr>
</table>
