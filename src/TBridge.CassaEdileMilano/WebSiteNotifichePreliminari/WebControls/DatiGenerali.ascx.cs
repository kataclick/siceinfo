using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Type.Collections;

public partial class WebControls_DatiGenerali : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void IntegraNotificaConDatiGenerali(Notifica notifica)
    {
        notifica.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
        notifica.NumeroAppalto = Presenter.NormalizzaCampoTesto(TextBoxNumeroAppalto.Text);

        notifica.Indirizzi = ListaIndirizzi1.GetIndirizzi();
    }

    public void CaricaNotifica(Notifica notifica)
    {
        TextBoxNaturaOpera.Text = notifica.NaturaOpera;
        TextBoxNumeroAppalto.Text = notifica.NumeroAppalto;
        ListaIndirizzi1.CaricaIndirizzi(notifica.Indirizzi);
    }

    #region Metodi per i Validator
    protected void CustomValidatorListaIndirizzi_ServerValidate(object source, ServerValidateEventArgs args)
    {
        IndirizzoCollection indirizzi = ListaIndirizzi1.GetIndirizzi();

        if (indirizzi.Count > 0)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
    #endregion
}
