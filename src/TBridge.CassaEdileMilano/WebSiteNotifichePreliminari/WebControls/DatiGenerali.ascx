﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatiGenerali.ascx.cs" Inherits="WebControls_DatiGenerali" %>
<%@ Register src="ListaIndirizzi.ascx" tagname="ListaIndirizzi" tagprefix="uc1" %>
<table>
    <tr>
        <td>
            Natura dell'opera (descrizione dettagliata delle attività<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNaturaOpera" 
                runat="server" 
                Width="400px" 
                Height="70px" 
                TextMode="MultiLine" 
                MaxLength="500">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidatorTextBoxNaturaOpera"
                runat="server" 
                ControlToValidate="TextBoxNaturaOpera"
                ErrorMessage="Natura dell'opera mancante" 
                ValidationGroup="stop">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            N° appalto (solo per Enti Pubblici):
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroAppalto" 
                runat="server" 
                Width="400px" 
                MaxLength="50">
            </asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo del cantiere<b>*</b>:
        </td>
        <td>
            <uc1:ListaIndirizzi 
                ID="ListaIndirizzi1" 
                runat="server" />
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorListaIndirizzi"
                runat="server"
                ErrorMessage="Non è stato inserito nessun indirizzo"
                ValidationGroup="stop" 
                onservervalidate="CustomValidatorListaIndirizzi_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>