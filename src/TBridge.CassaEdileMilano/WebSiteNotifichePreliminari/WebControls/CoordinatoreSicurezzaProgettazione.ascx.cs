using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Delegates;

public partial class WebControls_CoordinatoreSicurezzaProgettazione : System.Web.UI.UserControl
{
    public event CoordinatoreSicurezzaProgettazioneEventHandler OnCoordinatoreSicurezzaProgettazioneNonNominato;
    public event CoordinatoreSicurezzaProgettazioneComeResponsabileLavoriEventHandler OnStessiDatiResponsabileLavori;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public Boolean StessiDatiResponsabileLavori
    {
        get { return this.CheckBoxProgettazioneStessiResponsabile.Checked; }
    }

    protected void CheckBoxProgettazioneStessiResponsabile_CheckedChanged(object sender, EventArgs e)
    {
        ResetCampi();

        if (CheckBoxProgettazioneStessiResponsabile.Checked)
        {
            CheckBoxNonNominato.Checked = false;
            PanelCoordinatoreSicurezzaProgettazione.Enabled = false;

            if (OnCoordinatoreSicurezzaProgettazioneNonNominato != null)
            {
                OnCoordinatoreSicurezzaProgettazioneNonNominato(CheckBoxNonNominato.Checked);
            }
        }
        else
        {
            PanelCoordinatoreSicurezzaProgettazione.Enabled = true;
        }

        if (OnStessiDatiResponsabileLavori != null)
        {
            OnStessiDatiResponsabileLavori(CheckBoxProgettazioneStessiResponsabile.Checked);
        }
    }

    public void ResetCampi()
    {
        //CheckBoxProgettazioneStessiResponsabile.Checked = false;

        PersonaCoordinatoreSicurezzaProgettazione.ResetCampi();
    }

    public void ResetTuttiCampi()
    {
        CheckBoxProgettazioneStessiResponsabile.Checked = false;

        PersonaCoordinatoreSicurezzaProgettazione.ResetCampi();
    }

    public void CaricaPersona(PersonaNotificheTelematiche persona)
    {
        PersonaCoordinatoreSicurezzaProgettazione.CaricaPersona(persona);
    }

    public PersonaNotificheTelematiche GetCoordinatoreProgettazione()
    {
        return PersonaCoordinatoreSicurezzaProgettazione.CreaPersona();
    }

    public void IntegraNotificaConCoordinatoreProgettazione(NotificaTelematica notifica)
    {
        if (CheckBoxNonNominato.Checked)
        {
            notifica.CoordinatoreProgettazioneNonNominato = true;
        }
        else
        {
            notifica.CoordinatoreProgettazioneNonNominato = false;
            
            if (CheckBoxProgettazioneStessiResponsabile.Checked)
            {
                notifica.CoordinatoreSicurezzaProgettazione = notifica.DirettoreLavori;
            }
            else
            {
                notifica.CoordinatoreSicurezzaProgettazione = PersonaCoordinatoreSicurezzaProgettazione.CreaPersona();
            }
        }
    }

    public void GestisciStessiDatiResponsabileLavori(Boolean nonNominato)
    {
        CheckBoxProgettazioneStessiResponsabile.Enabled = !nonNominato;

        if (nonNominato
            && CheckBoxProgettazioneStessiResponsabile.Checked)
        {
            CheckBoxProgettazioneStessiResponsabile.Checked = false;
            PanelCoordinatoreSicurezzaProgettazione.Enabled = true;
        }
    }

    public void CaricaCoordinatoreProgettazione(NotificaTelematica notifica)
    {
        if (!notifica.CoordinatoreProgettazioneNonNominato)
        {
            PersonaCoordinatoreSicurezzaProgettazione.CaricaPersona(notifica.CoordinatoreSicurezzaProgettazione);

            if (notifica.CoordinatoreSicurezzaProgettazione != null
                && notifica.DirettoreLavori != null
                && notifica.CoordinatoreSicurezzaProgettazione.IdPersona == notifica.DirettoreLavori.IdPersona)
            {
                CheckBoxProgettazioneStessiResponsabile.Checked = true;
                PanelCoordinatoreSicurezzaProgettazione.Enabled = false;
            }
        }
        else
        {
            CheckBoxNonNominato.Checked = true;
            PanelCoordinatoreSicurezzaProgettazione.Enabled = false;

            PersonaCoordinatoreSicurezzaProgettazione.GestisciValidator(!CheckBoxNonNominato.Checked);
        }
        
    }

    protected void CheckBoxNonNominato_CheckedChanged(object sender, EventArgs e)
    {
        ResetTuttiCampi();
        
        // Se non � nominato devo diabilitare il controllo e i validator
        PanelCoordinatoreSicurezzaProgettazione.Enabled = !CheckBoxNonNominato.Checked;
        PersonaCoordinatoreSicurezzaProgettazione.GestisciValidator(!CheckBoxNonNominato.Checked);

        if (CheckBoxNonNominato.Checked)
        {
            CheckBoxProgettazioneStessiResponsabile.Checked = false;
        }

        if (OnCoordinatoreSicurezzaProgettazioneNonNominato != null)
        {
            OnCoordinatoreSicurezzaProgettazioneNonNominato(CheckBoxNonNominato.Checked);
        }
    }
}
