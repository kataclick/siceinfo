using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_DateNumeri : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void IntegraNotificaConDateENumeri(Notifica notifica)
    {
        if (!string.IsNullOrEmpty(TextBoxAmmontareComplessivo.Text))
        {
            notifica.AmmontareComplessivo = Decimal.Parse(TextBoxAmmontareComplessivo.Text);
        }
        if (!string.IsNullOrEmpty(TextBoxDataInizioLavori.Text))
        {
            notifica.DataInizioLavori = DateTime.Parse(TextBoxDataInizioLavori.Text);
        }
        if (!string.IsNullOrEmpty(TextBoxDurataLavori.Text))
        {
            notifica.Durata = Int32.Parse(TextBoxDurataLavori.Text);
        }
        // Calcolo la data di fine lavori
        if (notifica.DataInizioLavori.HasValue && notifica.Durata.HasValue)
        {
            notifica.DataFineLavori = notifica.DataInizioLavori.Value.AddDays(notifica.Durata.Value);
        }
        if (!string.IsNullOrEmpty(TextBoxNumeroLavoratori.Text))
        {
            notifica.NumeroMassimoLavoratori = Int32.Parse(TextBoxNumeroLavoratori.Text);
        }
        if (!string.IsNullOrEmpty(TextBoxNumeroImprese.Text))
        {
            notifica.NumeroImprese = Int32.Parse(TextBoxNumeroImprese.Text);
        }
        if (!string.IsNullOrEmpty(TextBoxNumeroLavoratoriAutonomi.Text))
        {
            notifica.NumeroLavoratoriAutonomi = Int32.Parse(TextBoxNumeroLavoratoriAutonomi.Text);
        }
        if (!string.IsNullOrEmpty(TextBoxNumeroUominiGiorno.Text))
        {
            notifica.NumeroGiorniUomo = Int32.Parse(TextBoxNumeroUominiGiorno.Text);
        }
    }

    public void CaricaDati(NotificaTelematica notifica)
    {
        TextBoxAmmontareComplessivo.Text = notifica.AmmontareComplessivo.ToString();
        if (notifica.DataInizioLavori.HasValue)
        {
            TextBoxDataInizioLavori.Text = notifica.DataInizioLavori.Value.ToShortDateString();
        }
        TextBoxDurataLavori.Text = notifica.Durata.ToString();
        if (notifica.DataFineLavori.HasValue)
        {
            TextBoxDataFineLavori.Text = notifica.DataFineLavori.Value.ToShortDateString();
        }
        TextBoxNumeroLavoratori.Text = notifica.NumeroMassimoLavoratori.ToString();
        TextBoxNumeroImprese.Text = notifica.NumeroImprese.ToString();
        TextBoxNumeroLavoratoriAutonomi.Text = notifica.NumeroLavoratoriAutonomi.ToString();
        TextBoxNumeroUominiGiorno.Text = notifica.NumeroGiorniUomo.ToString();
    }
}
