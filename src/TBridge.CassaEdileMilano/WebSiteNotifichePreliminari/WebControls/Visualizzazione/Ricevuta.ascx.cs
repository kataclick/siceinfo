using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;

public partial class WebControls_Visualizzazione_Ricevuta : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Int32? idNotifica = Context.Items["IdNotifica"] as Int32?;
            
            if (idNotifica.HasValue)
            {
                ReportViewerRicevuta.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                ReportViewerRicevuta.ServerReport.ReportPath = "/ReportCpt/ReportNotificaTelematica";
                ReportParameter[] listaParam = new ReportParameter[1];
                listaParam[0] = new ReportParameter("idNotifica", idNotifica.ToString());

                ReportViewerRicevuta.ServerReport.SetParameters(listaParam);
            }
        }
    }
}
