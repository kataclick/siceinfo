using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_Visualizzazione_Localizzazione : UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private string normalizzaStringa(string stringa, int lunghezza)
    {
        string res = string.Empty;

        if (stringa != null)
        {
            if (stringa.Length <= lunghezza)
                res = Server.HtmlEncode(stringa.Replace('\r', ' ').Replace('\n', ' '));
            else
                res =
                    Server.HtmlEncode(String.Format("{0}..",
                                                    stringa.Substring(0, lunghezza).Replace('\r', ' ').Replace('\n', ' ')));
        }

        return res;
    }

    public StringBuilder CaricaCantieri(CantiereNotificaCollection cantieri)
    {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.Append("LoadMap();");

        foreach (CantiereNotifica cantiere in cantieri)
        {
            stringBuilder.Append(CaricaCantiere(cantiere, false));
        }
        return stringBuilder;
    }

    public StringBuilder CaricaCantiere(CantiereNotifica cantiere, Boolean centraMappa)
    {
        StringBuilder stringBuilder = new StringBuilder();
        
        int i = 0;
        if (cantiere.Indirizzo.HaCoordinate())
        {
            var descrizione = new StringBuilder();

            // Indirizzo del cantiere
            descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Indirizzo1, 30));
            descrizione.Append(" ");
            descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Civico, 10));
            if (!string.IsNullOrEmpty(cantiere.Indirizzo.InfoAggiuntiva))
            {
                descrizione.Append(" (");
                descrizione.Append(normalizzaStringa(cantiere.Indirizzo.InfoAggiuntiva, 20));
                descrizione.Append(")");
            }
            descrizione.Append("<br />");
            descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Comune, 30));
            descrizione.Append(" ");
            if (!string.IsNullOrEmpty(cantiere.Indirizzo.Provincia))
            {
                descrizione.Append(" (");
                descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Provincia, 20));
                descrizione.Append(")");
            }

            // Informazioni aggiuntive
            descrizione.Append("<br />");
            descrizione.Append("<br />");
            descrizione.Append("<b>");
            descrizione.Append(normalizzaStringa(cantiere.NaturaOpera, 80));
            descrizione.Append("</b>");
            descrizione.Append("<br />");
            descrizione.Append("<br />");
            descrizione.Append("Comm.: ");
            descrizione.Append(normalizzaStringa(cantiere.CommittenteRagioneSociale, 30));
            descrizione.Append("<br />");
            descrizione.Append("<br />");

            ////// Link alla notifica
            //descrizione.Append("<br />");
            //descrizione.Append("<br />");

            //// Problema passaggio Id Notifica
            //String urlNotifica = ResolveUrl("~/Inserimento/DettagliNotifica.aspx");
            //descrizione.Append(String.Format("<a href={0}>Visualizza notifica</a>", urlNotifica));

            string immagineCantiere = "../Images/cantiere24.gif";

            stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", i,
                                               cantiere.Indirizzo.Latitudine.ToString().Replace(',', '.'),
                                               cantiere.Indirizzo.Longitudine.ToString().Replace(',', '.'), descrizione,
                                               immagineCantiere));
            i++;

            if (centraMappa)
            {
                stringBuilder.Append(String.Format("SetCenter('{0}','{1}','{2}');",
                                                   cantiere.Indirizzo.Latitudine.ToString().Replace(',', '.'),
                                                   cantiere.Indirizzo.Longitudine.ToString().Replace(',', '.'), 15));
            }
        }
        return stringBuilder;
    }
}