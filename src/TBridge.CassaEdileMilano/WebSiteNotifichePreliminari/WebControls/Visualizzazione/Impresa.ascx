﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Impresa.ascx.cs" Inherits="WebControls_Visualizzazione_Impresa" %>
<table class="tabellaConBordo" width="100%">
    <tr>
        <td>
            Ragione sociale<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxEnteRagioneSociale" 
                runat="server" 
                Width="300px" 
                MaxLength="255">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorRagioneSociale"
                runat="server"
                ControlToValidate="TextBoxEnteRagioneSociale"
                ValidationGroup="inserisciImpresa"
                ErrorMessage="Ragione sociale mancante">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Lavoratore autonomo:
        </td>
        <td>
            <asp:CheckBox 
                ID="CheckBoxLavoratoreAutonomo" 
                runat="server" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Partita IVA<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxEntePartitaIva" 
                runat="server" 
                Width="300px" 
                MaxLength="11">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEntePartitaIva"
                runat="server"
                ControlToValidate="TextBoxEntePartitaIva"
                ValidationGroup="inserisciImpresa"
                ErrorMessage="Partita Iva mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:CustomValidator
                ID="CustomValidatorEntePartitaIvaFormato"
                runat="server"
                ControlToValidate="TextBoxEntePartitaIva"
                ValidationGroup="stop"
                ErrorMessage="Partita Iva dell'ente non valida" 
                onservervalidate="CustomValidatorEntePartitaIvaFormato_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxEnteCodiceFiscale" 
                runat="server" 
                Width="300px" 
                MaxLength="16"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEnteCodiceFiscale"
                runat="server"
                ControlToValidate="TextBoxEnteCodiceFiscale"
                ValidationGroup="inserisciImpresa"
                ErrorMessage="Codice fiscale mancante">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator 
                ID="RegularExpressionValidatorEnteCodiceFiscale" 
                runat="server" 
                ControlToValidate="TextBoxEnteCodiceFiscale"
                ErrorMessage="Codice fiscale dell'ente non corretto" 
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                ValidationGroup="stop">
                *
            </asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Attività prevalente nel cantiere<b>*</b>:
        </td>
        <td>
            <asp:DropDownList 
                ID="DropDownListAttivitaPrevalente" 
                runat="server" 
                Width="300px" 
                AppendDataBoundItems="True" 
                ondatabound="DropDownListAttivitaPrevalente_DataBound">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorAttivitaPrevalente"
                runat="server"
                ControlToValidate="DropDownListAttivitaPrevalente"
                ErrorMessage="Attività prevalente nel cantiere mancante"
                ValidationGroup="inserisciImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr id="trIscrizioneMilano" runat="server" visible="false">
        <td>
            Iscrizione Cassa Edile Milano:
        </td>
        <td>
            <asp:RadioButton ID="RadioButtonSi" runat="server" Text="Sì" 
                GroupName="iscrittaCassa" Enabled="False" />
            <asp:RadioButton ID="RadioButtonNo" runat="server" Text="No" 
                GroupName="iscrittaCassa" Checked="True" Enabled="False" />
        </td>
        <td>
        </td>
    </tr>
    <tr id="trNumeroIscrizione" runat="server" visible="false">
        <td>
            N° iscrizione:
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceCassaEdile" runat="server" Width="300px" 
                Enabled="False" CssClass="campoDisabilitato"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Iscrizione Cassa Edile di:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListAltraCassa" runat="server" Width="300px" 
                AppendDataBoundItems="True"></asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            N° iscr. INAIL<b>*</b>:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxNumeroIscrizioneInail" 
                runat="server" 
                Width="300px">
            </asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNumeroIscrizioneInail"
                runat="server"
                ControlToValidate="TextBoxNumeroIscrizioneInail"
                ErrorMessage="N° iscr. INAIL mancante"
                ValidationGroup="inserisciImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            N° iscr. INPS<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxNumeroIscrizioneInps" runat="server" Width="300px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNumeroIscrizioneInps"
                runat="server"
                ControlToValidate="TextBoxNumeroIscrizioneInps"
                ErrorMessage="N° iscr. INPS mancante"
                ValidationGroup="inserisciImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            N° iscr. CCIAA<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxNumeroIscrizioneCciaa" runat="server" Width="300px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorNumeroIscrizioneCciaa"
                runat="server"
                ControlToValidate="TextBoxNumeroIscrizioneCciaa"
                ErrorMessage="N° iscr. CCIAA mancante"
                ValidationGroup="inserisciImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <b>
                Sede legale
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxEnteIndirizzo" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEnteIndirizzo"
                runat="server"
                ControlToValidate="TextBoxEnteIndirizzo"
                ErrorMessage="Indirizzo mancante"
                ValidationGroup="inserisciImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Provincia<b>*</b>:
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListEnteProvincia"
                runat="server"
                Width="300px"
                AppendDataBoundItems="true"
                AutoPostBack="true" 
                onselectedindexchanged="DropDownListEnteProvincia_SelectedIndexChanged">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEnteProvincia"
                runat="server"
                ControlToValidate="DropDownListEnteProvincia"
                ErrorMessage="Selezionare la provincia"
                ValidationGroup="inserisciImpresa">
             *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Città<b>*</b>:
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListEnteComune"
                runat="server"
                Width="300px"
                AppendDataBoundItems="true">
            </asp:DropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEnteCitta"
                runat="server"
                ControlToValidate="DropDownListEnteComune"
                ErrorMessage="Selezionare la città"
                ValidationGroup="inserisciImpresa">
             *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Cap:
        </td>
        <td>
            <asp:TextBox 
                ID="TextBoxEnteCap" 
                runat="server" 
                Width="300px" 
                MaxLength="5">
            </asp:TextBox>
        </td>
        <td>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidatorEnteCap"
                runat="server"
                ControlToValidate="TextBoxEnteCap"
                ErrorMessage="Cap non corretto"
                ValidationGroup="inserisciImpresa"
                ValidationExpression="^\d{5}$">
                *
            </asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Telefono<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxEnteTelefono" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorTelefono"
                runat="server"
                ControlToValidate="TextBoxEnteTelefono"
                ErrorMessage="Telefono mancante"
                ValidationGroup="inserisciImpresa">
            *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <asp:TextBox ID="TextBoxEnteFax" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
</table>