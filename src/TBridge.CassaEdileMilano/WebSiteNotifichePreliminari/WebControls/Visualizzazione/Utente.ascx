﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Utente.ascx.cs" Inherits="WebControls_Visualizzazione_Utente" %>
<asp:Panel
    ID="PanelUtente"
    runat="server"
    Width="100%">
    <table width="100%">
        <tr>
            <td colspan="3">
                <b>
                    Dati Utente
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Cognome<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxCognome"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCognome"
                    runat="server"
                    ControlToValidate="TextBoxCognome"
                    ErrorMessage="Cognome mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Nome<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxNome"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorNome"
                    runat="server"
                    ControlToValidate="TextBoxNome"
                    ErrorMessage="Nome mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                C.I. N°<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxCartaIdentita"
                    runat="server"
                    MaxLength="9"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCartaIdentita"
                    runat="server"
                    ControlToValidate="TextBoxCartaIdentita"
                    ErrorMessage="N° C.I. mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="RegularExpressionValidatorCartaIdentita"
                    runat="server"
                    ControlToValidate="TextBoxCartaIdentita"
                    ErrorMessage="Formato N° C.I. errato"
                    ValidationExpression="^[a-zA-Z]{2}\d{7}$">
                *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Indirizzo<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxIndirizzo"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorIndirizzo"
                    runat="server"
                    ControlToValidate="TextBoxIndirizzo"
                    ErrorMessage="Indirizzo mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Comune<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxComune"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorComune"
                    runat="server"
                    ControlToValidate="TextBoxComune"
                    ErrorMessage="Comune mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Provincia<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxProvincia"
                    runat="server"
                    MaxLength="50"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorProvincia"
                    runat="server"
                    ControlToValidate="TextBoxProvincia"
                    ErrorMessage="Provincia mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Cap<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxCap"
                    runat="server"
                    MaxLength="5"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorCap"
                    runat="server"
                    ControlToValidate="TextBoxCap"
                    ErrorMessage="Cap mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="RegularExpressionValidatorFax"
                    runat="server"
                    ControlToValidate="TextBoxCap"
                    ErrorMessage="Cap errato"
                    ValidationExpression="^\d{5}$">
                *
                </asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Telefono<b>*</b>:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxTelefono"
                    runat="server"
                    MaxLength="15"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidatorTelefono"
                    runat="server"
                    ControlToValidate="TextBoxTelefono"
                    ErrorMessage="Telefono mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Fax:
            </td>
            <td>
                <asp:TextBox
                    ID="TextBoxFax"
                    runat="server"
                    MaxLength="15"
                    Width="300px">
                </asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:ValidationSummary
                    ID="ValidationSummaryRegistrazione"
                    runat="server"
                    CssClass="messaggiErrore" />
            </td>
        </tr>
    </table>
</asp:Panel>