using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Impresa = TBridge.Cemi.Cantieri.Type.Entities.Impresa;
using TipologiaImpresa = TBridge.Cemi.Cantieri.Type.Enums.TipologiaImpresa;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_Visualizzazione_Impresa : System.Web.UI.UserControl
{
    private readonly CptBusiness biz = new CptBusiness();
    private Common commonBiz = new Common();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTipologieAttivita();
            CaricaCasseEdili();

            if (DropDownListEnteProvincia.Items.Count == 0)
            {
                Presenter.CaricaProvince(DropDownListEnteProvincia);
            }
        }
    }

    protected void DropDownListEnteProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        Presenter.CaricaComuni(
            DropDownListEnteComune,
            DropDownListEnteProvincia.SelectedItem.Text);
    }

    public void CaricaImpresa(ImpresaNotificheTelematiche impresa)
    {
        ResetCampi();

        TextBoxEnteRagioneSociale.Text = impresa.RagioneSociale;
        CheckBoxLavoratoreAutonomo.Checked = impresa.LavoratoreAutonomo;
        DropDownListAttivitaPrevalente.SelectedValue = impresa.AttivitaPrevalente;
        TextBoxEntePartitaIva.Text = impresa.PartitaIva;
        TextBoxEnteCodiceFiscale.Text = impresa.CodiceFiscale;
        TextBoxCodiceCassaEdile.Text = impresa.IdImpresa.ToString();
        DropDownListAltraCassa.SelectedValue = impresa.IdCassaEdile;
        TextBoxNumeroIscrizioneCciaa.Text = impresa.MatricolaCCIAA;
        TextBoxNumeroIscrizioneInail.Text = impresa.MatricolaINAIL;
        TextBoxNumeroIscrizioneInps.Text = impresa.MatricolaINPS;

        TextBoxEnteIndirizzo.Text = impresa.Indirizzo;

        Presenter.CaricaProvince(DropDownListEnteProvincia);
        DropDownListEnteProvincia.SelectedValue = impresa.Provincia;
        DropDownListEnteProvincia_SelectedIndexChanged(this, new EventArgs());

        if (DropDownListEnteComune.Items.Count > 0)
        {
            DropDownListEnteComune.SelectedValue = impresa.Comune;
        }
        
        TextBoxEnteCap.Text = impresa.Cap;
        TextBoxEnteTelefono.Text = impresa.Telefono;
        TextBoxEnteFax.Text = impresa.Fax;

        ViewState["IdImpresa"] = impresa.IdImpresa;
        ViewState["IdImpresaTelematica"] = impresa.IdImpresaTelematica;
        ViewState["IdImpresaAnagrafica"] = impresa.IdImpresaAnagrafica;
        ViewState["IdTemporaneo"] = impresa.IdTemporaneo;

        DisabilitaDatiPrincipali();
    }

    public void Nuovo()
    {
        ResetCampi();
    }

    private void DisabilitaDatiPrincipali()
    {
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text))
        {
            TextBoxEnteRagioneSociale.Enabled = false;
            TextBoxEnteRagioneSociale.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxEntePartitaIva.Text))
        {
            TextBoxEntePartitaIva.Enabled = false;
            TextBoxEntePartitaIva.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxEnteCodiceFiscale.Text))
        {
            TextBoxEnteCodiceFiscale.Enabled = false;
            TextBoxEnteCodiceFiscale.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxNumeroIscrizioneInps.Text))
        {
            TextBoxNumeroIscrizioneInps.Enabled = false;
            TextBoxNumeroIscrizioneInps.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxNumeroIscrizioneInail.Text))
        {
            TextBoxNumeroIscrizioneInail.Enabled = false;
            TextBoxNumeroIscrizioneInail.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxNumeroIscrizioneCciaa.Text))
        {
            TextBoxNumeroIscrizioneCciaa.Enabled = false;
            TextBoxNumeroIscrizioneCciaa.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxCodiceCassaEdile.Text))
        {
            TextBoxCodiceCassaEdile.Enabled = false;
            TextBoxCodiceCassaEdile.CssClass = "campoDisabilitato";
            DropDownListAltraCassa.SelectedIndex = 0;
            DropDownListAltraCassa.Enabled = false;

            RadioButtonSi.Checked = true;
            RadioButtonNo.Checked = false;
        }
        else
        {
            RadioButtonSi.Checked = false;
            RadioButtonNo.Checked = true;

            DropDownListAltraCassa.SelectedIndex = 0;
            DropDownListAltraCassa.Enabled = true;
        }
    }

    public void ResetCampi()
    {
        ViewState["IdImpresa"] = null;
        ViewState["IdImpresaAnagrafica"] = null;
        ViewState["IdTemporaneo"] = null;
        ViewState["IdImpresaTelematica"] = null;

        TextBoxEnteRagioneSociale.Enabled = true;
        TextBoxEntePartitaIva.Enabled = true;
        TextBoxEnteCodiceFiscale.Enabled = true;
        TextBoxNumeroIscrizioneInps.Enabled = true;
        TextBoxNumeroIscrizioneInail.Enabled = true;
        TextBoxNumeroIscrizioneCciaa.Enabled = true;
        //TextBoxCodiceCassaEdile.Enabled = true;

        TextBoxEnteRagioneSociale.CssClass = String.Empty;
        TextBoxEntePartitaIva.CssClass = String.Empty;
        TextBoxEnteCodiceFiscale.CssClass = String.Empty;
        TextBoxNumeroIscrizioneInps.CssClass = String.Empty;
        TextBoxNumeroIscrizioneInail.CssClass = String.Empty;
        TextBoxNumeroIscrizioneCciaa.CssClass = String.Empty;
        //TextBoxCodiceCassaEdile.CssClass = String.Empty;

        //RadioButtonSi.Enabled = true;
        //RadioButtonNo.Enabled = true;

        DropDownListAltraCassa.Enabled = true;

        Presenter.SvuotaCampo(TextBoxEnteRagioneSociale);
        Presenter.SvuotaCampo(TextBoxEntePartitaIva);
        Presenter.SvuotaCampo(TextBoxEnteCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxCodiceCassaEdile);
        Presenter.SvuotaCampo(TextBoxEnteIndirizzo);

        DropDownListEnteComune.Items.Clear();
        DropDownListEnteProvincia.SelectedIndex = -1;
        
        Presenter.SvuotaCampo(TextBoxEnteCap);
        Presenter.SvuotaCampo(TextBoxEnteTelefono);
        Presenter.SvuotaCampo(TextBoxEnteFax);
        Presenter.SvuotaCampo(TextBoxNumeroIscrizioneCciaa);
        Presenter.SvuotaCampo(TextBoxNumeroIscrizioneInail);
        Presenter.SvuotaCampo(TextBoxNumeroIscrizioneInps);

        CaricaCasseEdili();
        DropDownListAltraCassa.SelectedIndex = 0;
        CaricaTipologieAttivita();
        DropDownListAttivitaPrevalente.SelectedIndex = 0;

        CheckBoxLavoratoreAutonomo.Checked = false;
        RadioButtonSi.Checked = false;
        RadioButtonNo.Checked = true;
    }

    public ImpresaNotificheTelematiche CreaImpresa()
    {
        ImpresaNotificheTelematiche impresa = new ImpresaNotificheTelematiche();

        if (ViewState["IdTemporaneo"] == null
            || (Guid)ViewState["IdTemporaneo"] == Guid.Empty)
        {
            impresa.IdTemporaneo = Guid.NewGuid();
        }
        else
        {
            impresa.IdTemporaneo = (Guid)ViewState["IdTemporaneo"];
        }
        impresa.IdImpresa = ViewState["IdImpresa"] as Int32?;
        impresa.IdImpresaAnagrafica = ViewState["IdImpresaAnagrafica"] as Int32?;
        impresa.IdImpresaTelematica = ViewState["IdImpresaTelematica"] as Int32?;
        
        impresa.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxEnteRagioneSociale.Text);
        impresa.LavoratoreAutonomo = CheckBoxLavoratoreAutonomo.Checked;
        impresa.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxEntePartitaIva.Text);
        impresa.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxEnteCodiceFiscale.Text);
        impresa.AttivitaPrevalente = DropDownListAttivitaPrevalente.SelectedValue;
        impresa.IdCassaEdile = DropDownListAltraCassa.SelectedValue;
        impresa.MatricolaINPS = Presenter.NormalizzaCampoTesto(TextBoxNumeroIscrizioneInps.Text);
        impresa.MatricolaINAIL = Presenter.NormalizzaCampoTesto(TextBoxNumeroIscrizioneInail.Text);
        impresa.MatricolaCCIAA = Presenter.NormalizzaCampoTesto(TextBoxNumeroIscrizioneCciaa.Text);

        impresa.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxEnteIndirizzo.Text);
        impresa.Provincia = DropDownListEnteProvincia.SelectedItem.Text;
        impresa.Comune = DropDownListEnteComune.SelectedItem.Text;
        impresa.Cap = Presenter.NormalizzaCampoTesto(TextBoxEnteCap.Text);
        impresa.Telefono = Presenter.NormalizzaCampoTesto(TextBoxEnteTelefono.Text);
        impresa.Fax = Presenter.NormalizzaCampoTesto(TextBoxEnteFax.Text);
        
        return impresa;
    }

    #region Caricamento Drop Down
    private void CaricaTipologieAttivita()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListAttivitaPrevalente,
            biz.GetTipologieAttivita(),
            "",
            "");
    }

    protected void DropDownListAttivitaPrevalente_DataBound(object sender, EventArgs e)
    {
        for(Int32 i = 0; i < DropDownListAttivitaPrevalente.Items.Count; i++)
        {
            DropDownListAttivitaPrevalente.Items[i].Attributes.Add("Title", DropDownListAttivitaPrevalente.Items[i].Text);
        }
    }

    private void CaricaCasseEdili()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListAltraCassa,
            commonBiz.GetCasseEdili(),
            "Descrizione",
            "IdCassaEdile");
    }
    #endregion

    protected void CustomValidatorEntePartitaIvaFormato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        try
        {
            if (commonBiz.VerificaPartitaIVA(TextBoxEntePartitaIva.Text))
            {
                args.IsValid = true;
            }
        }
        catch { }
    }
}
