﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificaInAggiornamento.ascx.cs" Inherits="WebControls_Visualizzazione_NotificaInAggiornamento" %>

<style type="text/css">
    .stylePrimaColonna
    {
        width: 100px;
    }
</style>

<table class="tabellaConBordo" width="100%">
    <tr>
        <td colspan="2">
            <b>
                Notifica in aggiornamento
            </b>
        </td>
    </tr>    
    <tr>
        <td class="stylePrimaColonna">
            Protocollo
        </td>
        <td>
            <b>
                <asp:Label 
                    ID="LabelNotificaAggiornamentoProtocollo"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="stylePrimaColonna">
            Natura opera
        </td>
        <td>
            <b>
                <asp:Label 
                    ID="LabelNotificaAggiornamentoNaturaOpera"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
</table>
