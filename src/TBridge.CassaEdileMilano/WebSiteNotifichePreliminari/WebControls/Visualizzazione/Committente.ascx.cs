using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cpt.Business;

public partial class WebControls_Visualizzazione_Committente : System.Web.UI.UserControl
{
    Common commonBiz = new Common();
    CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (DropDownListPersonaProvincia.Items.Count == 0)
            {
                Presenter.CaricaProvince(DropDownListPersonaProvincia);
            }
            if (DropDownListEnteProvincia.Items.Count == 0)
            {
                Presenter.CaricaProvince(DropDownListEnteProvincia);
            }
            if (DropDownListTipologia.Items.Count == 0)
            {
                CaricaTipologieCommittente();
            }
        }
    }

    private void CaricaTipologieCommittente()
    {
        Presenter.CaricaElementiInDropDownConElementoVuoto(
            DropDownListTipologia,
            biz.GetTipologieCommittente(),
            "Descrizione",
            "IdTipologiaCommittente");
    }

    protected void DropDownListPersonaProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        Presenter.CaricaComuni(
            DropDownListPersonaComune,
            DropDownListPersonaProvincia.SelectedItem.Text);
    }

    protected void DropDownListEnteProvincia_SelectedIndexChanged(object sender, EventArgs e)
    {
        Presenter.CaricaComuni(
            DropDownListEnteComune,
            DropDownListEnteProvincia.SelectedItem.Text);
    }

    public void CaricaDati(CommittenteNotificheTelematiche committente)
    {
        ResetCampi();

        CaricaTipologieCommittente();
        if (committente.TipologiaCommittente != null)
        {
            DropDownListTipologia.SelectedValue = committente.TipologiaCommittente.IdTipologiaCommittente.ToString();
        }

        TextBoxPersonaCognome.Text = committente.PersonaCognome;
        TextBoxPersonaNome.Text = committente.PersonaNome;
        TextBoxPersonaIndirizzo.Text = committente.PersonaIndirizzo;
        
        Presenter.CaricaProvince(DropDownListPersonaProvincia);
        DropDownListPersonaProvincia.SelectedValue = committente.PersonaProvincia;
        DropDownListPersonaProvincia_SelectedIndexChanged(this, new EventArgs());

        if (DropDownListPersonaComune.Items.Count > 0)
        {
            DropDownListPersonaComune.SelectedValue = committente.PersonaComune;
        }
        
        TextBoxPersonaCap.Text = committente.PersonaCap;
        TextBoxPersonaTelefono.Text = committente.PersonaTelefono;
        TextBoxPersonaFax.Text = committente.PersonaFax;
        TextBoxPersonaCellulare.Text = committente.PersonaCellulare;
        TextBoxPersonaEmail.Text = committente.PersonaEmail;
        TextBoxPersonaCodiceFiscale.Text = committente.PersonaCodiceFiscale;

        ViewState["IdCommittenteTelematiche"] = committente.IdCommittenteTelematiche;
        ViewState["IdCommittenteAnagrafica"] = committente.IdCommittenteAnagrafica;
        TextBoxEnteRagioneSociale.Text = committente.RagioneSociale;
        TextBoxEntePartitaIva.Text = committente.PartitaIva;
        TextBoxEnteCodiceFiscale.Text = committente.CodiceFiscale;
        TextBoxEnteIndirizzo.Text = committente.Indirizzo;

        Presenter.CaricaProvince(DropDownListEnteProvincia);
        DropDownListEnteProvincia.SelectedValue = committente.Provincia;
        DropDownListEnteProvincia_SelectedIndexChanged(this, new EventArgs());

        if (DropDownListEnteComune.Items.Count > 0)
        {
            DropDownListEnteComune.SelectedValue = committente.Comune;
        }

        TextBoxEnteCap.Text = committente.Cap;
        TextBoxEnteTelefono.Text = committente.Telefono;
        TextBoxEnteFax.Text = committente.Fax;

        DisabilitaDatiPrincipali();
    }

    public void Nuovo()
    {
        ResetCampi();
    }

    private void DisabilitaDatiPrincipali()
    {
        if (!String.IsNullOrEmpty(DropDownListTipologia.SelectedValue))
        {
            DropDownListTipologia.Enabled = false;
        }

        if (!String.IsNullOrEmpty(TextBoxPersonaCognome.Text))
        {
            TextBoxPersonaCognome.Enabled = false;
            TextBoxPersonaCognome.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxPersonaNome.Text))
        {
            TextBoxPersonaNome.Enabled = false;
            TextBoxPersonaNome.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxPersonaCodiceFiscale.Text))
        {
            TextBoxPersonaCodiceFiscale.Enabled = false;
            TextBoxPersonaCodiceFiscale.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text))
        {
            TextBoxEnteRagioneSociale.Enabled = false;
            TextBoxEnteRagioneSociale.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxEnteCodiceFiscale.Text))
        {
            TextBoxEnteCodiceFiscale.Enabled = false;
            TextBoxEnteCodiceFiscale.CssClass = "campoDisabilitato";
        }
        if (!String.IsNullOrEmpty(TextBoxEntePartitaIva.Text))
        {
            TextBoxEntePartitaIva.Enabled = false;
            TextBoxEntePartitaIva.CssClass = "campoDisabilitato";
        }
    }

    private void ResetCampi()
    {
        ViewState["IdCommittenteTelematiche"] = null;
        ViewState["IdCommittenteAnagrafica"] = null;

        DropDownListTipologia.Enabled = true;
        DropDownListTipologia.SelectedValue = String.Empty;

        TextBoxPersonaCognome.Enabled = true;
        TextBoxPersonaNome.Enabled = true;
        TextBoxPersonaCodiceFiscale.Enabled = true;
        TextBoxEnteRagioneSociale.Enabled = true;
        TextBoxEnteCodiceFiscale.Enabled = true;
        TextBoxEntePartitaIva.Enabled = true;
        TextBoxPersonaCognome.CssClass = String.Empty;
        TextBoxPersonaNome.CssClass = String.Empty;
        TextBoxPersonaCodiceFiscale.CssClass = String.Empty;
        TextBoxEnteRagioneSociale.CssClass = String.Empty;
        TextBoxEnteCodiceFiscale.CssClass = String.Empty;
        TextBoxEntePartitaIva.CssClass = String.Empty;

        Presenter.SvuotaCampo(TextBoxPersonaCognome);
        Presenter.SvuotaCampo(TextBoxPersonaNome);
        Presenter.SvuotaCampo(TextBoxPersonaIndirizzo);

        DropDownListPersonaComune.Items.Clear();
        DropDownListPersonaProvincia.SelectedIndex = 0;
        
        Presenter.SvuotaCampo(TextBoxPersonaCap);
        Presenter.SvuotaCampo(TextBoxPersonaTelefono);
        Presenter.SvuotaCampo(TextBoxPersonaFax);
        Presenter.SvuotaCampo(TextBoxPersonaCellulare);
        Presenter.SvuotaCampo(TextBoxPersonaEmail);
        Presenter.SvuotaCampo(TextBoxPersonaCodiceFiscale);

        Presenter.SvuotaCampo(TextBoxEnteRagioneSociale);
        Presenter.SvuotaCampo(TextBoxEntePartitaIva);
        Presenter.SvuotaCampo(TextBoxEnteCodiceFiscale);
        Presenter.SvuotaCampo(TextBoxEnteIndirizzo);

        DropDownListEnteComune.Items.Clear();
        DropDownListEnteProvincia.SelectedIndex = 0;

        Presenter.SvuotaCampo(TextBoxEnteCap);
        Presenter.SvuotaCampo(TextBoxEnteTelefono);
        Presenter.SvuotaCampo(TextBoxEnteFax);
    }

    public CommittenteNotificheTelematiche CreaCommittente()
    {
        CommittenteNotificheTelematiche committente = new CommittenteNotificheTelematiche();

        if (ViewState["IdCommittenteTelematiche"] != null)
        {
            committente.IdCommittenteTelematiche = (Int32)ViewState["IdCommittenteTelematiche"];
        }

        if (ViewState["IdCommittenteAnagrafica"] != null)
        {
            committente.IdCommittenteAnagrafica = (Int32?)ViewState["IdCommittenteAnagrafica"];
        }

        if (!String.IsNullOrEmpty(DropDownListTipologia.SelectedValue))
        {
            committente.TipologiaCommittente = new TipologiaCommittente();
            committente.TipologiaCommittente.IdTipologiaCommittente = Int32.Parse(DropDownListTipologia.SelectedItem.Value);
            committente.TipologiaCommittente.Descrizione = DropDownListTipologia.SelectedItem.Text;
        }

        // Persona
        committente.PersonaCognome = Presenter.NormalizzaCampoTesto(TextBoxPersonaCognome.Text);
        committente.PersonaNome = Presenter.NormalizzaCampoTesto(TextBoxPersonaNome.Text);
        committente.PersonaIndirizzo = Presenter.NormalizzaCampoTesto(TextBoxPersonaIndirizzo.Text);
        committente.PersonaProvincia = DropDownListPersonaProvincia.SelectedItem.Text;
        if (DropDownListPersonaComune.SelectedItem != null)
        {
            committente.PersonaComune = DropDownListPersonaComune.SelectedItem.Text;
        }
        committente.PersonaCap = Presenter.NormalizzaCampoTesto(TextBoxPersonaCap.Text);
        committente.PersonaTelefono = Presenter.NormalizzaCampoTesto(TextBoxPersonaTelefono.Text);
        committente.PersonaFax = Presenter.NormalizzaCampoTesto(TextBoxPersonaFax.Text);
        committente.PersonaCellulare = Presenter.NormalizzaCampoTesto(TextBoxPersonaCellulare.Text);
        committente.PersonaEmail = TextBoxPersonaEmail.Text;
        committente.PersonaCodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxPersonaCodiceFiscale.Text);

        // Ente
        committente.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxEnteRagioneSociale.Text);
        committente.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxEntePartitaIva.Text);
        committente.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxEnteCodiceFiscale.Text);
        committente.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxEnteIndirizzo.Text);
        committente.Provincia = DropDownListEnteProvincia.SelectedItem.Text;
        if (DropDownListEnteComune.SelectedItem != null)
        {
            committente.Comune = DropDownListEnteComune.SelectedItem.Text;
        }
        committente.Cap = Presenter.NormalizzaCampoTesto(TextBoxEnteCap.Text);
        committente.Telefono = Presenter.NormalizzaCampoTesto(TextBoxEnteTelefono.Text);
        committente.Fax = Presenter.NormalizzaCampoTesto(TextBoxEnteFax.Text);
        
        return committente;
    }

    public void ImpostaValidationGroup(String validationGroup)
    {
        foreach (Control control in this.Controls)
        {
            if (control is BaseValidator)
            {
                BaseValidator val = control as BaseValidator;
                val.ValidationGroup = validationGroup;
            }
        }
    }

    #region Custom Validators
    protected void CustomValidatorEnteRagioneSociale_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Deve essere immesso il cognome e nome della persona o la ragione sociale dell'ente
        if (String.IsNullOrEmpty(TextBoxPersonaCognome.Text)
            && String.IsNullOrEmpty(TextBoxPersonaNome.Text)
            && String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorEntePartitaIva_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Se viene dichiarato un ente deve essere immessa anche la partita iva
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text)
            && String.IsNullOrEmpty(TextBoxEntePartitaIva.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorEnteIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Se viene dichiarato un ente deve essere immessa anche l'indirizzo
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text)
            && String.IsNullOrEmpty(TextBoxEnteIndirizzo.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorEnteCitta_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Se viene dichiarato un ente deve essere immessa anche la citt�
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text)
            && (DropDownListEnteComune.SelectedItem == null
                || String.IsNullOrEmpty(DropDownListEnteComune.SelectedItem.Text)))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorEnteProvincia_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Se viene dichiarato un ente deve essere immessa anche la provincia
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text)
            && (DropDownListEnteProvincia.SelectedItem == null
                || String.IsNullOrEmpty(DropDownListEnteProvincia.SelectedItem.Text)))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorEnteTelefono_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // Se viene dichiarato un ente deve essere immesso anche il telefono
        if (!String.IsNullOrEmpty(TextBoxEnteRagioneSociale.Text)
            && String.IsNullOrEmpty(TextBoxEnteTelefono.Text))
        {
            args.IsValid = false;
        }
    }

    protected void CustomValidatorEntePartitaIvaFormato_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = false;

        try
        {
            if (commonBiz.VerificaPartitaIVA(TextBoxEntePartitaIva.Text))
            {
                args.IsValid = true;
            }
        }
        catch { }
    }
    #endregion
}
