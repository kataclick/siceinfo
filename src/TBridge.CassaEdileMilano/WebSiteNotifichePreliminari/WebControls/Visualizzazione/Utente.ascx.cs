using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Presenter;

public partial class WebControls_Visualizzazione_Utente : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //TextBoxCognome.Text = "PROVACOGN";
            //TextBoxNome.Text = "PROVANOM";
            //TextBoxIndirizzo.Text = "VIA PROVA 4";
            //TextBoxCartaIdentita.Text = "AA1234567";
            //TextBoxComune.Text = "PROVACOM";
            //TextBoxProvincia.Text = "PROVAPROV";
            //TextBoxCap.Text = "12345";
            //TextBoxTelefono.Text = "12345678";
        }
    }

    public UtenteNotificheTelematiche GetUtente()
    {
        UtenteNotificheTelematiche utente = new UtenteNotificheTelematiche();

        utente.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        utente.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        utente.CartaIdentita = Presenter.NormalizzaCampoTesto(TextBoxCartaIdentita.Text);
        utente.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        utente.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        utente.Provincia = Presenter.NormalizzaCampoTesto(TextBoxProvincia.Text);
        utente.Cap = TextBoxCap.Text;
        utente.Telefono = TextBoxTelefono.Text;
        utente.Fax = TextBoxFax.Text;

        return utente;
    }

    public void CaricaUtente(UtenteNotificheTelematiche utente)
    {
        PanelUtente.Enabled = false;

        TextBoxCognome.Text = utente.Cognome;
        TextBoxNome.Text = utente.Nome;
        TextBoxCartaIdentita.Text = utente.CartaIdentita;
        TextBoxIndirizzo.Text = utente.Indirizzo;
        TextBoxComune.Text = utente.Comune;
        TextBoxProvincia.Text = utente.Provincia;
        TextBoxCap.Text = utente.Cap;
        TextBoxTelefono.Text = utente.Telefono;
        TextBoxFax.Text = utente.Fax;
    }
}
