using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Type.Entities;

public partial class WebControls_Ricerca_Committente : System.Web.UI.UserControl
{
    private readonly Common bizCommon = new Common();
    private readonly CptBusiness biz = new CptBusiness();
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CommittenteNotificheTelematiche committente = biz.GetCommittentiTelematicheDaAnagrafica(
                Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text));
            
            CaricaCommittenteDaRicerca(committente);

            ButtonNuovo.Enabled = true;
        }
    }

    private void CaricaCommittenteDaRicerca(CommittenteNotificheTelematiche committente)
    {
        if (committente != null)
        {
            // Carico i dati del committente
            PanelDatiCommittente.Enabled = true;
            LabelCommittenteNonTrovato.Visible = false;
            Committente1.CaricaDati(committente);
        }
        else
        {
            // Il committente va inserito come nuovo
            PanelDatiCommittente.Enabled = true;
            LabelCommittenteNonTrovato.Visible = true;
            Committente1.Nuovo();
        }
    }

    protected void ButtonNuovo_Click(object sender, EventArgs e)
    {
        Committente1.Nuovo();
    }

    public CommittenteNotificheTelematiche CreaCommittente()
    {
        return Committente1.CreaCommittente();
    }

    public void CaricaCommittente(CommittenteNotificheTelematiche committente)
    {
        PanelDatiCommittente.Enabled = true;
        Committente1.CaricaDati(committente);
    }

    public void DisabilitaRicerca()
    {
        tabellaFiltriRicerca.Visible = false;
        PanelDatiCommittente.Enabled = false;
    }
}
