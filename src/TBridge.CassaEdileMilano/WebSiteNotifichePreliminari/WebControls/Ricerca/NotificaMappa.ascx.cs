using System;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.Presenter;

public partial class WebControls_Ricerca_NotificaMappa : UserControl
{
    private const Int32 MAXPUSHPIN = 300;

    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaCantieri();
        }
    }

    private void CaricaCantieri()
    {
        CantiereFilter filtro = CreaFiltro();

        CantiereNotificaCollection cantieri =
            biz.RicercaCantieriPerCommittente(filtro);

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", cantieri.Count);

        if (cantieri.Count <= MAXPUSHPIN)
        {
            CaricaMappa(cantieri);
            LabelErrore.Visible = false;
        }
        else
        {
            LabelErrore.Visible = true;
        }
    }

    private void CaricaMappa(CantiereNotificaCollection cantieri)
    {
        StringBuilder stringBuilder;
        stringBuilder = Localizzazione1.CaricaCantieri(cantieri);
        RadAjaxPanel1.ResponseScripts.Add(stringBuilder.ToString());
    }

    private CantiereFilter CreaFiltro()
    {
        CantiereFilter filtro = new CantiereFilter();

        filtro.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
        filtro.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        filtro.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);

        // Imposto l'area di Milano
        filtro.IdArea = 1;

        // Imposto l'utente
        filtro.IdUtenteTelematiche = (Guid) Membership.GetUser().ProviderUserKey;

        return filtro;
    }
}