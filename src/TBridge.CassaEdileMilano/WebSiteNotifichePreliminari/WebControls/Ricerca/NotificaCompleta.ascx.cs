using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.Security;

public partial class WebControls_Ricerca_NotificaCompleta : System.Web.UI.UserControl
{
    private const String IMMAGINEAGGIORNAMENTO = "~/Images/aggiornaNotifica32.png";
    private const String IMMAGINELOCALIZZAZIONE = "~/Images/localizzazioneGrigia24.png";
    private const Int32 INDICEBOTTONEMAPPA = 0;

    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private NotificaFilter CreaFiltro()
    {
        NotificaFilter filtro = new NotificaFilter();

        if (!String.IsNullOrEmpty(TextBoxProtocollo.Text))
        {
            filtro.IdNotifica = Int32.Parse(TextBoxProtocollo.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxDataInserimento.Text))
        {
            filtro.Data = DateTime.Parse(TextBoxDataInserimento.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxIndirizzo.Text))
        {
            filtro.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxNaturaOpera.Text))
        {
            filtro.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
        }

        if (!String.IsNullOrEmpty(TextBoxComune.Text))
        {
            filtro.IndirizzoComune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
        }

        // Filtro su area Milano
        filtro.IdArea = 1;

        // Filtro su utente
        if (!Roles.IsUserInRole("Ricerca"))
        {
            filtro.IdUtenteTelematiche = (Guid)Membership.GetUser().ProviderUserKey;
        }

        return filtro;
    }

    public void CaricaNotifiche(Int32 pagina)
    {
        NotificaFilter filtro = CreaFiltro();

        NotificaCollection notifiche =
            biz.RicercaNotifiche(filtro);

        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Notifiche trovate: {0}", notifiche.Count.ToString());

        Presenter.CaricaElementiInGridView(
            GridViewNotifiche,
            notifiche,
            pagina);
    }

    protected void GridViewNotifiche_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notificaRif = (Notifica)e.Row.DataItem;

            Label lNotificaPrel = (Label)e.Row.FindControl("LabelProtocolloPreliminare");
            Label lNotificaUlti = (Label)e.Row.FindControl("LabelProtocolloUltimo");
            Label lData = (Label)e.Row.FindControl("LabelData");
            Label lNaturaOpera = (Label)e.Row.FindControl("LabelNaturaOpera");
            Label lNumeroAppalto = (Label)e.Row.FindControl("LabelNumeroAppalto");
            Label lCommittente = (Label)e.Row.FindControl("LabelCommittente");
            Label lDataUltimoAgg = (Label)e.Row.FindControl("LabelDataUltimoAggiornamento");
            GridView gvStoria = (GridView)e.Row.FindControl("GridViewAggiornamenti");
            GridView gvIndirizzi = (GridView)e.Row.FindControl("GridViewIndirizzi");

            lNotificaPrel.Text = notificaRif.IdNotificaPadre.ToString();
            if (notificaRif.IdNotifica != notificaRif.IdNotificaPadre)
            {
                lNotificaUlti.Text = notificaRif.IdNotifica.ToString();
                lData.Text = notificaRif.DataNotificaPadre.Value.ToShortDateString();
                lDataUltimoAgg.Text = notificaRif.Data.ToShortDateString();
            }
            else
            {
                lData.Text = notificaRif.Data.ToShortDateString();
            }
            lNaturaOpera.Text = notificaRif.NaturaOpera;
            lNumeroAppalto.Text = notificaRif.NumeroAppalto;
            lCommittente.Text =
                String.Format("{0}<BR/>{1} - {2} {3}", notificaRif.CommittenteRagioneSociale,
                              notificaRif.Committente.Indirizzo, notificaRif.Committente.Comune,
                              notificaRif.Committente.Provincia);

            Presenter.CaricaElementiInGridView(
                gvIndirizzi,
                notificaRif.Indirizzi,
                0);

            Presenter.CaricaElementiInGridView(
                gvStoria,
                notificaRif.Storia,
                0);
        }
    }

    protected void GridViewAggiornamenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvStoria = (GridView)sender;
        Int32 idNotifica = (int)gvStoria.DataKeys[e.NewSelectedIndex].Value;

        Context.Items["IdNotifica"] = idNotifica;
        Server.Transfer("~/Inserimento/DettagliNotifica.aspx");
    }

    protected void GridViewAggiornamenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notifica = (Notifica)e.Row.DataItem;

            if (notifica.IdNotificaPadre.HasValue)
                e.Row.ForeColor = Color.Gray;
        }
    }

    protected void GridViewNotifiche_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaNotifiche(e.NewPageIndex);
    }

    protected void GridViewAggiornamenti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        GridView gvStoria = (GridView)sender;
        Int32 idNotifica = (int)gvStoria.DataKeys[e.RowIndex].Value;

        Context.Items["IdNotifica"] = idNotifica;
        Server.Transfer("~/Inserimento/RicevutaNotifica.aspx");
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche(0);
        }
    }

    protected void GridViewIndirizzi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Indirizzo indirizzo = (Indirizzo)e.Row.DataItem;
            ImageButton ibMappa = (ImageButton)e.Row.FindControl("ImageButtonMappa");

            if (!indirizzo.HaCoordinate())
            {
                ibMappa.ImageUrl = IMMAGINELOCALIZZAZIONE;
                e.Row.Cells[INDICEBOTTONEMAPPA].Enabled = false;
            }
        }
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvIndirizzi = (GridView)sender;
        Int32 idIndirizzo = (Int32)gvIndirizzi.DataKeys[e.NewSelectedIndex].Values["IdIndirizzo"];

        Context.Items["IdIndirizzo"] = idIndirizzo;
        Server.Transfer("~/Inserimento/LocalizzaIndirizzo.aspx");
    }
}
