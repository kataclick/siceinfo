﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificaMappa.ascx.cs" Inherits="WebControls_Ricerca_NotificaMappa" %>


<%@ Register src="../Visualizzazione/Localizzazione.ascx" tagname="Localizzazione" tagprefix="uc1" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1" Width="100%">
<div>
    <table width="100%">
        <tr>
            <td>
                Natura opera
            </td>
            <td>
                Indirizzo
            </td>
            <td>
                Comune
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox 
                    ID="TextBoxNaturaOpera" 
                    runat="server"
                    MaxLength="500"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxIndirizzo" 
                    runat="server"
                    MaxLength="255" 
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxComune" 
                    runat="server" 
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricercaPerAggiornamento"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="100%" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricercaPerAggiornamento" />
            </td>
        </tr>
    </table>
    <asp:Label 
        ID="LabelErrore" 
        runat="server" 
        Text="I cantieri da visualizzare sono troppi, filtrare maggiormente"
        CssClass="messaggiErrore"
        Visible="False">
    </asp:Label>
    <br />
    <asp:Label 
        ID="LabelCantieriTrovati" 
        runat="server" 
        Text="Notifiche trovate" 
        Visible="False">
    </asp:Label>
</div>
<br />
<div>
    <uc1:Localizzazione ID="Localizzazione1" runat="server" />
</div>
</telerik:RadAjaxPanel>