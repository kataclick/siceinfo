﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Impresa.ascx.cs" Inherits="WebControls_Ricerca_Impresa" %>
<%@ Register src="../Visualizzazione/Impresa.ascx" tagname="Impresa" tagprefix="uc1" %>
<div>
    <asp:Panel ID="PanelFiltriRicerca" runat="server" DefaultButton="ButtonRicerca">
        <table>
            <tr>
                <td>
                    Partita IVA /Codice fiscale:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px" MaxLength="16"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                        ErrorMessage="Inserire un codice fiscale" ValidationGroup="ricercaImpresa">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                        ErrorMessage="Codice fiscale non corretto" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                        ValidationGroup="ricercaImpresa">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="ButtonRicerca" runat="server" Width="150px" Text="Ricerca" 
                        ValidationGroup="ricercaImpresa" onclick="ButtonRicerca_Click" />
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" 
                        CssClass="messaggiErrore" ValidationGroup="ricercaImpresa" />
                </td>
                <td>
                    <asp:Button ID="ButtonNuovo" runat="server" Width="150px" Text="Nuovo" 
                        Enabled="false" onclick="ButtonNuovo_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<div>
    <asp:Label ID="LabelImpresaNonTrovata" runat="server" 
        Text="Non è stata trovata nessuna impresa con la partita IVA / codice fiscale richiesto. Completare i campi sottostanti." 
        Visible="False" CssClass="messaggiErrore"></asp:Label>
</div>
<div>
    <asp:Panel ID="PanelDatiImprese" runat="server" Enabled="false">
        <uc1:Impresa ID="Impresa1" runat="server" />
    </asp:Panel>
</div>
