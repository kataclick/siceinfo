﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Committente.ascx.cs" Inherits="WebControls_Ricerca_Committente" %>
<%@ Register src="../Visualizzazione/Committente.ascx" tagname="Committente" tagprefix="uc1" %>

<div>
    <asp:Panel ID="PanelFiltriRicerca" runat="server" DefaultButton="ButtonRicerca">
        <table id="tabellaFiltriRicerca" runat="server" width="100%">
            <tr>
                <td>
                    Partita IVA /Codice fiscale:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px" MaxLength="16"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                        ErrorMessage="Inserire un codice fiscale" ValidationGroup="ricercaCommittente">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                        ErrorMessage="Codice fiscale non corretto" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                        ValidationGroup="ricercaCommittente">*</asp:RegularExpressionValidator>
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="ButtonRicerca" runat="server" Width="150px" Text="Ricerca" 
                        ValidationGroup="ricercaCommittente" onclick="ButtonRicerca_Click" />
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" CssClass="messaggiErrore" />
                </td>
                <td>
                    <asp:Button ID="ButtonNuovo" runat="server" Width="150px" Text="Nuovo" 
                        Enabled="false" onclick="ButtonNuovo_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
<div>
    <asp:Label ID="LabelCommittenteNonTrovato" runat="server" 
        Text="Non è stato trovato nessun committente con la partita IVA / codice fiscale richiesto. Completare i campi sottostanti." 
        Visible="False" CssClass="messaggiErrore"></asp:Label>
</div>
<div>
    <asp:Panel ID="PanelDatiCommittente" runat="server" Enabled="false">
        <uc1:Committente ID="Committente1" runat="server" />
    </asp:Panel>
</div>
