using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Type.Filters;
using System.Web.Security;
using System.Drawing;

public partial class WebControls_Ricerca_Utente : System.Web.UI.UserControl
{
    private String[] ruoli = null;
    private MembershipUser userTemp = null;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        CaricaUtenti(0);
    }

    private void CaricaUtenti(Int32 pagina)
    {
        UtenteNotificheTelematicheFilter filtro = CreaFiltro();
        MembershipUserCollection utenti = Membership.GetAllUsers();
        IEnumerable<MembershipUser> risultati = utenti.Cast<MembershipUser>();

        // Filtraggio degli utenti (LINQ)
        var utentiFiltrati = from utente in risultati
                             where ((String.IsNullOrEmpty(filtro.TipoUtenti) 
                                        || (filtro.TipoUtenti == "ABILITATI" && utente.IsApproved) 
                                        || (filtro.TipoUtenti == "NONABILITATI" && !utente.IsApproved))
                                    &&
                                    (String.IsNullOrEmpty(filtro.Login)
                                        || (filtro.Login == utente.UserName.ToUpper())))
                             select utente;
        
        Presenter.CaricaElementiInGridView(
            GridViewUtenti,
            utentiFiltrati,
            pagina);
    }

    private UtenteNotificheTelematicheFilter CreaFiltro()
    {
        UtenteNotificheTelematicheFilter filtro = new UtenteNotificheTelematicheFilter();

        filtro.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
        filtro.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
        filtro.Login = Presenter.NormalizzaCampoTesto(TextBoxNomeUtente.Text);
        filtro.TipoUtenti = DropDownListPosizione.SelectedValue;

        return filtro;
    }

    protected void GridViewUtenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        String userName = GridViewUtenti.DataKeys[e.NewSelectedIndex].Values["UserName"] as String;

        if (!String.IsNullOrEmpty(userName))
        {
            MembershipUser user = Membership.GetUser(userName);
            user.IsApproved = !user.IsApproved;
            
            Membership.UpdateUser(user);
            CaricaUtenti(GridViewUtenti.PageIndex);
        }
    }

    protected void GridViewUtenti_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            MembershipUser user = (MembershipUser)e.Row.DataItem;
            GridView gvRuoli = (GridView)e.Row.FindControl("GridViewRuoli");
            CheckBox cbAbilitato = (CheckBox)e.Row.FindControl("CheckBoxAbilitato");

            cbAbilitato.Checked = user.IsApproved;
            if (!user.IsApproved)
            {
                e.Row.ForeColor = Color.Gray;
            }

            userTemp = user;

            Presenter.CaricaElementiInGridView(
                gvRuoli,
                ruoli,
                0);
        }
    }

    protected void GridViewUtenti_DataBinding(object sender, EventArgs e)
    {
        ruoli = Roles.GetAllRoles();
    }

    protected void GridViewRuoli_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbRuolo = (CheckBox)e.Row.FindControl("CheckBoxRuolo");

            String ruolo = (String)e.Row.DataItem;
            cbRuolo.Text = ruolo;

            if (Roles.IsUserInRole(userTemp.UserName, ruolo))
            {
                cbRuolo.Checked = true;
            }
        }
    }

    protected void GridViewUtenti_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        String userName = GridViewUtenti.DataKeys[e.RowIndex].Values["UserName"] as String;

        if (!String.IsNullOrEmpty(userName))
        {
            GridViewRow row = GridViewUtenti.Rows[e.RowIndex];
            GridView gvRuoli = (GridView)row.FindControl("GridViewRuoli");

            foreach (GridViewRow rowRuolo in gvRuoli.Rows)
            {
                CheckBox cbRuolo = (CheckBox)rowRuolo.FindControl("CheckBoxRuolo");
                if (cbRuolo.Checked)
                {
                    if (!Roles.IsUserInRole(userName, cbRuolo.Text))
                    {
                        Roles.AddUserToRole(userName, cbRuolo.Text);
                    }
                }
                else
                {
                    if (Roles.IsUserInRole(userName, cbRuolo.Text))
                    {
                        Roles.RemoveUserFromRole(userName, cbRuolo.Text);
                    }
                }
            }

            CaricaUtenti(GridViewUtenti.PageIndex);
        }
    }

    protected void GridViewUtenti_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Guid userID = (Guid)GridViewUtenti.DataKeys[e.NewEditIndex].Values["ProviderUserKey"];
        Context.Items["userID"] = userID;

        Server.Transfer("~/Amministrazione/DettagliUtente.aspx");
    }
}
