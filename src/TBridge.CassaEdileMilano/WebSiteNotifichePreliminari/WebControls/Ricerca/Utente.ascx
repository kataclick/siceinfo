﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Utente.ascx.cs" Inherits="WebControls_Ricerca_Utente" %>

<div>
    <table width="100%">
        <tr>
            <td style="width:25%">
                Cognome
            </td>
            <td style="width:25%">
                Nome
            </td>
            <td style="width:25%">
                Nome Utente
            </td>
            <td style="width:25%">
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox 
                    ID="TextBoxCognome" 
                    runat="server" 
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxNome" 
                    runat="server"
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxNomeUtente" 
                    runat="server"
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Posizione
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DropDownList
                    ID="DropDownListPosizione"
                    runat="server"
                    Width="100%">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem Value="ABILITATI">Abilitati all&#39;accesso</asp:ListItem>
                    <asp:ListItem Value="NONABILITATI">Non abilitati all&#39;accesso</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricercaPerAggiornamento"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="100%" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricercaPerAggiornamento" />
            </td>
        </tr>
    </table>
</div>
<br />
<div>
    <asp:GridView 
        ID="GridViewUtenti" 
        runat="server" 
        Width="100%" AutoGenerateColumns="False" DataKeyNames="UserName,ProviderUserKey" 
        onselectedindexchanging="GridViewUtenti_SelectedIndexChanging" 
        ondatabinding="GridViewUtenti_DataBinding" 
        onrowdatabound="GridViewUtenti_RowDataBound" 
        onrowdeleting="GridViewUtenti_RowDeleting" 
        onrowediting="GridViewUtenti_RowEditing">
        <Columns>
            <asp:BoundField DataField="UserName" HeaderText="Nome Utente">
            <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="Email" HeaderText="Email" />
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Dettagli" ShowEditButton="True">
            <ItemStyle Width="10px" />
            </asp:CommandField>
            <asp:TemplateField HeaderText="Ruoli">
                <ItemTemplate>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:GridView ID="GridViewRuoli" runat="server" AutoGenerateColumns="False" 
                                    onrowdatabound="GridViewRuoli_RowDataBound" ShowHeader="False" Width="120px" GridLines="None" BorderStyle="None">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxRuolo" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td>
                                <asp:Button
                                    ID="ButtonAggiornaRuoli"
                                    runat="server"
                                    Text="Agg. ruoli"
                                    CommandName="Delete"
                                    Width="80px" />
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="220px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Abilitazione">
                <ItemTemplate>
                    <asp:CheckBox
                        ID="CheckBoxAbilitato"
                        runat="server"
                        Enabled="false" />
                    <asp:Button
                        ID="ButtonAbilitaDisabilita"
                        runat="server"
                        CommandName="Select"
                        Text="Abil./Disabil."
                        Width="80px" />
                </ItemTemplate>
                <ItemStyle Width="120px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>