﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Notifica.ascx.cs" Inherits="WebControls_Ricerca_Notifica" %>
<div>
    <table width="100%">
        <tr>
            <td style="width:25%">
                N°protocollo
                <asp:RangeValidator 
                    ID="RangeValidatorIdNotifica"
                    runat="server"
                    ControlToValidate="TextBoxProtocollo"
                    Type="Integer"
                    MinimumValue="1"
                    MaximumValue="2000000000"
                    ErrorMessage="Formato n° protocollo errato"
                    ValidationGroup="ricercaPerAggiornamento">
                    *
                </asp:RangeValidator>
            </td>
            <td style="width:25%">
                Data inserimento (gg/mm/aaaa)
                <asp:CompareValidator
                    ID="CompareValidatorDataInserimento"
                    runat="server"
                    ControlToValidate="TextBoxDataInserimento"
                    Type="Date"
                    Operator="DataTypeCheck"
                    ErrorMessage="Formato data inserimento errato"
                    ValidationGroup="ricercaPerAggiornamento">
                    *
                </asp:CompareValidator>
            </td>
            <td style="width:25%">
            </td>
            <td style="width:25%">
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox 
                    ID="TextBoxProtocollo" 
                    runat="server" 
                    MaxLength="10"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxDataInserimento" 
                    runat="server"
                    MaxLength="10"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Natura opera
            </td>
            <td>
                Indirizzo
            </td>
            <td>
                Comune
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox 
                    ID="TextBoxNaturaOpera" 
                    runat="server"
                    MaxLength="500"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxIndirizzo" 
                    runat="server"
                    MaxLength="255" 
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxComune" 
                    runat="server" 
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricercaPerAggiornamento"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="100%" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricercaPerAggiornamento" />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Notifiche trovate" Visible="False"></asp:Label>
</div>
<br />
<div>
    <asp:GridView ID="GridViewNotifiche" runat="server" Width="100%" 
        AutoGenerateColumns="False" 
        onrowdatabound="GridViewNotifiche_RowDataBound" AllowPaging="True" 
        DataKeyNames="IdNotifica,IdNotificaRiferimento" 
        onpageindexchanging="GridViewNotifiche_PageIndexChanging" 
        onselectedindexchanging="GridViewNotifiche_SelectedIndexChanging" 
        PageSize="5" onrowdeleting="GridViewNotifiche_RowDeleting" 
        onrowediting="GridViewNotifiche_RowEditing">
        <Columns>
            <asp:TemplateField HeaderText="Tipo">
                <ItemTemplate>
                    <asp:Label 
                        ID="LabelTipoNotifica" 
                        runat="server">
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="80px" />
            </asp:TemplateField>
            <asp:BoundField HeaderText="Protocollo" DataField="IdNotifica" >
            <ItemStyle Width="60px" />
            </asp:BoundField>
            <asp:BoundField DataField="IdNotificaPadre" HeaderText="Prot. rif.">
            <ItemStyle Width="60px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Data ins." DataField="DataInserimento" 
                DataFormatString="{0:dd/MM/yyyy}">
            <ItemStyle Width="70px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Dati">
                <ItemTemplate>
                    <b>
                        <asp:Label
                            ID="LabelNaturaOpera"
                            runat="server">
                        </asp:Label>
                    </b>
                    <br />
                    <br />
                    <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" 
                            ShowHeader="False" Width="100%" DataKeyNames="IdIndirizzo" 
                            onrowdatabound="GridViewIndirizzi_RowDataBound" 
                            onselectedindexchanging="GridViewIndirizzi_SelectedIndexChanging" 
                        BorderStyle="None" BorderWidth="0px" GridLines="None">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ImageButtonMappa" runat="server" CommandName="Select" 
                                            ImageUrl="~/Images/localizzazione24.png" ToolTip="Visualizza su Mappa" />
                                    </ItemTemplate>
                                    <ItemStyle Width="24px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
                            </Columns>
                            <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectText="Seleziona" 
                ShowSelectButton="True" SelectImageUrl="~/Images/dettagliNotifica32.png">
            <ItemStyle Width="10px" />
            </asp:CommandField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Ricevuta" 
                ShowDeleteButton="True" DeleteImageUrl="~/Images/report32.png">
            <ItemStyle Width="10px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna notifica trovata
        </EmptyDataTemplate>
    </asp:GridView>
</div>