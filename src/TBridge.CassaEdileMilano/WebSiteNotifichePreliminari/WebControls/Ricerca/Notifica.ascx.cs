using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Presenter;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Entities;
using System.Drawing;
using TBridge.Cemi.Cpt.Type.Delegates;
using TBridge.Cemi.Cpt.Type.Collections;
using System.Web.Security;

public partial class WebControls_Ricerca_Notifica : System.Web.UI.UserControl
{
    public event NotificaSelectedEventHandler OnNotificaSelected;

    private readonly CptBusiness biz = new CptBusiness();

    private const Int32 INDICEBOTTONEMAPPA = 0;
    private const Int32 INDICEBOTTONESELEZIONA = 5;
    private const String IMMAGINEAGGIORNAMENTO = "~/Images/aggiornaNotifica32.png";
    private const String IMMAGINELOCALIZZAZIONE = "~/Images/localizzazioneGrigia24.png";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ModalitaAggiornamento()
    {
        CommandField bSeleziona = (CommandField)GridViewNotifiche.Columns[INDICEBOTTONESELEZIONA];
        bSeleziona.SelectImageUrl = IMMAGINEAGGIORNAMENTO;
    }

    protected void ButtonRicerca_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            CaricaNotifiche(0);
        }
    }

    private void CaricaNotifiche(int numeroPagina)
    {
        NotificaTelematicaAggiornamentoFilter filtro = CreaFiltro();

        NotificaCollection notifiche = biz.RicercaNotificheTelematichePerAggiornamento(filtro);
        
        LabelCantieriTrovati.Visible = true;
        LabelCantieriTrovati.Text = String.Format("Notifiche trovate: {0}", notifiche.Count);

        Presenter.CaricaElementiInGridView(
            GridViewNotifiche,
            notifiche,
            numeroPagina);
    }

    private NotificaTelematicaAggiornamentoFilter CreaFiltro()
    {
        NotificaTelematicaAggiornamentoFilter filtro = new NotificaTelematicaAggiornamentoFilter();

        if (!String.IsNullOrEmpty(TextBoxProtocollo.Text))
        {
            filtro.IdNotifica = Int32.Parse(TextBoxProtocollo.Text);
        }
        if (!String.IsNullOrEmpty(TextBoxDataInserimento.Text))
        {
            filtro.DataInserimento = DateTime.Parse(TextBoxDataInserimento.Text);
        }
        filtro.NaturaOpera = Presenter.NormalizzaCampoTesto(TextBoxNaturaOpera.Text);
        filtro.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
        filtro.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);

        filtro.IdUtenteTelematiche = (Guid)Membership.GetUser().ProviderUserKey;

        return filtro;
    }

    protected void GridViewNotifiche_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Notifica notifica = (Notifica)e.Row.DataItem;
            GridView gvIndirizzi = (GridView)e.Row.FindControl("GridViewIndirizzi");
            Label lTipoNotifica = (Label)e.Row.FindControl("LabelTipoNotifica");
            Label lNaturaOpera = (Label)e.Row.FindControl("LabelNaturaOpera");

            lNaturaOpera.Text = notifica.NaturaOpera;

            if (notifica.IdNotificaPadre.HasValue)
            {
                lTipoNotifica.Text = "Aggiornamento";
                e.Row.ForeColor = Color.Gray;
            }
            else
            {
                lTipoNotifica.Text = "Notifica preliminare";
            }

            Presenter.CaricaElementiInGridView(
                gvIndirizzi,
                notifica.Indirizzi,
                0);
        }
    }

    protected void GridViewNotifiche_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaricaNotifiche(e.NewPageIndex);
    }

    protected void GridViewNotifiche_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        Int32 idNotifica = (Int32)GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotifica"];
        Int32 idNotificaRiferimento = (Int32)GridViewNotifiche.DataKeys[e.NewSelectedIndex].Values["IdNotificaRiferimento"];

        if (OnNotificaSelected != null)
        {
            OnNotificaSelected(idNotifica, idNotificaRiferimento);
        }
    }

    protected void GridViewNotifiche_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Int32 idNotifica = (Int32)GridViewNotifiche.DataKeys[e.RowIndex].Values["IdNotifica"];

        Context.Items["IdNotifica"] = idNotifica;
        Server.Transfer("~/Inserimento/RicevutaNotifica.aspx");
    }

    protected void GridViewNotifiche_RowEditing(object sender, GridViewEditEventArgs e)
    {
        
    }

    protected void GridViewIndirizzi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Indirizzo indirizzo = (Indirizzo)e.Row.DataItem;
            ImageButton ibMappa = (ImageButton)e.Row.FindControl("ImageButtonMappa");

            if (!indirizzo.HaCoordinate())
            {
                ibMappa.ImageUrl = IMMAGINELOCALIZZAZIONE;
                e.Row.Cells[INDICEBOTTONEMAPPA].Enabled = false;
            }
        }
    }

    protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        GridView gvIndirizzi = (GridView)sender;
        Int32 idIndirizzo = (Int32)gvIndirizzi.DataKeys[e.NewSelectedIndex].Values["IdIndirizzo"];

        Context.Items["IdIndirizzo"] = idIndirizzo;
        Server.Transfer("~/Inserimento/LocalizzaIndirizzo.aspx");
    }
}
