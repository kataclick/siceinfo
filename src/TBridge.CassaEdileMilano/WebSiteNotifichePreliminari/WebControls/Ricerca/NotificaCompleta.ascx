﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NotificaCompleta.ascx.cs" Inherits="WebControls_Ricerca_NotificaCompleta" %>
<style type="text/css">
    .style1
    {
        width: 140px;
    }
</style>
<div>
    <table width="100%">
        <tr>
            <td style="width:25%">
                N°protocollo
                <asp:RangeValidator 
                    ID="RangeValidatorIdNotifica"
                    runat="server"
                    ControlToValidate="TextBoxProtocollo"
                    Type="Integer"
                    MinimumValue="1"
                    MaximumValue="2000000000"
                    ErrorMessage="Formato n° protocollo errato"
                    ValidationGroup="ricercaPerAggiornamento">
                    *
                </asp:RangeValidator>
            </td>
            <td style="width:25%">
                Data inserimento (gg/mm/aaaa)
                <asp:CompareValidator
                    ID="CompareValidatorDataInserimento"
                    runat="server"
                    ControlToValidate="TextBoxDataInserimento"
                    Type="Date"
                    Operator="DataTypeCheck"
                    ErrorMessage="Formato data inserimento errato"
                    ValidationGroup="ricercaPerAggiornamento">
                    *
                </asp:CompareValidator>
            </td>
            <td style="width:25%">
            </td>
            <td style="width:25%">
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox 
                    ID="TextBoxProtocollo" 
                    runat="server" 
                    MaxLength="10"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxDataInserimento" 
                    runat="server"
                    MaxLength="10"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Natura opera
            </td>
            <td>
                Indirizzo
            </td>
            <td>
                Comune
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox 
                    ID="TextBoxNaturaOpera" 
                    runat="server"
                    MaxLength="500"
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxIndirizzo" 
                    runat="server"
                    MaxLength="255" 
                    Width="100%">
                </asp:TextBox>
            </td>
            <td>
                <asp:TextBox 
                    ID="TextBoxComune" 
                    runat="server" 
                    MaxLength="50"
                    Width="100%">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                <asp:ValidationSummary
                    ID="ValidationSummaryErroriRicerca"                    
                    runat="server"
                    ValidationGroup="ricercaPerAggiornamento"
                    CssClass="messaggiErrore" />
            </td>
            <td valign="top">
                <asp:Button 
                    ID="ButtonRicerca" 
                    runat="server" 
                    Width="100%" 
                    Text="Ricerca" 
                    onclick="ButtonRicerca_Click" 
                    ValidationGroup="ricercaPerAggiornamento" />
            </td>
        </tr>
    </table>
    <asp:Label ID="LabelCantieriTrovati" runat="server" Text="Notifiche trovate" Visible="False"></asp:Label>
</div>
<br />
<div>
    <asp:GridView 
        ID="GridViewNotifiche"
        runat="server"  
        AutoGenerateColumns="False" 
        ShowHeader="False" 
        Width="100%" 
        OnRowDataBound="GridViewNotifiche_RowDataBound" 
        DataKeyNames="IdNotifica,IdNotificaRiferimento" 
        AllowPaging="True" 
        OnPageIndexChanging="GridViewNotifiche_PageIndexChanging" 
        PageSize="5" BorderColor="#0D859C" BorderStyle="Solid" BorderWidth="1.5pt" 
        GridLines="Horizontal">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td class="style1">
                                        Notifica prelim.
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelProtocolloPreliminare" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Data:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelData" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Ultimo agg.
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelProtocolloUltimo" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Data ultimo agg.
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelDataUltimoAggiornamento" runat="server" Font-Bold="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Natura opera:
                                    </td>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelNaturaOpera" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Numero appalto:</td>
                                    <td>
                                        <asp:Label ID="LabelNumeroAppalto" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Committente:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        Indirizzi:
                                    </td>
                                    <td>
                                        <asp:GridView ID="GridViewIndirizzi" runat="server" AutoGenerateColumns="False" 
                                                ShowHeader="False" Width="100%" DataKeyNames="IdIndirizzo" 
                                                onrowdatabound="GridViewIndirizzi_RowDataBound" 
                                                onselectedindexchanging="GridViewIndirizzi_SelectedIndexChanging" 
                                                BorderStyle="None" BorderWidth="0px" GridLines="None">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButtonMappa" runat="server" CommandName="Select" 
                                                            ImageUrl="~/Images/localizzazione24.png" ToolTip="Visualizza la Mappa" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="24px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IndirizzoCompleto" HeaderText="Indirizzo" />
                                            </Columns>
                                            <AlternatingRowStyle BorderStyle="None" BorderWidth="0px" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="60%" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelAgg" runat="server" Font-Bold="True">Storia</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridViewAggiornamenti" runat="server" 
                                            AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdNotifica" 
                                            OnSelectedIndexChanging="GridViewAggiornamenti_SelectedIndexChanging" 
                                            Width="100%" OnRowDataBound="GridViewAggiornamenti_RowDataBound" 
                                            BorderColor="#0D859C" BorderStyle="Solid" 
                                            OnRowDeleting="GridViewAggiornamenti_RowDeleting" BorderWidth="1.5pt" 
                                            GridLines="Horizontal">
                                            <Columns>
                                                <asp:BoundField HeaderText="Protocollo" DataField="IdNotifica" >
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Data" DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" >
                                                </asp:BoundField>
                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectText="Dettagli" 
                                                    ShowSelectButton="True" SelectImageUrl="~/Images/dettagliNotifica32.png" >
                                                    <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Documento" 
                                                    ShowDeleteButton="True" DeleteImageUrl="~/Images/report32.png" >
                                                    <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="200px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna notifica trovata
                </EmptyDataTemplate>
            </asp:GridView>
</div>