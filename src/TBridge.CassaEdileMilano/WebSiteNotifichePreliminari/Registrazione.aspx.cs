using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Cpt.Type.Entities;
using System.Web.Security;
using TBridge.Cemi.Cpt.Business;

public partial class Registrazione : System.Web.UI.Page
{
    private readonly CptBusiness biz = new CptBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CaricaTitolo();
            LabelTotalePassi.Text = CreateUserWizard1.WizardSteps.Count.ToString();
        }
    }

    #region Metodi per il caricamento dei messaggi di Aiuto
    private void CaricaStringaTitolo(String titolo)
    {
        MasterPage master = (MasterPage)this.Master;
        master.CaricaTitolo(titolo);
    }

    private void CaricaTitolo()
    {
        // Carico la stringa di aiuto in base alla sezione corrente
        CaricaStringaTitolo(
            "Registrazione");
    }
    #endregion

    protected void CheckBoxUtenteCommittente_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBoxUtenteCommittente.Checked)
        {
            PanelCommittente.Enabled = true;
            Committente1.ImpostaValidationGroup(String.Empty);
        }
        else
        {
            PanelCommittente.Enabled = false;
            Committente1.ImpostaValidationGroup("stop");
        }
    }

    protected void CreateUserWizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        LabelPasso.Text = (CreateUserWizard1.ActiveStepIndex + 1).ToString();
    }

    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        String userName = ((CreateUserWizard)sender).UserName;
        MembershipUser nuovoUtente = Membership.GetUser(userName);

        UtenteNotificheTelematiche utente = Utente1.GetUtente();
        
        if (CheckBoxUtenteCommittente.Checked)
        {
            utente.CommittenteTelematiche = Committente1.CreaCommittente();
        }

        utente.IdUtente = (Guid)nuovoUtente.ProviderUserKey;
        try
        {
            if (!biz.InsertUtenteTelematiche(utente))
            {
                Membership.DeleteUser(userName);
            }
            else
            {
                Label lLogin = (Label)stepComplete.ContentTemplateContainer.FindControl("LabelLogin");
                Label lPassword = (Label)stepComplete.ContentTemplateContainer.FindControl("LabelPassword");

                lLogin.Text = nuovoUtente.UserName;
                lPassword.Text = nuovoUtente.GetPassword();
            }
        }
        catch 
        {
            if (Membership.GetUser(userName) != null)
            {
                Membership.DeleteUser(userName);
            }
        }
    }

    protected void CreateUserWizard1_CreateUserError(object sender, CreateUserErrorEventArgs e)
    {
        Int32 i = 0;
    }
}
