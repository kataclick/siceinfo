using TBridge.Cemi.Deleghe.Type.Entities;

namespace TBridge.Cemi.Deleghe.Type.Delegates
{
    public delegate void LavoratoriSelectedEventHandler(Lavoratore lavoratore);

    public delegate void DelegaSelectedEventHandler(Delega delega);

    public delegate void DelegaSearchedEventHandler();
}