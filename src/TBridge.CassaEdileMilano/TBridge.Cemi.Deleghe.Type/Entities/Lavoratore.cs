using System;

namespace TBridge.Cemi.Deleghe.Type.Entities
{
    public class Lavoratore
    {
        public Lavoratore()
        {
        }

        public Lavoratore(int? idlavoratore, string cognome, string nome, DateTime? dataNascita,
                          string codiceFiscale, string impresa, Indirizzo residenza)
        {
            IdLavoratore = idlavoratore;
            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            CodiceFiscale = codiceFiscale;
            Impresa = impresa;
            Residenza = residenza;
        }

        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public string Cellulare { get; set; }

        public int? IdImpresa { get; set; }

        public string Impresa { get; set; }

        public Indirizzo Residenza { get; set; }

        public string ResidenzaCompleto
        {
            get
            {
                if (Residenza != null) return Residenza.IndirizzoCompleto;
                else return string.Empty;
            }
        }
    }
}