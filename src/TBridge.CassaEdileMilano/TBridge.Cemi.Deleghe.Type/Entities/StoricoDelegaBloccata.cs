﻿using System;

namespace TBridge.Cemi.Deleghe.Type.Entities
{
    public class StoricoDelegaBloccata
    {
        public Int32 IdDelega { get; set; }
        public DateTime DataVariazione { get; set; }
        public String Cognome { get; set; }
        public String Nome { get; set; }
        public DateTime DataNascita { get; set; }
        public String Utente { get; set; }
    }
}