using System;
using TBridge.Cemi.Deleghe.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Deleghe.Type.Entities
{
    [Serializable]
    public class Delega
    {
        public Delega()
        {
            Lavoratore = new Lavoratore();
            Lavoratore.Residenza = new Indirizzo();
            Cantiere = new Indirizzo();
        }

        public Delega(Lavoratore lavoratore, Indirizzo cantiere, Sindacalista responsabileInserimento,
                      string operatoreTerritorio, Sindacato sindacato, ComprensorioSindacale comprensorio)
        {
            Lavoratore = lavoratore;
            Cantiere = cantiere;
            OperatoreInserimento = responsabileInserimento;
            OperatoreTerritorio = operatoreTerritorio;
            Sindacato = sindacato;
            ComprensorioSindacale = comprensorio;
        }

        public int? IdDelega { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public string NomeLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return String.Format("{0} {1}", Lavoratore.Cognome, Lavoratore.Nome);
                else
                    return string.Empty;
            }
        }

        public string LavoratoreCognome
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Cognome;
                else
                    return string.Empty;
            }
        }

        public string LavoratoreNome
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Nome;
                else
                    return string.Empty;
            }
        }

        public string LavoratoreCellulare
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Cellulare;
                else
                    return string.Empty;
            }
        }

        public string LavoratoreId
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.IdLavoratore.ToString();
                else
                    return string.Empty;
            }
        }

        public string DataNascitaLavoratore
        {
            get
            {
                if (Lavoratore != null && Lavoratore.DataNascita.HasValue)
                    return Lavoratore.DataNascita.Value.ToShortDateString();
                else
                    return string.Empty;
            }
        }

        public string IdImpresaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.IdImpresa.ToString();
                else
                    return string.Empty;
            }
        }

        public string ImpresaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Impresa;
                else
                    return string.Empty;
            }
        }

        public string ResidenzaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.ResidenzaCompleto;
                else
                    return string.Empty;
            }
        }

        public string IndirizzoDenominazioneLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.IndirizzoDenominazione;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string ComuneLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Comune;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string CapLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Cap;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string ProvinciaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Provincia;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string CivicoLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Civico;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public Indirizzo Cantiere { get; set; }

        public string IndirizzoCantiere
        {
            get
            {
                if (Cantiere != null)
                    return Cantiere.IndirizzoCompleto;
                else
                    return string.Empty;
            }
        }

        public Sindacalista OperatoreInserimento { get; set; }

        public string OperatoreTerritorio { get; set; }

        public ComprensorioSindacale ComprensorioSindacale { get; set; }

        public string ComprensorioStringa
        {
            get
            {
                if (ComprensorioSindacale != null)
                    return ComprensorioSindacale.Descrizione;
                else
                    return string.Empty;
            }
        }

        public Sindacato Sindacato { get; set; }

        public StatoDelega? Stato { get; set; }

        public DateTime? DataConferma { get; set; }

        public string CodiceSblocco { get; set; }

        public DateTime? DataAdesione { get; set; }

        public string IdArchidoc { get; set; }

        public DateTime DataInserimento { get; set; }

        public string NomeAllegatoLettera { get; set; }

        public string NomeAllegatoBusta { get; set; }

        public Boolean Stampata { get; set; }

        public String AnagraficaResidenzaIndirizzo { get; set; }

        public String AnagraficaResidenzaComune { get; set; }

        public String AnagraficaResidenzaProvincia { get; set; }

        public String AnagraficaResidenzaCap { get; set; }

        public String AnagraficaResidenzaCivico { get; set; }
    }
}