using System;

namespace TBridge.Cemi.Deleghe.Type.Entities
{
    public class Indirizzo : Cemi.Type.Entities.Indirizzo
    {
        public string IndirizzoDenominazione
        {
            get
            {
                return Via;
            }
            set
            {
                NomeVia = value;
            }
        }

        public new string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(Provincia))
                    return String.Format("{0} {1} {2} ({3}) {4}", IndirizzoDenominazione, Civico, Comune, Provincia, Cap);
                else return String.Format("{0} {1} {2} {3}", IndirizzoDenominazione, Civico, Comune, Cap);
            }
        }
    }
}