namespace TBridge.Cemi.Deleghe.Type.Enums
{
    public enum StatoDelega
    {
        InAttesa = 0,
        RicevutaCE,
        NonCorretta,
        NoCartaceo,
        Confermata,
        Rifiutata,
        Bloccata, //se torna da sicenew come creata
        Sospesa,
        Scaduta,
        Cessata, //dopo 5 anni
        Annullata //per deleghe che non dovevano neppure arrivare (ad es. quando vi sono gi� deleghe sospese)
    }
}