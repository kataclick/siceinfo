using System;
using TBridge.Cemi.Deleghe.Type.Enums;

namespace TBridge.Cemi.Deleghe.Type.Filters
{
    public class DelegheFilter
    {
        public DelegheFilter()
        {
            DataNascita = null;
            MeseConferma = null;
            DalMeseConferma = null;
            AlMeseConferma = null;
            Stato = null;
            MeseModificaStato = null;
        }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string ComprensorioSindacale { get; set; }

        public string Sindacato { get; set; }

        public bool Confermate { get; set; }

        public DateTime? MeseConferma { get; set; }

        public DateTime? DalMeseConferma { get; set; }

        public DateTime? AlMeseConferma { get; set; }

        public StatoDelega? Stato { get; set; }

        public string OperatoreTerritorio { get; set; }

        public DateTime? MeseModificaStato { get; set; }
    }
}