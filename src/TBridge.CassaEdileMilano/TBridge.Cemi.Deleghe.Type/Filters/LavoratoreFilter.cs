using System;

namespace TBridge.Cemi.Deleghe.Type.Filters
{
    public class LavoratoreFilter
    {
        public LavoratoreFilter()
        {
            EscludiDeceduti = true;
        }

        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        /// <summary>
        /// Se true i lavoratori vengono filtrati in modo che non compaiano i deceduti. per default � impostato a true.
        /// </summary>
        public bool? EscludiDeceduti { get; set; }
    }
}