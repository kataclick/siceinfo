using System;

namespace TBridge.Cemi.Deleghe.Type
{
    public class LetteraParam
    {
        public LetteraParam()
        {
            DataAdesione = null;
            DataModificaStato = null;
        }

        public string Protocollo { get; set; }

        public string Sindacato { get; set; }

        public string ComprensorioSindacale { get; set; }

        public DateTime? DataAdesione { get; set; }

        public DateTime? DataModificaStato { get; set; }
    }
}