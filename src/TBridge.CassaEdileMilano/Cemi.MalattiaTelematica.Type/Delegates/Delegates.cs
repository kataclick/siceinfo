﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.MalattiaTelematica.Type.Filters;
using TBridge.Cemi.Type.Domain;

namespace Cemi.MalattiaTelematica.Type.Delegates
{

    public delegate void AssenzeFilterSelectedEventHandler(AssenzeFilter filtro);


    public delegate void CertificatiSelectedEventHandler(Int32 id);

    public delegate void AltriDocumentiSelectedEventHandler();


    public delegate void CertificatiReturnedEventHandler(Boolean carica);


    public delegate void AltriDocumentiReturnedEventHandler(Boolean carica);
}
