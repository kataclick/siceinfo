﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class GiustificativiUpload
    {
        public Int32? IdImpresa { get; set; }
        public String RagioneSociale { get; set; }
        public String PartitaIVA { get; set; }
        public String CodiceINPS { get; set; }

        public Int32? IdLavoratore { get; set; }
        public String Nome { get; set; }
        public String Cognome { get; set; }
        public String CodiceFiscale { get; set; }
        public DateTime DataNascita { get; set; }

        public String Tipo { get; set; }
        public String Numero { get; set; }
        public DateTime DataRilascio { get; set; }
        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }

        public Byte[] Documento { get; set; }
        public String NomeDocumento { get; set; }
    }
}
