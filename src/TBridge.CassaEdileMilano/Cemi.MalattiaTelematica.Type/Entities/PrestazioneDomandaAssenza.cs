﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class PrestazioneDomandaAssenza
    {
        private Int32 idImpresa;
        private String ragioneSociale;
        private String codiceFiscale;
        private String partitaIVA;
        private String tipoProtocollo;
        private Int32? annoProtocollo;
        private Int32? numeroProtocollo;
        private DateTime? data;
        private Decimal importo;
        private Int32 numero;

        private Int32 idAssenza;

        public PrestazioneDomandaAssenza()
        {
        }

        public PrestazioneDomandaAssenza(
            Int32 idImpresa,
            String ragioneSociale,
            String codiceFiscale,
            String partitaIVA,
            String tipoProtocollo,
            Int32? annoProtocollo,
            Int32? numeroProtocollo,
            DateTime? data,
            Decimal importo,
            Int32 numero,
            Int32 idAssenza)
        {
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.codiceFiscale = codiceFiscale;
            this.partitaIVA = partitaIVA;
            this.tipoProtocollo = tipoProtocollo;
            this.annoProtocollo = annoProtocollo;
            this.numeroProtocollo = numeroProtocollo;
            this.data = data;
            this.importo = importo;
            this.numero = numero;
            this.idAssenza = idAssenza;

        }
            
            
        public Int32 IdImpresa { get { return idImpresa; } set { idImpresa = value; } }

        public String RagioneSociale { get { return ragioneSociale; } set { ragioneSociale = value; } }

        public String CodiceFiscale { get { return codiceFiscale; } set { codiceFiscale = value; } }

        public String PartitaIVA { get { return partitaIVA; } set { partitaIVA = value; } }

        public String TipoProtocollo { get { return tipoProtocollo; } set { tipoProtocollo = value; } }

        public Int32? AnnoProtocollo { get { return annoProtocollo; } set { annoProtocollo = value; } }

        public Int32? NumeroProtocollo { get { return numeroProtocollo; } set { numeroProtocollo = value; } }

        public DateTime? Data { get { return data; } set { data = value; } }

        public Decimal Importo { get { return importo; } set { importo = value; } }

        public Int32 Numero { get { return numero; } set { numero = value; } }
        
        public Int32 IdAssenza { get { return idAssenza; } set { idAssenza = value; } }


    }
}
