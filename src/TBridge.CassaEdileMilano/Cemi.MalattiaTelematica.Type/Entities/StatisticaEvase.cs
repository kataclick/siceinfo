﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
   

    public class StatisticaEvase
    {
        private String stato;
        private String utente;
        private Int32 numeroAssenze;



        public StatisticaEvase()
        {
        }

        public StatisticaEvase(

            String stato,
            String utente,
            Int32 numeroAssenze)
        {
            this.stato = stato;
            this.utente = utente;
            this.numeroAssenze = numeroAssenze;

        }


        public String Stato { get { return stato; } set { stato = value; } }

        public String Utente { get { return utente; } set { utente = value; } }

        public Int32 NumeroAssenze { get { return numeroAssenze; } set { numeroAssenze = value; } }



    }
}
