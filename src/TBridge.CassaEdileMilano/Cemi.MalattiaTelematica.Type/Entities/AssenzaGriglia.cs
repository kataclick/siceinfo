﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class AssenzaGriglia
    {
        public String Tipo { get; set; }    
        public DateTime DataInizioAssenza { get; set; }
        public DateTime DataFineAssenza { get; set; }
        public Int32 OrePerseDichiarate { get; set; }

        public List<GiustificativiPeriodoGriglia> GiustificativiPeriodo { get; set; }

        public List<DateTime> GiorniNonIndennizzabili { get; set; }



        public AssenzaGriglia()
        {
            this.GiustificativiPeriodo = new List<GiustificativiPeriodoGriglia>();

            this.GiorniNonIndennizzabili = new List<DateTime>();
        }
    }
}
