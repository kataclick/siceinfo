﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class CertificatoMedicoErrori : AssenzaErrori
    {
        private String numeroCertificato;
        private DateTime dataInizioCertificato;
        private DateTime dataFineCertificato;
        private DateTime dataRilascioCertificato;
       

        public CertificatoMedicoErrori()
        {
        }

        public CertificatoMedicoErrori(
            Int32 idImpresa,
            String ragioneSociale,
            Int32 idLavoratore,
            String cognome,
            String nome,
            String idCassaEdile,
            String tipoProtocollo,
            Int32 annoProtocollo,
            Int32 numeroProtocollo,
            DateTime dataInizio,
            DateTime dataFine,
            DateTime dataInizioMalattia,
            String descrizione,

            String numeroCertificato,
            DateTime dataInizioCertificato,
            DateTime dataFineCertificato,
            DateTime dataRilascioCertificato
            )
        {
            base.IdImpresa = idImpresa;
            base.RagioneSociale = ragioneSociale;
            base.IdLavoratore = idLavoratore;
            base.Cognome = cognome;
            base.Nome = nome;
            base.IdCassaEdile = idCassaEdile;
            base.TipoProtocollo = tipoProtocollo;
            base.AnnoProtocollo = annoProtocollo;
            base.NumeroProtocollo = numeroProtocollo;
            base.DataInizio = dataInizio;
            base.DataFine = dataFine;
            base.DataInizioMalattia = dataInizioMalattia;
            base.Descrizione = descrizione;

            this.numeroCertificato = numeroCertificato;
            this.dataInizioCertificato = dataInizioCertificato;
            this.dataFineCertificato = dataFineCertificato;
            this.dataRilascioCertificato = dataRilascioCertificato;
        }

        public String NumeroCertificato { get { return numeroCertificato; } set { numeroCertificato = value; } }

        public DateTime DataInizioCertificato { get { return dataInizioCertificato; } set { dataInizioCertificato = value; } }

        public DateTime DataFineCertificato { get { return dataFineCertificato; } set { dataFineCertificato = value; } }

        public DateTime DataRilascioCertificato { get { return dataRilascioCertificato; } set { dataRilascioCertificato = value; } }

    }
}

