﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
   

    public class StatisticaLiquidazione
    {
        private DateTime data;
        private Decimal importo;
        private Int32 numeroLavoratori;
        private Int32 numeroImprese;



        public StatisticaLiquidazione()
        {
        }

        public StatisticaLiquidazione(
           
            DateTime data,
            Decimal importo,
            Int32 numeroLavoratori,
            Int32 numeroImprese)
        {
            this.data = data;
            this.importo = importo;
            this.numeroLavoratori = numeroLavoratori;
            this.numeroImprese = numeroImprese;

        }


        public DateTime Data { get { return data; } set { data = value; } }

        public Decimal Importo { get { return importo; } set { importo = value; } }

        public Int32 NumeroLavoratori { get { return numeroLavoratori; } set { numeroLavoratori = value; } }

        public Int32 NumeroImprese { get { return numeroImprese; } set { numeroImprese = value; } }


    }
}
