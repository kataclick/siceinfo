﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Domain;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class AssenzaDettagli
    {
        //public AssenzaDettagli(Assenza ass, DateTime? dataAss, DateTime? dataInizioValiditaRapporto, DateTime dataFineValiditaRapporto, string descrizioneCategoria, int? partTimePercentuale)
        //{
        //    //throw new NotImplementedException();
        //    IdLavoratore = ass.IdLavoratore;
        //    IdImpresa = ass.IdImpresa;
            


        //    dataAssunzione = dataAss;
        //    dataInizio = dataInizioValiditaRapporto;
        //    dataFine = dataFineValiditaRapporto;
        //    categoria = descrizioneCategoria;
        //    percentualePT = partTimePercentuale;

        //}

        public Int32? IdLavoratore { get; set; }
        public Int32? IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String EmailImpresa { get; set; }
        public Int32 IdAssenza { get; set; }
        public String Cognome { get; set; }
        public String Nome { get; set; }
        public String CodiceFiscale { get; set; }
        public DateTime DataNascita { get; set; }
        public String Stato { get; set; }
        public String IdStato { get; set; }
        public String Tipo { get; set; }
        public String IdTipo { get; set; }
        public DateTime? DataInizioMalattia { get; set; }
        public DateTime? DataInizioAssenza { get; set; }
        public DateTime? DataFineAssenza { get; set; }
        public Boolean? Ricaduta { get; set; }

        public DateTime? DataAssunzione { get; set; }
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public String Categoria { get; set; }
        public Int32? PercentualePT { get; set; }
        public Int32? OreSettimanali { get; set; }
        public String Note { get; set; }
        public Boolean? Telefonata { get; set; }
        public Boolean? Email { get; set; }

        public DateTime? DataInvio { get; set; }
        public Int32? IdUtenteInCarico { get; set; }

        public Int32? NumeroProtocollo { get; set; }
        public Int32? AnnoProtocollo { get; set; }

        public Int32? OrePerseDich { get; set; }

        public DateTime? DataInAttesa { get; set; }
        public DateTime? DataInvioPrimoSollecito { get; set; }

        

        //public List<Cemi.MalattiaTelematica.Type.Entities.CasseEdili> CasseEdili;
        public CassaEdileCollection CasseEdili;


        public Boolean PresenteCassaEdileNellaLista(String idCassaEdile)
        {
            //foreach (Cemi.MalattiaTelematica.Type.Entities.CasseEdili casseEdili in CasseEdili)
            //{
            //    if (casseEdili.CE.IdCassaEdile == idCassaEdile)
            //    {
            //        return true;
            //    }
            //}
            return false;
        }

        public String TipoAssenza
        {
            get
            {
                String retVal = "";
                if (DataInizioMalattia.Value == DataInizioAssenza.Value)
                {
                    retVal = "Inizio";
                }
                else
                {
                    if (Ricaduta.HasValue && Ricaduta.Value)
                    {
                        retVal = "Ricaduta";
                    }
                    else
                    {
                        retVal = "Continuazione";
                    }
                }

                return retVal;
            }
        }

      
        public String NomeCompleto
        {
            get { return String.Format("{0} {1}", this.Cognome, this.Nome); }
        }

        public String RapportoLavoro
        {
            get
            {
                String ret = this.Categoria;

                if (DataInizio.HasValue)
                    ret += String.Format(" dal {0}", this.DataInizio.Value.ToShortDateString());

                if (DataFine.HasValue)
                    if (DataFine.Value != new DateTime(2079, 6, 6))
                        ret += String.Format(" al {0}", this.DataFine.Value.ToShortDateString());

                return ret;
            }
        }

        public String PartTime
        {
            get
            {
                String ret = "";

                if (PercentualePT.HasValue)
                    if (PercentualePT != 0) 
                        ret += String.Format("Part-time al {0} %", this.PercentualePT.ToString());


                return ret;
            }
        }

        public String Prestazione
        {
            get { return String.Format("{0} / {1}", this.NumeroProtocollo, this.AnnoProtocollo); }
        }
    }
}
