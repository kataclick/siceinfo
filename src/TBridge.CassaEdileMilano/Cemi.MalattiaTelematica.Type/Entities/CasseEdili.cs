﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Type.Domain;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class CasseEdili
    {
        public MalattiaTelematicaCassaEdile CE { get; set; }

        public String Descrizione { get; set; }

        public Boolean Cnce { get; set; }
    }
}
