﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class Immagine
    {
        public String IdArchidoc { get; set; }
        public Byte[] File { get; set; }
        public String NomeFile { get; set; }
    }
}
