﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class GiustificativiPeriodoGriglia
    {
        public String Tipo { get; set; }    
        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }
        public DateTime DataRilascio { get; set; }
        public Boolean? Ricovero { get; set; }
    }
}
