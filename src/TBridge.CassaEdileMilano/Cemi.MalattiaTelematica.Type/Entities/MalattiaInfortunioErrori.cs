﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class MalattiaInfortunioErrori : AssenzaErrori
    {
        private DateTime dataInizioEvento;
        private DateTime dataFineEvento;


        public MalattiaInfortunioErrori()
        {
        }

        public MalattiaInfortunioErrori(
            Int32 idImpresa,
            String ragioneSociale,
            Int32 idLavoratore,
            String cognome,
            String nome,
            String idCassaEdile,
            String tipoProtocollo,
            Int32 annoProtocollo,
            Int32 numeroProtocollo,
            DateTime dataInizio,
            DateTime dataFine,
            DateTime dataInizioMalattia,
            String descrizione,

            DateTime dataInizioEvento,
            DateTime dataFineEvento
            )
        {
            base.IdImpresa = idImpresa;
            base.RagioneSociale = ragioneSociale;
            base.IdLavoratore = idLavoratore;
            base.Cognome = cognome;
            base.Nome = nome;
            base.IdCassaEdile = idCassaEdile;
            base.TipoProtocollo = tipoProtocollo;
            base.AnnoProtocollo = annoProtocollo;
            base.NumeroProtocollo = numeroProtocollo;
            base.DataInizio = dataInizio;
            base.DataFine = dataFine;
            base.DataInizioMalattia = dataInizioMalattia;
            base.Descrizione = descrizione;

            this.dataInizioEvento = dataInizioEvento;
            this.dataFineEvento = dataFineEvento;
        }


        public DateTime DataInizioEvento { get { return dataInizioEvento; } set { dataInizioEvento = value; } }

        public DateTime DataFineEvento { get { return dataFineEvento; } set { dataFineEvento = value; } }


    }
}
