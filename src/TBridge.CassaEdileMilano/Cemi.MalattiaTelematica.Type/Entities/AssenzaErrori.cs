﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class AssenzaErrori
    {
        private Int32 idImpresa;
        private String ragioneSociale;
        private Int32 idLavoratore;
        private String cognome;
        private String nome;
        private String idCassaEdile;
        private String tipoProtocollo;
        private Int32 annoProtocollo;
        private Int32 numeroProtocollo;
        private DateTime dataInizio;
        private DateTime dataFine;
        private DateTime dataInizioMalattia;
        private String descrizione;

        public AssenzaErrori()
        {
        }

        public AssenzaErrori(
            Int32 idImpresa,
            String ragioneSociale,
            Int32 idLavoratore,
            String cognome,
            String nome,
            String idCassaEdile,
            String tipoProtocollo,
            Int32 annoProtocollo,
            Int32 numeroProtocollo,
            DateTime dataInizio,
            DateTime dataFine,
            DateTime dataInizioMalattia,
            String descrizione)
        {
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.idLavoratore = idLavoratore;
            this.cognome = cognome;
            this.nome = nome;
            this.idCassaEdile = idCassaEdile;
            this.tipoProtocollo = tipoProtocollo;
            this.annoProtocollo = annoProtocollo;
            this.numeroProtocollo = numeroProtocollo;
            this.dataInizio = dataInizio;
            this.dataFine = dataFine;
            this.dataInizioMalattia = dataInizioMalattia;
            this.descrizione = descrizione;

        }


        public Int32 IdImpresa { get { return idImpresa; } set { idImpresa = value; } }

        public String RagioneSociale { get { return ragioneSociale; } set { ragioneSociale = value; } }

        public Int32 IdLavoratore { get { return idLavoratore; } set { idLavoratore = value; } }

        public String Cognome { get { return cognome; } set { cognome = value; } }

        public String Nome { get { return nome; } set { nome = value; } }

        public String IdCassaEdile { get { return idCassaEdile; } set { idCassaEdile = value; } }

        public String TipoProtocollo { get { return tipoProtocollo; } set { tipoProtocollo = value; } }

        public Int32 AnnoProtocollo { get { return annoProtocollo; } set { annoProtocollo = value; } }

        public Int32 NumeroProtocollo { get { return numeroProtocollo; } set { numeroProtocollo = value; } }

        public DateTime DataInizio { get { return dataInizio; } set { dataInizio = value; } }

        public DateTime DataFine { get { return dataFine; } set { dataFine = value; } }

        public DateTime DataInizioMalattia { get { return dataInizioMalattia; } set { dataInizioMalattia = value; } }

        public String Descrizione { get { return descrizione; } set { descrizione = value; } }

    }
}

