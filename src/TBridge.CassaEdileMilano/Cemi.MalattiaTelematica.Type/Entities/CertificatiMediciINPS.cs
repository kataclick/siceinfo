﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class CertificatiMediciINPS
    {
        public DateTime DataInizio{ get; set; }
        public DateTime DataFine { get; set; }
        public DateTime DataRilascio { get; set; }

        public String Tipo { get; set; }

    
    }
}
