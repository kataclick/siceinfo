﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.MalattiaTelematica.Type.Filters
{
    [Serializable]
    public class AssenzeFilter
    {
        public Int32? IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String CodiceFiscaleImpresa { get; set; }
        public Int32? IdLavoratore { get; set; }
        public String NomeLavoratore { get; set; }
        public String CognomeLavoratore { get; set; }
        public DateTime? DataNascitaLavoratore { get; set; }
        public String StatoAssenza { get; set; }
        public String TipoAssenza { get; set; }
        public DateTime? PeriodoDa { get; set; }
        public DateTime? PeriodoA { get; set; }
        public Int32? IdAssenza { get; set; }
        public Int32? IdUtente { get; set; }
        public DateTime? DataInvioDa { get; set; }
        public DateTime? DataInvioA { get; set; }
        
        
    }
}
