﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.MalattiaTelematica.Type.Entities;

namespace Cemi.MalattiaTelematica.Type.Collections
{
    [Serializable]
    public class MalattiaInfortunioErroriCollection : List<MalattiaInfortunioErrori>
    {
    }
}
