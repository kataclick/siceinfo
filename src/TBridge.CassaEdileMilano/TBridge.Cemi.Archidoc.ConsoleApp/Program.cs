﻿using System;
using System.Configuration;
using TBridge.Cemi.Archidoc.Business;

namespace TBridge.Cemi.Archidoc.ConsoleApp
{
    public class Program
    {
        private static readonly Int16 giornoGiroCompleto = Int16.Parse(ConfigurationManager.AppSettings["GiornoGiroCompleto"]);
        private static readonly Int32 giorniRecupero = Int32.Parse(ConfigurationManager.AppSettings["GiorniRecupero"]);

        private static readonly DateTime recuperaDataDa= DateTime.Parse(ConfigurationManager.AppSettings["DataDa"]);
        private static readonly DateTime recuperaDataA = DateTime.Parse(ConfigurationManager.AppSettings["DataA"]);

        private static readonly String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
        private static readonly String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
        private static readonly String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
        private static readonly String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
        private static readonly String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
        private static readonly String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

        private static readonly String updateWorkFlow = (ConfigurationManager.AppSettings["UpdateWorkFlow"]);
        private static readonly String delegheWorkFlow = (ConfigurationManager.AppSettings["DelegheWorkFlow"]);

        private static readonly String notaAllegatoBustaDeleghe = ConfigurationManager.AppSettings["IdentificativoNotaAllegatoBustaDeleghe"];
        private static readonly Int32 giorniRecuperoBusteDeleghe = Int32.Parse(ConfigurationManager.AppSettings["GiorniRecuperoBusteDeleghe"]);

        static void Main(string[] args)
        {
            DateTime dataA, dataDa;
            String tipoFlusso = string.Empty; //il tipoFlusso può essere "Lettura" o "Scrittura"
            ArchidocBusiness biz = new ArchidocBusiness();
            DateTime oggi = DateTime.Now;

            #region Controllo args
            if (args != null && args.Length > 0)
            {
                tipoFlusso = args[0];
            }
            else
            {
                //Impostiamo il default
                tipoFlusso = "Lettura";
            } 
            #endregion

           
            if (tipoFlusso == "Lettura")
            {
                #region Flusso Lettura Documenti Archidoc

                //Definiamo in base al file di configurazione quali sono gli estremi
                if (recuperaDataDa.Year == 1900)
                {
                    dataA = oggi;
                    dataDa = dataA.AddDays(-giorniRecupero);

                    if ((Int16)oggi.DayOfWeek == giornoGiroCompleto)
                    {
                        dataDa = oggi.AddDays(-365);
                    }
                }
                else
                {
                    dataA = recuperaDataA;
                    dataDa = recuperaDataDa;
                }

                //Leggere i documenti da Archidoc e scriverli in siceinfo
                biz.GetDataFromArchidoc(dataDa, dataA);

                #endregion

                #region Flusso Lettura Deleghe

                //Attiviamo il giro di deleghe (gira comunque solo nel giorno di giro completo )
                if (delegheWorkFlow == "1" && (Int16)oggi.DayOfWeek == giornoGiroCompleto)
                {
                    Int32? delegaDa = biz.GetMinDelega();
                    Int32? delegaA = biz.GetMaxDelega();

                    if (delegaDa.HasValue && delegaA.HasValue)
                        //caricare da siceinfo gli aggiornamenti fatti dall'operatore e portarli su siceinfo
                        biz.GetDelegheFromArchidoc(delegaDa.Value, delegaA.Value);
                }

                #endregion

                #region Gestione allegati deleghe (Buste)

                DateTime busteA = oggi.Date.AddDays(1);
                DateTime busteDa = oggi.Date.AddDays(-giorniRecuperoBusteDeleghe);

                biz.AllegaBusteDeleghe(busteDa, busteA, notaAllegatoBustaDeleghe);

                #endregion
            }
            else if (tipoFlusso == "Scrittura")
            {
                #region Flusso Scrittura Documenti Archidoc

                //Se è abilitato il flusso di update:
                if (updateWorkFlow == "1")
                {
                    //get data elaborazione
                    DateTime? data = biz.GetDataElaborazione();
                    if (data.HasValue)
                        //caricare da siceinfo gli aggiornamenti fatti dall'operatore e portarli su siceinfo
                        biz.GetDataFromSiceInfo(data.Value);
                }

                #endregion
            }

            #region Test
            //TEST SINGOLO
            //ModuloCollection mp = biz.GetModuloPrestazioneCollection(null);
            //Modulo modulo = mp[0];
            //ArchidocConnector ac = ArchidocConnector.GetIstance();
            //ac.UpdateCard(modulo, "Generale", "Prestazioni"); 
            #endregion
        }
    }
}
