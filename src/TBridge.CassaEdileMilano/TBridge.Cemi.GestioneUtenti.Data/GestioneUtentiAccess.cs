using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;
using TBridge.Cemi.GestioneUtenti.Type.Filters;
using TBridge.Cemi.Type.Entities;
using CassaEdile = TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;
using Impresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;
using TipiUtente = TBridge.Cemi.GestioneUtenti.Type.TipiUtenti;

namespace TBridge.Cemi.GestioneUtenti.Data
{
    public class GestioneUtentiAccess
    {
        public GestioneUtentiAccess()
        {
            DatabaseSice = DatabaseFactory.CreateDatabase("CEMI");
        }

        private Database DatabaseSice { get; set; }

        public ImpreseCollection GetUtentiImprese()
        {
            ImpreseCollection imprese = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelectRegistratiPortale"))
            {
                using (DataSet dataSetImprese = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetImprese != null && dataSetImprese.Tables.Count > 0 &&
                        dataSetImprese.Tables[0].Rows.Count > 0)
                    {
                        imprese = new ImpreseCollection();

                        for (int i = 0; i < dataSetImprese.Tables[0].Rows.Count; i++)
                        {
                            //Impresa impresa = new Impresa();
                            Impresa impresa = new Impresa(
                                (Int32) dataSetImprese.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetImprese.Tables[0].Rows[i]["login"]);

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idImpresa"]))
                                impresa.IdImpresa = (int) dataSetImprese.Tables[0].Rows[i]["idImpresa"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["ragioneSociale"]))
                                impresa.RagioneSociale = (string) dataSetImprese.Tables[0].Rows[i]["ragioneSociale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceFiscale"]))
                                impresa.CodiceFiscale = (string) dataSetImprese.Tables[0].Rows[i]["codiceFiscale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["partitaIVA"]))
                                impresa.PartitaIVA = (string) dataSetImprese.Tables[0].Rows[i]["partitaIVA"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idTipoImpresa"]))
                                impresa.IdTipoImpresa = (string) dataSetImprese.Tables[0].Rows[i]["idTipoImpresa"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceContratto"]))
                                impresa.CodiceContratto = (string) dataSetImprese.Tables[0].Rows[i]["codiceContratto"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceINAIL"]))
                                impresa.CodiceINAIL = (string) dataSetImprese.Tables[0].Rows[i]["codiceINAIL"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceINPS"]))
                                impresa.CodiceINAIL = (string) dataSetImprese.Tables[0].Rows[i]["codiceINPS"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["numeroIscrizioneCCIAA"]))
                                impresa.NumeroIscrizioneCCIAA =
                                    (int) dataSetImprese.Tables[0].Rows[i]["numeroIscrizioneCCIAA"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idAttivitaISTAT"]))
                                impresa.IdAttivitaISTAT = (string) dataSetImprese.Tables[0].Rows[i]["idAttivitaISTAT"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idNaturaGiuridica"]))
                                impresa.IdNaturaGiuridica =
                                    (string) dataSetImprese.Tables[0].Rows[i]["idNaturaGiuridica"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["indirizzoSedeLegale"]))
                                impresa.IndirizzoSedeLegale =
                                    (string) dataSetImprese.Tables[0].Rows[i]["indirizzoSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["capSedeLegale"]))
                                impresa.CapSedeLegale = (string) dataSetImprese.Tables[0].Rows[i]["capSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["localitaSedeLegale"]))
                                impresa.LocalitaSedeLegale =
                                    (string) dataSetImprese.Tables[0].Rows[i]["localitaSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["provinciaSedeLegale"]))
                                impresa.ProvinciaSedeLegale =
                                    (string) dataSetImprese.Tables[0].Rows[i]["provinciaSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["indirizzoSedeAmministrazione"]))
                                impresa.IndirizzoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["indirizzoSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["capSedeAmministrazione"]))
                                impresa.CapSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["capSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["localitaSedeAmministrazione"]))
                                impresa.LocalitaSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["localitaSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["provinciaSedeAmministrazione"]))
                                impresa.PressoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["provinciaSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"]))
                                impresa.PressoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"]))
                                impresa.PressoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["telefonosedeAmministrazione"]))
                                impresa.TelefonoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["telefonosedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["faxSedeAmministrazione"]))
                                impresa.FaxSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["faxSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["eMailSedeAmministrazione"]))
                                impresa.Email = (string) dataSetImprese.Tables[0].Rows[i]["eMailSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["sitoWeb"]))
                                impresa.SitoWeb = (string) dataSetImprese.Tables[0].Rows[i]["sitoWeb"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["cellulare"]))
                                impresa.Cellulare = (string) dataSetImprese.Tables[0].Rows[i]["cellulare"];

                            //if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idConsulente"]))
                            //    impresa.IdConsulente = (int) dataSetImprese.Tables[0].Rows[i]["idConsulente"];


                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //impresa.IdUtente = (int) dataSetImprese.Tables[0].Rows[i]["idUtente"];
                            //impresa.Username = (string) dataSetImprese.Tables[0].Rows[i]["login"];
                            //impresa.Ruoli = GetRuoliUtente(impresa.IdUtente);
                            //impresa.Funzionalita = GetFunzionalitaUtente(impresa.IdUtente);

                            imprese.Add(impresa);
                        }
                    }
                }
            }

            return imprese;
        }

        public ConsulentiCollection GetUtentiConsulenti()
        {
            ConsulentiCollection consulenti = new ConsulentiCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ConsulentiSelectRegistratiPortale"))
            {
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        //Consulente consulente = new Consulente();
                        Consulente consulente = new Consulente(
                            (Int32) dr["idUtente"],
                            (String) dr["login"]);

                        if (dr["idConsulente"] != DBNull.Value)
                            consulente.IdConsulente = (int) dr["idConsulente"];

                        if (dr["ragioneSociale"] != DBNull.Value)
                            consulente.RagioneSociale = dr["ragioneSociale"] as string;

                        if (dr["codiceFiscale"] != DBNull.Value)
                            consulente.CodiceFiscale = dr["codiceFiscale"] as string;

                        if (dr["indirizzo"] != DBNull.Value)
                            consulente.Indirizzo = dr["indirizzo"] as string;

                        if (dr["cap"] != DBNull.Value)
                            consulente.Cap = dr["cap"] as string;

                        if (dr["localita"] != DBNull.Value)
                            consulente.Comune = dr["localita"] as string;

                        if (dr["provincia"] != DBNull.Value)
                            consulente.Provincia = dr["provincia"] as string;

                        if (dr["nTelefono"] != DBNull.Value)
                            consulente.Telefono = dr["nTelefono"] as string;

                        if (dr["nFax"] != DBNull.Value)
                            consulente.Fax = dr["nFax"] as string;

                        if (dr["nCellulare"] != DBNull.Value)
                            consulente.Cellulare = dr["nCellulare"] as string;

                        if (dr["eMail"] != DBNull.Value)
                            consulente.EMail = dr["eMail"] as string;

                        //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                        //consulente.IdUtente = (int) dr["idUtente"];
                        //consulente.Username = (string) dr["login"];
                        //consulente.Ruoli = GetRuoliUtente(consulente.IdUtente);
                        //consulente.Funzionalita = GetFunzionalitaUtente(consulente.IdUtente);

                        consulenti.Add(consulente);
                    }
                }
            }

            return consulenti;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public LavoratoriCollection GetUtentiLavoratori()
        {
            LavoratoriCollection lavoratori = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectRegistratiPortale"))
            {
                using (DataSet dataSetLavoratori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetLavoratori != null && dataSetLavoratori.Tables.Count > 0 &&
                        dataSetLavoratori.Tables[0].Rows.Count > 0)
                    {
                        lavoratori = new LavoratoriCollection();

                        for (int i = 0; i < dataSetLavoratori.Tables[0].Rows.Count; i++)
                        {
                            //Lavoratore lavoratore = new Lavoratore();
                            Lavoratore lavoratore = new Lavoratore(
                                (Int32) dataSetLavoratori.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetLavoratori.Tables[0].Rows[i]["login"]);

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idLavoratore"]))
                                lavoratore.IdLavoratore = (int) dataSetLavoratori.Tables[0].Rows[i]["idLavoratore"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["nome"]))
                                lavoratore.Nome = (string) dataSetLavoratori.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["cognome"]))
                                lavoratore.Cognome = (string) dataSetLavoratori.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["codiceFiscale"]))
                                lavoratore.CodiceFiscale = (string) dataSetLavoratori.Tables[0].Rows[i]["codiceFiscale"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["dataNascita"]))
                                lavoratore.DataNascita = (DateTime) dataSetLavoratori.Tables[0].Rows[i]["dataNascita"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["luogoNascita"]))
                                lavoratore.LuogoNascita = (string) dataSetLavoratori.Tables[0].Rows[i]["luogoNascita"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idNazionalita"]))
                                lavoratore.IdNazionalita = (string) dataSetLavoratori.Tables[0].Rows[i]["idNazionalita"];

                            //if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idLingua"]))
                            //    lavoratore.IdLingua = (string) dataSetLavoratori.Tables[0].Rows[i]["idLingua"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["sesso"]))
                                lavoratore.Sesso = (string) dataSetLavoratori.Tables[0].Rows[i]["sesso"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idStatoCivile"]))
                                lavoratore.IdStatoCivile = (string) dataSetLavoratori.Tables[0].Rows[i]["idStatoCivile"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoDenominazione"]))
                                lavoratore.IndirizzoDenominazione =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoDenominazione"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoComune"]))
                                lavoratore.IndirizzoComune =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoComune"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoCAP"]))
                                lavoratore.IndirizzoCAP = (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoCAP"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoProvincia"]))
                                lavoratore.IndirizzoProvincia =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoProvincia"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["telefono"]))
                                lavoratore.Telefono = (string) dataSetLavoratori.Tables[0].Rows[i]["telefono"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["cellulare"]))
                                lavoratore.Cellulare = (string) dataSetLavoratori.Tables[0].Rows[i]["cellulare"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["eMail"]))
                                lavoratore.Email = (string) dataSetLavoratori.Tables[0].Rows[i]["eMail"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["dataDecesso"]))
                                lavoratore.DataDecesso = (DateTime) dataSetLavoratori.Tables[0].Rows[i]["dataDecesso"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idTipoDecesso"]))
                                lavoratore.IdTipoDecesso = (string) dataSetLavoratori.Tables[0].Rows[i]["idTipoDecesso"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idTipoPagamento"]))
                                lavoratore.IdTipoPagamento =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["idTipoPagamento"];

                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //lavoratore.IdUtente = (int) dataSetLavoratori.Tables[0].Rows[i]["idUtente"];
                            //lavoratore.Username = (string) dataSetLavoratori.Tables[0].Rows[i]["login"];
                            //lavoratore.Ruoli = GetRuoliUtente(lavoratore.IdUtente);
                            //lavoratore.Funzionalita = GetFunzionalitaUtente(lavoratore.IdUtente);

                            lavoratori.Add(lavoratore);
                        }
                    }
                }
            }

            return lavoratori;
        }

        /// <summary>
        /// Ritorna i dipendenti che hanno un utente associato
        /// </summary>
        /// <returns></returns>
        public DipendentiCollection GetUtentiDipendenti()
        {
            DipendentiCollection dipendenti = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_DipendentiSelectRegistratiPortale"))
            {
                using (DataSet dataSetDipendenti = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetDipendenti != null && dataSetDipendenti.Tables.Count > 0 &&
                        dataSetDipendenti.Tables[0].Rows.Count > 0)
                    {
                        dipendenti = new DipendentiCollection();

                        for (int i = 0; i < dataSetDipendenti.Tables[0].Rows.Count; i++)
                        {
                            //Dipendente dipendente = new Dipendente();
                            Dipendente dipendente = new Dipendente(
                                (Int32) dataSetDipendenti.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetDipendenti.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["idDipendente"]))
                                dipendente.IdDipendente = (int) dataSetDipendenti.Tables[0].Rows[i]["idDipendente"];

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["nome"]))
                                dipendente.Nome = (string) dataSetDipendenti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["cognome"]))
                                dipendente.Cognome = (string) dataSetDipendenti.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["eMail"]))
                                dipendente.EMail = (string) dataSetDipendenti.Tables[0].Rows[i]["eMail"];

                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //dipendente.IdUtente = (int) dataSetDipendenti.Tables[0].Rows[i]["idUtente"];
                            //dipendente.Username = (string) dataSetDipendenti.Tables[0].Rows[i]["login"];
                            //dipendente.Ruoli = GetRuoliUtente(dipendente.IdUtente);
                            //dipendente.Funzionalita = GetFunzionalitaUtente(dipendente.IdUtente);

                            dipendenti.Add(dipendente);
                        }
                    }
                }
            }

            return dipendenti;
        }

        /// <summary>
        /// Ritorna l'elenco degli ispettori iscritti regolarmente al sito ovvero con utente associato
        /// </summary>
        /// <returns></returns>
        public IspettoriCollection GetUtentiIspettori()
        {
            IspettoriCollection ispettori = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_IspettoriSelectRegistratiPortale"))
            {
                using (DataSet dataSetIspettori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetIspettori != null && dataSetIspettori.Tables.Count > 0 &&
                        dataSetIspettori.Tables[0].Rows.Count > 0)
                    {
                        ispettori = new IspettoriCollection();

                        for (int i = 0; i < dataSetIspettori.Tables[0].Rows.Count; i++)
                        {
                            //Ispettore ispettore = new Ispettore();
                            Ispettore ispettore = new Ispettore(
                                (Int32) dataSetIspettori.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetIspettori.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["idIspettore"]))
                                ispettore.IdIspettore = (int) dataSetIspettori.Tables[0].Rows[i]["idIspettore"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["nome"]))
                                ispettore.Nome = (string) dataSetIspettori.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["cognome"]))
                                ispettore.Cognome = (string) dataSetIspettori.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["idCantieriZona"]))
                                ispettore.IdCantieriZona = (int) dataSetIspettori.Tables[0].Rows[i]["idCantieriZona"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["descrizioneZona"]))
                                ispettore.DescrizioneZona =
                                    (string) dataSetIspettori.Tables[0].Rows[i]["descrizioneZona"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["nomeZona"]))
                                ispettore.NomeZona = (string) dataSetIspettori.Tables[0].Rows[i]["nomeZona"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["operativo"]))
                                ispettore.Operativo = (bool) dataSetIspettori.Tables[0].Rows[i]["operativo"];

                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //ispettore.IdUtente = (int) dataSetIspettori.Tables[0].Rows[i]["idUtente"];
                            //ispettore.Username = (string) dataSetIspettori.Tables[0].Rows[i]["login"];
                            //ispettore.Ruoli = GetRuoliUtente(ispettore.IdUtente);
                            //ispettore.Funzionalita = GetFunzionalitaUtente(ispettore.IdUtente);

                            ispettori.Add(ispettore);
                        }
                    }
                }
            }

            return ispettori;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public EsattoriCollection GetUtentiEsattori()
        {
            EsattoriCollection esattori = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_EsattoriSelectRegistratiPortale"))
            {
                using (DataSet dataSetEsattori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetEsattori != null && dataSetEsattori.Tables.Count > 0 &&
                        dataSetEsattori.Tables[0].Rows.Count > 0)
                    {
                        esattori = new EsattoriCollection();

                        for (int i = 0; i < dataSetEsattori.Tables[0].Rows.Count; i++)
                        {
                            //Esattore esattore = new Esattore();
                            Esattore esattore = new Esattore(
                                (Int32) dataSetEsattori.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetEsattori.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetEsattori.Tables[0].Rows[i]["idEsattore"]))
                                esattore.IdEsattore = (string) dataSetEsattori.Tables[0].Rows[i]["idEsattore"];

                            if (!Convert.IsDBNull(dataSetEsattori.Tables[0].Rows[i]["nome"]))
                                esattore.Nome = (string) dataSetEsattori.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetEsattori.Tables[0].Rows[i]["cognome"]))
                                esattore.Cognome = (string) dataSetEsattori.Tables[0].Rows[i]["cognome"];

                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //esattore.IdUtente = (int) dataSetEsattori.Tables[0].Rows[i]["idUtente"];
                            //esattore.Username = (string) dataSetEsattori.Tables[0].Rows[i]["login"];
                            //esattore.Ruoli = GetRuoliUtente(esattore.IdUtente);
                            //esattore.Funzionalita = GetFunzionalitaUtente(esattore.IdUtente);

                            esattori.Add(esattore);
                        }
                    }
                }
            }

            return esattori;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public FornitoriCollection GetUtentiFornitori()
        {
            FornitoriCollection fornitori = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_FornitoriSelectRegistratiPortale"))
            {
                using (DataSet dataSetFornitori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetFornitori != null && dataSetFornitori.Tables.Count > 0 &&
                        dataSetFornitori.Tables[0].Rows.Count > 0)
                    {
                        fornitori = new FornitoriCollection();

                        for (int i = 0; i < dataSetFornitori.Tables[0].Rows.Count; i++)
                        {
                            //Fornitore fornitore = new Fornitore();
                            Fornitore fornitore = new Fornitore(
                                (int) dataSetFornitori.Tables[0].Rows[i]["idUtente"],
                                (string) dataSetFornitori.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["idFornitore"]))
                                fornitore.IdFornitore = (int) dataSetFornitori.Tables[0].Rows[i]["idFornitore"];

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["ragionesociale"]))
                                fornitore.RagioneSociale = (string) dataSetFornitori.Tables[0].Rows[i]["ragionesociale"];

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["codice"]))
                                fornitore.Codice = (string) dataSetFornitori.Tables[0].Rows[i]["codice"];

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["eMail"]))
                                fornitore.EMail = (string) dataSetFornitori.Tables[0].Rows[i]["eMail"];

                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //fornitore.IdUtente = (int) dataSetFornitori.Tables[0].Rows[i]["idUtente"];
                            //fornitore.Username = (string) dataSetFornitori.Tables[0].Rows[i]["login"];
                            //fornitore.Ruoli = GetRuoliUtente(fornitore.IdUtente);
                            //fornitore.Funzionalita = GetFunzionalitaUtente(fornitore.IdUtente);

                            fornitori.Add(fornitore);
                        }
                    }
                }
            }

            return fornitori;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SindacalistiCollection GetUtentiSindacalisti()
        {
            SindacalistiCollection sindacalisti;
            using (
                DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_SindacalistiSelectRegistratiPortale"))
            {
                using (DataSet dataSetSindacalisti = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    sindacalisti = new SindacalistiCollection();

                    Sindacalista sindacalista;

                    if (dataSetSindacalisti != null && dataSetSindacalisti.Tables.Count > 0 &&
                        dataSetSindacalisti.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetSindacalisti.Tables[0].Rows.Count; i++)
                        {
                            //sindacalista = new Sindacalista();
                            sindacalista = new Sindacalista(
                                (int) dataSetSindacalisti.Tables[0].Rows[i]["idUtente"],
                                (string) dataSetSindacalisti.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idSindacalista"]))
                                sindacalista.Id = (int) dataSetSindacalisti.Tables[0].Rows[i]["idSindacalista"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["nome"]))
                                sindacalista.Nome = (string) dataSetSindacalisti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["cognome"]))
                                sindacalista.Cognome = (string) dataSetSindacalisti.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idSindacato"]))
                            {
                                if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["descrizioneSindacato"]))
                                {
                                    Sindacato sindacato =
                                        new Sindacato((string) dataSetSindacalisti.Tables[0].Rows[i]["idSindacato"],
                                                      (string)
                                                      dataSetSindacalisti.Tables[0].Rows[i]["descrizioneSindacato"]);

                                    sindacalista.Sindacato = sindacato;
                                }
                            }

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idComprensorioSindacale"]))
                            {
                                ComprensorioSindacale comprensorio = null;

                                if (
                                    !Convert.IsDBNull(
                                         dataSetSindacalisti.Tables[0].Rows[i]["descrizioneComprensorioSindacale"]))
                                {
                                    comprensorio =
                                        new ComprensorioSindacale(
                                            (string) dataSetSindacalisti.Tables[0].Rows[i]["idComprensorioSindacale"],
                                            (string)
                                            dataSetSindacalisti.Tables[0].Rows[i]["descrizioneComprensorioSindacale"]);
                                }

                                sindacalista.ComprensorioSindacale = comprensorio;
                            }
                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //sindacalista.IdUtente = (int) dataSetSindacalisti.Tables[0].Rows[i]["idUtente"];
                            //sindacalista.Username = (string) dataSetSindacalisti.Tables[0].Rows[i]["login"];
                            //sindacalista.Ruoli = GetRuoliUtente(sindacalista.IdUtente);
                            //sindacalista.Funzionalita = GetFunzionalitaUtente(sindacalista.IdUtente);

                            sindacalisti.Add(sindacalista);
                        }
                    }
                }
            }

            return sindacalisti;
        }

        public ASLCollection GetUtentiASL()
        {
            ASLCollection asles = new ASLCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ASLSelectRegistratiPortale"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        //ASL asl = new ASL();
                        ASL asl = new ASL(
                            (int) reader["idUtente"],
                            (string) reader["login"]
                            );
                        asles.Add(asl);

                        asl.IdASLUtente = (int) reader["idASL"];
                        if (!Convert.IsDBNull(reader["cognome"]))
                            asl.Cognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            asl.Nome = (string) reader["nome"];

                        asl.IdASL = (int) reader["idASL"];
                        asl.Codice = (string) reader["codice"];
                        if (!Convert.IsDBNull(reader["descrizione"]))
                            asl.Descrizione = (string) reader["descrizione"];

                        //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                        //asl.IdUtente = (int) reader["idUtente"];
                        //asl.Username = (string) reader["login"];
                        //asl.Ruoli = GetRuoliUtente(asl.IdUtente);
                        //asl.Funzionalita = GetFunzionalitaUtente(asl.IdUtente);
                    }
                }
            }

            return asles;
        }

        public CommittenteCollection GetUtentiCommittenti()
        {
            CommittenteCollection committenti = new CommittenteCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_CommittentiSelectRegistratiPortale")
                )
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        //Committente committente = new Committente();
                        Committente committente = new Committente(
                            (Int32) reader["idUtente"],
                            (String) reader["login"]
                            );
                        committenti.Add(committente);

                        if (!Convert.IsDBNull(reader["cognome"]))
                            committente.Cognome = (String) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            committente.Nome = (String) reader["nome"];
                        if (!Convert.IsDBNull(reader["ragioneSociale"]))
                            committente.RagioneSociale = (String) reader["ragioneSociale"];
                        if (!Convert.IsDBNull(reader["partitaIva"]))
                            committente.PartitaIva = (String) reader["partitaIva"];
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            committente.CodiceFiscale = (String) reader["codiceFiscale"];
                        if (!Convert.IsDBNull(reader["indirizzo"]))
                            committente.Indirizzo = (String) reader["indirizzo"];
                        if (!Convert.IsDBNull(reader["comune"]))
                            committente.Comune = (Int64) reader["comune"];
                        if (!Convert.IsDBNull(reader["provincia"]))
                            committente.Provincia = (Int32) reader["provincia"];
                        if (!Convert.IsDBNull(reader["cap"]))
                            committente.Cap = (String) reader["cap"];
                        if (!Convert.IsDBNull(reader["telefono"]))
                            committente.Telefono = (String) reader["telefono"];
                        if (!Convert.IsDBNull(reader["fax"]))
                            committente.Fax = (String) reader["fax"];
                        if (!Convert.IsDBNull(reader["email"]))
                            committente.Email = (String) reader["email"];
                        if (!Convert.IsDBNull(reader["tipologia"]))
                            committente.Tipologia =
                                (TipologiaCommittente)
                                Enum.Parse(typeof(TipologiaCommittente), reader["tipologia"].ToString());

                        //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                        //committente.IdUtente = (Int32) reader["idUtente"];
                        //committente.Username = (String) reader["login"];
                        //committente.Ruoli = GetRuoliUtente(committente.IdUtente);
                        //committente.Funzionalita = GetFunzionalitaUtente(committente.IdUtente);
                    }
                }
            }

            return committenti;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public OspitiCollection GetUtentiOspiti()
        {
            OspitiCollection ospiti = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_OspitiSelectRegistratiPortale"))
            {
                using (DataSet dataSetOspiti = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetOspiti != null && dataSetOspiti.Tables.Count > 0 &&
                        dataSetOspiti.Tables[0].Rows.Count > 0)
                    {
                        ospiti = new OspitiCollection();

                        for (int i = 0; i < dataSetOspiti.Tables[0].Rows.Count; i++)
                        {
                            //Ospite ospite = new Ospite();
                            Ospite ospite = new Ospite(
                                (int) dataSetOspiti.Tables[0].Rows[i]["idUtente"],
                                (string) dataSetOspiti.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["idOspite"]))
                                ospite.IdOspite = (int) dataSetOspiti.Tables[0].Rows[i]["idOspite"];

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["nome"]))
                                ospite.Nome = (string) dataSetOspiti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["cognome"]))
                                ospite.Cognome = (string) dataSetOspiti.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["ente"]))
                                ospite.Ente = (string) dataSetOspiti.Tables[0].Rows[i]["ente"];
                            //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                            //ospite.IdUtente = (int) dataSetOspiti.Tables[0].Rows[i]["idUtente"];
                            //ospite.Username = (string) dataSetOspiti.Tables[0].Rows[i]["login"];
                            //ospite.Ruoli = GetRuoliUtente(ospite.IdUtente);
                            //ospite.Funzionalita = GetFunzionalitaUtente(ospite.IdUtente);

                            ospiti.Add(ospite);
                        }
                    }
                }
            }

            return ospiti;
        }

        public CasseEdiliCollection GetUtentiCasseEdili()
        {
            CasseEdiliCollection casseEdili = new CasseEdiliCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_CasseEdiliSelectRegistratiPortale"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        //CassaEdile cassaEdile = new CassaEdile();
                        CassaEdile cassaEdile = new CassaEdile(
                            (int) reader["idUtente"],
                            (string) reader["login"]
                            );
                        casseEdili.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        if (reader["descrizione"] != DBNull.Value)
                            cassaEdile.Descrizione = (string) reader["descrizione"];

                        //Deve essere un utente del sistema per forza se non funziona � giusto che sollevi un eccezione
                        //cassaEdile.IdUtente = (int) reader["idUtente"];
                        //cassaEdile.Username = (string) reader["login"];
                        //cassaEdile.Ruoli = GetRuoliUtente(cassaEdile.IdUtente);
                        //cassaEdile.Funzionalita = GetFunzionalitaUtente(cassaEdile.IdUtente);
                    }
                }
            }

            return casseEdili;
        }

        public Utente GetUtente(int idUtente)
        {
            Utente utente = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectByIdUtente"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);

                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        try
                        {
                            TipiUtente tipoUtente = (TipiUtente) reader["tipoUtente"];

                            switch (tipoUtente)
                            {
                                case TipiUtente.Impresa:
                                    utente = GetUtenteImpresaDaReader(reader);
                                    break;
                                case TipiUtente.Fornitore:
                                    utente = GetUtenteFornitoreDaReader(reader);
                                    break;
                                case TipiUtente.Ispettore:
                                    utente = GetUtenteIspettoreDaReader(reader);
                                    break;
                                case TipiUtente.Esattore:
                                    utente = GetUtenteEsattoreDaReader(reader);
                                    break;
                                case TipiUtente.Sindacalista:
                                    utente = GetUtenteSindacalistaDaReader(reader);
                                    break;
                                case TipiUtente.Lavoratore:
                                    utente = GetUtenteLavoratoreDaReader(reader);
                                    break;
                                case TipiUtente.Consulente:
                                    utente = GetUtenteConsulenteDaReader(reader);
                                    break;
                                case TipiUtente.CassaEdile:
                                    utente = GetUtenteCassaEdileDaReader(reader);
                                    break;
                                case TipiUtente.ASL:
                                    utente = GetUtenteASLDaReader(reader);
                                    break;
                                case TipiUtente.Committente:
                                    utente = GetUtenteCommittenteDaReader(reader);
                                    break;
                                case TipiUtente.Dipendente:
                                    utente = GetUtenteDipendenteDaReader(reader);
                                    break;
                                case TipiUtente.Ospite:
                                    utente = GetUtenteOspiteDaReader(reader);
                                    break;
                            }
                        }
                        catch (Exception exception)
                        {
                            throw new Exception("GetUtente: impossibile recuperare l'utente corrente - " + exception.Message);
                        }
                    }
                }
            }

            return utente;
        }

        public Impresa GetUtenteImpresaDaReader(IDataReader reader)
        {
            Impresa impresa = new Impresa(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idImpresa"]))
                impresa.IdImpresa = (int) reader["idImpresa"];

            if (!Convert.IsDBNull(reader["ragioneSociale"]))
                impresa.RagioneSociale = (string) reader["ragioneSociale"];

            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                impresa.CodiceFiscale = (string) reader["codiceFiscale"];

            if (!Convert.IsDBNull(reader["partitaIVA"]))
                impresa.PartitaIVA = (string) reader["partitaIVA"];

            if (!Convert.IsDBNull(reader["idTipoImpresa"]))
                impresa.IdTipoImpresa = (string) reader["idTipoImpresa"];

            if (!Convert.IsDBNull(reader["codiceContratto"]))
                impresa.CodiceContratto = (string) reader["codiceContratto"];

            if (!Convert.IsDBNull(reader["codiceINAIL"]))
                impresa.CodiceINAIL = (string) reader["codiceINAIL"];

            if (!Convert.IsDBNull(reader["codiceINPS"]))
                impresa.CodiceINAIL = (string) reader["codiceINPS"];

            if (!Convert.IsDBNull(reader["numeroIscrizioneCCIAA"]))
                impresa.NumeroIscrizioneCCIAA =
                    (int) reader["numeroIscrizioneCCIAA"];

            if (!Convert.IsDBNull(reader["idAttivitaISTAT"]))
                impresa.IdAttivitaISTAT = (string) reader["idAttivitaISTAT"];

            if (!Convert.IsDBNull(reader["idNaturaGiuridica"]))
                impresa.IdNaturaGiuridica = (string) reader["idNaturaGiuridica"];

            if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                impresa.IndirizzoSedeLegale =
                    (string) reader["indirizzoSedeLegale"];

            if (!Convert.IsDBNull(reader["capSedeLegale"]))
                impresa.CapSedeLegale = (string) reader["capSedeLegale"];

            if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                impresa.LocalitaSedeLegale = (string) reader["localitaSedeLegale"];

            if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                impresa.ProvinciaSedeLegale =
                    (string) reader["provinciaSedeLegale"];

            if (!Convert.IsDBNull(reader["indirizzoSedeAmministrazione"]))
                impresa.IndirizzoSedeAmministrazione =
                    (string) reader["indirizzoSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["capSedeAmministrazione"]))
                impresa.CapSedeAmministrazione =
                    (string) reader["capSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["localitaSedeAmministrazione"]))
                impresa.LocalitaSedeAmministrazione =
                    (string) reader["localitaSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["provinciaSedeAmministrazione"]))
                impresa.ProvinciaSedeAmministrazione =
                    (string) reader["provinciaSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["pressoSedeAmministrazione"]))
                impresa.PressoSedeAmministrazione =
                    (string) reader["pressoSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["telefonosedeAmministrazione"]))
                impresa.TelefonoSedeAmministrazione =
                    (string) reader["telefonosedeAmministrazione"];

            if (!Convert.IsDBNull(reader["faxSedeAmministrazione"]))
                impresa.FaxSedeAmministrazione =
                    (string) reader["faxSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["eMailSedeAmministrazione"]))
                impresa.Email = (string) reader["eMailSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["sitoWeb"]))
                impresa.SitoWeb = (string) reader["sitoWeb"];

            if (!Convert.IsDBNull(reader["cellulare"]))
                impresa.Cellulare = (string) reader["cellulare"];

            if (!Convert.IsDBNull(reader["guidEdilconnect"]))
                impresa.GuidEdilconnect = ((Guid) reader["guidEdilconnect"]).ToString();

            //impresa.Ruoli = GetRuoliUtente(impresa.IdUtente);
            //impresa.Funzionalita = GetFunzionalitaUtente(impresa.IdUtente);

            return impresa;
        }

        private Utente GetUtenteCommittenteDaReader(IDataReader reader)
        {
            Committente committente = new Committente(
                (Int32) reader["idUtente"],
                (String) reader["login"]
                );

            committente.IdCommittente = (Int32) reader["idCommittente"];
            if (!Convert.IsDBNull(reader["cognome"]))
                committente.Cognome = (String) reader["cognome"];
            if (!Convert.IsDBNull(reader["nome"]))
                committente.Nome = (String) reader["nome"];
            if (!Convert.IsDBNull(reader["ragioneSociale"]))
                committente.RagioneSociale = (String) reader["ragioneSociale"];
            if (!Convert.IsDBNull(reader["partitaIva"]))
                committente.PartitaIva = (String) reader["partitaIva"];
            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                committente.CodiceFiscale = (String) reader["codiceFiscale"];
            if (!Convert.IsDBNull(reader["indirizzo"]))
                committente.Indirizzo = (String) reader["indirizzo"];
            if (!Convert.IsDBNull(reader["comune"]))
                committente.Comune = (Int64) reader["comune"];
            if (!Convert.IsDBNull(reader["provincia"]))
                committente.Provincia = (Int32) reader["provincia"];
            if (!Convert.IsDBNull(reader["cap"]))
                committente.Cap = (String) reader["cap"];
            if (!Convert.IsDBNull(reader["telefono"]))
                committente.Telefono = (String) reader["telefono"];
            if (!Convert.IsDBNull(reader["fax"]))
                committente.Fax = (String) reader["fax"];
            if (!Convert.IsDBNull(reader["email"]))
                committente.Email = (String) reader["email"];
            if (!Convert.IsDBNull(reader["tipologia"]))
                committente.Tipologia =
                    (TipologiaCommittente)
                    Enum.Parse(typeof(TipologiaCommittente), reader["tipologia"].ToString());

            //committente.Ruoli = GetRuoliUtente(committente.IdUtente);
            //committente.Funzionalita = GetFunzionalitaUtente(committente.IdUtente);

            return committente;
        }

        private Utente GetUtenteASLDaReader(IDataReader reader)
        {
            ASL asl = new ASL(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            asl.IdASLUtente = (int) reader["idASL"];
            if (!Convert.IsDBNull(reader["cognome"]))
                asl.Cognome = (string) reader["cognome"];
            if (!Convert.IsDBNull(reader["nome"]))
                asl.Nome = (string) reader["nome"];

            asl.IdASL = (int) reader["idASL"];
            asl.Codice = (string) reader["codice"];
            if (!Convert.IsDBNull(reader["descrizione"]))
                asl.Descrizione = (string) reader["descrizione"];

            //asl.Ruoli = GetRuoliUtente(asl.IdUtente);
            //asl.Funzionalita = GetFunzionalitaUtente(asl.IdUtente);

            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    asl.Comuni.Add((string) reader["descrizione"]);
                }
            }

            return asl;
        }

        private Utente GetUtenteCassaEdileDaReader(IDataReader reader)
        {
            CassaEdile cassaEdile = new CassaEdile(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
            if (reader["descrizione"] != DBNull.Value)
                cassaEdile.Descrizione = (string) reader["descrizione"];

            //cassaEdile.Ruoli = GetRuoliUtente(cassaEdile.IdUtente);
            //cassaEdile.Funzionalita = GetFunzionalitaUtente(cassaEdile.IdUtente);

            return cassaEdile;
        }

        private Utente GetUtenteConsulenteDaReader(IDataReader reader)
        {
            Consulente consulente = new Consulente(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idConsulente"]))
                consulente.IdConsulente = (int) reader["idConsulente"];

            if (!Convert.IsDBNull(reader["ragioneSociale"]))
                consulente.RagioneSociale = (string) reader["ragioneSociale"];

            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                consulente.CodiceFiscale = (string) reader["codiceFiscale"];

            if (!Convert.IsDBNull(reader["indirizzo"]))
                consulente.Indirizzo = (string) reader["indirizzo"];

            if (!Convert.IsDBNull(reader["localita"]))
                consulente.Comune = (string) reader["localita"];

            if (!Convert.IsDBNull(reader["cap"]))
                consulente.Cap = (string) reader["cap"];

            if (!Convert.IsDBNull(reader["provincia"]))
                consulente.Provincia = (string) reader["provincia"];

            if (!Convert.IsDBNull(reader["nTelefono"]))
                consulente.Telefono = (string) reader["nTelefono"];

            if (!Convert.IsDBNull(reader["PIN"]))
                consulente.PIN = (string) reader["PIN"];

            if (!Convert.IsDBNull(reader["email"]))
                consulente.EMail = (string) reader["email"];

            if (!Convert.IsDBNull(reader["guidEdilconnect"]))
                consulente.GuidEdilconnect = ((Guid) reader["guidEdilconnect"]).ToString();

            //consulente.Ruoli = GetRuoliUtente(consulente.IdUtente);
            //consulente.Funzionalita = GetFunzionalitaUtente(consulente.IdUtente);

            return consulente;
        }

        private Utente GetUtenteLavoratoreDaReader(IDataReader reader)
        {
            Lavoratore lavoratore = new Lavoratore(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idLavoratore"]))
                lavoratore.IdLavoratore = (int) reader["idLavoratore"];

            if (!Convert.IsDBNull(reader["cognome"]))
                lavoratore.Cognome = (string) reader["cognome"];

            if (!Convert.IsDBNull(reader["nome"]))
                lavoratore.Nome = (string) reader["nome"];

            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                lavoratore.CodiceFiscale = (string) reader["codiceFiscale"];

            if (!Convert.IsDBNull(reader["dataNascita"]))
                lavoratore.DataNascita = (DateTime) reader["dataNascita"];

            if (!Convert.IsDBNull(reader["luogoNascita"]))
                lavoratore.LuogoNascita = (string) reader["luogoNascita"];

            if (!Convert.IsDBNull(reader["idNazionalita"]))
                lavoratore.IdNazionalita = (string) reader["idNazionalita"];

            if (!Convert.IsDBNull(reader["sesso"]))
                lavoratore.Sesso = (string) reader["sesso"];

            if (!Convert.IsDBNull(reader["idStatoCivile"]))
                lavoratore.IdStatoCivile = (string) reader["idStatoCivile"];

            if (!Convert.IsDBNull(reader["indirizzoDenominazione"]))
                lavoratore.IndirizzoDenominazione =
                    (string) reader["indirizzoDenominazione"];

            if (!Convert.IsDBNull(reader["indirizzoComune"]))
                lavoratore.IndirizzoComune = (string) reader["indirizzoComune"];

            if (!Convert.IsDBNull(reader["indirizzoCAP"]))
                lavoratore.IndirizzoCAP = (string) reader["indirizzoCAP"];

            if (!Convert.IsDBNull(reader["indirizzoProvincia"]))
                lavoratore.IndirizzoProvincia = (string) reader["indirizzoProvincia"];

            if (!Convert.IsDBNull(reader["telefono"]))
                lavoratore.Telefono = (string) reader["telefono"];

            if (!Convert.IsDBNull(reader["cellulare"]))
                lavoratore.Cellulare = (string) reader["cellulare"];

            if (!Convert.IsDBNull(reader["eMail"]))
                lavoratore.Email = (string) reader["eMail"];

            if (!Convert.IsDBNull(reader["dataDecesso"]))
                lavoratore.DataDecesso = (DateTime) reader["dataDecesso"];

            if (!Convert.IsDBNull(reader["idTipoDecesso"]))
                lavoratore.IdTipoDecesso = (string) reader["idTipoDecesso"];

            if (!Convert.IsDBNull(reader["idTipoPagamento"]))
                lavoratore.IdTipoPagamento = (string) reader["idTipoPagamento"];

            if (!Convert.IsDBNull(reader["PIN"]))
                lavoratore.Pin = (string) reader["PIN"];

            //lavoratore.Ruoli = GetRuoliUtente(lavoratore.IdUtente);
            //lavoratore.Funzionalita = GetFunzionalitaUtente(lavoratore.IdUtente);

            return lavoratore;
        }

        private Utente GetUtenteSindacalistaDaReader(IDataReader reader)
        {
            Sindacalista sindacalista = new Sindacalista(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idSindacalista"]))
                sindacalista.Id = (int) reader["idSindacalista"];

            if (!Convert.IsDBNull(reader["nome"]))
                sindacalista.Nome = (string) reader["nome"];

            if (!Convert.IsDBNull(reader["cognome"]))
                sindacalista.Cognome = (string) reader["cognome"];

            if (!Convert.IsDBNull(reader["idSindacato"]))
            {
                Sindacato sindacato = null;

                if (!Convert.IsDBNull(reader["descrizioneSindacato"]))
                {
                    sindacato = new Sindacato((string) reader["idSindacato"],
                                              (string)
                                              reader["descrizioneSindacato"]);
                }

                sindacalista.Sindacato = sindacato;
            }

            if (!Convert.IsDBNull(reader["idComprensorioSindacale"]))
            {
                ComprensorioSindacale comprensorio = null;

                if (
                    !Convert.IsDBNull(
                         reader["descrizioneComprensorioSindacale"]))
                {
                    comprensorio =
                        new ComprensorioSindacale(
                            (string) reader["idComprensorioSindacale"],
                            (string) reader["descrizioneComprensorioSindacale"]);
                }

                sindacalista.ComprensorioSindacale = comprensorio;
            }

            //sindacalista.Ruoli = GetRuoliUtente(sindacalista.IdUtente);
            //sindacalista.Funzionalita = GetFunzionalitaUtente(sindacalista.IdUtente);

            return sindacalista;
        }

        private Utente GetUtenteEsattoreDaReader(IDataReader reader)
        {
            Esattore esattore = new Esattore(
                (int) reader["idUtente"],
                (string) reader["login"]);

            if (!Convert.IsDBNull(reader["idEsattore"]))
                esattore.IdEsattore = (string) reader["idEsattore"];

            if (!Convert.IsDBNull(reader["nome"]))
                esattore.Nome = (string) reader["nome"];

            if (!Convert.IsDBNull(reader["cognome"]))
                esattore.Cognome = (string) reader["cognome"];

            //esattore.Ruoli = GetRuoliUtente(esattore.IdUtente);
            //esattore.Funzionalita = GetFunzionalitaUtente(esattore.IdUtente);

            return esattore;
        }

        private Utente GetUtenteIspettoreDaReader(IDataReader reader)
        {
            Ispettore ispettore = new Ispettore(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idIspettore"]))
                ispettore.IdIspettore = (int) reader["idIspettore"];

            if (!Convert.IsDBNull(reader["nome"]))
                ispettore.Nome = (string) reader["nome"];

            if (!Convert.IsDBNull(reader["cognome"]))
                ispettore.Cognome = (string) reader["cognome"];

            if (!Convert.IsDBNull(reader["cellulare"]))
                ispettore.Cellulare = (string) reader["cellulare"];

            if (!Convert.IsDBNull(reader["idCantieriZona"]))
                ispettore.IdCantieriZona = (int) reader["idCantieriZona"];

            if (!Convert.IsDBNull(reader["operativo"]))
                ispettore.Operativo = (bool) reader["operativo"];

            //ispettore.Ruoli = GetRuoliUtente(ispettore.IdUtente);
            //ispettore.Funzionalita = GetFunzionalitaUtente(ispettore.IdUtente);

            return ispettore;
        }

        private Utente GetUtenteFornitoreDaReader(IDataReader reader)
        {
            Fornitore fornitore = new Fornitore(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idFornitore"]))
                fornitore.IdFornitore = (int) reader["idFornitore"];

            if (!Convert.IsDBNull(reader["ragioneSociale"]))
                fornitore.RagioneSociale = (string) reader["ragioneSociale"];

            if (!Convert.IsDBNull(reader["codice"]))
                fornitore.Codice = (string) reader["codice"];

            if (!Convert.IsDBNull(reader["email"]))
                fornitore.Email = (string) reader["email"];

            //fornitore.Ruoli = GetRuoliUtente(fornitore.IdUtente);
            //fornitore.Funzionalita = GetFunzionalitaUtente(fornitore.IdUtente);

            return fornitore;
        }

        private Utente GetUtenteDipendenteDaReader(IDataReader reader)
        {
            Dipendente dipendente = new Dipendente(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idDipendente"]))
                dipendente.IdDipendente = (int) reader["idDipendente"];

            if (!Convert.IsDBNull(reader["cognome"]))
                dipendente.Cognome = (string) reader["cognome"];

            if (!Convert.IsDBNull(reader["nome"]))
                dipendente.Nome = (string) reader["nome"];

            if (!Convert.IsDBNull(reader["email"]))
                dipendente.EMail = (string) reader["email"];

            return dipendente;
        }

        private Ospite GetUtenteOspiteDaReader(IDataReader reader)
        {
            Ospite ospite = new Ospite(
                (int) reader["idUtente"],
                (string) reader["login"]
                );

            if (!Convert.IsDBNull(reader["idOspite"]))
                ospite.IdOspite = (int) reader["idOspite"];

            if (!Convert.IsDBNull(reader["cognome"]))
                ospite.Cognome = (string) reader["cognome"];

            if (!Convert.IsDBNull(reader["nome"]))
                ospite.Nome = (string) reader["nome"];

            if (!Convert.IsDBNull(reader["email"]))
                ospite.EMail = (string) reader["email"];

            return ospite;
        }

        /// <summary>
        /// Restituisce la collezione di imprese presenti nel DB
        /// </summary>
        /// <returns></returns>
        public ImpreseCollection GetImpreseAll()
        {
            ImpreseCollection imprese = new ImpreseCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelectAll"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        Impresa impresa = CaricaImpresaDaReader(reader);
                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        public List<ImpresaConCredenziali> GetImprese(ImpresaFilter filtro)
        {
            List<ImpresaConCredenziali> imprese = new List<ImpresaConCredenziali>();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelect"))
            {
                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                    DatabaseSice.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                if (!string.IsNullOrEmpty(filtro.PartitaIVA))
                    DatabaseSice.AddInParameter(dbCommand, "@partitaIVA", DbType.String, filtro.PartitaIVA);
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                    DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                if (!string.IsNullOrEmpty(filtro.Cap))
                    DatabaseSice.AddInParameter(dbCommand, "@cap", DbType.String, filtro.Cap);
                if (filtro.PinDal.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@pinDal", DbType.DateTime, filtro.PinDal.Value);
                if (filtro.PinAl.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@pinAl", DbType.DateTime, filtro.PinAl.Value);
                if (filtro.NuoveIscritte.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@nuoveIscritte", DbType.Boolean, filtro.NuoveIscritte.Value);
                if (filtro.ConPin.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@conPin", DbType.Boolean, filtro.ConPin.Value);
                if (filtro.ConFabbisogno.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@conFabbisogno", DbType.Boolean, filtro.ConFabbisogno.Value);
                if (filtro.ConDenuncia.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@conDenuncia", DbType.Boolean, filtro.ConDenuncia.Value);
                if (filtro.IscrittaDal.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@iscrittaDal", DbType.DateTime, filtro.IscrittaDal.Value);
                if (filtro.IscrittaAl.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@iscrittaAl", DbType.DateTime, filtro.IscrittaAl.Value);
                if (filtro.Codice.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filtro.Codice.Value);
                if (filtro.IscrittaSitoWeb.HasValue)
                    DatabaseSice.AddInParameter(dbCommand, "@iscrittaWeb", DbType.Boolean, filtro.IscrittaSitoWeb.Value);

                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ImpresaConCredenziali impresa = CaricaImpresaDaReader(reader);
                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        private static ImpresaConCredenziali CaricaImpresaDaReader(IDataReader reader)
        {
            ImpresaConCredenziali impresa = new ImpresaConCredenziali();

            if (!Convert.IsDBNull(reader["idImpresa"]))
                impresa.IdImpresa = (int) reader["idImpresa"];

            if (!Convert.IsDBNull(reader["ragioneSociale"]))
                impresa.RagioneSociale = (string) reader["ragioneSociale"];

            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                impresa.CodiceFiscale = (string) reader["codiceFiscale"];

            if (!Convert.IsDBNull(reader["partitaIVA"]))
                impresa.PartitaIVA = (string) reader["partitaIVA"];

            if (!Convert.IsDBNull(reader["idTipoImpresa"]))
                impresa.IdTipoImpresa = (string) reader["idTipoImpresa"];

            if (!Convert.IsDBNull(reader["codiceContratto"]))
                impresa.CodiceContratto = (string) reader["codiceContratto"];

            if (!Convert.IsDBNull(reader["codiceINAIL"]))
                impresa.CodiceINAIL = (string) reader["codiceINAIL"];

            if (!Convert.IsDBNull(reader["codiceINPS"]))
                impresa.CodiceINAIL = (string) reader["codiceINPS"];

            if (!Convert.IsDBNull(reader["numeroIscrizioneCCIAA"]))
                impresa.NumeroIscrizioneCCIAA = (int) reader["numeroIscrizioneCCIAA"];

            if (!Convert.IsDBNull(reader["idAttivitaISTAT"]))
                impresa.IdAttivitaISTAT = (string) reader["idAttivitaISTAT"];

            if (!Convert.IsDBNull(reader["idNaturaGiuridica"]))
                impresa.IdNaturaGiuridica = (string) reader["idNaturaGiuridica"];

            if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                impresa.IndirizzoSedeLegale = (string) reader["indirizzoSedeLegale"];

            if (!Convert.IsDBNull(reader["capSedeLegale"]))
                impresa.CapSedeLegale = (string) reader["capSedeLegale"];

            if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                impresa.LocalitaSedeLegale = (string) reader["localitaSedeLegale"];

            if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                impresa.ProvinciaSedeLegale = (string) reader["provinciaSedeLegale"];

            if (!Convert.IsDBNull(reader["indirizzoSedeAmministrazione"]))
                impresa.IndirizzoSedeAmministrazione =
                    (string) reader["indirizzoSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["capSedeAmministrazione"]))
                impresa.CapSedeAmministrazione =
                    (string) reader["capSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["localitaSedeAmministrazione"]))
                impresa.LocalitaSedeAmministrazione =
                    (string) reader["localitaSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["provinciaSedeAmministrazione"]))
                impresa.ProvinciaSedeAmministrazione =
                    (string) reader["provinciaSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["pressoSedeAmministrazione"]))
                impresa.PressoSedeAmministrazione =
                    (string) reader["pressoSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["telefonosedeAmministrazione"]))
                impresa.TelefonoSedeAmministrazione =
                    (string) reader["telefonosedeAmministrazione"];

            if (!Convert.IsDBNull(reader["faxSedeAmministrazione"]))
                impresa.FaxSedeAmministrazione =
                    (string) reader["faxSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["eMailSedeAmministrazione"]))
                impresa.Email = (string) reader["eMailSedeAmministrazione"];

            if (!Convert.IsDBNull(reader["sitoWeb"]))
                impresa.SitoWeb = (string) reader["sitoWeb"];

            if (!Convert.IsDBNull(reader["cellulare"]))
                impresa.Cellulare = (string) reader["cellulare"];

            if (!Convert.IsDBNull(reader["PIN"]))
                impresa.PIN = (string) reader["PIN"];

            if (!Convert.IsDBNull(reader["dataIscrizione"]))
                impresa.DataIscrizione = (DateTime) reader["dataIscrizione"];

            if (!Convert.IsDBNull(reader["dataDisdetta"]))
                impresa.DataIscrizione = (DateTime) reader["dataDisdetta"];

            if (!Convert.IsDBNull(reader["dataGenerazionePIN"]))
                impresa.DataGenerazionePIN = (DateTime) reader["dataGenerazionePIN"];

            if (!Convert.IsDBNull(reader["login"]))
                impresa.Username = (string) reader["login"];

            //if (!Convert.IsDBNull(reader["password"]))
            //    impresa.Password = (string) reader["password"];

            if (!Convert.IsDBNull(reader["idUtente"]))
                impresa.IdUtente = (int) reader["idUtente"];

            return impresa;
        }

        public CAPCollection GetImpreseCAP()
        {
            CAPCollection listaCAP = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelectListaCAP"))
            {
                using (DataSet dataSetCAP = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetCAP != null && dataSetCAP.Tables.Count > 0 && dataSetCAP.Tables[0].Rows.Count > 0)
                    {
                        listaCAP = new CAPCollection();

                        for (int i = 0; i < dataSetCAP.Tables[0].Rows.Count; i++)
                        {
                            CAP cap = new CAP();

                            if (!Convert.IsDBNull(dataSetCAP.Tables[0].Rows[i]["capSedeAmministrazione"]))
                                cap.Cap = (string) dataSetCAP.Tables[0].Rows[i]["capSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetCAP.Tables[0].Rows[i]["localitaSedeAmministrazione"]))
                                cap.Comune = (string) dataSetCAP.Tables[0].Rows[i]["localitaSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetCAP.Tables[0].Rows[i]["provinciaSedeAmministrazione"]))
                                cap.Provincia = (string) dataSetCAP.Tables[0].Rows[i]["provinciaSedeAmministrazione"];

                            listaCAP.Add(cap);
                        }
                    }
                }
            }

            return listaCAP;
        }

        //public Collections.CAPCollection GetLavoratoriCAP()
        //{
        //    Collections.CAPCollection listaCAP = null;

        //    DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectListaCAP");
        //    DataSet dataSetCAP = DatabaseSice.ExecuteDataSet(dbCommand);

        //    if (dataSetCAP != null && dataSetCAP.Tables.Count > 0 && dataSetCAP.Tables[0].Rows.Count > 0)
        //    {
        //        listaCAP = new Collections.CAPCollection();

        //        for (int i = 0; i < dataSetCAP.Tables[0].Rows.Count; i++)
        //        {
        //            Entities.CAP cap = new Entities.CAP();

        //            if (!Convert.IsDBNull(dataSetCAP.Tables[0].Rows[i]["indirizzoCAP"]))
        //                cap.Cap = (string)dataSetCAP.Tables[0].Rows[i]["indirizzoCAP"];

        //            if (!Convert.IsDBNull(dataSetCAP.Tables[0].Rows[i]["indirizzoDenominazione"]))
        //                cap.Comune = (string)dataSetCAP.Tables[0].Rows[i]["indirizzoDenominazione"];

        //            if (!Convert.IsDBNull(dataSetCAP.Tables[0].Rows[i]["indirizzoProvincia"]))
        //                cap.Provincia = (string)dataSetCAP.Tables[0].Rows[i]["indirizzoProvincia"];

        //            listaCAP.Add(cap);
        //        }
        //    }

        //    return listaCAP;
        //}

        ///// <summary>
        ///// Ritorna una collection di imprese che hanno una denuncia a CE entro i 9 mesi ma non hanno un PIN
        ///// </summary>
        ///// <returns></returns>
        //public ImpreseCollection GetImpreseIscritteCESenzaPIN()
        //{
        //    ImpreseCollection imprese = null;

        //    using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelectIscritteCESenzaPIN"))
        //    {
        //        using (DataSet dataSetImprese = DatabaseSice.ExecuteDataSet(dbCommand))
        //        {
        //            if (dataSetImprese != null && dataSetImprese.Tables.Count > 0 && dataSetImprese.Tables[0].Rows.Count > 0)
        //            {
        //                imprese = new ImpreseCollection();

        //                for (int i = 0; i < dataSetImprese.Tables[0].Rows.Count; i++)
        //                {
        //                    Impresa impresa = new Impresa();

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idImpresa"]))
        //                        impresa.IdImpresa = (int) dataSetImprese.Tables[0].Rows[i]["idImpresa"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["ragioneSociale"]))
        //                        impresa.RagioneSociale = (string) dataSetImprese.Tables[0].Rows[i]["ragioneSociale"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceFiscale"]))
        //                        impresa.CodiceFiscale = (string) dataSetImprese.Tables[0].Rows[i]["codiceFiscale"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["partitaIVA"]))
        //                        impresa.PartitaIVA = (string) dataSetImprese.Tables[0].Rows[i]["partitaIVA"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idTipoImpresa"]))
        //                        impresa.IdTipoImpresa = (string) dataSetImprese.Tables[0].Rows[i]["idTipoImpresa"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceContratto"]))
        //                        impresa.CodiceContratto = (string) dataSetImprese.Tables[0].Rows[i]["codiceContratto"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceINAIL"]))
        //                        impresa.CodiceINAIL = (string) dataSetImprese.Tables[0].Rows[i]["codiceINAIL"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceINPS"]))
        //                        impresa.CodiceINAIL = (string) dataSetImprese.Tables[0].Rows[i]["codiceINPS"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["numeroIscrizioneCCIAA"]))
        //                        impresa.NumeroIscrizioneCCIAA = (int) dataSetImprese.Tables[0].Rows[i]["numeroIscrizioneCCIAA"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idAttivitaISTAT"]))
        //                        impresa.IdAttivitaISTAT = (string) dataSetImprese.Tables[0].Rows[i]["idAttivitaISTAT"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idNaturaGiuridica"]))
        //                        impresa.IdNaturaGiuridica = (string) dataSetImprese.Tables[0].Rows[i]["idNaturaGiuridica"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["indirizzoSedeLegale"]))
        //                        impresa.IndirizzoSedeLegale = (string) dataSetImprese.Tables[0].Rows[i]["indirizzoSedeLegale"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["capSedeLegale"]))
        //                        impresa.CapSedeLegale = (string) dataSetImprese.Tables[0].Rows[i]["capSedeLegale"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["localitaSedeLegale"]))
        //                        impresa.LocalitaSedeLegale = (string) dataSetImprese.Tables[0].Rows[i]["localitaSedeLegale"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["provinciaSedeLegale"]))
        //                        impresa.ProvinciaSedeLegale = (string) dataSetImprese.Tables[0].Rows[i]["provinciaSedeLegale"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["indirizzoSedeAmministrazione"]))
        //                        impresa.IndirizzoSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["indirizzoSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["capSedeAmministrazione"]))
        //                        impresa.CapSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["capSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["localitaSedeAmministrazione"]))
        //                        impresa.LocalitaSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["localitaSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["provinciaSedeAmministrazione"]))
        //                        impresa.ProvinciaSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["provinciaSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"]))
        //                        impresa.PressoSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["telefonosedeAmministrazione"]))
        //                        impresa.TelefonoSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["telefonosedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["faxSedeAmministrazione"]))
        //                        impresa.FaxSedeAmministrazione =
        //                            (string) dataSetImprese.Tables[0].Rows[i]["faxSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["eMailSedeAmministrazione"]))
        //                        impresa.Email = (string) dataSetImprese.Tables[0].Rows[i]["eMailSedeAmministrazione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["sitoWeb"]))
        //                        impresa.SitoWeb = (string) dataSetImprese.Tables[0].Rows[i]["sitoWeb"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["cellulare"]))
        //                        impresa.Cellulare = (string) dataSetImprese.Tables[0].Rows[i]["cellulare"];

        //                    //if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idConsulente"]))
        //                    //    impresa.IdConsulente = (int) dataSetImprese.Tables[0].Rows[i]["idConsulente"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["PIN"]))
        //                        impresa.PIN = (string) dataSetImprese.Tables[0].Rows[i]["PIN"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["dataIscrizione"]))
        //                        impresa.DataIscrizione = (DateTime) dataSetImprese.Tables[0].Rows[i]["dataIscrizione"];

        //                    if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["dataDisdetta"]))
        //                        impresa.DataIscrizione = (DateTime) dataSetImprese.Tables[0].Rows[i]["dataDisdetta"];

        //                    imprese.Add(impresa);
        //                }
        //            }
        //        }
        //    }

        //    return imprese;
        //}

        /// <summary>
        /// Aggiorna il PIN dell'impresa
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pin"></param>
        /// <returns></returns>
        public bool AggiornaPinImpresa(int id, string pin)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseUpdateAggiornaPIN"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, id);
                DatabaseSice.AddInParameter(dbCommand, "@PIN", DbType.String, pin);

                //TODO verificare perch� torna -1
                if (DatabaseSice.ExecuteNonQuery(dbCommand) == 1)
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Aggiorna il PIN del lavoratore
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pin"></param>
        /// <returns></returns>
        public bool AggiornaPinLavoratore(int id, string pin)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriUpdateAggiornaPIN"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, id);
                DatabaseSice.AddInParameter(dbCommand, "@PIN", DbType.String, pin);

                //TODO verificare perch� torna -1
                if (DatabaseSice.ExecuteNonQuery(dbCommand) == 1)
                    return true;
                else
                    return false;
            }
        }

        public bool AggiornaPinConsulente(int id, string pin)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ConsulentiUpdateAggiornaPIN"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idConsulente", DbType.Int32, id);
                DatabaseSice.AddInParameter(dbCommand, "@PIN", DbType.String, pin);

                //TODO verificare perch� torna -1
                if (DatabaseSice.ExecuteNonQuery(dbCommand) == 1)
                    return true;
                else
                    return false;
            }
        }

        public bool EsisteUtente(string username, string password)
        {
            Utente utente = GetUtente(username, password);
            return utente != null;
        }

        public bool EsisteUtenteRegistrato(int idUtente)
        {
            bool ret = false;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectById"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                        if (!Convert.IsDBNull(dr["password"]))
                            ret = true;
                }
            }

            return ret;
        }

        public Utente GetUtente(string username, string password)
        {
            Utente utente = null;
            password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@username", DbType.String, username);
                DatabaseSice.AddInParameter(dbCommand, "@password", DbType.String, password);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        //utente = new Utente();
                        utente = new Utente((int) dr["idUtente"], (string) dr["login"], null);

                        //utente.Ruoli = GetRuoliUtente(utente.IdUtente);
                        //utente.Funzionalita = GetFunzionalitaUtente(utente.IdUtente);
                    }
                }
            }

            return utente;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns>L'idUtente</returns>
        public int GetIdUtente(string username)
        {
            int ret = -1;

            //Questa particolare stored procedure prende solo lo username!!!!!
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectByUsername"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@username", DbType.String, username);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                        ret = (int) dr["idUtente"];
                }
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Utente GetUtente(string username)
        {
            Utente utente = null;
            //Questa particolare stored procedure prende solo lo username!!!!!
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectByUsername"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@username", DbType.String, username);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        //utente = new Utente();
                        utente = new Utente(
                            (int) dr["idUtente"],
                            (string) dr["login"],
                            null);

                        //utente.IdUtente = (int) dr["idUtente"];
                        //utente.Username = (string) dr["login"];
                        //utente.Password = (string) dr["password"];

                        //utente.Ruoli = GetRuoliUtente(utente.IdUtente);
                        //utente.Funzionalita = GetFunzionalitaUtente(utente.IdUtente);
                    }
                }
            }

            return utente;
        }

        /// <summary>
        /// Controlla l'esistenza di un utente a partire da un idutente
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public bool EsisteUtente(int idUtente)
        {
            bool ret = false;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectById"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                        ret = true;
                }
            }

            return ret;
        }

        /// <summary>
        /// Ritorna l'utente corrispondente ad un dato id
        /// </summary>
        /// <returns></returns>
        public Utente GetUtenteById(int idUtente)
        {
            //Utente utente = new Utente();
            Utente utente = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectById"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        utente = new Utente(
                            (int) dr["idUtente"],
                            (string) dr["login"],
                            null); //(string) dr["password"]

                        //utente.IdUtente = (int) dr["idUtente"];
                        //utente.Username = (string) dr["login"];
                        //utente.Password = (string) dr["password"];
                    }
                }
            }

            return utente;
        }

        /// <summary>
        /// Ritorna la lista degli utenti del sistema
        /// </summary>
        /// <returns></returns>
        public UtentiCollection GetUtenti()
        {
            UtentiCollection utenti = new UtentiCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectAll"))
            {
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        //Utente utente = new Utente();
                        Utente utente = new Utente(
                            (int) dr["idUtente"],
                            (string) dr["login"],
                            null
                            );

                        //utente.IdUtente = (int) dr["idUtente"];
                        //utente.Username = (string) dr["login"];

                        //TODO ha senso caricare subito tutti i ruoli? gli utenti sono tutte le anagrafiche del sistema
                        //utente.Ruoli = GetRuoliUtente(utente.IdUtente);
                        //utente.Funzionalita = GetFunzionalitaUtente(utente.IdUtente);

                        utenti.Add(utente);
                    }
                }
            }

            return utenti;
        }

        /// <summary>
        /// Gli utenti vengono inseriti automaticamente con Trigger. La registraUtente si occupa di rendere effettiva la registrazione 
        /// assegnando Username e password. E' essenziale che l'entit� utente abbia l'idUtente assegnato
        /// </summary>
        /// <param name="utente"></param>
        /// <returns></returns>
        public bool RegistraUtente(Utente utente)
        {
            bool ret = false;
            utente.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(utente.Password, "sha1");
            if (utente.IdUtente > 0)
            {
                using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiUpdate"))
                {
                    DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, utente.IdUtente);
                    DatabaseSice.AddInParameter(dbCommand, "@username", DbType.String, utente.UserName);
                    DatabaseSice.AddInParameter(dbCommand, "@password", DbType.String, utente.Password);

                    if (DatabaseSice.ExecuteNonQuery(dbCommand) == 1)
                    {
                        ret = true;
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="utente"></param>
        public void AggiornaUtente(Utente utente)
        {
            //Per come � fatta la registrazione dell'utente, l'aggiornamento � identico, ma dovr� essere gestito a livello di business 
            RegistraUtente(utente);
        }

        ///// <summary>
        ///// Gli utenti vengono inseriti automaticamente con Trigger. La registraUtente si occupa di rendere effettiva la registrazione 
        ///// assegnando Username e password. E' essenziale che l'entit� utente abbia l'idUtente assegnato
        ///// </summary>
        ///// <param name="utente"></param>
        ///// <param name="transaction"></param>
        ///// <returns></returns>
        //protected void RegistraUtente(Utente utente, DbTransaction transaction)
        //{
        //    utente.Password = FormsAuthentication.HashPasswordForStoringInConfigFile(utente.Password, "sha1");
        //    using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiUpdate"))
        //    {
        //        DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, utente.IdUtente);
        //        DatabaseSice.AddInParameter(dbCommand, "@username", DbType.String, utente.UserName);
        //        DatabaseSice.AddInParameter(dbCommand, "@password", DbType.String, utente.Password);

        //        DatabaseSice.ExecuteNonQuery(dbCommand, transaction);
        //    }
        //}

        /// <summary>
        /// Inserisce un ispettore nel database. In caso di inserimento corretto il campo IdIspettore risulter� valorizzato
        /// </summary>
        /// <param name="ispettore"></param>
        public int InserisciIspettore(Ispettore ispettore)
        {
            int ret = -1;

            object obj;
            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_IspettoriInsert"))
            {
                DatabaseSice.AddInParameter(comando, "@cognome", DbType.String, ispettore.Cognome);
                DatabaseSice.AddInParameter(comando, "@nome", DbType.String, ispettore.Nome);
                if (!string.IsNullOrEmpty(ispettore.Cellulare))
                    DatabaseSice.AddInParameter(comando, "@cellulare", DbType.String, ispettore.Cellulare);
                DatabaseSice.AddInParameter(comando, "@idCantieriZona", DbType.String, ispettore.IdCantieriZona);
                DatabaseSice.AddInParameter(comando, "@operativo", DbType.Boolean, ispettore.Operativo);

                obj = DatabaseSice.ExecuteScalar(comando);
            }

            if (obj != null && obj is Int32)
                ret = (int) obj;

            return ret;
        }

        /// <summary>
        /// Inserisce un sindacalista nel database. In caso di inserimento corretto il campo IdSindacalista risulter� valorizzato
        /// </summary>
        /// <param name="sindacalista"></param>
        public int InserisciSindacalista(Sindacalista sindacalista)
        {
            int ret = -1;

            object obj;
            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_SindacalistiInsert"))
            {
                DatabaseSice.AddInParameter(comando, "@cognome", DbType.String, sindacalista.Cognome);
                DatabaseSice.AddInParameter(comando, "@nome", DbType.String, sindacalista.Nome);
                DatabaseSice.AddInParameter(comando, "@idSindacato", DbType.String, sindacalista.Sindacato.Id);
                if (sindacalista.ComprensorioSindacale != null)
                    DatabaseSice.AddInParameter(comando, "@idComprensorioSindacale", DbType.String,
                                                sindacalista.ComprensorioSindacale.Id);
                if (!string.IsNullOrEmpty(sindacalista.Email))
                    DatabaseSice.AddInParameter(comando, "@email", DbType.String, sindacalista.Email);

                obj = DatabaseSice.ExecuteScalar(comando);
            }

            if (obj != null && obj is Int32)
                ret = (int) obj;

            return ret;
        }

        /// <summary>
        /// Inserisce un committente nel database. In caso di inserimento corretto il campo IdCommittente risulter� valorizzato
        /// </summary>
        /// <param name="committente"></param>
        /// <returns></returns>
        public int InserisciCommittente(Committente committente)
        {
            int ret = -1;

            object obj;
            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_CommittentiInsert"))
            {
                if (!string.IsNullOrEmpty(committente.Cognome))
                    DatabaseSice.AddInParameter(comando, "@cognome", DbType.String, committente.Cognome);
                if (!string.IsNullOrEmpty(committente.Nome))
                    DatabaseSice.AddInParameter(comando, "@nome", DbType.String, committente.Nome);
                if (!string.IsNullOrEmpty(committente.RagioneSociale))
                    DatabaseSice.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                                committente.RagioneSociale);
                if (!string.IsNullOrEmpty(committente.PartitaIva))
                    DatabaseSice.AddInParameter(comando, "@partitaIva", DbType.String, committente.PartitaIva);
                if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                    DatabaseSice.AddInParameter(comando, "@codiceFiscale", DbType.String, committente.CodiceFiscale);
                if (!string.IsNullOrEmpty(committente.Indirizzo))
                    DatabaseSice.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                if (committente.Comune.HasValue)
                    DatabaseSice.AddInParameter(comando, "@comune", DbType.String, committente.Comune.Value);
                if (committente.Provincia.HasValue)
                    DatabaseSice.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia.Value);
                if (!string.IsNullOrEmpty(committente.Cap))
                    DatabaseSice.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                if (!string.IsNullOrEmpty(committente.Telefono))
                    DatabaseSice.AddInParameter(comando, "@telefono", DbType.String, committente.Telefono);
                if (!string.IsNullOrEmpty(committente.Fax))
                    DatabaseSice.AddInParameter(comando, "@fax", DbType.String, committente.Fax);
                if (!string.IsNullOrEmpty(committente.Email))
                    DatabaseSice.AddInParameter(comando, "@email", DbType.String, committente.Email);

                DatabaseSice.AddInParameter(comando, "@tipologia", DbType.Int16, committente.Tipologia);

                obj = DatabaseSice.ExecuteScalar(comando);
            }

            if (obj != null && obj is Int32)
                ret = (int) obj;

            return ret;
        }

        /// <summary>
        /// Modifica ispettore:l'ispettore una volta inserito nel sistema pu� subire modifiche relativamente a: operativo e zona cantiere
        /// </summary>
        /// <param name="ispettore"></param>
        /// <returns></returns>
        public bool ModificaIspettore(Ispettore ispettore)
        {
            bool rtn;

            try
            {
                if (ispettore != null)
                {
                    // Proseguo con l'inserimento
                    using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_IspettoriUpdate"))
                    {
                        DatabaseSice.AddInParameter(comando, "@idIspettore", DbType.Int32, ispettore.IdIspettore);
                        DatabaseSice.AddInParameter(comando, "@idCantieriZona", DbType.Int32, ispettore.IdCantieriZona);
                        DatabaseSice.AddInParameter(comando, "@operativo", DbType.Boolean, ispettore.Operativo);
                        if (!string.IsNullOrEmpty(ispettore.Cellulare))
                            DatabaseSice.AddInParameter(comando, "@cellulare", DbType.String, ispettore.Cellulare);

                        DatabaseSice.ExecuteScalar(comando);
                    }

                    rtn = true;
                }
                else
                    rtn = false;
            }
            catch (Exception ex)
            {
                rtn = false;
            }

            return rtn;
        }

        /// <summary>
        /// Inseriamo un dipendente nel DB
        /// </summary>
        /// <param name="dipendente"></param>
        /// <returns>L'idUtente registrato, -1 altrimenti</returns>
        public int InserisciDipendente(Dipendente dipendente)
        {
            int ret = -1;

            object obj;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_DipendentiInsert"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, dipendente.Nome);
                DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, dipendente.Cognome);
                DatabaseSice.AddInParameter(dbCommand, "@eMail", DbType.String, dipendente.EMail);

                obj = DatabaseSice.ExecuteScalar(dbCommand);
            }

            if (obj != null && obj is Int32)
                ret = (int) obj;

            return ret;
        }

        /// <summary>
        /// Inseriamo un fornitore nel DB
        /// </summary>
        /// <param name="fornitore"></param>
        /// <returns></returns>
        public int InserisciFornitore(Fornitore fornitore)
        {
            int ret = -1;

            object obj;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_FornitoriInsert"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, fornitore.RagioneSociale);
                DatabaseSice.AddInParameter(dbCommand, "@codice", DbType.String, fornitore.Codice);
                DatabaseSice.AddInParameter(dbCommand, "@eMail", DbType.String, fornitore.EMail);

                obj = DatabaseSice.ExecuteScalar(dbCommand);
            }

            if (obj != null && obj is Int32)
                ret = (int) obj;

            return ret;
        }

        /// <summary>
        /// Inseriamo un ospite nel DB
        /// </summary>
        /// <param name="ospite"></param>
        /// <returns></returns>
        public int InserisciOspite(Ospite ospite)
        {
            int ret = -1;

            object obj;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_OspitiInsert"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, ospite.Nome);
                DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, ospite.Cognome);
                DatabaseSice.AddInParameter(dbCommand, "@ente", DbType.String, ospite.Ente);
                DatabaseSice.AddInParameter(dbCommand, "@eMail", DbType.String, ospite.Email);

                obj = DatabaseSice.ExecuteScalar(dbCommand);
            }

            if (obj != null && obj is Int32)
                ret = (int) obj;

            return ret;
        }

        public bool ResetPassword(string username, string pin, string password, out Int32? idUtente)
        {
            password = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiResetPassword"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@login", DbType.String, username);
                DatabaseSice.AddInParameter(dbCommand, "@PIN", DbType.String, pin);
                DatabaseSice.AddInParameter(dbCommand, "@password", DbType.String, password);

                DatabaseSice.AddOutParameter(dbCommand, "@idUtente", DbType.Int32, 4);

                int ret = DatabaseSice.ExecuteNonQuery(dbCommand);

                idUtente = dbCommand.Parameters["@idUtente"].Value as Int32?;

                return (ret > 0);
            }
        }


        public int ChallengeLavoratore(string nome, string cognome, string codiceFiscale, string pin)
        {
            object obj;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectIdUtente"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, nome);
                DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, cognome);
                DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseSice.AddInParameter(dbCommand, "@pin", DbType.String, pin);

                obj = DatabaseSice.ExecuteScalar(dbCommand);
            }

            int res = -1;
            if (obj != null)
                res = (int) obj;

            return res;
        }

        //public int ChallengeImpresa(string ragioneSociale, string partitaIVA)
        //{
        //    object obj;
        //    using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelectIdUtente"))
        //    {
        //        DatabaseSice.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, ragioneSociale);
        //        DatabaseSice.AddInParameter(dbCommand, "@partitaIVA", DbType.String, partitaIVA);

        //        obj = DatabaseSice.ExecuteScalar(dbCommand);
        //    }

        //    int res = -1;
        //    if (obj != null)
        //        res = (int) obj;

        //    return res;
        //}

        public int ChallengeImpresaPIN(int idImpresa, string PIN)
        {
            object obj;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ImpreseSelectControlloPIN"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseSice.AddInParameter(dbCommand, "@PIN", DbType.String, PIN);

                obj = DatabaseSice.ExecuteScalar(dbCommand);
            }

            int res = -1;
            if (obj != null)
                res = (int) obj;

            return res;
        }

        public int ChallengeConsulentePIN(int idImpresa, string PIN)
        {
            object obj;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ConsulentiSelectControlloPIN"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idConsulente", DbType.Int32, idImpresa);
                DatabaseSice.AddInParameter(dbCommand, "@PIN", DbType.String, PIN);

                obj = DatabaseSice.ExecuteScalar(dbCommand);
            }

            int res = -1;
            if (obj != null)
                res = (int) obj;

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruolo"></param>
        /// <returns></returns>
        public int GetIdRuolo(string ruolo)
        {
            Ruolo ruoloE = GetRuolo(ruolo);

            if (ruolo != null)
            {
                return ruoloE.IdRuolo;
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruolo"></param>
        /// <returns></returns>
        public Ruolo GetRuolo(string ruolo)
        {
            Ruolo ruoloE = null;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_RuoliSelectIdByName"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@nomeRuolo", DbType.String, ruolo);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        ruoloE = new Ruolo();

                        ruoloE.IdRuolo = (int) dr["idRuolo"];
                        ruoloE.Nome = (string) dr["nome"];
                        ruoloE.Descrizione = (string) dr["descrizione"];
                    }
                }
            }

            return ruoloE;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtente"></param>
        /// <param name="idRuolo"></param>
        public int DisassociaUtenteRuolo(int idUtente, int idRuolo)
        {
            //controllare che il ruolo non sia gi� presente
            RuoliCollection ruoliUtente = GetRuoliUtente(idUtente);

            if (ruoliUtente.Contains(idRuolo))
            {
                using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliDelete"))
                {
                    DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                    DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);

                    //TODO verificare perch� torna -1
                    return DatabaseSice.ExecuteNonQuery(dbCommand);
                }
            }
            else
                //non era presente e torno 1
                return 1;
        }

        /// <summary>
        /// Ritorna tutti i ruoli presenti nel sistema
        /// </summary>
        /// <returns></returns>
        public RuoliCollection GetRuoli()
        {
            RuoliCollection ruoliCollection = new RuoliCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_RuoliSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, -1);
                //prende anche -1
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Ruolo ruolo = new Ruolo();

                        ruolo.IdRuolo = (int) dr["idRuolo"];
                        ruolo.Nome = (string) dr["nome"];
                        ruolo.Descrizione = (string) dr["descrizione"];
                        ruolo.Predefinito = (bool) dr["predefinito"];
                        ruolo.Modificabile = (bool) dr["modificabile"];

                        ruoliCollection.Add(ruolo);
                    }
                }
            }

            return ruoliCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <returns></returns>
        public Ruolo GetRuolo(int idRuolo)
        {
            Ruolo ruolo = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_RuoliSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    if (dr.Read())
                    {
                        ruolo = new Ruolo();

                        ruolo.IdRuolo = (int) dr["idRuolo"];
                        ruolo.Nome = (string) dr["nome"];
                        ruolo.Descrizione = (string) dr["descrizione"];
                        ruolo.Predefinito = (bool) dr["predefinito"];
                        ruolo.Modificabile = (bool) dr["modificabile"];
                    }
                }
            }

            return ruolo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruolo"></param>
        public void AggiornaRuolo(Ruolo ruolo)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_RuoliUpdate"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, ruolo.IdRuolo);
                DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, ruolo.Nome);
                DatabaseSice.AddInParameter(dbCommand, "@descrizione", DbType.String, ruolo.Descrizione);

                DatabaseSice.ExecuteNonQuery(dbCommand);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruolo"></param>
        /// <returns>IdRuolo</returns>
        public int CreaRuolo(Ruolo ruolo)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_RuoliInsert"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, ruolo.Nome);
                DatabaseSice.AddInParameter(dbCommand, "@descrizione", DbType.String, ruolo.Descrizione);
                return (int) DatabaseSice.ExecuteScalar(dbCommand);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruolo"></param>
        /// <returns></returns>
        public int CancellaRuolo(Ruolo ruolo)
        {
            return CancellaRuolo(ruolo.IdRuolo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <returns></returns>
        public int CancellaRuolo(int idRuolo)
        {
            //todo alfredo  /***??? tutte le associazinoni??? id utente???****/
            //cancellare tutti le associazioni ruolo/utente
            //cancellare tutte le funzionalita del ruolo
            //cancellare ruolo
            //DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_CancellaRuoloDelete");
            //DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, ruolo.Nome);
            //return (int)DatabaseSice.ExecuteNonQuery(dbCommand);
            try
            {
                DbConnection conn = null;
                DbTransaction trans = null;
                try
                {
                    conn = DatabaseSice.CreateConnection();
                    conn.Open();
                    trans = conn.BeginTransaction();
                    DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Ruoli_FunzionalitaDelete");
                    DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                    DatabaseSice.ExecuteNonQuery(dbCommand, trans);

                    dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliDeleteAllByRuolo");
                    DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                    DatabaseSice.ExecuteNonQuery(dbCommand, trans);

                    dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_RuoliDelete");
                    DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                    DatabaseSice.ExecuteNonQuery(dbCommand, trans);
                    trans.Commit();
                    conn.Close();
                    return 1;
                }
                catch
                {
                    if (trans != null)
                        trans.Rollback();

                    if (conn != null)
                        conn.Close();

                    return -1;
                }
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public RuoliCollection GetRuoliUtente(int idUtente)
        {
            RuoliCollection ruoli = new RuoliCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Ruolo ruolo = new Ruolo();

                        ruolo.IdRuolo = (int) dr["idRuolo"];
                        ruolo.Nome = (string) dr["nome"];
                        ruolo.Descrizione = (string) dr["descrizione"];
                        ruolo.Predefinito = (bool) dr["predefinito"];
                        ruolo.Modificabile = (bool) dr["modificabile"];

                        ruoli.Add(ruolo);
                    }
                }
            }

            return ruoli;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtente"></param>
        /// <param name="ruoli"></param>
        /// <returns></returns>
        public int AssociaUtenteRuoli(int idUtente, RuoliCollection ruoli)
        {
            int rtn = 0;

            DbConnection connection = null;

            DbTransaction transaction = null;

            try
            {
                //begin transaction
                //togliamo tutti i ruoli che aveva
                connection = DatabaseSice.CreateConnection();

                connection.Open();

                transaction = connection.BeginTransaction();

                try
                {
                    CancellaRuoliUtente(idUtente, transaction);
                    //mettiamo i nuovi ruoli:
                    //  per ogni ruolo lanciamo il metodo AssegnaRuoli
                    bool inserimentoFunzionalitaEffettuato = true;

                    for (int i = 0; i < ruoli.Count; i++)
                    {
                        object obj;
                        using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliInsert"))
                        {
                            DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                            DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, ruoli[i].IdRuolo);

                            obj = DatabaseSice.ExecuteScalar(dbCommand, transaction);
                        }

                        if (obj != null)
                            rtn = (int) obj;
                        else
                            rtn = -1;

                        if (rtn == -1)
                        {
                            inserimentoFunzionalitaEffettuato = false;

                            //C'� stato un errore nell'assegnazione di una funzionalit�: interrompiamo tutto
                            break;
                        }
                    }

                    //Se abbiamo effettuato tutti gli inserimenti facciamo la commit
                    if (inserimentoFunzionalitaEffettuato)
                        transaction.Commit();
                    else
                        transaction.Rollback();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                if (connection != null)
                    connection.Close();
            }

            return rtn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtente"></param>
        /// <param name="ruolo"></param>
        /// <returns></returns>
        public void AssociaUtenteRuolo(int idUtente, string ruolo)
        {
            //Otteniamo l'idRuolo a prtire dal nome
            AssociaUtenteRuolo(idUtente, GetIdRuolo(ruolo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtente"></param>
        /// <param name="idRuolo"></param>
        public void AssociaUtenteRuolo(int idUtente, int idRuolo)
        {
            //controllare che il ruolo non sia gi� presente
            //RuoliCollection ruoliUtente = GetRuoliUtente(idUtente);

            //if (!ruoliUtente.Contains(idRuolo))
            //{
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliInsert"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);

                DatabaseSice.ExecuteNonQuery(dbCommand);
            }
            //}
        }

        /*
                /// <summary>
                /// 
                /// </summary>
                /// <param name="idUtente"></param>
                private void CancellaRuoliUtente(int idUtente, int idRuolo)
                {
                    DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliDelete");
                    DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                    DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);

                    DatabaseSice.ExecuteNonQuery(dbCommand);
                }
        */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUtente"></param>
        /// <param name="transaction"></param>
        private void CancellaRuoliUtente(int idUtente, DbTransaction transaction)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_RuoliDeleteAll"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseSice.ExecuteNonQuery(dbCommand, transaction);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaUtente(string username)
        {
            //Otteniamo l'idutente
            int idUtente = GetIdUtente(username);

            //Ritorniamo le funzionalit� dell'utente
            return GetFunzionalitaUtente(idUtente);
        }

        /// <summary>
        /// Ritorna
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaUtente(int idUtente)
        {
            FunzionalitaCollection funzionalita = new FunzionalitaCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_FunzionalitaSelectbyUtente"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                //prende anche -1
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Funzionalita funz = new Funzionalita();

                        funz.IdFunzionalita = (int) dr["idFunzionalita"];
                        funz.Nome = (string) dr["nome"];
                        funz.Descrizione = (string) dr["descrizione"];

                        funzionalita.Add(funz);
                    }
                }
            }

            return funzionalita;
        }

        /// <summary>
        /// Ritorna
        /// </summary>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalita()
        {
            FunzionalitaCollection funzionalita = new FunzionalitaCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_FunzionalitaSelectAll"))
            {
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Funzionalita funz = new Funzionalita();

                        funz.IdFunzionalita = (int) dr["idFunzionalita"];
                        funz.Nome = (string) dr["nome"];
                        funz.Descrizione = (string) dr["descrizione"];
                        funz.Assegnabile = (bool) dr["assegnabile"];

                        funzionalita.Add(funz);
                    }
                }
            }

            return funzionalita;
        }

        /// <summary>
        /// Restituisce tutte le funzionalit� appartenenti ad un ruolo
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaRuolo(int idRuolo)
        {
            FunzionalitaCollection funzionalita = new FunzionalitaCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_FunzionalitaSelectbyRuolo"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                //prende anche -1

                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Funzionalita funz = new Funzionalita();

                        funz.IdFunzionalita = (int) dr["idFunzionalita"];
                        funz.Nome = (string) dr["nome"];
                        funz.Descrizione = (string) dr["descrizione"];

                        funzionalita.Add(funz);
                    }
                }
            }

            return funzionalita;
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <param name="funzionalita"></param>
        /// <returns></returns>
        public int AssociaFuzionalitaRuolo(int idRuolo, FunzionalitaCollection funzionalita)
        {
            int res;

            //- aprire transazione

            DbConnection connection = null;
            DbTransaction transaction = null;

            try
            {
                connection = DatabaseSice.CreateConnection();
                connection.Open();

                transaction = connection.BeginTransaction();

                //- cancellare le precedenti funzionalita del ruolo
                CancellaFunzionalita(idRuolo, transaction);

                //- inserire tutte le nuove funzionalita con

                for (int i = 0; i < funzionalita.Count; i++)
                {
                    DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Ruoli_FunzionalitaInsert");
                    DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                    DatabaseSice.AddInParameter(dbCommand, "@idFunzionalita", DbType.Int32,
                                                funzionalita[i].IdFunzionalita);

                    DatabaseSice.ExecuteNonQuery(dbCommand, transaction);

                    //Ritorna sempre -1!!!!!!
                    //if (rtn == -1)
                    //{
                    //    transaction.Rollback();
                    //    res = -1;

                    //    break;
                    //}
                }

                //- chiudere transazione
                transaction.Commit();
                res = 1;
            }
            catch
            {
                if (transaction != null)
                    transaction.Rollback();

                res = -1;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <param name="transaction"></param>
        private void CancellaFunzionalita(int idRuolo, DbTransaction transaction)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Ruoli_FunzionalitaDelete"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idRuolo", DbType.Int32, idRuolo);
                //prende anche -1

                DatabaseSice.ExecuteNonQuery(dbCommand, transaction);
            }
        }

        ///// <summary>
        ///// Il filtro su nome e cognome funziona come LIKE e i campi relativi accettano % in testa e/o coda
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <returns></returns>
        //public LavoratoriCollection GetLavoratori(FilterLavoratore filter)
        //{
        //    LavoratoriCollection lavoratori = new LavoratoriCollection();
        //    DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectWithFilter");
        //    if (filter != null)
        //    {
        //        if (filter.IdLavoratore.HasValue)
        //            DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, filter.IdLavoratore);
        //        if (filter.Cognome != null && filter.Cognome != "")
        //            DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, filter.Cognome);
        //        if (filter.Nome != null && filter.Nome != "")
        //            DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, filter.Nome);
        //        if (filter.CodiceFiscale != null && filter.CodiceFiscale != "")
        //            DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
        //        if (filter.DataNascita.HasValue)
        //            DatabaseSice.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, filter.DataNascita);
        //        if (filter.WithPIN.HasValue)
        //            DatabaseSice.AddInParameter(dbCommand, "@withPIN", DbType.Boolean, filter.WithPIN);
        //        if (filter.DataGenerazionePinDa.HasValue)
        //            DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinDa", DbType.DateTime,
        //                                        filter.DataGenerazionePinDa);
        //        if (filter.DataGenerazionePinA.HasValue)
        //            DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinA", DbType.DateTime,
        //                                        filter.DataGenerazionePinA);
        //    }
        //    IDataReader dr = DatabaseSice.ExecuteReader(dbCommand);
        //    while (dr.Read())
        //    {
        //        Lavoratore lav = new Lavoratore();
        //        if (!Convert.IsDBNull(dr["idLavoratore"]))
        //            lav.IdLavoratore = (int) dr["idLavoratore"];
        //        if (!Convert.IsDBNull(dr["cognome"]))
        //            lav.Cognome = (string) dr["cognome"];
        //        if (!Convert.IsDBNull(dr["nome"]))
        //            lav.Nome = (string) dr["nome"];
        //        if (!Convert.IsDBNull(dr["codiceFiscale"]))
        //            lav.CodiceFiscale = (string) dr["codiceFiscale"];
        //        if (!Convert.IsDBNull(dr["PIN"]))
        //            lav.Pin = (string) dr["PIN"];
        //        if (!Convert.IsDBNull(dr["dataGenerazionePIN"]))
        //            lav.DataGenerazionePIN = (DateTime) dr["dataGenerazionePIN"];
        //        lavoratori.Add(lav);
        //    }
        //    return lavoratori;
        //}

        /// <summary>
        /// Il filtro su nome, cognome e ragione sociale funziona come LIKE e i campi relativi accettano % in testa e/o coda
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public LavoratoriConImpresaCollection GetLavoratoriConImpresa(FilterLavoratore filter)
        {
            LavoratoriConImpresaCollection lavoratori = null;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectWithImpWithFilter"))
            {
                if (filter != null)
                {
                    if (filter.IdLavoratore.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, filter.IdLavoratore);
                    if (!string.IsNullOrEmpty(filter.Cognome))
                        DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, filter.Cognome);
                    if (!string.IsNullOrEmpty(filter.Nome))
                        DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, filter.Nome);
                    if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                        DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                    if (filter.DataNascita.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, filter.DataNascita);
                    if (filter.WithPIN.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@withPIN", DbType.Boolean, filter.WithPIN);
                    if (filter.DataGenerazionePinDa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinDa", DbType.DateTime,
                                                    filter.DataGenerazionePinDa);
                    if (filter.DataGenerazionePinA.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinA", DbType.DateTime,
                                                    filter.DataGenerazionePinA);
                    if (filter.IdImpresa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filter.IdImpresa);
                    if (!string.IsNullOrEmpty(filter.RagioneSociale))
                        DatabaseSice.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filter.RagioneSociale);
                }

                using (DataSet ds = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        lavoratori = new LavoratoriConImpresaCollection();

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            LavoratoreConImpresa lav = new LavoratoreConImpresa();

                            if (!Convert.IsDBNull(dr["idLavoratore"]))
                                lav.IdLavoratore = (int) dr["idLavoratore"];
                            if (!Convert.IsDBNull(dr["cognome"]))
                                lav.Cognome = (string) dr["cognome"];
                            if (!Convert.IsDBNull(dr["nome"]))
                                lav.Nome = (string) dr["nome"];
                            if (!Convert.IsDBNull(dr["codiceFiscale"]))
                                lav.CodiceFiscale = (string) dr["codiceFiscale"];
                            if (!Convert.IsDBNull(dr["indirizzoDenominazione"]))
                                lav.IndirizzoDenominazione = (string) dr["indirizzoDenominazione"];
                            if (!Convert.IsDBNull(dr["indirizzoComune"]))
                                lav.IndirizzoComune = (string) dr["indirizzoComune"];
                            if (!Convert.IsDBNull(dr["indirizzoCap"]))
                                lav.IndirizzoCAP = (string) dr["indirizzoCap"];
                            if (!Convert.IsDBNull(dr["indirizzoProvincia"]))
                                lav.IndirizzoProvincia = (string) dr["indirizzoProvincia"];
                            if (!Convert.IsDBNull(dr["PIN"]))
                                lav.Pin = (string) dr["PIN"];
                            if (!Convert.IsDBNull(dr["dataGenerazionePIN"]))
                                lav.DataGenerazionePIN = (DateTime) dr["dataGenerazionePIN"];
                            if (!Convert.IsDBNull(dr["idImpresa"]))
                                lav.IdImpresa = (int) dr["idImpresa"];
                            if (!Convert.IsDBNull(dr["ragioneSociale"]))
                                lav.RagioneSocialeImpresa = (string) dr["ragioneSociale"];
                            if (!Convert.IsDBNull(dr["dataNascita"]))
                                lav.DataNascita = (DateTime) dr["dataNascita"];

                            lavoratori.Add(lav);
                        }
                    }
                }
            }

            return lavoratori;
        }

        /// <summary>
        /// Il filtro su nome e cognome funziona come LIKE e i campi relativi accettano % in testa e/o coda
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public LavoratoriCollection GetLavoratoriConTel(FilterLavoratore filter)
        {
            LavoratoriCollection lavoratori = new LavoratoriCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectWithTelWithFilter"))
            {
                if (filter != null)
                {
                    if (filter.IdLavoratore.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, filter.IdLavoratore);
                    if (!String.IsNullOrEmpty(filter.Cognome))
                        DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, filter.Cognome);
                    if (!String.IsNullOrEmpty(filter.Nome))
                        DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, filter.Nome);
                    if (!String.IsNullOrEmpty(filter.CodiceFiscale))
                        DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                }

                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Lavoratore lav = new Lavoratore();

                        if (!Convert.IsDBNull(dr["idLavoratore"]))
                            lav.IdLavoratore = (int) dr["idLavoratore"];
                        if (!Convert.IsDBNull(dr["cognome"]))
                            lav.Cognome = (string) dr["cognome"];
                        if (!Convert.IsDBNull(dr["nome"]))
                            lav.Nome = (string) dr["nome"];
                        if (!Convert.IsDBNull(dr["codiceFiscale"]))
                            lav.CodiceFiscale = (string) dr["codiceFiscale"];
                        if (!Convert.IsDBNull(dr["numeroTelefono"]))
                            lav.Telefono = (string) dr["numeroTelefono"];

                        lavoratori.Add(lav);
                    }
                }
            }

            return lavoratori;
        }

        /// <summary>
        /// Il filtro su nome e cognome funziona come LIKE e i campi relativi accettano % in testa e/o coda
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public LavoratoriCollection GetLavoratoriConTelPrest(FilterLavoratorePrest filter)
        {
            LavoratoriCollection lavoratori = null;

            using (
                DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectWithTelPrestWithFilter")
                )
            {
                if (filter != null)
                {
                    if (filter.IdLavoratore.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, filter.IdLavoratore);
                    if (!string.IsNullOrEmpty(filter.Cognome))
                        DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, filter.Cognome);
                    if (!string.IsNullOrEmpty(filter.Nome))
                        DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, filter.Nome);
                    if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                        DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                    if (!String.IsNullOrEmpty(filter.StatoAssegno))
                        DatabaseSice.AddInParameter(dbCommand, "@statoAssegno", DbType.String, filter.StatoAssegno);
                    //uso DataGenerazionePin per velocit� - fa schifo, lo so...
                    if (filter.DataGenerazionePinDa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataUltimaNotificaDa", DbType.DateTime,
                                                    filter.DataGenerazionePinDa);
                    if (filter.DataGenerazionePinA.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataUltimaNotificaDa", DbType.DateTime,
                                                    filter.DataGenerazionePinA);
                }

                using (DataSet ds = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        lavoratori = new LavoratoriCollection();

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            Lavoratore lav = new Lavoratore();

                            if (!Convert.IsDBNull(dr["idLavoratore"]))
                                lav.IdLavoratore = (int) dr["idLavoratore"];
                            if (!Convert.IsDBNull(dr["cognome"]))
                                lav.Cognome = (string) dr["cognome"];
                            if (!Convert.IsDBNull(dr["nome"]))
                                lav.Nome = (string) dr["nome"];
                            if (!Convert.IsDBNull(dr["codiceFiscale"]))
                                lav.CodiceFiscale = (string) dr["codiceFiscale"];
                            if (!Convert.IsDBNull(dr["numeroTelefono"]))
                                lav.Telefono = (string) dr["numeroTelefono"];
                            if (!Convert.IsDBNull(dr["indirizzoDenominazione"]))
                                lav.IndirizzoDenominazione = (string) dr["indirizzoDenominazione"];
                            if (!Convert.IsDBNull(dr["indirizzoComune"]))
                                lav.IndirizzoComune = (string) dr["indirizzoComune"];
                            if (!Convert.IsDBNull(dr["indirizzoCAP"]))
                                lav.IndirizzoCAP = (string) dr["indirizzoCAP"];
                            if (!Convert.IsDBNull(dr["indirizzoProvincia"]))
                                lav.IndirizzoProvincia = (string) dr["indirizzoProvincia"];
                            //uso DataGenerazionePin per velocit� - fa schifo, lo so...
                            if (!Convert.IsDBNull(dr["dataUltimaNotifica"]))
                                lav.DataGenerazionePIN = (DateTime) dr["dataUltimaNotifica"];

                            lavoratori.Add(lav);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public bool RichiediAggiornamentoIndirizzoLavoratore(int idLavoratore, string indirizzoDenominazione,
                                                             string indirizzoCAP, string indirizzoProvincia,
                                                             string indirizzoComune)
        {
            bool ret = false;

            int numRow;
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriIndirizzoNotificheInsert")
                )
            {
                DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseSice.AddInParameter(dbCommand, "@indirizzoDenominazione", DbType.String, indirizzoDenominazione);
                DatabaseSice.AddInParameter(dbCommand, "@indirizzoCAP", DbType.String, indirizzoCAP);
                DatabaseSice.AddInParameter(dbCommand, "@indirizzoProvincia", DbType.String, indirizzoProvincia);
                DatabaseSice.AddInParameter(dbCommand, "@indirizzoComune", DbType.String, indirizzoComune);

                numRow = (int.Parse(DatabaseSice.ExecuteScalar(dbCommand).ToString()));
            }

            if (numRow > 0)
                ret = true;

            return ret;
        }

        public DataSet CaricaCodiciDismessi()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("CodiciDismessi");
            DataColumn dc1 = new DataColumn("codiceDismesso");
            DataColumn dc2 = new DataColumn("codiceValido");
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            DataRow dr = dt.NewRow();
            object[] val = new object[2];
            val[0] = 10;
            val[1] = 11;
            dr.ItemArray = val;
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);
            return ds;
        }

        public DataSet CaricaOperazioni(int codDismesso, int codValido)
        {
            DataSet ds;

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_CodiciDismessiReportOperazioni"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idLavoratoreVecchio", DbType.Int32, codDismesso);
                DatabaseSice.AddInParameter(dbCommand, "@idLavoratoreNuovo", DbType.Int32, codValido);

                ds = DatabaseSice.ExecuteDataSet(dbCommand);
            }

            return ds;
        }

        public ConsulentiCollection GetConsulenti(string ragioneSociale, string codiceFiscale, bool? conPIN,
                                                  DateTime? dal, DateTime? al, int? idConsulente)
        {
            ConsulentiCollection consulenti = new ConsulentiCollection();
            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_ConsulentiSelect"))
            {
                if (!string.IsNullOrEmpty(ragioneSociale))
                    DatabaseSice.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSociale);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseSice.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                if (conPIN.HasValue)
                    DatabaseSice.AddInParameter(comando, "@conPIN", DbType.Boolean, conPIN);
                if (conPIN.HasValue)
                    DatabaseSice.AddInParameter(comando, "@dal", DbType.DateTime, dal);
                if (conPIN.HasValue)
                    DatabaseSice.AddInParameter(comando, "@al", DbType.DateTime, al);
                if (idConsulente.HasValue)
                    DatabaseSice.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente.Value);

                using (IDataReader reader = DatabaseSice.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Consulente consulente = new Consulente();

                        consulente.IdConsulente = reader.GetInt32(reader.GetOrdinal("idConsulente"));

                        int tempOrdinal = reader.GetOrdinal("ragioneSociale");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.RagioneSociale = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("indirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Indirizzo = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("localita");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Comune = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Provincia = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("cap");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Cap = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("codiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.CodiceFiscale = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("nTelefono");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Telefono = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("PIN");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.PIN = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("dataGenerazionePIN");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.DataGenerazionePIN = reader.GetDateTime(tempOrdinal);

                        consulenti.Add(consulente);
                    }
                }
            }

            return consulenti;
        }

        public List<ConsulenteConCredenziali> GetConsulentiConCredenziali(ConsulenteFilter filter)
        {
            List<ConsulenteConCredenziali> consulenti = new List<ConsulenteConCredenziali>();
            using (DbCommand comando = DatabaseSice.GetStoredProcCommand("dbo.USP_ConsulentiSelectConCredenziali"))
            {
                if (!string.IsNullOrEmpty(filter.RagioneSociale))
                    DatabaseSice.AddInParameter(comando, "@ragioneSociale", DbType.String, filter.RagioneSociale);
                if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                    DatabaseSice.AddInParameter(comando, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                if (filter.ConPIN.HasValue)
                    DatabaseSice.AddInParameter(comando, "@conPIN", DbType.Boolean, filter.ConPIN);
                if (filter.ConPIN.HasValue)
                    DatabaseSice.AddInParameter(comando, "@dal", DbType.DateTime, filter.PinGeneratoDal);
                if (filter.ConPIN.HasValue)
                    DatabaseSice.AddInParameter(comando, "@al", DbType.DateTime, filter.PinGeneratoAl);
                if (filter.IdConsulente.HasValue)
                    DatabaseSice.AddInParameter(comando, "@idConsulente", DbType.Int32, filter.IdConsulente.Value);
                if (filter.IscrittoSitoWeb.HasValue)
                    DatabaseSice.AddInParameter(comando, "@iscrittoWeb", DbType.Boolean, filter.IscrittoSitoWeb.Value);

                using (IDataReader reader = DatabaseSice.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        ConsulenteConCredenziali consulente = new ConsulenteConCredenziali();

                        consulente.IdConsulente = reader.GetInt32(reader.GetOrdinal("idConsulente"));

                        int tempOrdinal = reader.GetOrdinal("ragioneSociale");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.RagioneSociale = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("indirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Indirizzo = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("localita");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Comune = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Provincia = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("cap");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Cap = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("codiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.CodiceFiscale = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("nTelefono");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.Telefono = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("PIN");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.PIN = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("dataGenerazionePIN");
                        if (!reader.IsDBNull(tempOrdinal))
                            consulente.DataGenerazionePIN = reader.GetDateTime(tempOrdinal);

                        if (!Convert.IsDBNull(reader["login"]))
                            consulente.Username = (string) reader["login"];

                        //if (!Convert.IsDBNull(reader["password"]))
                        //    consulente.Password = (string) reader["password"];

                        if (!Convert.IsDBNull(reader["idUtente"]))
                            consulente.IdUtente = (int) reader["idUtente"];

                        if (!Convert.IsDBNull(reader["eMail"]))
                            consulente.EMail = (string) reader["eMail"];

                        consulenti.Add(consulente);
                    }
                }
            }

            return consulenti;
        }

        public LavoratoriConImpresaSmsCollection GetLavoratoriConImpresaSms(FilterLavoratore filter)
        {
            LavoratoriConImpresaSmsCollection lavoratori = null;

            using (
                DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectWithImpSmsWithFilter"))
            {
                if (filter != null)
                {
                    if (filter.IdLavoratore.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, filter.IdLavoratore);
                    if (!string.IsNullOrEmpty(filter.Cognome))
                        DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, filter.Cognome);
                    if (!string.IsNullOrEmpty(filter.Nome))
                        DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, filter.Nome);
                    if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                        DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                    if (filter.DataNascita.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, filter.DataNascita);
                    if (filter.WithPIN.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@withPIN", DbType.Boolean, filter.WithPIN);
                    if (filter.DataGenerazionePinDa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinDa", DbType.DateTime,
                                                    filter.DataGenerazionePinDa);
                    if (filter.DataGenerazionePinA.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinA", DbType.DateTime,
                                                    filter.DataGenerazionePinA);
                    if (filter.IdImpresa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filter.IdImpresa);
                    if (!string.IsNullOrEmpty(filter.RagioneSociale))
                        DatabaseSice.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filter.RagioneSociale);
                }

                using (DataSet ds = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        lavoratori = new LavoratoriConImpresaSmsCollection();

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            LavoratoreConImpresaSms lav = new LavoratoreConImpresaSms();

                            if (!Convert.IsDBNull(dr["idLavoratore"]))
                                lav.IdLavoratore = (int) dr["idLavoratore"];
                            if (!Convert.IsDBNull(dr["cognome"]))
                                lav.Cognome = (string) dr["cognome"];
                            if (!Convert.IsDBNull(dr["nome"]))
                                lav.Nome = (string) dr["nome"];
                            if (!Convert.IsDBNull(dr["codiceFiscale"]))
                                lav.CodiceFiscale = (string) dr["codiceFiscale"];
                            if (!Convert.IsDBNull(dr["indirizzoDenominazione"]))
                                lav.IndirizzoDenominazione = (string) dr["indirizzoDenominazione"];
                            if (!Convert.IsDBNull(dr["indirizzoComune"]))
                                lav.IndirizzoComune = (string) dr["indirizzoComune"];
                            if (!Convert.IsDBNull(dr["indirizzoCap"]))
                                lav.IndirizzoCAP = (string) dr["indirizzoCap"];
                            if (!Convert.IsDBNull(dr["indirizzoProvincia"]))
                                lav.IndirizzoProvincia = (string) dr["indirizzoProvincia"];
                            if (!Convert.IsDBNull(dr["PIN"]))
                                lav.Pin = (string) dr["PIN"];
                            if (!Convert.IsDBNull(dr["dataGenerazionePIN"]))
                                lav.DataGenerazionePIN = (DateTime) dr["dataGenerazionePIN"];
                            if (!Convert.IsDBNull(dr["idImpresa"]))
                                lav.IdImpresa = (int) dr["idImpresa"];
                            if (!Convert.IsDBNull(dr["ragioneSociale"]))
                                lav.RagioneSocialeImpresa = (string) dr["ragioneSociale"];
                            if (!Convert.IsDBNull(dr["numeroTelefono"]))
                                lav.NumeroTelefono = (string) dr["numeroTelefono"];

                            lavoratori.Add(lav);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public List<LavoratoreConImpresaSmsCredenziali> GetLavoratoriConImpresaSmsCredenziali(FilterLavoratore filter)
        {
            List<LavoratoreConImpresaSmsCredenziali> lavoratori = new List<LavoratoreConImpresaSmsCredenziali>();

            using (
                DbCommand dbCommand =
                    DatabaseSice.GetStoredProcCommand("dbo.USP_LavoratoriSelectWithImpSmsCredenzialiWithFilter"))
            {
                if (filter != null)
                {
                    if (filter.IdLavoratore.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, filter.IdLavoratore);
                    if (!string.IsNullOrEmpty(filter.Cognome))
                        DatabaseSice.AddInParameter(dbCommand, "@cognome", DbType.String, filter.Cognome);
                    if (!string.IsNullOrEmpty(filter.Nome))
                        DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, filter.Nome);
                    if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                        DatabaseSice.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                    if (filter.DataNascita.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, filter.DataNascita);
                    if (filter.WithPIN.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@withPIN", DbType.Boolean, filter.WithPIN);
                    if (filter.DataGenerazionePinDa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinDa", DbType.DateTime,
                                                    filter.DataGenerazionePinDa);
                    if (filter.DataGenerazionePinA.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@dataGenerazionePinA", DbType.DateTime,
                                                    filter.DataGenerazionePinA);
                    if (filter.IdImpresa.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, filter.IdImpresa);
                    if (!string.IsNullOrEmpty(filter.RagioneSociale))
                        DatabaseSice.AddInParameter(dbCommand, "@ragioneSociale", DbType.String, filter.RagioneSociale);
                    if (filter.IscrittoSitoWeb.HasValue)
                        DatabaseSice.AddInParameter(dbCommand, "@iscrittoWeb", DbType.Boolean,
                                                    filter.IscrittoSitoWeb.Value);
                    if (!String.IsNullOrEmpty(filter.NumeroCelluare))
                        DatabaseSice.AddInParameter(dbCommand, "@numeroCellulare", DbType.String, filter.NumeroCelluare);
                }

                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        LavoratoreConImpresaSmsCredenziali lav = new LavoratoreConImpresaSmsCredenziali();

                        if (!Convert.IsDBNull(dr["idLavoratore"]))
                            lav.IdLavoratore = (int) dr["idLavoratore"];
                        if (!Convert.IsDBNull(dr["cognome"]))
                            lav.Cognome = (string) dr["cognome"];
                        if (!Convert.IsDBNull(dr["nome"]))
                            lav.Nome = (string) dr["nome"];
                        if (!Convert.IsDBNull(dr["codiceFiscale"]))
                            lav.CodiceFiscale = (string) dr["codiceFiscale"];
                        if (!Convert.IsDBNull(dr["indirizzoDenominazione"]))
                            lav.IndirizzoDenominazione = (string) dr["indirizzoDenominazione"];
                        if (!Convert.IsDBNull(dr["indirizzoComune"]))
                            lav.IndirizzoComune = (string) dr["indirizzoComune"];
                        if (!Convert.IsDBNull(dr["indirizzoCap"]))
                            lav.IndirizzoCAP = (string) dr["indirizzoCap"];
                        if (!Convert.IsDBNull(dr["indirizzoProvincia"]))
                            lav.IndirizzoProvincia = (string) dr["indirizzoProvincia"];
                        if (!Convert.IsDBNull(dr["PIN"]))
                            lav.Pin = (string) dr["PIN"];
                        if (!Convert.IsDBNull(dr["dataGenerazionePIN"]))
                            lav.DataGenerazionePIN = (DateTime) dr["dataGenerazionePIN"];
                        if (!Convert.IsDBNull(dr["idImpresa"]))
                            lav.IdImpresa = (int) dr["idImpresa"];
                        if (!Convert.IsDBNull(dr["ragioneSociale"]))
                            lav.RagioneSocialeImpresa = (string) dr["ragioneSociale"];
                        if (!Convert.IsDBNull(dr["numeroTelefono"]))
                            lav.NumeroTelefono = (string) dr["numeroTelefono"];
                        if (!Convert.IsDBNull(dr["login"]))
                            lav.Username = (string) dr["login"];
                        //if (!Convert.IsDBNull(dr["password"]))
                        //    lav.Password = (string) dr["password"];
                        if (!Convert.IsDBNull(dr["dataInvioSmsPin"]))
                            lav.DataInvioSmsPin = (DateTime) dr["dataInvioSmsPin"];

                        lavoratori.Add(lav);
                    }
                }
            }

            return lavoratori;
        }

        public DateTime GetScadenzaPasswordFromUsername(string username)
        {
            DateTime ret;

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiSelectDataScadenzaFromUsername"))
            {
                DatabaseSice.AddInParameter(command, "@username", DbType.String, username);
                var scadenza = DatabaseSice.ExecuteScalar(command) as string;
                DateTime scad;
                if (DateTime.TryParse(scadenza, out scad))
                {
                    return scad;
                }
                return new DateTime(2100, 12, 31);
            }

            return ret;
        }

        public void AggiornaScadenzaPassword(int idUtente, int numeroMesi)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiUpdateDataScadenzaPasswordByIdUtente"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseSice.AddInParameter(dbCommand, "@numeroMesi", DbType.Int32, numeroMesi);
                DatabaseSice.ExecuteNonQuery(dbCommand);
            }
        }
    }
}