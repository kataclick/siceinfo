using System;
using System.Collections.Specialized;
using System.Web.Security;
using TBridge.Cemi.GestioneUtenti.Type.Collections;

namespace TBridge.Cemi.GestioneUtenti.Data.Providers
{
    internal class SQLServerRoleProvider : RoleProvider
    {
        private GestioneUtentiAccess gestioneUtentiDataAccess;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
                throw new ArgumentNullException("config");

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
                name = "SQLServerRoleProvider";

            //Inizializzo lo strato di accesso al db
            gestioneUtentiDataAccess = new GestioneUtentiAccess();

            // Call the base class's Initialize method
            base.Initialize(name, config);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override bool IsUserInRole(string username, string roleName)
        {
            try
            {
                // Validate input parameters
                if (username == null || roleName == null)
                    throw new ArgumentNullException();

                if (username == String.Empty || roleName == string.Empty)
                    throw new ArgumentException();

                // Determine whether the user is in the specified role
                FunzionalitaCollection funzionalitaUtente =
                    gestioneUtentiDataAccess.GetFunzionalitaUtente(username);

                return funzionalitaUtente.Contains(roleName);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public override string[] GetRolesForUser(string username)
        {
            try
            {
                // Validate input parameters
                if (username == null)
                    throw new ArgumentNullException();

                if (username == string.Empty)
                    throw new ArgumentException();

                //TODO ACTIVITY TRACKING

                // Return role names
                return (gestioneUtentiDataAccess.GetFunzionalitaUtente(username)).ToArray();
            }
            catch (Exception ex)
            {
                //TODO ACTIVITY TRACKING
                return new string[0];
            }
        }

        #region Metodi non supportati

        public override string ApplicationName
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public override string[] GetUsersInRole(string roleName)
        {
            // Validate input parameters
            if (roleName == null)
                throw new ArgumentNullException();

            if (roleName == string.Empty)
                throw new ArgumentException();

            //TODO da implementare
            throw new NotSupportedException();
        }

        public override string[] GetAllRoles()
        {
            //int i = 0;
            string[] roles = new string[1 /*_RolesAndUsers.Count*/];

            //foreach (KeyValuePair<string, string[]> pair in _RolesAndUsers)
            //    roles[i++] = pair.Key;

            return roles;
        }

        public override bool RoleExists(string roleName)
        {
            // Validate input parameters
            if (roleName == null)
                throw new ArgumentNullException();

            if (roleName == string.Empty)
                throw new ArgumentException();

            // Determine whether the role exists

            //TODO da implementare
            return false;
        }

        // TODO RoleProvider properties - DA IMPLEMENTARE

        public override void CreateRole(string roleName)
        {
            //TODO da implementare
            throw new NotSupportedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            //TODO da implementare
            throw new NotSupportedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            //TODO da implementare
            throw new NotSupportedException();
        }

        //Direi che per il momento non ci serve
        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotSupportedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            //TODO da implementare
            throw new NotSupportedException();
        }

        #endregion

        //TODO metodi per gestire le funzionalitą e per associarle ai ruoli!
    }
}