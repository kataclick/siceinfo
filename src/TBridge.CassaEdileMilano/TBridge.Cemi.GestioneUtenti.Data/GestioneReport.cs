﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.GestioneUtenti.Type.Collections;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Enums;
using TBridge.Cemi.Type.Entities;
using CassaEdile = TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile;
using Impresa = TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa;

namespace TBridge.Cemi.GestioneUtenti.Data
{
    public class GestioneReport
    {
        public GestioneReport()
        {
            DatabaseSice = DatabaseFactory.CreateDatabase("CEMI");
        }

        private Database DatabaseSice { get; set; }

        public Report GetReport(string nomeReport)
        {
            Report report = new Report();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ReportSelectByNome"))
            {
                DatabaseSice.AddInParameter(command, "@nome", DbType.String, nomeReport);
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        report.Id = (int) reader["idReport"];
                        report.Nome = (string) reader["nome"];
                        report.Path = (string) reader["path"];
                        report.Theme = (string) reader["theme"];
                    }
                }
            }

            return report;
        }

        public ReportCollection GetListaReport()
        {
            ReportCollection listaReport = new ReportCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_ReportSelect"))
            {
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Report report = new Report();
                        report.Id = (int) reader["idReport"];
                        report.Nome = (string) reader["nome"];
                        report.Path = (string) reader["path"];
                        report.Theme = (string) reader["theme"];

                        listaReport.Add(report);
                    }
                }
            }

            return listaReport;
        }

        public ReportCollection GetListaReport(Int32 idUtente)
        {
            ReportCollection listaReport = new ReportCollection();

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_ReportSelectByIdUtente"))
            {
                DatabaseSice.AddInParameter(command,"@idUtente",DbType.Int32,idUtente);
                
                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Report report = new Report();
                        report.Id = (int)reader["idReport"];
                        report.Nome = (string)reader["nome"];
                        report.Path = (string)reader["path"];
                        report.Theme = (string)reader["theme"];

                        listaReport.Add(report);
                    }
                }
            }

            return listaReport;
        }

        public bool Autorizzato(int idUtente, string nomeReport)
        {
            bool ret = false;

            using (DbCommand command = DatabaseSice.GetStoredProcCommand("USP_Utenti_ReportSelect"))
            {
                DatabaseSice.AddInParameter(command, "@idUtente", DbType.Int32, idUtente);
                DatabaseSice.AddInParameter(command, "@nome", DbType.String, nomeReport);

                using (IDataReader reader = DatabaseSice.ExecuteReader(command))
                {
                    ret = reader.Read();
                }
            }

            return ret;
        }

        private void CancellaReportUtente(int idUtente, DbTransaction transaction)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_ReportDeleteAll"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseSice.ExecuteNonQuery(dbCommand, transaction);
            }
        }

        public int AssociaUtenteReport(int idUtente, int idReport)
        {
            int rtn = 0;

            DbConnection connection = null;

            DbTransaction transaction = null;

            try
            {
                connection = DatabaseSice.CreateConnection();

                connection.Open();

                transaction = connection.BeginTransaction();

                try
                {
                    bool inserimentoReportEffettuato = true;

                    object obj;
                    using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_ReportInsert"))
                    {
                        DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                        DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);

                        obj = DatabaseSice.ExecuteScalar(dbCommand, transaction);
                    }

                    if (obj != null)
                        rtn = (int) obj;
                    else rtn = -1;

                    if (rtn == -1)
                    {
                        inserimentoReportEffettuato = false;
                    }


                    if (inserimentoReportEffettuato)
                        transaction.Commit();
                    else
                        transaction.Rollback();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                if (connection != null)
                    connection.Close();
            }

            return rtn;
        }

        public void DeleteUtenteReport(int? idUtente, int idReport)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_Utenti_ReportDelete"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);

                DatabaseSice.ExecuteNonQuery(dbCommand);
            }
        }

        public void DeleteReport(int idReport)
        {
            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ReportDelete"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);

                DatabaseSice.ExecuteNonQuery(dbCommand);
            }
        }

        #region GetUtentiReport

        public LavoratoriCollection GetUtentiReportLavoratori(int? idReport)
        {
            LavoratoriCollection lavoratori = new LavoratoriCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportLavoratoriSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetLavoratori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetLavoratori != null && dataSetLavoratori.Tables.Count > 0 &&
                        dataSetLavoratori.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetLavoratori.Tables[0].Rows.Count; i++)
                        {
                            Lavoratore lavoratore = new Lavoratore(
                                (Int32) dataSetLavoratori.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetLavoratori.Tables[0].Rows[i]["login"]);

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idLavoratore"]))
                                lavoratore.IdLavoratore = (int) dataSetLavoratori.Tables[0].Rows[i]["idLavoratore"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["nome"]))
                                lavoratore.Nome = (string) dataSetLavoratori.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["cognome"]))
                                lavoratore.Cognome = (string) dataSetLavoratori.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["codiceFiscale"]))
                                lavoratore.CodiceFiscale = (string) dataSetLavoratori.Tables[0].Rows[i]["codiceFiscale"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["dataNascita"]))
                                lavoratore.DataNascita = (DateTime) dataSetLavoratori.Tables[0].Rows[i]["dataNascita"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["luogoNascita"]))
                                lavoratore.LuogoNascita = (string) dataSetLavoratori.Tables[0].Rows[i]["luogoNascita"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idNazionalita"]))
                                lavoratore.IdNazionalita = (string) dataSetLavoratori.Tables[0].Rows[i]["idNazionalita"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["sesso"]))
                                lavoratore.Sesso = (string) dataSetLavoratori.Tables[0].Rows[i]["sesso"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idStatoCivile"]))
                                lavoratore.IdStatoCivile = (string) dataSetLavoratori.Tables[0].Rows[i]["idStatoCivile"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoDenominazione"]))
                                lavoratore.IndirizzoDenominazione =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoDenominazione"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoComune"]))
                                lavoratore.IndirizzoComune =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoComune"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoCAP"]))
                                lavoratore.IndirizzoCAP = (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoCAP"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["indirizzoProvincia"]))
                                lavoratore.IndirizzoProvincia =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["indirizzoProvincia"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["telefono"]))
                                lavoratore.Telefono = (string) dataSetLavoratori.Tables[0].Rows[i]["telefono"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["cellulare"]))
                                lavoratore.Cellulare = (string) dataSetLavoratori.Tables[0].Rows[i]["cellulare"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["eMail"]))
                                lavoratore.Email = (string) dataSetLavoratori.Tables[0].Rows[i]["eMail"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["dataDecesso"]))
                                lavoratore.DataDecesso = (DateTime) dataSetLavoratori.Tables[0].Rows[i]["dataDecesso"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idTipoDecesso"]))
                                lavoratore.IdTipoDecesso = (string) dataSetLavoratori.Tables[0].Rows[i]["idTipoDecesso"];

                            if (!Convert.IsDBNull(dataSetLavoratori.Tables[0].Rows[i]["idTipoPagamento"]))
                                lavoratore.IdTipoPagamento =
                                    (string) dataSetLavoratori.Tables[0].Rows[i]["idTipoPagamento"];

                            lavoratori.Add(lavoratore);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public ImpreseCollection GetUtentiReportImprese(int? idReport)
        {
            ImpreseCollection imprese = new ImpreseCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportImpreseSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetImprese = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetImprese != null && dataSetImprese.Tables.Count > 0 &&
                        dataSetImprese.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetImprese.Tables[0].Rows.Count; i++)
                        {
                            Impresa impresa = new Impresa(
                                (Int32) dataSetImprese.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetImprese.Tables[0].Rows[i]["login"]);

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idImpresa"]))
                                impresa.IdImpresa = (int) dataSetImprese.Tables[0].Rows[i]["idImpresa"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["ragioneSociale"]))
                                impresa.RagioneSociale = (string) dataSetImprese.Tables[0].Rows[i]["ragioneSociale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceFiscale"]))
                                impresa.CodiceFiscale = (string) dataSetImprese.Tables[0].Rows[i]["codiceFiscale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["partitaIVA"]))
                                impresa.PartitaIVA = (string) dataSetImprese.Tables[0].Rows[i]["partitaIVA"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idTipoImpresa"]))
                                impresa.IdTipoImpresa = (string) dataSetImprese.Tables[0].Rows[i]["idTipoImpresa"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceContratto"]))
                                impresa.CodiceContratto = (string) dataSetImprese.Tables[0].Rows[i]["codiceContratto"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceINAIL"]))
                                impresa.CodiceINAIL = (string) dataSetImprese.Tables[0].Rows[i]["codiceINAIL"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["codiceINPS"]))
                                impresa.CodiceINAIL = (string) dataSetImprese.Tables[0].Rows[i]["codiceINPS"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["numeroIscrizioneCCIAA"]))
                                impresa.NumeroIscrizioneCCIAA =
                                    (int) dataSetImprese.Tables[0].Rows[i]["numeroIscrizioneCCIAA"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idAttivitaISTAT"]))
                                impresa.IdAttivitaISTAT = (string) dataSetImprese.Tables[0].Rows[i]["idAttivitaISTAT"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["idNaturaGiuridica"]))
                                impresa.IdNaturaGiuridica =
                                    (string) dataSetImprese.Tables[0].Rows[i]["idNaturaGiuridica"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["indirizzoSedeLegale"]))
                                impresa.IndirizzoSedeLegale =
                                    (string) dataSetImprese.Tables[0].Rows[i]["indirizzoSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["capSedeLegale"]))
                                impresa.CapSedeLegale = (string) dataSetImprese.Tables[0].Rows[i]["capSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["localitaSedeLegale"]))
                                impresa.LocalitaSedeLegale =
                                    (string) dataSetImprese.Tables[0].Rows[i]["localitaSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["provinciaSedeLegale"]))
                                impresa.ProvinciaSedeLegale =
                                    (string) dataSetImprese.Tables[0].Rows[i]["provinciaSedeLegale"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["indirizzoSedeAmministrazione"]))
                                impresa.IndirizzoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["indirizzoSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["capSedeAmministrazione"]))
                                impresa.CapSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["capSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["localitaSedeAmministrazione"]))
                                impresa.LocalitaSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["localitaSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["provinciaSedeAmministrazione"]))
                                impresa.PressoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["provinciaSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"]))
                                impresa.PressoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"]))
                                impresa.PressoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["pressoSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["telefonosedeAmministrazione"]))
                                impresa.TelefonoSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["telefonosedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["faxSedeAmministrazione"]))
                                impresa.FaxSedeAmministrazione =
                                    (string) dataSetImprese.Tables[0].Rows[i]["faxSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["eMailSedeAmministrazione"]))
                                impresa.Email = (string) dataSetImprese.Tables[0].Rows[i]["eMailSedeAmministrazione"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["sitoWeb"]))
                                impresa.SitoWeb = (string) dataSetImprese.Tables[0].Rows[i]["sitoWeb"];

                            if (!Convert.IsDBNull(dataSetImprese.Tables[0].Rows[i]["cellulare"]))
                                impresa.Cellulare = (string) dataSetImprese.Tables[0].Rows[i]["cellulare"];

                            imprese.Add(impresa);
                        }
                    }
                }
            }

            return imprese;
        }

        public ConsulentiCollection GetUtentiReportConsulenti(int? idReport)
        {
            ConsulentiCollection consulenti = new ConsulentiCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportConsulentiSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (IDataReader dr = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (dr.Read())
                    {
                        Consulente consulente = new Consulente(
                            (Int32) dr["idUtente"],
                            (String) dr["login"]);

                        if (dr["idConsulente"] != DBNull.Value)
                            consulente.IdConsulente = (int) dr["idConsulente"];

                        if (dr["ragioneSociale"] != DBNull.Value)
                            consulente.RagioneSociale = dr["ragioneSociale"] as string;

                        if (dr["codiceFiscale"] != DBNull.Value)
                            consulente.CodiceFiscale = dr["codiceFiscale"] as string;

                        if (dr["indirizzo"] != DBNull.Value)
                            consulente.Indirizzo = dr["indirizzo"] as string;

                        if (dr["cap"] != DBNull.Value)
                            consulente.Cap = dr["cap"] as string;

                        if (dr["localita"] != DBNull.Value)
                            consulente.Comune = dr["localita"] as string;

                        if (dr["provincia"] != DBNull.Value)
                            consulente.Provincia = dr["provincia"] as string;

                        if (dr["nTelefono"] != DBNull.Value)
                            consulente.Telefono = dr["nTelefono"] as string;

                        if (dr["nFax"] != DBNull.Value)
                            consulente.Fax = dr["nFax"] as string;

                        if (dr["nCellulare"] != DBNull.Value)
                            consulente.Cellulare = dr["nCellulare"] as string;

                        if (dr["eMail"] != DBNull.Value)
                            consulente.EMail = dr["eMail"] as string;

                        consulenti.Add(consulente);
                    }
                }
            }

            return consulenti;
        }

        public DipendentiCollection GetUtentiReportDipendenti(int? idReport)
        {
            DipendentiCollection dipendenti = new DipendentiCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportDipendentiSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetDipendenti = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetDipendenti != null && dataSetDipendenti.Tables.Count > 0 &&
                        dataSetDipendenti.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetDipendenti.Tables[0].Rows.Count; i++)
                        {
                            Dipendente dipendente = new Dipendente(
                                (Int32) dataSetDipendenti.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetDipendenti.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["idDipendente"]))
                                dipendente.IdDipendente = (int) dataSetDipendenti.Tables[0].Rows[i]["idDipendente"];

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["nome"]))
                                dipendente.Nome = (string) dataSetDipendenti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["cognome"]))
                                dipendente.Cognome = (string) dataSetDipendenti.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetDipendenti.Tables[0].Rows[i]["eMail"]))
                                dipendente.EMail = (string) dataSetDipendenti.Tables[0].Rows[i]["eMail"];

                            dipendenti.Add(dipendente);
                        }
                    }
                }
            }

            return dipendenti;
        }

        public IspettoriCollection GetUtentiReportIspettori(int? idReport)
        {
            IspettoriCollection ispettori = new IspettoriCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportIspettoriSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetIspettori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetIspettori != null && dataSetIspettori.Tables.Count > 0 &&
                        dataSetIspettori.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetIspettori.Tables[0].Rows.Count; i++)
                        {
                            Ispettore ispettore = new Ispettore(
                                (Int32) dataSetIspettori.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetIspettori.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["idIspettore"]))
                                ispettore.IdIspettore = (int) dataSetIspettori.Tables[0].Rows[i]["idIspettore"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["nome"]))
                                ispettore.Nome = (string) dataSetIspettori.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["cognome"]))
                                ispettore.Cognome = (string) dataSetIspettori.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["idCantieriZona"]))
                                ispettore.IdCantieriZona = (int) dataSetIspettori.Tables[0].Rows[i]["idCantieriZona"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["descrizioneZona"]))
                                ispettore.DescrizioneZona =
                                    (string) dataSetIspettori.Tables[0].Rows[i]["descrizioneZona"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["nomeZona"]))
                                ispettore.NomeZona = (string) dataSetIspettori.Tables[0].Rows[i]["nomeZona"];

                            if (!Convert.IsDBNull(dataSetIspettori.Tables[0].Rows[i]["operativo"]))
                                ispettore.Operativo = (bool) dataSetIspettori.Tables[0].Rows[i]["operativo"];

                            ispettori.Add(ispettore);
                        }
                    }
                }
            }

            return ispettori;
        }

        public EsattoriCollection GetUtentiReportEsattori(int? idReport)
        {
            EsattoriCollection esattori = new EsattoriCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportEsattoriSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetEsattori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetEsattori != null && dataSetEsattori.Tables.Count > 0 &&
                        dataSetEsattori.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetEsattori.Tables[0].Rows.Count; i++)
                        {
                            Esattore esattore = new Esattore(
                                (Int32) dataSetEsattori.Tables[0].Rows[i]["idUtente"],
                                (String) dataSetEsattori.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetEsattori.Tables[0].Rows[i]["idEsattore"]))
                                esattore.IdEsattore = (string) dataSetEsattori.Tables[0].Rows[i]["idEsattore"];

                            if (!Convert.IsDBNull(dataSetEsattori.Tables[0].Rows[i]["nome"]))
                                esattore.Nome = (string) dataSetEsattori.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetEsattori.Tables[0].Rows[i]["cognome"]))
                                esattore.Cognome = (string) dataSetEsattori.Tables[0].Rows[i]["cognome"];

                            esattori.Add(esattore);
                        }
                    }
                }
            }

            return esattori;
        }

        public FornitoriCollection GetUtentiReportFornitori(int? idReport)
        {
            FornitoriCollection fornitori = new FornitoriCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportFornitoriSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetFornitori = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetFornitori != null && dataSetFornitori.Tables.Count > 0 &&
                        dataSetFornitori.Tables[0].Rows.Count > 0)
                    {
                        fornitori = new FornitoriCollection();

                        for (int i = 0; i < dataSetFornitori.Tables[0].Rows.Count; i++)
                        {
                            Fornitore fornitore = new Fornitore(
                                (int) dataSetFornitori.Tables[0].Rows[i]["idUtente"],
                                (string) dataSetFornitori.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["idFornitore"]))
                                fornitore.IdFornitore = (int) dataSetFornitori.Tables[0].Rows[i]["idFornitore"];

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["ragionesociale"]))
                                fornitore.RagioneSociale = (string) dataSetFornitori.Tables[0].Rows[i]["ragionesociale"];

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["codice"]))
                                fornitore.Codice = (string) dataSetFornitori.Tables[0].Rows[i]["codice"];

                            if (!Convert.IsDBNull(dataSetFornitori.Tables[0].Rows[i]["eMail"]))
                                fornitore.EMail = (string) dataSetFornitori.Tables[0].Rows[i]["eMail"];

                            fornitori.Add(fornitore);
                        }
                    }
                }
            }

            return fornitori;
        }

        public SindacalistiCollection GetUtentiReportSindacalisti(int? idReport)
        {
            SindacalistiCollection sindacalisti = new SindacalistiCollection();
            using (
                DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportSindacalistiSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetSindacalisti = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    Sindacalista sindacalista;

                    if (dataSetSindacalisti != null && dataSetSindacalisti.Tables.Count > 0 &&
                        dataSetSindacalisti.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dataSetSindacalisti.Tables[0].Rows.Count; i++)
                        {
                            sindacalista = new Sindacalista(
                                (int) dataSetSindacalisti.Tables[0].Rows[i]["idUtente"],
                                (string) dataSetSindacalisti.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idSindacalista"]))
                                sindacalista.Id = (int) dataSetSindacalisti.Tables[0].Rows[i]["idSindacalista"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["nome"]))
                                sindacalista.Nome = (string) dataSetSindacalisti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["cognome"]))
                                sindacalista.Cognome = (string) dataSetSindacalisti.Tables[0].Rows[i]["cognome"];

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idSindacato"]))
                            {
                                if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["descrizioneSindacato"]))
                                {
                                    Sindacato sindacato =
                                        new Sindacato((string) dataSetSindacalisti.Tables[0].Rows[i]["idSindacato"],
                                                      (string)
                                                      dataSetSindacalisti.Tables[0].Rows[i]["descrizioneSindacato"]);

                                    sindacalista.Sindacato = sindacato;
                                }
                            }

                            if (!Convert.IsDBNull(dataSetSindacalisti.Tables[0].Rows[i]["idComprensorioSindacale"]))
                            {
                                ComprensorioSindacale comprensorio = null;

                                if (
                                    !Convert.IsDBNull(
                                        dataSetSindacalisti.Tables[0].Rows[i]["descrizioneComprensorioSindacale"]))
                                {
                                    comprensorio =
                                        new ComprensorioSindacale(
                                            (string) dataSetSindacalisti.Tables[0].Rows[i]["idComprensorioSindacale"],
                                            (string)
                                            dataSetSindacalisti.Tables[0].Rows[i]["descrizioneComprensorioSindacale"]);
                                }

                                sindacalista.ComprensorioSindacale = comprensorio;
                            }
                            sindacalisti.Add(sindacalista);
                        }
                    }
                }
            }

            return sindacalisti;
        }

        public ASLCollection GetUtentiReportAsl(int? idReport)
        {
            ASLCollection asles = new ASLCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportASLSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ASL asl = new ASL(
                            (int) reader["idUtente"],
                            (string) reader["login"]
                            );
                        asles.Add(asl);

                        asl.IdASLUtente = (int) reader["idASL"];
                        if (!Convert.IsDBNull(reader["cognome"]))
                            asl.Cognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            asl.Nome = (string) reader["nome"];

                        asl.IdASL = (int) reader["idASL"];
                        asl.Codice = (string) reader["codice"];
                        if (!Convert.IsDBNull(reader["descrizione"]))
                            asl.Descrizione = (string) reader["descrizione"];
                    }
                }
            }

            return asles;
        }

        public CommittenteCollection GetUtentiReportCommittenti(int? idReport)
        {
            CommittenteCollection committenti = new CommittenteCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportCommittentiSelect")
                )
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        Committente committente = new Committente(
                            (Int32) reader["idUtente"],
                            (String) reader["login"]
                            );
                        committenti.Add(committente);

                        if (!Convert.IsDBNull(reader["cognome"]))
                            committente.Cognome = (String) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            committente.Nome = (String) reader["nome"];
                        if (!Convert.IsDBNull(reader["ragioneSociale"]))
                            committente.RagioneSociale = (String) reader["ragioneSociale"];
                        if (!Convert.IsDBNull(reader["partitaIva"]))
                            committente.PartitaIva = (String) reader["partitaIva"];
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            committente.CodiceFiscale = (String) reader["codiceFiscale"];
                        if (!Convert.IsDBNull(reader["indirizzo"]))
                            committente.Indirizzo = (String) reader["indirizzo"];
                        if (!Convert.IsDBNull(reader["comune"]))
                            committente.Comune = (Int64) reader["comune"];
                        if (!Convert.IsDBNull(reader["provincia"]))
                            committente.Provincia = (Int32) reader["provincia"];
                        if (!Convert.IsDBNull(reader["cap"]))
                            committente.Cap = (String) reader["cap"];
                        if (!Convert.IsDBNull(reader["telefono"]))
                            committente.Telefono = (String) reader["telefono"];
                        if (!Convert.IsDBNull(reader["fax"]))
                            committente.Fax = (String) reader["fax"];
                        if (!Convert.IsDBNull(reader["email"]))
                            committente.Email = (String) reader["email"];
                        if (!Convert.IsDBNull(reader["tipologia"]))
                            committente.Tipologia =
                                (TipologiaCommittente)
                                Enum.Parse(typeof (TipologiaCommittente), reader["tipologia"].ToString());
                    }
                }
            }

            return committenti;
        }

        public OspitiCollection GetUtentiReportOspiti(int? idReport)
        {
            OspitiCollection ospiti = new OspitiCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportOspitiSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (DataSet dataSetOspiti = DatabaseSice.ExecuteDataSet(dbCommand))
                {
                    if (dataSetOspiti != null && dataSetOspiti.Tables.Count > 0 &&
                        dataSetOspiti.Tables[0].Rows.Count > 0)
                    {
                        ospiti = new OspitiCollection();

                        for (int i = 0; i < dataSetOspiti.Tables[0].Rows.Count; i++)
                        {
                            Ospite ospite = new Ospite(
                                (int) dataSetOspiti.Tables[0].Rows[i]["idUtente"],
                                (string) dataSetOspiti.Tables[0].Rows[i]["login"]
                                );

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["idOspite"]))
                                ospite.IdOspite = (int) dataSetOspiti.Tables[0].Rows[i]["idOspite"];

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["nome"]))
                                ospite.Nome = (string) dataSetOspiti.Tables[0].Rows[i]["nome"];

                            if (!Convert.IsDBNull(dataSetOspiti.Tables[0].Rows[i]["cognome"]))
                                ospite.Cognome = (string) dataSetOspiti.Tables[0].Rows[i]["cognome"];

                            ospiti.Add(ospite);
                        }
                    }
                }
            }

            return ospiti;
        }

        public CasseEdiliCollection GetUtentiReportCasseEdili(int? idReport)
        {
            CasseEdiliCollection casseEdili = new CasseEdiliCollection();

            using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_UtentiReportCasseEdiliSelect"))
            {
                DatabaseSice.AddInParameter(dbCommand, "@idReport", DbType.Int32, idReport);
                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile(
                            (int) reader["idUtente"],
                            (string) reader["login"]
                            );
                        casseEdili.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        if (reader["descrizione"] != DBNull.Value)
                            cassaEdile.Descrizione = (string) reader["descrizione"];
                    }
                }
            }

            return casseEdili;
        }

        #endregion

        public int InsertReport(Report report)
        {
            int rtn = 0;

            DbConnection connection = null;

            DbTransaction transaction = null;

            try
            {
                connection = DatabaseSice.CreateConnection();

                connection.Open();

                transaction = connection.BeginTransaction();

                try
                {
                    bool inserimentoReportEffettuato = true;

                    object obj;
                    using (DbCommand dbCommand = DatabaseSice.GetStoredProcCommand("dbo.USP_ReportInsert"))
                    {
                        DatabaseSice.AddInParameter(dbCommand, "@nome", DbType.String, report.Nome);
                        DatabaseSice.AddInParameter(dbCommand, "@path", DbType.String, report.Path);
                        DatabaseSice.AddInParameter(dbCommand, "@theme", DbType.String, report.Theme);

                        obj = DatabaseSice.ExecuteScalar(dbCommand, transaction);
                    }

                    if (obj != null)
                        rtn = (int)obj;
                    else rtn = -1;

                    if (rtn == -1)
                    {
                        inserimentoReportEffettuato = false;
                    }


                    if (inserimentoReportEffettuato)
                        transaction.Commit();
                    else
                        transaction.Rollback();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                    transaction.Rollback();

                if (connection != null)
                    connection.Close();
            }

            return rtn;
        }
    }
}