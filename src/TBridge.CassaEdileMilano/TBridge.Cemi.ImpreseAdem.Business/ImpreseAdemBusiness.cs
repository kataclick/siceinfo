using TBridge.Cemi.ImpreseAdemp.Data;
using TBridge.Cemi.ImpreseAdemp.Type.Collections;
using TBridge.Cemi.ImpreseAdemp.Type.Entities;
using TBridge.Cemi.ImpreseAdemp.Type.Filters;

namespace TBridge.Cemi.ImpreseAdem.Business
{
    public class ImpreseAdemBusiness
    {
        private readonly ImpreseAdemDataAccess dataAccess = new ImpreseAdemDataAccess();

        public EccezioneListaCollection GetEccezioniListaImpreseAdempienti(EccezioneListaFilter filtro)
        {
            return dataAccess.GetEccezioniListaImpreseAdempienti(filtro);
        }

        public bool InsertEccezioneListaImpreseAdempienti(EccezioneLista eccezione, out bool giaPresente)
        {
            return dataAccess.InsertEccezioneListaImpreseAdempienti(eccezione, out giaPresente);
        }

        public bool DeleteEccezioneListaImpreseAdempienti(int idImpresa)
        {
            return dataAccess.DeleteEccezioneListaImpreseAdempienti(idImpresa);
        }

        public void UpdateParametriImpreseRegolari(string singolo, string totale)
        {
            dataAccess.UpdateParametriImpreseRegolari(singolo, totale);
        }

        public void GetParametriImpreseRegolari(out string singolo, out string totale)
        {
            dataAccess.GetParametriImpreseRegolari(out singolo, out totale);
        }

        public AttivitaIstatCollection GetAttivitaIstatImpreseAdempienti()
        {
            return dataAccess.GetAttivitaIstatImpreseAdempienti();
        }

        public ComuniCollection GetComuniSedeAmministrativaImpreseRegolari()
        {
            return dataAccess.GetComuniSedeAmministrativaImpreseRegolari();
        }

        public ComuniCollection GetComuniSedeLegaleImpreseRegolari()
        {
            return dataAccess.GetComuniSedeLegaleImpreseRegolari();
        }
    }
}