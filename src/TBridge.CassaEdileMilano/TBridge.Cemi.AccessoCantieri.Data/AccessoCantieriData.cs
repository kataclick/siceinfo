﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Collections.CexChange;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Exceptions;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Business;

//using System.Data.OleDb;
//using System.IO;
//using System.Reflection;
//using System.Runtime.InteropServices;
//using Microsoft.Office.Interop.Excel;

namespace TBridge.Cemi.AccessoCantieri.Data
{
    public class AccessoCantieriData
    {
        public AccessoCantieriData()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        private Database DatabaseCemi
        {
            get;
            set;
        }

        public ImpresaCollection GetImprese(int? idImpresaParam,
                                            string ragioneSocialeParam, string comuneParam, string indirizzoParam,
                                            string ivaFiscale, string stringExpression, string sortDirection)
        {
            ImpresaCollection listaImprese = new ImpresaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriImpreseSelect"))
            {
                if (idImpresaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresaParam.Value);
                if (!string.IsNullOrEmpty(ragioneSocialeParam))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSocialeParam);
                if (!string.IsNullOrEmpty(comuneParam))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comuneParam);
                if (!string.IsNullOrEmpty(indirizzoParam))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoParam);
                if (!string.IsNullOrEmpty(ivaFiscale))
                    DatabaseCemi.AddInParameter(comando, "@ivaFiscale", DbType.String, ivaFiscale);

                DataSet dsImprese = DatabaseCemi.ExecuteDataSet(comando);

                if ((dsImprese != null) && (dsImprese.Tables.Count == 1))
                {
                    if (stringExpression != null && sortDirection != null)
                    {
                        dsImprese.Tables[0].DefaultView.Sort = stringExpression + " " + sortDirection;
                    }

                    for (int i = 0; i < dsImprese.Tables[0].DefaultView.Count; i++)
                    {
                        string provincia = null;
                        string comune = null;
                        string cap = null;
                        string indirizzo = null;
                        string partitaIva = null;
                        string codiceFiscale = null;
                        string telefonoSedeLegale = null;
                        string telefonoSedeAmministrativa = null;
                        string emailSedeLegale = null;
                        string emailSedeAmministrativa = null;

                        int idImpresa = (int) dsImprese.Tables[0].DefaultView[i]["idImpresa"];
                        string ragioneSociale = (string) dsImprese.Tables[0].DefaultView[i]["ragioneSociale"];

                        if (dsImprese.Tables[0].DefaultView[i]["indirizzo"] != DBNull.Value)
                            indirizzo = (string) dsImprese.Tables[0].DefaultView[i]["indirizzo"];
                        if (dsImprese.Tables[0].DefaultView[i]["comune"] != DBNull.Value)
                            comune = (string) dsImprese.Tables[0].DefaultView[i]["comune"];
                        if (dsImprese.Tables[0].DefaultView[i]["provincia"] != DBNull.Value)
                            provincia = (string) dsImprese.Tables[0].DefaultView[i]["provincia"];
                        if (dsImprese.Tables[0].DefaultView[i]["cap"] != DBNull.Value)
                            cap = (string) dsImprese.Tables[0].DefaultView[i]["cap"];
                        if (dsImprese.Tables[0].DefaultView[i]["partitaIva"] != DBNull.Value)
                            partitaIva = (string) dsImprese.Tables[0].DefaultView[i]["partitaIva"];
                        if (dsImprese.Tables[0].DefaultView[i]["codiceFiscale"] != DBNull.Value)
                            codiceFiscale = (string) dsImprese.Tables[0].DefaultView[i]["codiceFiscale"];
                        if (dsImprese.Tables[0].DefaultView[i]["legaleTelefono"] != DBNull.Value)
                            telefonoSedeLegale =
                                (string) dsImprese.Tables[0].DefaultView[i]["legaleTelefono"];
                        if (dsImprese.Tables[0].DefaultView[i]["ammiTelefono"] != DBNull.Value)
                            telefonoSedeAmministrativa =
                                (string) dsImprese.Tables[0].DefaultView[i]["ammiTelefono"];
                        if (dsImprese.Tables[0].DefaultView[i]["legaleEmail"] != DBNull.Value)
                            emailSedeLegale =
                                (string) dsImprese.Tables[0].DefaultView[i]["legaleEmail"];
                        if (dsImprese.Tables[0].DefaultView[i]["ammiEmail"] != DBNull.Value)
                            emailSedeAmministrativa =
                                (string) dsImprese.Tables[0].DefaultView[i]["ammiEmail"];

                        Impresa impresa = new Impresa(idImpresa, ragioneSociale, indirizzo, provincia, comune, cap,
                                                      partitaIva, codiceFiscale, TipologiaImpresa.SiceNew, null,
                                                      telefonoSedeLegale, telefonoSedeAmministrativa, emailSedeLegale,
                                                      emailSedeAmministrativa
                            );
                        listaImprese.Add(impresa);
                    }
                }
            }

            return listaImprese;
        }

        #region INPS vecchio metodo
        //public void ImportaExcelINPS(string path)
        //{
        //    DirectoryInfo directoryInfo = new DirectoryInfo(path);
        //    FileInfo[] files = directoryInfo.GetFiles("*.xls");
        //    FileInfo[] fileInfoArray = files;
        //    int num = 0;
        //    while (true)
        //    {
        //        bool length = num < (int)fileInfoArray.Length;
        //        if (!length)
        //        {
        //            break;
        //        }
        //        FileInfo fileInfo = fileInfoArray[num];
        //        OleDbConnection oleDbConnection = new OleDbConnection(string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", string.Concat(path, fileInfo.Name)));
        //        try
        //        {
        //            oleDbConnection.Open();
        //            string str = "select * from [Foglio1$]";
        //            OleDbCommand oleDbCommand = new OleDbCommand(str, oleDbConnection);
        //            try
        //            {
        //                IDataReader dataReader = oleDbCommand.ExecuteReader();
        //                try
        //                {
        //                    while (true)
        //                    {
        //                        length = dataReader.Read();
        //                        if (!length)
        //                        {
        //                            break;
        //                        }
        //                        string str1 = dataReader[1].ToString();
        //                        DateTime? nullable = null;
        //                        length = string.IsNullOrEmpty(dataReader[6].ToString());
        //                        if (!length)
        //                        {
        //                            string str2 = dataReader[6].ToString();
        //                            char[] chrArray = new char[1];
        //                            chrArray[0] = '/';
        //                            chrArray = new char[1];
        //                            chrArray[0] = '/';
        //                            nullable = new DateTime?(new DateTime(int.Parse(str2.Split(chrArray)[1]), int.Parse(str2.Split(chrArray)[0]), 1));
        //                        }
        //                        string str3 = null;
        //                        length = string.IsNullOrEmpty(dataReader[2].ToString());
        //                        if (!length)
        //                        {
        //                            str3 = dataReader[2].ToString();
        //                        }
        //                        string str4 = null;
        //                        length = string.IsNullOrEmpty(dataReader[8].ToString());
        //                        if (!length)
        //                        {
        //                            str4 = dataReader[8].ToString();
        //                        }
        //                        string str5 = null;
        //                        length = string.IsNullOrEmpty(dataReader[9].ToString());
        //                        if (!length)
        //                        {
        //                            str5 = dataReader[9].ToString().PadLeft(11, '0');
        //                        }
        //                        this.UpdateControlliINPS(str1, str3, nullable, str4, str5);
        //                    }
        //                }
        //                finally
        //                {
        //                    length = dataReader == null;
        //                    if (!length)
        //                    {
        //                        dataReader.Dispose();
        //                    }
        //                }
        //            }
        //            finally
        //            {
        //                length = oleDbCommand == null;
        //                if (!length)
        //                {
        //                    oleDbCommand.Dispose();
        //                }
        //            }
        //            oleDbConnection.Close();
        //        }
        //        finally
        //        {
        //            length = oleDbConnection == null;
        //            if (!length)
        //            {
        //                oleDbConnection.Dispose();
        //            }
        //        }
        //        num++;
        //    }
        //}

        //public bool UpdateControlliINPS(string codiceFiscaleLavoratore, string partitaIvaImpresaCEMI, DateTime? dataUltimaContribuzione, string codiceFiscaleImpresaINPS, string partitaIvaImpresaINPS)
        //{
        //    bool flag;
        //    bool flag1 = false;
        //    DbCommand storedProcCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSUpdate");
        //    try
        //    {
        //        this.DatabaseCemi.AddInParameter(storedProcCommand, "@codiceFiscaleLavoratore", DbType.String, codiceFiscaleLavoratore);
        //        this.DatabaseCemi.AddInParameter(storedProcCommand, "@partitaIvaImpresaCEMI", DbType.String, partitaIvaImpresaCEMI);
        //        this.DatabaseCemi.AddInParameter(storedProcCommand, "@dataUltimaContribuzione", DbType.DateTime, dataUltimaContribuzione);
        //        this.DatabaseCemi.AddInParameter(storedProcCommand, "@codiceFiscaleImpresaINPS", DbType.String, codiceFiscaleImpresaINPS);
        //        this.DatabaseCemi.AddInParameter(storedProcCommand, "@partitaIVAImpresaINPS", DbType.String, partitaIvaImpresaINPS);
        //        flag = this.DatabaseCemi.ExecuteNonQuery(storedProcCommand) != 1;
        //        if (!flag)
        //        {
        //            flag1 = true;
        //        }
        //    }
        //    finally
        //    {
        //        flag = storedProcCommand == null;
        //        if (!flag)
        //        {
        //            storedProcCommand.Dispose();
        //        }
        //    }
        //    bool flag2 = flag1;
        //    return flag2;
        //}
        #endregion

        public void ImportaExcelINPS2(string pathScaricati, string pathElaborati)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(pathScaricati);
            FileInfo[] files = directoryInfo.GetFiles("*.xls");
            FileInfo[] fileInfoArray = files;
            int num = 0;
            while (true)
            {
                bool length = num < (int) fileInfoArray.Length;
                if (!length)
                {
                    break;
                }
                FileInfo fileInfo = fileInfoArray[num];
                length = File.Exists(string.Format("{0}{1}", pathElaborati, fileInfo.Name));
                if (!length)
                {
                    OleDbConnection oleDbConnection = new OleDbConnection(string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", string.Concat(pathScaricati, fileInfo.Name)));
                    try
                    {
                        oleDbConnection.Open();
                        string str = "select * from [Foglio1$]";
                        OleDbCommand oleDbCommand = new OleDbCommand(str, oleDbConnection);
                        try
                        {
                            IDataReader dataReader = oleDbCommand.ExecuteReader();
                            try
                            {
                                while (true)
                                {
                                    length = dataReader.Read();
                                    if (!length)
                                    {
                                        break;
                                    }
                                    string str1 = dataReader[1].ToString();
                                    string str2 = null;
                                    length = string.IsNullOrEmpty(dataReader[2].ToString());
                                    if (!length)
                                    {
                                        str2 = dataReader[2].ToString();
                                    }
                                    int num1 = int.Parse(dataReader[3].ToString());
                                    int num2 = int.Parse(dataReader[4].ToString());
                                    bool? nullable = null;
                                    length = string.IsNullOrEmpty(dataReader[5].ToString());
                                    if (!length)
                                    {
                                        nullable = new bool?(dataReader[5].ToString().StartsWith("OK"));
                                    }
                                    length = str2 == null;
                                    if (!length)
                                    {
                                        this.ControlliINPSInsert2aux(str1, str2, num1, num2, nullable);
                                    }
                                }
                            }
                            finally
                            {
                                length = dataReader == null;
                                if (!length)
                                {
                                    dataReader.Dispose();
                                }
                            }
                        }
                        finally
                        {
                            length = oleDbCommand == null;
                            if (!length)
                            {
                                oleDbCommand.Dispose();
                            }
                        }
                        oleDbConnection.Close();
                        File.Copy(string.Format("{0}{1}", pathScaricati, fileInfo.Name), string.Format("{0}{1}", pathElaborati, fileInfo.Name));
                    }
                    finally
                    {
                        length = oleDbConnection == null;
                        if (!length)
                        {
                            oleDbConnection.Dispose();
                        }
                    }
                }
                num++;
            }
        }

        public void ControlliINPSInsert2aux(string codiceFiscale, string partitaIva, int anno, int mese, bool? superato)
        {
            DbCommand storedProcCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSInsert2aux");
            try
            {
                this.DatabaseCemi.AddInParameter(storedProcCommand, "@codiceFiscale", DbType.String, codiceFiscale);
                this.DatabaseCemi.AddInParameter(storedProcCommand, "@partitaIva", DbType.String, partitaIva);
                this.DatabaseCemi.AddInParameter(storedProcCommand, "@anno", DbType.String, anno);
                this.DatabaseCemi.AddInParameter(storedProcCommand, "@mese", DbType.String, mese);
                this.DatabaseCemi.AddInParameter(storedProcCommand, "@superato", DbType.Boolean, superato);
                this.DatabaseCemi.ExecuteNonQuery(storedProcCommand);
            }
            finally
            {
                bool flag = storedProcCommand == null;
                if (!flag)
                {
                    storedProcCommand.Dispose();
                }
            }
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIva, string codiceFiscale)
        {
            bool[] res = new bool[4];

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptEsisteImpresaConIvaFisc"))
            {
                if (!string.IsNullOrEmpty(partitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, partitaIva);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@impresaIva", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@cantieriImpresaIva", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@impresaFisc", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@cantieriImpresaFisc", DbType.Boolean, 1);
                DatabaseCemi.ExecuteNonQuery(comando);

                res[0] = (bool) comando.Parameters["@impresaIva"].Value;
                res[1] = (bool) comando.Parameters["@cantieriImpresaIva"].Value;
                res[2] = (bool) comando.Parameters["@impresaFisc"].Value;
                res[3] = (bool) comando.Parameters["@cantieriImpresaFisc"].Value;
            }

            return res;
        }

        public void LogSelezioneImpresaSubappalto(int idUtente, Subappalto subappalto)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLogSubappaltiInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                if (subappalto.Appaltante != null && subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    if (subappalto.Appaltante.IdImpresa != null)
                        DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltante", DbType.Int32,
                                                    subappalto.Appaltante.IdImpresa.Value);
                }
                if (subappalto.Appaltata != null && subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    if (subappalto.Appaltata.IdImpresa != null)
                        DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltata", DbType.Int32,
                                                    subappalto.Appaltata.IdImpresa.Value);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public LavoratoreCollection GetLavoratoriOrdinatiReader(
            int? idLavoratore, string cognome, string nome, DateTime? dataNascita, string codiceFiscale,
            DateTime? dataAssunzione, DateTime? dataCessazione, string sortExpression,
            string direct, int? idImpresa)
        {
            LavoratoreCollection listaLavoratori = new LavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@Cognome", DbType.String, cognome);
                if (!string.IsNullOrEmpty(nome))
                    DatabaseCemi.AddInParameter(comando, "@Nome", DbType.String, nome);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@CodiceFiscale", DbType.String, codiceFiscale);
                if (dataNascita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@DataNascita", DbType.DateTime, dataNascita);
                if (idLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@IdLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();

                        lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        if (reader["idLavoratore"] != DBNull.Value)
                        {
                            lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                        }
                        if (reader["cognome"] != DBNull.Value)
                        {
                            lavoratore.Cognome = (string) reader["cognome"];
                        }
                        if (reader["nome"] != DBNull.Value)
                        {
                            lavoratore.Nome = (string) reader["nome"];
                        }
                        if (reader["dataNascita"] != DBNull.Value)
                        {
                            lavoratore.DataNascita = (DateTime) reader["dataNascita"];
                        }
                        if (reader["codiceFiscale"] != DBNull.Value)
                        {
                            lavoratore.CodiceFiscale = (string) reader["codiceFiscale"];
                        }
                        if (reader["idNazionalita"] != DBNull.Value)
                        {
                            lavoratore.PaeseNascita = (string) reader["idNazionalita"];
                        }
                        if (reader["provinciaNascita"] != DBNull.Value)
                        {
                            lavoratore.ProvinciaNascita = (string) reader["provinciaNascita"];
                        }
                        if (reader["luogoNascita"] != DBNull.Value)
                        {
                            lavoratore.LuogoNascita = (string) reader["luogoNascita"];
                        }

                        listaLavoratori.Add(lavoratore);
                    }
                }
            }

            return listaLavoratori;
        }

        public ControlloWhiteListCollection GetControlliWhiteList(ControlloIdentitaFilter filtro)
        {
            ControlloWhiteListCollection controlli = new ControlloWhiteListCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriControlliCEMIWhiteListSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, filtro.IdCantiere);
                if (!String.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }


                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceLavoratoreCognome = reader.GetOrdinal("lavoratoreCognome");
                    Int32 indiceLavoratoreNome = reader.GetOrdinal("lavoratoreNome");
                    Int32 indiceLavoratoreCodiceFiscale = reader.GetOrdinal("lavoratoreCodiceFiscale");
                    Int32 indiceLavoratoreDataNascita = reader.GetOrdinal("lavoratoreDataNascita");
                    Int32 indiceLavoratoreDataInizio = reader.GetOrdinal("lavoratoreDataInizio");
                    Int32 indiceLavoratoreDataFine = reader.GetOrdinal("lavoratoreDataFine");
                    Int32 indiceRapportoImpresaLavoratore = reader.GetOrdinal("rapportoImpresaLavoratore");

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceImpresaRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    Int32 indiceImpresaPartitaIva = reader.GetOrdinal("impresaPartitaIva");
                    Int32 indiceImpresaCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    Int32 indiceImpresaDataUltimaDenuncia = reader.GetOrdinal("impresaUltimaDenuncia");
                    Int32 indiceImpresaLavoratoreUltimaDenuncia = reader.GetOrdinal("impresaLavoratoreUltimaDenuncia");
                    Int32 indiceStatoImpresa = reader.GetOrdinal("statoImpresa");
                    Int32 indiceDataCessazione = reader.GetOrdinal("dataDisdetta");
                    Int32 indiceDataSospensione = reader.GetOrdinal("dataSospensione");

                    #endregion

                    while (reader.Read())
                    {
                        ControlloWhiteList controllo = new ControlloWhiteList();
                        controlli.Add(controllo);

                        controllo.Lavoratore = new Lavoratore();
                        controllo.Impresa = new Impresa();

                        // Lavoratore
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            controllo.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceLavoratoreCognome))
                        {
                            controllo.Lavoratore.Cognome = reader.GetString(indiceLavoratoreCognome);
                        }
                        if (!reader.IsDBNull(indiceLavoratoreNome))
                        {
                            controllo.Lavoratore.Nome = reader.GetString(indiceLavoratoreNome);
                        }
                        if (!reader.IsDBNull(indiceLavoratoreCodiceFiscale))
                        {
                            controllo.Lavoratore.CodiceFiscale = reader.GetString(indiceLavoratoreCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceLavoratoreDataNascita))
                        {
                            controllo.Lavoratore.DataNascita = reader.GetDateTime(indiceLavoratoreDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLavoratoreDataInizio))
                        {
                            controllo.Lavoratore.DataInizioAttivita = reader.GetDateTime(indiceLavoratoreDataInizio);
                        }
                        if (!reader.IsDBNull(indiceLavoratoreDataFine))
                        {
                            controllo.Lavoratore.DataFineAttivita = reader.GetDateTime(indiceLavoratoreDataFine);
                        }
                        controllo.RapportoImpresaLavoratore = reader.GetBoolean(indiceRapportoImpresaLavoratore);

                        // Impresa
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            controllo.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        }
                        if (!reader.IsDBNull(indiceImpresaRagioneSociale))
                        {
                            controllo.Impresa.RagioneSociale = reader.GetString(indiceImpresaRagioneSociale);
                        }
                        if (!reader.IsDBNull(indiceImpresaPartitaIva))
                        {
                            controllo.Impresa.PartitaIva = reader.GetString(indiceImpresaPartitaIva);
                        }
                        if (!reader.IsDBNull(indiceImpresaCodiceFiscale))
                        {
                            controllo.Impresa.CodiceFiscale = reader.GetString(indiceImpresaCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceStatoImpresa))
                        {
                            controllo.Impresa.Stato = reader.GetString(indiceStatoImpresa);
                        }
                        if (!reader.IsDBNull(indiceDataSospensione))
                        {
                            controllo.Impresa.DataSospensione = reader.GetDateTime(indiceDataSospensione);
                        }
                        if (!reader.IsDBNull(indiceDataCessazione))
                        {
                            controllo.Impresa.DataCessazione = reader.GetDateTime(indiceDataCessazione);
                        }

                        // Controlli
                        if (!reader.IsDBNull(indiceImpresaDataUltimaDenuncia))
                        {
                            controllo.DataUltimaDenuncia = reader.GetDateTime(indiceImpresaDataUltimaDenuncia);
                        }
                        if (!reader.IsDBNull(indiceImpresaLavoratoreUltimaDenuncia))
                        {
                            controllo.DataUltimaDenunciaLavoratore =
                                reader.GetDateTime(indiceImpresaLavoratoreUltimaDenuncia);
                        }
                    }
                }
            }

            return controlli;
        }

        public WhiteListImpresaCollection GetLavoratoriInDomandaNoFoto(Int32 idDomanda)
        {
            WhiteListImpresaCollection domandaImpresaColl = new WhiteListImpresaCollection();
            WhiteListImpresa domandaImpresa = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriReportWhiteListLavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdDomandaLavoratore = reader.GetOrdinal("idAccessoCantieriWhiteListLavoratori");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceIdAttestatoLavoratore = reader.GetOrdinal("idAccessoCantieriLavoratore");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("idAccessoCantieriImpresa");

                    Int32 indiceDataInizioAttivita = reader.GetOrdinal("dataInizioAttivita");
                    Int32 indiceDataFineAttivita = reader.GetOrdinal("dataFineAttivita");
                    Int32 indiceEffettuaControlli = reader.GetOrdinal("effettuaControlli");
                    Int32 indiceDataAssunzione = reader.GetOrdinal("dataAssunzione");
                    //Int32 indiceFoto = reader.GetOrdinal("foto");
                    Int32 indiceDataStampaBadge = reader.GetOrdinal("dataStampaBadge");

                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atRagioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceAtPartitaIva = reader.GetOrdinal("atPartitaIva");
                    Int32 indiceCodiceFiscaleImpresa = reader.GetOrdinal("codiceFiscaleImpresa");
                    Int32 indiceAtCodiceFiscaleImpresa = reader.GetOrdinal("atCodiceFiscaleImpresa");

                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indicePaeseNascita = reader.GetOrdinal("paeseNascita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");

                    Int32 indiceAtCognome = reader.GetOrdinal("atCognome");
                    Int32 indiceAtNome = reader.GetOrdinal("atNome");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atCodiceFiscale");
                    Int32 indiceAtDataNascita = reader.GetOrdinal("atDataNascita");
                    Int32 indiceAtLuogoNascita = reader.GetOrdinal("atLuogoNascita");
                    Int32 indiceAtPaeseNascita = reader.GetOrdinal("atPaeseNascita");
                    Int32 indiceAtProvinciaNascita = reader.GetOrdinal("atProvinciaNascita");

                    #endregion

                    Int32 idImpresaSice = -1;
                    Int32 idImpresaNuova = -1;

                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            idImpresaSice = reader.GetInt32(indiceIdImpresa);
                        }


                        if (!reader.IsDBNull(indiceIdAttestatoImpresa))
                        {
                            idImpresaNuova = reader.GetInt32(indiceIdAttestatoImpresa);
                        }

                        if (domandaImpresa == null
                            ||
                            (idImpresaSice != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew &&
                             domandaImpresa.Impresa.IdImpresa.Value != idImpresaSice)
                            ||
                            (idImpresaNuova != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.Nuova &&
                             domandaImpresa.Impresa.IdImpresa.Value != idImpresaNuova)
                            ||
                            (idImpresaSice != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.Nuova)
                            ||
                            (idImpresaNuova != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                            )
                        {
                            domandaImpresa = new WhiteListImpresa();
                            domandaImpresaColl.Add(domandaImpresa);

                            domandaImpresa.IdDomanda = idDomanda;

                            if (idImpresaSice != -1)
                            {
                                domandaImpresa.Impresa = new Impresa
                                {
                                    TipoImpresa = TipologiaImpresa.SiceNew,
                                    IdImpresa = idImpresaSice,
                                    RagioneSociale = reader.GetString(indiceRagioneSociale)
                                };

                                if (!reader.IsDBNull(indicePartitaIva))
                                {
                                    domandaImpresa.Impresa.PartitaIva = reader.GetString(indicePartitaIva);
                                }
                                if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                                {
                                    domandaImpresa.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                                }
                            }
                            else
                            {
                                domandaImpresa.Impresa = new Impresa
                                {
                                    TipoImpresa = TipologiaImpresa.Nuova,
                                    IdImpresa = idImpresaNuova,
                                    RagioneSociale =
                                        reader.GetString(indiceAtRagioneSociale)
                                };

                                if (!reader.IsDBNull(indiceAtPartitaIva))
                                {
                                    domandaImpresa.Impresa.PartitaIva = reader.GetString(indiceAtPartitaIva);
                                }
                                if (!reader.IsDBNull(indiceAtCodiceFiscaleImpresa))
                                {
                                    domandaImpresa.Impresa.CodiceFiscale = reader.GetString(indiceAtCodiceFiscaleImpresa);
                                }
                            }
                        }

                        Lavoratore lavoratore = new Lavoratore();
                        domandaImpresa.Lavoratori.Add(lavoratore);

                        lavoratore.IdDomandaLavoratore = reader.GetInt32(indiceIdDomandaLavoratore);
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                            if (!reader.IsDBNull(indiceDataInizioAttivita))
                                lavoratore.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                            if (!reader.IsDBNull(indiceDataFineAttivita))
                                lavoratore.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                            lavoratore.EffettuaControlli = reader.GetBoolean(indiceEffettuaControlli);

                            if (!reader.IsDBNull(indiceDataAssunzione))
                                lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                            //if (reader["foto"] != null && reader["foto"] != DBNull.Value)
                            //    lavoratore.Foto = ((byte[])reader["foto"]);

                            if (!reader.IsDBNull(indiceDataStampaBadge))
                                lavoratore.DataStampaBadge = reader.GetDateTime(indiceDataStampaBadge);

                            if (!reader.IsDBNull(indiceCognome))
                                lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                                lavoratore.Nome = reader.GetString(indiceNome);
                            if (!reader.IsDBNull(indiceDataNascita))
                                lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);

                            if (!reader.IsDBNull(indiceLuogoNascita))
                                lavoratore.LuogoNascita = reader.GetString(indiceLuogoNascita);
                            if (!reader.IsDBNull(indicePaeseNascita))
                                lavoratore.PaeseNascita = reader.GetString(indicePaeseNascita);
                            if (!reader.IsDBNull(indiceProvinciaNascita))
                                lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);

                            if (!reader.IsDBNull(indiceCodiceFiscale))
                                lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdAttestatoLavoratore);

                            if (!reader.IsDBNull(indiceDataInizioAttivita))
                                lavoratore.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                            if (!reader.IsDBNull(indiceDataFineAttivita))
                                lavoratore.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                            lavoratore.EffettuaControlli = reader.GetBoolean(indiceEffettuaControlli);

                            if (!reader.IsDBNull(indiceDataAssunzione))
                                lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                            //if (reader["foto"] != null && reader["foto"] != DBNull.Value)
                            //    lavoratore.Foto = ((byte[])reader["foto"]);

                            if (!reader.IsDBNull(indiceDataStampaBadge))
                                lavoratore.DataStampaBadge = reader.GetDateTime(indiceDataStampaBadge);

                            if (!reader.IsDBNull(indiceAtCognome))
                                lavoratore.Cognome = reader.GetString(indiceAtCognome);
                            if (!reader.IsDBNull(indiceAtNome))
                                lavoratore.Nome = reader.GetString(indiceAtNome);
                            if (!reader.IsDBNull(indiceAtDataNascita))
                                lavoratore.DataNascita = reader.GetDateTime(indiceAtDataNascita);

                            if (!reader.IsDBNull(indiceAtLuogoNascita))
                                lavoratore.LuogoNascita = reader.GetString(indiceAtLuogoNascita);
                            if (!reader.IsDBNull(indiceAtPaeseNascita))
                                lavoratore.PaeseNascita = reader.GetString(indiceAtPaeseNascita);
                            if (!reader.IsDBNull(indiceAtProvinciaNascita))
                                lavoratore.ProvinciaNascita = reader.GetString(indiceAtProvinciaNascita);

                            if (!reader.IsDBNull(indiceAtCodiceFiscale))
                                lavoratore.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                        }

                        idImpresaSice = -1;
                        idImpresaNuova = -1;
                    }
                }
            }

            return domandaImpresaColl;
        }

        public WhiteListImpresaCollection GetLavoratoriInDomanda(Int32 idDomanda)
        {
            WhiteListImpresaCollection domandaImpresaColl = new WhiteListImpresaCollection();
            WhiteListImpresa domandaImpresa = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriReportWhiteListLavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdDomandaLavoratore = reader.GetOrdinal("idAccessoCantieriWhiteListLavoratori");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceIdAttestatoLavoratore = reader.GetOrdinal("idAccessoCantieriLavoratore");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("idAccessoCantieriImpresa");

                    Int32 indiceDataInizioAttivita = reader.GetOrdinal("dataInizioAttivita");
                    Int32 indiceDataFineAttivita = reader.GetOrdinal("dataFineAttivita");
                    Int32 indiceEffettuaControlli = reader.GetOrdinal("effettuaControlli");
                    Int32 indiceDataAssunzione = reader.GetOrdinal("dataAssunzione");
                    //Int32 indiceFoto = reader.GetOrdinal("foto");
                    Int32 indiceDataStampaBadge = reader.GetOrdinal("dataStampaBadge");

                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atRagioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceAtPartitaIva = reader.GetOrdinal("atPartitaIva");
                    Int32 indiceCodiceFiscaleImpresa = reader.GetOrdinal("codiceFiscaleImpresa");
                    Int32 indiceAtCodiceFiscaleImpresa = reader.GetOrdinal("atCodiceFiscaleImpresa");

                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indicePaeseNascita = reader.GetOrdinal("paeseNascita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");

                    Int32 indiceAtCognome = reader.GetOrdinal("atCognome");
                    Int32 indiceAtNome = reader.GetOrdinal("atNome");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atCodiceFiscale");
                    Int32 indiceAtDataNascita = reader.GetOrdinal("atDataNascita");
                    Int32 indiceAtLuogoNascita = reader.GetOrdinal("atLuogoNascita");
                    Int32 indiceAtPaeseNascita = reader.GetOrdinal("atPaeseNascita");
                    Int32 indiceAtProvinciaNascita = reader.GetOrdinal("atProvinciaNascita");

                    #endregion

                    Int32 idImpresaSice = -1;
                    Int32 idImpresaNuova = -1;

                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            idImpresaSice = reader.GetInt32(indiceIdImpresa);
                        }


                        if (!reader.IsDBNull(indiceIdAttestatoImpresa))
                        {
                            idImpresaNuova = reader.GetInt32(indiceIdAttestatoImpresa);
                        }

                        if (domandaImpresa == null
                            ||
                            (idImpresaSice != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew &&
                             domandaImpresa.Impresa.IdImpresa.Value != idImpresaSice)
                            ||
                            (idImpresaNuova != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.Nuova &&
                             domandaImpresa.Impresa.IdImpresa.Value != idImpresaNuova)
                            ||
                            (idImpresaSice != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.Nuova)
                            ||
                            (idImpresaNuova != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                            )
                        {
                            domandaImpresa = new WhiteListImpresa();
                            domandaImpresaColl.Add(domandaImpresa);

                            domandaImpresa.IdDomanda = idDomanda;

                            if (idImpresaSice != -1)
                            {
                                domandaImpresa.Impresa = new Impresa
                                                             {
                                                                 TipoImpresa = TipologiaImpresa.SiceNew,
                                                                 IdImpresa = idImpresaSice,
                                                                 RagioneSociale = reader.GetString(indiceRagioneSociale)
                                                             };

                                if (!reader.IsDBNull(indicePartitaIva))
                                {
                                    domandaImpresa.Impresa.PartitaIva = reader.GetString(indicePartitaIva);
                                }
                                if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                                {
                                    domandaImpresa.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                                }
                            }
                            else
                            {
                                domandaImpresa.Impresa = new Impresa
                                                             {
                                                                 TipoImpresa = TipologiaImpresa.Nuova,
                                                                 IdImpresa = idImpresaNuova,
                                                                 RagioneSociale =
                                                                     reader.GetString(indiceAtRagioneSociale)
                                                             };

                                if (!reader.IsDBNull(indiceAtPartitaIva))
                                {
                                    domandaImpresa.Impresa.PartitaIva = reader.GetString(indiceAtPartitaIva);
                                }
                                if (!reader.IsDBNull(indiceAtCodiceFiscaleImpresa))
                                {
                                    domandaImpresa.Impresa.CodiceFiscale = reader.GetString(indiceAtCodiceFiscaleImpresa);
                                }
                            }
                        }

                        Lavoratore lavoratore = new Lavoratore();
                        domandaImpresa.Lavoratori.Add(lavoratore);

                        lavoratore.IdDomandaLavoratore = reader.GetInt32(indiceIdDomandaLavoratore);
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                            if (!reader.IsDBNull(indiceDataInizioAttivita))
                                lavoratore.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                            if (!reader.IsDBNull(indiceDataFineAttivita))
                                lavoratore.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                            lavoratore.EffettuaControlli = reader.GetBoolean(indiceEffettuaControlli);

                            if (!reader.IsDBNull(indiceDataAssunzione))
                                lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                            if (reader["foto"] != null && reader["foto"] != DBNull.Value)
                                lavoratore.Foto = ((byte[]) reader["foto"]);

                            if (!reader.IsDBNull(indiceDataStampaBadge))
                                lavoratore.DataStampaBadge = reader.GetDateTime(indiceDataStampaBadge);

                            if (!reader.IsDBNull(indiceCognome))
                                lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                                lavoratore.Nome = reader.GetString(indiceNome);
                            if (!reader.IsDBNull(indiceDataNascita))
                                lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);

                            if (!reader.IsDBNull(indiceLuogoNascita))
                                lavoratore.LuogoNascita = reader.GetString(indiceLuogoNascita);
                            if (!reader.IsDBNull(indicePaeseNascita))
                                lavoratore.PaeseNascita = reader.GetString(indicePaeseNascita);
                            if (!reader.IsDBNull(indiceProvinciaNascita))
                                lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);

                            if (!reader.IsDBNull(indiceCodiceFiscale))
                                lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdAttestatoLavoratore);

                            if (!reader.IsDBNull(indiceDataInizioAttivita))
                                lavoratore.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                            if (!reader.IsDBNull(indiceDataFineAttivita))
                                lavoratore.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                            lavoratore.EffettuaControlli = reader.GetBoolean(indiceEffettuaControlli);

                            if (!reader.IsDBNull(indiceDataAssunzione))
                                lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                            if (reader["foto"] != null && reader["foto"] != DBNull.Value)
                                lavoratore.Foto = ((byte[]) reader["foto"]);

                            if (!reader.IsDBNull(indiceDataStampaBadge))
                                lavoratore.DataStampaBadge = reader.GetDateTime(indiceDataStampaBadge);

                            if (!reader.IsDBNull(indiceAtCognome))
                                lavoratore.Cognome = reader.GetString(indiceAtCognome);
                            if (!reader.IsDBNull(indiceAtNome))
                                lavoratore.Nome = reader.GetString(indiceAtNome);
                            if (!reader.IsDBNull(indiceAtDataNascita))
                                lavoratore.DataNascita = reader.GetDateTime(indiceAtDataNascita);

                            if (!reader.IsDBNull(indiceAtLuogoNascita))
                                lavoratore.LuogoNascita = reader.GetString(indiceAtLuogoNascita);
                            if (!reader.IsDBNull(indiceAtPaeseNascita))
                                lavoratore.PaeseNascita = reader.GetString(indiceAtPaeseNascita);
                            if (!reader.IsDBNull(indiceAtProvinciaNascita))
                                lavoratore.ProvinciaNascita = reader.GetString(indiceAtProvinciaNascita);

                            if (!reader.IsDBNull(indiceAtCodiceFiscale))
                                lavoratore.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                        }

                        idImpresaSice = -1;
                        idImpresaNuova = -1;
                    }
                }
            }

            return domandaImpresaColl;
        }

        public AltraPersonaCollection GetAltrePersone(Int32 idAccessoCantieriWhiteList)
        {
            AltraPersonaCollection altrePersone = new AltraPersonaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriAltrePersoneSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idAccessoCantieriWhiteList);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdAltraPersona = reader.GetOrdinal("idAltraPersona");
                    Int32 indiceIdAccessoCantieriWhiteList = reader.GetOrdinal("idAccessoCantieriWhiteList");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceTelefono = reader.GetOrdinal("telefono");
                    Int32 indiceEmail = reader.GetOrdinal("email");
                    Int32 indiceRuolo = reader.GetOrdinal("ruolo");

                    #endregion

                    while (reader.Read())
                    {
                        AltraPersona altraPersona = new AltraPersona
                                                        {
                                                            IdAltraPersona = reader.GetInt32(indiceIdAltraPersona),
                                                            IdWhiteListAltraPersona =
                                                                reader.GetInt32(indiceIdAccessoCantieriWhiteList)
                                                        };

                        if (!reader.IsDBNull(indiceNome))
                            altraPersona.Nome = reader.GetString(indiceNome);
                        if (!reader.IsDBNull(indiceCognome))
                            altraPersona.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                            altraPersona.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        if (!reader.IsDBNull(indiceDataNascita))
                            altraPersona.DataNascita = reader.GetDateTime(indiceDataNascita);
                        if (!reader.IsDBNull(indiceTelefono))
                            altraPersona.Telefono = reader.GetString(indiceTelefono);
                        if (!reader.IsDBNull(indiceEmail))
                            altraPersona.Email = reader.GetString(indiceEmail);
                        if (!reader.IsDBNull(indiceRuolo))
                            altraPersona.TipoRuolo = (TipologiaRuolo) reader.GetInt32(indiceRuolo);

                        altrePersone.Add(altraPersona);
                    }
                }
            }

            return altrePersone;
        }

        public Boolean UpdateFotoLavoratoreByLavoratoreImpresaCantiere(Int32 idLavoratore, TipologiaLavoratore tipoLavoratore, Int32 idImpresa, TipologiaImpresa tipoImpresa, Int32 idCantiere, byte[] foto)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLavoratoreFotoUpdateByLavoratoreImpresaCantiere"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@tipoLavoratore", DbType.Int32, tipoLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@tipoImpresa", DbType.Int32, tipoImpresa);
                DatabaseCemi.AddInParameter(comando, "@foto", DbType.Binary, foto);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public byte[] GetFotoLavoratoreByLavoratoreImpresaCantiere(Int32 idLavoratore, TipologiaLavoratore tipoLavoratore, Int32 idImpresa, TipologiaImpresa tipoImpresa, Int32 idCantiere)
        {
            byte[] foto = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLavoratoreFotoSelectByLavoratoreImpresaCantiere"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@tipoLavoratore", DbType.Int32, tipoLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@tipoImpresa", DbType.Int32, tipoImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    //Int32 indiceFoto = reader.GetOrdinal("foto");

                    #endregion

                    while (reader.Read())
                    {
                        if (reader["foto"] != null && reader["foto"] != DBNull.Value)
                            foto = ((byte[]) reader["foto"]);
                    }
                }
            }

            return foto;
        }


        public ReferenteCollection GetReferenti(Int32 idAccessoCantieriWhiteList)
        {
            ReferenteCollection referenti = new ReferenteCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriReferentiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idAccessoCantieriWhiteList);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdReferente = reader.GetOrdinal("idReferente");
                    Int32 indiceIdAccessoCantieriWhiteList = reader.GetOrdinal("idAccessoCantieriWhiteList");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceTelefono = reader.GetOrdinal("telefono");
                    Int32 indiceEmail = reader.GetOrdinal("email");
                    Int32 indiceModalitaContatto = reader.GetOrdinal("modalitaContatto");
                    Int32 indiceRuolo = reader.GetOrdinal("ruolo");
                    Int32 indiceAffiliazione = reader.GetOrdinal("ruolo");

                    #endregion

                    while (reader.Read())
                    {
                        Referente referente = new Referente
                                                  {
                                                      IdReferente = reader.GetInt32(indiceIdReferente),
                                                      IdWhiteListReferente =
                                                          reader.GetInt32(indiceIdAccessoCantieriWhiteList)
                                                  };

                        if (!reader.IsDBNull(indiceNome))
                            referente.Nome = reader.GetString(indiceNome);
                        if (!reader.IsDBNull(indiceCognome))
                            referente.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                            referente.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        if (!reader.IsDBNull(indiceDataNascita))
                            referente.DataNascita = reader.GetDateTime(indiceDataNascita);
                        if (!reader.IsDBNull(indiceTelefono))
                            referente.Telefono = reader.GetString(indiceTelefono);
                        if (!reader.IsDBNull(indiceEmail))
                            referente.Email = reader.GetString(indiceEmail);

                        if (!reader.IsDBNull(indiceModalitaContatto))
                            referente.ModalitaContatto = reader.GetBoolean(indiceModalitaContatto);

                        if (!reader.IsDBNull(indiceRuolo))
                            referente.TipoRuolo = (TipologiaRuoloReferente) reader.GetInt32(indiceRuolo);
                        if (!reader.IsDBNull(indiceAffiliazione))
                            referente.TipoAffiliazione = (TipologiaAffiliazione) reader.GetInt32(indiceAffiliazione);

                        referenti.Add(referente);
                    }
                }
            }

            return referenti;
        }

        public TimbraturaCollection GetTimbrature2(Int32 idAccessoCantieriWhiteList)
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idAccessoCantieriWhiteList);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTimbratura = reader.GetOrdinal("idTimbratura");

                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIngressoUscita = reader.GetOrdinal("ingressoUscita");
                    Int32 indiceGestito = reader.GetOrdinal("gestito");
                    Int32 indiceAnomalia = reader.GetOrdinal("anomalia");
                    Int32 indiceDataOra = reader.GetOrdinal("dataOra");
                    Int32 indiceControlliEffettuati = reader.GetOrdinal("controlliEffettuati");
                    Int32 indiceControlloDenunciaSuperato = reader.GetOrdinal("controlloDenunciaSuperato");
                    Int32 indiceControlloLavoratoreDenunciaSuperato =
                        reader.GetOrdinal("controlloLavoratoreDenunciaSuperato");
                    Int32 indiceControlloOreDenunciaSuperato = reader.GetOrdinal("controlloOreDenunciaSuperato");
                    Int32 indiceControlloDebitiSuperato = reader.GetOrdinal("controlloDebitiSuperato");
                    Int32 indiceControlloDebitiEffettuato = reader.GetOrdinal("controlloDebitiEffettuato");

                    #endregion

                    while (reader.Read())
                    {
                        Timbratura timbratura = new Timbratura
                                                    {
                                                        IdTimbratura = reader.GetInt32(indiceIdTimbratura),
                                                        CodiceRilevatore = reader.GetString(indiceCodiceRilevatore),
                                                        RagioneSociale = reader.GetString(indiceRagioneSociale)
                                                    };

                        if (!reader.IsDBNull(indiceLatitudine))
                            timbratura.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            timbratura.Longitudine = reader.GetDecimal(indiceLongitudine);

                        timbratura.CodiceFiscale = reader.GetString(indiceCodiceFiscale);

                        timbratura.IngressoUscita = reader.GetBoolean(indiceIngressoUscita);

                        if (!reader.IsDBNull(indiceGestito))
                            timbratura.Gestito = reader.GetBoolean(indiceGestito);
                        if (!reader.IsDBNull(indiceAnomalia))
                            timbratura.Anomalia = (TipologiaAnomalia) reader.GetInt32(indiceAnomalia);

                        timbratura.DataOra = reader.GetDateTime(indiceDataOra);

                        timbratura.ControlliEffettuati = reader.GetBoolean(indiceControlliEffettuati);
                        timbratura.ControlloDenunciaSuperato = reader.GetBoolean(indiceControlloDenunciaSuperato);
                        timbratura.ControlloLavoratoreDenunciaSuperato =
                            reader.GetBoolean(indiceControlloLavoratoreDenunciaSuperato);
                        timbratura.ControlloOreDenunciaSuperato = reader.GetBoolean(indiceControlloOreDenunciaSuperato);
                        timbratura.ControlloDebitiSuperato = reader.GetBoolean(indiceControlloDebitiSuperato);
                        timbratura.ControlloDebitiEffettuato = reader.GetBoolean(indiceControlloDebitiEffettuato);

                        timbrature.Add(timbratura);
                    }
                }
            }

            return timbrature;
        }

        public ControlloIdentitaCollection GetControlloIdentita(ControlloIdentitaFilter filter)
        {
            ControlloIdentitaCollection controlloIdentitaColl = new ControlloIdentitaCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliCEMISelect")
                )
            {
                if (filter.IdCantiere.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filter.IdCantiere);

                if (filter.Mese.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, filter.Mese.Value);
                if (filter.Anno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, filter.Anno.Value);
                if (filter.IdImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filter.IdImpresa.Value);
                if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                if (filter.TipologiaAnomaliaControlli.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoAnomalia", DbType.Int32,
                                                filter.TipologiaAnomaliaControlli.Value);
                if (filter.TipologiaRuoloTimbratura.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoUtente", DbType.Int32,
                                                filter.TipologiaRuoloTimbratura.Value);
                if (!string.IsNullOrEmpty(filter.PartitaIVA))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, filter.PartitaIVA);

                //comando.CommandTimeout = 60;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    Int32 indiceMese = reader.GetOrdinal("mese");
                    Int32 indiceAnno = reader.GetOrdinal("anno");

                    Int32 indiceControlliEffettuati = reader.GetOrdinal("controlliEffettuati");
                    Int32 indiceControlloDenunciaSuperato = reader.GetOrdinal("controlloDenunciaSuperato");
                    Int32 indiceControlloLavoratoreDenunciaSuperato =
                        reader.GetOrdinal("controlloLavoratoreDenunciaSuperato");
                    Int32 indiceControlloOreDenunciaSuperato = reader.GetOrdinal("controlloOreDenunciaSuperato");
                    Int32 indiceControlloDebitiSuperato = reader.GetOrdinal("controlloDebitiSuperato");
                    Int32 indiceControlloDebitiEffettuato = reader.GetOrdinal("controlloDebitiEffettuato");

                    Int32 indiceDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");
                    Int32 indiceNomeCassaEdileUltimaDenuncia = reader.GetOrdinal("nomeCassaEdileUltimaDenuncia");
                    //Int32 indiceCexchangeCassaEdileDescrizione = reader.GetOrdinal("cexchangeCassaEdileDescrizione");
                    //Int32 indiceCexchangeCassaEdileId = reader.GetOrdinal("cexchangeCassaEdileId");
                    //Int32 indiceCexchangeOreOrdinarie = reader.GetOrdinal("cexchangeCassaEdileOreOrdinarie");

                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIvaImpresa");

                    #endregion

                    ControlloIdentita controlloIdentita = null;

                    while (reader.Read())
                    {
                        controlloIdentita = new ControlloIdentita();

                        if (!reader.IsDBNull(indiceIdCantiere))
                            controlloIdentita.IdCantiere = reader.GetInt32(indiceIdCantiere);

                        controlloIdentita.CodiceFiscale = reader.GetString(indiceCodiceFiscale);

                        controlloIdentita.Mese = reader.GetInt32(indiceMese);
                        controlloIdentita.Anno = reader.GetInt32(indiceAnno);

                        controlloIdentita.ControlliEffettuati = reader.GetBoolean(indiceControlliEffettuati);
                        controlloIdentita.ControlloDenunciaSuperato = reader.GetBoolean(indiceControlloDenunciaSuperato);
                        controlloIdentita.ControlloLavoratoreDenunciaSuperato =
                            reader.GetBoolean(indiceControlloLavoratoreDenunciaSuperato);
                        controlloIdentita.ControlloOreDenunciaSuperato =
                            reader.GetBoolean(indiceControlloOreDenunciaSuperato);
                        controlloIdentita.ControlloDebitiSuperato = reader.GetBoolean(indiceControlloDebitiSuperato);
                        controlloIdentita.ControlloDebitiEffettuato = reader.GetBoolean(indiceControlloDebitiEffettuato);

                        if (!reader.IsDBNull(indiceDataUltimaDenuncia))
                            controlloIdentita.DataUltimaDenuncia = reader.GetDateTime(indiceDataUltimaDenuncia);
                        if (!reader.IsDBNull(indiceNomeCassaEdileUltimaDenuncia))
                            controlloIdentita.CassaEdileUltimaDenuncia =
                                reader.GetString(indiceNomeCassaEdileUltimaDenuncia);

                        if (!reader.IsDBNull(indicePartitaIva))
                            controlloIdentita.PartitaIvaImpresa = reader.GetString(indicePartitaIva);

                        //using (
                        //    DbCommand comando2 =
                        //        databaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSSelect"))
                        //{
                        //    DatabaseCemi.AddInParameter(comando2, "@codiceFiscale", DbType.String,
                        //                                controlloIdentita.CodiceFiscale);

                        //    using (IDataReader reader2 = databaseCemi.ExecuteReader(comando2))
                        //    {
                        //        Int32 indiceDataUltimaContribuzione = reader2.GetOrdinal("dataUltimaContribuzione");
                        //        Int32 indicePartitaIVAImpresa = reader2.GetOrdinal("partitaIVAImpresaINPS");
                        //        Int32 indiceCodiceFiscaleImpresa = reader2.GetOrdinal("codiceFiscaleImpresaINPS");

                        //        controlloIdentita.ContribuzioniINPS = new ContribuzioneINPSCollection();
                        //        while (reader2.Read())
                        //        {
                        //            ContribuzioneINPS cont = new ContribuzioneINPS();

                        //            if (!reader2.IsDBNull(indiceDataUltimaContribuzione))
                        //                cont.DataUltimaContribuzioneINPS =
                        //                    reader2.GetDateTime(indiceDataUltimaContribuzione);
                        //            if (!reader2.IsDBNull(indicePartitaIVAImpresa))
                        //                cont.PartitaIVAImpresaINPS =
                        //                    reader2.GetString(indicePartitaIVAImpresa);
                        //            if (!reader2.IsDBNull(indiceCodiceFiscaleImpresa))
                        //                cont.CodiceFiscaleImpresaINPS =
                        //                    reader2.GetString(indiceCodiceFiscaleImpresa);

                        //            controlloIdentita.ContribuzioniINPS.Add(cont);
                        //        }
                        //    }
                        //}

                        //controlloIdentita.DenunceOreCexChange = new DenunceOreCexChangeCollection();
                        //using (DbCommand comando3 =
                        //        DatabaseCemi.GetStoredProcCommand("[dbo].[USP_AccessoCantieriControlliCEXChangeDenunceRisposteSelectNoMilano]"))
                        //{


                        //    DatabaseCemi.AddInParameter(comando3, "@codiceFiscaleLavoratore", DbType.String,
                        //                                controlloIdentita.CodiceFiscale);
                        //    DatabaseCemi.AddInParameter(comando3, "@anno", DbType.Int32,
                        //                                controlloIdentita.Anno);
                        //    DatabaseCemi.AddInParameter(comando3, "@mese", DbType.Int32,
                        //                                controlloIdentita.Mese);

                        //    using (IDataReader reader3 = DatabaseCemi.ExecuteReader(comando3))
                        //    {
                        //        Int32 indexOreOrdinarie = reader3.GetOrdinal("oreOrdinarie");
                        //        Int32 indexCassaEdileId = reader3.GetOrdinal("codiceCassaEdile");
                        //        Int32 indexCassaEdileDesc = reader3.GetOrdinal("descrizioneCassaEdile");

                        //        DenunceOreCexChange attestatoOre;

                        //        while (reader3.Read())
                        //        {
                        //            attestatoOre = new DenunceOreCexChange
                        //                                {
                        //                                    CassaEdileCodice = reader3.GetString(indexCassaEdileId),
                        //                                    CassaEdileDescrizione = reader3.IsDBNull(indexCassaEdileDesc) ? null : reader3.GetString(indexCassaEdileDesc),
                        //                                    OreOrdinarie = reader3.GetDecimal(indexOreOrdinarie),
                        //                                    PeriodoAnno = controlloIdentita.Anno,
                        //                                    PeriodoMese = controlloIdentita.Mese
                        //                                };

                        //            controlloIdentita.DenunceOreCexChange.Add(attestatoOre);
                        //        }
                        //    }
                        //}

                        using (
                            DbCommand comando2 =
                                DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSSelect2"))
                        {
                            DatabaseCemi.AddInParameter(comando2, "@codiceFiscale", DbType.String,
                                                        controlloIdentita.CodiceFiscale);

                            using (IDataReader reader2 = DatabaseCemi.ExecuteReader(comando2))
                            {
                                Int32 indiceSuperato = reader2.GetOrdinal("superato");
                                Int32 indicePartitaIvaImpresa = reader2.GetOrdinal("partitaIVAImpresa");
                                Int32 indiceAnnoInps = reader2.GetOrdinal("anno");
                                Int32 indiceMeseInps = reader2.GetOrdinal("mese");

                                controlloIdentita.ContribuzioniInps = new ContribuzioneInps2Collection();
                                while (reader2.Read())
                                {
                                    ContribuzioneINPS2 cont = new ContribuzioneINPS2();

                                    if (!reader2.IsDBNull(indiceAnnoInps))
                                        cont.Anno =
                                            reader2.GetInt32(indiceAnnoInps);

                                    if (!reader2.IsDBNull(indiceMeseInps))
                                        cont.Mese =
                                            reader2.GetInt32(indiceMeseInps);

                                    if (!reader2.IsDBNull(indiceSuperato))
                                        cont.Superato =
                                            reader2.GetBoolean(indiceSuperato);

                                    if (!reader2.IsDBNull(indicePartitaIvaImpresa))
                                        cont.PartitaIVAImpresa =
                                            reader2.GetString(indicePartitaIvaImpresa);

                                    controlloIdentita.ContribuzioniInps.Add(cont);
                                }
                            }
                        }

                        controlloIdentitaColl.Add(controlloIdentita);
                    }
                }
            }

            return controlloIdentitaColl;
        }

        public Int32 GetCountTimbrature(string partitaIva, string codiceFiscale, int idCantiere)
        {
            Int32 ret = 0;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriGetCountTimbrature")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                if (!string.IsNullOrEmpty(partitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);

                DatabaseCemi.AddOutParameter(comando, "@numeroTimbrature", DbType.Int32, 4);

                using (DatabaseCemi.ExecuteReader(comando))
                {
                    if (DatabaseCemi.GetParameterValue(comando, "@numeroTimbrature") != DBNull.Value)
                        ret = (Int32) DatabaseCemi.GetParameterValue(comando, "@numeroTimbrature");
                }
            }

            return ret;
        }

        public TimbraturaCollection GetTimbratureByFilter(TimbraturaFilter filter)
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelectByFilter")
                )
            {
                if (filter.IdAccessoCantieriWhiteList.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filter.IdAccessoCantieriWhiteList);

                if (filter.DataInizio.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filter.DataInizio.Value);
                if (filter.DataFine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, filter.DataFine.Value);
                if (filter.IdImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filter.IdImpresa.Value);
                if (filter.TipoUtente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoUtente", DbType.Int32, filter.TipoUtente.Value);
                if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filter.CodiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTimbratura = reader.GetOrdinal("idTimbratura");

                    Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceIngressoUscita = reader.GetOrdinal("ingressoUscita");
                    Int32 indiceGestito = reader.GetOrdinal("gestito");
                    Int32 indiceAnomalia = reader.GetOrdinal("anomalia");
                    Int32 indiceDataOra = reader.GetOrdinal("dataOra");
                    Int32 indiceControlliEffettuati = reader.GetOrdinal("controlliEffettuati");
                    Int32 indiceControlloDenunciaSuperato = reader.GetOrdinal("controlloDenunciaSuperato");
                    Int32 indiceControlloLavoratoreDenunciaSuperato =
                        reader.GetOrdinal("controlloLavoratoreDenunciaSuperato");
                    Int32 indiceControlloOreDenunciaSuperato = reader.GetOrdinal("controlloOreDenunciaSuperato");
                    Int32 indiceControlloDebitiSuperato = reader.GetOrdinal("controlloDebitiSuperato");
                    Int32 indiceControlloDebitiEffettuato = reader.GetOrdinal("controlloDebitiEffettuato");

                    #endregion

                    while (reader.Read())
                    {
                        Timbratura timbratura = new Timbratura
                                                    {
                                                        IdTimbratura = reader.GetInt32(indiceIdTimbratura)
                                                    };

                        if (!reader.IsDBNull(indiceIdCantiere))
                            timbratura.IdCantiere = reader.GetInt32(indiceIdCantiere);

                        timbratura.CodiceRilevatore = reader.GetString(indiceCodiceRilevatore);
                        timbratura.RagioneSociale = reader.GetString(indiceRagioneSociale);

                        if (!reader.IsDBNull(indiceLatitudine))
                            timbratura.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            timbratura.Longitudine = reader.GetDecimal(indiceLongitudine);

                        timbratura.CodiceFiscale = reader.GetString(indiceCodiceFiscale);

                        timbratura.IngressoUscita = reader.GetBoolean(indiceIngressoUscita);

                        if (!reader.IsDBNull(indiceGestito))
                            timbratura.Gestito = reader.GetBoolean(indiceGestito);
                        if (!reader.IsDBNull(indiceAnomalia))
                            timbratura.Anomalia = (TipologiaAnomalia) reader.GetInt32(indiceAnomalia);

                        timbratura.DataOra = reader.GetDateTime(indiceDataOra);

                        timbratura.ControlliEffettuati = reader.GetBoolean(indiceControlliEffettuati);
                        timbratura.ControlloDenunciaSuperato = reader.GetBoolean(indiceControlloDenunciaSuperato);
                        timbratura.ControlloLavoratoreDenunciaSuperato =
                            reader.GetBoolean(indiceControlloLavoratoreDenunciaSuperato);
                        timbratura.ControlloOreDenunciaSuperato = reader.GetBoolean(indiceControlloOreDenunciaSuperato);
                        timbratura.ControlloDebitiSuperato = reader.GetBoolean(indiceControlloDebitiSuperato);
                        timbratura.ControlloDebitiEffettuato = reader.GetBoolean(indiceControlloDebitiEffettuato);

                        timbrature.Add(timbratura);
                    }
                }
            }

            return timbrature;
        }

        public WhiteListImpresa GetLavoratoriInDomandaPerImpresa(Int32 idDomanda, Int32? idImpresa,
                                                                 Int32? idAttestatoImpresa)
        {
            WhiteListImpresa domandaImpresa = new WhiteListImpresa();

            if ((!idImpresa.HasValue && !idAttestatoImpresa.HasValue) ||
                (idImpresa.HasValue && idAttestatoImpresa.HasValue))
            {
                throw new ArgumentException(
                    "idImpresa e idAttestatoImpresa non possono essere entrambi nulli o entrambi valorizzati");
            }
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriWhiteListLavoratoriSelectByDomandaEImpresa"))
            {
                domandaImpresa.Impresa = new Impresa();

                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                if (idImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);
                    domandaImpresa.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                    domandaImpresa.Impresa.IdImpresa = idImpresa.Value;
                }
                if (idAttestatoImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriImpresa", DbType.Int32,
                                                idAttestatoImpresa.Value);
                    domandaImpresa.Impresa.TipoImpresa = TipologiaImpresa.Nuova;
                    domandaImpresa.Impresa.IdImpresa = idAttestatoImpresa.Value;
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdDomandaLavoratore = reader.GetOrdinal("idAccessoCantieriWhiteListLavoratori");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceIdAttestatoLavoratore = reader.GetOrdinal("idAccessoCantieriLavoratore");

                    Int32 indiceDataInizioAttivita = reader.GetOrdinal("dataInizioAttivita");
                    Int32 indiceDataFineAttivita = reader.GetOrdinal("dataFineAttivita");

                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");

                    Int32 indiceAtCognome = reader.GetOrdinal("atCognome");
                    Int32 indiceAtNome = reader.GetOrdinal("atNome");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atCodiceFiscale");
                    Int32 indiceAtDataNascita = reader.GetOrdinal("atDataNascita");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        domandaImpresa.Lavoratori.Add(lavoratore);

                        lavoratore.IdDomandaLavoratore = reader.GetInt32(indiceIdDomandaLavoratore);
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                            lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                                lavoratore.Nome = reader.GetString(indiceNome);
                            if (!reader.IsDBNull(indiceDataNascita))
                                lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                                lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);

                            if (!reader.IsDBNull(indiceDataInizioAttivita))
                                lavoratore.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                            if (!reader.IsDBNull(indiceDataFineAttivita))
                                lavoratore.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdAttestatoLavoratore);

                            lavoratore.Cognome = reader.GetString(indiceAtCognome);
                            lavoratore.Nome = reader.GetString(indiceAtNome);
                            lavoratore.DataNascita = reader.GetDateTime(indiceAtDataNascita);
                            lavoratore.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);

                            if (!reader.IsDBNull(indiceDataInizioAttivita))
                                lavoratore.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                            if (!reader.IsDBNull(indiceDataFineAttivita))
                                lavoratore.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                        }
                    }
                }
            }

            return domandaImpresa;
        }

        public ImpresaCollection GetImpreseSelezionateInSubappalto(int idDomanda)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriImpreseSelezionateSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@lavoratoriAutonomi", DbType.Boolean, false);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("idAccessoCantieriImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("impRagioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atImpRagioneSociale");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("impCodiceFiscale");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atImpCodiceFiscale");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            // Impresa SiceNew

                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        else
                        {
                            // Nuova Impresa

                            impresa.TipoImpresa = TipologiaImpresa.Nuova;
                            impresa.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);
                            impresa.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                        }
                    }
                }
            }

            return imprese;
        }

        public ImpresaCollection GetImpreseAutonomeSelezionateInSubappalto(int idDomanda)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriImpreseSelezionateSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@lavoratoriAutonomi", DbType.Boolean, true);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("idAccessoCantieriImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("impRagioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atImpRagioneSociale");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("impCodiceFiscale");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atImpCodiceFiscale");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            // Impresa SiceNew

                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            impresa.LavoratoreAutonomo = true;
                        }
                        else
                        {
                            // Nuova Impresa

                            impresa.TipoImpresa = TipologiaImpresa.Nuova;
                            impresa.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);
                            impresa.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                            impresa.LavoratoreAutonomo = true;
                        }
                    }
                }
            }

            return imprese;
        }

        private Boolean UpdateCommittente(Committente committente, DbTransaction transaction)
        {
            Boolean res = false;

            if (committente == null)
            {
                throw new ArgumentNullException("committente");
            }
            if (!committente.IdCommittente.HasValue)
            {
                throw new ArgumentException("IdCommittente deve essere valorizato");
            }
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriCommittentiUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32,
                                            committente.IdCommittente.Value);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                            committente.RagioneSociale);
                DatabaseCemi.AddInParameter(comando, "@tipologia", DbType.Int16,
                                            (Int16) committente.Tipologia);
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                if (!String.IsNullOrEmpty(committente.Civico))
                    DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, committente.Civico);
                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                if (!String.IsNullOrEmpty(committente.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                committente.CodiceFiscale);
                if (!String.IsNullOrEmpty(committente.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String,
                                                committente.PartitaIva);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomanda(WhiteList domanda)
        {
            bool res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        using (
                            DbCommand comando =
                                DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListUpdate"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, domanda.Indirizzo);

                            if (domanda.IdWhiteList.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32,
                                                            domanda.IdWhiteList);
                            if (!String.IsNullOrEmpty(domanda.Civico))
                                DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, domanda.Civico);
                            if (!String.IsNullOrEmpty(domanda.Comune))
                                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, domanda.Comune);
                            if (!String.IsNullOrEmpty(domanda.Provincia))
                                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, domanda.Provincia);
                            if (!String.IsNullOrEmpty(domanda.Cap))
                                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, domanda.Cap);
                            if (domanda.Latitudine.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal,
                                                            domanda.Latitudine.Value);
                            if (domanda.Longitudine.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal,
                                                            domanda.Longitudine.Value);
                            if (!String.IsNullOrEmpty(domanda.InfoAggiuntiva))
                                DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String,
                                                            domanda.InfoAggiuntiva);

                            if (!String.IsNullOrEmpty(domanda.Descrizione))
                                DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String,
                                                            domanda.Descrizione);

                            if (domanda.DataInizio.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime,
                                                            domanda.DataInizio);
                            if (domanda.DataFine.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, domanda.DataFine);

                            if (!String.IsNullOrEmpty(domanda.AutorizzazioneAlSubappalto))
                                DatabaseCemi.AddInParameter(comando, "@autorizzazioneAlSubappalto", DbType.String,
                                                            domanda.AutorizzazioneAlSubappalto);

                            bool resC;
                            Int32? idComm = null;
                            if (domanda.Committente != null)
                            {
                                if (domanda.Committente.IdCommittente.HasValue)
                                {
                                    resC = UpdateCommittente(domanda.Committente, transaction);
                                }
                                else
                                {
                                    idComm = InsertCommittente(domanda.Committente, transaction);
                                    resC = idComm.HasValue;
                                }
                            }
                            else
                            {
                                idComm = InsertCommittente(domanda.Committente, transaction);
                                resC = idComm.HasValue;
                            }

                            if (idComm.HasValue)
                            {
                                if (idComm.Value != -1)
                                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriCommittente",
                                                                DbType.Int32,
                                                                idComm.Value);
                            }
                            else
                            {
                                if (domanda.Committente != null)
                                {
                                    if (domanda.Committente.IdCommittente.HasValue)
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriCommittente",
                                                                    DbType.Int32,
                                                                    domanda.Committente.IdCommittente.Value);
                                    }
                                }
                            }

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                if (resC)
                                {
                                    DeleteSubappaltiDellaDomanda(domanda.IdWhiteList.Value, transaction);
                                    DeleteLavoratoriDellaDomanda(domanda.IdWhiteList.Value, transaction);
                                    DeleteAltrePersoneDellaDomanda(domanda.IdWhiteList.Value, transaction);
                                    DeleteRilevatoriDellaDomanda(domanda.IdWhiteList.Value, transaction);
                                    DeleteReferentiDellaDomanda(domanda.IdWhiteList.Value, transaction);

                                    if (InsertUpdateSubappaltieLavoratori(domanda, transaction))
                                    {
                                        if (InsertReferenti(domanda, transaction))
                                        {
                                            if (InsertAltrePersoneAbilitate(domanda, transaction))
                                            {
                                                if (InsertRilevatori(domanda, transaction))
                                                {
                                                    res = true;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Errore durante l'InsertUpdateSubappaltieLavoratori");
                                    }
                                }
                                else
                                {
                                    throw new Exception("Errore durante l'UpdateCommittente");
                                }
                            }
                            else
                            {
                                throw new Exception("Errore durante l'inserimento della domanda");
                            }

                            if (res)
                            {
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        public bool InsertDomanda(WhiteList domanda)
        {
            bool res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        using (
                            DbCommand comando =
                                DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, domanda.Indirizzo);

                            if (!String.IsNullOrEmpty(domanda.Civico))
                                DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, domanda.Civico);
                            if (!String.IsNullOrEmpty(domanda.Comune))
                                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, domanda.Comune);
                            if (!String.IsNullOrEmpty(domanda.Provincia))
                                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, domanda.Provincia);
                            if (!String.IsNullOrEmpty(domanda.Cap))
                                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, domanda.Cap);
                            if (domanda.Latitudine.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal,
                                                            domanda.Latitudine.Value);
                            if (domanda.Longitudine.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal,
                                                            domanda.Longitudine.Value);
                            if (!String.IsNullOrEmpty(domanda.InfoAggiuntiva))
                                DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String,
                                                            domanda.InfoAggiuntiva);

                            if (!String.IsNullOrEmpty(domanda.Descrizione))
                                DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String,
                                                            domanda.Descrizione);

                            if (domanda.DataInizio.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime,
                                                            domanda.DataInizio);
                            if (domanda.DataFine.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, domanda.DataFine);

                            if (!String.IsNullOrEmpty(domanda.AutorizzazioneAlSubappalto))
                                DatabaseCemi.AddInParameter(comando, "@autorizzazioneAlSubappalto", DbType.String,
                                                            domanda.AutorizzazioneAlSubappalto);


                            DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32,
                                                        domanda.IdUtente);
                            DatabaseCemi.AddInParameter(comando, "@guid", DbType.Guid,
                                                        domanda.GuidId);

                            DatabaseCemi.AddOutParameter(comando, "@idWhiteList", DbType.Int32, 4);

                            Int32? resCommittente = null;

                            if (domanda.Committente != null)
                            {
                                resCommittente = InsertCommittente(domanda, transaction);

                                if (resCommittente.HasValue)
                                {
                                    if (resCommittente.Value != -1)
                                        DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriCommittente",
                                                                    DbType.Int32,
                                                                    resCommittente.Value);
                                }
                            }

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                domanda.IdWhiteList =
                                    (Int32) DatabaseCemi.GetParameterValue(comando, "@idWhiteList");

                                if (domanda.Committente != null || resCommittente != -1)
                                {
                                    if (InsertUpdateSubappaltieLavoratori(domanda, transaction))
                                    {
                                        if (InsertReferenti(domanda, transaction))
                                        {
                                            if (InsertAltrePersoneAbilitate(domanda, transaction))
                                            {
                                                if (InsertRilevatori(domanda, transaction))
                                                {
                                                    res = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception(
                                        "Errore durante l'InsertUpdateSubappaltieLavoratori o l'InsertAltrePersoneAbilitate o l'insertRilevatori o l'insertReferenti o l'insertcommittente");
                                }
                            }
                            else
                            {
                                object pValue = DatabaseCemi.GetParameterValue(comando, "@idWhiteList");
                                if (pValue != null)
                                {
                                    Int32 codiceErrore = (Int32) pValue;

                                    if (codiceErrore == -1)
                                    {
                                        throw new RichiestaGiaInseritaException();
                                    }
                                }

                                throw new Exception("Errore durante l'inserimento della domanda");
                            }

                            if (res)
                            {
                                transaction.Commit();
                            }
                            else
                            {
                                transaction.Rollback();
                            }
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private Boolean InsertAltrePersoneAbilitate(WhiteList domanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }
            if (domanda.ListaAltrePersone == null)
            {
                throw new ArgumentException("Lista altre persone deve essere valorizzata");
            }
            if (domanda.IdWhiteList != null)
                res = InsertAltrePersone(domanda.ListaAltrePersone, domanda.IdWhiteList.Value, transaction);

            return res;
        }

        private Int32? InsertCommittente(WhiteList domanda, DbTransaction transaction)
        {
            Int32? res = null;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }
            if (domanda.Committente == null)
            {
                //throw new ArgumentException("Lista referenti deve essere valorizzata");
            }
            else
            {
                res = InsertCommittente(domanda.Committente, transaction);
            }

            return res;
        }

        private Boolean InsertReferenti(WhiteList domanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }
            if (domanda.ListaReferenti == null)
            {
                throw new ArgumentException("Lista referenti deve essere valorizzata");
            }
            if (domanda.IdWhiteList != null)
                res = InsertReferenti(domanda.ListaReferenti, domanda.IdWhiteList.Value, transaction);

            return res;
        }

        private Boolean InsertAltrePersone(IEnumerable<AltraPersona> altrePersone, Int32 idDomanda,
                                           DbTransaction transaction)
        {
            Boolean res;

            try
            {
                foreach (AltraPersona ap in altrePersone)
                {
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriAltrePersoneInsert");
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, ap.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, ap.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, ap.DataNascita);
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, ap.CodiceFiscale);
                    DatabaseCemi.AddInParameter(comando, "@email", DbType.String, ap.Email);
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, ap.Telefono);
                    DatabaseCemi.AddInParameter(comando, "@modalitaContatto", DbType.String, ap.ModalitaContatto);
                    DatabaseCemi.AddInParameter(comando, "@ruolo", DbType.Int32, ap.TipoRuolo);
                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriWhiteList", DbType.Int32, idDomanda);

                    int numRes = DatabaseCemi.ExecuteNonQuery(comando, transaction);

                    if (numRes != 1)
                        throw new Exception();
                }

                res = true;
            }
            catch
            {
                res = false;
            }

            return res;
        }

        private Int32? InsertCommittente(Committente committente, DbTransaction transaction)
        {
            Int32? res = null;

            try
            {
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriCommittentiInsert");
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, committente.RagioneSociale);
                DatabaseCemi.AddInParameter(comando, "@tipologia", DbType.Int32, committente.Tipologia);
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, committente.Civico);
                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.Int32, committente.Provincia);
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.Int32, committente.Comune);
                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                DatabaseCemi.AddInParameter(comando, "@codicefiscale", DbType.String, committente.CodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, committente.PartitaIva);

                DatabaseCemi.AddOutParameter(comando, "@idCommittente", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando, transaction);

                object pValue = DatabaseCemi.GetParameterValue(comando, "@idCommittente");
                if (pValue != null)
                {
                    res = Int32.Parse(pValue.ToString());
                }
            }
            catch (Exception)
            {
                res = -1;
            }

            return res;
        }

        public Boolean InsertReferenti(IEnumerable<Referente> referenti, Int32 idDomanda,
                                        DbTransaction transaction)
        {
            Boolean res;

            try
            {
                foreach (Referente ap in referenti)
                {
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriReferentiInsert");
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, ap.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, ap.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, ap.DataNascita);
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, ap.CodiceFiscale);
                    DatabaseCemi.AddInParameter(comando, "@email", DbType.String, ap.Email);
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, ap.Telefono);
                    DatabaseCemi.AddInParameter(comando, "@modalitaContatto", DbType.String, ap.ModalitaContatto);
                    DatabaseCemi.AddInParameter(comando, "@ruolo", DbType.Int32, ap.TipoRuolo);
                    DatabaseCemi.AddInParameter(comando, "@affiliazione", DbType.Int32, ap.TipoAffiliazione);
                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriWhiteList", DbType.Int32, idDomanda);

                    Int32 numRes;
                    if (transaction != null)
                    {
                        numRes = DatabaseCemi.ExecuteNonQuery(comando, transaction);
                    }
                    else
                    {
                        numRes = DatabaseCemi.ExecuteNonQuery(comando);
                    }

                    if (numRes != 1)
                        throw new Exception();
                }

                res = true;
            }
            catch
            {
                res = false;
            }

            return res;
        }

        private Boolean InsertRilevatori(WhiteList domanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            if (transaction == null)
            {
                throw new ArgumentNullException("transaction");
            }
            if (domanda.Rilevatori == null)
            {
                throw new ArgumentException("Rilevatori deve essere valorizzata");
            }
            if (domanda.IdWhiteList != null)
                res = InsertTerminali(domanda.Rilevatori, domanda.IdWhiteList.Value, transaction);

            return res;
        }

        private Boolean InsertTerminali(IEnumerable<RilevatoreCantiere> rilevatori, Int32 idDomanda,
                                        DbTransaction transaction)
        {
            Boolean res;

            try
            {
                foreach (RilevatoreCantiere ap in rilevatori)
                {
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoriCantieriInsert");

                    DatabaseCemi.AddInParameter(comando, "@idRilevatore", DbType.Int32, ap.IdRilevatore);
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idDomanda);
                    DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, ap.DataInizio);
                    DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, ap.DataFine);


                    int numRes = DatabaseCemi.ExecuteNonQuery(comando, transaction);

                    if (numRes != 1)
                        throw new Exception();
                }

                res = true;
            }
            catch (Exception)
            {
                res = false;
            }

            return res;
        }


        public Boolean InsertUpdateSubappaltieLavoratori(WhiteList domanda, DbTransaction transaction)
        {
            if (transaction == null)
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();

                    using (transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        Boolean res = false;

                        if (domanda == null)
                        {
                            throw new ArgumentNullException("domanda");
                        }
                        if (transaction == null)
                        {
                            throw new ArgumentNullException("transaction");
                        }
                        if (domanda.Subappalti == null)
                        {
                            throw new ArgumentException("Subappalti deve essere valorizzato");
                        }
                        if (domanda.Lavoratori == null)
                        {
                            throw new ArgumentException("Lavoratori deve essere valorizzato");
                        }
                        // Subappalti
                        if (domanda.IdWhiteList != null)
                            res = InsertUpdateSubappalti(domanda.Subappalti, domanda.IdWhiteList.Value, transaction);

                        GestisciImpreseConIdTemporaneiLavoratori(domanda.Subappalti, domanda.Lavoratori);

                        if (res)
                        {
                            // Lavoratori
                            res = InsertUpdateLavoratori(domanda.Lavoratori, domanda.IdWhiteList.Value, transaction);

                            if (res)
                            {
                                transaction.Commit();
                            }
                        }

                        return res;
                    }
                }
            }
            else
            {
                Boolean res = false;

                if (domanda == null)
                {
                    throw new ArgumentNullException("domanda");
                }
                if (transaction == null)
                {
                    throw new ArgumentNullException("transaction");
                }
                if (domanda.Subappalti == null)
                {
                    throw new ArgumentException("Subappalti deve essere valorizzato");
                }
                if (domanda.Lavoratori == null)
                {
                    throw new ArgumentException("Lavoratori deve essere valorizzato");
                }
                // Subappalti
                if (domanda.IdWhiteList != null)
                    res = InsertUpdateSubappalti(domanda.Subappalti, domanda.IdWhiteList.Value, transaction);

                GestisciImpreseConIdTemporaneiLavoratori(domanda.Subappalti, domanda.Lavoratori);

                if (res)
                {
                    // Lavoratori
                    res = InsertUpdateLavoratori(domanda.Lavoratori, domanda.IdWhiteList.Value, transaction);
                }

                return res;
            }
        }

        public Boolean InsertUpdateLavoratori(IEnumerable<WhiteListImpresa> lavoratori, Int32 idDomanda,
                                               DbTransaction transaction)
        {
            Boolean res = true;

            foreach (WhiteListImpresa domandaImpresa in lavoratori)
            {
                if (domandaImpresa.Impresa == null)
                {
                    throw new ArgumentException("L'impresa deve essere valorizzata e avere un Id");
                }
                if (domandaImpresa.Lavoratori == null)
                {
                    throw new Exception("Lavoratori deve essere valorizzato");
                }
                foreach (Lavoratore lavoratore in domandaImpresa.Lavoratori)
                {
                    if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew
                        && !lavoratore.IdLavoratore.HasValue)
                    {
                        throw new ArgumentException(
                            "I lavoratori dell'anagrafica devono avere obbligatoriamente un Id");
                    }
                    if (
                        !InsertLavoratoreImpresa(lavoratore, idDomanda, domandaImpresa.Impresa,
                                                 transaction))
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        public Boolean InsertUpdateSubappalti(SubappaltoCollection subappalti, Int32 idDomanda,
                                               DbTransaction transaction)
        {
            Boolean res = true;

            for (Int32 i = 0; i < subappalti.Count; i++)
            {
                Subappalto subappalto = subappalti[i];

                if (subappalto.Appaltata == null)
                {
                    throw new ArgumentNullException("subappalto.Appaltata");
                }
                if ((subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew
                     && !subappalto.Appaltata.IdImpresa.HasValue)
                    ||
                    (subappalto.Appaltante != null
                     && subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew
                     && !subappalto.Appaltante.IdImpresa.HasValue))
                {
                    throw new ArgumentException(
                        "Le imprese provenienti dall'anagrafica devono avere obbligatoriamente un Id");
                }
                if (!InsertSubappalto(subappalto, idDomanda, transaction))
                {
                    res = false;
                    break;
                }

                GestisciImpreseConIdTemporanei(subappalti, i);
            }

            return res;
        }

        private static void GestisciImpreseConIdTemporanei(IList<Subappalto> subappalti, Int32 indice)
        {
            // Ciclo sull'appaltata
            if (subappalti[indice].Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
            {
                for (Int32 k = indice + 1; k < subappalti.Count; k++)
                {
                    if (subappalti[k].Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltata.IdImpresa.HasValue
                        && subappalti[k].Appaltata.IdTemporaneo == subappalti[indice].Appaltata.IdTemporaneo)
                    {
                        subappalti[k].Appaltata.IdImpresa = subappalti[indice].Appaltata.IdImpresa;
                    }

                    if (subappalti[k].Appaltante != null
                        && subappalti[k].Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltante.IdImpresa.HasValue
                        && subappalti[k].Appaltante.IdTemporaneo == subappalti[indice].Appaltata.IdTemporaneo)
                    {
                        subappalti[k].Appaltante.IdImpresa = subappalti[indice].Appaltata.IdImpresa;
                    }
                }
            }

            // Ciclo sull'appaltante
            if (subappalti[indice].Appaltante != null &&
                subappalti[indice].Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
            {
                for (Int32 k = indice + 1; k < subappalti.Count; k++)
                {
                    if (subappalti[k].Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltata.IdImpresa.HasValue
                        && subappalti[k].Appaltata.IdTemporaneo == subappalti[indice].Appaltante.IdTemporaneo)
                    {
                        subappalti[k].Appaltata.IdImpresa = subappalti[indice].Appaltante.IdImpresa;
                    }

                    if (subappalti[k].Appaltante != null
                        && subappalti[k].Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltante.IdImpresa.HasValue
                        && subappalti[k].Appaltante.IdTemporaneo == subappalti[indice].Appaltante.IdTemporaneo)
                    {
                        subappalti[k].Appaltante.IdImpresa = subappalti[indice].Appaltante.IdImpresa;
                    }
                }
            }
        }

        private static void GestisciImpreseConIdTemporaneiLavoratori(IEnumerable<Subappalto> subappalti,
                                                                     IEnumerable<WhiteListImpresa> lavoratori)
        {
            foreach (Subappalto sub in subappalti)
            {
                // Ciclo sull'appaltata
                if (sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                {
                    foreach (WhiteListImpresa domImp in lavoratori)
                    {
                        if (domImp.Impresa.TipoImpresa == TipologiaImpresa.Nuova
                            && !domImp.Impresa.IdImpresa.HasValue
                            && domImp.Impresa.IdTemporaneo == sub.Appaltata.IdTemporaneo)
                        {
                            domImp.Impresa.IdImpresa = sub.Appaltata.IdImpresa;
                        }
                    }
                }

                // Ciclo sull'appaltante
                if (sub.Appaltante != null && sub.Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
                {
                    foreach (WhiteListImpresa domImp in lavoratori)
                    {
                        if (domImp.Impresa.TipoImpresa == TipologiaImpresa.Nuova
                            && !domImp.Impresa.IdImpresa.HasValue
                            && domImp.Impresa.IdTemporaneo == sub.Appaltante.IdTemporaneo)
                        {
                            domImp.Impresa.IdImpresa = sub.Appaltante.IdImpresa;
                        }
                    }
                }
            }
        }

        public Boolean InsertSubappalto(Subappalto subappalto, Int32 idDomanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (GestioneImprese(subappalto, transaction))
            {
                if (subappalto.Appaltante != null)
                {
                    if (subappalto.Appaltante.LavoratoreAutonomo)
                    {
                        UpdateImpresa(subappalto.Appaltante, transaction);
                    }
                }
                if (subappalto.Appaltata != null)
                {
                    if (subappalto.Appaltata.LavoratoreAutonomo)
                    {
                        UpdateImpresa(subappalto.Appaltata, transaction);
                    }
                }

                // Inserimento del subappalto
                using (
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSubappaltiInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                    if (subappalto.Appaltata != null)
                        if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                        {
                            if (subappalto.Appaltata.IdImpresa != null)
                                DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltata", DbType.Int32,
                                                            subappalto.Appaltata.IdImpresa.Value);
                        }
                        else
                        {
                            if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                            {
                                if (subappalto.Appaltata.IdImpresa != null)
                                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriImpresaAppaltata",
                                                                DbType.Int32,
                                                                subappalto.Appaltata.IdImpresa.Value);
                            }
                        }
                    if (subappalto.Appaltante != null)
                    {
                        if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                        {
                            if (subappalto.Appaltante.IdImpresa != null)
                                DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltante", DbType.Int32,
                                                            subappalto.Appaltante.IdImpresa.Value);
                        }
                        else
                        {
                            if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
                            {
                                if (subappalto.Appaltante.IdImpresa != null)
                                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriImpresaAppaltante",
                                                                DbType.Int32,
                                                                subappalto.Appaltante.IdImpresa.Value);
                            }
                        }
                    }

                    if (subappalto.DataInizioAttivitaAppaltatrice.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioAttivitaAppaltante", DbType.DateTime,
                                                    subappalto.DataInizioAttivitaAppaltatrice.Value);

                    if (subappalto.DataFineAttivitaAppaltatrice.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFineAttivitaAppaltante", DbType.DateTime,
                                                    subappalto.DataFineAttivitaAppaltatrice.Value);

                    if (subappalto.DataInizioAttivitaAppaltata.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioAttivitaAppaltata", DbType.DateTime,
                                                    subappalto.DataInizioAttivitaAppaltata.Value);

                    if (subappalto.DataFineAttivitaAppaltata.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFineAttivitaAppaltata", DbType.DateTime,
                                                    subappalto.DataFineAttivitaAppaltata.Value);

                    //databaseCemi.AddInParameter(comando, "@autorizzazioneAlSubappaltoAppaltata", DbType.String,
                    //                            subappalto.AutorizzazioneAlSubappaltoAppaltata);

                    //databaseCemi.AddInParameter(comando, "@autorizzazioneAlSubappaltoAppaltatrice", DbType.String,
                    //                            subappalto.AutorizzazioneAlSubappaltoAppaltatrice);


                    DatabaseCemi.AddOutParameter(comando, "@idSubappalto", DbType.Int32, 4);

                    if (transaction != null)
                    {
                        if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        {
                            subappalto.IdSubappalto = (Int32) DatabaseCemi.GetParameterValue(comando, "@idSubappalto");

                            //if (subappalto.Appaltante != null)
                            //{
                            //    if (subappalto.Appaltante.LavoratoreAutonomo)
                            //    {
                            //        UpdateImpresa(subappalto.Appaltante, transaction);
                            //    }
                            //}
                            //if (subappalto.Appaltata != null)
                            //{
                            //    if (subappalto.Appaltata.LavoratoreAutonomo)
                            //    {
                            //        UpdateImpresa(subappalto.Appaltata, transaction);
                            //    }
                            //}

                            res = true;
                        }
                        else
                        {
                            throw new Exception("Errore durante l'inserimento del subappalto");
                        }
                    }
                    else
                    {
                        if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        {
                            subappalto.IdSubappalto = (Int32) DatabaseCemi.GetParameterValue(comando, "@idSubappalto");
                            res = true;
                        }
                    }
                }
            }

            return res;
        }

        public bool InsertLavoratoreImpresa(Lavoratore lavoratore, Int32 idDomanda, Impresa impresa,
                                             DbTransaction transaction)
        {
            Boolean res = false;

            if (GestioneLavoratori(lavoratore, transaction))
            {
                // Inserimento del rapporto lavoratore impresa
                using (
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListLavoratoriInsert"))
                {
                    if (lavoratore.DataInizioAttivita.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioAttivita", DbType.DateTime,
                                                    lavoratore.DataInizioAttivita);

                    if (lavoratore.DataFineAttivita.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFineAttivita", DbType.DateTime,
                                                    lavoratore.DataFineAttivita);

                    DatabaseCemi.AddInParameter(comando, "@effettuaControlli", DbType.Boolean,
                                                lavoratore.EffettuaControlli);

                    if (lavoratore.DataAssunzione.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                                                    lavoratore.DataAssunzione);

                    if (lavoratore.PaeseNascita != null)
                        DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String,
                                                    lavoratore.PaeseNascita);

                    if (lavoratore.LuogoNascita != null)
                        DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String,
                                                    lavoratore.LuogoNascita);

                    if (lavoratore.ProvinciaNascita != null)
                        DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                                                    lavoratore.ProvinciaNascita);

                    if (lavoratore.Foto != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@foto", DbType.Binary,
                                                    lavoratore.Foto);
                    }

                    if (lavoratore.DataStampaBadge.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataStampaBadge", DbType.DateTime,
                                                    lavoratore.DataStampaBadge);

                    if (!String.IsNullOrWhiteSpace(lavoratore.ContrattoApplicato))
                        DatabaseCemi.AddInParameter(comando, "@contrattoApplicato", DbType.String,
                                                    lavoratore.ContrattoApplicato);

                    DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                    if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        if (impresa.IdImpresa != null)
                            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                    }
                    else
                    {
                        if (impresa.TipoImpresa == TipologiaImpresa.Nuova)
                        {
                            if (impresa.IdImpresa != null)
                                DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriImpresa", DbType.Int32,
                                                            impresa.IdImpresa.Value);
                        }
                    }
                    if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                    {
                        if (lavoratore.IdLavoratore != null)
                            DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                        lavoratore.IdLavoratore.Value);
                    }
                    else
                    {
                        if (lavoratore.TipoLavoratore == TipologiaLavoratore.Nuovo)
                        {
                            if (lavoratore.IdLavoratore != null)
                                DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriLavoratore", DbType.Int32,
                                                            lavoratore.IdLavoratore.Value);
                        }
                    }
                    DatabaseCemi.AddOutParameter(comando, "@idWhiteListLavoratore", DbType.Int32, 4);

                    if (transaction != null)
                    {
                        if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        {
                            lavoratore.IdDomandaLavoratore =
                                (Int32) DatabaseCemi.GetParameterValue(comando, "@idWhiteListLavoratore");
                            res = true;
                        }
                        else
                        {
                            throw new Exception("Errore durante l'inserimento del lavoratore associato all'impresa");
                        }
                    }
                    else
                    {
                        if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        {
                            lavoratore.IdDomandaLavoratore =
                                (Int32) DatabaseCemi.GetParameterValue(comando, "@idWhiteListLavoratore");
                            res = true;
                        }
                        else
                        {
                            throw new Exception("Errore durante l'inserimento del lavoratore associato all'impresa");
                        }
                    }
                }
            }

            return res;
        }

        private bool GestioneLavoratori(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res;

            if (lavoratore.TipoLavoratore == TipologiaLavoratore.Nuovo
                && !lavoratore.IdLavoratore.HasValue)
            {
                res = InsertLavoratore(lavoratore, transaction);
            }
            else
                res = true;

            return res;
        }

        private Boolean GestioneImprese(Subappalto subappalto, DbTransaction transaction)
        {
            Boolean res;

            if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                && !subappalto.Appaltata.IdImpresa.HasValue)
            {
                res = InsertImpresa(subappalto.Appaltata, transaction);

                if (!res)
                {
                    throw new Exception(String.Format("Errore durante l'inserimento dell'impresa {0}",
                                                      subappalto.Appaltata.RagioneSociale));
                }
            }

            if (subappalto.Appaltante != null)
            {
                if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                    && !subappalto.Appaltante.IdImpresa.HasValue)
                {
                    res = InsertImpresa(subappalto.Appaltante, transaction);

                    if (!res)
                    {
                        throw new Exception(String.Format("Errore durante l'inserimento dell'impresa {0}",
                                                          subappalto.Appaltante.RagioneSociale));
                    }
                }
            }

            return true;
        }

        public void DeleteSubappaltiDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSubappaltiDeleteByIdWhiteList"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);

                if (transaction != null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
            }
        }

        public void DeleteSubappalto(Int32 idSubappalto, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSubappaltiDeleteById"))
            {
                DatabaseCemi.AddInParameter(comando, "@id", DbType.Int32, idSubappalto);

                if (transaction != null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
            }
        }

        private void DeleteAltrePersoneDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriAltrePersoneDeleteByIdWhiteList"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        public void DeleteReferentiDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriReferentiDeleteByIdWhiteList"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                if (transaction != null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
            }
        }

        private void DeleteRilevatoriDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoriDeleteByIdWhiteList"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        public void DeleteLavoratoriDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListLavoratoriDeleteByIdWhiteList"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);
                if (transaction != null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
            }
        }


        public WhiteListCollection GetDomandeByFilter(WhiteListFilter filtro)
        {
            WhiteListCollection domande = new WhiteListCollection();


            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSelectByFilter")
                )
            {
                if (!String.IsNullOrEmpty(filtro.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filtro.Indirizzo);
                if (!String.IsNullOrEmpty(filtro.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.Comune);

                if (filtro.IdCommittente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32, filtro.IdCommittente.Value);
                }
                else
                {
                    if (filtro.IdUtente.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, filtro.IdUtente.Value);
                    }
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdDomanda = reader.GetOrdinal("idAccessoCantieriWhiteList");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceInfoAggiuntiva = reader.GetOrdinal("infoAggiuntiva");

                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizio");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFine");

                    Int32 indiceIdUtente = reader.GetOrdinal("idUtente");
                    Int32 indiceLoginUtente = reader.GetOrdinal("login");

                    Int32 indiceAutorizzazioneAlSubappalto = reader.GetOrdinal("autorizzazioneAlSubappalto");

                    while (reader.Read())
                    {
                        WhiteList domanda = new WhiteList();
                        domande.Add(domanda);

                        domanda.IdWhiteList = reader.GetInt32(indiceIdDomanda);
                        domanda.Indirizzo = reader.GetString(indiceIndirizzo);
                        domanda.IdUtente = reader.GetInt32(indiceIdUtente);
                        domanda.LoginUtente = reader.GetString(indiceLoginUtente);

                        if (!reader.IsDBNull(indiceCivico))
                            domanda.Civico = reader.GetString(indiceCivico);
                        if (!reader.IsDBNull(indiceComune))
                            domanda.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            domanda.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            domanda.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceInfoAggiuntiva))
                            domanda.InfoAggiuntiva = reader.GetString(indiceInfoAggiuntiva);

                        if (!reader.IsDBNull(indiceDescrizione))
                            domanda.Descrizione = reader.GetString(indiceDescrizione);

                        if (!reader.IsDBNull(indiceAutorizzazioneAlSubappalto))
                            domanda.AutorizzazioneAlSubappalto = reader.GetString(indiceAutorizzazioneAlSubappalto);

                        if (!reader.IsDBNull(indiceDataInizio))
                            domanda.DataInizio = reader.GetDateTime(indiceDataInizio);
                        if (!reader.IsDBNull(indiceDataFine))
                            domanda.DataFine = reader.GetDateTime(indiceDataFine);
                    }
                }
            }

            return domande;
        }

        public WhiteList GetDomandaByKey(Int32 idDomanda)
        {
            WhiteList domanda = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Domanda

                    #region Indici per reader

                    Int32 indiceIdDomanda = reader.GetOrdinal("idAccessoCantieriWhiteList");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceInfoAggiuntiva = reader.GetOrdinal("infoAggiuntiva");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");

                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizio");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFine");

                    Int32 indiceAutorizzazioneAlSubappalto = reader.GetOrdinal("autorizzazioneAlSubappalto");

                    Int32 indiceCommittenteIdAccessoCantieriCommittente =
                        reader.GetOrdinal("idAccessoCantieriCommittente");
                    Int32 indiceCommittenteRagioneSociale = reader.GetOrdinal("ragioneSocialeCommittente");
                    Int32 indiceCommittenteTipologia = reader.GetOrdinal("tipologiaCommittente");
                    Int32 indiceCommittenteIndirizzo = reader.GetOrdinal("indirizzoCommittente");
                    Int32 indiceCommittenteCivico = reader.GetOrdinal("civicoCommittente");
                    Int32 indiceCommittenteProvincia = reader.GetOrdinal("provinciaCommittente");
                    Int32 indiceCommittenteProvinciaDescrizione = reader.GetOrdinal("provinciaDescrizioneCommittente");
                    Int32 indiceCommittenteComune = reader.GetOrdinal("comuneCommittente");
                    Int32 indiceCommittenteComuneDescrizione = reader.GetOrdinal("comuneDescrizioneCommittente");
                    Int32 indiceCommittenteCap = reader.GetOrdinal("capCommittente");
                    Int32 indiceCommittenteCodiceFiscale = reader.GetOrdinal("codiceFiscaleCommittente");
                    Int32 indiceCommittentePartitaIva = reader.GetOrdinal("partitaIVACommittente");

                    Int32 indiceCommittenteUtIdCommittente = reader.GetOrdinal("committenteIdCommittente");
                    Int32 indiceCommittenteUtIdUtente = reader.GetOrdinal("committenteIdUtente");
                    Int32 indiceCommittenteUtCognome = reader.GetOrdinal("committenteCognome");
                    Int32 indiceCommittenteUtNome = reader.GetOrdinal("committenteNome");
                    Int32 indiceCommittenteUtRagioneSociale = reader.GetOrdinal("committenteRagioneSociale");
                    Int32 indiceCommittenteUtPartitaIva = reader.GetOrdinal("committentePartitaIva");
                    Int32 indiceCommittenteUtCodiceFiscale = reader.GetOrdinal("committenteCodiceFiscale");
                    Int32 indiceCommittenteUtIndirizzo = reader.GetOrdinal("committenteIndirizzo");
                    Int32 indiceCommittenteUtComune = reader.GetOrdinal("committenteComune");
                    Int32 indiceCommittenteUtProvincia = reader.GetOrdinal("committenteProvincia");
                    Int32 indiceCommittenteUtCap = reader.GetOrdinal("committenteCap");
                    Int32 indiceCommittenteUtTelefono = reader.GetOrdinal("committenteTelefono");
                    Int32 indiceCommittenteUtFax = reader.GetOrdinal("committenteFax");
                    Int32 indiceCommittenteUtEmail = reader.GetOrdinal("committenteEmail");
                    Int32 indiceCommittenteUtTipologia = reader.GetOrdinal("committenteTipologia");

                    #endregion

                    if (reader.Read())
                    {
                        domanda = new WhiteList
                                      {
                                          IdWhiteList = reader.GetInt32(indiceIdDomanda),
                                          Indirizzo = reader.GetString(indiceIndirizzo)
                                      };

                        if (!reader.IsDBNull(indiceCivico))
                            domanda.Civico = reader.GetString(indiceCivico);
                        if (!reader.IsDBNull(indiceComune))
                            domanda.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            domanda.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            domanda.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceInfoAggiuntiva))
                            domanda.InfoAggiuntiva = reader.GetString(indiceInfoAggiuntiva);
                        if (!reader.IsDBNull(indiceLatitudine))
                            domanda.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            domanda.Longitudine = reader.GetDecimal(indiceLongitudine);

                        if (!reader.IsDBNull(indiceDescrizione))
                            domanda.Descrizione = reader.GetString(indiceDescrizione);

                        if (!reader.IsDBNull(indiceAutorizzazioneAlSubappalto))
                            domanda.AutorizzazioneAlSubappalto = reader.GetString(indiceAutorizzazioneAlSubappalto);

                        if (!reader.IsDBNull(indiceDataInizio))
                            domanda.DataInizio = reader.GetDateTime(indiceDataInizio);
                        if (!reader.IsDBNull(indiceDataFine))
                            domanda.DataFine = reader.GetDateTime(indiceDataFine);

                        if (!reader.IsDBNull(indiceCommittenteIdAccessoCantieriCommittente))
                        {
                            domanda.Committente = new Committente
                                                      {
                                                          IdCommittente =
                                                              reader.GetInt32(
                                                                  indiceCommittenteIdAccessoCantieriCommittente)
                                                      };
                        }
                        if (!reader.IsDBNull(indiceCommittenteRagioneSociale))
                            domanda.Committente.RagioneSociale = reader.GetString(indiceCommittenteRagioneSociale);
                        if (!reader.IsDBNull(indiceCommittenteTipologia))
                            domanda.Committente.Tipologia =
                                (TipologiaCommittente) reader.GetInt16(indiceCommittenteTipologia);
                        if (!reader.IsDBNull(indiceCommittenteIndirizzo))
                            domanda.Committente.Indirizzo = reader.GetString(indiceCommittenteIndirizzo);
                        if (!reader.IsDBNull(indiceCommittenteCivico))
                            domanda.Committente.Civico = reader.GetString(indiceCommittenteCivico);
                        if (!reader.IsDBNull(indiceCommittenteProvincia))
                            domanda.Committente.Provincia = reader.GetInt32(indiceCommittenteProvincia);
                        if (!reader.IsDBNull(indiceCommittenteProvinciaDescrizione))
                            domanda.Committente.ProvinciaDescrizione =
                                reader.GetString(indiceCommittenteProvinciaDescrizione);
                        if (!reader.IsDBNull(indiceCommittenteComune))
                            domanda.Committente.Comune = reader.GetInt64(indiceCommittenteComune);
                        if (!reader.IsDBNull(indiceCommittenteComuneDescrizione))
                            domanda.Committente.ComuneDescrizione = reader.GetString(indiceCommittenteComuneDescrizione);
                        if (!reader.IsDBNull(indiceCommittenteCap))
                            domanda.Committente.Cap = reader.GetString(indiceCommittenteCap);
                        if (!reader.IsDBNull(indiceCommittenteCodiceFiscale))
                            domanda.Committente.CodiceFiscale = reader.GetString(indiceCommittenteCodiceFiscale);
                        if (!reader.IsDBNull(indiceCommittentePartitaIva))
                            domanda.Committente.PartitaIva = reader.GetString(indiceCommittentePartitaIva);

                        if (!reader.IsDBNull(indiceCommittenteUtIdCommittente))
                        {
                            domanda.CommittenteUtente = new TBridge.Cemi.GestioneUtenti.Type.Entities.Committente();
                            domanda.CommittenteUtente.IdCommittente = reader.GetInt32(indiceCommittenteUtIdCommittente);

                            if (!reader.IsDBNull(indiceCommittenteUtIdUtente))
                            {
                                domanda.CommittenteUtente.IdUtente = reader.GetInt32(indiceCommittenteUtIdUtente);
                            }
                            if (!reader.IsDBNull(indiceCommittenteUtCognome))
                            {
                                domanda.CommittenteUtente.Cognome = reader.GetString(indiceCommittenteUtCognome);
                            }
                            if (!reader.IsDBNull(indiceCommittenteUtNome))
                            {
                                domanda.CommittenteUtente.Nome = reader.GetString(indiceCommittenteUtNome);
                            }
                            if (!reader.IsDBNull(indiceCommittenteUtRagioneSociale))
                            {
                                domanda.CommittenteUtente.RagioneSociale = reader.GetString(indiceCommittenteUtRagioneSociale);
                            }
                            if (!reader.IsDBNull(indiceCommittenteUtPartitaIva))
                            {
                                domanda.CommittenteUtente.PartitaIva = reader.GetString(indiceCommittenteUtPartitaIva);
                            }
                            if (!reader.IsDBNull(indiceCommittenteUtCodiceFiscale))
                            {
                                domanda.CommittenteUtente.CodiceFiscale = reader.GetString(indiceCommittenteUtCodiceFiscale);
                            }
                            if (!reader.IsDBNull(indiceCommittenteUtTipologia))
                            {
                                domanda.CommittenteUtente.Tipologia = (TBridge.Cemi.GestioneUtenti.Type.Enums.TipologiaCommittente) (Int32) reader.GetInt16(indiceCommittenteUtTipologia);
                            }
                        }
                    }

                    #endregion

                    reader.NextResult();

                    #region Subappalti

                    #region Indici per Reader

                    Int32 indiceIdSubappalto = reader.GetOrdinal("idAccessoCantieriWhiteListSubappalto");
                    Int32 indiceIdImpresaAppaltante = reader.GetOrdinal("idImpresaAppaltante");
                    Int32 indiceIdAttestatoImpresaAppaltante =
                        reader.GetOrdinal("idAccessoCantieriImpresaAppaltante");
                    Int32 indiceIdImpresaAppaltata = reader.GetOrdinal("idImpresaAppaltata");
                    Int32 indiceIdAttestatoImpresaAppaltata = reader.GetOrdinal("idAccessoCantieriImpresaAppaltata");

                    // Appaltata
                    Int32 indiceImpAtaRagioneSociale = reader.GetOrdinal("impAtaRagioneSociale");
                    Int32 indiceImpAtaPartitaIva = reader.GetOrdinal("impAtaPartitaIva");
                    Int32 indiceImpAtaCodiceFiscale = reader.GetOrdinal("impAtaCodiceFiscale");
                    Int32 indiceImpAtaIndirizzo = reader.GetOrdinal("impAtaIndirizzo");
                    Int32 indiceImpAtaComune = reader.GetOrdinal("impAtaComune");
                    Int32 indiceImpAtaProvincia = reader.GetOrdinal("impAtaProvincia");
                    Int32 indiceImpAtaCap = reader.GetOrdinal("impAtaCap");

                    Int32 indiceImpAtaTipologiaContratto = reader.GetOrdinal("impAtaTipologiaContratto");
                    Int32 indiceImpAtaLavoratoreAutonomo = reader.GetOrdinal("impAtaLavoratoreAutonomo");

                    Int32 indiceImpAtaTelefonoSedeAmministrativa = reader.GetOrdinal("impAtaAmmiTelefono");
                    Int32 indiceImpAtaTelefonoSedeLegale = reader.GetOrdinal("impAtaLegaleTelefono");
                    Int32 indiceImpAtaEmailSedeAmministrativa = reader.GetOrdinal("impAtaAmmiEmail");
                    Int32 indiceImpAtaEmailSedeLegale = reader.GetOrdinal("impAtaLegaleEmail");

                    Int32 indiceAtImpAtaRagioneSociale = reader.GetOrdinal("atImpAtaRagioneSociale");
                    Int32 indiceAtImpAtaPartitaIva = reader.GetOrdinal("atImpAtaPartitaIva");
                    Int32 indiceAtImpAtaCodiceFiscale = reader.GetOrdinal("atImpAtaCodiceFiscale");
                    Int32 indiceAtImpAtaIndirizzo = reader.GetOrdinal("atImpAtaIndirizzo");
                    Int32 indiceAtImpAtaComune = reader.GetOrdinal("atImpAtaComune");
                    Int32 indiceAtImpAtaProvincia = reader.GetOrdinal("atImpAtaProvincia");
                    Int32 indiceAtImpAtaCap = reader.GetOrdinal("atImpAtaCap");

                    Int32 indiceAtImpAtaTipologiaContratto = reader.GetOrdinal("atImpAtaTipologiaContratto");
                    Int32 indiceAtImpAtaLavoratoreAutonomo = reader.GetOrdinal("atImpAtaLavoratoreAutonomo");

                    Int32 indiceAtImpAtaTelefonoSedeAmministrativa = reader.GetOrdinal("atImpAtaAmmiTelefono");
                    Int32 indiceAtImpAtaTelefonoSedeLegale = reader.GetOrdinal("atImpAtaLegaleTelefono");
                    Int32 indiceAtImpAtaEmailSedeAmministrativa = reader.GetOrdinal("atImpAtaAmmiEmail");
                    Int32 indiceAtImpAtaEmailSedeLegale = reader.GetOrdinal("atImpAtaLegaleEmail");

                    Int32 indiceAtImpAtaNome = reader.GetOrdinal("atImpAtaNome");
                    Int32 indiceAtImpAtaCognome = reader.GetOrdinal("atImpAtaCognome");
                    Int32 indiceAtImpAtaDataNascita = reader.GetOrdinal("atImpAtaDataNascita");
                    Int32 indiceAtImpAtaLuogoNascita = reader.GetOrdinal("atImpAtaLuogoNascita");
                    Int32 indiceAtImpAtaPaeseNascita = reader.GetOrdinal("atImpAtaPaeseNascita");
                    Int32 indiceAtImpAtaProvinciaNascita = reader.GetOrdinal("atImpAtaPaeseNascita");
                    Int32 indiceAtImpAtaCommittente = reader.GetOrdinal("atImpAtaCommittente");
                    //Int32 indiceAtImpAtaDataAssunzione = reader.GetOrdinal("atImpAtaDataAssunzione");
                    Int32 indiceAtImpAtaDataStampaBadge = reader.GetOrdinal("AtImpAtaDataStampaBadge");


                    // Appaltante
                    Int32 indiceImpAnteRagioneSociale = reader.GetOrdinal("impAnteRagioneSociale");
                    Int32 indiceImpAntePartitaIva = reader.GetOrdinal("impAntePartitaIva");
                    Int32 indiceImpAnteCodiceFiscale = reader.GetOrdinal("impAnteCodiceFiscale");
                    Int32 indiceImpAnteIndirizzo = reader.GetOrdinal("impAnteIndirizzo");
                    Int32 indiceImpAnteComune = reader.GetOrdinal("impAnteComune");
                    Int32 indiceImpAnteProvincia = reader.GetOrdinal("impAnteProvincia");
                    Int32 indiceImpAnteCap = reader.GetOrdinal("impAnteCap");

                    Int32 indiceImpAnteTipologiaContratto = reader.GetOrdinal("impAnteTipologiaContratto");
                    Int32 indiceImpAnteLavoratoreAutonomo = reader.GetOrdinal("impAnteLavoratoreAutonomo");

                    Int32 indiceImpAnteTelefonoSedeAmministrativa = reader.GetOrdinal("impAnteAmmiTelefono");
                    Int32 indiceImpAnteTelefonoSedeLegale = reader.GetOrdinal("impAnteLegaleTelefono");
                    Int32 indiceImpAnteEmailSedeAmministrativa = reader.GetOrdinal("impAnteAmmiEmail");
                    Int32 indiceImpAnteEmailSedeLegale = reader.GetOrdinal("impAnteLegaleEmail");

                    Int32 indiceAtImpAnteRagioneSociale = reader.GetOrdinal("atImpAnteRagioneSociale");
                    Int32 indiceAtImpAntePartitaIva = reader.GetOrdinal("atImpAntePartitaIva");
                    Int32 indiceAtImpAnteCodiceFiscale = reader.GetOrdinal("atImpAnteCodiceFiscale");
                    Int32 indiceAtImpAnteIndirizzo = reader.GetOrdinal("atImpAnteIndirizzo");
                    Int32 indiceAtImpAnteComune = reader.GetOrdinal("atImpAnteComune");
                    Int32 indiceAtImpAnteProvincia = reader.GetOrdinal("atImpAnteProvincia");
                    Int32 indiceAtImpAnteCap = reader.GetOrdinal("atImpAnteCap");

                    Int32 indiceAtImpAnteTipologiaContratto = reader.GetOrdinal("atImpAnteTipologiaContratto");
                    Int32 indiceAtImpAnteLavoratoreAutonomo = reader.GetOrdinal("atImpAnteLavoratoreAutonomo");

                    Int32 indiceAtImpAnteTelefonoSedeAmministrativa = reader.GetOrdinal("atImpAnteAmmiTelefono");
                    Int32 indiceAtImpAnteTelefonoSedeLegale = reader.GetOrdinal("atImpAnteLegaleTelefono");
                    Int32 indiceAtImpAnteEmailSedeAmministrativa = reader.GetOrdinal("atImpAnteAmmiEmail");
                    Int32 indiceAtImpAnteEmailSedeLegale = reader.GetOrdinal("atImpAnteLegaleEmail");

                    Int32 indiceAtImpAnteNome = reader.GetOrdinal("atImpAnteNome");
                    Int32 indiceAtImpAnteCognome = reader.GetOrdinal("atImpAnteCognome");
                    Int32 indiceAtImpAnteDataNascita = reader.GetOrdinal("atImpAnteDataNascita");
                    Int32 indiceAtImpAnteLuogoNascita = reader.GetOrdinal("atImpAnteLuogoNascita");
                    Int32 indiceAtImpAntePaeseNascita = reader.GetOrdinal("atImpAntePaeseNascita");
                    Int32 indiceAtImpAnteProvinciaNascita = reader.GetOrdinal("atImpAnteProvinciaNascita");
                    Int32 indiceAtImpAnteCommittente = reader.GetOrdinal("atImpAnteCommittente");
                    //Int32 indiceAtImpAnteDataAssunzione = reader.GetOrdinal("atImpAnteDataAssunzione");
                    Int32 indiceAtImpAnteDataStampaBadge = reader.GetOrdinal("AtImpAnteDataStampaBadge");

                    Int32 indiceDataInizioAttivitaAppaltata = reader.GetOrdinal("dataInizioAttivitaAppaltata");
                    Int32 indiceDataFineAttivitaAppaltata = reader.GetOrdinal("dataFineAttivitaAppaltata");
                    Int32 indiceDataInizioAttivitaAppaltante = reader.GetOrdinal("dataInizioAttivitaAppaltante");
                    Int32 indiceDataFineAttivitaAppaltante = reader.GetOrdinal("dataFineAttivitaAppaltante");

                    //Int32 indiceAutorizzazioneAlSubappaltoAppaltata =
                    //    reader.GetOrdinal("autorizzazioneAlSubappaltoAppaltata");
                    //Int32 indiceAutorizzazioneAlSubappaltoAppaltatrice =
                    //    reader.GetOrdinal("autorizzazioneAlSubappaltoAppaltatrice");

                    #endregion

                    while (reader.Read())
                    {
                        Subappalto subappalto = new Subappalto();
                        domanda.Subappalti.Add(subappalto);

                        subappalto.IdSubappalto = reader.GetInt32(indiceIdSubappalto);

                        if (!reader.IsDBNull(indiceDataFineAttivitaAppaltante))
                            subappalto.DataFineAttivitaAppaltatrice =
                                reader.GetDateTime(indiceDataFineAttivitaAppaltante);
                        if (!reader.IsDBNull(indiceDataInizioAttivitaAppaltante))
                            subappalto.DataInizioAttivitaAppaltatrice =
                                reader.GetDateTime(indiceDataInizioAttivitaAppaltante);

                        //if (!reader.IsDBNull(indiceAutorizzazioneAlSubappaltoAppaltata))
                        //    subappalto.AutorizzazioneAlSubappaltoAppaltata =
                        //        reader.GetString(indiceAutorizzazioneAlSubappaltoAppaltata);
                        //if (!reader.IsDBNull(indiceAutorizzazioneAlSubappaltoAppaltatrice))
                        //    subappalto.AutorizzazioneAlSubappaltoAppaltatrice =
                        //        reader.GetString(indiceAutorizzazioneAlSubappaltoAppaltatrice);

                        if (!reader.IsDBNull(indiceDataFineAttivitaAppaltata))
                            subappalto.DataFineAttivitaAppaltata = reader.GetDateTime(indiceDataFineAttivitaAppaltata);
                        if (!reader.IsDBNull(indiceDataInizioAttivitaAppaltata))
                            subappalto.DataInizioAttivitaAppaltata =
                                reader.GetDateTime(indiceDataInizioAttivitaAppaltata);

                        // Impresa appaltata, c'è sicuramente
                        Impresa impresaAppaltata = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresaAppaltata))
                        {
                            // Impresa SiceNew
                            impresaAppaltata.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresaAppaltata.IdImpresa = reader.GetInt32(indiceIdImpresaAppaltata);

                            impresaAppaltata.RagioneSociale = reader.GetString(indiceImpAtaRagioneSociale);
                            if (!reader.IsDBNull(indiceImpAtaPartitaIva))
                                impresaAppaltata.PartitaIva = reader.GetString(indiceImpAtaPartitaIva);
                            if (!reader.IsDBNull(indiceImpAtaCodiceFiscale))
                                impresaAppaltata.CodiceFiscale = reader.GetString(indiceImpAtaCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpAtaIndirizzo))
                                impresaAppaltata.Indirizzo = reader.GetString(indiceImpAtaIndirizzo);
                            if (!reader.IsDBNull(indiceImpAtaComune))
                                impresaAppaltata.Comune = reader.GetString(indiceImpAtaComune);
                            if (!reader.IsDBNull(indiceImpAtaProvincia))
                                impresaAppaltata.Provincia = reader.GetString(indiceImpAtaProvincia);
                            if (!reader.IsDBNull(indiceImpAtaCap))
                                impresaAppaltata.Cap = reader.GetString(indiceImpAtaCap);

                            if (!reader.IsDBNull(indiceImpAtaTipologiaContratto))
                                impresaAppaltata.TipologiaContratto =
                                    (TipologiaContratto) reader.GetInt32(indiceImpAtaTipologiaContratto);
                            impresaAppaltata.LavoratoreAutonomo = reader.GetBoolean(indiceImpAtaLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceImpAtaTelefonoSedeAmministrativa))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceImpAtaTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAtaTelefonoSedeLegale))
                                impresaAppaltata.LegaleTelefono = reader.GetString(indiceImpAtaTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceImpAtaEmailSedeAmministrativa))
                                impresaAppaltata.AmmiEmail = reader.GetString(indiceImpAtaEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAtaEmailSedeLegale))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceImpAtaEmailSedeLegale);

                            if (reader["atImpAtaFoto"] != null && reader["atImpAtaFoto"] != DBNull.Value)
                                impresaAppaltata.Foto = ((byte[]) reader["atImpAtaFoto"]);
                        }
                        else
                        {
                            // Impresa Nuova
                            impresaAppaltata.TipoImpresa = TipologiaImpresa.Nuova;
                            impresaAppaltata.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresaAppaltata);

                            impresaAppaltata.RagioneSociale = reader.GetString(indiceAtImpAtaRagioneSociale);
                            if (!reader.IsDBNull(indiceAtImpAtaPartitaIva))
                                impresaAppaltata.PartitaIva = reader.GetString(indiceAtImpAtaPartitaIva);
                            impresaAppaltata.CodiceFiscale = reader.GetString(indiceAtImpAtaCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtImpAtaIndirizzo))
                                impresaAppaltata.Indirizzo = reader.GetString(indiceAtImpAtaIndirizzo);
                            if (!reader.IsDBNull(indiceAtImpAtaComune))
                                impresaAppaltata.Comune = reader.GetString(indiceAtImpAtaComune);
                            if (!reader.IsDBNull(indiceAtImpAtaProvincia))
                                impresaAppaltata.Provincia = reader.GetString(indiceAtImpAtaProvincia);
                            if (!reader.IsDBNull(indiceAtImpAtaCap))
                                impresaAppaltata.Cap = reader.GetString(indiceAtImpAtaCap);

                            impresaAppaltata.TipologiaContratto =
                                (TipologiaContratto) reader.GetInt16(indiceAtImpAtaTipologiaContratto);
                            impresaAppaltata.LavoratoreAutonomo = reader.GetBoolean(indiceAtImpAtaLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceAtImpAtaTelefonoSedeAmministrativa))
                                impresaAppaltata.AmmiTelefono =
                                    reader.GetString(indiceAtImpAtaTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAtaTelefonoSedeLegale))
                                impresaAppaltata.LegaleTelefono = reader.GetString(indiceAtImpAtaTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceAtImpAtaEmailSedeAmministrativa))
                                impresaAppaltata.AmmiEmail = reader.GetString(indiceAtImpAtaEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAtaEmailSedeLegale))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceAtImpAtaEmailSedeLegale);


                            if (!reader.IsDBNull(indiceAtImpAtaNome))
                                impresaAppaltata.Nome = reader.GetString(indiceAtImpAtaNome);
                            if (!reader.IsDBNull(indiceAtImpAtaCognome))
                                impresaAppaltata.Cognome = reader.GetString(indiceAtImpAtaCognome);
                            if (!reader.IsDBNull(indiceAtImpAtaDataNascita))
                                impresaAppaltata.DataNascita = reader.GetDateTime(indiceAtImpAtaDataNascita);
                            if (!reader.IsDBNull(indiceAtImpAtaLuogoNascita))
                                impresaAppaltata.LuogoNascita = reader.GetString(indiceAtImpAtaLuogoNascita);
                            if (!reader.IsDBNull(indiceAtImpAtaPaeseNascita))
                                impresaAppaltata.PaeseNascita = reader.GetString(indiceAtImpAtaPaeseNascita);
                            if (!reader.IsDBNull(indiceAtImpAtaProvinciaNascita))
                                impresaAppaltata.ProvinciaNascita = reader.GetString(indiceAtImpAtaProvinciaNascita);
                            if (!reader.IsDBNull(indiceAtImpAtaCommittente))
                                impresaAppaltata.Committente = reader.GetString(indiceAtImpAtaCommittente);
                            //if (!reader.IsDBNull(indiceAutorizzazioneAlSubappaltoAppaltata))
                            //    impresaAppaltata.AutorizzazioneAlSubappalto =
                            //        reader.GetString(indiceAutorizzazioneAlSubappaltoAppaltata);
                            //if (!reader.IsDBNull(indiceAtImpAtaDataAssunzione))
                            //    impresaAppaltata.DataAssunzione = reader.GetDateTime(indiceAtImpAtaDataAssunzione);
                            if (!reader.IsDBNull(indiceAtImpAtaDataStampaBadge))
                                impresaAppaltata.DataStampaBadge = reader.GetDateTime(indiceAtImpAtaDataStampaBadge);

                            if (reader["atImpAtaFoto"] != null && reader["atImpAtaFoto"] != DBNull.Value)
                                impresaAppaltata.Foto = ((byte[]) reader["atImpAtaFoto"]);
                        }

                        subappalto.Appaltata = impresaAppaltata;

                        // Impresa appaltante
                        Impresa impresaAppaltante = null;

                        if (!reader.IsDBNull(indiceIdImpresaAppaltante))
                        {
                            // Impresa SiceNew
                            impresaAppaltante = new Impresa
                                                    {
                                                        TipoImpresa = TipologiaImpresa.SiceNew,
                                                        IdImpresa = reader.GetInt32(indiceIdImpresaAppaltante),
                                                        RagioneSociale = reader.GetString(indiceImpAnteRagioneSociale)
                                                    };

                            if (!reader.IsDBNull(indiceImpAntePartitaIva))
                                impresaAppaltante.PartitaIva = reader.GetString(indiceImpAntePartitaIva);
                            if (!reader.IsDBNull(indiceImpAnteCodiceFiscale))
                                impresaAppaltante.CodiceFiscale = reader.GetString(indiceImpAnteCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpAnteIndirizzo))
                                impresaAppaltante.Indirizzo = reader.GetString(indiceImpAnteIndirizzo);
                            if (!reader.IsDBNull(indiceImpAnteComune))
                                impresaAppaltante.Comune = reader.GetString(indiceImpAnteComune);
                            if (!reader.IsDBNull(indiceImpAnteProvincia))
                                impresaAppaltante.Provincia = reader.GetString(indiceImpAnteProvincia);
                            if (!reader.IsDBNull(indiceImpAnteCap))
                                impresaAppaltante.Cap = reader.GetString(indiceImpAnteCap);

                            if (!reader.IsDBNull(indiceImpAnteTipologiaContratto))
                                impresaAppaltante.TipologiaContratto =
                                    (TipologiaContratto) reader.GetInt32(indiceImpAnteTipologiaContratto);
                            impresaAppaltante.LavoratoreAutonomo = reader.GetBoolean(indiceImpAnteLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceImpAnteTelefonoSedeAmministrativa))
                                impresaAppaltante.AmmiTelefono =
                                    reader.GetString(indiceImpAnteTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAnteTelefonoSedeLegale))
                                impresaAppaltante.LegaleTelefono = reader.GetString(indiceImpAnteTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceImpAnteEmailSedeAmministrativa))
                                impresaAppaltante.AmmiEmail = reader.GetString(indiceImpAnteEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAnteEmailSedeLegale))
                                impresaAppaltante.AmmiTelefono = reader.GetString(indiceImpAnteEmailSedeLegale);

                            if (reader["atImpAnteFoto"] != null && reader["atImpAnteFoto"] != DBNull.Value)
                                impresaAppaltante.Foto = ((byte[]) reader["atImpAnteFoto"]);
                        }

                        if (!reader.IsDBNull(indiceIdAttestatoImpresaAppaltante))
                        {
                            // Impresa Nuova
                            impresaAppaltante = new Impresa
                                                    {
                                                        TipoImpresa = TipologiaImpresa.Nuova,
                                                        IdImpresa = reader.GetInt32(indiceIdAttestatoImpresaAppaltante),
                                                        RagioneSociale = reader.GetString(indiceAtImpAnteRagioneSociale)
                                                    };

                            if (!reader.IsDBNull(indiceAtImpAntePartitaIva))
                                impresaAppaltante.PartitaIva = reader.GetString(indiceAtImpAntePartitaIva);
                            impresaAppaltante.CodiceFiscale = reader.GetString(indiceAtImpAnteCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtImpAnteIndirizzo))
                                impresaAppaltante.Indirizzo = reader.GetString(indiceAtImpAnteIndirizzo);
                            if (!reader.IsDBNull(indiceAtImpAnteComune))
                                impresaAppaltante.Comune = reader.GetString(indiceAtImpAnteComune);
                            if (!reader.IsDBNull(indiceAtImpAnteProvincia))
                                impresaAppaltante.Provincia = reader.GetString(indiceAtImpAnteProvincia);
                            if (!reader.IsDBNull(indiceAtImpAnteCap))
                                impresaAppaltante.Cap = reader.GetString(indiceAtImpAnteCap);

                            impresaAppaltante.TipologiaContratto =
                                (TipologiaContratto) reader.GetInt16(indiceAtImpAnteTipologiaContratto);
                            impresaAppaltante.LavoratoreAutonomo = reader.GetBoolean(indiceAtImpAnteLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceAtImpAnteTelefonoSedeAmministrativa))
                                impresaAppaltante.AmmiTelefono =
                                    reader.GetString(indiceAtImpAnteTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAnteTelefonoSedeLegale))
                                impresaAppaltante.LegaleTelefono = reader.GetString(indiceAtImpAnteTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceAtImpAnteEmailSedeAmministrativa))
                                impresaAppaltante.AmmiEmail = reader.GetString(indiceAtImpAnteEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAnteEmailSedeLegale))
                                impresaAppaltante.AmmiTelefono = reader.GetString(indiceAtImpAnteEmailSedeLegale);

                            if (!reader.IsDBNull(indiceAtImpAnteNome))
                                impresaAppaltante.Nome = reader.GetString(indiceAtImpAnteNome);
                            if (!reader.IsDBNull(indiceAtImpAnteCognome))
                                impresaAppaltante.Cognome = reader.GetString(indiceAtImpAnteCognome);
                            if (!reader.IsDBNull(indiceAtImpAnteDataNascita))
                                impresaAppaltante.DataNascita = reader.GetDateTime(indiceAtImpAnteDataNascita);
                            if (!reader.IsDBNull(indiceAtImpAnteLuogoNascita))
                                impresaAppaltante.LuogoNascita = reader.GetString(indiceAtImpAnteLuogoNascita);
                            if (!reader.IsDBNull(indiceAtImpAntePaeseNascita))
                                impresaAppaltante.PaeseNascita = reader.GetString(indiceAtImpAntePaeseNascita);
                            if (!reader.IsDBNull(indiceAtImpAnteProvinciaNascita))
                                impresaAppaltante.ProvinciaNascita = reader.GetString(indiceAtImpAnteProvinciaNascita);
                            if (!reader.IsDBNull(indiceAtImpAnteCommittente))
                                impresaAppaltante.Committente = reader.GetString(indiceAtImpAnteCommittente);
                            //if (!reader.IsDBNull(indiceAutorizzazioneAlSubappaltoAppaltatrice))
                            //    impresaAppaltante.AutorizzazioneAlSubappalto =
                            //        reader.GetString(indiceAutorizzazioneAlSubappaltoAppaltatrice);
                            //if (!reader.IsDBNull(indiceAtImpAnteDataAssunzione))
                            //    impresaAppaltante.DataAssunzione = reader.GetDateTime(indiceAtImpAnteDataAssunzione);
                            if (!reader.IsDBNull(indiceAtImpAnteDataStampaBadge))
                                impresaAppaltante.DataStampaBadge = reader.GetDateTime(indiceAtImpAnteDataStampaBadge);

                            if (reader["atImpAnteFoto"] != null && reader["atImpAnteFoto"] != DBNull.Value)
                                impresaAppaltante.Foto = ((byte[]) reader["atImpAnteFoto"]);
                        }

                        subappalto.Appaltante = impresaAppaltante;
                    }

                    #endregion
                }
            }

            return domanda;
        }


        public WhiteList GetDomandaByRilevatoreData(string codiceRilevatore, DateTime dataOra)
        {
            WhiteList domanda = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSelectByRilevatoreData"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@dataOra", DbType.DateTime, dataOra);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Domanda

                    #region Indici per reader

                    Int32 indiceIdDomanda = reader.GetOrdinal("idAccessoCantieriWhiteList");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceInfoAggiuntiva = reader.GetOrdinal("infoAggiuntiva");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");

                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    Int32 indiceAutorizzazioneAlSubappalto = reader.GetOrdinal("autorizzazioneAlSubappalto");

                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizio");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFine");

                    #endregion

                    if (reader.Read())
                    {
                        domanda = new WhiteList
                                      {
                                          IdWhiteList = reader.GetInt32(indiceIdDomanda),
                                          Indirizzo = reader.GetString(indiceIndirizzo)
                                      };

                        if (!reader.IsDBNull(indiceCivico))
                            domanda.Civico = reader.GetString(indiceCivico);
                        if (!reader.IsDBNull(indiceComune))
                            domanda.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            domanda.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            domanda.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceInfoAggiuntiva))
                            domanda.InfoAggiuntiva = reader.GetString(indiceInfoAggiuntiva);
                        if (!reader.IsDBNull(indiceLatitudine))
                            domanda.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            domanda.Longitudine = reader.GetDecimal(indiceLongitudine);

                        if (!reader.IsDBNull(indiceDescrizione))
                            domanda.Descrizione = reader.GetString(indiceDescrizione);

                        if (!reader.IsDBNull(indiceAutorizzazioneAlSubappalto))
                            domanda.AutorizzazioneAlSubappalto = reader.GetString(indiceAutorizzazioneAlSubappalto);

                        if (!reader.IsDBNull(indiceDataInizio))
                            domanda.DataInizio = reader.GetDateTime(indiceDataInizio);
                        if (!reader.IsDBNull(indiceDataFine))
                            domanda.DataFine = reader.GetDateTime(indiceDataFine);
                    }

                    #endregion

                    reader.NextResult();

                    #region Subappalti

                    #region Indici per Reader

                    Int32 indiceIdSubappalto = reader.GetOrdinal("idAccessoCantieriWhiteListSubappalto");
                    Int32 indiceIdImpresaAppaltante = reader.GetOrdinal("idImpresaAppaltante");
                    Int32 indiceIdAttestatoImpresaAppaltante =
                        reader.GetOrdinal("idAccessoCantieriImpresaAppaltante");
                    Int32 indiceIdImpresaAppaltata = reader.GetOrdinal("idImpresaAppaltata");
                    Int32 indiceIdAttestatoImpresaAppaltata = reader.GetOrdinal("idAccessoCantieriImpresaAppaltata");

                    // Appaltata
                    Int32 indiceImpAtaRagioneSociale = reader.GetOrdinal("impAtaRagioneSociale");
                    Int32 indiceImpAtaPartitaIva = reader.GetOrdinal("impAtaPartitaIva");
                    Int32 indiceImpAtaCodiceFiscale = reader.GetOrdinal("impAtaCodiceFiscale");
                    Int32 indiceImpAtaIndirizzo = reader.GetOrdinal("impAtaIndirizzo");
                    Int32 indiceImpAtaComune = reader.GetOrdinal("impAtaComune");
                    Int32 indiceImpAtaProvincia = reader.GetOrdinal("impAtaProvincia");
                    Int32 indiceImpAtaCap = reader.GetOrdinal("impAtaCap");

                    Int32 indiceImpAtaTipologiaContratto = reader.GetOrdinal("impAtaTipologiaContratto");
                    Int32 indiceImpAtaLavoratoreAutonomo = reader.GetOrdinal("impAtaLavoratoreAutonomo");

                    Int32 indiceImpAtaTelefonoSedeAmministrativa = reader.GetOrdinal("impAtaAmmiTelefono");
                    Int32 indiceImpAtaTelefonoSedeLegale = reader.GetOrdinal("impAtaLegaleTelefono");
                    Int32 indiceImpAtaEmailSedeAmministrativa = reader.GetOrdinal("impAtaAmmiEmail");
                    Int32 indiceImpAtaEmailSedeLegale = reader.GetOrdinal("impAtaLegaleEmail");

                    Int32 indiceAtImpAtaRagioneSociale = reader.GetOrdinal("atImpAtaRagioneSociale");
                    Int32 indiceAtImpAtaPartitaIva = reader.GetOrdinal("atImpAtaPartitaIva");
                    Int32 indiceAtImpAtaCodiceFiscale = reader.GetOrdinal("atImpAtaCodiceFiscale");
                    Int32 indiceAtImpAtaIndirizzo = reader.GetOrdinal("atImpAtaIndirizzo");
                    Int32 indiceAtImpAtaComune = reader.GetOrdinal("atImpAtaComune");
                    Int32 indiceAtImpAtaProvincia = reader.GetOrdinal("atImpAtaProvincia");
                    Int32 indiceAtImpAtaCap = reader.GetOrdinal("atImpAtaCap");

                    Int32 indiceAtImpAtaTipologiaContratto = reader.GetOrdinal("atImpAtaTipologiaContratto");
                    Int32 indiceAtImpAtaLavoratoreAutonomo = reader.GetOrdinal("atImpAtaLavoratoreAutonomo");

                    Int32 indiceAtImpAtaTelefonoSedeAmministrativa = reader.GetOrdinal("atImpAtaAmmiTelefono");
                    Int32 indiceAtImpAtaTelefonoSedeLegale = reader.GetOrdinal("atImpAtaLegaleTelefono");
                    Int32 indiceAtImpAtaEmailSedeAmministrativa = reader.GetOrdinal("atImpAtaAmmiEmail");
                    Int32 indiceAtImpAtaEmailSedeLegale = reader.GetOrdinal("atImpAtaLegaleEmail");

                    // Appaltante
                    Int32 indiceImpAnteRagioneSociale = reader.GetOrdinal("impAnteRagioneSociale");
                    Int32 indiceImpAntePartitaIva = reader.GetOrdinal("impAntePartitaIva");
                    Int32 indiceImpAnteCodiceFiscale = reader.GetOrdinal("impAnteCodiceFiscale");
                    Int32 indiceImpAnteIndirizzo = reader.GetOrdinal("impAnteIndirizzo");
                    Int32 indiceImpAnteComune = reader.GetOrdinal("impAnteComune");
                    Int32 indiceImpAnteProvincia = reader.GetOrdinal("impAnteProvincia");
                    Int32 indiceImpAnteCap = reader.GetOrdinal("impAnteCap");

                    Int32 indiceImpAnteTipologiaContratto = reader.GetOrdinal("impAnteTipologiaContratto");
                    Int32 indiceImpAnteLavoratoreAutonomo = reader.GetOrdinal("impAnteLavoratoreAutonomo");

                    Int32 indiceImpAnteTelefonoSedeAmministrativa = reader.GetOrdinal("impAnteAmmiTelefono");
                    Int32 indiceImpAnteTelefonoSedeLegale = reader.GetOrdinal("impAnteLegaleTelefono");
                    Int32 indiceImpAnteEmailSedeAmministrativa = reader.GetOrdinal("impAnteAmmiEmail");
                    Int32 indiceImpAnteEmailSedeLegale = reader.GetOrdinal("impAnteLegaleEmail");

                    Int32 indiceAtImpAnteRagioneSociale = reader.GetOrdinal("atImpAnteRagioneSociale");
                    Int32 indiceAtImpAntePartitaIva = reader.GetOrdinal("atImpAntePartitaIva");
                    Int32 indiceAtImpAnteCodiceFiscale = reader.GetOrdinal("atImpAnteCodiceFiscale");
                    Int32 indiceAtImpAnteIndirizzo = reader.GetOrdinal("atImpAnteIndirizzo");
                    Int32 indiceAtImpAnteComune = reader.GetOrdinal("atImpAnteComune");
                    Int32 indiceAtImpAnteProvincia = reader.GetOrdinal("atImpAnteProvincia");
                    Int32 indiceAtImpAnteCap = reader.GetOrdinal("atImpAnteCap");

                    Int32 indiceAtImpAnteTipologiaContratto = reader.GetOrdinal("atImpAnteTipologiaContratto");
                    Int32 indiceAtImpAnteLavoratoreAutonomo = reader.GetOrdinal("atImpAnteLavoratoreAutonomo");

                    Int32 indiceAtImpAnteTelefonoSedeAmministrativa = reader.GetOrdinal("atImpAnteAmmiTelefono");
                    Int32 indiceAtImpAnteTelefonoSedeLegale = reader.GetOrdinal("atImpAnteLegaleTelefono");
                    Int32 indiceAtImpAnteEmailSedeAmministrativa = reader.GetOrdinal("atImpAnteAmmiEmail");
                    Int32 indiceAtImpAnteEmailSedeLegale = reader.GetOrdinal("atImpAnteLegaleEmail");

                    #endregion

                    while (reader.Read())
                    {
                        Subappalto subappalto = new Subappalto();
                        if (domanda != null)
                            domanda.Subappalti.Add(subappalto);

                        subappalto.IdSubappalto = reader.GetInt32(indiceIdSubappalto);

                        // Impresa appaltata, c'è sicuramente
                        Impresa impresaAppaltata = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresaAppaltata))
                        {
                            // Impresa SiceNew
                            impresaAppaltata.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresaAppaltata.IdImpresa = reader.GetInt32(indiceIdImpresaAppaltata);

                            impresaAppaltata.RagioneSociale = reader.GetString(indiceImpAtaRagioneSociale);
                            if (!reader.IsDBNull(indiceImpAtaPartitaIva))
                                impresaAppaltata.PartitaIva = reader.GetString(indiceImpAtaPartitaIva);
                            if (!reader.IsDBNull(indiceImpAtaCodiceFiscale))
                                impresaAppaltata.CodiceFiscale = reader.GetString(indiceImpAtaCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpAtaIndirizzo))
                                impresaAppaltata.Indirizzo = reader.GetString(indiceImpAtaIndirizzo);
                            if (!reader.IsDBNull(indiceImpAtaComune))
                                impresaAppaltata.Comune = reader.GetString(indiceImpAtaComune);
                            if (!reader.IsDBNull(indiceImpAtaProvincia))
                                impresaAppaltata.Provincia = reader.GetString(indiceImpAtaProvincia);
                            if (!reader.IsDBNull(indiceImpAtaCap))
                                impresaAppaltata.Cap = reader.GetString(indiceImpAtaCap);

                            if (!reader.IsDBNull(indiceImpAtaTipologiaContratto))
                                impresaAppaltata.TipologiaContratto =
                                    (TipologiaContratto) reader.GetInt32(indiceImpAtaTipologiaContratto);
                            impresaAppaltata.LavoratoreAutonomo = reader.GetBoolean(indiceImpAtaLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceImpAtaTelefonoSedeAmministrativa))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceImpAtaTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAtaTelefonoSedeLegale))
                                impresaAppaltata.LegaleTelefono = reader.GetString(indiceImpAtaTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceImpAtaEmailSedeAmministrativa))
                                impresaAppaltata.AmmiEmail = reader.GetString(indiceImpAtaEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAtaEmailSedeLegale))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceImpAtaEmailSedeLegale);
                        }
                        else
                        {
                            // Impresa Nuova
                            impresaAppaltata.TipoImpresa = TipologiaImpresa.Nuova;
                            impresaAppaltata.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresaAppaltata);

                            impresaAppaltata.RagioneSociale = reader.GetString(indiceAtImpAtaRagioneSociale);
                            if (!reader.IsDBNull(indiceAtImpAtaPartitaIva))
                                impresaAppaltata.PartitaIva = reader.GetString(indiceAtImpAtaPartitaIva);
                            impresaAppaltata.CodiceFiscale = reader.GetString(indiceAtImpAtaCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtImpAtaIndirizzo))
                                impresaAppaltata.Indirizzo = reader.GetString(indiceAtImpAtaIndirizzo);
                            if (!reader.IsDBNull(indiceAtImpAtaComune))
                                impresaAppaltata.Comune = reader.GetString(indiceAtImpAtaComune);
                            if (!reader.IsDBNull(indiceAtImpAtaProvincia))
                                impresaAppaltata.Provincia = reader.GetString(indiceAtImpAtaProvincia);
                            if (!reader.IsDBNull(indiceAtImpAtaCap))
                                impresaAppaltata.Cap = reader.GetString(indiceAtImpAtaCap);

                            impresaAppaltata.TipologiaContratto =
                                (TipologiaContratto) reader.GetInt16(indiceAtImpAtaTipologiaContratto);
                            impresaAppaltata.LavoratoreAutonomo = reader.GetBoolean(indiceAtImpAtaLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceAtImpAtaTelefonoSedeAmministrativa))
                                impresaAppaltata.AmmiTelefono =
                                    reader.GetString(indiceAtImpAtaTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAtaTelefonoSedeLegale))
                                impresaAppaltata.LegaleTelefono = reader.GetString(indiceAtImpAtaTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceAtImpAtaEmailSedeAmministrativa))
                                impresaAppaltata.AmmiEmail = reader.GetString(indiceAtImpAtaEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAtaEmailSedeLegale))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceAtImpAtaEmailSedeLegale);
                        }

                        subappalto.Appaltata = impresaAppaltata;

                        // Impresa appaltante
                        Impresa impresaAppaltante = null;

                        if (!reader.IsDBNull(indiceIdImpresaAppaltante))
                        {
                            // Impresa SiceNew
                            impresaAppaltante = new Impresa
                                                    {
                                                        TipoImpresa = TipologiaImpresa.SiceNew,
                                                        IdImpresa = reader.GetInt32(indiceIdImpresaAppaltante),
                                                        RagioneSociale = reader.GetString(indiceImpAnteRagioneSociale)
                                                    };

                            if (!reader.IsDBNull(indiceImpAntePartitaIva))
                                impresaAppaltante.PartitaIva = reader.GetString(indiceImpAntePartitaIva);
                            if (!reader.IsDBNull(indiceImpAnteCodiceFiscale))
                                impresaAppaltante.CodiceFiscale = reader.GetString(indiceImpAnteCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpAnteIndirizzo))
                                impresaAppaltante.Indirizzo = reader.GetString(indiceImpAnteIndirizzo);
                            if (!reader.IsDBNull(indiceImpAnteComune))
                                impresaAppaltante.Comune = reader.GetString(indiceImpAnteComune);
                            if (!reader.IsDBNull(indiceImpAnteProvincia))
                                impresaAppaltante.Provincia = reader.GetString(indiceImpAnteProvincia);
                            if (!reader.IsDBNull(indiceImpAnteCap))
                                impresaAppaltante.Cap = reader.GetString(indiceImpAnteCap);

                            if (!reader.IsDBNull(indiceImpAnteTipologiaContratto))
                                impresaAppaltante.TipologiaContratto =
                                    (TipologiaContratto) reader.GetInt32(indiceImpAnteTipologiaContratto);
                            impresaAppaltante.LavoratoreAutonomo = reader.GetBoolean(indiceImpAnteLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceImpAnteTelefonoSedeAmministrativa))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceImpAnteTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAnteTelefonoSedeLegale))
                                impresaAppaltata.LegaleTelefono = reader.GetString(indiceImpAnteTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceImpAnteEmailSedeAmministrativa))
                                impresaAppaltata.AmmiEmail = reader.GetString(indiceImpAnteEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceImpAnteEmailSedeLegale))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceImpAnteEmailSedeLegale);
                        }

                        if (!reader.IsDBNull(indiceIdAttestatoImpresaAppaltante))
                        {
                            // Impresa Nuova
                            impresaAppaltante = new Impresa
                                                    {
                                                        TipoImpresa = TipologiaImpresa.Nuova,
                                                        IdImpresa = reader.GetInt32(indiceIdAttestatoImpresaAppaltante),
                                                        RagioneSociale = reader.GetString(indiceAtImpAnteRagioneSociale)
                                                    };

                            if (!reader.IsDBNull(indiceAtImpAntePartitaIva))
                                impresaAppaltante.PartitaIva = reader.GetString(indiceAtImpAntePartitaIva);
                            impresaAppaltante.CodiceFiscale = reader.GetString(indiceAtImpAnteCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtImpAnteIndirizzo))
                                impresaAppaltante.Indirizzo = reader.GetString(indiceAtImpAnteIndirizzo);
                            if (!reader.IsDBNull(indiceAtImpAnteComune))
                                impresaAppaltante.Comune = reader.GetString(indiceAtImpAnteComune);
                            if (!reader.IsDBNull(indiceAtImpAnteProvincia))
                                impresaAppaltante.Provincia = reader.GetString(indiceAtImpAnteProvincia);
                            if (!reader.IsDBNull(indiceAtImpAnteCap))
                                impresaAppaltante.Cap = reader.GetString(indiceAtImpAnteCap);

                            impresaAppaltante.TipologiaContratto =
                                (TipologiaContratto) reader.GetInt16(indiceAtImpAnteTipologiaContratto);
                            impresaAppaltante.LavoratoreAutonomo = reader.GetBoolean(indiceAtImpAnteLavoratoreAutonomo);

                            if (!reader.IsDBNull(indiceAtImpAnteTelefonoSedeAmministrativa))
                                impresaAppaltata.AmmiTelefono =
                                    reader.GetString(indiceAtImpAnteTelefonoSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAnteTelefonoSedeLegale))
                                impresaAppaltata.LegaleTelefono = reader.GetString(indiceAtImpAnteTelefonoSedeLegale);
                            if (!reader.IsDBNull(indiceAtImpAnteEmailSedeAmministrativa))
                                impresaAppaltata.AmmiEmail = reader.GetString(indiceAtImpAnteEmailSedeAmministrativa);
                            if (!reader.IsDBNull(indiceAtImpAnteEmailSedeLegale))
                                impresaAppaltata.AmmiTelefono = reader.GetString(indiceAtImpAnteEmailSedeLegale);
                        }

                        subappalto.Appaltante = impresaAppaltante;
                    }

                    #endregion
                }
            }

            return domanda;
        }

        public Int32 GetIdTimbratura(Timbratura timbratura, TipologiaFornitore fornitore)
        {
            Int32 ret = -1;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbraturaIdSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, timbratura.CodiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, timbratura.CodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@dataOra", DbType.DateTime, timbratura.DataOra);
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, (Int32) fornitore);
                if (timbratura.IngressoUscita)
                    DatabaseCemi.AddInParameter(comando, "@ingressoUscita", DbType.Int32, 1);
                else
                    DatabaseCemi.AddInParameter(comando, "@ingressoUscita", DbType.Int32, 0);


                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTimbratura = reader.GetOrdinal("idTimbratura");

                    #endregion

                    while (reader.Read())
                    {
                        ret = reader.GetInt32(indiceIdTimbratura);
                    }
                }
            }

            return ret;
        }

        public Int32 GetCodiceCe(string codiceFiscale, int? idImpresa, int? idCantiere)
        {
            Int32 ret = -1;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriCodiceCESelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@idSICE", DbType.Int32, 4);

                if (idImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);

                if (idCantiere.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere.Value);

                using (DatabaseCemi.ExecuteReader(comando))
                {
                    if (DatabaseCemi.GetParameterValue(comando, "@idSICE") != DBNull.Value)
                        ret = (Int32) DatabaseCemi.GetParameterValue(comando, "@idSICE");
                }
            }

            return ret;
        }

        public Nominativo GetNominativo(int codiceCe)
        {
            Nominativo nominativo = new Nominativo();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriNominativoSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idSICE", DbType.String, codiceCe);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Int32 indiceNome = reader.GetOrdinal("nome");
                        Int32 indiceCognome = reader.GetOrdinal("cognome");
                        Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                        if (!reader.IsDBNull(indiceNome))
                            nominativo.Nome = reader.GetString(indiceNome);
                        if (!reader.IsDBNull(indiceCognome))
                            nominativo.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                            nominativo.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        nominativo.CodiceCe = codiceCe;
                    }
                }
            }

            return nominativo;
        }

        public string[] GetLavoratoreByCodFisc(string codiceFiscale)
        {
            string[] nominativo = new string[2];
            nominativo[0] = string.Empty;
            nominativo[1] = string.Empty;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("USP_AccessoCantieriTimbratureNominativoLavoratoreSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Int32 indiceNome = reader.GetOrdinal("nome");
                        Int32 indiceCognome = reader.GetOrdinal("cognome");

                        if (!reader.IsDBNull(indiceNome))
                            nominativo[0] = reader.GetString(indiceNome);
                        else
                        {
                            nominativo[0] = string.Empty;
                        }
                        if (!reader.IsDBNull(indiceCognome))
                            nominativo[1] = reader.GetString(indiceCognome);
                        else
                        {
                            nominativo[1] = string.Empty;
                        }
                    }
                }
            }

            return nominativo;
        }


        public Boolean ImpresaRegolareBni(int idImpresa)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_BNIIrregolariSelectByIdImpresa"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public Boolean ImpresaRegolareFreccia(int idImpresa, int anno, int mese, int tolleranza)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_RecuperoCreditiSelectByImpresaAnnoMeseTolleranza"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);
                DatabaseCemi.AddInParameter(comando, "@tolleranza", DbType.Int32, tolleranza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public Boolean ImpresaRegolareSaldoContabile(int idImpresa, int anno, int mese, int tolleranza)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_SituazioniContabiliImpreseSelectByImpresaAnnoMeseTolleranza"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);
                DatabaseCemi.AddInParameter(comando, "@tolleranza", DbType.Int32, tolleranza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        // gestire anche il fatto che non lo trovo perchè troppo presto e non ho il dato
                        return false;
                    }
                }
            }

            return true;
        }

        public CantiereCollection GetCantieriByIdImpresa(int idImpresa)
        {
            CantiereCollection cantieri = new CantiereCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListSelectCantieriByIdImpresa"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Cantiere cantiere = new Cantiere();

                        Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");
                        Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                        Int32 indiceComune = reader.GetOrdinal("comune");
                        Int32 indiceProvincia = reader.GetOrdinal("provincia");
                        Int32 indiceCap = reader.GetOrdinal("cap");
                        Int32 indiceAutorizzazioneAlSubappalto = reader.GetOrdinal("autorizzazioneAlSubappalto");

                        if (!reader.IsDBNull(indiceIdCantiere))
                            cantiere.IdCantiere = reader.GetInt32(indiceIdCantiere);
                        if (!reader.IsDBNull(indiceIndirizzo))
                            cantiere.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                            cantiere.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            cantiere.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            cantiere.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceAutorizzazioneAlSubappalto))
                            cantiere.AutorizzazioneAlSubappalto = reader.GetString(indiceAutorizzazioneAlSubappalto);

                        cantieri.Add(cantiere);
                    }
                }
            }

            return cantieri;
        }

        public ImpresaCollection GetSubappaltateByIdImpresa(int idImpresa, int idCantiere)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriWhiteListSubappaltiSubappaltateDiretteSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();

                        Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                        Int32 indiceIdAccessoCantieriImpresa = reader.GetOrdinal("idAccessoCantieriImpresa");

                        Int32 indiceRagioneSociale = reader.GetOrdinal("impRagioneSociale");
                        Int32 indiceAtRagioneSociale = reader.GetOrdinal("atImpRagioneSociale");

                        Int32 indiceCodiceFiscale = reader.GetOrdinal("impCodiceFiscale");
                        Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atImpCodiceFiscale");

                        if (!reader.IsDBNull(indiceIdImpresa))
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        if (!reader.IsDBNull(indiceIdAccessoCantieriImpresa))
                            impresa.IdImpresa = reader.GetInt32(indiceIdAccessoCantieriImpresa);

                        if (!reader.IsDBNull(indiceRagioneSociale))
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indiceAtRagioneSociale))
                            impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);

                        if (!reader.IsDBNull(indiceCodiceFiscale))
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        if (!reader.IsDBNull(indiceAtCodiceFiscale))
                            impresa.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);

                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        public void InsertComunicazione(int? idSms, int? idEmail, int idCantiere, int? idReferente, int? idAltraPersona)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriComunicazioniInsert"))
            {
                if (idSms.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idSms", DbType.Int32, idSms.Value);
                if (idEmail.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, idEmail.Value);

                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

                if (idReferente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idReferente", DbType.Int32, idReferente.Value);
                if (idAltraPersona.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idAltraPersona", DbType.Int32, idAltraPersona.Value);

                DatabaseCemi.ExecuteNonQuery(comando);
            }

            return;
        }

        public Statistica GetStatistica(Int32? idCantiere, Int32? mese, Int32? anno, DateTime? dataDa, DateTime? dataA,
                                        Int32? idFornitore, string codiceRilevatore)
        {
            Statistica statistica;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriStatisticheSelect"))
            {
                if (idCantiere.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                if (mese.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);
                if (anno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);

                if (dataDa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, dataDa);
                if (dataA.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataA);
                if (idFornitore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);
                if (!string.IsNullOrEmpty(codiceRilevatore))
                    DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    statistica = new Statistica();

                    #region ImpreseCEAppaltatrici

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.ImpreseCeAppaltatrici = new ImpresaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region ImpreseCESubappaltate

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.ImpreseCeSubappaltate = new ImpresaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region ImpreseNuoveAppaltatrici

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.ImpreseNuoveAppaltatrici = new ImpresaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region ImpreseNuoveSubappaltate

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.ImpreseNuoveSubappaltate = new ImpresaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region ImpreseArtigianeAppaltatrici

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.ImpreseArtigianeAppaltatrici = new ImpresaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region ImpreseArtigianeSubappaltate

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.ImpreseArtigianeSubappaltate = new ImpresaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region LavoratoriCE

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.LavoratoriCe = new LavoratoreCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region LavoratoriNuovi

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.LavoratoriNuovi = new LavoratoreCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region Timbrature

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.Timbrature = new TimbraturaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region TimbratureFiltrate

                    #region Indici per reader

                    #endregion

                    while (reader.Read())
                    {
                        statistica.TimbratureFiltrate = new TimbraturaCollection();
                    }

                    #endregion

                    reader.NextResult();

                    #region Totali

                    #region Indici per reader

                    Int32 indiceTotaleImpreseCeAppaltatrici = reader.GetOrdinal("totaleImpreseCEAppaltatrici");
                    Int32 indiceTotaleImpreseCeSubappaltate = reader.GetOrdinal("totaleImpreseCESubappaltate");
                    Int32 indiceTotaleImpreseNuoveAppaltatrici = reader.GetOrdinal("totaleImpreseNuoveAppaltatrici");
                    Int32 indiceTotaleImpreseNuoveSubappaltate = reader.GetOrdinal("totaleImpreseNuoveSubappaltate");
                    Int32 indiceTotaleImpreseArtigianeSubappaltate =
                        reader.GetOrdinal("totaleImpreseArtigianeSubappaltate");
                    Int32 indiceTotaleImpreseArtigianeAppaltatrici =
                        reader.GetOrdinal("totaleImpreseArtigianeAppaltatrici");
                    Int32 indiceTotaleLavoratoriCe = reader.GetOrdinal("totaleLavoratoriCE");
                    Int32 indiceTotaleLavoratoriNuovi = reader.GetOrdinal("totaleLavoratoriNuovi");
                    Int32 indiceTotaleTimbrature = reader.GetOrdinal("totaleTimbrature");
                    Int32 indiceTotaleTimbratureFiltrate = reader.GetOrdinal("totaleTimbratureFiltrate");

                    #endregion

                    while (reader.Read())
                    {
                        statistica.TotaleImpreseCeAppaltatrici = reader.GetInt32(indiceTotaleImpreseCeAppaltatrici);
                        statistica.TotaleImpreseCeSubappaltate = reader.GetInt32(indiceTotaleImpreseCeSubappaltate);
                        statistica.TotaleImpreseNuoveAppaltatrici = reader.GetInt32(indiceTotaleImpreseNuoveAppaltatrici);
                        statistica.TotaleImpreseNuoveSubappaltate = reader.GetInt32(indiceTotaleImpreseNuoveSubappaltate);
                        statistica.TotaleImpreseArtigianeSubappaltate =
                            reader.GetInt32(indiceTotaleImpreseArtigianeSubappaltate);
                        statistica.TotaleImpreseArtigianeAppaltatrici =
                            reader.GetInt32(indiceTotaleImpreseArtigianeAppaltatrici);
                        statistica.TotaleLavoratoriCe = reader.GetInt32(indiceTotaleLavoratoriCe);
                        statistica.TotaleLavoratoriNuovi = reader.GetInt32(indiceTotaleLavoratoriNuovi);
                        statistica.TotaleTimbrature = reader.GetInt32(indiceTotaleTimbrature);
                        statistica.TotaleTimbratureFiltrate = reader.GetInt32(indiceTotaleTimbratureFiltrate);
                    }

                    #endregion
                }
            }

            return statistica;
        }

        public Denuncia GetDenuncia(int anno, int mese, int idImpresa)
        {
            Denuncia denuncia = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_DenunceConfermateSelectByidImpresaAnnoMese"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@meseDenuncia", DbType.Int32, mese);
                DatabaseCemi.AddInParameter(comando, "@annoDenuncia", DbType.Int32, anno);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceAnno = reader.GetOrdinal("annoDenuncia");
                    Int32 indiceMese = reader.GetOrdinal("meseDenuncia");
                    Int32 indiceStatoDenuncia = reader.GetOrdinal("statoDenuncia");
                    Int32 indiceDataDenuncia = reader.GetOrdinal("dataDenuncia");

                    while (reader.Read())
                    {
                        denuncia = new Denuncia
                                       {
                                           IdImpresa = reader.GetInt32(indiceIdImpresa),
                                           AnnoDenuncia = reader.GetInt32(indiceAnno),
                                           MeseDenuncia = reader.GetInt32(indiceMese),
                                           StatoDenuncia = reader.GetString(indiceStatoDenuncia),
                                           DataDenuncia = reader.GetDateTime(indiceDataDenuncia)
                                       };
                    }
                }

                return denuncia;
            }
        }

        public RapportoLavoratoreImpresa GetRapportoLavoratoreImpresa(int idImpresa, int idLavoratore,
                                                                      DateTime dataDenuncia)
        {
            RapportoLavoratoreImpresa rapporto = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_RapportiLavoratoreImpresaCompletiSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@dataDenuncia", DbType.DateTime, dataDenuncia);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceDataDenuncia = reader.GetOrdinal("dataDenuncia");
                    Int32 indiceTipoRapporto = reader.GetOrdinal("tipoRapporto");

                    while (reader.Read())
                    {
                        rapporto = new RapportoLavoratoreImpresa
                                       {
                                           IdImpresa = reader.GetInt32(indiceIdImpresa),
                                           IdLavoratore = reader.GetInt32(indiceIdLavoratore),
                                           DataDenuncia = reader.GetDateTime(indiceDataDenuncia)
                                       };

                        if (!reader.IsDBNull(indiceTipoRapporto))
                        {
                            rapporto.Tipo = reader.GetString(indiceTipoRapporto);
                        }
                    }
                }
            }
            return rapporto;
        }

        public OreDenunciate GetOreDenunciate(int idImpresa, int idLavoratore, DateTime dataDenuncia, string tipoOra)
        {
            OreDenunciate oreDenunciate = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_OreDenunciateCompleteSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@dataDenormalizzazione", DbType.DateTime, dataDenuncia);
                DatabaseCemi.AddInParameter(comando, "@idTipoOra", DbType.String, tipoOra);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceDataDenuncia = reader.GetOrdinal("dataDenormalizzazione");
                    Int32 indiceTipoOra = reader.GetOrdinal("idTipoOra");
                    Int32 indiceOreDichiarate = reader.GetOrdinal("oreDichiarate");

                    while (reader.Read())
                    {
                        oreDenunciate = new OreDenunciate
                                            {
                                                IdImpresa = reader.GetInt32(indiceIdImpresa),
                                                IdLavoratore = reader.GetInt32(indiceIdLavoratore),
                                                DataDenormalizzazione = reader.GetDateTime(indiceDataDenuncia),
                                                IdTipoOra = reader.GetString(indiceTipoOra),
                                                OreDichiarate = reader.GetDecimal(indiceOreDichiarate)
                                            };
                    }
                }
            }
            return oreDenunciate;
        }

        //public Int32? GetImpresaByFilter(int idLavoratore, int idCantiere)
        //{
        //    Int32? ret = null;

        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriImpresaIscrittaSelectByFilter"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
        //        DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);


        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            #region Indici per reader

        //            Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");

        //            #endregion

        //            while (reader.Read())
        //            {
        //                ret = reader.GetInt32(indiceIdImpresa);
        //            }
        //        }
        //    }

        //    return ret;
        //}

        public string GetImpresaByIdCantiereCodFiscLav(Int32? idCantiere, string codiceFiscale, DateTime dataOra)
        {
            string ret = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriWhiteListLavoratoriSelectPIvaByIdCantiereCodeFisc"))
            {
                if (idCantiere != null)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere.Value);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@dataOra", DbType.DateTime, dataOra);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");

                    #endregion

                    while (reader.Read())
                    {
                        ret = reader.GetString(indicePartitaIva);
                    }
                }
            }

            return ret;
        }

        public Impresa GetImpresaControlli(int idCantiere, string codiceFiscale, string partitaIva)
        {
            Impresa imp = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliImpresaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                if (!string.IsNullOrEmpty(partitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);


                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atRagioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("PartitaIVA");
                    Int32 indiceAtPartitaIva = reader.GetOrdinal("atPartitaIVA");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atCodiceFiscale");
                    Int32 indiceLavoratoreAutonomo = reader.GetOrdinal("lavoratoreAutonomo");
                    Int32 indiceDataIscrizione = reader.GetOrdinal("dataIscrizione");
                    Int32 indiceStatoImpresa = reader.GetOrdinal("statoImpresa");
                    Int32 indiceDataSospensione = reader.GetOrdinal("dataSospensione");
                    Int32 indiceDataCessazione = reader.GetOrdinal("dataDisdetta");

                    #endregion

                    while (reader.Read())
                    {
                        imp = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            imp.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            if (!reader.IsDBNull(indiceRagioneSociale))
                                imp.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                                imp.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            if (!reader.IsDBNull(indicePartitaIva))
                                imp.PartitaIva = reader.GetString(indicePartitaIva);
                            if (!reader.IsDBNull(indiceDataIscrizione))
                                imp.DataIscrizione = reader.GetDateTime(indiceDataIscrizione);
                            if (!reader.IsDBNull(indiceStatoImpresa))
                                imp.Stato = reader.GetString(indiceStatoImpresa);
                            if (!reader.IsDBNull(indiceDataSospensione))
                                imp.DataSospensione = reader.GetDateTime(indiceDataSospensione);
                            if (!reader.IsDBNull(indiceDataCessazione))
                                imp.DataCessazione = reader.GetDateTime(indiceDataCessazione);
                        }
                        else
                        {
                            imp.IdImpresa = null;
                            if (!reader.IsDBNull(indiceAtRagioneSociale))
                                imp.RagioneSociale = reader.GetString(indiceAtRagioneSociale);
                            if (!reader.IsDBNull(indiceAtCodiceFiscale))
                                imp.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtPartitaIva))
                                imp.PartitaIva = reader.GetString(indiceAtPartitaIva);
                            if (!reader.IsDBNull(indiceLavoratoreAutonomo))
                                imp.LavoratoreAutonomo = reader.GetBoolean(indiceLavoratoreAutonomo);
                        }
                    }
                }
            }

            return imp;
        }

        public LavoratoreCollection GetLavoratoreControlli(int idCantiere, string codiceFiscale, int? idImpresa)
        {
            Lavoratore lav;
            LavoratoreCollection lavColl = new LavoratoreCollection();
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliLavoratoreSelect")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                if (idImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceIncongruenze = reader.GetOrdinal("incongruenze");

                    #endregion

                    while (reader.Read())
                    {
                        lav = new Lavoratore();

                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lav.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }

                        lav.Nome = reader.GetString(indiceNome);
                        lav.Cognome = reader.GetString(indiceCognome);
                        lav.DataNascita = reader.GetDateTime(indiceDataNascita);
                        if (reader.GetInt32(indiceIncongruenze) == 0)
                            lav.Incongruenze = false;
                        else
                            lav.Incongruenze = true;

                        lavColl.Add(lav);
                    }
                }
            }

            return lavColl;
        }

        public Impresa GetLavoratoreAutonomoControlli(int idCantiere, string codiceFiscale)
        {
            Impresa imp = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliLavoratoreAutonomoSelect")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");

                    #endregion

                    while (reader.Read())
                    {
                        imp = new Impresa
                                  {
                                      Nome = reader.GetString(indiceNome),
                                      Cognome = reader.GetString(indiceCognome),
                                      DataNascita = reader.GetDateTime(indiceDataNascita)
                                  };
                    }
                }
            }

            return imp;
        }

        //private void UpdateControlliInps(string codiceFiscaleLavoratore, string partitaIvaImpresaCEMI, DateTime? dataUltimaContribuzione, string codiceFiscaleImpresaINPS, string partitaIvaImpresaINPS)
        //{
        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSUpdate"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String,
        //                                    codiceFiscaleLavoratore);
        //        DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresaCEMI", DbType.String,
        //                                    partitaIvaImpresaCEMI);
        //        DatabaseCemi.AddInParameter(comando, "@dataUltimaContribuzione", DbType.DateTime,
        //                                    dataUltimaContribuzione);
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleImpresaINPS", DbType.String,
        //                                    codiceFiscaleImpresaINPS);
        //        DatabaseCemi.AddInParameter(comando, "@partitaIVAImpresaINPS", DbType.String,
        //                                    partitaIvaImpresaINPS);

        //        DatabaseCemi.ExecuteNonQuery(comando);

        //    }

        //    return;
        //}

        //public Boolean UpdateControlliINPS2(string codiceFiscaleLavoratore, string partitaIvaImpresa,
        //                                    Int32 anno, Int32 mese,
        //                                    Boolean? superato)
        //{
        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSUpdate2"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String,
        //                                    codiceFiscaleLavoratore);
        //        DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String,
        //                                    partitaIvaImpresa);
        //        DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32,
        //                                    anno);
        //        DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32,
        //                                    mese);
        //        DatabaseCemi.AddInParameter(comando, "@superato", DbType.Boolean,
        //                                    superato);

        //        DatabaseCemi.ExecuteNonQuery(comando);
        //    }
        //}

        //public void ImportaExcelINPS(string path)
        //{
        //    DirectoryInfo di = new DirectoryInfo(path);
        //    FileInfo[] rgFiles = di.GetFiles("*.xls");
        //    foreach (FileInfo fi in rgFiles)
        //    {
        //        using (
        //            OleDbConnection conn =
        //                new OleDbConnection(
        //                    string.Format(
        //                        @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0",
        //                        path + fi.Name))
        //            )
        //        {
        //            conn.Open();
        //            string query = "select * from [Foglio1$]";
        //            using (OleDbCommand command = new OleDbCommand(query, conn))
        //            {
        //                using (IDataReader reader = command.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {
        //                        string codiceFiscaleLavoratore = reader[1].ToString();
        //                        string ultimecontribuzioneString = null;
        //                        DateTime? dataUltimaContribuzione = null;
        //                        if (!string.IsNullOrEmpty(reader[6].ToString()))
        //                        {
        //                            ultimecontribuzioneString = reader[6].ToString();
        //                            dataUltimaContribuzione =
        //                                new DateTime(Int32.Parse(ultimecontribuzioneString.Split('/')[1]),
        //                                             Int32.Parse(ultimecontribuzioneString.Split('/')[0]), 1);
        //                        }
        //                        string partitaIvaImpresaCEMI = null;
        //                        if (!string.IsNullOrEmpty(reader[2].ToString()))
        //                        {
        //                            partitaIvaImpresaCEMI = reader[2].ToString();
        //                        }
        //                        string codiceFiscaleImpresaINPS = null;
        //                        if (!string.IsNullOrEmpty(reader[8].ToString()))
        //                        {
        //                            codiceFiscaleImpresaINPS = reader[8].ToString();
        //                        }
        //                        string partitaIvaImpresaINPS = null;
        //                        if (!string.IsNullOrEmpty(reader[9].ToString()))
        //                        {
        //                            partitaIvaImpresaINPS = reader[9].ToString().PadLeft(11, '0');
        //                        }

        //                        UpdateControlliInps(codiceFiscaleLavoratore, partitaIvaImpresaCEMI,
        //                                            dataUltimaContribuzione, codiceFiscaleImpresaINPS,
        //                                            partitaIvaImpresaINPS);
        //                    }
        //                }
        //            }
        //            conn.Close();
        //            //File.Move(string.Format("{0}{1}", path, fi.Name), string.Format("{0}Elaborati\\{1}", path, fi.Name));
        //        }
        //    }
        //}

        //public void ImportaExcelINPS2(string pathScaricati, string pathElaborati)
        //{
        //    DirectoryInfo di = new DirectoryInfo(pathScaricati);
        //    FileInfo[] rgFiles = di.GetFiles("*.xls");
        //    foreach (FileInfo fi in rgFiles)
        //    {
        //        if (!File.Exists(string.Format("{0}{1}", pathElaborati, fi.Name)))
        //        {
        //            using (
        //                OleDbConnection conn =
        //                    new OleDbConnection(
        //                        string.Format(
        //                            @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0",
        //                            pathScaricati + fi.Name))
        //                )
        //            {
        //                conn.Open();
        //                string query = "select * from [Foglio1$]";
        //                using (OleDbCommand command = new OleDbCommand(query, conn))
        //                {
        //                    using (IDataReader reader = command.ExecuteReader())
        //                    {
        //                        while (reader.Read())
        //                        {
        //                            string codiceFiscaleLavoratore = reader[1].ToString();

        //                            string partitaIvaImpresa = null;
        //                            if (!string.IsNullOrEmpty(reader[2].ToString()))
        //                            {
        //                                partitaIvaImpresa = reader[2].ToString();
        //                            }

        //                            Int32 anno = Int32.Parse(reader[3].ToString());

        //                            Int32 mese = Int32.Parse(reader[4].ToString());

        //                            Boolean? superato = null;
        //                            if (!string.IsNullOrEmpty(reader[5].ToString()))
        //                            {
        //                                //superato = reader[5].ToString() == "OK";

        //                                superato = reader[5].ToString().StartsWith("OK");
        //                            }

        //                            if (partitaIvaImpresa != null)
        //                                //UpdateControlliINPS2(codiceFiscaleLavoratore, partitaIvaImpresa, anno, mese, superato);
        //                                ControlliINPSInsert2aux(codiceFiscaleLavoratore, partitaIvaImpresa, anno, mese,
        //                                                        superato);
        //                        }
        //                    }
        //                }
        //                conn.Close();
        //                //File.Move(string.Format("{0}{1}", path, fi.Name), string.Format("{0}Elaborati\\{1}", path, fi.Name));
        //                File.Copy(string.Format("{0}{1}", pathScaricati, fi.Name),
        //                          string.Format("{0}{1}", pathElaborati, fi.Name));
        //            }
        //        }
        //    }
        //}

        //public List<string> GetCodiciFiscaliINPS()
        //{
        //    List<string> codiciFiscaliINPS = new List<string>();

        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("USP_AccessoCantieriControlliINPSCodiciFiscaliSelect"))
        //    {
        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            while (reader.Read())
        //            {
        //                codiciFiscaliINPS.Add(reader.GetString(reader.GetOrdinal("codiceFiscale")));
        //            }
        //        }
        //    }

        //    return codiciFiscaliINPS;
        //}


        //private ElementoINPSCollection GetDatiRichiestaInps()
        //{
        //    ElementoINPSCollection inpsColl = new ElementoINPSCollection();

        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSSelectRichiesta")
        //        )
        //    {
        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            #region Indici per reader

        //            Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscaleLavoratore");
        //            Int32 indicePartitaIva = reader.GetOrdinal("partitaIvaImpresaCemi");

        //            #endregion

        //            while (reader.Read())
        //            {
        //                ElementoINPS elemInps = new ElementoINPS
        //                                            {
        //                                                CodiceFiscaleLavoratoreINPS =
        //                                                    reader.GetString(indiceCodiceFiscale)
        //                                            };

        //                if (!reader.IsDBNull(indicePartitaIva))
        //                {
        //                    elemInps.PartitaIVAImpresaCEMIINPS = reader.GetString(indicePartitaIva);
        //                }

        //                inpsColl.Add(elemInps);
        //            }
        //        }
        //    }

        //    return inpsColl;
        //}

        public int? GetIdLavoratore(string codiceFiscale)
        {
            int? idLav = null;

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriSelectIdLavByCF"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);

                try
                {
                    idLav = (int) DatabaseCemi.ExecuteScalar(dbCommand);
                }
                catch (Exception)
                {
                }
            }

            return idLav;
        }

        //public void CreaExcelINPS(string path)
        //{
        //    string filename = DateTime.Now.ToString("yyyyMMdd-hhmmss");
        //    //File.Create(string.Format("{0}{1}.xls", path, filename)).Close();

        //    CreateWorkbook(string.Format("{0}{1}.xls", path, filename));

        //    using (
        //        OleDbConnection conn =
        //            new OleDbConnection(
        //                string.Format(
        //                    @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0",
        //                    string.Format("{0}{1}.xls", path, filename)))
        //        )
        //    {
        //        conn.Open();

        //        string query1 = "CREATE TABLE [Foglio1$]([CodiceFiscale] VARCHAR(255), [PartitaIVA] VARCHAR(11))";
        //        using (OleDbCommand command = new OleDbCommand(query1, conn))
        //        {
        //            command.ExecuteNonQuery();
        //        }


        //        //List<string> codiciFiscali = GetCodiciFiscaliINPS();

        //        //foreach (string codFisc in codiciFiscali)
        //        //{
        //        //    string query = string.Format("{0}{1}')", "insert into [Foglio1$] (CodiceFiscale) values ('", codFisc);
        //        //    using (OleDbCommand command = new OleDbCommand(query, conn))
        //        //    {
        //        //        command.ExecuteNonQuery();
        //        //    }
        //        //}

        //        ElementoINPSCollection inpsColl = GetDatiRichiestaInps();

        //        foreach (ElementoINPS elemInps in inpsColl)
        //        {
        //            string query = string.Format("{0}{1}','{2}')",
        //                                         "insert into [Foglio1$] (CodiceFiscale, PartitaIVA) values ('",
        //                                         elemInps.CodiceFiscaleLavoratoreINPS,
        //                                         elemInps.PartitaIVAImpresaCEMIINPS);
        //            using (OleDbCommand command = new OleDbCommand(query, conn))
        //            {
        //                command.ExecuteNonQuery();
        //            }
        //        }


        //        conn.Close();
        //    }
        //}

        public void UpdateDataStampaBadge(int idDomanda, int? idLavoratore, int? idAccessoCantieriLavoratore,
                                          int? idImpresa, int? idAccessoCantieriImpresa, DateTime? dataStampa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListLavoratoriStampaBadgeUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32,
                                            idDomanda);
                if (idLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                idLavoratore);
                if (idAccessoCantieriLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriLavoratore", DbType.Int32,
                                                idAccessoCantieriLavoratore);
                if (idImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32,
                                                idImpresa);
                if (idAccessoCantieriImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriImpresa", DbType.Int32,
                                                idAccessoCantieriImpresa);

                if (dataStampa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataStampaBadge", DbType.DateTime,
                                                    dataStampa);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
            }

            return;
        }

        public void UpdateDataStampaBadgeAutonomo(int idAccessoCantieriImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriImpreseLavoratoriAutonomiStampaBadgeUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriImpresa", DbType.Int32,
                                            idAccessoCantieriImpresa);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        //da mettere nel common
        public string GetCodiceCatastale(string nazione)
        {
            string codiceCatastale = null;

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ComuniSiceNewSelectByNomeNazione"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@nomeNazione", DbType.String, nazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        codiceCatastale = reader["codiceCatastale"].ToString();
                    }
                }
            }
            return codiceCatastale;
        }

        #region temp

        //public void ControlliINPSInsert2(string codiceFiscale)
        //{
        //    using (
        //        DbCommand comando =
        //            databaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSInsert2"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
        //        //DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

        //        DatabaseCemi.ExecuteNonQuery(comando);
        //    }
        //}

        //public Boolean UpdateControlliINPSTemp(string codiceFiscale, DateTime? dataUltimaContribuzione, string pIVAAzienda, string codiceFiscaleAzienda, string pIVAAziendaCemi)
        //{
        //    Boolean res = false;

        //    using (
        //        DbCommand comando =
        //            databaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSUpdate2"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
        //                                    codiceFiscale);
        //        DatabaseCemi.AddInParameter(comando, "@dataUltimaContribuzione", DbType.DateTime,
        //                                    dataUltimaContribuzione);
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleAzienda", DbType.String,
        //                                   codiceFiscaleAzienda);
        //        DatabaseCemi.AddInParameter(comando, "@pIVAAzienda", DbType.String,
        //                                   pIVAAzienda);
        //        DatabaseCemi.AddInParameter(comando, "@pIVAAziendaCemi", DbType.String,
        //                                   pIVAAziendaCemi);

        //        if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
        //        {
        //            res = true;
        //        }
        //    }

        //    return res;
        //}

        //public void ImportaExcelINPSTemp(string path)
        //{
        //    DirectoryInfo di = new DirectoryInfo(path);
        //    FileInfo[] rgFiles = di.GetFiles("*.xls");
        //    foreach (FileInfo fi in rgFiles)
        //    {

        //        using (
        //       OleDbConnection conn =
        //           new OleDbConnection(
        //               string.Format(
        //                   @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", path + fi.Name))
        //       )
        //        {
        //            conn.Open();
        //            string query = "select * from [Foglio1$]";
        //            using (OleDbCommand command = new OleDbCommand(query, conn))
        //            {
        //                using (IDataReader reader = command.ExecuteReader())
        //                {
        //                    while (reader.Read())
        //                    {

        //                        string codiceFiscale = reader[1].ToString();
        //                        string ultimecontribuzioneString = null;
        //                        DateTime? dataUltimaContribuzione = null;
        //                        if (!string.IsNullOrEmpty(reader[6].ToString()))
        //                        {
        //                            ultimecontribuzioneString = reader[6].ToString();
        //                            dataUltimaContribuzione =
        //                                new DateTime(Int32.Parse(ultimecontribuzioneString.Split('/')[1]),
        //                                             Int32.Parse(ultimecontribuzioneString.Split('/')[0]), 1);
        //                        }
        //                        string codiceFiscaleAzienda = null;
        //                        //if (!string.IsNullOrEmpty(reader[7].ToString()))
        //                        //{
        //                        //    codiceFiscaleAzienda = reader[7].ToString();
        //                        //}
        //                        string pIVAAzienda = null;
        //                        if (!string.IsNullOrEmpty(reader[9].ToString()))
        //                        {
        //                            pIVAAzienda = reader[9].ToString().PadLeft(11,'0');
        //                        }

        //                        string pIVAAziendaCemi = null;
        //                        if (!string.IsNullOrEmpty(reader[2].ToString()))
        //                        {
        //                            pIVAAziendaCemi = reader[2].ToString().PadLeft(11, '0');
        //                        }

        //                        //UpdateControlliINPS(codiceFiscale, dataUltimaContribuzione, codiceFiscaleAzienda, pIVAAzienda);
        //                        //ControlliINPSInsert2(codiceFiscale);
        //                        UpdateControlliINPS(codiceFiscale, dataUltimaContribuzione, codiceFiscaleAzienda, pIVAAzienda);
        //                    }
        //                }
        //            }
        //            conn.Close();
        //            //File.Move(string.Format("{0}{1}", path, fi.Name), string.Format("{0}Elaborati\\{1}", path, fi.Name));
        //        }


        //    }

        //}

        #endregion

        #region Impresa

        private Boolean InsertImpresa(Impresa impresa, DbTransaction transaction)
        {
            Boolean res = false;

            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            if (impresa.TipoImpresa != TipologiaImpresa.Nuova)
            {
                throw new ArgumentException("Possono essere inserite solo imprese di tipo Nuova");
            }
            if (impresa.IdImpresa.HasValue)
            {
                throw new ArgumentException("L'impresa che si sta tentando di inserire ha già un Id");
            }
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriImpreseInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                            impresa.RagioneSociale);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                if (!String.IsNullOrEmpty(impresa.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                if (!String.IsNullOrEmpty(impresa.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                if (!String.IsNullOrEmpty(impresa.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                if (!String.IsNullOrEmpty(impresa.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                if (!String.IsNullOrEmpty(impresa.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);

                DatabaseCemi.AddInParameter(comando, "@tipologiaContratto", DbType.Int16,
                                            impresa.TipologiaContratto);
                DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean,
                                            impresa.LavoratoreAutonomo);
                if (!String.IsNullOrEmpty(impresa.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, impresa.Cognome);
                if (!String.IsNullOrEmpty(impresa.Nome))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, impresa.Nome);
                if (impresa.DataNascita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                impresa.DataNascita);
                if (!String.IsNullOrEmpty(impresa.NumeroCameraCommercio))
                    DatabaseCemi.AddInParameter(comando, "@numeroCameraCommercio", DbType.String,
                                                impresa.NumeroCameraCommercio);

                if (!String.IsNullOrEmpty(impresa.LuogoNascita))
                    DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String,
                                                impresa.LuogoNascita);

                if (!String.IsNullOrEmpty(impresa.PaeseNascita))
                    DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String,
                                                impresa.PaeseNascita);

                if (!String.IsNullOrEmpty(impresa.ProvinciaNascita))
                    DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                                                impresa.ProvinciaNascita);

                //if (impresa.DataAssunzione.HasValue)
                //    DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                //                                impresa.DataAssunzione);
                if (!String.IsNullOrEmpty(impresa.Committente))
                    DatabaseCemi.AddInParameter(comando, "@committente", DbType.String,
                                                impresa.Committente);
                if (impresa.Foto != null)
                    DatabaseCemi.AddInParameter(comando, "@foto", DbType.Binary,
                                                impresa.Foto);
                if (impresa.DataStampaBadge.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataStampaBadge", DbType.DateTime,
                                                impresa.DataStampaBadge);

                if (!String.IsNullOrEmpty(impresa.LegaleTelefono))
                    DatabaseCemi.AddInParameter(comando, "@telefonoSedeLegale", DbType.String,
                                                impresa.LegaleTelefono);
                if (!String.IsNullOrEmpty(impresa.LegaleEmail))
                    DatabaseCemi.AddInParameter(comando, "@emailSedeLegale", DbType.String,
                                                impresa.LegaleEmail);
                if (!String.IsNullOrEmpty(impresa.AmmiTelefono))
                    DatabaseCemi.AddInParameter(comando, "@telefonoSedeAmministrazione", DbType.String,
                                                impresa.AmmiTelefono);
                if (!String.IsNullOrEmpty(impresa.Cap))
                    DatabaseCemi.AddInParameter(comando, "@emailSedeAmministrazione", DbType.String,
                                                impresa.AmmiEmail);
                if (!String.IsNullOrEmpty(impresa.TipoAttivita))
                    DatabaseCemi.AddInParameter(comando, "@tipoAttivita", DbType.String,
                                                impresa.TipoAttivita);

                if (!String.IsNullOrEmpty(impresa.ContrattoApplicato))
                    DatabaseCemi.AddInParameter(comando, "@contrattoApplicato", DbType.String,
                                                impresa.ContrattoApplicato);

                DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        impresa.IdImpresa = (Int32) DatabaseCemi.GetParameterValue(comando, "@idImpresa");
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        impresa.IdImpresa = (Int32) DatabaseCemi.GetParameterValue(comando, "@idImpresa");
                        res = true;
                    }
                }
            }

            return res;
        }

        public void UpdateImpresa(Impresa impresa, DbTransaction transaction)
        {
            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            if (impresa.TipoImpresa != TipologiaImpresa.Nuova)
            {
                throw new ArgumentException("Possono essere aggiornate solo imprese di tipo Nuova");
            }
            if (!impresa.IdImpresa.HasValue)
            {
                throw new ArgumentException("L'impresa che si sta tentando di inserire non ha un Id");
            }
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriImpreseUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                            impresa.RagioneSociale);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                if (!String.IsNullOrEmpty(impresa.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                if (!String.IsNullOrEmpty(impresa.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                if (!String.IsNullOrEmpty(impresa.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                if (!String.IsNullOrEmpty(impresa.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                if (!String.IsNullOrEmpty(impresa.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);

                DatabaseCemi.AddInParameter(comando, "@tipologiaContratto", DbType.Int16,
                                            impresa.TipologiaContratto);
                DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean,
                                            impresa.LavoratoreAutonomo);
                if (!String.IsNullOrEmpty(impresa.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, impresa.Cognome);
                if (!String.IsNullOrEmpty(impresa.Nome))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, impresa.Nome);
                if (impresa.DataNascita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                impresa.DataNascita);
                if (!String.IsNullOrEmpty(impresa.NumeroCameraCommercio))
                    DatabaseCemi.AddInParameter(comando, "@numeroCameraCommercio", DbType.String,
                                                impresa.NumeroCameraCommercio);

                if (!String.IsNullOrEmpty(impresa.LuogoNascita))
                    DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String,
                                                impresa.LuogoNascita);

                if (!String.IsNullOrEmpty(impresa.PaeseNascita))
                    DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String,
                                                impresa.PaeseNascita);

                if (!String.IsNullOrEmpty(impresa.ProvinciaNascita))
                    DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                                                impresa.ProvinciaNascita);

                //if (impresa.DataAssunzione.HasValue)
                //    DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                //                                impresa.DataAssunzione);
                if (!String.IsNullOrEmpty(impresa.Committente))
                    DatabaseCemi.AddInParameter(comando, "@committente", DbType.String,
                                                impresa.Committente);
                if (impresa.Foto != null)
                    DatabaseCemi.AddInParameter(comando, "@foto", DbType.Binary,
                                                impresa.Foto);

                if (!String.IsNullOrEmpty(impresa.LegaleTelefono))
                    DatabaseCemi.AddInParameter(comando, "@telefonoSedeLegale", DbType.String,
                                                impresa.LegaleTelefono);
                if (!String.IsNullOrEmpty(impresa.LegaleEmail))
                    DatabaseCemi.AddInParameter(comando, "@emailSedeLegale", DbType.String,
                                                impresa.LegaleEmail);
                if (!String.IsNullOrEmpty(impresa.AmmiTelefono))
                    DatabaseCemi.AddInParameter(comando, "@telefonoSedeAmministrazione", DbType.String,
                                                impresa.AmmiTelefono);
                if (!String.IsNullOrEmpty(impresa.Cap))
                    DatabaseCemi.AddInParameter(comando, "@emailSedeAmministrazione", DbType.String,
                                                impresa.AmmiEmail);
                if (!String.IsNullOrEmpty(impresa.TipoAttivita))
                    DatabaseCemi.AddInParameter(comando, "@tipoAttivita", DbType.String,
                                                impresa.TipoAttivita);

                //DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                    }
                }
            }

            return;
        }

        #endregion

        #region Lavoratore

        private Boolean InsertLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            if (lavoratore.TipoLavoratore != TipologiaLavoratore.Nuovo)
            {
                throw new ArgumentException("Possono essere inseriti solo lavoratori di tipo Nuovo");
            }
            if (lavoratore.IdLavoratore.HasValue)
            {
                throw new ArgumentException("Il lavoratore che si sta tentando di inserire ha già un Id");
            }
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLavoratoriInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                            lavoratore.CodiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@idLavoratoreSICE", DbType.Int32, 4);

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        lavoratore.IdLavoratore =
                            (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratore");

                        try
                        {
                            lavoratore.IdLavoratoreTrovato =
                                (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratoreSICE");
                        }
                        catch
                        {
                            lavoratore.IdLavoratoreTrovato = null;
                        }

                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        lavoratore.IdLavoratore =
                            (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratore");

                        try
                        {
                            lavoratore.IdLavoratoreTrovato =
                                (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratoreSICE");
                        }
                        catch
                        {
                            lavoratore.IdLavoratoreTrovato = null;
                        }

                        res = true;
                    }
                }
            }

            return res;
        }


        public Boolean UpdateLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            else
            {
                if (lavoratore.TipoLavoratore != TipologiaLavoratore.Nuovo)
                {
                    throw new ArgumentException("Possono essere aggiornati solo lavoratori di tipo Nuovo");
                }
                else
                {
                    if (!lavoratore.IdLavoratore.HasValue)
                    {
                        throw new ArgumentException("Il lavoratore che si sta tentando di aggiornare non ha un Id");
                    }
                    else
                    {
                        using (
                            DbCommand comando =
                                DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLavoratoriUpdate"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                        lavoratore.IdLavoratore.Value);
                            DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                            DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                            DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                        lavoratore.CodiceFiscale);

                            if (transaction != null)
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    res = true;
                                }
                            }
                            else
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                                {
                                    res = true;
                                }
                            }
                        }
                    }
                }
            }

            return res;
        }


        public Boolean InsertRilevatore(string codiceRilevatore, Int32 idFornitore, Boolean invioWhiteList)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoreInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);
                DatabaseCemi.AddInParameter(comando, "@invioWhiteList", DbType.Boolean, invioWhiteList);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public void UpdateTimbratura(int idTimbratura, TipologiaAnomalia tipoAnomalia)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTimbratura", DbType.Int32,
                                            idTimbratura);
                DatabaseCemi.AddInParameter(comando, "@anomalia", DbType.Int32,
                                            tipoAnomalia);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void UpdateTimbraturaAnomalieDenunce(int idTimbratura, TipologiaAnomaliaDenuncia tipoAnomaliaDenuncia)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureAnomalieDenunceUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTimbratura", DbType.Int32,
                                            idTimbratura);
                DatabaseCemi.AddInParameter(comando, "@tipologiaAnomaliaDenuncia", DbType.Int32,
                                            tipoAnomaliaDenuncia);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void UpdateTimbraturaAnomalieDebiti(int idTimbratura, bool debiti)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureAnomalieDebitiUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTimbratura", DbType.Int32,
                                            idTimbratura);
                DatabaseCemi.AddInParameter(comando, "@controlloDebitiSuperato", DbType.Boolean,
                                            debiti);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public TimbraturaCollection GetTimbratureTrexom()
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureTrexomSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataOra = reader.GetOrdinal("dataOra");

                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");

                    Int32 indiceIngressoUscita = reader.GetOrdinal("ingressoUscita");

                    #endregion

                    while (reader.Read())
                    {
                        Timbratura timbratura = new Timbratura
                        {

                            CodiceFiscale = reader.GetString(indiceCodiceFiscale),
                            DataOra = reader.GetDateTime(indiceDataOra),
                            CodiceRilevatore = reader.GetString(indiceCodiceRilevatore),
                            IngressoUscita = reader.GetString(indiceIngressoUscita) == "1" ? false : true
                        };

                        timbrature.Add(timbratura);
                    }
                }
            }

            return timbrature;
        }

        public TimbraturaCollection GetTimbratureNonGestite()
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelectNonGestite"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTimbratura = reader.GetOrdinal("idTimbratura");

                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataOra = reader.GetOrdinal("dataOra");

                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceIngressoUscita = reader.GetOrdinal("ingressoUscita");
                    Int32 indiceIdFornitore = reader.GetOrdinal("idFornitore");

                    #endregion

                    while (reader.Read())
                    {
                        Timbratura timbratura = new Timbratura
                                                    {
                                                        IdTimbratura = reader.GetInt32(indiceIdTimbratura),
                                                        CodiceFiscale = reader.GetString(indiceCodiceFiscale),
                                                        DataOra = reader.GetDateTime(indiceDataOra),
                                                        CodiceRilevatore = reader.GetString(indiceCodiceRilevatore),
                                                        RagioneSociale = reader.GetString(indiceRagioneSociale),
                                                        IngressoUscita = reader.GetBoolean(indiceIngressoUscita),
                                                        Fornitore = (TipologiaFornitore) reader.GetInt32(indiceIdFornitore)
                                                    };

                        timbrature.Add(timbratura);
                    }
                }
            }

            return timbrature;
        }

        public TimbraturaCollection GetTimbratureAnomalieDenunce()
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelectAnomalieDenunce"))
            {
                comando.CommandTimeout = 120;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTimbratura = reader.GetOrdinal("idTimbratura");

                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataOra = reader.GetOrdinal("dataOra");

                    Int32 indiceControlliEffettuati = reader.GetOrdinal("controlliEffettuati");
                    Int32 indiceControlloDenunciaSuperato = reader.GetOrdinal("controlloDenunciaSuperato");
                    Int32 indiceControlloLavoratoreDenunciaSuperato =
                        reader.GetOrdinal("controlloLavoratoreDenunciaSuperato");
                    Int32 indiceControlloOreDenunciaSuperato = reader.GetOrdinal("controlloOreDenunciaSuperato");
                    Int32 indiceControlloDebitiSuperato = reader.GetOrdinal("controlloDebitiSuperato");
                    Int32 indiceControlloDebitiEffettuato = reader.GetOrdinal("controlloDebitiEffettuato");

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");

                    #endregion

                    while (reader.Read())
                    {
                        Timbratura timbratura = new Timbratura
                                                    {
                                                        IdTimbratura = reader.GetInt32(indiceIdTimbratura),
                                                        CodiceFiscale = reader.GetString(indiceCodiceFiscale),
                                                        DataOra = reader.GetDateTime(indiceDataOra)
                                                    };

                        if (!reader.IsDBNull(indiceIdLavoratore))
                            timbratura.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                        if (!reader.IsDBNull(indiceIdImpresa))
                            timbratura.IdImpresa = reader.GetInt32(indiceIdImpresa);

                        timbratura.ControlliEffettuati = reader.GetBoolean(indiceControlliEffettuati);
                        timbratura.ControlloDenunciaSuperato = reader.GetBoolean(indiceControlloDenunciaSuperato);
                        timbratura.ControlloLavoratoreDenunciaSuperato =
                            reader.GetBoolean(indiceControlloLavoratoreDenunciaSuperato);
                        timbratura.ControlloOreDenunciaSuperato = reader.GetBoolean(indiceControlloOreDenunciaSuperato);
                        timbratura.ControlloDebitiSuperato = reader.GetBoolean(indiceControlloDebitiSuperato);
                        timbratura.ControlloDebitiEffettuato = reader.GetBoolean(indiceControlloDebitiEffettuato);

                        timbrature.Add(timbratura);
                    }
                }
            }

            return timbrature;
        }

        public TimbraturaCollection GetTimbratureAnomalieDebiti()
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelectAnomalieDebiti"))
            {
                comando.CommandTimeout = 120;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTimbratura = reader.GetOrdinal("idTimbratura");

                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataOra = reader.GetOrdinal("dataOra");

                    Int32 indiceControlliEffettuati = reader.GetOrdinal("controlliEffettuati");
                    Int32 indiceControlloDenunciaSuperato = reader.GetOrdinal("controlloDenunciaSuperato");
                    Int32 indiceControlloLavoratoreDenunciaSuperato =
                        reader.GetOrdinal("controlloLavoratoreDenunciaSuperato");
                    Int32 indiceControlloOreDenunciaSuperato = reader.GetOrdinal("controlloOreDenunciaSuperato");
                    Int32 indiceControlloDebitiSuperato = reader.GetOrdinal("controlloDebitiSuperato");
                    Int32 indiceControlloDebitiEffettuato = reader.GetOrdinal("controlloDebitiEffettuato");

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");

                    #endregion

                    while (reader.Read())
                    {
                        Timbratura timbratura = new Timbratura
                                                    {
                                                        IdTimbratura = reader.GetInt32(indiceIdTimbratura),
                                                        CodiceFiscale = reader.GetString(indiceCodiceFiscale),
                                                        DataOra = reader.GetDateTime(indiceDataOra)
                                                    };

                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            timbratura.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }

                        timbratura.IdImpresa = reader.GetInt32(indiceIdImpresa);

                        timbratura.ControlliEffettuati = reader.GetBoolean(indiceControlliEffettuati);
                        timbratura.ControlloDenunciaSuperato = reader.GetBoolean(indiceControlloDenunciaSuperato);
                        timbratura.ControlloLavoratoreDenunciaSuperato =
                            reader.GetBoolean(indiceControlloLavoratoreDenunciaSuperato);
                        timbratura.ControlloOreDenunciaSuperato = reader.GetBoolean(indiceControlloOreDenunciaSuperato);
                        timbratura.ControlloDebitiSuperato = reader.GetBoolean(indiceControlloDebitiSuperato);
                        timbratura.ControlloDebitiEffettuato = reader.GetBoolean(indiceControlloDebitiEffettuato);

                        timbrature.Add(timbratura);
                    }
                }
            }

            return timbrature;
        }

        public RilevatoreCantiereCollection GetRilevatoriLiberiCantieri()
        {
            RilevatoreCantiereCollection rilevatoriCantieri = new RilevatoreCantiereCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoriLiberiCantieriSelect")
                )
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdRilevatore = reader.GetOrdinal("idRilevatore");

                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");

                    #endregion

                    while (reader.Read())
                    {
                        RilevatoreCantiere rilevatoreCantiere = new RilevatoreCantiere
                                                                    {
                                                                        IdRilevatore =
                                                                            reader.GetInt32(indiceIdRilevatore)
                                                                    };

                        if (!reader.IsDBNull(indiceCodiceRilevatore))
                            rilevatoreCantiere.CodiceRilevatore = reader.GetString(indiceCodiceRilevatore);
                        if (!reader.IsDBNull(indiceRagioneSociale))
                            rilevatoreCantiere.RagioneSociale = reader.GetString(indiceRagioneSociale);

                        rilevatoreCantiere.DataInizio = null;
                        rilevatoreCantiere.DataFine = null;

                        rilevatoriCantieri.Add(rilevatoreCantiere);
                    }
                }
            }

            return rilevatoriCantieri;
        }

        public RilevatoreCantiereCollection GetRilevatoriCantieri(Int32 idAccessoCantieriWhiteList)
        {
            RilevatoreCantiereCollection rilevatoriCantieri = new RilevatoreCantiereCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoriCantieriSelect")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idAccessoCantieriWhiteList", DbType.Int32,
                                            idAccessoCantieriWhiteList);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdRilevatore = reader.GetOrdinal("idRilevatore");
                    Int32 indiceIdAccessoCantieriWhiteList = reader.GetOrdinal("idCantiere");
                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizio");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFine");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceInvioWhitelist = reader.GetOrdinal("inviowhitelist");

                    #endregion

                    while (reader.Read())
                    {
                        RilevatoreCantiere rilevatoreCantiere = new RilevatoreCantiere
                                                                    {
                                                                        IdRilevatore =
                                                                            reader.GetInt32(indiceIdRilevatore)
                                                                    };

                        if (!reader.IsDBNull(indiceIdAccessoCantieriWhiteList))
                            rilevatoreCantiere.IdCantiere = reader.GetInt32(indiceIdAccessoCantieriWhiteList);
                        if (!reader.IsDBNull(indiceCodiceRilevatore))
                            rilevatoreCantiere.CodiceRilevatore = reader.GetString(indiceCodiceRilevatore);
                        if (!reader.IsDBNull(indiceRagioneSociale))
                            rilevatoreCantiere.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indiceLatitudine))
                            rilevatoreCantiere.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            rilevatoreCantiere.Longitudine = reader.GetDecimal(indiceLongitudine);
                        if (!reader.IsDBNull(indiceDataInizio))
                            rilevatoreCantiere.DataInizio = reader.GetDateTime(indiceDataInizio);
                        if (!reader.IsDBNull(indiceDataFine))
                            rilevatoreCantiere.DataFine = reader.GetDateTime(indiceDataFine);
                        if (!reader.IsDBNull(indiceCivico))
                            rilevatoreCantiere.Civico = reader.GetString(indiceCivico);
                        if (!reader.IsDBNull(indiceComune))
                            rilevatoreCantiere.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            rilevatoreCantiere.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceIndirizzo))
                            rilevatoreCantiere.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceInvioWhitelist))
                            rilevatoreCantiere.InvioWhitelist = reader.GetBoolean(indiceInvioWhitelist);

                        rilevatoriCantieri.Add(rilevatoreCantiere);
                    }
                }
            }

            return rilevatoriCantieri;
        }

        public FornitoreCollection GetFornitori()
        {
            FornitoreCollection fornitori = new FornitoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriFornitoriSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdFornitore = reader.GetOrdinal("idFornitore");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");

                    #endregion

                    while (reader.Read())
                    {
                        Fornitore fornitore = new Fornitore
                                                  {
                                                      IdFornitore = reader.GetInt32(indiceIdFornitore),
                                                      RagioneSociale = reader.GetString(indiceRagioneSociale)
                                                  };

                        fornitori.Add(fornitore);
                    }
                }
            }

            return fornitori;
        }

        public RilevatoreCollection GetRilevatori()
        {
            RilevatoreCollection rilevatori = new RilevatoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoriSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdRilevatore = reader.GetOrdinal("idRilevatore");
                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");

                    #endregion

                    while (reader.Read())
                    {
                        Rilevatore rilevatore = new Rilevatore
                                                    {
                                                        IdRilevatore = reader.GetInt32(indiceIdRilevatore),
                                                        Codice = reader.GetString(indiceCodiceRilevatore)
                                                    };

                        rilevatori.Add(rilevatore);
                    }
                }
            }

            return rilevatori;
        }

        public RilevatoreCollection GetRilevatoriAttiviByIdFornitore(int idFornitore)
        {
            RilevatoreCollection rilevatori = new RilevatoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoriAttiviSelectByIdFornitore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, idFornitore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdRilevatore = reader.GetOrdinal("idRilevatore");
                    Int32 indiceCodiceRilevatore = reader.GetOrdinal("codiceRilevatore");
                    Int32 indiceInvioWhitelist = reader.GetOrdinal("invioWhitelist");

                    #endregion

                    while (reader.Read())
                    {
                        Rilevatore rilevatore = new Rilevatore
                                                    {
                                                        IdRilevatore = reader.GetInt32(indiceIdRilevatore),
                                                        Codice = reader.GetString(indiceCodiceRilevatore),
                                                        InvioWhitelist = reader.GetBoolean(indiceInvioWhitelist)
                                                    };

                        rilevatori.Add(rilevatore);
                    }
                }
            }

            return rilevatori;
        }

        public List<string> GetCodFiscWhiteListByCodRilevatore(Int32 idRilevatore)
        {
            List<string> codiciFiscaliAbilitati = new List<string>();
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListCodFiscSelectByCodRilevatore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idRilevatore", DbType.Int32, idRilevatore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        codiciFiscaliAbilitati.Add(reader.GetString(reader.GetOrdinal("codiceFiscale")));
                    }
                }
            }

            return codiciFiscaliAbilitati;
        }

        public Dictionary<String, Boolean> GetCodFiscWhiteListCompletaByCodRilevatore(Int32 idRilevatore)
        {
            Dictionary<String, Boolean> whiteList = new Dictionary<String, Boolean>();
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListCodFiscCompletiSelectByCodRilevatore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idRilevatore", DbType.Int32, idRilevatore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceAbilitato = reader.GetOrdinal("abilitato");
                    #endregion

                    while (reader.Read())
                    {
                        String codFisc = reader.GetString(indiceCodiceFiscale);
                        Boolean abil = reader.GetBoolean(indiceAbilitato);

                        if (!whiteList.ContainsKey(codFisc))
                        {
                            whiteList.Add(codFisc, abil);
                        }
                    }
                }
            }

            return whiteList;
        }

        public Int32 GetIdCantiere(TipologiaFornitore fornitore, String codiceRilevatore, DateTime dataOra)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriCantiereSelectByRilevatoreData"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFornitore", DbType.Int32, (Int32) fornitore);
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@dataOra", DbType.DateTime, dataOra);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");

                    #endregion

                    try
                    {
                        reader.Read();
                        return reader.GetInt32(indiceIdCantiere);
                    }
                    catch
                    {
                        return -1;
                    }
                }
            }
        }

        public Int32 GetIdRilevatore(string codiceRilevatore, string ragioneSociale, DateTime dataOra)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriRilevatoreIdSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSociale);
                DatabaseCemi.AddInParameter(comando, "@dataOra", DbType.DateTime, dataOra);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdRilevatore = reader.GetOrdinal("idRilevatore");

                    #endregion

                    try
                    {
                        reader.Read();
                        return reader.GetInt32(indiceIdRilevatore);
                    }
                    catch
                    {
                        return -1;
                    }
                }
            }
        }


        private void InsertTimbratura(int idRilevatore, string codiceFiscale, DateTime data, int ingresso,
                                      string codiceFiscaleImpresa, string partitaIvaImpresa)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idRilevatore", DbType.Int32, idRilevatore);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@dataOra", DbType.DateTime, data);
                DatabaseCemi.AddInParameter(comando, "@ingressoUscita", DbType.Int32, ingresso);

                if (!string.IsNullOrEmpty(codiceFiscaleImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscaleImpresa", DbType.String, codiceFiscaleImpresa);
                }

                if (!string.IsNullOrEmpty(partitaIvaImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, partitaIvaImpresa);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public List<string> GetTipologieAttivita()
        {
            List<string> tipologieAttivita = new List<string>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptTipologieAttivitaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        tipologieAttivita.Add(reader.GetString(reader.GetOrdinal("descrizione")));
                    }
                }
            }

            return tipologieAttivita;
        }

        public Int32 InsertTimbrature(TimbraturaCollection timbrature)
        {
            Int32 ret = 0;


            foreach (Timbratura timb in timbrature)
            {
                int idRilevatore = GetIdRilevatore(timb.CodiceRilevatore, timb.RagioneSociale, timb.DataOra);
                int ingressoUscita = 0;
                if (timb.IngressoUscita)
                    ingressoUscita = 1;

                if (idRilevatore != -1)
                {
                    InsertTimbratura(idRilevatore, timb.CodiceFiscale, timb.DataOra, ingressoUscita,
                                     timb.CodiceFiscaleImpresa, timb.PartitaIvaImpresa);
                    //CEXChange
                    //if (!ControlliCexChangeControlloPresenzaCodiceFiscale(timb.CodiceFiscale, timb.IdCantiere.Value))
                    //{
                    //    Impresa impresa = GetLavoratoreAutonomoControlli(timb.IdCantiere.Value, timb.CodiceFiscale);
                    //    if (impresa == null)
                    //        ControlliCexChangeInsert(timb.CodiceFiscale, timb.IdCantiere.Value);
                    //}

                    //CEXChange New
                    Impresa impresa = GetLavoratoreAutonomoControlli(timb.IdCantiere.Value, timb.CodiceFiscale);
                    if (impresa == null)
                    {
                        ControlliCexChangeDenunceRichiesteInsertUnico(timb);
                    }

                    //if (!EsisteCodiceFiscaleINPS(timb.CodiceFiscale, timb.PartitaIVAImpresa))
                    //{
                    //    if (CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(timb.CodiceFiscale))
                    //    {
                    //        //if (CodiceFiscaleManager.GetDataSpedizioneTesseraSanitariaByCf(timb.CodiceFiscale).HasValue)

                    //        ControlliINPSInsert(timb.CodiceFiscale, timb.PartitaIVAImpresa);
                    //    }
                    //}

                    if (
                        !EsisteCodiceFiscaleInps2(timb.CodiceFiscale, timb.PartitaIvaImpresa, timb.DataOra.Year,
                                                  timb.DataOra.Month))
                    {
                        if (timb.CodiceFiscale.Length == 16)
                        {
                            if (CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(timb.CodiceFiscale))
                            {
                                //if (CodiceFiscaleManager.GetDataSpedizioneTesseraSanitariaByCf(timb.CodiceFiscale).HasValue)

                                ControlliInpsInsert2(timb.CodiceFiscale, timb.PartitaIvaImpresa, timb.DataOra.Year,
                                                     timb.DataOra.Month);
                            }
                        }
                    }
                }
                else
                    ret++;
            }

            return ret;
        }

        private Boolean ControlliCexChangeControlloPresenzaCodiceFiscale(string codiceFiscale, int idCantiere)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriControlliCexChangeSelectByCodFiscIdCantiere"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        //public Boolean EsisteCodiceFiscaleInps(string codiceFiscale, string partitaIvaImpresaCEMI)
        //{
        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSSelectByCodFisc"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, codiceFiscale);
        //        DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresaCEMI", DbType.String, partitaIvaImpresaCEMI);

        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            while (reader.Read())
        //            {
        //                return true;
        //            }
        //        }
        //    }

        //    return false;
        //}

        private Boolean EsisteCodiceFiscaleInps2(string codiceFiscale, string partitaIvaImpresa, int anno, int mese)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSSelectByCodFisc2"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, partitaIvaImpresa);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void ControlliCexChangeInsert(string codiceFiscale, int idCantiere)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliCexChangeInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        private void ControlliCexChangeDenunceRichiesteInsertUnico(Timbratura timbratura)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliCEXChangeDenunceRichiesteInsert"))
            {

                DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, timbratura.CodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, timbratura.DataOra.Year);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, timbratura.DataOra.Month);
                DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, timbratura.PartitaIvaImpresa);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        //public void ControlliINPSInsert(string codiceFiscale, string partitaIva)
        //{
        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSInsert"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
        //        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);

        //        DatabaseCemi.ExecuteNonQuery(comando);
        //    }
        //}

        private void ControlliInpsInsert2(string codiceFiscale, string partitaIva, int anno, int mese)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSInsert2"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.String, anno);
                DatabaseCemi.AddInParameter(comando, "@mese", DbType.String, mese);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        //public void ControlliINPSInsert2aux(string codiceFiscale, string partitaIva, int anno, int mese,
        //                                    Boolean? superato)
        //{
        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSInsert2aux"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
        //        DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);
        //        DatabaseCemi.AddInParameter(comando, "@anno", DbType.String, anno);
        //        DatabaseCemi.AddInParameter(comando, "@mese", DbType.String, mese);
        //        DatabaseCemi.AddInParameter(comando, "@superato", DbType.Boolean, superato);

        //        DatabaseCemi.ExecuteNonQuery(comando);
        //    }
        //}

        //public void tmpDELETE(string codiceFiscale)
        //{
        //    using (
        //        DbCommand comando =
        //            DatabaseCemi.GetStoredProcCommand("dbo._VALLATMPDELETE"))
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
        //        //DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);

        //        DatabaseCemi.ExecuteNonQuery(comando);
        //    }
        //}

        //public List<string> tmpSELECT()
        //{
        //    List<string> ret = new List<string>();
        //    using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo._VALLATMPSELECT"))
        //    {
        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            string codice = string.Empty;
        //            while (reader.Read())
        //            {
        //                ret.Add(reader.GetString(reader.GetOrdinal("codiceFiscalelavoratore")));
        //            }
        //        }
        //    }

        //    return ret;
        //}

        //public void tmpPULISCI()
        //{
        //    List<string> codiciFiscali = tmpSELECT();

        //    foreach (string codFisc in codiciFiscali)
        //    {
        //        //if (string.IsNullOrEmpty(codFisc) || codFisc.Length != 16)
        //        //{
        //        //    tmpDELETE(codFisc);
        //        //}

        //        if (!string.IsNullOrEmpty(codFisc) && codFisc.Length == 16)
        //        {
        //            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codFisc))
        //            {
        //                tmpDELETE(codFisc);
        //            }
        //        }
        //        else
        //        {
        //            tmpDELETE(codFisc);
        //        }

        //        //if (!string.IsNullOrEmpty(codFisc))
        //        //{
        //        //    if (codFisc.Length == 16)
        //        //    {
        //        //        if (CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codFisc))
        //        //        {
        //        //            if (!CodiceFiscaleManager.GetDataSpedizioneTesseraSanitariaByCf(codFisc).HasValue)

        //        //                tmpDELETE(codFisc);
        //        //        }
        //        //        else
        //        //        {
        //        //            tmpDELETE(codFisc);
        //        //        }
        //        //    }
        //        //}
        //    }
        //}


        //public List<string> ListaCodiciFiscali()
        //{
        //    List<string> ret = new List<string>();
        //    using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriListaCodiciFiscali"))
        //    {
        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            string codice = string.Empty;
        //            while (reader.Read())
        //            {
        //                ret.Add(reader.GetString(reader.GetOrdinal("codiceFiscale")));
        //            }
        //        }
        //    }

        //    return ret;
        //}

        //public void FillINPS()
        //{
        //    List<string> codiciFiscali = ListaCodiciFiscali();

        //    foreach (string codFisc in codiciFiscali)
        //    {
        //        if (!string.IsNullOrEmpty(codFisc))
        //        {
        //            if (codFisc.Length == 16)
        //            {
        //                if (!EsisteCodiceFiscaleINPS(codFisc))
        //                {
        //                    if (CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codFisc))
        //                    {
        //                        if (CodiceFiscaleManager.GetDataSpedizioneTesseraSanitariaByCf(codFisc).HasValue)
        //                            ControlliINPSInsert(codFisc);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion

        #region Trexom

        public Boolean? TrexomGetUltimoComandoWhiteList(String codiceRilevatore, String codiceFiscale)
        {
            Boolean? ultimoComando = null;

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TrexomWhiteListSelectUltimoComando"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(dbCommand, "@ultimoComando", DbType.String, 4);

                DatabaseCemi.ExecuteNonQuery(dbCommand);

                String ultimoComandoStr = dbCommand.Parameters["@ultimoComando"].Value as String;
                switch (ultimoComandoStr)
                {
                    case "0B":
                        ultimoComando = true;
                        break;
                    case "0C":
                        ultimoComando = false;
                        break;
                }
            }
            return ultimoComando;
        }

        public void TrexomInsertComandoWhiteList(String codiceRilevatore, String codiceFiscale, Boolean comando)
        {
            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TrexomWhiteListInsert"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(dbCommand, "@comando", DbType.Boolean, comando);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public void TrexomInsertDeleteWhiteList(String codiceRilevatore)
        {
            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TrexomWhiteListDelete"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codiceRilevatore", DbType.String, codiceRilevatore);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        #endregion

        public LavoratoreCollection GetLavoratoriRicerca(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriSubappaltiSelectLavoratori"))
            {
                DatabaseCemi.AddInParameter(comando, "@idWhiteList", DbType.Int32, filtro.IdCantiere);
                if (!String.IsNullOrWhiteSpace(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!String.IsNullOrWhiteSpace(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (!String.IsNullOrWhiteSpace(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici
                    Int32 indiceTipoLavoratore = reader.GetOrdinal("tipoLavoratore");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataInizioAttivita = reader.GetOrdinal("dataInizioAttivita");
                    Int32 indiceDataFineAttivita = reader.GetOrdinal("dataFineAttivita");
                    Int32 indiceTipoImpresa = reader.GetOrdinal("tipoImpresa");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceImpCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lav = new Lavoratore();
                        lavoratori.Add(lav);

                        lav.TipoLavoratore = (TipologiaLavoratore) reader.GetInt32(indiceTipoLavoratore);
                        lav.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            lav.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lav.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lav.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lav.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceDataInizioAttivita))
                        {
                            lav.DataInizioAttivita = reader.GetDateTime(indiceDataInizioAttivita);
                        }
                        if (!reader.IsDBNull(indiceDataFineAttivita))
                        {
                            lav.DataFineAttivita = reader.GetDateTime(indiceDataFineAttivita);
                        }

                        lav.Impresa = new Impresa();
                        lav.Impresa.TipoImpresa = (TipologiaImpresa) reader.GetInt32(indiceTipoImpresa);
                        lav.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);

                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            lav.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            lav.Impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceImpCodiceFiscale))
                        {
                            lav.Impresa.CodiceFiscale = reader.GetString(indiceImpCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public Boolean CheckCodiceControlloPerImportAutomatico(Int32 idCantiere, Guid controlCode)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_AccessoCantieriCheckControlloCodice"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, idCantiere);
                DatabaseCemi.AddInParameter(comando, "@guid", DbType.Guid, controlCode);
                DatabaseCemi.AddOutParameter(comando, "@cantieriTrovati", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);
                Int32 cantieriTrovati = (Int32) DatabaseCemi.GetParameterValue(comando, "@cantieriTrovati");

                if (cantieriTrovati == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public void InsertLogWSImprese(String codiceRilevatore, String ragioneSociale, String codiceFiscale, String partitaIva, String errore)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWSLogImpreseInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSociale);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);
                DatabaseCemi.AddInParameter(comando, "@errore", DbType.String, errore);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void InsertLogWSLavoratore(String codiceRilevatore, String cognome, String nome, String codiceFiscale, String errore)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWSLogLavoratoriInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceRilevatore", DbType.String, codiceRilevatore);
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@errore", DbType.String, errore);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public List<Presenze> GetPresenze(PresenzeFilter filtro)
        {
            List<Presenze> presenze = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelectAggregate"))
            {
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filtro.IdCantiere);
                if (!String.IsNullOrWhiteSpace(filtro.PartitaIvaImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, filtro.PartitaIvaImpresa);
                }
                if (!String.IsNullOrWhiteSpace(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici
                    Int32 indiceAnno = reader.GetOrdinal("anno");
                    Int32 indiceMese = reader.GetOrdinal("mese");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataPrimoAccesso = reader.GetOrdinal("dataPrimoAccesso");
                    Int32 indiceDataUltimoAccesso = reader.GetOrdinal("dataUltimoAccesso");
                    Int32 indiceNumeroPresenze = reader.GetOrdinal("numeroPresenze");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    #endregion

                    presenze = new List<Presenze>();

                    while (reader.Read())
                    {
                        Presenze pres = new Presenze();
                        presenze.Add(pres);

                        pres.Anno = reader.GetInt32(indiceAnno);
                        pres.Mese = reader.GetInt32(indiceMese);

                        pres.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            pres.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            pres.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            pres.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        pres.DataPrimoAccesso = reader.GetDateTime(indiceDataPrimoAccesso);
                        pres.DataUltimoAccesso = reader.GetDateTime(indiceDataUltimoAccesso);
                        pres.NumeroPresenze = reader.GetInt32(indiceNumeroPresenze);
                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            pres.ImpresaRagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            pres.ImpresaPartitaIva = reader.GetString(indicePartitaIva);
                        }
                    }
                }
            }

            return presenze;
        }

        public List<Impresa> GetPresenzeImprese(PresenzeFilter filtro)
        {
            List<Impresa> imprese = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriTimbratureSelectAggregateImprese"))
            {
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filtro.IdCantiere);
                if (!String.IsNullOrWhiteSpace(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    #endregion

                    imprese = new List<Impresa>();

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                    }
                }
            }

            return imprese;
        }

        public ControlloIdentitaCexChangeCollection GetControlloIdentitaCexchange(ControlloIdentitaFilter filter)
        {
            ControlloIdentitaCexChangeCollection controlloIdentitaColl = new ControlloIdentitaCexChangeCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliCEMISelect")
                )
            {
                if (filter.IdCantiere.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, filter.IdCantiere);

                if (filter.Mese.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, filter.Mese.Value);
                if (filter.Anno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, filter.Anno.Value);
                if (filter.IdImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filter.IdImpresa.Value);
                if (!string.IsNullOrEmpty(filter.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filter.CodiceFiscale);
                if (filter.TipologiaAnomaliaControlli.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoAnomalia", DbType.Int32,
                                                filter.TipologiaAnomaliaControlli.Value);
                if (filter.TipologiaRuoloTimbratura.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoUtente", DbType.Int32,
                                                filter.TipologiaRuoloTimbratura.Value);
                if (!string.IsNullOrEmpty(filter.PartitaIVA))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, filter.PartitaIVA);

                //comando.CommandTimeout = 60;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    Int32 indiceMese = reader.GetOrdinal("mese");
                    Int32 indiceAnno = reader.GetOrdinal("anno");

                    Int32 indiceControlliEffettuati = reader.GetOrdinal("controlliEffettuati");
                    Int32 indiceControlloDenunciaSuperato = reader.GetOrdinal("controlloDenunciaSuperato");
                    Int32 indiceControlloLavoratoreDenunciaSuperato =
                        reader.GetOrdinal("controlloLavoratoreDenunciaSuperato");
                    Int32 indiceControlloOreDenunciaSuperato = reader.GetOrdinal("controlloOreDenunciaSuperato");
                    Int32 indiceControlloDebitiSuperato = reader.GetOrdinal("controlloDebitiSuperato");
                    Int32 indiceControlloDebitiEffettuato = reader.GetOrdinal("controlloDebitiEffettuato");

                    //Int32 indiceDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");
                    // Int32 indiceNomeCassaEdileUltimaDenuncia = reader.GetOrdinal("nomeCassaEdileUltimaDenuncia");
                    //Int32 indiceCexchangeCassaEdileDescrizione = reader.GetOrdinal("cexchangeCassaEdileDescrizione");
                    //Int32 indiceCexchangeCassaEdileId = reader.GetOrdinal("cexchangeCassaEdileId");
                    //Int32 indiceCexchangeOreOrdinarie = reader.GetOrdinal("cexchangeCassaEdileOreOrdinarie");

                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIvaImpresa");

                    #endregion

                    ControlloIdentitaCexChange controlloIdentita = null;

                    while (reader.Read())
                    {
                        controlloIdentita = new ControlloIdentitaCexChange();

                        if (!reader.IsDBNull(indiceIdCantiere))
                            controlloIdentita.IdCantiere = reader.GetInt32(indiceIdCantiere);

                        controlloIdentita.CodiceFiscaleLavoratore = reader.GetString(indiceCodiceFiscale);

                        controlloIdentita.Mese = reader.GetInt32(indiceMese);
                        controlloIdentita.Anno = reader.GetInt32(indiceAnno);

                        //controlloIdentita.ControlliEffettuati = reader.GetBoolean(indiceControlliEffettuati);
                        //controlloIdentita.ControlloDenunciaSuperato = reader.GetBoolean(indiceControlloDenunciaSuperato);
                        //controlloIdentita.ControlloLavoratoreDenunciaSuperato =
                        //    reader.GetBoolean(indiceControlloLavoratoreDenunciaSuperato);
                        //controlloIdentita.ControlloOreDenunciaSuperato =
                        //    reader.GetBoolean(indiceControlloOreDenunciaSuperato);
                        //controlloIdentita.ControlloDebitiSuperato = reader.GetBoolean(indiceControlloDebitiSuperato);
                        //controlloIdentita.ControlloDebitiEffettuato = reader.GetBoolean(indiceControlloDebitiEffettuato);

                        if (!reader.IsDBNull(indicePartitaIva))
                            controlloIdentita.PartitaIvaImpresa = reader.GetString(indicePartitaIva);


                        controlloIdentita.ControlliDenunceCexChange = ControlliDenunceCexChangeSelect(controlloIdentita.CodiceFiscaleLavoratore,
                                                                                                        controlloIdentita.PartitaIvaImpresa,
                                                                                                        controlloIdentita.Anno,
                                                                                                        controlloIdentita.Mese);

                        //INPS
                        using (
                                DbCommand comando2 =
                                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriControlliINPSSelect2"))
                        {
                            DatabaseCemi.AddInParameter(comando2, "@codiceFiscale", DbType.String,
                                                        controlloIdentita.CodiceFiscaleLavoratore);

                            using (IDataReader reader2 = DatabaseCemi.ExecuteReader(comando2))
                            {
                                Int32 indiceSuperato = reader2.GetOrdinal("superato");
                                Int32 indicePartitaIvaImpresa = reader2.GetOrdinal("partitaIVAImpresa");
                                Int32 indiceAnnoInps = reader2.GetOrdinal("anno");
                                Int32 indiceMeseInps = reader2.GetOrdinal("mese");

                                controlloIdentita.ContribuzioniInps = new ContribuzioneInps2Collection();
                                while (reader2.Read())
                                {
                                    ContribuzioneINPS2 cont = new ContribuzioneINPS2();

                                    if (!reader2.IsDBNull(indiceAnnoInps))
                                        cont.Anno =
                                            reader2.GetInt32(indiceAnnoInps);

                                    if (!reader2.IsDBNull(indiceMeseInps))
                                        cont.Mese =
                                            reader2.GetInt32(indiceMeseInps);

                                    if (!reader2.IsDBNull(indiceSuperato))
                                        cont.Superato =
                                            reader2.GetBoolean(indiceSuperato);

                                    if (!reader2.IsDBNull(indicePartitaIvaImpresa))
                                        cont.PartitaIVAImpresa =
                                            reader2.GetString(indicePartitaIvaImpresa);

                                    controlloIdentita.ContribuzioniInps.Add(cont);
                                }
                            }
                        }



                        controlloIdentitaColl.Add(controlloIdentita);
                    }
                }
            }

            return controlloIdentitaColl;
        }

        //private ControlloDenunciaOreCexChangeCollection ControlliDenunceCexChangeSelectOLD(String codiceFiscaleLavoratore, String partitaIvaImpresa, Int32 periodoAnno, Int32 periodoMese)
        //{
        //    ControlloDenunciaOreCexChangeCollection controlli = new ControlloDenunciaOreCexChangeCollection();

        //    if (!String.IsNullOrEmpty(partitaIvaImpresa))
        //    {
        //        #region Denunce complete
        //        using (
        //           DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_AccessoCantieriControlliCEXChangeDenunceRisposteSelectComplete]")
        //           )
        //        {
        //            DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, codiceFiscaleLavoratore);
        //            DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, partitaIvaImpresa);
        //            DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, periodoMese);
        //            DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, periodoAnno);

        //            using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //            {
        //                #region indexes
        //                Int32 indexDenunciaCodiceCassaEdile = reader.GetOrdinal("denunciaCodiceCassaEdile");
        //                Int32 indexDenunciaDescCassaEdile = reader.GetOrdinal("denunciaDescCassaEdile");
        //                Int32 indexDenunciaCodiceCELavoratore = reader.GetOrdinal("denunciaCodiceCELavoratore");
        //                Int32 indexDenuciaCodiceCEImpresa = reader.GetOrdinal("denuciaCodiceCEImpresa");
        //                Int32 indexPeriodoAnno = reader.GetOrdinal("periodoAnno");
        //                Int32 indexPeriodoMese = reader.GetOrdinal("periodoMese");
        //                Int32 indexOreOrdinarie = reader.GetOrdinal("oreOrdinarie");
        //                Int32 indexImponibileGNF = reader.GetOrdinal("imponibileGNF");

        //                Int32 indexImpPosizioneBni = reader.GetOrdinal("posizioneBNI");
        //                Int32 indexImpPresenzaDenunce = reader.GetOrdinal("impresaPresenzaDenunce");
        //                Int32 indexImpCassaEdileId = reader.GetOrdinal("impresaIdCassaEdile");
        //                Int32 indexImpCassaEdileDesc = reader.GetOrdinal("impresaDescCassaEdile");
        //                Int32 indexImpCodiceCE = reader.GetOrdinal("impresaCodiceCE");
        //                Int32 indexImpCodFisc = reader.GetOrdinal("impresaCodiceFiscale");
        //                Int32 indexImpParitaIva = reader.GetOrdinal("partitaIva");
        //                Int32 indexImpDataCessazione = reader.GetOrdinal("impresadataCessazione");
        //                Int32 indexImpDataDecorrenza = reader.GetOrdinal("impresadataDecorrenza");
        //                Int32 indexImpDataIscrizione = reader.GetOrdinal("impresadataIscrizione");
        //                Int32 indexImpNatura = reader.GetOrdinal("natura");
        //                Int32 indexImpRagSoc = reader.GetOrdinal("ragioneSociale");
        //                Int32 indexImpStato = reader.GetOrdinal("stato");
        //                Int32 indexImpUltimaDenunciaAnno = reader.GetOrdinal("impresaUltimaDenunciaAnno");
        //                Int32 indexImpUltimaDenunciaMese = reader.GetOrdinal("impresaUltimaDenunciaMese");

        //                Int32 indexLavCassaEdileId = reader.GetOrdinal("lavoratoreIdCassaEdile");
        //                Int32 indexLavCassaEdileDesc = reader.GetOrdinal("lavoratoreDescCassaEdile");
        //                Int32 indexLavCodiceCE = reader.GetOrdinal("lavoratoreCodiceCE");
        //                Int32 indexLavCodFisc = reader.GetOrdinal("lavoratoreCodiceFiscale");
        //                Int32 indexLavCognome = reader.GetOrdinal("cognome");
        //                Int32 indexLavNome = reader.GetOrdinal("nome");
        //                Int32 indexLavDataNascita = reader.GetOrdinal("dataNascita");
        //                Int32 indexLavLuogoNascita = reader.GetOrdinal("luogoNascita");
        //                Int32 indexLavNazioneNascita = reader.GetOrdinal("nazioneNascita");
        //                Int32 indexLavProvNascita = reader.GetOrdinal("provinciaNascita");
        //                Int32 indexLavSesso = reader.GetOrdinal("sesso");
        //                Int32 indexLavUltimaDenunciaAnno = reader.GetOrdinal("lavoratoreUltimaDenunciaAnno");
        //                Int32 indexLavUltimaDenunciaMese = reader.GetOrdinal("lavoratoreUltimaDenunciaMese");
        //                #endregion

        //                ControlloDenunciaOreCexChange controllo;
        //                ImpresaCexChange impresa = null;
        //                LavoratoreCexChange lavoratore = null;
        //                DenunciaOreCexChange denuncia = null;
        //                String cassaEdileCodice = null;
        //                String cassaEdileDescrizone = null;
        //                while (reader.Read())
        //                {
        //                    //  cassaEdileCodice = reader.GetString(indexDenunciaCodiceCassaEdile);
        //                    //  cassaEdileDescrizone = reader.IsDBNull(indexDenunciaDescCassaEdile) ? null : reader.GetString(indexDenunciaDescCassaEdile);

        //                    controllo = new ControlloDenunciaOreCexChange();
        //                    controllo.PeriodoAnno = periodoAnno;
        //                    controllo.PeriodoMese = periodoMese;
        //                    controllo.ImpresaPresenzaDenunceInPeriodo = reader.GetBoolean(indexImpPresenzaDenunce);
        //                    //controllo.CassaEdileCodice = cassaEdileCodice;
        //                    //controllo.CassaEdileDescrizione = cassaEdileDescrizone;

        //                    if (!reader.IsDBNull(indexDenunciaCodiceCassaEdile))
        //                    {
        //                        cassaEdileCodice = reader.GetString(indexDenunciaCodiceCassaEdile);
        //                        cassaEdileDescrizone = reader.IsDBNull(indexDenunciaDescCassaEdile) ? null : reader.GetString(indexDenunciaDescCassaEdile);

        //                        denuncia = new DenunciaOreCexChange();
        //                        denuncia.CassaEdileCodice = cassaEdileCodice;
        //                        denuncia.CassaEdileDescrizione = cassaEdileDescrizone;
        //                        denuncia.CodiceCEImpresa = reader.GetString(indexDenuciaCodiceCEImpresa);
        //                        denuncia.CodiceCELavoratore = reader.GetString(indexDenunciaCodiceCELavoratore);
        //                        denuncia.PeriodoAnno = periodoAnno;
        //                        denuncia.PeriodoMese = periodoMese;
        //                        denuncia.ImponibileGNF = reader.GetDecimal(indexImponibileGNF);
        //                        denuncia.OreOrdinarie = reader.GetDecimal(indexOreOrdinarie);
        //                        controllo.DeunciaOre = denuncia;
        //                    }

        //                    if (!reader.IsDBNull(indexLavCodiceCE))
        //                    {
        //                        cassaEdileCodice = reader.GetString(indexLavCassaEdileId);
        //                        cassaEdileDescrizone = reader.IsDBNull(indexLavCassaEdileDesc) ? null : reader.GetString(indexLavCassaEdileDesc);


        //                        lavoratore = new LavoratoreCexChange();
        //                        controllo.Lavoratore = lavoratore;
        //                        lavoratore.CassaEdileCodice = cassaEdileCodice;
        //                        lavoratore.CassaEdileDescrizione = cassaEdileDescrizone;
        //                        lavoratore.CodiceCELavoratore = reader.GetString(indexLavCodiceCE);
        //                        lavoratore.CodiceFiscale = reader.GetString(indexLavCodFisc);
        //                        lavoratore.Cognome = reader.GetString(indexLavCognome);
        //                        lavoratore.DataNascita = reader.IsDBNull(indexLavDataNascita) ? (DateTime?)null : reader.GetDateTime(indexLavDataNascita);
        //                        lavoratore.LuogoNascita = reader.GetString(indexLavLuogoNascita);
        //                        lavoratore.NazioneNascita = reader.GetString(indexLavNazioneNascita);
        //                        lavoratore.Nome = reader.GetString(indexLavNome);
        //                        lavoratore.ProvinciaNascita = reader.GetString(indexLavProvNascita);
        //                        lavoratore.Sesso = reader.GetString(indexLavSesso);
        //                        lavoratore.UltimaDenunciaAnno = reader.IsDBNull(indexLavUltimaDenunciaAnno) ? (int?)null : reader.GetInt32(indexLavUltimaDenunciaAnno);
        //                        lavoratore.UltimaDenunciaMese = reader.IsDBNull(indexLavUltimaDenunciaMese) ? (int?)null : reader.GetInt32(indexLavUltimaDenunciaMese);
        //                    }

        //                    if (!reader.IsDBNull(indexImpCodiceCE))
        //                    {
        //                        cassaEdileCodice = reader.GetString(indexImpCassaEdileId);
        //                        cassaEdileDescrizone = reader.IsDBNull(indexImpCassaEdileDesc) ? null : reader.GetString(indexImpCassaEdileDesc);

        //                        impresa = new ImpresaCexChange();
        //                        controllo.Impresa = impresa;
        //                        impresa.CassaEdileCodice = cassaEdileCodice;
        //                        impresa.CassaEdileDescrizione = cassaEdileDescrizone;
        //                        impresa.CodiceCEImpresa = reader.GetString(indexImpCodiceCE);
        //                        impresa.CodiceFiscale = reader.GetString(indexImpCodFisc);
        //                        impresa.DataCessazione = reader.IsDBNull(indexImpDataCessazione) ? (DateTime?)null : reader.GetDateTime(indexImpDataCessazione);
        //                        impresa.DataDecorrenza = reader.IsDBNull(indexImpDataDecorrenza) ? (DateTime?)null : reader.GetDateTime(indexImpDataDecorrenza);
        //                        impresa.DataIscrizione = reader.IsDBNull(indexImpDataIscrizione) ? (DateTime?)null : reader.GetDateTime(indexImpDataIscrizione);
        //                        impresa.Natura = reader.GetString(indexImpNatura);
        //                        impresa.PartitaIva = reader.GetString(indexImpParitaIva);
        //                        impresa.RagioneSociale = reader.GetString(indexImpRagSoc);
        //                        impresa.Stato = reader.GetString(indexImpStato);
        //                        impresa.UltimaDenunciaAnno = reader.IsDBNull(indexImpUltimaDenunciaAnno) ? (int?)null : reader.GetInt32(indexImpUltimaDenunciaAnno);
        //                        impresa.UltimaDenunciaMese = reader.IsDBNull(indexImpUltimaDenunciaMese) ? (int?)null : reader.GetInt32(indexImpUltimaDenunciaMese);
        //                        impresa.PosizioneBNI = reader.IsDBNull(indexImpPosizioneBni) ? null : reader.GetString(indexImpPosizioneBni);
        //                    }


        //                    controllo.CassaEdileCodice = cassaEdileCodice;
        //                    controllo.CassaEdileDescrizione = cassaEdileDescrizone;
        //                    controlli.Add(controllo);

        //                }

        //            }

        //        }
        //        #endregion

        //        #region Imprese

        //        using (
        //            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_AccessoCantieriControlliCEXChangeImpreseSelect]")
        //            )
        //        {  
        //            DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIvaImpresa);
        //            DatabaseCemi.AddInParameter(comando, "@mesePresenzaDenunce", DbType.String, periodoMese);
        //            DatabaseCemi.AddInParameter(comando, "@annoPresenzaDenunce", DbType.String, periodoAnno);

        //            using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //            {
        //                #region indexes

        //                Int32 indexImpPosizioneBni = reader.GetOrdinal("posizioneBNI");
        //                Int32 indexImpPresenzaDenunce = reader.GetOrdinal("impresaPresenzaDenunce");
        //                Int32 indexImpCassaEdileId = reader.GetOrdinal("impresaIdCassaEdile");
        //                Int32 indexImpCassaEdileDesc = reader.GetOrdinal("impresaDescCassaEdile");
        //                Int32 indexImpCodiceCE = reader.GetOrdinal("impresaCodiceCE");
        //                Int32 indexImpCodFisc = reader.GetOrdinal("impresaCodiceFiscale");
        //                Int32 indexImpParitaIva = reader.GetOrdinal("partitaIva");
        //                Int32 indexImpDataCessazione = reader.GetOrdinal("impresadataCessazione");
        //                Int32 indexImpDataDecorrenza = reader.GetOrdinal("impresadataDecorrenza");
        //                Int32 indexImpDataIscrizione = reader.GetOrdinal("impresadataIscrizione");
        //                Int32 indexImpNatura = reader.GetOrdinal("natura");
        //                Int32 indexImpRagSoc = reader.GetOrdinal("ragioneSociale");
        //                Int32 indexImpStato = reader.GetOrdinal("stato");
        //                Int32 indexImpUltimaDenunciaAnno = reader.GetOrdinal("impresaUltimaDenunciaAnno");
        //                Int32 indexImpUltimaDenunciaMese = reader.GetOrdinal("impresaUltimaDenunciaMese");


        //                #endregion

        //                ControlloDenunciaOreCexChange controllo;
        //                ImpresaCexChange impresa = null;
        //               // LavoratoreCexChange lavoratore = null;
        //               // DenunciaOreCexChange denuncia = null;
        //                String cassaEdileCodice = null;
        //                String cassaEdileDescrizone = null;
        //                while (reader.Read())
        //                {
        //                    cassaEdileCodice = reader.GetString(indexImpCassaEdileId);
        //                    cassaEdileDescrizone = reader.IsDBNull(indexImpCassaEdileDesc) ? null : reader.GetString(indexImpCassaEdileDesc);

        //                    if (controlli.FirstOrDefault(x => x.CassaEdileCodice == cassaEdileCodice) != null)
        //                    {
        //                        continue;
        //                    }

        //                    controllo = new ControlloDenunciaOreCexChange();
        //                    controllo.PeriodoAnno = periodoAnno;
        //                    controllo.PeriodoMese = periodoMese;
        //                    controllo.CassaEdileCodice = cassaEdileCodice;
        //                    controllo.CassaEdileDescrizione = cassaEdileDescrizone;
        //                    controllo.ImpresaPresenzaDenunceInPeriodo = reader.GetBoolean(indexImpPresenzaDenunce);

        //                    if (!reader.IsDBNull(indexImpCodiceCE))
        //                    {

        //                        impresa = new ImpresaCexChange();
        //                        controllo.Impresa = impresa;
        //                        impresa.CassaEdileCodice = cassaEdileCodice;
        //                        impresa.CassaEdileDescrizione = cassaEdileDescrizone;
        //                        impresa.CodiceCEImpresa = reader.GetString(indexImpCodiceCE);
        //                        impresa.CodiceFiscale = reader.GetString(indexImpCodFisc);
        //                        impresa.DataCessazione = reader.IsDBNull(indexImpDataCessazione) ? (DateTime?)null : reader.GetDateTime(indexImpDataCessazione);
        //                        impresa.DataDecorrenza = reader.IsDBNull(indexImpDataDecorrenza) ? (DateTime?)null : reader.GetDateTime(indexImpDataDecorrenza);
        //                        impresa.DataIscrizione = reader.IsDBNull(indexImpDataIscrizione) ? (DateTime?)null : reader.GetDateTime(indexImpDataIscrizione);
        //                        impresa.Natura = reader.GetString(indexImpNatura);
        //                        impresa.PartitaIva = reader.GetString(indexImpParitaIva);
        //                        impresa.RagioneSociale = reader.GetString(indexImpRagSoc);
        //                        impresa.Stato = reader.GetString(indexImpStato);
        //                        impresa.UltimaDenunciaAnno = reader.IsDBNull(indexImpUltimaDenunciaAnno) ? (int?)null : reader.GetInt32(indexImpUltimaDenunciaAnno);
        //                        impresa.UltimaDenunciaMese = reader.IsDBNull(indexImpUltimaDenunciaMese) ? (int?)null : reader.GetInt32(indexImpUltimaDenunciaMese);
        //                        impresa.PosizioneBNI = reader.IsDBNull(indexImpPosizioneBni) ? null : reader.GetString(indexImpPosizioneBni);
        //                    }



        //                    controlli.Add(controllo);

        //                }

        //            }

        //        }

        //        #endregion
        //    }


        //    #region Lavoratori
        //    using (
        //            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_AccessoCantieriControlliCEXChangeLavoratoriSelect]")
        //        )
        //    {
        //        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscaleLavoratore);

        //        using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
        //        {
        //            #region indexes

        //            Int32 indexLavCassaEdileId = reader.GetOrdinal("lavoratoreIdCassaEdile");
        //            Int32 indexLavCassaEdileDesc = reader.GetOrdinal("lavoratoreDescCassaEdile");
        //            Int32 indexLavCodiceCE = reader.GetOrdinal("lavoratoreCodiceCE");
        //            Int32 indexLavCodFisc = reader.GetOrdinal("lavoratoreCodiceFiscale");
        //            Int32 indexLavCognome = reader.GetOrdinal("cognome");
        //            Int32 indexLavNome = reader.GetOrdinal("nome");
        //            Int32 indexLavDataNascita = reader.GetOrdinal("dataNascita");
        //            Int32 indexLavLuogoNascita = reader.GetOrdinal("luogoNascita");
        //            Int32 indexLavNazioneNascita = reader.GetOrdinal("nazioneNascita");
        //            Int32 indexLavProvNascita = reader.GetOrdinal("provinciaNascita");
        //            Int32 indexLavSesso = reader.GetOrdinal("sesso");
        //            Int32 indexLavUltimaDenunciaAnno = reader.GetOrdinal("lavoratoreUltimaDenunciaAnno");
        //            Int32 indexLavUltimaDenunciaMese = reader.GetOrdinal("lavoratoreUltimaDenunciaMese");
        //            #endregion

        //            ControlloDenunciaOreCexChange controllo;
        //            //ImpresaCexChange impresa = null;
        //            LavoratoreCexChange lavoratore = null;
        //            ///DenunciaOreCexChange denuncia = null;
        //            String cassaEdileCodice = null;
        //            String cassaEdileDescrizone = null;
        //            while (reader.Read())
        //            {
        //                cassaEdileCodice = reader.GetString(indexLavCassaEdileId);
        //                cassaEdileDescrizone = reader.IsDBNull(indexLavCassaEdileDesc) ? null : reader.GetString(indexLavCassaEdileDesc);

        //                if (!reader.IsDBNull(indexLavCodiceCE))
        //                {

        //                    lavoratore = new LavoratoreCexChange();
        //                    lavoratore.CassaEdileCodice = cassaEdileCodice;
        //                    lavoratore.CassaEdileDescrizione = cassaEdileDescrizone;
        //                    lavoratore.CodiceCELavoratore = reader.GetString(indexLavCodiceCE);
        //                    lavoratore.CodiceFiscale = reader.GetString(indexLavCodFisc);
        //                    lavoratore.Cognome = reader.GetString(indexLavCognome);
        //                    lavoratore.DataNascita = reader.IsDBNull(indexLavDataNascita) ? (DateTime?)null : reader.GetDateTime(indexLavDataNascita);
        //                    lavoratore.LuogoNascita = reader.GetString(indexLavLuogoNascita);
        //                    lavoratore.NazioneNascita = reader.GetString(indexLavNazioneNascita);
        //                    lavoratore.Nome = reader.GetString(indexLavNome);
        //                    lavoratore.ProvinciaNascita = reader.GetString(indexLavProvNascita);
        //                    lavoratore.Sesso = reader.GetString(indexLavSesso);
        //                    lavoratore.UltimaDenunciaAnno = reader.IsDBNull(indexLavUltimaDenunciaAnno) ? (int?)null : reader.GetInt32(indexLavUltimaDenunciaAnno);
        //                    lavoratore.UltimaDenunciaMese = reader.IsDBNull(indexLavUltimaDenunciaMese) ? (int?)null : reader.GetInt32(indexLavUltimaDenunciaMese);
        //                }


        //                ControlloDenunciaOreCexChange controlloEsistente = controlli.FirstOrDefault(x => x.CassaEdileCodice == cassaEdileCodice
        //                                                                                                );
        //                if (controlloEsistente == null)
        //                {
        //                    controllo = new ControlloDenunciaOreCexChange();
        //                    controllo.PeriodoAnno = periodoAnno;
        //                    controllo.PeriodoMese = periodoMese;
        //                    controllo.CassaEdileCodice = cassaEdileCodice;
        //                    controllo.CassaEdileDescrizione = cassaEdileDescrizone;
        //                    controllo.Lavoratore = lavoratore;
        //                    controlli.Add(controllo);
        //                }
        //                else if (controlloEsistente.Lavoratore == null)
        //                {
        //                    controlloEsistente.Lavoratore = lavoratore;
        //                }



        //            }

        //        }

        //    }
        //    #endregion

        //    return controlli;
        //}


        private ControlloDenunciaOreCexChangeCollection ControlliDenunceCexChangeSelect(String codiceFiscaleLavoratore, String partitaIvaImpresa, Int32 periodoAnno, Int32 periodoMese)
        {
            ControlloDenunciaOreCexChangeCollection controlli = new ControlloDenunciaOreCexChangeCollection();

            if (!String.IsNullOrEmpty(partitaIvaImpresa))
            {
                #region Denunce complete
                using (
                   DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_AccessoCantieriControlliCEXChangeDenunceImpreseLavoratoriSelect]")
                   )
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, codiceFiscaleLavoratore);
                    DatabaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, partitaIvaImpresa);
                    DatabaseCemi.AddInParameter(comando, "@mese", DbType.Int32, periodoMese);
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, periodoAnno);

                    using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                    {
                        #region indexes
                        Int32 indexIdDenunciaRisposta = reader.GetOrdinal("idRisposta");
                        Int32 indexCodiceCassaEdile = reader.GetOrdinal("codiceCassaEdile");
                        Int32 indexDescCassaEdile = reader.GetOrdinal("descrizioneCassaEdile");
                        Int32 indexDenunciaCodiceCELavoratore = reader.GetOrdinal("denunciaCodiceCELavoratore");
                        Int32 indexDenuciaCodiceCEImpresa = reader.GetOrdinal("denuciaCodiceCEImpresa");
                        Int32 indexPeriodoAnno = reader.GetOrdinal("periodoAnno");
                        Int32 indexPeriodoMese = reader.GetOrdinal("periodoMese");
                        Int32 indexOreOrdinarie = reader.GetOrdinal("oreOrdinarie");
                        Int32 indexImponibileGNF = reader.GetOrdinal("imponibileGNF");

                        Int32 indexImpPosizioneBni = reader.GetOrdinal("posizioneBNI");
                        Int32 indexImpPresenzaDenunce = reader.GetOrdinal("impresaPresenzaDenunce");
                        //Int32 indexImpCassaEdileId = reader.GetOrdinal("impresaIdCassaEdile");
                        //Int32 indexImpCassaEdileDesc = reader.GetOrdinal("impresaDescCassaEdile");
                        Int32 indexImpCodiceCE = reader.GetOrdinal("impresaCodiceCE");
                        Int32 indexImpCodFisc = reader.GetOrdinal("impresaCodiceFiscale");
                        Int32 indexImpParitaIva = reader.GetOrdinal("partitaIva");
                        Int32 indexImpDataCessazione = reader.GetOrdinal("impresadataCessazione");
                        Int32 indexImpDataDecorrenza = reader.GetOrdinal("impresadataDecorrenza");
                        Int32 indexImpDataIscrizione = reader.GetOrdinal("impresadataIscrizione");
                        Int32 indexImpNatura = reader.GetOrdinal("natura");
                        Int32 indexImpRagSoc = reader.GetOrdinal("ragioneSociale");
                        Int32 indexImpStato = reader.GetOrdinal("stato");
                        Int32 indexImpUltimaDenunciaAnno = reader.GetOrdinal("impresaUltimaDenunciaAnno");
                        Int32 indexImpUltimaDenunciaMese = reader.GetOrdinal("impresaUltimaDenunciaMese");

                        //Int32 indexLavCassaEdileId = reader.GetOrdinal("lavoratoreIdCassaEdile");
                        //Int32 indexLavCassaEdileDesc = reader.GetOrdinal("lavoratoreDescCassaEdile");
                        Int32 indexLavCodiceCE = reader.GetOrdinal("lavoratoreCodiceCE");
                        Int32 indexLavCodFisc = reader.GetOrdinal("lavoratoreCodiceFiscale");
                        Int32 indexLavCognome = reader.GetOrdinal("cognome");
                        Int32 indexLavNome = reader.GetOrdinal("nome");
                        Int32 indexLavDataNascita = reader.GetOrdinal("dataNascita");
                        Int32 indexLavLuogoNascita = reader.GetOrdinal("luogoNascita");
                        Int32 indexLavNazioneNascita = reader.GetOrdinal("nazioneNascita");
                        Int32 indexLavProvNascita = reader.GetOrdinal("provinciaNascita");
                        Int32 indexLavSesso = reader.GetOrdinal("sesso");
                        Int32 indexLavUltimaDenunciaAnno = reader.GetOrdinal("lavoratoreUltimaDenunciaAnno");
                        Int32 indexLavUltimaDenunciaMese = reader.GetOrdinal("lavoratoreUltimaDenunciaMese");

                        Int32 indexEsitoVersamentoCodice = reader.GetOrdinal("esitoVersamentoCodice");
                        Int32 indexEsitoVersamentoDescrizione = reader.GetOrdinal("esitoVersamentoDescrizione");
                        Int32 indexEsitoVersamentoId = reader.GetOrdinal("esitoVersamentoId");
                        #endregion

                        ControlloDenunciaOreCexChange controllo;
                        ImpresaCexChange impresa = null;
                        LavoratoreCexChange lavoratore = null;
                        DenunciaOreCexChange denuncia = null;
                        String cassaEdileCodice = null;
                        String cassaEdileDescrizone = null;
                        while (reader.Read())
                        {
                            cassaEdileCodice = reader.GetString(indexCodiceCassaEdile);
                            cassaEdileDescrizone = reader.IsDBNull(indexDescCassaEdile) ? null : reader.GetString(indexDescCassaEdile);

                            controllo = new ControlloDenunciaOreCexChange();
                            controllo.PeriodoAnno = periodoAnno;
                            controllo.PeriodoMese = periodoMese;
                            controllo.ImpresaPresenzaDenunceInPeriodo = reader.GetBoolean(indexImpPresenzaDenunce);
                            controllo.CassaEdileCodice = cassaEdileCodice;
                            controllo.CassaEdileDescrizione = cassaEdileDescrizone;

                            if (!reader.IsDBNull(indexIdDenunciaRisposta))
                            {
                                //cassaEdileCodice = reader.GetString(indexDenunciaCodiceCassaEdile);
                                //cassaEdileDescrizone = reader.IsDBNull(indexDenunciaDescCassaEdile) ? null : reader.GetString(indexDenunciaDescCassaEdile);

                                denuncia = new DenunciaOreCexChange();
                                denuncia.CassaEdileCodice = cassaEdileCodice;
                                denuncia.CassaEdileDescrizione = cassaEdileDescrizone;
                                denuncia.CodiceCEImpresa = reader.GetString(indexDenuciaCodiceCEImpresa);
                                denuncia.CodiceCELavoratore = reader.GetString(indexDenunciaCodiceCELavoratore);
                                denuncia.PeriodoAnno = periodoAnno;
                                denuncia.PeriodoMese = periodoMese;
                                denuncia.ImponibileGNF = reader.GetDecimal(indexImponibileGNF);
                                denuncia.OreOrdinarie = reader.GetDecimal(indexOreOrdinarie);
                                controllo.DeunciaOre = denuncia;
                            }

                            if (!reader.IsDBNull(indexLavCodiceCE))
                            {
                                //cassaEdileCodice = reader.GetString(indexLavCassaEdileId);
                                //cassaEdileDescrizone = reader.IsDBNull(indexLavCassaEdileDesc) ? null : reader.GetString(indexLavCassaEdileDesc);


                                lavoratore = new LavoratoreCexChange();
                                controllo.Lavoratore = lavoratore;
                                lavoratore.CassaEdileCodice = cassaEdileCodice;
                                lavoratore.CassaEdileDescrizione = cassaEdileDescrizone;
                                lavoratore.CodiceCELavoratore = reader.GetString(indexLavCodiceCE);
                                lavoratore.CodiceFiscale = reader.GetString(indexLavCodFisc);
                                lavoratore.Cognome = reader.GetString(indexLavCognome);
                                lavoratore.DataNascita = reader.IsDBNull(indexLavDataNascita) ? (DateTime?) null : reader.GetDateTime(indexLavDataNascita);
                                lavoratore.LuogoNascita = reader.GetString(indexLavLuogoNascita);
                                lavoratore.NazioneNascita = reader.GetString(indexLavNazioneNascita);
                                lavoratore.Nome = reader.GetString(indexLavNome);
                                lavoratore.ProvinciaNascita = reader.GetString(indexLavProvNascita);
                                lavoratore.Sesso = reader.GetString(indexLavSesso);
                                lavoratore.UltimaDenunciaAnno = reader.IsDBNull(indexLavUltimaDenunciaAnno) ? (int?) null : reader.GetInt32(indexLavUltimaDenunciaAnno);
                                lavoratore.UltimaDenunciaMese = reader.IsDBNull(indexLavUltimaDenunciaMese) ? (int?) null : reader.GetInt32(indexLavUltimaDenunciaMese);
                            }

                            if (!reader.IsDBNull(indexImpCodiceCE))
                            {
                                //cassaEdileCodice = reader.GetString(indexImpCassaEdileId);
                                //cassaEdileDescrizone = reader.IsDBNull(indexImpCassaEdileDesc) ? null : reader.GetString(indexImpCassaEdileDesc);

                                controllo.ImpresaPresenzaDenunceInPeriodo = reader.GetBoolean(indexImpPresenzaDenunce);

                                impresa = new ImpresaCexChange();
                                controllo.Impresa = impresa;
                                impresa.CassaEdileCodice = cassaEdileCodice;
                                impresa.CassaEdileDescrizione = cassaEdileDescrizone;
                                impresa.CodiceCEImpresa = reader.GetString(indexImpCodiceCE);
                                impresa.CodiceFiscale = reader.GetString(indexImpCodFisc);
                                impresa.DataCessazione = reader.IsDBNull(indexImpDataCessazione) ? (DateTime?) null : reader.GetDateTime(indexImpDataCessazione);
                                impresa.DataDecorrenza = reader.IsDBNull(indexImpDataDecorrenza) ? (DateTime?) null : reader.GetDateTime(indexImpDataDecorrenza);
                                impresa.DataIscrizione = reader.IsDBNull(indexImpDataIscrizione) ? (DateTime?) null : reader.GetDateTime(indexImpDataIscrizione);
                                impresa.Natura = reader.GetString(indexImpNatura);
                                impresa.PartitaIva = reader.GetString(indexImpParitaIva);
                                impresa.RagioneSociale = reader.GetString(indexImpRagSoc);
                                impresa.Stato = reader.GetString(indexImpStato);
                                impresa.UltimaDenunciaAnno = reader.IsDBNull(indexImpUltimaDenunciaAnno) ? (int?) null : reader.GetInt32(indexImpUltimaDenunciaAnno);
                                impresa.UltimaDenunciaMese = reader.IsDBNull(indexImpUltimaDenunciaMese) ? (int?) null : reader.GetInt32(indexImpUltimaDenunciaMese);
                                impresa.PosizioneBNI = reader.IsDBNull(indexImpPosizioneBni) ? null : reader.GetString(indexImpPosizioneBni);
                            }

                            if (!reader.IsDBNull(indexEsitoVersamentoId))
                            {
                                controllo.ImpresaEsitoVersamentoInPeriodo = new EsitoVersamentoCexChange
                                                                                {
                                                                                    Id = reader.GetInt32(indexEsitoVersamentoId),
                                                                                    CodiceCexChange = reader.GetString(indexEsitoVersamentoCodice),
                                                                                    Descrizione = reader.GetString(indexEsitoVersamentoDescrizione),
                                                                                };
                            }


                            //controllo.CassaEdileCodice = cassaEdileCodice;
                            //controllo.CassaEdileDescrizione = cassaEdileDescrizone;
                            controlli.Add(controllo);

                        }

                    }

                }
                #endregion

                #region Imprese

                using (
                    DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_AccessoCantieriControlliCEXChangeImpreseSelect]")
                    )
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIvaImpresa);
                    DatabaseCemi.AddInParameter(comando, "@mesePresenzaDenunce", DbType.String, periodoMese);
                    DatabaseCemi.AddInParameter(comando, "@annoPresenzaDenunce", DbType.String, periodoAnno);

                    using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                    {
                        #region indexes

                        Int32 indexImpPosizioneBni = reader.GetOrdinal("posizioneBNI");
                        Int32 indexImpPresenzaDenunce = reader.GetOrdinal("impresaPresenzaDenunce");
                        Int32 indexImpCassaEdileId = reader.GetOrdinal("impresaIdCassaEdile");
                        Int32 indexImpCassaEdileDesc = reader.GetOrdinal("impresaDescCassaEdile");
                        Int32 indexImpCodiceCE = reader.GetOrdinal("impresaCodiceCE");
                        Int32 indexImpCodFisc = reader.GetOrdinal("impresaCodiceFiscale");
                        Int32 indexImpParitaIva = reader.GetOrdinal("partitaIva");
                        Int32 indexImpDataCessazione = reader.GetOrdinal("impresadataCessazione");
                        Int32 indexImpDataDecorrenza = reader.GetOrdinal("impresadataDecorrenza");
                        Int32 indexImpDataIscrizione = reader.GetOrdinal("impresadataIscrizione");
                        Int32 indexImpNatura = reader.GetOrdinal("natura");
                        Int32 indexImpRagSoc = reader.GetOrdinal("ragioneSociale");
                        Int32 indexImpStato = reader.GetOrdinal("stato");
                        Int32 indexImpUltimaDenunciaAnno = reader.GetOrdinal("impresaUltimaDenunciaAnno");
                        Int32 indexImpUltimaDenunciaMese = reader.GetOrdinal("impresaUltimaDenunciaMese");


                        #endregion

                        ControlloDenunciaOreCexChange controllo;
                        ImpresaCexChange impresa = null;
                        // LavoratoreCexChange lavoratore = null;
                        // DenunciaOreCexChange denuncia = null;
                        String cassaEdileCodice = null;
                        String cassaEdileDescrizone = null;
                        while (reader.Read())
                        {
                            cassaEdileCodice = reader.GetString(indexImpCassaEdileId);
                            cassaEdileDescrizone = reader.IsDBNull(indexImpCassaEdileDesc) ? null : reader.GetString(indexImpCassaEdileDesc);

                            if (controlli.FirstOrDefault(x => x.CassaEdileCodice == cassaEdileCodice) != null)
                            {
                                continue;
                            }

                            controllo = new ControlloDenunciaOreCexChange();
                            controllo.PeriodoAnno = periodoAnno;
                            controllo.PeriodoMese = periodoMese;
                            controllo.CassaEdileCodice = cassaEdileCodice;
                            controllo.CassaEdileDescrizione = cassaEdileDescrizone;
                            controllo.ImpresaPresenzaDenunceInPeriodo = reader.GetBoolean(indexImpPresenzaDenunce);

                            if (!reader.IsDBNull(indexImpCodiceCE))
                            {

                                impresa = new ImpresaCexChange();
                                controllo.Impresa = impresa;
                                impresa.CassaEdileCodice = cassaEdileCodice;
                                impresa.CassaEdileDescrizione = cassaEdileDescrizone;
                                impresa.CodiceCEImpresa = reader.GetString(indexImpCodiceCE);
                                impresa.CodiceFiscale = reader.GetString(indexImpCodFisc);
                                impresa.DataCessazione = reader.IsDBNull(indexImpDataCessazione) ? (DateTime?) null : reader.GetDateTime(indexImpDataCessazione);
                                impresa.DataDecorrenza = reader.IsDBNull(indexImpDataDecorrenza) ? (DateTime?) null : reader.GetDateTime(indexImpDataDecorrenza);
                                impresa.DataIscrizione = reader.IsDBNull(indexImpDataIscrizione) ? (DateTime?) null : reader.GetDateTime(indexImpDataIscrizione);
                                impresa.Natura = reader.GetString(indexImpNatura);
                                impresa.PartitaIva = reader.GetString(indexImpParitaIva);
                                impresa.RagioneSociale = reader.GetString(indexImpRagSoc);
                                impresa.Stato = reader.GetString(indexImpStato);
                                impresa.UltimaDenunciaAnno = reader.IsDBNull(indexImpUltimaDenunciaAnno) ? (int?) null : reader.GetInt32(indexImpUltimaDenunciaAnno);
                                impresa.UltimaDenunciaMese = reader.IsDBNull(indexImpUltimaDenunciaMese) ? (int?) null : reader.GetInt32(indexImpUltimaDenunciaMese);
                                impresa.PosizioneBNI = reader.IsDBNull(indexImpPosizioneBni) ? null : reader.GetString(indexImpPosizioneBni);
                            }



                            controlli.Add(controllo);

                        }

                    }

                }

                #endregion
            }




            return controlli;
        }

        public void DeleteLavoratoreInWhiteList(Int32 idWhiteListLavoratore)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListLavoratoriDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@id", DbType.Int32, idWhiteListLavoratore);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cancellazione lavoratore non riuscita");
                }
            }
        }

        public Boolean UpdateWhiteListLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            else
            {
                if (!lavoratore.IdDomandaLavoratore.HasValue)
                {
                    throw new ArgumentException("Il lavoratore che si sta tentando di aggiornare non è stato trovato");
                }
                else
                {
                    using (
                        DbCommand comando =
                            DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriWhiteListLavoratoriUpdate"))
                    {
                        DatabaseCemi.AddInParameter(comando, "@id", DbType.Int32, lavoratore.IdDomandaLavoratore.Value);
                        if (lavoratore.DataInizioAttivita.HasValue)
                        {
                            DatabaseCemi.AddInParameter(comando, "@dataInizioAttivita", DbType.DateTime, lavoratore.DataInizioAttivita.Value);
                        }
                        if (lavoratore.DataFineAttivita.HasValue)
                        {
                            DatabaseCemi.AddInParameter(comando, "@dataFineAttivita", DbType.DateTime, lavoratore.DataFineAttivita.Value);
                        }
                        if (!String.IsNullOrWhiteSpace(lavoratore.ContrattoApplicato))
                        {
                            DatabaseCemi.AddInParameter(comando, "@contrattoApplicato", DbType.String, lavoratore.ContrattoApplicato);
                        }

                        if (transaction != null)
                        {
                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                res = true;
                            }
                        }
                        else
                        {
                            if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                            {
                                res = true;
                            }
                        }
                    }
                }
            }

            return res;
        }

        public LavoratoreCollection GetLavoratoriInForza(Int32 idImpresa)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AccessoCantieriLavoratoriInForzaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }
    }
}
