﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Cemi.MalattiaTelematica.Type.Collections;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.MalattiaTelematica.Type.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Business;
using System.Xml.Serialization;
using System.IO;
using System.Data.SqlClient;

namespace Cemi.MalattiaTelematica.Data
{
    public class MalattiaTelematicaDataAccess
    {
        private Database databaseCemi;

        public MalattiaTelematicaDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }


        public Int32 GetOreDaLavorare()
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectMalattiaTelematicaOreLavorate"))
            {
                DatabaseCemi.AddOutParameter(comando, "@oreLavorate", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                return (Int32)DatabaseCemi.GetParameterValue(comando, "@oreLavorate");
            }

            return -1;
        }

        public Decimal GetOreLavorate(Int32 idLavoratore, DateTime dataInizio, DateTime dataAssunzione, Int32 idAssenza)
        {
            Decimal oreDich = 0.0M;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaOreLavorate")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, dataInizio);
                DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime, dataAssunzione);
                DatabaseCemi.AddInParameter(comando, "@idAssenza", DbType.Int32, idAssenza);
              
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {

                    Int32 indiceOreDichiarate = reader.GetOrdinal("OreDichiarate");

                    while (reader.Read())
                    {
                        oreDich = reader.GetDecimal(indiceOreDichiarate);
                    }
                }

                return oreDich;
            }
        }

        public AssenzaErroriCollection GetAssenzaErrori(DateTime data)
        {
            AssenzaErroriCollection assenzaErrori = new AssenzaErroriCollection();

            DataSet dsErrori;
            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaErroriAssenze")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                dsErrori = databaseCemi.ExecuteDataSet(comando);
            }
            if ((dsErrori != null) && (dsErrori.Tables.Count == 1))
            {
                for (int i = 0; i < dsErrori.Tables[0].Rows.Count; i++)
                {
                    Int32 idImpresa;
                    String ragioneSociale;
                    Int32 idLavoratore;
                    String cognome;
                    String nome;
                    String idCassaEdile;
                    String tipoProtocollo;
                    Int32 annoProtocollo;
                    Int32 numeroProtocollo;
                    DateTime dataInizio;
                    DateTime dataFine;
                    DateTime dataInizioMalattia;
                    String descrizione;

                    idImpresa = (Int32)dsErrori.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (String)dsErrori.Tables[0].Rows[i]["ragioneSociale"];
                    idLavoratore = (Int32)dsErrori.Tables[0].Rows[i]["idLavoratore"];
                    cognome = (String)dsErrori.Tables[0].Rows[i]["cognome"];
                    nome = (String)dsErrori.Tables[0].Rows[i]["nome"];
                    idCassaEdile = (String)dsErrori.Tables[0].Rows[i]["idCassaEdile"];
                    tipoProtocollo = (String)dsErrori.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (Int32)dsErrori.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (Int32)dsErrori.Tables[0].Rows[i]["numeroProtocollo"];
                    dataInizio = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime)dsErrori.Tables[0].Rows[i]["dataFine"];
                    dataInizioMalattia = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizioMalattia"];
                    descrizione = (String)dsErrori.Tables[0].Rows[i]["descrizione"];

                    AssenzaErrori ass = new AssenzaErrori(idImpresa, ragioneSociale, idLavoratore, cognome, nome, idCassaEdile, tipoProtocollo, annoProtocollo, numeroProtocollo, dataInizio, dataFine, dataInizioMalattia, descrizione);

                    assenzaErrori.Add(ass);
                }
            }

            return assenzaErrori;
        }

        public CertificatoMedicoErroriCollection GetCertificatoMedicoErrori(DateTime data)
        {
            CertificatoMedicoErroriCollection cmErrori = new CertificatoMedicoErroriCollection();

            DataSet dsErrori;
            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaErroriCertificatiMedici")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                dsErrori = databaseCemi.ExecuteDataSet(comando);
            }
            if ((dsErrori != null) && (dsErrori.Tables.Count == 1))
            {
                for (int i = 0; i < dsErrori.Tables[0].Rows.Count; i++)
                {
                    Int32 idImpresa;
                    String ragioneSociale;
                    Int32 idLavoratore;
                    String cognome;
                    String nome;
                    String idCassaEdile;
                    String tipoProtocollo;
                    Int32 annoProtocollo;
                    Int32 numeroProtocollo;
                    DateTime dataInizio;
                    DateTime dataFine;
                    DateTime dataInizioMalattia;
                    String descrizione;
                    String numeroCertificato;
                    DateTime dataInizioCertificato;
                    DateTime dataFineCertificato;
                    DateTime dataRilascioCertificato;


                    idImpresa = (Int32)dsErrori.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (String)dsErrori.Tables[0].Rows[i]["ragioneSociale"];
                    idLavoratore = (Int32)dsErrori.Tables[0].Rows[i]["idLavoratore"];
                    cognome = (String)dsErrori.Tables[0].Rows[i]["cognome"];
                    nome = (String)dsErrori.Tables[0].Rows[i]["nome"];
                    idCassaEdile = (String)dsErrori.Tables[0].Rows[i]["idCassaEdile"];
                    tipoProtocollo = (String)dsErrori.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (Int32)dsErrori.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (Int32)dsErrori.Tables[0].Rows[i]["numeroProtocollo"];
                    dataInizio = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime)dsErrori.Tables[0].Rows[i]["dataFine"];
                    dataInizioMalattia = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizioMalattia"];
                    descrizione = (String)dsErrori.Tables[0].Rows[i]["descrizione"];


                    numeroCertificato = (String)dsErrori.Tables[0].Rows[i]["numeroEnte"];
                    dataInizioCertificato = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizioCertificato"];
                    dataFineCertificato = (DateTime)dsErrori.Tables[0].Rows[i]["dataFineCertificato"];
                    dataRilascioCertificato = (DateTime)dsErrori.Tables[0].Rows[i]["dataRilascioCertificato"];
                    


                    CertificatoMedicoErrori cm = new CertificatoMedicoErrori(idImpresa, ragioneSociale, idLavoratore, cognome, nome, idCassaEdile, tipoProtocollo, annoProtocollo, numeroProtocollo, dataInizio, dataFine, dataInizioMalattia, descrizione, numeroCertificato, dataInizioCertificato, dataFineCertificato, dataRilascioCertificato);

                    cmErrori.Add(cm);
                }
            }

            return cmErrori;
        }

        public MalattiaInfortunioErroriCollection GetMalattiaInfortunioErrori(DateTime data)
        {
            MalattiaInfortunioErroriCollection miErrori = new MalattiaInfortunioErroriCollection();

            DataSet dsErrori;
            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaErroriMalattieInfortuni")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                dsErrori = databaseCemi.ExecuteDataSet(comando);
            }
            if ((dsErrori != null) && (dsErrori.Tables.Count == 1))
            {
                for (int i = 0; i < dsErrori.Tables[0].Rows.Count; i++)
                {
                    Int32 idImpresa;
                    String ragioneSociale;
                    Int32 idLavoratore;
                    String cognome;
                    String nome;
                    String idCassaEdile;
                    String tipoProtocollo;
                    Int32 annoProtocollo;
                    Int32 numeroProtocollo;
                    DateTime dataInizio;
                    DateTime dataFine;
                    DateTime dataInizioMalattia;
                    String descrizione;
                    DateTime dataInizioEvento;
                    DateTime dataFineEvento;


                    idImpresa = (Int32)dsErrori.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (String)dsErrori.Tables[0].Rows[i]["ragioneSociale"];
                    idLavoratore = (Int32)dsErrori.Tables[0].Rows[i]["idLavoratore"];
                    cognome = (String)dsErrori.Tables[0].Rows[i]["cognome"];
                    nome = (String)dsErrori.Tables[0].Rows[i]["nome"];
                    idCassaEdile = (String)dsErrori.Tables[0].Rows[i]["idCassaEdile"];
                    tipoProtocollo = (String)dsErrori.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (Int32)dsErrori.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (Int32)dsErrori.Tables[0].Rows[i]["numeroProtocollo"];
                    dataInizio = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime)dsErrori.Tables[0].Rows[i]["dataFine"];
                    dataInizioMalattia = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizioMalattia"];
                    descrizione = (String)dsErrori.Tables[0].Rows[i]["descrizione"];

                    dataInizioEvento = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizioEvento"];
                    dataFineEvento = (DateTime)dsErrori.Tables[0].Rows[i]["dataInizioEvento"];

                    MalattiaInfortunioErrori mi = new MalattiaInfortunioErrori(idImpresa, ragioneSociale, idLavoratore, cognome, nome, idCassaEdile, tipoProtocollo, annoProtocollo, numeroProtocollo, dataInizio, dataFine, dataInizioMalattia, descrizione, dataInizioEvento, dataFineEvento);

                    miErrori.Add(mi);
                }
            }

            return miErrori;
        }

        public PrestazioneDomandaAssenzaCollection GetPrestazioneDomandaAssenza(DateTime? inizio, DateTime? fine, String ragSoc, String codFisc, Int32? idImpr)
        {
            PrestazioneDomandaAssenzaCollection prestazioniDomandeAssenza = new PrestazioneDomandaAssenzaCollection();

            DataSet dsAssenze;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaPrestazioniDomandeAssenzaSelect"))
            {
                if (inizio.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@Inizio", DbType.DateTime, inizio.Value);
                if (fine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, fine.Value);
                if (ragSoc != String.Empty)
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragSoc);
                if (codFisc != String.Empty)
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codFisc);
                if (idImpr.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpr);

                dsAssenze = databaseCemi.ExecuteDataSet(comando);
            }

            if ((dsAssenze != null) && (dsAssenze.Tables.Count == 1))
            {
                for (int i = 0; i < dsAssenze.Tables[0].Rows.Count; i++)
                {
                    Int32 idImpresa;
                    String ragioneSociale;
                    String codiceFiscale;
                    String partitaIVA = "";
                    String tipoProtocollo = "";
                    Int32 annoProtocollo;
                    Int32 numeroProtocollo;
                    DateTime data;
                    Decimal importo;
                    Int32 numero;
                    Int32 idAssenza;

                    idImpresa = (Int32)dsAssenze.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (String)dsAssenze.Tables[0].Rows[i]["ragioneSociale"];
                    codiceFiscale = (String)dsAssenze.Tables[0].Rows[i]["codiceFiscale"];
                    if (dsAssenze.Tables[0].Rows[i]["partitaIVA"] != DBNull.Value)
                        partitaIVA = (String)dsAssenze.Tables[0].Rows[i]["partitaIVA"];
                    if (dsAssenze.Tables[0].Rows[i]["tipoProtocollo"] != DBNull.Value)
                        tipoProtocollo = (String)dsAssenze.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (Int32)dsAssenze.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (Int32)dsAssenze.Tables[0].Rows[i]["numeroProtocollo"];
                    data = (DateTime)dsAssenze.Tables[0].Rows[i]["data"];
                    importo = (Decimal)dsAssenze.Tables[0].Rows[i]["importo"];
                    numero = (Int32)dsAssenze.Tables[0].Rows[i]["numero"];

                    idAssenza = (Int32)dsAssenze.Tables[0].Rows[i]["idAssenza"];

                    PrestazioneDomandaAssenza ass = new PrestazioneDomandaAssenza(idImpresa, ragioneSociale, codiceFiscale, partitaIVA, tipoProtocollo, annoProtocollo, numeroProtocollo, data, importo, numero, idAssenza);
                    
                    prestazioniDomandeAssenza.Add(ass);
                }
            }

            return prestazioniDomandeAssenza;
        }

        //public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        //{
        //    bool res = false;
        //    oreDuplicate = false;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertUpdate"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, ore.IdLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, ore.IdCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, ore.Anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, ore.Mese);

        //        databaseCemi.AddInParameter(comando, "@oreLavorate", DbType.Int32, ore.OreLavorate);
        //        databaseCemi.AddInParameter(comando, "@oreFerie", DbType.Int32, ore.OreFerie);
        //        databaseCemi.AddInParameter(comando, "@oreInfortunio", DbType.Int32, ore.OreInfortunio);
        //        databaseCemi.AddInParameter(comando, "@oreMalattia", DbType.Int32, ore.OreMalattia);
        //        databaseCemi.AddInParameter(comando, "@oreCassaIntegrazione", DbType.Int32, ore.OreCassaIntegrazione);
        //        databaseCemi.AddInParameter(comando, "@orePermessoRetribuito", DbType.Int32, ore.OrePermessoRetribuito);
        //        databaseCemi.AddInParameter(comando, "@orePermessoNonRetribuito", DbType.Int32,
        //                                    ore.OrePermessoNonRetribuito);
        //        databaseCemi.AddInParameter(comando, "@oreAltro", DbType.Int32, ore.OreAltro);
        //        databaseCemi.AddInParameter(comando, "@livelloErogazione", DbType.Int32, ore.LivelloErogazione);
        //        databaseCemi.AddInParameter(comando, "@inseriteManualmente", DbType.Boolean, ore.InseriteManualmente);

        //        try
        //        {
        //            if (databaseCemi.ExecuteNonQuery(comando) == 1)
        //                res = true;
        //        }
        //        catch (SqlException sqlExc)
        //        {
        //            // Eccezione che viene generata se provo ad inserire due volte le ore per lo stesso mese
        //            if (sqlExc.Number == 2627)
        //                oreDuplicate = true;
        //            else
        //                throw;
        //        }
        //    }

        //    return res;
        //}


        public StatisticaLiquidazioneCollection GetStatisticaLiquidazioni(DateTime inizio, DateTime fine, String tipo)
        {
            StatisticaLiquidazioneCollection collection = new StatisticaLiquidazioneCollection();

            DataSet dsRighe;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaStatisticaLiquidazioneSelect"))
            {
                //if (inizio.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@Inizio", DbType.DateTime, inizio);
                //if (fine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, fine);
                if (tipo != String.Empty)
                    DatabaseCemi.AddInParameter(comando, "@tipoAssenza", DbType.String, tipo);

                dsRighe = databaseCemi.ExecuteDataSet(comando);
            }

            if ((dsRighe != null) && (dsRighe.Tables.Count == 1))
            {
                for (int i = 0; i < dsRighe.Tables[0].Rows.Count; i++)
                {
                    DateTime data;
                    Decimal importo;
                    Int32 numeroLavoratori;
                    Int32 numeroImprese;

                    data = (DateTime)dsRighe.Tables[0].Rows[i]["data"];
                    importo = (Decimal)dsRighe.Tables[0].Rows[i]["importo"];
                    numeroLavoratori = (Int32)dsRighe.Tables[0].Rows[i]["numeroLavoratori"];
                    numeroImprese = (Int32)dsRighe.Tables[0].Rows[i]["numeroImprese"];


                    StatisticaLiquidazione stat = new StatisticaLiquidazione(data, importo, numeroLavoratori, numeroImprese);

                    collection.Add(stat);
                }
            }

            return collection;
        }

        public StatisticaEvaseCollection GetStatisticaEvase(DateTime inizio, DateTime fine, String tipo)
        {
            StatisticaEvaseCollection collection = new StatisticaEvaseCollection();

            DataSet dsRighe;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaStatisticaEvaseSelect"))
            {
                //if (inizio.HasValue)
                DatabaseCemi.AddInParameter(comando, "@Inizio", DbType.DateTime, inizio);
                //if (fine.HasValue)
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, fine);
                if (tipo != String.Empty)
                    DatabaseCemi.AddInParameter(comando, "@tipoAssenza", DbType.String, tipo);

                dsRighe = databaseCemi.ExecuteDataSet(comando);
            }

            if ((dsRighe != null) && (dsRighe.Tables.Count == 1))
            {
                for (int i = 0; i < dsRighe.Tables[0].Rows.Count; i++)
                {
                    String stato;
                    String utente;
                    Int32 numeroAssenze;

                    stato = (String)dsRighe.Tables[0].Rows[i]["stato"];
                    utente = (String)dsRighe.Tables[0].Rows[i]["utente"];
                    numeroAssenze = (Int32)dsRighe.Tables[0].Rows[i]["numeroAssenze"];


                    StatisticaEvase stat = new StatisticaEvase(stato, utente, numeroAssenze);

                    collection.Add(stat);
                }
            }

            return collection;
        }

        public Boolean InsertCassaEdile(int idAssenza, string idCassaEdile)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCasseEdiliInsert"))
            {
                databaseCemi.AddInParameter(comando, "@idAssenza", DbType.Int32, idAssenza);
                databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                databaseCemi.AddInParameter(comando, "@inseritaManualmente", DbType.Boolean, true);

                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }
        public Boolean DeleteCassaEdile(int idAssenza, string idCassaEdile, int idLavoratore)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCasseEdiliDelete"))
            {
                databaseCemi.AddInParameter(comando, "@idAssenza", DbType.Int32, idAssenza);
                databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                
                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }


        public Boolean UpdateMalattiaTelematicaCertificatoMedico(int idCertificatoMedico, DateTime dataInizio, DateTime dataFine, DateTime dataRilascio, byte[] immagine, string nomeFile, string numero, string tipo, string tipoAssenza, Boolean? ricovero)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCertificatiMediciUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@idCertificatoMedico", DbType.Int32, idCertificatoMedico);
                databaseCemi.AddInParameter(comando, "@dataRilascio", DbType.DateTime, dataRilascio);
                databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, dataInizio);
                databaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataFine);
                databaseCemi.AddInParameter(comando, "@numero", DbType.String, numero);
                databaseCemi.AddInParameter(comando, "@tipo", DbType.String, tipo);
                databaseCemi.AddInParameter(comando, "@tipoAssenza", DbType.String, tipoAssenza);
                databaseCemi.AddInParameter(comando, "@immagine", DbType.Binary, immagine);
                databaseCemi.AddInParameter(comando, "@nomeFile", DbType.String, nomeFile);
                databaseCemi.AddInParameter(comando, "@ricovero", DbType.Boolean, ricovero.HasValue? ricovero.Value: false);
                
                if (databaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        #region filtri ricerca
        public void InsertFiltroRicerca(Int32 idUtente, AssenzeFilter filtro, Int32? pagina)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException();
            }

            XmlSerializer ser = new XmlSerializer(typeof(AssenzeFilter));
            String filtroSerializzato = String.Empty;
            if (filtro != null)
            {
                using (StringWriter sw = new StringWriter())
                {
                    ser.Serialize(sw, filtro);
                    filtroSerializzato = sw.ToString();
                }
            }


            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaFiltriRicercaInsertUpdate"))
            {
                databaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                if (filtro != null)
                {
                    databaseCemi.AddInParameter(comando, "@filtro", DbType.Xml, filtroSerializzato);
                    databaseCemi.AddInParameter(comando, "@pagina", DbType.Int32, pagina.Value);
                }
              
                if (databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Errore durante l'inserimento del filtro di ricerca");
                }
            }
        }

        public AssenzeFilter GetFiltroRicerca(Int32 idUtente, out Int32 pagina)
        {
            AssenzeFilter filtro = null;
            pagina = 0;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaFiltriRicercaSelect"))
            {
                databaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        #region Indici reader
                        Int32 indiceUserID = reader.GetOrdinal("idUtente");
                        Int32 indiceFiltro = reader.GetOrdinal("filtro");
                        Int32 indicePagina = reader.GetOrdinal("pagina");
                        #endregion

                        if (!reader.IsDBNull(indiceUserID))
                        {
                            String filtroSerializzato = null;

                            if (!reader.IsDBNull(indiceFiltro))
                            {
                                filtroSerializzato = reader.GetString(indiceFiltro);
                                pagina = reader.GetInt32(indicePagina);
                            }
                            if (!String.IsNullOrEmpty(filtroSerializzato))
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(AssenzeFilter));
                                using (StringReader sr = new StringReader(filtroSerializzato))
                                {
                                    filtro = (AssenzeFilter)ser.Deserialize(sr);
                                }
                            }
                        }
                    }
                }
            }

            return filtro;
        }

        public void DeleteFiltroRicerca(Int32 idUtente)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaFiltriRicercaDelete"))
            {
                databaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }
        #endregion
    }
}
