namespace TBridge.Cemi.TuteScarpe.Business
{
    public class TipoRicerca
    {
        public TipoRicerca(TipoRicercaEnum tipo, int? codice, string ragioneSociale)
        {
            Tipo = tipo;
            Codice = codice;
            RagioneSociale = ragioneSociale;
        }

        public TipoRicercaEnum Tipo { get; private set; }

        public int? Codice { get; private set; }

        public string RagioneSociale { get; private set; }
    }

    public enum TipoRicercaEnum
    {
        Tutti,
        Codice,
        RagioneSociale
    }
}