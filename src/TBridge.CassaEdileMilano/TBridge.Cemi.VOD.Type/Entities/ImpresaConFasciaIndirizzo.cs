﻿using System;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.VOD.Type.Entities
{
    public class ImpresaConFasciaIndirizzo
    {
        public Int32 IdImpresa { get; set; }
        public String RagioneSociale { get; set; }
        public String Stato { get; set; }
        public Decimal MediaFinale { get; set; }
        public DateTime DataDenuncia { get; set; }
        public Int32 IdConsulente { get; set; }
        public Int32 NumeroOperai { get; set; }
        public String IdEsattore { get; set; }
        public Decimal MediaAssenze { get; set; }
        public Int32 PartTime { get; set; }
        public Indirizzo IndirizzoConsulente { get; set; }
        public Indirizzo IndirizzoContatto { get; set; }
    }
}