﻿using System;

namespace TBridge.Cemi.VOD.Type.Filters
{
    [Serializable]
    public class ImpreseFilter
    {
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public String Fascia { get; set; }
        public Int32? IdConsulente { get; set; }
        public Int32? IdImpresa { get; set; }
        public Int32? MediaPartTime { get; set; }
        public Int32? NumeroOperai { get; set; }
        public String Stato { get; set; }
        public String IdEsattore { get; set; }
        public String EsitoControlloDenuncia { get; set; }
        public Double? OreFerie { set; get; }
        public Double? OrePermessiRetribuiti { set; get; }
        public Double? OreCigStraordinaria { set; get; }
        public Double? OreCigDeroga { set; get; }
        public Double? OreCigo { set; get; }
        public Double? OreCigoMaltempo { set; get; }
    }
}