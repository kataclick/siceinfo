﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TBridge.Cemi.AccessoCantieri.Type.WSTypes;

namespace Cemi.AccessoCantieri.WSProvider
{
    [ServiceContract(Namespace = "www.cassaedilemilano.it/WSAccessoCantieri")]
    public interface IWSProviderService
    {
        [OperationContract]
        String CaricaImpreseLavoratori(String codiceRilevatore, String controlCode, List<Impresa> imprese);

        [OperationContract]
        String CaricaAccessi(String codiceRilevatore, String controlCode, List<Accesso> accessi);

        [OperationContract]
        String CaricaAccesso(String codiceRilevatore,
            String controlCode,
            String data,
            String ora,
            String codiceFiscaleLavoratore,
            String codiceFiscaleImpresa,
            Boolean ingresso);

        [OperationContract]
        String CaricaImpresa(String codiceRilevatore,
            String controlCode,
            String ragioneSociale,
            String codiceFiscale,
            String partitaIVA,
            String indirizzo,
            String comune,
            String provincia,
            String CAP,
            String contrattoApplicato,
            String codiceFiscaleAppaltatrice);

        [OperationContract]
        String CaricaLavoratore(String codiceRilevatore,
            String controlCode,
            String codiceFiscaleImpresa,
            String cognome,
            String nome,
            String codiceFiscale,
            String dataNascita,
            String contrattoApplicato,
            String dal,
            String al);
    }
}
