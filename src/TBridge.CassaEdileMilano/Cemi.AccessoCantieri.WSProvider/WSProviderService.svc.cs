﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.WSTypes;

namespace Cemi.AccessoCantieri.WSProvider
{
    public class WSProviderService : IWSProviderService
    {
        private AccessoCantieriBusiness biz = new AccessoCantieriBusiness();
        private WSConnector wsConnector = new WSConnector();

        public String CaricaImpreseLavoratori(String codiceRilevatore, String controlCode, List<Impresa> imprese)
        {
            String res = "OK";

            // Controllo corrispondenza codiceRilevatore con controlCode
            Int32 idCantiere = biz.GetIdCantiere(TipologiaFornitore.WebService, codiceRilevatore, DateTime.Now);
            Guid guidControllo;

            try
            {
                if (!String.IsNullOrWhiteSpace(controlCode))
                {
                    guidControllo = Guid.Parse(controlCode);

                    if (idCantiere > 0)
                    {
                        if (biz.CheckCodiceControlloPerImportAutomatico(idCantiere, guidControllo))
                        {
                            res = wsConnector.CaricaImpreseLavoratori(codiceRilevatore, idCantiere, imprese);
                        }
                        else
                        {
                            res = "Il controlCode fornito non è valido";
                        }
                    }
                    else
                    {
                        res = "Il codiceRilevatore fornito non è valido";
                    }
                }
                else
                {
                    res = "Il controlCode fornito non può essere vuoto";
                }
            }
            catch (FormatException)
            {
                res = "Il controlCode fornito non è valido";
            }
            catch (Exception exc)
            {
                res = String.Format("Si è verificato un errore: {0}", exc.Message);
            }

            return res;
        }

        public string CaricaImpresa(
            String codiceRilevatore,
            String controlCode,
            String ragioneSociale,
            String codiceFiscale,
            String partitaIVA,
            String indirizzo,
            String comune,
            String provincia,
            String CAP,
            String contrattoApplicato,
            String codiceFiscaleAppaltatrice)
        {
            String res = "OK";

            // Controllo corrispondenza codiceRilevatore con controlCode
            Int32 idCantiere = biz.GetIdCantiere(TipologiaFornitore.WebService, codiceRilevatore, DateTime.Now);
            Guid guidControllo;

            try
            {
                if (!String.IsNullOrWhiteSpace(controlCode))
                {
                    guidControllo = Guid.Parse(controlCode);

                    if (idCantiere > 0)
                    {
                        if (biz.CheckCodiceControlloPerImportAutomatico(idCantiere, guidControllo))
                        {
                            if (!String.IsNullOrWhiteSpace(ragioneSociale))
                            {
                                if (!String.IsNullOrWhiteSpace(codiceFiscale) && (codiceFiscale.Trim().Length == 11 || codiceFiscale.Trim().Length == 16))
                                {
                                    if (!String.IsNullOrWhiteSpace(partitaIVA) && partitaIVA.Trim().Length == 11)
                                    {
                                        Impresa impresa = new Impresa()
                                        {
                                            RagioneSociale = ragioneSociale,
                                            CodiceFiscale = codiceFiscale,
                                            PartitaIVA = partitaIVA,
                                            Indirizzo = indirizzo,
                                            Comune = comune,
                                            Provincia = provincia,
                                            CAP = CAP,
                                            ContrattoApplicato = contrattoApplicato,
                                            CodiceFiscaleAppaltatrice = codiceFiscaleAppaltatrice
                                        };

                                        res = wsConnector.CaricaImpresa(codiceRilevatore, idCantiere, impresa);
                                    }
                                    else
                                    {
                                        res = "Partita IVA non valida";
                                    }
                                }
                                else
                                {
                                    res = "Codice fiscale non valido";
                                }
                            }
                            else
                            {
                                res = "La ragione sociale non può essere vuota";
                            }
                        }
                        else
                        {
                            res = "Il controlCode fornito non è valido";
                        }
                    }
                    else
                    {
                        res = "Il codiceRilevatore fornito non è valido";
                    }
                }
                else
                {
                    res = "Il controlCode fornito non può essere vuoto";
                }
            }
            catch (FormatException)
            {
                res = "Il controlCode fornito non è valido";
            }
            catch (Exception exc)
            {
                res = String.Format("Si è verificato un errore: {0}", exc.Message);
            }

            return res;
        }

        public String CaricaAccessi(String codiceRilevatore, String controlCode, List<Accesso> accessi)
        {
            String res = "OK";

            // Controllo corrispondenza codiceRilevatore con controlCode
            Int32 idCantiere = biz.GetIdCantiere(TipologiaFornitore.WebService, codiceRilevatore, DateTime.Now);
            Guid guidControllo;

            try
            {
                if (!String.IsNullOrWhiteSpace(controlCode))
                {
                    guidControllo = Guid.Parse(controlCode);

                    if (idCantiere > 0)
                    {
                        if (biz.CheckCodiceControlloPerImportAutomatico(idCantiere, guidControllo))
                        {
                            res = wsConnector.CaricaAccessi(codiceRilevatore, idCantiere, accessi);
                        }
                        else
                        {
                            res = "Il controlCode fornito non è valido";
                        }
                    }
                    else
                    {
                        res = "Il codiceRilevatore fornito non è valido";
                    }
                }
                else
                {
                    res = "Il controlCode fornito non può essere vuoto";
                }
            }
            catch (FormatException)
            {
                res = "Il controlCode fornito non è valido";
            }
            catch (Exception exc)
            {
                res = String.Format("Si è verificato un errore: {0}", exc.Message);
            }

            return res;
        }


        public string CaricaAccesso(
            String codiceRilevatore,
            String controlCode,
            String data,
            String ora,
            String codiceFiscaleLavoratore,
            String codiceFiscaleImpresa,
            Boolean ingresso)
        {
            String res = "OK";

            // Controllo corrispondenza codiceRilevatore con controlCode
            Int32 idCantiere = biz.GetIdCantiere(TipologiaFornitore.WebService, codiceRilevatore, DateTime.Now);
            Guid guidControllo;

            try
            {
                if (!String.IsNullOrWhiteSpace(controlCode))
                {
                    guidControllo = Guid.Parse(controlCode);

                    if (idCantiere > 0)
                    {
                        if (biz.CheckCodiceControlloPerImportAutomatico(idCantiere, guidControllo))
                        {
                            if (!String.IsNullOrWhiteSpace(codiceFiscaleLavoratore) && codiceFiscaleLavoratore.Trim().Length == 16)
                            {
                                if (!String.IsNullOrWhiteSpace(codiceFiscaleImpresa) && (codiceFiscaleImpresa.Trim().Length == 11 || codiceFiscaleImpresa.Trim().Length == 16))
                                {
                                    DateTime dataD = DateTime.Now;
                                    DateTime oraD = DateTime.Now;

                                    if (!String.IsNullOrWhiteSpace(data) && DateTime.TryParseExact(data, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dataD))
                                    {
                                        String[] splitted = ora.Split(':');
                                        Int32 oraI;
                                        Int32 minutiI;

                                        if (!String.IsNullOrWhiteSpace(ora) && splitted.Length == 2 && Int32.TryParse(splitted[0], out oraI) && Int32.TryParse(splitted[1], out minutiI) && oraI >= 0 && oraI < 24 && minutiI >= 0 && minutiI < 60)
                                        {
                                            DateTime dataOra = new DateTime(dataD.Year, dataD.Month, dataD.Day, oraI, minutiI, 0);

                                            Accesso accesso = new Accesso()
                                            {
                                                DataOra = dataOra,
                                                CodiceFiscaleLavoratore = codiceFiscaleLavoratore,
                                                CodiceFiscaleImpresa = codiceFiscaleImpresa,
                                                Ingresso = ingresso
                                            };

                                            res = wsConnector.CaricaAccesso(codiceRilevatore, idCantiere, accesso);
                                        }
                                        else
                                        {
                                            res = "Ora non valida";
                                        }
                                    }
                                    else
                                    {
                                        res = "Data non valida";
                                    }
                                }
                                else
                                {
                                    res = "Codice fiscale dell'impresa non valido";
                                }
                            }
                            else
                            {
                                res = "Codice fiscale del lavoratore non valido";
                            }
                        }
                        else
                        {
                            res = "Il controlCode fornito non è valido";
                        }
                    }
                    else
                    {
                        res = "Il codiceRilevatore fornito non è valido";
                    }
                }
                else
                {
                    res = "Il controlCode fornito non può essere vuoto";
                }
            }
            catch (FormatException)
            {
                res = "Il controlCode fornito non è valido";
            }
            catch (Exception exc)
            {
                res = String.Format("Si è verificato un errore: {0}", exc.Message);
            }

            return res;
        }


        public string CaricaLavoratore(
            String codiceRilevatore,
            String controlCode,
            String codiceFiscaleImpresa,
            String cognome,
            String nome,
            String codiceFiscale,
            String dataNascita,
            String contrattoApplicato,
            String dal,
            String al)
        {
            String res = "OK";

            // Controllo corrispondenza codiceRilevatore con controlCode
            Int32 idCantiere = biz.GetIdCantiere(TipologiaFornitore.WebService, codiceRilevatore, DateTime.Now);
            Guid guidControllo;

            try
            {
                if (!String.IsNullOrWhiteSpace(controlCode))
                {
                    guidControllo = Guid.Parse(controlCode);

                    if (idCantiere > 0)
                    {
                        if (biz.CheckCodiceControlloPerImportAutomatico(idCantiere, guidControllo))
                        {
                            if (!String.IsNullOrWhiteSpace(codiceFiscale) && codiceFiscale.Length == 16)
                            {
                                if (!String.IsNullOrWhiteSpace(codiceFiscaleImpresa) && (codiceFiscaleImpresa.Length == 11 || codiceFiscaleImpresa.Length == 16))
                                {
                                    TBridge.Cemi.AccessoCantieri.Type.Entities.WhiteList wl = biz.GetDomandaByKey(idCantiere);
                                    TBridge.Cemi.AccessoCantieri.Type.Entities.Impresa imp = wl.Subappalti.GetImpresaByCodiceFiscale(codiceFiscaleImpresa);

                                    if (imp != null)
                                    {
                                        if (!String.IsNullOrWhiteSpace(cognome))
                                        {
                                            if (!String.IsNullOrWhiteSpace(nome))
                                            {
                                                DateTime? dataNascitaD = null;
                                                DateTime dataDN = DateTime.Now;
                                                DateTime? dataDalD = null;
                                                DateTime dataDD = DateTime.Now;
                                                DateTime? dataAlD = null;
                                                DateTime dataDA = DateTime.Now;
                                                Boolean ok = true;

                                                if (!String.IsNullOrWhiteSpace(dataNascita))
                                                {
                                                    if (DateTime.TryParseExact(dataNascita, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dataDN))
                                                    {
                                                        dataNascitaD = dataDN;
                                                    }
                                                    else
                                                    {
                                                        ok = false;
                                                        res = "Data di nascita non valida";
                                                    }
                                                }

                                                if (!String.IsNullOrWhiteSpace(dal))
                                                {
                                                    if (DateTime.TryParseExact(dal, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dataDD))
                                                    {
                                                        dataDalD = dataDD;
                                                    }
                                                    else
                                                    {
                                                        ok = false;
                                                        res = "Data Dal non valida";
                                                    }
                                                }

                                                if (!String.IsNullOrWhiteSpace(al))
                                                {
                                                    if (DateTime.TryParseExact(al, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dataDA))
                                                    {
                                                        dataAlD = dataDA;
                                                    }
                                                    else
                                                    {
                                                        ok = false;
                                                        res = "Data Al non valida";
                                                    }
                                                }

                                                if (ok)
                                                {
                                                    Lavoratore lavoratore = new Lavoratore()
                                                    {
                                                        Cognome = cognome,
                                                        Nome = nome,
                                                        CodiceFiscale = codiceFiscale,
                                                        DataNascita = dataNascitaD,
                                                        ContrattoApplicato = contrattoApplicato,
                                                        Dal = dataDalD,
                                                        Al = dataAlD,
                                                        CodiceFiscaleImpresa = codiceFiscaleImpresa
                                                    };

                                                    res = wsConnector.CaricaLavoratore(codiceRilevatore, idCantiere, lavoratore);
                                                }
                                            }
                                            else
                                            {
                                                res = "Nome non valido";
                                            }
                                        }
                                        else
                                        {
                                            res = "Cognome non valido";
                                        }
                                    }
                                    else
                                    {
                                        res = "Impresa non presente nel cantiere";
                                    }
                                }
                                else
                                {
                                    res = "Codice fiscale dell'impresa non valido";
                                }
                            }
                            else
                            {
                                res = "Codice fiscale del lavoratore non valido";
                            }
                        }
                        else
                        {
                            res = "Il controlCode fornito non è valido";
                        }
                    }
                    else
                    {
                        res = "Il codiceRilevatore fornito non è valido";
                    }
                }
                else
                {
                    res = "Il controlCode fornito non può essere vuoto";
                }
            }
            catch (FormatException)
            {
                res = "Il controlCode fornito non è valido";
            }
            catch (Exception exc)
            {
                res = String.Format("Si è verificato un errore: {0}", exc.Message);
            }

            return res;
        }
    }
}
