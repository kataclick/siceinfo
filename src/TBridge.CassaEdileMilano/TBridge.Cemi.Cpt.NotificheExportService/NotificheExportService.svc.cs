﻿using System;
using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Filters;

namespace TBridge.Cemi.Cpt.NotificheExportService
{
    public class NotificheExportService : INotificheExportService
    {
        #region INotificheExportService Members

        public NotificaCollection EsportaNotifiche(String partitaIvaCodiceFiscale, String password)
        {
            if (VerificaPassword(password))
            {
                NotificaCollection notifiche = null;

                if (!String.IsNullOrEmpty(partitaIvaCodiceFiscale))
                {
                    CptBusiness biz = new CptBusiness();

                    TBridge.Cemi.Cpt.Type.Filters.NotificaFilter filtro = new NotificaFilter();
                    filtro.FiscIva = partitaIvaCodiceFiscale;
                    filtro.IdArea = 1;

                    TBridge.Cemi.Cpt.Type.Collections.NotificaCollection notificheTrovate =
                        biz.RicercaNotifiche(filtro);

                    notifiche = ConvertiNotifichePerWebService(notificheTrovate, biz);
                }

                return notifiche;
            }

            return null;
        }

        #region Metodi privati
        private NotificaCollection ConvertiNotifichePerWebService(Type.Collections.NotificaCollection notificheTrovate, CptBusiness biz)
        {
            NotificaCollection notificheWS = new NotificaCollection();

            foreach (TBridge.Cemi.Cpt.Type.Entities.Notifica notificaTrovata in notificheTrovate)
            {
                if (!String.IsNullOrEmpty(notificaTrovata.ProtocolloRegione))
                {
                    TBridge.Cemi.Cpt.Type.Entities.NotificaTelematica notificaTelematica =
                        biz.GetNotificaTelematica(notificaTrovata.IdNotifica.Value);

                    Notifica notifica = new Notifica();
                    notificheWS.Add(notifica);

                    if (notificaTelematica.AmmontareComplessivo.HasValue)
                    {
                        notifica.AmmontareComplessivo = notificaTelematica.AmmontareComplessivo.Value;
                    }
                    if (notificaTelematica.DataPrimoInserimento.HasValue)
                    {
                        notifica.DataComunicazione = notificaTelematica.DataPrimoInserimento.Value;
                    }
                    notifica.DataUltimoAggiornamento = notificaTelematica.Data;
                    if (notificaTelematica.Indirizzi != null && notificaTelematica.Indirizzi.Count > 0)
                    {
                        notifica.Indirizzi = new IndirizzoCollection();

                        foreach (TBridge.Cemi.Cpt.Type.Entities.Indirizzo ind in notificaTelematica.Indirizzi)
                        {
                            Indirizzo indirizzo = new Indirizzo();
                            notifica.Indirizzi.Add(indirizzo);

                            indirizzo.IndirizzoDenominazione = ind.Indirizzo1;
                            indirizzo.Localita = ind.Comune;
                            indirizzo.Provincia = ind.Provincia;
                            indirizzo.Cap = ind.Cap;
                            indirizzo.DataInizioLavori = ind.DataInizioLavori;
                            indirizzo.DurataDescrizione = ind.DescrizioneDurata;
                            indirizzo.DurataNumero = ind.NumeroDurata;
                            indirizzo.NumeroMassimoLavoratori = ind.NumeroMassimoLavoratori;
                        }
                    }
                    notifica.NaturaOpera = notificaTelematica.NaturaOpera;
                    if (notificaTelematica.NumeroImprese.HasValue)
                    {
                        notifica.NumeroImprese = notificaTelematica.NumeroImprese.Value;
                    }
                    if (notificaTelematica.NumeroLavoratoriAutonomi.HasValue)
                    {
                        notifica.NumeroLavoratoriAutonomi = notificaTelematica.NumeroLavoratoriAutonomi.Value;
                    }
                    if (notificaTelematica.NumeroMassimoLavoratori.HasValue)
                    {
                        notifica.NumeroMassimoLavoratori = notificaTelematica.NumeroMassimoLavoratori.Value;
                    }
                    notifica.Protocollo = notificaTelematica.ProtocolloRegione;
                    notifica.Note = notificaTelematica.Note;

                    if (notificaTelematica.Committente != null)
                    {
                        notifica.Committente = new Persona();

                        notifica.Committente.Cognome = notificaTelematica.Committente.PersonaCognome;
                        notifica.Committente.Nome = notificaTelematica.Committente.PersonaNome;
                        notifica.Committente.CodiceFiscale = notificaTelematica.Committente.PersonaCodiceFiscale;
                        notifica.Committente.Email = notificaTelematica.Committente.PersonaEmail;
                        notifica.Committente.Indirizzo = notificaTelematica.Committente.PersonaIndirizzo;
                        notifica.Committente.Localita = notificaTelematica.Committente.PersonaComune;
                        notifica.Committente.Provincia = notificaTelematica.Committente.PersonaProvincia;
                    }

                    if (notificaTelematica.DirettoreLavori != null)
                    {
                        notifica.ResponsabileLavori = new Persona();

                        notifica.ResponsabileLavori.Cognome = notificaTelematica.DirettoreLavori.PersonaCognome;
                        notifica.ResponsabileLavori.Nome = notificaTelematica.DirettoreLavori.PersonaNome;
                        notifica.ResponsabileLavori.CodiceFiscale = notificaTelematica.DirettoreLavori.PersonaCodiceFiscale;
                        notifica.ResponsabileLavori.Email = notificaTelematica.DirettoreLavori.PersonaEmail;
                        notifica.ResponsabileLavori.Indirizzo = notificaTelematica.DirettoreLavori.Indirizzo;
                        notifica.ResponsabileLavori.Localita = notificaTelematica.DirettoreLavori.PersonaComune;
                        notifica.ResponsabileLavori.Provincia = notificaTelematica.DirettoreLavori.PersonaProvincia;
                    }

                    if (notificaTelematica.CoordinatoreSicurezzaProgettazione != null)
                    {
                        notifica.CoordinatoreSicurezzaProgettazione = new Persona();

                        notifica.CoordinatoreSicurezzaProgettazione.Cognome = notificaTelematica.CoordinatoreSicurezzaProgettazione.PersonaCognome;
                        notifica.CoordinatoreSicurezzaProgettazione.Nome = notificaTelematica.CoordinatoreSicurezzaProgettazione.PersonaNome;
                        notifica.CoordinatoreSicurezzaProgettazione.CodiceFiscale = notificaTelematica.CoordinatoreSicurezzaProgettazione.PersonaCodiceFiscale;
                        notifica.CoordinatoreSicurezzaProgettazione.Email = notificaTelematica.CoordinatoreSicurezzaProgettazione.PersonaEmail;
                        notifica.CoordinatoreSicurezzaProgettazione.Indirizzo = notificaTelematica.CoordinatoreSicurezzaProgettazione.Indirizzo;
                        notifica.CoordinatoreSicurezzaProgettazione.Localita = notificaTelematica.CoordinatoreSicurezzaProgettazione.PersonaComune;
                        notifica.CoordinatoreSicurezzaProgettazione.Provincia = notificaTelematica.CoordinatoreSicurezzaProgettazione.PersonaProvincia;
                    }

                    if (notificaTelematica.CoordinatoreSicurezzaRealizzazione != null)
                    {
                        notifica.CoordinatoreSicurezzaRealizzazione = new Persona();

                        notifica.CoordinatoreSicurezzaRealizzazione.Cognome = notificaTelematica.CoordinatoreSicurezzaRealizzazione.PersonaCognome;
                        notifica.CoordinatoreSicurezzaRealizzazione.Nome = notificaTelematica.CoordinatoreSicurezzaRealizzazione.PersonaNome;
                        notifica.CoordinatoreSicurezzaRealizzazione.CodiceFiscale = notificaTelematica.CoordinatoreSicurezzaRealizzazione.PersonaCodiceFiscale;
                        notifica.CoordinatoreSicurezzaRealizzazione.Email = notificaTelematica.CoordinatoreSicurezzaRealizzazione.PersonaEmail;
                        notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo = notificaTelematica.CoordinatoreSicurezzaRealizzazione.Indirizzo;
                        notifica.CoordinatoreSicurezzaRealizzazione.Localita = notificaTelematica.CoordinatoreSicurezzaRealizzazione.PersonaComune;
                        notifica.CoordinatoreSicurezzaRealizzazione.Provincia = notificaTelematica.CoordinatoreSicurezzaRealizzazione.PersonaProvincia;
                    }

                    notifica.Imprese = new ImpresaCollection();
                    foreach (TBridge.Cemi.Cpt.Type.Entities.SubappaltoNotificheTelematiche subTelematico in notificaTelematica.ImpreseAffidatarie)
                    {
                        Impresa impresa = new Impresa();
                        notifica.Imprese.Add(impresa);

                        if (!String.IsNullOrEmpty(subTelematico.ImpresaSelezionata.IdCassaEdile))
                        {
                            impresa.CodiceCassaEdileMilano = Int32.Parse(subTelematico.ImpresaSelezionata.IdCassaEdile);
                        }
                        impresa.CodiceFiscale = subTelematico.ImpresaSelezionata.CodiceFiscale;
                        impresa.Indirizzo = subTelematico.ImpresaSelezionata.Indirizzo;
                        impresa.Localita = subTelematico.ImpresaSelezionata.Comune;
                        impresa.PartitaIva = subTelematico.ImpresaSelezionata.PartitaIva;
                        impresa.Provincia = subTelematico.ImpresaSelezionata.Provincia;
                        impresa.RagioneSociale = subTelematico.ImpresaSelezionata.RagioneSociale;
                    }

                    foreach (TBridge.Cemi.Cpt.Type.Entities.SubappaltoNotificheTelematiche subTelematico in notificaTelematica.ImpreseEsecutrici)
                    {
                        Impresa impresa = new Impresa();
                        notifica.Imprese.Add(impresa);

                        if (!String.IsNullOrEmpty(subTelematico.ImpresaSelezionata.IdCassaEdile))
                        {
                            impresa.CodiceCassaEdileMilano = Int32.Parse(subTelematico.ImpresaSelezionata.IdCassaEdile);
                        }
                        impresa.CodiceFiscale = subTelematico.ImpresaSelezionata.CodiceFiscale;
                        impresa.Indirizzo = subTelematico.ImpresaSelezionata.Indirizzo;
                        impresa.Localita = subTelematico.ImpresaSelezionata.Comune;
                        impresa.PartitaIva = subTelematico.ImpresaSelezionata.PartitaIva;
                        impresa.Provincia = subTelematico.ImpresaSelezionata.Provincia;
                        impresa.RagioneSociale = subTelematico.ImpresaSelezionata.RagioneSociale;
                    }
                }
            }

            return notificheWS;
        }

        private Boolean VerificaPassword(String password)
        {
            if (password != ConfigurationManager.AppSettings["password"])
            {
                return false;
            }

            return true;
        }
        #endregion

        public List<String> EsportaProgressiviCaricati(String password)
        {
            if (VerificaPassword(password))
            {
                CptBusiness biz = new CptBusiness();

                return biz.GetProtocolliRegioneCaricati();
            }

            return null;
        }

        #endregion
    }
}