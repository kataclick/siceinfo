﻿using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace TBridge.Cemi.Cpt.NotificheExportService
{
    [ServiceContract]
    public interface INotificheExportService
    {
        [OperationContract]
        NotificaCollection EsportaNotifiche(String partitaIvaCodiceFiscale, String password);

        [OperationContract]
        List<String> EsportaProgressiviCaricati(String password);
    }
}