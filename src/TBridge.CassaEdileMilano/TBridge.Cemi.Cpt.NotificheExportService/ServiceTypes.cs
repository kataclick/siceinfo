﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TBridge.Cemi.Cpt.NotificheExportService
{
    [CollectionDataContract]
    public class NotificaCollection : List<Notifica>
    {
    }

    [DataContract]
    public class Notifica
    {
        [DataMember]
        public String Protocollo { get; set; }

        [DataMember]
        public IndirizzoCollection Indirizzi { get; set; }

        [DataMember]
        public String NaturaOpera { get; set; }

        [DataMember]
        public Decimal? AmmontareComplessivo { get; set; }

        [DataMember]
        public DateTime DataComunicazione { get; set; }

        [DataMember]
        public DateTime DataUltimoAggiornamento { get; set; }

        [DataMember]
        public Int32? NumeroImprese { get; set; }

        [DataMember]
        public Int32? NumeroMassimoLavoratori { get; set; }

        [DataMember]
        public Int32? NumeroLavoratoriAutonomi { get; set; }

        [DataMember]
        public Persona Committente { get; set; }

        [DataMember]
        public Persona ResponsabileLavori { get; set; }

        [DataMember]
        public Persona CoordinatoreSicurezzaProgettazione { get; set; }

        [DataMember]
        public Persona CoordinatoreSicurezzaRealizzazione { get; set; }

        [DataMember]
        public ImpresaCollection Imprese { get; set; }

        [DataMember]
        public String Note { get; set; }
    }

    [CollectionDataContract]
    public class IndirizzoCollection : List<Indirizzo>
    {
    }

    [DataContract]
    public class Indirizzo
    {
        [DataMember]
        public String IndirizzoDenominazione { get; set; }

        [DataMember]
        public String Provincia { get; set; }

        [DataMember]
        public String Localita { get; set; }

        [DataMember]
        public String Cap { get; set; }

        [DataMember]
        public DateTime? DataInizioLavori { get; set; }

        [DataMember]
        public String DurataDescrizione { get; set; }

        [DataMember]
        public Int32? DurataNumero { get; set; }

        [DataMember]
        public Int32? NumeroMassimoLavoratori { get; set; }
    }

    [DataContract]
    public class Persona
    {
        [DataMember]
        public String Cognome { get; set; }

        [DataMember]
        public String Nome { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String Indirizzo { get; set; }

        [DataMember]
        public String Provincia { get; set; }

        [DataMember]
        public String Localita { get; set; }

        [DataMember]
        public String Email { get; set; }
    }

    [CollectionDataContract]
    public class ImpresaCollection : List<Impresa>
    {
    }

    [DataContract]
    public class Impresa
    {
        [DataMember]
        public String RagioneSociale { get; set; }

        [DataMember]
        public String PartitaIva { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String Indirizzo { get; set; }

        [DataMember]
        public String Provincia { get; set; }

        [DataMember]
        public String Localita { get; set; }

        [DataMember]
        public Int32? CodiceCassaEdileMilano { get; set; }
    }
}