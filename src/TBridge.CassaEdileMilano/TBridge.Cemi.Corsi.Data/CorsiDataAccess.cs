using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Business;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Enums;
using TBridge.Cemi.Corsi.Type.Exceptions;
using TBridge.Cemi.Corsi.Type.Filters;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Corsi.Data
{
    public class CorsiDataAccess
    {
        private Database databaseCemi;

        public CorsiDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        public LocazioneCollection GetLocazioni()
        {
            LocazioneCollection locazioni = new LocazioneCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniSelectAll"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceNote = reader.GetOrdinal("note");
                    Int32 indiceEsistePianificazione = reader.GetOrdinal("esistePianificazione");

                    #endregion

                    while (reader.Read())
                    {
                        Locazione locazione = new Locazione();
                        locazioni.Add(locazione);

                        locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                            locazione.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            locazione.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            locazione.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceLatitudine))
                            locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        if (!reader.IsDBNull(indiceNote))
                            locazione.Note = reader.GetString(indiceNote);
                        locazione.EsistePianificazione = reader.GetBoolean(indiceEsistePianificazione);
                    }
                }
            }

            return locazioni;
        }

        public Boolean InsertLocazione(Locazione locazione)
        {
            Boolean res = false;

            if (locazione == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniInsert"))
                {
                    databaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, locazione.Indirizzo);
                    if (!String.IsNullOrEmpty(locazione.Comune))
                        databaseCemi.AddInParameter(comando, "@comune", DbType.String, locazione.Comune);
                    if (!String.IsNullOrEmpty(locazione.Provincia))
                        databaseCemi.AddInParameter(comando, "@provincia", DbType.String, locazione.Provincia);
                    if (!String.IsNullOrEmpty(locazione.Cap))
                        databaseCemi.AddInParameter(comando, "@cap", DbType.String, locazione.Cap);
                    if (locazione.Latitudine.HasValue)
                        databaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal, locazione.Latitudine);
                    if (locazione.Longitudine.HasValue)
                        databaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal, locazione.Longitudine);
                    if (!String.IsNullOrEmpty(locazione.Note))
                        databaseCemi.AddInParameter(comando, "@note", DbType.String, locazione.Note);

                    if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public CorsoCollection GetCorsiAll(Boolean conPianificazioni, Boolean pianificabili)
        {
            CorsoCollection corsi = new CorsoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiSelectAll"))
            {
                DatabaseCemi.AddInParameter(comando, "@conSchedulazioni", DbType.Boolean, conPianificazioni);
                DatabaseCemi.AddInParameter(comando, "@pianificabili", DbType.Boolean, pianificabili);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");
                    Int32 indiceVersione = reader.GetOrdinal("versione");
                    Int32 indiceDurataComplessiva = reader.GetOrdinal("durataComplessiva");
                    Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    Int32 indiceDescrizioneModulo = reader.GetOrdinal("descrizioneModulo");
                    Int32 indiceDurataModulo = reader.GetOrdinal("durataModulo");
                    Int32 indiceProgressivoModulo = reader.GetOrdinal("progressivoModulo");
                    Int32 indiceNumeroPianificazioni = reader.GetOrdinal("numeroPianificazioni");
                    Int32 indiceIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");

                    #endregion

                    Corso corso = null;

                    while (reader.Read())
                    {
                        Int32 idCorso = reader.GetInt32(indiceIdCorso);

                        if (corso == null || corso.IdCorso != idCorso)
                        {
                            corso = new Corso();
                            corsi.Add(corso);

                            corso.IdCorso = reader.GetInt32(indiceIdCorso);
                            corso.Codice = reader.GetString(indiceCodice);
                            corso.Descrizione = reader.GetString(indiceDescrizione);
                            if (!reader.IsDBNull(indiceVersione))
                            {
                                corso.Versione = reader.GetString(indiceVersione);
                            }
                            corso.DurataComplessiva = reader.GetInt16(indiceDurataComplessiva);
                            corso.NumeroPianificazioni = reader.GetInt32(indiceNumeroPianificazioni);
                            corso.IscrizioneLibera = reader.GetBoolean(indiceIscrizioneLibera);
                        }

                        Modulo modulo = new Modulo();
                        corso.Moduli.Add(modulo);

                        modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        modulo.Descrizione = reader.GetString(indiceDescrizioneModulo);
                        modulo.OreDurata = reader.GetInt16(indiceDurataModulo);
                        modulo.Progressivo = reader.GetInt16(indiceProgressivoModulo);
                    }
                }
            }

            return corsi;
        }

        public Corso GetCorsoByKey(Int32 idCorso)
        {
            Corso corso = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader 1

                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
                    Int32 indiceCodice = reader.GetOrdinal("codice");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");
                    Int32 indiceVersione = reader.GetOrdinal("versione");
                    Int32 indiceDurataComplessiva = reader.GetOrdinal("durataComplessiva");
                    Int32 indiceIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");
                    Int32 indiceReportSingolo = reader.GetOrdinal("nomeReportSingolo");
                    Int32 indiceReportMultiplo = reader.GetOrdinal("nomeReportMultipli");

                    #endregion

                    reader.Read();

                    // Corso
                    corso = new Corso();
                    corso.IdCorso = reader.GetInt32(indiceIdCorso);
                    corso.Codice = reader.GetString(indiceCodice);
                    corso.Descrizione = reader.GetString(indiceDescrizione);
                    if (!reader.IsDBNull(indiceVersione))
                    {
                        corso.Versione = reader.GetString(indiceVersione);
                    }
                    corso.DurataComplessiva = reader.GetInt16(indiceDurataComplessiva);
                    corso.IscrizioneLibera = reader.GetBoolean(indiceIscrizioneLibera);
                    corso.ReportAttestato = reader.GetString(indiceReportSingolo);
                    corso.ReportAttestati = reader.GetString(indiceReportMultiplo);

                    reader.NextResult();

                    #region Indici per reader 2

                    Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    Int32 indiceDescrizioneModulo = reader.GetOrdinal("descrizioneModulo");
                    Int32 indiceDurataModulo = reader.GetOrdinal("durataModulo");
                    Int32 indiceProgressivoModulo = reader.GetOrdinal("progressivoModulo");

                    #endregion

                    // Moduli
                    while (reader.Read())
                    {
                        Modulo modulo = new Modulo();
                        corso.Moduli.Add(modulo);

                        modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        modulo.Descrizione = reader.GetString(indiceDescrizioneModulo);
                        modulo.OreDurata = reader.GetInt16(indiceDurataModulo);
                        modulo.Progressivo = reader.GetInt16(indiceProgressivoModulo);
                    }
                }
            }

            return corso;
        }

        public Boolean InsertCorso(Corso corso)
        {
            Boolean res = false;

            if (corso == null)
            {
                throw new ArgumentNullException();
            }

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@codice", DbType.String, corso.Codice);
                            DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, corso.Descrizione);
                            if (!String.IsNullOrEmpty(corso.Versione))
                            {
                                DatabaseCemi.AddInParameter(comando, "@versione", DbType.String, corso.Versione);
                            }
                            DatabaseCemi.AddInParameter(comando, "@durata", DbType.Int16, corso.DurataComplessiva);
                            DatabaseCemi.AddInParameter(comando, "@iscrizioneLibera", DbType.Int16,
                                                        corso.IscrizioneLibera);
                            DatabaseCemi.AddOutParameter(comando, "@idCorso", DbType.Int32, 4);
                            DatabaseCemi.ExecuteNonQuery(comando, transaction);

                            corso.IdCorso = (Int32) DatabaseCemi.GetParameterValue(comando, "@idCorso");
                            if (corso.IdCorso > 0)
                            {
                                res = true;

                                foreach (Modulo modulo in corso.Moduli)
                                {
                                    if (!InsertModulo(corso.IdCorso.Value, modulo, transaction))
                                    {
                                        res = false;
                                        throw new Exception(
                                            String.Format("InsertModulo: Errore nell'inserimento di un modulo: {0}",
                                                          modulo.Descrizione));
                                    }
                                }

                                transaction.Commit();
                            }
                            else
                            {
                                throw new Exception(String.Format(
                                                        "InsertCorso: Errore nell'inserimento del corso: {0}",
                                                        corso.Descrizione));
                            }
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private Boolean InsertModulo(Int32 idCorso, Modulo modulo, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, modulo.Descrizione);
                DatabaseCemi.AddInParameter(comando, "@durata", DbType.Int16, modulo.OreDurata);
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);
                DatabaseCemi.AddInParameter(comando, "@progressivo", DbType.Int16, modulo.Progressivo);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
                else
                {
                    throw new Exception(String.Format("InsertModulo: Errore nell'inserimento di un modulo: {0}",
                                                      modulo.Descrizione));
                }
            }

            return res;
        }

        public ModuloCollection GetModuliConProgrammazioniFuture(Int32 idCorso, DateTime dataInizio, DateTime dataFine,
                                                                 Boolean? prenotabili, Boolean overBooking)
        {
            ModuloCollection moduli = new ModuloCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectByCorsoKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, dataInizio);
                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataFine);
                DatabaseCemi.AddInParameter(comando, "@registroConfermato", DbType.Boolean, false);
                DatabaseCemi.AddInParameter(comando, "@overBooking", DbType.Boolean, overBooking);
                if (prenotabili.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@prenotabili", DbType.Boolean, prenotabili.Value);
                }

                Modulo moduloCorr = null;

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    Int32 indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    Int32 indiceInizio = reader.GetOrdinal("inizio");
                    Int32 indiceFine = reader.GetOrdinal("fine");
                    Int32 indicePartecipanti = reader.GetOrdinal("partecipanti");
                    Int32 indiceMaxPartecipanti = reader.GetOrdinal("maxPartecipanti");
                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
                    Int32 indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    Int32 indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    Int32 indiceIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");
                    Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    Int32 indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    Int32 indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    Int32 indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        Int32 idModulo = reader.GetInt32(indiceIdModulo);

                        if (moduloCorr == null || moduloCorr.IdModulo != idModulo)
                        {
                            moduloCorr = new Modulo();
                            moduloCorr.Programmazioni = new ProgrammazioneModuloCollection();
                            moduli.Add(moduloCorr);

                            // Modulo
                            moduloCorr.IdModulo = reader.GetInt32(indiceIdModulo);
                            moduloCorr.Descrizione = reader.GetString(indiceModuloDescrizione);
                            moduloCorr.OreDurata = reader.GetInt16(indiceModuloDurata);
                            moduloCorr.Progressivo = reader.GetInt16(indiceModuloProgressivo);
                        }

                        ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                        moduloCorr.Programmazioni.Add(programmazione);

                        programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                        programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);
                        programmazione.Partecipanti = reader.GetInt32(indicePartecipanti);
                        programmazione.MaxPartecipanti = reader.GetInt32(indiceMaxPartecipanti);

                        // Corso
                        programmazione.Corso = new Corso();
                        programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);
                        programmazione.Corso.IscrizioneLibera = reader.GetBoolean(indiceIscrizioneLibera);

                        // Modulo
                        programmazione.Modulo = new Modulo();
                        programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                        // Locazione
                        programmazione.Locazione = new Locazione();
                        programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }
                    }
                }
            }

            return moduli;
        }


        protected Boolean InsertPartecipazioneModulo(PartecipazioneModulo partecipazioneModulo, Int32 idPartecipazione,
                                                     DbTransaction transaction, Lavoratore lavoratore,
                                                     Boolean overBooking)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPartecipazioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);
                DatabaseCemi.AddInParameter(comando, "@idPianificazioneModulo", DbType.Int32,
                                            partecipazioneModulo.Programmazione.IdProgrammazioneModulo);
                DatabaseCemi.AddInParameter(comando, "@overBooking", DbType.Int32, overBooking);

                if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore);
                }
                else
                {
                    if (lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica)
                    {
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String,
                                                    lavoratore.CodiceFiscale);
                    }
                }
                DatabaseCemi.AddOutParameter(comando, "@risultato", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando, transaction);
                Int32 risultato = (Int32) DatabaseCemi.GetParameterValue(comando, "@risultato");

                switch (risultato)
                {
                    case -1:
                        throw new LavoratoreGiaIscrittoException();
                        break;
                    case -2:
                        throw new DisponibilitaEsauritaException();
                        break;
                    case 1:
                        res = true;
                        break;
                    default:
                        throw new Exception("Errore nell'inserimento di una partecipazione modulo");
                        break;
                }
            }

            return res;
        }

        public bool InsertPartecipazione(Partecipazione partecipazione, Boolean overBooking)
        {
            Boolean res = true;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Devo controllare che il lavoratore e l'impresa siano presenti nel db
                        if (!partecipazione.Lavoratore.IdLavoratore.HasValue)
                        {
                            // Inserisco nel database il lavoratore
                            InsertLavoratore(partecipazione.Lavoratore, transaction);
                        }

                        if (!partecipazione.Impresa.IdImpresa.HasValue)
                        {
                            // Inserisco nel database l'impresa
                            InsertImpresa(partecipazione.Impresa, transaction);
                        }

                        using (
                            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32,
                                                        partecipazione.Corso.IdCorso.Value);
                            DatabaseCemi.AddInParameter(comando, "@prenotazioneImpresa", DbType.Boolean,
                                                        partecipazione.PrenotazioneImpresa);
                            DatabaseCemi.AddInParameter(comando, "@prenotazioneConsulente", DbType.Boolean,
                                                        partecipazione.PrenotazioneConsulente);
                            if (partecipazione.IdConsulente.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32,
                                                            partecipazione.IdConsulente);
                            }
                            if (partecipazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                            partecipazione.Lavoratore.IdLavoratore);
                            }
                            else
                            {
                                if (partecipazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idCorsiLavoratore", DbType.Int32,
                                                                partecipazione.Lavoratore.IdLavoratore);
                                }
                            }
                            if (partecipazione.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                                            partecipazione.Impresa.IdImpresa);
                            }
                            else
                            {
                                if (partecipazione.Impresa.TipoImpresa == TipologiaImpresa.Anagrafica)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idCorsiImpresa", DbType.Int32,
                                                                partecipazione.Impresa.IdImpresa);
                                }
                            }
                            if (partecipazione.Lavoratore.PrimaEsperienza.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@primaEsperienza", DbType.Boolean,
                                                                partecipazione.Lavoratore.PrimaEsperienza.Value);
                            }
                            if (partecipazione.Lavoratore.DataAssunzione.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                                                                partecipazione.Lavoratore.DataAssunzione.Value);
                            }

                            DatabaseCemi.AddOutParameter(comando, "@idPartecipazione", DbType.Int32, 4);

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                partecipazione.IdPartecipazione =
                                    (Int32) DatabaseCemi.GetParameterValue(comando, "@idPartecipazione");

                                foreach (
                                    PartecipazioneModulo partecipazioneModulo in partecipazione.PartecipazioneModuli)
                                {
                                    if (
                                        !InsertPartecipazioneModulo(partecipazioneModulo,
                                                                    partecipazione.IdPartecipazione.Value, transaction,
                                                                    partecipazione.Lavoratore, overBooking))
                                    {
                                        res = false;
                                        throw new Exception("Errore nell'inserimento di una partecipazione modulo");
                                    }
                                }
                            }
                            else
                            {
                                res = false;
                            }
                        }

                        if (res)
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            throw new Exception(
                                "InsertPartecipazioni: l'inserimento di una partecipazione non � andato a buon fine");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        res = false;
                        throw;
                    }
                }
            }

            return true;
        }

        public ProgrammazioneModuloCollection GetPartecipazione(PartecipazioneFilter filtro)
        {
            ProgrammazioneModuloCollection programmazioni = null;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectPartecipazioni")
                )
            {
                if (filtro.IdLavoratore.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    programmazioni = TrasformaReaderInProgrammazioneModuloCollection(reader);
                }
            }

            return programmazioni;
        }

        public ProgrammazioneModuloCollection GetPartecipazione(int idPartecipazione)
        {
            ProgrammazioneModuloCollection programmazioni = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    programmazioni = TrasformaReaderInProgrammazioneModuloCollection(reader);
                }
            }

            return programmazioni;
        }

        private ProgrammazioneModuloCollection TrasformaReaderInProgrammazioneModuloCollection(IDataReader reader)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            #region Indici per il reader

            Int32 indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
            Int32 indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
            Int32 indiceInizio = reader.GetOrdinal("inizio");
            Int32 indiceFine = reader.GetOrdinal("fine");
            Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
            Int32 indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
            Int32 indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
            Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
            Int32 indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
            Int32 indiceModuloDurata = reader.GetOrdinal("moduloDurata");
            Int32 indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
            Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
            Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
            Int32 indiceComune = reader.GetOrdinal("comune");
            Int32 indiceProvincia = reader.GetOrdinal("provincia");
            Int32 indiceCap = reader.GetOrdinal("cap");
            Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
            Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
            Int32 indiceNote = reader.GetOrdinal("note");

            #endregion

            while (reader.Read())
            {
                ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                programmazioni.Add(programmazione);

                programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);

                // Corso
                programmazione.Corso = new Corso();
                programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);

                // Modulo
                programmazione.Modulo = new Modulo();
                programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                // Locazione
                programmazione.Locazione = new Locazione();
                programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                if (!reader.IsDBNull(indiceComune))
                {
                    programmazione.Locazione.Comune = reader.GetString(indiceComune);
                }
                if (!reader.IsDBNull(indiceProvincia))
                {
                    programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                }
                if (!reader.IsDBNull(indiceCap))
                {
                    programmazione.Locazione.Cap = reader.GetString(indiceCap);
                }
                if (!reader.IsDBNull(indiceLatitudine))
                {
                    programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                }
                if (!reader.IsDBNull(indiceLongitudine))
                {
                    programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                }
                if (!reader.IsDBNull(indiceNote))
                {
                    programmazione.Locazione.Note = reader.GetString(indiceNote);
                }
            }

            return programmazioni;
        }

        public Boolean UpdatePartecipazioneModuloPresenza(Int32 idPartecipazioneModulo, Boolean presente)
        {
            Boolean res = false;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPartecipazioniUpdatePresenza")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazioneModulo", DbType.Int32, idPartecipazioneModulo);
                DatabaseCemi.AddInParameter(comando, "@presente", DbType.Boolean, presente);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean DeleteCorso(int idCorso)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = true;
            }

            return res;
        }

        public PartecipazioneModuloCollection GetPartecipazioniByLavoratoreImpresa(
            PartecipazioneLavoratoreImpresaFilter filtro)
        {
            PartecipazioneModuloCollection partecipazioni = new PartecipazioneModuloCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectByLavoratoreImpresa"))
            {
                if (!String.IsNullOrEmpty(filtro.LavoratoreCognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@lavCognome", DbType.String, filtro.LavoratoreCognome);
                }
                if (!String.IsNullOrEmpty(filtro.LavoratoreNome))
                {
                    DatabaseCemi.AddInParameter(comando, "@lavNome", DbType.String, filtro.LavoratoreNome);
                }
                if (!String.IsNullOrEmpty(filtro.LavoratoreCodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@lavCodFisc", DbType.String, filtro.LavoratoreCodiceFiscale);
                }
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!String.IsNullOrEmpty(filtro.ImpresaRagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@impRagSoc", DbType.String, filtro.ImpresaRagioneSociale);
                }
                if (!String.IsNullOrEmpty(filtro.ImpresaIvaFisc))
                {
                    DatabaseCemi.AddInParameter(comando, "@impIvaFisc", DbType.String, filtro.ImpresaIvaFisc);
                }
                if (filtro.IdConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    Int32 indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    Int32 indiceInizio = reader.GetOrdinal("inizio");
                    Int32 indiceFine = reader.GetOrdinal("fine");
                    Int32 indiceConfermaPresenze = reader.GetOrdinal("registroConfermato");
                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
                    Int32 indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    Int32 indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    Int32 indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    Int32 indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    Int32 indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceNote = reader.GetOrdinal("note");
                    Int32 indicePresente = reader.GetOrdinal("presente");
                    Int32 indiceAttestato = reader.GetOrdinal("attestatoRilasciabile");

                    Int32 indiceIdLavoratore = reader.GetOrdinal("lavoratoreIdLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("lavoratoreCognome");
                    Int32 indiceNome = reader.GetOrdinal("lavoratoreNome");
                    Int32 indiceDataNascita = reader.GetOrdinal("lavoratoreDataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("lavoratoreCodiceFiscale");
                    Int32 indiceCorsiIdLavoratore = reader.GetOrdinal("corsiLavoratoreIdLavoratore");
                    Int32 indiceCorsiCognome = reader.GetOrdinal("corsiLavoratoreCognome");
                    Int32 indiceCorsiNome = reader.GetOrdinal("corsiLavoratoreNome");
                    Int32 indiceCorsiDataNascita = reader.GetOrdinal("corsiLavoratoreDataNascita");
                    Int32 indiceCorsiCodiceFiscale = reader.GetOrdinal("corsiLavoratoreCodiceFiscale");

                    Int32 indiceIdImpresa = reader.GetOrdinal("impresaIdImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    Int32 indiceCorsiIdImpresa = reader.GetOrdinal("corsiImpresaIdImpresa");
                    Int32 indiceCorsiRagioneSociale = reader.GetOrdinal("corsiImpresaRagioneSociale");

                    #endregion

                    while (reader.Read())
                    {
                        PartecipazioneModulo partecipazione = new PartecipazioneModulo();
                        partecipazioni.Add(partecipazione);

                        partecipazione.Programmazione = new ProgrammazioneModulo();

                        partecipazione.Programmazione.IdProgrammazioneModulo =
                            reader.GetInt32(indiceIdModuliProgrammazione);
                        partecipazione.Programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        partecipazione.Programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        partecipazione.Programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);

                        // Corso
                        partecipazione.Programmazione.Corso = new Corso();
                        partecipazione.Programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        partecipazione.Programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        partecipazione.Programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);

                        // Modulo
                        partecipazione.Programmazione.Modulo = new Modulo();
                        partecipazione.Programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        partecipazione.Programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        partecipazione.Programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        partecipazione.Programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);
                        partecipazione.Programmazione.PresenzeConfermate = reader.GetBoolean(indiceConfermaPresenze);

                        // Locazione
                        partecipazione.Programmazione.Locazione = new Locazione();
                        partecipazione.Programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        partecipazione.Programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            partecipazione.Programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            partecipazione.Programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            partecipazione.Programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            partecipazione.Programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            partecipazione.Programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            partecipazione.Programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }

                        #region Lavoratore

                        partecipazione.Lavoratore = new Lavoratore();

                        partecipazione.Lavoratore.Presente = reader.GetBoolean(indicePresente);
                        partecipazione.Lavoratore.Attestato = reader.GetBoolean(indiceAttestato);

                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            partecipazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            partecipazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                            partecipazione.Lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                            {
                                partecipazione.Lavoratore.Nome = reader.GetString(indiceNome);
                            }
                            if (!reader.IsDBNull(indiceDataNascita))
                            {
                                partecipazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                partecipazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                        }
                        else
                        {
                            if (!reader.IsDBNull(indiceCorsiIdLavoratore))
                            {
                                partecipazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                                partecipazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceCorsiIdLavoratore);
                                partecipazione.Lavoratore.Cognome = reader.GetString(indiceCorsiCognome);
                                if (!reader.IsDBNull(indiceCorsiNome))
                                {
                                    partecipazione.Lavoratore.Nome = reader.GetString(indiceCorsiNome);
                                }
                                if (!reader.IsDBNull(indiceCorsiDataNascita))
                                {
                                    partecipazione.Lavoratore.DataNascita = reader.GetDateTime(indiceCorsiDataNascita);
                                }
                                if (!reader.IsDBNull(indiceCorsiCodiceFiscale))
                                {
                                    partecipazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCorsiCodiceFiscale);
                                }
                            }
                        }

                        #endregion

                        #region Impresa

                        partecipazione.Impresa = new Impresa();
                        partecipazione.Impresa = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            partecipazione.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            partecipazione.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            partecipazione.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        else
                        {
                            partecipazione.Impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                            partecipazione.Impresa.IdImpresa = reader.GetInt32(indiceCorsiIdImpresa);
                            partecipazione.Impresa.RagioneSociale = reader.GetString(indiceCorsiRagioneSociale);
                        }

                        #endregion
                    }
                }
            }

            return partecipazioni;
        }

        public Boolean DeletePartecipazione(Int32 idPartecipazioneModulo)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazioneModulo", DbType.Int32, idPartecipazioneModulo);

                if (DatabaseCemi.ExecuteNonQuery(comando) > 0)
                {
                    res = true;
                }
            }

            return res;
        }

        public Locazione GetLocazioneByKey(Int32 idLocazione)
        {
            Locazione locazione = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, idLocazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        locazione = new Locazione();

                        locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                            locazione.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            locazione.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            locazione.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceLatitudine))
                            locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        if (!reader.IsDBNull(indiceNote))
                            locazione.Note = reader.GetString(indiceNote);
                    }
                }
            }

            return locazione;
        }

        public EstrazioneFormedilImpresaCollection EstrazioneFormedilImprese(EstrazioneFormedilFilter filtro)
        {
            EstrazioneFormedilImpresaCollection estrazioni = new EstrazioneFormedilImpresaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiEstrazioneFormedilImprese"))
            {
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceCorso = reader.GetOrdinal("corso");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceTipoContratto = reader.GetOrdinal("tipoContratto");
                    Int32 indiceTipoIscrizione = reader.GetOrdinal("tipoIscrizione");
                    Int32 indiceNumeroDipendenti = reader.GetOrdinal("numeroDipendenti");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceNumeroLavoratori = reader.GetOrdinal("numeroLavoratori");
                    Int32 indiceDateCorso = reader.GetOrdinal("dateCorso");

                    #endregion

                    while (reader.Read())
                    {
                        EstrazioneFormedilImpresa estrazione = new EstrazioneFormedilImpresa();
                        estrazioni.Add(estrazione);

                        estrazione.Corso = reader.GetString(indiceCorso);
                        estrazione.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            estrazione.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceTipoContratto))
                        {
                            estrazione.TipoContratto = reader.GetInt32(indiceTipoContratto);
                        }
                        if (!reader.IsDBNull(indiceTipoIscrizione))
                        {
                            estrazione.TipoIscrizione = reader.GetInt32(indiceTipoIscrizione);
                        }
                        if (!reader.IsDBNull(indiceNumeroDipendenti))
                        {
                            estrazione.NumeroDipendenti = reader.GetInt32(indiceNumeroDipendenti);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            estrazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            estrazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        estrazione.NumeroLavoratori = reader.GetInt32(indiceNumeroLavoratori);
                        estrazione.DateCorso = reader.GetString(indiceDateCorso);
                    }
                }
            }

            return estrazioni;
        }

        public EstrazioneFormedilLavoratoreCollection EstrazioneFormedilLavoratori(EstrazioneFormedilFilter filtro)
        {
            EstrazioneFormedilLavoratoreCollection estrazioni = new EstrazioneFormedilLavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiEstrazioneFormedilLavoratori"))
            {
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceCorso = reader.GetOrdinal("corso");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceSesso = reader.GetOrdinal("sesso");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceCittadinanza = reader.GetOrdinal("cittadinanza");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceProvinciaResidenza = reader.GetOrdinal("provinciaResidenza");
                    Int32 indiceTitoloStudio = reader.GetOrdinal("titoloDiStudio");
                    Int32 indiceAzienda = reader.GetOrdinal("azienda");
                    Int32 indiceDateCorso = reader.GetOrdinal("dateCorso");

                    #endregion

                    while (reader.Read())
                    {
                        EstrazioneFormedilLavoratore estrazione = new EstrazioneFormedilLavoratore();
                        estrazioni.Add(estrazione);

                        estrazione.Corso = reader.GetString(indiceCorso);
                        estrazione.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            estrazione.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            estrazione.Sesso = reader.GetInt32(indiceSesso);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            estrazione.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceCittadinanza))
                        {
                            estrazione.Cittadinanza = reader.GetString(indiceCittadinanza);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            estrazione.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaResidenza))
                        {
                            estrazione.ProvinciaResidenza = reader.GetString(indiceProvinciaResidenza);
                        }
                        if (!reader.IsDBNull(indiceTitoloStudio))
                        {
                            estrazione.TitoloDiStudio = reader.GetInt32(indiceTitoloStudio);
                        }
                        if (!reader.IsDBNull(indiceAzienda))
                        {
                            estrazione.Azienda = reader.GetString(indiceAzienda);
                        }
                        estrazione.DateCorso = reader.GetString(indiceDateCorso);
                    }
                }
            }

            return estrazioni;
        }

        public Boolean EsisteImpresaConStessaIvaFisc(String partitaIva, String codiceFiscale)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiEsisteImpresaConIvaFisc"))
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@esiste", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = (Boolean) DatabaseCemi.GetParameterValue(comando, "@esiste");
            }

            return res;
        }

        public Boolean EsisteLavoratoreConStessoCodiceFiscale(String codiceFiscale)
        {
            Boolean res = false;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiEsisteLavoratoreConCodiceFiscale"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@esiste", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = (Boolean) DatabaseCemi.GetParameterValue(comando, "@esiste");
            }

            return res;
        }

        public bool DeleteLocazione(int idLocazione)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, idLocazione);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean LavoratoreSeguito16Ore(String codiceFiscale)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectByCodiceFiscaleFor8Ore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, -1);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscaleP", DbType.String, codiceFiscale);

                String date = DatabaseCemi.ExecuteScalar(comando).ToString();
                if (!String.IsNullOrEmpty(date))
                {
                    res = true;
                }
            }

            return res;
        }

        #region Gestione Prestazioni domande corsi

        /// <summary>
        /// Ritorna l'elenco delle prestazioni domande corsi generate (filtrate)
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public PrestazioniDomandeCorsiCollection GetPrestazioniDomandeCorsi(PrestazioneDomandaCorsoFilter filtro)
        {
            PrestazioneDomandaCorso domanda = null;
            PrestazioniDomandeCorsiCollection domande = new PrestazioniDomandeCorsiCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandeSelect"))
            {
                if (filtro != null)
                {
                    if (filtro.IdCorsiPrestazioneDomanda.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idCorsiPrestazioneDomanda", DbType.Int32,
                                                    filtro.IdCorsiPrestazioneDomanda.Value);
                    if (filtro.IdLavoratore.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                    if (!string.IsNullOrEmpty(filtro.Cognome))
                        DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                    if (!string.IsNullOrEmpty(filtro.Nome))
                        DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                    if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);

                    if (!string.IsNullOrEmpty(filtro.IdTipoStatoPrestazione))
                        DatabaseCemi.AddInParameter(comando, "@idTipoStatoPrestazione", DbType.String,
                                                    filtro.IdTipoStatoPrestazione);

                    if (filtro.CarpaPrepagataPresente.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@CP", DbType.Boolean, filtro.CarpaPrepagataPresente.Value);
                    if (filtro.CodiceFiscalePresente.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@CF", DbType.Boolean, filtro.CodiceFiscalePresente.Value);
                    if (filtro.DenunciaPresente.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@denuncia", DbType.Boolean, filtro.DenunciaPresente.Value);
                    if (filtro.Iscritto.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@iscritto", DbType.Boolean, filtro.Iscritto.Value);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 iIdCorsiPrestazioneDomanda = reader.GetOrdinal("idCorsiPrestazioneDomanda");
                    Int32 iIdCorsiPartecipazione = reader.GetOrdinal("idCorsiPartecipazione");
                    Int32 iIdLavoratoreBeneficiario = reader.GetOrdinal("idLavoratoreBeneficiario");
                    Int32 iCognome = reader.GetOrdinal("cognome");
                    Int32 iNome = reader.GetOrdinal("nome");
                    Int32 iCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 iDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 iCodiceFiscaleUnico = reader.GetOrdinal("codiceFiscaleUnico");
                    Int32 iDenunciaPresente = reader.GetOrdinal("denunciaPresente");
                    Int32 iCartaPrepagataPresente = reader.GetOrdinal("cartaPrepagataPresente");
                    Int32 iDataLiquidazione = reader.GetOrdinal("dataLiquidazione");
                    Int32 iDataConferma = reader.GetOrdinal("dataConferma");
                    Int32 iIdTipoStatoPrestazione = reader.GetOrdinal("idTipoStatoPrestazione");
                    Int32 iDescrizioneStatoPrestazione = reader.GetOrdinal("descrizioneStatoPrestazione");
                    Int32 iInizioCorso = reader.GetOrdinal("inizioCorso");
                    Int32 iFineCorso = reader.GetOrdinal("fineCorso");

                    Int32 iNumeroProtocolloPrestazione = reader.GetOrdinal("numeroProtocolloPrestazione");
                    Int32 iProtocolloPrestazione = reader.GetOrdinal("protocolloPrestazione");
                    Int32 iPosizioneValida = reader.GetOrdinal("posizioneValida");

                    #endregion

                    while (reader.Read())
                    {
                        domanda = new PrestazioneDomandaCorso();

                        domanda.IdCorsiPrestazioneDomanda = reader.GetInt32(iIdCorsiPrestazioneDomanda);

                        if (!reader.IsDBNull(iIdLavoratoreBeneficiario))
                            domanda.IdLavoratore = reader.GetInt32(iIdLavoratoreBeneficiario);

                        domanda.Cognome = reader.GetString(iCognome);
                        domanda.Nome = reader.GetString(iNome);
                        domanda.DataNascita = reader.GetDateTime(iDataNascita);
                        if (!reader.IsDBNull(iCodiceFiscale))
                            domanda.CodiceFiscale = reader.GetString(iCodiceFiscale);

                        domanda.CodiceFiscaleUnico = reader.GetBoolean(iCodiceFiscaleUnico);
                        domanda.CartaPrepagataPresente = reader.GetBoolean(iCartaPrepagataPresente);
                        domanda.DenunciaPresente = reader.GetBoolean(iDenunciaPresente);

                        domanda.PosizioneValida = reader.GetBoolean(iPosizioneValida);

                        if (!reader.IsDBNull(iDataLiquidazione))
                            domanda.DataLiquidazione = reader.GetDateTime(iDataLiquidazione);
                        if (!reader.IsDBNull(iDataConferma))
                            domanda.DataConferma = reader.GetDateTime(iDataConferma);

                        if (!reader.IsDBNull(iInizioCorso))
                            domanda.InizioCorso = reader.GetDateTime(iInizioCorso);
                        if (!reader.IsDBNull(iFineCorso))
                            domanda.FineCorso = reader.GetDateTime(iFineCorso);

                        if (!reader.IsDBNull(iIdTipoStatoPrestazione))
                        {
                            domanda.Stato = new StatoDomanda(
                                reader.GetString(iIdTipoStatoPrestazione),
                                reader.GetString(iDescrizioneStatoPrestazione)
                                );
                        }

                        if (!reader.IsDBNull(iNumeroProtocolloPrestazione))
                            domanda.NumeroProtocolloPrestazione = reader.GetInt32(iNumeroProtocolloPrestazione);
                        if (!reader.IsDBNull(iProtocolloPrestazione))
                            domanda.ProtocolloPrestazione = reader.GetInt32(iProtocolloPrestazione);

                        domande.Add(domanda);
                    }
                }
            }

            return domande;
        }

        /// <summary>
        /// Aggiorna lo stato di una do
        /// </summary>
        /// <param name="idCorsiPrestazioneDomanda"></param>
        /// <param name="idTipoStatoPrestazione"></param>
        /// <param name="idUtente"></param>
        public Boolean AggiornaStatoPrestazioneDomandaCorso(int idCorsiPrestazioneDomanda, string idTipoStatoPrestazione,
                                                            int idUtente)
        {
            Boolean res = false;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandeAggiornaStato")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idCorsiPrestazioneDomanda", DbType.Int32,
                                            idCorsiPrestazioneDomanda);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@idTipoStatoPrestazione", DbType.String, idTipoStatoPrestazione);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public StatoDomandaCollection GetStatiDomanda()
        {
            StatoDomandaCollection stati = new StatoDomandaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniStatiDomandaSelect"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    int indiceIdStato = reader.GetOrdinal("idPrestazioniStatoDomanda");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        StatoDomanda stato = new StatoDomanda();
                        stati.Add(stato);

                        stato.IdStato = reader.GetString(indiceIdStato);
                        stato.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return stati;
        }

        /// <summary>
        /// Permette l'aggiornamento del beneficiario della prestazione 
        /// </summary>
        /// <returns></returns>
        public Boolean AggiornaLavoratoreBeneficiarioDomandaPrestazione(int idCorsiPrestazioneDomanda, int idLavoratore,
                                                                        int idUtente)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandeAggiornaLavoratore")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@idCorsiPrestazioneDomanda", DbType.Int32,
                                            idCorsiPrestazioneDomanda);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Get Lookup Collection

        public TitoloDiStudioCollection GetTitoliDiStudioAll()
        {
            TitoloDiStudioCollection titoliStudio = new TitoloDiStudioCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiTitoliDiStudioSelectAll"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTitoloStudio = reader.GetOrdinal("idCorsiTitoloDiStudio");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TitoloDiStudio titoloStudio = new TitoloDiStudio();
                        titoliStudio.Add(titoloStudio);

                        titoloStudio.IdTitoloStudio = reader.GetInt32(indiceIdTitoloStudio);
                        titoloStudio.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return titoliStudio;
        }

        public TipoIscrizioneCollection GetTipiIscrizioneAll()
        {
            TipoIscrizioneCollection tipiIscrizione = new TipoIscrizioneCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiTipiIscrizioneSelectAll"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTipoIscrizione = reader.GetOrdinal("idCorsiTipoIscrizione");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoIscrizione tipoIscrizione = new TipoIscrizione();
                        tipiIscrizione.Add(tipoIscrizione);

                        tipoIscrizione.IdTipoIscrizione = reader.GetInt32(indiceIdTipoIscrizione);
                        tipoIscrizione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiIscrizione;
        }

        public TipoContrattoCollection GetTipiContrattoAll()
        {
            TipoContrattoCollection tipiContratto = new TipoContrattoCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiTipiContrattoSelectAll"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdTipoContratto = reader.GetOrdinal("idCorsiTipoContratto");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoContratto tipoContratto = new TipoContratto();
                        tipiContratto.Add(tipoContratto);

                        tipoContratto.IdTipoContratto = reader.GetInt32(indiceIdTipoContratto);
                        tipoContratto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiContratto;
        }

        #endregion

        #region Gestione Lavoratori Impresa

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriSiceNewSelect"))
            {
                if (!String.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!String.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (filtro.DataNascita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                }
                if (!String.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceComuneNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    Int32 indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indiceIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    Int32 indiceIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    Int32 indiceOreDenunciate = reader.GetOrdinal("oreDenunciate");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.i
                        lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.IndirizzoDenominazione = reader.GetString(indiceIndirizzoDenominazione);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoComune))
                        {
                            lavoratore.IndirizzoComune = reader.GetString(indiceIndirizzoComune);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoProvincia))
                        {
                            lavoratore.IndirizzoProvincia = reader.GetString(indiceIndirizzoProvincia);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoCap))
                        {
                            lavoratore.IndirizzoCap = reader.GetString(indiceIndirizzoCap);
                        }

                        lavoratore.OreDenunciate = reader.GetInt32(indiceOreDenunciate);
                    }
                }
            }

            return lavoratori;
        }

        public LavoratoreCollection GetLavoratoriAnagraficaCondivisa(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriAnagraficaCondivisaSelect"))
            {
                if (!String.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!String.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (filtro.DataNascita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                }
                if (!String.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                DatabaseCemi.AddInParameter(comando, "@inForza", DbType.Boolean, filtro.InForza);
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("codiceCassaEdile");
                    Int32 indiceIdAnagraficaCondivisa = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceComuneNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    Int32 indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indiceIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    Int32 indiceIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    Int32 indiceOreDenunciate = reader.GetOrdinal("oreDenunciate");
                    Int32 indiceIdGestore = reader.GetOrdinal("idEnte");
                    Int32 indiceGestore = reader.GetOrdinal("fonte");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.i
                        //lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            //lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        //else
                        //{
                        //    lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        //}
                        lavoratore.IdAnagraficaCondivisa = reader.GetInt32(indiceIdAnagraficaCondivisa);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.IndirizzoDenominazione = reader.GetString(indiceIndirizzoDenominazione);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoComune))
                        {
                            lavoratore.IndirizzoComune = reader.GetString(indiceIndirizzoComune);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoProvincia))
                        {
                            lavoratore.IndirizzoProvincia = reader.GetString(indiceIndirizzoProvincia);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoCap))
                        {
                            lavoratore.IndirizzoCap = reader.GetString(indiceIndirizzoCap);
                        }

                        lavoratore.OreDenunciate = reader.GetInt32(indiceOreDenunciate);

                        Int32 idEnte = reader.GetInt32(indiceIdGestore);
                        if (idEnte == 1)
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        }
                        lavoratore.Gestore = reader.GetString(indiceGestore);
                    }
                }
            }

            return lavoratori;
        }

        public Lavoratore GetLavoratoreAnagraficaCondivisaByKey(Int32 idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriAnagraficaCondivisaSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("codiceCassaEdile");
                    Int32 indiceIdAnagraficaCondivisa = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceSesso = reader.GetOrdinal("sesso");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indicePaeseNascita = reader.GetOrdinal("idNazionalita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceComuneNascita = reader.GetOrdinal("luogoNascita");
                    Int32 indiceCodiceCatastaleNascita = reader.GetOrdinal("codiceCatastaleNascita");
                    Int32 indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    Int32 indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    Int32 indiceIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    Int32 indiceIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    Int32 indiceOreDenunciate = reader.GetOrdinal("oreDenunciate");
                    Int32 indiceIdGestore = reader.GetOrdinal("idEnte");
                    Int32 indiceGestore = reader.GetOrdinal("fonte");

                    #endregion

                    if (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        //lavoratore.i
                        //lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        lavoratore.IdAnagraficaCondivisa = reader.GetInt32(indiceIdAnagraficaCondivisa);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            String sesso = reader.GetString(indiceSesso);
                            if (sesso.Length == 1)
                            {
                                lavoratore.Sesso = sesso[0];
                            }
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indicePaeseNascita))
                        {
                            lavoratore.PaeseNascita = reader.GetString(indicePaeseNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceCatastaleNascita))
                        {
                            lavoratore.CodiceCatastaleComuneNascita = reader.GetString(indiceCodiceCatastaleNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.IndirizzoDenominazione = reader.GetString(indiceIndirizzoDenominazione);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoComune))
                        {
                            lavoratore.IndirizzoComune = reader.GetString(indiceIndirizzoComune);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoProvincia))
                        {
                            lavoratore.IndirizzoProvincia = reader.GetString(indiceIndirizzoProvincia);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoCap))
                        {
                            lavoratore.IndirizzoCap = reader.GetString(indiceIndirizzoCap);
                        }

                        lavoratore.OreDenunciate = reader.GetInt32(indiceOreDenunciate);

                        Int32 idEnte = reader.GetInt32(indiceIdGestore);
                        if (idEnte == 1)
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        }
                        lavoratore.Gestore = reader.GetString(indiceGestore);
                    }
                }
            }

            return lavoratore;
        }

        public Lavoratore GetLavoratoreSiceNewByKey(Int32 idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoreSiceNewSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratore;
        }

        private void InsertImpresa(Impresa impresa, DbTransaction transaction)
        {
            Common commonBiz = new Common();
            commonBiz.InsertImpresa(impresa, transaction);
            InsertImpresaDatiAggiuntivi(impresa, transaction);
        }

        private Boolean InsertImpresaDatiAggiuntivi(Impresa impresa, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseDatiAggiuntiviInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                if (impresa.TipoContratto != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipoContratto", DbType.Int32,
                                                impresa.TipoContratto.IdTipoContratto);
                }
                if (impresa.TipoIscrizione != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipoIscrizione", DbType.Int32,
                                                impresa.TipoIscrizione.IdTipoIscrizione);
                }
                if (impresa.NumeroDipendenti.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@numeroDipendenti", DbType.Int32, impresa.NumeroDipendenti);
                }
                if (!String.IsNullOrEmpty(impresa.ProvinciaSedeLegale))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaSedeLegale", DbType.String,
                                                impresa.ProvinciaSedeLegale);
                }
                if (!String.IsNullOrEmpty(impresa.ComuneSedeLegale))
                {
                    DatabaseCemi.AddInParameter(comando, "@comuneSedeLegale", DbType.String, impresa.ComuneSedeLegale);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private void InsertLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Common commonBiz = new Common();
            commonBiz.InsertLavoratore(lavoratore, transaction);
            InsertLavoratoreDatiAggiuntivi(lavoratore, transaction);
        }

        private Boolean InsertLavoratoreDatiAggiuntivi(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriDatiAggiuntiviInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                if (!String.IsNullOrEmpty(lavoratore.Cittadinanza))
                {
                    DatabaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, lavoratore.Cittadinanza);
                }
                if (!String.IsNullOrEmpty(lavoratore.ProvinciaResidenza))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaResidenza", DbType.String,
                                                lavoratore.ProvinciaResidenza);
                }
                if (lavoratore.TitoloStudio != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTitoloDiStudio", DbType.Int32,
                                                lavoratore.TitoloStudio.IdTitoloStudio);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public LavoratoreCollection GetPartecipanti(PartecipanteFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPartecipazioniSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idModuloProgrammazione", DbType.Int32,
                                            filtro.IdProgrammazioneModulo);
                if (!String.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!String.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (!String.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                DatabaseCemi.AddInParameter(comando, "@attestatoRilasciato", DbType.Boolean, filtro.AttestatoRilasciato);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceLuogoNascita = reader.GetOrdinal("LuogoNascita");
                    Int32 indiceCorsiIdLavoratore = reader.GetOrdinal("corsiIdLavoratore");
                    Int32 indiceCorsiCognome = reader.GetOrdinal("corsiCognome");
                    Int32 indiceCorsiNome = reader.GetOrdinal("corsiNome");
                    Int32 indiceCorsiDataNascita = reader.GetOrdinal("corsiDataNascita");
                    Int32 indiceCorsiCodiceFiscale = reader.GetOrdinal("corsiCodiceFiscale");
                    Int32 indiceCorsiLuogoNascita = reader.GetOrdinal("corsiLuogoNascita");
                    Int32 indicePresente = reader.GetOrdinal("presente");
                    Int32 indiceIdPartecipazione = reader.GetOrdinal("idCorsiPartecipazione");
                    Int32 indiceAttestato = reader.GetOrdinal("attestatoRilasciabile");
                    Int32 indicePrenotazioneImpresa = reader.GetOrdinal("prenotazioneImpresa");
                    Int32 indicePrenotazioneConsulente = reader.GetOrdinal("prenotazioneConsulente");
                    Int32 indiceIdPartecipazioneModulo = reader.GetOrdinal("idCorsiModuliPartecipazione");
                    Int32 indiceRegistroConfermato = reader.GetOrdinal("registroConfermato");

                    Int32 indiceIdImpresa = reader.GetOrdinal("impresaIdImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("impresaPartitaIva");
                    Int32 indiceImpCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    Int32 indiceCorsiIdImpresa = reader.GetOrdinal("corsiImpresaIdImpresa");
                    Int32 indiceCorsiRagioneSociale = reader.GetOrdinal("corsiImpresaRagioneSociale");
                    Int32 indiceCorsiPartitaIva = reader.GetOrdinal("corsiImpresaPartitaIva");
                    Int32 indiceCorsiImpCodiceFiscale = reader.GetOrdinal("corsiImpresaCodiceFiscale");
                    Int32 indiceCodiceCorso = reader.GetOrdinal("codiceCorso");
                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");

                    Int32 indicePrimaEsperienza = reader.GetOrdinal("primaEsperienza");
                    Int32 indiceDataAssunzione = reader.GetOrdinal("dataAssunzione");
                    Int32 indiceDataInizio = reader.GetOrdinal("inizio");
                    Int32 indiceDiritto = reader.GetOrdinal("diritto");
                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.Presente = reader.GetBoolean(indicePresente);
                        lavoratore.Attestato = reader.GetBoolean(indiceAttestato);
                        lavoratore.PrenotazioneImpresa = reader.GetBoolean(indicePrenotazioneImpresa);
                        lavoratore.PrenotazioneConsulente = reader.GetBoolean(indicePrenotazioneConsulente);
                        lavoratore.RegistroConfermato = reader.GetBoolean(indiceRegistroConfermato);
                        lavoratore.IdPartecipazione = reader.GetInt32(indiceIdPartecipazione);
                        lavoratore.IdPartecipazioneModulo = reader.GetInt32(indiceIdPartecipazioneModulo);
                        lavoratore.CodiceCorso = reader.GetString(indiceCodiceCorso);
                        lavoratore.IdCorso = reader.GetInt32(indiceIdCorso);

                        if (!reader.IsDBNull(indicePrimaEsperienza))
                        {
                            lavoratore.PrimaEsperienza = reader.GetBoolean(indicePrimaEsperienza);
                        }
                        if (!reader.IsDBNull(indiceDataAssunzione))
                        {
                            lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                        }
                        if (!reader.IsDBNull(indiceDataInizio))
                        {
                            lavoratore.DataInizioCorso = reader.GetDateTime(indiceDataInizio);
                        }
                        if (!reader.IsDBNull(indiceDiritto))
                        {
                            lavoratore.Diritto = reader.GetBoolean(indiceDiritto);
                        }

                        #region Lavoratore

                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                            lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                            {
                                lavoratore.Nome = reader.GetString(indiceNome);
                            }
                            if (!reader.IsDBNull(indiceDataNascita))
                            {
                                lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                            if (!reader.IsDBNull(indiceLuogoNascita))
                            {
                                lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                            }
                        }
                        else
                        {
                            if (!reader.IsDBNull(indiceCorsiIdLavoratore))
                            {
                                lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                                lavoratore.IdLavoratore = reader.GetInt32(indiceCorsiIdLavoratore);
                                lavoratore.Cognome = reader.GetString(indiceCorsiCognome);
                                if (!reader.IsDBNull(indiceCorsiNome))
                                {
                                    lavoratore.Nome = reader.GetString(indiceCorsiNome);
                                }
                                if (!reader.IsDBNull(indiceCorsiDataNascita))
                                {
                                    lavoratore.DataNascita = reader.GetDateTime(indiceCorsiDataNascita);
                                }
                                if (!reader.IsDBNull(indiceCorsiCodiceFiscale))
                                {
                                    lavoratore.CodiceFiscale = reader.GetString(indiceCorsiCodiceFiscale);
                                }
                                if (!reader.IsDBNull(indiceCorsiLuogoNascita))
                                {
                                    lavoratore.ComuneNascita = reader.GetString(indiceCorsiLuogoNascita);
                                }
                            }
                        }

                        #endregion

                        #region Impresa

                        lavoratore.Impresa = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            lavoratore.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            lavoratore.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            lavoratore.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        else
                        {
                            lavoratore.Impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                            lavoratore.Impresa.IdImpresa = reader.GetInt32(indiceCorsiIdImpresa);
                            lavoratore.Impresa.RagioneSociale = reader.GetString(indiceCorsiRagioneSociale);
                        }

                        #endregion
                    }
                }
            }

            return lavoratori;
        }

        public ImpresaCollection GetImpreseSiceNewEAnagrafica(ImpresaFilter filtro)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseSelect"))
            {
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!String.IsNullOrEmpty(filtro.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                }
                if (!String.IsNullOrEmpty(filtro.IvaFisc))
                {
                    DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, filtro.IvaFisc);
                }
                if (filtro.IdConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceCorsiIdImpresa = reader.GetOrdinal("corsiIdImpresa");
                    Int32 indiceCorsiRagioneSociale = reader.GetOrdinal("corsiRagioneSociale");
                    Int32 indiceCorsiPartitaIva = reader.GetOrdinal("corsiPartitaIva");
                    Int32 indiceCorsiCodiceFiscale = reader.GetOrdinal("corsiCodiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            if (!reader.IsDBNull(indicePartitaIva))
                            {
                                impresa.PartitaIva = reader.GetString(indicePartitaIva);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                        }
                        else
                        {
                            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                            impresa.IdImpresa = reader.GetInt32(indiceCorsiIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceCorsiRagioneSociale);
                            if (!reader.IsDBNull(indiceCorsiPartitaIva))
                            {
                                impresa.PartitaIva = reader.GetString(indiceCorsiPartitaIva);
                            }
                            if (!reader.IsDBNull(indiceCorsiCodiceFiscale))
                            {
                                impresa.CodiceFiscale = reader.GetString(indiceCorsiCodiceFiscale);
                            }
                        }
                    }
                }
            }

            return imprese;
        }

        public ImpresaCollection GetImpreseAnagraficaCondivisa(ImpresaFilter filtro)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseAnagraficaCondivisaSelect"))
            {
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!String.IsNullOrEmpty(filtro.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                }
                if (!String.IsNullOrEmpty(filtro.IvaFisc))
                {
                    DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, filtro.IvaFisc);
                }
                if (filtro.IdConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceCodiceCassaEdile = reader.GetOrdinal("codiceCassaEdile");

                    #endregion

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceCodiceCassaEdile))
                        {
                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceCodiceCassaEdile);
                        }
                        else
                        {
                            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        }

                        impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        impresa.IdAnagraficaCondivisa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return imprese;
        }

        public Impresa GetImpresaAnagraficaCondivisaByKey(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseAnagraficaCondivisaSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceCodiceCassaEdile = reader.GetOrdinal("codiceCassaEdile");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        impresa = new Impresa();
                        impresa.IdAnagraficaCondivisa = reader.GetInt32(indiceIdImpresa);

                        if (reader.IsDBNull(indiceCodiceCassaEdile))
                        {
                            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        }
                        else
                        {
                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceCodiceCassaEdile);
                        }
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresaSiceNewByKey(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpresaSiceNewSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                        impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresaCorsiByKey(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpresaCorsiSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indicePartitaIva = reader.GetOrdinal("partitaIva");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return impresa;
        }

        public Lavoratore GetCorsiLavoratore(Int32 idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indicePaeseNascita = reader.GetOrdinal("paeseNascita");
                    Int32 indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    Int32 indiceComuneNascita = reader.GetOrdinal("codiceCatastaleComuneNascita");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceSesso = reader.GetOrdinal("sesso");
                    Int32 indiceFonte = reader.GetOrdinal("fonte");
                    Int32 indiceCittadinanza = reader.GetOrdinal("cittadinanza");
                    Int32 indiceProvinciaResidenza = reader.GetOrdinal("provinciaResidenza");
                    Int32 indiceIdTitoloStudio = reader.GetOrdinal("idCorsiTitoloDiStudio");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        lavoratore = new Lavoratore();
                        lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        lavoratore.Fonte = (FonteAnagraficheComuni) reader.GetInt32(indiceFonte);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        lavoratore.Nome = reader.GetString(indiceNome);
                        lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        lavoratore.PaeseNascita = reader.GetString(indicePaeseNascita);

                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceCittadinanza))
                        {
                            lavoratore.Cittadinanza = reader.GetString(indiceCittadinanza);
                        }
                        if (!reader.IsDBNull(indiceProvinciaResidenza))
                        {
                            lavoratore.ProvinciaResidenza = reader.GetString(indiceProvinciaResidenza);
                        }
                        if (!reader.IsDBNull(indiceIdTitoloStudio))
                        {
                            lavoratore.TitoloStudio = new TitoloDiStudio();
                            lavoratore.TitoloStudio.IdTitoloStudio = reader.GetInt32(indiceIdTitoloStudio);
                            lavoratore.TitoloStudio.Descrizione = reader.GetString(indiceDescrizione);
                        }
                    }
                }
            }

            return lavoratore;
        }

        public void GetDatiAggiuntiviPartecipazione(Int32 idPartecipazione, out Boolean? primaEsperienza, out DateTime? dataAssunzione)
        {
            primaEsperienza = null;
            dataAssunzione = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectDatiAggiuntivi"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        primaEsperienza = reader["primaEsperienza"] as Boolean?;
                        dataAssunzione = reader["dataAssunzione"] as DateTime?;
                    }
                }
            }
        }

        public bool UpdateCorsiLavoratore(Lavoratore lavoratore, Int32 idPartecipazione)
        {
            Boolean res = false;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        Common commonBiz = new Common();
                        if (commonBiz.UpdateLavoratore(lavoratore, transaction)
                            && UpdateLavoratoreDatiAggiuntivi(lavoratore, transaction)
                            && UpdatePartecipazione(idPartecipazione, lavoratore.PrimaEsperienza, lavoratore.DataAssunzione, transaction))
                        {
                            res = true;
                            transaction.Commit();
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private Boolean UpdateLavoratoreDatiAggiuntivi(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriDatiAggiuntiviUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                if (!String.IsNullOrEmpty(lavoratore.Cittadinanza))
                {
                    DatabaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, lavoratore.Cittadinanza);
                }
                if (!String.IsNullOrEmpty(lavoratore.ProvinciaResidenza))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaResidenza", DbType.String,
                                                lavoratore.ProvinciaResidenza);
                }
                if (lavoratore.TitoloStudio != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTitoloDiStudio", DbType.Int32,
                                                lavoratore.TitoloStudio.IdTitoloStudio);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdatePartecipazione(Int32 idPartecipazione, Boolean? primaEsperienza, DateTime? dataAssunzione, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);
                if (primaEsperienza.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@primaEsperienza", DbType.Boolean, primaEsperienza.Value);
                }
                if (dataAssunzione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime, dataAssunzione.Value);
                }

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public Int32? GetUltimaImpresaLavoratore(Int32 idLavoratore)
        {
            Int32? idImpresa = null;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_CorsiRapportiImpresaPersonaSelectUltimoRapporto"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);
                idImpresa = DatabaseCemi.GetParameterValue(comando, "@idImpresa") as Int32?;
            }

            return idImpresa;
        }

        #endregion

        #region Gestione Programmazione

        public ProgrammazioneModuloCollection GetProgrammazioni(ProgrammazioneModuloFilter filtro)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectByFilter"))
            {
                if (filtro.Dal.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal.Value);
                }
                if (filtro.Al.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al.Value);
                }
                if (filtro.IdCorso.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, filtro.IdCorso.Value);
                }
                if (filtro.IdLocazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, filtro.IdLocazione.Value);
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    Int32 indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    Int32 indiceInizio = reader.GetOrdinal("inizio");
                    Int32 indiceFine = reader.GetOrdinal("fine");
                    Int32 indiceConfermaPresenze = reader.GetOrdinal("registroConfermato");
                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
                    Int32 indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    Int32 indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    Int32 indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    Int32 indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    Int32 indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                        programmazioni.Add(programmazione);

                        programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                        programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);
                        programmazione.PresenzeConfermate = reader.GetBoolean(indiceConfermaPresenze);

                        // Corso
                        programmazione.Corso = new Corso();
                        programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);

                        // Modulo
                        programmazione.Modulo = new Modulo();
                        programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                        // Locazione
                        programmazione.Locazione = new Locazione();
                        programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }
                    }
                }
            }

            return programmazioni;
        }

        public Boolean InsertProgrammazione(Programmazione programmazione)
        {
            Boolean res = false;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (
                            DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32,
                                                        programmazione.Corso.IdCorso.Value);
                            DatabaseCemi.AddOutParameter(comando, "@idPianificazione", DbType.Int32, 4);
                            DatabaseCemi.ExecuteNonQuery(comando, transaction);
                            programmazione.IdProgrammazione =
                                (Int32) DatabaseCemi.GetParameterValue(comando, "@idPianificazione");

                            if (programmazione.IdProgrammazione > 0)
                            {
                                res = true;

                                foreach (ProgrammazioneModulo prog in programmazione.ProgrammazioniModuli)
                                {
                                    if (!InsertProgrammazione(prog, programmazione.IdProgrammazione.Value, transaction))
                                    {
                                        res = false;
                                        throw new Exception("Errore durante l'inserimento di una programmazione modulo");
                                    }
                                }

                                transaction.Commit();
                            }
                            else
                            {
                                throw new Exception("Errore durante l'inserimento della programmazione");
                            }
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        public Boolean UpdateProgrammazione(Programmazione programmazione)
        {
            Boolean res = true;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (ProgrammazioneModulo prog in programmazione.ProgrammazioniModuli)
                        {
                            if (!UpdateProgrammazione(prog, transaction))
                            {
                                res = false;
                                throw new Exception("Errore durante l'aggiornamento di una programmazione modulo");
                            }
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private Boolean InsertProgrammazione(ProgrammazioneModulo prog, Int32 idProgrammazione,
                                             DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPianificazioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idModulo", DbType.Int32, prog.Modulo.IdModulo);
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, prog.Locazione.IdLocazione.Value);
                DatabaseCemi.AddInParameter(comando, "@inizio", DbType.DateTime, prog.Schedulazione);
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, prog.SchedulazioneFine);
                DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.Int32, idProgrammazione);
                DatabaseCemi.AddInParameter(comando, "@maxPartecipanti", DbType.Int32, prog.MaxPartecipanti);
                DatabaseCemi.AddInParameter(comando, "@prenotabile", DbType.Boolean, prog.Prenotabile);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
                else
                {
                    throw new Exception("InsertProgrammazione: Errore nell'inserimento della programmazione modulo");
                }
            }

            return res;
        }

        private Boolean UpdateProgrammazione(ProgrammazioneModulo prog, DbTransaction transaction)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPianificazioniUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idModuliPianificazione", DbType.Int32,
                                            prog.IdProgrammazioneModulo.Value);
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, prog.Locazione.IdLocazione.Value);
                DatabaseCemi.AddInParameter(comando, "@inizio", DbType.DateTime, prog.Schedulazione);
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, prog.SchedulazioneFine);
                DatabaseCemi.AddInParameter(comando, "@maxPartecipanti", DbType.Int32, prog.MaxPartecipanti);
                DatabaseCemi.AddInParameter(comando, "@prenotabile", DbType.Boolean, prog.Prenotabile);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
                else
                {
                    throw new Exception("UpdateProgrammazione: Errore nell'aggiornamento della programmazione modulo");
                }
            }

            return res;
        }

        public ProgrammazioneModuloCollection GetProgrammazione(Int32 idProgrammazione)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectTutteDelCorsoByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.Int32, idProgrammazione);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    Int32 indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    Int32 indiceInizio = reader.GetOrdinal("inizio");
                    Int32 indiceFine = reader.GetOrdinal("fine");
                    Int32 indiceConfermaPresenze = reader.GetOrdinal("registroConfermato");
                    Int32 indicePartecipanti = reader.GetOrdinal("partecipanti");
                    Int32 indiceMaxPartecipanti = reader.GetOrdinal("maxPartecipanti");
                    Int32 indicePrenotabile = reader.GetOrdinal("prenotabile");
                    Int32 indiceIdCorso = reader.GetOrdinal("idCorso");
                    Int32 indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    Int32 indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    Int32 indiceCorsoIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");
                    Int32 indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    Int32 indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    Int32 indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    Int32 indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    Int32 indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                        programmazioni.Add(programmazione);

                        programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                        programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);
                        programmazione.PresenzeConfermate = reader.GetBoolean(indiceConfermaPresenze);
                        programmazione.Partecipanti = reader.GetInt32(indicePartecipanti);
                        programmazione.MaxPartecipanti = reader.GetInt32(indiceMaxPartecipanti);
                        programmazione.Prenotabile = reader.GetBoolean(indicePrenotabile);

                        // Corso
                        programmazione.Corso = new Corso();
                        programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);
                        programmazione.Corso.IscrizioneLibera = reader.GetBoolean(indiceCorsoIscrizioneLibera);

                        // Modulo
                        programmazione.Modulo = new Modulo();
                        programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                        // Locazione
                        programmazione.Locazione = new Locazione();
                        programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }
                    }
                }
            }

            return programmazioni;
        }

        public Boolean UpdateProgrammazioneModuloConfermaPresenze(Int32 idProgrammazioneModulo)
        {
            Boolean res = false;

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPianificazioniUpdateConfermaPresenze"))
            {
                DatabaseCemi.AddInParameter(comando, "@idProgrammazioneModulo", DbType.Int32, idProgrammazioneModulo);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean DeleteProgrammazione(int idProgrammazione)
        {
            Boolean res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.Int32, idProgrammazione);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = true;
            }

            return res;
        }

        #endregion

        #region Anagrafica Unica
        public Type.DataSets.Corsi AnagraficaUnicaRicercaCorsi(AnagraficaUnicaCorsiFilter filtro)
        {
            Type.DataSets.Corsi dsCorsi = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiCemiSelect"))
            {
                comando.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (filtro.IdPianificazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.String, filtro.IdPianificazione.Value);
                }

                dsCorsi = new Type.DataSets.Corsi();
                DatabaseCemi.LoadDataSet(comando, dsCorsi, "Corsi");
            }

            return dsCorsi;
        }

        public Type.DataSets.Iscrizioni AnagraficaUnicaRicercaIscrizioni(AnagraficaUnicaIscrizioniFilter filtro)
        {
            Type.DataSets.Iscrizioni dsIscrizioni = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiLavoratoriImpreseCemiSelect"))
            {
                comando.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (filtro.IdPianificazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.String, filtro.IdPianificazione.Value);
                }

                dsIscrizioni = new Type.DataSets.Iscrizioni();
                DatabaseCemi.LoadDataSet(comando, dsIscrizioni, new String[2] { "Lavoratori", "Imprese" });
            }

            return dsIscrizioni;
        }

        public Type.DataSets.Lavoratori AnagraficaUnicaRicercaLavoratori(AnagraficaUnicaLavoratoriFilter filtro)
        {
            Type.DataSets.Lavoratori dsLavoratori = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaLavoratoriSelect"))
            {
                comando.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (!String.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                dsLavoratori = new Type.DataSets.Lavoratori();
                DatabaseCemi.LoadDataSet(comando, dsLavoratori, "Lavoratori");
            }

            return dsLavoratori;
        }

        public Type.DataSets.Imprese AnagraficaUnicaRicercaImprese(AnagraficaUnicaImpreseFilter filtro)
        {
            Type.DataSets.Imprese dsImprese = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaImpreseSelect"))
            {
                comando.CommandTimeout = Int32.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (!String.IsNullOrEmpty(filtro.CodiceFiscalePartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscalePartitaIva", DbType.String, filtro.CodiceFiscalePartitaIva);
                }

                dsImprese = new Type.DataSets.Imprese();
                DatabaseCemi.LoadDataSet(comando, dsImprese, "Imprese");
            }

            return dsImprese;
        }
        #endregion

        #region Prestazioni DTA
        public DateTime? DataErogazionePrestazioniDTA(Int32 anno)
        {
            DateTime? dataErogazione = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTASelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddOutParameter(comando, "@erogato", DbType.DateTime, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                dataErogazione = DatabaseCemi.GetParameterValue(comando, "@erogato") as DateTime?;
            }

            return dataErogazione;
        }

        public List<PrestazioneDTA> GetPrestazioniDTALavoratori(Int32 anno, DTAFilter filtro)
        {
            List<PrestazioneDTA> prestazioni = new List<PrestazioneDTA>();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTALavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                if (filtro != null)
                {
                    if (!String.IsNullOrEmpty(filtro.Cognome))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                    }
                    if (!String.IsNullOrEmpty(filtro.Corso))
                    {
                        DatabaseCemi.AddInParameter(comando, "@corso", DbType.String, filtro.Corso);
                    }
                    if (filtro.Diritto.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@diritto", DbType.Boolean, filtro.Diritto.Value);
                    }
                }

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceDataInizioCorso = reader.GetOrdinal("dataInizioCorso");
                    Int32 indiceDataFineCorso = reader.GetOrdinal("dataFineCorso");
                    Int32 indiceCodiceCorso = reader.GetOrdinal("codiceCorso");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceDataErogazione = reader.GetOrdinal("dataErogazione");
                    Int32 indiceSelezionato = reader.GetOrdinal("selezionato");
                    Int32 indiceDenunciaPresente = reader.GetOrdinal("denunciaPresente");
                    Int32 indiceIbanPresente = reader.GetOrdinal("ibanPresente");
                    Int32 indiceProtocolloPrestazione = reader.GetOrdinal("protocolloPrestazione");
                    Int32 indiceNumeroProtocolloPrestazione = reader.GetOrdinal("numeroProtocolloPrestazione");
                    Int32 indiceIdStatoProtocolloPrestazione = reader.GetOrdinal("idTipoStatoPrestazione");
                    Int32 indiceDescrizioneStatoProtocolloPrestazione = reader.GetOrdinal("descrizioneStatoPrestazione");

                    #endregion

                    while (reader.Read())
                    {
                        PrestazioneDTA prestazione = new PrestazioneDTA();
                        prestazioni.Add(prestazione);

                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            prestazione.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            prestazione.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            prestazione.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            prestazione.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceDataInizioCorso))
                        {
                            prestazione.DataInizioCorso = reader.GetDateTime(indiceDataInizioCorso);
                        }
                        if (!reader.IsDBNull(indiceDataFineCorso))
                        {
                            prestazione.DataFineCorso = reader.GetDateTime(indiceDataFineCorso);
                        }
                        if (!reader.IsDBNull(indiceCodiceCorso))
                        {
                            prestazione.CodiceCorso = reader.GetString(indiceCodiceCorso);
                        }
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            prestazione.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceDataErogazione))
                        {
                            prestazione.DataErogazione = reader.GetDateTime(indiceDataErogazione);
                        }
                        if (!reader.IsDBNull(indiceSelezionato))
                        {
                            prestazione.Selezionato = reader.GetBoolean(indiceSelezionato);
                        }
                        if (!reader.IsDBNull(indiceDenunciaPresente))
                        {
                            prestazione.DenunciaPresente = reader.GetBoolean(indiceDenunciaPresente);
                        }
                        if (!reader.IsDBNull(indiceIbanPresente))
                        {
                            prestazione.IbanPresente = reader.GetBoolean(indiceIbanPresente);
                        }
                        if (!reader.IsDBNull(indiceProtocolloPrestazione))
                        {
                            prestazione.ProtocolloPrestazione = reader.GetInt32(indiceProtocolloPrestazione);
                        }
                        if (!reader.IsDBNull(indiceNumeroProtocolloPrestazione))
                        {
                            prestazione.NumeroProtocolloPrestazione = reader.GetInt32(indiceNumeroProtocolloPrestazione);
                        }
                        if (!reader.IsDBNull(indiceIdStatoProtocolloPrestazione))
                        {
                            prestazione.IdStato = reader.GetString(indiceIdStatoProtocolloPrestazione);
                        }
                        if (!reader.IsDBNull(indiceDescrizioneStatoProtocolloPrestazione))
                        {
                            prestazione.DescrizioneStato = reader.GetString(indiceDescrizioneStatoProtocolloPrestazione);
                        }
                    }
                }
            }

            return prestazioni;
        }

        public void SalvaPrestazioniDTALavoratori(List<PrestazioneDTALavoratore> prestazioni)
        {
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (PrestazioneDTALavoratore prestazione in prestazioni)
                        {
                            SalvaPrestazioneDTALavoratore(prestazione, transaction);
                        }

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void SalvaPrestazioneDTALavoratore(PrestazioneDTALavoratore prestazione, DbTransaction transaction)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTALavoratoriInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, prestazione.Anno);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, prestazione.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@selezionato", DbType.Boolean, prestazione.Selezionato);

                if (transaction != null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
            }
        }

        public void GeneraDomandePrestazioniDTALavoratori(Int32 anno)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandaGeneraDTA"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public Dictionary<Int32, String> GetPrestazioniDTAAnni()
        {
            Dictionary<Int32, String> anni = new Dictionary<Int32, String>();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTAAnniSelect"))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceAnno = reader.GetOrdinal("anno");

                    #endregion

                    while (reader.Read())
                    {
                        Int32 anno = reader.GetInt32(indiceAnno);
                        anni.Add(anno, String.Format("{0}/{1}", anno, anno + 1));
                    }
                }
            }

            return anni;
        }
        #endregion
    }
}