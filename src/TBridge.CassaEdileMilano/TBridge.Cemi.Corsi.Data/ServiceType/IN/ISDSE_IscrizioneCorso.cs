﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


// 
// This source code was auto-generated by xsd, Version=4.0.30319.1.
// 

namespace TBridge.Cemi.Corsi.Data.ServiceType.IN
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ISDSE_IscrizioneCorso
    {

        private ISDSE_IscrizioneCorsoCEM_Iscrizione cEM_IscrizioneField;

        /// <remarks/>
        public ISDSE_IscrizioneCorsoCEM_Iscrizione CEM_Iscrizione
        {
            get
            {
                return this.cEM_IscrizioneField;
            }
            set
            {
                this.cEM_IscrizioneField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ISDSE_IscrizioneCorsoCEM_Iscrizione
    {

        private ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Corso cEM_CorsoField;

        private string cEM_ContattoField;

        private ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Anag_Lav cEM_Anag_LavField;

        private string cEM_ImpresaField;

        /// <remarks/>
        public ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Corso CEM_Corso
        {
            get
            {
                return this.cEM_CorsoField;
            }
            set
            {
                this.cEM_CorsoField = value;
            }
        }

        /// <remarks/>
        public string CEM_Contatto
        {
            get
            {
                return this.cEM_ContattoField;
            }
            set
            {
                this.cEM_ContattoField = value;
            }
        }

        /// <remarks/>
        public ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Anag_Lav CEM_Anag_Lav
        {
            get
            {
                return this.cEM_Anag_LavField;
            }
            set
            {
                this.cEM_Anag_LavField = value;
            }
        }

        /// <remarks/>
        public string CEM_Impresa
        {
            get
            {
                return this.cEM_ImpresaField;
            }
            set
            {
                this.cEM_ImpresaField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Corso
    {

        private string cEM_CodiceField;

        private string cEM_Data_IniField;

        private string cEM_SedeField;

        private int cEM_ProgField;

        private string cEM_TicketField;

        private string cEM_Pag_CorField;

        /// <remarks/>
        public string CEM_Codice
        {
            get
            {
                return this.cEM_CodiceField;
            }
            set
            {
                this.cEM_CodiceField = value;
            }
        }

        /// <remarks/>
        public string CEM_Data_Ini
        {
            get
            {
                return this.cEM_Data_IniField;
            }
            set
            {
                this.cEM_Data_IniField = value;
            }
        }

        /// <remarks/>
        public string CEM_Sede
        {
            get
            {
                return this.cEM_SedeField;
            }
            set
            {
                this.cEM_SedeField = value;
            }
        }

        /// <remarks/>
        public int CEM_Prog
        {
            get
            {
                return this.cEM_ProgField;
            }
            set
            {
                this.cEM_ProgField = value;
            }
        }

        /// <remarks/>
        public string CEM_Ticket
        {
            get
            {
                return this.cEM_TicketField;
            }
            set
            {
                this.cEM_TicketField = value;
            }
        }

        /// <remarks/>
        public string CEM_Pag_Cor
        {
            get
            {
                return this.cEM_Pag_CorField;
            }
            set
            {
                this.cEM_Pag_CorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Anag_Lav
    {

        private string cEM_CodField;

        private string cEM_CognomeField;

        private string cEM_NomeField;

        private string cEM_SessoField;

        private string cEM_Nato_LuoField;

        private string cEM_Nato_IlField;

        private string cEM_Com_ResField;

        private string cEM_Indi_ResField;

        private string cEM_CAP_ResField;

        private string cEM_Codi_FisField;

        private string cEM_Nume_TelField;

        private string cEM_Nume_CelField;

        private string cEM_MailField;

        /// <remarks/>
        public string CEM_Cod
        {
            get
            {
                return this.cEM_CodField;
            }
            set
            {
                this.cEM_CodField = value;
            }
        }

        /// <remarks/>
        public string CEM_Cognome
        {
            get
            {
                return this.cEM_CognomeField;
            }
            set
            {
                this.cEM_CognomeField = value;
            }
        }

        /// <remarks/>
        public string CEM_Nome
        {
            get
            {
                return this.cEM_NomeField;
            }
            set
            {
                this.cEM_NomeField = value;
            }
        }

        /// <remarks/>
        public string CEM_Sesso
        {
            get
            {
                return this.cEM_SessoField;
            }
            set
            {
                this.cEM_SessoField = value;
            }
        }

        /// <remarks/>
        public string CEM_Nato_Luo
        {
            get
            {
                return this.cEM_Nato_LuoField;
            }
            set
            {
                this.cEM_Nato_LuoField = value;
            }
        }

        /// <remarks/>
        public string CEM_Nato_Il
        {
            get
            {
                return this.cEM_Nato_IlField;
            }
            set
            {
                this.cEM_Nato_IlField = value;
            }
        }

        /// <remarks/>
        public string CEM_Com_Res
        {
            get
            {
                return this.cEM_Com_ResField;
            }
            set
            {
                this.cEM_Com_ResField = value;
            }
        }

        /// <remarks/>
        public string CEM_Indi_Res
        {
            get
            {
                return this.cEM_Indi_ResField;
            }
            set
            {
                this.cEM_Indi_ResField = value;
            }
        }

        /// <remarks/>
        public string CEM_CAP_Res
        {
            get
            {
                return this.cEM_CAP_ResField;
            }
            set
            {
                this.cEM_CAP_ResField = value;
            }
        }

        /// <remarks/>
        public string CEM_Codi_Fis
        {
            get
            {
                return this.cEM_Codi_FisField;
            }
            set
            {
                this.cEM_Codi_FisField = value;
            }
        }

        /// <remarks/>
        public string CEM_Nume_Tel
        {
            get
            {
                return this.cEM_Nume_TelField;
            }
            set
            {
                this.cEM_Nume_TelField = value;
            }
        }

        /// <remarks/>
        public string CEM_Nume_Cel
        {
            get
            {
                return this.cEM_Nume_CelField;
            }
            set
            {
                this.cEM_Nume_CelField = value;
            }
        }

        /// <remarks/>
        public string CEM_Mail
        {
            get
            {
                return this.cEM_MailField;
            }
            set
            {
                this.cEM_MailField = value;
            }
        }
    }
}
