﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Corsi.Data.ServiceType.IN;
using TBridge.Cemi.Corsi.Data.ServiceType.OUT;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.Corsi.Type.Filters.Esem;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Lavoratore = TBridge.Cemi.Corsi.Type.Entities.Lavoratore;
using System.Data.Common;
using TBridge.Cemi.Corsi.Data.ServiceType.ERR;

namespace TBridge.Cemi.Corsi.Data
{
    public class CorsiEsemDataAccess
    {
        private Database databaseCemi;

        public CorsiEsemDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        private ServiceReferenceIscrizioneCorsiEsem.ServiceSoapClient esemService = new ServiceReferenceIscrizioneCorsiEsem.ServiceSoapClient();

        public List<TipoCorso> GetTipiCorso(Utente utente)
        {
            List<TipoCorso> tipiCorso = new List<TipoCorso>();

            String aut = GeneraCredenzialiServizioXML(utente);
            String elencoCorsiXML = esemService.ISDSEElenco_TipoCorso(aut);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(elencoCorsiXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_ElencoTipoCorso tipiCorsoXml = GetXmlTipiCorso(elencoCorsiXML);

                    if (tipiCorsoXml != null
                        && tipiCorsoXml.Items != null
                        && tipiCorsoXml.Items.Length == 1
                        && tipiCorsoXml.Items[0].RS_TipoCorso != null)
                    {
                        for (Int32 i = 0; i < tipiCorsoXml.Items[0].RS_TipoCorso.Length; i++)
                        {
                            ISDSE_RS_ElencoTipoCorsoRS_ElencoCorsoRS_TipoCorso tC = tipiCorsoXml.Items[0].RS_TipoCorso[i];

                            TipoCorso tipoCorso = new TipoCorso();
                            tipiCorso.Add(tipoCorso);

                            tipoCorso.Codice = tC.RS_Codice;
                            tipoCorso.Descrizione = tC.RS_Descrizione;
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, elencoCorsiXML, "ISDSEElenco_TipoCorso");
                }

            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML dei tipi corso", exc);
            }

            tipiCorso.Sort();
            return tipiCorso;
        }

        public List<Sede> GetSedi(Utente utente)
        {
            List<Sede> sedi = new List<Sede>();

            String aut = GeneraCredenzialiServizioXML(utente);
            String elencoSediXML = esemService.ISDSEElenco_SedeCorso(aut);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(elencoSediXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_ElencoSedeCorso sediXml = GetXmlSedi(elencoSediXML);

                    if (sediXml != null
                        && sediXml.Items != null
                        && sediXml.Items.Length == 1
                        && sediXml.Items[0].RS_SedeCorso != null)
                    {
                        for (Int32 i = 0; i < sediXml.Items[0].RS_SedeCorso.Length; i++)
                        {
                            ISDSE_RS_ElencoSedeCorsoRS_ElencoSedeRS_SedeCorso s = sediXml.Items[0].RS_SedeCorso[i];

                            Sede sede = new Sede();
                            sedi.Add(sede);
                            sede.Codice = s.RS_Codice;
                            sede.Descrizione = s.RS_Descrizione;
                            sede.Indirizzo = s.RS_Indirizzo;
                            sede.Calendarizzato = s.RS_FlagCale;

                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, elencoSediXML, "ISDSEElenco_SedeCorso");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML delle sedi", exc);
            }

            sedi.Sort();
            return sedi;
        }

        public List<Corso> GetCorsi(Utente utente, CorsoFilter filter)
        {
            List<Corso> corsi = new List<Corso>();

            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraFiltroCorsiServizioXML(filter);
            String elencoCorsiXML = esemService.ISDSEElencoCorsi(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(elencoCorsiXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_ElencoCorsi corsiXml = GetXmlCorsi(elencoCorsiXML);

                    if (corsiXml != null
                        && corsiXml.Items != null
                        && corsiXml.Items.Length == 1
                        && corsiXml.Items[0].RS_Corso != null)
                    {
                        for (Int32 i = 0; i < corsiXml.Items[0].RS_Corso.Length; i++)
                        {
                            if (((DateTime.ParseExact(corsiXml.Items[0].RS_Corso[i].RS_Data_Ini, "yyyy-MM-dd", null) > DateTime.Today) && (DateTime.ParseExact(String.IsNullOrEmpty(corsiXml.Items[0].RS_Corso[i].RS_Data_Fin_Pre) ? "1900-01-01" : corsiXml.Items[0].RS_Corso[i].RS_Data_Fin_Pre, "yyyy-MM-dd", null) >= DateTime.Today)) || ((DateTime.ParseExact(corsiXml.Items[0].RS_Corso[i].RS_Data_Ini, "yyyy-MM-dd", null) == DateTime.ParseExact("1900-01-01", "yyyy-MM-dd", null)) ))
                            {
                                ISDSE_RS_ElencoCorsiRS_ElencoCorsiRS_Corso c = corsiXml.Items[0].RS_Corso[i];

                                Corso corso = new Corso();
                                corsi.Add(corso);

                                corso.Codice = c.RS_Codice;
                                if (!String.IsNullOrWhiteSpace(c.RS_Data_Ini))
                                {
                                    corso.DataInizio = DateTime.ParseExact(c.RS_Data_Ini, "yyyy-MM-dd", null);
                                }
                                if (!String.IsNullOrWhiteSpace(c.RS_Data_Fin))
                                {
                                    corso.DataFine = DateTime.ParseExact(c.RS_Data_Fin, "yyyy-MM-dd", null);
                                }
                                if (!String.IsNullOrWhiteSpace(c.RS_Data_Ini_Pre))
                                {
                                    corso.DataPresuntaPerPrenotazione = DateTime.ParseExact(c.RS_Data_Ini_Pre, "yyyy-MM-dd", null);
                                }

                                if (!String.IsNullOrWhiteSpace(c.RS_Data_Fin_Pre))
                                {
                                    corso.DataScadenza = DateTime.ParseExact(c.RS_Data_Fin_Pre, "yyyy-MM-dd", null);
                                }

                                corso.CodiceSede = c.RS_Sede;

                                corso.Descrizione = c.RS_Desc;

                                if (!String.IsNullOrWhiteSpace(c.RS_Durata))
                                {
                                    corso.Durata = Decimal.Parse(c.RS_Durata.Replace('.', ','));
                                }
                                corso.Progressivo = c.RS_Prog;
                                corso.PostiDisponibili = c.RS_Posti_Disp;

                                switch (c.RS_Obb_Vis_Med)
                                {
                                    case "S":
                                        corso.ObbligoVisitaMedica = "Obbligatoria";
                                        break;
                                    default:
                                        corso.ObbligoVisitaMedica = "Non Obbligatoria";
                                        break;
                                }   
                                //corso.ObbligoVisitaMedica = c.RS_Obb_Vis_Med;

                                if (!String.IsNullOrWhiteSpace(c.RS_Costo))
                                {
                                    corso.Costo = Decimal.Parse(c.RS_Costo.Replace('.', ','));
                                }

                                if (corsiXml.Items[0].RS_Corso[i].RS_Dett_Corso != null)
                                {
                                    List<DateCorso> dateCorso = new List<DateCorso>();
                                    for (Int32 j = 0; j < corsiXml.Items[0].RS_Corso[i].RS_Dett_Corso.Length; j++)
                                    {
                                        DateCorso dataCorso = new DateCorso();
                                        dateCorso.Add(dataCorso);

                                        if (!String.IsNullOrWhiteSpace(c.RS_Dett_Corso[j].RS_Giorno_Corso))
                                        {
                                            dataCorso.GiornoCorso = DateTime.ParseExact(c.RS_Dett_Corso[j].RS_Giorno_Corso, "yyyy-MM-dd", null);
                                        }
                                        dataCorso.OraInizio = c.RS_Dett_Corso[j].RS_Ora_Ini;
                                        dataCorso.OraFine = c.RS_Dett_Corso[j].RS_Ora_Fin;
                                    }
                                    corso.DateCorso = dateCorso;
                                }
                            }
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, elencoCorsiXML, "ISDSEElencoCorsi");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML dei corsi", exc);
            }

            corsi.Sort();
            return corsi;
        }

        public List<LavoratoreCorsi> GetCorsiScadenzeImpresa(Utente utente, String codiceFiscaleImpresa, Int32? idImpresa)
        {
            List<LavoratoreCorsi> scadenze = new List<LavoratoreCorsi>();
            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraFiltroCorsiScadenzaImpresaXML(codiceFiscaleImpresa, idImpresa);
            String scadImpXML = esemService.ISDSECorsiScadenzaDitta(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(scadImpXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_CorsiScadenzaDitta corsiScadXml = GetXmlCorsiScadenzaImpresa(scadImpXML);

                    if (corsiScadXml != null
                        && corsiScadXml.Items != null
                        && corsiScadXml.Items.Length == 1
                        && corsiScadXml.Items[0].RS_Lavoratore != null)
                    {
                        for (Int32 i = 0; i < corsiScadXml.Items[0].RS_Lavoratore.Length; i++)
                        {
                            ISDSE_RS_CorsiScadenzaDittaRS_CorsiScadenzaDittaRS_Lavoratore l = corsiScadXml.Items[0].RS_Lavoratore[i];

                            LavoratoreCorsi scad = new LavoratoreCorsi();
                            scadenze.Add(scad);

                            scad.Lavoratore = new Lavoratore();
                            if (!String.IsNullOrWhiteSpace(l.RS_Cod_Lav))
                                scad.Lavoratore.IdLavoratore = Int32.Parse(l.RS_Cod_Lav);
                            scad.Lavoratore.Cognome = l.RS_Cognome;
                            scad.Lavoratore.Nome = l.RS_Nome;
                            scad.Lavoratore.CodiceFiscale = l.RS_CF_Lavoratore;

                            if (l.RS_Corso != null)
                            {
                                for (Int32 j = 0; j < l.RS_Corso.Length; j++)
                                {
                                    Corso c = new Corso();
                                    scad.Corsi.Add(c);

                                    c.Codice = l.RS_Corso[j].RS_Codice;
                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Ini))
                                    {
                                        c.DataInizio = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Ini, "yyyy-MM-dd", null);
                                    }
                                    c.CodiceSede = l.RS_Corso[j].RS_Sede;
                                    c.Descrizione = l.RS_Corso[j].RS_Desc;

                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Sca))
                                    {
                                        c.DataScadenza = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Sca, "yyyy-MM-dd", null);
                                    }
                                }

                                scad.Corsi.Sort();
                            }
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, scadImpXML, "ISDSECorsiScadenzaDitta");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML delle scadenze per impresa", exc);
            }

            scadenze.Sort();
            return scadenze;
        }

        public LavoratoreCorsi GetCorsiScadenzeLavoratore(Utente utente, String codiceFiscaleLavoratore, Int32? idLavoratore)
        {
            LavoratoreCorsi scadenze = null;
            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraFiltroCorsiScadenzaLavoratoreXML(codiceFiscaleLavoratore, idLavoratore);
            String scadLavXML = esemService.ISDSECorsiScadenzaLav(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(scadLavXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_CorsiScadenzaLav corsiScadXml = GetXmlCorsiScadenzaLavoratore(scadLavXML);

                    if (corsiScadXml != null
                        && corsiScadXml.Items != null
                        && corsiScadXml.Items.Length == 1
                        && corsiScadXml.Items[0].RS_Lavoratore != null)
                    {
                        for (Int32 i = 0; i < corsiScadXml.Items[0].RS_Lavoratore.Length; i++)
                        {
                            scadenze = new LavoratoreCorsi();
                            ISDSE_RS_CorsiScadenzaLavRS_CorsiScadenzaLavRS_Lavoratore l = corsiScadXml.Items[0].RS_Lavoratore[i];

                            scadenze.Lavoratore = new Lavoratore();
                            if (!String.IsNullOrWhiteSpace(l.RS_Cod_Lav))
                                scadenze.Lavoratore.IdLavoratore = Int32.Parse(l.RS_Cod_Lav);
                            scadenze.Lavoratore.Cognome = l.RS_Cognome;
                            scadenze.Lavoratore.Nome = l.RS_Nome;
                            scadenze.Lavoratore.CodiceFiscale = l.RS_CF_Lavoratore;

                            if (l.RS_Corso != null)
                            {
                                for (Int32 j = 0; j < l.RS_Corso.Length; j++)
                                {
                                    Corso c = new Corso();
                                    scadenze.Corsi.Add(c);

                                    c.Codice = l.RS_Corso[j].RS_Codice;
                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Ini))
                                    {
                                        c.DataInizio = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Ini, "yyyy-MM-dd", null);
                                    }
                                    c.CodiceSede = l.RS_Corso[j].RS_Sede;
                                    c.Descrizione = l.RS_Corso[j].RS_Desc;

                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Sca))
                                    {
                                        c.DataScadenza = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Sca, "yyyy-MM-dd", null);
                                    }
                                }

                                scadenze.Corsi.Sort();
                            }
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, scadLavXML, "ISDSECorsiScadenzaLav");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML delle scadenze per lavoratore", exc);
            }

            return scadenze;
        }

        public List<LavoratoreCorsi> GetIscrizioniImpresa(Utente utente, String codiceFiscaleImpresa, Int32? idImpresa)
        {
            List<LavoratoreCorsi> iscrizioni = new List<LavoratoreCorsi>();
            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraFiltroIscrizioniImpresaXML(codiceFiscaleImpresa,idImpresa);
            String iscrImpXML = esemService.ISDSEStorico_IscrizioniDitta(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(iscrImpXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_StoricoDitta iscrImpXml = GetXmlIscrizioniImpresa(iscrImpXML);

                    if (iscrImpXml != null
                        && iscrImpXml.Items != null
                        && iscrImpXml.Items.Length == 1
                        && iscrImpXml.Items[0].RS_Lavoratore != null)
                    {
                        for (Int32 i = 0; i < iscrImpXml.Items[0].RS_Lavoratore.Length; i++)
                        {
                            ISDSE_RS_StoricoDittaRS_StoricoDittaRS_Lavoratore l = iscrImpXml.Items[0].RS_Lavoratore[i];

                            LavoratoreCorsi iscrizione = new LavoratoreCorsi();
                            iscrizioni.Add(iscrizione);

                            iscrizione.Lavoratore = new Lavoratore();
                            if (!String.IsNullOrWhiteSpace(l.RS_Cod_Lav))
                                iscrizione.Lavoratore.IdLavoratore = Int32.Parse(l.RS_Cod_Lav);
                            iscrizione.Lavoratore.Cognome = l.RS_Cognome;
                            iscrizione.Lavoratore.Nome = l.RS_Nome;
                            iscrizione.Lavoratore.CodiceFiscale = l.RS_CF_Lavoratore;

                            if (l.RS_Corso != null)
                            {
                                for (Int32 j = 0; j < l.RS_Corso.Length; j++)
                                {
                                    Corso c = new Corso();
                                    iscrizione.Corsi.Add(c);

                                    c.Codice = l.RS_Corso[j].RS_Codice;
                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Ini))
                                    {
                                        c.DataInizio = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Ini, "yyyy-MM-dd", null);
                                    }
                                    c.CodiceSede = l.RS_Corso[j].RS_Sede;
                                    c.DescrizioneSede = l.RS_Corso[j].RS_Desc_Sede;
                                    c.IndirizzoSede = l.RS_Corso[j].RS_Indi_Sede;

                                    c.Descrizione = l.RS_Corso[j].RS_Desc;

                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Sca))
                                    {
                                        c.DataScadenza = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Sca, "yyyy-MM-dd", null);
                                    }

                                    c.Progressivo = l.RS_Corso[j].RS_Prog;

                                    if (l.RS_Corso[j].RS_Cauz != null)
                                    {
                                        c.CostoCauzione = l.RS_Corso[j].RS_Cauz;
                                    }

                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Pag_Cauz))
                                    {
                                        c.DataPagamentoCauzione = DateTime.ParseExact(l.RS_Corso[j].RS_Pag_Cauz, "yyyy-MM-dd", null);
                                    }
                                }

                                iscrizione.Corsi.Sort();
                            }
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, iscrImpXML, "ISDSE_StoricoDitta");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML delle iscrizioni per impresa", exc);
            }

            iscrizioni.Sort();

            return iscrizioni;
        }

        public LavoratoreCorsi GetIscrizioniLavoratore(Utente utente, Int32 idLavoratore, String codiceFiscaleLavoratore)
        {
            LavoratoreCorsi iscrizioni = null;
            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraFiltroIscrizioniLavoratoreXML(idLavoratore, codiceFiscaleLavoratore);
            String iscrLavXML = esemService.ISDSEStorico_IscrizioniLav(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(iscrLavXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_StoricoLav iscrLavXml = GetXmlIscrizioniLavoratore(iscrLavXML);

                    if (iscrLavXml != null
                        && iscrLavXml.Items != null
                        && iscrLavXml.Items.Length == 1
                        && iscrLavXml.Items[0].RS_Lavoratore != null)
                    {
                        iscrizioni = new LavoratoreCorsi();
                        for (Int32 i = 0; i < iscrLavXml.Items[0].RS_Lavoratore.Length; i++)
                        {
                            ISDSE_RS_StoricoLavRS_StoricoLavRS_Lavoratore l = iscrLavXml.Items[0].RS_Lavoratore[i];

                            iscrizioni.Lavoratore = new Lavoratore();

                            if (!String.IsNullOrWhiteSpace(l.RS_Cod_Lav))
                                iscrizioni.Lavoratore.IdLavoratore = Int32.Parse(l.RS_Cod_Lav);
                            iscrizioni.Lavoratore.Cognome = l.RS_Cognome;
                            iscrizioni.Lavoratore.Nome = l.RS_Nome;
                            iscrizioni.Lavoratore.CodiceFiscale = l.RS_CF_Lavoratore;

                            if (l.RS_Corso != null)
                            {
                                for (Int32 j = 0; j < l.RS_Corso.Length; j++)
                                {
                                    Corso c = new Corso();
                                    iscrizioni.Corsi.Add(c);

                                    c.Codice = l.RS_Corso[j].RS_Codice;
                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Ini))
                                    {
                                        c.DataInizio = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Ini, "yyyy-MM-dd", null);
                                    }
                                    c.CodiceSede = l.RS_Corso[j].RS_Sede;
                                    c.DescrizioneSede = l.RS_Corso[j].RS_Desc_Sede;
                                    c.IndirizzoSede = l.RS_Corso[j].RS_Indi_Sede;

                                    c.Descrizione = l.RS_Corso[j].RS_Desc;

                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Data_Sca))
                                    {
                                        c.DataScadenza = DateTime.ParseExact(l.RS_Corso[j].RS_Data_Sca, "yyyy-MM-dd", null);
                                    }
                                    c.Progressivo = l.RS_Corso[j].RS_Prog;

                                    if (l.RS_Corso[j].RS_Cauz != null)
                                    {
                                        c.CostoCauzione = l.RS_Corso[j].RS_Cauz;
                                    }

                                    if (!String.IsNullOrWhiteSpace(l.RS_Corso[j].RS_Pag_Cauz))
                                    {
                                        c.DataPagamentoCauzione = DateTime.ParseExact(l.RS_Corso[j].RS_Pag_Cauz, "yyyy-MM-dd", null);
                                    }
                                }

                                iscrizioni.Corsi.Sort();
                            }
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, iscrLavXML, "ISDSE_StoricoLav");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML delle iscrizioni per lavoratore", exc);
            }

            return iscrizioni;
        }

        public Boolean IscriviLavoratore(out String messaggio, Utente utente, Iscrizione iscrizione)
        {
            Boolean stato = false;
            messaggio = String.Empty;

            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraIscrizioneXML(iscrizione);
            String iscrLavXML = esemService.ISDSEIscrizione_Corso(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(iscrLavXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_IscrizioneCorso iscrLavXml = GetXmlIscrizioneLavoratore(iscrLavXML);

                    if (iscrLavXml != null
                        && iscrLavXml.Items != null
                        && iscrLavXml.Items.Length == 1
                        && (iscrLavXml.Items[0].RS_Esito != null || iscrLavXml.Items[0].RS_Segnalazione != null))
                    {
                        if (iscrLavXml.Items[0].RS_Esito != null
                            && iscrLavXml.Items[0].RS_Esito == "OK")
                        {
                            stato = true;
                        }

                        if (iscrLavXml.Items[0].RS_Segnalazione != null)
                        {
                            messaggio = iscrLavXml.Items[0].RS_Segnalazione;
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, iscrLavXML, "ISDSEIscrizione_Corso");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML dell'iscrizione ai corsi", exc);
            }


            return stato;
        }

        public Boolean CancellaIscrizione(out String messaggio, Utente utente, Iscrizione iscrizione)
        {
            Boolean stato = false;
            messaggio = "OK";

            String aut = GeneraCredenzialiServizioXML(utente);
            String fil = GeneraCancellazioneIscrizioneXML(iscrizione);
            String cancIscrXML = esemService.ISDSECancella_Iscrizione(aut, fil);

            try
            {
                ISDSE_Esito esito = GetXmlEsito(cancIscrXML);
                if (esito.RS_EsitoRichiesta == "0")
                {
                    ISDSE_RS_CancellaIscrizione cancIscrXml = GetXmlCancellazioneIscrizioneLavoratore(cancIscrXML);

                    if (cancIscrXml != null
                        && cancIscrXml.Items != null
                        && cancIscrXml.Items.Length == 1
                        && (cancIscrXml.Items[0].RS_Esito != null || cancIscrXml.Items[0].RS_Segnalazione != null))
                    {
                        if (cancIscrXml.Items[0].RS_Esito != null
                            && cancIscrXml.Items[0].RS_Esito == "OK")
                        {
                            stato = true;
                        }

                        if (cancIscrXml.Items[0].RS_Segnalazione != null)
                        {
                            messaggio = cancIscrXml.Items[0].RS_Segnalazione;
                        }
                    }
                }
                else
                {
                    GestioneErrori(esito, cancIscrXML, "ISDSECancella_Iscrizione");
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Si è verificato un errore nel parsing dell'XML della cancellazione dell'iscrizione ai corsi", exc);
            }


            return stato;
        }

        public Boolean CorsiGratuita(Int32 idLavoratore, DateTime dataIscrizione)
        { 
            Boolean res = false;
            using (DbCommand comando = DatabaseCemi.GetSqlStringCommand("SELECT dbo.UF_CorsiGratuita(@dataIscrizioneCorso,@idLavoratore)"))
            {
                DatabaseCemi.AddInParameter(comando,"@dataIscrizioneCorso", DbType.DateTime,dataIscrizione);
                DatabaseCemi.AddInParameter(comando,"@idLavoratore", DbType.Int32,idLavoratore);
                
                res = (Boolean)DatabaseCemi.ExecuteScalar(comando);
            }
            return res;
        }

        public Boolean CorsiTicket(DateTime dataCorso, DateTime dataAssunzione, Boolean primaEsperienza)
        { 
            Boolean res = false;
            using (DbCommand comando = DatabaseCemi.GetSqlStringCommand("SELECT dbo.UF_CorsiDiritto(@dataCorso,@dataAssunzione,@primaEsperienza)"))
            {
                DatabaseCemi.AddInParameter(comando,"@dataCorso", DbType.DateTime,dataCorso);
                DatabaseCemi.AddInParameter(comando,"@dataAssunzione", DbType.DateTime,dataAssunzione);
                DatabaseCemi.AddInParameter(comando, "@primaEsperienza", DbType.Boolean, primaEsperienza);
                
                res = (Boolean)DatabaseCemi.ExecuteScalar(comando);
            }
            return res;
        }

        public void GestioneErrori(ISDSE_Esito esito, String xmlServ, String nomeMetodoWS)
        {
            RS_ListaErrori xmlErrori = GetXmlErrori(xmlServ);
            if (xmlErrori.Items != null && xmlErrori.Items.Length == 2)
            {
                // L'errore con codice 3 è "Occorrenze non trovate", non dobbiamo fare niente
                if (((RS_ListaErroriRS_CodiceErrore)(xmlErrori.Items[0])).Value != "3")
                {
                    throw new Exception(String.Format("Esito negativo ({0}) della richiesta per {1}: {2}",
                        esito.RS_EsitoRichiesta,
                        nomeMetodoWS,
                        ((RS_ListaErroriRS_DescrizioneErrore)(xmlErrori.Items[1])).Value));
                }
            }
            else
            {
                throw new Exception(String.Format("Esito negativo ({0}) della richiesta per {1}", esito.RS_EsitoRichiesta, nomeMetodoWS));
            }
        }

        #region XML strutturati Risposte
        private ISDSE_Esito GetXmlEsito(String serviceRes)
        {
            ISDSE_Esito esito = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_Esito")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_Esito));
            TextReader reader = new StringReader(esitoXml);
            esito = (ISDSE_Esito) deserializer.Deserialize(reader);

            return esito;
        }

        private ISDSE_RS_ElencoTipoCorso GetXmlTipiCorso(String serviceRes)
        {
            ISDSE_RS_ElencoTipoCorso tipiCorso = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_ElencoTipoCorso")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_ElencoTipoCorso));
            TextReader reader = new StringReader(esitoXml);
            tipiCorso = (ISDSE_RS_ElencoTipoCorso) deserializer.Deserialize(reader);

            return tipiCorso;
        }

        private ISDSE_RS_ElencoSedeCorso GetXmlSedi(String serviceRes)
        {
            ISDSE_RS_ElencoSedeCorso sedi = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_ElencoSedeCorso")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_ElencoSedeCorso));
            TextReader reader = new StringReader(esitoXml);
            sedi = (ISDSE_RS_ElencoSedeCorso) deserializer.Deserialize(reader);

            return sedi;
        }

        private ISDSE_RS_ElencoCorsi GetXmlCorsi(String serviceRes)
        {
            ISDSE_RS_ElencoCorsi corsi = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_ElencoCorsi")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_ElencoCorsi));
            TextReader reader = new StringReader(esitoXml);
            corsi = (ISDSE_RS_ElencoCorsi) deserializer.Deserialize(reader);

            return corsi;
        }

        private ISDSE_RS_CorsiScadenzaDitta GetXmlCorsiScadenzaImpresa(String serviceRes)
        {
            ISDSE_RS_CorsiScadenzaDitta corsiScad = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_CorsiScadenzaDitta")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_CorsiScadenzaDitta));
            TextReader reader = new StringReader(esitoXml);
            corsiScad = (ISDSE_RS_CorsiScadenzaDitta) deserializer.Deserialize(reader);

            return corsiScad;
        }

        private ISDSE_RS_CorsiScadenzaLav GetXmlCorsiScadenzaLavoratore(String serviceRes)
        {
            ISDSE_RS_CorsiScadenzaLav corsiScad = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_CorsiScadenzaLav")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_CorsiScadenzaLav));
            TextReader reader = new StringReader(esitoXml);
            corsiScad = (ISDSE_RS_CorsiScadenzaLav) deserializer.Deserialize(reader);

            return corsiScad;
        }

        private ISDSE_RS_StoricoDitta GetXmlIscrizioniImpresa(String serviceRes)
        {
            ISDSE_RS_StoricoDitta iscrImp = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_StoricoDitta")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_StoricoDitta));
            TextReader reader = new StringReader(esitoXml);
            iscrImp = (ISDSE_RS_StoricoDitta) deserializer.Deserialize(reader);

            return iscrImp;
        }

        private ISDSE_RS_StoricoLav GetXmlIscrizioniLavoratore(String serviceRes)
        {
            ISDSE_RS_StoricoLav iscrLav = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_StoricoLav")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_StoricoLav));
            TextReader reader = new StringReader(esitoXml);
            iscrLav = (ISDSE_RS_StoricoLav) deserializer.Deserialize(reader);

            return iscrLav;
        }

        private ISDSE_RS_IscrizioneCorso GetXmlIscrizioneLavoratore(String serviceRes)
        {
            ISDSE_RS_IscrizioneCorso iscrLav = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_IscrizioneCorso")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_IscrizioneCorso));
            TextReader reader = new StringReader(esitoXml);
            iscrLav = (ISDSE_RS_IscrizioneCorso) deserializer.Deserialize(reader);

            return iscrLav;
        }

        private ISDSE_RS_CancellaIscrizione GetXmlCancellazioneIscrizioneLavoratore(String serviceRes)
        {
            ISDSE_RS_CancellaIscrizione cancIscrLav = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("ISDSE_RS_CancellaIscrizione")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(ISDSE_RS_CancellaIscrizione));
            TextReader reader = new StringReader(esitoXml);
            cancIscrLav = (ISDSE_RS_CancellaIscrizione) deserializer.Deserialize(reader);

            return cancIscrLav;
        }

        private RS_ListaErrori GetXmlErrori(String serviceRes)
        {
            RS_ListaErrori errori = null;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(serviceRes);

            String esitoXml = doc.GetElementsByTagName("RS_ListaErrori")[0].OuterXml;
            XmlSerializer deserializer = new XmlSerializer(typeof(RS_ListaErrori));
            TextReader reader = new StringReader(esitoXml);
            errori = (RS_ListaErrori)deserializer.Deserialize(reader);

            return errori;
        }
        #endregion

        #region Creazione XML input
        private String GeneraFiltroCorsiServizioXML(CorsoFilter filter)
        {
            ISDSE_ElencoCorsi elCor = GetFiltroCorsiServizio(filter);
            return GetXMLSerializzato(elCor);
        }

        private ISDSE_ElencoCorsi GetFiltroCorsiServizio(CorsoFilter filter)
        {
            ISDSE_ElencoCorsi filt = new ISDSE_ElencoCorsi();
            filt.Items = new ISDSE_ElencoCorsiCEM_RicercaCorsi[1];
            filt.Items[0] = new ISDSE_ElencoCorsiCEM_RicercaCorsi();

            filt.Items[0].CEM_Data_Ini = filter.Dal.ToString("yyyy-MM-dd");
            filt.Items[0].CEM_Data_Fin = filter.Al.ToString("yyyy-MM-dd");
            filt.Items[0].CEM_Tipo_Corso = HttpContext.Current.Server.HtmlEncode(filter.CodiceTipoCorso);
            filt.Items[0].CEM_Sede_Corso = HttpContext.Current.Server.HtmlEncode(filter.CodiceSede);

            return filt;
        }

        private String GeneraFiltroCorsiScadenzaImpresaXML(String codiceFiscaleImpresa, Int32? idImpresa)
        {
            ISDSE_CorsiScadenzaDitta cScad = GetFiltroCorsiScadenzaImpresaServizio(codiceFiscaleImpresa, idImpresa);
            return GetXMLSerializzato(cScad);
        }

        private ISDSE_CorsiScadenzaDitta GetFiltroCorsiScadenzaImpresaServizio(String codiceFiscaleImpresa, Int32? idImpresa)
        {
            ISDSE_CorsiScadenzaDitta filt = new ISDSE_CorsiScadenzaDitta();
            filt.Items = new ISDSE_CorsiScadenzaDittaCEM_RicercaScaDitta[1];
            filt.Items[0] = new ISDSE_CorsiScadenzaDittaCEM_RicercaScaDitta();

            if (idImpresa.HasValue)
            {
                filt.Items[0].CEM_CF_Ditta = HttpContext.Current.Server.HtmlEncode(codiceFiscaleImpresa);
                filt.Items[0].CEM_Cod_Ditta_Cassa = idImpresa.ToString();
            }
            else
            {
                filt.Items[0].CEM_CF_Ditta = HttpContext.Current.Server.HtmlEncode(codiceFiscaleImpresa);
                filt.Items[0].CEM_Cod_Ditta_Cassa = String.Empty;
            }


            return filt;
        }

        private String GeneraFiltroCorsiScadenzaLavoratoreXML(String codiceFiscaleLavoratore, Int32? idLavoratore)
        {
            ISDSE_CorsiScadenzaLav cScad = GetFiltroCorsiScadenzaLavoratoreServizio(codiceFiscaleLavoratore, idLavoratore);
            return GetXMLSerializzato(cScad);
        }

        private ISDSE_CorsiScadenzaLav GetFiltroCorsiScadenzaLavoratoreServizio(String codiceFiscaleLavoratore, Int32? idLavoratore)
        {
            ISDSE_CorsiScadenzaLav filt = new ISDSE_CorsiScadenzaLav();
            filt.Items = new ISDSE_CorsiScadenzaLavCEM_RicercaScaLav[1];
            filt.Items[0] = new ISDSE_CorsiScadenzaLavCEM_RicercaScaLav();



            if (idLavoratore.HasValue)
            {
                filt.Items[0].CEM_CF_Lavoratore = HttpContext.Current.Server.HtmlEncode(codiceFiscaleLavoratore);
                filt.Items[0].CEM_Cod_Lav_Cassa= idLavoratore.ToString();
            }
            else
            {
                filt.Items[0].CEM_CF_Lavoratore = HttpContext.Current.Server.HtmlEncode(codiceFiscaleLavoratore);
                filt.Items[0].CEM_Cod_Lav_Cassa = String.Empty;
            }


            return filt;
        }

        private String GeneraFiltroIscrizioniImpresaXML(String codiceFiscaleImpresa, Int32? idImpresa)
        {
            ISDSE_StoricoDitta sImp = GetFiltroIscrizioniImpresaServizio(codiceFiscaleImpresa, idImpresa);
            return GetXMLSerializzato(sImp);
        }

        private ISDSE_StoricoDitta GetFiltroIscrizioniImpresaServizio(String codiceFiscaleImpresa, Int32? idImpresa)
        {
            ISDSE_StoricoDitta filt = new ISDSE_StoricoDitta();
            
            filt.Items = new ISDSE_StoricoDittaCEM_StoricoDitta[1];
            filt.Items[0] = new ISDSE_StoricoDittaCEM_StoricoDitta();

            if (idImpresa.HasValue)
            {
                filt.Items[0].CEM_CF_Ditta = HttpContext.Current.Server.HtmlEncode(codiceFiscaleImpresa);
                filt.Items[0].CEM_Cod_Ditta_Cassa = idImpresa.ToString();
            }
            else
            {
                filt.Items[0].CEM_CF_Ditta = HttpContext.Current.Server.HtmlEncode(codiceFiscaleImpresa);
                filt.Items[0].CEM_Cod_Ditta_Cassa = String.Empty;
            }

            return filt;
        }

        private String GeneraFiltroIscrizioniLavoratoreXML(Int32 idLavoratore, String codiceFiscaleLavoratore)
        {
            ISDSE_StoricoLav sLav = GetFiltroIscrizioniLavoratoreServizio(idLavoratore, codiceFiscaleLavoratore);
            return GetXMLSerializzato(sLav);
        }

        private ISDSE_StoricoLav GetFiltroIscrizioniLavoratoreServizio(Int32 idLavoratore, String codiceFiscaleLavoratore)
        {
            ISDSE_StoricoLav filt = new ISDSE_StoricoLav();
            filt.Items = new ISDSE_StoricoLavCEM_StoricoLav[1];
            filt.Items[0] = new ISDSE_StoricoLavCEM_StoricoLav();

            filt.Items[0].CEM_CF_Lavoratore = HttpContext.Current.Server.HtmlEncode(codiceFiscaleLavoratore);
            filt.Items[0].CEM_Cod_Lav_Cassa = idLavoratore.ToString();

            return filt;
        }

        private String GeneraIscrizioneXML(Iscrizione iscrizione)
        {
            ISDSE_IscrizioneCorso sLav = GetIscrizioneServizio(iscrizione);
            return GetXMLSerializzato(sLav);
        }

        private ISDSE_IscrizioneCorso GetIscrizioneServizio(Iscrizione iscrizione)
        {
            ISDSE_IscrizioneCorso filt = new ISDSE_IscrizioneCorso();
            filt.CEM_Iscrizione = new ISDSE_IscrizioneCorsoCEM_Iscrizione();

            filt.CEM_Iscrizione.CEM_Anag_Lav = new ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Anag_Lav();
            filt.CEM_Iscrizione.CEM_Corso = new ISDSE_IscrizioneCorsoCEM_IscrizioneCEM_Corso();

            filt.CEM_Iscrizione.CEM_Corso.CEM_Codice = iscrizione.Corso.Codice;
            filt.CEM_Iscrizione.CEM_Corso.CEM_Data_Ini = iscrizione.Corso.DataInizio.Value.ToString("yyyy-MM-dd");
            filt.CEM_Iscrizione.CEM_Corso.CEM_Sede = iscrizione.Corso.CodiceSede;
            filt.CEM_Iscrizione.CEM_Corso.CEM_Prog = iscrizione.Corso.Progressivo;

            // --- GESTIONE TICKET E PAGAMENTO CORSO - INIZIO
            filt.CEM_Iscrizione.CEM_Corso.CEM_Ticket = iscrizione.Ticket ? "1" : "0";
            filt.CEM_Iscrizione.CEM_Corso.CEM_Pag_Cor = iscrizione.Gratuito ? "1" : "0";
            // --- GESTIONE TICKET E PAGAMENTO CORSO - FINE

            if (iscrizione.Lavoratore.IdAnagraficaCondivisa > 0)
            {
                if (iscrizione.Lavoratore.IdLavoratore.HasValue)
                {
                    filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Cod = iscrizione.Lavoratore.IdLavoratore.ToString();
                }
                else
                {
                    filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Cod = iscrizione.Lavoratore.IdAnagraficaCondivisa.ToString();
                }
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Cognome = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nome = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Sesso = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nato_Luo = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nato_Il = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Com_Res = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Indi_Res = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_CAP_Res = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Codi_Fis = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nume_Tel = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nume_Cel = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Mail = String.Empty;
            }
            else
            {
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Cod = String.Empty;
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Cognome = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.Cognome) ? String.Empty : iscrizione.Lavoratore.Cognome);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nome = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.Nome) ? String.Empty : iscrizione.Lavoratore.Nome);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Sesso = String.IsNullOrEmpty(iscrizione.Lavoratore.Sesso.ToString()) ? String.Empty : iscrizione.Lavoratore.Sesso.ToString();
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nato_Luo = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.CodiceCatastaleComuneNascita) ? String.Empty : iscrizione.Lavoratore.CodiceCatastaleComuneNascita);
                if (iscrizione.Lavoratore.DataNascita > new DateTime())
                {
                    filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nato_Il = iscrizione.Lavoratore.DataNascita.ToString("yyyy-MM-dd");
                }
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Com_Res = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.IndirizzoComune) ? String.Empty : iscrizione.Lavoratore.IndirizzoComune);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Indi_Res = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.IndirizzoDenominazione) ? String.Empty : iscrizione.Lavoratore.IndirizzoDenominazione);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_CAP_Res = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.IndirizzoCap) ? String.Empty : iscrizione.Lavoratore.IndirizzoCap);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Codi_Fis = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.CodiceFiscale) ? String.Empty : iscrizione.Lavoratore.CodiceFiscale);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nume_Tel = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.Telefono) ? String.Empty : iscrizione.Lavoratore.Telefono);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Nume_Cel = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.Cellulare) ? String.Empty : iscrizione.Lavoratore.Cellulare);
                filt.CEM_Iscrizione.CEM_Anag_Lav.CEM_Mail = HttpContext.Current.Server.HtmlEncode(String.IsNullOrEmpty(iscrizione.Lavoratore.Email) ? String.Empty : iscrizione.Lavoratore.Email);
            }

            if (iscrizione.Impresa != null)
            {
                filt.CEM_Iscrizione.CEM_Impresa = iscrizione.Impresa.IdImpresa.ToString();
            }
            else
            {
                filt.CEM_Iscrizione.CEM_Impresa = String.Empty;
            }

            filt.CEM_Iscrizione.CEM_Contatto = iscrizione.Contatto;

            return filt;
        }

        private String GeneraCancellazioneIscrizioneXML(Iscrizione iscrizione)
        {
            ISDSE_CancellaIscrizione cIscr = GetCancellazioneIscrizioneServizio(iscrizione);
            return GetXMLSerializzato(cIscr);
        }

        private ISDSE_CancellaIscrizione GetCancellazioneIscrizioneServizio(Iscrizione iscrizione)
        {
            ISDSE_CancellaIscrizione filt = new ISDSE_CancellaIscrizione();
            filt.Items = new ISDSE_CancellaIscrizioneCEM_Cancella[1];
            filt.Items[0] = new ISDSE_CancellaIscrizioneCEM_Cancella();

            filt.Items[0].CEM_Corso = new ISDSE_CancellaIscrizioneCEM_CancellaCEM_Corso[1];
            filt.Items[0].CEM_Corso[0] = new ISDSE_CancellaIscrizioneCEM_CancellaCEM_Corso();

            filt.Items[0].CEM_Corso[0].CEM_Codice = HttpContext.Current.Server.HtmlEncode(iscrizione.Corso.Codice);
            filt.Items[0].CEM_Corso[0].CEM_Data_Ini = iscrizione.Corso.DataInizio.Value.ToString("yyyy-MM-dd");
            filt.Items[0].CEM_Corso[0].CEM_Sede = HttpContext.Current.Server.HtmlEncode(iscrizione.Corso.CodiceSede);
            filt.Items[0].CEM_Corso[0].CEM_Prog = iscrizione.Corso.Progressivo;

            filt.Items[0].CEM_Lavoratore = new ISDSE_CancellaIscrizioneCEM_CancellaCEM_Lavoratore[1];
            filt.Items[0].CEM_Lavoratore[0] = new ISDSE_CancellaIscrizioneCEM_CancellaCEM_Lavoratore();

            if (iscrizione.Lavoratore.IdLavoratore.HasValue)
            {
                filt.Items[0].CEM_Lavoratore[0].CEM_Cod_Lav_Cassa = HttpContext.Current.Server.HtmlEncode(iscrizione.Lavoratore.IdLavoratore.ToString());
                //filt.Items[0].CEM_Lavoratore[0].CEM_Cod_Lav_Cassa = String.Empty;
                filt.Items[0].CEM_Lavoratore[0].CEM_CF_Lavoratore = HttpContext.Current.Server.HtmlEncode(iscrizione.Lavoratore.CodiceFiscale);
            }
            else
            {
                filt.Items[0].CEM_Lavoratore[0].CEM_Cod_Lav_Cassa = String.Empty;
                filt.Items[0].CEM_Lavoratore[0].CEM_CF_Lavoratore = HttpContext.Current.Server.HtmlEncode(iscrizione.Lavoratore.CodiceFiscale);
            }

            return filt;
        }
        #endregion

        #region Credenziali accesso
        private String GeneraCredenzialiServizioXML(Utente utente)
        {
            ISDSE_Autenticazione cred = GetCredenzialiServizio(utente);
            return GetXMLSerializzato(cred);
        }

        private ISDSE_Autenticazione GetCredenzialiServizio(Utente utente)
        {
            ISDSE_Autenticazione autRes = new ISDSE_Autenticazione();
            autRes.Items = new ISDSE_AutenticazioneCEM_Autenticazione[1];
            autRes.Items[0] = new ISDSE_AutenticazioneCEM_Autenticazione();

            autRes.Items[0].CEM_CodiSoc = "MI0";
            autRes.Items[0].CEM_Password = String.Empty;

            switch (utente.GetType().ToString())
            {
                case "TBridge.Cemi.GestioneUtenti.Type.Entities.Impresa":
                    Impresa imp = (Impresa) utente;
                    autRes.Items[0].CEM_Identificativo = "IM";
                    autRes.Items[0].CEM_CodiceUtente = imp.IdImpresa.ToString();
                    break;
                case "TBridge.Cemi.GestioneUtenti.Type.Entities.Consulente":
                    Consulente cons = (Consulente) utente;
                    autRes.Items[0].CEM_Identificativo = "CS";
                    autRes.Items[0].CEM_CodiceUtente = cons.IdConsulente.ToString();
                    break;
                case "TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore":
                    TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore lav = (TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore) utente;
                    autRes.Items[0].CEM_Identificativo = "DI";
                    autRes.Items[0].CEM_CodiceUtente = lav.IdLavoratore.ToString();
                    break;
            }
            //autRes.CEM_Autenticazione.CEM_CodiceUtente = "milano";
            //autRes.CEM_Autenticazione.CEM_Identificativo = "AM";

            return autRes;
        }

        private String GetXMLSerializzato(object obj)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
            XmlSerializerNamespaces xnameSpace = new XmlSerializerNamespaces();
            xnameSpace.Add("", "");

            StringWriter textWriter = new StringWriter();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = false;
            settings.OmitXmlDeclaration = true;
            XmlWriter writer = XmlWriter.Create(textWriter, settings);
            xmlSerializer.Serialize(writer, obj, xnameSpace);

            //xmlSerializer.Serialize(textWriter, aut);
            return textWriter.ToString();
        }
        #endregion
    }
}
