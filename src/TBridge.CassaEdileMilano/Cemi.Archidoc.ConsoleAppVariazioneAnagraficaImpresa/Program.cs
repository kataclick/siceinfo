﻿using System;
using System.Configuration;
using TBridge.Cemi.Archidoc.Business;
using TBridge.Cemi.Archidoc.Type.Collection;

namespace Cemi.Archidoc.ConsoleAppVariazioneAnagraficaImpresa
{
    class Program
    {
        private const string NOME_TIPO_DOCUMENTO_LIBRO_UNICO = "Libro Unico";
        private const string NOME_TIPO_DOCUMENTO_DENUNCE_ACE = "Denunce altre CE";
        private const string NOME_TIPO_DOCUMENTO_UNIEMENS = "Uniemens";

        private static readonly String archidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
        private static readonly String archidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
        private static readonly String archidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
        private static readonly String archidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
        private static readonly String archidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
        private static readonly String archidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

        static void Main(string[] args)
        {
            ArchidocBusiness _biz = new ArchidocBusiness();

            _biz.InsertLog("START_VariazioniStatoImpresa", String.Empty);
            Console.WriteLine("START_VariazioniStatoImpresa");

            try
            {
                ElaboraDocumentiSospensione(_biz);
            }
            catch (Exception ex)
            {
                _biz.InsertLog("ERROR_VariazioniStatoImpresa", ex.Message);
                Console.WriteLine($"ERROR_VariazioniStatoImpresa: {ex.Message}");
            }

            _biz.InsertLog("END_VariazioniStatoImpresa", String.Empty);
            Console.WriteLine("END_VariazioniStatoImpresa");

        }

        private static void ElaboraDocumentiSospensione(ArchidocBusiness _biz)
        {

            _biz.InsertLog("READ_ImpreseRichiesteVariazioneRecapitoLibroUnico", String.Empty);
            Console.WriteLine($"READ_ImpreseRichiesteVariazioneRecapitoLibroUnico");
            LibroUnicoCollection documentiSospensioniLU = _biz.GetDocumentiSospensioneImpresaLibroUnico();

            if (documentiSospensioniLU.Count > 0)
            {
                _biz.InsertLog("ARCHIDOC_INSERT_LibroUnico", $"Documenti da inserire: {documentiSospensioniLU.Count}");
                Console.WriteLine($"ARCHIDOC_INSERT_LibroUnico: Documenti da inserire: {documentiSospensioniLU.Count}");

                _biz.InsertCardsWithDocumentsInArchidoc(NOME_TIPO_DOCUMENTO_LIBRO_UNICO, documentiSospensioniLU, true);

                _biz.InsertLog("UPDATE_LibroUnico", String.Empty);
                Console.WriteLine($"UPDATE_LibroUnico:");

                _biz.LibroUnicoUpdateIdArchidoc(documentiSospensioniLU);
            }

            _biz.InsertLog("READ_ImpreseRichiesteVariazioneRecapitoDenunceACE", String.Empty);
            Console.WriteLine($"READ_ImpreseRichiesteVariazioneRecapitoDenunceACE");
            DenunciaACECollection documentiSospensioniDACE = _biz.GetDocumentiSospensioneImpresaDenunceACE();

            if (documentiSospensioniDACE.Count > 0)
            {
                _biz.InsertLog("ARCHIDOC_INSERT_DenunciaACE", $"Documenti da inserire: {documentiSospensioniDACE.Count}");
                Console.WriteLine($"ARCHIDOC_INSERT_DenunciaACE: Documenti da inserire: {documentiSospensioniDACE.Count}");

                _biz.InsertCardsWithDocumentsInArchidoc(NOME_TIPO_DOCUMENTO_DENUNCE_ACE, documentiSospensioniDACE, true);

                _biz.InsertLog("UPDATE_DenunciaACE", String.Empty);
                Console.WriteLine($"UPDATE_DenunciaACE");

                _biz.DenunceACEUpdateIdArchidoc(documentiSospensioniDACE);
            }

            _biz.InsertLog("READ_ImpreseRichiesteVariazioneRecapitoUniEmens", String.Empty);
            Console.WriteLine($"READ_ImpreseRichiesteVariazioneRecapitoUniEmens");
            UniemensCollection documentiSospensioniUE = _biz.GetDocumentiSospensioneImpresaUniemens();

            if (documentiSospensioniUE.Count > 0)
            {
                _biz.InsertLog("ARCHIDOC_INSERT_UniEmens", $"Documenti da inserire: {documentiSospensioniUE.Count}");
                Console.WriteLine($"ARCHIDOC_INSERT_UniEmens: Documenti da inserire: {documentiSospensioniUE.Count}");

                _biz.InsertCardsWithDocumentsInArchidoc(NOME_TIPO_DOCUMENTO_UNIEMENS, documentiSospensioniUE, true);

                _biz.InsertLog("UPDATE_UniEmens", String.Empty);
                Console.WriteLine($"UPDATE_UniEmens:");

                _biz.UniemensUpdateIdArchidoc(documentiSospensioniUE);
            }
        }
    }
}
