﻿using System;
using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.SelestaProvider;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.ConsoleAppWhitelist
{

    class Program
    {
        private static readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
        private static readonly TymbroConnector _TymbroBiz = new TymbroConnector();
        private static readonly TrexomConnector _TrexomBiz = new TrexomConnector();

        static void Main(string[] args)
        {
            if (ConfigurationManager.AppSettings["InserimentoWhitelistTymbroNuovoServerAttivo"] == "1")
            {
                Console.WriteLine("INIZIO INVIO WHITELIST TYMBRO");
                try
                {
                    UpdateWhitelistTymbro();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("FINE INVIO WHITELIST TYMBRO");
            }

            if (ConfigurationManager.AppSettings["InserimentoWhitelistSelestaAttivo"] == "1")
            {
                Console.WriteLine("INIZIO INVIO WHITELIST SELESTA");
                try
                {
                    UpdateWhitelistClickFind();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("FINE INVIO WHITELIST SELESTA");
            }

            if (ConfigurationManager.AppSettings["InserimentoWhitelistTrexomNuovoServerAttivo"] == "1")
            {
                Console.WriteLine("INIZIO INVIO WHITELIST TREXOM");
                try
                {
                    UpdateWhitelistTrexom();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.WriteLine("FINE INVIO WHITELIST TREXOM");
            }
        }

        private static void UpdateWhitelistTymbro()
        {
            FornitoreCollection fornitori = _biz.GetFornitori();

            int idFornitore = -1;
            foreach (Fornitore forn in fornitori)
            {
                if (forn.RagioneSociale == "Tymbro")
                {
                    idFornitore = forn.IdFornitore;
                    break;
                }
            }

            RilevatoreCollection rilevatori = _biz.GetRilevatoriAttiviByIdFornitore(idFornitore);

            foreach (Rilevatore ril in rilevatori)
            {
                if (ril.InvioWhitelist)
                {
                    Console.WriteLine(ril.Codice);
                    //_TymbroBiz.DeleteWhitelist(ril.Codice);
                    //List<string> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListByCodRilevatore(ril.Codice);
                    //_TymbroBiz.CaricaWhitelist(codiciFiscaliAbilitatiList, ril.Codice);
                    Dictionary<String, Boolean> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListCompletaByCodRilevatore(ril.IdRilevatore);
                    _TymbroBiz.CaricaWhitelist(codiciFiscaliAbilitatiList, ril.Codice);
                }
            }
        }

        private static void UpdateWhitelistTrexom()
        {
            FornitoreCollection fornitori = _biz.GetFornitori();

            int idFornitore = -1;
            foreach (Fornitore forn in fornitori)
            {
                if (forn.RagioneSociale == "Trexom")
                {
                    idFornitore = forn.IdFornitore;
                    break;
                }
            }

            RilevatoreCollection rilevatori = _biz.GetRilevatoriAttiviByIdFornitore(idFornitore);

            foreach (Rilevatore ril in rilevatori)
            {
                if (ril.InvioWhitelist)
                {
                    Console.WriteLine(ril.Codice);
                    Dictionary<String, Boolean> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListCompletaByCodRilevatore(ril.IdRilevatore);
                    _TrexomBiz.CaricaWhitelist(codiciFiscaliAbilitatiList, ril.Codice);
                }
            }
        }

        private static void UpdateWhitelistClickFind()
        {
            WSTimbrature ws = new WSTimbrature();

            FornitoreCollection fornitori = _biz.GetFornitori();

            int idFornitore = -1;
            foreach (Fornitore forn in fornitori)
            {
                if (forn.RagioneSociale == "Click & Find")
                {
                    idFornitore = forn.IdFornitore;
                    break;
                }
            }

            RilevatoreCollection rilevatori = _biz.GetRilevatoriAttiviByIdFornitore(idFornitore);

            foreach (Rilevatore ril in rilevatori)
            {
                if (ril.InvioWhitelist)
                {
                    Console.WriteLine(ril.Codice);
                    bool ret;
                    bool retSpec;

                    List<string> codiciFiscaliAbilitatiList = _biz.GetCodFiscWhiteListByCodRilevatore(ril.IdRilevatore);

                    string[] codiciFiscaliAbilitati = new string[codiciFiscaliAbilitatiList.Count];

                    int i = 0;
                    foreach (string codFisc in codiciFiscaliAbilitatiList)
                    {
                        codiciFiscaliAbilitati[i] = codFisc;
                        i++;
                    }

                    ws.insertAccessList(ril.Codice, codiciFiscaliAbilitati, out ret, out retSpec);
                }
            }
        }
    }
}

