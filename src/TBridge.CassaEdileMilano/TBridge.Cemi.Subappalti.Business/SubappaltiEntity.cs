using TBridge.Cemi.GestioneUtenti.Type.Entities;

namespace TBridge.Cemi.Subappalti.Business
{
    /// <summary>
    /// Classe business di base Subappalti
    /// </summary>
    public abstract class SubappaltiEntity
    {
        /// <summary>
        /// Numero di telefono cellulare dell'entity - riferimento interno
        /// </summary>
        protected string cellulare;

        /// <summary>
        /// Codice fiscale dell'entity - riferimento interno
        /// </summary>
        protected string codiceFiscale;

        /// <summary>
        /// Indirizzo di posta elettronica dell'entity - riferimento interno
        /// </summary>
        protected string email;

        /// <summary>
        /// Codice identificativo unico dell'entity - riferimento interno
        /// </summary>
        protected int id;

        /// <summary>
        /// Numero telefonico dell'entity - riferimento interno
        /// </summary>
        protected string telefono;

        /// <summary>
        /// Identifica se l'entity � anche un utente - Riferimento interno
        /// </summary>
        protected Utente utente;

        /// <summary>
        /// Codice identificativo unico dell'entity
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Identifica se l'entity � anche un utente - Riferimento interno
        /// </summary>
        public Utente Utente
        {
            get { return utente; }
            set { utente = value; }
        }

        /// <summary>
        /// Codice fiscale dell'entity
        /// </summary>
        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        /// <summary>
        /// Numero telefonico dell'entity
        /// </summary>
        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        /// <summary>
        /// Numero di telefono cellulare dell'entity
        /// </summary>
        public string Cellulare
        {
            get { return cellulare; }
            set { cellulare = value; }
        }

        /// <summary>
        /// Indirizzo di posta elettronica dell'entity
        /// </summary>
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
    } // Class
} // NameSpace