using System;

namespace TBridge.Cemi.Subappalti.Business
{
    /// <summary>
    /// Business Entity che identifica un lavoratore, compreso il rapporto di lavoro con l'impresa
    /// </summary>
    public class LavoratoreEntity : SubappaltiEntity
    {
        #region ProprietÓ

        /// <summary>
        /// Cognome dell'utente - riferimento interno
        /// </summary>
        private string cognome;

        /// <summary>
        /// Nome dell'utente - riferimento interno
        /// </summary>
        private string nome;

        /// <summary>
        /// Cognome dell'utente
        /// </summary>
        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        /// <summary>
        /// Nome dell'utente
        /// </summary>
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        /// <summary>
        /// Data di inserimento nell'impresa
        /// </summary>
        public DateTime DataDenuncia { get; set; }

        #endregion

        #region Metodi pubblici

        /// <summary>
        /// Restituisce la stringa che rappresenta il lavoratore.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Id + " - " + nome + " " + cognome;
        }

        #endregion
    } // class
} // namespace