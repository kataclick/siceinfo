using System;
using System.Collections.Generic;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Prestazioni.Type.Collections
{
    [Serializable]
    public class PrestazioneImportoCollection : List<PrestazioneImporto>
    {
        public Decimal Totale()
        {
            Decimal totale = 0;

            foreach (PrestazioneImporto importo in this)
            {
                totale += importo.Valore;
            }

            return totale;
        }
    }
}