﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Prestazioni.Type.Collections
{
    public class DocumentoCopertinaCollection : List<DocumentoCopertina>
    {
        public Int32 NumeroDocumentiConsegnati
        {
            get
            {
                Int32 counter = 0;

                foreach (DocumentoCopertina doc in this)
                {
                    if (doc.Consegnato)
                    {
                        counter++;
                    }
                }

                return counter;
            }
        }
    }
}
