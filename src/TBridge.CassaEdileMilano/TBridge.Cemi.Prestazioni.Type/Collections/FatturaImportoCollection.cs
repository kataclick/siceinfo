using System;
using System.Collections.Generic;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Prestazioni.Type.Collections
{
    [Serializable]
    public class FatturaImportoCollection : List<FatturaImporto>
    {
        public Decimal TotaleFattureConImportoTotale()
        {
            Decimal importo = 0;

            foreach (FatturaImporto fImporto in this)
            {
                if (fImporto.Descrizione == "Importo Totale")
                {
                    importo += fImporto.Valore;
                }
            }

            return importo;
        }
    }
}