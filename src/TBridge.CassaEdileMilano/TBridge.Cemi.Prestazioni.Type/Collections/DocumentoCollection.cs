using System;
using System.Collections.Generic;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Prestazioni.Type.Collections
{
    [Serializable]
    public class DocumentoCollection : List<Documento>
    {
        public Boolean DocumentoGiaPresente(Documento documento)
        {
            foreach (Documento doc in this)
            {
                if (doc.TipoDocumento.IdTipoDocumento == documento.TipoDocumento.IdTipoDocumento
                    && doc.RiferitoA == documento.RiferitoA)
                {
                    return true;
                }
            }

            return false;
        }
    }
}