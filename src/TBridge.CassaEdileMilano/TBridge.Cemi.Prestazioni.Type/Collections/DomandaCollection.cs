using System;
using System.Collections.Generic;
using TBridge.Cemi.Prestazioni.Type.Entities;

namespace TBridge.Cemi.Prestazioni.Type.Collections
{
    [Serializable]
    public class DomandaCollection : List<Domanda>
    {
    }
}