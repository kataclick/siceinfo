using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Lavoratore
    {
        private string cellulare;
        private string codiceFiscale;
        private string cognome;
        private Comunicazioni comunicazioni;
        private Comunicazioni comunicazioniFornite;
        private bool? controlloCellulare;
        private bool? controlloEmail;
        private bool? controlloIndirizzo;
        private DateTime dataNascita;
        private string email;
        private int? idLavoratore;
        private string idTipoPagamento;
        private Indirizzo indirizzo;
        private Indirizzo indirizzoFornito;
        private string nome;

        public int? IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public DateTime DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }

        public Indirizzo Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }

        public Comunicazioni Comunicazioni
        {
            get { return comunicazioni; }
            set { comunicazioni = value; }
        }

        public Indirizzo IndirizzoFornito
        {
            get { return indirizzoFornito; }
            set { indirizzoFornito = value; }
        }

        public Comunicazioni ComunicazioniFornite
        {
            get { return comunicazioniFornite; }
            set { comunicazioniFornite = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Cellulare
        {
            get { return cellulare; }
            set { cellulare = value; }
        }

        public bool? ControlloIndirizzo
        {
            get { return controlloIndirizzo; }
            set { controlloIndirizzo = value; }
        }

        public bool? ControlloEmail
        {
            get { return controlloEmail; }
            set { controlloEmail = value; }
        }

        public bool? ControlloCellulare
        {
            get { return controlloCellulare; }
            set { controlloCellulare = value; }
        }

        public string IdTipoPagamento
        {
            get { return idTipoPagamento; }
            set { idTipoPagamento = value; }
        }
    }
}