using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    public class Documento
    {
        private string idArchidoc;

        public int? IdDocumento { get; set; }

        public TipoDocumento TipoDocumento { get; set; }

        public string IdArchidoc
        {
            get { return idArchidoc; }
            set { idArchidoc = value; }
        }

        public int? IdLavoratore { get; set; }

        public int? IdFamiliare { get; set; }

        public string LavoratoreCognome { get; set; }

        public string LavoratoreNome { get; set; }

        public string FamiliareCognome { get; set; }

        public string FamiliareNome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public DateTime? DataScansione { get; set; }

        public int? IdPrestazioniDomanda { get; set; }

        public string RiferitoA { get; set; }

        public bool PerPrestazione { get; set; }

        public bool? Originale { get; set; }

        public string IdTipoPrestazione { get; set; }

        public int? NumeroProtocolloPrestazione { get; set; }

        public int? ProtocolloPrestazione { get; set; }

        /// <summary>
        /// Indica se il documento � in archidoc
        /// </summary>
        public bool PresenteInArchidoc
        {
            get { return !string.IsNullOrEmpty(idArchidoc); }
        }

        #region Propriet� per la memorizzazione dei documenti consegnati (FAST)

        public String BeneficiarioPrestazione { get; set; }

        public Boolean? Consegnato { get; set; }

        #endregion
    }
}