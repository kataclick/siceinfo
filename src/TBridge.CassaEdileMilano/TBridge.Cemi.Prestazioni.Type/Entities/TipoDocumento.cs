using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoDocumento
    {
        private string descrizione;
        private short idTipoDocumento;

        public short IdTipoDocumento
        {
            get { return idTipoDocumento; }
            set { idTipoDocumento = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        public override string ToString()
        {
            return descrizione;
        }
    }
}