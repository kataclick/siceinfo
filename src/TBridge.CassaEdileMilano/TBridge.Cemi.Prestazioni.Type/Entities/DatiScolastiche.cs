using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class DatiScolastiche
    {
        private int? annoDaErogare;
        private Int16? annoFrequenza;
        private Int16? annoImmatricolazione;
        private string applicaDeduzione;
        private Int16? cfuConseguiti;
        private Int16? cfuPrevisti;
        private Int16? classeConclusa;
        private DateTime? dataDecesso;
        private Int32? idDatiScolastiche;
        private int? idFamiliareErede;
        private string idTipoPrestazione;
        private string idTipoPrestazioneDecesso;
        private Decimal? mediaVoti;
        private Int16? numeroAnniFrequentati;
        private Int16? numeroAnniFuoriCorso;
        private Int16? numeroEsamiSostenuti;
        private Int16? numeroMesiFrequentati;
        private int? tipoDaErogare;
        private TipoPromozione tipoPromozione;
        private TipoScuola tipoScuola;

        public Int32? IdDatiScolastiche
        {
            get
            {
                return idDatiScolastiche;
            }
            set
            {
                idDatiScolastiche = value;
            }
        }

        public Int16? AnnoFrequenza
        {
            get
            {
                return annoFrequenza;
            }
            set
            {
                annoFrequenza = value;
            }
        }

        public TipoScuola TipoScuola
        {
            get
            {
                return tipoScuola;
            }
            set
            {
                tipoScuola = value;
            }
        }

        public TipoPromozione TipoPromozione
        {
            get
            {
                return tipoPromozione;
            }
            set
            {
                tipoPromozione = value;
            }
        }

        public Int16? AnnoImmatricolazione
        {
            get
            {
                return annoImmatricolazione;
            }
            set
            {
                annoImmatricolazione = value;
            }
        }

        public Int16? NumeroAnniFrequentati
        {
            get
            {
                return numeroAnniFrequentati;
            }
            set
            {
                numeroAnniFrequentati = value;
            }
        }

        public Int16? NumeroAnniFuoriCorso
        {
            get
            {
                return numeroAnniFuoriCorso;
            }
            set
            {
                numeroAnniFuoriCorso = value;
            }
        }

        public Decimal? MediaVoti
        {
            get
            {
                return mediaVoti;
            }
            set
            {
                mediaVoti = value;
            }
        }

        public Int16? NumeroEsamiSostenuti
        {
            get
            {
                return numeroEsamiSostenuti;
            }
            set
            {
                numeroEsamiSostenuti = value;
            }
        }

        public Int16? CfuConseguiti
        {
            get
            {
                return cfuConseguiti;
            }
            set
            {
                cfuConseguiti = value;
            }
        }

        public Int16? CfuPrevisti
        {
            get
            {
                return cfuPrevisti;
            }
            set
            {
                cfuPrevisti = value;
            }
        }

        public Int16? NumeroMesiFrequentati
        {
            get
            {
                return numeroMesiFrequentati;
            }
            set
            {
                numeroMesiFrequentati = value;
            }
        }

        public Int16? ClasseConclusa
        {
            get
            {
                return classeConclusa;
            }
            set
            {
                classeConclusa = value;
            }
        }

        public string IdTipoPrestazione
        {
            get
            {
                return idTipoPrestazione;
            }
            set
            {
                idTipoPrestazione = value;
            }
        }

        public int? AnnoDaErogare
        {
            get
            {
                return annoDaErogare;
            }
            set
            {
                annoDaErogare = value;
            }
        }

        public int? TipoDaErogare
        {
            get
            {
                return tipoDaErogare;
            }
            set
            {
                tipoDaErogare = value;
            }
        }

        public DateTime? DataDecesso
        {
            get
            {
                return dataDecesso;
            }
            set
            {
                dataDecesso = value;
            }
        }

        //Non deve essere pi� usato
        //private int? numeroFigliCarico;
        //public int? NumeroFigliCarico
        //{
        //    get { return numeroFigliCarico; }
        //    set { numeroFigliCarico = value; }
        //}

        public string IdTipoPrestazioneDecesso
        {
            get
            {
                return idTipoPrestazioneDecesso;
            }
            set
            {
                idTipoPrestazioneDecesso = value;
            }
        }

        public string ApplicaDeduzione
        {
            get
            {
                return applicaDeduzione;
            }
            set
            {
                applicaDeduzione = value;
            }
        }

        public int? IdFamiliareErede
        {
            get
            {
                return idFamiliareErede;
            }
            set
            {
                idFamiliareErede = value;
            }
        }

        public DateTime? PeriodoDal
        {
            get;
            set;
        }

        public DateTime? PeriodoAl
        {
            get;
            set;
        }

        public Boolean ForzaturaPeriodoFrequenza180Giorni
        {
            get;
            set;
        }

        public Boolean ForzaturaPeriodoFrequenza3Anni
        {
            get;
            set;
        }

        public Boolean ForzaturaAnnoFrequenza { get; set; }

        public Boolean? RedditoSi { get; set; }

        public String AnnoScolastico { get; set; }

        public String IstitutoDenominazione { get; set; }

        public String IstitutoIndirizzo { get; set; }

        public String IstitutoProvincia { get; set; }

        public String IstitutoLocalita { get; set; }
    }
}