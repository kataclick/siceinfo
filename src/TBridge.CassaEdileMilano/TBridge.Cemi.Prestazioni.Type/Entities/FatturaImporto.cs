using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class FatturaImporto
    {
        private string _descrizione = string.Empty;
        private string _descrizioneTipoPrestazione = string.Empty;
        private int _idPrestazioneImportoFattura;
        private int _idPrestazioniFattura;
        private string _idTipoPrestazione = string.Empty;
        private int _idTipoPrestazioneImportoFattura;
        private decimal _valore;

        public int IdPrestazioneImportoFattura
        {
            get { return _idPrestazioneImportoFattura; }
            set { _idPrestazioneImportoFattura = value; }
        }

        public int IdTipoPrestazioneImportoFattura
        {
            get { return _idTipoPrestazioneImportoFattura; }
            set { _idTipoPrestazioneImportoFattura = value; }
        }

        public int IdPrestazioniFattura
        {
            get { return _idPrestazioniFattura; }
            set { _idPrestazioniFattura = value; }
        }

        public string IdTipoPrestazione
        {
            get { return _idTipoPrestazione; }
            set { _idTipoPrestazione = value; }
        }

        public string Descrizione
        {
            get { return _descrizione; }
            set { _descrizione = value; }
        }

        public string DescrizioneTipoPrestazione
        {
            get { return _descrizioneTipoPrestazione; }
            set { _descrizioneTipoPrestazione = value; }
        }

        public decimal Valore
        {
            get { return _valore; }
            set { _valore = value; }
        }
    }
}