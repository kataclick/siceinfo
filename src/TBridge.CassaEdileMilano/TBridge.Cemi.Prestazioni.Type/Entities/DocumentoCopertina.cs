﻿using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    public class DocumentoCopertina
    {
        public Int32 IdDomanda { get; set; }

        public Int16? IdTipoDocumento { get; set; }

        public Int32 IdTipoDocumentoArchidoc { get; set; }

        public String DescrizioneDocumento { get; set; }

        public String DescrizioneBeneficiario { get; set; }

        public Int32? IdFatturaDichiarata { get; set; }

        public Boolean Consegnato { get; set; }

        public String GradoParentela { get; set; }

        public String GradoParentelaDocumento { get; set; }

        public DateTime? FatturaData { get; set; }

        public String FatturaNumero { get; set; }
    }
}
