using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Familiare
    {
        private string codiceFiscale;
        private string cognome;
        private bool? controlloCodiceFiscale;
        private bool? controlloDataDecesso;
        private bool? controlloDataNascita;
        private bool? controlloFamiliareSelezionato;
        private bool? controlloGradoParentela;
        private bool? controlloACarico;
        private DateTime? dataDecesso;
        private DateTime? dataNascita;
        private string gradoParentela;
        private int? idFamiliare;
        private int idLavoratore;
        private string nome;
        private char sesso = 'M';
        private String aCarico;

        public int IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public int? IdFamiliare
        {
            get { return idFamiliare; }
            set { idFamiliare = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string NomeCompleto
        {
            get { return String.Format("{0} {1}", cognome, nome); }
        }

        public DateTime? DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }

        public DateTime? DataDecesso
        {
            get { return dataDecesso; }
            set { dataDecesso = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public string GradoParentela
        {
            get { return gradoParentela; }
            set { gradoParentela = value; }
        }

        public bool? ControlloFamiliareSelezionato
        {
            get { return controlloFamiliareSelezionato; }
            set { controlloFamiliareSelezionato = value; }
        }

        public bool? ControlloDataDecesso
        {
            get { return controlloDataDecesso; }
            set { controlloDataDecesso = value; }
        }

        public bool? ControlloGradoParentela
        {
            get { return controlloGradoParentela; }
            set { controlloGradoParentela = value; }
        }

        public bool? ControlloDataNascita
        {
            get { return controlloDataNascita; }
            set { controlloDataNascita = value; }
        }

        public bool? ControlloCodiceFiscale
        {
            get { return controlloCodiceFiscale; }
            set { controlloCodiceFiscale = value; }
        }

        public bool? ControlloACarico
        {
            get { return controlloACarico; }
            set { controlloACarico = value; }
        }

        public char Sesso
        {
            get { return sesso; }
            set { sesso = value; }
        }

        public String ACarico
        {
            get { return aCarico; }
            set { aCarico = value; }
        }
    }
}