﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    public class DocumentoStampaCopertina
    {
        public String Id { set; get; }
        public Boolean? Originale { set; get; }

        public DocumentoStampaCopertina(String id, Boolean? originale)
        {
            Id = id;
            Originale = originale;
        }
    }
}
