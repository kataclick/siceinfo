using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoCausale
    {
        /// <summary>
        /// Costruttore
        /// </summary>
        public TipoCausale()
        {
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="idTipoCausale">idTipoCausale</param>
        public TipoCausale(string idTipoCausale)
        {
            IdTipoCausale = idTipoCausale;
        }

        public string IdTipoCausale { get; set; }
        public string Descrizione { get; set; }
        public string DescrizioneEstesa { get; set; }
    }
}