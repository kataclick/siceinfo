using System;
using TBridge.Cemi.Prestazioni.Type.Collections;
using TBridge.Cemi.Prestazioni.Type.Enums;
using TBridge.Cemi.Type.Collections;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Domanda
    {
        private string beneficiario;
        private CassaEdileCollection casseEdili;
        private Boolean certificatoFamiglia;
        private bool confermata;
        private bool? controlloFamiliare;
        private bool? controlloFatture;
        private bool? controlloLavoratore;
        private bool? controlloOreCnce;
        private bool? controlloPresenzaDocumenti;
        private bool? controlloScolastiche;
        private bool? controlloUnivocitaPrestazione;

        //private DateTime? dataConferma;

        private DateTime dataRiferimento;
        private DatiScolastiche datiAggiuntiviScolastiche;
        private Familiare familiare;
        private Familiare familiareFornito;
        private FatturaDichiarataCollection fattureDichiarate;
        private FatturaCollection fattureRicevute;
        private Guid guid;
        private int? idDomanda;
        private int? idUtenteInCarico;
        private PrestazioneImportoCollection importi;
        private Lavoratore lavoratore;
        private string loginInCarico;
        private Boolean moduloDentista;

        /// <summary>
        /// Rappresenta il numero di fatture dichiarate all'atto di compilazione della domanda; 
        /// per il pregresso viene usato il count delle fatture dichiarate
        /// </summary>
        private Int32? numeroFatture;

        private Int32 numeroProtocolloPrestazione;
        private Int16 protocolloPrestazione;
        private StatoDomanda stato;
        private TipoPrestazione tipoPrestazione;

        public int? IdDomanda
        {
            get { return idDomanda; }
            set { idDomanda = value; }
        }

        /// <summary>
        /// [DEPRECATO] utilizzare data domanda. La propriet� usa comunque dataDomanda
        /// </summary>
        public DateTime? DataConferma
        {
            get { return DataDomanda; }
            set { DataDomanda = value; }
        }

        public Lavoratore Lavoratore
        {
            get { return lavoratore; }
            set { lavoratore = value; }
        }

        public Familiare Familiare
        {
            get { return familiare; }
            set { familiare = value; }
        }

        public Familiare FamiliareFornito
        {
            get { return familiareFornito; }
            set { familiareFornito = value; }
        }

        public string Beneficiario
        {
            get { return beneficiario; }
            set { beneficiario = value; }
        }

        public string IdTipoPrestazione
        {
            get
            {
                if (tipoPrestazione != null)
                    return tipoPrestazione.IdTipoPrestazione;
                else
                    return string.Empty;
            }
        }

        public string DescrizioneTipoPrestazione
        {
            get
            {
                if (tipoPrestazione != null)
                    return tipoPrestazione.Descrizione;
                else
                    return string.Empty;
            }
        }

        public TipoPrestazione TipoPrestazione
        {
            get { return tipoPrestazione; }
            set { tipoPrestazione = value; }
        }

        public FatturaDichiarataCollection FattureDichiarate
        {
            get { return fattureDichiarate; }
            set { fattureDichiarate = value; }
        }

        public FatturaCollection FattureRicevute
        {
            get { return fattureRicevute; }
            set { fattureRicevute = value; }
        }

        public StatoDomanda Stato
        {
            get { return stato; }
            set { stato = value; }
        }

        public bool? ControlloFamiliare
        {
            get { return controlloFamiliare; }
            set { controlloFamiliare = value; }
        }

        public bool? ControlloPresenzaDocumenti
        {
            get { return controlloPresenzaDocumenti; }
            set { controlloPresenzaDocumenti = value; }
        }

        public bool? ControlloFatture
        {
            get { return controlloFatture; }
            set { controlloFatture = value; }
        }

        public bool? ControlloUnivocitaPrestazione
        {
            get { return controlloUnivocitaPrestazione; }
            set { controlloUnivocitaPrestazione = value; }
        }

        public bool? ControlloLavoratore
        {
            get { return controlloLavoratore; }
            set { controlloLavoratore = value; }
        }

        public bool? ControlloOreCnce
        {
            get { return controlloOreCnce; }
            set { controlloOreCnce = value; }
        }

        public int? IdUtenteInCarico
        {
            get { return idUtenteInCarico; }
            set { idUtenteInCarico = value; }
        }

        public string LoginInCarico
        {
            get { return loginInCarico; }
            set { loginInCarico = value; }
        }

        public CassaEdileCollection CasseEdili
        {
            get { return casseEdili; }
            set { casseEdili = value; }
        }

        public DateTime DataRiferimento
        {
            get { return dataRiferimento; }
            set { dataRiferimento = value; }
        }

        public Guid Guid
        {
            get { return guid; }
            set { guid = value; }
        }

        public Int16 ProtocolloPrestazione
        {
            get { return protocolloPrestazione; }
            set { protocolloPrestazione = value; }
        }

        public Int32 NumeroProtocolloPrestazione
        {
            get { return numeroProtocolloPrestazione; }
            set { numeroProtocolloPrestazione = value; }
        }

        public bool Confermata
        {
            get { return confermata; }
            set { confermata = value; }
        }

        public string ProtocolloPrestazioneCompleto
        {
            get { return String.Format("{0}/{1}", ProtocolloPrestazione, NumeroProtocolloPrestazione); }
        }

        public bool? ControlloScolastiche
        {
            get { return controlloScolastiche; }
            set { controlloScolastiche = value; }
        }

        public DatiScolastiche DatiAggiuntiviScolastiche
        {
            get { return datiAggiuntiviScolastiche; }
            set { datiAggiuntiviScolastiche = value; }
        }

        public PrestazioneImportoCollection Importi
        {
            get { return importi; }
            set { importi = value; }
        }

        public Boolean CertificatoFamiglia
        {
            get { return certificatoFamiglia; }
            set { certificatoFamiglia = value; }
        }

        public Boolean ModuloDentista
        {
            get { return moduloDentista; }
            set { moduloDentista = value; }
        }

        /// <summary>
        /// Data della domanda, se nulla viene usata quella di inserimento    
        /// </summary>
        public DateTime? DataDomanda { get; set; }

        public DocumentoCollection Documenti { get; set; }

        /// <summary>
        /// Tipo di insermento, tipicamente fast o normale
        /// </summary>
        public TipoInserimento TipoInserimento { get; set; }

        public Int32? NumeroFatture
        {
            get
            {
                if (numeroFatture.HasValue)
                    return numeroFatture;
                else if (FattureDichiarate != null)
                {
                    return FattureDichiarate.Count;
                }
                else
                    return -1;
            }
            set { numeroFatture = value; }
        }

        /// <summary>
        /// Causale di respinta
        /// </summary>
        public TipoCausale TipoCausale { get; set; }

        /// <summary>
        /// indica il tipo modulo della prestazione
        /// </summary>
        public TipoModulo TipoModulo { get; set; }

        /// <summary>
        /// Data di gestione della domanda
        /// </summary>
        public DateTime? DataGestione { get; set; }

        /// <summary>
        /// Serve per differenziare la domanda nel funerario, � solo di facciata
        /// </summary>
        public Boolean GenitoriConviventi { get; set; }

        public String Nota { get; set; }

        public Domanda CopiaBase()
        {
            Domanda domanda = new Domanda();

            domanda.TipoPrestazione = this.TipoPrestazione;
            domanda.NumeroProtocolloPrestazione = this.NumeroProtocolloPrestazione;
            domanda.ProtocolloPrestazione = this.ProtocolloPrestazione;
            domanda.beneficiario = this.Beneficiario;
            domanda.DataDomanda = this.DataDomanda;
            domanda.DataRiferimento = this.DataRiferimento;
            domanda.Lavoratore = this.Lavoratore;
            domanda.Familiare = this.Familiare;
            domanda.Stato = this.Stato;

            domanda.FattureDichiarate = new FatturaDichiarataCollection();
            domanda.FattureRicevute = new FatturaCollection();
            domanda.CasseEdili = new CassaEdileCollection();

            domanda.Guid = Guid.NewGuid();

            return domanda;
        }
    }
}