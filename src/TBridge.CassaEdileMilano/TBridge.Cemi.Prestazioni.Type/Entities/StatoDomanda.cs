using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class StatoDomanda
    {
        private string descrizione;
        private string idStato;

        public StatoDomanda()
        {
        }

        public StatoDomanda(string idStato)
        {
            this.idStato = idStato;
        }

        public string IdStato
        {
            get { return idStato; }
            set { idStato = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        #region Metodi statici

        /// <summary>
        /// Ritorna true se la domanda � in uno stato tale per cui pu� essere gestita dall'utente
        /// </summary>
        /// <param name="idStato">Stato da controllare</param>
        /// <returns>True, false</returns>
        public static bool StatoDomandaModificabile(string idStato)
        {
            if (idStato == "I" || idStato == "T" || idStato == "O" || idStato == "N" || idStato == "E")
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Ritorna true se la domanda � in uno stato definitivo e quindi la domanda non � pi� modificabile dall'utente
        /// </summary>
        /// <param name="idStato">Stato da controllare</param>
        /// <returns>True, false</returns>
        public static bool StatoDomandaDefinitivo(string idStato)
        {
            return !StatoDomandaModificabile(idStato);
        }

        #endregion
    }
}