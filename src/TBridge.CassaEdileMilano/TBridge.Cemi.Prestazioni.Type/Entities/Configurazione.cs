using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Configurazione
    {
        private int differenzaMesiFatturaDomanda;
        private string gradoParentela;
        private string idTipoPrestazione;
        private DateTime periodoRiferimentoFine;
        private DateTime periodoRiferimentoInizio;
        private bool reciprocita123;
        private bool reciprocita45;
        private bool richiestaFattura;
        private bool richiestaTipoScuola;
        private TipoMacroPrestazione tipoMacroPrestazione;
        private TipoModulo tipoModulo;
        private bool univocitaAnnuale;
        private bool univocitaFamiliare;

        public string IdTipoPrestazione
        {
            get { return idTipoPrestazione; }
            set { idTipoPrestazione = value; }
        }

        public string GradoParentela
        {
            get { return gradoParentela; }
            set { gradoParentela = value; }
        }

        public bool Reciprocita123
        {
            get { return reciprocita123; }
            set { reciprocita123 = value; }
        }

        public bool Reciprocita45
        {
            get { return reciprocita45; }
            set { reciprocita45 = value; }
        }

        public bool UnivocitaAnnuale
        {
            get { return univocitaAnnuale; }
            set { univocitaAnnuale = value; }
        }

        public bool UnivocitaFamiliare
        {
            get { return univocitaFamiliare; }
            set { univocitaFamiliare = value; }
        }

        public DateTime PeriodoRiferimentoInizio
        {
            get { return periodoRiferimentoInizio; }
            set { periodoRiferimentoInizio = value; }
        }

        public DateTime PeriodoRiferimentoFine
        {
            get { return periodoRiferimentoFine; }
            set { periodoRiferimentoFine = value; }
        }

        public int DifferenzaMesiFatturaDomanda
        {
            get { return differenzaMesiFatturaDomanda; }
            set { differenzaMesiFatturaDomanda = value; }
        }

        public bool RichiestaFattura
        {
            get { return richiestaFattura; }
            set { richiestaFattura = value; }
        }

        public bool RichiestaTipoScuola
        {
            get { return richiestaTipoScuola; }
            set { richiestaTipoScuola = value; }
        }

        public TipoMacroPrestazione TipoMacroPrestazione
        {
            get { return tipoMacroPrestazione; }
            set { tipoMacroPrestazione = value; }
        }

        public TipoModulo TipoModulo
        {
            get { return tipoModulo; }
            set { tipoModulo = value; }
        }
    }
}