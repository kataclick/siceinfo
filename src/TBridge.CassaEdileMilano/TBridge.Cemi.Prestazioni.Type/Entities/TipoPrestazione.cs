using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoPrestazione
    {
        private string descrizione;
        private string gradoParentela;
        private string idTipoPrestazione;

        public string IdTipoPrestazione
        {
            get { return idTipoPrestazione; }
            set { idTipoPrestazione = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        public string GradoParentela
        {
            get { return gradoParentela; }
            set { gradoParentela = value; }
        }

        public DateTime? ValidaDa { get; set; }
        public DateTime? ValidaA { get; set; }

        public override string ToString()
        {
            return descrizione;
        }

        public Gruppo Gruppo { get; set; }
    }
}