using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoDecesso
    {
        private string descrizione;
        private string idTipoDecesso;

        public string IdTipoDecesso
        {
            get { return idTipoDecesso; }
            set { idTipoDecesso = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        #region Costruttori

        public TipoDecesso(string idTipoDecesso)
        {
            this.idTipoDecesso = idTipoDecesso;
        }

        public TipoDecesso()
        {
        }

        #endregion

        public override string ToString()
        {
            return Descrizione;
        }
    }
}