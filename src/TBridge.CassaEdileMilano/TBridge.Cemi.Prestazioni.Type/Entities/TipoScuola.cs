using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoScuola
    {
        private string descrizione;
        private int idTipoScuola;

        public int IdTipoScuola
        {
            get { return idTipoScuola; }
            set { idTipoScuola = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        #region Costruttori

        public TipoScuola(int idTipoScuola)
        {
            this.idTipoScuola = idTipoScuola;
        }

        public TipoScuola()
        {
        }

        #endregion

        public override string ToString()
        {
            return Descrizione;
        }
    }
}