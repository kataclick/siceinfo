using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoModulo
    {
        private string descrizione;
        private Int16 idTipoModulo;
        private string modulo;

        public Int16 IdTipoModulo
        {
            get { return idTipoModulo; }
            set { idTipoModulo = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        public string Modulo
        {
            get { return modulo; }
            set { modulo = value; }
        }
    }
}