﻿using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    public class StatiCureProtesi
    {
        public String StatoCL { get; set; }

        public String StatoPL { get; set; }
    }
}
