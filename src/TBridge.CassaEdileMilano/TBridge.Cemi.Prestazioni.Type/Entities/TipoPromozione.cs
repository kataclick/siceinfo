using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoPromozione
    {
        private String descrizione;
        private Int32 idTipoPromozione;

        public Int32 IdTipoPromozione
        {
            get { return idTipoPromozione; }
            set { idTipoPromozione = value; }
        }

        public String Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        #region Costruttori

        public TipoPromozione(Int32 idTipoPromozione)
        {
            this.idTipoPromozione = idTipoPromozione;
        }

        public TipoPromozione()
        {
        }

        #endregion

        public override string ToString()
        {
            return Descrizione;
        }
    }
}