using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class FatturaDichiarata
    {
        private bool aggiuntaOperatore;
        private DateTime data;
        private DateTime? dataAnnullamento;
        private DateTime dataInserimentoRecord;
        private string idFatturaArchidoc;
        private int? idPrestazioniDomanda;
        private int? idPrestazioniFattura;
        private int? idPrestazioniFatturaDichiarata;
        private decimal importoTotale;
        private string numero;
        private bool saldo;

        public int? IdPrestazioniFatturaDichiarata
        {
            get { return idPrestazioniFatturaDichiarata; }
            set { idPrestazioniFatturaDichiarata = value; }
        }

        public int? IdPrestazioniDomanda
        {
            get { return idPrestazioniDomanda; }
            set { idPrestazioniDomanda = value; }
        }

        public int? IdPrestazioniFattura
        {
            get { return idPrestazioniFattura; }
            set { idPrestazioniFattura = value; }
        }

        public string IdFatturaArchidoc
        {
            get { return idFatturaArchidoc; }
            set { idFatturaArchidoc = value; }
        }

        public DateTime Data
        {
            get { return data; }
            set { data = value; }
        }

        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public decimal ImportoTotale
        {
            get { return importoTotale; }
            set { importoTotale = value; }
        }

        public bool Saldo
        {
            get { return saldo; }
            set { saldo = value; }
        }

        public DateTime? DataAnnullamento
        {
            get { return dataAnnullamento; }
            set { dataAnnullamento = value; }
        }

        public DateTime DataInserimentoRecord
        {
            get { return dataInserimentoRecord; }
            set { dataInserimentoRecord = value; }
        }

        //Indica se la fattura dichiarata � stata aggiunta dall'operatore successivamente all'inserimento della domanda da parte del lavoratore
        public bool AggiuntaOperatore
        {
            get { return aggiuntaOperatore; }
            set { aggiuntaOperatore = value; }
        }

        public Int32? IdUtenteInserimento { get; set; }

        public String UtenteInserimento { get; set; }
    }
}