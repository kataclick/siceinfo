using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class TipoMacroPrestazione
    {
        private string descrizione;
        private Int16 idTipoMacroPrestazione;

        public Int16 IdTipoMacroPrestazione
        {
            get { return idTipoMacroPrestazione; }
            set { idTipoMacroPrestazione = value; }
        }

        public string Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }
    }
}