using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Comunicazioni
    {
        private DateTime? appuntamento;
        private string cellulare;

        private string email;
        private string iban;

        private string idTipoPagamento;

        public string Cellulare
        {
            get { return cellulare; }
            set { cellulare = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string IdTipoPagamento
        {
            get { return idTipoPagamento; }
            set { idTipoPagamento = value; }
        }

        public string Iban
        {
            get { return iban; }
            set { iban = value; }
        }

        public DateTime? Appuntamento
        {
            get { return appuntamento; }
            set { appuntamento = value; }
        }
    }
}