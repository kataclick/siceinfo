﻿using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Gruppo
    {
        public Int32 Codice { get; set; }

        public String Descrizione { get; set; }

        public override String ToString()
        {
            return this.Descrizione;
        }
    }
}
