using System;
using TBridge.Cemi.Prestazioni.Type.Collections;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class Fattura
    {
        private DateTime? data;
        private DateTime? dataAssociazione;
        private DateTime? dataInserimentoRecord;
        private DateTime? dataValidazione;
        private string idArchidoc;
        private int? idLavoratore;
        private int? idPrestazioniFattura;
        private FatturaImportoCollection importi;
        private decimal importoTotale;
        private string numero;
        private bool originale;
        private bool? valida;

        public int? IdPrestazioniFattura
        {
            get { return idPrestazioniFattura; }
            set { idPrestazioniFattura = value; }
        }

        public int? IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public string IdArchidoc
        {
            get { return idArchidoc; }
            set { idArchidoc = value; }
        }

        public DateTime? Data
        {
            get { return data; }
            set { data = value; }
        }


        public DateTime? DataValidazione
        {
            get { return dataValidazione; }
            set { dataValidazione = value; }
        }

        public DateTime? DataAssociazione
        {
            get { return dataAssociazione; }
            set { dataAssociazione = value; }
        }

        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public decimal ImportoTotale
        {
            get { return importoTotale; }
            set { importoTotale = value; }
        }

        public bool? Valida
        {
            get { return valida; }
            set { valida = value; }
        }

        public bool Originale
        {
            get { return originale; }
            set { originale = value; }
        }

        public DateTime? DataInserimentoRecord
        {
            get { return dataInserimentoRecord; }
            set { dataInserimentoRecord = value; }
        }

        public FatturaImportoCollection Importi
        {
            get { return importi; }
            set { importi = value; }
        }

        public DateTime? DataScansione { get; set; }
    }
}