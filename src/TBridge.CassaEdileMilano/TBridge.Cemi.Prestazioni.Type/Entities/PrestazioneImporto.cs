using System;

namespace TBridge.Cemi.Prestazioni.Type.Entities
{
    [Serializable]
    public class PrestazioneImporto
    {
        private String descrizione;
        private Int32 idDomanda;

        private Int32? idImporto;

        private Int32 idTipoImporto;

        private String idTipoPrestazione;
        private Decimal valore;

        public Int32 IdDomanda
        {
            get { return idDomanda; }
            set { idDomanda = value; }
        }

        public Int32? IdImporto
        {
            get { return idImporto; }
            set { idImporto = value; }
        }

        public Int32 IdTipoImporto
        {
            get { return idTipoImporto; }
            set { idTipoImporto = value; }
        }

        public String IdTipoPrestazione
        {
            get { return idTipoPrestazione; }
            set { idTipoPrestazione = value; }
        }

        public String Descrizione
        {
            get { return descrizione; }
            set { descrizione = value; }
        }

        public Decimal Valore
        {
            get { return valore; }
            set { valore = value; }
        }
    }
}