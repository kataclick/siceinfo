using System;
using TBridge.Cemi.Prestazioni.Type.Enums;

namespace TBridge.Cemi.Prestazioni.Type.Filters
{
    public class DomandaFilter
    {
        public DateTime? DataNascitaLavoratore { get; set; }

        public String NomeLavoratore { get; set; }

        public String CognomeLavoratore { get; set; }

        public int? IdLavoratore { get; set; }

        public char? Stato { get; set; }

        public string IdTipoPrestazione { get; set; }

        public int? Anno { get; set; }

        public int? IdUtente { get; set; }

        public int? Protocollo { get; set; }

        public TipoInserimento? TipologiaInserimento { get; set; }

        public DateTime? DataDomandaDal { get; set; }

        public DateTime? DataDomandaAl { get; set; }

        public Int32? IdGruppo { get; set; }
    }
}