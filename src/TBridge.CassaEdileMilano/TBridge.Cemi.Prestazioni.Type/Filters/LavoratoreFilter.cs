using System;

namespace TBridge.Cemi.Prestazioni.Type.Filters
{
    [Serializable]
    public class LavoratoreFilter
    {
        public Int32? IdLavoratore { get; set; }

        public String Cognome { get; set; }

        public String Nome { get; set; }

        public DateTime? DataNascita { get; set; }
    }
}