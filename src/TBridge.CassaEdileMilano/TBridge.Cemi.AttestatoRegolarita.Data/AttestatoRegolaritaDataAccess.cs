#region

using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.AttestatoRegolarita.Type.Collections;
using TBridge.Cemi.AttestatoRegolarita.Type.Entities;
using TBridge.Cemi.AttestatoRegolarita.Type.Enums;
using TBridge.Cemi.AttestatoRegolarita.Type.Exceptions;
using TBridge.Cemi.AttestatoRegolarita.Type.Filters;

#endregion

namespace TBridge.Cemi.AttestatoRegolarita.Data
{
    public class AttestatoRegolaritaDataAccess
    {
        private Database databaseCemi;

        public AttestatoRegolaritaDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        /// <summary>
        /// Restituisce la lista delle imprese presenti nel database
        /// </summary>
        /// <returns></returns>
        public ImpresaCollection GetImprese(int? idImpresaParam,
                                            string ragioneSocialeParam, string comuneParam, string indirizzoParam,
                                            string ivaFiscale, string stringExpression, string sortDirection)
        {
            ImpresaCollection listaImprese = new ImpresaCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaImpreseSelect"))
            {
                if (idImpresaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresaParam.Value);
                if (!string.IsNullOrEmpty(ragioneSocialeParam))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragioneSocialeParam);
                if (!string.IsNullOrEmpty(comuneParam))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comuneParam);
                if (!string.IsNullOrEmpty(indirizzoParam))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoParam);
                if (!string.IsNullOrEmpty(ivaFiscale))
                    DatabaseCemi.AddInParameter(comando, "@ivaFiscale", DbType.String, ivaFiscale);

                DataSet dsImprese = databaseCemi.ExecuteDataSet(comando);

                if ((dsImprese != null) && (dsImprese.Tables.Count == 1))
                {
                    if (stringExpression != null && sortDirection != null)
                    {
                        dsImprese.Tables[0].DefaultView.Sort = stringExpression + " " + sortDirection;
                    }

                    for (int i = 0; i < dsImprese.Tables[0].DefaultView.Count; i++)
                    {
                        int idImpresa;
                        string ragioneSociale;
                        string provincia = null;
                        string comune = null;
                        string cap = null;
                        string indirizzo = null;
                        string partitaIva = null;
                        string codiceFiscale = null;
                        TipologiaImpresa tipoImpresa;
                        string idCassaEdileProvenienza = null;


                        idImpresa = (int) dsImprese.Tables[0].DefaultView[i]["idImpresa"];
                        ragioneSociale = (string) dsImprese.Tables[0].DefaultView[i]["ragioneSociale"];

                        if (dsImprese.Tables[0].DefaultView[i]["indirizzo"] != DBNull.Value)
                            indirizzo = (string) dsImprese.Tables[0].DefaultView[i]["indirizzo"];
                        if (dsImprese.Tables[0].DefaultView[i]["comune"] != DBNull.Value)
                            comune = (string) dsImprese.Tables[0].DefaultView[i]["comune"];
                        if (dsImprese.Tables[0].DefaultView[i]["provincia"] != DBNull.Value)
                            provincia = (string) dsImprese.Tables[0].DefaultView[i]["provincia"];
                        if (dsImprese.Tables[0].DefaultView[i]["cap"] != DBNull.Value)
                            cap = (string) dsImprese.Tables[0].DefaultView[i]["cap"];
                        if (dsImprese.Tables[0].DefaultView[i]["partitaIva"] != DBNull.Value)
                            partitaIva = (string) dsImprese.Tables[0].DefaultView[i]["partitaIva"];
                        if (dsImprese.Tables[0].DefaultView[i]["codiceFiscale"] != DBNull.Value)
                            codiceFiscale = (string) dsImprese.Tables[0].DefaultView[i]["codiceFiscale"];
                        if (dsImprese.Tables[0].DefaultView[i]["idCassaEdileProvenienza"] != DBNull.Value)
                            idCassaEdileProvenienza =
                                (string) dsImprese.Tables[0].DefaultView[i]["idCassaEdileProvenienza"];

                        //Valla: vedere cosa mettere come tipo impresa
                        Impresa impresa = new Impresa(idImpresa, ragioneSociale, indirizzo, provincia, comune, cap,
                                                      partitaIva, codiceFiscale, TipologiaImpresa.SiceNew, null,
                                                      idCassaEdileProvenienza);
                        listaImprese.Add(impresa);
                    }
                }
            }

            return listaImprese;
        }

        ///// <summary>
        ///// Seleziona un lavoratore in base ai parametri, inserito in una DataTable
        ///// </summary>
        ///// <param name="idImpresa"></param>
        ///// <param name="idLavoratore"></param>
        ///// <returns></returns>
        //public DataTable RicercaLavoratori(int idImpresa, int idLavoratore)
        //{
        //    DataTable dtRes;
        //    // Seleziono la stored procedure
        //    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaLavoratoriSelect");
        //    // Imposto i parametri
        //    databaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
        //    databaseCemi.AddInParameter(comando, "@IdLavoratore", DbType.Int32, idLavoratore);
        //    // Ricavo il risultato da un dataset
        //    dtRes = databaseCemi.ExecuteDataSet(comando).Tables[0]; // Se non c'� � giusto che esploda
        //    return dtRes;
        //} // RicercaLavoratori

        ///// <summary>
        ///// Seleziona uno o pi� lavoratori in base ai parametri, restituendo una DataTable
        ///// </summary>
        ///// <param name="idImpresa"></param>
        ///// <param name="cognome"></param>
        ///// <param name="nome"></param>
        ///// <returns></returns>
        //public DataTable RicercaLavoratori(int idImpresa, string cognome, string nome)
        //{
        //    // TODO: Vedere se si pu� fondere in qualche modo con quella sopra
        //    // Si pu� fare con i nullabletypes
        //    DataTable dtRes;
        //    // Seleziono la stored procedure
        //    DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaLavoratoriSelect");
        //    // Imposto i parametri
        //    databaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
        //    databaseCemi.AddInParameter(comando, "@Cognome", DbType.String, cognome);
        //    if (!string.IsNullOrEmpty(nome))
        //        databaseCemi.AddInParameter(comando, "@Nome", DbType.String, nome);
        //    // Ricavo il risultato da un dataset
        //    dtRes = databaseCemi.ExecuteDataSet(comando).Tables[0]; // Se non c'� � giusto che esploda
        //    return dtRes;
        //} //RicercaLavoratori


        public LavoratoreCollection GetLavoratoriOrdinatiReader( /*TipologiaLavoratore tipoLavoratore,*/
            int? idLavoratore, string cognome, string nome, DateTime? dataNascita, string codiceFiscale,
            DateTime? dataAssunzione, DateTime? dataCessazione, string sortExpression,
            string direct, int? idImpresa)
        {
            LavoratoreCollection listaLavoratori = new LavoratoreCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaLavoratoriSelect"))
            {
                databaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
                databaseCemi.AddInParameter(comando, "@Cognome", DbType.String, cognome);
                if (!string.IsNullOrEmpty(nome))
                    databaseCemi.AddInParameter(comando, "@Nome", DbType.String, nome);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    databaseCemi.AddInParameter(comando, "@CodiceFiscale", DbType.String, codiceFiscale);
                if (dataNascita.HasValue)
                    databaseCemi.AddInParameter(comando, "@DataNascita", DbType.DateTime, dataNascita);
                if (idLavoratore.HasValue)
                    databaseCemi.AddInParameter(comando, "@IdLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        int idLavoratoreLav = -1;
                        string cognomeLav = String.Empty;
                        string nomeLav = String.Empty;
                        DateTime? dataNascitaLav = null;
                        string codiceFiscaleLav = string.Empty;
                        DateTime? dataAssunzioneLav = null;
                        DateTime? dataCessazioneLav = null;
                        //TipologiaLavoratore tipoLav;

                        if (reader["idLavoratore"] != DBNull.Value)
                            idLavoratoreLav = (int) reader["idLavoratore"];
                        if (reader["cognome"] != DBNull.Value)
                            cognomeLav = (string) reader["cognome"];
                        if (reader["nome"] != DBNull.Value)
                            nomeLav = (string) reader["nome"];
                        if (reader["dataNascita"] != DBNull.Value)
                            dataNascitaLav = (DateTime) reader["dataNascita"];
                        if (reader["codiceFiscale"] != DBNull.Value)
                            codiceFiscaleLav = (string) reader["codiceFiscale"];
                        if (reader["dataAssunzione"] != DBNull.Value)
                            dataAssunzioneLav = (DateTime) reader["dataAssunzione"];
                        if (reader["dataCessazione"] != DBNull.Value)
                            dataCessazioneLav = (DateTime) reader["dataCessazione"];

                        listaLavoratori.Add(new Lavoratore(idLavoratoreLav, cognomeLav, nomeLav, dataNascitaLav,
                                                           codiceFiscaleLav, dataAssunzioneLav, dataCessazioneLav));
                    }
                }
            }

            return listaLavoratori;
        }

        public DomandaCollection GetDomandeByFilter(DomandaFilter filtro)
        {
            DomandaCollection domande = new DomandaCollection();

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandeSelectByFilter")
                )
            {
                if (!String.IsNullOrEmpty(filtro.Indirizzo))
                    databaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filtro.Indirizzo);
                if (!String.IsNullOrEmpty(filtro.Comune))
                    databaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.Comune);
                if (filtro.Mese.HasValue)
                    databaseCemi.AddInParameter(comando, "@mese", DbType.DateTime, filtro.Mese.Value);
                if (filtro.IdUtente.HasValue)
                    databaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, filtro.IdUtente.Value);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdDomanda = reader.GetOrdinal("idAttestatoRegolaritaDomanda");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceInfoAggiuntiva = reader.GetOrdinal("infoAggiuntiva");
                    Int32 indiceMese = reader.GetOrdinal("mese");
                    Int32 indiceImporto = reader.GetOrdinal("importo");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");
                    Int32 indicePercentualeManodopera = reader.GetOrdinal("percentualeManodopera");
                    Int32 indiceNumeroLavoratori = reader.GetOrdinal("numeroLavoratori");
                    Int32 indiceNumeroGiorni = reader.GetOrdinal("numeroGiorni");
                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizio");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFine");
                    Int32 indiceDIA = reader.GetOrdinal("numeroDIA");
                    Int32 indiceConcessione = reader.GetOrdinal("numeroConcessione");
                    Int32 indiceAutorizzazione = reader.GetOrdinal("numeroAutorizzazione");
                    Int32 indiceRichiestaVisitaIspettiva = reader.GetOrdinal("richiestaVisitaIspettiva");
                    Int32 indiceDataVisitaIspettiva = reader.GetOrdinal("dataVisitaIspettiva");
                    Int32 indiceOrarioVisitaIspettiva = reader.GetOrdinal("orarioVisitaIspettiva");
                    Int32 indiceContattoTelefonicoVisitaIspettiva =
                        reader.GetOrdinal("contattoTelefonicoVisitaIspettiva");
                    Int32 indiceMotivoVisitaIspettiva = reader.GetOrdinal("motivoVisitaIspettiva");
                    Int32 indiceDataControllo = reader.GetOrdinal("dataControllo");
                    Int32 indiceAttestatoRilasciabile = reader.GetOrdinal("controlloAttestatoRilasciabile");
                    Int32 indiceIdImpresaRichiedente = reader.GetOrdinal("impresaIdImpresa");
                    Int32 indiceRagioneSocialeImpresaRichiedente = reader.GetOrdinal("impresaRagioneSociale");
                    Int32 indiceIdCommittenteRichiedente = reader.GetOrdinal("committenteIdCommittente");
                    Int32 indiceRagioneSocialeCommittenteRichiedente = reader.GetOrdinal("committenteRagioneSociale");
                    Int32 indiceIdUtente = reader.GetOrdinal("idUtente");
                    Int32 indiceLoginUtente = reader.GetOrdinal("login");

                    while (reader.Read())
                    {
                        Domanda domanda = new Domanda();
                        domande.Add(domanda);

                        domanda.IdDomanda = reader.GetInt32(indiceIdDomanda);
                        domanda.Indirizzo = reader.GetString(indiceIndirizzo);
                        domanda.IdUtente = reader.GetInt32(indiceIdUtente);
                        domanda.LoginUtente = reader.GetString(indiceLoginUtente);

                        if (!reader.IsDBNull(indiceCivico))
                            domanda.Civico = reader.GetString(indiceCivico);
                        if (!reader.IsDBNull(indiceComune))
                            domanda.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            domanda.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            domanda.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceInfoAggiuntiva))
                            domanda.InfoAggiuntiva = reader.GetString(indiceInfoAggiuntiva);
                        if (!reader.IsDBNull(indiceMese))
                            domanda.Mese = reader.GetDateTime(indiceMese);
                        if (!reader.IsDBNull(indiceImporto))
                            domanda.Importo = reader.GetDecimal(indiceImporto);
                        if (!reader.IsDBNull(indiceDescrizione))
                            domanda.Descrizione = reader.GetString(indiceDescrizione);
                        if (!reader.IsDBNull(indicePercentualeManodopera))
                            domanda.PercentualeManodopera = reader.GetDecimal(indicePercentualeManodopera);
                        if (!reader.IsDBNull(indiceNumeroLavoratori))
                            domanda.NumeroLavoratori = reader.GetInt32(indiceNumeroLavoratori);
                        if (!reader.IsDBNull(indiceNumeroGiorni))
                            domanda.NumeroGiorni = reader.GetInt32(indiceNumeroGiorni);
                        if (!reader.IsDBNull(indiceDataInizio))
                            domanda.DataInizio = reader.GetDateTime(indiceDataInizio);
                        if (!reader.IsDBNull(indiceDataFine))
                            domanda.DataFine = reader.GetDateTime(indiceDataFine);
                        if (!reader.IsDBNull(indiceDIA))
                            domanda.Dia = reader.GetString(indiceDIA);
                        if (!reader.IsDBNull(indiceConcessione))
                            domanda.Concessione = reader.GetString(indiceConcessione);
                        if (!reader.IsDBNull(indiceAutorizzazione))
                            domanda.Autorizzazione = reader.GetString(indiceAutorizzazione);
                        if (!reader.IsDBNull(indiceRichiestaVisitaIspettiva))
                            domanda.RichiestaVisitaIspettiva = reader.GetBoolean(indiceRichiestaVisitaIspettiva);
                        if (!reader.IsDBNull(indiceDataVisitaIspettiva))
                            domanda.DataVisitaIspettiva = reader.GetDateTime(indiceDataVisitaIspettiva);
                        if (!reader.IsDBNull(indiceOrarioVisitaIspettiva))
                            domanda.OrarioVisitaIspettiva = reader.GetString(indiceOrarioVisitaIspettiva);
                        if (!reader.IsDBNull(indiceContattoTelefonicoVisitaIspettiva))
                            domanda.ContattoTelefonicoVisitaIspettiva =
                                reader.GetString(indiceContattoTelefonicoVisitaIspettiva);
                        if (!reader.IsDBNull(indiceMotivoVisitaIspettiva))
                            domanda.MotivoVisitaIspettiva = reader.GetString(indiceMotivoVisitaIspettiva);
                        if (!reader.IsDBNull(indiceDataControllo))
                            domanda.DataControllo = reader.GetDateTime(indiceDataControllo);
                        if (!reader.IsDBNull(indiceAttestatoRilasciabile))
                            domanda.AttestatoRilasciabile = reader.GetBoolean(indiceAttestatoRilasciabile);

                        if (!reader.IsDBNull(indiceIdImpresaRichiedente))
                        {
                            domanda.ImpresaRichiedente = new Impresa();
                            domanda.ImpresaRichiedente.TipoImpresa = TipologiaImpresa.SiceNew;
                            domanda.ImpresaRichiedente.IdImpresa = reader.GetInt32(indiceIdImpresaRichiedente);
                            domanda.ImpresaRichiedente.RagioneSociale =
                                reader.GetString(indiceRagioneSocialeImpresaRichiedente);
                        }
                        if (!reader.IsDBNull(indiceIdCommittenteRichiedente))
                        {
                            domanda.CommittenteRichiedente = new Committente();
                            domanda.CommittenteRichiedente.IdCommittente =
                                reader.GetInt32(indiceIdCommittenteRichiedente);
                            domanda.CommittenteRichiedente.RagioneSociale =
                                reader.GetString(indiceRagioneSocialeCommittenteRichiedente);
                        }
                    }
                }
            }

            return domande;
        }

        public Domanda GetDomandaByKey(Int32 idDomanda)
        {
            Domanda domanda = null;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandeSelectByKey"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Domanda

                    #region Indici per reader

                    Int32 indiceIdDomanda = reader.GetOrdinal("idAttestatoRegolaritaDomanda");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceInfoAggiuntiva = reader.GetOrdinal("infoAggiuntiva");
                    Int32 indiceLatitudine = reader.GetOrdinal("latitudine");
                    Int32 indiceLongitudine = reader.GetOrdinal("longitudine");
                    Int32 indiceMese = reader.GetOrdinal("mese");
                    Int32 indiceImporto = reader.GetOrdinal("importo");
                    Int32 indiceDescrizione = reader.GetOrdinal("descrizione");
                    Int32 indicePercentualeManodopera = reader.GetOrdinal("percentualeManodopera");
                    Int32 indiceNumeroLavoratori = reader.GetOrdinal("numeroLavoratori");
                    Int32 indiceNumeroGiorni = reader.GetOrdinal("numeroGiorni");
                    Int32 indiceDataInizio = reader.GetOrdinal("dataInizio");
                    Int32 indiceDataFine = reader.GetOrdinal("dataFine");
                    Int32 indiceDIA = reader.GetOrdinal("numeroDIA");
                    Int32 indiceConcessione = reader.GetOrdinal("numeroConcessione");
                    Int32 indiceAutorizzazione = reader.GetOrdinal("numeroAutorizzazione");
                    Int32 indiceRichiestaVisitaIspettiva = reader.GetOrdinal("richiestaVisitaIspettiva");
                    Int32 indiceDataVisitaIspettiva = reader.GetOrdinal("dataVisitaIspettiva");
                    Int32 indiceOrarioVisitaIspettiva = reader.GetOrdinal("orarioVisitaIspettiva");
                    Int32 indiceContattoTelefonicoVisitaIspettiva =
                        reader.GetOrdinal("contattoTelefonicoVisitaIspettiva");
                    Int32 indiceMotivoVisitaIspettiva = reader.GetOrdinal("motivoVisitaIspettiva");
                    Int32 indiceDataControllo = reader.GetOrdinal("dataControllo");

                    // Committente
                    Int32 indiceIdCommittente = reader.GetOrdinal("idCommittente");
                    Int32 indiceCommittenteRagioneSociale = reader.GetOrdinal("committenteRagioneSociale");
                    Int32 indiceCommittenteTipologia = reader.GetOrdinal("committenteTipologia");
                    Int32 indiceCommittenteIndirizzo = reader.GetOrdinal("committenteIndirizzo");
                    Int32 indiceCommittenteCivico = reader.GetOrdinal("committenteCivico");
                    Int32 indiceCommittenteProvincia = reader.GetOrdinal("committenteProvincia");
                    Int32 indiceCommittenteProvinciaDescr = reader.GetOrdinal("committenteProvinciaDescrizione");
                    Int32 indiceCommittenteComune = reader.GetOrdinal("committenteComune");
                    Int32 indiceCommittenteComuneDescr = reader.GetOrdinal("committenteComuneDescrizione");
                    Int32 indiceCommittenteCap = reader.GetOrdinal("committenteCap");
                    Int32 indiceCommittenteCodiceFiscale = reader.GetOrdinal("committenteCodiceFiscale");
                    Int32 indiceCommittentePartitaIVA = reader.GetOrdinal("committentePartitaIVA");

                    #endregion

                    if (reader.Read())
                    {
                        domanda = new Domanda();

                        domanda.IdDomanda = reader.GetInt32(indiceIdDomanda);
                        domanda.Indirizzo = reader.GetString(indiceIndirizzo);

                        if (!reader.IsDBNull(indiceCivico))
                            domanda.Civico = reader.GetString(indiceCivico);
                        if (!reader.IsDBNull(indiceComune))
                            domanda.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            domanda.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            domanda.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceInfoAggiuntiva))
                            domanda.InfoAggiuntiva = reader.GetString(indiceInfoAggiuntiva);
                        if (!reader.IsDBNull(indiceLatitudine))
                            domanda.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            domanda.Longitudine = reader.GetDecimal(indiceLongitudine);
                        if (!reader.IsDBNull(indiceMese))
                            domanda.Mese = reader.GetDateTime(indiceMese);
                        if (!reader.IsDBNull(indiceImporto))
                            domanda.Importo = reader.GetDecimal(indiceImporto);
                        if (!reader.IsDBNull(indiceDescrizione))
                            domanda.Descrizione = reader.GetString(indiceDescrizione);
                        if (!reader.IsDBNull(indicePercentualeManodopera))
                            domanda.PercentualeManodopera = reader.GetDecimal(indicePercentualeManodopera);
                        if (!reader.IsDBNull(indiceNumeroLavoratori))
                            domanda.NumeroLavoratori = reader.GetInt32(indiceNumeroLavoratori);
                        if (!reader.IsDBNull(indiceNumeroGiorni))
                            domanda.NumeroGiorni = reader.GetInt32(indiceNumeroGiorni);
                        if (!reader.IsDBNull(indiceDataInizio))
                            domanda.DataInizio = reader.GetDateTime(indiceDataInizio);
                        if (!reader.IsDBNull(indiceDataFine))
                            domanda.DataFine = reader.GetDateTime(indiceDataFine);
                        if (!reader.IsDBNull(indiceDIA))
                            domanda.Dia = reader.GetString(indiceDIA);
                        if (!reader.IsDBNull(indiceConcessione))
                            domanda.Concessione = reader.GetString(indiceConcessione);
                        if (!reader.IsDBNull(indiceAutorizzazione))
                            domanda.Autorizzazione = reader.GetString(indiceAutorizzazione);
                        if (!reader.IsDBNull(indiceRichiestaVisitaIspettiva))
                            domanda.RichiestaVisitaIspettiva = reader.GetBoolean(indiceRichiestaVisitaIspettiva);
                        if (!reader.IsDBNull(indiceDataVisitaIspettiva))
                            domanda.DataVisitaIspettiva = reader.GetDateTime(indiceDataVisitaIspettiva);
                        if (!reader.IsDBNull(indiceOrarioVisitaIspettiva))
                            domanda.OrarioVisitaIspettiva = reader.GetString(indiceOrarioVisitaIspettiva);
                        if (!reader.IsDBNull(indiceContattoTelefonicoVisitaIspettiva))
                            domanda.ContattoTelefonicoVisitaIspettiva =
                                reader.GetString(indiceContattoTelefonicoVisitaIspettiva);
                        if (!reader.IsDBNull(indiceMotivoVisitaIspettiva))
                            domanda.MotivoVisitaIspettiva = reader.GetString(indiceMotivoVisitaIspettiva);
                        if (!reader.IsDBNull(indiceDataControllo))
                            domanda.DataControllo = reader.GetDateTime(indiceDataControllo);

                        domanda.Committente = new Committente();
                        domanda.Committente.IdCommittente = reader.GetInt32(indiceIdCommittente);
                        domanda.Committente.RagioneSociale = reader.GetString(indiceCommittenteRagioneSociale);
                        domanda.Committente.Tipologia =
                            (TipologiaCommittente) reader.GetInt16(indiceCommittenteTipologia);
                        domanda.Committente.Indirizzo = reader.GetString(indiceCommittenteIndirizzo);
                        if (!reader.IsDBNull(indiceCommittenteCivico))
                            domanda.Committente.Civico = reader.GetString(indiceCommittenteCivico);
                        domanda.Committente.Comune = reader.GetInt64(indiceCommittenteComune);
                        domanda.Committente.ComuneDescrizione = reader.GetString(indiceCommittenteComuneDescr);
                        domanda.Committente.Provincia = reader.GetInt32(indiceCommittenteProvincia);
                        domanda.Committente.ProvinciaDescrizione = reader.GetString(indiceCommittenteProvinciaDescr);
                        domanda.Committente.Cap = reader.GetString(indiceCommittenteCap);
                        if (!reader.IsDBNull(indiceCommittenteCodiceFiscale))
                            domanda.Committente.CodiceFiscale = reader.GetString(indiceCommittenteCodiceFiscale);
                        if (!reader.IsDBNull(indiceCommittentePartitaIVA))
                            domanda.Committente.PartitaIVA = reader.GetString(indiceCommittentePartitaIVA);
                    }

                    #endregion

                    reader.NextResult();

                    #region Subappalti

                    #region Indici per Reader

                    Int32 indiceIdSubappalto = reader.GetOrdinal("idAttestatoRegolaritaDomandaSubappalto");
                    Int32 indiceIdImpresaAppaltante = reader.GetOrdinal("idImpresaAppaltante");
                    Int32 indiceIdAttestatoImpresaAppaltante =
                        reader.GetOrdinal("idAttestatoRegolaritaImpresaAppaltante");
                    Int32 indiceIdImpresaAppaltata = reader.GetOrdinal("idImpresaAppaltata");
                    Int32 indiceIdAttestatoImpresaAppaltata = reader.GetOrdinal("idAttestatoRegolaritaImpresaAppaltata");

                    // Appaltata
                    Int32 indiceImpAtaRagioneSociale = reader.GetOrdinal("impAtaRagioneSociale");
                    Int32 indiceImpAtaPartitaIva = reader.GetOrdinal("impAtaPartitaIva");
                    Int32 indiceImpAtaCodiceFiscale = reader.GetOrdinal("impAtaCodiceFiscale");
                    Int32 indiceImpAtaIndirizzo = reader.GetOrdinal("impAtaIndirizzo");
                    Int32 indiceImpAtaComune = reader.GetOrdinal("impAtaComune");
                    Int32 indiceImpAtaProvincia = reader.GetOrdinal("impAtaProvincia");
                    Int32 indiceImpAtaCap = reader.GetOrdinal("impAtaCap");
                    Int32 indiceImpAtaIdCassaEdileProvenienza = reader.GetOrdinal("impAtaIdCassaEdileProvenienza");
                    Int32 indiceImpAtaTipologiaContratto = reader.GetOrdinal("impAtaTipologiaContratto");
                    Int32 indiceImpAtaLavoratoreAutonomo = reader.GetOrdinal("impAtaLavoratoreAutonomo");

                    Int32 indiceAtImpAtaRagioneSociale = reader.GetOrdinal("atImpAtaRagioneSociale");
                    Int32 indiceAtImpAtaPartitaIva = reader.GetOrdinal("atImpAtaPartitaIva");
                    Int32 indiceAtImpAtaCodiceFiscale = reader.GetOrdinal("atImpAtaCodiceFiscale");
                    Int32 indiceAtImpAtaIndirizzo = reader.GetOrdinal("atImpAtaIndirizzo");
                    Int32 indiceAtImpAtaComune = reader.GetOrdinal("atImpAtaComune");
                    Int32 indiceAtImpAtaProvincia = reader.GetOrdinal("atImpAtaProvincia");
                    Int32 indiceAtImpAtaCap = reader.GetOrdinal("atImpAtaCap");
                    Int32 indiceAtImpAtaIdCassaEdileProvenienza = reader.GetOrdinal("atImpAtaIdCassaEdileProvenienza");
                    Int32 indiceAtImpAtaTipologiaContratto = reader.GetOrdinal("atImpAtaTipologiaContratto");
                    Int32 indiceAtImpAtaLavoratoreAutonomo = reader.GetOrdinal("atImpAtaLavoratoreAutonomo");

                    // Appaltante
                    Int32 indiceImpAnteRagioneSociale = reader.GetOrdinal("impAnteRagioneSociale");
                    Int32 indiceImpAntePartitaIva = reader.GetOrdinal("impAntePartitaIva");
                    Int32 indiceImpAnteCodiceFiscale = reader.GetOrdinal("impAnteCodiceFiscale");
                    Int32 indiceImpAnteIndirizzo = reader.GetOrdinal("impAnteIndirizzo");
                    Int32 indiceImpAnteComune = reader.GetOrdinal("impAnteComune");
                    Int32 indiceImpAnteProvincia = reader.GetOrdinal("impAnteProvincia");
                    Int32 indiceImpAnteCap = reader.GetOrdinal("impAnteCap");
                    Int32 indiceImpAnteIdCassaEdileProvenienza = reader.GetOrdinal("impAnteIdCassaEdileProvenienza");
                    Int32 indiceImpAnteTipologiaContratto = reader.GetOrdinal("impAnteTipologiaContratto");
                    Int32 indiceImpAnteLavoratoreAutonomo = reader.GetOrdinal("impAnteLavoratoreAutonomo");

                    Int32 indiceAtImpAnteRagioneSociale = reader.GetOrdinal("atImpAnteRagioneSociale");
                    Int32 indiceAtImpAntePartitaIva = reader.GetOrdinal("atImpAntePartitaIva");
                    Int32 indiceAtImpAnteCodiceFiscale = reader.GetOrdinal("atImpAnteCodiceFiscale");
                    Int32 indiceAtImpAnteIndirizzo = reader.GetOrdinal("atImpAnteIndirizzo");
                    Int32 indiceAtImpAnteComune = reader.GetOrdinal("atImpAnteComune");
                    Int32 indiceAtImpAnteProvincia = reader.GetOrdinal("atImpAnteProvincia");
                    Int32 indiceAtImpAnteCap = reader.GetOrdinal("atImpAnteCap");
                    Int32 indiceAtImpAnteIdCassaEdileProvenienza = reader.GetOrdinal("atImpAnteIdCassaEdileProvenienza");
                    Int32 indiceAtImpAnteTipologiaContratto = reader.GetOrdinal("atImpAnteTipologiaContratto");
                    Int32 indiceAtImpAnteLavoratoreAutonomo = reader.GetOrdinal("atImpAnteLavoratoreAutonomo");

                    #endregion

                    while (reader.Read())
                    {
                        Subappalto subappalto = new Subappalto();
                        domanda.Subappalti.Add(subappalto);

                        subappalto.IdSubappalto = reader.GetInt32(indiceIdSubappalto);

                        // Impresa appaltata, c'� sicuramente
                        Impresa impresaAppaltata = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresaAppaltata))
                        {
                            // Impresa SiceNew
                            impresaAppaltata.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresaAppaltata.IdImpresa = reader.GetInt32(indiceIdImpresaAppaltata);

                            impresaAppaltata.RagioneSociale = reader.GetString(indiceImpAtaRagioneSociale);
                            if (!reader.IsDBNull(indiceImpAtaPartitaIva))
                                impresaAppaltata.PartitaIva = reader.GetString(indiceImpAtaPartitaIva);
                            if (!reader.IsDBNull(indiceImpAtaCodiceFiscale))
                                impresaAppaltata.CodiceFiscale = reader.GetString(indiceImpAtaCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpAtaIndirizzo))
                                impresaAppaltata.Indirizzo = reader.GetString(indiceImpAtaIndirizzo);
                            if (!reader.IsDBNull(indiceImpAtaComune))
                                impresaAppaltata.Comune = reader.GetString(indiceImpAtaComune);
                            if (!reader.IsDBNull(indiceImpAtaProvincia))
                                impresaAppaltata.Provincia = reader.GetString(indiceImpAtaProvincia);
                            if (!reader.IsDBNull(indiceImpAtaCap))
                                impresaAppaltata.Cap = reader.GetString(indiceImpAtaCap);
                            if (!reader.IsDBNull(indiceImpAtaIdCassaEdileProvenienza))
                                impresaAppaltata.IdCassaEdileProvenienza =
                                    reader.GetString(indiceImpAtaIdCassaEdileProvenienza);
                            if (!reader.IsDBNull(indiceImpAtaTipologiaContratto))
                                impresaAppaltata.TipologiaContratto =
                                    (TipologiaContratto) reader.GetInt32(indiceImpAtaTipologiaContratto);
                            impresaAppaltata.LavoratoreAutonomo = reader.GetBoolean(indiceImpAtaLavoratoreAutonomo);
                        }
                        else
                        {
                            // Impresa Nuova
                            impresaAppaltata.TipoImpresa = TipologiaImpresa.Nuova;
                            impresaAppaltata.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresaAppaltata);

                            impresaAppaltata.RagioneSociale = reader.GetString(indiceAtImpAtaRagioneSociale);
                            if (!reader.IsDBNull(indiceAtImpAtaPartitaIva))
                                impresaAppaltata.PartitaIva = reader.GetString(indiceAtImpAtaPartitaIva);
                            impresaAppaltata.CodiceFiscale = reader.GetString(indiceAtImpAtaCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtImpAtaIndirizzo))
                                impresaAppaltata.Indirizzo = reader.GetString(indiceAtImpAtaIndirizzo);
                            if (!reader.IsDBNull(indiceAtImpAtaComune))
                                impresaAppaltata.Comune = reader.GetString(indiceAtImpAtaComune);
                            if (!reader.IsDBNull(indiceAtImpAtaProvincia))
                                impresaAppaltata.Provincia = reader.GetString(indiceAtImpAtaProvincia);
                            if (!reader.IsDBNull(indiceAtImpAtaCap))
                                impresaAppaltata.Cap = reader.GetString(indiceAtImpAtaCap);
                            if (!reader.IsDBNull(indiceAtImpAtaIdCassaEdileProvenienza))
                                impresaAppaltata.IdCassaEdileProvenienza =
                                    reader.GetString(indiceAtImpAtaIdCassaEdileProvenienza);
                            impresaAppaltata.TipologiaContratto =
                                (TipologiaContratto) reader.GetInt16(indiceAtImpAtaTipologiaContratto);
                            impresaAppaltata.LavoratoreAutonomo = reader.GetBoolean(indiceAtImpAtaLavoratoreAutonomo);
                        }

                        subappalto.Appaltata = impresaAppaltata;

                        // Impresa appaltante
                        Impresa impresaAppaltante = null;

                        if (!reader.IsDBNull(indiceIdImpresaAppaltante))
                        {
                            // Impresa SiceNew
                            impresaAppaltante = new Impresa();
                            impresaAppaltante.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresaAppaltante.IdImpresa = reader.GetInt32(indiceIdImpresaAppaltante);

                            impresaAppaltante.RagioneSociale = reader.GetString(indiceImpAnteRagioneSociale);
                            if (!reader.IsDBNull(indiceImpAntePartitaIva))
                                impresaAppaltante.PartitaIva = reader.GetString(indiceImpAntePartitaIva);
                            if (!reader.IsDBNull(indiceImpAnteCodiceFiscale))
                                impresaAppaltante.CodiceFiscale = reader.GetString(indiceImpAnteCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpAnteIndirizzo))
                                impresaAppaltante.Indirizzo = reader.GetString(indiceImpAnteIndirizzo);
                            if (!reader.IsDBNull(indiceImpAnteComune))
                                impresaAppaltante.Comune = reader.GetString(indiceImpAnteComune);
                            if (!reader.IsDBNull(indiceImpAnteProvincia))
                                impresaAppaltante.Provincia = reader.GetString(indiceImpAnteProvincia);
                            if (!reader.IsDBNull(indiceImpAnteCap))
                                impresaAppaltante.Cap = reader.GetString(indiceImpAnteCap);
                            if (!reader.IsDBNull(indiceImpAnteIdCassaEdileProvenienza))
                                impresaAppaltante.IdCassaEdileProvenienza =
                                    reader.GetString(indiceImpAnteIdCassaEdileProvenienza);
                            if (!reader.IsDBNull(indiceImpAnteTipologiaContratto))
                                impresaAppaltante.TipologiaContratto =
                                    (TipologiaContratto) reader.GetInt32(indiceImpAnteTipologiaContratto);
                            impresaAppaltante.LavoratoreAutonomo = reader.GetBoolean(indiceImpAnteLavoratoreAutonomo);
                        }

                        if (!reader.IsDBNull(indiceIdAttestatoImpresaAppaltante))
                        {
                            // Impresa Nuova
                            impresaAppaltante = new Impresa();
                            impresaAppaltante.TipoImpresa = TipologiaImpresa.Nuova;
                            impresaAppaltante.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresaAppaltante);

                            impresaAppaltante.RagioneSociale = reader.GetString(indiceAtImpAnteRagioneSociale);
                            if (!reader.IsDBNull(indiceAtImpAntePartitaIva))
                                impresaAppaltante.PartitaIva = reader.GetString(indiceAtImpAntePartitaIva);
                            impresaAppaltante.CodiceFiscale = reader.GetString(indiceAtImpAnteCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtImpAnteIndirizzo))
                                impresaAppaltante.Indirizzo = reader.GetString(indiceAtImpAnteIndirizzo);
                            if (!reader.IsDBNull(indiceAtImpAnteComune))
                                impresaAppaltante.Comune = reader.GetString(indiceAtImpAnteComune);
                            if (!reader.IsDBNull(indiceAtImpAnteProvincia))
                                impresaAppaltante.Provincia = reader.GetString(indiceAtImpAnteProvincia);
                            if (!reader.IsDBNull(indiceAtImpAnteCap))
                                impresaAppaltante.Cap = reader.GetString(indiceAtImpAnteCap);
                            if (!reader.IsDBNull(indiceAtImpAnteIdCassaEdileProvenienza))
                                impresaAppaltante.IdCassaEdileProvenienza =
                                    reader.GetString(indiceAtImpAnteIdCassaEdileProvenienza);
                            impresaAppaltante.TipologiaContratto =
                                (TipologiaContratto) reader.GetInt16(indiceAtImpAnteTipologiaContratto);
                            impresaAppaltante.LavoratoreAutonomo = reader.GetBoolean(indiceAtImpAnteLavoratoreAutonomo);
                        }

                        subappalto.Appaltante = impresaAppaltante;
                    }

                    #endregion
                }
            }

            return domanda;
        }


        public DomandaCollection GetRiepilogoAttestati(DomandaFilter filtro)
        {
            Domanda domanda = null;
            DomandaCollection domColl = new DomandaCollection();

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaRiepilogoAttestati"))
            {
                if (!String.IsNullOrEmpty(filtro.RagSocDenominazione))
                    databaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagSocDenominazione);
                if (!String.IsNullOrEmpty(filtro.Comune))
                    databaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.Comune);
                if (filtro.Mese.HasValue)
                    databaseCemi.AddInParameter(comando, "@mese", DbType.DateTime, filtro.Mese.Value);
                if (!String.IsNullOrEmpty(filtro.IvaCodFisc))
                    databaseCemi.AddInParameter(comando, "@ivaCodFisc", DbType.String, filtro.IvaCodFisc);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Domanda

                    #region Indici per reader

                    Int32 indiceIdDomanda = reader.GetOrdinal("idAttestatoRegolaritaDomanda");
                    Int32 indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    Int32 indiceCivico = reader.GetOrdinal("civico");
                    Int32 indiceComune = reader.GetOrdinal("comune");
                    Int32 indiceProvincia = reader.GetOrdinal("provincia");
                    Int32 indiceCap = reader.GetOrdinal("cap");
                    Int32 indiceInfoAggiuntiva = reader.GetOrdinal("infoAggiuntiva");

                    Int32 indiceMese = reader.GetOrdinal("mese");

                    // Committente
                    Int32 indiceIdCommittente = reader.GetOrdinal("idCommittenteRichiedente");
                    Int32 indiceCommittenteRagioneSociale = reader.GetOrdinal("ragioneSocialeCommittente");
                    Int32 indiceCommittenteCodiceFiscale = reader.GetOrdinal("codiceFiscaleCommittente");
                    Int32 indiceCommittentePartitaIVA = reader.GetOrdinal("partitaIVACommittente");
                    Int32 indiceCommittenteCognome = reader.GetOrdinal("cognomeCommittente");
                    Int32 indiceCommittenteNome = reader.GetOrdinal("nomeCommittente");

                    // ImpresaRichiedente
                    Int32 indiceIdImpresaRichiedente = reader.GetOrdinal("idImpresaRichiedente");
                    Int32 indiceImpresaRagioneSociale = reader.GetOrdinal("ragioneSocialeImpresa");
                    Int32 indiceImpresaCodiceFiscale = reader.GetOrdinal("codiceFiscaleImpresa");
                    Int32 indiceImpresaPartitaIVA = reader.GetOrdinal("partitaIVAImpresa");

                    //imprese
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indicePartitaIVA = reader.GetOrdinal("partitaIVA");

                    #endregion

                    int idDom = -1;

                    while (reader.Read())
                    {
                        //if (!reader.IsDBNull(indiceIdDomanda))
                        //{
                        //    idDom = reader.GetInt32(indiceIdDomanda);
                        //}

                        if (domanda == null || (reader.GetInt32(indiceIdDomanda) != idDom))
                        {
                            domanda = new Domanda();
                            domColl.Add(domanda);
                            domanda.IdDomanda = reader.GetInt32(indiceIdDomanda);

                            domanda.Indirizzo = reader.GetString(indiceIndirizzo);
                            if (!reader.IsDBNull(indiceCivico))
                                domanda.Civico = reader.GetString(indiceCivico);
                            if (!reader.IsDBNull(indiceComune))
                                domanda.Comune = reader.GetString(indiceComune);
                            if (!reader.IsDBNull(indiceProvincia))
                                domanda.Provincia = reader.GetString(indiceProvincia);
                            if (!reader.IsDBNull(indiceCap))
                                domanda.Cap = reader.GetString(indiceCap);
                            if (!reader.IsDBNull(indiceInfoAggiuntiva))
                                domanda.InfoAggiuntiva = reader.GetString(indiceInfoAggiuntiva);

                            if (!reader.IsDBNull(indiceMese))
                                domanda.Mese = reader.GetDateTime(indiceMese);

                            domanda.Committente = new Committente();
                            if (!reader.IsDBNull(indiceIdCommittente))
                                domanda.Committente.IdCommittente = reader.GetInt32(indiceIdCommittente);
                            if (!reader.IsDBNull(indiceCommittenteRagioneSociale))
                                domanda.Committente.RagioneSociale = reader.GetString(indiceCommittenteRagioneSociale);
                            if (!reader.IsDBNull(indiceCommittenteCodiceFiscale))
                                domanda.Committente.CodiceFiscale = reader.GetString(indiceCommittenteCodiceFiscale);
                            if (!reader.IsDBNull(indiceCommittentePartitaIVA))
                                domanda.Committente.PartitaIVA = reader.GetString(indiceCommittentePartitaIVA);
                            if (!reader.IsDBNull(indiceCommittenteCognome))
                                domanda.Committente.Cognome = reader.GetString(indiceCommittenteCognome);
                            if (!reader.IsDBNull(indiceCommittenteNome))
                                domanda.Committente.Nome = reader.GetString(indiceCommittenteNome);

                            domanda.ImpresaRichiedente = new Impresa();
                            if (!reader.IsDBNull(indiceIdImpresaRichiedente))
                                domanda.ImpresaRichiedente.IdImpresa = reader.GetInt32(indiceIdImpresaRichiedente);
                            if (!reader.IsDBNull(indiceImpresaRagioneSociale))
                                domanda.ImpresaRichiedente.RagioneSociale = reader.GetString(indiceImpresaRagioneSociale);
                            if (!reader.IsDBNull(indiceImpresaCodiceFiscale))
                                domanda.ImpresaRichiedente.CodiceFiscale = reader.GetString(indiceImpresaCodiceFiscale);
                            if (!reader.IsDBNull(indiceImpresaPartitaIVA))
                                domanda.ImpresaRichiedente.PartitaIva = reader.GetString(indiceImpresaPartitaIVA);

                            idDom = reader.GetInt32(indiceIdDomanda);

                            domanda.ListaImprese = new ImpresaCollection();

                            Impresa imp = new Impresa();
                            if (!reader.IsDBNull(indiceIdImpresa))
                                imp.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            imp.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            imp.PartitaIva = reader.GetString(indicePartitaIVA);
                            imp.CodiceFiscale = reader.GetString(indiceCodiceFiscale);

                            domanda.ListaImprese.Add(imp);
                        }
                        else
                        {
                            Impresa imp = new Impresa();
                            if (!reader.IsDBNull(indiceIdImpresa))
                                imp.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            imp.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            imp.PartitaIva = reader.GetString(indicePartitaIVA);
                            imp.CodiceFiscale = reader.GetString(indiceCodiceFiscale);

                            domanda.ListaImprese.Add(imp);
                        }
                    }

                    #endregion
                }
            }

            return domColl;
        }


        //vallla

        public DomandaImpresaCollection GetLavoratoriInDomanda(Int32 idDomanda)
        {
            DomandaImpresaCollection domandaImpresaColl = new DomandaImpresaCollection();
            DomandaImpresa domandaImpresa = null;

            using ( 
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand(
                        "dbo.USP_AttestatoRegolaritaReportDomandaLavoratoriSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    Int32 indiceIdDomandaLavoratore = reader.GetOrdinal("idAttestatoRegolaritaDomandaLavoratori");
                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceIdAttestatoLavoratore = reader.GetOrdinal("idAttestatoRegolaritaLavoratore");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("idAttestatoRegolaritaImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atRagioneSociale");

                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                    Int32 indiceTipoAssunzione = reader.GetOrdinal("tipoAssunzione");
                    Int32 indiceDataAssunzione = reader.GetOrdinal("dataAssunzione");
                    Int32 indiceDataCessazione = reader.GetOrdinal("dataCessazione");

                    Int32 indiceAtCognome = reader.GetOrdinal("atCognome");
                    Int32 indiceAtNome = reader.GetOrdinal("atNome");
                    Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atCodiceFiscale");
                    Int32 indiceAtDataNascita = reader.GetOrdinal("atDataNascita");
                    Int32 indiceAtTipoAssunzione = reader.GetOrdinal("atTipoAssunzione");
                    Int32 indiceAtDataAssunzione = reader.GetOrdinal("atDataAssunzione");
                    Int32 indiceAtDataCessazione = reader.GetOrdinal("atDataCessazione");

                    #endregion

                    Int32 idImpresaSice = -1;
                    Int32 idImpresaNuova = -1;

                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            idImpresaSice = reader.GetInt32(indiceIdImpresa);
                        }


                        if (!reader.IsDBNull(indiceIdAttestatoImpresa))
                        {
                            idImpresaNuova = reader.GetInt32(indiceIdAttestatoImpresa);
                        }


                        if (domandaImpresa == null
                            ||
                            (idImpresaSice != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew &&
                             domandaImpresa.Impresa.IdImpresa.Value != idImpresaSice)
                            ||
                            (idImpresaNuova != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.Nuova &&
                             domandaImpresa.Impresa.IdImpresa.Value != idImpresaNuova)
                            ||
                            (idImpresaSice != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.Nuova)
                            ||
                            (idImpresaNuova != -1 && domandaImpresa.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                            )
                        {
                            domandaImpresa = new DomandaImpresa();
                            domandaImpresaColl.Add(domandaImpresa);

                            if (idImpresaSice != -1)
                            {
                                domandaImpresa.Impresa = new Impresa();
                                domandaImpresa.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                                domandaImpresa.Impresa.IdImpresa = idImpresaSice;
                                domandaImpresa.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            }
                            else
                            {
                                domandaImpresa.Impresa = new Impresa();
                                domandaImpresa.Impresa.TipoImpresa = TipologiaImpresa.Nuova;
                                domandaImpresa.Impresa.IdImpresa = idImpresaNuova;
                                domandaImpresa.Impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);
                            }
                        }

                        Lavoratore lavoratore = new Lavoratore();
                        domandaImpresa.Lavoratori.Add(lavoratore);

                        lavoratore.IdDomandaLavoratore = reader.GetInt32(indiceIdDomandaLavoratore);
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                            lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                                lavoratore.Nome = reader.GetString(indiceNome);
                            if (!reader.IsDBNull(indiceDataNascita))
                                lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                                lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            if (!reader.IsDBNull(indiceTipoAssunzione))
                                lavoratore.TipoAssunzione = reader.GetInt32(indiceTipoAssunzione);
                            if (!reader.IsDBNull(indiceDataAssunzione))
                                lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                            if (!reader.IsDBNull(indiceDataCessazione))
                                lavoratore.DataCessazione = reader.GetDateTime(indiceDataCessazione);
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdAttestatoLavoratore);

                            lavoratore.Cognome = reader.GetString(indiceAtCognome);
                            lavoratore.Nome = reader.GetString(indiceAtNome);
                            lavoratore.DataNascita = reader.GetDateTime(indiceAtDataNascita);
                            lavoratore.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                            if (!reader.IsDBNull(indiceAtTipoAssunzione))
                                lavoratore.TipoAssunzione = reader.GetInt32(indiceAtTipoAssunzione);
                            if (!reader.IsDBNull(indiceAtDataAssunzione))
                                lavoratore.DataAssunzione = reader.GetDateTime(indiceAtDataAssunzione);
                            if (!reader.IsDBNull(indiceAtDataCessazione))
                                lavoratore.DataCessazione = reader.GetDateTime(indiceAtDataCessazione);
                        }

                        idImpresaSice = -1;
                        idImpresaNuova = -1;
                    }
                }
            }

            return domandaImpresaColl;
        }

        public DomandaImpresa GetLavoratoriInDomandaPerImpresa(Int32 idDomanda, Int32? idImpresa,
                                                               Int32? idAttestatoImpresa)
        {
            DomandaImpresa domandaImpresa = new DomandaImpresa();

            if ((!idImpresa.HasValue && !idAttestatoImpresa.HasValue) ||
                (idImpresa.HasValue && idAttestatoImpresa.HasValue))
            {
                throw new ArgumentException(
                    "idImpresa e idAttestatoImpresa non possono essere entrambi nulli o entrambi valorizzati");
            }
            else
            {
                using (
                    DbCommand comando =
                        databaseCemi.GetStoredProcCommand(
                            "dbo.USP_AttestatoRegolaritaDomandaLavoratoriSelectByDomandaEImpresa"))
                {
                    domandaImpresa.Impresa = new Impresa();

                    databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                    if (idImpresa.HasValue)
                    {
                        databaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa.Value);
                        domandaImpresa.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                        domandaImpresa.Impresa.IdImpresa = idImpresa.Value;
                    }
                    if (idAttestatoImpresa.HasValue)
                    {
                        databaseCemi.AddInParameter(comando, "@idAttestatoImpresa", DbType.Int32,
                                                    idAttestatoImpresa.Value);
                        domandaImpresa.Impresa.TipoImpresa = TipologiaImpresa.Nuova;
                        domandaImpresa.Impresa.IdImpresa = idAttestatoImpresa.Value;
                    }

                    using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                    {
                        #region Indici per reader

                        Int32 indiceIdDomandaLavoratore = reader.GetOrdinal("idAttestatoRegolaritaDomandaLavoratori");
                        Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                        Int32 indiceIdAttestatoLavoratore = reader.GetOrdinal("idAttestatoRegolaritaLavoratore");

                        Int32 indiceCognome = reader.GetOrdinal("cognome");
                        Int32 indiceNome = reader.GetOrdinal("nome");
                        Int32 indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                        Int32 indiceDataNascita = reader.GetOrdinal("dataNascita");
                        Int32 indiceTipoAssunzione = reader.GetOrdinal("tipoAssunzione");
                        Int32 indiceDataAssunzione = reader.GetOrdinal("dataAssunzione");
                        Int32 indiceDataCessazione = reader.GetOrdinal("dataCessazione");

                        Int32 indiceAtCognome = reader.GetOrdinal("atCognome");
                        Int32 indiceAtNome = reader.GetOrdinal("atNome");
                        Int32 indiceAtCodiceFiscale = reader.GetOrdinal("atCodiceFiscale");
                        Int32 indiceAtDataNascita = reader.GetOrdinal("atDataNascita");
                        Int32 indiceAtTipoAssunzione = reader.GetOrdinal("atTipoAssunzione");
                        Int32 indiceAtDataAssunzione = reader.GetOrdinal("atDataAssunzione");
                        Int32 indiceAtDataCessazione = reader.GetOrdinal("atDataCessazione");

                        #endregion

                        while (reader.Read())
                        {
                            Lavoratore lavoratore = new Lavoratore();
                            domandaImpresa.Lavoratori.Add(lavoratore);

                            lavoratore.IdDomandaLavoratore = reader.GetInt32(indiceIdDomandaLavoratore);
                            if (!reader.IsDBNull(indiceIdLavoratore))
                            {
                                lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                                lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                                lavoratore.Cognome = reader.GetString(indiceCognome);
                                if (!reader.IsDBNull(indiceNome))
                                    lavoratore.Nome = reader.GetString(indiceNome);
                                if (!reader.IsDBNull(indiceDataNascita))
                                    lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                                if (!reader.IsDBNull(indiceCodiceFiscale))
                                    lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                                if (!reader.IsDBNull(indiceTipoAssunzione))
                                    lavoratore.TipoAssunzione = reader.GetInt32(indiceTipoAssunzione);
                                if (!reader.IsDBNull(indiceDataAssunzione))
                                    lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                                if (!reader.IsDBNull(indiceDataCessazione))
                                    lavoratore.DataCessazione = reader.GetDateTime(indiceDataCessazione);
                            }
                            else
                            {
                                lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
                                lavoratore.IdLavoratore = reader.GetInt32(indiceIdAttestatoLavoratore);

                                lavoratore.Cognome = reader.GetString(indiceAtCognome);
                                lavoratore.Nome = reader.GetString(indiceAtNome);
                                lavoratore.DataNascita = reader.GetDateTime(indiceAtDataNascita);
                                lavoratore.CodiceFiscale = reader.GetString(indiceAtCodiceFiscale);
                                if (!reader.IsDBNull(indiceAtTipoAssunzione))
                                    lavoratore.TipoAssunzione = reader.GetInt32(indiceAtTipoAssunzione);
                                if (!reader.IsDBNull(indiceAtDataAssunzione))
                                    lavoratore.DataAssunzione = reader.GetDateTime(indiceAtDataAssunzione);
                                if (!reader.IsDBNull(indiceAtDataCessazione))
                                    lavoratore.DataCessazione = reader.GetDateTime(indiceAtDataCessazione);
                            }
                        }
                    }
                }
            }

            return domandaImpresa;
        }

        public ImpresaCollection GetImpreseSelezionateInSubappalto(int idDomanda)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaImpreseSelezionateSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(comando, "@lavoratoriAutonomi", DbType.Boolean, false);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("idAttestatoImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("impRagioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atImpRagioneSociale");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            // Impresa SiceNew

                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        else
                        {
                            // Nuova Impresa

                            impresa.TipoImpresa = TipologiaImpresa.Nuova;
                            impresa.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);
                        }
                    }
                }
            }

            return imprese;
        }

        public bool UpdateDomanda(Domanda domanda)
        {
            bool res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            else
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();

                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            using (
                                DbCommand comando =
                                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandeUpdate"))
                            {
                                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, domanda.Indirizzo);

                                if (domanda.IdDomanda.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda);
                                if (!String.IsNullOrEmpty(domanda.Civico))
                                    DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, domanda.Civico);
                                if (!String.IsNullOrEmpty(domanda.Comune))
                                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, domanda.Comune);
                                if (!String.IsNullOrEmpty(domanda.Provincia))
                                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, domanda.Provincia);
                                if (!String.IsNullOrEmpty(domanda.Cap))
                                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, domanda.Cap);
                                if (domanda.Latitudine.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal,
                                                                domanda.Latitudine.Value);
                                if (domanda.Longitudine.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal,
                                                                domanda.Longitudine.Value);
                                if (!String.IsNullOrEmpty(domanda.InfoAggiuntiva))
                                    DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String,
                                                                domanda.InfoAggiuntiva);
                                if (domanda.IdImpresaRichiedente.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@idImpresaRichiedente", DbType.Int32,
                                                                domanda.IdImpresaRichiedente.Value);
                                if (domanda.IdCommittenteRichiedente.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@idCommittenteRichiedente", DbType.Int32,
                                                                domanda.IdCommittenteRichiedente.Value);
                                if (domanda.Importo.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@importo", DbType.Decimal,
                                                                domanda.Importo.Value);
                                if (!String.IsNullOrEmpty(domanda.Descrizione))
                                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String,
                                                                domanda.Descrizione);
                                if (domanda.PercentualeManodopera.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@percentualeManodopera", DbType.Decimal,
                                                                domanda.PercentualeManodopera.Value);
                                if (domanda.NumeroLavoratori.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@numeroLavoratori", DbType.Int32,
                                                                domanda.NumeroLavoratori.Value);
                                if (domanda.NumeroGiorni.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@numeroGiorni", DbType.Int32,
                                                                domanda.NumeroGiorni.Value);

                                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, domanda.DataInizio);
                                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, domanda.DataFine);

                                DatabaseCemi.AddInParameter(comando, "@mese", DbType.DateTime, domanda.Mese);

                                if (!String.IsNullOrEmpty(domanda.Dia))
                                    DatabaseCemi.AddInParameter(comando, "@numeroDIA", DbType.String, domanda.Dia);
                                if (!String.IsNullOrEmpty(domanda.Concessione))
                                    DatabaseCemi.AddInParameter(comando, "@numeroConcessione", DbType.String,
                                                                domanda.Concessione);
                                if (!String.IsNullOrEmpty(domanda.Autorizzazione))
                                    DatabaseCemi.AddInParameter(comando, "@numeroAutorizzazione", DbType.String,
                                                                domanda.Autorizzazione);

                                DatabaseCemi.AddInParameter(comando, "@richiestaVisitaIspettiva", DbType.Boolean,
                                                            domanda.RichiestaVisitaIspettiva);
                                if (!String.IsNullOrEmpty(domanda.OrarioVisitaIspettiva))
                                    DatabaseCemi.AddInParameter(comando, "@orarioVisitaIspettiva", DbType.String,
                                                                domanda.OrarioVisitaIspettiva);
                                if (!String.IsNullOrEmpty(domanda.MotivoVisitaIspettiva))
                                    DatabaseCemi.AddInParameter(comando, "@motivoVisitaIspettiva", DbType.String,
                                                                domanda.MotivoVisitaIspettiva);
                                if (!String.IsNullOrEmpty(domanda.ContattoTelefonicoVisitaIspettiva))
                                    DatabaseCemi.AddInParameter(comando, "@contattoTelefonicoVisitaIspettiva",
                                                                DbType.String, domanda.ContattoTelefonicoVisitaIspettiva);
                                if (domanda.DataVisitaIspettiva.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@dataVisitaIspettiva", DbType.DateTime,
                                                                domanda.DataVisitaIspettiva.Value);

                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    //deleteComm
                                    if (UpdateCommittente(domanda.Committente, transaction))
                                    {
                                        DeleteSubappaltiDellaDomanda(domanda.IdDomanda.Value, transaction);
                                        DeleteLavoratoriDellaDomanda(domanda.IdDomanda.Value, transaction);

                                        if (InsertUpdateSubappaltieLavoratori(domanda, transaction))
                                        {
                                            res = true;
                                        }
                                        else
                                        {
                                            throw new Exception("Errore durante l'InsertUpdateSubappaltieLavoratori");
                                        }
                                    }
                                    else
                                        throw new Exception("Errore durante l'UpdateCommittente");
                                }
                                else
                                {
                                    throw new Exception("Errore durante l'inserimento della domanda");
                                }

                                if (res)
                                {
                                    transaction.Commit();
                                }
                                else
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }

            return res;
        }

        public bool InsertDomanda(Domanda domanda)
        {
            bool res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            else
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();

                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            using (
                                DbCommand comando =
                                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandeInsert"))
                            {
                                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, domanda.Indirizzo);

                                if (!String.IsNullOrEmpty(domanda.Civico))
                                    DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, domanda.Civico);
                                if (!String.IsNullOrEmpty(domanda.Comune))
                                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, domanda.Comune);
                                if (!String.IsNullOrEmpty(domanda.Provincia))
                                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, domanda.Provincia);
                                if (!String.IsNullOrEmpty(domanda.Cap))
                                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, domanda.Cap);
                                if (domanda.Latitudine.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal,
                                                                domanda.Latitudine.Value);
                                if (domanda.Longitudine.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal,
                                                                domanda.Longitudine.Value);
                                if (!String.IsNullOrEmpty(domanda.InfoAggiuntiva))
                                    DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String,
                                                                domanda.InfoAggiuntiva);
                                if (domanda.IdImpresaRichiedente.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@idImpresaRichiedente", DbType.Int32,
                                                                domanda.IdImpresaRichiedente.Value);
                                if (domanda.IdCommittenteRichiedente.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@idCommittenteRichiedente", DbType.Int32,
                                                                domanda.IdCommittenteRichiedente.Value);
                                if (domanda.Importo.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@importo", DbType.Decimal,
                                                                domanda.Importo.Value);
                                if (!String.IsNullOrEmpty(domanda.Descrizione))
                                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String,
                                                                domanda.Descrizione);
                                if (domanda.PercentualeManodopera.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@percentualeManodopera", DbType.Decimal,
                                                                domanda.PercentualeManodopera.Value);
                                if (domanda.NumeroLavoratori.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@numeroLavoratori", DbType.Int32,
                                                                domanda.NumeroLavoratori.Value);
                                if (domanda.NumeroGiorni.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@numeroGiorni", DbType.Int32,
                                                                domanda.NumeroGiorni.Value);

                                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, domanda.DataInizio);
                                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, domanda.DataFine);

                                DatabaseCemi.AddInParameter(comando, "@mese", DbType.DateTime, domanda.Mese);

                                if (!String.IsNullOrEmpty(domanda.Dia))
                                    DatabaseCemi.AddInParameter(comando, "@numeroDIA", DbType.String, domanda.Dia);
                                if (!String.IsNullOrEmpty(domanda.Concessione))
                                    DatabaseCemi.AddInParameter(comando, "@numeroConcessione", DbType.String,
                                                                domanda.Concessione);
                                if (!String.IsNullOrEmpty(domanda.Autorizzazione))
                                    DatabaseCemi.AddInParameter(comando, "@numeroAutorizzazione", DbType.String,
                                                                domanda.Autorizzazione);

                                DatabaseCemi.AddInParameter(comando, "@richiestaVisitaIspettiva", DbType.Boolean,
                                                            domanda.RichiestaVisitaIspettiva);
                                if (!String.IsNullOrEmpty(domanda.OrarioVisitaIspettiva))
                                    DatabaseCemi.AddInParameter(comando, "@orarioVisitaIspettiva", DbType.String,
                                                                domanda.OrarioVisitaIspettiva);
                                if (!String.IsNullOrEmpty(domanda.MotivoVisitaIspettiva))
                                    DatabaseCemi.AddInParameter(comando, "@motivoVisitaIspettiva", DbType.String,
                                                                domanda.MotivoVisitaIspettiva);
                                if (!String.IsNullOrEmpty(domanda.ContattoTelefonicoVisitaIspettiva))
                                    DatabaseCemi.AddInParameter(comando, "@contattoTelefonicoVisitaIspettiva",
                                                                DbType.String, domanda.ContattoTelefonicoVisitaIspettiva);
                                if (domanda.DataVisitaIspettiva.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@dataVisitaIspettiva", DbType.DateTime,
                                                                domanda.DataVisitaIspettiva.Value);

                                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32,
                                                            domanda.IdUtente);
                                DatabaseCemi.AddInParameter(comando, "@guid", DbType.Guid,
                                                            domanda.GuidId);

                                DatabaseCemi.AddOutParameter(comando, "@idDomanda", DbType.Int32, 4);

                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    domanda.IdDomanda = (Int32) DatabaseCemi.GetParameterValue(comando, "@idDomanda");

                                    if (InsertCommittente(domanda, transaction))
                                    {
                                        if (InsertUpdateSubappaltieLavoratori(domanda, transaction))
                                        {
                                            res = true;
                                        }
                                        else
                                        {
                                            throw new Exception("Errore durante l'InsertUpdateSubappaltieLavoratori");
                                        }
                                    }
                                    else
                                        throw new Exception("Errore durante l'InsertCommittente");
                                }
                                else
                                {
                                    object pValue = DatabaseCemi.GetParameterValue(comando, "@idDomanda");
                                    if (pValue != null)
                                    {
                                        Int32 codiceErrore = (Int32) pValue;

                                        if (codiceErrore == -1)
                                        {
                                            throw new RichiestaGiaInseritaException();
                                        }
                                    }

                                    throw new Exception("Errore durante l'inserimento della domanda");
                                }

                                if (res)
                                {
                                    transaction.Commit();
                                }
                                else
                                {
                                    transaction.Rollback();
                                }
                            }
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                }
            }

            return res;
        }

        private bool InsertCommittente(Domanda domanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (domanda.Committente == null)
            {
                throw new ArgumentNullException("domanda.Committente");
            }
            else
            {
                using (
                    DbCommand comando =
                        databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaCommittenteInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                                domanda.Committente.RagioneSociale);
                    DatabaseCemi.AddInParameter(comando, "@tipologia", DbType.Int16,
                                                (Int16) domanda.Committente.Tipologia);
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, domanda.Committente.Indirizzo);
                    if (!String.IsNullOrEmpty(domanda.Committente.Civico))
                        DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, domanda.Committente.Civico);
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, domanda.Committente.Provincia);
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, domanda.Committente.Comune);
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, domanda.Committente.Cap);
                    DatabaseCemi.AddInParameter(comando, "@idAttestatoRegolaritaDomanda", DbType.Int32,
                                                domanda.IdDomanda.Value);
                    if (!String.IsNullOrEmpty(domanda.Committente.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                    domanda.Committente.CodiceFiscale);
                    if (!String.IsNullOrEmpty(domanda.Committente.PartitaIVA))
                        DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String,
                                                    domanda.Committente.PartitaIVA);

                    DatabaseCemi.AddOutParameter(comando, "@idCommittente", DbType.Int32, 4);

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        domanda.Committente.IdCommittente =
                            (Int32) DatabaseCemi.GetParameterValue(comando, "@idCommittente");
                        res = true;
                    }
                }
            }

            return res;
        }

        private Boolean UpdateCommittente(Committente committente, DbTransaction transaction)
        {
            //[dbo].[USP_AttestatoRegolaritaDomandaCommittenteUpdate]
            Boolean res = false;

            if (committente == null)
            {
                throw new ArgumentNullException("committente");
            }
            else
            {
                if (!committente.IdCommittente.HasValue)
                {
                    throw new ArgumentException("IdCommittente deve essere valorizato");
                }
                else
                {
                    using (
                        DbCommand comando =
                            databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaCommittenteUpdate"))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32,
                                                    committente.IdCommittente.Value);
                        DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                                    committente.RagioneSociale);
                        DatabaseCemi.AddInParameter(comando, "@tipologia", DbType.Int16,
                                                    (Int16) committente.Tipologia);
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                        if (!String.IsNullOrEmpty(committente.Civico))
                            DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, committente.Civico);
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                        if (!String.IsNullOrEmpty(committente.CodiceFiscale))
                            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                        committente.CodiceFiscale);
                        if (!String.IsNullOrEmpty(committente.PartitaIVA))
                            DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String,
                                                        committente.PartitaIVA);

                        if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        {
                            res = true;
                        }
                    }
                }
            }

            return res;
        }

        private Boolean InsertUpdateSubappaltieLavoratori(Domanda domanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (domanda == null)
            {
                throw new ArgumentNullException("domanda");
            }
            else
            {
                if (transaction == null)
                {
                    throw new ArgumentNullException("transaction");
                }
                else
                {
                    if (domanda.Subappalti == null)
                    {
                        throw new ArgumentException("Subappalti deve essere valorizzato");
                    }
                    else
                    {
                        if (domanda.Lavoratori == null)
                        {
                            throw new ArgumentException("Lavoratori deve essere valorizzato");
                        }
                        else
                        {
                            // Subappalti
                            res = InsertUpdateSubappalti(domanda.Subappalti, domanda.IdDomanda.Value, transaction);

                            GestisciImpreseConIdTemporaneiLavoratori(domanda.Subappalti, domanda.Lavoratori);

                            if (res)
                            {
                                // Lavoratori
                                res = InsertUpdateLavoratori(domanda.Lavoratori, domanda.IdDomanda.Value, transaction);
                            }
                        }
                    }
                }
            }

            return res;
        }

        private Boolean InsertUpdateLavoratori(DomandaImpresaCollection lavoratori, Int32 idDomanda,
                                               DbTransaction transaction)
        {
            Boolean res = true;

            foreach (DomandaImpresa domandaImpresa in lavoratori)
            {
                if (domandaImpresa.Impresa == null) //|| !domandaImpresa.Impresa.IdImpresa.HasValue)
                {
                    throw new ArgumentException("L'impresa deve essere valorizzata e avere un Id");
                }
                else
                {
                    if (domandaImpresa.Lavoratori == null)
                    {
                        throw new Exception("Lavoratori deve essere valorizzato");
                    }
                    else
                    {
                        foreach (Lavoratore lavoratore in domandaImpresa.Lavoratori)
                        {
                            if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew
                                && !lavoratore.IdLavoratore.HasValue)
                            {
                                throw new ArgumentException(
                                    String.Format("I lavoratori dell'anagrafica devono avere obbligatoriamente un Id"));
                            }
                            else
                            {
                                //if (!lavoratore.IdDomandaLavoratore.HasValue)
                                //{
                                if (
                                    !InsertLavoratoreImpresa(lavoratore, idDomanda, domandaImpresa.Impresa,
                                                             transaction))
                                {
                                    res = false;
                                    break;
                                }
                                //}
                            }
                        }
                    }
                }
            }

            return res;
        }

        private Boolean InsertUpdateSubappalti(SubappaltoCollection subappalti, Int32 idDomanda,
                                               DbTransaction transaction)
        {
            Boolean res = true;

            for (Int32 i = 0; i < subappalti.Count; i++)
            {
                Subappalto subappalto = subappalti[i];

                if (subappalto.Appaltata == null)
                {
                    throw new ArgumentNullException("subappalto.Appaltata");
                }
                else
                {
                    if ((subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew
                         && !subappalto.Appaltata.IdImpresa.HasValue)
                        ||
                        (subappalto.Appaltante != null
                         && subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew
                         && !subappalto.Appaltante.IdImpresa.HasValue))
                    {
                        throw new ArgumentException(
                            "Le imprese provenienti dall'anagrafica devono avere obbligatoriamente un Id");
                    }
                    else
                    {
                        //if (!subappalto.IdSubappalto.HasValue)
                        //{
                        if (!InsertSubappalto(subappalto, idDomanda, transaction))
                        {
                            res = false;
                            break;
                        }
                        //}
                        //else
                        //{
                        //    if (!UpdateSubappalto(subappalto, transaction))
                        //    {
                        //        res = false;
                        //        break;
                        //    }
                        //}

                        GestisciImpreseConIdTemporanei(subappalti, i);
                    }
                }
            }

            return res;
        }

        private void GestisciImpreseConIdTemporanei(SubappaltoCollection subappalti, Int32 indice)
        {
            // Ciclo sull'appaltata
            if (subappalti[indice].Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
            {
                for (Int32 k = indice + 1; k < subappalti.Count; k++)
                {
                    if (subappalti[k].Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltata.IdImpresa.HasValue
                        && subappalti[k].Appaltata.IdTemporaneo == subappalti[indice].Appaltata.IdTemporaneo)
                    {
                        subappalti[k].Appaltata.IdImpresa = subappalti[indice].Appaltata.IdImpresa;
                    }

                    if (subappalti[k].Appaltante != null
                        && subappalti[k].Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltante.IdImpresa.HasValue
                        && subappalti[k].Appaltante.IdTemporaneo == subappalti[indice].Appaltata.IdTemporaneo)
                    {
                        subappalti[k].Appaltante.IdImpresa = subappalti[indice].Appaltata.IdImpresa;
                    }
                }
            }

            // Ciclo sull'appaltante
            if (subappalti[indice].Appaltante != null &&
                subappalti[indice].Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
            {
                for (Int32 k = indice + 1; k < subappalti.Count; k++)
                {
                    if (subappalti[k].Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltata.IdImpresa.HasValue
                        && subappalti[k].Appaltata.IdTemporaneo == subappalti[indice].Appaltante.IdTemporaneo)
                    {
                        subappalti[k].Appaltata.IdImpresa = subappalti[indice].Appaltante.IdImpresa;
                    }

                    if (subappalti[k].Appaltante != null
                        && subappalti[k].Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                        && !subappalti[k].Appaltante.IdImpresa.HasValue
                        && subappalti[k].Appaltante.IdTemporaneo == subappalti[indice].Appaltante.IdTemporaneo)
                    {
                        subappalti[k].Appaltante.IdImpresa = subappalti[indice].Appaltante.IdImpresa;
                    }
                }
            }
        }

        private void GestisciImpreseConIdTemporaneiLavoratori(SubappaltoCollection subappalti,
                                                              DomandaImpresaCollection lavoratori)
        {
            foreach (Subappalto sub in subappalti)
            {
                // Ciclo sull'appaltata
                if (sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                {
                    foreach (DomandaImpresa domImp in lavoratori)
                    {
                        if (domImp.Impresa.TipoImpresa == TipologiaImpresa.Nuova
                            && !domImp.Impresa.IdImpresa.HasValue
                            && domImp.Impresa.IdTemporaneo == sub.Appaltata.IdTemporaneo)
                        {
                            domImp.Impresa.IdImpresa = sub.Appaltata.IdImpresa;
                        }
                    }
                }

                // Ciclo sull'appaltante
                if (sub.Appaltante != null && sub.Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
                {
                    foreach (DomandaImpresa domImp in lavoratori)
                    {
                        if (domImp.Impresa.TipoImpresa == TipologiaImpresa.Nuova
                            && !domImp.Impresa.IdImpresa.HasValue
                            && domImp.Impresa.IdTemporaneo == sub.Appaltante.IdTemporaneo)
                        {
                            domImp.Impresa.IdImpresa = sub.Appaltante.IdImpresa;
                        }
                    }
                }
            }
        }

        //private Boolean UpdateSubappalto(Subappalto subappalto, DbTransaction transaction)
        //{
        //    Boolean res = false;

        //    if (GestioneImprese(subappalto, transaction))
        //    {
        //        // Inserimento del subappalto

        //        using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaSubappaltiUpdate"))
        //        {
        //            databaseCemi.AddInParameter(comando, "@idSubappalto", DbType.Int32, subappalto.IdSubappalto.Value);
        //            if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
        //            {
        //                databaseCemi.AddInParameter(comando, "@idImpresaAppaltata", DbType.Int32, subappalto.Appaltata.IdImpresa.Value);
        //            }
        //            else
        //            {
        //                if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
        //                {
        //                    databaseCemi.AddInParameter(comando, "@idAttestatoImpresaAppaltata", DbType.Int32, subappalto.Appaltata.IdImpresa.Value);
        //                }
        //            }
        //            if (subappalto.Appaltante != null)
        //            {
        //                if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
        //                {
        //                    databaseCemi.AddInParameter(comando, "@idImpresaAppaltante", DbType.Int32, subappalto.Appaltante.IdImpresa.Value);
        //                }
        //                else
        //                {
        //                    if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
        //                    {
        //                        databaseCemi.AddInParameter(comando, "@idAttestatoImpresaAppaltante", DbType.Int32, subappalto.Appaltante.IdImpresa.Value);
        //                    }
        //                }
        //            }

        //            if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
        //            {
        //                res = true;
        //            }
        //            else
        //            {
        //                throw new Exception(String.Format("Errore durante l'aggiornamento del subappalto {0}", subappalto.IdSubappalto));
        //            }
        //        }
        //    }

        //    return res;
        //}

        private Boolean InsertSubappalto(Subappalto subappalto, Int32 idDomanda, DbTransaction transaction)
        {
            Boolean res = false;

            if (GestioneImprese(subappalto, transaction))
            {
                // Inserimento del subappalto

                using (
                    DbCommand comando =
                        databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaSubappaltiInsert"))
                {
                    databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                    if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        databaseCemi.AddInParameter(comando, "@idImpresaAppaltata", DbType.Int32,
                                                    subappalto.Appaltata.IdImpresa.Value);
                    }
                    else
                    {
                        if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                        {
                            databaseCemi.AddInParameter(comando, "@idAttestatoImpresaAppaltata", DbType.Int32,
                                                        subappalto.Appaltata.IdImpresa.Value);
                        }
                    }
                    if (subappalto.Appaltante != null)
                    {
                        if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                        {
                            databaseCemi.AddInParameter(comando, "@idImpresaAppaltante", DbType.Int32,
                                                        subappalto.Appaltante.IdImpresa.Value);
                        }
                        else
                        {
                            if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.Nuova)
                            {
                                databaseCemi.AddInParameter(comando, "@idAttestatoImpresaAppaltante", DbType.Int32,
                                                            subappalto.Appaltante.IdImpresa.Value);
                            }
                        }
                    }
                    databaseCemi.AddOutParameter(comando, "@idSubappalto", DbType.Int32, 4);

                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        subappalto.IdSubappalto = (Int32) databaseCemi.GetParameterValue(comando, "@idSubappalto");
                        res = true;
                    }
                    else
                    {
                        throw new Exception("Errore durante l'inserimento del subappalto");
                    }
                }
            }

            return res;
        }

        private bool InsertLavoratoreImpresa(Lavoratore lavoratore, Int32 idDomanda, Impresa impresa,
                                             DbTransaction transaction)
        {
            Boolean res = false;

            if (GestioneLavoratori(lavoratore, transaction))
            {
                // Inserimento del rapporto lavoratore impresa

                using (
                    DbCommand comando =
                        databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaLavoratoriInsert"))
                {
                    databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                    if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        databaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                    }
                    else
                    {
                        if (impresa.TipoImpresa == TipologiaImpresa.Nuova)
                        {
                            // Valla: problemi con nuovo lavoratore di nuova impresa... idImpresa � ancora a null
                            databaseCemi.AddInParameter(comando, "@idAttestatoImpresa", DbType.Int32,
                                                        impresa.IdImpresa.Value);
                        }
                    }
                    if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                    {
                        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                    lavoratore.IdLavoratore.Value);
                    }
                    else
                    {
                        if (lavoratore.TipoLavoratore == TipologiaLavoratore.Nuovo)
                        {
                            databaseCemi.AddInParameter(comando, "@idAttestatoLavoratore", DbType.Int32,
                                                        lavoratore.IdLavoratore.Value);
                        }
                    }
                    databaseCemi.AddOutParameter(comando, "@idDomandaLavoratore", DbType.Int32, 4);

                    if (databaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        lavoratore.IdDomandaLavoratore =
                            (Int32) databaseCemi.GetParameterValue(comando, "@idDomandaLavoratore");
                        res = true;
                    }
                    else
                    {
                        throw new Exception("Errore durante l'inserimento del lavoratore associato all'impresa");
                    }
                }
            }

            return res;
        }

        private bool GestioneLavoratori(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore.TipoLavoratore == TipologiaLavoratore.Nuovo
                && !lavoratore.IdLavoratore.HasValue)
            {
                // Devo inserire il lavoratore

                res = InsertLavoratore(lavoratore, transaction);
            }
            else
                res = true;

            return res;
        }

        private Boolean GestioneImprese(Subappalto subappalto, DbTransaction transaction)
        {
            Boolean res = true;

            if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                && !subappalto.Appaltata.IdImpresa.HasValue)
            {
                // Devo inserire l'impresa
                res = InsertImpresa(subappalto.Appaltata, transaction);

                if (!res)
                {
                    res = false;
                    throw new Exception(String.Format("Errore durante l'inserimento dell'impresa {0}",
                                                      subappalto.Appaltata.RagioneSociale));
                }
            }

            if (subappalto.Appaltante != null)
            {
                // Ho anche l'impresa appaltante

                // Inserimento
                if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                    && !subappalto.Appaltante.IdImpresa.HasValue)
                {
                    res = InsertImpresa(subappalto.Appaltante, transaction);

                    if (!res)
                    {
                        res = false;
                        throw new Exception(String.Format("Errore durante l'inserimento dell'impresa {0}",
                                                          subappalto.Appaltante.RagioneSociale));
                    }
                }
            }

            return res;
        }

        public Int32 GetMesiAvvioControlli()
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectAttestatoRegolaritaMesiAvvioControlli"))
            {
                DatabaseCemi.AddOutParameter(comando, "@mesiAvvioControlli", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                return (Int32) DatabaseCemi.GetParameterValue(comando, "@mesiAvvioControlli");
            }

            return -1;
        }

        private void DeleteSubappaltiDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaSubappaltiDeleteByIdDomanda"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        private void DeleteLavoratoriDellaDomanda(Int32 idDomanda, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandaLavoratoriDeleteByIdDomanda"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        public void EffettuaControlli()
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaDomandeEffettuaControlli"))
            {
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public Domanda GetDomandaControlli(Int32 idDomanda)
        {
            Domanda domanda = null;

            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaReportDomandaSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Domanda

                    #region Indici per reader

                    Int32 indiceIdDomanda = reader.GetOrdinal("idAttestatoRegolaritaDomanda");
                    Int32 indiceDataControllo = reader.GetOrdinal("dataControllo");
                    Int32 indiceControlloTutteImpreseAnagrafica = reader.GetOrdinal("controlloTutteImpreseAnagrafica");
                    Int32 indiceControlloTutteImpreseNoCessate = reader.GetOrdinal("controlloTutteImpreseNoCessate");
                    Int32 indiceControlloTutteImpreseIscrittePrecedentemente =
                        reader.GetOrdinal("controlloTutteImpreseIscrittePrecedentemente");
                    Int32 indiceControlloTutteImpreseSospese = reader.GetOrdinal("controlloTutteImpreseSospese");
                    Int32 indiceControlloTutteImpreseBNI = reader.GetOrdinal("controlloTutteImpreseBNI");
                    Int32 indiceControlloTutteImpreseNonEdiliNoAnagrafica =
                        reader.GetOrdinal("controlloTutteImpreseNonEdiliNoAnagrafica");
                    Int32 indiceControlloTuttiLavoratoriAnagrafica =
                        reader.GetOrdinal("controlloTuttiLavoratoriAnagrafica");
                    Int32 indiceControlloTuttiLavoratoriConRapporto =
                        reader.GetOrdinal("controlloTuttiLavoratoriConRapporto");
                    Int32 indiceControlloTuttiLavoratoriOreLavorate =
                        reader.GetOrdinal("controlloTuttiLavoratoriOreLavorate");
                    Int32 indiceControlloTuttiLavoratoriPartTime = reader.GetOrdinal("controlloTuttiLavoratoriPartTime");
                    Int32 indiceControlloAttestatoRilasciabile = reader.GetOrdinal("controlloAttestatoRilasciabile");
                    Int32 indiceSegnalazioneCantiereInNotifiche = reader.GetOrdinal("segnalazioneCantiereInNotifiche");

                    #endregion

                    if (reader.Read())
                    {
                        domanda = new Domanda();

                        domanda.IdDomanda = reader.GetInt32(indiceIdDomanda);

                        if (!reader.IsDBNull(indiceDataControllo))
                            domanda.DataControllo = reader.GetDateTime(indiceDataControllo);

                        if (!reader.IsDBNull(indiceControlloTutteImpreseAnagrafica))
                            domanda.ControlloTutteImpreseAnagrafica =
                                reader.GetBoolean(indiceControlloTutteImpreseAnagrafica);

                        if (!reader.IsDBNull(indiceControlloTutteImpreseNoCessate))
                            domanda.ControlloTutteImpreseNoCessate =
                                reader.GetBoolean(indiceControlloTutteImpreseNoCessate);

                        if (!reader.IsDBNull(indiceControlloTutteImpreseIscrittePrecedentemente))
                            domanda.ControlloTutteImpreseIscrittePrecedentemente =
                                reader.GetBoolean(indiceControlloTutteImpreseIscrittePrecedentemente);

                        if (!reader.IsDBNull(indiceControlloTutteImpreseSospese))
                            domanda.ControlloTutteImpreseSospese =
                                reader.GetBoolean(indiceControlloTutteImpreseSospese);

                        if (!reader.IsDBNull(indiceControlloTutteImpreseBNI))
                            domanda.ControlloTutteImpreseBNI =
                                reader.GetBoolean(indiceControlloTutteImpreseBNI);

                        if (!reader.IsDBNull(indiceControlloTutteImpreseNonEdiliNoAnagrafica))
                            domanda.ControlloTutteImpreseNonEdiliNoAnagrafica =
                                reader.GetBoolean(indiceControlloTutteImpreseNonEdiliNoAnagrafica);

                        if (!reader.IsDBNull(indiceControlloTuttiLavoratoriAnagrafica))
                            domanda.ControlloTuttiLavoratoriAnagrafica =
                                reader.GetBoolean(indiceControlloTuttiLavoratoriAnagrafica);

                        if (!reader.IsDBNull(indiceControlloTuttiLavoratoriConRapporto))
                            domanda.ControlloTuttiLavoratoriConRapporto =
                                reader.GetBoolean(indiceControlloTuttiLavoratoriConRapporto);

                        if (!reader.IsDBNull(indiceControlloTuttiLavoratoriOreLavorate))
                            domanda.ControlloTuttiLavoratoriOreLavorate =
                                reader.GetBoolean(indiceControlloTuttiLavoratoriOreLavorate);

                        if (!reader.IsDBNull(indiceControlloTuttiLavoratoriPartTime))
                            domanda.ControlloTuttiLavoratoriPartTime =
                                reader.GetBoolean(indiceControlloTuttiLavoratoriPartTime);

                        if (!reader.IsDBNull(indiceControlloAttestatoRilasciabile))
                            domanda.ControlloAttestatoRilasciabile =
                                reader.GetBoolean(indiceControlloAttestatoRilasciabile);

                        if (!reader.IsDBNull(indiceSegnalazioneCantiereInNotifiche))
                            domanda.SegnalazioneCantiereInNotifiche =
                                reader.GetBoolean(indiceSegnalazioneCantiereInNotifiche);
                    }

                    #endregion

                    reader.NextResult();
                }
            }

            return domanda;
        }

        public ImpresaCollection GetImpreseEdiliControlli(Int32 idDomanda)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaReportDomandaImpreseSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("atIdImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atRagioneSociale");

                    Int32 indiceControlloCessataAttivita = reader.GetOrdinal("controlloCessataAttivita");
                    Int32 indiceControlloBNI = reader.GetOrdinal("controlloBNI");
                    Int32 indiceControlloIscrittaPrecedentemente = reader.GetOrdinal("controlloIscrittaPrecedentemente");
                    Int32 indiceControlloNonEdileNoAnagrafica = reader.GetOrdinal("controlloNonEdileNoAnagrafica");
                    Int32 indiceControlloSospesa = reader.GetOrdinal("controlloSospesa");
                    Int32 indiceIdImpresaAssociata = reader.GetOrdinal("idImpresaAssociata");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            // Impresa SiceNew

                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);

                            if (!reader.IsDBNull(indiceControlloCessataAttivita))
                                impresa.ControlloCessataAttivita = reader.GetBoolean(indiceControlloCessataAttivita);
                            if (!reader.IsDBNull(indiceControlloIscrittaPrecedentemente))
                                impresa.ControlloIscrittaPrecedentemente =
                                    reader.GetBoolean(indiceControlloIscrittaPrecedentemente);
                            if (!reader.IsDBNull(indiceControlloNonEdileNoAnagrafica))
                                impresa.ControlloNonEdileNoAnagrafica =
                                    reader.GetBoolean(indiceControlloNonEdileNoAnagrafica);
                            if (!reader.IsDBNull(indiceControlloSospesa))
                                impresa.ControlloSospesa = reader.GetBoolean(indiceControlloSospesa);
                            if (!reader.IsDBNull(indiceControlloBNI))
                                impresa.ControlloBNI = reader.GetBoolean(indiceControlloBNI);
                        }
                        else
                        {
                            // Nuova Impresa

                            impresa.TipoImpresa = TipologiaImpresa.Nuova;
                            impresa.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);

                            if (!reader.IsDBNull(indiceControlloCessataAttivita))
                                impresa.ControlloCessataAttivita = reader.GetBoolean(indiceControlloCessataAttivita);
                            if (!reader.IsDBNull(indiceControlloIscrittaPrecedentemente))
                                impresa.ControlloIscrittaPrecedentemente =
                                    reader.GetBoolean(indiceControlloIscrittaPrecedentemente);
                            if (!reader.IsDBNull(indiceControlloNonEdileNoAnagrafica))
                                impresa.ControlloNonEdileNoAnagrafica =
                                    reader.GetBoolean(indiceControlloNonEdileNoAnagrafica);
                            if (!reader.IsDBNull(indiceControlloSospesa))
                                impresa.ControlloSospesa = reader.GetBoolean(indiceControlloSospesa);
                            if (!reader.IsDBNull(indiceControlloBNI))
                                impresa.ControlloBNI = reader.GetBoolean(indiceControlloBNI);
                            if (!reader.IsDBNull(indiceIdImpresaAssociata))
                                impresa.IdImpresaTrovata = reader.GetInt32(indiceIdImpresaAssociata);
                        }
                    }
                }
            }

            return imprese;
        }

        public ImpresaCollection GetImpreseNonEdiliControlli(Int32 idDomanda)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaReportDomandaImpreseNonEdiliSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    Int32 indiceIdAttestatoImpresa = reader.GetOrdinal("atIdImpresa");
                    Int32 indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtRagioneSociale = reader.GetOrdinal("atRagioneSociale");

                    Int32 indiceControlloCessataAttivita = reader.GetOrdinal("controlloCessataAttivita");
                    Int32 indiceControlloBNI = reader.GetOrdinal("controlloBNI");
                    Int32 indiceControlloIscrittaPrecedentemente = reader.GetOrdinal("controlloIscrittaPrecedentemente");
                    Int32 indiceControlloNonEdileNoAnagrafica = reader.GetOrdinal("controlloNonEdileNoAnagrafica");
                    Int32 indiceControlloSospesa = reader.GetOrdinal("controlloSospesa");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            // Impresa SiceNew

                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);

                            if (!reader.IsDBNull(indiceControlloCessataAttivita))
                                impresa.ControlloCessataAttivita = reader.GetBoolean(indiceControlloCessataAttivita);
                            if (!reader.IsDBNull(indiceControlloIscrittaPrecedentemente))
                                impresa.ControlloIscrittaPrecedentemente =
                                    reader.GetBoolean(indiceControlloIscrittaPrecedentemente);
                            if (!reader.IsDBNull(indiceControlloNonEdileNoAnagrafica))
                                impresa.ControlloNonEdileNoAnagrafica =
                                    reader.GetBoolean(indiceControlloNonEdileNoAnagrafica);
                            if (!reader.IsDBNull(indiceControlloSospesa))
                                impresa.ControlloSospesa = reader.GetBoolean(indiceControlloSospesa);
                            if (!reader.IsDBNull(indiceControlloBNI))
                                impresa.ControlloBNI = reader.GetBoolean(indiceControlloBNI);
                        }
                        else
                        {
                            // Nuova Impresa

                            impresa.TipoImpresa = TipologiaImpresa.Nuova;
                            impresa.IdImpresa = reader.GetInt32(indiceIdAttestatoImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceAtRagioneSociale);

                            if (!reader.IsDBNull(indiceControlloCessataAttivita))
                                impresa.ControlloCessataAttivita = reader.GetBoolean(indiceControlloCessataAttivita);
                            if (!reader.IsDBNull(indiceControlloIscrittaPrecedentemente))
                                impresa.ControlloIscrittaPrecedentemente =
                                    reader.GetBoolean(indiceControlloIscrittaPrecedentemente);
                            if (!reader.IsDBNull(indiceControlloNonEdileNoAnagrafica))
                                impresa.ControlloNonEdileNoAnagrafica =
                                    reader.GetBoolean(indiceControlloNonEdileNoAnagrafica);
                            if (!reader.IsDBNull(indiceControlloSospesa))
                                impresa.ControlloSospesa = reader.GetBoolean(indiceControlloSospesa);
                            if (!reader.IsDBNull(indiceControlloBNI))
                                impresa.ControlloBNI = reader.GetBoolean(indiceControlloBNI);
                        }
                    }
                }
            }

            return imprese;
        }

        public LavoratoreCollection GetLavoratoriControlli(Int32 idDomanda)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaReportDomandaLavoratoriSelect"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    Int32 indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    Int32 indiceIdAttestatoRegolaritaLavoratore = reader.GetOrdinal("idAttestatoRegolaritaLavoratore");
                    Int32 indiceNome = reader.GetOrdinal("nome");
                    Int32 indiceAtNome = reader.GetOrdinal("atNome");
                    Int32 indiceCognome = reader.GetOrdinal("cognome");
                    Int32 indiceAtCognome = reader.GetOrdinal("atCognome");

                    // Impresa
                    Int32 indiceImpresa = reader.GetOrdinal("ragioneSociale");
                    Int32 indiceAtImpresa = reader.GetOrdinal("atRagioneSociale");
                    Int32 indiceTipoContratto = reader.GetOrdinal("tipologiaContratto");
                    Int32 indiceAtTipoContratto = reader.GetOrdinal("atTipologiaContratto");
                    Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");

                    Int32 indiceControlloOreLavorate = reader.GetOrdinal("controlloOreLavorate");
                    Int32 indiceControlloPartTime = reader.GetOrdinal("controlloPartTime");
                    Int32 indiceControlloPresenzaRapporto = reader.GetOrdinal("controlloPresenzaRapporto");
                    Int32 indiceIdLavoratoreAssociato = reader.GetOrdinal("idLavoratoreAssociato");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.IdDomandaLavoratore = reader.GetInt32(indiceIdDomandaLavoratore);
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                            if (!reader.IsDBNull(indiceCognome))
                                lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                                lavoratore.Nome = reader.GetString(indiceNome);

                            if (!reader.IsDBNull(indiceImpresa))
                            {
                                lavoratore.Impresa = reader.GetString(indiceImpresa);
                                if (!reader.IsDBNull(indiceIdImpresa))
                                {
                                    lavoratore.IdImpresa = reader.GetInt32(indiceIdImpresa);
                                }
                            }
                            else
                            {
                                if (!reader.IsDBNull(indiceAtImpresa))
                                {
                                    lavoratore.Impresa = reader.GetString(indiceAtImpresa);
                                }
                            }

                            if (!reader.IsDBNull(indiceControlloPresenzaRapporto))
                                lavoratore.ControlloPresenzaRapporto = reader.GetBoolean(indiceControlloPresenzaRapporto);
                            if (!reader.IsDBNull(indiceControlloPartTime))
                                lavoratore.ControlloPartTime = reader.GetBoolean(indiceControlloPartTime);
                            if (!reader.IsDBNull(indiceControlloOreLavorate))
                                lavoratore.ControlloOreLavorate = reader.GetBoolean(indiceControlloOreLavorate);
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Nuovo;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdAttestatoRegolaritaLavoratore);


                            if (!reader.IsDBNull(indiceAtCognome))
                                lavoratore.Cognome = reader.GetString(indiceAtCognome);
                            if (!reader.IsDBNull(indiceAtNome))
                                lavoratore.Nome = reader.GetString(indiceAtNome);

                            if (!reader.IsDBNull(indiceImpresa))
                            {
                                lavoratore.Impresa = reader.GetString(indiceImpresa);
                                if (!reader.IsDBNull(indiceIdImpresa))
                                {
                                    lavoratore.IdImpresa = reader.GetInt32(indiceIdImpresa);
                                }
                                lavoratore.TipoContrattoImpresa =
                                    (TipologiaContratto) reader.GetInt16(indiceTipoContratto);
                            }
                            else
                            {
                                if (!reader.IsDBNull(indiceAtImpresa))
                                {
                                    lavoratore.Impresa = reader.GetString(indiceAtImpresa);
                                    lavoratore.TipoContrattoImpresa =
                                        (TipologiaContratto) reader.GetInt16(indiceAtTipoContratto);
                                }
                            }

                            if (!reader.IsDBNull(indiceControlloPresenzaRapporto))
                                lavoratore.ControlloPresenzaRapporto = reader.GetBoolean(indiceControlloPresenzaRapporto);
                            if (!reader.IsDBNull(indiceControlloPartTime))
                                lavoratore.ControlloPartTime = reader.GetBoolean(indiceControlloPartTime);
                            if (!reader.IsDBNull(indiceControlloOreLavorate))
                                lavoratore.ControlloOreLavorate = reader.GetBoolean(indiceControlloOreLavorate);
                            if (!reader.IsDBNull(indiceIdLavoratoreAssociato))
                                lavoratore.IdLavoratoreTrovato = reader.GetInt32(indiceIdLavoratoreAssociato);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public void LogSelezioneImpresaSubappalto(int idUtente, Subappalto subappalto)
        {
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaLogSubappaltiInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                if (subappalto.Appaltante != null && subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltante", DbType.Int32,
                                                subappalto.Appaltante.IdImpresa.Value);
                }
                if (subappalto.Appaltata != null && subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltata", DbType.Int32,
                                                subappalto.Appaltata.IdImpresa.Value);
                }

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        #region Impresa

        public Boolean InsertImpresa(Impresa impresa, DbTransaction transaction)
        {
            Boolean res = false;

            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            else
            {
                if (impresa.TipoImpresa != TipologiaImpresa.Nuova)
                {
                    throw new ArgumentException("Possono essere inserite solo imprese di tipo Nuova");
                }
                else
                {
                    if (impresa.IdImpresa.HasValue)
                    {
                        throw new ArgumentException("L'impresa che si sta tentando di inserire ha gi� un Id");
                    }
                    else
                    {
                        using (
                            DbCommand comando =
                                databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaImpreseInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                                        impresa.RagioneSociale);
                            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                            if (!String.IsNullOrEmpty(impresa.PartitaIva))
                                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                            if (!String.IsNullOrEmpty(impresa.Indirizzo))
                                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                            if (!String.IsNullOrEmpty(impresa.Provincia))
                                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                            if (!String.IsNullOrEmpty(impresa.Cap))
                                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                            if (!String.IsNullOrEmpty(impresa.IdCassaEdileProvenienza))
                                DatabaseCemi.AddInParameter(comando, "@idCassaEdileProvenienza", DbType.String,
                                                            impresa.IdCassaEdileProvenienza);
                            DatabaseCemi.AddInParameter(comando, "@tipologiaContratto", DbType.Int16,
                                                        impresa.TipologiaContratto);
                            DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean,
                                                        impresa.LavoratoreAutonomo);
                            if (!String.IsNullOrEmpty(impresa.Cognome))
                                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, impresa.Cognome);
                            if (!String.IsNullOrEmpty(impresa.Nome))
                                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, impresa.Nome);
                            if (impresa.DataNascita.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                            impresa.DataNascita);
                            if (!String.IsNullOrEmpty(impresa.NumeroCameraCommercio))
                                DatabaseCemi.AddInParameter(comando, "@numeroCameraCommercio", DbType.String,
                                                            impresa.NumeroCameraCommercio);

                            DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                            if (transaction != null)
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    impresa.IdImpresa = (Int32) DatabaseCemi.GetParameterValue(comando, "@idImpresa");
                                    res = true;
                                }
                            }
                            else
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                                {
                                    impresa.IdImpresa = (Int32) DatabaseCemi.GetParameterValue(comando, "@idImpresa");
                                    res = true;
                                }
                            }
                        }
                    }
                }
            }

            return res;
        }

        public Boolean UpdateImpresa(Impresa impresa, DbTransaction transaction)
        {
            Boolean res = false;

            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            else
            {
                if (impresa.TipoImpresa != TipologiaImpresa.Nuova)
                {
                    throw new ArgumentException("Possono essere aggiornate solo imprese di tipo Nuova");
                }
                else
                {
                    if (!impresa.IdImpresa.HasValue)
                    {
                        throw new ArgumentException("L'impresa che si sta tentando di inserire non ha un Id");
                    }
                    else
                    {
                        using (
                            DbCommand comando =
                                databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaImpreseUpdate"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                            DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String,
                                                        impresa.RagioneSociale);
                            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                            if (!String.IsNullOrEmpty(impresa.PartitaIva))
                                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                            if (!String.IsNullOrEmpty(impresa.Indirizzo))
                                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                            if (!String.IsNullOrEmpty(impresa.Provincia))
                                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                            if (!String.IsNullOrEmpty(impresa.Cap))
                                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                            if (!String.IsNullOrEmpty(impresa.IdCassaEdileProvenienza))
                                DatabaseCemi.AddInParameter(comando, "@idCassaEdileProvenienza", DbType.String,
                                                            impresa.IdCassaEdileProvenienza);
                            DatabaseCemi.AddInParameter(comando, "@tipologiaContratto", DbType.Int16,
                                                        impresa.TipologiaContratto);
                            DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean,
                                                        impresa.LavoratoreAutonomo);
                            if (!String.IsNullOrEmpty(impresa.Cognome))
                                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, impresa.Cognome);
                            if (!String.IsNullOrEmpty(impresa.Nome))
                                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, impresa.Nome);
                            if (impresa.DataNascita.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                            impresa.DataNascita);
                            if (!String.IsNullOrEmpty(impresa.NumeroCameraCommercio))
                                DatabaseCemi.AddInParameter(comando, "@numeroCameraCommercio", DbType.String,
                                                            impresa.NumeroCameraCommercio);

                            DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                            if (transaction != null)
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    res = true;
                                }
                            }
                            else
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                                {
                                    res = true;
                                }
                            }
                        }
                    }
                }
            }

            return res;
        }

        #endregion

        #region Lavoratore

        public Boolean InsertLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            else
            {
                if (lavoratore.TipoLavoratore != TipologiaLavoratore.Nuovo)
                {
                    throw new ArgumentException("Possono essere inserite solo lavoratori di tipo Nuovo");
                }
                else
                {
                    if (lavoratore.IdLavoratore.HasValue)
                    {
                        throw new ArgumentException("Il lavoratore che si sta tentando di inserire ha gi� un Id");
                    }
                    else
                    {
                        using (
                            DbCommand comando =
                                databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaLavoratoriInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                            DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                            DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                        lavoratore.CodiceFiscale);
                            if (lavoratore.TipoAssunzione.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@tipoAssunzione", DbType.Int32,
                                                            lavoratore.TipoAssunzione.Value);
                            if (lavoratore.DataAssunzione.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                                                            lavoratore.DataAssunzione.Value);
                            if (lavoratore.DataCessazione.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime,
                                                            lavoratore.DataCessazione.Value);
                            else
                                DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime,
                                                            new DateTime(2079, 6, 6));
                            //if (!String.IsNullOrEmpty(lavoratore.IdCassaEdileProvenienza))
                            //    DatabaseCemi.AddInParameter(comando, "@idCassaEdileProvenienza", DbType.String, lavoratore.IdCassaEdileProvenienza);

                            DatabaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);

                            if (transaction != null)
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    lavoratore.IdLavoratore =
                                        (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratore");
                                    res = true;
                                }
                            }
                            else
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                                {
                                    lavoratore.IdLavoratore =
                                        (Int32) DatabaseCemi.GetParameterValue(comando, "@idLavoratore");
                                    res = true;
                                }
                            }
                        }
                    }
                }
            }

            return res;
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIVA, string codiceFiscale)
        {
            bool[] res = new bool[4];

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CptEsisteImpresaConIvaFisc"))
            {
                if (!string.IsNullOrEmpty(partitaIVA))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, partitaIVA);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                databaseCemi.AddOutParameter(comando, "@impresaIva", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@cantieriImpresaIva", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@impresaFisc", DbType.Boolean, 1);
                databaseCemi.AddOutParameter(comando, "@cantieriImpresaFisc", DbType.Boolean, 1);
                databaseCemi.ExecuteNonQuery(comando);

                res[0] = (bool) comando.Parameters["@impresaIva"].Value;
                res[1] = (bool) comando.Parameters["@cantieriImpresaIva"].Value;
                res[2] = (bool) comando.Parameters["@impresaFisc"].Value;
                res[3] = (bool) comando.Parameters["@cantieriImpresaFisc"].Value;
            }

            return res;
        }

        public Boolean UpdateLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            Boolean res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            else
            {
                if (lavoratore.TipoLavoratore != TipologiaLavoratore.Nuovo)
                {
                    throw new ArgumentException("Possono essere aggiornati solo lavoratori di tipo Nuovo");
                }
                else
                {
                    if (!lavoratore.IdLavoratore.HasValue)
                    {
                        throw new ArgumentException("Il lavoratore che si sta tentando di aggiornare non ha un Id");
                    }
                    else
                    {
                        using (
                            DbCommand comando =
                                databaseCemi.GetStoredProcCommand("dbo.USP_AttestatoRegolaritaLavoratoriUpdate"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                                        lavoratore.IdLavoratore.Value);
                            DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                            DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                            DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                        lavoratore.CodiceFiscale);
                            if (lavoratore.TipoAssunzione.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@tipoAssunzione", DbType.Int32,
                                                            lavoratore.TipoAssunzione.Value);
                            if (lavoratore.DataAssunzione.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.Int32,
                                                            lavoratore.DataAssunzione.Value);
                            if (lavoratore.DataCessazione.HasValue)
                                DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.Int32,
                                                            lavoratore.DataCessazione.Value);
                            //if (!String.IsNullOrEmpty(lavoratore.IdCassaEdileProvenienza))
                            //    DatabaseCemi.AddInParameter(comando, "@idCassaEdileProvenienza", DbType.String, lavoratore.IdCassaEdileProvenienza);

                            if (transaction != null)
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    res = true;
                                }
                            }
                            else
                            {
                                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                                {
                                    res = true;
                                }
                            }
                        }
                    }
                }
            }

            return res;
        }

        #endregion
    }
}